<?php

namespace TeamSoft\CrmRepositoryBundle\Service;

use Doctrine\DBAL\Platforms\PostgreSQLPlatform;
use Doctrine\DBAL\Platforms\SQLServerPlatform;
use Doctrine\DBAL\Schema\Column;
use Doctrine\DBAL\Types\DateTimeType;
use Doctrine\ORM\EntityManagerInterface;

class DatabasePlatform
{
    private $em;
    private $dateTimeWithGmtOffset;

    /**
     * https://dev.mysql.com/doc/refman/8.0/en/integer-types.html
     */
    public const MAX_INTEGER = 2147483647;

    function __construct(EntityManagerInterface $em, DateTimeWithGmtOffset $dateTimeWithGmtOffset)
    {
        $this->em = $em;
        $this->dateTimeWithGmtOffset = $dateTimeWithGmtOffset;
    }

    public function isPostgreSQL100()
    {
        return ($this->em->getConnection()->getDatabasePlatform() instanceof PostgreSQLPlatform);
    }

    public function isSQLServer2008()
    {
        return ($this->em->getConnection()->getDatabasePlatform() instanceof SQLServerPlatform);
    }

    public function getIsNullFunctionExpression($expr1, $expr2)
    {
        if ($this->isSQLServer2008()) {
            return 'ISNULL(' . $expr1 . ', ' . $expr2 . ')';
        } else if ($this->isPostgreSQL100()) {
            return 'COALESCE(' . $expr1 . ', ' . $expr2 . ')';
        }
    }

    public function getIsNumericExpression($val)
    {
        if ($this->isSQLServer2008()) {
            return "ISNUMERIC($val)";
        } else if ($this->isPostgreSQL100()) {
            return "($val ~ '^-?\d+(\.\d+)?$')::int";
        }
    }

    public function getYearExpression($val)
    {
        if ($this->isSQLServer2008()) {
            return 'YEAR(' . $val . ')';
        } else if ($this->isPostgreSQL100()) {
            return "DATE_PART('year', $val)";
        }
    }

    public function getMonthExpression($val)
    {
        if ($this->isSQLServer2008()) {
            return 'MONTH(' . $val . ')';
        } else if ($this->isPostgreSQL100()) {
            return "DATE_PART('month', $val)";
        }
    }

    public function getDateFormatExpression(string $val, string $format)
    {
        if ($this->isSQLServer2008()) {
            return "FORMAT($val, $format)";
        } else if ($this->isPostgreSQL100()) {
            $format = str_replace('hh', 'hh24', $format);
            $format = str_replace('mm', 'mi', $format);
            return "TO_CHAR($val, '$format')";
        }
    }

    public function charIndexExpression($delim, $val)
    {
        if ($this->isSQLServer2008()) {
            return 'CHARINDEX(' . $delim . ',' . $val . ')';
        } else if ($this->isPostgreSQL100()) {
            return "strpos($val, $delim)";
        }
    }

    public function getTableExistsExpression($tableName)
    {
        if ($this->isSQLServer2008()) {
            return "OBJECT_ID('$tableName') IS NOT NULL";
        } else if ($this->isPostgreSQL100()) {
            return "to_regclass('public.$tableName') IS NOT NULL";
        }
    }

    public function dateToYearAndMonth($value)
    {
        return $this->getDateFormatExpression($value, 'yyyy-MM');
    }

    public function dateAddExpression($interval, $number, $date)
    {
        if ($this->isSQLServer2008()) {
            return 'DATEADD(' . $interval . ',' . $number . ',' . $date . ' )';
        } else if ($this->isPostgreSQL100()) {
            return "($date)::date + 1 * INTERVAL '$number $interval'";
        }
    }

    public function getIfThenElseExpression($condition, $then = '', $else = '')
    {
        if ($this->isSQLServer2008()) {
            $elseExpr = '';
            if ($else) {
                $elseExpr = "ELSE \nBEGIN $else \nEND";
            }
            return "IF \n$condition \nBEGIN \n$then \nEND \n$elseExpr";
        } else if ($this->isPostgreSQL100()) {
            $elseExpr = '';
            if ($else) {
                $elseExpr = "ELSE \n$else";
            }

            return "DO $$ BEGIN IF \n$condition \nTHEN \n$then \n $elseExpr \nEND IF; END $$;";
        }
    }

    public function getConcatOperator()
    {
        if ($this->isSQLServer2008()) {
            return '+';
        } else if ($this->isPostgreSQL100()) {
            return '||';
        }
    }

    /**
     * @return string
     * @deprecated  use getDatabasePlatform()->getLengthExpression() instead
     */
    public function getLengthOperator()
    {
        if ($this->isSQLServer2008()) {
            return 'len';
        } else if ($this->isPostgreSQL100()) {
            return 'length';
        }
    }

    public function getLikeExpression()
    { // TODO: Alex Kryvets, replace with custom solution
        if ($this->isSQLServer2008()) {
            return 'LIKE';
        } else if ($this->isPostgreSQL100()) {
            return 'ILIKE';
        }
    }

    public function escapeColumn($column)
    {
        if ($this->isSQLServer2008()) {
            return "[$column]";
        } else if ($this->isPostgreSQL100()) {
            return "\"$column\"";
        }
    }

    public function lastInsertId($name = null)
    {
        if ($this->isSQLServer2008()) {
            return $this->em->getConnection()->lastInsertId($name);
        } else if ($this->isPostgreSQL100()) {
            return $this->em->getConnection()->lastInsertId($name ? $name . '_id_seq' : $name);
        }
        return null;
    }

    public function generateGiud()
    {
        $result = null;
        if ($this->isSQLServer2008()) {
            $result = $this->em->getConnection()->executeQuery('SELECT newid()')->fetchOne();
        } else if ($this->isPostgreSQL100()) {
            $result = $this->em->getConnection()->executeQuery('SELECT gen_random_uuid()')->fetchOne();
        }
        return $result ? strtolower($result) : null;
    }

    public function generateCode()
    {
        $result = null;
        if ($this->isSQLServer2008()) {
            $result = $this->em->getConnection()->executeQuery('SELECT dbo.get_currentCode()')->fetchOne();
        } else if ($this->isPostgreSQL100()) {
            $result = $this->em->getConnection()->executeQuery('SELECT get_code()')->fetchOne();
        }
        return $result ? $result : null;
    }

    public function modifyOnlyLimitQuery($sql, $limit)
    {
        if ($this->isSQLServer2008()) {
            $selectPattern = '/^(\s*SELECT\s+(?:DISTINCT\s+)?)(.*)$/is';
            $replacePattern = sprintf('$1%s $2', "top $limit");
            return preg_replace($selectPattern, $replacePattern, $sql);
        } else if ($this->isPostgreSQL100()) {
            return "$sql LIMIT $limit";
        }
    }

    public function castColumnToType($column, $type)
    {
        if ($this->isPostgreSQL100()) {
            return $column . "::" . $type;
        }
        return $column;

    }

    public function getDateTimeWithGmtOffsetExpression($dateTimeExpression, $gmtOffsetExpression)
    {
        $dp = $this->em->getConnection()->getDatabasePlatform();
        $clientGmtOffset = $this->dateTimeWithGmtOffset->getClientGmtOffset();
        if (method_exists($dp, 'getDateTimeWithGmtOffsetExpression') && $clientGmtOffset) {
            return $dp->getDateTimeWithGmtOffsetExpression($dateTimeExpression, $gmtOffsetExpression, $clientGmtOffset);
        } else {
            return $dateTimeExpression;
        }
    }

    public function tableExists(string $tableName): bool
    {
        return $this->em->getConnection()->getSchemaManager()->tablesExist([$tableName]);
    }

    /**
     * @param string $tableName
     * @return string[]
     * @throws \Doctrine\DBAL\DBALException
     * @see getListTableColumns
     *
     * @since v19
     */
    public function getTableColumns(string $tableName): array
    {
        $stmt = $this->em
            ->getConnection()
            ->executeQuery('SELECT column_name FROM INFORMATION_SCHEMA.COLUMNS where table_name=?', [$tableName]);

        return $stmt->fetchFirstColumn();
    }

    /**
     * if need select from other database use `.` Example $tableName="db.employee"
     * @param string $tableName
     * @return Column[]
     */
    public function listTableColumns(string $tableName): array
    {
        return $this->em->getConnection()->getSchemaManager()->listTableColumns($tableName);
    }

    /**
     * conctructor "DROP TABLE IF EXIST" add to MSSQL from 2016 version
     * @param $tableName
     */
    public function dropTable($tableName)
    {
        $sql = $this->getIfThenElseExpression(
            $this->getTableExistsExpression($tableName),
            "DROP TABLE {$tableName};"
        );
        $this->em->getConnection()->executeStatement($sql);
    }

    public function getUtcDateTimeExpression()
    {
        if ($this->isPostgreSQL100()) {
            return "NOW()";
        }
        return "SYSUTCDATETIME()";
    }

    public function getDateExpression()
    {
        if ($this->isPostgreSQL100()) {
            return "CURRENT_DATE";
        }
        return "GETDATE()";
    }

    /**
     * @return string
     */
    public function getUtcCurrentTimestamp(): string
    {
        if ($this->isSQLServer2008()) {
            return 'GETUTCDATE()';
        }

        //postgres db setting in UTC timezone
        return 'CURRENT_TIMESTAMP';
    }

    /**
     * field is timestamp in db
     * if field is simple date use '=' instead
     * @param string $field
     * @param string $value (YYYY-mm-dd)
     * @return string
     *
     * @see https://stackoverflow.com/questions/22506930/how-to-query-datetime-field-using-only-date-in-microsoft-sql-server
     */
    public function getDateEqualExpression(string $field, string $value)
    {
        if ($this->isSQLServer2008()) {
            return "{$field} BETWEEN '{$value}' AND '{$value} 23:59:59'";
        }

        // postgres, mysql
        return "DATE({$field}) = '{$value}'";
    }

    public function convertDateTimeToPHPValue($value) {
        $dp = $this->em->getConnection()->getDatabasePlatform();
        return (new DateTimeType())->convertToPHPValue($value, $dp);
    }

    public function getDefaultDateTimeFormat() {
        if ($this->isSQLServer2008()) {
            return 'Y-m-d\TH:i:s';
        } else {
            return 'c';
        }
    }
}
