<?php

namespace TeamSoft\CrmRepositoryBundle\Service;


class GeoIp
{

    public function getRealIP()
    {
        if (isset($_SERVER['HTTP_CLIENT_IP'])) {
            $ip = $_SERVER['HTTP_CLIENT_IP'];
        } elseif (isset($_SERVER['HTTP_X_FORWARDED_FOR'])) {
            $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
        } else {
            $ip = $_SERVER['REMOTE_ADDR'];
        }
        return $ip;
    }

    public function getCountryCode() {
        //TODO: free and commercial databases which consist geodata according to ip address
        // https://dev.maxmind.com/geoip/geoip2/geolite2/
        // This package provides an API for the GeoIP2 web services and databases.
        // The API also works with the free GeoLite2 databases. https://github.com/maxmind/GeoIP2-php
        try {
            $response = json_decode(file_get_contents('http://api.ipapi.com/api/' . $this->getRealIP() . '?access_key=4b4ce566b3cdd2924c10acd612cb7fd3'), true);
            $countryCode = $response['country_code'];
        } catch (\Exception $e) {
            $countryCode = null;
        }

        return $countryCode;
    }
}
