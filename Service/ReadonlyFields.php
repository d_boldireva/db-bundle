<?php

namespace TeamSoft\CrmRepositoryBundle\Service;

use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;
use TeamSoft\CrmRepositoryBundle\Entity\InfoReadonlyfield;

class ReadonlyFields
{
    private $em;
    private $tokenStorage;

    function __construct(EntityManagerInterface $em, TokenStorageInterface $tokenStorage)
    {
        $this->em = $em;
        $this->tokenStorage = $tokenStorage;
    }

    public function get(int $page, $typeId = null, $stateId = null, $companyId = null, $roleId = null)
    {

        // TODO: refactor getting default roleId
        if (!$roleId) {
            /**
             * @var UsernamePasswordToken $token
             */
            $token = $this->tokenStorage->getToken();
            $user = $token ? $token->getUser() : null;
            if ($user) {
                $roleId = $user->getRole() ? $user->getRole()->getId() : null;
            }
        }

        $result = [];
        /**
         * @var \TeamSoft\CrmRepositoryBundle\Repository\InfoReadonlyfield $repository
         */
        $repository = $this->em->getRepository(InfoReadonlyfield::class);
        $requiredFields = $repository->findByPage($page, $typeId, $stateId, $roleId);
        if ($result) {
            $result = array_merge($requiredFields, $result);
        }
        return $result;
    }
}
