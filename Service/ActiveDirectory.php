<?php

namespace TeamSoft\CrmRepositoryBundle\Service;

class ActiveDirectory
{

    private $options;

    private $activeDirectoryConfig;

    public function __construct(Options $options)
    {
        $this->options = $options;
    }

    public function getConfig()
    {
        if (!$this->activeDirectoryConfig) {
            $this->activeDirectoryConfig = json_decode($this->options->get('activeDirectoryConfig'), true);
        }
        return $this->activeDirectoryConfig;
    }

    public function checkAuthentication($username, $password)
    {
        if ($activeDirectoryConfig = $this->getConfig()) {
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'POST');
            curl_setopt($ch, CURLOPT_POSTFIELDS, [
                'username' => $username,
                'password' => $password,
            ]);
            curl_setopt($ch, CURLOPT_HEADER, false);
            curl_setopt($ch, CURLOPT_HTTPHEADER, [
                'Authorization: Bearer ' . ($activeDirectoryConfig['jwt'] ?? ''),
            ]);
            curl_setopt($ch, CURLOPT_NOBODY, false);
            curl_setopt($ch, CURLOPT_URL, $activeDirectoryConfig['api']['url'] . '/user/check-auth');
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
            curl_exec($ch);
            $httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
            curl_close($ch);
            return $httpCode === 200;
        } else {
            throw new \InvalidArgumentException('Option "activeDirectoryConfig" is missing');
        }
    }

    public function getUserByIdentifier($identifier)
    {
        return $this->executeGetQuery('/user/' . $identifier);
    }

    public function getUserByUsername($username)
    {
        return $this->executeGetQuery('/user?username=' . $username);
    }

    public function getUsers()
    {
        return $this->executeGetQuery('/users');
    }

    private function executeGetQuery($url)
    {
        if ($activeDirectoryConfig = $this->getConfig()) {
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'GET');
            curl_setopt($ch, CURLOPT_HEADER, 0);
            curl_setopt($ch, CURLOPT_HTTPHEADER, [
                'Accept: application/json',
                'Content-Type: application/json',
                'Authorization: Bearer ' . ($activeDirectoryConfig['jwt'] ?? ''),
            ]);
            curl_setopt($ch, CURLOPT_URL, $activeDirectoryConfig['api']['url'] . $url);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
            $result = curl_exec($ch);
            curl_close($ch);
            return json_decode($result, true);
        } else {
            throw new \InvalidArgumentException('Option "activeDirectoryConfig" is missing');
        }
    }
}
