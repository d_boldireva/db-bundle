<?php

namespace TeamSoft\CrmRepositoryBundle\Service;

use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Asset\Packages;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\Routing\RouterInterface;
use Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface;
use Symfony\Component\ExpressionLanguage\Expression;
use TeamSoft\CrmRepositoryBundle\Entity\InfoService;
use TeamSoft\CrmRepositoryBundle\Entity\PoExternallink;
use TeamSoft\CrmRepositoryBundle\Model\WebService;
use Twig\Environment as Twig;

class WebServices
{
    private $twig;
    private $router;
    private $em;
    private $authorizationChecker;
    private $projectDir;
    /**
     * @var InfoService[]
     */
    private $services = [];

    /**
     * @var PoExternallink[]
     */
    private $externalLinks = [];

    protected $parameterBag;

    protected $packages;

    function __construct(
        ParameterBagInterface $parameterBag,
        Twig $twig,
        RouterInterface $router,
        AuthorizationCheckerInterface $authorizationChecker,
        EntityManagerInterface $em,
        $projectDir,
        ?Packages $packages
    )
    {
        $this->parameterBag = $parameterBag;
        //TODO: Remove this workaround, use only DI
        $this->packages = $parameterBag->has('assets.packages') ? $parameterBag->get('assets.packages') : $packages;
        $this->twig = $twig;
        $this->router = $router;
        $this->em = $em;
        $this->authorizationChecker = $authorizationChecker;
        $this->projectDir = $projectDir;

        $qb = $this->em->getRepository(InfoService::class)
            ->createQueryBuilder('s')
            ->join('s.webService', 'ws')
            ->addSelect('ws')
            ->orderBy('s.identifier', 'asc');

        $services = $qb->getQuery()->getResult();

        /**
         * @var InfoService $service
         */
        foreach ($services as $service) {
            $this->services[$service->getIdentifier()] = $service;
        }

        $this->externalLinks = $this->em->getRepository(PoExternallink::class)->findAll();
    }

    /**
     * @return WebService[]
     */
    public function getServices($all = null)
    {
        $services = [];

        /**
         * @var InfoService $service
         */
        foreach ($this->services as $service) {
            if ($webService = $service->getWebService()) {
                $allowIf = $this->parameterBag->get('db.web_services.' . $service->getIdentifier() . '.allow_if');
                $granted = $this->isGrantedForBundle($allowIf);
                if ($all || $service->getEnable() && $granted) {
                    $services[] = new WebService(
                        $service->getIdentifier(),
                        $service->getEnable(),
                        $webService->getOrder(),
                        $granted,
                        $webService->getTargetBlank(),
                        $service->getName(),
                        $service->getDescription(),
                        $this->getImageUrl($service->getIdentifier()),
                        $webService->getRoute(),
                        $webService->getUrl()
                    );
                }
            }
        }

        foreach ($this->externalLinks as $externalLink) {
            $enable = $externalLink->getEnable();
            $url = $enable ? $externalLink->getUrl() : null;

            if ($url) {
                $services[] = new WebService(
                    'external_link',
                    $enable,
                    null,
                    true,
                    $externalLink->getTargetBlank(),
                    $externalLink->getName(),
                    $externalLink->getDescription(),
                    null, // TODO: implement;
                    null,
                    $url
                );
            }
        }

        return $services;
    }

    public function get($identifier)
    {
        return $this->services[$identifier] ?? null;
    }

    private function isGrantedForBundle($allowIf)
    {
        if ($allowIf) {
            return $this->authorizationChecker->isGranted(new Expression($allowIf));
        } else {
            return false;
        }
    }

    private function getImageUrl($identifier)
    {
        $imagePath = 'images/bundles/team_soft_' . $identifier . '.png';
        return $this->assetExists($imagePath) ? $this->packages->getUrl($imagePath) : null;
    }

    private function assetExists($assetPath)
    {
        return file_exists($this->projectDir . '/public/' . $assetPath);
    }

//    private function translate($id, $locale = null)
//    {
//        $catalogue = $this->translator->getCatalogue($locale);
//        return $catalogue->defines($id) ? $this->translator->trans($id, [], null, $locale) : $id;
//    }

//    private function getLocale()
//    {
//        return $this->translator->getLocale();
//    }
}
