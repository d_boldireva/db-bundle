<?php

namespace TeamSoft\CrmRepositoryBundle\Service;

use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;
use TeamSoft\CrmRepositoryBundle\Entity\InfoRequired;
use TeamSoft\CrmRepositoryBundle\Entity\InfoTaskstate;
use TeamSoft\CrmRepositoryBundle\Entity\InfoTasktype;
use TeamSoft\CrmRepositoryBundle\Entity\InfoCompany;
use TeamSoft\CrmRepositoryBundle\Repository\InfoRequired as InfoRequiredRepository;

class RequiredFields
{
    private $em;
    private $tokenStorage;

    function __construct(EntityManagerInterface $em, TokenStorageInterface $tokenStorage)
    {
        $this->em = $em;
        $this->tokenStorage = $tokenStorage;
    }

    public function get(int $page, $typeId = null, $stateId = null, $companyId = null, $roleId = null)
    {

        // TODO: refactor getting default roleId
        if (!$roleId) {
            /**
             * @var UsernamePasswordToken $token
             */
            $token = $this->tokenStorage->getToken();
            $user = $token ? $token->getUser() : null;
            if ($user) {
                $roleId = $user->getRole() ? $user->getRole()->getId() : null;
            }
        }

        $requiredFields = [];
        /**
         * @var \TeamSoft\CrmRepositoryBundle\Repository\InfoRequired $requiredRepository
         */
        $requiredRepository = $this->em->getRepository(InfoRequired::class);
        $result = $requiredRepository->findByFilter($page, $typeId, $stateId, $roleId);
        if ($result) {
            $requiredFields = array_merge($requiredFields, $result);
        }
        if ($page === InfoRequiredRepository::TASK_PAGE) {
            $requiredFields = array_merge($requiredFields, $this->getTaskRequiredCollections($typeId, $stateId, $companyId));
        }
        return $requiredFields;
    }

    /**
     * @param integer $stateId
     * @param integer $taskTypeId
     * @param integer $companyId
     * @return array
     */
    private function getTaskRequiredCollections($taskTypeId, $stateId, $companyId)
    {
        $collections = [];

        if ($stateId && $taskTypeId) {
            /**
             * @var \TeamSoft\CrmRepositoryBundle\Entity\InfoTaskState $infoTaskState
             */
            $infoTaskState = $this->em->getRepository(InfoTaskstate::class)->find($stateId);

            /**
             * @var \TeamSoft\CrmRepositoryBundle\Entity\InfoTasktype $infoTaskType
             */
            $infoTaskType = $this->em->getRepository(InfoTasktype::class)->find($taskTypeId);

            if ($infoTaskState && $infoTaskState->getIsclosed() == 1 && $infoTaskState->getIsfinish() == 1) {

                if ($infoTaskType && $infoTaskType->getMinpromo()) {
                    $collections[] = $this->createRequiredField('info_taskmaterial', $infoTaskState);
                }

                if ($companyId) {
                    $company = $this->em->getRepository(InfoCompany::class)->find($companyId);
                    /**
                     * @var \TeamSoft\CrmRepositoryBundle\Entity\InfoCompanytype $companyType
                     */
                    $companyType = $company ? $company->getCompanytype() : null;


                    if ($companyType && $companyType->getIsshop()
                        && $infoTaskType && $infoTaskType->getIsshowmerch() == 1) {
                        $collections[] = $this->createRequiredField('info_companypreparation2', $infoTaskState);
                        $collections[] = $this->createRequiredField('info_taskpreparation', $infoTaskState);
                    }

                    if ($companyType && !$companyType->getIsshop()
                        && $infoTaskType && $infoTaskType->getIsshowdogov() == 1) {
                        $collections[] = $this->createRequiredField('info_contactpotential', $infoTaskState);
                    }
                }
            }
        }

        return $collections;
    }

    private function createRequiredField($name, InfoTaskstate $infoTaskState)
    {
        $requiredField = new InfoRequired;
        $requiredField->setName($name);
        $requiredField->setPage(InfoRequiredRepository::TASK_PAGE);
        $requiredField->setType('{' . $infoTaskState->getGuid() . '}');
        $requiredField->setGuid(null);
        $requiredField->setCurrenttime(new \DateTime);
        $requiredField->setModuser(null);
        return $requiredField;
    }
}
