<?php

namespace TeamSoft\CrmRepositoryBundle\Service;

use Doctrine\ORM\EntityManagerInterface;
use TeamSoft\CrmRepositoryBundle\Entity\InfoActivedirectorydictionary;
use TeamSoft\CrmRepositoryBundle\Model\ActiveDirectoryUser;

class ActiveDirectoryRepository
{

    private $activeDirectory;
    private $em;

    public function __construct(ActiveDirectory $activeDirectory, EntityManagerInterface $em)
    {
        $this->activeDirectory = $activeDirectory;
        $this->em = $em;
    }

    public function getUser($identifier)
    {
        $user = null;
        try {
            $user = $this->activeDirectory->getUserByIdentifier($identifier);
        } catch (\InvalidArgumentException $e) {
        }

        if ($user) {
            $user = $this->createUser($user);
        }
        return $user;
    }

    public function getUsers()
    {
        $users = $this->activeDirectory->getUsers();
        $result = null;
        if (is_array($users)) {
            $result = [];
            foreach ($users as $user) {
                $result[] = $this->createUser($user);
            }
        }
        return $result;
    }

    private function createUser($user)
    {
        /**
         * @var InfoActivedirectorydictionary $activeDirectoryDictionary
         */
        $activeDirectoryDictionary = isset($user['department']) ? $this->em->getRepository(InfoActivedirectorydictionary::class)->findOneBy([
            'departmentValue' => $user['department'],
        ]) : null;
        $activeDirectoryUser = new ActiveDirectoryUser(
            $user['employeeNumber'] ?? null,
            $user['cn'] ?? null,
            $user['mail'] ?? null,
            $user['mobile'] ?? null,
            $user['l'] ?? null,
            $activeDirectoryDictionary ? $activeDirectoryDictionary->getDirection() : null
        );
        return $activeDirectoryUser;
    }
}
