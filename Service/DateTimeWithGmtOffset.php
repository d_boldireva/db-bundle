<?php

namespace TeamSoft\CrmRepositoryBundle\Service;

use Doctrine\Common\Proxy\Proxy;
use Doctrine\DBAL\Platforms\PostgreSQL100Platform;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\RequestStack;
use TeamSoft\CrmRepositoryBundle\Entity\DateTimeWithGmtOffsetInterface;

class DateTimeWithGmtOffset
{
    private $em;
    private $clientGmtOffset;
    private $clientTimezoneName;
    private $showTasksWithGMTOffset;

    function __construct(EntityManagerInterface $em, RequestStack $requestStack, Options $options)
    {
        $this->em = $em;
        $request = $requestStack->getCurrentRequest();
        $clientGmtOffset = $request ? $request->headers->get("X-Gmt-Offset") : null;
        $this->clientTimezoneName = $request ? $request->headers->get("X-Timezone-Name") : null;
        $this->clientGmtOffset = is_numeric($clientGmtOffset) ? intval($clientGmtOffset) : null;
        $this->showTasksWithGMTOffset = $options->get('showTasksWithGMTOffset') == 1;
    }

    public function getClientGmtOffset () {
        return $this->enable() ? $this->clientGmtOffset : 0;
    }

    public function getClientTimezoneName () {
        return $this->enable() ? $this->clientTimezoneName : null;
    }

    public function tryModifyDateTimeEntity(DateTimeWithGmtOffsetInterface $entity, $setOriginal = true)
    {
        if ($this->enable()) {
            $entityGmtOffset = $entity->getGmtOffset();
            $dateTimeWithGmtOffsetPropertyNameList = $entity->getDateTimeWithGmtOffsetPropertyNameList();
            if (
                is_int($this->clientGmtOffset) && is_int($entityGmtOffset)
                && $this->clientGmtOffset !== $entityGmtOffset
                && is_array($dateTimeWithGmtOffsetPropertyNameList)
            ) {
                $gmtOffset = $this->clientGmtOffset - $entityGmtOffset;
                foreach ($dateTimeWithGmtOffsetPropertyNameList as $dateTimeWithGmtOffsetPropertyName) {
                    if ($entity instanceof Proxy) {
                        $reflectionClass = new \ReflectionClass(get_class($entity));
                        $entityClassName = $reflectionClass->getParentClass()->getName();
                    } else {
                        $entityClassName = get_class($entity);
                    }
                    $propertyReflection = new \ReflectionProperty($entityClassName, $dateTimeWithGmtOffsetPropertyName);
                    $propertyReflection->setAccessible(true);
                    $propertyValue = $propertyReflection->getValue($entity);
                    if ($propertyValue instanceof \DateTime && $propertyValue->getTimestamp() >= 0) {

                        if ($this->clientTimezoneName) {
                            $clientTimezone = new \DateTimeZone($this->clientTimezoneName);
                            $gmtOffset -= $clientTimezone->getOffset(new \DateTime) - $clientTimezone->getOffset($propertyValue);
                        }

                        $propertyValue = $this->addGmtOffset($propertyValue, $gmtOffset);
                        $propertyReflection->setValue($entity, $propertyValue);
                        if ($setOriginal) {
                            $this->em->getUnitOfWork()->setOriginalEntityProperty(spl_object_hash($entity), $dateTimeWithGmtOffsetPropertyName, $propertyValue);
                        }
                    }
                }
            }
        }
    }

    public function tryModifyGmtOffsetEntity(DateTimeWithGmtOffsetInterface $entity, $setOriginal = true)
    {
        if ($this->enable()) {
            $entityGmtOffset = $entity->getGmtOffset();
            $dateTimeWithGmtOffsetPropertyNameList = $entity->getDateTimeWithGmtOffsetPropertyNameList();
            if (
                is_int($this->clientGmtOffset)
                && $this->clientGmtOffset !== $entityGmtOffset
                && is_array($dateTimeWithGmtOffsetPropertyNameList)
            ) {
                $entity->setGmtOffset($this->clientGmtOffset);
                foreach ($dateTimeWithGmtOffsetPropertyNameList as $dateTimeWithGmtOffsetPropertyName) {
                    $propertyReflection = new \ReflectionProperty(get_class($entity), $dateTimeWithGmtOffsetPropertyName);
                    $propertyReflection->setAccessible(true);
                    $propertyValue = $propertyReflection->getValue($entity);
                    $this->em->getUnitOfWork()->propertyChanged($entity, $dateTimeWithGmtOffsetPropertyName, null, $propertyValue);
                }
            }
        }
    }

    public function tryModifyDateTime(\DateTime $dateTime, int $dateTimeGmtOffset)
    {
        if ($this->enable()) {
            if (is_int($this->clientGmtOffset) && $dateTime->getTimestamp() >= 0) {
                $this->addGmtOffset($dateTime, $this->clientGmtOffset - $dateTimeGmtOffset);
            }
        }
        return $dateTime;
    }

    private function enable()
    {
        $dp = $this->em->getConnection()->getDatabasePlatform();
        return $dp instanceof PostgreSQL100Platform && $this->showTasksWithGMTOffset;
    }

    private function addGmtOffset(\DateTime $dateTime, int $gmtOffset)
    {
        $dateTime->add(\DateInterval::createFromDateString(-$gmtOffset . ' second'));
        return $dateTime;
    }
}
