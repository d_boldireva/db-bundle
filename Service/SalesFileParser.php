<?php

namespace TeamSoft\CrmRepositoryBundle\Service;

use Doctrine\DBAL\DBALException;
use Doctrine\DBAL\Driver\PDOException;
use Doctrine\ORM\EntityManagerInterface;
use PhpOffice\PhpSpreadsheet\Exception;
use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\Reader\IReader;
use PhpOffice\PhpSpreadsheet\RichText\RichText;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Worksheet\Worksheet;
use Psr\Log\LoggerInterface;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\Filesystem\Exception\IOException;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use TeamSoft\CrmRepositoryBundle\Entity\Sales\InfoSalesfile;
use TeamSoft\CrmRepositoryBundle\Entity\Sales\PsUploadedfiles;
use TeamSoft\CrmRepositoryBundle\Model\Chunk;

class SalesFileParser
{
    /**
     * @var EntityManagerInterface
     */
    private $em;
    /**
     * @var DatabasePlatform
     */
    private $dp;
    /**
     * @var LoggerInterface
     */
    private $logger;

    /** @var Options */
    private $options;

    /** @var Filesystem */
    private $filesystem;

    protected $parameterBag;

    private $maxSize;

    const BATCH_SIZE = 8000;
    const INSERT_BATCH = 64;
    const FIELD_SIZE = 1025;

    private $cellLimit = 0;
    private $rowLimit = 0;
    private $cellsPerRow = 100;

    /**
     * Parser constructor.
     * @param EntityManagerInterface $em
     * @param DatabasePlatform $dp
     */
    public function __construct(
        EntityManagerInterface $em,
        DatabasePlatform $dp,
        LoggerInterface $logger,
        Options $options,
        Filesystem $filesystem,
        ParameterBagInterface $parameterBag
    )
    {
        $this->em = $em;
        $this->dp = $dp;
        $this->logger = $logger;
        $this->options = $options;
        $this->filesystem = $filesystem;
        $this->parameterBag = $parameterBag;

        ini_set('memory_limit', '1G');
        $memory_limit = ini_get('memory_limit');

        if (preg_match('/^(\d+)(\w+)$/', $memory_limit, $matches)) {
            if (in_array($matches[2], ['G', 'Gb', 'GB'])) {
                $memory_limit = $matches[1] * 1024 * 1024 * 1024; // nnnM -> nnn MB
            } else if (in_array($matches[2], ['M', 'm', 'MB', 'mb', 'Mb'])) {
                $memory_limit = $matches[1] * 1024 * 1024; // nnnM -> nnn MB
            } else if (in_array($matches[2], ['K', 'Kb', 'KB'])) {
                $memory_limit = $matches[1] * 1024; // nnnK -> nnn KB
            }
        }

        $this->cellLimit = (round(0.9 * $memory_limit / 1024)) - 1;
        $this->maxSize = min(ini_get('upload_max_filesize'), ini_get('post_max_size'));
    }

    /**
     * @param InfoSalesfile $salesFile
     * @param null $sheetName
     * @return array
     * @throws \Exception
     */
    public function getRealHeaders(InfoSalesfile &$salesFile, $sheetName = null)
    {
        $headers = [];

        $topRows = $this->getRows($salesFile, 50, 0, $sheetName);

        foreach ($topRows as $rowIndex => $row) {
            foreach ($row as $cellIndex => $cell) {
                if ($cell && !isset($headers[$cellIndex])) {
                    $headers[$cellIndex] = $cell;
                }
            }
        }

        return $headers;
    }

    /**
     * @param InfoSalesfile $salesFile
     * @param null $sheetName
     * @return array
     * @throws \Exception
     */
    public function GetHeaders(InfoSalesfile &$salesFile, $sheetName = null)
    {
        $headers = [];

        if ($worksheet = $this->openWorksheet($salesFile, $sheetName)) {
            $columnIterator = $worksheet->getColumnIterator();
            while ($columnIterator->valid()) {
                $headers[] = $columnIterator->current()->getColumnIndex();
                $columnIterator->next();
            }
        }

        $spreadsheet = $worksheet->getParent();
        $this->unloadFile($spreadsheet);

        $this->cellsPerRow = count($headers);

        return $headers;
    }

    /**
     * @param InfoSalesfile $salesFile
     * @param int $limit
     * @param int $offset
     * @param null $sheetName
     * @return array
     * @throws \Exception
     */
    public function getRows(InfoSalesfile $salesFile, $limit = 20, $offset = 0, $sheetName = null)
    {
        $rows = [];

        $worksheet = $this->openWorksheet($salesFile, $limit, $offset, $sheetName);

        $top = $offset + $limit;
        $highestRow = $worksheet->getHighestRow();

        if ($offset > $highestRow) {
            return $rows;
        }

        $rowIterator = $worksheet->getRowIterator($offset, (($highestRow < $top) ? $highestRow : $top));
        while ($rowIterator->valid()) {
            $current = $rowIterator->current();

            $row = [];

            $cellIterator = $current->getCellIterator();
            while ($cellIterator->valid()) {
                $cell = $worksheet->getCell($cellIterator->key() . $rowIterator->key(), false);

                if ($cell) {
                    $cell = trim($cell->getValue() ?? $cell->getFormattedValue());
                }
                $row[] = $cell;

                unset($cell);

                $cellIterator->next();
            }
            $cellIterator->rewind();

            $rows[$rowIterator->key()] = $row;

            $rowIterator->next();
        }

        $spreadsheet = $worksheet->getParent();
        $this->unloadFile($spreadsheet);
        return $rows;
    }

    protected function openWorksheet(InfoSalesfile $salesFile, $limit = 20, $offset = 0, $sheetName = null)
    {
        /**
         * @var Worksheet|null
         */
        $worksheet = null;

        $spreadsheet = $this->loadFile($salesFile, $limit, $offset);

        if ($sheetName) {
            if (in_array($sheetName, $spreadsheet->getSheetNames())) {
                $worksheet = $spreadsheet->getSheetByName($sheetName);
            }
        } else {
            $worksheet = $spreadsheet->getActiveSheet();
        }
        return $worksheet;
    }

    /**
     * @param InfoSalesfile $salesFile
     * @param int $limit
     * @param int $offset
     * @return Spreadsheet|null
     * @throws \Exception
     */
    protected function loadFile(InfoSalesfile $salesFile, $limit = 20, $offset = 0)
    {
        ini_set('max_execution_time', 0);
        set_time_limit(0);
        /**
         * @var Spreadsheet|null $spreadSheet
         */
        $spreadSheet = null;
        $path = $this->getPathToSavePatternDirectory() . DIRECTORY_SEPARATOR . $salesFile->getName();
        $reader = $this->createReader($path);

        if (method_exists($reader, 'getReadFilter')) {
            /**
             * @var Chunk $readFilter
             */
            $readFilter = $reader->getReadFilter();
            $readFilter->setRows($offset, $limit);
        } else {
            $this->logger->debug('exceptions.parse.loading_file_in_full', ['fileName' => $salesFile->getFile()->getRealPath()]);
        }

        if (method_exists($reader, 'setDelimiter')) {
            $reader->setDelimiter(";");
            $this->logger->debug('exceptions.parse.file_delimiter_set_to_semicolon', ['fileName' => $salesFile->getFile()->getRealPath()]);
        }

        try {
            $spreadSheet = $reader->load($path);
        } catch (Exception $exception) {
            throw new \Exception('exceptions.parse.error_loading_spreadsheets');
        }

        return $spreadSheet;
    }

    /**
     * @param Spreadsheet $spreadsheet
     * @return Spreadsheet
     * @deprecated
     */
    protected function unloadFile(Spreadsheet &$spreadsheet)
    {
        $spreadsheet->disconnectWorksheets();
        $spreadsheet->garbageCollect();

        return $spreadsheet;
    }

    /**
     * @param InfoSalesfile $salesFile
     * @return IReader|null
     * @throws \Exception
     */
    protected function createReader(string $path)
    {
        try {
            /**
             * @var IReader|null $reader
             */
            $reader = IOFactory::createReaderForFile($path);
        } catch (Exception $exception) {
            throw new \Exception('exception.parse.unable_to_read_file');
        }

        if (!$reader->canRead($path)) {
            throw new \Exception('exception.parse.unable_to_read_file');
        }

        if (method_exists($reader, 'setReadFilter')) {
            $filter = new Chunk();
            $reader->setReadFilter($filter);
        } else {
            $this->logger->debug('exception.parse.unable_to_run_chunk_load', ['fileName' => $path]);
        }

        if (method_exists($reader, 'setReadDataOnly')) {
            $reader->setReadDataOnly(true);
        } else {
            $this->logger->debug('exception.parse.unable_to_run_in_data_only', ['fileName' => $path]);
        }

        return $reader;
    }

    /**
     * @param InfoSalesfile $salesFile
     * @param string $tableName
     * @return string
     * @throws \Doctrine\DBAL\DBALException
     */
    protected function createTableForFile(InfoSalesfile &$salesFile, string $tableName)
    {
        $headers = $this->getRealHeaders($salesFile);

        $headers = array_map(function ($header) {
            return $this->dp->escapeColumn($header) . ($this->dp->isPostgreSQL100() ? ' text' : ' VARCHAR(' . static::FIELD_SIZE . ')');
        }, $headers);

        if ($this->dp->isPostgreSQL100()) {
            $headers[] = $this->dp->escapeColumn('id') . ' SERIAL PRIMARY KEY';
        } else {
            $headers[] = $this->dp->escapeColumn('id') . ' INT NOT NULL IDENTITY(1,1)';
            $headers[] = 'PRIMARY KEY(' . $this->dp->escapeColumn('id') . ')';
        }

        $sql = 'CREATE TABLE ' . $this->dp->escapeColumn($tableName) . ' (' . implode(',', $headers) . ')';

        $this->em->getConnection()->executeStatement($sql);

        return $tableName;
    }

    /**
     * Gererate random table
     *
     * Attraction.
     * MSSQL max table length 128 and throw exception, Postgress max length 63 and will be save only first 63
     * strlen(sha1(random_bytes(12)));  // 40
     *
     * @param string $prefix
     * @param string $suffix
     *
     * @return string
     * @throws \RuntimeException
     */
    public function getRandomTableName(string $prefix = '', string $suffix = ''): string
    {
        $protectionFromInfinity = 0;
        do {
            $tableName = $prefix . sha1(random_bytes(12)) . $suffix;
            if ($protectionFromInfinity++ > 5) {
                throw new \RuntimeException(sprintf('Can not create new table for pharma_sales. Perhaps %s:tableExists() work wrong', get_class($this->dp)));
            }
        } while ($this->dp->tableExists($tableName));

        return $tableName;
    }

    /**
     * @param string $tableName
     * @param string[] $colums
     * @throws \LogicException
     * @throws DBALException
     */
    public function createTable(string $tableName, array $colums): void
    {
        $colums = array_filter($colums);

        if (!$colums) {
            throw new \LogicException('Columns can not be blank. Create or check your validator');
        }

        $headers = [];
        if ($this->dp->isPostgreSQL100()) {
            foreach ($colums as $colum) {
                $headers[] = $this->dp->escapeColumn($colum) . ' text';
            }
        } else {
            foreach ($colums as $colum) {
                $headers[] = $this->dp->escapeColumn($colum) . ' VARCHAR(' . static::FIELD_SIZE . ')';
            }
        }


        $sql = 'CREATE TABLE ' . $this->dp->escapeColumn($tableName) . ' (' . implode(',', $headers) . ')';
        $this->em->getConnection()->executeStatement($sql);

        if ($this->dp->isPostgreSQL100()) {
            $this->em->getConnection()->executeStatement("alter table {$this->dp->escapeColumn($tableName)} owner to postgres");
        }
    }

    /**
     * @param string $tableName
     * @throws DBALException
     */
    public function addPrimeryKey(string $tableName): void
    {
        $tn = $this->dp->escapeColumn($tableName);
        $tbPk = $this->dp->escapeColumn($tableName . '_pk');

        if ($this->dp->isSQLServer2008()) {
            $this->em->getConnection()->executeStatement("ALTER TABLE {$tn} ADD id int identity");
        } else {// Postgres
            $this->em->getConnection()->executeStatement("ALTER TABLE {$tn} ADD id serial");
        }

        $this->em->getConnection()->executeStatement("ALTER TABLE  {$tn} ADD CONSTRAINT {$tbPk} PRIMARY KEY (id)");

//        if ($this->dp->isSQLServer2008()) {
//            $this->em->getConnection()->executeStatement(" DBCC CHECKIDENT('{$tn}')");
//        }
    }

    /**
     * Insert data from csv file
     *
     * @param string $tableName
     * @param string $fullPathToFile
     * @throws DBALException
     * @throws \RuntimeException
     */
    public function insertDataFromCsvFile(string $tableName, string $fullPathToFile)
    {
        if (!$this->dp->tableExists($tableName)) {
            throw new \RuntimeException(sprintf('Table "%s" not exist', $tableName));
        }

        if (!file_exists($fullPathToFile)) {
            //TODO parse file name
            throw new \RuntimeException(sprintf('File "%s" not exist', $fullPathToFile));
        }

        $scapeTableName = $this->dp->escapeColumn($tableName);

        if ($this->dp->isSQLServer2008()) {
            // FIELDQUOTE sinse MSSQL 2017
            // FORMAT = 'CSV',
            // FIELDQUOTE = '\"',

            // ROWTERMINATOR = '0x0a'
            // ROWTERMINATOR = '\n'
            $this->em->getConnection()->executeStatement(
                "BULK INSERT {$scapeTableName}
FROM '{$fullPathToFile}'
WITH
(
    FIRSTROW = 2,
    DATAFILETYPE = 'char',
    FIELDTERMINATOR = '{$this->getDelimiter()}',  --CSV field delimiter
    ROWTERMINATOR = '\n',   --Use to shift the control to next row
    TABLOCK
)"
            );
        } else {
            $this->em->getConnection()->executeStatement(
                "COPY {$scapeTableName} FROM '{$fullPathToFile}' DELIMITER '{$this->getDelimiter()}' CSV HEADER"
            );
        }

    }

    /**
     * @param InfoSalesfile $salesFile
     * @param string $tableName
     * @return int
     * @throws \Doctrine\DBAL\DBALException
     */
    protected function populateTableForFile(InfoSalesfile &$salesFile, string $tableName)
    {
        $startIndex = 1;
        $count = $startIndex;

        $rowChunkSize = round($this->cellLimit / $this->cellsPerRow);

        $headers = $this->getRealHeaders($salesFile);
        $columnCount = count($headers);

        $hasHeader = true;

        while ($rows = $this->getRows($salesFile, $rowChunkSize, $count)) {
            $rowCount = count($rows);
            $count += $rowCount;

            $firstElement = reset($rows);
            $cellCount = $rowCount * count($firstElement);
            $chunksCount = ($cellCount / 2000 + 1);
            $chunkSize = round($rowCount / $chunksCount);

            $connection = $this->em->getConnection();

            foreach (array_chunk($rows, $chunkSize) as $chunk) {
                $connection->beginTransaction();
                foreach ($chunk as $rowId => $row) {
                    if (!implode('', $row)) {
                        continue;
                    } else if ($hasHeader) {
                        $hasHeader = false;
                        continue;
                    }

                    $values = [];

                    $placeholders = array_fill(0, count($headers), '?');

                    $sql = sprintf('INSERT INTO %s VALUES (%s)', $this->dp->escapeColumn($tableName), implode(',', $placeholders));

                    $statement = $connection->prepare($sql);

                    foreach ($row as $cellId => $cell) {
                        if (array_key_exists($cellId, array_keys($headers))) {
                            $values[] = $cell;
                        }
                    }

                    try {
                        $statement->execute($values);
                    } catch (PDOException $e) {
                        $connection->rollback();
                        echo $e->getMessage() . PHP_EOL;
                    }
                }

                $connection->commit();
            }
        }

        return $count;
    }

    /**
     * @param InfoSalesfile $salesFile
     * @return int
     * @throws \Doctrine\DBAL\DBALException
     * @throws \PhpOffice\PhpSpreadsheet\Exception
     * @throws \PhpOffice\PhpSpreadsheet\Reader\Exception
     */
    public function parseFile(InfoSalesfile &$salesFile)
    {
        $tableName = "ps_{$salesFile->getName()}";

        $this->removeAssociatedTable($salesFile);

        if ($this->createTableForFile($salesFile, $tableName)) {
            $salesFile->setTableName($tableName);
            $totalRows = $this->populateTableForFile($salesFile, $tableName);
        }

        unset($sheet);

        return $totalRows;
    }

    /**
     * @param InfoSalesfile $salesFile
     * @return array
     * @throws \Exception
     */
    public function previewColumns(InfoSalesfile &$salesFile)
    {
        return $this->getRealHeaders($salesFile);
    }

    /**
     * @param InfoSalesfile $salesFile
     * @param int $limit
     * @return array
     * @throws \Exception
     */
    public function previewFile(InfoSalesfile &$salesFile, $limit = 20)
    {
        $headers = $this->getRealHeaders($salesFile);
        $rows = $this->getRows($salesFile, $limit, $offset = 1);

        $data = [];

        foreach ($rows as $rowKey => $row) {
            $_row = [];
            foreach ($headers as $key => $header) {
                if ($header instanceof RichText) {
                    $header = $header->getPlainText();
                }
                $_row[trim($header)] = $row[$key];
            }
            $data[$rowKey - 1] = $_row;
        }

        unset($rows);

        return $data;
    }

    public function removeAssociatedTable(InfoSalesfile &$salesFile)
    {
        if ($salesFile->getTableName()) {
            if ($this->dp->isPostgreSQL100()) {
                $this->em->getConnection()->executeStatement($sql = 'DROP TABLE IF EXISTS ' . $salesFile->getTableName());
            } else {
                $this->em->getConnection()->executeStatement($sql = "IF OBJECT_ID(N'[{$salesFile->getTableName()}]','U') IS NOT NULL DROP TABLE [{$salesFile->getTableName()}]");
            }
            $salesFile->setTableName(null);
        }
    }

    /**
     * @param InfoSalesfile $salesFile
     * @return array
     * @throws \Doctrine\DBAL\DBALException
     * @throws \PhpOffice\PhpSpreadsheet\Exception
     * @throws \PhpOffice\PhpSpreadsheet\Reader\Exception
     */
    public function getParsedColumns(InfoSalesfile $salesFile)
    {
        if (!$salesFile->getTableName()) {
            $this->parseFile($salesFile);
        }

        $row = $this->em->getConnection()->fetchAssociative(
            $this->dp->modifyOnlyLimitQuery('
                SELECT * FROM ' . $this->dp->escapeColumn($salesFile->getTableName()), 1
            )
        );

        return array_keys($row);
    }

    /**
     * @param InfoSalesfile $salesFile
     * @param int $limit
     * @param int $offset
     * @return array|false|mixed[]
     * @throws \Doctrine\DBAL\DBALException
     * @throws \PhpOffice\PhpSpreadsheet\Exception
     * @throws \PhpOffice\PhpSpreadsheet\Reader\Exception
     */
    public function getParsedData(InfoSalesfile $salesFile, $limit = 20, $offset = 1)
    {
        if (!$salesFile->getTableName()) {
            $this->parseFile($salesFile);
        }

        $sql = $this->dp->modifyOnlyLimitQuery(
            'SELECT * FROM ' . $this->dp->escapeColumn($salesFile->getTableName()), $limit + $offset
        );

        $data = $this->em->getConnection()->fetchAllAssociative($sql);

        return array_slice($data, $offset - 1, $limit);
    }

    /**
     * @param InfoSalesfile $salesFile
     * @return bool
     */
    public function isCSV(InfoSalesfile $salesFile)
    {
        return ($salesFile->getFile()->getClientOriginalExtension() ?: $salesFile->getFile()->getExtension()) === 'csv';
    }

    /**
     * @param InfoSalesfile $salesFile
     * @return InfoSalesfile
     * @deprecated
     */
    public function convertToUTF8(InfoSalesfile $salesFile)
    {
        $handle_source = fopen($salesFile->getFile()->getPathname(), 'r');
        $handle_target = fopen($salesFile->getFile()->getPathname() . '.converted', 'w+');

        while (($line = fgets($handle_source)) !== false) {
            fwrite($handle_target, $line);
        }

        fclose($handle_source);
        fclose($handle_target);

        $handle_source = fopen($salesFile->getFile()->getPathname() . '.converted', 'r');
        $handle_target = fopen($salesFile->getFile()->getPathname(), 'w');

        while (($line = fgets($handle_source)) !== false) {
            fwrite($handle_target, iconv('windows-1251', 'utf-8', $line));
        }

        fclose($handle_source);
        fclose($handle_target);

        $salesFile->setFile(new UploadedFile($salesFile->getFile()->getPathname(), $salesFile->getFile()->getClientOriginalName()));

        return $salesFile;
    }

    /**
     * @param string $fullPathToFileEncode
     * @param string $fullPathToFileUTF16
     * @return bool
     */
    public function convertToUTF16(string $fullPathToFileEncode, string $fullPathToFileUTF16): bool
    {
        $copy = file_get_contents($fullPathToFileEncode);
        $decode = iconv("UTF-8", "UTF-16LE", $copy);

        return file_put_contents($fullPathToFileUTF16, pack("S", 0xfeff) . $this->chbo($decode)) > 0;
    }

    protected function chbo($num)
    {
        $data = dechex($num);
        if (strlen($data) <= 2) {
            return $num;
        }
        $u = unpack("H*", strrev(pack("H*", $data)));
        $f = hexdec($u[1]);

        return $f;
    }

    /**
     * @return string
     * @throws \RuntimeException
     */
    public function getPathToSavePatternDirectory(): string
    {
        return $this->parameterBag->get('app.tmp_dir');
    }

    /**
     * @param InfoSalesfile $salesfile
     * @return string
     */
    public function getPathToSavePatternFile(InfoSalesfile $salesfile): string
    {
        if (!$salesfile->isPattern()) {
            return '';
        }

        return $this->getPathToSavePatternDirectory() . DIRECTORY_SEPARATOR . $salesfile->getName();
    }

    /**
     * Return path to directory where need save .csv. See param "team_soft_pharma_sales.sales_ftp_path"
     * @param $date
     * @return string
     */
    public function getPathToSaveImportDirectory(\DateTime $date): string
    {
        $salesFtpPath = $this->parameterBag->get('team_soft_pharma_sales.sales_ftp_path');

        if (!$salesFtpPath) {
            return '';
        }

        $ds = DIRECTORY_SEPARATOR;
        $salesFtpPath = rtrim($salesFtpPath, $ds);
        $databaseName = $this->em->getConnection()->getDatabase();
        $path = $salesFtpPath . $ds . strtoupper($databaseName) . $ds . $date->format('Y') . $ds . $date->format('m');

        if (!$this->createDirectory($path)) {
            throw new \RuntimeException("Failed to create directory \"$path\". Unknown reasons");
        }

        return $path;
    }

    public function getPathToSaveImportFile(PsUploadedfiles $psUploadedfiles): string
    {
        $path = $this->getPathToSaveImportDirectory($psUploadedfiles->getCreatedate());
        if (!$path) {
            return '';
        }

        return $path . DIRECTORY_SEPARATOR . $psUploadedfiles->getName();
    }

    /**
     * @param string $path
     * @param int $mode
     * @return bool
     * @throws IOException
     */
    protected function createDirectory($path, $mode = 0775): bool
    {
        $this->filesystem->mkdir([$path], $mode);

        return true;
    }

    public function getMaxSize()
    {
        return $this->maxSize;
    }

    public function getMaxSizeInBytes(): int
    {
        return $this->stringToBytes($this->getMaxSize());
    }

    private function stringToBytes($val): int
    {
        $val = trim($val);

        if (is_numeric($val)) {
            return (int)$val;
        }

        if (!$val) {
            return 0;
        }

        $last = strtolower($val[strlen($val) - 1]);
        $val = (int)substr($val, 0, -1); // necessary since PHP 7.1; otherwise optional

        switch ($last) {
            case 'g':
                $val *= 1024;
            case 'm':
                $val *= 1024;
            case 'k':
                $val *= 1024;
        }

        return $val;
    }

    /**
     * support for mssql 2008-2016 version
     * attention postgre support only one symbol
     * @return string
     */
    public function getDelimiter(): string
    {
        if ($this->dp->isPostgreSQL100()) {
            return ',';
        }

        return '~%~';
    }
}
