<?php

namespace TeamSoft\CrmRepositoryBundle\Service;

use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Query\Expr\Join;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use TeamSoft\CrmRepositoryBundle\Entity\InfoOptions;
use TeamSoft\CrmRepositoryBundle\Entity\InfoOptionsbyrole;
use TeamSoft\CrmRepositoryBundle\Entity\InfoRole;
use TeamSoft\CrmRepositoryBundle\Entity\InfoUser;

class Options
{
    /**
     * format: json
     * example: {"2": "L","3": "M","4": "H","5": "VH","6": "VH+","default": "-"}
     * if not empty rewrite default categories
     */
    public const OPTION_TARGETING_CATEGORY_NAME  = 'targetingCategoriesNames';

    /**
     * format: integer
     * enables to change approved category in targeting
     */
    public const OPTION_TARGETING_CAN_CHANGE_CATEGORY = 'targetingCanChangeCategory';

    /**
     * format: 0 | 1 | null
     * if activate check if target company/contact belong user in target across info_userbrick
     * if model (info_userbrick) not found rewrite status
     */
    public const OPTION_TARGETING_CHECK_STATUS_ON_USER_BRICK = 'targetingCheckStatusOnUserBrick';

    /**
     * TODO add description what this option doing
     */
    public const OPTION_CAN_USERS_VISIBILITY_BY_POSITIONS = 'canUsersVisibilityByPositions';

    /**
     * TODO add description what this option doing
     */
    public const OPTION_VISIBLE_REGION = 'visibleregion';

    /**
     * format: integer
     * how many months shows tasks and mcm for contact
     */
    public const OPTION_CONTACT_VISITS_HISTORY_DEPTH = 'contactVisitsHistoryDepth';

    /**
     * format: integer
     * if ON join table info_contactcateg|info_companycateg across info_companycategory|info_contactcategory
     * if off join table info_contactcateg|info_companycateg straight Category_id
     */
    public const OPTION_USE_CATEGORY_WITH_DIRECTION = 'useCategoryWithDirection';

    /**
     * format: integer
     * if the option is enabled, the password for each user is changes once in the number of days specified in the option
     */
    public const OPTION_DAYS_INTERVAL_FOR_PASSWORD_CHANGE = 'daysIntervalForPasswordChange';

    public const OPTION_SHOW_GRID_LAST_TASK_DATE = 'show_grid_last_task_date';

    /**
     * @todo add description
     * format: integer (boolean)
     */
    public const OPTION_SHOW_DCR_REQUESTS_BY_SUBMISSION = 'showDCRRequestsBySubmission';

    /**
     * @todo add description
     * format: integer (boolean)
     */
    public const OPTION_SEARCH_FOR_COMPANY_REQUESTS_BY_MODIFIER_ID = 'searchForCompanyRequestsByModifier_id';

    /**
     * format: integer
     * allows the possibility of multilingual in the content
     */
    public const MCM_IS_MULTILINGUAL_CONTENT_ON = 'MCMIsMultilingualContentOn';

    /**
     * format: integer
     * maximum number of emails per week
     */
    public const MCM_EMAIL_WEEK_LIMIT = 'MCMEmailWeekLimit';

    /**
     * format: integer
     * maximum number of emails per month
     */
    public const MCM_EMAIL_MONTH_LIMIT = 'MCMEmailMonthLimit';

    /**
     * format: integer
     * maximum number of sms per week
     */
    public const MCM_SMS_WEEK_LIMIT = 'MCMSmsWeekLimit';

    /**
     * format: integer
     * maximum number of sms per month
     */
    public const MCM_SMS_MONTH_LIMIT = 'MCMSmsMonthLimit';

    /**
     * format: integer
     * allows sending messages with a link to the webinar from the table McmWebinarcontact
     */
    public const LINK_FROM_MCM_WEBINAR_CONTACT = 'linkFromMcmWebinarContact';

    /**
     * format: integer
     * enables the ability to send invitations for video calls
     */
    public const MCM_INVITE = 'MCMInvite';

    /**
     * format: integer
     * maximum number of sms
     */
    public const MCM_LINK_LIMIT = 'MCMLinkLimit';

    /**
     * format: string
     * period type for sms limit
     * values: 'day', 'week', 'month', 'quarter'
     */
    public const MCM_LINK_LIMIT_PERIOD = 'MCMLinkLimitPeriod';

    /**
     * format: 0 | 1
     * enable FTE calculation form in geoforce
     */
    public const GEOMARKETING_ENABLE_FTE = 'geomarketingEnableFTE';

    /**
     * format: string
     * email recipients
     * examples: default@hhh.com,qwerty@ddd.ua,...
     */
    public const MCM_SENDING_CONFIRMATION_SCHEDULER_RECIPIENTS = 'MCMSendingConfirmationSchedulerRecipients';

    /**
     * format: integer, second
     * timezone relative to gmt
     * examples: 7200 or 10800 or ...
     */
    public const MCM_SENDING_CONFIRMATION_SCHEDULER_GMT = 'MCMSendingConfirmationSchedulerGMT';

    /**
     * format: string
     * the times that email should come
     * examples: 4:30,16:30,12:00,...
     */
    public const MCM_SENDING_CONFIRMATION_SCHEDULER_TIMES = 'MCMSendingConfirmationSchedulerTimes';

    /**
     * format: integer
     * for API change radio input into checkbox on page manage versions
     * examples: 0,1
     */
    public const GEO_MARKETING_USE_MULTI_VERSION = 'geomarketingUseMultiVersion';

    /**
     * format: integer
     * enables sending SMS through the MTS client
     */
    public const MCM_USE_FOR_SEND_SMS_MTS_CLIENT = 'MCMUseForSendSMSMTSClient';

    /**
     * format: integer
     * enables add to result phone and email data
     */
    public const MCM_CONTACT_BY_TOKEN_ADD_PHONE_EMAIL = 'MCMContactByTokenAddPhoneEmail';

    /**
     * format: 0|1
     * enables build pie chart for phone and email data
     */
    public const GEO_MARKETING_ENABLE_PIE_CHART_TEL_WITH_EMAIL = 'geomarketingPieChartTelWithEmail';

    /**
     * format: 0|1
     * enables build pie chart for viber and WhatsApp applications
     */
    public const GEO_MARKETING_ENABLE_PIE_CHART_VIBER_WITH_WHATS_APP = 'geomarketingPieChartViberWithWhatsApp';

    /**
     * @var EntityManagerInterface
     */
    private $em;
    private $tokenStorage;

    function __construct(EntityManagerInterface $em, TokenStorageInterface $tokenStorage)
    {
        $this->em = $em;
        $this->tokenStorage = $tokenStorage;
    }

    /**
     * @param string | string[] $optionNameOrNames
     * @param bool $assoc
     * @param InfoRole $role
     * @return mixed
     */
    public function get($optionNameOrNames, $assoc = true, $role = null)
    {
        $isArray = is_array($optionNameOrNames);
        $options = [];
        $lowerNames = [];
        if ($optionNameOrNames) {
            if (!$isArray) {
                $optionNameOrNames = [$optionNameOrNames];
            }

            $options = array_fill_keys($optionNameOrNames, null);

            foreach ($optionNameOrNames as $name) {
                $lowerNames[mb_strtolower($name)] = $name;
            }
        }

        $infoOptions = $this->createOptionsQueryBuilder(array_keys($lowerNames), $role)->getResult();

        if (!$assoc) {
            return $infoOptions;
        }

        /**
         * @var InfoOptions $infoOption
         */
        foreach ($infoOptions as $infoOption) {
            if (is_array(($infoOption))) {
                $name = $infoOption['name'];
                $value = $infoOption['value'];
            } else {
                $name = $infoOption->getName();
                $value = $infoOption->getValue();
            }

            if (is_numeric($value)) {
                $value += 0;
            }
            $options[$lowerNames[mb_strtolower($name)]] = $value;
        }

        return $isArray ? $options : $options[$optionNameOrNames[0]];
    }

    private function createOptionsQueryBuilder($optionNameOrNames, $role = null)
    {
        if (!$role) {
            $token = $this->tokenStorage->getToken();
            $user = $token ? $token->getUser() : null;
            if ($user && $user instanceof InfoUser) {
                $role = $user->getRole();
            }
        }

        $query = $this
            ->em
            ->getRepository(InfoOptions::class)
            ->createQueryBuilder('o')
            ->select('o.name');
        if ($role) {
            $query
                ->addSelect("case when COALESCE(obr.id, 0) != 0 then obr.id else o.id end as id")
                ->addSelect("case when COALESCE(obr.value, '') != '' then obr.value else o.value end as value");

            $query->leftJoin(InfoOptionsbyrole::class, 'obr', Join::WITH, 'o.id = obr.option and obr.role = :role');

            $query->setParameter('role', $role);
        } else {
            $query
                ->addSelect('o.id')
                ->addSelect('o.value');
        }
        if ($optionNameOrNames) {
            $query->where('LOWER(o.name) IN (:name)')
                ->setParameter('name', $optionNameOrNames);
        }
        return $query->getQuery();
    }
}
