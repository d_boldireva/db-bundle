<?php

namespace TeamSoft\CrmRepositoryBundle\Service;

use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use TeamSoft\CrmRepositoryBundle\Entity\ServiceFieldInterface;
use TeamSoft\CrmRepositoryBundle\Model\FileInterface;

class UploadFile
{
    private $em;

    function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }

    public function saveEntityFile(FileInterface $entity)
    {
        $filePath = $entity->getContent();
        if (is_resource($entity->getContent())) {
            $filePath = $this->createFilePath($entity);
            file_put_contents($filePath, $entity->getContent());
        } else if (($content = $entity->getContent()) && $content instanceof UploadedFile) {
            $filePath = $this->createFilePath($entity);
            $content->move(pathinfo($filePath, PATHINFO_DIRNAME), pathinfo($filePath, PATHINFO_BASENAME));
        }

        return $filePath;
    }

    private function createFilePath(FileInterface $entity)
    {
        $clientPath = $this->em->getConnection()->executeQuery('SELECT get_client_path()')->fetchOne();
        $tableName = $this->em->getClassMetadata(get_class($entity))->getTableName();
        $dir = "$clientPath/db-content/$tableName";
        $this->makeDir($dir);
        $name = ($entity instanceof ServiceFieldInterface && $entity->getGuid() !== null) ? $entity->getGuid() : uniqid();
        return $dir . '/' . $name . '.' . $this->getExtension($entity);
    }

    private function getExtension(FileInterface $entity)
    {
        $extension = $entity->getExtension();
        if (!$extension && ($content = $entity->getContent()) && $content instanceof UploadedFile) {
            $extension = $content->getExtension();
        }
        return $extension;
    }

    private function makeDir($dir)
    {
        if (!is_dir($dir)) {
            if (false === @mkdir($dir, 0777, true) && !is_dir($dir)) {
                throw new \RuntimeException(sprintf("Unable to create the %s directory\n", $dir));
            }
        } elseif (!is_writable($dir)) {
            throw new \RuntimeException(sprintf("Unable to write in the %s directory\n", $dir));
        }
    }
}
