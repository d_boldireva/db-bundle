<?php
declare(strict_types=1);

namespace TeamSoft\CrmRepositoryBundle\Service;

use Symfony\Component\HttpFoundation\ParameterBag;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\Security\Core\Security;
use Symfony\Component\Security\Core\User\UserInterface;
use TeamSoft\CrmRepositoryBundle\Entity\GridConfig\PoGridConfig;
use TeamSoft\CrmRepositoryBundle\Entity\InfoUser;
use TeamSoft\CrmRepositoryBundle\Entity\Page;
use TeamSoft\CrmRepositoryBundle\Repository\Company\InfoCompanyRepository;
use TeamSoft\CrmRepositoryBundle\Repository\Contact\InfoContactRepository;
use TeamSoft\CrmRepositoryBundle\Repository\PoGridConfig\PoGridConfigRepository;

class GridConfigService
{
    const HEADER_APP_MODE = 'X-App-Mode';
    const MODE_DCR = 'dcr';

    public const COLUMN_IS_ARCHIVE = 'is_archive';
    public string $limitParam = 'limit';
    public string $offsetParam = 'offset';
    public string $orderParam = 'order_by';
    public string $withTotalCount = 'with_total_count';

    protected Security $security;
    protected array $configs = [];
    protected PoGridConfigRepository $poGridConfigRepository;
    protected InfoCompanyRepository $companyRepository;
    protected InfoContactRepository $contactRepository;
    protected UserInterface $user;
    protected Options $options;
    protected Request $request;

    function __construct(
        Security               $security,
        PoGridConfigRepository $poGridConfigRepository,
        Options                $options,
        InfoCompanyRepository  $companyRepository,
        InfoContactRepository  $contactRepository,
        RequestStack           $requestStack
    )
    {
        $this->security = $security;
        $this->poGridConfigRepository = $poGridConfigRepository;
        $this->companyRepository = $companyRepository;
        $this->contactRepository = $contactRepository;
        $this->options = $options;
        $this->request = $requestStack->getCurrentRequest();
    }

    private function getAll(int $pageId)
    {
        $user = $this->security->getUser();
        $config = [];

        if ($user) {
            $key = $pageId . ":" . $user->getId();
            if (!isset($this->configs[$key])) {
                $config = $this->poGridConfigRepository->findMergedGridConfigs($pageId, $user);
                $this->configs[$key] = $config;
            } else {
                $config = $this->configs[$key];
            }
        }

        return $config;
    }

    /**
     * @param int $pageId
     * @param bool $isShown
     * @return PoGridConfig[]|array
     */
    private function get(int $pageId, bool $isShown): array
    {
        return array_filter($this->getAll($pageId), function ($gridConfig) use ($isShown) {
            if ($this->request->headers->get(self::HEADER_APP_MODE) == self::MODE_DCR) {
                return $gridConfig->getIsShownDcr() == $isShown;
            } else {
                return $gridConfig->getIsShown() == $isShown;
            }
        });
    }

    public function getSerializerGroups(int $pageId): array
    {
        $serializes = [];
        $config = $this->getSerializeColumns($pageId);
        $columns = $this->getGridColumns($pageId);

        foreach ($columns as $column) {
            if (!isset($config[$column])) {
                // todo add notification
                continue;
            }

            foreach ($config[$column] as $serialize) {
                if (is_callable($serialize)) {
                    $serialize = $serialize();
                }

                if (is_array($serialize)) {
                    $serializes = array_merge($serializes, $serialize);
                } else {
                    $serializes[] = $serialize;
                }
            }
        }

        return array_unique($serializes);
    }

    private function getGridColumns(int $pageId): array
    {
        $columns = [];

        foreach ($this->get($pageId, true) as $item) {
            $columns[] = $item->getFieldCode();
        }

        return $columns;
    }

    private function getFilters(int $pageId, ParameterBag $parameterBag): array
    {
        $columns = $this->getGridColumns($pageId);
        $filterBy = [];
        foreach ($columns as $column) {
            if ($parameterBag->has($column)) {
                $filterBy[$column] = $parameterBag->get($column);
            }
        }

        return $filterBy;
    }

    private function getSerializeColumns(int $pageId): array
    {

        if ($pageId === Page::COMPANY) {
            return [
                'name' => ['Company:name'],
                'type' => ['Company:companytype', 'CompanyType:name', 'CompanyType:id'],
                'category' => [function (): array {
                    if ($this->options->get(Options::OPTION_USE_CATEGORY_WITH_DIRECTION)) {
                        return [
                            'Company:companyCategories',
                            'Companycateg:category',
                            'CompanyCategory:id',
                            'CompanyCategory:name',
                            'CompanyCategory:direction',
                            "Direction:id",
                            "Direction:name",
                        ];
                    }

                    return ['Company:category', 'CompanyCategory:id', 'CompanyCategory:name'];
                }],
                'address' => ['Company:address', 'Company:fullAddress'],
                'last_task_date' => [],
                'cr_status_verification' => [],
                'get_verification' => ['Company:getVerification'],
                'modifier_user_name' => [],
                GridConfigService::COLUMN_IS_ARCHIVE => ['Company:isarchive'],
                'code' => ['Company:code'],
                'postcode' => ['Company:postcode'],
                'phone1' => ['Company:phone1'],
                'phone2' => ['Company:phone2'],
                'eaddr' => ['Company:eaddr'],
                'waddr' => ['Company:waddr'],
                'owner' => ['Company:owner', 'User:name'],
            ];
        } elseif ($pageId === Page::CONTACT) {
            return [
                'lastname' => ['Contact:lastname'],
                'firstname' => ['Contact:firstname'],
                'middlename' => ['Contact:middlename'],
                'specialization' => [
                    'Contact:specialization',
                    'Contact:specialization_id',
                    'Dictionary:id',
                    'Dictionary:name'
                ],
                'companyname' => [
                    'Contact:company',
                    'Company:name',
                    'Contact:company_id'
                ],
                'category' => [function (): array {
                    if ($this->options->get(Options::OPTION_USE_CATEGORY_WITH_DIRECTION)) {
                        return [
                            'Contact:contactCategories',
                            'Contactcategory:category',
                            'ContactCateg:id',
                            'ContactCateg:name',
                            'ContactCateg:direction',
                            'CompanyCategory:direction',
                            "Direction:id",
                            "Direction:name",
                        ];
                    }

                    return ['Contact:category', 'ContactCateg:id', 'ContactCateg:name'];
                }],
                'address' => ['Contact:address'],
                'phone1' => ['Contact:phone1'],
                'phone2' => ['Contact:phone2'],
                'eaddr' => ['Contact:eaddr'],
                'cr_status_verification' => [],
                'get_verification' => ['Contact:getVerification'],
                'modifier_user_name' => [],
                GridConfigService::COLUMN_IS_ARCHIVE => ['Contact:isarchive'],
                'contacttype' => ['Contact:contacttype', 'ContactType:name'],
                'contactspec' => ['Contact:specializations', 'ContactSpecialization:spec', 'Dictionary:name'],
                'position' => ['Contact:position', 'Dictionary:name'],
                'postcode' => ['Contact:postcode'],
                'owner' => ['Contact:owner', 'User:name'],
                'contactowner' => ['Contact:contactOwners', 'InfoContactowner:owner', 'User:name'],
                'legal_name' => ['Contact:legalName'],
                'reg_address' => ['Contact:regAddress'],
                'insurance_certificate' => ['Contact:insuranceCertificate'],
                'identification_number' => ['Contact:identificationNumber'],
                'bank' => ['Contact:bank'],
                'bic' => ['Contact:bic'],
                'correspondent_account' => ['Contact:correspondentAccount'],
                'snils' => ['Contact:snils'],
                'kpp' => ['Contact:kpp'],
                'legal_status' => ['Contact:legalStatus', 'CustomDictionaryValue:name'],
                'interaction' => ['Contact:interaction', 'CustomDictionaryValue:name'],
                'passport_seriesnumber' => ['Contact:passportSeriesNumber'],
                'passport_date' => ['Contact:passportDate'],
                'passport_issued' => ['Contact:passportIssued'],
                'city' => ['Contact:city', 'City:name'],
                'region' => ['Contact:region', 'Region:name'],
                'contact_sign_createdate' => ['Contact:getContactSignsNotArchive', 'ContactSign:createDate']
            ];
        }


        return [];
    }

    /**
     * @param int $pageId
     * @param Request $request
     * @param InfoUser $user
     * @param array $extraFilters
     * @return array
     */
    public function findEntities(
        int $pageId, ParameterBag $parameterBag, InfoUser $user, ?string $appMode, array $extraFilters = [], array $extraColumns = []
    ): array
    {
        $orderBy = json_decode($parameterBag->get($this->orderParam, '{}'), true);
        $limit = max(min($parameterBag->getInt($this->limitParam, 100), 2000), 10);
        $offset = max($parameterBag->getInt($this->offsetParam, 0), 0);
        $withTotalCount = $parameterBag->getBoolean($this->withTotalCount, false);

        if (!$orderBy) {
            $orderBy = [];
        }

        $filters = $this->getFilters($pageId, $parameterBag);

        if ($extraFilters) {
            $filters = array_merge($filters, $extraFilters);
        }

        if ($pageId === Page::COMPANY) {
            $repository = $this->companyRepository;
        } elseif ($pageId === Page::CONTACT) {
            $repository = $this->contactRepository;
        } else {
            throw new \RuntimeException();
        }

        $repository->setMode($appMode);
        $columns = $this->getGridColumns($pageId);

        if ($extraColumns) {
            $columns = array_merge($columns, $extraColumns);
        }

        return $repository->findByFilterAdvanced($columns, $filters, $orderBy, $user, $withTotalCount, $limit, $offset);
    }
}
