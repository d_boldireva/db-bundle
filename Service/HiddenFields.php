<?php

namespace TeamSoft\CrmRepositoryBundle\Service;

use Doctrine\ORM\EntityManagerInterface;
use TeamSoft\CrmRepositoryBundle\Entity\InfoHidefield;

class HiddenFields
{
    private $em;

    function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }

    public function get(int $page, $typeId = null, $roleId = null)
    {
        $hiddenFields = [];
        /**
         * @var \TeamSoft\CrmRepositoryBundle\Repository\InfoHidefield $repository
         */
        $repository = $this->em->getRepository(InfoHidefield::class);
        $result = $repository->findByPage($page, $typeId, $roleId);
        if ($result) {
            $hiddenFields = array_merge($hiddenFields, $result);
        }
        return $hiddenFields;
    }
}