<?php

namespace TeamSoft\CrmRepositoryBundle\Validator\Constraints;

use Symfony\Component\Validator\Constraint;

class BlobMimeContentType extends Constraint
{
    public $message = '';
    public $mimeTypes = [];
}
