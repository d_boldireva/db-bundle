<?php

namespace TeamSoft\CrmRepositoryBundle\Validator\Constraints;

use Symfony\Component\Validator\Constraint;

/**
 * @Annotation
 * @see InfoSalePeriodValidator
 */
class InfoSalePeriod extends Constraint
{
    public $message = 'There is another period from {{ dateFrom }} to {{ dateTill }}';

    public $isDiscounts = false;

    /**
     * @return string
     */
    public function getTargets()
    {
        return self::CLASS_CONSTRAINT;
    }
}
