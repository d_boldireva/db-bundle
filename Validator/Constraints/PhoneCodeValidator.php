<?php

namespace TeamSoft\CrmRepositoryBundle\Validator\Constraints;

use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use TeamSoft\CrmRepositoryBundle\Entity\InfoContactphone;
use Symfony\Component\Validator\Exception\{ConstraintDefinitionException, UnexpectedTypeException, UnexpectedValueException};

class PhoneCodeValidator extends ConstraintValidator
{
    /** @var TokenStorageInterface */
    protected $tokenStorage;

    /** @param TokenStorageInterface $tokenStorage */
    public function __construct(TokenStorageInterface $tokenStorage)
    {
        $this->tokenStorage = $tokenStorage;
    }

    public function validate($value, Constraint $constraint)
    {
        if (!($constraint instanceof PhoneCode)) {
            throw new UnexpectedTypeException($constraint, PhoneCode::class);
        }

        if ($value === null) {
            return;
        }

        if (!is_string($value)) {
            throw new UnexpectedValueException($value, 'string');
        }

        $parent = $this->context->getObject()->getParent()->getData();
        if ($parent instanceof InfoContactphone) {
            if (!$parent->getIsVisibleInCrm()) {
                return;
            }
        }

        $currentUser = $this->tokenStorage->getToken()->getUser();
        if (!$currentUser) {
            return;
        }

        $country = $currentUser->getCountry();
        if (!$country || !$country->getPhonemask()) {
            return;
        }

        $code = $country->getPhonemask();

        try {
            if (!preg_match($code, $value)) {
                $this->context->buildViolation($constraint->message)
                    ->addViolation();
            }
        } catch (\Exception $exception) {
            throw new ConstraintDefinitionException($exception->getMessage());
        }
    }
}
