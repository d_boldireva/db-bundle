<?php

namespace TeamSoft\CrmRepositoryBundle\Validator\Constraints;

use Symfony\Component\Validator\ConstraintValidator;
use Symfony\Component\Validator\Constraint;
use TeamSoft\CrmRepositoryBundle\Entity\InfoContactpotential;
use TeamSoft\CrmRepositoryBundle\Entity\InfoTask;

/**
 * Class NotEmptyContactPotentialCollectionValidator
 * @Annotation
 */
class NotEmptyContactPotentialCollectionValidator extends ConstraintValidator
{

    /**
     * @param InfoTask $object
     * @param Constraint $constraint
     */
    public function validate($object, Constraint $constraint)
    {
        /**
         * @var \Symfony\Component\Form\Form $taskForm
         */
        $taskForm = $this->context->getRoot();
        /**
         * @var InfoTask $task
         */
        $task = $taskForm->getData();

        if ($task->isDone() && $task->getTasktype() && $task->getTasktype()->getIsshowdogov() == 1) {

            $count = 0;

            foreach ($object as $contactPotential) {
                /**
                 * @var InfoContactpotential $contactPotential
                 */
                if ($contactPotential->getPlancount() || $contactPotential->getFactcount() || $contactPotential->getPotential() || $contactPotential->getControl()) {
                    $count++;
                    break;
                }
            }

            if ($count === 0) {
                $this->context
                    ->buildViolation('Please, fill at least one agreement')
                    ->atPath('contactPotentialCollection')
                    ->addViolation();
            }
        }
    }
}
