<?php
declare(strict_types=1);


namespace TeamSoft\CrmRepositoryBundle\Validator\Constraints;

use Symfony\Component\Validator\Constraint;

/**
 * @Annotation
 * @see InfoTargetingActivityLogValidator
 */
class InfoTargetingActivityLog extends Constraint
{
    public $message = 'Log has been created';
    public $user = null;

    public function getTargets(): string|array
    {
        return self::CLASS_CONSTRAINT;
    }
}
