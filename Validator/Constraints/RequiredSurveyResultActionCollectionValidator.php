<?php


namespace TeamSoft\CrmRepositoryBundle\Validator\Constraints;


use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\PersistentCollection;
use Symfony\Component\Validator\Exception\UnexpectedTypeException;
use Symfony\Component\Validator\ConstraintValidator;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\Exception\UnexpectedValueException;
use TeamSoft\CrmRepositoryBundle\Entity\InfoHidefield;
use TeamSoft\CrmRepositoryBundle\Entity\InfoSurvey;
use TeamSoft\CrmRepositoryBundle\Entity\InfoSurveyQuestion;
use TeamSoft\CrmRepositoryBundle\Entity\InfoSurveyResultAction;
use TeamSoft\CrmRepositoryBundle\Entity\InfoAction;
use TeamSoft\CrmRepositoryBundle\Model\ContainsActionInterface;
use TeamSoft\CrmRepositoryBundle\Model\ContainsTaskInterface;


/**
 * Class RequiredSurveyResultActionCollectionValidator
 * @package TeamSoft\CrmRepositoryBundle\Validator\Constraints
 */
class RequiredSurveyResultActionCollectionValidator extends ConstraintValidator
{

    private $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * @param PersistentCollection $surveyResultActions
     * @param Constraint $constraint
     */
    public function validate($surveyResultActions, Constraint $constraint)
    {
        if (!($constraint instanceof RequiredSurveyResultActionCollection)) {
            throw new UnexpectedTypeException($constraint, RequiredSurveyResultActionCollection::class);
        }

        if (!\is_array($surveyResultActions) && !$surveyResultActions instanceof \IteratorAggregate) {
            throw new UnexpectedValueException($surveyResultActions, 'array|IteratorAggregate');
        }

        /**
         * @var \Symfony\Component\Form\Form $form
         */
        $form = $this->context->getRoot();

        $action = $form->getData();

        if ($action instanceof ContainsActionInterface) {
            $action = $action->getAction();
        }

        if (!($action instanceof InfoAction)) {
            throw new UnexpectedTypeException($action, InfoAction::class);
        }

        /**
         * @var InfoAction $action
         */
        $actionState = $action->getActionstate();

        //validate if action try to close
        if ($actionState && $actionState->getIsclosed()) {
            $filterBy = [
                'dateFrom' => $action->getDatefrom() ? $action->getDatefrom()->format('Y-m-d H:i:s') : null,
                'actionId' => $action->getId(),
                'companyId' => $action->getCompanyId(),
                'contactId' => $action->getContactId(),
                'actiontypeId' => $action->getActiontypeId(),
                'responsibleId' => $action->getResponsibleId(),
                'page' => 8,
            ];

            $surveys = $this->entityManager->getRepository(InfoSurvey::class)->findByFilter($filterBy);

            if ($surveys) {
                $surveyResultActionsSort = [];
                //architectural crutch if survey is not have questionId and is_rejected=1 that mean all questions are rejected
                $surveyResultActionsRejected = [];
                //if answer is empty client remove an answer from collection and not sending it
                foreach ($surveyResultActions as $surveyResultAction) {
                    /**
                     * @var InfoSurveyResultAction $surveyResultAction
                     */

                    if ($surveyResultAction->getIsRejected()) {
                        $surveyResultActionsRejected[$surveyResultAction->getSurveyId()] = $surveyResultAction;
                    } else {
                        $surveyResultActionsSort[$surveyResultAction->getQuestionId()] = $surveyResultAction;
                    }
                }

                $requiredQuestionIds = [];
                foreach ($surveys as $survey) {
                    /**
                     * @var InfoSurvey $survey
                     */
                    if (!$survey->getIsRequired()) {
                        continue;
                    }

                    if (isset($surveyResultActionsRejected[$survey->getId()])) {
                        continue;
                    }

                    foreach ($survey->getSurveyQuestionCollection() as $question) {
                        /** @var InfoSurveyQuestion $question */
                        if (!$question->getIsRequired()) {
                            continue;
                        }

                        if (!isset($surveyResultActionsSort[$question->getId()])) {
                            $requiredQuestionIds[] = $question->getId();
                        }
                    }
                }

                if ($requiredQuestionIds) {
                    $this->context
                        ->buildViolation($constraint->message)
                        ->setParameter('{{string}}', join(',', $requiredQuestionIds))
                        ->atPath('surveyResultActions')
                        ->addViolation();
                }
            }
        }
    }
}