<?php

namespace TeamSoft\CrmRepositoryBundle\Validator\Constraints;

use Symfony\Component\Validator\Constraint;

/**
 * @Annotation
 * @see InfoSaleProjectValidator
 */
class InfoSaleProject extends Constraint
{
    public $message = 'There is another period from {{ dateFrom }} to {{ dateTill }}';

    /**
     * @return string
     */
    public function getTargets()
    {
        return self::CLASS_CONSTRAINT;
    }
}
