<?php

namespace TeamSoft\CrmRepositoryBundle\Validator\Constraints;

use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;
use TeamSoft\CrmRepositoryBundle\Entity\InfoUser;
use Symfony\Component\Validator\Exception\{UnexpectedTypeException, UnexpectedValueException};

/**
 * Class AvailableUserSubordinateValidator
 *
 * check if my submission is my subordinate in the same moment
 *
 * @package App\Validator\TreeWard
 */
class AvailableUserSubordinateValidator extends ConstraintValidator
{
    /**
     * @param mixed $value
     * @param Constraint $constraint
     */
    public function validate($value, Constraint $constraint): void
    {
        if (!($constraint instanceof AvailableUserSubordinate)) {
            throw new UnexpectedTypeException($constraint, AvailableUserSubordinate::class);
        }

        if (!$value) {
            return;
        }

        if (!($value instanceof InfoUser)) {
            throw new UnexpectedValueException($value, InfoUser::class);
        }

        /** @var InfoUser|null $submission */
        $submission = $value->getSubmission();
        do {
            if ($submission->getId() == $value->getId()) {
                $this->context->buildViolation($constraint->message)
                    ->setParameter('{{ field }}', $this->context->getObject()->getName())
                    ->addViolation();

                return;
            }
        } while ($submission = $submission->getSubmission());
    }
}
