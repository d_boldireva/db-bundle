<?php

declare(strict_types=1);

namespace TeamSoft\CrmRepositoryBundle\Validator\Constraints;

use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;
use Symfony\Component\Validator\Exception\UnexpectedTypeException;
use Symfony\Component\Validator\Exception\UnexpectedValueException;
use TeamSoft\CrmRepositoryBundle\Repository\InfoStorageRepository;

final class StorageSinglePrimaryValidator extends ConstraintValidator
{
    private InfoStorageRepository $repository;

    public function __construct(InfoStorageRepository $repository)
    {
        $this->repository = $repository;
    }

    public function validate($value, Constraint $constraint): void
    {
        if (!$constraint  instanceof StorageSinglePrimary) {
            throw new UnexpectedTypeException($constraint, StorageSinglePrimary::class);
        }

        if (null === $value || '' === $value) {
            return;
        }

        if (!is_int($value)) {
            throw new UnexpectedValueException($value, 'int');
        }

        if ($value === 1) {
            $qb = $this->repository->createQueryBuilder('s');
            $qb->where('s.isMain = 1');
            if ($constraint->storage !== null) {
                $qb->andWhere('s.id <> :id')
                    ->setParameter('id', $constraint->storage->getId());
            }

            $query = $qb->getQuery();

            if (count($query->getArrayResult()) > 0) {
                $this->context->buildViolation($constraint->message)->addViolation();
            }
        }
    }
}
