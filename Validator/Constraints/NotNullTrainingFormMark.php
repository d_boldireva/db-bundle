<?php

namespace TeamSoft\CrmRepositoryBundle\Validator\Constraints;

use Symfony\Component\Validator\Constraint;

/**
 * @Annotation
 */
class NotNullTrainingFormMark extends Constraint
{

    public $message = 'This value should not be null.';

    /**
     * @return string
     */
    public function getTargets()
    {
        return self::CLASS_CONSTRAINT;
    }
}
