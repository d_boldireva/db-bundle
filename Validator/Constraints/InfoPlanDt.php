<?php

namespace TeamSoft\CrmRepositoryBundle\Validator\Constraints;

use Symfony\Component\Validator\Constraint;

/**
 * @Annotation
 * @see InfoPlanDtValidator
 */
class InfoPlanDt extends Constraint
{
    public $message = 'There is another plan dt from {{ dt1 }} to {{ dt2 }}';

    /**
     * @return string
     */
    public function getTargets()
    {
        return self::CLASS_CONSTRAINT;
    }
}