<?php


namespace TeamSoft\CrmRepositoryBundle\Validator\Constraints;

use Symfony\Component\Validator\Constraint;

/**
 * Class RequiredSurveyResultActionCollection
 * @package TeamSoft\CrmRepositoryBundle\Validator\Constraints
 */
class RequiredSurveyResultActionCollection extends Constraint
{
    public $message = '{{string}}';
}