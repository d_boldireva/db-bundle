<?php

namespace TeamSoft\CrmRepositoryBundle\Validator\Constraints;

use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;
use Symfony\Component\Validator\Exception\UnexpectedTypeException;

class BlobMimeContentTypeValidator extends ConstraintValidator
{
    /**
     * @param mixed $value
     * @param Constraint $constraint
     */
    public function validate($value, Constraint $constraint)
    {
        if (!($constraint instanceof BlobMimeContentType)) {
            throw new UnexpectedTypeException($constraint, BlobMimeContentType::class);
        }

        if (!$value) {
            return;
        }

        $mime = @mime_content_type($value);

        if (!$mime) {
            $this->context
                ->buildViolation('Unknown file type')
                ->addViolation();

            return;
        }

        $mimeTypes = (array) $constraint->mimeTypes;

        foreach ($mimeTypes as $mimeType) {
            if ($mimeType === $mime) {
                return;
            }

            if ($discrete = strstr($mimeType, '/*', true)) {
                if (strstr($mime, '/', true) === $discrete) {
                    return;
                }
            }
        }

        $this->context
            ->buildViolation($constraint->message)
            ->addViolation();
    }
}
