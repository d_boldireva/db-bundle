<?php

namespace TeamSoft\CrmRepositoryBundle\Validator\Constraints;

use Symfony\Component\Validator\Constraint;

/**
 * @Annotation
 * @see RequiredSurveyResultCollectionValidator
 */
class RequiredSurveyResultCollection extends Constraint
{
    public $message = '{{string}}';
}
