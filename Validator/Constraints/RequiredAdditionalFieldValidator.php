<?php

namespace TeamSoft\CrmRepositoryBundle\Validator\Constraints;

use Symfony\Component\Validator\ConstraintValidator;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\Exception\UnexpectedTypeException;
use TeamSoft\CrmRepositoryBundle\Entity\AdditionalFieldInterface;
use TeamSoft\CrmRepositoryBundle\Entity\InfoAddinfotype;
use TeamSoft\CrmRepositoryBundle\Entity\InfoHidefield;
use TeamSoft\CrmRepositoryBundle\Entity\InfoRequired;

class RequiredAdditionalFieldValidator extends ConstraintValidator
{
    public function validate($value, Constraint $constraint)
    {
        if (!$constraint instanceof RequiredAdditionalField) {
            throw new UnexpectedTypeException($constraint, RequiredAdditionalField::class);
        }

        if (!$value instanceof AdditionalFieldInterface) {
            throw new UnexpectedTypeException($value, AdditionalFieldInterface::class);
        }


        $infoType = $value->getInfotype();
        $isRequired = $this->isRequired($constraint->requiredFields, $infoType) && !$this->isHidden($constraint->hiddenFields, $infoType);

        if ($isRequired && ($value->getValue() === null || $value->getValue() === '')) {
            $this->context->buildViolation($constraint->message)->addViolation();
        }
    }

    private function isRequired($requiredFields, InfoAddinfotype $infoType)
    {
        $requiredFieldNames = array_map(function ($item) {
            /**
             * @var InfoRequired $item
             */
            return strtolower($item->getName());
        }, $requiredFields);
        return in_array('{' . strtolower($infoType->getGuid()) . '}', $requiredFieldNames);
    }

    private function isHidden($hiddenFields, InfoAddinfotype $infoType)
    {
        $hiddenFieldNames = array_map(function ($item) {
            /**
             * @var InfoHidefield $item
             */
            return strtolower($item->getName());
        }, $hiddenFields);
        return in_array('{' . strtolower($infoType->getGuid()) . '}', $hiddenFieldNames);
    }
}
