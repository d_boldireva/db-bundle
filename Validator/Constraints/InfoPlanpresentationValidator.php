<?php

namespace TeamSoft\CrmRepositoryBundle\Validator\Constraints;

use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Validator\ConstraintValidator;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\Exception\UnexpectedTypeException;
use TeamSoft\CrmRepositoryBundle\Entity\InfoPlanpresentation as InfoPlanpresentationEntity;

/**
 * Class InfoPlanpresentationValidator
 * @package TeamSoft\CrmRepositoryBundle\Validator\Constraints
 * @Annotation
 */
class InfoPlanpresentationValidator extends ConstraintValidator
{
    protected $em;

    /**
     * InfoPlanpresentationValidator constructor.
     * @param EntityManagerInterface $em
     */
    function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }

    /**
     * @param mixed $value
     * @param Constraint $constraint
     */
    public function validate($value, Constraint $constraint)
    {
        if (!$constraint instanceof InfoPlanpresentation) {
            throw new UnexpectedTypeException($constraint, InfoPlanpresentation::class);
        }

        if (!$value instanceof InfoPlanpresentationEntity) {
            throw new UnexpectedTypeException($value, InfoPlanpresentationEntity::class);
        }

        $clm = $value->getClmId();

        if (!$value->getId() && $clm) {
            $sql = 'SELECT COUNT(1) FROM info_planpresentation WHERE plandetail_id = ? AND presentation_id = ?';
            if ($value->getNtask()) {
                $sql .= ' AND ntask=' . ((int) $value->getNtask());
            } else {
                $sql .= ' AND (ntask IS NULL OR ntask=0)';
            }

            $statement = $this->em->getConnection()->prepare($sql);
            $statement->bindValue(1, $value->getPlandetail()->getId(), \PDO::PARAM_INT);
            $statement->bindValue(2, $clm, \PDO::PARAM_INT);

            if (intval($statement->executeQuery()->fetchOne()) > 0) {
                $this->context->buildViolation($constraint->message)->atPath('presentation')->addViolation();
            }
        }
    }
}
