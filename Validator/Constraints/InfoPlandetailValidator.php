<?php

namespace TeamSoft\CrmRepositoryBundle\Validator\Constraints;

use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Validator\ConstraintValidator;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\Exception\UnexpectedTypeException;
use TeamSoft\CrmRepositoryBundle\Entity\InfoPlandetail as InfoPlandetailEntity;

/**
 * Class PlandetailValidator
 * @package TeamSoft\CrmRepositoryBundle\Validator\Constraints
 * @Annotation
 */
class InfoPlandetailValidator extends ConstraintValidator
{
    protected $em;

    /**
     * InfoPlandetailValidator constructor.
     * @param EntityManagerInterface $em
     */
    function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }

    /**
     * @param mixed $value
     * @param Constraint $constraint
     */
    public function validate($value, Constraint $constraint)
    {

        if (!$constraint instanceof InfoPlandetail) {
            throw new UnexpectedTypeException($constraint, InfoPlandetail::class);
        }

        if (!$value instanceof InfoPlandetailEntity) {
            throw new UnexpectedTypeException($value, InfoPlandetailEntity::class);
        }

        $plan = $value->getPlan();
        $target = $value->getTarget();
        $taskType = $value->getTaskType();
        $contactCategory = $value->getCategory();

        if ($plan && $target && $taskType && $contactCategory) {
            $sql = 'SELECT COUNT(1) FROM info_plandetail WHERE plan_id = ? AND target_id = ? AND tasktype_id = ? AND id != ? AND category_id = ?';
            $statement = $this->em->getConnection()->prepare($sql);
            $statement->bindValue(1, $plan->getId(), \PDO::PARAM_INT);
            $statement->bindValue(2, $target->getId(), \PDO::PARAM_INT);
            $statement->bindValue(3, $taskType->getId(), \PDO::PARAM_INT);
            $statement->bindValue(4, $value->getId() !== null ? $value->getId() : -1 , \PDO::PARAM_INT);
            $statement->bindValue(5, $contactCategory->getId(), \PDO::PARAM_INT);
            if (intval($statement->executeQuery()->fetchOne()) > 0) {
                $this->context->buildViolation($constraint->message)->addViolation();
            }
        }
    }
}
