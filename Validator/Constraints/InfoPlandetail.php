<?php

namespace TeamSoft\CrmRepositoryBundle\Validator\Constraints;

use Symfony\Component\Validator\Constraint;

/**
 * @Annotation
 */
class InfoPlandetail extends Constraint
{
    public $message = 'Plan detail with same target and task type has already been added';

    /**
     * @return string
     */
    public function getTargets()
    {
        return self::CLASS_CONSTRAINT;
    }
}