<?php
declare(strict_types=1);


namespace TeamSoft\CrmRepositoryBundle\Validator\Constraints;


use Symfony\Component\Validator\Constraint;

class RequiredContactSign extends Constraint
{
    public $message = 'This value should not be blank.';

    public $requiredFields = [];

    public $hiddenFields = [];

    public $requiredContactSign = false;

    public function validatedBy(): string
    {
        return RequiredContactSignValidator::class;
    }
}
