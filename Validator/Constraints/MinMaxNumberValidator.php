<?php

namespace TeamSoft\CrmRepositoryBundle\Validator\Constraints;

use Symfony\Component\Form\Exception\UnexpectedTypeException;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;

class MinMaxNumberValidator extends ConstraintValidator
{
    public function validate($value, Constraint $constraint)
    {
        if (!$constraint instanceof MinMaxNumber) {
            throw new UnexpectedTypeException($constraint, MinMaxNumber::class);
        }

        if ($value === null) {
            return;
        }

        if (!is_numeric($value)) {
            throw new UnexpectedTypeException($value, 'number');
        }

        if ($constraint->min !== null && $value < $constraint->min) {
            $message = $constraint->max != null ? $constraint->rangeMessage : $constraint->minMessage;

            $this->addError($constraint, $message);
        }

        if ($constraint->max !== null && $value > $constraint->max) {
            $message = $constraint->min !== null ? $constraint->rangeMessage : $constraint->minMessage;

            $this->addError($constraint, $message);
        }
    }

    protected function addError(MinMaxNumber $constraint, string $message)
    {
        $this->context
            ->addViolation($message, [
                '{{ min }}' => $constraint->min,
                '{{ max }}' => $constraint->max
            ]);

//            ->setParameter('{{ min }}', $constraint->min)
//            ->setParameter('{{ max }}', $constraint->max)
//            ->addViolation();
    }
}
