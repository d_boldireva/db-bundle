<?php

namespace TeamSoft\CrmRepositoryBundle\Validator\Constraints;

use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Validator\ConstraintValidator;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\Exception\UnexpectedTypeException;
use TeamSoft\CrmRepositoryBundle\Entity\SaleProject\InfoSaleproject as InfoSaleProjectEntity;

/**
 * Class InfoSaleProjectValidator
 * @package TeamSoft\CrmRepositoryBundle\Validator\Constraints
 * @Annotation
 */
class InfoSaleProjectValidator extends ConstraintValidator
{
    protected $em;

    /**
     * InfoPlanpresentationValidator constructor.
     * @param EntityManagerInterface $em
     */
    function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }

    /**
     * @param InfoSaleProjectEntity $value
     * @param Constraint $constraint
     */
    public function validate($value, Constraint $constraint)
    {
        if (!$constraint instanceof InfoSaleProject) {
            throw new UnexpectedTypeException($constraint, InfoSaleProject::class);
        }

        if (!$value instanceof InfoSaleProjectEntity) {
            throw new UnexpectedTypeException($value, InfoSaleProjectEntity::class);
        }

        $dateFrom = $value->getDatefrom() ? $value->getDatefrom()->format('Y-m-d') : null;
        $dateTill = $value->getDatetill() ? $value->getDatetill()->format('Y-m-d') : null;

        if ($dateFrom && $dateTill) {
            $sql = "SELECT datefrom, datetill FROM info_saleproject WHERE (('$dateFrom' BETWEEN datefrom AND datetill) OR ('$dateTill' BETWEEN datefrom AND datetill) OR (datefrom BETWEEN '$dateFrom' AND '$dateTill') OR (datetill BETWEEN '$dateFrom' AND '$dateTill'))";
            if ($value->getId()) {
                $sql .= " AND NOT id = " . $value->getId();
            }
            $statement = $this->em->getConnection()->prepare($sql);
            if ($result =  $statement->executeQuery()->fetchAllAssociative()) {
                $this->context->buildViolation($constraint->message)
                    ->setParameter('{{ dateFrom }}', (new \DateTime($result[0]['datefrom']))->format('Y-m-d'))
                    ->setParameter('{{ dateTill }}', (new \DateTime($result[0]['datetill']))->format('Y-m-d'))
                    ->addViolation();
            }
        }
    }
}
