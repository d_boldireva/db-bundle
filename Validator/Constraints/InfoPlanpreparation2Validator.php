<?php

namespace TeamSoft\CrmRepositoryBundle\Validator\Constraints;

use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Validator\ConstraintValidator;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\Exception\UnexpectedTypeException;
use TeamSoft\CrmRepositoryBundle\Entity\InfoPlanpreparation2 as InfoPlanpreparation2Entity;

/**
 * Class InfoPlanpreparationValidator
 * @package TeamSoft\CrmRepositoryBundle\Validator\Constraints
 * @Annotation
 */
class InfoPlanpreparation2Validator extends ConstraintValidator
{
    protected $em;

    /**
     * InfoPlanpreparation2Validator constructor.
     * @param EntityManagerInterface $em
     */
    function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }

    /**
     * @param mixed $value
     * @param Constraint $constraint
     */
    public function validate($value, Constraint $constraint)
    {
        if (!$constraint instanceof InfoPlanpreparation2) {
            throw new UnexpectedTypeException($constraint, InfoPlanpreparation2::class);
        }

        if (!$value instanceof InfoPlanpreparation2Entity) {
            throw new UnexpectedTypeException($value, InfoPlanpreparation2Entity::class);
        }

        $prep = $value->getPrep();

        if ($prep) {
            $sql = 'SELECT COUNT(1) FROM info_planpreparation2 WHERE plan_id = ? AND prep_id = ?';

            $statement = $this->em->getConnection()->prepare($sql);
            $statement->bindValue(1, $value->getPlan()->getId(), \PDO::PARAM_INT);
            $statement->bindValue(2, $prep->getId(), \PDO::PARAM_INT);

            if (intval($statement->executeQuery()->fetchOne()) > 0) {
                $this->context->buildViolation($constraint->message)->atPath('prep')->addViolation();
            }
        }
    }
}
