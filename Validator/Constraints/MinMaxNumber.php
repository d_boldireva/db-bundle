<?php

namespace TeamSoft\CrmRepositoryBundle\Validator\Constraints;

use Symfony\Component\Validator\Constraint;

/**
 * Class MinMaxNumber
 * @see MinMaxNumberValidator
 * @package TeamSoft\CrmRepositoryBundle\Validator\Constraints
 */
class MinMaxNumber  extends Constraint
{
    public $minMessage = 'Value must be larger or equal to {{ min }}';
    public $maxMessage = 'Value must be less or equal to {{ max }}';
    public $rangeMessage = 'The value must be in the range from {{ min }} to {{ max }}';

    /**
     * @var int|null
     */
    public $min;

    /**
     * @var int|null
     */
    public $max;

    /**
     * @return string
     */
    public function getTargets()
    {
        return self::CLASS_CONSTRAINT;
    }
}
