<?php

namespace TeamSoft\CrmRepositoryBundle\Validator\Constraints;

use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Validator\ConstraintValidator;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\Exception\UnexpectedTypeException;
use TeamSoft\CrmRepositoryBundle\Entity\InfoPlanpreparation as InfoPlanpreparationEntity;

/**
 * Class InfoPlanpreparationValidator
 * @package TeamSoft\CrmRepositoryBundle\Validator\Constraints
 * @Annotation
 */
class InfoPlanpreparationValidator extends ConstraintValidator
{
    protected $em;

    /**
     * InfoPlanpreparationValidator constructor.
     * @param EntityManagerInterface $em
     */
    function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }

    /**
     * @param mixed $value
     * @param Constraint $constraint
     */
    public function validate($value, Constraint $constraint)
    {
        if (!$constraint instanceof InfoPlanpreparation) {
            throw new UnexpectedTypeException($constraint, InfoPlanpreparation::class);
        }

        if (!$value instanceof InfoPlanpreparationEntity) {
            throw new UnexpectedTypeException($value, InfoPlanpreparationEntity::class);
        }

        $brand = $value->getBrend();
        if (!$value->getId() && $brand) {
            $numberTask = intval($value->getNtask());
            $sql = 'SELECT COUNT(1) FROM info_planpreparation WHERE plandetail_id = ? AND brend_id = ?';
            if ($numberTask !== 0) {
                $sql .= ' AND ntask = ?';
            } else {
                $sql .= ' AND ntask IS NULL';
            }
            $statement = $this->em->getConnection()->prepare($sql);
            $statement->bindValue(1, $value->getPlandetail()->getId(), \PDO::PARAM_INT);
            $statement->bindValue(2, $brand->getId(), \PDO::PARAM_INT);
            if ($numberTask !== 0) {
                $statement->bindValue(3, $numberTask, \PDO::PARAM_INT);
            }
            if (intval($statement->executeQuery()->fetchOne()) > 0) {
                $this->context->buildViolation($constraint->message)->atPath('brend')->addViolation();
            }
        }
    }
}
