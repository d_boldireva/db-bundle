<?php

namespace TeamSoft\CrmRepositoryBundle\Validator\Constraints;

use Symfony\Component\Validator\Constraint;


class UniqueCollection extends Constraint
{
    public $message = 'One or more items duplicated';
    public $propertyGetter;

    public function validatedBy(): string
    {
        return get_class($this) . 'Validator';
    }

    public function getRequiredOptions(): array
    {
        return ['propertyGetter'];
    }
}
