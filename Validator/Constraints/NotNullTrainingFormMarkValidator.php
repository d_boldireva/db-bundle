<?php

namespace TeamSoft\CrmRepositoryBundle\Validator\Constraints;

use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Form\Exception\UnexpectedTypeException;
use Symfony\Component\Validator\ConstraintValidator;
use Symfony\Component\Validator\Constraint;
use TeamSoft\CrmRepositoryBundle\Entity\InfoTask;
use TeamSoft\CrmRepositoryBundle\Entity\InfoTrainingform;

/**
 * Class NotNullTrainingFormMarkValidator
 * @Annotation
 */
class NotNullTrainingFormMarkValidator extends ConstraintValidator
{

    private $em;

    function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }

    /**
     * @param InfoTask $object
     * @param Constraint $constraint
     */
    public function validate($object, Constraint $constraint)
    {
        if (!$constraint instanceof NotNullTrainingFormMark) {
            throw new UnexpectedTypeException($constraint, NotNullTrainingFormMark::class);
        }

        if (!$object instanceof InfoTrainingform) {
            throw new UnexpectedTypeException($object, InfoTrainingform::class);
        }
        $sql = "SELECT COUNT(1) FROM info_tasktypeentity WHERE tasktype_id = ? AND entity = 'field_training_evaluation_form_pts'";

        $statement = $this->em->getConnection()->prepare($sql);
        $statement->bindValue(1, $object->getTasktypeId(), \PDO::PARAM_INT);

        if (intval($statement->executeQuery()->fetchOne()) > 0 && $object->getMark() === null && intval($object->getParamfortask()->getMark()) == 1 && intval($object->getParamfortask()->getIshidden()) != 1) {
            $this->context->buildViolation($constraint->message)->atPath('mark')->addViolation();
        }
    }
}
