<?php

declare(strict_types=1);

namespace TeamSoft\CrmRepositoryBundle\Validator\Constraints;

use Symfony\Component\Validator\Constraint;
use TeamSoft\CrmRepositoryBundle\Entity\InfoStorage;

final class StorageHasEnoughMaterials extends Constraint
{
    public string $message = 'The storage has not enough quantity of "{{ name }}" (total qty. {{ quantity }})';
    public InfoStorage $storage;

    public function getRequiredOptions(): array
    {
        return ['storage'];
    }
}
