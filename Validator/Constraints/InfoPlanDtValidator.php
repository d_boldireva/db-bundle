<?php

namespace TeamSoft\CrmRepositoryBundle\Validator\Constraints;

use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Validator\ConstraintValidator;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\Exception\UnexpectedTypeException;
use TeamSoft\CrmRepositoryBundle\Entity\InfoPlandt as InfoPlandtEntity;

/**
 * Class InfoPlanpresentationValidator
 * @package TeamSoft\CrmRepositoryBundle\Validator\Constraints
 * @Annotation
 */
class InfoPlanDtValidator extends ConstraintValidator
{
    protected $em;

    /**
     * InfoPlanpresentationValidator constructor.
     * @param EntityManagerInterface $em
     */
    function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }

    /**
     * @param mixed $value
     * @param Constraint $constraint
     */
    public function validate($value, Constraint $constraint)
    {
        if (!$constraint instanceof InfoPlandt) {
            throw new UnexpectedTypeException($constraint, InfoPlandt::class);
        }

        if (!$value instanceof InfoPlandtEntity) {
            throw new UnexpectedTypeException($value, InfoPlandtEntity::class);
        }

        $dt1 = $value->getDt1() ? $value->getDt1()->format('Y-m-d') : null;
        $dt2 = $value->getDt2() ? $value->getDt2()->format('Y-m-d') : null;
        $planId = $value->getPlanId();

        if ($dt1 && $dt2 && $planId) {
            $sql = "SELECT dt1, dt2 FROM info_plandt WHERE plan_id = $planId and ('$dt1' between dt1 and dt2 or '$dt2' between dt1 and dt2)";
            if ($value->getId()) {
                $sql .= " and not id = " . $value->getId();
            }
            $statement = $this->em->getConnection()->prepare($sql);
            if ($result = $statement->executeQuery()->fetchAllAssociative()) {
                $this->context->buildViolation($constraint->message)
                    ->setParameter('{{ dt1 }}', (new \DateTime($result[0]['dt1']))->format('Y-m-d'))
                    ->setParameter('{{ dt2 }}', (new \DateTime($result[0]['dt2']))->format('Y-m-d'))
                    ->addViolation();
            }
        }
    }
}
