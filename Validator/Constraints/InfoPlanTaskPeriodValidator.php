<?php

namespace TeamSoft\CrmRepositoryBundle\Validator\Constraints;

use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Validator\ConstraintValidator;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\Exception\UnexpectedTypeException;
use TeamSoft\CrmRepositoryBundle\Entity\InfoPlantaskperiod as InfoPlantaskperiodEntity;
/**
 * Class InfoPlanpresentationValidator
 * @package TeamSoft\CrmRepositoryBundle\Validator\Constraints
 * @Annotation
 */
class InfoPlanTaskPeriodValidator extends ConstraintValidator
{
    protected $em;

    /**
     * InfoPlanpresentationValidator constructor.
     * @param EntityManagerInterface $em
     */
    function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }

    /**
     * @param mixed $value
     * @param Constraint $constraint
     */
    public function validate($value, Constraint $constraint)
    {
        if (!$constraint instanceof InfoPlantaskperiod) {
            throw new UnexpectedTypeException($constraint, InfoPlantaskperiod::class);
        }

        if (!$value instanceof InfoPlantaskperiodEntity) {
            throw new UnexpectedTypeException($value, InfoPlantaskperiodEntity::class);
        }

        $dt1 = $value->getDt1() ? $value->getDt1()->format('Y-m-d') : null;
        $dt2 = $value->getDt2() ? $value->getDt2()->format('Y-m-d') : null;
        $planDetailId = $value->getPlandetail() ? $value->getPlandetail()->getId() : null;

        if ($dt1 && $dt2 && $planDetailId) {
            $sql = "SELECT dt1, dt2 FROM info_plantaskperiod WHERE plandetail_id = $planDetailId and (('$dt1' between dt1 and dt2) or ('$dt2' between dt1 and dt2))";
            if ($value->getId()) {
                $sql .= " and not id = " . $value->getId();
            }
            $statement = $this->em->getConnection()->prepare($sql);
            if ($result = $statement->executeQuery()->fetchAllAssociative()) {
                $this->context->buildViolation($constraint->message)
                    ->setParameter('{{ dt1 }}', (new \DateTime($result[0]['dt1']))->format('Y-m-d'))
                    ->setParameter('{{ dt2 }}', (new \DateTime($result[0]['dt2']))->format('Y-m-d'))
                    ->addViolation();
            }
        }
    }
}
