<?php

namespace TeamSoft\CrmRepositoryBundle\Validator\Constraints;

use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\PersistentCollection;
use Symfony\Component\Validator\Exception\UnexpectedTypeException;
use Symfony\Component\Validator\ConstraintValidator;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\Exception\UnexpectedValueException;
use TeamSoft\CrmRepositoryBundle\Entity\InfoHidefield;
use TeamSoft\CrmRepositoryBundle\Entity\InfoSurvey;
use TeamSoft\CrmRepositoryBundle\Entity\InfoSurveyQuestion;
use TeamSoft\CrmRepositoryBundle\Entity\InfoSurveyResult;
use TeamSoft\CrmRepositoryBundle\Entity\InfoTask;
use TeamSoft\CrmRepositoryBundle\Model\ContainsTaskInterface;

/**
 * Class NotEmptyContactPotentialCollectionValidator
 * @Annotation
 */
class RequiredSurveyResultCollectionValidator extends ConstraintValidator
{

    private $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * @param PersistentCollection $surveyResults
     * @param Constraint $constraint
     */
    public function validate($surveyResults, Constraint $constraint)
    {
        if (!($constraint instanceof RequiredSurveyResultCollection)) {
            throw new UnexpectedTypeException($constraint, RequiredSurveyResultCollection::class);
        }

        if (!\is_array($surveyResults) && !$surveyResults instanceof \IteratorAggregate) {
            throw new UnexpectedValueException($surveyResults, 'array|IteratorAggregate');
        }

        if ($surveyResults !== null) {
            /**
             * @var \Symfony\Component\Form\Form $form
             */
            $form = $this->context->getRoot();

            $task = $form->getData();
            if ($task instanceof ContainsTaskInterface) {
                $task = $task->getTask();
            }

            if (!($task instanceof InfoTask)) {
                throw new UnexpectedTypeException($task, InfoTask::class);
            }

            /**
             * @var InfoTask $task
             */
            $taskState = $task->getTaskstate();

            //validate if task try to close
            if ($taskState && $taskState->isDone()) {
                $filterBy = [
                    'dateFrom' => $task->getDatefrom() ? $task->getDatefrom()->format('Y-m-d H:i:s') : null,
                    'taskId' => $task->getId(),
                    'companyId' => $task->getCompanyId(),
                    'contactId' => $task->getContactId(),
                    'tasktypeId' => $task->getTasktypeId(),
                    'specializationId' => $task->getSpecializationId(),
                    'responsibleId' => $task->getResponsibleId(),
                ];

                $surveys = $this->entityManager->getRepository(InfoSurvey::class)->findByFilter($filterBy);

                if ($surveys) {
                    $surveyResultsSort = [];
                    //architectural crutch if survey is not have questionId and is_rejected=1 that mean all questions are rejected
                    $surveyResultsRejected = [];
                    //if answer is empty client remove an answer from collection and not sending it
                    foreach ($surveyResults as $surveyResult) {
                        /**
                         * @var InfoSurveyResult $surveyResult
                         */

                        if ($surveyResult->getIsRejected()) {
                            $surveyResultsRejected[$surveyResult->getSurveyId()] = $surveyResult;
                        } else {
                            $surveyResultsSort[$surveyResult->getQuestionId()] = $surveyResult;
                        }
                    }

                    $requiredQuestionIds = [];
                    foreach ($surveys as $survey) {
                        /**
                         * @var InfoSurvey $survey
                         */
                        if (!$survey->getIsRequired()) {
                            continue;
                        }

                        if (isset($surveyResultsRejected[$survey->getId()])) {
                            continue;
                        }

                        foreach ($survey->getSurveyQuestionCollection() as $question) {
                            /** @var InfoSurveyQuestion $question */
                            if (!$question->getIsRequired()) {
                                continue;
                            }

                            if (!isset($surveyResultsSort[$question->getId()])) {
                                $requiredQuestionIds[] = $question->getId();
                            }
                        }
                    }

                    if ($requiredQuestionIds) {
                        $this->context
                            ->buildViolation($constraint->message)
                            ->setParameter('{{string}}', join(',', $requiredQuestionIds))
                            ->atPath('surveyResultCollection')
                            ->addViolation();
                    }
                }
            }
        }
    }
}
