<?php

namespace TeamSoft\CrmRepositoryBundle\Validator\Constraints;

use Symfony\Component\Validator\Constraint;

class InfoMcmDistribution extends Constraint
{
    public $checkIsForMp = true;
    public $checkCurrentlyPeriod = true;


    public $messageIfNotIsformp = 'Selected distribution is not for mailing by medical representatives';
    public $messageIfNotCurrentlyPeriod = 'Selected distribution has other period';

    /**
     * @return string
     */
    public function getTargets()
    {
        return self::CLASS_CONSTRAINT;
    }
}
