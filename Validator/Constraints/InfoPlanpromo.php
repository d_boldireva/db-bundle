<?php

namespace TeamSoft\CrmRepositoryBundle\Validator\Constraints;

use Symfony\Component\Validator\Constraint;

/**
 * @Annotation
 * @see InfoPlanpromoValidator
 */
class InfoPlanpromo extends Constraint
{
    public $message = 'This promomaterial has already been added';

    /**
     * @return string
     */
    public function getTargets()
    {
        return self::CLASS_CONSTRAINT;
    }
}