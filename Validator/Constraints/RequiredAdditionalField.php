<?php

namespace TeamSoft\CrmRepositoryBundle\Validator\Constraints;

use Symfony\Component\Validator\Constraint;

/**
 * @Annotation
 */
class RequiredAdditionalField extends Constraint
{
    public $message = 'This value should not be null.';

    public $requiredFields = [];

    public $hiddenFields = [];

    /**
     * @return string
     */
    public function getTargets()
    {
        return self::CLASS_CONSTRAINT;
    }
}
