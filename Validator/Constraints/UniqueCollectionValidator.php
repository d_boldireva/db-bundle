<?php

namespace TeamSoft\CrmRepositoryBundle\Validator\Constraints;

use Symfony\Component\Form\Exception\UnexpectedTypeException;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;
use Symfony\Component\Validator\Exception\UnexpectedValueException;

class UniqueCollectionValidator extends ConstraintValidator
{
    public function validate($value, Constraint $constraint)
    {
        if (!($constraint instanceof UniqueCollection)) {
            throw new UnexpectedTypeException($constraint, UniqueCollection::class);
        }

        if (!\is_array($value) && !$value instanceof \IteratorAggregate) {
            throw new UnexpectedValueException($value, 'array|IteratorAggregate');
        }

        if ($value) {
            $elements = [];
            foreach ($value as $index => $item) {
                $link = $item->{$constraint->propertyGetter}();
                if (!$link) {
                    continue;
                }
//TODO: make field agnostic (use Callable for comparation)
                foreach ($this->getLinks($link) as $element) {
                    if (in_array($element, $elements, true)) {
                        $this->context->buildViolation($constraint->message)
                            //todo: now this error not show in frontend find to method how to set correct propertyPath
//                    ->atPath('children[' . $constraint->fieldCollection . '].children[' . $index . '].' . $constraint->field )
                            ->setParameter('{{ value }}', $this->formatValue($item))
                            ->addViolation();

                        break 2;
                    }

                    $elements[] = $element;
                }
            }
        }
    }

    protected function getLinks(string $nativeLink): array
    {
        $link = preg_replace('~\Ahttps?://~', '', $nativeLink);
        $links = [];
        if ($link === $nativeLink || !$link) {
            $links[] = $nativeLink;
        } else {
            $links[] = 'https://' . $link;
            $links[] = 'http://' . $link;
        }

        return $links;
    }
}
