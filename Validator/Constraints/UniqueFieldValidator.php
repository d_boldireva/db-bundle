<?php
declare(strict_types=1);

namespace TeamSoft\CrmRepositoryBundle\Validator\Constraints;

use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;
use Symfony\Component\Validator\Exception\UnexpectedTypeException;
use Symfony\Component\Validator\Exception\UnexpectedValueException;
use Symfony\Contracts\Translation\TranslatorInterface;

class UniqueFieldValidator extends ConstraintValidator
{

    protected $entityManager;
    protected $translator;

    public function __construct(EntityManagerInterface $entityManager, TranslatorInterface $translator)
    {
        $this->entityManager = $entityManager;
        $this->translator = $translator;
    }

    /**
     * {@inheritdoc}
     */
    public function validate($value, Constraint $constraint)
    {
        if (!$constraint instanceof UniqueField) {
            throw new UnexpectedTypeException($constraint, UniqueField::class);
        }

        if (null === $value) {
            return;
        }

        if (!\is_string($value) && !\is_numeric($value)) {
            throw new UnexpectedValueException($value, 'string|number');
        }

        $entity = $this->context->getRoot()->getData();

        $repository = $this->entityManager->getRepository($constraint->entityClass);
        $query = $repository->createQueryBuilder('r')
            ->andWhere("r.{$constraint->field} = :field")
            ->setParameter('field', $value);

        if (method_exists($entity, 'getId') && $entity->getId()) {
            $query->andWhere('r.id <> :id')->setParameter('id', $entity->getId());
        }

        $result = $query->getQuery()->getResult();

        if ($result) {
            $this->context->buildViolation($constraint->message, [
                '{{field_name}}' => $this->translator->trans($constraint->field),
                ])
                ->addViolation();
        }
    }
}
