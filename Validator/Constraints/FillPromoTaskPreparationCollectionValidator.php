<?php

namespace TeamSoft\CrmRepositoryBundle\Validator\Constraints;

use Symfony\Component\Validator\ConstraintValidator;
use Symfony\Component\Validator\Constraint;
use TeamSoft\CrmRepositoryBundle\Entity\InfoTask;
use TeamSoft\CrmRepositoryBundle\Entity\InfoTaskpreparation;

/**
 * Class FillPromoTaskPreparationCollectionValidator
 * @Annotation
 */
class FillPromoTaskPreparationCollectionValidator extends ConstraintValidator
{

    /**
     * @param InfoTask $object
     * @param Constraint $constraint
     */
    public function validate($object, Constraint $constraint)
    {
        /**
         * @var \Symfony\Component\Form\Form $taskForm
         */
        $taskForm = $this->context->getRoot();
        /**
         * @var InfoTask $task
         */
        $task = $taskForm->getData();

        if ($task->isDone() && $task->getTasktype() && $task->getTasktype()->getIsshowmerch() === 1) {
            /**
             * @var $preparation InfoTaskpreparation
             */
            foreach ($object as $preparation) {
                if (!$preparation->getIspromo()) {
                    $this->context
                        ->buildViolation('Please, fill promotions')
                        ->atPath('taskPreparationCollection')
                        ->addViolation();
                    break;
                }
            }
        }
    }
}
