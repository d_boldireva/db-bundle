<?php

namespace TeamSoft\CrmRepositoryBundle\Validator\Constraints;

use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Validator\ConstraintValidator;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\Exception\UnexpectedTypeException;
use TeamSoft\CrmRepositoryBundle\Entity\InfoPlanpromo as InfoPlanpromoEntity;

/**
 * Class InfoplanpromoValidator
 * @package TeamSoft\CrmRepositoryBundle\Validator\Constraints
 * @Annotation
 */
class InfoPlanpromoValidator extends ConstraintValidator
{
    protected $em;

    /**
     * InfoplanpromoValidator constructor.
     * @param EntityManagerInterface $em
     */
    function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }

    /**
     * @param mixed $value
     * @param Constraint $constraint
     */
    public function validate($value, Constraint $constraint)
    {
        if (!$constraint instanceof InfoPlanpromo) {
            throw new UnexpectedTypeException($constraint, InfoPlanpromo::class);
        }

        if (!$value instanceof InfoPlanpromoEntity) {
            throw new UnexpectedTypeException($value, InfoPlanpromoEntity::class);
        }

        $promomaterial = $value->getPromomaterial();

        if (!$value->getId() && $promomaterial) {
            $sql = 'SELECT COUNT(1) FROM info_planpromo WHERE plandetail_id = :plan AND promo_id = :promo';
            if ($value->getNtask()) {
                $sql .= ' AND ntask=' . ((int) $value->getNtask());
            } else {
                $sql .= ' AND (ntask IS NULL OR ntask=0)';
            }

            $statement = $this->em->getConnection()->prepare($sql);
            $statement->bindValue(':plan', $value->getPlandetail()->getId(), \PDO::PARAM_INT);
            $statement->bindValue(':promo', $promomaterial->getId(), \PDO::PARAM_INT);

            if (intval($statement->executeQuery()->fetchOne()) > 0) {
                $this->context->buildViolation($constraint->message)->atPath('promomaterial')->addViolation();
            }
        }
    }
}
