<?php

namespace TeamSoft\CrmRepositoryBundle\Validator\Constraints;

use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;
use Symfony\Component\Validator\Exception\UnexpectedTypeException;
use Symfony\Component\Validator\Exception\UnexpectedValueException;
use TeamSoft\CrmRepositoryBundle\Entity\InfoMcmdistribution as InfoMcmDistributionEntity;

class InfoMcmDistributionValidator extends ConstraintValidator
{
    public function validate($value, Constraint $constraint)
    {
        if (!$constraint instanceof InfoMcmDistribution) {
            throw new UnexpectedTypeException($constraint, InfoMcmDistribution::class);
        }

        if (!$value instanceof InfoMcmDistributionEntity) {
            throw new UnexpectedValueException($constraint, InfoMcmDistributionEntity::class);
        }

        if ($value) {
            if ($constraint->checkIsForMp && !$value->getIsformp()) {
                $this->context->buildViolation($constraint->messageIfNotIsformp)->atPath('distribution_id')->addViolation();
            } elseif ($constraint->checkCurrentlyPeriod) {
                if ($value->getDatefrom()->getTimestamp() > time() || $value->getDatetill()->getTimestamp() < time()) {
                    $this->context->buildViolation($constraint->messageIfNotCurrentlyPeriod)->atPath('distribution_id')->addViolation();
                }
            }
        }
    }
}
