<?php

declare(strict_types=1);

namespace TeamSoft\CrmRepositoryBundle\Validator\Constraints;

use Symfony\Component\Validator\Constraint;
use TeamSoft\CrmRepositoryBundle\Entity\InfoStorage;

final class StorageSinglePrimary extends Constraint
{
    public string $message = 'Only one storage can be primary';
    public ?InfoStorage $storage;
}
