<?php

namespace TeamSoft\CrmRepositoryBundle\Validator\Constraints;

use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Validator\ConstraintValidator;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\Exception\UnexpectedTypeException;
use TeamSoft\CrmRepositoryBundle\Entity\InfoContactBlog as InfoContactBlogEntity;
// TODO: transform to custom UniqueEntityConstraint
/**
 * Class InfoPlanpreparationValidator
 * @package TeamSoft\CrmRepositoryBundle\Validator\Constraints
 * @Annotation
 */
class InfoContactBlogValidator extends ConstraintValidator
{
    protected $em;

    /**
     * InfoPlanpreparation2Validator constructor.
     * @param EntityManagerInterface $em
     */
    function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }

    /**
     * @param mixed $value
     * @param Constraint $constraint
     */
    public function validate($value, Constraint $constraint)
    {
        if (!$constraint instanceof InfoContactBlog) {
            throw new UnexpectedTypeException($constraint, InfoContactBlog::class);
        }

        if (!$value instanceof InfoContactBlogEntity) {
            throw new UnexpectedTypeException($value, InfoContactBlogEntity::class);
        }

        $link = $value->getLink();

        if ($link) {
            $linkWithoutSchema = preg_replace('~\Ahttps?://~', '', $link);
            $links = [];
            if (!$linkWithoutSchema || $linkWithoutSchema === $link) {
                $links[] = $link;
            } else {
                $links[] = 'https://' . $linkWithoutSchema;
                $links[] = 'http://' . $linkWithoutSchema;
            }

            $sql = "SELECT COUNT(1) FROM info_contactblog WHERE link IN ('" . implode("', '", $links). "')";
            $id = $value->getId() ?? $this->context->getObject()->getExtraData()['id'] ?? null;
            if ($id) {
                $sql .= " AND id <> $id";
            }
            $statement = $this->em->getConnection()->prepare($sql);
            $statement->execute();

            if (intval($statement->executeQuery()->fetchOne()) > 0) {
                $this->context->buildViolation($constraint->message)->atPath('link')->addViolation();
            }
        }
    }
}
