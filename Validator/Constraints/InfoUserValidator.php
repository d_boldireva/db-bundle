<?php

namespace TeamSoft\CrmRepositoryBundle\Validator\Constraints;

use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Validator\ConstraintValidator;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\Exception\UnexpectedTypeException;
use TeamSoft\CrmRepositoryBundle\Entity\InfoUser as InfoUserEntity;

/**
 * Class InfoUserValidator
 * @package TeamSoft\CrmRepositoryBundle\Validator\Constraints
 * @Annotation
 */
class InfoUserValidator extends ConstraintValidator
{
    protected $em;

    /**
     * InfoPlanpresentationValidator constructor.
     * @param EntityManagerInterface $em
     */
    function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }

    /**
     * @param mixed $value
     * @param Constraint $constraint
     */
    public function validate($value, Constraint $constraint)
    {
        if (!$constraint instanceof InfoUser) {
            throw new UnexpectedTypeException($constraint, InfoUser::class);
        }

        if (!$value instanceof InfoUserEntity) {
            throw new UnexpectedTypeException($value, InfoUserEntity::class);
        }

        if ($value->getAdIdentifier()) {
            $sql = 'SELECT COUNT(1) FROM info_user WHERE ad_identifier = ?';
            if ($value->getId()) {
                $sql .= ' AND id != ?';
            }

            $statement = $this->em->getConnection()->prepare($sql);

            $statement->bindValue(1, $value->getAdIdentifier(), \PDO::PARAM_STR);
            if ($value->getId()) {
                $statement->bindValue(2, $value->getId(), \PDO::PARAM_INT);
            }

            if (intval($statement->executeQuery()->fetchOne()) > 0) {
                $this->context
                    ->buildViolation('This Active Directory user has already been added.')
                    ->addViolation();
            }
        }
    }
}
