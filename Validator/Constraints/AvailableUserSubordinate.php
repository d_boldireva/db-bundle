<?php

namespace TeamSoft\CrmRepositoryBundle\Validator\Constraints;

use Symfony\Component\Validator\Constraint;


class AvailableUserSubordinate extends Constraint
{
    public $message = "This user isn't available as subordinate";

    public function validatedBy(): string
    {
        return AvailableUserSubordinateValidator::class;
    }
}
