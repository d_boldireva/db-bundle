<?php

namespace TeamSoft\CrmRepositoryBundle\Validator\Constraints;

use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Validator\ConstraintValidator;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\Exception\UnexpectedTypeException;
use TeamSoft\CrmRepositoryBundle\Entity\InfoSaleperiod as InfoSalePeriodEntity;

/**
 * Class InfoSalePeriodValidator
 * @package TeamSoft\CrmRepositoryBundle\Validator\Constraints
 * @Annotation
 */
class InfoSalePeriodValidator extends ConstraintValidator
{
    protected $em;

    /**
     * InfoPlanpresentationValidator constructor.
     * @param EntityManagerInterface $em
     */
    function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }

    /**
     * @param InfoSaleperiod $value
     * @param Constraint $constraint
     */
    public function validate($value, Constraint $constraint)
    {
        if (!$constraint instanceof InfoSalePeriod) {
            throw new UnexpectedTypeException($constraint, InfoSalePeriod::class);
        }

        if (!$value instanceof InfoSalePeriodEntity) {
            throw new UnexpectedTypeException($value, InfoSalePeriodEntity::class);
        }

        $dateFrom = $value->getDatefrom() ? $value->getDatefrom()->format('Y-m-d') : null;
        $dateTill = $value->getDatetill() ? $value->getDatetill()->format('Y-m-d') : null;

        if ($dateFrom && $dateTill) {
            $sql = "SELECT datefrom, datetill FROM info_saleperiod WHERE (('$dateFrom' BETWEEN datefrom AND datetill) OR ('$dateTill' BETWEEN datefrom AND datetill) OR (datefrom BETWEEN '$dateFrom' AND '$dateTill') OR (datetill BETWEEN '$dateFrom' AND '$dateTill'))";

            if ($constraint->isDiscounts) {
                $sql .= " AND COALESCE(is_discount, 0) = 1";
            }

            if ($value->getId()) {
                $sql .= " AND NOT id = " . $value->getId();
            }
            $statement = $this->em->getConnection()->prepare($sql);
            if ($result = $statement->executeQuery()->fetchAllAssociative()) {
                $this->context->buildViolation($constraint->message)
                    ->setParameter('{{ dateFrom }}', (new \DateTime($result[0]['datefrom']))->format('Y-m-d'))
                    ->setParameter('{{ dateTill }}', (new \DateTime($result[0]['datetill']))->format('Y-m-d'))
                    ->addViolation();
            }
        }
    }
}
