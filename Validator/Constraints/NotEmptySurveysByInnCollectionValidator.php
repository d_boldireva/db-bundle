<?php

namespace TeamSoft\CrmRepositoryBundle\Validator\Constraints;

use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Validator\ConstraintValidator;
use Symfony\Component\Validator\Constraint;
use TeamSoft\CrmRepositoryBundle\Entity\InfoHidefield;
use TeamSoft\CrmRepositoryBundle\Entity\InfoTask;
use TeamSoft\CrmRepositoryBundle\Entity\InfoTaskSurveyByInn;

/**
 * Class NotEmptyContactPotentialCollectionValidator
 * @Annotation
 */
class NotEmptySurveysByInnCollectionValidator extends ConstraintValidator
{

    private $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * @param InfoTask $object
     * @param Constraint $constraint
     */
    public function validate($object, Constraint $constraint)
    {
        /**
         * @var \Symfony\Component\Form\Form $taskForm
         */
        $taskForm = $this->context->getRoot();
        /**
         * @var InfoTask $task
         */
        $task = $taskForm->getData();

        $taskState = $task->getTaskstate();
        $taskType = $task->getTasktype();

        // add condition for $taskType that it is not null
        if (
            ($taskState && $taskState->getIsclosed() === 1 && $taskState->getIsfinish() === 1)
            && ($taskType && empty(
            $this->entityManager
                ->getRepository(InfoHidefield::class)
                ->findByPage(4, $taskType->getId(), null, 'task_surveys_by_inn')
            ))
        ) {
            $valid = false;
            foreach ($object as $survey) {
                /**
                 * @var InfoTaskSurveyByInn $survey
                 */
                if (
                    $survey->getInn()
                    && $survey->getPurchaseType()
                    && $survey->getPurchaseState()
                ) {
                    $valid = true;
                    break;
                }
            }

            if (!$valid) {
                $this->context
                    ->buildViolation($constraint->message)
                    ->atPath('taskSurveyByInnCollection')
                    ->addViolation();
            }
        }
    }
}
