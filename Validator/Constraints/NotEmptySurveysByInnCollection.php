<?php

namespace TeamSoft\CrmRepositoryBundle\Validator\Constraints;

use Symfony\Component\Validator\Constraint;

/**
 * @Annotation
 */
class NotEmptySurveysByInnCollection extends Constraint
{
    public $message = 'Please, fill at least one survey';
}
