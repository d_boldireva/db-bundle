<?php

namespace TeamSoft\CrmRepositoryBundle\Validator\Constraints;

use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Validator\ConstraintValidator;
use Symfony\Component\Validator\Constraint;
use TeamSoft\CrmRepositoryBundle\Entity\InfoTask;
use TeamSoft\CrmRepositoryBundle\Entity\InfoTasktype;

/**
 * Class AvailableTaskTypeValidator
 * @Annotation
 */
class AvailableTaskTypeValidator extends ConstraintValidator
{

    private $em;

    /**
     * InfoplanpromoValidator constructor.
     * @param EntityManagerInterface $em
     */
    function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }

    /**
     * @param InfoTask $object
     * @param Constraint $constraint
     */
    public function validate($object, Constraint $constraint)
    {
        /**
         * @var \Symfony\Component\Form\Form $taskForm
         */
        $taskForm = $this->context->getRoot();
        /**
         * @var InfoTask $task
         */
        $task = $taskForm->getData();
        if ($task && !$task->getTasktype()) {
            $filterBy = array(
                'companyId' => $task->getCompanyId(),
                'planId' => $task->getPlanId(),
            );
            /**
             * @var \TeamSoft\CrmRepositoryBundle\Repository\InfoTasktype $repository
             */
            $repository = $this->em->getRepository(InfoTasktype::class);
            $taskTypes = $repository->findByFilter($filterBy);
            if (count($taskTypes) == 0) {
                $this->context
                    ->buildViolation('Chosen company doesn\'t have any available task type')
                    ->atPath('tasktype')
                    ->addViolation();
            }
        }
    }
}
