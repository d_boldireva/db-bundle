<?php
declare(strict_types=1);


namespace TeamSoft\CrmRepositoryBundle\Validator\Constraints;


use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Form\Exception\UnexpectedTypeException;
use Symfony\Component\Validator\Exception\UnexpectedValueException;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;
use TeamSoft\CrmRepositoryBundle\Entity\InfoHidefield;
use TeamSoft\CrmRepositoryBundle\Entity\InfoRequired;

class RequiredContactSignValidator extends ConstraintValidator
{
    public function validate($value, Constraint $constraint)
    {
        if (!$constraint instanceof RequiredContactSign) {
            throw new UnexpectedTypeException($constraint, RequiredContactSign::class);
        }
        if (!$value instanceof ArrayCollection && !$value instanceof \IteratorAggregate) {
            throw new UnexpectedValueException($value, 'array|IteratorAggregate');
        }

        $isRequired = $this->isRequired($constraint->requiredFields) && !$this->isHidden($constraint->hiddenFields);

        if(($isRequired || $constraint->requiredContactSign)) {
            if(!$this->isExistsContactSignWithSignType($value)) {
                $this->context->buildViolation($constraint->message)->atPath($this->context->getPropertyPath())->addViolation();
            }
        }
    }

    private function isRequired($requiredFields)
    {
        $requiredFieldNames = array_map(function ($item) {
            /**
             * @var InfoRequired $item
             */
            return strtolower($item->getName());
        }, $requiredFields);

        return in_array('contact_sign', $requiredFieldNames);
    }

    private function isHidden($hiddenFields)
    {
        $hiddenFieldNames = array_map(function ($item) {
            /**
             * @var InfoHidefield $item
             */
            return strtolower($item->getName());
        }, $hiddenFields);
        return in_array('contact_sign', $hiddenFieldNames);
    }

    private function isExistsContactSignWithSignType($value): bool {
        foreach ($value->getValues() as $contactSign) {
            if($contactSign->getSignType() === 'sign') {
                return true;
            }
        }
        return false;
    }
}
