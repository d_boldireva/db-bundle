<?php
declare(strict_types=1);


namespace TeamSoft\CrmRepositoryBundle\Validator\Constraints;

use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\File\Exception\UnexpectedTypeException;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;
use TeamSoft\CrmRepositoryBundle\Entity\InfoTargetingActivityLog as InfoTargetingActivityLogEntity;
use TeamSoft\CrmRepositoryBundle\Entity\InfoUser;

/**
 * Class InfoTargetingActivityLogValidator
 * @package TeamSoft\CrmRepositoryBundle\Validator\Constraints
 * @Annotation
 */
class InfoTargetingActivityLogValidator extends ConstraintValidator
{
    protected $em;

    /**
     * InfoPlanpresentationValidator constructor.
     * @param EntityManagerInterface $em
     */
    function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }

    public function validate($value, Constraint $constraint)
    {
        if (!$constraint instanceof InfoTargetingActivityLog) {
            throw new UnexpectedTypeException($constraint, InfoTargetingActivityLog::class);
        }

        if (!$value instanceof InfoTargetingActivityLogEntity) {
            throw new UnexpectedTypeException($value, InfoTargetingActivityLogEntity::class);
        }

        if (!$constraint->user instanceof InfoUser) {
            throw new UnexpectedTypeException($constraint->user, InfoUser::class);
        }

        $user = $constraint->user;
        /**
         * @var InfoTargetingActivityLogEntity $lastActivityLog
         */

        if ($value->getPanelUser() !== null && $value->getPanelType() !== null && $value->getPeriod() !== null) {
            $lastActivityLog = $this->em->getRepository(InfoTargetingActivityLogEntity::class)->findOneBy([
                'user' => $user->getId(),
                'panelUser' => $value->getPanelUser()->getId(),
                'panelType' => $value->getPanelType(),
                'period' => $value->getPeriod(),
            ], ['id' => 'DESC']);

            if ($lastActivityLog && ($lastActivityLog->getApprovementStatus() == $value->getApprovementStatus())) {
                $this->context->buildViolation($constraint->message)->addViolation();
            }
        }
    }
}
