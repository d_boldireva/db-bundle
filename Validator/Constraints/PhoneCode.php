<?php

namespace TeamSoft\CrmRepositoryBundle\Validator\Constraints;

use Symfony\Component\Validator\Constraint;

/**
 * @Annotation
 */
class PhoneCode extends Constraint
{
    public $message = "Phone number doesn't match with phone pattern.";

    public function validatedBy(): string
    {
        return PhoneCodeValidator::class;
    }
}
