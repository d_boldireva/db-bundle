<?php

declare(strict_types=1);

namespace TeamSoft\CrmRepositoryBundle\Validator\Constraints;

use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;
use Symfony\Component\Validator\Exception\UnexpectedTypeException;
use Symfony\Component\Validator\Exception\UnexpectedValueException;
use TeamSoft\CrmRepositoryBundle\Repository\InfoStorageRepository;

final class StorageHasEnoughMaterialsValidator extends ConstraintValidator
{
    private InfoStorageRepository $repository;

    public function __construct(InfoStorageRepository $repository)
    {
        $this->repository = $repository;
    }

    public function validate($value, Constraint $constraint): void
    {
        if (!$constraint  instanceof StorageHasEnoughMaterials) {
            throw new UnexpectedTypeException($constraint, StorageHasEnoughMaterials::class);
        }

        if (empty($value)) {
            return;
        }

        if (!is_array($value)) {
            throw new UnexpectedValueException($value, 'array');
        }

        $storageMaterials = [];
        foreach ($this->repository->findMaterials($constraint->storage->getId()) as $material) {
            $storageMaterials[$material['id']] = $material;
        }

        foreach ($value as $moveItem) {
            $material = $storageMaterials[$moveItem->getMaterial()->getId()] ?? null;

            if ($material === null || $material['quantity'] < $moveItem->getQuantity()) {
                $this->context->buildViolation($constraint->message)
                    ->setParameter('{{ name }}', $material['name'])
                    ->setParameter('{{ quantity }}', (string) $material['quantity'])
                    ->addViolation();
            }
        }
    }
}
