<?php

declare(strict_types=1);
namespace TeamSoft\CrmRepositoryBundle\Validator\Constraints;


use Symfony\Component\Validator\Constraint;
use TeamSoft\CrmRepositoryBundle\Translation\Validators;

class UniqueField extends Constraint
{
    /**
     * @var string
     */
    public $field = '';

    /**
     * @var string
     */
    public $entityClass = '';

    /**
     * @var string
     */
    public $message = Validators::UNIQUE_CONSTRAINT;


    public function getRequiredOptions(): array
    {
        return ['field', 'entityClass'];
    }
}
