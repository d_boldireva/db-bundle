<?php

namespace TeamSoft\CrmRepositoryBundle\Validator\Constraints;

use Symfony\Component\Validator\ConstraintValidator;
use Symfony\Component\Validator\Constraint;
use TeamSoft\CrmRepositoryBundle\Entity\InfoTask;
use TeamSoft\CrmRepositoryBundle\Entity\InfoTaskpreparation;

/**
 * Class MinPromoTaskMaterialCollectionValidator
 * @Annotation
 */
class MinPromoTaskMaterialCollectionValidator extends ConstraintValidator
{

    /**
     * @param InfoTask $object
     * @param Constraint $constraint
     */
    public function validate($object, Constraint $constraint)
    {
        /**
         * @var \Symfony\Component\Form\Form $taskForm
         */
        $taskForm = $this->context->getRoot();
        /**
         * @var InfoTask $task
         */
        $task = $taskForm->getData();

        if ($task->isDone() && $task->getTasktype() && $task->getTasktype()->getMinpromo()) {
            if (count($object) < $task->getTasktype()->getMinpromo()) {
                $this->context
                    ->buildViolation('Please, add at least {{ limit }} promo material.|Please, add at least {{ limit }} promo materials.')
                    ->setParameter('{{ limit }}', $task->getTasktype()->getMinpromo())
                    ->setPlural((int) $task->getTasktype()->getMinpromo())
                    ->atPath('taskMaterialCollection')
                    ->addViolation();
            }
        }
    }
}
