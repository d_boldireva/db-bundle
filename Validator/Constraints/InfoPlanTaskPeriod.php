<?php

namespace TeamSoft\CrmRepositoryBundle\Validator\Constraints;

use Symfony\Component\Validator\Constraint;

/**
 * @Annotation
 * @see InfoPlanTaskPeriodValidator
 */
class InfoPlanTaskPeriod extends Constraint
{
    public $message = 'There is another plan task period from {{ dt1 }} to {{ dt2 }}';

    /**
     * @return string
     */
    public function getTargets()
    {
        return self::CLASS_CONSTRAINT;
    }
}