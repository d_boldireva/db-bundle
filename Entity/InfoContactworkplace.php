<?php

namespace TeamSoft\CrmRepositoryBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * InfoContactworkplace
 *
 * @ORM\Table(name="info_contactworkplace")
 * @ORM\Entity
 */
class InfoContactworkplace implements ServiceFieldInterface
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="Description", type="blob", nullable=true)
     */
    private $description;

    /**
     * @var int
     *
     * @ORM\Column(name="DayCode", type="integer", nullable=true)
     */
    private $daycode;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="timefrom", type="datetime", nullable=true)
     */
    private $timefrom;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="timetill", type="datetime", nullable=true)
     */
    private $timetill;

    /**
     * @var string
     *
     * @ORM\Column(name="phone", type="string", length=50, nullable=true)
     */
    private $phone;

    /**
     * @var string
     *
     * @ORM\Column(name="guid", type="guid", nullable=true)
     */
    private $guid;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="currenttime", type="datetime", nullable=true)
     */
    private $currenttime;

    /**
     * @var string
     *
     * @ORM\Column(name="moduser", type="string", length=16, nullable=true)
     */
    private $moduser;

    /**
     * @var int
     *
     * @ORM\Column(name="even_odd", type="integer", nullable=true)
     */
    private $evenOdd;

    /**
     * @var int
     *
     * @ORM\Column(name="statusverification", type="integer", nullable=true)
     */
    private $statusverification;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="sendverification", type="datetime", nullable=true)
     */
    private $sendverification;

    /**
     * @var int
     *
     * @ORM\Column(name="morionid", type="integer", nullable=true)
     */
    private $morionid;

    /**
     * @var InfoContact
     *
     * @ORM\ManyToOne(targetEntity="InfoContact", inversedBy="contactWorkplaceCollection")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="Contact_id", referencedColumnName="id")
     * })
     */
    private $contact;

    /**
     * @var InfoDictionary
     *
     * @ORM\ManyToOne(targetEntity="InfoDictionary")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="specialization_id", referencedColumnName="id")
     * })
     */
    private $specialization;

    /**
     * @var InfoDictionary
     *
     * @ORM\ManyToOne(targetEntity="InfoDictionary")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="Department_id", referencedColumnName="id")
     * })
     */
    private $department;

    /**
     * @var InfoCompany
     *
     * @ORM\ManyToOne(targetEntity="InfoCompany")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="Company_id", referencedColumnName="id")
     * })
     */
    private $company;

    /**
     * @var InfoDictionary
     *
     * @ORM\ManyToOne(targetEntity="InfoDictionary")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="Position_id", referencedColumnName="id")
     * })
     */
    private $position;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set description
     *
     * @param string $description
     *
     * @return InfoContactworkplace
     */
    public function setDescription($description)
    {
        $this->description = $description;
    
        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set daycode
     *
     * @param int $daycode
     *
     * @return InfoContactworkplace
     */
    public function setDaycode($daycode)
    {
        $this->daycode = $daycode;
    
        return $this;
    }

    /**
     * Get daycode
     *
     * @return int
     */
    public function getDaycode()
    {
        return $this->daycode;
    }

    /**
     * Set timefrom
     *
     * @param \DateTime $timefrom
     *
     * @return InfoContactworkplace
     */
    public function setTimefrom($timefrom)
    {
        $this->timefrom = $timefrom;
    
        return $this;
    }

    /**
     * Get timefrom
     *
     * @return \DateTime
     */
    public function getTimefrom()
    {
        return $this->timefrom;
    }

    /**
     * Set timetill
     *
     * @param \DateTime $timetill
     *
     * @return InfoContactworkplace
     */
    public function setTimetill($timetill)
    {
        $this->timetill = $timetill;
    
        return $this;
    }

    /**
     * Get timetill
     *
     * @return \DateTime
     */
    public function getTimetill()
    {
        return $this->timetill;
    }

    /**
     * Set phone
     *
     * @param string $phone
     *
     * @return InfoContactworkplace
     */
    public function setPhone($phone)
    {
        $this->phone = $phone;
    
        return $this;
    }

    /**
     * Get phone
     *
     * @return string
     */
    public function getPhone()
    {
        return $this->phone;
    }

    /**
     * Set guid
     *
     * @param guid $guid
     *
     * @return InfoContactworkplace
     */
    public function setGuid($guid)
    {
        $this->guid = $guid;
    
        return $this;
    }

    /**
     * Get guid
     *
     * @return guid
     */
    public function getGuid()
    {
        return $this->guid;
    }

    /**
     * Set currenttime
     *
     * @param \DateTime $currenttime
     *
     * @return InfoContactworkplace
     */
    public function setCurrenttime(\DateTime $currenttime)
    {
        $this->currenttime = $currenttime;
    
        return $this;
    }

    /**
     * Get currenttime
     *
     * @return \DateTime
     */
    public function getCurrenttime()
    {
        return $this->currenttime;
    }

    /**
     * Set moduser
     *
     * @param string $moduser
     *
     * @return InfoContactworkplace
     */
    public function setModuser($moduser)
    {
        $this->moduser = $moduser;
    
        return $this;
    }

    /**
     * Get moduser
     *
     * @return string
     */
    public function getModuser()
    {
        return $this->moduser;
    }

    /**
     * Set evenOdd
     *
     * @param int $evenOdd
     *
     * @return InfoContactworkplace
     */
    public function setEvenOdd($evenOdd)
    {
        $this->evenOdd = $evenOdd;
    
        return $this;
    }

    /**
     * Get evenOdd
     *
     * @return int
     */
    public function getEvenOdd()
    {
        return $this->evenOdd;
    }

    /**
     * Set statusverification
     *
     * @param int $statusverification
     *
     * @return InfoContactworkplace
     */
    public function setStatusverification($statusverification)
    {
        $this->statusverification = $statusverification;
    
        return $this;
    }

    /**
     * Get statusverification
     *
     * @return int
     */
    public function getStatusverification()
    {
        return $this->statusverification;
    }

    /**
     * Set sendverification
     *
     * @param \DateTime $sendverification
     *
     * @return InfoContactworkplace
     */
    public function setSendverification($sendverification)
    {
        $this->sendverification = $sendverification;
    
        return $this;
    }

    /**
     * Get sendverification
     *
     * @return \DateTime
     */
    public function getSendverification()
    {
        return $this->sendverification;
    }

    /**
     * Set morionid
     *
     * @param int $morionid
     *
     * @return InfoContactworkplace
     */
    public function setMorionid($morionid)
    {
        $this->morionid = $morionid;
    
        return $this;
    }

    /**
     * Get morionid
     *
     * @return int
     */
    public function getMorionid()
    {
        return $this->morionid;
    }

    /**
     * Set contact
     *
     * @param \TeamSoft\CrmRepositoryBundle\Entity\InfoContact $contact
     *
     * @return InfoContactworkplace
     */
    public function setContact(\TeamSoft\CrmRepositoryBundle\Entity\InfoContact $contact = null)
    {
        $this->contact = $contact;
    
        return $this;
    }

    /**
     * Get contact
     *
     * @return \TeamSoft\CrmRepositoryBundle\Entity\InfoContact
     */
    public function getContact()
    {
        return $this->contact;
    }

    /**
     * Set specialization
     *
     * @param \TeamSoft\CrmRepositoryBundle\Entity\InfoDictionary $specialization
     *
     * @return InfoContactworkplace
     */
    public function setSpecialization(\TeamSoft\CrmRepositoryBundle\Entity\InfoDictionary $specialization = null)
    {
        $this->specialization = $specialization;
    
        return $this;
    }

    /**
     * Get specialization
     *
     * @return \TeamSoft\CrmRepositoryBundle\Entity\InfoDictionary
     */
    public function getSpecialization()
    {
        return $this->specialization;
    }

    /**
     * Set department
     *
     * @param \TeamSoft\CrmRepositoryBundle\Entity\InfoDictionary $department
     *
     * @return InfoContactworkplace
     */
    public function setDepartment(\TeamSoft\CrmRepositoryBundle\Entity\InfoDictionary $department = null)
    {
        $this->department = $department;
    
        return $this;
    }

    /**
     * Get department
     *
     * @return \TeamSoft\CrmRepositoryBundle\Entity\InfoDictionary
     */
    public function getDepartment()
    {
        return $this->department;
    }

    /**
     * Set company
     *
     * @param \TeamSoft\CrmRepositoryBundle\Entity\InfoCompany $company
     *
     * @return InfoContactworkplace
     */
    public function setCompany(\TeamSoft\CrmRepositoryBundle\Entity\InfoCompany $company = null)
    {
        $this->company = $company;
    
        return $this;
    }

    /**
     * Get company
     *
     * @return \TeamSoft\CrmRepositoryBundle\Entity\InfoCompany
     */
    public function getCompany()
    {
        return $this->company;
    }

    /**
     * Set position
     *
     * @param \TeamSoft\CrmRepositoryBundle\Entity\InfoDictionary $position
     *
     * @return InfoContactworkplace
     */
    public function setPosition(\TeamSoft\CrmRepositoryBundle\Entity\InfoDictionary $position = null)
    {
        $this->position = $position;
    
        return $this;
    }

    /**
     * Get position
     *
     * @return \TeamSoft\CrmRepositoryBundle\Entity\InfoDictionary
     */
    public function getPosition()
    {
        return $this->position;
    }

    public function getCompanyId()
    {
        return $this->getCompany() ? $this->getCompany()->getId() : null;
    }

    public function getContactId()
    {
        return $this->getContact() ? $this->getContact()->getId() : null;
    }

    public function getPositionId()
    {
        return $this->getPosition() ? $this->getPosition()->getId() : null;
    }
}
