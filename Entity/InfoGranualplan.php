<?php

namespace TeamSoft\CrmRepositoryBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use \TeamSoft\CrmRepositoryBundle\Entity\InfoPreparation;

/**
 * InfoGranualplan
 *
 * @ORM\Table(name="info_granualplan")
 * @ORM\Entity(repositoryClass="TeamSoft\CrmRepositoryBundle\Repository\InfoGranualplan")
 */
class InfoGranualplan
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;


    /**
     * @var  \TeamSoft\CrmRepositoryBundle\Entity\InfoPreparation
     *
     * @ORM\ManyToOne(targetEntity="\TeamSoft\CrmRepositoryBundle\Entity\InfoPreparation")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="prep_id", referencedColumnName="id")
     * })
     */
    private $prep;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="DateFrom", type="datetime", nullable=true)
     */
    private $datefrom;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="DateTill", type="datetime", nullable=true)
     */
    private $datetill;

    /**
     * @var int|null
     *
     * @ORM\Column(name="limit", type="integer", nullable=true)
     */
    private $limit;

    /**
     * @var InfoDirection
     *
     * @ORM\ManyToOne(targetEntity="InfoDirection")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="direction_id", referencedColumnName="id")
     * })
     */
    private $direction;

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set prep.
     *
     * @param \TeamSoft\CrmRepositoryBundle\Entity\InfoPreparation $prep
     *
     * @return InfoGranualplan
     */
    public function setPrepId(InfoPreparation $prep = null)
    {
        $this->prep = $prep;

        return $this;
    }

    /**
     * Get prep.
     *
     * @return \TeamSoft\CrmRepositoryBundle\Entity\InfoPreparation
     */
    public function getPrepId()
    {
        return $this->prep;
    }

    /**
     * Set datefrom.
     *
     * @param \DateTime|null $datefrom
     *
     * @return InfoGranualplan
     */
    public function setDatefrom($datefrom = null)
    {
        $this->datefrom = $datefrom;

        return $this;
    }

    /**
     * Get datefrom.
     *
     * @return \DateTime|null
     */
    public function getDatefrom()
    {
        return $this->datefrom;
    }

    /**
     * Set datetill.
     *
     * @param \DateTime|null $datetill
     *
     * @return InfoGranualplan
     */
    public function setDatetill($datetill = null)
    {
        $this->datetill = $datetill;

        return $this;
    }

    /**
     * Get datetill.
     *
     * @return \DateTime|null
     */
    public function getDatetill()
    {
        return $this->datetill;
    }

    /**
     * Set limit.
     *
     * @param int|null $limit
     *
     * @return InfoGranualplan
     */
    public function setLimit($limit = null)
    {
        $this->limit = $limit;

        return $this;
    }

    /**
     * Get limit.
     *
     * @return int|null
     */
    public function getLimit()
    {
        return $this->limit;
    }

    /**
     * Set prep.
     *
     * @param \TeamSoft\CrmRepositoryBundle\Entity\InfoPreparation|null $prep
     *
     * @return InfoGranualplan
     */
    public function setPrep(\TeamSoft\CrmRepositoryBundle\Entity\InfoPreparation $prep = null)
    {
        $this->prep = $prep;

        return $this;
    }

    /**
     * Get prep.
     *
     * @return \TeamSoft\CrmRepositoryBundle\Entity\InfoPreparation|null
     */
    public function getPrep()
    {
        return $this->prep;
    }

    /**
     * @return InfoDirection
     */
    public function getDirection(): ?InfoDirection
    {
        return $this->direction;
    }

    /**
     * @param InfoDirection $direction
     * @return self
     */
    public function setDirection(?InfoDirection $direction): self
    {
        $this->direction = $direction;
        return $this;
    }
}
