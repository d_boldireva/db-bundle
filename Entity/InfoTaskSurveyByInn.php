<?php

namespace TeamSoft\CrmRepositoryBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * InfoTaskSurveyByInn
 *
 * @ORM\Table(name="info_tasksurveybyinn")
 * @ORM\Entity
 */
class InfoTaskSurveyByInn
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="actual_shipment_percentage", type="integer", nullable=true)
     */
    private $actualShipmentPercentage;

    /**
     * @var string
     *
     * @ORM\Column(name="comment", type="text", length=-1, nullable=true)
     */
    private $comment;

    /**
     * @var \InfoTask
     *
     * @ORM\ManyToOne(targetEntity="InfoTask")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="task_id", referencedColumnName="id")
     * })
     */
    private $task;

    /**
     * @var \InfoCustomdictionaryvalue
     *
     * @ORM\ManyToOne(targetEntity="InfoCustomdictionaryvalue")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="inn_id", referencedColumnName="id")
     * })
     */
    private $inn;

    /**
     * @var \InfoCustomdictionaryvalue
     *
     * @ORM\ManyToOne(targetEntity="InfoCustomdictionaryvalue")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="purchase_type_id", referencedColumnName="id")
     * })
     */
    private $purchaseType;

    /**
     * @var \InfoCustomdictionaryvalue
     *
     * @ORM\ManyToOne(targetEntity="InfoCustomdictionaryvalue")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="purchase_state_id", referencedColumnName="id")
     * })
     */
    private $purchaseState;

    /**
     * @var \InfoCustomdictionaryvalue
     *
     * @ORM\ManyToOne(targetEntity="InfoCustomdictionaryvalue")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="delivery_schedule_id", referencedColumnName="id")
     * })
     */
    private $deliverySchedule;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param integer $actualShipmentPercentage
     * @return InfoTaskSurveyByInn
     */
    public function setActualShipmentPercentage($actualShipmentPercentage)
    {
        $this->actualShipmentPercentage = $actualShipmentPercentage;

        return $this;
    }

    /**
     * @return integer
     */
    public function getActualShipmentPercentage()
    {
        return $this->actualShipmentPercentage;
    }

    /**
     * @param string $comment
     * @return InfoTaskSurveyByInn
     */
    public function setComment($comment)
    {
        $this->comment = $comment;

        return $this;
    }

    /**
     * @return string
     */
    public function getComment()
    {
        return $this->comment;
    }

    /**
     * @param \TeamSoft\CrmRepositoryBundle\Entity\InfoTask $task
     * @return InfoTaskSurveyByInn
     */
    public function setTask(\TeamSoft\CrmRepositoryBundle\Entity\InfoTask $task)
    {
        $this->task = $task;

        return $this;
    }

    /**
     * @return \TeamSoft\CrmRepositoryBundle\Entity\InfoTask
     */
    public function getTask()
    {
        return $this->task;
    }

    /**
     * @param \TeamSoft\CrmRepositoryBundle\Entity\InfoCustomdictionaryvalue $value
     * @return InfoTaskSurveyByInn
     */
    public function setInn(\TeamSoft\CrmRepositoryBundle\Entity\InfoCustomdictionaryvalue $value = null)
    {
        $this->inn = $value;

        return $this;
    }

    /**
     * @return \TeamSoft\CrmRepositoryBundle\Entity\InfoCustomdictionaryvalue
     */
    public function getInn()
    {
        return $this->inn;
    }

    /**
     * @param \TeamSoft\CrmRepositoryBundle\Entity\InfoCustomdictionaryvalue $value
     * @return InfoTaskSurveyByInn
     */
    public function setPurchaseType(\TeamSoft\CrmRepositoryBundle\Entity\InfoCustomdictionaryvalue $value = null)
    {
        $this->purchaseType = $value;

        return $this;
    }

    /**
     * @return \TeamSoft\CrmRepositoryBundle\Entity\InfoCustomdictionaryvalue
     */
    public function getPurchaseType()
    {
        return $this->purchaseType;
    }

    /**
     * @param \TeamSoft\CrmRepositoryBundle\Entity\InfoCustomdictionaryvalue $value
     * @return InfoTaskSurveyByInn
     */
    public function setPurchaseState(\TeamSoft\CrmRepositoryBundle\Entity\InfoCustomdictionaryvalue $value = null)
    {
        $this->purchaseState = $value;

        return $this;
    }

    /**
     * @return \TeamSoft\CrmRepositoryBundle\Entity\InfoCustomdictionaryvalue
     */
    public function getPurchaseState()
    {
        return $this->purchaseState;
    }

    /**
     * @param \TeamSoft\CrmRepositoryBundle\Entity\InfoCustomdictionaryvalue $value
     * @return InfoTaskSurveyByInn
     */
    public function setDeliverySchedule(\TeamSoft\CrmRepositoryBundle\Entity\InfoCustomdictionaryvalue $value = null)
    {
        $this->deliverySchedule = $value;

        return $this;
    }

    /**
     * @return \TeamSoft\CrmRepositoryBundle\Entity\InfoCustomdictionaryvalue
     */
    public function getDeliverySchedule()
    {
        return $this->deliverySchedule;
    }

    /**
     * @return intager
     */
    public function getInnId()
    {
        return $this->inn ? $this->inn->getId() : null;
    }

    /**
     * @return intager
     */
    public function getPurchaseTypeId()
    {
        return $this->purchaseType ? $this->purchaseType->getId() : null;
    }

    /**
     * @return intager
     */
    public function getPurchaseStateId()
    {
        return $this->purchaseState ? $this->purchaseState->getId() : null;
    }

    /**
     * @return intager
     */
    public function getDeliveryScheduleId()
    {
        return $this->deliverySchedule ? $this->deliverySchedule->getId() : null;
    }
}
