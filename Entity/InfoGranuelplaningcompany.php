<?php

namespace TeamSoft\CrmRepositoryBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * InfoGranuelplaningcompany
 *
 * @ORM\Table(name="info_granuelplaningcompany")
 * @ORM\Entity(repositoryClass="TeamSoft\CrmRepositoryBundle\Repository\InfoGranuelplaningcompany")
 */
class InfoGranuelplaningcompany
{
    /**
     * @var int
     *
     * @ORM\Column(name="ID", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var \TeamSoft\CrmRepositoryBundle\Entity\InfoCompany
     *
     * @ORM\ManyToOne(targetEntity="\TeamSoft\CrmRepositoryBundle\Entity\InfoCompany")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="company_id", referencedColumnName="id")
     * })
     */
    private $company;

    /**
     * @var \TeamSoft\CrmRepositoryBundle\Entity\InfoUser
     *
     * @ORM\ManyToOne(targetEntity="\TeamSoft\CrmRepositoryBundle\Entity\InfoUser")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     * })
     */
    private $user;



    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set company.
     *
     * @param \TeamSoft\CrmRepositoryBundle\Entity\InfoCompany|null $company
     *
     * @return InfoGranuelplaningcompany
     */
    public function setCompany(\TeamSoft\CrmRepositoryBundle\Entity\InfoCompany $company = null)
    {
        $this->company = $company;

        return $this;
    }

    /**
     * Get company.
     *
     * @return \TeamSoft\CrmRepositoryBundle\Entity\InfoCompany|null
     */
    public function getCompany()
    {
        return $this->company;
    }

    /**
     * Set user.
     *
     * @param \TeamSoft\CrmRepositoryBundle\Entity\InfoUser|null $user
     *
     * @return InfoGranuelplaningcompany
     */
    public function setUser(\TeamSoft\CrmRepositoryBundle\Entity\InfoUser $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user.
     *
     * @return \TeamSoft\CrmRepositoryBundle\Entity\InfoUser|null
     */
    public function getUser()
    {
        return $this->user;
    }
}
