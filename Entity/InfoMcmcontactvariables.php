<?php

namespace TeamSoft\CrmRepositoryBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * InfoMcmcontactvariables
 *
 * @ORM\Table(name="info_mcmcontactvariables")
 * @ORM\Entity
 */
class InfoMcmcontactvariables implements ServiceFieldInterface
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var InfoContact
     *
     * @ORM\ManyToOne(targetEntity="InfoContact", inversedBy="mcmContactVariablesCollection")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="contact_id", referencedColumnName="id")
     * })
     */
    private $contact;

    /**
     * /**
     * @var InfoMcmcontentvariable
     *
     * @ORM\ManyToOne(targetEntity="InfoMcmcontentvariable")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="mcmvariable_id", referencedColumnName="id")
     * })
     */
    private $mcmvariable;

    /**
     * @var string
     *
     * @ORM\Column(name="stringValue", type="string", length=255, nullable=true)
     */
    private $stringvalue;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="currenttime", type="datetime", nullable=true)
     */
    private $currenttime;

    /**
     * @var string
     *
     * @ORM\Column(name="moduser", type="string", length=16, nullable=true)
     */
    private $moduser;

    /**
     * @var string
     *
     * @ORM\Column(name="guid", type="guid", nullable=true)
     */
    private $guid;



    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set stringvalue
     *
     * @param string $stringvalue
     *
     * @return InfoMcmcontactvariables
     */
    public function setStringvalue($stringvalue)
    {
        $this->stringvalue = $stringvalue;
    
        return $this;
    }

    /**
     * Get stringvalue
     *
     * @return string
     */
    public function getStringvalue()
    {
        return $this->stringvalue;
    }

    /**
     * Set currenttime
     *
     * @param \DateTime $currenttime
     *
     * @return InfoMcmcontactvariables
     */
    public function setCurrenttime(\DateTime $currenttime)
    {
        $this->currenttime = $currenttime;
    
        return $this;
    }

    /**
     * Get currenttime
     *
     * @return \DateTime
     */
    public function getCurrenttime()
    {
        return $this->currenttime;
    }

    /**
     * Set moduser
     *
     * @param string $moduser
     *
     * @return InfoMcmcontactvariables
     */
    public function setModuser($moduser)
    {
        $this->moduser = $moduser;
    
        return $this;
    }

    /**
     * Get moduser
     *
     * @return string
     */
    public function getModuser()
    {
        return $this->moduser;
    }

    /**
     * Set guid
     *
     * @param string $guid
     *
     * @return InfoMcmcontactvariables
     */
    public function setGuid($guid)
    {
        $this->guid = $guid;
    
        return $this;
    }

    /**
     * Get guid
     *
     * @return string
     */
    public function getGuid()
    {
        return $this->guid;
    }

    /**
     * Set contact
     *
     * @param \TeamSoft\CrmRepositoryBundle\Entity\InfoContact $contact
     *
     * @return InfoMcmcontactvariables
     */
    public function setContact(\TeamSoft\CrmRepositoryBundle\Entity\InfoContact $contact = null)
    {
        $this->contact = $contact;
    
        return $this;
    }

    /**
     * Get contact
     *
     * @return \TeamSoft\CrmRepositoryBundle\Entity\InfoContact
     */
    public function getContact()
    {
        return $this->contact;
    }

    /**
     * Set mcmvariable
     *
     * @param \TeamSoft\CrmRepositoryBundle\Entity\InfoMcmcontentvariable $mcmvariable
     *
     * @return InfoMcmcontactvariables
     */
    public function setMcmvariable(\TeamSoft\CrmRepositoryBundle\Entity\InfoMcmcontentvariable $mcmvariable = null)
    {
        $this->mcmvariable = $mcmvariable;
    
        return $this;
    }

    /**
     * Get mcmvariable
     *
     * @return \TeamSoft\CrmRepositoryBundle\Entity\InfoMcmcontentvariable
     */
    public function getMcmvariable()
    {
        return $this->mcmvariable;
    }
}
