<?php

namespace TeamSoft\CrmRepositoryBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * InfoAddinfoactiontype
 *
 * @ORM\Table(name="info_addinfoactiontype")
 * @ORM\Entity
 */
class InfoAddinfoactiontype implements ServiceFieldInterface
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="guid", type="guid", nullable=true)
     */
    private $guid;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="currenttime", type="datetime", nullable=true)
     */
    private $currenttime;

    /**
     * @var string
     *
     * @ORM\Column(name="moduser", type="string", length=16, nullable=true)
     */
    private $moduser;

    /**
     * @var InfoAddinfotype
     *
     * @ORM\ManyToOne(targetEntity="InfoAddinfotype", inversedBy="addInfoActionTypes")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="AddInfoType_id", referencedColumnName="id")
     * })
     */
    private $addinfotype;

    /**
     * @var InfoActiontype
     *
     * @ORM\ManyToOne(targetEntity="InfoActiontype")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="SubjType_id", referencedColumnName="id")
     * })
     */
    private $subjtype;

    /**
     * @var InfoRole
     *
     * @ORM\ManyToOne(targetEntity="InfoRole")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="role_id", referencedColumnName="id")
     * })
     */
    private $role;

    public function __clone()
    {
        $this->id = null;
        $this->guid = null;
        $this->currenttime = null;
    }


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set guid
     *
     * @param string $guid
     *
     * @return InfoAddinfoactiontype
     */
    public function setGuid($guid)
    {
        $this->guid = $guid;
    
        return $this;
    }

    /**
     * Get guid
     *
     * @return string
     */
    public function getGuid()
    {
        return $this->guid;
    }

    /**
     * Set currenttime
     *
     * @param \DateTime $currenttime
     *
     * @return InfoAddinfoactiontype
     */
    public function setCurrenttime(\DateTime $currenttime)
    {
        $this->currenttime = $currenttime;
    
        return $this;
    }

    /**
     * Get currenttime
     *
     * @return \DateTime
     */
    public function getCurrenttime()
    {
        return $this->currenttime;
    }

    /**
     * Set moduser
     *
     * @param string $moduser
     *
     * @return InfoAddinfoactiontype
     */
    public function setModuser($moduser)
    {
        $this->moduser = $moduser;
    
        return $this;
    }

    /**
     * Get moduser
     *
     * @return string
     */
    public function getModuser()
    {
        return $this->moduser;
    }

    /**
     * Set addinfotype
     *
     * @param \TeamSoft\CrmRepositoryBundle\Entity\InfoAddinfotype $addinfotype
     *
     * @return InfoAddinfoactiontype
     */
    public function setAddinfotype(\TeamSoft\CrmRepositoryBundle\Entity\InfoAddinfotype $addinfotype = null)
    {
        $this->addinfotype = $addinfotype;

        return $this;
    }

    /**
     * Get addinfotype
     *
     * @return \TeamSoft\CrmRepositoryBundle\Entity\InfoAddinfotype
     */
    public function getAddinfotype()
    {
        return $this->addinfotype;
    }

    /**
     * Set subjtype
     *
     * @param \TeamSoft\CrmRepositoryBundle\Entity\InfoActiontype $subjtype
     *
     * @return InfoAddinfoactiontype
     */
    public function setSubjtype(\TeamSoft\CrmRepositoryBundle\Entity\InfoActiontype $subjtype = null)
    {
        $this->subjtype = $subjtype;

        return $this;
    }

    /**
     * Get subjtype
     *
     * @return \TeamSoft\CrmRepositoryBundle\Entity\InfoActiontype
     */
    public function getSubjtype()
    {
        return $this->subjtype;
    }

    /**
     * Set role
     *
     * @param InfoRole $role
     *
     * @return InfoAddinfocontacttype
     */
    public function setRole(InfoRole $role = null)
    {
        $this->role = $role;

        return $this;
    }

    /**
     * Get role
     *
     * @return InfoRole
     */
    public function getRole()
    {
        return $this->role;
    }
}
