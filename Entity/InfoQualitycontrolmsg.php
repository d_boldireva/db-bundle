<?php

namespace TeamSoft\CrmRepositoryBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints\DateTime;

/**
 * InfoQualitycontrolmsg
 *
 * @ORM\Table(name="info_qualitycontrolmsg")
 * @ORM\Entity
 */
class InfoQualitycontrolmsg implements ServiceFieldInterface
{
    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dt", type="datetime", nullable=true)
     */
    private $dt;

    /**
     * @var string
     *
     * @ORM\Column(name="msg", type="string", length=255, nullable=true)
     */
    private $msg;

    /**
     * @var int
     *
     * @ORM\Column(name="isShownAfterSync", type="integer", nullable=true)
     */
    private $isshownaftersync;

    /**
     * @var int
     *
     * @ORM\Column(name="isClientShown", type="integer", nullable=true)
     */
    private $isclientshown;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="currenttime", type="datetime", nullable=true)
     */
    private $currenttime;

    /**
     * @var string
     *
     * @ORM\Column(name="moduser", type="string", length=16, nullable=true)
     */
    private $moduser;

    /**
     * @var guid
     *
     * @ORM\Column(name="guid", type="guid", nullable=true)
     */
    private $guid;

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var \TeamSoft\CrmRepositoryBundle\Entity\InfoQualitycontrol
     *
     * @ORM\ManyToOne(targetEntity="InfoQualitycontrol")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="qualitycontrol_id", referencedColumnName="id")
     * })
     */
    private $qualitycontrol;



    /**
     * Set dt
     *
     * @param \DateTime $dt
     *
     * @return InfoQualitycontrolmsg
     */
    public function setDt(\DateTime $dt)
    {
        $this->dt = $dt;

        return $this;
    }

    /**
     * Get dt
     *
     * @return \DateTime
     */
    public function getDt()
    {
        return $this->dt;
    }

    /**
     * Set msg
     *
     * @param string $msg
     *
     * @return InfoQualitycontrolmsg
     */
    public function setMsg($msg)
    {
        $this->msg = $msg;

        return $this;
    }

    /**
     * Get msg
     *
     * @return string
     */
    public function getMsg()
    {
        return $this->msg;
    }

    /**
     * Set isshownaftersync
     *
     * @param int $isshownaftersync
     *
     * @return InfoQualitycontrolmsg
     */
    public function setIsshownaftersync($isshownaftersync)
    {
        $this->isshownaftersync = $isshownaftersync;

        return $this;
    }

    /**
     * Get isshownaftersync
     *
     * @return int
     */
    public function getIsshownaftersync()
    {
        return $this->isshownaftersync;
    }

    /**
     * Set isclientshown
     *
     * @param int $isclientshown
     *
     * @return InfoQualitycontrolmsg
     */
    public function setIsclientshown($isclientshown)
    {
        $this->isclientshown = $isclientshown;

        return $this;
    }

    /**
     * Get isclientshown
     *
     * @return int
     */
    public function getIsclientshown()
    {
        return $this->isclientshown;
    }

    /**
     * Set currenttime
     *
     * @param \DateTime $currenttime
     *
     * @return InfoQualitycontrolmsg
     */
    public function setCurrenttime(\DateTime $currenttime) {

        $this->currenttime = $currenttime;

        return $this;
    }

    /**
     * Get currenttime
     *
     * @return \DateTime
     */
    public function getCurrenttime()
    {
        return $this->currenttime;
    }

    /**
     * Set moduser
     *
     * @param string $moduser
     *
     * @return InfoQualitycontrolmsg
     */
    public function setModuser($moduser)
    {
        $this->moduser = $moduser;

        return $this;
    }

    /**
     * Get moduser
     *
     * @return string
     */
    public function getModuser()
    {
        return $this->moduser;
    }

    /**
     * Set guid
     *
     * @param guid $guid
     *
     * @return InfoQualitycontrolmsg
     */
    public function setGuid($guid)
    {
        $this->guid = $guid;

        return $this;
    }

    /**
     * Get guid
     *
     * @return guid
     */
    public function getGuid()
    {
        return $this->guid;
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set qualitycontrol
     *
     * @param \TeamSoft\CrmRepositoryBundle\Entity\InfoQualitycontrol $qualitycontrol
     *
     * @return InfoQualitycontrolmsg
     */
    public function setQualitycontrol(\TeamSoft\CrmRepositoryBundle\Entity\InfoQualitycontrol $qualitycontrol = null)
    {
        $this->qualitycontrol = $qualitycontrol;

        return $this;
    }

    /**
     * Get qualitycontrol
     *
     * @return \TeamSoft\CrmRepositoryBundle\Entity\InfoQualitycontrol
     */
    public function getQualitycontrol()
    {
        return $this->qualitycontrol;
    }
}
