<?php

namespace TeamSoft\CrmRepositoryBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * InfoActioninfo
 *
 * @ORM\Table(name="info_actioninfo")
 * @ORM\Entity
 */
class InfoActioninfo extends AdditionalField implements ServiceFieldInterface
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="StringValue", type="string", length=250, nullable=true)
     */
    private $stringvalue;

    /**
     * @var string
     *
     * @ORM\Column(name="FloatValue", type="decimal", precision=18, scale=4, nullable=true)
     */
    private $floatvalue;

    /**
     * @var int
     *
     * @ORM\Column(name="IntValue", type="integer", nullable=true)
     */
    private $intvalue;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="DateValue", type="datetime", nullable=true)
     */
    private $datevalue;

    /**
     * @var int
     *
     * @ORM\Column(name="ShowInEdit", type="integer", nullable=true)
     */
    private $showinedit = '1';

    /**
     * @var string
     *
     * @ORM\Column(name="guid", type="guid", nullable=true)
     */
    private $guid;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="currenttime", type="datetime", nullable=true)
     */
    private $currenttime;

    /**
     * @var string
     *
     * @ORM\Column(name="moduser", type="string", length=16, nullable=true)
     */
    private $moduser;

    /**
     * @var int
     *
     * @ORM\Column(name="sysdictvalue_id", type="integer", nullable=true)
     */
    private $sysdictvalueId;

    private $sysdictvalue;

    /**
     * @var InfoCustomdictionaryvalue
     *
     * @ORM\ManyToOne(targetEntity="InfoCustomdictionaryvalue")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="DictValue_id", referencedColumnName="id")
     * })
     */
    private $dictvalue;

    /**
     * @var InfoAction
     *
     * @ORM\ManyToOne(targetEntity="InfoAction", inversedBy="actionInfoCollection")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="Subj_id", referencedColumnName="id")
     * })
     */
    private $subj;

    /**
     * @var InfoAddinfotype
     *
     * @ORM\ManyToOne(targetEntity="InfoAddinfotype")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="InfoType_id", referencedColumnName="id")
     * })
     */
    private $infotype;



    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set stringvalue
     *
     * @param string $stringvalue
     *
     * @return InfoActioninfo
     */
    public function setStringvalue($stringvalue)
    {
        $this->stringvalue = $stringvalue;
    
        return $this;
    }

    /**
     * Get stringvalue
     *
     * @return string
     */
    public function getStringvalue()
    {
        return $this->stringvalue;
    }

    /**
     * Set floatvalue
     *
     * @param string $floatvalue
     *
     * @return InfoActioninfo
     */
    public function setFloatvalue($floatvalue)
    {
        $this->floatvalue = $floatvalue;
    
        return $this;
    }

    /**
     * Get floatvalue
     *
     * @return string
     */
    public function getFloatvalue()
    {
        return $this->floatvalue;
    }

    /**
     * Set intvalue
     *
     * @param int $intvalue
     *
     * @return InfoActioninfo
     */
    public function setIntvalue($intvalue)
    {
        $this->intvalue = $intvalue;
    
        return $this;
    }

    /**
     * Get intvalue
     *
     * @return int
     */
    public function getIntvalue()
    {
        return $this->intvalue;
    }

    /**
     * Set datevalue
     *
     * @param \DateTime $datevalue
     *
     * @return InfoActioninfo
     */
    public function setDatevalue($datevalue)
    {
        $this->datevalue = $datevalue;
    
        return $this;
    }

    /**
     * Get datevalue
     *
     * @return \DateTime
     */
    public function getDatevalue()
    {
        return $this->datevalue;
    }

    /**
     * Set showinedit
     *
     * @param int $showinedit
     *
     * @return InfoActioninfo
     */
    public function setShowinedit($showinedit)
    {
        $this->showinedit = $showinedit;
    
        return $this;
    }

    /**
     * Get showinedit
     *
     * @return int
     */
    public function getShowinedit()
    {
        return $this->showinedit;
    }

    /**
     * Set guid
     *
     * @param string $guid
     *
     * @return InfoActioninfo
     */
    public function setGuid($guid)
    {
        $this->guid = $guid;
    
        return $this;
    }

    /**
     * Get guid
     *
     * @return string
     */
    public function getGuid()
    {
        return $this->guid;
    }

    /**
     * Set currenttime
     *
     * @param \DateTime $currenttime
     *
     * @return InfoActioninfo
     */
    public function setCurrenttime(\DateTime $currenttime)
    {
        $this->currenttime = $currenttime;
    
        return $this;
    }

    /**
     * Get currenttime
     *
     * @return \DateTime
     */
    public function getCurrenttime()
    {
        return $this->currenttime;
    }

    /**
     * Set moduser
     *
     * @param string $moduser
     *
     * @return InfoActioninfo
     */
    public function setModuser($moduser)
    {
        $this->moduser = $moduser;
    
        return $this;
    }

    /**
     * Get moduser
     *
     * @return string
     */
    public function getModuser()
    {
        return $this->moduser;
    }

    /**
     * Set sysdictvalueId
     *
     * @param int $sysdictvalueId
     *
     * @return InfoActioninfo
     */
    public function setSysdictvalueId($sysdictvalueId)
    {
        $this->sysdictvalueId = $sysdictvalueId;
    
        return $this;
    }

    /**
     * Get sysdictvalueId
     *
     * @return int
     */
    public function getSysdictvalueId()
    {
        return $this->sysdictvalueId;
    }

    /**
     * @return string
     */
    public function getSysdictvalue()
    {
        return $this->sysdictvalue;
    }

    /**
     * @param string $sysdictvalue
     */
    public function setSysdictvalue($sysdictvalue)
    {
        $this->sysdictvalue = $sysdictvalue;
    }

    /**
     * Set dictvalue
     *
     * @param \TeamSoft\CrmRepositoryBundle\Entity\InfoCustomdictionaryvalue $dictvalue
     *
     * @return InfoActioninfo
     */
    public function setDictvalue(\TeamSoft\CrmRepositoryBundle\Entity\InfoCustomdictionaryvalue $dictvalue = null)
    {
        $this->dictvalue = $dictvalue;
    
        return $this;
    }

    /**
     * Get dictvalue
     *
     * @return \TeamSoft\CrmRepositoryBundle\Entity\InfoCustomdictionaryvalue
     */
    public function getDictvalue()
    {
        return $this->dictvalue;
    }

    /**
     * Set subj
     *
     * @param \TeamSoft\CrmRepositoryBundle\Entity\InfoAction $subj
     *
     * @return InfoActioninfo
     */
    public function setSubj(\TeamSoft\CrmRepositoryBundle\Entity\InfoAction $subj = null)
    {
        $this->subj = $subj;
    
        return $this;
    }

    /**
     * Get subj
     *
     * @return \TeamSoft\CrmRepositoryBundle\Entity\InfoAction
     */
    public function getSubj()
    {
        return $this->subj;
    }

    /**
     * Set infotype
     *
     * @param \TeamSoft\CrmRepositoryBundle\Entity\InfoAddinfotype $infotype
     *
     * @return InfoActioninfo
     */
    public function setInfotype(\TeamSoft\CrmRepositoryBundle\Entity\InfoAddinfotype $infotype = null)
    {
        $this->infotype = $infotype;
    
        return $this;
    }

    /**
     * Get infotype
     *
     * @return \TeamSoft\CrmRepositoryBundle\Entity\InfoAddinfotype
     */
    public function getInfotype()
    {
        return $this->infotype;
    }

    public function getInfoTypeId()
    {
        return $this->getInfotype() ? $this->getInfotype()->getId() : null;
    }

    public function getSubjId()
    {
        return $this->getSubj() ? $this->getSubj()->getId() : null;
    }

    public function getDictionaryValueId()
    {
        return $this->getDictvalue() ? $this->getDictvalue()->getId() : null;
    }
}
