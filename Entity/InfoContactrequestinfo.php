<?php

namespace TeamSoft\CrmRepositoryBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * InfoContactrequestinfo
 *
 * @ORM\Table(name="info_contactrequestinfo")
 * @ORM\Entity
 */
class InfoContactrequestinfo
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dt", type="datetime", nullable=true)
     */
    private $dt;

    /**
     * @var string
     *
     * @ORM\Column(name="status", type="string", length=255, nullable=true)
     */
    private $status;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="string", length=255, nullable=true)
     */
    private $description;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="currenttime", type="datetime", nullable=true)
     */
    private $currenttime;

    /**
     * @var string
     *
     * @ORM\Column(name="moduser", type="string", length=16, nullable=true)
     */
    private $moduser;

    /**
     * @var guid
     *
     * @ORM\Column(name="guid", type="guid", nullable=true)
     */
    private $guid;

    /**
     * @var \InfoPreparationbrend
     *
     * @ORM\ManyToOne(targetEntity="InfoPreparationbrend")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="brend_id", referencedColumnName="id")
     * })
     */
    private $brend;

    /**
     * @var \InfoContact
     *
     * @ORM\ManyToOne(targetEntity="InfoContact")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="contact_id", referencedColumnName="id")
     * })
     */
    private $contact;

    /**
     * @var \InfoCustomdictionaryvalue
     *
     * @ORM\ManyToOne(targetEntity="InfoCustomdictionaryvalue")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="infotype_id", referencedColumnName="id")
     * })
     */
    private $infotype;

    /**
     * @var \InfoTask
     *
     * @ORM\ManyToOne(targetEntity="InfoTask")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="task_id", referencedColumnName="id")
     * })
     */
    private $task;

    /**
     * @var \InfoCustomdictionaryvalue
     *
     * @ORM\ManyToOne(targetEntity="InfoCustomdictionaryvalue")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="want_id", referencedColumnName="id")
     * })
     */
    private $want;

    /**
     * @var \InfoCustomdictionaryvalue
     *
     * @ORM\ManyToOne(targetEntity="InfoCustomdictionaryvalue")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="waystype_id", referencedColumnName="id")
     * })
     */
    private $waystype;



    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set dt
     *
     * @param \DateTime $dt
     *
     * @return InfoContactrequestinfo
     */
    public function setDt($dt)
    {
        $this->dt = $dt;
    
        return $this;
    }

    /**
     * Get dt
     *
     * @return \DateTime
     */
    public function getDt()
    {
        return $this->dt;
    }

    /**
     * Set status
     *
     * @param string $status
     *
     * @return InfoContactrequestinfo
     */
    public function setStatus($status)
    {
        $this->status = $status;
    
        return $this;
    }

    /**
     * Get status
     *
     * @return string
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set description
     *
     * @param string $description
     *
     * @return InfoContactrequestinfo
     */
    public function setDescription($description)
    {
        $this->description = $description;
    
        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set currenttime
     *
     * @param \DateTime $currenttime
     *
     * @return InfoContactrequestinfo
     */
    public function setCurrenttime($currenttime)
    {
        $this->currenttime = $currenttime;
    
        return $this;
    }

    /**
     * Get currenttime
     *
     * @return \DateTime
     */
    public function getCurrenttime()
    {
        return $this->currenttime;
    }

    /**
     * Set time
     *
     * @param string $time
     *
     * @return InfoContactrequestinfo
     */
    public function setTime($time)
    {
        $this->time = $time;
    
        return $this;
    }

    /**
     * Get time
     *
     * @return string
     */
    public function getTime()
    {
        return $this->time;
    }

    /**
     * Set moduser
     *
     * @param string $moduser
     *
     * @return InfoContactrequestinfo
     */
    public function setModuser($moduser)
    {
        $this->moduser = $moduser;
    
        return $this;
    }

    /**
     * Get moduser
     *
     * @return string
     */
    public function getModuser()
    {
        return $this->moduser;
    }

    /**
     * Set guid
     *
     * @param guid $guid
     *
     * @return InfoContactrequestinfo
     */
    public function setGuid($guid)
    {
        $this->guid = $guid;
    
        return $this;
    }

    /**
     * Get guid
     *
     * @return guid
     */
    public function getGuid()
    {
        return $this->guid;
    }

    /**
     * Set brend
     *
     * @param \TeamSoft\CrmRepositoryBundle\Entity\InfoPreparationbrend $brend
     *
     * @return InfoContactrequestinfo
     */
    public function setBrend(\TeamSoft\CrmRepositoryBundle\Entity\InfoPreparationbrend $brend = null)
    {
        $this->brend = $brend;
    
        return $this;
    }

    /**
     * Get brend
     *
     * @return \TeamSoft\CrmRepositoryBundle\Entity\InfoPreparationbrend
     */
    public function getBrend()
    {
        return $this->brend;
    }

    /**
     * Set contact
     *
     * @param \TeamSoft\CrmRepositoryBundle\Entity\InfoContact $contact
     *
     * @return InfoContactrequestinfo
     */
    public function setContact(\TeamSoft\CrmRepositoryBundle\Entity\InfoContact $contact = null)
    {
        $this->contact = $contact;
    
        return $this;
    }

    /**
     * Get contact
     *
     * @return \TeamSoft\CrmRepositoryBundle\Entity\InfoContact
     */
    public function getContact()
    {
        return $this->contact;
    }

    /**
     * Set infotype
     *
     * @param \TeamSoft\CrmRepositoryBundle\Entity\InfoCustomdictionaryvalue $infotype
     *
     * @return InfoContactrequestinfo
     */
    public function setInfotype(\TeamSoft\CrmRepositoryBundle\Entity\InfoCustomdictionaryvalue $infotype = null)
    {
        $this->infotype = $infotype;
    
        return $this;
    }

    /**
     * Get infotype
     *
     * @return \TeamSoft\CrmRepositoryBundle\Entity\InfoCustomdictionaryvalue
     */
    public function getInfotype()
    {
        return $this->infotype;
    }

    /**
     * Set task
     *
     * @param \TeamSoft\CrmRepositoryBundle\Entity\InfoTask $task
     *
     * @return InfoContactrequestinfo
     */
    public function setTask(\TeamSoft\CrmRepositoryBundle\Entity\InfoTask $task = null)
    {
        $this->task = $task;
    
        return $this;
    }

    /**
     * Get task
     *
     * @return \TeamSoft\CrmRepositoryBundle\Entity\InfoTask
     */
    public function getTask()
    {
        return $this->task;
    }

    /**
     * Set want
     *
     * @param \TeamSoft\CrmRepositoryBundle\Entity\InfoCustomdictionaryvalue $want
     *
     * @return InfoContactrequestinfo
     */
    public function setWant(\TeamSoft\CrmRepositoryBundle\Entity\InfoCustomdictionaryvalue $want = null)
    {
        $this->want = $want;
    
        return $this;
    }

    /**
     * Get want
     *
     * @return \TeamSoft\CrmRepositoryBundle\Entity\InfoCustomdictionaryvalue
     */
    public function getWant()
    {
        return $this->want;
    }

    /**
     * Set waystype
     *
     * @param \TeamSoft\CrmRepositoryBundle\Entity\InfoCustomdictionaryvalue $waystype
     *
     * @return InfoContactrequestinfo
     */
    public function setWaystype(\TeamSoft\CrmRepositoryBundle\Entity\InfoCustomdictionaryvalue $waystype = null)
    {
        $this->waystype = $waystype;
    
        return $this;
    }

    /**
     * Get waystype
     *
     * @return \TeamSoft\CrmRepositoryBundle\Entity\InfoCustomdictionaryvalue
     */
    public function getWaystype()
    {
        return $this->waystype;
    }
}
