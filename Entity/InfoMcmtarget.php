<?php

namespace TeamSoft\CrmRepositoryBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * InfoMcmtarget
 *
 * @ORM\Table(name="info_mcmtarget")
 * @ORM\Entity(repositoryClass="TeamSoft\CrmRepositoryBundle\Repository\InfoMcmtarget")
 */
class InfoMcmtarget implements ServiceFieldInterface
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="Name", type="string", length=255, nullable=true)
     */
    private $name;

    /**
     * @var int
     *
     * @ORM\ManyToOne(targetEntity="\TeamSoft\CrmRepositoryBundle\Entity\InfoUser")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="creator_id", referencedColumnName="id")
     * })
     */
    private $creator;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="createdate", type="datetime", nullable=true)
     */
    private $createdate;

    /**
     * @var int
     *
     * @ORM\Column(name="sms", type="integer", nullable=true)
     */
    private $sms;

    /**
     * @var int
     *
     * @ORM\Column(name="email", type="integer", nullable=true)
     */
    private $email;

    /**
     * @var int
     *
     * @ORM\Column(name="viber", type="integer", nullable=true)
     */
    private $viber;

    /**
     * @var int
     *
     * @ORM\Column(name="isapproved", type="integer", nullable=true)
     */
    private $isapproved;

    /**
     * @var int
     *
     * @ORM\Column(name="isarchive", type="integer", nullable=true)
     */
    private $isarchive;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="approved_date", type="datetime", nullable=true)
     */
    private $approvedDate;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="currenttime", type="datetime", nullable=true)
     */
    private $currenttime;

    /**
     * @var string
     *
     * @ORM\Column(name="moduser", type="string", length=16, nullable=true)
     */
    private $moduser;

    /**
     * @var string
     *
     * @ORM\Column(name="guid", type="guid", nullable=true)
     */
    private $guid;

    /**
     * @var \TeamSoft\CrmRepositoryBundle\Entity\InfoUser
     *
     * @ORM\ManyToOne(targetEntity="\TeamSoft\CrmRepositoryBundle\Entity\InfoUser")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="approved_by", referencedColumnName="id")
     * })
     */
    private $approvedBy;

    /**
     * @var \TeamSoft\CrmRepositoryBundle\Entity\InfoMcmtargetfilter
     *
     * @ORM\OneToMany(targetEntity="InfoMcmtargetfilter", mappedBy="mcmtarget", cascade={"persist", "remove"})
     */
    private $filterCollection;

    /**
     * @var int
     *
     * @ORM\Column(name="fromfile", type="integer", nullable=true)
     */
    private $fromfile;

    /**
     * @var \TeamSoft\CrmRepositoryBundle\Entity\InfoMcmcontactintarget
     *
     * @ORM\ManyToMany(targetEntity="InfoContact")
     * @ORM\JoinTable(
     *     name="info_mcmcontactintarget",
     *     joinColumns={@ORM\JoinColumn(name="target_id", referencedColumnName="id")},
     *     inverseJoinColumns={@ORM\JoinColumn(name="contact_id", referencedColumnName="id")}
     * )
     */
    private $contacts;

    /**
     * @var int
     *
     * @ORM\Column(name="sms_actual", type="integer", nullable=true)
     */
    private $smsActual;

    /**
     * @var int
     *
     * @ORM\Column(name="email_actual", type="integer", nullable=true)
     */
    private $emailActual;

    /**
     * @var int
     *
     * @ORM\Column(name="viber_actual", type="integer", nullable=true)
     */
    private $viberActual;

    /**
     * @var int
     *
     * @ORM\Column(name="is_custom_modify", type="integer", nullable=true)
     */
    private $isCustomModify;

    /**
     * @var int
     *
     * @ORM\Column(name="telegram", type="integer", nullable=true)
     */
    private $telegram;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->filterCollection = new \Doctrine\Common\Collections\ArrayCollection();
        $this->contacts = new \Doctrine\Common\Collections\ArrayCollection();
    }


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return InfoMcmtarget
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set creator
     *
     * @param \TeamSoft\CrmRepositoryBundle\Entity\InfoUser $creator
     *
     * @return InfoMcmtarget
     */
    public function setCreator(\TeamSoft\CrmRepositoryBundle\Entity\InfoUser $creator)
    {
        $this->creator = $creator;

        return $this;
    }

    /**
     * Get creator
     *
     * @return \TeamSoft\CrmRepositoryBundle\Entity\InfoUser
     */
    public function getCreator()
    {
        return $this->creator;
    }

    /**
     * Set createdate
     *
     * @param \DateTime $createdate
     *
     * @return InfoMcmtarget
     */
    public function setCreatedate($createdate)
    {
        $this->createdate = $createdate;

        return $this;
    }

    /**
     * Get createdate
     *
     * @return \DateTime
     */
    public function getCreatedate()
    {
        return $this->createdate;
    }

    /**
     * Set sms
     *
     * @param int $sms
     *
     * @return InfoMcmtarget
     */
    public function setSms($sms)
    {
        $this->sms = $sms;

        return $this;
    }

    /**
     * Get sms
     *
     * @return int
     */
    public function getSms()
    {
        return $this->sms;
    }

    /**
     * Set email
     *
     * @param int $email
     *
     * @return InfoMcmtarget
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Get email
     *
     * @return int
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set viber
     *
     * @param int $viber
     *
     * @return InfoMcmtarget
     */
    public function setViber($viber)
    {
        $this->viber = $viber;

        return $this;
    }

    /**
     * Get viber
     *
     * @return int
     */
    public function getViber()
    {
        return $this->viber;
    }

    /**
     * Set isapproved
     *
     * @param int $isapproved
     *
     * @return InfoMcmtarget
     */
    public function setIsapproved($isapproved)
    {
        $this->isapproved = $isapproved;

        return $this;
    }

    /**
     * Get isapproved
     *
     * @return int
     */
    public function getIsapproved()
    {
        return $this->isapproved;
    }

    /**
     * Set isarchive
     *
     * @param int $isarchive
     *
     * @return InfoMcmtarget
     */
    public function setIsarchive($isarchive)
    {
        $this->isarchive = $isarchive;

        return $this;
    }

    /**
     * Get isarchive
     *
     * @return int
     */
    public function getIsarchive()
    {
        return $this->isarchive;
    }

    /**
     * Set approvedDate
     *
     * @param \DateTime $approvedDate
     *
     * @return InfoMcmtarget
     */
    public function setApprovedDate($approvedDate)
    {
        $this->approvedDate = $approvedDate;

        return $this;
    }

    /**
     * Get approvedDate
     *
     * @return \DateTime
     */
    public function getApprovedDate()
    {
        return $this->approvedDate;
    }

    /**
     * Set approvedBy
     *
     * @param int $approvedBy
     *
     * @return InfoMcmtarget
     */
    public function setApprovedBy($approvedBy)
    {
        $this->approvedBy = $approvedBy;

        return $this;
    }

    /**
     * Get approvedBy
     *
     * @return int
     */
    public function getApprovedBy()
    {
        return $this->approvedBy;
    }

    /**
     * Set currenttime
     *
     * @param \DateTime $currenttime
     *
     * @return InfoMcmtarget
     */
    public function setCurrenttime(\DateTime $currenttime)
    {
        $this->currenttime = $currenttime;

        return $this;
    }

    /**
     * Get currenttime
     *
     * @return \DateTime
     */
    public function getCurrenttime()
    {
        return $this->currenttime;
    }

    /**
     * Set moduser
     *
     * @param string $moduser
     *
     * @return InfoMcmtarget
     */
    public function setModuser($moduser)
    {
        $this->moduser = $moduser;

        return $this;
    }

    /**
     * Get moduser
     *
     * @return string
     */
    public function getModuser()
    {
        return $this->moduser;
    }

    /**
     * Set guid
     *
     * @param string $guid
     *
     * @return InfoMcmtarget
     */
    public function setGuid($guid)
    {
        $this->guid = $guid;

        return $this;
    }

    /**
     * Get guid
     *
     * @return string
     */
    public function getGuid()
    {
        return $this->guid;
    }

    /**
     * Add filterCollection
     *
     * @param \TeamSoft\CrmRepositoryBundle\Entity\InfoMcmtargetfilter $filter
     * @return InfoMcmtarget
     */
    public function addFilterCollection(\TeamSoft\CrmRepositoryBundle\Entity\InfoMcmtargetfilter $filter)
    {
        $filter->setMcmtarget($this);
        $this->filterCollection->add($filter);
        return $this;
    }

    /**
     * Remove filterCollection
     *
     * @param \TeamSoft\CrmRepositoryBundle\Entity\InfoMcmtargetfilter $filter
     */
    public function removeFilterCollection(\TeamSoft\CrmRepositoryBundle\Entity\InfoMcmtargetfilter $filter)
    {
        $filter->setMcmtarget(null);
        $this->filterCollection->removeElement($filter);
    }

    /**
     * Get filterCollection
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getFilterCollection()
    {
        return $this->filterCollection;
    }

    /**
     * Get fromfile
     *
     * @return null|int
     */
    public function getFromfile(): ?int
    {
        return $this->fromfile;
    }

    /**
     * Set fromfile
     *
     * @param null|int $fromfile
     *
     * @return InfoMcmtarget
     */
    public function setFromfile(?int $fromfile): self
    {
        $this->fromfile = $fromfile;

        return $this;
    }

    /**
     * Add contact
     *
     * @param InfoContact $contact
     *
     * @return self
     */
    public function addContact(InfoContact $contact)
    {
        if (!$this->contacts->contains($contact)) {
            $this->contacts[] = $contact;
        }
        return $this;
    }

    /**
     * Remove contact
     *
     * @param InfoContact $contact
     *
     * @return self
     */
    public function removeContact(InfoContact $contact)
    {
        if ($this->contacts->contains($contact)) {
            $this->contacts->removeElement($contact);
        }

        return $this;
    }

    /**
     * Get contacts
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getContacts()
    {
        return $this->contacts;
    }

    /**
     * Set smsActual
     *
     * @param int $smsActual
     *
     * @return InfoMcmtarget
     */
    public function setSmsActual($smsActual)
    {
        $this->smsActual = $smsActual;

        return $this;
    }

    /**
     * Get smsActual
     *
     * @return int
     */
    public function getSmsActual()
    {
        return $this->smsActual;
    }

    /**
     * Set viberActual
     *
     * @param int $smsActual
     *
     * @return InfoMcmtarget
     */
    public function setViberActual($viberActual)
    {
        $this->viberActual = $viberActual;

        return $this;
    }

    /**
     * Get viberActual
     *
     * @return int
     */
    public function getViberActual()
    {
        return $this->viberActual;
    }

    /**
     * Set emailActual
     *
     * @param int $smsActual
     *
     * @return InfoMcmtarget
     */
    public function setEmailActual($emailActual)
    {
        $this->emailActual = $emailActual;

        return $this;
    }

    /**
     * Get emailActual
     *
     * @return int
     */
    public function getEmailActual()
    {
        return $this->emailActual;
    }

    /**
     * Set isCustomModify
     *
     * @param int $isCustomModify
     *
     * @return InfoMcmtarget
     */
    public function setIsCustomModify($isCustomModify)
    {
        $this->isCustomModify = $isCustomModify;

        return $this;
    }

    /**
     * Get isCustomModify
     *
     * @return int
     */
    public function getIsCustomModify()
    {
        return $this->isCustomModify;
    }

    /**
     * Set telegram
     *
     * @param int $telegram
     *
     * @return InfoMcmtarget
     */
    public function setTelegram($telegram)
    {
        $this->telegram = $telegram;

        return $this;
    }

    /**
     * Get telegram
     *
     * @return int
     */
    public function getTelegram()
    {
        return $this->telegram;
    }
}
