<?php

namespace TeamSoft\CrmRepositoryBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * InfoPlanpresentation
 *
 * @ORM\Table(name="info_planpresentation")
 * @ORM\Entity(repositoryClass="TeamSoft\CrmRepositoryBundle\Repository\InfoPlanpresentation")
 */
class InfoPlanpresentation implements ServiceFieldInterface
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var guid
     *
     * @ORM\Column(name="guid", type="guid", nullable=true)
     */
    private $guid;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="currenttime", type="datetime", nullable=true)
     */
    private $currenttime;

    /**
     * @var string
     *
     * @ORM\Column(name="moduser", type="string", length=16, nullable=true)
     */
    private $moduser;

    /**
     * @var int
     *
     * @ORM\Column(name="ntask", type="integer", nullable=true)
     */
    private $ntask;

    /**
     * @var \InfoClm
     *
     * @ORM\ManyToOne(targetEntity="InfoClm")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="presentation_id", referencedColumnName="id")
     * })
     */
    private $presentation;

    /**
     * @var \InfoPlandetail
     *
     * @ORM\ManyToOne(targetEntity="InfoPlandetail", inversedBy="planPresentations")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="plandetail_id", referencedColumnName="id")
     * })
     */
    private $plandetail;

    /**
     * @var int|null
     *
     * @ORM\Column(name="position", type="integer", nullable=true)
     */
    private $position;

    public function __clone()
    {
        $this->id = null;
        $this->guid = null;
        $this->currenttime = null;
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set guid
     *
     * @param guid $guid
     *
     * @return InfoPlanpresentation
     */
    public function setGuid($guid)
    {
        $this->guid = $guid;

        return $this;
    }

    /**
     * Get guid
     *
     * @return guid
     */
    public function getGuid()
    {
        return $this->guid;
    }

    /**
     * Set currenttime
     *
     * @param \DateTime $currenttime
     *
     * @return InfoPlanpresentation
     */
    public function setCurrenttime(\DateTime $currenttime = null)
    {
        $this->currenttime = $currenttime;

        return $this;
    }

    /**
     * Get currenttime
     *
     * @return \DateTime
     */
    public function getCurrenttime()
    {
        return $this->currenttime;
    }

    /**
     * Set moduser
     *
     * @param string $moduser
     *
     * @return InfoPlanpresentation
     */
    public function setModuser($moduser)
    {
        $this->moduser = $moduser;

        return $this;
    }

    /**
     * Get moduser
     *
     * @return string
     */
    public function getModuser()
    {
        return $this->moduser;
    }

    /**
     * Set ntask
     *
     * @param int $ntask
     *
     * @return InfoPlanpresentation
     */
    public function setNtask($ntask)
    {
        $this->ntask = $ntask;

        return $this;
    }

    /**
     * Get ntask
     *
     * @return int
     */
    public function getNtask()
    {
        return $this->ntask;
    }

    /**
     * Set presentation
     *
     * @param \TeamSoft\CrmRepositoryBundle\Entity\InfoClm $presentation
     *
     * @return InfoPlanpresentation
     */
    public function setPresentation(\TeamSoft\CrmRepositoryBundle\Entity\InfoClm $presentation = null)
    {
        $this->presentation = $presentation;

        return $this;
    }

    /**
     * Get brend
     *
     * @return \TeamSoft\CrmRepositoryBundle\Entity\InfoClm
     */
    public function getPresentation()
    {
        return $this->presentation;
    }

    /**
     * Set plandetail
     *
     * @param \TeamSoft\CrmRepositoryBundle\Entity\InfoPlandetail $plandetail
     *
     * @return InfoPlanpresentation
     */
    public function setPlandetail(\TeamSoft\CrmRepositoryBundle\Entity\InfoPlandetail $plandetail = null)
    {
        $this->plandetail = $plandetail;

        return $this;
    }

    /**
     * Get plandetail
     *
     * @return \TeamSoft\CrmRepositoryBundle\Entity\InfoPlandetail
     */
    public function getPlandetail()
    {
        return $this->plandetail;
    }

    /**
     * @return int|null
     */
    public function getPlanDetailId()
    {
        return $this->getPlandetail() ? $this->getPlandetail()->getId() : null;
    }

    /**
     * @return int|null
     */
    public function getClmId()
    {
        return $this->getPresentation() ? $this->getPresentation()->getId() : null;
    }


    /**
     * @return int|null
     */
    public function getPresentationId()
    {
        return $this->getPresentation() ? $this->getPresentation()->getId() : null;
    }

    /**
     * @return int|null
     */
    public function getPosition(): ?int
    {
        return $this->position;
    }

    /**
     * @param int|null $position
     * @return self
     */
    public function setPosition(?int $position): self
    {
        $this->position = $position;

        return $this;
    }
}
