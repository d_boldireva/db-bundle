<?php

namespace TeamSoft\CrmRepositoryBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * InfoPromoprojectfilter
 *
 * @ORM\Table(name="info_promoprojectfilter")
 * @ORM\Entity
 */
class InfoPromoprojectfilter
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="currenttime", type="datetime", nullable=true)
     */
    private $currenttime;

    /**
     * @var string|null
     *
     * @ORM\Column(name="moduser", type="string", length=16, nullable=true)
     */
    private $moduser;

    /**
     * @var string|null
     *
     * @ORM\Column(name="guid", type="guid", nullable=true)
     */
    private $guid;

    /**
     * @var \InfoPromoproject
     *
     * @ORM\ManyToOne(targetEntity="InfoPromoproject")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="promoproject_id", referencedColumnName="id")
     * })
     */
    private $promoproject;

    /**
     * @var \InfoDirection
     *
     * @ORM\ManyToOne(targetEntity="InfoDirection")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="direction_id", referencedColumnName="id")
     * })
     */
    private $direction;

    /**
     * @var \InfoDictionary
     *
     * @ORM\ManyToOne(targetEntity="InfoDictionary")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="specialization_id", referencedColumnName="id")
     * })
     */
    private $specialization;



    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set currenttime.
     *
     * @param \DateTime|null $currenttime
     *
     * @return InfoPromoprojectfilter
     */
    public function setCurrenttime($currenttime = null)
    {
        $this->currenttime = $currenttime;
    
        return $this;
    }

    /**
     * Get currenttime.
     *
     * @return \DateTime|null
     */
    public function getCurrenttime()
    {
        return $this->currenttime;
    }

    /**
     * Set moduser.
     *
     * @param string|null $moduser
     *
     * @return InfoPromoprojectfilter
     */
    public function setModuser($moduser = null)
    {
        $this->moduser = $moduser;
    
        return $this;
    }

    /**
     * Get moduser.
     *
     * @return string|null
     */
    public function getModuser()
    {
        return $this->moduser;
    }

    /**
     * Set guid.
     *
     * @param string|null $guid
     *
     * @return InfoPromoprojectfilter
     */
    public function setGuid($guid = null)
    {
        $this->guid = $guid;
    
        return $this;
    }

    /**
     * Get guid.
     *
     * @return string|null
     */
    public function getGuid()
    {
        return $this->guid;
    }

    /**
     * Set promoproject.
     *
     * @param \TeamSoft\CrmRepositoryBundle\Entity\InfoPromoproject|null $promoproject
     *
     * @return InfoPromoprojectfilter
     */
    public function setPromoproject(\TeamSoft\CrmRepositoryBundle\Entity\InfoPromoproject $promoproject = null)
    {
        $this->promoproject = $promoproject;
    
        return $this;
    }

    /**
     * Get promoproject.
     *
     * @return \TeamSoft\CrmRepositoryBundle\Entity\InfoPromoproject|null
     */
    public function getPromoproject()
    {
        return $this->promoproject;
    }

    /**
     * Set direction.
     *
     * @param \TeamSoft\CrmRepositoryBundle\Entity\InfoDirection|null $direction
     *
     * @return InfoPromoprojectfilter
     */
    public function setDirection(\TeamSoft\CrmRepositoryBundle\Entity\InfoDirection $direction = null)
    {
        $this->direction = $direction;
    
        return $this;
    }

    /**
     * Get direction.
     *
     * @return \TeamSoft\CrmRepositoryBundle\Entity\InfoDirection|null
     */
    public function getDirection()
    {
        return $this->direction;
    }

    /**
     * Set specialization.
     *
     * @param \TeamSoft\CrmRepositoryBundle\Entity\InfoDictionary|null $specialization
     *
     * @return InfoPromoprojectfilter
     */
    public function setSpecialization(\TeamSoft\CrmRepositoryBundle\Entity\InfoDictionary $specialization = null)
    {
        $this->specialization = $specialization;
    
        return $this;
    }

    /**
     * Get specialization.
     *
     * @return \TeamSoft\CrmRepositoryBundle\Entity\InfoDictionary|null
     */
    public function getSpecialization()
    {
        return $this->specialization;
    }
}
