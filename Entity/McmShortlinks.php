<?php

namespace TeamSoft\CrmRepositoryBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * McmShortlinks
 *
 * @ORM\Table(name="mcm_shortlinks")
 * @ORM\Entity
 */
class McmShortlinks implements ServiceFieldInterface
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="url", type="string", length=255, nullable=true)
     */
    private $url;

    /**
     * @var string
     *
     * @ORM\Column(name="hash", type="string", length=255, nullable=true)
     */
    private $hash;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="currenttime", type="datetime", nullable=true)
     */
    private $currenttime;

    /**
     * @var string
     *
     * @ORM\Column(name="moduser", type="string", length=16, nullable=true)
     */
    private $moduser;

    /**
     * @var string
     *
     * @ORM\Column(name="guid", type="guid", nullable=true)
     */
    private $guid;

    /**
     * @ORM\ManyToOne(targetEntity="InfoMcmdistribution", cascade={"remove"})
     * @ORM\JoinColumn(name="distribution_id", referencedColumnName="id")
     */
    private $distribution;

    /**
     * @ORM\ManyToOne(targetEntity="InfoMcmtrigger", cascade={"remove"})
     * @ORM\JoinColumn(name="trigger_id", referencedColumnName="id")
     */
    private $trigger;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set url
     *
     * @param string $url
     *
     * @return McmShortlinks
     */
    public function setUrl($url)
    {
        $this->url = str_replace('&amp;', '&', str_replace('&nbsp;', '', $url));
    
        return $this;
    }

    /**
     * Get url
     *
     * @return string
     */
    public function getUrl()
    {
        return $this->url;
    }

    /**
     * Set hash
     *
     * @param string $hash
     *
     * @return McmShortlinks
     */
    public function setHash($hash)
    {
        $this->hash = $hash;
    
        return $this;
    }

    /**
     * Get hash
     *
     * @return string
     */
    public function getHash()
    {
        return $this->hash;
    }

    /**
     * Set currenttime
     *
     * @param \DateTime $currenttime
     *
     * @return McmShortlinks
     */
    public function setCurrenttime(\DateTime $currenttime)
    {
        $this->currenttime = $currenttime;
    
        return $this;
    }

    /**
     * Get currenttime
     *
     * @return \DateTime
     */
    public function getCurrenttime()
    {
        return $this->currenttime;
    }

    /**
     * Set moduser
     *
     * @param string $moduser
     *
     * @return McmShortlinks
     */
    public function setModuser($moduser)
    {
        $this->moduser = $moduser;
    
        return $this;
    }

    /**
     * Get moduser
     *
     * @return string
     */
    public function getModuser()
    {
        return $this->moduser;
    }

    /**
     * Set guid
     *
     * @param string $guid
     *
     * @return McmShortlinks
     */
    public function setGuid($guid)
    {
        $this->guid = $guid;
    
        return $this;
    }

    /**
     * Get guid
     *
     * @return string
     */
    public function getGuid()
    {
        return $this->guid;
    }

    /**
     * Set distribution
     *
     * @param \TeamSoft\CrmRepositoryBundle\Entity\InfoMcmdistribution $distribution
     *
     * @return MCMShortlinks
     */
    public function setDistribution(InfoMcmdistribution $distribution = null)
    {
        $this->distribution = $distribution;

        return $this;
    }

    /**
     * Get distribution
     *
     * @return \TeamSoft\CrmRepositoryBundle\Entity\InfoMcmdistribution
     */
    public function getDistribution()
    {
        return $this->distribution;
    }

    /**
     * Set trigger
     *
     * @param \TeamSoft\CrmRepositoryBundle\Entity\InfoMcmtrigger $trigger
     *
     * @return MCMShortlinks
     */
    public function setTrigger(InfoMcmtrigger $trigger = null)
    {
        $this->trigger = $trigger;

        return $this;
    }

    /**
     * Get trigger
     *
     * @return \TeamSoft\CrmRepositoryBundle\Entity\InfoMcmtrigger
     */
    public function getTrigger()
    {
        return $this->trigger;
    }
}
