<?php

namespace TeamSoft\CrmRepositoryBundle\Entity;

interface LanguageInterface
{
    /**
     * @param InfoLanguage $language
     * @return self
     */
    public function setLanguage(InfoLanguage $language);

    /**
     * @return InfoLanguage
     */
    public function getLanguage();
}
