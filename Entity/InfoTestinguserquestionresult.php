<?php

namespace TeamSoft\CrmRepositoryBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * InfoTestinguserquestionresult
 *
 * @ORM\Table(name="info_testinguserquestionresult")
 * @ORM\Entity
 */
class InfoTestinguserquestionresult implements ServiceFieldInterface
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var int
     *
     * @ORM\Column(name="question_id", type="integer", nullable=true)
     */
    private $questionId;

    /**
     * @var int
     *
     * @ORM\Column(name="is_correct", type="integer", nullable=true)
     */
    private $isCorrect;

    /**
     * @var int
     *
     * @ORM\Column(name="spenttime", type="integer", nullable=true)
     */
    private $spenttime;

    /**
     * @var string
     *
     * @ORM\Column(name="answer", type="string", length=255, nullable=true)
     */
    private $answer;

    /**
     * @var guid
     *
     * @ORM\Column(name="guid", type="guid", nullable=true)
     */
    private $guid;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="currenttime", type="datetime", nullable=true)
     */
    private $currenttime;

    /**
     * @var string
     *
     * @ORM\Column(name="moduser", type="string", length=16, nullable=true)
     */
    private $moduser;

    /**
     * @var \InfoTestinganswer
     *
     * @ORM\ManyToOne(targetEntity="InfoTestinganswer")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="answer_id", referencedColumnName="id")
     * })
     */
    private $answer2;

    /**
     * @var \InfoContact
     *
     * @ORM\ManyToOne(targetEntity="InfoContact")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="contact_id", referencedColumnName="id")
     * })
     */
    private $contact;

    /**
     * @var \InfoUser
     *
     * @ORM\ManyToOne(targetEntity="InfoUser")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     * })
     */
    private $user;



    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set questionId
     *
     * @param int $questionId
     *
     * @return InfoTestinguserquestionresult
     */
    public function setQuestionId($questionId)
    {
        $this->questionId = $questionId;
    
        return $this;
    }

    /**
     * Get questionId
     *
     * @return int
     */
    public function getQuestionId()
    {
        return $this->questionId;
    }

    /**
     * Set isCorrect
     *
     * @param int $isCorrect
     *
     * @return InfoTestinguserquestionresult
     */
    public function setIsCorrect($isCorrect)
    {
        $this->isCorrect = $isCorrect;
    
        return $this;
    }

    /**
     * Get isCorrect
     *
     * @return int
     */
    public function getIsCorrect()
    {
        return $this->isCorrect;
    }

    /**
     * Set spenttime
     *
     * @param int $spenttime
     *
     * @return InfoTestinguserquestionresult
     */
    public function setSpenttime($spenttime)
    {
        $this->spenttime = $spenttime;
    
        return $this;
    }

    /**
     * Get spenttime
     *
     * @return int
     */
    public function getSpenttime()
    {
        return $this->spenttime;
    }

    /**
     * Set answer
     *
     * @param string $answer
     *
     * @return InfoTestinguserquestionresult
     */
    public function setAnswer($answer)
    {
        $this->answer = $answer;
    
        return $this;
    }

    /**
     * Get answer
     *
     * @return string
     */
    public function getAnswer()
    {
        return $this->answer;
    }

    /**
     * Set guid
     *
     * @param guid $guid
     *
     * @return InfoTestinguserquestionresult
     */
    public function setGuid($guid)
    {
        $this->guid = $guid;
    
        return $this;
    }

    /**
     * Get guid
     *
     * @return guid
     */
    public function getGuid()
    {
        return $this->guid;
    }

    /**
     * Set currenttime
     *
     * @param \DateTime $currenttime
     *
     * @return InfoTestinguserquestionresult
     */
    public function setCurrenttime(\DateTime $currenttime)
    {
        $this->currenttime = $currenttime;
    
        return $this;
    }

    /**
     * Get currenttime
     *
     * @return \DateTime
     */
    public function getCurrenttime()
    {
        return $this->currenttime;
    }

    /**
     * Set moduser
     *
     * @param string $moduser
     *
     * @return InfoTestinguserquestionresult
     */
    public function setModuser($moduser)
    {
        $this->moduser = $moduser;
    
        return $this;
    }

    /**
     * Get moduser
     *
     * @return string
     */
    public function getModuser()
    {
        return $this->moduser;
    }

    /**
     * Set answer2
     *
     * @param \TeamSoft\CrmRepositoryBundle\Entity\InfoTestinganswer $answer2
     *
     * @return InfoTestinguserquestionresult
     */
    public function setAnswer2(\TeamSoft\CrmRepositoryBundle\Entity\InfoTestinganswer $answer2 = null)
    {
        $this->answer2 = $answer2;
    
        return $this;
    }

    /**
     * Get answer2
     *
     * @return \TeamSoft\CrmRepositoryBundle\Entity\InfoTestinganswer
     */
    public function getAnswer2()
    {
        return $this->answer2;
    }

    /**
     * Set contact
     *
     * @param \TeamSoft\CrmRepositoryBundle\Entity\InfoContact $contact
     *
     * @return InfoTestinguserquestionresult
     */
    public function setContact(\TeamSoft\CrmRepositoryBundle\Entity\InfoContact $contact = null)
    {
        $this->contact = $contact;
    
        return $this;
    }

    /**
     * Get contact
     *
     * @return \TeamSoft\CrmRepositoryBundle\Entity\InfoContact
     */
    public function getContact()
    {
        return $this->contact;
    }

    /**
     * Set user
     *
     * @param \TeamSoft\CrmRepositoryBundle\Entity\InfoUser $user
     *
     * @return InfoTestinguserquestionresult
     */
    public function setUser(\TeamSoft\CrmRepositoryBundle\Entity\InfoUser $user = null)
    {
        $this->user = $user;
    
        return $this;
    }

    /**
     * Get user
     *
     * @return \TeamSoft\CrmRepositoryBundle\Entity\InfoUser
     */
    public function getUser()
    {
        return $this->user;
    }
}
