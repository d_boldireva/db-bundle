<?php

namespace TeamSoft\CrmRepositoryBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * InfoCustomdictionaryvalue
 *
 * @ORM\Table(name="info_CustomDictionaryValue")
 * @ORM\Entity(repositoryClass="TeamSoft\CrmRepositoryBundle\Repository\InfoCustomdictionaryvalue")
 */
class InfoCustomdictionaryvalue
{
    public const CODE_NO_DISTANCE_VISIT = 'no_distance_visit';
    public const CODE_MONTH_BUDGET = 'month_budget';
    public const CODE_EXTRA_BUDGET = 'extra_budget';
    public const CODE_ACTIVITIES_BUDGET = 'activities_budget';
    public const CODE_JUSTIFICATION_REPORT = 'justification_report';

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="DictionaryValue", type="string", length=200, nullable=true)
     */
    private $dictionaryvalue;

    /**
     * @var int
     *
     * @ORM\Column(name="pos", type="integer", nullable=true)
     */
    private $pos;

    /**
     * @var guid
     *
     * @ORM\Column(name="guid", type="guid", nullable=true)
     */
    private $guid;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="currenttime", type="datetime", nullable=true)
     */
    private $currenttime;

    /**
     * @var string
     *
     * @ORM\Column(name="moduser", type="string", length=16, nullable=true)
     */
    private $moduser;

    /**
     * @var string
     *
     * @ORM\Column(name="code", type="string", length=255, nullable=true)
     */
    private $code;

    /**
     * @var string
     *
     * @ORM\Column(name="descr", type="blob", nullable=true)
     */
    private $descr;

    /**
     * @var int
     *
     * @ORM\Column(name="disabled", type="integer", nullable=true)
     */
    private $disabled;

    /**
     * @var \InfoCustomdictionary
     *
     * @ORM\ManyToOne(targetEntity="InfoCustomdictionary", inversedBy="customDictionariesValues")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="CustomDictionary_id", referencedColumnName="id")
     * })
     */
    private $customdictionary;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set dictionaryvalue
     *
     * @param string $dictionaryvalue
     *
     * @return InfoCustomdictionaryvalue
     */
    public function setDictionaryvalue($dictionaryvalue)
    {
        $this->dictionaryvalue = $dictionaryvalue;

        return $this;
    }

    /**
     * Get dictionaryvalue
     *
     * @return string
     */
    public function getDictionaryvalue()
    {
        return $this->dictionaryvalue;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->getDictionaryvalue();
    }

    /**
     * Set guid
     *
     * @param guid $guid
     *
     * @return InfoCustomdictionaryvalue
     */
    public function setGuid($guid)
    {
        $this->guid = $guid;

        return $this;
    }

    /**
     * Get guid
     *
     * @return guid
     */
    public function getGuid()
    {
        return $this->guid;
    }

    /**
     * Set currenttime
     *
     * @param \DateTime $currenttime
     *
     * @return InfoCustomdictionaryvalue
     */
    public function setCurrenttime($currenttime)
    {
        $this->currenttime = $currenttime;

        return $this;
    }

    /**
     * Get currenttime
     *
     * @return \DateTime
     */
    public function getCurrenttime()
    {
        return $this->currenttime;
    }

    /**
     * Set moduser
     *
     * @param string $moduser
     *
     * @return InfoCustomdictionaryvalue
     */
    public function setModuser($moduser)
    {
        $this->moduser = $moduser;

        return $this;
    }

    /**
     * Get moduser
     *
     * @return string
     */
    public function getModuser()
    {
        return $this->moduser;
    }

    /**
     * Set descr
     *
     * @param string $descr
     *
     * @return InfoCustomdictionaryvalue
     */
    public function setDescr($descr)
    {
        $this->descr = $descr;

        return $this;
    }

    /**
     * Get descr
     *
     * @return string
     */
    public function getDescr()
    {
        return $this->descr;
    }

    /**
     * Set customdictionary
     *
     * @param \TeamSoft\CrmRepositoryBundle\Entity\InfoCustomdictionary $customdictionary
     *
     * @return InfoCustomdictionaryvalue
     */
    public function setCustomdictionary(\TeamSoft\CrmRepositoryBundle\Entity\InfoCustomdictionary $customdictionary = null)
    {
        $this->customdictionary = $customdictionary;

        return $this;
    }

    /**
     * Get customdictionary
     *
     * @return \TeamSoft\CrmRepositoryBundle\Entity\InfoCustomdictionary
     */
    public function getCustomdictionary()
    {
        return $this->customdictionary;
    }

    /**
     * Set pos.
     *
     * @param int|null $pos
     *
     * @return InfoCustomdictionaryvalue
     */
    public function setPos($pos = null)
    {
        $this->pos = $pos;

        return $this;
    }

    /**
     * Get pos.
     *
     * @return int|null
     */
    public function getPos()
    {
        return $this->pos;
    }

    /**
     * Set code.
     *
     * @param string|null $code
     *
     * @return InfoCustomdictionaryvalue
     */
    public function setCode($code = null)
    {
        $this->code = $code;

        return $this;
    }

    /**
     * Get code.
     *
     * @return string|null
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * Set disabled.
     *
     * @param int|null $disabled
     *
     * @return InfoCustomdictionaryvalue
     */
    public function setDisabled(?int $disabled): self
    {
        $this->disabled = $disabled;

        return $this;
    }

    /**
     * Get disabled.
     *
     * @return int|null
     */
    public function getDisabled(): ?int
    {
        return $this->disabled;
    }
}
