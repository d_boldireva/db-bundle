<?php

namespace TeamSoft\CrmRepositoryBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * InfoSalefact
 *
 * @ORM\Table(name="info_salefact")
 * @ORM\Entity
 */
class InfoSalefact
{
    public const SALE_TYPE_3T4KA = '3t4ka';
    public const SALE_TYPE_PERENOS = 'Perenos';
    public const SALE_TYPE_RETAIL = 'Retail';
    public const SALE_TYPE_TENDER = 'Tender';
    public const SALE_TYPE_PURCHASE = 'Закупка';
    public const SALE_TYPE_DISTRIBUTION = 'Распределение';
    public const SALE_TYPE_STOCK = 'Склад';

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var int|null
     *
     * @ORM\Column(name="Count", type="integer", nullable=true)
     */
    private $count;

    /**
     * @var int|null
     *
     * @ORM\Column(name="Amount", type="integer", nullable=true)
     */
    private $amount;

    /**
     * @var int|null
     *
     * @ORM\Column(name="review", type="integer", nullable=true)
     */
    private $review;

    /**
     * @var string|null
     *
     * @ORM\Column(name="guid", type="guid", nullable=true)
     */
    private $guid;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="currenttime", type="datetime", nullable=true)
     */
    private $currenttime;

    /**
     * @var string|null
     *
     * @ORM\Column(name="moduser", type="string", length=16, nullable=true)
     */
    private $moduser;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="saledate", type="datetime", nullable=true)
     */
    private $saledate;

    /**
     * @var string|null
     *
     * @ORM\Column(name="distributor", type="string", length=255, nullable=true)
     */
    private $distributor;

    /**
     * @var int|null
     *
     * @ORM\Column(name="document_presence", type="integer", nullable=true)
     */
    private $documentPresence;

    /**
     * @var int|null
     *
     * @ORM\Column(name="company_morion_id", type="integer", nullable=true)
     */
    private $companyMorionId;

    /**
     * @var string|null
     *
     * @ORM\Column(name="sale_type", type="string", length=50, nullable=true)
     */
    private $saleType;

    /**
     * @var InfoRegionpart
     *
     * @ORM\ManyToOne(targetEntity="InfoRegionpart")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="regionpart_id", referencedColumnName="id")
     * })
     */
    private $regionpart;

    /**
     * @var InfoRegion
     *
     * @ORM\ManyToOne(targetEntity="InfoRegion")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="Region_id", referencedColumnName="id")
     * })
     */
    private $region;

    /**
     * @var InfoCompany
     *
     * @ORM\ManyToOne(targetEntity="InfoCompany")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="company_id", referencedColumnName="id")
     * })
     */
    private $company;

    /**
     * @var InfoSaleperiod
     *
     * @ORM\ManyToOne(targetEntity="InfoSaleperiod")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="SalePeriod_id", referencedColumnName="id")
     * })
     */
    private $saleperiod;

    /**
     * @var InfoPreparation
     *
     * @ORM\ManyToOne(targetEntity="InfoPreparation")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="Preparation_id", referencedColumnName="id")
     * })
     */
    private $preparation;

    /**
     * @var \InfoCity
     *
     * @ORM\ManyToOne(targetEntity="InfoCity")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="city_id", referencedColumnName="id")
     * })
     */
    private $city;

    /**
     * @var \InfoCountry
     *
     * @ORM\ManyToOne(targetEntity="InfoCountry")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="country_id", referencedColumnName="id")
     * })
     */
    private $country;

    /**
     * @var InfoUser
     *
     * @ORM\ManyToOne(targetEntity="InfoUser")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     * })
     */
    private $user;

    /**
     * @var InfoDistributor
     *
     * @ORM\ManyToOne(targetEntity="InfoDistributor")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="Distributor_id", referencedColumnName="id")
     * })
     */
    private $distributor2;

    /**
     * @var int|null
     *
     * @ORM\Column(name="discount_1", type="integer", nullable=true)
     */
    private $discount1;

    /**
     * @var int|null
     *
     * @ORM\Column(name="discount_2", type="integer", nullable=true)
     */
    private $discount2;

    /**
     * @var InfoSaleType
     *
     * @ORM\ManyToOne(targetEntity="InfoSaleType")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="sale_type_id", referencedColumnName="id")
     * })
     */
    private $infoSaleType;

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set count.
     *
     * @param int|null $count
     *
     * @return InfoSalefact
     */
    public function setCount($count = null)
    {
        $this->count = $count;

        return $this;
    }

    /**
     * Get count.
     *
     * @return int|null
     */
    public function getCount()
    {
        return $this->count;
    }

    /**
     * Set amount.
     *
     * @param int|null $amount
     *
     * @return InfoSalefact
     */
    public function setAmount($amount = null)
    {
        $this->amount = $amount;

        return $this;
    }

    /**
     * Get amount.
     *
     * @return int|null
     */
    public function getAmount()
    {
        return $this->amount;
    }

    /**
     * Set review.
     *
     * @param int|null $review
     *
     * @return InfoSalefact
     */
    public function setReview($review = null)
    {
        $this->review = $review;

        return $this;
    }

    /**
     * Get review.
     *
     * @return int|null
     */
    public function getReview()
    {
        return $this->review;
    }

    /**
     * Set guid.
     *
     * @param string|null $guid
     *
     * @return InfoSalefact
     */
    public function setGuid($guid = null)
    {
        $this->guid = $guid;

        return $this;
    }

    /**
     * Get guid.
     *
     * @return string|null
     */
    public function getGuid()
    {
        return $this->guid;
    }

    /**
     * Set currenttime.
     *
     * @param \DateTime|null $currenttime
     *
     * @return InfoSalefact
     */
    public function setCurrenttime($currenttime = null)
    {
        $this->currenttime = $currenttime;

        return $this;
    }

    /**
     * Get currenttime.
     *
     * @return \DateTime|null
     */
    public function getCurrenttime()
    {
        return $this->currenttime;
    }

    /**
     * Set moduser.
     *
     * @param string|null $moduser
     *
     * @return InfoSalefact
     */
    public function setModuser($moduser = null)
    {
        $this->moduser = $moduser;

        return $this;
    }

    /**
     * Get moduser.
     *
     * @return string|null
     */
    public function getModuser()
    {
        return $this->moduser;
    }

    /**
     * Set saledate.
     *
     * @param \DateTime|null $saledate
     *
     * @return InfoSalefact
     */
    public function setSaledate($saledate = null)
    {
        $this->saledate = $saledate;

        return $this;
    }

    /**
     * Get saledate.
     *
     * @return \DateTime|null
     */
    public function getSaledate()
    {
        return $this->saledate;
    }

    /**
     * Set distributor.
     *
     * @param string|null $distributor
     *
     * @return InfoSalefact
     */
    public function setDistributor($distributor = null)
    {
        $this->distributor = $distributor;

        return $this;
    }

    /**
     * Get distributor.
     *
     * @return string|null
     */
    public function getDistributor()
    {
        return $this->distributor;
    }

    /**
     * Set documentPresence.
     *
     * @param int|null $documentPresence
     *
     * @return InfoSalefact
     */
    public function setDocumentPresence($documentPresence = null)
    {
        $this->documentPresence = $documentPresence;

        return $this;
    }

    /**
     * Get documentPresence.
     *
     * @return int|null
     */
    public function getDocumentPresence()
    {
        return $this->documentPresence;
    }

    /**
     * Set companyMorionId.
     *
     * @param int|null $companyMorionId
     *
     * @return InfoSalefact
     */
    public function setCompanyMorionId($companyMorionId = null)
    {
        $this->companyMorionId = $companyMorionId;

        return $this;
    }

    /**
     * Get companyMorionId.
     *
     * @return int|null
     */
    public function getCompanyMorionId()
    {
        return $this->companyMorionId;
    }

    /**
     * Set saleType.
     *
     * @param string|null $saleType
     *
     * @return InfoSalefact
     */
    public function setSaleType($saleType = null)
    {
        $this->saleType = $saleType;

        return $this;
    }

    /**
     * Get saleType.
     *
     * @return string|null
     */
    public function getSaleType()
    {
        return $this->saleType;
    }

    /**
     * Set regionpart.
     *
     * @param \TeamSoft\CrmRepositoryBundle\Entity\InfoRegionpart|null $regionpart
     *
     * @return InfoSalefact
     */
    public function setRegionpart(\TeamSoft\CrmRepositoryBundle\Entity\InfoRegionpart $regionpart = null)
    {
        $this->regionpart = $regionpart;

        return $this;
    }

    /**
     * Get regionpart.
     *
     * @return \TeamSoft\CrmRepositoryBundle\Entity\InfoRegionpart|null
     */
    public function getRegionpart()
    {
        return $this->regionpart;
    }

    /**
     * Set region.
     *
     * @param \TeamSoft\CrmRepositoryBundle\Entity\InfoRegion|null $region
     *
     * @return InfoSalefact
     */
    public function setRegion(\TeamSoft\CrmRepositoryBundle\Entity\InfoRegion $region = null)
    {
        $this->region = $region;

        return $this;
    }

    /**
     * Get region.
     *
     * @return \TeamSoft\CrmRepositoryBundle\Entity\InfoRegion|null
     */
    public function getRegion()
    {
        return $this->region;
    }

    /**
     * Set company.
     *
     * @param \TeamSoft\CrmRepositoryBundle\Entity\InfoCompany|null $company
     *
     * @return InfoSalefact
     */
    public function setCompany(\TeamSoft\CrmRepositoryBundle\Entity\InfoCompany $company = null)
    {
        $this->company = $company;

        return $this;
    }

    /**
     * Get company.
     *
     * @return \TeamSoft\CrmRepositoryBundle\Entity\InfoCompany|null
     */
    public function getCompany()
    {
        return $this->company;
    }

    /**
     * Set saleperiod.
     *
     * @param \TeamSoft\CrmRepositoryBundle\Entity\InfoSaleperiod|null $saleperiod
     *
     * @return InfoSalefact
     */
    public function setSaleperiod(\TeamSoft\CrmRepositoryBundle\Entity\InfoSaleperiod $saleperiod = null)
    {
        $this->saleperiod = $saleperiod;

        return $this;
    }

    /**
     * Get saleperiod.
     *
     * @return \TeamSoft\CrmRepositoryBundle\Entity\InfoSaleperiod|null
     */
    public function getSaleperiod()
    {
        return $this->saleperiod;
    }

    /**
     * Set preparation.
     *
     * @param \TeamSoft\CrmRepositoryBundle\Entity\InfoPreparation|null $preparation
     *
     * @return InfoSalefact
     */
    public function setPreparation(\TeamSoft\CrmRepositoryBundle\Entity\InfoPreparation $preparation = null)
    {
        $this->preparation = $preparation;

        return $this;
    }

    /**
     * Get preparation.
     *
     * @return \TeamSoft\CrmRepositoryBundle\Entity\InfoPreparation|null
     */
    public function getPreparation()
    {
        return $this->preparation;
    }

    /**
     * Set city.
     *
     * @param \TeamSoft\CrmRepositoryBundle\Entity\InfoCity|null $city
     *
     * @return InfoSalefact
     */
    public function setCity(\TeamSoft\CrmRepositoryBundle\Entity\InfoCity $city = null)
    {
        $this->city = $city;

        return $this;
    }

    /**
     * Get city.
     *
     * @return \TeamSoft\CrmRepositoryBundle\Entity\InfoCity|null
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
     * Set country.
     *
     * @param \TeamSoft\CrmRepositoryBundle\Entity\InfoCountry|null $country
     *
     * @return InfoSalefact
     */
    public function setCountry(\TeamSoft\CrmRepositoryBundle\Entity\InfoCountry $country = null)
    {
        $this->country = $country;

        return $this;
    }

    /**
     * Get country.
     *
     * @return \TeamSoft\CrmRepositoryBundle\Entity\InfoCountry|null
     */
    public function getCountry()
    {
        return $this->country;
    }

    /**
     * Set user.
     *
     * @param \TeamSoft\CrmRepositoryBundle\Entity\InfoUser|null $user
     *
     * @return InfoSalefact
     */
    public function setUser(\TeamSoft\CrmRepositoryBundle\Entity\InfoUser $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user.
     *
     * @return \TeamSoft\CrmRepositoryBundle\Entity\InfoUser|null
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Set distributor2.
     *
     * @param \TeamSoft\CrmRepositoryBundle\Entity\InfoDistributor|null $distributor2
     *
     * @return InfoSalefact
     */
    public function setDistributor2(\TeamSoft\CrmRepositoryBundle\Entity\InfoDistributor $distributor2 = null)
    {
        $this->distributor2 = $distributor2;

        return $this;
    }

    /**
     * Get distributor2.
     *
     * @return \TeamSoft\CrmRepositoryBundle\Entity\InfoDistributor|null
     */
    public function getDistributor2()
    {
        return $this->distributor2;
    }

    /**
     * @return int|null
     */
    public function getDiscount1(): ?int
    {
        return $this->discount1;
    }

    /**
     * @param int|null $discount1
     * @return self
     */
    public function setDiscount1(?int $discount1): self
    {
        $this->discount1 = $discount1;
        return $this;
    }

    /**
     * @return int|null
     */
    public function getDiscount2(): ?int
    {
        return $this->discount2;
    }

    /**
     * @param int|null $discount2
     * @return self
     */
    public function setDiscount2(?int $discount2): self
    {
        $this->discount2 = $discount2;
        return $this;
    }

    public function getInfoSaleType(): ?InfoSaleType
    {
        return $this->infoSaleType;
    }

    public function setInfoSaleType(?InfoSaleType $saleType): self
    {
        $this->infoSaleType = $saleType;
        return $this;
    }
}
