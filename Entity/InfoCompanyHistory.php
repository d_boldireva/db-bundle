<?php

namespace TeamSoft\CrmRepositoryBundle\Entity;


use Doctrine\ORM\Mapping as ORM;

/**
 * InfoContactHistory
 *
 * @ORM\Table(name="info_companyhist")
 * @ORM\Entity
 */
class InfoCompanyHistory implements ServiceFieldInterface
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="company_id", type="integer", nullable=true)
     */
    private $companyId;

    /**
     * @var integer
     *
     * @ORM\Column(name="modifier_id", type="integer", nullable=true)
     */
    private $modifierId;


    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="modified", type="datetime")
     */
    private $modified;

    /**
     * @var string|null
     *
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $field;

    /**
     * @var string|null
     *
     * @ORM\Column(name="oldvalue", type="string")
     */
    private $oldValue;

    /**
     * @var string|null
     *
     * @ORM\Column(name="newvalue", type="string", length=255, nullable=true)
     */
    private $newValue;


    /**
     * @var integer|null
     *
     * @ORM\Column(name="archivereason_id_old", type="integer", nullable=true)
     */
    private $archiveReasonIdOld;

    /**
     * @var integer|null
     *
     * @ORM\Column(name="archivereason_id_new", type="integer", nullable=true)
     */
    private $archiveReasonIdNew;

    /**
     * @var integer|null
     *
     * @ORM\Column(name="companytype_id_old", type="integer", nullable=true)
     */
    private $companyTypeIdOld;

    /**
     * @var integer|null
     *
     * @ORM\Column(name="companytype_id_new", type="integer", nullable=true)
     */
    private $companyTypeIdNew;

    /**
     * @var integer|null
     *
     * @ORM\Column(name="city_id_old", type="integer", nullable=true)
     */
    private $cityIdOld;

    /**
     * @var integer|null
     *
     * @ORM\Column(name="city_id_new", type="integer", nullable=true)
     */
    private $cityIdNew;

    /**
     * @var string|null
     *
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $comment;

    /**
     * @var string|null
     *
     * @ORM\Column(type="guid", nullable=true)
     */
    private $guid;

    /**
     * @ORM\Column(name="currenttime", type="datetime", nullable=true)
     */
    private $currentTime;

    /**
     * @var string
     *
     * @ORM\Column(name="moduser", type="string", length=16, nullable=true)
     */
    private $modUser;

    /**
     * @ORM\Column(name="statusverification", type="integer", nullable=true)
     */
    private $statusVerification;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="sendverification", type="datetime", nullable=true)
     */
    private $sendVerification;

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return int
     */
    public function getCompanyId(): int
    {
        return $this->companyId;
    }

    /**
     * @param int $companyId
     */
    public function setCompanyId(int $companyId): void
    {
        $this->companyId = $companyId;
    }

    /**
     * @return int
     */
    public function getModifierId(): int
    {
        return $this->modifierId;
    }

    /**
     * @param int $modifierId
     */
    public function setModifierId(int $modifierId): void
    {
        $this->modifierId = $modifierId;
    }

    /**
     * @return \DateTime|null
     */
    public function getModified(): ?\DateTime
    {
        return $this->modified;
    }

    /**
     * @param \DateTime|null $modified
     */
    public function setModified(?\DateTime $modified): void
    {
        $this->modified = $modified;
    }

    /**
     * @return string|null
     */
    public function getField(): ?string
    {
        return $this->field;
    }

    /**
     * @param string|null $field
     */
    public function setField(?string $field): void
    {
        $this->field = $field;
    }

    /**
     * @return string|null
     */
    public function getOldValue(): ?string
    {
        return $this->oldValue;
    }

    /**
     * @param string|null $oldValue
     */
    public function setOldValue(?string $oldValue): void
    {
        $this->oldValue = $oldValue;
    }

    /**
     * @return string|null
     */
    public function getNewValue(): ?string
    {
        return $this->newValue;
    }

    /**
     * @param string|null $newValue
     */
    public function setNewValue(?string $newValue): void
    {
        $this->newValue = $newValue;
    }

    /**
     * @return int|null
     */
    public function getArchiveReasonIdOld(): ?int
    {
        return $this->archiveReasonIdOld;
    }

    /**
     * @param int|null $archiveReasonIdOld
     */
    public function setArchiveReasonIdOld(?int $archiveReasonIdOld): void
    {
        $this->archiveReasonIdOld = $archiveReasonIdOld;
    }

    /**
     * @return int|null
     */
    public function getArchiveReasonIdNew(): ?int
    {
        return $this->archiveReasonIdNew;
    }

    /**
     * @param int|null $archiveReasonIdNew
     */
    public function setArchiveReasonIdNew(?int $archiveReasonIdNew): void
    {
        $this->archiveReasonIdNew = $archiveReasonIdNew;
    }

    /**
     * @return int|null
     */
    public function getCompanyTypeIdOld(): ?int
    {
        return $this->companyTypeIdOld;
    }

    /**
     * @param int|null $companyTypeIdOld
     */
    public function setCompanyTypeIdOld(?int $companyTypeIdOld): void
    {
        $this->companyTypeIdOld = $companyTypeIdOld;
    }

    /**
     * @return int|null
     */
    public function getCompanyTypeIdNew(): ?int
    {
        return $this->companyTypeIdNew;
    }

    /**
     * @param int|null $companyTypeIdNew
     */
    public function setCompanyTypeIdNew(?int $companyTypeIdNew): void
    {
        $this->companyTypeIdNew = $companyTypeIdNew;
    }

    /**
     * @return int|null
     */
    public function getCityIdOld(): ?int
    {
        return $this->cityIdOld;
    }

    /**
     * @param int|null $cityIdOld
     */
    public function setCityIdOld(?int $cityIdOld): void
    {
        $this->cityIdOld = $cityIdOld;
    }

    /**
     * @return int|null
     */
    public function getCityIdNew(): ?int
    {
        return $this->cityIdNew;
    }

    /**
     * @param int|null $cityIdNew
     */
    public function setCityIdNew(?int $cityIdNew): void
    {
        $this->cityIdNew = $cityIdNew;
    }

    /**
     * @return string|null
     */
    public function getComment(): ?string
    {
        return $this->comment;
    }

    /**
     * @param string|null $comment
     */
    public function setComment(?string $comment): void
    {
        $this->comment = $comment;
    }

    /**
     * Set guid
     *
     * @param string $guid
     * @return InfoCompanyHistory
     */
    public function setGuid($guid)
    {
        $this->guid = $guid;

        return $this;
    }

    /**
     * Get guid
     *
     * @return string
     */
    public function getGuid()
    {
        return $this->guid;
    }

    /**
     * Set currenttime
     *
     * @param \DateTime $currenttime
     * @return InfoCompanyHistory
     */
    public function setCurrenttime(\DateTime $currenttime)
    {
        $this->currentTime = $currenttime;

        return $this;
    }

    /**
     * Get currenttime
     *
     * @return \DateTime
     */
    public function getCurrenttime()
    {
        return $this->currentTime;
    }

    /**
     * Set moduser
     *
     * @param string $moduser
     * @return InfoCompanyHistory
     */
    public function setModuser($moduser)
    {
        $this->modUser = $moduser;

        return $this;
    }

    /**
     * Get moduser
     *
     * @return string
     */
    public function getModuser()
    {
        return $this->modUser;
    }

    /**
     * @return mixed
     */
    public function getStatusVerification()
    {
        return $this->statusVerification;
    }

    /**
     * @param mixed $statusVerification
     */
    public function setStatusVerification($statusVerification): void
    {
        $this->statusVerification = $statusVerification;
    }

    /**
     * @return \DateTime|null
     */
    public function getSendVerification(): ?\DateTime
    {
        return $this->sendVerification;
    }

    /**
     * @param mixed $sendVerification
     */
    public function setSendVerification($sendVerification): void
    {
        $this->sendVerification = $sendVerification;
    }
}
