<?php

namespace TeamSoft\CrmRepositoryBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * InfoQualitycontrol
 *
 * @ORM\Table(name="info_qualitycontrol")
 * @ORM\Entity(repositoryClass="TeamSoft\CrmRepositoryBundle\Repository\InfoQualitycontrol")
 */
class InfoQualitycontrol implements ServiceFieldInterface
{
    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dt", type="datetime", nullable=true)
     */
    private $dt;

    /**
     * @var string
     *
     * @ORM\Column(name="serialnumber", type="string", length=255, nullable=true)
     */
    private $serialnumber;

    /**
     * @var string
     *
     * @ORM\Column(name="descr", type="string", length=255, nullable=true)
     */
    private $descr;

    /**
     * @var string
     *
     * @ORM\Column(name="status", type="string", length=255, nullable=true)
     */
    private $status;

    private $statusTranslation;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="currenttime", type="datetime", nullable=true)
     */
    private $currenttime;

    /**
     * @var string
     *
     * @ORM\Column(name="moduser", type="string", length=16, nullable=true)
     */
    private $moduser;

    /**
     * @var guid
     *
     * @ORM\Column(name="guid", type="guid", nullable=true)
     */
    private $guid;

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var \TeamSoft\CrmRepositoryBundle\Entity\InfoDictionary
     *
     * @ORM\ManyToOne(targetEntity="InfoDictionary")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="type_id", referencedColumnName="id")
     * })
     */
    private $type;

    /**
     * @var \TeamSoft\CrmRepositoryBundle\Entity\InfoTask
     *
     * @ORM\ManyToOne(targetEntity="InfoTask")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="task_id", referencedColumnName="id")
     * })
     */
    private $task;

    /**
     * @var \TeamSoft\CrmRepositoryBundle\Entity\InfoPreparation
     *
     * @ORM\ManyToOne(targetEntity="InfoPreparation")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="preparation_id", referencedColumnName="id")
     * })
     */
    private $preparation;

    /**
     * @var \TeamSoft\CrmRepositoryBundle\Entity\InfoContact
     *
     * @ORM\ManyToOne(targetEntity="InfoContact")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="contact_id", referencedColumnName="id")
     * })
     */
    private $contact;

    /**
     * @var \TeamSoft\CrmRepositoryBundle\Entity\InfoCompany
     *
     * @ORM\ManyToOne(targetEntity="InfoCompany")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="company_id", referencedColumnName="id")
     * })
     */
    private $company;

    /**
     * @var \TeamSoft\CrmRepositoryBundle\Entity\InfoQualitycontrolphoto
     *
     * @ORM\OneToMany(targetEntity="InfoQualitycontrolphoto", mappedBy="qualitycontrol", cascade={"persist"})
     */

    private $photo;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dt_status", type="datetime", nullable=true)
     */
    private $dtStatus;

    /**
     * Set dt
     *
     * @param \DateTime $dt
     *
     * @return InfoQualitycontrol
     */
    public function setDt($dt)
    {
        $this->dt = $dt;

        return $this;
    }

    /**
     * Get dt
     *
     * @return \DateTime
     */
    public function getDt()
    {
        return $this->dt;
    }

    /**
     * Set serialnumber
     *
     * @param string $serialnumber
     *
     * @return InfoQualitycontrol
     */
    public function setSerialnumber($serialnumber)
    {
        $this->serialnumber = $serialnumber;

        return $this;
    }

    /**
     * Get serialnumber
     *
     * @return string
     */
    public function getSerialnumber()
    {
        return $this->serialnumber;
    }

    /**
     * Set descr
     *
     * @param string $descr
     *
     * @return InfoQualitycontrol
     */
    public function setDescr($descr)
    {
        $this->descr = $descr;

        return $this;
    }

    /**
     * Get descr
     *
     * @return string
     */
    public function getDescr()
    {
        return $this->descr;
    }

    /**
     * Set status
     *
     * @param string $status
     *
     * @return InfoQualitycontrol
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return string
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set currenttime
     *
     * @param \DateTime $currenttime
     *
     * @return InfoQualitycontrol
     */
    public function setCurrenttime(\DateTime $currenttime)
    {

        $this->currenttime = $currenttime;

        return $this;
    }

    /**
     * Get currenttime
     *
     * @return \DateTime
     */
    public function getCurrenttime()
    {
        return $this->currenttime;
    }

    /**
     * Set moduser
     *
     * @param string $moduser
     *
     * @return InfoQualitycontrol
     */
    public function setModuser($moduser)
    {
        $this->moduser = $moduser;

        return $this;
    }

    /**
     * Get moduser
     *
     * @return string
     */
    public function getModuser()
    {
        return $this->moduser;
    }

    /**
     * Set guid
     *
     * @param guid $guid
     *
     * @return InfoQualitycontrol
     */
    public function setGuid($guid)
    {
        $this->guid = $guid;

        return $this;
    }

    /**
     * Get guid
     *
     * @return guid
     */
    public function getGuid()
    {
        return $this->guid;
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set type
     *
     * @param \TeamSoft\CrmRepositoryBundle\Entity\InfoDictionary $type
     *
     * @return InfoQualitycontrol
     */
    public function setType(\TeamSoft\CrmRepositoryBundle\Entity\InfoDictionary $type = null)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type
     *
     * @return \TeamSoft\CrmRepositoryBundle\Entity\InfoDictionary
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set task
     *
     * @param \TeamSoft\CrmRepositoryBundle\Entity\InfoTask $task
     *
     * @return InfoQualitycontrol
     */
    public function setTask(\TeamSoft\CrmRepositoryBundle\Entity\InfoTask $task = null)
    {
        $this->task = $task;

        return $this;
    }

    /**
     * Get task
     *
     * @return \TeamSoft\CrmRepositoryBundle\Entity\InfoTask
     */
    public function getTask()
    {
        return $this->task;
    }

    /**
     * Set preparation
     *
     * @param \TeamSoft\CrmRepositoryBundle\Entity\InfoPreparation $preparation
     *
     * @return InfoQualitycontrol
     */
    public function setPreparation(\TeamSoft\CrmRepositoryBundle\Entity\InfoPreparation $preparation = null)
    {
        $this->preparation = $preparation;

        return $this;
    }

    /**
     * Get preparation
     *
     * @return \TeamSoft\CrmRepositoryBundle\Entity\InfoPreparation
     */
    public function getPreparation()
    {
        return $this->preparation;
    }

    /**
     * Set contact
     *
     * @param \TeamSoft\CrmRepositoryBundle\Entity\InfoContact $contact
     *
     * @return InfoQualitycontrol
     */
    public function setContact(\TeamSoft\CrmRepositoryBundle\Entity\InfoContact $contact = null)
    {
        $this->contact = $contact;

        return $this;
    }

    /**
     * Get contact
     *
     * @return \TeamSoft\CrmRepositoryBundle\Entity\InfoContact
     */
    public function getContact()
    {
        return $this->contact;
    }

    /**
     * Set company
     *
     * @param \TeamSoft\CrmRepositoryBundle\Entity\InfoCompany $company
     *
     * @return InfoQualitycontrol
     */
    public function setCompany(\TeamSoft\CrmRepositoryBundle\Entity\InfoCompany $company = null)
    {
        $this->company = $company;

        return $this;
    }

    /**
     * Get company
     *
     * @return \TeamSoft\CrmRepositoryBundle\Entity\InfoCompany
     */
    public function getCompany()
    {
        return $this->company;
    }

    /**
     * Get company
     *
     * @return \TeamSoft\CrmRepositoryBundle\Entity\InfoQualitycontrolphoto
     */
    public function getQualityControlPhoto()
    {
        return $this->photo;
    }


    /**
     * Set statusTranslation
     *
     * @param string $statusTranslation
     *
     * @return InfoQualitycontrol
     */
    public function setStatusTranslation($statusTranslation)
    {
        $this->statusTranslation = $statusTranslation;

        return $this;
    }

    /**
     * Get statusTranslation
     *
     * @return InfoQualitycontrol
     */
    public function getStatusTranslation()
    {
        return $this->statusTranslation;
    }

    /**
     * Set dtStatus
     *
     * @param \DateTime $dtStatus
     *
     * @return InfoQualitycontrol
     */
    public function setDtStatus($dtStatus)
    {
        $this->dtStatus = $dtStatus;

        return $this;
    }

    /**
     * Get dtStatus
     *
     * @return \DateTime
     */
    public function getDtStatus()
    {
        return $this->dtStatus;
    }

}
