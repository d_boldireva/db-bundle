<?php

namespace TeamSoft\CrmRepositoryBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * InfoPlanpromo
 *
 * @ORM\Table(name="info_planpromo")
 * @ORM\Entity(repositoryClass="TeamSoft\CrmRepositoryBundle\Repository\InfoPlanpromo")
 */
class InfoPlanpromo implements ServiceFieldInterface
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var guid
     *
     * @ORM\Column(name="guid", type="guid", nullable=true)
     */
    private $guid;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="currenttime", type="datetime", nullable=true)
     */
    private $currenttime;

    /**
     * @var string
     *
     * @ORM\Column(name="moduser", type="string", length=16, nullable=true)
     */
    private $moduser;

    /**
     * @var int
     *
     * @ORM\Column(name="Cnt", type="integer", nullable=true)
     */
    private $count;

    /**
     * @var int
     *
     * @ORM\Column(name="ntask", type="integer", nullable=true)
     */
    private $ntask;

    /**
     * @var InfoPlandetail
     *
     * @ORM\ManyToOne(targetEntity="InfoPlandetail", inversedBy="planPromos")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="plandetail_id", referencedColumnName="id")
     * })
     */
    private $plandetail;

    /**
     * @var \InfoPromomaterial
     *
     * @ORM\ManyToOne(targetEntity="InfoPromomaterial")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="promo_id", referencedColumnName="id")
     * })
     */
    private $promomaterial;

    public function __clone()
    {
        $this->id = null;
        $this->guid = null;
        $this->currenttime = null;
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set guid
     *
     * @param guid $guid
     *
     * @return InfoPlanpromo
     */
    public function setGuid($guid)
    {
        $this->guid = $guid;

        return $this;
    }

    /**
     * Get guid
     *
     * @return guid
     */
    public function getGuid()
    {
        return $this->guid;
    }

    /**
     * Set currenttime
     *
     * @param \DateTime $currenttime
     *
     * @return InfoPlanpromo
     */
    public function setCurrenttime(\DateTime $currenttime = null)
    {
        $this->currenttime = $currenttime;

        return $this;
    }

    /**
     * Get currenttime
     *
     * @return \DateTime
     */
    public function getCurrenttime()
    {
        return $this->currenttime;
    }

    /**
     * Set moduser
     *
     * @param string $moduser
     *
     * @return InfoPlanpromo
     */
    public function setModuser($moduser)
    {
        $this->moduser = $moduser;

        return $this;
    }

    /**
     * Get moduser
     *
     * @return string
     */
    public function getModuser()
    {
        return $this->moduser;
    }

    /**
     * Set count
     *
     * @param int $count
     *
     * @return InfoPlanpromo
     */
    public function setCount($count)
    {
        $this->count = $count;

        return $this;
    }

    /**
     * Get count
     *
     * @return int
     */
    public function getCount()
    {
        return $this->count;
    }

    /**
     * Set ntask
     *
     * @param int $ntask
     *
     * @return InfoPlanpromo
     */
    public function setNtask($ntask)
    {
        $this->ntask = $ntask;

        return $this;
    }

    /**
     * Get ntask
     *
     * @return int
     */
    public function getNtask()
    {
        return $this->ntask;
    }

    /**
     * Set promomaterial
     *
     * @param \TeamSoft\CrmRepositoryBundle\Entity\InfoPromomaterial $promomaterial
     *
     * @return InfoPromomaterial
     */
    public function setPromomaterial(\TeamSoft\CrmRepositoryBundle\Entity\InfoPromomaterial $promomaterial = null)
    {
        $this->promomaterial = $promomaterial;

        return $this;
    }

    /**
     * Get promomaterial
     *
     * @return \TeamSoft\CrmRepositoryBundle\Entity\InfoPromomaterial
     */
    public function getPromomaterial()
    {
        return $this->promomaterial;
    }

    /**
     * Set plandetail
     *
     * @param \TeamSoft\CrmRepositoryBundle\Entity\InfoPlandetail $plandetail
     *
     * @return InfoPlanpromo
     */
    public function setPlandetail(\TeamSoft\CrmRepositoryBundle\Entity\InfoPlandetail $plandetail = null)
    {
        $this->plandetail = $plandetail;

        return $this;
    }

    /**
     * Get plandetail
     *
     * @return \TeamSoft\CrmRepositoryBundle\Entity\InfoPlandetail
     */
    public function getPlandetail()
    {
        return $this->plandetail;
    }

    /**
     * @return int|null
     */
    public function getPromomaterialId()
    {
        return $this->getPromomaterial() ? $this->getPromomaterial()->getId() : null;
    }

    /**
     * @return int|null
     */
    public function getPlanDetailId()
    {
        return $this->getPlandetail() ? $this->getPlandetail()->getId() : null;
    }
}
