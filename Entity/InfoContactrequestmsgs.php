<?php

namespace TeamSoft\CrmRepositoryBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * InfoContactrequestmsgs
 *
 * @ORM\Table(name="info_contactrequestmsgs")
 * @ORM\Entity
 */
class InfoContactrequestmsgs
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="msg", type="string", length=255, nullable=true)
     */
    private $msg;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dt", type="datetime", nullable=true)
     */
    private $dt;

    /**
     * @var int
     *
     * @ORM\Column(name="disabled", type="integer", nullable=true)
     */
    private $disabled;

    /**
     * @var int
     *
     * @ORM\Column(name="isOnlyTaskOwner", type="integer", nullable=true)
     */
    private $isOnlyTaskOwner;

    /**
     * @var int
     *
     * @ORM\Column(name="contactrequestinfo_id", type="integer", nullable=true)
     */
    private $contactrequestinfoId;

    /**
     * @var int
     *
     * @ORM\Column(name="taskparam_id", type="integer", nullable=true)
     */
    private $taskparamId;

    /**
     * @var InfoUser
     *
     * @ORM\ManyToMany(targetEntity="InfoClm")
     * @ORM\JoinTable(name="info_contactrequestmsgsclm",
     *     joinColumns={@ORM\JoinColumn(name="contactrequestmsgs_id", referencedColumnName="id")},
     *     inverseJoinColumns={@ORM\JoinColumn(name="clm_id", referencedColumnName="id")}
     * )
     */
    private $clms;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->clms = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set msg
     *
     * @param string $msg
     *
     * @return InfoContactrequestmsgs
     */
    public function setMsg($msg)
    {
        $this->msg = $msg;

        return $this;
    }

    /**
     * Get msg
     *
     * @return string
     */
    public function getMsg()
    {
        return $this->msg;
    }

    /**
     * Set dt
     *
     * @param \DateTime $dt
     *
     * @return InfoContactrequestmsgs
     */
    public function setDt($dt)
    {
        $this->dt = $dt;

        return $this;
    }

    /**
     * Get dt
     *
     * @return \DateTime
     */
    public function getDt()
    {
        return $this->dt;
    }

    /**
     * Set disabled
     *
     * @param int $disabled
     *
     * @return InfoContactrequestmsgs
     */
    public function setDisabled($disabled)
    {
        $this->disabled = $disabled;

        return $this;
    }

    /**
     * Get disabled
     *
     * @return int
     */
    public function getDisabled()
    {
        return $this->disabled;
    }

    /**
     * Set isOnlyTaskOwner
     *
     * @param int $isOnlyTaskOwner
     *
     * @return InfoContactrequestmsgs
     */
    public function setIsOnlyTaskOwner($isOnlyTaskOwner)
    {
        $this->isOnlyTaskOwner = $isOnlyTaskOwner;

        return $this;
    }

    /**
     * Get isOnlyTaskOwner
     *
     * @return int
     */
    public function getIsOnlyTaskOwner()
    {
        return $this->isOnlyTaskOwner;
    }

    /**
     * Set contactrequestinfoId
     *
     * @param int $contactrequestinfoId
     *
     * @return InfoContactrequestmsgs
     */
    public function setContactrequestinfoId($contactrequestinfoId)
    {
        $this->contactrequestinfoId = $contactrequestinfoId;

        return $this;
    }

    /**
     * Get contactrequestinfoId
     *
     * @return int
     */
    public function getContactrequestinfoId()
    {
        return $this->contactrequestinfoId;
    }

    /**
     * Set taskparamId
     *
     * @param int $taskparamId
     *
     * @return InfoContactrequestmsgs
     */
    public function setTaskparamId($taskparamId)
    {
        $this->taskparamId = $taskparamId;
    
        return $this;
    }

    /**
     * Get taskparamId
     *
     * @return int
     */
    public function getTaskparamId()
    {
        return $this->taskparamId;
    }

    /**
     * Add clm.
     *
     * @param \TeamSoft\CrmRepositoryBundle\Entity\InfoClm $clm
     *
     * @return InfoContactrequestmsgs
     */
    public function addClm(\TeamSoft\CrmRepositoryBundle\Entity\InfoClm $clm)
    {
        $this->clms->add($clm);

        return $this;
    }

    /**
     * Remove clm.
     *
     * @param \TeamSoft\CrmRepositoryBundle\Entity\InfoClm $clm
     *
     * @return boolean TRUE if this collection contained the specified element, FALSE otherwise.
     */
    public function removeClm(\TeamSoft\CrmRepositoryBundle\Entity\InfoClm $clm)
    {
        return $this->clms->removeElement($clm);
    }

    /**
     * Get clms.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getClms()
    {
        return $this->clms;
    }
}
