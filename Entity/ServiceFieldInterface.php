<?php
namespace TeamSoft\CrmRepositoryBundle\Entity;

interface ServiceFieldInterface {
    /**
     * @return \DateTime|string
     */
    public function getCurrenttime();

    /**
     * @param \DateTime $value
     * @return self
     */
    public function setCurrenttime(\DateTime $value);

    /**
     * @return integer|string
     */
    public function getGuid();

    /**
     * @param integer $value
     * @return self
     */
    public function setGuid($value);

    /**
     * @return string
     */
    public function getModuser();

    /**
     * @param string $value
     * @return self
     */
    public function setModuser($value);
}