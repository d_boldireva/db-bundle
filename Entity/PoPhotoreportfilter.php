<?php

namespace TeamSoft\CrmRepositoryBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * PoPhotoreportfilter
 *
 * @ORM\Table(name="po_photoreportfilter")
 * @ORM\Entity()
 */
class PoPhotoreportfilter implements ServiceFieldInterface
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string|null
     *
     * @ORM\Column(name="code", type="string", length=255, nullable=true)
     */
    private $code;

    /**
     * @var string|null
     *
     * @ORM\Column(name="name", type="string", length=255, nullable=true)
     */
    private $name;

    /**
     * @var string|null
     *
     * @ORM\Column(name="filter", type="string", length=255, nullable=true)
     */
    private $filter;

    /**
     * @var string|null
     *
     * @ORM\Column(name="default_filter", type="string", length=255, nullable=true)
     */
    private $defaultFilter;

    /**
     * @var string
     *
     * @ORM\Column(name="position", type="integer", nullable=false)
     */
    private $position;

    /**
     * @var int
     *
     * @ORM\Column(name="field_data_type", type="integer", nullable=true)
     */
    private $fieldDataType;

    /**
     * @var string
     *
     * @ORM\Column(name="enable", type="integer", length=1, nullable=false)
     */
    private $enable;

    /**
     * @var string
     *
     * @ORM\Column(name="is_required", type="integer", length=1, nullable=false)
     */
    private $isRequired;

    /**
     * @var string
     *
     * @ORM\Column(name="custom_where", type="string", length=255, nullable=true)
     */
    private $customWhere;

    /**
     * @var \TeamSoft\CrmRepositoryBundle\Entity\InfoCustomdictionary
     *
     * @ORM\ManyToOne(targetEntity="\TeamSoft\CrmRepositoryBundle\Entity\InfoCustomdictionary")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="customdictionary_id", referencedColumnName="id")
     * })
     */
    private $customDictionary;

    /**
     * @var \TeamSoft\CrmRepositoryBundle\Entity\InfoSystemdictionary
     *
     * @ORM\ManyToOne(targetEntity="\TeamSoft\CrmRepositoryBundle\Entity\InfoSystemdictionary")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="systemdictionary_id", referencedColumnName="id")
     * })
     */
    private $systemDictionary;

    /**
     * @var DateTime|null
     *
     * @ORM\Column(name="currenttime", type="datetime", nullable=true)
     */
    private $currenttime;

    /**
     * @var string|null
     *
     * @ORM\Column(name="moduser", type="string", length=16, nullable=true)
     * )
     */
    private $moduser;

    /**
     * @var string|null
     *
     * @ORM\Column(name="guid", type="guid", nullable=true)
     */
    private $guid;

    /**
     * @var ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="PoPhotoreportfilterrelation", mappedBy="child", cascade={"persist"})
     */
    private $parents;

    /**
     * @var string|null
     *
     * @ORM\Column(name="additional_filter", type="string", length=255, nullable=true)
     */
    private $additionalFilter;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->parents = new ArrayCollection();
    }


    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getCode(): string
    {
        return $this->code;
    }

    /**
     * @param string $code
     *
     * @return PoPhotoreportfilter
     */
    public function setCode(string $code)
    {
        $this->code = $code;

        return $this;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return PoPhotoreportfilter
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set filter.
     *
     * @param string|null $filter
     *
     * @return PoPhotoreportfilter
     */
    public function setFilter($filter = null)
    {
        $this->filter = $filter;

        return $this;
    }

    /**
     * Get filter.
     *
     * @return string|null
     */
    public function getFilter()
    {
        return $this->filter;
    }

    /**
     * Set defaultFilter.
     *
     * @param string|null $defaultFilter
     *
     * @return PoPhotoreportfilter
     */
    public function setDefaultFilter($defaultFilter = null)
    {
        $this->defaultFilter = $defaultFilter;

        return $this;
    }

    /**
     * Get defaultFilter.
     *
     * @return string|null
     */
    public function getDefaultFilter()
    {
        return $this->defaultFilter;
    }

    /**
     * Set position.
     *
     * @param int|null $position
     *
     * @return PoPhotoreportfilter
     */
    public function setPosition($position = null)
    {
        $this->position = $position;

        return $this;
    }

    /**
     * Get position.
     *
     * @return int|null
     */
    public function getPosition()
    {
        return $this->position;
    }

    /**
     * Set fieldDataType
     *
     * @param int $fieldDataType
     *
     * @return PoPhotoreportfilter
     */
    public function setFieldDataType($fieldDataType)
    {
        $this->fieldDataType = $fieldDataType;

        return $this;
    }

    /**
     * Get fieldDataType
     *
     * @return int
     */
    public function getFieldDataType()
    {
        return $this->fieldDataType;
    }

    /**
     * Set enable.
     *
     * @param int|null $enable
     *
     * @return PoPhotoreportfilter
     */
    public function setEnable($enable = null)
    {
        $this->enable = $enable;

        return $this;
    }

    /**
     * Get enable.
     *
     * @return int|null
     */
    public function getEnable()
    {
        return $this->enable;
    }

    /**
     * Set isRequired
     *
     * @param string $isRequired
     *
     * @return PoPhotoreportfilter
     */
    public function setIsRequired($isRequired)
    {
        $this->isRequired = $isRequired;

        return $this;
    }

    /**
     * Get isRequired
     *
     * @return string
     */
    public function getIsRequired()
    {
        return $this->isRequired;
    }

    /**
     * Set customWhere
     *
     * @param string $customWhere
     *
     * @return PoPhotoreportfilter
     */
    public function setCustomWhere($customWhere)
    {
        $this->customWhere = $customWhere;

        return $this;
    }

    /**
     * Get customWhere
     *
     * @return string
     */
    public function getCustomWhere()
    {
        return $this->customWhere;
    }

    /**
     * Set customDictionary
     *
     * @param InfoCustomDictionary $customDictionary
     *
     * @return PoPhotoreportfilter
     */
    public function setCustomDictionary(InfoCustomDictionary $customDictionary = null)
    {
        $this->customDictionary = $customDictionary;

        return $this;
    }

    /**
     * Get customDictionary
     *
     * @return InfoCustomDictionary
     */
    public function getCustomDictionary()
    {
        return $this->customDictionary;
    }

    /**
     * Set systemDictionary
     *
     * @param InfoSystemDictionary $systemDictionary
     *
     * @return PoPhotoreportfilter
     */
    public function setSystemDictionary(InfoSystemDictionary $systemDictionary = null)
    {
        $this->systemDictionary = $systemDictionary;

        return $this;
    }

    /**
     * Get systemDictionary
     *
     * @return InfoSystemDictionary
     */
    public function getSystemDictionary()
    {
        return $this->systemDictionary;
    }

    /**
     * Set currenttime.
     *
     * @param DateTime|null $currenttime
     *
     * @return PoPhotoreportfilter
     */
    public function setCurrenttime(\DateTime $currenttime = null)
    {
        $this->currenttime = $currenttime;

        return $this;
    }

    /**
     * Get currenttime.
     *
     * @return DateTime|null
     */
    public function getCurrenttime()
    {
        return $this->currenttime;
    }

    /**
     * Set moduser.
     *
     * @param string|null $moduser
     *
     * @return PoPhotoreportfilter
     */
    public function setModuser($moduser = null)
    {
        $this->moduser = $moduser;

        return $this;
    }

    /**
     * Get moduser.
     *
     * @return string|null
     */
    public function getModuser()
    {
        return $this->moduser;
    }

    /**
     * Set guid.
     *
     * @param string|null $guid
     *
     * @return PoPhotoreportfilter
     */
    public function setGuid($guid = null)
    {
        $this->guid = $guid;

        return $this;
    }

    /**
     * Get guid.
     *
     * @return string|null
     */
    public function getGuid()
    {
        return $this->guid;
    }

    /**
     * Get parentsCollection
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getParents()
    {
        return $this->parents;
    }

    /**
     * Add parent
     *
     * @param PoPhotoreportfilterrelation $sliceFilter
     *
     * @return self
     */
    public function addParent(PoPhotoreportfilterrelation $parent)
    {
        $this->parents->add($parent);
        return $this;
    }

    /**
     * Remove parent
     *
     * @param PoPhotoreportfilterrelation $parent
     */
    public function removeSliceFilter(PoPhotoreportfilterrelation $parent)
    {
        $this->parents->removeElement($parent);
    }

    /**
     * Set additionalFilter.
     *
     * @param string|null $additionalFilter
     *
     * @return PoPhotoreportfilter
     */
    public function setAdditionalFilter($additionalFilter = null)
    {
        $this->additionalFilter = $additionalFilter;

        return $this;
    }

    /**
     * Get additionalFilter.
     *
     * @return string|null
     */
    public function getAdditionalFilter()
    {
        return $this->additionalFilter;
    }
}
