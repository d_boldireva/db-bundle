<?php

namespace TeamSoft\CrmRepositoryBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * InfoMcmcontactanswer
 *
 * @ORM\Table(name="info_mcmcontactanswer")
 * @ORM\Entity
 */
class InfoMcmcontactanswer implements ServiceFieldInterface
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var InfoMcmcontactdistributionmsg
     *
     * @ORM\ManyToOne(targetEntity="InfoMcmcontactdistributionmsg", cascade={"persist", "remove"})
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="distributionmsg_id", referencedColumnName="id")
     * })
     */
    private $distributionMsg;

    /**
     * @var string|null
     *
     * @ORM\Column(name="msg", type="string", length=1024, nullable=true)
     */
    private $msg;

    /**
     * @var string|null
     *
     * @ORM\Column(name="url", type="string", length=1024, nullable=true)
     */
    private $url;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dt", type="datetime", nullable=true)
     */
    private $dt;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="currenttime", type="datetime", nullable=true)
     */
    private $currenttime;

    /**
     * @var string
     *
     * @ORM\Column(name="moduser", type="string", length=16, nullable=true)
     */
    private $moduser;

    /**
     * @var string
     *
     * @ORM\Column(name="guid", type="guid", nullable=true)
     */
    private $guid;

    /**
     * @var integer|null
     *
     * @ORM\Column(name="telegramchat_id", type="integer", nullable=true)
     */
    private $telegramChat;

    /**
     * @var integer|null
     *
     * @ORM\Column(name="telegramupdate_id", type="integer", nullable=true)
     */
    private $telegramUpdate;

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set distributionmsg
     *
     * @param InfoMcmcontactdistributionmsg|null $distributionMsg
     *
     * @return InfoMcmcontactanswer
     */
    public function setDistributionMsg(InfoMcmcontactdistributionmsg $distributionMsg = null)
    {
        $this->distributionMsg = $distributionMsg;

        return $this;
    }

    /**
     * Get distributionmsg
     *
     * @return InfoMcmcontactdistributionmsg
     */
    public function getDistributionMsg()
    {
        return $this->distributionMsg;
    }

    /**
     * Set msg
     *
     * @param string|null $msg
     *
     * @return InfoMcmcontactanswer
     */
    public function setMsg(string $msg = null): self
    {
        $this->msg = $msg;

        return $this;
    }

    /**
     * Get msg
     *
     * @return null|string
     */
    public function getMsg(): ?string
    {
        return $this->msg;
    }

    /**
     * Set url
     *
     * @param string|null $url
     *
     * @return InfoMcmcontactanswer
     */
    public function setUrl(string $url = null): self
    {
        $this->url = $url;

        return $this;
    }

    /**
     * Get url
     *
     * @return string|null
     */
    public function getUrl()
    {
        return $this->url;
    }

    /**
     * Set dt
     *
     * @param \DateTime $dt
     *
     * @return InfoMcmcontactanswer
     */
    public function setDt(\DateTime $dt): self
    {
        $this->dt = $dt;

        return $this;
    }

    /**
     * Get dt
     *
     * @return \DateTime
     */
    public function getDt()
    {
        return $this->dt;
    }

    /**
     * Set currenttime
     *
     * @param \DateTime $currenttime
     *
     * @return InfoMcmcontactanswer
     */
    public function setCurrenttime(\DateTime $currenttime): self
    {
        $this->currenttime = $currenttime;

        return $this;
    }

    /**
     * Get currenttime
     *
     * @return \DateTime
     */
    public function getCurrenttime()
    {
        return $this->currenttime;
    }

    /**
     * Set moduser
     *
     * @param string $moduser
     *
     * @return InfoMcmcontactanswer
     */
    public function setModuser($moduser): self
    {
        $this->moduser = $moduser;

        return $this;
    }

    /**
     * Get moduser
     *
     * @return string
     */
    public function getModuser()
    {
        return $this->moduser;
    }

    /**
     * Set guid
     *
     * @param string $guid
     *
     * @return InfoMcmcontactanswer
     */
    public function setGuid($guid): self
    {
        $this->guid = $guid;

        return $this;
    }

    /**
     * Get guid
     *
     * @return string
     */
    public function getGuid()
    {
        return $this->guid;
    }

    /**
     * Set telegramChat
     *
     * @param int $telegramChat
     *
     * @return InfoMcmcontactanswer
     */
    public function setTelegramChat(?int $telegramChat): self
    {
        $this->telegramChat = $telegramChat;

        return $this;
    }

    /**
     * Get telegramChat
     *
     * @return int
     */
    public function getTelegramChat(): ?int
    {
        return $this->telegramChat;
    }

    /**
     * Set telegramUpdate
     *
     * @param int $telegramUpdate
     *
     * @return InfoMcmcontactanswer
     */
    public function setTelegramUpdate(?int $telegramUpdate): self
    {
        $this->telegramUpdate = $telegramUpdate;

        return $this;
    }

    /**
     * Get telegramChat
     *
     * @return int
     */
    public function getTelegramUpdate(): ?int
    {
        return $this->telegramUpdate;
    }
}
