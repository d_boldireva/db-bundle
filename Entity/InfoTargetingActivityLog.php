<?php
declare(strict_types=1);


namespace TeamSoft\CrmRepositoryBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * InfoTargetingActivityLog
 *
 * @ORM\Table(name="info_targetingactivitylog", indexes={
 *     @ORM\Index(name="info_targetingactivitylog", columns={"guid"})
 * })
 * @ORM\Entity(repositoryClass="TeamSoft\CrmRepositoryBundle\Repository\InfoTargetingActivityLogRepository")
 */
class InfoTargetingActivityLog implements ServiceFieldInterface
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var InfoUser
     *
     * @ORM\ManyToOne(targetEntity="InfoUser")
     * @ORM\JoinColumns({
     *     @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     * })
     */
    private $user;

    /**
     * @var InfoUser
     *
     * @ORM\ManyToOne(targetEntity="InfoUser")
     * @ORM\JoinColumns({
     *     @ORM\JoinColumn(name="panel_user_id", referencedColumnName="id")
     * })
     */
    private $panelUser;

    /**
     * @var string
     * @ORM\Column(name="panel_type", type="string", nullable=false)
     */
    private $panelType;

    /**
     * @var integer
     * @ORM\Column(name="approvementstatus", type="integer", nullable=false)
     */
    private $approvementStatus;

    /**
     * @var string
     * @ORM\Column(name="period", type="string", nullable=false)
     */
    private $period;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="action_date", type="datetime", nullable=false)
     */
    private $actionDate;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="currenttime", type="datetime", nullable=true)
     */
    private $currenttime;

    /**
     * @var string
     *
     * @ORM\Column(name="moduser", type="string", length=16, nullable=true)
     */
    private $moduser;

    /**
     * @var string
     *
     * @ORM\Column(name="guid", type="guid", nullable=true)
     */
    private $guid;

    public function getId(): int
    {
        return $this->id;
    }

    public function getUser(): ?InfoUser
    {
        return $this->user;
    }

    public function setUser(?InfoUser $user): self
    {
        $this->user = $user;
        return $this;
    }

    public function getPanelUser(): ?InfoUser
    {
        return $this->panelUser;
    }

    public function setPanelUser(?InfoUser $panelUser): self
    {
        $this->panelUser = $panelUser;
        return $this;
    }

    public function getPanelType(): ?string
    {
        return $this->panelType;
    }

    public function setPanelType(?string $panelType): self
    {
        $this->panelType = $panelType;
        return $this;
    }

    public function getApprovementStatus(): ?int
    {
        return $this->approvementStatus;
    }

    public function setApprovementStatus(?int $approvementStatus): self
    {
        $this->approvementStatus = $approvementStatus;
        return $this;
    }

    public function getPeriod(): ?string
    {
        return $this->period;
    }

    public function setPeriod(?string $period): self
    {
        $this->period = $period;
        return $this;
    }

    public function getActionDate(): ?\DateTime
    {
        return $this->actionDate;
    }

    public function setActionDate(?\DateTime $actionDate): self
    {
        $this->actionDate = $actionDate;
        return $this;
    }

    public function getCurrenttime(): ?\DateTime
    {
        return $this->currenttime;
    }

    public function setCurrenttime(?\DateTime $currenttime): self
    {
        $this->currenttime = $currenttime;
        return $this;
    }

    public function getModuser(): ?string
    {
        return $this->moduser;
    }

    public function setModuser($moduser): self
    {
        $this->moduser = $moduser;
        return $this;
    }

    public function getGuid(): ?string
    {
        return $this->guid;
    }

    public function setGuid($guid): self
    {
        $this->guid = $guid;
        return $this;
    }
}