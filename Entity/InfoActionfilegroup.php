<?php

namespace TeamSoft\CrmRepositoryBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * InfoActionfilegroup
 *
 * @ORM\Table(name="info_actionfilegroup")
 * @ORM\Entity
 */
class InfoActionfilegroup implements ServiceFieldInterface
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=50, nullable=true)
     */
    private $name;

    /**
     * @var guid
     *
     * @ORM\Column(name="guid", type="guid", nullable=true)
     */
    private $guid;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="currenttime", type="datetime", nullable=true)
     */
    private $currenttime;

    /**
     * @var string
     *
     * @ORM\Column(name="moduser", type="string", length=16, nullable=true)
     */
    private $moduser;

    /**
     * @var InfoActionfilegroup
     *
     * @ORM\ManyToOne(targetEntity="InfoActionfilegroup")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="parent", referencedColumnName="id")
     * })
     */
    private $parent;

    /**
     * @var InfoAction
     *
     * @ORM\ManyToOne(targetEntity="InfoAction", inversedBy="actionFileGroupCollection")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="subj_id", referencedColumnName="id")
     * })
     */
    private $subj;

    /**
     * @var ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="InfoActionfile", mappedBy="subj", cascade={"persist"}, orphanRemoval=true)
     */
    private $actionFileCollection;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->actionFileCollection = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return InfoActionfilegroup
     */
    public function setName($name)
    {
        $this->name = $name;
    
        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set guid
     *
     * @param guid $guid
     *
     * @return InfoActionfilegroup
     */
    public function setGuid($guid)
    {
        $this->guid = $guid;
    
        return $this;
    }

    /**
     * Get guid
     *
     * @return guid
     */
    public function getGuid()
    {
        return $this->guid;
    }

    /**
     * Set currenttime
     *
     * @param \DateTime $currenttime
     *
     * @return InfoActionfilegroup
     */
    public function setCurrenttime(\DateTime $currenttime)
    {
        $this->currenttime = $currenttime;
    
        return $this;
    }

    /**
     * Get currenttime
     *
     * @return \DateTime
     */
    public function getCurrenttime()
    {
        return $this->currenttime;
    }

    /**
     * Set moduser
     *
     * @param string $moduser
     *
     * @return InfoActionfilegroup
     */
    public function setModuser($moduser)
    {
        $this->moduser = $moduser;
    
        return $this;
    }

    /**
     * Get moduser
     *
     * @return string
     */
    public function getModuser()
    {
        return $this->moduser;
    }

    /**
     * Set parent
     *
     * @param \TeamSoft\CrmRepositoryBundle\Entity\InfoActionfilegroup $parent
     *
     * @return InfoActionfilegroup
     */
    public function setParent(\TeamSoft\CrmRepositoryBundle\Entity\InfoActionfilegroup $parent = null)
    {
        $this->parent = $parent;
    
        return $this;
    }

    /**
     * Get parent
     *
     * @return \TeamSoft\CrmRepositoryBundle\Entity\InfoActionfilegroup
     */
    public function getParent()
    {
        return $this->parent;
    }

    /**
     * Set subj
     *
     * @param \TeamSoft\CrmRepositoryBundle\Entity\InfoAction $subj
     *
     * @return InfoActionfilegroup
     */
    public function setSubj(\TeamSoft\CrmRepositoryBundle\Entity\InfoAction $subj = null)
    {
        $this->subj = $subj;
    
        return $this;
    }

    /**
     * Get subj
     *
     * @return \TeamSoft\CrmRepositoryBundle\Entity\InfoAction
     */
    public function getSubj()
    {
        return $this->subj;
    }

    /**
     * Add actionFileCollection
     *
     * @param \TeamSoft\CrmRepositoryBundle\Entity\InfoActionfile $actionFile
     *
     * @return InfoActionfilegroup
     */
    public function addActionFileCollection(\TeamSoft\CrmRepositoryBundle\Entity\InfoActionfile $actionFile)
    {
        $actionFile->setSubj($this);
        $this->actionFileCollection->add($actionFile);
    
        return $this;
    }

    /**
     * Remove actionFileCollection
     *
     * @param \TeamSoft\CrmRepositoryBundle\Entity\InfoActionfile $actionFile
     */
    public function removeActionFileCollection(\TeamSoft\CrmRepositoryBundle\Entity\InfoActionfile $actionFile)
    {
        $actionFile->setSubj(null);
        $this->actionFileCollection->removeElement($actionFile);
    }

    /**
     * Get actionFileCollection
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getActionFileCollection()
    {
        return $this->actionFileCollection;
    }
}
