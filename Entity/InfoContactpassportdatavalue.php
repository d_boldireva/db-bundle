<?php

namespace TeamSoft\CrmRepositoryBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\JoinColumn;
use Doctrine\ORM\Mapping\ManyToOne;

/**
 * InfoContactpassportdatavalue
 *
 * @ORM\Table(name="info_Contactpassportdatavalue")
 * @ORM\Entity
 */
class InfoContactpassportdatavalue
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @ManyToOne(targetEntity="InfoContactPassportData", inversedBy="multichoice")
     *@JoinColumn(name="passportdata_id", referencedColumnName="id")
     */
    private $passportData;

    /**
     * @var InfoCustomdictionaryvalue
     *
     * @ORM\ManyToOne(targetEntity="InfoCustomdictionaryvalue", cascade={"persist"})
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="value_id", referencedColumnName="id")
     * })
     */
    private $value;

    /**
     * @var string
     *
     * @ORM\Column(name="fieldname", type="string", nullable=true)
     */
    private $fieldName;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getPassportData()
    {
        return $this->passportData;
    }

    /**
     * @param mixed $passportData
     *
     * @return InfoCustomdictionaryvalue
     */
    public function setPassportData($passportData): self
    {
        $this->passportData = $passportData;

        return $this;
    }

    /**
     * @return InfoCustomdictionaryvalue
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     * @param  $value
     *
     * @return InfoCustomdictionaryvalue
     */
    public function setValue( $value): self
    {
        $this->value = $value;

        return $this;
    }

    /**
     * @return string
     */
    public function getFieldName()
    {
        return $this->fieldName;
    }

    /**
     * @param string $fieldName
     *
     * @return InfoCustomdictionaryvalue
     */
    public function setFieldName(string $fieldName): self
    {
        $this->fieldName = $fieldName;

        return $this;
    }
}
