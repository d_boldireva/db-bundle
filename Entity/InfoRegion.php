<?php

namespace TeamSoft\CrmRepositoryBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * InfoRegion
 *
 * @ORM\Table(name="info_region", indexes={@ORM\Index(name="ix_info_region_guid", columns={"guid"}), @ORM\Index(name="ix_info_region_Country_id", columns={"Country_id"})})
 * @ORM\Entity(repositoryClass="TeamSoft\CrmRepositoryBundle\Repository\InfoRegion")
 */
class InfoRegion
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="Name", type="string", length=255, nullable=false)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="guid", type="guid", nullable=true)
     */
    private $guid;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="currenttime", type="datetime", nullable=true)
     */
    private $currenttime;

    /**
     * @var string
     *
     * @ORM\Column(name="moduser", type="string", length=16, nullable=true)
     */
    private $moduser;

    /**
     * @var string
     *
     * @ORM\Column(name="name2", type="string", length=255, nullable=true)
     */
    private $name2;

    /**
     * @var integer
     *
     * @ORM\Column(name="x", type="integer", nullable=true)
     */
    private $x;

    /**
     * @var integer
     *
     * @ORM\Column(name="y", type="integer", nullable=true)
     */
    private $y;

    /**
     * @var integer
     *
     * @ORM\Column(name="morionid", type="integer", nullable=true)
     */
    private $morionid;

    /**
     * @var integer
     *
     * @ORM\Column(name="isverification", type="integer", nullable=true)
     */
    private $isverification;

    /**
     * @var integer
     *
     * @ORM\Column(name="IsMillion", type="integer", nullable=true)
     */
    private $ismillion;

    /**
     * @var string
     *
     * @ORM\Column(name="gmt", type="string", nullable=true)
     */
    private $gmt;

    /**
     * @var InfoCountry
     *
     * @ORM\ManyToOne(targetEntity="InfoCountry", inversedBy="regions")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="Country_id", referencedColumnName="id")
     * })
     */
    private $country;

    /**
     * @ORM\ManyToMany(targetEntity="InfoRegionpart", inversedBy="regionCollection")
     * @ORM\JoinTable(name="info_regioninregionpart",
     *     joinColumns={@ORM\JoinColumn(name="region_id", referencedColumnName="id")},
     *     inverseJoinColumns={@ORM\JoinColumn(name="regionpart_id", referencedColumnName="id")}
     * )
     */
    private $regionparts;

    /**
     * @var ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="InfoCity", mappedBy="region")
     */
    private $citiesCollection;

    /**
     * @var InfoUser
     *
     * @ORM\ManyToMany(targetEntity="InfoUser")
     * @ORM\JoinTable(name="info_regioninuser",
     *     joinColumns={@ORM\JoinColumn(name="region_id", referencedColumnName="id")},
     *     inverseJoinColumns={@ORM\JoinColumn(name="user_id", referencedColumnName="id")}
     * )
     */
    private $usersCollection;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->citiesCollection = new ArrayCollection();
        $this->usersCollection = new ArrayCollection();
    }


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return InfoRegion
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set guid
     *
     * @param string $guid
     * @return InfoRegion
     */
    public function setGuid($guid)
    {
        $this->guid = $guid;

        return $this;
    }

    /**
     * Get guid
     *
     * @return string
     */
    public function getGuid()
    {
        return $this->guid;
    }

    /**
     * Set currenttime
     *
     * @param \DateTime $currenttime
     * @return InfoRegion
     */
    public function setCurrenttime(\DateTime $currenttime)
    {
        $this->currenttime = $currenttime;

        return $this;
    }

    /**
     * Get currenttime
     *
     * @return \DateTime
     */
    public function getCurrenttime()
    {
        return $this->currenttime;
    }

    /**
     * Set moduser
     *
     * @param string $moduser
     * @return InfoRegion
     */
    public function setModuser($moduser)
    {
        $this->moduser = $moduser;

        return $this;
    }

    /**
     * Get moduser
     *
     * @return string
     */
    public function getModuser()
    {
        return $this->moduser;
    }

    /**
     * Set name2
     *
     * @param string $name2
     * @return InfoRegion
     */
    public function setName2($name2)
    {
        $this->name2 = $name2;

        return $this;
    }

    /**
     * Get name2
     *
     * @return string
     */
    public function getName2()
    {
        return $this->name2;
    }

    /**
     * Set x
     *
     * @param integer $x
     * @return InfoRegion
     */
    public function setX($x)
    {
        $this->x = $x;

        return $this;
    }

    /**
     * Get x
     *
     * @return integer
     */
    public function getX()
    {
        return $this->x;
    }

    /**
     * Set y
     *
     * @param integer $y
     * @return InfoRegion
     */
    public function setY($y)
    {
        $this->y = $y;

        return $this;
    }

    /**
     * Get y
     *
     * @return integer
     */
    public function getY()
    {
        return $this->y;
    }

    /**
     * Set morionid
     *
     * @param integer $morionid
     * @return InfoRegion
     */
    public function setMorionid($morionid)
    {
        $this->morionid = $morionid;

        return $this;
    }

    /**
     * Get morionid
     *
     * @return integer
     */
    public function getMorionid()
    {
        return $this->morionid;
    }

    /**
     * Set isverification
     *
     * @param integer $isverification
     * @return InfoRegion
     */
    public function setIsverification($isverification)
    {
        $this->isverification = $isverification;

        return $this;
    }

    /**
     * Get isverification
     *
     * @return integer
     */
    public function getIsverification()
    {
        return $this->isverification;
    }

    /**
     * Set ismillion
     *
     * @param integer $ismillion
     * @return InfoRegion
     */
    public function setIsmillion($ismillion)
    {
        $this->ismillion = $ismillion;

        return $this;
    }

    /**
     * Get ismillion
     *
     * @return integer
     */
    public function getIsmillion()
    {
        return $this->ismillion;
    }

    /**
     * Set country
     *
     * @param \TeamSoft\CrmRepositoryBundle\Entity\InfoCountry $country
     * @return InfoRegion
     */
    public function setCountry(\TeamSoft\CrmRepositoryBundle\Entity\InfoCountry $country = null)
    {
        $this->country = $country;

        return $this;
    }

    /**
     * Get country
     *
     * @return \TeamSoft\CrmRepositoryBundle\Entity\InfoCountry
     */
    public function getCountry()
    {
        return $this->country;
    }

    /**
     * @return mixed
     */
    public function getRegionparts()
    {
        return $this->regionparts;
    }

    /**
     * @param mixed $regionparts
     */
    public function setRegionparts($regionparts)
    {
        $this->regionparts = $regionparts;
    }

    /**
     * Set gmt
     *
     * @param string|null $gmt
     * @return InfoRegion
     */
    public function setGmt(?string $gmt): self
    {
        $this->gmt = $gmt;

        return $this;
    }

    /**
     * Get gmt
     *
     * @return string|null
     */
    public function getGmt()
    {
        return $this->gmt;
    }

    /**
     * Add citiesCollection
     *
     * @param \TeamSoft\CrmRepositoryBundle\Entity\InfoCity $citiesCollection
     *
     * @return InfoRegion
     */

    public function addCitiesCollection(\TeamSoft\CrmRepositoryBundle\Entity\InfoCity $citiesCollection)
    {
        $this->citiesCollection[] = $citiesCollection;

        return $this;
    }

    /**
     * Remove citiesCollection
     *
     * @param \TeamSoft\CrmRepositoryBundle\Entity\InfoCity $citiesCollection
     */
    public function removeCitiesCollection(\TeamSoft\CrmRepositoryBundle\Entity\InfoCity $citiesCollection)
    {
        $this->citiesCollection->removeElement($citiesCollection);
    }

    /**
     * Get citiesCollection
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getCitiesCollection()
    {
        return $this->citiesCollection;
    }

    /**
     * Add usersCollection
     *
     * @param \TeamSoft\CrmRepositoryBundle\Entity\InfoCity $usersCollection
     *
     * @return InfoRegion
     */

    public function addUsersCollection(\TeamSoft\CrmRepositoryBundle\Entity\InfoCity $usersCollection)
    {
        $this->usersCollection[] = $usersCollection;

        return $this;
    }

    /**
     * Remove usersCollection
     *
     * @param \TeamSoft\CrmRepositoryBundle\Entity\InfoCity $usersCollection
     */
    public function removeUsersCollection(\TeamSoft\CrmRepositoryBundle\Entity\InfoCity $usersCollection)
    {
        $this->usersCollection->removeElement($usersCollection);
    }

    /**
     * Get usersCollection
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getUsersCollection()
    {
        return $this->usersCollection;
    }

    /**
     * @return bool
     */
    public function isVerification(): bool
    {
        return (bool) $this->getIsverification();
    }
}
