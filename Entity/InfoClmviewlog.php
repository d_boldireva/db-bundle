<?php

namespace TeamSoft\CrmRepositoryBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use TeamSoft\CrmRepositoryBundle\Entity\ServiceFieldInterface;

/**
 * InfoClmviewlog
 *
 * @ORM\Table(name="info_clmviewlog")
 * @ORM\Entity(repositoryClass="TeamSoft\CrmRepositoryBundle\Repository\InfoClmviewlog")
 */
class InfoClmviewlog implements ServiceFieldInterface
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="clmslidefilename", type="string", length=255, nullable=true)
     */
    private $clmslidefilename;

    /**
     * @var \DateTime
     */
    private $datefrom;

    /**
     * @var int
     *
     * @ORM\Column(name="numofsec", type="integer", nullable=true)
     */
    private $numofsec;

    /**
     * @var string
     *
     * @ORM\Column(name="guid", type="guid", nullable=true)
     */
    private $guid;

    /**
     * @var \DateTime
     */
    private $currenttime;

    /**
     * @var string
     */
    private $moduser;

    /**
     * @var \TeamSoft\CrmRepositoryBundle\Entity\InfoClm
     *
     * @ORM\ManyToOne(targetEntity="InfoClm", inversedBy="clmViewLogs")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="clm_id", referencedColumnName="id")
     * })
     */
    private $clm;

    /**
     * @var \TeamSoft\CrmRepositoryBundle\Entity\InfoClmslide
     *
     * @ORM\ManyToOne(targetEntity="InfoClmslide")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="clmslide_id", referencedColumnName="id")
     * })
     */
    private $clmslide;

    /**
     * @var \TeamSoft\CrmRepositoryBundle\Entity\InfoTask
     *
     * @ORM\ManyToOne(targetEntity="\TeamSoft\CrmRepositoryBundle\Entity\InfoTask")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="task_id", referencedColumnName="id")
     * })
     */
    private $task;

    /**
     * @var \TeamSoft\CrmRepositoryBundle\Entity\InfoUser
     *
     * @ORM\ManyToOne(targetEntity="\TeamSoft\CrmRepositoryBundle\Entity\InfoUser")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     * })
     */
    private $user;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set clmslidefilename
     *
     * @param string $clmslidefilename
     *
     * @return InfoClmviewlog
     */
    public function setClmslidefilename($clmslidefilename)
    {
        $this->clmslidefilename = $clmslidefilename;

        return $this;
    }

    /**
     * Get clmslidefilename
     *
     * @return string
     */
    public function getClmslidefilename()
    {
        return $this->clmslidefilename;
    }

    /**
     * Set datefrom
     *
     * @param \DateTime $datefrom
     *
     * @return InfoClmviewlog
     */
    public function setDatefrom($datefrom)
    {
        $this->datefrom = $datefrom;

        return $this;
    }

    /**
     * Get datefrom
     *
     * @return \DateTime
     */
    public function getDatefrom()
    {
        return $this->datefrom;
    }

    /**
     * Set numofsec
     *
     * @param int $numofsec
     *
     * @return InfoClmviewlog
     */
    public function setNumofsec($numofsec)
    {
        $this->numofsec = $numofsec;

        return $this;
    }

    /**
     * Get numofsec
     *
     * @return int
     */
    public function getNumofsec()
    {
        return $this->numofsec;
    }

    /**
     * Set guid
     *
     * @param guid $guid
     *
     * @return InfoClmviewlog
     */
    public function setGuid($guid)
    {
        $this->guid = $guid;

        return $this;
    }

    /**
     * Get guid
     *
     * @return guid
     */
    public function getGuid()
    {
        return $this->guid;
    }

    /**
     * Set currenttime
     *
     * @param \DateTime $currenttime
     *
     * @return InfoClmviewlog
     */
    public function setCurrenttime(\DateTime $currenttime)
    {
        $this->currenttime = $currenttime;

        return $this;
    }

    /**
     * Get currenttime
     *
     * @return \DateTime
     */
    public function getCurrenttime()
    {
        return $this->currenttime;
    }

    /**
     * Set moduser
     *
     * @param string $moduser
     *
     * @return InfoClmviewlog
     */
    public function setModuser($moduser)
    {
        $this->moduser = $moduser;

        return $this;
    }

    /**
     * Get moduser
     *
     * @return string
     */
    public function getModuser()
    {
        return $this->moduser;
    }

    /**
     * Set clm
     *
     * @param \TeamSoft\CrmRepositoryBundle\Entity\InfoClm $clm
     *
     * @return InfoClmviewlog
     */
    public function setClm(\TeamSoft\CrmRepositoryBundle\Entity\InfoClm $clm = null)
    {
        $this->clm = $clm;

        return $this;
    }

    /**
     * Get clm
     *
     * @return \TeamSoft\CrmRepositoryBundle\Entity\InfoClm
     */
    public function getClm()
    {
        return $this->clm;
    }

    /**
     * Set clmslide
     *
     * @param \TeamSoft\CrmRepositoryBundle\Entity\InfoClmslide $clmslide
     *
     * @return InfoClmviewlog
     */
    public function setClmslide(\TeamSoft\CrmRepositoryBundle\Entity\InfoClmslide $clmslide = null)
    {
        $this->clmslide = $clmslide;

        return $this;
    }

    /**
     * Get clmslide
     *
     * @return \TeamSoft\CrmRepositoryBundle\Entity\InfoClmslide
     */
    public function getClmslide()
    {
        return $this->clmslide;
    }

    /**
     * Set task
     *
     * @param \TeamSoft\CrmRepositoryBundle\Entity\InfoTask $task
     *
     * @return InfoClmviewlog
     */
    public function setTask(\TeamSoft\CrmRepositoryBundle\Entity\InfoTask $task = null)
    {
        $this->task = $task;

        return $this;
    }

    /**
     * Get task
     *
     * @return \TeamSoft\CrmRepositoryBundle\Entity\InfoTask
     */
    public function getTask()
    {
        return $this->task;
    }

    /**
     * Set user
     *
     * @param \TeamSoft\CrmRepositoryBundle\Entity\InfoUser $user
     *
     * @return InfoClmviewlog
     */
    public function setUser(\TeamSoft\CrmRepositoryBundle\Entity\InfoUser $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \TeamSoft\CrmRepositoryBundle\Entity\InfoUser
     */
    public function getUser()
    {
        return $this->user;
    }
}

