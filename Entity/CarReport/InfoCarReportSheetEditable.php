<?php declare(strict_types=1);

namespace TeamSoft\CrmRepositoryBundle\Entity\CarReport;

use Doctrine\ORM\Mapping as ORM;
use TeamSoft\CrmRepositoryBundle\Entity\InfoUser;
use TeamSoft\CrmRepositoryBundle\Entity\ServiceFieldInterface;

/**
 * Class InfoCarReportSheetEditable
 * @package TeamSoft\CrmRepositoryBundle\Entity\CarReport
 * @ORM\Table(name="info_carreportsheeteditable")
 * @ORM\Entity(repositoryClass="TeamSoft\CrmRepositoryBundle\Repository\CarReport\InfoCarReportSheetEditableRepository")
 */
class InfoCarReportSheetEditable implements ServiceFieldInterface
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id()
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="currenttime", type="datetime", nullable=true)
     */
    private $currenttime;

    /**
     * @var string|null
     *
     * @ORM\Column(name="moduser", type="string", length=16, nullable=true)
     */
    private $moduser;

    /**
     * @var string
     *
     * @ORM\Column(name="guid", type="guid", nullable=true)
     */
    private $guid;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="month", type="date")
     */
    private $month;

    /**
     * @var InfoUser|null
     *
     * @ORM\ManyToOne(targetEntity="TeamSoft\CrmRepositoryBundle\Entity\InfoUser")
     * @ORM\JoinColumn(referencedColumnName="id", name="user_id")
     */
    private $user;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="period_start", type="datetime")
     */
    private $periodStart;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="period_end", type="datetime")
     */
    private $periodEnd;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param \DateTime $currenttime
     * @return $this|ServiceFieldInterface
     */
    public function setCurrenttime(\DateTime $currenttime)
    {
        $this->currenttime = $currenttime;
        return $this;
    }

    /**
     * @return \DateTime|string|null
     */
    public function getCurrenttime()
    {
        return $this->currenttime;
    }

    /**
     * @param string $moduser
     * @return $this|ServiceFieldInterface
     */
    public function setModuser($moduser)
    {
        $this->moduser = $moduser;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getModuser()
    {
        return $this->moduser;
    }

    /**
     * @param int $guid
     * @return $this|ServiceFieldInterface
     */
    public function setGuid($guid)
    {
        $this->guid = $guid;
        return $this;
    }

    /**
     * @return int|string
     */
    public function getGuid()
    {
        return $this->guid;
    }

    /**
     * @return \DateTime|null
     */
    public function getMonth(): ?\DateTime
    {
        return $this->month;
    }

    /**
     * @param \DateTime|null $month
     * @return self
     */
    public function setMonth(?\DateTime $month): self
    {
        $this->month = $month;
        return $this;
    }

    /**
     * @return InfoUser|null
     */
    public function getUser(): ?InfoUser
    {
        return $this->user;
    }

    /**
     * @param InfoUser|null $user
     * @return self
     */
    public function setUser(?InfoUser $user): self
    {
        $this->user = $user;
        return $this;
    }

    /**
     * @return \DateTime|null
     */
    public function getPeriodStart(): ?\DateTime
    {
        return $this->periodStart;
    }

    /**
     * @param \DateTime|null $periodStart
     * @return self
     */
    public function setPeriodStart(?\DateTime $periodStart): self
    {
        $this->periodStart = $periodStart;
        return $this;
    }

    /**
     * @return \DateTime|null
     */
    public function getPeriodEnd(): ?\DateTime
    {
        return $this->periodEnd;
    }

    /**
     * @param \DateTime|null $periodEnd
     * @return self
     */
    public function setPeriodEnd(?\DateTime $periodEnd): self
    {
        $this->periodEnd = $periodEnd;
        return $this;
    }

    /**
     * @return int|null
     */
    public function getUserId(): ?int
    {
        return $this->getUser() ? $this->getUser()->getId() : null;
    }

    /**
     * @return \DatePeriod
     */
    public function getDatePeriod(): \DatePeriod
    {
        return new \DatePeriod(
            $this->getPeriodStart(),
            new \DateInterval('P1D'),
            (clone $this->getPeriodEnd())->setTime(23, 59, 59)
        );
    }
}
