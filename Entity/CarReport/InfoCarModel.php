<?php declare(strict_types=1);

namespace TeamSoft\CrmRepositoryBundle\Entity\CarReport;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use TeamSoft\CrmRepositoryBundle\Entity\ServiceFieldInterface;

/**
 * Class InfoCarModel
 * @package TeamSoft\CrmRepositoryBundle\Entity\CarReport
 * @ORM\Table(name="info_carmodel")
 * @ORM\Entity()
 */
class InfoCarModel implements ServiceFieldInterface
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id()
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="currenttime", type="datetime", nullable=true)
     */
    private $currenttime;

    /**
     * @var string|null
     *
     * @ORM\Column(name="moduser", type="string", length=16, nullable=true)
     */
    private $moduser;

    /**
     * @var string
     *
     * @ORM\Column(name="guid", type="guid", nullable=true)
     */
    private $guid;

    /**
     * @var string|null
     *
     * @ORM\Column(name="model", type="string", length=255, nullable=true)
     */
    private $model;

    /**
     * @var float|null
     *
     * @ORM\Column(name="fuel_norma_in_summer", type="float", nullable=true)
     */
    private $fuelNormaInSummer;

    /**
     * @var float|null
     *
     * @ORM\Column(name="fuel_norma_in_winter", type="float", nullable=true)
     */
    private $fuelNormaInWinter;

    /**
     * @var InfoCar[]|Collection
     * @ORM\OneToMany (targetEntity="TeamSoft\CrmRepositoryBundle\Entity\CarReport\InfoCar", mappedBy="model")
     */
    private $cars;

    public function __construct()
    {
        $this->cars = new ArrayCollection();
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param \DateTime $currenttime
     * @return $this|ServiceFieldInterface
     */
    public function setCurrenttime(\DateTime $currenttime)
    {
        $this->currenttime = $currenttime;
        return $this;
    }

    /**
     * @return \DateTime|string|null
     */
    public function getCurrenttime()
    {
        return $this->currenttime;
    }

    /**
     * @param string $moduser
     * @return $this|ServiceFieldInterface
     */
    public function setModuser($moduser)
    {
        $this->moduser = $moduser;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getModuser()
    {
        return $this->moduser;
    }

    /**
     * @param int $guid
     * @return $this|ServiceFieldInterface
     */
    public function setGuid($guid)
    {
        $this->guid = $guid;
        return $this;
    }

    /**
     * @return int|string
     */
    public function getGuid()
    {
        return $this->guid;
    }

    /**
     * @return string|null
     */
    public function getModel(): ?string
    {
        return $this->model;
    }

    /**
     * @param string|null $model
     * @return self
     */
    public function setModel(?string $model): self
    {
        $this->model = $model;
        return $this;
    }

    /**
     * @return float|null
     */
    public function getFuelNormaInSummer(): ?float
    {
        return $this->fuelNormaInSummer;
    }

    /**
     * @param float|null $fuelNormaInSummer
     * @return self
     */
    public function setFuelNormaInSummer(?float $fuelNormaInSummer): self
    {
        $this->fuelNormaInSummer = $fuelNormaInSummer;
        return $this;
    }

    /**
     * @return float|null
     */
    public function getFuelNormaInWinter(): ?float
    {
        return $this->fuelNormaInWinter;
    }

    /**
     * @param float|null $fuelNormaInWinter
     * @return self
     */
    public function setFuelNormaInWinter(?float $fuelNormaInWinter): self
    {
        $this->fuelNormaInWinter = $fuelNormaInWinter;
        return $this;
    }

    /**
     * @return Collection|InfoCar[]
     */
    public function getCars(): Collection
    {
        return $this->cars;
    }

    /**
     * @param InfoCar $car
     * @return $this
     */
    public function addCar(InfoCar $car): self
    {
        if (!$this->cars->contains($car)) {
            $car->setModel($this);
            $this->cars->add($car);
        }

        return $this;
    }

    /**
     * @param InfoCar $car
     * @return $this
     */
    public function removeCar(InfoCar $car): self
    {
        $this->cars->removeElement($car);

        return $this;
    }

    public function getFuelNormaByMonthNumber(int $month): ?float
    {
        /**
         * From november to march 
         */
        if ($month > 10 || $month < 4) {
            return $this->getFuelNormaInWinter();
        } 
        
        return $this->getFuelNormaInSummer();
    }
}
