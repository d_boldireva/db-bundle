<?php declare(strict_types=1);

namespace TeamSoft\CrmRepositoryBundle\Entity\CarReport;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use TeamSoft\CrmRepositoryBundle\Entity\ServiceFieldInterface;

/**
 * Class InfoCar
 * @package TeamSoft\CrmRepositoryBundle\Entity\CarReport
 * @ORM\Table(name="info_car")
 * @ORM\Entity(repositoryClass="TeamSoft\CrmRepositoryBundle\Repository\CarReport\InfoCarRepository")
 */
class InfoCar implements ServiceFieldInterface
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id()
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="currenttime", type="datetime", nullable=true)
     */
    private $currenttime;

    /**
     * @var string|null
     *
     * @ORM\Column(name="moduser", type="string", length=16, nullable=true)
     */
    private $moduser;

    /**
     * @var string
     *
     * @ORM\Column(name="guid", type="guid", nullable=true)
     */
    private $guid;

    /**
     * @var string|null
     *
     * @ORM\Column(name="num", type="string", length=16, nullable=true)
     */
    private $num;

    /**
     * @var string|null
     *
     * @ORM\Column(name="car_name", type="string", length=255, nullable=true)
     */
    private $carName;

    /**
     * @var float|null
     *
     * @ORM\Column(name="fuel_norma", type="float", nullable=true)
     */
    private $fuelNorma;

    /**
     * @ORM\OneToMany(targetEntity="TeamSoft\CrmRepositoryBundle\Entity\InfoUser", mappedBy="carModel")
     */
    private $users;

    /**
     * @var InfoCarModel|null
     * @ORM\ManyToOne (targetEntity="TeamSoft\CrmRepositoryBundle\Entity\CarReport\InfoCarModel", inversedBy="cars")
     * @ORM\JoinColumn(name="model_id", referencedColumnName="id")
     */
    private $model;

    public function __construct()
    {
        $this->users = new ArrayCollection();
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param \DateTime $currenttime
     * @return $this|ServiceFieldInterface
     */
    public function setCurrenttime(\DateTime $currenttime)
    {
        $this->currenttime = $currenttime;
        return $this;
    }

    /**
     * @return \DateTime|string|null
     */
    public function getCurrenttime()
    {
        return $this->currenttime;
    }

    /**
     * @param string $moduser
     * @return $this|ServiceFieldInterface
     */
    public function setModuser($moduser)
    {
        $this->moduser = $moduser;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getModuser()
    {
        return $this->moduser;
    }

    /**
     * @param int $guid
     * @return $this|ServiceFieldInterface
     */
    public function setGuid($guid)
    {
        $this->guid = $guid;
        return $this;
    }

    /**
     * @return int|string
     */
    public function getGuid()
    {
        return $this->guid;
    }

    /**
     * @return string|null
     */
    public function getNum(): ?string
    {
        return $this->num;
    }

    /**
     * @param string|null $num
     * @return self
     */
    public function setNum(?string $num): self
    {
        $this->num = $num;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getCarName(): ?string
    {
        return $this->carName;
    }

    /**
     * @param string|null $carName
     * @return self
     */
    public function setCarName(?string $carName): self
    {
        $this->carName = $carName;
        return $this;
    }

    /**
     * @return float|null
     */
    public function getFuelNorma(): ?float
    {
        return $this->fuelNorma;
    }

    /**
     * @param float|null $fuelNorma
     * @return self
     */
    public function setFuelNorma(?float $fuelNorma): self
    {
        $this->fuelNorma = $fuelNorma;
        return $this;
    }

    /**
     * @return InfoCarModel|null
     */
    public function getModel(): ?InfoCarModel
    {
        return $this->model;
    }

    /**
     * @param InfoCarModel|null $model
     * @return self
     */
    public function setModel(?InfoCarModel $model): self
    {
        $this->model = $model;
        return $this;
    }
}
