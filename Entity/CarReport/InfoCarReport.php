<?php declare(strict_types=1);

namespace TeamSoft\CrmRepositoryBundle\Entity\CarReport;

use Doctrine\ORM\Mapping as ORM;
use TeamSoft\CrmRepositoryBundle\Entity\InfoUser;
use TeamSoft\CrmRepositoryBundle\Entity\ServiceFieldInterface;

/**
 * Class InfoCarReport
 * @package TeamSoft\CrmRepositoryBundle\Entity\CarReport
 * @ORM\Entity(repositoryClass="TeamSoft\CrmRepositoryBundle\Repository\CarReport\InfoCarReportRepository")
 * @ORM\EntityListeners({"TeamSoft\CrmRepositoryBundle\EventListener\CarReport\InfoCarReportListener"})
 * @ORM\Table(name="info_carreport")
 */
class InfoCarReport implements ServiceFieldInterface
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id()
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="currenttime", type="datetime", nullable=true)
     */
    private $currenttime;

    /**
     * @var string|null
     *
     * @ORM\Column(name="moduser", type="string", length=16, nullable=true)
     */
    private $moduser;

    /**
     * @var string
     *
     * @ORM\Column(name="guid", type="guid", nullable=true)
     */
    private $guid;

    /**
     * @var InfoUser|null
     *
     * @ORM\ManyToOne(targetEntity="TeamSoft\CrmRepositoryBundle\Entity\InfoUser")
     * @ORM\JoinColumn(referencedColumnName="id", name="user_id")
     */
    private $user;

    /**
     * @var integer|null
     *
     * @ORM\Column(name="odometrvalue", type="integer", nullable=true)
     */
    private $odometerValue;

    /**
     * @var integer|null
     *
     * @ORM\Column(name="fuel_rest", type="integer", nullable=true)
     */
    private $fuelRest;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="real_date", type="datetime", nullable=true)
     */
    private $realDate;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="date", type="datetime", nullable=true)
     */
    private $date;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="dt_start", type="datetime", nullable=true)
     */
    private $dtStart;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="dt_finish", type="datetime", nullable=true)
     */
    private $dtFinish;

    /**
     * @var integer|null
     *
     * @ORM\Column(name="odometer_start", type="integer", nullable=true)
     */
    private $odometerStart;

    /**
     * @var integer|null
     *
     * @ORM\Column(name="odometer_finish", type="integer", nullable=true)
     */
    private $odometerFinish;

    /**
     * @var float|null
     *
     * @ORM\Column(name="refueled", type="float", nullable=true)
     */
    private $refueled;

    /**
     * @var float|null
     *
     * @ORM\Column(name="refueledbycard", type="float", nullable=true)
     */
    private $refueledByCard;

    /**
     * @var float|null
     *
     * @ORM\Column(name="fuel_start", type="float", nullable=true)
     */
    private $fuelStart;

    /**
     * @var float|null
     *
     * @ORM\Column(name="fuel_finish", type="float", nullable=true)
     */
    private $fuelFinish;

    /**
     * @var float|null
     *
     * @ORM\Column(name="isclosed", type="float", nullable=true)
     */
    private $isClosed;

    /**
     * @var InfoCar|null
     *
     * @ORM\ManyToOne(targetEntity="InfoCar")
     * @ORM\JoinColumn(name="car_id", referencedColumnName="id")
     */
    private $car;

    /**
     * @var InfoCarReportSheet|null
     *
     * @ORM\ManyToOne(targetEntity="InfoCarReportSheet", inversedBy="carReports")
     * @ORM\JoinColumn(name="car_report_sheet_id", referencedColumnName="id")
     */
    private $carReportSheet;

    /**
     * @var integer|null
     * @ORM\Column(name="skip_report", type="integer", nullable=true)
     */
    private $skipReport;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param \DateTime $currenttime
     * @return $this|ServiceFieldInterface
     */
    public function setCurrenttime(\DateTime $currenttime)
    {
        $this->currenttime = $currenttime;
        return $this;
    }

    /**
     * @return \DateTime|string|null
     */
    public function getCurrenttime()
    {
        return $this->currenttime;
    }

    /**
     * @param string $moduser
     * @return $this|ServiceFieldInterface
     */
    public function setModuser($moduser)
    {
        $this->moduser = $moduser;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getModuser()
    {
        return $this->moduser;
    }

    /**
     * @param int $guid
     * @return $this|ServiceFieldInterface
     */
    public function setGuid($guid)
    {
        $this->guid = $guid;
        return $this;
    }

    /**
     * @return int|string
     */
    public function getGuid()
    {
        return $this->guid;
    }

    /**
     * @return InfoUser|null
     */
    public function getUser(): ?InfoUser
    {
        return $this->user;
    }

    /**
     * @param InfoUser|null $user
     * @return self
     */
    public function setUser(?InfoUser $user): self
    {
        $this->user = $user;
        return $this;
    }

    /**
     * @return int|null
     */
    public function getOdometerValue(): ?int
    {
        return $this->odometerValue;
    }

    /**
     * @param int|null $odometerValue
     * @return self
     */
    public function setOdometerValue(?int $odometerValue): self
    {
        $this->odometerValue = $odometerValue;
        return $this;
    }

    /**
     * @return int|null
     */
    public function getFuelRest(): ?int
    {
        return $this->fuelRest;
    }

    /**
     * @param int|null $fuelRest
     * @return self
     */
    public function setFuelRest(?int $fuelRest): self
    {
        $this->fuelRest = $fuelRest;
        return $this;
    }

    /**
     * @return \DateTime|null
     */
    public function getRealDate(): ?\DateTime
    {
        return $this->realDate;
    }

    /**
     * @param \DateTime|null $realDate
     * @return self
     */
    public function setRealDate(?\DateTime $realDate): self
    {
        $this->realDate = $realDate;
        return $this;
    }

    /**
     * @return \DateTime|null
     */
    public function getDate(): ?\DateTime
    {
        return $this->date;
    }

    /**
     * @param \DateTime|null $date
     * @return self
     */
    public function setDate(?\DateTime $date): self
    {
        $this->date = $date;
        return $this;
    }

    /**
     * @return \DateTime|null
     */
    public function getDtStart(): ?\DateTime
    {
        return $this->dtStart;
    }

    /**
     * @param \DateTime|null $dtStart
     * @return self
     */
    public function setDtStart(?\DateTime $dtStart): self
    {
        $this->dtStart = $dtStart;
        return $this;
    }

    /**
     * @return \DateTime|null
     */
    public function getDtFinish(): ?\DateTime
    {
        return $this->dtFinish;
    }

    /**
     * @param \DateTime|null $dtFinish
     * @return self
     */
    public function setDtFinish(?\DateTime $dtFinish): self
    {
        $this->dtFinish = $dtFinish;
        return $this;
    }

    /**
     * @return int|null
     */
    public function getOdometerStart(): ?int
    {
        return $this->odometerStart;
    }

    /**
     * @param int|null $odometerStart
     * @return self
     */
    public function setOdometerStart(?int $odometerStart): self
    {
        $this->odometerStart = $odometerStart;
        return $this;
    }

    /**
     * @return int|null
     */
    public function getOdometerFinish(): ?int
    {
        return $this->odometerFinish;
    }

    /**
     * @param int|null $odometerFinish
     * @return self
     */
    public function setOdometerFinish(?int $odometerFinish): self
    {
        $this->odometerFinish = $odometerFinish;
        return $this;
    }

    /**
     * @return float|null
     */
    public function getRefueled(): ?float
    {
        return $this->refueled;
    }

    /**
     * @param float|null $refueled
     * @return self
     */
    public function setRefueled(?float $refueled): self
    {
        $this->refueled = $refueled;
        return $this;
    }

    /**
     * @return float|null
     */
    public function getRefueledByCard(): ?float
    {
        return $this->refueledByCard;
    }

    /**
     * @param float|null $refueledByCard
     * @return self
     */
    public function setRefueledByCard(?float $refueledByCard): self
    {
        $this->refueledByCard = $refueledByCard;
        return $this;
    }

    /**
     * @return float|null
     */
    public function getFuelStart(): ?float
    {
        return $this->fuelStart;
    }

    /**
     * @param float|null $fuelStart
     * @return self
     */
    public function setFuelStart(?float $fuelStart): self
    {
        $this->fuelStart = $fuelStart;
        return $this;
    }

    /**
     * @return float|null
     */
    public function getFuelFinish(): ?float
    {
        return $this->fuelFinish;
    }

    /**
     * @param float|null $fuelFinish
     * @return self
     */
    public function setFuelFinish(?float $fuelFinish): self
    {
        $this->fuelFinish = $fuelFinish;
        return $this;
    }

    /**
     * @return float|null
     */
    public function getIsClosed(): ?float
    {
        return $this->isClosed;
    }

    /**
     * @param float|null $isClosed
     * @return self
     */
    public function setIsClosed(?float $isClosed): self
    {
        $this->isClosed = $isClosed;
        return $this;
    }

    /**
     * @return InfoCar|null
     */
    public function getCar(): ?InfoCar
    {
        return $this->car;
    }

    /**
     * @param InfoCar|null $car
     * @return self
     */
    public function setCar(?InfoCar $car): self
    {
        $this->car = $car;
        return $this;
    }

    /**
     * @return InfoCarReportSheet|null
     */
    public function getCarReportSheet(): ?InfoCarReportSheet
    {
        return $this->carReportSheet;
    }

    /**
     * @param InfoCarReportSheet|null $carReportSheet
     * @return self
     */
    public function setCarReportSheet(?InfoCarReportSheet $carReportSheet): self
    {
        $this->carReportSheet = $carReportSheet;
        return $this;
    }

    /**
     * @return int|null
     */
    public function getSkipReport(): ?int
    {
        return $this->skipReport;
    }

    /**
     * @param int|null $skipReport
     * @return self
     */
    public function setSkipReport(?int $skipReport): self
    {
        $this->skipReport = $skipReport;
        return $this;
    }

    /**
     * @return int|null
     */
    public function getCarReportSheetId(): ?int
    {
        return $this->getCarReportSheet() ? $this->getCarReportSheet()->getId() : null;
    }

    /**
     * @return int|null
     */
    public function getUserId(): ?int
    {
        return $this->getUser() ? $this->getUser()->getId() : null;
    }

    public function isSkipReport(): bool
    {
        return (bool)$this->getSkipReport();
    }
}
