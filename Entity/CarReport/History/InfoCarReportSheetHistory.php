<?php declare(strict_types=1);

namespace TeamSoft\CrmRepositoryBundle\Entity\CarReport\History;

use TeamSoft\CrmRepositoryBundle\Entity\CarReport\InfoCarReportSheet;
use TeamSoft\CrmRepositoryBundle\Entity\InfoUser;
use TeamSoft\CrmRepositoryBundle\Entity\ServiceFieldInterface;
use Doctrine\ORM\Mapping as ORM;

/**
 * Class InfoCarReportSheetHistory
 * @package TeamSoft\CrmRepositoryBundle\Entity\CarReport\History
 * @ORM\Entity()
 * @ORM\Table(name="info_carreportsheethistory")
 */
class InfoCarReportSheetHistory implements ServiceFieldInterface
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id()
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="currenttime", type="datetime", nullable=true)
     */
    private $currenttime;

    /**
     * @var string|null
     *
     * @ORM\Column(name="moduser", type="string", length=16, nullable=true)
     */
    private $moduser;

    /**
     * @var string
     *
     * @ORM\Column(name="guid", type="guid", nullable=true)
     */
    private $guid;

    /**
     * @var integer|null
     *
     * @ORM\Column(name="odometer_finish", type="integer", nullable=true)
     */
    private $odometerFinish;

    /**
     * @var integer|null
     *
     * @ORM\Column(name="odometer_start", type="integer", nullable=true)
     */
    private $odometerStart;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="month", type="date")
     */
    private $month;
    
    /**
     * @var InfoUser|null
     *
     * @ORM\ManyToOne(targetEntity="TeamSoft\CrmRepositoryBundle\Entity\InfoUser")
     * @ORM\JoinColumn(referencedColumnName="id", name="user_id")
     */
    private $user;

    /**
     * @var InfoCarReportSheet|null
     * @ORM\ManyToOne(targetEntity="TeamSoft\CrmRepositoryBundle\Entity\CarReport\InfoCarReportSheet")
     * @ORM\JoinColumn(referencedColumnName="id", name="sheet_id")
     */
    private $sheet;
    
    /**
     * @var float|null
     * @ORM\Column(name="fuel_start", type="float", nullable=true)
     */
    private $fuelStart;

    /**
     * @var float|null
     * @ORM\Column(name="fuel_finish", type="float", nullable=true)
     */
    private $fuelFinish;

    /**
     * @var float|null
     * @ORM\Column(name="refueled", type="float", nullable=true)
     */
    private $refueled;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param \DateTime $currenttime
     * @return $this|ServiceFieldInterface
     */
    public function setCurrenttime(\DateTime $currenttime)
    {
        $this->currenttime = $currenttime;
        return $this;
    }

    /**
     * @return \DateTime|string|null
     */
    public function getCurrenttime()
    {
        return $this->currenttime;
    }

    /**
     * @param string $moduser
     * @return $this|ServiceFieldInterface
     */
    public function setModuser($moduser)
    {
        $this->moduser = $moduser;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getModuser()
    {
        return $this->moduser;
    }

    /**
     * @param int $guid
     * @return $this|ServiceFieldInterface
     */
    public function setGuid($guid)
    {
        $this->guid = $guid;
        return $this;
    }

    /**
     * @return int|string
     */
    public function getGuid()
    {
        return $this->guid;
    }

    /**
     * @return int|null
     */
    public function getOdometerStart(): ?int
    {
        return $this->odometerStart;
    }

    /**
     * @param int|null $odometerStart
     * @return self
     */
    public function setOdometerStart(?int $odometerStart): self
    {
        $this->odometerStart = $odometerStart;
        return $this;
    }

    /**
     * @return int|null
     */
    public function getOdometerFinish(): ?int
    {
        return $this->odometerFinish;
    }

    /**
     * @param int|null $odometerFinish
     * @return self
     */
    public function setOdometerFinish(?int $odometerFinish): self
    {
        $this->odometerFinish = $odometerFinish;
        return $this;
    }

    /**
     * @return \DateTime|null
     */
    public function getMonth(): ?\DateTime
    {
        return $this->month;
    }

    /**
     * @param \DateTime|null $month
     * @return self
     */
    public function setMonth(?\DateTime $month): self
    {
        $this->month = $month;
        return $this;
    }

    /**
     * @return InfoUser|null
     */
    public function getUser(): ?InfoUser
    {
        return $this->user;
    }

    /**
     * @param InfoUser|null $user
     * @return self
     */
    public function setUser(?InfoUser $user): self
    {
        $this->user = $user;
        return $this;
    }
    
    /**
     * @return float|null
     */
    public function getFuelStart(): ?float
    {
        return $this->fuelStart;
    }

    /**
     * @param float|null $fuelStart
     * @return self
     */
    public function setFuelStart(?float $fuelStart): self
    {
        $this->fuelStart = $fuelStart;
        return $this;
    }

    /**
     * @return float|null
     */
    public function getFuelFinish(): ?float
    {
        return $this->fuelFinish;
    }

    /**
     * @param float|null $fuelFinish
     * @return self
     */
    public function setFuelFinish(?float $fuelFinish): self
    {
        $this->fuelFinish = $fuelFinish;
        return $this;
    }

    /**
     * @return float|null
     */
    public function getRefueled(): ?float
    {
        return $this->refueled;
    }

    /**
     * @param float|null $refueled
     * @return self
     */
    public function setRefueled(?float $refueled): self
    {
        $this->refueled = $refueled;
        return $this;
    }

    /**
     * @return int|null
     */
    public function getUserId(): ?int
    {
        return $this->user ? $this->user->getId() : null;
    }

    /**
     * @return InfoCarReportSheet|null
     */
    public function getSheet(): ?InfoCarReportSheet
    {
        return $this->sheet;
    }

    /**
     * @param InfoCarReportSheet|null $sheet
     * @return self
     */
    public function setSheet(?InfoCarReportSheet $sheet): self
    {
        $this->sheet = $sheet;
        return $this;
    }
}
