<?php declare(strict_types=1);

namespace TeamSoft\CrmRepositoryBundle\Entity\CarReport;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use TeamSoft\CrmRepositoryBundle\Entity\InfoUser;
use TeamSoft\CrmRepositoryBundle\Entity\ServiceFieldInterface;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * Class InfoCarReportSheet
 * @package TeamSoft\CrmRepositoryBundle\Entity\CarReport
 * @ORM\Entity(repositoryClass="TeamSoft\CrmRepositoryBundle\Repository\CarReport\InfoCarReportSheetRepository")
 * @ORM\EntityListeners({"TeamSoft\CrmRepositoryBundle\EventListener\CarReport\InfoCarReportSheetListener"})
 * @ORM\Table(name="info_carreportsheet")
 * @UniqueEntity(fields={"user", "month"})
 */
class InfoCarReportSheet implements ServiceFieldInterface
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id()
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="currenttime", type="datetime", nullable=true)
     */
    private $currenttime;

    /**
     * @var string|null
     *
     * @ORM\Column(name="moduser", type="string", length=16, nullable=true)
     */
    private $moduser;

    /**
     * @var string
     *
     * @ORM\Column(name="guid", type="guid", nullable=true)
     */
    private $guid;

    /**
     * @var integer|null
     *
     * @ORM\Column(name="odometer_finish", type="integer", nullable=true)
     */
    private $odometerFinish;

    /**
     * @var integer|null
     *
     * @ORM\Column(name="odometer_start", type="integer", nullable=true)
     */
    private $odometerStart;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="month", type="date")
     */
    private $month;

    /**
     * @var InfoCarReportSheetFile|null
     * @ORM\OneToOne(targetEntity="TeamSoft\CrmRepositoryBundle\Entity\CarReport\InfoCarReportSheetFile", mappedBy="sheet", cascade={"persist"})
     */
    private $file;

    /**
     * @ORM\ManyToOne(targetEntity=InfoUser::class, inversedBy="carReportSheets")
     * @ORM\JoinColumn(referencedColumnName="id", name="user_id")
     */
    private $user;

    /**
     * @ORM\OneToMany(targetEntity="InfoCarReport", mappedBy="carReportSheet")
     * @ORM\OrderBy({"date" = "ASC"})
     */
    private $carReports;

    /**
     * @var float|null
     * @ORM\Column(name="fuel_start", type="float", nullable=true)
     */
    private $fuelStart;

    /**
     * @var float|null
     * @ORM\Column(name="fuel_finish", type="float", nullable=true)
     */
    private $fuelFinish;

    /**
     * @var float|null
     * @ORM\Column(name="refueled", type="float", nullable=true)
     */
    private $refueled;

    public function __construct()
    {
        $this->carReports = new ArrayCollection();
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param \DateTime $currenttime
     * @return $this|ServiceFieldInterface
     */
    public function setCurrenttime(\DateTime $currenttime)
    {
        $this->currenttime = $currenttime;
        return $this;
    }

    /**
     * @return \DateTime|string|null
     */
    public function getCurrenttime()
    {
        return $this->currenttime;
    }

    /**
     * @param string $moduser
     * @return $this|ServiceFieldInterface
     */
    public function setModuser($moduser)
    {
        $this->moduser = $moduser;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getModuser()
    {
        return $this->moduser;
    }

    /**
     * @param int $guid
     * @return $this|ServiceFieldInterface
     */
    public function setGuid($guid)
    {
        $this->guid = $guid;
        return $this;
    }

    /**
     * @return int|string
     */
    public function getGuid()
    {
        return $this->guid;
    }

    /**
     * @return int|null
     */
    public function getOdometerStart(): ?int
    {
        return $this->odometerStart;
    }

    /**
     * @param int|null $odometerStart
     * @return self
     */
    public function setOdometerStart(?int $odometerStart): self
    {
        $this->odometerStart = $odometerStart;
        return $this;
    }

    /**
     * @return int|null
     */
    public function getOdometerFinish(): ?int
    {
        return $this->odometerFinish;
    }

    /**
     * @param int|null $odometerFinish
     * @return self
     */
    public function setOdometerFinish(?int $odometerFinish): self
    {
        $this->odometerFinish = $odometerFinish;
        return $this;
    }

    /**
     * @return \DateTime|null
     */
    public function getMonth(): ?\DateTime
    {
        return $this->month;
    }

    /**
     * @param \DateTime|null $month
     * @return self
     */
    public function setMonth(?\DateTime $month): self
    {
        $this->month = $month;
        return $this;
    }

    /**
     * @return InfoUser|null
     */
    public function getUser(): ?InfoUser
    {
        return $this->user;
    }

    /**
     * @param InfoUser|null $user
     * @return self
     */
    public function setUser(?InfoUser $user): self
    {
        $this->user = $user;
//        $this->user_id = $user ? $user->getId() : null;
        return $this;
    }

    /**
     * @return Collection|InfoCarReport[]
     */
    public function getCarReports(): Collection
    {
        return $this->carReports;
    }

    public function addCarReport(InfoCarReport $carReport): self
    {
        if (!$this->carReports->contains($carReport)) {
            $this->carReports[] = $carReport;
            $carReport->setCarReportSheet($this);
        }

        return $this;
    }

    public function removeCarReport(InfoCarReport $carReport): self
    {
        if ($this->carReports->removeElement($carReport)) {
            // set the owning side to null (unless already changed)
            if ($carReport->getCarReportSheet() === $this) {
                $carReport->setCarReportSheet(null);
            }
        }

        return $this;
    }

    /**
     * @return float|null
     */
    public function getFuelStart(): ?float
    {
        return $this->fuelStart;
    }

    /**
     * @param float|null $fuelStart
     * @return self
     */
    public function setFuelStart(?float $fuelStart): self
    {
        $this->fuelStart = $fuelStart;
        return $this;
    }

    /**
     * @return float|null
     */
    public function getFuelFinish(): ?float
    {
        return $this->fuelFinish;
    }

    /**
     * @param float|null $fuelFinish
     * @return self
     */
    public function setFuelFinish(?float $fuelFinish): self
    {
        $this->fuelFinish = $fuelFinish;
        return $this;
    }

    /**
     * @return float|null
     */
    public function getRefueled(): ?float
    {
        return $this->refueled;
    }

    /**
     * @param float|null $refueled
     * @return self
     */
    public function setRefueled(?float $refueled): self
    {
        $this->refueled = $refueled;
        return $this;
    }

    /**
     * @return int|null
     */
    public function getUserId(): ?int
    {
        return $this->user ? $this->user->getId() : null;
    }

    /**
     * @return InfoCarReportSheetFile|null
     */
    public function getFile(): ?InfoCarReportSheetFile
    {
        return $this->file;
    }

    /**
     * @param InfoCarReportSheetFile $file
     * @return self
     */
    public function setFile(?InfoCarReportSheetFile $file): self
    {
        $this->file = $file;
        if ($file) {
            $file->setSheet($this);
        }

        return $this;
    }
}
