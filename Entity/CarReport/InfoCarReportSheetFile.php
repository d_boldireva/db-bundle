<?php declare(strict_types=1);

namespace TeamSoft\CrmRepositoryBundle\Entity\CarReport;

use Doctrine\ORM\Mapping as ORM;
use TeamSoft\CrmRepositoryBundle\Entity\ServiceFieldInterface;
use TeamSoft\CrmRepositoryBundle\Model\InfoFile;

/**
 * Class InfoCarReportSheet
 * @package TeamSoft\CrmRepositoryBundle\Entity\CarReport
 * @ORM\Entity(repositoryClass="TeamSoft\CrmRepositoryBundle\Repository\CarReport\InfoCarReportSheetRepository")
 * @ORM\Table(name="info_carreportsheetfile")
 */
class InfoCarReportSheetFile extends InfoFile implements ServiceFieldInterface
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id()
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="currenttime", type="datetime", nullable=true)
     */
    private $currenttime;

    /**
     * @var string|null
     *
     * @ORM\Column(name="moduser", type="string", length=16, nullable=true)
     */
    private $moduser;

    /**
     * @var string
     *
     * @ORM\Column(name="guid", type="guid", nullable=true)
     */
    private $guid;

    /**
     * @var string|null
     *
     * @ORM\Column(name="content", type="blob_or_string", nullable=true)
     */
    private $content;

    /**
     * @var InfoCarReportSheet|null
     *
     * @ORM\OneToOne (targetEntity="InfoCarReportSheet", inversedBy="file")
     * @ORM\JoinColumn(name="car_report_sheet_id", referencedColumnName="id")
     */
    private $sheet;
    
    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param \DateTime $currenttime
     * @return $this|ServiceFieldInterface
     */
    public function setCurrenttime(\DateTime $currenttime)
    {
        $this->currenttime = $currenttime;
        return $this;
    }

    /**
     * @return \DateTime|string|null
     */
    public function getCurrenttime()
    {
        return $this->currenttime;
    }

    /**
     * @param string $moduser
     * @return $this|ServiceFieldInterface
     */
    public function setModuser($moduser)
    {
        $this->moduser = $moduser;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getModuser()
    {
        return $this->moduser;
    }

    /**
     * @param int $guid
     * @return $this|ServiceFieldInterface
     */
    public function setGuid($guid)
    {
        $this->guid = $guid;
        return $this;
    }

    /**
     * @return int|string
     */
    public function getGuid()
    {
        return $this->guid;
    }

    /**
     * @return string|null
     */
    public function getContent()
    {
        return $this->content;
    }

    /**
     * @param $content
     * @return self
     */
    public function setContent($content): self
    {
        $this->content = $content;
        return $this;
    }

    /**
     * @return InfoCarReportSheet|null
     */
    public function getSheet(): ?InfoCarReportSheet
    {
        return $this->sheet;
    }

    /**
     * @param InfoCarReportSheet|null $sheet
     * @return self
     */
    public function setSheet(?InfoCarReportSheet $sheet): self
    {
        $this->sheet = $sheet;
        return $this;
    }

    public function getName()
    {
        return '';
    }
}
