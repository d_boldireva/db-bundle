<?php

namespace TeamSoft\CrmRepositoryBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * InfoPromoprojectmaterial
 *
 * @ORM\Table(name="info_promoprojectmaterial")
 * @ORM\Entity
 */
class InfoPromoprojectmaterial implements ServiceFieldInterface
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="currenttime", type="datetime", nullable=true)
     */
    private $currenttime;

    /**
     * @var string|null
     *
     * @ORM\Column(name="moduser", type="string", length=1, nullable=true)
     */
    private $moduser;

    /**
     * @var string|null
     *
     * @ORM\Column(name="guid", type="guid", nullable=true)
     */
    private $guid;

    /**
     * @var int|null
     *
     * @ORM\Column(name="cnt", type="integer", nullable=true)
     */
    private $cnt;

    /**
     * @var InfoPromoproject
     *
     * @ORM\ManyToOne(targetEntity="InfoPromoproject")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="promoproject_id", referencedColumnName="id")
     * })
     */
    private $promoproject;

    /**
     * @var InfoPromomaterial
     *
     * @ORM\ManyToOne(targetEntity="InfoPromomaterial")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="promomaterial_id", referencedColumnName="id")
     * })
     */
    private $promomaterial;

    /**
     * @var \InfoPromoprojectdetail
     *
     * @ORM\ManyToOne(targetEntity="InfoPromoprojectdetail")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="promoprojectdetail_id", referencedColumnName="id")
     * })
     */
    private $promoprojectdetail;



    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set currenttime.
     *
     * @param \DateTime|null $currenttime
     *
     * @return InfoPromoprojectmaterial
     */
    public function setCurrenttime(\DateTime $currenttime = null)
    {
        $this->currenttime = $currenttime;

        return $this;
    }

    /**
     * Get currenttime.
     *
     * @return \DateTime|null
     */
    public function getCurrenttime()
    {
        return $this->currenttime;
    }

    /**
     * Set moduser.
     *
     * @param string|null $moduser
     *
     * @return InfoPromoprojectmaterial
     */
    public function setModuser($moduser = null)
    {
        $this->moduser = $moduser;

        return $this;
    }

    /**
     * Get moduser.
     *
     * @return string|null
     */
    public function getModuser()
    {
        return $this->moduser;
    }

    /**
     * Set guid.
     *
     * @param string|null $guid
     *
     * @return InfoPromoprojectmaterial
     */
    public function setGuid($guid = null)
    {
        $this->guid = $guid;

        return $this;
    }

    /**
     * Get guid.
     *
     * @return string|null
     */
    public function getGuid()
    {
        return $this->guid;
    }

    /**
     * Set cnt.
     *
     * @param int|null $cnt
     *
     * @return InfoPromoprojectmaterial
     */
    public function setCnt($cnt = null)
    {
        $this->cnt = $cnt;

        return $this;
    }

    /**
     * Get cnt.
     *
     * @return int|null
     */
    public function getCnt()
    {
        return $this->cnt;
    }

    /**
     * Set promoproject.
     *
     * @param \TeamSoft\CrmRepositoryBundle\Entity\InfoPromoproject|null $promoproject
     *
     * @return InfoPromoprojectmaterial
     */
    public function setPromoproject(\TeamSoft\CrmRepositoryBundle\Entity\InfoPromoproject $promoproject = null)
    {
        $this->promoproject = $promoproject;

        return $this;
    }

    /**
     * Get promoproject.
     *
     * @return \TeamSoft\CrmRepositoryBundle\Entity\InfoPromoproject|null
     */
    public function getPromoproject()
    {
        return $this->promoproject;
    }

    /**
     * Set promomaterial.
     *
     * @param \TeamSoft\CrmRepositoryBundle\Entity\InfoPromomaterial|null $promomaterial
     *
     * @return InfoPromoprojectmaterial
     */
    public function setPromomaterial(\TeamSoft\CrmRepositoryBundle\Entity\InfoPromomaterial $promomaterial = null)
    {
        $this->promomaterial = $promomaterial;

        return $this;
    }

    /**
     * Get promomaterial.
     *
     * @return \TeamSoft\CrmRepositoryBundle\Entity\InfoPromomaterial|null
     */
    public function getPromomaterial()
    {
        return $this->promomaterial;
    }

    /**
     * Set promoprojectdetail.
     *
     * @param \TeamSoft\CrmRepositoryBundle\Entity\InfoPromoprojectdetail|null $promoprojectdetail
     *
     * @return InfoPromoprojectmaterial
     */
    public function setPromoprojectdetail(\TeamSoft\CrmRepositoryBundle\Entity\InfoPromoprojectdetail $promoprojectdetail = null)
    {
        $this->promoprojectdetail = $promoprojectdetail;

        return $this;
    }

    /**
     * Get promoprojectdetail.
     *
     * @return \TeamSoft\CrmRepositoryBundle\Entity\InfoPromoprojectdetail|null
     */
    public function getPromoprojectdetail()
    {
        return $this->promoprojectdetail;
    }

    /**
     * @return int|null
     */
    public function getPromomaterialId()
    {
        return $this->getPromomaterial() ? $this->getPromomaterial()->getId() : null;
    }
}
