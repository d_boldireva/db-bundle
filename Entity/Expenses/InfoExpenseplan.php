<?php

namespace TeamSoft\CrmRepositoryBundle\Entity\Expenses;

use Doctrine\ORM\Mapping as ORM;
use TeamSoft\CrmRepositoryBundle\Entity\ServiceFieldInterface;

/**
 * InfoExpenseplan
 *
 * @ORM\Table(name="info_expenseplan")
 * @ORM\Entity(repositoryClass="TeamSoft\CrmRepositoryBundle\Repository\Expenses\InfoExpenseplan")
 */
class InfoExpenseplan implements ServiceFieldInterface
{
    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="currenttime", type="datetime", nullable=true)
     */
    private $currenttime;

    /**
     * @var string|null
     *
     * @ORM\Column(name="moduser", type="string", length=16, nullable=true)
     */
    private $moduser;

    /**
     * @var string|null
     *
     * @ORM\Column(name="guid", type="guid", nullable=true)
     */
    private $guid;

    /**
     * @var float|null
     *
     * @ORM\Column(name="budget", type="float", precision=53, scale=0, nullable=true)
     */
    private $budget;

    /**
     * @var int|null
     *
     * @ORM\Column(name="user_id", type="integer", nullable=true)
     */
    private $userId;

    /**
     * @var int
     *
     * @ORM\Column(name="ID", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var int|null
     *
     * @ORM\Column(name="FY", type="integer", nullable=true)
     */
    private $fy;



    /**
     * Set currenttime.
     *
     * @param \DateTime|null $currenttime
     *
     * @return InfoExpenseplan
     */
    public function setCurrenttime(\DateTime $currenttime = null)
    {
        $this->currenttime = $currenttime;

        return $this;
    }

    /**
     * Get currenttime.
     *
     * @return \DateTime|null
     */
    public function getCurrenttime()
    {
        return $this->currenttime;
    }

    /**
     * Set moduser.
     *
     * @param string|null $moduser
     *
     * @return InfoExpenseplan
     */
    public function setModuser($moduser = null)
    {
        $this->moduser = $moduser;

        return $this;
    }

    /**
     * Get moduser.
     *
     * @return string|null
     */
    public function getModuser()
    {
        return $this->moduser;
    }

    /**
     * Set guid.
     *
     * @param string|null $guid
     *
     * @return InfoExpenseplan
     */
    public function setGuid($guid = null)
    {
        $this->guid = $guid;

        return $this;
    }

    /**
     * Get guid.
     *
     * @return string|null
     */
    public function getGuid()
    {
        return $this->guid;
    }

    /**
     * Set budget.
     *
     * @param float|null $budget
     *
     * @return InfoExpenseplan
     */
    public function setBudget($budget = null)
    {
        $this->budget = $budget;

        return $this;
    }

    /**
     * Get budget.
     *
     * @return float|null
     */
    public function getBudget()
    {
        return $this->budget;
    }

    /**
     * Set userId.
     *
     * @param int|null $userId
     *
     * @return InfoExpenseplan
     */
    public function setUserId($userId = null)
    {
        $this->userId = $userId;

        return $this;
    }

    /**
     * Get userId.
     *
     * @return int|null
     */
    public function getUserId()
    {
        return $this->userId;
    }

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set fy.
     *
     * @param int|null $fy
     *
     * @return InfoExpenseplan
     */
    public function setFy($fy = null)
    {
        $this->fy = $fy;

        return $this;
    }

    /**
     * Get fy.
     *
     * @return int|null
     */
    public function getFy()
    {
        return $this->fy;
    }
}
