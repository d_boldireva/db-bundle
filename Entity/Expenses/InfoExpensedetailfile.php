<?php

namespace TeamSoft\CrmRepositoryBundle\Entity\Expenses;

use Doctrine\ORM\Mapping as ORM;
use TeamSoft\CrmRepositoryBundle\Entity\ServiceFieldInterface;
use TeamSoft\CrmRepositoryBundle\Model\InfoFile;

/**
 * InfoExpensedetailfile
 *
 * @ORM\Table(name="info_expensedetailfile")
 * @ORM\Entity(repositoryClass="TeamSoft\CrmRepositoryBundle\Repository\Expenses\InfoExpensedetailfile")
 */
class InfoExpensedetailfile extends InfoFile implements ServiceFieldInterface
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="currenttime", type="datetime", nullable=true)
     */
    private $currenttime;

    /**
     * @var string|null
     *
     * @ORM\Column(name="moduser", type="string", length=16, nullable=true)
     */
    private $moduser;

    /**
     * @var string|null
     *
     * @ORM\Column(name="guid", type="guid", nullable=true)
     */
    private $guid;

    /**
     * @var string|null
     *
     * @ORM\Column(name="content", type="blob_or_string", nullable=true)
     */
    private $content;

    /**
     * @var string|null
     *
     * @ORM\Column(name="filetype", type="string", length=16, nullable=true)
     */
    private $filetype;

    /**
     * @var InfoExpensedetail
     *
     * @ORM\OneToOne (targetEntity="InfoExpensedetail", inversedBy="file")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="expensedetail_id", referencedColumnName="id")
     * })
     */
    private $expensedetail;



    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set currenttime.
     *
     * @param \DateTime|null $currenttime
     *
     * @return InfoExpensedetailfile
     */
    public function setCurrenttime(\DateTime $currenttime = null)
    {
        $this->currenttime = $currenttime;

        return $this;
    }

    /**
     * Get currenttime.
     *
     * @return \DateTime|null
     */
    public function getCurrenttime()
    {
        return $this->currenttime;
    }

    /**
     * Set moduser.
     *
     * @param string|null $moduser
     *
     * @return InfoExpensedetailfile
     */
    public function setModuser($moduser = null)
    {
        $this->moduser = $moduser;

        return $this;
    }

    /**
     * Get moduser.
     *
     * @return string|null
     */
    public function getModuser()
    {
        return $this->moduser;
    }

    /**
     * Set guid.
     *
     * @param string|null $guid
     *
     * @return InfoExpensedetailfile
     */
    public function setGuid($guid = null)
    {
        $this->guid = $guid;

        return $this;
    }

    /**
     * Get guid.
     *
     * @return string|null
     */
    public function getGuid()
    {
        return $this->guid;
    }

    /**
     * Set content.
     *
     * @param string|null $content
     *
     * @return InfoExpensedetailfile
     */
    public function setContent($content = null)
    {
        $this->content = $content;

        return $this;
    }

    /**
     * Get content.
     *
     * @return string|null
     */
    public function getContent()
    {
        return $this->content;
    }

    /**
     * Set filetype.
     *
     * @param string|null $filetype
     *
     * @return InfoExpensedetailfile
     */
    public function setFiletype(?string $filetype)
    {
        $this->filetype = $filetype;

        return $this;
    }

    /**
     * Get filetype.
     *
     * @return string|null
     */
    public function getFiletype(): ?string
    {
        return $this->filetype;
    }

    /**
     * Set expensedetail.
     *
     * @param \TeamSoft\ExpensesBundle\Entity\InfoExpensedetail|null $expensedetail
     *
     * @return InfoExpensedetailfile
     */
    public function setExpensedetail(InfoExpensedetail $expensedetail = null)
    {
        $this->expensedetail = $expensedetail;

        return $this;
    }

    /**
     * Get expensedetail.
     *
     * @return \TeamSoft\ExpensesBundle\Entity\InfoExpensedetail|null
     */
    public function getExpensedetail()
    {
        return $this->expensedetail;
    }

    public function getName()
    {
        return '';
    }
    
    public function setExtension(?string $type)
    {
        $this->setFiletype($type);
    }

    public function getExtension(): ?string
    {
        return $this->getFiletype();
    }
}
