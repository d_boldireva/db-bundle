<?php

namespace TeamSoft\CrmRepositoryBundle\Entity\Expenses;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use TeamSoft\CrmRepositoryBundle\Entity\InfoCustomdictionaryvalue;
use TeamSoft\CrmRepositoryBundle\Entity\ServiceFieldInterface;

/**
 * InfoExpensedetail
 *
 * @ORM\Table(name="info_expensedetail")
 * @ORM\Entity(repositoryClass="TeamSoft\CrmRepositoryBundle\Repository\Expenses\InfoExpensedetail")
 */
class InfoExpensedetail implements ServiceFieldInterface
{

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="currenttime", type="datetime", nullable=true)
     */
    private $currenttime;

    /**
     * @var string|null
     *
     * @ORM\Column(name="moduser", type="string", length=16, nullable=true)
     */
    private $moduser;

    /**
     * @var string|null
     *
     * @ORM\Column(name="guid", type="guid", nullable=true)
     */
    private $guid;

    /**
     * @var float|null
     *
     * @ORM\Column(name="amount", type="float", precision=53, scale=0, nullable=true)
     */
    private $amount;

    /**
     * @var string|null
     *
     * @ORM\Column(name="vendorname", type="string", length=255, nullable=true)
     */
    private $vendorname;

    /**
     * @var string|null
     *
     * @ORM\Column(name="comment", type="string", length=255, nullable=true)
     */
    private $comment;

    /**
     * @var string|null
     *
     * @ORM\Column(name="managers_comment", type="string", length=255, nullable=true)
     */
    private $managersComment;

    /**
     * @var InfoExpense
     *
     * @ORM\ManyToOne(targetEntity="InfoExpense", inversedBy="details")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="expense_id", referencedColumnName="id")
     * })
     */
    private $expense;

    /**
     * @var InfoExpensedetailfile|null
     *
     * @ORM\OneToOne(targetEntity="InfoExpensedetailfile", mappedBy="expensedetail", cascade={"persist"})
     */
    private $file;


    /**
     * @var \TeamSoft\CrmRepositoryBundle\Entity\InfoCustomdictionaryvalue
     *
     * @ORM\ManyToOne(targetEntity="\TeamSoft\CrmRepositoryBundle\Entity\InfoCustomdictionaryvalue")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="expensetype_id", referencedColumnName="id")
     * })
     */
    private $expenseType;

    /**
     * @ORM\Column(name="receipt_number", type="string", length=255, nullable=true)
     */
    private $receiptNumber;

    /**
     * @ORM\Column(name="receipt_dt", type="datetime", nullable=true)
     */
    private $receiptDt;

    /**
     * @var ArrayCollection
     * @ORM\OneToMany(targetEntity="InfoExpensehcp", mappedBy="expensedetail", cascade={"persist", "remove"})
     */
    private $expenseHcpCollection;

    public function __construct() {
        $this->expenseHcpCollection = new ArrayCollection();
    }

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set currenttime.
     *
     * @param \DateTime|null $currenttime
     *
     * @return InfoExpensedetail
     */
    public function setCurrenttime(\DateTime $currenttime = null)
    {
        $this->currenttime = $currenttime;

        return $this;
    }

    /**
     * Get currenttime.
     *
     * @return \DateTime|null
     */
    public function getCurrenttime()
    {
        return $this->currenttime;
    }

    /**
     * Set moduser.
     *
     * @param string|null $moduser
     *
     * @return InfoExpensedetail
     */
    public function setModuser($moduser = null)
    {
        $this->moduser = $moduser;

        return $this;
    }

    /**
     * Get moduser.
     *
     * @return string|null
     */
    public function getModuser()
    {
        return $this->moduser;
    }

    /**
     * Set guid.
     *
     * @param string|null $guid
     *
     * @return InfoExpensedetail
     */
    public function setGuid($guid = null)
    {
        $this->guid = $guid;

        return $this;
    }

    /**
     * Get guid.
     *
     * @return string|null
     */
    public function getGuid()
    {
        return $this->guid;
    }

    /**
     * Set amount.
     *
     * @param float|null $amount
     *
     * @return InfoExpensedetail
     */
    public function setAmount($amount = null)
    {
        $this->amount = $amount;

        return $this;
    }

    /**
     * Get amount.
     *
     * @return float|null
     */
    public function getAmount()
    {
        return $this->amount;
    }

    /**
     * Set vendorname.
     *
     * @param string|null $vendorname
     *
     * @return InfoExpensedetail
     */
    public function setVendorname($vendorname = null)
    {
        $this->vendorname = $vendorname;

        return $this;
    }

    /**
     * Get vendorname.
     *
     * @return string|null
     */
    public function getVendorname()
    {
        return $this->vendorname;
    }

    /**
     * Set comment.
     *
     * @param string|null $comment
     *
     * @return InfoExpensedetail
     */
    public function setComment($comment = null)
    {
        $this->comment = $comment;

        return $this;
    }

    /**
     * Get comment.
     *
     * @return string|null
     */
    public function getComment()
    {
        return $this->comment;
    }

    /**
     * Set managers comment.
     *
     * @param string|null $managersComment
     *
     * @return InfoExpensedetail
     */
    public function setManagersComment($managersComment = null)
    {
        $this->managersComment = $managersComment;

        return $this;
    }

    /**
     * Get managers comment.
     *
     * @return string|null
     */
    public function getManagersComment()
    {
        return $this->managersComment;
    }

    /**
     * @return int|null
     */
    public function getExpenseId(){
        return $this->expense ? $this->expense->getId() : null;
    }

    /**
     * Set expense.
     *
     * @param InfoExpense|null $expense
     *
     * @return InfoExpensedetail
     */
    public function setExpense(InfoExpense $expense = null)
    {
        $this->expense = $expense;

        return $this;
    }

    /**
     * Get expense.
     *
     * @return InfoExpense|null
     */
    public function getExpense()
    {
        return $this->expense;
    }

    public function getExpenseTypeId(): ?int
    {
        return $this->expenseType ? $this->expenseType->getId() : null;
    }

    public function getExpenseTypeCode(): ?string
    {
        return $this->expenseType ? $this->expenseType->getCode() : null;
    }

    public function setExpenseType(?InfoCustomdictionaryvalue $expenseType): self
    {
        $this->expenseType = $expenseType;

        return $this;
    }

    public function getExpenseType(): ?InfoCustomdictionaryvalue
    {
        return $this->expenseType;
    }

    /**
     * @return int|null
     */
    public function getFileId() {
        return $this->file ? $this->file->getId() : null;
    }

    /**
     * @return InfoExpensedetailfile|null
     */
    public function getFile(): ?InfoExpensedetailfile
    {
        return $this->file;
    }

    /**
     * @param InfoExpensedetailfile|null $file
     * @return self
     */
    public function setFile(?InfoExpensedetailfile $file): self
    {
        if ($file) {
            $file->setExpensedetail($this);
        }
        $this->file = $file;

        return $this;
    }

    public function getReceiptNumber(): ?string
    {
        return $this->receiptNumber;
    }

    public function setReceiptNumber(?string $receiptNumber): self
    {
        $this->receiptNumber = $receiptNumber;

        return $this;
    }

    public function getReceiptDt(): ?\DateTimeInterface
    {
        return $this->receiptDt;
    }

    public function setReceiptDt(?\DateTimeInterface $receiptDt): self
    {
        $this->receiptDt = $receiptDt;

        return $this;
    }

    public function getExpenseHcpCollection(): ArrayCollection
    {
        return $this->expenseHcpCollection;
    }

    public function addExpenseHcpCollection(InfoExpensehcp $expenseHcp): self
    {
        if(!$this->expenseHcpCollection->contains($expenseHcp)) {
            $this->expenseHcpCollection->add($expenseHcp);
            $expenseHcp->setExpensedetail($this);
        }

        return $this;
    }

    public function removeExpenseHcpCollection(InfoExpensehcp $expenseHcp): self
    {
        if($this->expenseHcpCollection->contains($expenseHcp)) {
            $this->expenseHcpCollection->remove($expenseHcp);
            $expenseHcp->setExpensedetail(null);
        }

        return $this;
    }


}
