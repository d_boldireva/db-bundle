<?php

namespace TeamSoft\CrmRepositoryBundle\Entity\Expenses;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use TeamSoft\CrmRepositoryBundle\Entity\InfoTask;
use TeamSoft\CrmRepositoryBundle\Entity\InfoUser;
use TeamSoft\CrmRepositoryBundle\Entity\ServiceFieldInterface;
use TeamSoft\CrmRepositoryBundle\Entity\InfoCustomdictionaryvalue;
use TeamSoft\CrmRepositoryBundle\Repository\Expenses\InfoExpenseRepository;

/**
 * InfoExpense
 *
 * @ORM\Table(name="info_expense")
 * @ORM\Entity(repositoryClass=InfoExpenseRepository::class)
 */
class InfoExpense implements ServiceFieldInterface
{
    public const STATUS_APPROVED = 1;
    public const STATUS_REJECTED = 2;
    public const STATUS_RETURNED = 3;
    public const STATUS_EDITABLE = 0;

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="currenttime", type="datetime", nullable=true)
     */
    private $currenttime;

    /**
     * @var string|null
     *
     * @ORM\Column(name="moduser", type="string", length=16, nullable=true)
     */
    private $moduser;

    /**
     * @var string|null
     *
     * @ORM\Column(name="guid", type="guid", nullable=true)
     */
    private $guid;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="dt", type="datetime", nullable=true)
     */
    private $dt;

    /**
     * @var int|null
     *
     * @ORM\Column(name="status", type="integer", nullable=true)
     */
    private $status;

    /**
     * @var int|null
     *
     * @ORM\Column(name="claimid", type="integer", nullable=true)
     */
    private $claimid;

    /**
     * @var int|null
     *
     * @ORM\Column(name="iscountrytrip", type="integer", nullable=true)
     */
    private $iscountrytrip;

    /**
     * @var int|null
     *
     * @ORM\Column(name="line2status", type="integer", nullable=true)
     */
    private $line2status;

    /**
     * @var InfoUser
     *
     * @ORM\ManyToOne(targetEntity="\TeamSoft\CrmRepositoryBundle\Entity\InfoUser")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     * })
     */
    private $user;

    /**
     * @var string|null
     *
     * @ORM\Column(name="line2comment", type="string", length=255, nullable=true)
     */
    private $line2comment;

    /**
     * @var string|null
     *
     * @ORM\Column(name="moduser_line1", type="string", length=16, nullable=true)
     */
    private $moduserLine1;

    /**
     * @var string|null
     *
     * @ORM\Column(name="modified_line1", type="datetime", nullable=true)
     */
    private $modifiedLine1;

    /**
     * @var string|null
     *
     * @ORM\Column(name="moduser_line2", type="string", length=16, nullable=true)
     */
    private $moduserLine2;

    /**
     * @var string|null
     *
     * @ORM\Column(name="modified_line2", type="datetime", nullable=true)
     */
    private $modifiedLine2;

    /**
     * @var int|null
     *
     * @ORM\Column(name="line_pm_status", type="integer", nullable=true)
     */
    private $linePMstatus;

    /**
     * @var string
     *
     * @ORM\Column(name="line_pm_comment", type="string", length=255, nullable=true)
     */
    private $linePMcomment;

    /**
     * @var string|null
     *
     * @ORM\Column(name="moduser_line_pm", type="string", length=16, nullable=true)
     */
    private $moduserLinePM;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="modified_line_pm", type="datetime", nullable=true)
     */
    private $modifiedLinePM;

    /**
     * @ORM\OneToMany(targetEntity="InfoExpensedetail", mappedBy="expense", cascade={"persist"})
     */
    private $details;

    /**
     * @var InfoTask
     *
     * @ORM\ManyToOne(targetEntity="\TeamSoft\CrmRepositoryBundle\Entity\InfoTask", inversedBy="expenseCollection")
     * @ORM\JoinColumn(name="task_id", referencedColumnName="id")
     */
    private $task;

    /**
     * @var \TeamSoft\CrmRepositoryBundle\Entity\InfoCustomdictionaryvalue
     *
     * @ORM\ManyToOne(targetEntity="\TeamSoft\CrmRepositoryBundle\Entity\InfoCustomdictionaryvalue")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="expensetype_id", referencedColumnName="id")
     * })
     */
    private $expenseType;

    public function __construct()
    {
        $this->details = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * Set currenttime.
     *
     * @param \DateTime|null $currenttime
     *
     * @return InfoExpense
     */
    public function setCurrenttime(\DateTime $currenttime = null)
    {
        $this->currenttime = $currenttime;

        return $this;
    }

    /**
     * Get currenttime.
     *
     * @return \DateTime|null
     */
    public function getCurrenttime()
    {
        return $this->currenttime;
    }

    /**
     * Set moduser.
     *
     * @param string|null $moduser
     *
     * @return InfoExpense
     */
    public function setModuser($moduser = null)
    {
        $this->moduser = $moduser;

        return $this;
    }

    /**
     * Get moduser.
     *
     * @return string|null
     */
    public function getModuser()
    {
        return $this->moduser;
    }

    /**
     * Set guid.
     *
     * @param string|null $guid
     *
     * @return InfoExpense
     */
    public function setGuid($guid = null)
    {
        $this->guid = $guid;

        return $this;
    }

    /**
     * Get guid.
     *
     * @return string|null
     */
    public function getGuid()
    {
        return $this->guid;
    }

    public function setDt(?\DateTime $dt): self
    {
        $this->dt = $dt;

        return $this;
    }

    public function getDt(): ?\DateTime
    {
        return $this->dt;
    }

    public function setStatus(?int $status): self
    {
        $this->status = $status;

        return $this;
    }

    public function getStatus(): ?int
    {
        return $this->status;
    }

    public function setClaimid(?int $claimid): self
    {
        $this->claimid = $claimid;

        return $this;
    }

    public function getClaimid(): ?int
    {
        return $this->claimid;
    }

    public function setIscountrytrip(?int $iscountrytrip): self
    {
        $this->iscountrytrip = $iscountrytrip;

        return $this;
    }

    public function getIscountrytrip(): ?int
    {
        return $this->iscountrytrip;
    }

    public function setLine2status(?int $line2status): self
    {
        $this->line2status = $line2status;

        return $this;
    }

    public function getLine2status(): ?int
    {
        return $this->line2status;
    }

    public function setUser(?InfoUser $user): self
    {
        $this->user = $user;

        return $this;
    }

    public function getUser(): ?InfoUser
    {
        return $this->user;
    }

    public function getUserId(): ?int
    {
        return $this->user ? $this->user->getId() : null;
    }

    public function setLine2comment(?string $line2comment): self
    {
        $this->line2comment = $line2comment;

        return $this;
    }

    public function getLine2comment(): ?string
    {
        return $this->line2comment;
    }

    public function setModuserLine1(?string $moduserLine1): self
    {
        $this->moduserLine1 = $moduserLine1;

        return $this;
    }

    public function getModuserLine1(): ?string
    {
        return $this->moduserLine1;
    }

    public function setModifiedLine1(?\DateTime $modifiedLine1): self
    {
        $this->modifiedLine1 = $modifiedLine1;

        return $this;
    }

    public function getModifiedLine1(): ?\DateTime
    {
        return $this->modifiedLine1;
    }

    public function setModuserLine2(?string $moduserLine2): self
    {
        $this->moduserLine2 = $moduserLine2;

        return $this;
    }

    public function getModuserLine2(): ?string
    {
        return $this->moduserLine2;
    }

    public function setModifiedLine2(?\DateTime $modifiedLine2): self
    {
        $this->modifiedLine2 = $modifiedLine2;

        return $this;
    }

    public function getModifiedLine2(): ?\DateTime
    {
        return $this->modifiedLine2;
    }

    public function getDetails()
    {
        return $this->details;
    }

    public function addDetail(InfoExpensedetail $expenseDetail): self
    {
        $expenseDetail->setExpense($this);
        if (!$this->details->contains($expenseDetail)) {
            $this->details->add($expenseDetail);
            $expenseDetail->setExpense($this);
        }

        return $this;
    }

    public function removeDetail(InfoExpensedetail $expenseDetail): self
    {
        if ($this->details->contains($expenseDetail)) {
            $expenseDetail->setExpense(null);
            $this->details->removeElement($expenseDetail);
        }

        return $this;
    }

    public function isEditable(): bool
    {
        return ((int)$this->getStatus()) === self::STATUS_EDITABLE;
    }

    public function isApproved(): bool
    {
        return ((int)$this->getStatus()) === self::STATUS_APPROVED;
    }

    public function setLinePMStatus(?int $linePMstatus): self
    {
        $this->linePMstatus = $linePMstatus;

        return $this;
    }

    public function getLinePMStatus(): ?int
    {
        return $this->linePMstatus;
    }

    public function setLinePMComment(?string $comment): self
    {
        $this->linePMcomment = $comment;

        return $this;
    }

    public function getLinePMComment(): ?string
    {
        return $this->linePMcomment;
    }

    public function setModuserLinePM(?string $moduser): ?string
    {
        $this->moduserLinePM = $moduser;

        return $this;
    }

    public function getModuserLinePM(): ?string
    {
        return $this->moduserLinePM;
    }

    public function setModifiedLinePM(\DateTime $dateTime): self
    {
        $this->modifiedLinePM = $dateTime;

        return $this;
    }

    public function getModifiedLinePM(): ?\DateTime
    {
        return $this->modifiedLinePM;
    }

    public function getTaskId(): ?int
    {
        return $this->task ? $this->task->getId() : null;
    }

    public function getTask(): ?InfoTask
    {
        return $this->task;
    }

    public function setTask(InfoTask $task): self
    {
        $this->task = $task;

        return $this;
    }

    public function getExpenseTypeId(): ?int
    {
        return $this->expenseType ? $this->expenseType->getId() : null;
    }

    public function getExpenseTypeCode(): ?string
    {
        return $this->expenseType ? $this->expenseType->getCode() : null;
    }

    public function setExpenseType(?InfoCustomdictionaryvalue $expenseType): self
    {
        $this->expenseType = $expenseType;

        return $this;
    }

    public function getExpenseType(): ?InfoCustomdictionaryvalue
    {
        return $this->expenseType;
    }
}
