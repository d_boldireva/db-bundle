<?php
declare(strict_types=1);


namespace TeamSoft\CrmRepositoryBundle\Entity\Expenses;

use Doctrine\ORM\Mapping as ORM;
use TeamSoft\CrmRepositoryBundle\Entity\InfoContact;

/**
 * InfoExpensehcp
 *
 * @ORM\Table(name="info_expensehcp")
 * @ORM\Entity(repositoryClass="TeamSoft\CrmRepositoryBundle\Repository\Expenses\InfoExpensehcpRepository")
 */
class InfoExpensehcp
{
    /**
     * @var int
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var InfoExpensedetail|null
     * @ORM\ManyToOne(targetEntity="InfoExpensedetail", inversedBy="hcp")
     * @ORM\JoinColumn(name="expensedetail_id")
     */
    private $expenseDetail;

    /**
     * @var InfoContact|null
     * @ORM\ManyToOne(targetEntity="\TeamSoft\CrmRepositoryBundle\Entity\InfoContact")
     * @ORM\JoinColumn(name="contact_id")
     */
    private $contact;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="currenttime", type="datetime", nullable=true)
     */
    private $currenttime;

    /**
     * @var string|null
     *
     * @ORM\Column(name="moduser", type="string", length=16, nullable=true)
     */
    private $moduser;

    /**
     * @var string|null
     *
     * @ORM\Column(name="guid", type="guid", nullable=true)
     */
    private $guid;

    public function getId(): int
    {
        return $this->id;
    }

    public function getExpenseDetail(): ?InfoExpensedetail
    {
        return $this->expenseDetail;
    }

    public function setExpenseDetail(?InfoExpensedetail $expenseDetail): self
    {
        $this->expenseDetail = $expenseDetail;
        return $this;
    }

    public function getContact(): ?InfoContact
    {
        return $this->contact;
    }

    public function setContact(?InfoContact $contact): self
    {
        $this->contact = $contact;
        return $this;
    }

    public function getCurrenttime(): ?\DateTime
    {
        return $this->currenttime;
    }

    public function setCurrenttime(?\DateTime $currenttime): self
    {
        $this->currenttime = $currenttime;
        return $this;
    }

    public function getModuser(): ?string
    {
        return $this->moduser;
    }

    public function setModuser(?string $moduser): self
    {
        $this->moduser = $moduser;
        return $this;
    }

    public function getGuid(): ?string
    {
        return $this->guid;
    }

    public function setGuid(?string $guid): self
    {
        $this->guid = $guid;
        return $this;
    }
}
