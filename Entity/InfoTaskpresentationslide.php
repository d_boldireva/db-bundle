<?php

namespace TeamSoft\CrmRepositoryBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use TeamSoft\CrmRepositoryBundle\Entity\ServiceFieldInterface;

/**
 * InfoTaskpresentationslide
 *
 * @ORM\Table(name="info_taskpresentationslide")
 * @ORM\Entity(repositoryClass="TeamSoft\CrmRepositoryBundle\Repository\InfoTaskpresentationslide")
 */
class InfoTaskpresentationslide implements ServiceFieldInterface
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var int
     */
    private $slidenum;

    /**
     * @var int
     */
    private $numofsec;

    /**
     * @var string
     */
    private $guid;

    /**
     * @var \DateTime
     */
    private $currenttime;

    /**
     * @var string
     */
    private $moduser;

    /**
     * @var InfoClm
     *
     * @ORM\ManyToOne(targetEntity="InfoClm", inversedBy="taskPresentationSlides", cascade={"persist"})
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="presentation_id", referencedColumnName="id")
     * })
     */
    private $presentation;

    /**
     * @var \TeamSoft\CrmRepositoryBundle\Entity\InfoTask
     *
     * @ORM\ManyToOne(targetEntity="\TeamSoft\CrmRepositoryBundle\Entity\InfoTask", cascade={"persist", "remove"})
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="task_id", referencedColumnName="id")
     * })
     */
    private $task;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set slidenum
     *
     * @param int $slidenum
     *
     * @return InfoTaskpresentationslide
     */
    public function setSlidenum($slidenum)
    {
        $this->slidenum = $slidenum;

        return $this;
    }

    /**
     * Get slidenum
     *
     * @return int
     */
    public function getSlidenum()
    {
        return $this->slidenum;
    }

    /**
     * Set numofsec
     *
     * @param int $numofsec
     *
     * @return InfoTaskpresentationslide
     */
    public function setNumofsec($numofsec)
    {
        $this->numofsec = $numofsec;

        return $this;
    }

    /**
     * Get numofsec
     *
     * @return int
     */
    public function getNumofsec()
    {
        return $this->numofsec;
    }

    /**
     * Set guid
     *
     * @param string $guid
     *
     * @return InfoTaskpresentationslide
     */
    public function setGuid($guid)
    {
        $this->guid = $guid;

        return $this;
    }

    /**
     * Get guid
     *
     * @return string
     */
    public function getGuid()
    {
        return $this->guid;
    }

    /**
     * Set currenttime
     *
     * @param \DateTime $currenttime
     *
     * @return InfoTaskpresentationslide
     */
    public function setCurrenttime(\DateTime $currenttime)
    {
        $this->currenttime = $currenttime;

        return $this;
    }

    /**
     * Get currenttime
     *
     * @return \DateTime
     */
    public function getCurrenttime()
    {
        return $this->currenttime;
    }

    /**
     * Set moduser
     *
     * @param string $moduser
     *
     * @return InfoTaskpresentationslide
     */
    public function setModuser($moduser)
    {
        $this->moduser = $moduser;

        return $this;
    }

    /**
     * Get moduser
     *
     * @return string
     */
    public function getModuser()
    {
        return $this->moduser;
    }

    /**
     * Set presentation
     *
     * @param \TeamSoft\CrmRepositoryBundle\Entity\InfoClm $presentation
     *
     * @return InfoTaskpresentationslide
     */
    public function setPresentation(\TeamSoft\CrmRepositoryBundle\Entity\InfoClm $presentation = null)
    {
        $this->presentation = $presentation;

        return $this;
    }

    /**
     * Get presentation
     *
     * @return \TeamSoft\CrmRepositoryBundle\Entity\InfoClm
     */
    public function getPresentation()
    {
        return $this->presentation;
    }

    /**
     * Set task
     *
     * @param \TeamSoft\CrmRepositoryBundle\Entity\InfoTask $task
     *
     * @return InfoTaskpresentationslide
     */
    public function setTask(\TeamSoft\CrmRepositoryBundle\Entity\InfoTask $task = null)
    {
        $this->task = $task;

        return $this;
    }

    /**
     * Get task
     *
     * @return \TeamSoft\CrmRepositoryBundle\Entity\InfoTask
     */
    public function getTask()
    {
        return $this->task;
    }
}
