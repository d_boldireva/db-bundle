<?php

namespace TeamSoft\CrmRepositoryBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * InfoTargetspec
 *
 * @ORM\Table(name="info_targetspec", indexes={@ORM\Index(name="ix_info_targetspec_guid", columns={"guid"}), @ORM\Index(name="ix_info_targetspec_time", columns={"time"}), @ORM\Index(name="ix_info_targetspec_spec_id", columns={"spec_id"}), @ORM\Index(name="ix_info_targetspec_target_id", columns={"target_id"})})
 * @ORM\Entity
 */
class InfoTargetspec
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var guid
     *
     * @ORM\Column(name="guid", type="guid", nullable=true)
     */
    private $guid;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="currenttime", type="datetime", nullable=true)
     */
    private $currenttime;

    /**
     * @var string
     *
     * @ORM\Column(name="moduser", type="string", length=16, nullable=true)
     */
    private $moduser;

    /**
     * @var InfoTarget
     *
     * @ORM\ManyToOne(targetEntity="InfoTarget")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="target_id", referencedColumnName="id")
     * })
     */
    private $target;

    /**
     * @var InfoDictionary
     *
     * @ORM\ManyToOne(targetEntity="InfoDictionary")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="spec_id", referencedColumnName="id")
     * })
     */
    private $spec;



    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set guid
     *
     * @param guid $guid
     *
     * @return InfoTargetspec
     */
    public function setGuid($guid)
    {
        $this->guid = $guid;
    
        return $this;
    }

    /**
     * Get guid
     *
     * @return guid
     */
    public function getGuid()
    {
        return $this->guid;
    }

    /**
     * Set currenttime
     *
     * @param \DateTime $currenttime
     *
     * @return InfoTargetspec
     */
    public function setCurrenttime($currenttime)
    {
        $this->currenttime = $currenttime;
    
        return $this;
    }

    /**
     * Get currenttime
     *
     * @return \DateTime
     */
    public function getCurrenttime()
    {
        return $this->currenttime;
    }

    /**
     * Set moduser
     *
     * @param string $moduser
     *
     * @return InfoTargetspec
     */
    public function setModuser($moduser)
    {
        $this->moduser = $moduser;
    
        return $this;
    }

    /**
     * Get moduser
     *
     * @return string
     */
    public function getModuser()
    {
        return $this->moduser;
    }

    /**
     * Set target
     *
     * @param \TeamSoft\CrmRepositoryBundle\Entity\InfoTarget $target
     *
     * @return InfoTargetspec
     */
    public function setTarget(\TeamSoft\CrmRepositoryBundle\Entity\InfoTarget $target = null)
    {
        $this->target = $target;
    
        return $this;
    }

    /**
     * Get target
     *
     * @return \TeamSoft\CrmRepositoryBundle\Entity\InfoTarget
     */
    public function getTarget()
    {
        return $this->target;
    }

    /**
     * Set spec
     *
     * @param \TeamSoft\CrmRepositoryBundle\Entity\InfoDictionary $spec
     *
     * @return InfoTargetspec
     */
    public function setSpec(\TeamSoft\CrmRepositoryBundle\Entity\InfoDictionary $spec = null)
    {
        $this->spec = $spec;
    
        return $this;
    }

    /**
     * Get spec
     *
     * @return \TeamSoft\CrmRepositoryBundle\Entity\InfoDictionary
     */
    public function getSpec()
    {
        return $this->spec;
    }
}
