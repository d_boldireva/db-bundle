<?php

namespace TeamSoft\CrmRepositoryBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * InfoMcmconference
 *
 * @ORM\Table(name="info_mcmconference")
 * @ORM\Entity(repositoryClass="TeamSoft\CrmRepositoryBundle\Repository\InfoMcmconference")
 */
class InfoMcmconference implements ServiceFieldInterface, DateTimeWithGmtOffsetInterface
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255, nullable=true)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="code", type="string", length=255, nullable=true)
     */
    private $code;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="datefrom", type="datetime", nullable=true)
     */
    private $datefrom;

    /**
     * @var string
     *
     * @ORM\Column(name="gmt_offset", type="integer", nullable=true)
     */
    private $gmtOffset;

    /**
     * @var string
     *
     * @ORM\Column(name="url", type="string", nullable=true)
     */
    private $url;

    /**
     * @var string
     *
     * @ORM\Column(name="room", type="string", nullable=true)
     */
    private $room;

    /**
     * @var string
     *
     * @ORM\Column(name="pdf", type="string", nullable=true)
     */
    private $pdf;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="currenttime", type="datetime", nullable=true)
     */
    private $currenttime;

    /**
     * @var string
     *
     * @ORM\Column(name="moduser", type="string", length=16, nullable=true)
     */
    private $moduser;

    /**
     * @var string
     *
     * @ORM\Column(name="guid", type="guid", nullable=true)
     */
    private $guid;

    /**
     * @ORM\ManyToMany(targetEntity="InfoUser")
     * @ORM\JoinTable(name="info_mcmconferenceresponsible",
     *   joinColumns={@ORM\JoinColumn(name="conference_id", referencedColumnName="id")},
     *   inverseJoinColumns={@ORM\JoinColumn(name="user_id", referencedColumnName="id")}
     * )
     */
    private $responsibles;

    /**
     * @ORM\ManyToMany(targetEntity="InfoCustomdictionaryvalue")
     * @ORM\JoinTable(name="info_mcmconferencerole",
     *   joinColumns={@ORM\JoinColumn(name="conference_id", referencedColumnName="id")},
     *   inverseJoinColumns={@ORM\JoinColumn(name="role_id", referencedColumnName="id")}
     * )
     */
    private $roles;

    /**
     * @var ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="InfoMcmconferencecontact", mappedBy="conference", cascade={"persist"})
     */
    private $contacts;

    /**
     * @var ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="InfoMcmconferencevideo", mappedBy="conference", cascade={"persist"})
     */
    private $videos;

    /**
     * @var ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="InfoMcmconferencedetail", mappedBy="conference", cascade={"persist"})
     */
    private $details;

    /**
     * @var ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="InfoMcmconferencepackage", mappedBy="conference", cascade={"persist"})
     */
    private $packages;

    /**
     * @var \InfoCustomdictionaryvalue
     *
     * @ORM\ManyToOne(targetEntity="InfoCustomdictionaryvalue")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="group_id", referencedColumnName="id")
     * })
     */
    private $group;

    /**
     * @var ArrayCollection
     *
     * @ORM\ManyToMany(targetEntity="InfoCustomdictionaryvalue")
     * @ORM\JoinTable(name="info_mcmconferenceformat",
     *   joinColumns={@ORM\JoinColumn(name="conference_id", referencedColumnName="id")},
     *   inverseJoinColumns={@ORM\JoinColumn(name="format_id", referencedColumnName="id")}
     * )
     */
    private $formats;

    /**
     * @var \TeamSoft\CrmRepositoryBundle\Entity\InfoUser
     *
     * @ORM\ManyToOne(targetEntity="\TeamSoft\CrmRepositoryBundle\Entity\InfoUser")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="owner_id", referencedColumnName="id")
     * })
     */
    private $owner;

    /**
     * @var string
     *
     * @ORM\Column(name="url_ua", type="string", nullable=true)
     */
    private $urlUa;

    /**
     * @var string
     *
     * @ORM\Column(name="url_en", type="string", nullable=true)
     */
    private $urlEn;

    public function __construct()
    {
        $this->responsibles = new ArrayCollection();
        $this->roles = new ArrayCollection();
        $this->contacts = new ArrayCollection();
        $this->videos = new ArrayCollection();
        $this->formats = new ArrayCollection();
        $this->details = new ArrayCollection();
        $this->packages = new ArrayCollection();
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return InfoMcmconference
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set code
     *
     * @param string $code
     *
     * @return InfoMcmconference
     */
    public function setCode($code)
    {
        $this->code = $code;

        return $this;
    }

    /**
     * Get code
     *
     * @return string
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * set datefrom
     *
     * @param \DateTime $datefrom
     *
     * @return InfoMcmconference
     */
    public function setDatefrom(\DateTime $datefrom = null)
    {
        $this->datefrom = $datefrom;

        return $this;
    }

    /**
     * Get datefrom
     *
     * @return \DateTime
     */
    public function getDatefrom()
    {
        return $this->datefrom;
    }

    /**
     * Set gmtOffset
     *
     * @param integer $gmtOffset
     *
     * @return InfoMcmconference
     */
    public function setGmtOffset($gmtOffset)
    {
        $this->gmtOffset = $gmtOffset;

        return $this;
    }

    /**
     * Get gmt
     *
     * @return integer
     */
    public function getGmtOffset()
    {
        return $this->gmtOffset;
    }

    /**
     * Set url
     *
     * @param string|null $url
     *
     * @return InfoMcmconference
     */
    public function setUrl(string $url = null)
    {
        $this->url = $url;

        return $this;
    }

    /**
     * Get url
     *
     * @return string
     */
    public function getUrl()
    {
        return $this->url;
    }

    /**
     * Set room
     *
     * @param string $room
     *
     * @return InfoMcmconference
     */
    public function setRoom($room)
    {
        $this->room = $room;

        return $this;
    }

    /**
     * Get room
     *
     * @return string
     */
    public function getRoom()
    {
        return $this->room;
    }

    /**
     * Set pdf
     *
     * @param string $pdf
     *
     * @return InfoMcmconference
     */
    public function setPdf($pdf)
    {
        $this->pdf = $pdf;

        return $this;
    }

    /**
     * Get pdf
     *
     * @return string
     */
    public function getPdf()
    {
        return $this->pdf;
    }

    /**
     * Set group
     *
     * @param InfoCustomdictionaryvalue $group
     *
     * @return InfoMcmconference
     */
    public function setGroup($group)
    {
        $this->group = $group;

        return $this;
    }

    /**
     * Get group
     *
     * @return InfoCustomdictionaryvalue
     */
    public function getGroup()
    {
        return $this->group;
    }

    /**
     * Set currenttime
     *
     * @param \DateTime $currenttime
     *
     * @return InfoMcmconference
     */
    public function setCurrenttime(\DateTime $currenttime)
    {
        $this->currenttime = $currenttime;

        return $this;
    }

    /**
     * Get currenttime
     *
     * @return \DateTime
     */
    public function getCurrenttime()
    {
        return $this->currenttime;
    }

    /**
     * Set moduser
     *
     * @param string $moduser
     *
     * @return InfoMcmconference
     */
    public function setModuser($moduser)
    {
        $this->moduser = $moduser;

        return $this;
    }

    /**
     * Get moduser
     *
     * @return string
     */
    public function getModuser()
    {
        return $this->moduser;
    }

    /**
     * Set guid
     *
     * @param string $guid
     *
     * @return InfoMcmconference
     */
    public function setGuid($guid)
    {
        $this->guid = $guid;

        return $this;
    }

    /**
     * Get guid
     *
     * @return string
     */
    public function getGuid()
    {
        return $this->guid;
    }

    /**
     * Add responsible
     *
     * @param InfoUser $responsible
     *
     * @return InfoMcmconference
     */
    public function addResponsible(InfoUser $responsible)
    {
        $this->responsibles->add($responsible);

        return $this;
    }

    /**
     * Remove responsible
     *
     * @param InfoUser $responsible
     */
    public function removeResponsible(InfoUser $responsible)
    {
        $this->responsibles->removeElement($responsible);
    }

    /**
     * Get responsibles
     *
     * @return ArrayCollection
     */
    public function getResponsibles()
    {
        return $this->responsibles;
    }

    /**
     * Add role
     *
     * @param InfoCustomdictionaryvalue $role
     *
     * @return InfoMcmconference
     */
    public function addRole(InfoCustomdictionaryvalue $role)
    {
        $this->roles->add($role);

        return $this;
    }

    /**
     * Remove role
     *
     * @param InfoCustomdictionaryvalue $role
     */
    public function removeRole(InfoCustomdictionaryvalue $role)
    {
        $this->roles->removeElement($role);
    }

    /**
     * Get roles
     *
     * @return ArrayCollection
     */
    public function getRoles()
    {
        return $this->roles;
    }

    /**
     * Add video
     *
     * @param InfoMcmconferencevideo $video
     *
     * @return InfoMcmconference
     */
    public function addVideo(InfoMcmconferencevideo $video)
    {
        $this->videos->add($video);
        $video->setConference($this);

        return $this;
    }

    /**
     * Remove video
     *
     * @param InfoMcmconferencevideo $video
     */
    public function removeVideo(InfoMcmconferencevideo $video)
    {
        $this->videos->removeElement($video);
        $video->setConference(null);
    }

    /**
     * Get videos
     *
     * @return ArrayCollection
     */
    public function getVideos()
    {
        return $this->videos;
    }

    /**
     * Add contact
     *
     * @param InfoMcmconferencecontact $contact
     *
     * @return InfoMcmconference
     */
    public function addContact(InfoMcmconferencecontact $contact)
    {
        $this->contacts->add($contact);
        $contact->setConference($this);

        return $this;
    }

    /**
     * Remove contact
     *
     * @param InfoMcmconferencecontact $contact
     */
    public function removeContact(InfoMcmconferencecontact $contact)
    {
        $this->contacts->removeElement($contact);
    }

    /**
     * Get contacts
     *
     * @return ArrayCollection
     */
    public function getContacts()
    {
        return $this->contacts;
    }

    /**
     * Add format
     *
     * @param InfoCustomdictionaryvalue $format
     *
     * @return InfoMcmconference
     */
    public function addFormat(InfoCustomdictionaryvalue $format)
    {
        $this->formats->add($format);

        return $this;
    }

    /**
     * Remove format
     *
     * @param InfoCustomdictionaryvalue $format
     */
    public function removeFormat(InfoCustomdictionaryvalue $format)
    {
        $this->formats->removeElement($format);
    }

    /**
     * Get formats
     *
     * @return ArrayCollection
     */
    public function getFormats()
    {
        return $this->formats;
    }

    /**
     * Add detail
     *
     * @param InfoMcmconferencedetail $detail
     *
     * @return InfoMcmconference
     */
    public function addDetail(InfoMcmconferencedetail $detail)
    {
        $this->details->add($detail);
        $detail->setConference($this);

        return $this;
    }

    /**
     * Remove detail
     *
     * @param InfoMcmconferencedetail $detail
     */
    public function removeDetail(InfoMcmconferencedetail $detail)
    {
        $this->details->removeElement($detail);
        $detail->setConference(null);
    }

    /**
     * Get details
     *
     * @return ArrayCollection
     */
    public function getDetails()
    {
        return $this->details;
    }

    /**
     * Add package
     *
     * @param InfoMcmconferencepackage $package
     *
     * @return InfoMcmconference
     */
    public function addPackage(InfoMcmconferencepackage $package)
    {
        $this->packages->add($package);
        $package->setConference($this);

        return $this;
    }

    /**
     * Remove package
     *
     * @param InfoMcmconferencepackage $package
     */
    public function removePackage(InfoMcmconferencepackage $package)
    {
        $this->packages->removeElement($package);
    }

    /**
     * Get packages
     *
     * @return ArrayCollection
     */
    public function getPackages()
    {
        return $this->packages;
    }

    /**
     * Set owner
     *
     * @param \TeamSoft\CrmRepositoryBundle\Entity\InfoUser $owner
     *
     * @return InfoMcmconference
     */
    public function setOwner(\TeamSoft\CrmRepositoryBundle\Entity\InfoUser $owner)
    {
        $this->owner = $owner;

        return $this;
    }

    /**
     * Get owner
     *
     * @return \TeamSoft\CrmRepositoryBundle\Entity\InfoUser
     */
    public function getOwner()
    {
        return $this->owner;
    }

    public function getDateTimeWithGmtOffsetPropertyNameList()
    {
        return ['datefrom'];
    }

    /**
     * @return string
     */
    public function getUrlUa()
    {
        return $this->urlUa;
    }

    /**
     * @param string|null $urlUa
     */
    public function setUrlUa(string $urlUa = null)
    {
        $this->urlUa = $urlUa;
        return $this;
    }

    /**
     * @return string
     */
    public function getUrlEn()
    {
        return $this->urlEn;
    }

    /**
     * @param string|null $urlEn
     */
    public function setUrlEn(string $urlEn = null)
    {
        $this->urlEn = $urlEn;
        return $this;
    }
}
