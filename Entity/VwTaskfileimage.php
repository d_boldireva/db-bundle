<?php

namespace TeamSoft\CrmRepositoryBundle\Entity;


use Doctrine\ORM\Mapping as ORM;
use TeamSoft\CrmRepositoryBundle\Entity\InfoTaskfile;
use TeamSoft\CrmRepositoryBundle\Entity\ServiceFieldInterface;
use TeamSoft\CrmRepositoryBundle\Model\InfoFile;

/**
 * Class VwTaskfile
 * @package TeamSoft\CrmRepositoryBundle\Entity
 *
 * @ORM\Table(name="vw_task_file_image")
 * @ORM\Entity(repositoryClass="TeamSoft\CrmRepositoryBundle\Repository\VwTaskfileimage")
 */
class VwTaskfileimage extends InfoFile implements ServiceFieldInterface{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255, nullable=true)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="content", type="blob_or_string", nullable=true)
     */
    private $content;

    /**
     * @var integer
     *
     * @ORM\Column(name="fsize", type="integer", nullable=true)
     */
    private $fsize;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="fmodified", type="datetime", nullable=true)
     */
    private $fmodified;

    /**
     * @var guid
     *
     * @ORM\Column(name="guid", type="guid", nullable=true)
     */
    private $guid;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="currenttime", type="datetime", nullable=true)
     */
    private $currenttime;

    /**
     * @var string
     *
     * @ORM\Column(name="moduser", type="string", length=16, nullable=true)
     */
    private $moduser;

    /**
     * @var \TeamSoft\CrmRepositoryBundle\Entity\InfoTaskfilegroup
     *
     * @ORM\ManyToOne(targetEntity="InfoTaskfilegroup")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="subj_id", referencedColumnName="id")
     * })
     */
    protected $subj;

    /**
     * @var int
     *
     * @ORM\Column(name="photostate", type="integer", nullable=true)
     */
    private $photostate;

    /**
     * @var InfoCustomdictionaryvalue
     *
     * @ORM\OneToOne(targetEntity="InfoCustomdictionaryvalue")
     * @ORM\JoinColumn(name="whatsinside", referencedColumnName="id")
     */
    private $whatsinside;

    /**
     * @var InfoPreparationbrend
     *
     * @ORM\OneToOne(targetEntity="InfoPreparationbrend")
     * @ORM\JoinColumn(name="photobrand", referencedColumnName="id")
     */
    private $photobrand;

    /**
     * @var InfoCustomdictionaryvalue
     *
     * @ORM\OneToOne(targetEntity="InfoCustomdictionaryvalue")
     * @ORM\JoinColumn(name="photoservicetype", referencedColumnName="id")
     */
    private $photoservicetype;

    /**
     * @var string
     *
     * @ORM\Column(name="comment", type="string", length=2500, nullable=true)
     */
    private $comment;

    /**
     * @var InfoPreparation
     *
     * @ORM\ManyToOne(targetEntity="InfoPreparation")
     * @ORM\JoinColumn(name="photoprep", referencedColumnName="id")
     */
    private $photoprep;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Get content
     *
     * @return string
     */
    public function getContent()
    {
        return $this->content;
    }

    /**
     * Set content.
     *
     * @param null $content
     *
     * @return $this
     */
    public function setContent($content = null)
    {
        $this->content = $content;

        return $this;
    }

    /**
     * Get fsize
     *
     * @return integer
     */
    public function getFsize()
    {
        return $this->fsize;
    }

    /**
     * Get fmodified
     *
     * @return \DateTime
     */
    public function getFmodified()
    {
        return $this->fmodified;
    }

    /**
     * Get guid
     *
     * @return guid
     */
    public function getGuid()
    {
        return $this->guid;
    }

    /**
     * Set guid.
     *
     * @param string|null $guid
     *
     * @return VwTaskfileimage
     */
    public function setGuid($guid = null)
    {
        $this->guid = $guid;

        return $this;
    }

    /**
     * Get currenttime
     *
     * @return \DateTime
     */
    public function getCurrenttime()
    {
        return $this->currenttime;
    }

    /**
     * Set currenttime.
     *
     * @param \DateTime|null $currenttime
     *
     * @return VwTaskfileimage
     */
    public function setCurrenttime(\DateTime $currenttime = null)
    {
        $this->currenttime = $currenttime;

        return $this;
    }

    /**
     * Get moduser
     *
     * @return string
     */
    public function getModuser()
    {
        return $this->moduser;
    }

    /**
     * Set moduser.
     *
     * @param string|null $moduser
     *
     * @return VwTaskfileimage
     */
    public function setModuser($moduser = null)
    {
        $this->moduser = $moduser;

        return $this;
    }

    /**
     * Get subj
     *
     * @return \TeamSoft\CrmRepositoryBundle\Entity\InfoTaskfilegroup
     */
    public function getSubj()
    {
        return $this->subj;
    }

    /**
     * Get photostate
     *
     * @return null|int
     */
    public function getPhotoState() : ?int{
        return $this->photostate;
    }

    /**
     * Get what's inside
     *
     * @return null|InfoCustomdictionaryvalue
     */
    public function getWhatsInside() : ?InfoCustomdictionaryvalue{
        return $this->whatsinside;
    }

    /**
     * Get Brand
     *
     * @return null|InfoPreparationbrend
     */
    public function getPhotoBrand() : ?InfoPreparationbrend{
        return $this->photobrand;
    }

    /**
     * Get service type
     *
     * @return null|InfoCustomdictionaryvalue
     */
    public function getServiceType() : ?InfoCustomdictionaryvalue{
        return $this->photoservicetype;
    }

    /**
     * Get comment
     *
     * @return null|string
     */
    public function getComment() : ?string{
        return $this->comment;
    }

    /**
     * Get photo prep
     *
     * @return InfoPreparation
     */
    public function getPhotoPrep() {
        return $this->photoprep;
    }
}