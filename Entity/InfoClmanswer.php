<?php

namespace TeamSoft\CrmRepositoryBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * InfoClmanswer
 *
 * @ORM\Table(name="info_clmanswer")
 * @ORM\Entity
 */
class InfoClmanswer implements ServiceFieldInterface
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255, nullable=true)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="value", type="string", length=255, nullable=true)
     */
    private $value;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date", type="datetime", nullable=true)
     */
    private $date;

    /**
     * @var \TeamSoft\CrmRepositoryBundle\Entity\InfoUser $task
     *
     * @ORM\ManyToOne(targetEntity="\TeamSoft\CrmRepositoryBundle\Entity\InfoUser")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     * })
     */
    private $user;

    /**
     * @var string
     *
     * @ORM\Column(name="clmslidefilename", type="string", length=255, nullable=true)
     */
    private $clmslidefilename;

    /**
     * @var string
     *
     * @ORM\Column(name="guid", type="guid", nullable=true)
     */
    private $guid;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="currenttime", type="datetime", nullable=true)
     */
    private $currenttime;

    /**
     * @var string
     *
     * @ORM\Column(name="moduser", type="string", length=16, nullable=true)
     */
    private $moduser;

    /**
     * @var \TeamSoft\CrmRepositoryBundle\Entity\InfoTask $task
     *
     * @ORM\ManyToOne(targetEntity="\TeamSoft\CrmRepositoryBundle\Entity\InfoTask")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="task_id", referencedColumnName="id")
     * })
     */
    private $task;

    /**
     * @var \TeamSoft\CrmRepositoryBundle\Entity\InfoClm
     *
     * @ORM\ManyToOne(targetEntity="\TeamSoft\CrmRepositoryBundle\Entity\InfoClm", inversedBy="clmAnswers")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="clm_id", referencedColumnName="id")
     * })
     */
    private $clm;

    /**
     * @var \TeamSoft\CrmRepositoryBundle\Entity\InfoClmslide
     *
     * @ORM\ManyToOne(targetEntity="\TeamSoft\CrmRepositoryBundle\Entity\InfoClmslide")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="clmslide_id", referencedColumnName="id")
     * })
     */
    private $clmslide;



    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return InfoClmanswer
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set value
     *
     * @param string $value
     *
     * @return InfoClmanswer
     */
    public function setValue($value)
    {
        $this->value = $value;

        return $this;
    }

    /**
     * Get value
     *
     * @return string
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     * Set date
     *
     * @param \DateTime $date
     *
     * @return InfoClmanswer
     */
    public function setDate($date)
    {
        $this->date = $date;

        return $this;
    }

    /**
     * Get date
     *
     * @return \DateTime
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * Set userId
     *
     * @param \TeamSoft\CrmRepositoryBundle\Entity\InfoUser $userId
     *
     * @return InfoClmanswer
     */
    public function setUser(\TeamSoft\CrmRepositoryBundle\Entity\InfoUser $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get userId
     *
     * @return \TeamSoft\CrmRepositoryBundle\Entity\InfoUser
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Set clmslidefilename
     *
     * @param string $clmslidefilename
     *
     * @return InfoClmanswer
     */
    public function setClmslidefilename($clmslidefilename)
    {
        $this->clmslidefilename = $clmslidefilename;

        return $this;
    }

    /**
     * Get clmslidefilename
     *
     * @return string
     */
    public function getClmslidefilename()
    {
        return $this->clmslidefilename;
    }

    /**
     * Set guid
     *
     * @param string $guid
     *
     * @return InfoClmanswer
     */
    public function setGuid($guid)
    {
        $this->guid = $guid;

        return $this;
    }

    /**
     * Get guid
     *
     * @return string
     */
    public function getGuid()
    {
        return $this->guid;
    }

    /**
     * Set currenttime
     *
     * @param \DateTime $currenttime
     *
     * @return InfoClmanswer
     */
    public function setCurrenttime(\DateTime $currenttime)
    {
        $this->currenttime = $currenttime;

        return $this;
    }

    /**
     * Get currenttime
     *
     * @return \DateTime
     */
    public function getCurrenttime()
    {
        return $this->currenttime;
    }

    /**
     * Set moduser
     *
     * @param string $moduser
     *
     * @return InfoClmanswer
     */
    public function setModuser($moduser)
    {
        $this->moduser = $moduser;

        return $this;
    }

    /**
     * Get moduser
     *
     * @return string
     */
    public function getModuser()
    {
        return $this->moduser;
    }

    /**
     * Set task
     *
     * @param \TeamSoft\CrmRepositoryBundle\Entity\InfoTask $task
     *
     * @return InfoClmanswer
     */
    public function setTask(\TeamSoft\CrmRepositoryBundle\Entity\InfoTask $task = null)
    {
        $this->task = $task;

        return $this;
    }

    /**
     * Get task
     *
     * @return \TeamSoft\CrmRepositoryBundle\Entity\InfoTask
     */
    public function getTask()
    {
        return $this->task;
    }

    /**
     * Set clm
     *
     * @param \TeamSoft\CrmRepositoryBundle\Entity\InfoClm $clm
     *
     * @return InfoClmanswer
     */
    public function setClm(\TeamSoft\CrmRepositoryBundle\Entity\InfoClm $clm = null)
    {
        $this->clm = $clm;

        return $this;
    }

    /**
     * Get clm
     *
     * @return \TeamSoft\CrmRepositoryBundle\Entity\InfoClm
     */
    public function getClm()
    {
        return $this->clm;
    }

    /**
     * Set clmslide
     *
     * @param \TeamSoft\CrmRepositoryBundle\Entity\InfoClmslide $clmslide
     *
     * @return InfoClmanswer
     */
    public function setClmslide(\TeamSoft\CrmRepositoryBundle\Entity\InfoClmslide $clmslide = null)
    {
        $this->clmslide = $clmslide;

        return $this;
    }

    /**
     * Get clmslide
     *
     * @return \TeamSoft\CrmRepositoryBundle\Entity\InfoClmslide
     */
    public function getClmslide()
    {
        return $this->clmslide;
    }
}
