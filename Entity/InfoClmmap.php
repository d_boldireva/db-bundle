<?php

namespace TeamSoft\CrmRepositoryBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use TeamSoft\CrmRepositoryBundle\Entity\ServiceFieldInterface;

/**
 * InfoClmmap
 *
 * @ORM\Table(name="info_clmmap")
 * @ORM\Entity
 */
class InfoClmmap implements ServiceFieldInterface
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255, nullable=true)
     */
    private $name;

    /**
     * @var integer
     *
     * @ORM\Column(name="ishidden", type="integer", length=255, nullable=true)
     */
    private $isHidden;

    /**
     * @var guid
     *
     * @ORM\Column(name="guid", type="guid", nullable=true)
     */
    private $guid;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="currenttime", type="datetime", nullable=true)
     */
    private $currenttime;

    /**
     * @var string
     *
     * @ORM\Column(name="moduser", type="string", length=16, nullable=true)
     */
    private $moduser;

    /**
     * @var InfoClm
     *
     * @ORM\ManyToOne(targetEntity="InfoClm", inversedBy="clmMaps")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="clm_id", referencedColumnName="id")
     * })
     */
    private $clm;

    /**
     * @var \TeamSoft\CrmRepositoryBundle\Entity\InfoUser
     *
     * @ORM\ManyToOne(targetEntity="\TeamSoft\CrmRepositoryBundle\Entity\InfoUser")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     * })
     */
    private $user;

    /**
     * @ORM\OneToMany(targetEntity="InfoClmmapslide", mappedBy="clmmap", cascade={"persist"}, orphanRemoval=true)
     */
    private $clmMapSlides;

    public function __construct()
    {
        $this->clmMapSlides = new ArrayCollection();
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return InfoClmmap
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set isHidden
     *
     * @param integer $isHidden
     *
     * @return InfoClmmap
     */
    public function setIsHidden($isHidden)
    {
        $this->isHidden = $isHidden;

        return $this;
    }

    /**
     * Get isHidden
     *
     * @return integer
     */
    public function getIsHidden()
    {
        return $this->isHidden;
    }

    /**
     * Set guid
     *
     * @param string $guid
     *
     * @return InfoClmmap
     */
    public function setGuid($guid)
    {
        $this->guid = $guid;

        return $this;
    }

    /**
     * Get guid
     *
     * @return string
     */
    public function getGuid()
    {
        return $this->guid;
    }

    /**
     * Set currenttime
     *
     * @param \DateTime $currenttime
     *
     * @return InfoClmmap
     */
    public function setCurrenttime(\DateTime $currenttime)
    {
        $this->currenttime = $currenttime;

        return $this;
    }

    /**
     * Get currenttime
     *
     * @return \DateTime
     */
    public function getCurrenttime()
    {
        return $this->currenttime;
    }

    /**
     * Set moduser
     *
     * @param string $moduser
     *
     * @return InfoClmmap
     */
    public function setModuser($moduser)
    {
        $this->moduser = $moduser;

        return $this;
    }

    /**
     * Get moduser
     *
     * @return string
     */
    public function getModuser()
    {
        return $this->moduser;
    }

    /**
     * Set clm
     *
     * @param \TeamSoft\CrmRepositoryBundle\Entity\InfoClm $clm
     *
     * @return InfoClmmap
     */
    public function setClm(\TeamSoft\CrmRepositoryBundle\Entity\InfoClm $clm = null)
    {
        $this->clm = $clm;

        return $this;
    }

    /**
     * Get clm
     *
     * @return \TeamSoft\CrmRepositoryBundle\Entity\InfoClm
     */
    public function getClm()
    {
        return $this->clm;
    }

    /**
     * Set user
     *
     * @param \TeamSoft\CrmRepositoryBundle\Entity\InfoUser $user
     *
     * @return InfoClmmap
     */
    public function setUser(\TeamSoft\CrmRepositoryBundle\Entity\InfoUser $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \TeamSoft\CrmRepositoryBundle\Entity\InfoUser
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @return ArrayCollection|InfoClmmapslide[]|Collection
     */
    public function getClmMapSlides(): Collection
    {
        return $this->clmMapSlides;
    }

    /**
     * Add $clmMapSlide
     *
     * @param InfoClmmapslide $clmMapSlide
     *
     * @return self
     */
    public function addClmMapSlide(InfoClmmapslide $clmMapSlide): self
    {
        if (!$this->clmMapSlides->contains($clmMapSlide)) {
            $this->clmMapSlides->add($clmMapSlide);
        }

        return $this;
    }

    /**
     * Remove $clmMapSlide
     *
     * @param InfoClmmapslide $clmMapSlide
     */
    public function removeClmMapSlide(InfoClmmapslide $clmMapSlide)
    {
        $this->clmMapSlides->removeElement($clmMapSlide);
    }
}
