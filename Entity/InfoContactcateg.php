<?php

namespace TeamSoft\CrmRepositoryBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * InfoContactcateg
 *
 * @ORM\Table(name="info_contactcateg", indexes={@ORM\Index(name="ix_info_contactcateg_guid", columns={"guid"}), @ORM\Index(name="ix_info_contactcateg_time", columns={"time"}), @ORM\Index(name="ix_info_contactcateg_contacttype_id", columns={"contacttype_id"}), @ORM\Index(name="ix_info_contactcateg_brend_id", columns={"brend_id"}), @ORM\Index(name="ix_info_contactcateg_target_id", columns={"target_id"})})
 * @ORM\Entity(repositoryClass="TeamSoft\CrmRepositoryBundle\Repository\InfoContactcateg")
 */
class InfoContactcateg
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255, nullable=true)
     */
    private $name;

    /**
     * @var integer
     *
     * @ORM\Column(name="isauto", type="integer", nullable=true)
     */
    private $isauto;

    /**
     * @var float
     *
     * @ORM\Column(name="value11", type="float", precision=53, scale=0, nullable=true)
     */
    private $value11;

    /**
     * @var float
     *
     * @ORM\Column(name="value21", type="float", precision=53, scale=0, nullable=true)
     */
    private $value21;

    /**
     * @var float
     *
     * @ORM\Column(name="value12", type="float", precision=53, scale=0, nullable=true)
     */
    private $value12;

    /**
     * @var float
     *
     * @ORM\Column(name="value22", type="float", precision=53, scale=0, nullable=true)
     */
    private $value22;

    /**
     * @var string
     *
     * @ORM\Column(name="descr", type="text", length=16, nullable=true)
     */
    private $descr;

    /**
     * @var integer
     *
     * @ORM\Column(name="color", type="integer", nullable=true)
     */
    private $color;

    /**
     * @var integer
     *
     * @ORM\Column(name="isgen", type="integer", nullable=true)
     */
    private $isgen;

    /**
     * @var guid
     *
     * @ORM\Column(name="guid", type="guid", nullable=true)
     */
    private $guid;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="currenttime", type="datetime", nullable=true)
     */
    private $currenttime;

    /**
     * @var string
     *
     * @ORM\Column(name="moduser", type="string", length=16, nullable=true)
     */
    private $moduser;

    /**
     * @var integer
     *
     * @ORM\Column(name="limit", type="integer", nullable=true)
     */
    private $limit;

    /**
     * @var integer
     *
     * @ORM\Column(name="value1", type="integer", nullable=true)
     */
    private $value1;

    /**
     * @var integer
     *
     * @ORM\Column(name="value2", type="integer", nullable=true)
     */
    private $value2;

    /**
     * @var integer
     *
     * @ORM\Column(name="istop", type="integer", nullable=true)
     */
    private $istop;

    /**
     * @var \InfoTarget
     *
     * @ORM\ManyToOne(targetEntity="InfoTarget")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="target_id", referencedColumnName="id")
     * })
     */
    private $target;

    /**
     * @var \InfoPreparationbrend
     *
     * @ORM\ManyToOne(targetEntity="InfoPreparationbrend")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="brend_id", referencedColumnName="id")
     * })
     */
    private $brend;

    /**
     * @var InfoContacttype|null
     * @ORM\ManyToOne(targetEntity="InfoContacttype")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="contacttype_id", referencedColumnName="id")
     * })
     */
    private $contacttype;


    /**
     * @var InfoDirection|null
     * @ORM\ManyToOne(targetEntity="InfoDirection")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="direction_id", referencedColumnName="id")
     * })
     */
    private $direction;

    /**
     * @var integer
     *
     * @ORM\Column(name="targeting_cnt", type="integer", nullable=true)
     */
    private $targetingCnt;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return InfoContactcateg
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set isauto
     *
     * @param integer $isauto
     * @return InfoContactcateg
     */
    public function setIsauto($isauto)
    {
        $this->isauto = $isauto;

        return $this;
    }

    /**
     * Get isauto
     *
     * @return integer 
     */
    public function getIsauto()
    {
        return $this->isauto;
    }

    /**
     * Set value11
     *
     * @param float $value11
     * @return InfoContactcateg
     */
    public function setValue11($value11)
    {
        $this->value11 = $value11;

        return $this;
    }

    /**
     * Get value11
     *
     * @return float 
     */
    public function getValue11()
    {
        return $this->value11;
    }

    /**
     * Set value21
     *
     * @param float $value21
     * @return InfoContactcateg
     */
    public function setValue21($value21)
    {
        $this->value21 = $value21;

        return $this;
    }

    /**
     * Get value21
     *
     * @return float 
     */
    public function getValue21()
    {
        return $this->value21;
    }

    /**
     * Set value12
     *
     * @param float $value12
     * @return InfoContactcateg
     */
    public function setValue12($value12)
    {
        $this->value12 = $value12;

        return $this;
    }

    /**
     * Get value12
     *
     * @return float 
     */
    public function getValue12()
    {
        return $this->value12;
    }

    /**
     * Set value22
     *
     * @param float $value22
     * @return InfoContactcateg
     */
    public function setValue22($value22)
    {
        $this->value22 = $value22;

        return $this;
    }

    /**
     * Get value22
     *
     * @return float 
     */
    public function getValue22()
    {
        return $this->value22;
    }

    /**
     * Set descr
     *
     * @param string $descr
     * @return InfoContactcateg
     */
    public function setDescr($descr)
    {
        $this->descr = $descr;

        return $this;
    }

    /**
     * Get descr
     *
     * @return string 
     */
    public function getDescr()
    {
        return $this->descr;
    }

    /**
     * Set color
     *
     * @param integer $color
     * @return InfoContactcateg
     */
    public function setColor($color)
    {
        $this->color = $color;

        return $this;
    }

    /**
     * Get color
     *
     * @return integer 
     */
    public function getColor()
    {
        return $this->color;
    }

    /**
     * Set isgen
     *
     * @param integer $isgen
     * @return InfoContactcateg
     */
    public function setIsgen($isgen)
    {
        $this->isgen = $isgen;

        return $this;
    }

    /**
     * Get isgen
     *
     * @return integer 
     */
    public function getIsgen()
    {
        return $this->isgen;
    }

    /**
     * Set guid
     *
     * @param guid $guid
     * @return InfoContactcateg
     */
    public function setGuid($guid)
    {
        $this->guid = $guid;

        return $this;
    }

    /**
     * Get guid
     *
     * @return guid 
     */
    public function getGuid()
    {
        return $this->guid;
    }

    /**
     * Set currenttime
     *
     * @param \DateTime $currenttime
     * @return InfoContactcateg
     */
    public function setCurrenttime($currenttime)
    {
        $this->currenttime = $currenttime;

        return $this;
    }

    /**
     * Get currenttime
     *
     * @return \DateTime 
     */
    public function getCurrenttime()
    {
        return $this->currenttime;
    }

    /**
     * Set moduser
     *
     * @param string $moduser
     * @return InfoContactcateg
     */
    public function setModuser($moduser)
    {
        $this->moduser = $moduser;

        return $this;
    }

    /**
     * Get moduser
     *
     * @return string 
     */
    public function getModuser()
    {
        return $this->moduser;
    }

    /**
     * Set limit
     *
     * @param integer $limit
     * @return InfoContactcateg
     */
    public function setLimit($limit)
    {
        $this->limit = $limit;

        return $this;
    }

    /**
     * Get limit
     *
     * @return integer 
     */
    public function getLimit()
    {
        return $this->limit;
    }

    /**
     * Set value1
     *
     * @param integer $value1
     * @return InfoContactcateg
     */
    public function setValue1($value1)
    {
        $this->value1 = $value1;

        return $this;
    }

    /**
     * Get value1
     *
     * @return integer 
     */
    public function getValue1()
    {
        return $this->value1;
    }

    /**
     * Set value2
     *
     * @param integer $value2
     * @return InfoContactcateg
     */
    public function setValue2($value2)
    {
        $this->value2 = $value2;

        return $this;
    }

    /**
     * Get value2
     *
     * @return integer 
     */
    public function getValue2()
    {
        return $this->value2;
    }

    /**
     * Set istop
     *
     * @param integer $istop
     * @return InfoContactcateg
     */
    public function setIstop($istop)
    {
        $this->istop = $istop;

        return $this;
    }

    /**
     * Get istop
     *
     * @return integer 
     */
    public function getIstop()
    {
        return $this->istop;
    }

    /**
     * Set target
     *
     * @param \TeamSoft\CrmRepositoryBundle\Entity\InfoTarget $target
     * @return InfoContactcateg
     */
    public function setTarget(\TeamSoft\CrmRepositoryBundle\Entity\InfoTarget $target = null)
    {
        $this->target = $target;

        return $this;
    }

    /**
     * Get target
     *
     * @return \TeamSoft\CrmRepositoryBundle\Entity\InfoTarget 
     */
    public function getTarget()
    {
        return $this->target;
    }

    /**
     * Set brend
     *
     * @param \TeamSoft\CrmRepositoryBundle\Entity\InfoPreparationbrend $brend
     * @return InfoContactcateg
     */
    public function setBrend(\TeamSoft\CrmRepositoryBundle\Entity\InfoPreparationbrend $brend = null)
    {
        $this->brend = $brend;

        return $this;
    }

    /**
     * Get brend
     *
     * @return \TeamSoft\CrmRepositoryBundle\Entity\InfoPreparationbrend 
     */
    public function getBrend()
    {
        return $this->brend;
    }

    /**
     * Set contacttype
     *
     * @param \TeamSoft\CrmRepositoryBundle\Entity\InfoContacttype $contacttype
     * @return InfoContactcateg
     */
    public function setContacttype(\TeamSoft\CrmRepositoryBundle\Entity\InfoContacttype $contacttype = null)
    {
        $this->contacttype = $contacttype;

        return $this;
    }

    /**
     * Get contacttype
     *
     * @return \TeamSoft\CrmRepositoryBundle\Entity\InfoContacttype 
     */
    public function getContacttype()
    {
        return $this->contacttype;
    }

    /**
     * @return InfoDirection|null
     */
    public function getDirection(): ?InfoDirection
    {
        return $this->direction;
    }

    /**
     * @param InfoDirection|null $direction
     *
     * @return InfoContactcateg
     */
    public function setDirection(?InfoDirection $direction): InfoContactcateg
    {
        $this->direction = $direction;

        return $this;
    }

    /**
    * Set sale
    *
    * @param integer $targetingCnt
    * @return $this
    */
    public function setTargetingCnt($targetingCnt): self
    {
        $this->targetingCnt = $targetingCnt;

        return $this;
    }

    /**
     * Get sale
     *
     * @return integer
     */
    public function getTargetingCnt(): int
    {
        return $this->targetingCnt;
    }
}
