<?php

namespace TeamSoft\CrmRepositoryBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * InfoTasktype
 *
 * @ORM\Table(name="info_tasktype", indexes={@ORM\Index(name="ix_info_tasktype_guid", columns={"guid"}), @ORM\Index(name="ix_info_tasktype_time", columns={"time"})})
 * @ORM\Entity(repositoryClass="TeamSoft\CrmRepositoryBundle\Repository\InfoTasktype")
 */
class InfoTasktype
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="Name", type="string", length=255, nullable=false)
     */
    private $name;

    /**
     * @var integer
     *
     * @ORM\Column(name="Promocount", type="integer", nullable=true)
     */
    private $promocount = '0';

    /**
     * @var string
     *
     * @ORM\Column(name="icon", type="text", length=16, nullable=true)
     */
    private $icon;

    /**
     * @var string
     *
     * @ORM\Column(name="guid", type="guid", nullable=true)
     */
    private $guid;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="currenttime", type="datetime", nullable=true)
     */
    private $currenttime;

    /**
     * @var string
     *
     * @ORM\Column(name="moduser", type="string", length=16, nullable=true)
     */
    private $moduser;

    /**
     * @var integer
     *
     * @ORM\Column(name="isrespons", type="integer", nullable=true)
     */
    private $isrespons;

    /**
     * @var integer
     *
     * @ORM\Column(name="promobrend", type="integer", nullable=true)
     */
    private $promobrend;

    /**
     * @var integer
     *
     * @ORM\Column(name="isshowdogov", type="integer", nullable=true)
     */
    private $isshowdogov;

    /**
     * @var integer
     *
     * @ORM\Column(name="showname", type="integer", nullable=true)
     */
    private $showname;

    /**
     * @var integer
     *
     * @ORM\Column(name="minbudgetfact", type="integer", nullable=true)
     */
    private $minbudgetfact;

    /**
     * @var integer
     *
     * @ORM\Column(name="minpromo", type="integer", nullable=true)
     */
    private $minpromo;

    /**
     * @var integer
     *
     * @ORM\Column(name="isshowmerch", type="integer", nullable=true)
     */
    private $isshowmerch;

    /**
     * @var integer
     *
     * @ORM\Column(name="ischeckcategory", type="integer", nullable=true)
     */
    private $ischeckcategory;

    /**
     * @var integer
     *
     * @ORM\Column(name="grouptask", type="integer", nullable=true)
     */
    private $grouptask;

    /**
     * @var string
     *
     * @ORM\Column(name="colorhex", type="string", length=255, nullable=true)
     */
    private $colorhex;

    /**
     * @var string
     *
     * @ORM\Column(name="isinvisibly", type="integer", nullable=true)
     */
    private $isInvisibly;

    /**
     * @var \Doctrine\Common\Collections\ArrayCollection
     *
     * @ORM\ManyToMany(targetEntity="InfoCompanytype")
     * @ORM\JoinTable(name="info_companytypetasktype",
     *   joinColumns={@ORM\JoinColumn(name="tasktype_id", referencedColumnName="id")},
     *   inverseJoinColumns={@ORM\JoinColumn(name="companytype_id", referencedColumnName="id")}
     * )
     */
    private $companytypeCollection;

    /**
     * @var InfoLanguage
     *
     * @ORM\ManyToOne(targetEntity="InfoLanguage")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="language_id", referencedColumnName="id")
     * })
     */
    private $language;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return InfoTasktype
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set promocount
     *
     * @param integer $promocount
     * @return InfoTasktype
     */
    public function setPromocount($promocount)
    {
        $this->promocount = $promocount;

        return $this;
    }

    /**
     * Get promocount
     *
     * @return integer
     */
    public function getPromocount()
    {
        return $this->promocount;
    }

    /**
     * Set icon
     *
     * @param string $icon
     * @return InfoTasktype
     */
    public function setIcon($icon)
    {
        $this->icon = $icon;

        return $this;
    }

    /**
     * Get icon
     *
     * @return string
     */
    public function getIcon()
    {
        return $this->icon;
    }

    /**
     * Set guid
     *
     * @param string $guid
     * @return InfoTasktype
     */
    public function setGuid($guid)
    {
        $this->guid = $guid;

        return $this;
    }

    /**
     * Get guid
     *
     * @return string
     */
    public function getGuid()
    {
        return $this->guid;
    }

    /**
     * Set currenttime
     *
     * @param \DateTime $currenttime
     * @return InfoTasktype
     */
    public function setCurrenttime($currenttime)
    {
        $this->currenttime = $currenttime;

        return $this;
    }

    /**
     * Get currenttime
     *
     * @return \DateTime
     */
    public function getCurrenttime()
    {
        return $this->currenttime;
    }

    /**
     * Set moduser
     *
     * @param string $moduser
     * @return InfoTasktype
     */
    public function setModuser($moduser)
    {
        $this->moduser = $moduser;

        return $this;
    }

    /**
     * Get moduser
     *
     * @return string
     */
    public function getModuser()
    {
        return $this->moduser;
    }

    /**
     * Set isrespons
     *
     * @param integer $isrespons
     * @return InfoTasktype
     */
    public function setIsrespons($isrespons)
    {
        $this->isrespons = $isrespons;

        return $this;
    }

    /**
     * Get isrespons
     *
     * @return integer
     */
    public function getIsrespons()
    {
        return $this->isrespons;
    }

    /**
     * Set promobrend
     *
     * @param integer $promobrend
     * @return InfoTasktype
     */
    public function setPromobrend($promobrend)
    {
        $this->promobrend = $promobrend;

        return $this;
    }

    /**
     * Get promobrend
     *
     * @return integer
     */
    public function getPromobrend()
    {
        return $this->promobrend;
    }

    /**
     * Set isshowdogov
     *
     * @param integer $isshowdogov
     * @return InfoTasktype
     */
    public function setIsshowdogov($isshowdogov)
    {
        $this->isshowdogov = $isshowdogov;

        return $this;
    }

    /**
     * Get isshowdogov
     *
     * @return integer
     */
    public function getIsshowdogov()
    {
        return $this->isshowdogov;
    }

    /**
     * Set showname
     *
     * @param integer $showname
     * @return InfoTasktype
     */
    public function setShowname($showname)
    {
        $this->showname = $showname;

        return $this;
    }

    /**
     * Get showname
     *
     * @return integer
     */
    public function getShowname()
    {
        return $this->showname;
    }

    /**
     * Set minbudgetfact
     *
     * @param integer $minbudgetfact
     * @return InfoTasktype
     */
    public function setMinbudgetfact($minbudgetfact)
    {
        $this->minbudgetfact = $minbudgetfact;

        return $this;
    }

    /**
     * Get minbudgetfact
     *
     * @return integer
     */
    public function getMinbudgetfact()
    {
        return $this->minbudgetfact;
    }

    /**
     * Set minpromo
     *
     * @param integer $minpromo
     * @return InfoTasktype
     */
    public function setMinpromo($minpromo)
    {
        $this->minpromo = $minpromo;

        return $this;
    }

    /**
     * Get minpromo
     *
     * @return integer
     */
    public function getMinpromo()
    {
        return $this->minpromo;
    }

    /**
     * Set isshowmerch
     *
     * @param integer $isshowmerch
     * @return InfoTasktype
     */
    public function setIsshowmerch($isshowmerch)
    {
        $this->isshowmerch = $isshowmerch;

        return $this;
    }

    /**
     * Get isshowmerch
     *
     * @return integer
     */
    public function getIsshowmerch()
    {
        return $this->isshowmerch;
    }

    /**
     * Set ischeckcategory
     *
     * @param integer $ischeckcategory
     * @return InfoTasktype
     */
    public function setIscheckcategory($ischeckcategory)
    {
        $this->ischeckcategory = $ischeckcategory;

        return $this;
    }

    /**
     * Get ischeckcategory
     *
     * @return integer
     */
    public function getIscheckcategory()
    {
        return $this->ischeckcategory;
    }

    /**
     * Set grouptask
     *
     * @param integer $grouptask
     * @return InfoTasktype
     */
    public function setGrouptask($grouptask)
    {
        $this->grouptask = $grouptask;

        return $this;
    }

    /**
     * Get grouptask
     *
     * @return integer
     */
    public function getGrouptask()
    {
        return $this->grouptask;
    }

    /**
     * Set colorhex
     *
     * @param string $colorhex
     *
     * @return InfoTasktype
     */
    public function setColorhex($colorhex)
    {
        $this->colorhex = $colorhex;

        return $this;
    }

    /**
     * Set isInvisibly.
     *
     * @param int|null $isInvisibly
     *
     * @return InfoTasktype
     */
    public function setIsInvisibly($isInvisibly = null)
    {
        $this->isInvisibly = $isInvisibly;

        return $this;
    }

    /**
     * Get isInvisibly.
     *
     * @return int|null
     */
    public function getIsInvisibly()
    {
        return $this->isInvisibly;
    }

    /**
     * Get colorhex
     *
     * @return string
     */
    public function getColorhex()
    {
        return $this->colorhex;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->companytypeCollection = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add companytypeCollection
     *
     * @param \TeamSoft\CrmRepositoryBundle\Entity\InfoCompanytype $companytypeCollection
     *
     * @return InfoTasktype
     */
    public function addCompanytypeCollection(\TeamSoft\CrmRepositoryBundle\Entity\InfoCompanytype $companytypeCollection)
    {
        $this->companytypeCollection[] = $companytypeCollection;

        return $this;
    }

    /**
     * Remove companytypeCollection
     *
     * @param \TeamSoft\CrmRepositoryBundle\Entity\InfoCompanytype $companytypeCollection
     */
    public function removeCompanytypeCollection(\TeamSoft\CrmRepositoryBundle\Entity\InfoCompanytype $companytypeCollection)
    {
        $this->companytypeCollection->removeElement($companytypeCollection);
    }

    /**
     * Get companytypeCollection
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getCompanytypeCollection()
    {
        return $this->companytypeCollection;
    }

    /**
     * Set language
     *
     * @param InfoLanguage|null $content
     * @return InfoTasktype
     */
    public function setLanguage(InfoLanguage $language = null)
    {
        $this->language = $language;

        return $this;
    }

    /**
     * Get language
     *
     * @return InfoLanguage
     */
    public function getLanguage()
    {
        return $this->language;
    }
}
