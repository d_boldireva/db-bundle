<?php

namespace TeamSoft\CrmRepositoryBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use TeamSoft\CrmRepositoryBundle\Model\InfoFile;

/**
 * InfoContactsignfile
 *
 * @ORM\Table(name="info_contactsignfile")
 * @ORM\Entity
 */
class InfoContactsignfile extends InfoFile implements ServiceFieldInterface
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="currenttime", type="datetime", nullable=true)
     */
    private $currenttime;

    /**
     * @var string|null
     *
     * @ORM\Column(name="moduser", type="string", length=16, nullable=true)
     */
    private $moduser;

    /**
     * @var string|null
     *
     * @ORM\Column(name="guid", type="guid", nullable=true)
     */
    private $guid;

    /**
     * @var string|null
     *
     * @ORM\Column(name="agreement", type="blob_or_string", nullable=true)
     */
    private $agreement;

    /**
     * @var InfoContactsign
     *
     * @ORM\ManyToOne (targetEntity="InfoContactsign", inversedBy="contactSignFiles")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="contactsign_id", referencedColumnName="id")
     * })
     */
    private $contactSign;

    /**
     * @var string|null
     *
     * @ORM\Column(name="filetype", type="string", length=16, nullable=true)
     */
    private $filetype;


    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set currenttime.
     *
     * @param \DateTime|null $currenttime
     *
     * @return InfoContactsignfile
     */
    public function setCurrenttime(\DateTime $currenttime = null)
    {
        $this->currenttime = $currenttime;

        return $this;
    }

    /**
     * Get currenttime.
     *
     * @return \DateTime|null
     */
    public function getCurrenttime()
    {
        return $this->currenttime;
    }

    /**
     * Set moduser.
     *
     * @param string|null $moduser
     *
     * @return InfoContactsignfile
     */
    public function setModuser($moduser = null)
    {
        $this->moduser = $moduser;

        return $this;
    }

    /**
     * Get moduser.
     *
     * @return string|null
     */
    public function getModuser()
    {
        return $this->moduser;
    }

    /**
     * Set guid.
     *
     * @param string|null $guid
     *
     * @return InfoContactsignfile
     */
    public function setGuid($guid = null)
    {
        $this->guid = $guid;

        return $this;
    }

    /**
     * Get guid.
     *
     * @return string|null
     */
    public function getGuid()
    {
        return $this->guid;
    }

    /**
     * Set agreement.
     *
     * @param string|null $agreement
     *
     * @return InfoContactsignfile
     */
    public function setAgreement($agreement = null)
    {
        $this->agreement = $agreement;

        return $this;
    }

    /**
     * Get agreement.
     *
     * @return string|null
     */
    public function getAgreement()
    {
        return $this->agreement;
    }

    /**
     * Set filetype.
     *
     * @param string|null $filetype
     *
     * @return InfoContactsignfile
     */
    public function setFiletype($filetype = null)
    {
        $this->filetype = $filetype;

        return $this;
    }

    /**
     * Get filetype.
     *
     * @return string|null
     */
    public function getFiletype(): ?string
    {
        return $this->filetype;
    }

    /**
     * Set contactSign.
     *
     * @param InfoContactsign|null $contactSign
     *
     * @return InfoContactsignfile
     */
    public function setContactSign(InfoContactsign $contactSign = null)
    {
        $this->contactSign = $contactSign;

        return $this;
    }

    /**
     * Get contactSign.
     *
     * @return InfoContactsign|null
     */
    public function getContactSign()
    {
        return $this->contactSign;
    }

    public function getName()
    {
        return '';
    }

    /**
     * Set content
     *
     * @param string $content
     *
     * @return InfoContactsignfile
     */
    public function setContent($content)
    {
        return $this->setAgreement($content);
    }

    /**
     * Get content
     *
     * @return resource | string
     */
    public function getContent()
    {
        return $this->getAgreement();
    }

    public function setExtension(?string $type)
    {
        $this->setFiletype($type);
    }

    public function getExtension(): ?string
    {
        return $this->getFiletype();
    }
}
