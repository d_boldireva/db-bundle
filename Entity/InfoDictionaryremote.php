<?php
declare(strict_types=1);


namespace TeamSoft\CrmRepositoryBundle\Entity;


use Doctrine\ORM\Mapping as ORM;

/**
 * InfoDictionaryremote
 *
 * @ORM\Table(name="info_dictionaryremote")
 * @ORM\Entity
 */
class InfoDictionaryremote implements ServiceFieldInterface
{
    const TYPE_SIGN_CAPTION = 'sign_caption';
    const TYPE_SIGN_AGREEMENT = 'sign_agreement';
    const TYPE_SIGN_SQL = 'sign_sql';
    const TYPE_SIGN_SQL_SERVER= 'sign_sql_server';

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="guid", type="guid", nullable=true)
     */
    private $guid;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="currenttime", type="datetime", nullable=true)
     */
    private $currenttime;

    /**
     * @var string
     *
     * @ORM\Column(name="moduser", type="string", length=16, nullable=true)
     */
    private $moduser;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", nullable=true)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="type", type="string", nullable=true)
     */
    private $type;

    /**
     * @var InfoCountry
     *
     * @ORM\ManyToOne(targetEntity="InfoCountry")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="country_id", referencedColumnName="id")
     * })
     */
    private $country;

    /**
     * @var string|null
     */
    private $agreementWithContactInfo;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function setGuid($guid): self
    {
        $this->guid = $guid;

        return $this;
    }

    public function getGuid(): ?string
    {
        return $this->guid;
    }

    public function setCurrenttime(\DateTime $currenttime): self
    {
        $this->currenttime = $currenttime;

        return $this;
    }

    public function getCurrenttime(): ?\DateTime
    {
        return $this->currenttime;
    }

    public function setModuser($moduser): self
    {
        $this->moduser = $moduser;

        return $this;
    }

    public function getModuser(): ?string
    {
        return $this->moduser;
    }

    public function setName(?string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setType(?string $type): self
    {
        $this->type = $type;

        return $this;
    }

    public function getType(): ?string
    {
        return $this->type;
    }

    public function setCountry(InfoCountry $country): self
    {
        $this->country = $country;

        return $this;
    }

    public function getCountry(): ?InfoCountry
    {
        return $this->country;
    }

    /**
     * @return string|null
     */
    public function getAgreementWithContactInfo(): ?string
    {
        return $this->agreementWithContactInfo;
    }

    /**
     * @param string|null $agreementWithContactInfo
     * @return self
     */
    public function setAgreementWithContactInfo(?string $agreementWithContactInfo): self
    {
        $this->agreementWithContactInfo = $agreementWithContactInfo;
        return $this;
    }
}
