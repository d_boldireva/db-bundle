<?php

namespace TeamSoft\CrmRepositoryBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * InfoMcmtargetfilter
 *
 * @ORM\Table(name="info_mcmtargetfilter")
 * @ORM\Entity
 */
class InfoMcmtargetfilter implements ServiceFieldInterface
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var \TeamSoft\CrmRepositoryBundle\Entity\InfoMcmtarget
     *
     * @ORM\ManyToOne(targetEntity="\TeamSoft\CrmRepositoryBundle\Entity\InfoMcmtarget", inversedBy="filterCollection")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="mcmtarget_id", referencedColumnName="id")
     * })
     */
    private $mcmtarget;

    /**
     * @var \TeamSoft\CrmRepositoryBundle\Entity\InfoMcmfilter
     *
     * @ORM\ManyToOne(targetEntity="\TeamSoft\CrmRepositoryBundle\Entity\InfoMcmfilter")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="mcmfilter_id", referencedColumnName="id")
     * })
     */
    private $mcmfilter;

    /**
     * @var \TeamSoft\CrmRepositoryBundle\Entity\InfoAddinfotype
     *
     * @ORM\ManyToOne(targetEntity="\TeamSoft\CrmRepositoryBundle\Entity\InfoAddinfotype")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="addinfotype_id", referencedColumnName="id")
     * })
     */
    private $addinfotype;

    /**
     * @var string
     *
     * @ORM\Column(name="value", type="string", length=1000, nullable=true)
     */
    private $value;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="currenttime", type="datetime", nullable=true)
     */
    private $currenttime;

    /**
     * @var string
     *
     * @ORM\Column(name="moduser", type="string", length=16, nullable=true)
     */
    private $moduser;

    /**
     * @var string
     *
     * @ORM\Column(name="guid", type="guid", nullable=true)
     */
    private $guid;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set mcmtargetId
     *
     * @param \TeamSoft\CrmRepositoryBundle\Entity\InfoMcmtarget $mcmtarget
     *
     * @return InfoMcmtargetfilter
     */
    public function setMcmtarget(\TeamSoft\CrmRepositoryBundle\Entity\InfoMcmtarget $mcmtarget = null)
    {
        $this->mcmtarget = $mcmtarget;

        return $this;
    }

    /**
     * Get mcmtarget
     *
     * @return \TeamSoft\CrmRepositoryBundle\Entity\InfoMcmtarget
     */
    public function getMcmtarget()
    {
        return $this->mcmtarget;
    }

    /**
     * Set mcmfilter
     *
     * @param \TeamSoft\CrmRepositoryBundle\Entity\InfoMcmfilter $mcmfilter
     *
     * @return InfoMcmtargetfilter
     */
    public function setMcmfilter(\TeamSoft\CrmRepositoryBundle\Entity\InfoMcmfilter $mcmfilter)
    {
        $this->mcmfilter = $mcmfilter;

        return $this;
    }

    /**
     * Get mcmfilterId
     *
     * @return \TeamSoft\CrmRepositoryBundle\Entity\InfoMcmfilter
     */
    public function getMcmfilter()
    {
        return $this->mcmfilter;
    }

    /**
     * Set addinfotype
     *
     * @param \TeamSoft\CrmRepositoryBundle\Entity\InfoAddinfotype $addinfotype
     *
     * @return InfoMcmtargetfilter
     */
    public function setAddinfotype(\TeamSoft\CrmRepositoryBundle\Entity\InfoAddinfotype $addinfotype = null)
    {
        $this->addinfotype = $addinfotype;

        return $this;
    }

    /**
     * Get addinfotype
     *
     * @return \TeamSoft\CrmRepositoryBundle\Entity\InfoAddinfotype
     */
    public function getAddinfotype()
    {
        return $this->addinfotype;
    }

    /**
     * Set value
     *
     * @param string $value
     *
     * @return InfoMcmtargetfilter
     */
    public function setValue($value)
    {
        $this->value = $value;

        return $this;
    }

    /**
     * Get value
     *
     * @return string
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     * Set currenttime
     *
     * @param \DateTime $currenttime
     *
     * @return InfoMcmtargetfilter
     */
    public function setCurrenttime(\DateTime $currenttime)
    {
        $this->currenttime = $currenttime;

        return $this;
    }

    /**
     * Get currenttime
     *
     * @return \DateTime
     */
    public function getCurrenttime()
    {
        return $this->currenttime;
    }

    /**
     * Set moduser
     *
     * @param string $moduser
     *
     * @return InfoMcmtargetfilter
     */
    public function setModuser($moduser)
    {
        $this->moduser = $moduser;

        return $this;
    }

    /**
     * Get moduser
     *
     * @return string
     */
    public function getModuser()
    {
        return $this->moduser;
    }

    /**
     * Set guid
     *
     * @param string $guid
     *
     * @return InfoMcmtargetfilter
     */
    public function setGuid($guid)
    {
        $this->guid = $guid;

        return $this;
    }

    /**
     * Get guid
     *
     * @return string
     */
    public function getGuid()
    {
        return $this->guid;
    }

    /**
     * get filter id
     *
     * @return integer
     */

    public function getMcmfilterId()
    {
        return $this->getMcmfilter() !== null ? $this->getMcmfilter()->getId() : null;
    }

    /**
     * get addinfotype
     *
     * @return integer
     */

    public function getAddinfotypeId()
    {
        return $this->getAddinfotype() !== null ? $this->getAddinfotype()->getId() : null;
    }
}
