<?php

namespace TeamSoft\CrmRepositoryBundle\Entity\SaleProject;

use Doctrine\ORM\Mapping as ORM;
use TeamSoft\CrmRepositoryBundle\Entity\InfoPreparation;
use TeamSoft\CrmRepositoryBundle\Entity\ServiceFieldInterface;

/**
 * Class InfoSaleprojecprep
 * @package TeamSoft\CrmRepositoryBundle\Entity\SaleProject
 *
 * @ORM\Table(name="info_saleprojectprep")
 * @ORM\Entity`
 */
class InfoSaleprojectprep implements ServiceFieldInterface
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id()
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var int
     * @ORM\Column(name="discount_threshold_1", type="integer", nullable=true)
     */
    private $discountThreshold1;

    /**
     * @var float
     * @ORM\Column(name="discount_1", type="float", nullable=true)
     */
    private $discount1;

    /**
     * @var int
     * @ORM\Column(name="discount_threshold_2", type="integer", nullable=true)
     */
    private $discountThreshold2;


    /**
     * @var float
     * @ORM\Column(name="discount_2", type="float", nullable=true)
     */
    private $discount2;

    /**
     * @var int
     * @ORM\Column(name="discount_threshold_3", type="integer", nullable=true)
     */
    private $discountThreshold3;

    /**
     * @var float
     * @ORM\Column(name="discount_3", type="float", nullable=true)
     */
    private $discount3;

    /**
     * @var InfoSaleproject
     * @ORM\ManyToOne(targetEntity="InfoSaleproject", inversedBy="saleProjectPreps")
     * @ORM\JoinColumn(name="saleproject_id", referencedColumnName="id", onDelete="CASCADE")
     */
    private $saleproject;

    /**
     * @var InfoPreparation
     * @ORM\ManyToOne(targetEntity="\TeamSoft\CrmRepositoryBundle\Entity\InfoPreparation")
     * @ORM\JoinColumn(name="prep_id", referencedColumnName="id", onDelete="CASCADE")
     */
    private $prep;

    /**
     * @var InfoPreparation
     * @ORM\ManyToOne(targetEntity="\TeamSoft\CrmRepositoryBundle\Entity\InfoPreparation")
     * @ORM\JoinColumn(name="prepdiscount_id", referencedColumnName="id", onDelete="CASCADE")
     */
    private $prepDiscount;

    /**
     * @var int
     * @ORM\Column(name="prepdiscount_threshold", type="integer", nullable=true)
     */
    private $prepDiscountThreshold;

    /**
     * @var string
     *
     * @ORM\Column(name="guid", type="guid", nullable=true)
     */
    private $guid;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="currenttime", type="datetime", nullable=true)
     */
    private $currenttime;

    /**
     * @var string
     *
     * @ORM\Column(name="moduser", type="string", length=16, nullable=true)
     */
    private $moduser;

    /**
     * @var int
     * @ORM\Column(name="discount_threshold_4", type="integer", nullable=true)
     */
    private $discountThreshold4;

    /**
     * @var float
     * @ORM\Column(name="discount_4", type="float", nullable=true)
     */
    private $discount4;


    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $threshold
     * @return $this
     */
    public function setDiscountThreshold1($threshold)
    {
        $this->discountThreshold1 = $threshold;
        return $this;
    }

    /**
     * @return int
     */
    public function getDiscountThreshold1()
    {
        return $this->discountThreshold1;
    }

    /**
     * @param float $discount
     * @return $this
     */
    public function setDiscount1($discount)
    {
        $this->discount1 = $discount;
        return $this;
    }

    /**
     * @return float
     */
    public function getDiscount1()
    {
        return $this->discount1;
    }

    /**
     * @param int $threshold
     * @return $this
     */
    public function setDiscountThreshold2($threshold)
    {
        $this->discountThreshold2 = $threshold;
        return $this;
    }

    /**
     * @return int
     */
    public function getDiscountThreshold2()
    {
        return $this->discountThreshold2;
    }

    /**
     * @param float $discount
     * @return $this
     */
    public function setDiscount2($discount)
    {
        $this->discount2 = $discount;
        return $this;
    }

    /**
     * @return float
     */
    public function getDiscount2()
    {
        return $this->discount2;
    }

    /**
     * @param int $threshold
     * @return $this
     */
    public function setDiscountThreshold3($threshold)
    {
        $this->discountThreshold3 = $threshold;
        return $this;
    }

    /**
     * @return int
     */
    public function getDiscountThreshold3()
    {
        return $this->discountThreshold3;
    }

    /**
     * @param float $discount
     * @return $this
     */
    public function setDiscount3($discount)
    {
        $this->discount3 = $discount;
        return $this;
    }

    /**
     * @return float
     */
    public function getDiscount3()
    {
        return $this->discount3;
    }

    /**
     * @param InfoSaleproject $saleproject
     * @return $this
     */
    public function setSaleproject(?InfoSaleproject $saleproject)
    {
        $this->saleproject = $saleproject;
        return $this;
    }

    /**
     * @return InfoSaleproject
     */
    public function getSaleproject()
    {
        return $this->saleproject;
    }

    /**
     * @return int|null
     */
    public function getSaleprojectId()
    {
        return $this->saleproject ? $this->saleproject->getId() : null;
    }

    /**
     * @param InfoPreparation $preparation
     * @return $this
     */
    public function setPrep(InfoPreparation $preparation)
    {
        $this->prep = $preparation;

        return $this;
    }

    /**
     * @return InfoPreparation
     */
    public function getPrep()
    {
        return $this->prep;
    }

    /**
     * @param InfoPreparation $preparation
     * @return self
     */
    public function setPrepDiscount(?InfoPreparation $preparation): self
    {
        $this->prepDiscount = $preparation;

        return $this;
    }

    /**
     * @return InfoPreparation
     */
    public function getPrepDiscount(): ?InfoPreparation
    {
        return $this->prepDiscount;
    }

    /**
     * @param int $threshold
     * @return self
     */
    public function setPrepDiscountThreshold(?int $threshold): self
    {
        $this->prepDiscountThreshold = $threshold;

        return $this;
    }

    /**
     * @return int
     */
    public function getPrepDiscountThreshold(): ?int
    {
        return $this->prepDiscountThreshold;
    }

    /**
     * @return int|null
     */
    public function getPrepId()
    {
        return $this->prep ? $this->prep->getId() : null;
    }

    /**
     * @return int|null
     */
    public function getPrepDiscountId()
    {
        return $this->prepDiscount ? $this->prepDiscount->getId() : null;
    }


    /**
     * Set guid
     *
     * @param string $guid
     * @return $this
     */
    public function setGuid($guid)
    {
        $this->guid = $guid;

        return $this;
    }

    /**
     * Get guid
     *
     * @return string
     */
    public function getGuid()
    {
        return $this->guid;
    }

    /**
     * Set currenttime
     *
     * @param \DateTime $currenttime
     * @return $this
     */
    public function setCurrenttime(\DateTime $currenttime = null)
    {
        $this->currenttime = $currenttime;

        return $this;
    }

    /**
     * Get currenttime
     *
     * @return \DateTime
     */
    public function getCurrenttime()
    {
        return $this->currenttime;
    }

    /**
     * Set moduser
     *
     * @param string $moduser
     * @return $this
     */
    public function setModuser($moduser)
    {
        $this->moduser = $moduser;

        return $this;
    }

    /**
     * Get moduser
     *
     * @return string
     */
    public function getModuser()
    {
        return $this->moduser;
    }

    /**
     * @param int $threshold
     * @return $this
     */
    public function setDiscountThreshold4($threshold)
    {
        $this->discountThreshold4 = $threshold;
        return $this;
    }

    /**
     * @return int
     */
    public function getDiscountThreshold4()
    {
        return $this->discountThreshold4;
    }

    /**
     * @param float $discount
     * @return $this
     */
    public function setDiscount4($discount)
    {
        $this->discount4 = $discount;
        return $this;
    }

    /**
     * @return float
     */
    public function getDiscount4()
    {
        return $this->discount4;
    }
}
