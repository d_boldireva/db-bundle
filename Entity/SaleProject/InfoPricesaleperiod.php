<?php

namespace TeamSoft\CrmRepositoryBundle\Entity\SaleProject;

use Doctrine\ORM\Mapping as ORM;
use TeamSoft\CrmRepositoryBundle\Entity\InfoPreparation;
use TeamSoft\CrmRepositoryBundle\Entity\InfoSaleperiod;
use TeamSoft\CrmRepositoryBundle\Entity\ServiceFieldInterface;

/**
 * Class InfoPricesaleperiod
 * @package TeamSoft\CrmRepositoryBundle\Entity\SaleProject
 *
 * @ORM\Table(name="info_pricesaleperiod")
 * @ORM\Entity(repositoryClass="\TeamSoft\CrmRepositoryBundle\Repository\SaleProject\InfoPriceSalePeriod")
 */
class InfoPricesaleperiod implements ServiceFieldInterface
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id()
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var \DateTime
     * @ORM\Column(name="datefrom", type="datetime", nullable=true)
     */
    private $datefrom;

    /**
     * @var \DateTime
     * @ORM\Column(name="datetill" , type="datetime", nullable=true)
     */
    private $datetill;

    /**
     * @var float
     * @ORM\Column(name="cip", type="float", nullable=true)
     */
    private $cip;

    /**
     * @var float
     * @ORM\Column(name="price", type="float", nullable=true)
     */
    private $price;

    /**
     * @var float
     * @ORM\Column(name="pp", type="float", nullable=true)
     */
    private $pp;

    /**
     * @var float
     * @ORM\Column(name="copay", type="float", nullable=true)
     */
    private $copay;

    /**
     * @var InfoSaleperiod
     * @ORM\ManyToOne(targetEntity="\TeamSoft\CrmRepositoryBundle\Entity\InfoSaleperiod", inversedBy="priceSalePeriods")
     * @ORM\JoinColumn(name="saleperiod_id", referencedColumnName="id", onDelete="SET NULL")
     */
    private $saleperiod;

    /**
     * @var InfoPreparation
     * @ORM\ManyToOne(targetEntity="\TeamSoft\CrmRepositoryBundle\Entity\InfoPreparation")
     * @ORM\JoinColumn(name="prep_id", referencedColumnName="id", onDelete="CASCADE")
     */
    private $prep;


    /**
     * @var string
     *
     * @ORM\Column(name="guid", type="guid", nullable=true)
     */
    private $guid;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="currenttime", type="datetime", nullable=true)
     */
    private $currenttime;

    /**
     * @var string
     *
     * @ORM\Column(name="moduser", type="string", length=16, nullable=true)
     */
    private $moduser;


    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param \DateTime $datefrom
     * @return $this
     */
    public function setDatefrom(\DateTime $datefrom)
    {
        $this->datefrom = $datefrom;
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getDatefrom()
    {
        return $this->datefrom;
    }

    /**
     * @param \DateTime $datetill
     * @return $this
     */
    public function setDatetill(\DateTime $datetill)
    {
        $this->datetill = $datetill;
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getDatetill()
    {
        return $this->datetill;
    }

    /**
     * @param float $cip
     * @return $this
     */
    public function setCIP($cip)
    {
        $this->cip = $cip;
        return $this;
    }

    /**
     * @return float
     */
    public function getCIP()
    {
        return $this->cip;
    }

    /**
     * @param float $price
     * @return $this
     */
    public function setPrice($price)
    {
        $this->price = $price;
        return $this;
    }

    /**
     * @return float
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * @param float $pp
     * @return $this
     */
    public function setPP($pp)
    {
        $this->pp = $pp;
        return $this;
    }

    /**
     * @return float
     */
    public function getPP()
    {
        return $this->pp;
    }

    /**
     * @param float $copay
     * @return $this
     */
    public function setCopay(?float $copay): self
    {
        $this->copay = $copay;

        return $this;
    }

    /**
     * @return float
     */
    public function getCopay(): ?float
    {
        return $this->copay;
    }

    /**
     * @param InfoSaleperiod $saleperiod
     * @return $this
     */
    public function setSaleperiod(?InfoSaleperiod $saleperiod)
    {
        $this->saleperiod = $saleperiod;

        return $this;
    }

    /**
     * @return InfoSaleperiod
     */
    public function getSaleperiod()
    {
        return $this->saleperiod;
    }

    /**
     * @return int|null
     */
    public function getSaleperiodId()
    {
        return $this->saleperiod ? $this->saleperiod->getId() : null;
    }

    /**
     * @param InfoPreparation $preparation
     * @return $this
     */
    public function setPrep(InfoPreparation $preparation)
    {
        $this->prep = $preparation;
        return $this;
    }

    /**
     * @return InfoPreparation
     */
    public function getPrep()
    {
        return $this->prep;
    }

    /**
     * @return int|null
     */
    public function getPrepId()
    {
        return $this->prep ? $this->prep->getId() : null;
    }


    /**
     * Set guid
     *
     * @param string $guid
     * @return $this
     */
    public function setGuid($guid)
    {
        $this->guid = $guid;

        return $this;
    }

    /**
     * Get guid
     *
     * @return string
     */
    public function getGuid()
    {
        return $this->guid;
    }

    /**
     * Set currenttime
     *
     * @param \DateTime $currenttime
     * @return $this
     */
    public function setCurrenttime(\DateTime $currenttime = null)
    {
        $this->currenttime = $currenttime;

        return $this;
    }

    /**
     * Get currenttime
     *
     * @return \DateTime
     */
    public function getCurrenttime()
    {
        return $this->currenttime;
    }

    /**
     * Set moduser
     *
     * @param string $moduser
     * @return $this
     */
    public function setModuser($moduser)
    {
        $this->moduser = $moduser;

        return $this;
    }

    /**
     * Get moduser
     *
     * @return string
     */
    public function getModuser()
    {
        return $this->moduser;
    }
}
