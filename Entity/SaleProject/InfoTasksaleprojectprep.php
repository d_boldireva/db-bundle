<?php
declare(strict_types=1);

namespace TeamSoft\CrmRepositoryBundle\Entity\SaleProject;

use Doctrine\ORM\Mapping as ORM;
use TeamSoft\CrmRepositoryBundle\Entity\InfoPreparation;
use TeamSoft\CrmRepositoryBundle\Entity\ServiceFieldInterface;

/**
 * Class InfoTasksaleprojectprep
 * @ORM\Table(name="info_tasksaleprojectprep")
 * @ORM\Entity()
 */
class InfoTasksaleprojectprep implements ServiceFieldInterface
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var InfoTasksaleproject|null
     *
     * @ORM\ManyToOne(targetEntity="InfoTasksaleproject")
     * @ORM\JoinColumns({
     *      @ORM\JoinColumn(name="tasksaleproject_id", referencedColumnName="id")
     * })
     */
    private $taskSaleProject;

    /**
     * @var InfoPreparation|null
     *
     * @ORM\ManyToOne(targetEntity="\TeamSoft\CrmRepositoryBundle\Entity\InfoPreparation")
     * @ORM\JoinColumns({
     *      @ORM\JoinColumn(name="prep_id", referencedColumnName="id")
     * })
     */
    private $prep;

    /**
     * @var integer|null
     *
     * @ORM\Column(name="quantity", type="integer", nullable=true)
     */
    private $quantity;

    /**
     * @var integer|null
     *
     * @ORM\Column(name="discount", type="integer", nullable=true)
     */
    private $discount;

    /**
     * @var integer|null
     *
     * @ORM\Column(name="adddiscount", type="integer", nullable=true)
     */
    private $addDiscount;

    /**
     * @var integer|null
     *
     * @ORM\Column(name="adddiscountvalue", type="integer", nullable=true)
     */
    private $addDiscountValue;

    /**
     * @var string|null
     *
     * @ORM\Column(name="guid", type="guid", nullable=true)
     */
    private $guid;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="currenttime", type="datetime", nullable=true)
     */
    private $currenttime;

    /**
     * @var string|null
     *
     * @ORM\Column(name="moduser", type="string", length=16, nullable=true)
     */
    private $moduser;

    public function getId(): int
    {
        return $this->id;
    }

    public function getTaskSaleProject(): ?InfoTasksaleproject
    {
        return $this->taskSaleProject;
    }

    public function setTaskSaleProject(?InfoTasksaleproject $taskSaleProject): self
    {
        $this->taskSaleProject = $taskSaleProject;
        return $this;
    }

    public function getPrep(): ?InfoPreparation
    {
        return $this->prep;
    }

    public function setPrep(?InfoPreparation $prep): self
    {
        $this->prep = $prep;
        return $this;
    }

    public function getQuantity(): ?string
    {
        return $this->quantity;
    }

    public function setQuantity(?string $quantity): self
    {
        $this->quantity = $quantity;
        return $this;
    }

    public function getDiscount(): ?int
    {
        return $this->discount;
    }

    public function setDiscount(?int $discount): self
    {
        $this->discount = $discount;
        return $this;
    }

    public function getAddDiscount(): ?int
    {
        return $this->addDiscount;
    }

    public function setAddDiscount(?int $addDiscount): self
    {
        $this->addDiscount = $addDiscount;
        return $this;
    }

    public function getAddDiscountValue(): ?int
    {
        return $this->addDiscountValue;
    }

    public function setAddDiscountValue(?int $addDiscountValue): self
    {
        $this->addDiscountValue = $addDiscountValue;
        return $this;
    }

    public function getGuid(): ?string
    {
        return $this->guid;
    }

    public function setGuid($guid): self
    {
        $this->guid = $guid;
        return $this;
    }

    public function getCurrenttime(): ?\DateTime
    {
        return $this->currenttime;
    }

    public function setCurrenttime(?\DateTime $currenttime): self
    {
        $this->currenttime = $currenttime;
        return $this;
    }

    public function getModuser(): ?string
    {
        return $this->moduser;
    }

    public function setModuser($moduser = null): self
    {
        $this->moduser = $moduser;
        return $this;
    }

    public function getTaskSaleProjectId(): ?int
    {
        return $this->getTaskSaleProject() ? $this->getTaskSaleProject()->getId() : null;
    }

    public function getPrepId(): ?int
    {
        return $this->getPrep() ? $this->getPrep()->getId() : null;
    }
}
