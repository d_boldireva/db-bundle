<?php
declare(strict_types=1);

namespace TeamSoft\CrmRepositoryBundle\Entity\SaleProject;

use DateTime;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use TeamSoft\CrmRepositoryBundle\Entity\InfoCompany;
use TeamSoft\CrmRepositoryBundle\Entity\InfoDistributor;
use TeamSoft\CrmRepositoryBundle\Entity\InfoTask;
use TeamSoft\CrmRepositoryBundle\Entity\InfoUser;
use TeamSoft\CrmRepositoryBundle\Entity\ServiceFieldInterface;

/**
 * Class InfoTasksaleproject
 * @ORM\Table(name="info_tasksaleproject")
 * @ORM\Entity(repositoryClass="TeamSoft\CrmRepositoryBundle\Repository\SaleProject\InfoTasksaleprojectRepository")
 */
class InfoTasksaleproject implements ServiceFieldInterface
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string|null
     *
     * @ORM\Column(name="guid", type="guid", nullable=true)
     */
    private $guid;

    /**
     * @var DateTime|null
     *
     * @ORM\Column(name="currenttime", type="datetime", nullable=true)
     */
    private $currenttime;

    /**
     * @var string|null
     *
     * @ORM\Column(name="moduser", type="string", length=16, nullable=true)
     */
    private $moduser;

    /**
     * @var InfoTask|null
     * @ORM\ManyToOne(targetEntity="\TeamSoft\CrmRepositoryBundle\Entity\InfoTask")
     * @ORM\JoinColumns({
     *      @ORM\JoinColumn(name="task_id", referencedColumnName="id")
     * })
     */
    private $task;

    /**
     * @var InfoSaleproject|null
     * @ORM\ManyToOne(targetEntity="InfoSaleproject")
     * @ORM\JoinColumns({
     *      @ORM\JoinColumn(name="saleproject_id", referencedColumnName="id")
     * })
     */
    private $saleProject;

    /**
     * @var InfoDistributor|null
     * @ORM\ManyToOne(targetEntity="\TeamSoft\CrmRepositoryBundle\Entity\InfoDistributor")
     * @ORM\JoinColumns({
     *      @ORM\JoinColumn(name="distributor_id", referencedColumnName="id")
     * })
     */
    private $distributor;

    /**
     * @var InfoUser|null
     * @ORM\ManyToOne(targetEntity="\TeamSoft\CrmRepositoryBundle\Entity\InfoUser")
     * @ORM\JoinColumns({
     *      @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     * })
     */
    private $user;

    /**
     * @var InfoCompany|null
     * @ORM\ManyToOne(targetEntity="\TeamSoft\CrmRepositoryBundle\Entity\InfoCompany")
     * @ORM\JoinColumns({
     *      @ORM\JoinColumn(name="company_id", referencedColumnName="id")
     * })
     */
    private $company;

    /**
     * @var DateTime|null
     *
     * @ORM\Column(name="dt", type="datetime", nullable=true)
     */
    private $dt;

    /**
     * @var integer|null
     *
     * @ORM\Column(name="approvementstatus", type="integer", nullable=true)
     */
    private $approvementStatus;

    /**
     * @var InfoTasksaleprojectprep[]|ArrayCollection|[]
     *
     * @ORM\OneToMany(targetEntity="InfoTasksaleprojectprep", mappedBy="taskSaleProject")
     *
     */
    private $taskSaleProjectPreps;

    public function __construct()
    {
        $this->taskSaleProjectPreps = new ArrayCollection();
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getCurrenttime(): ?DateTime
    {
        return $this->currenttime;
    }

    public function setCurrenttime(?DateTime $value): self
    {
        $this->currenttime = $value;
        return $this;
    }

    public function getGuid(): ?string
    {
        return $this->guid;
    }

    public function setGuid($value): self
    {
        $this->guid = $value;
        return $this;
    }

    public function getModuser(): ?string
    {
        return $this->moduser;
    }

    public function setModuser($value = null): self
    {
        $this->moduser = $value;
        return $this;
    }

    public function getTask(): ?InfoTask
    {
        return $this->task;
    }

    public function setTask(?InfoTask $task): self
    {
        $this->task = $task;
        return $this;
    }

    public function getSaleProject(): ?InfoSaleProject
    {
        return $this->saleProject;
    }

    public function setSaleProject(?InfoSaleProject $saleProject): self
    {
        $this->saleProject = $saleProject;
        return $this;
    }

    public function getDistributor(): ?InfoDistributor
    {
        return $this->distributor;
    }

    public function setDistributor(?InfoDistributor $distributor): self
    {
        $this->distributor = $distributor;
        return $this;
    }

    public function getUser(): ?InfoUser
    {
        return $this->user;
    }

    public function setUser(?InfoUser $user): self
    {
        $this->user = $user;
        return $this;
    }

    public function getCompany(): ?InfoCompany
    {
        return $this->company;
    }

    public function setCompany(?InfoCompany $company): self
    {
        $this->company = $company;
        return $this;
    }

    public function getDt(): ?DateTime
    {
        return $this->dt;
    }

    public function setDt(?DateTime $dt): self
    {
        $this->dt = $dt;
        return $this;
    }

    public function getApprovementStatus(): ?int
    {
        return $this->approvementStatus;
    }

    public function setApprovementStatus(?int $approvementStatus): self
    {
        $this->approvementStatus = $approvementStatus;
        return $this;
    }

    /**
     * Get TaskSaleProjectPreps
     *
     * @return ArrayCollection|InfoTasksaleprojectprep[]
     */
    public function getTaskSaleProjectPreps()
    {
        return $this->taskSaleProjectPreps;
    }

    /**
     * Add TaskSaleProjectPrep
     *
     * @param InfoTasksaleprojectprep $taskSaleProjectPrep
     * @return $this
     */
    public function addTaskSaleProjectPrep(InfoTasksaleprojectprep $taskSaleProjectPrep): self
    {
        if (!$this->taskSaleProjectPreps->contains($taskSaleProjectPrep)) {
            $this->taskSaleProjectPreps->add($taskSaleProjectPrep);
        }
        return $this;
    }

    /**
     * Remove TaskSaleProjectPrep
     *
     * @param InfoTasksaleprojectprep $taskSaleProjectPrep
     * @return $this
     */
    public function removeTaskSaleProjectPrep(InfoTasksaleprojectprep $taskSaleProjectPrep): self
    {
        if ($this->taskSaleProjectPreps->contains($taskSaleProjectPrep)) {
            $this->taskSaleProjectPreps->removeElement($taskSaleProjectPrep);
        }
        return $this;
    }

    public function getTaskId(): ?int
    {
        return $this->getTask() ? $this->getTask()->getId() : null;
    }

    public function getSaleProjectId(): ?int
    {
        return $this->getSaleProject() ? $this->getSaleProject()->getId() : null;
    }

    public function getDistributorId(): ?int
    {
        return $this->getDistributor() ? $this->getDistributor()->getId() : null;
    }

    public function getDistributorName(): ?string
    {
        return $this->getDistributor() ? $this->getDistributor()->getName() : null;
    }

    public function getUserId(): ?int
    {
        return $this->getUser() ? $this->getUser()->getId() : null;
    }

    public function getCompanyId(): ?int
    {
        return $this->getCompany() ? $this->getCompany()->getId() : null;
    }

}
