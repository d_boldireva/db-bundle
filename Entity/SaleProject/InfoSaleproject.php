<?php

namespace TeamSoft\CrmRepositoryBundle\Entity\SaleProject;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use TeamSoft\CrmRepositoryBundle\Entity\ServiceFieldInterface;

/**
 * Class InfoSaleproject
 * @package TeamSoft\CrmRepositoryBundle\Entity\SaleProject
 *
 * @ORM\Table(name="info_saleproject")
 * @ORM\Entity(repositoryClass="TeamSoft\CrmRepositoryBundle\Repository\SaleProject\InfoSaleprojectRepository")
 */
class InfoSaleproject implements ServiceFieldInterface
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id()
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     * @ORM\Column(name="name", type="string", length=255, nullable=true)
     */
    private $name;

    /**
     * @var \DateTime
     * @ORM\Column(name="datefrom", type="datetime", nullable=true)
     */
    private $datefrom;

    /**
     * @var \DateTime
     * @ORM\Column(name="datetill" , type="datetime", nullable=true)
     */
    private $datetill;

    /**
     * @var string
     * @ORM\Column(name="description", type="string", length=1024, nullable=true)
     */
    private $description;

    /**
     * @var int
     * @ORM\Column(name="state", type="integer", nullable=true)
     */
    private $state;

    /**
     * @var ArrayCollection
     * @ORM\OneToMany(targetEntity="InfoSaleprojectprep", mappedBy="saleproject", cascade={"persist","remove"})
     */
    private $saleProjectPreps;


    /**
     * @var string
     *
     * @ORM\Column(name="guid", type="guid", nullable=true)
     */
    private $guid;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="currenttime", type="datetime", nullable=true)
     */
    private $currenttime;

    /**
     * @var string
     *
     * @ORM\Column(name="moduser", type="string", length=16, nullable=true)
     */
    private $moduser;


    public function __construct()
    {
        $this->saleProjectPreps = new ArrayCollection();
    }


    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param string $name
     * @return mixed
     */
    public function setName($name)
    {
        $this->name = $name;
        return $name;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param \DateTime $datefrom
     * @return $this
     */
    public function setDatefrom(\DateTime $datefrom)
    {
        $this->datefrom = $datefrom;
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getDatefrom()
    {
        return $this->datefrom;
    }

    /**
     * @param \DateTime $datetill
     * @return $this
     */
    public function setDatetill(\DateTime $datetill)
    {
        $this->datetill = $datetill;
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getDatetill()
    {
        return $this->datetill;
    }

    /**
     * @param string|null $description
     * @return $this
     */
    public function setDescription($description)
    {
        $this->description = $description;
        return $this;
    }

    /**
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param int|null $state
     * @return $this
     */
    public function setState($state = null)
    {
        $this->state = $state;
        return $this;
    }

    /**
     * @return int
     */
    public function getState()
    {
        return $this->state;
    }

    /**
     * @return ArrayCollection
     */
    public function getSaleProjectPreps()
    {
        return $this->saleProjectPreps;
    }

    /**
     * @param InfoSaleprojectprep $saleprojectprep
     * @return self
     */
    public function addSaleProjectPrep(InfoSaleprojectprep $saleprojectprep): self
    {
        if (!$this->saleProjectPreps->contains($saleprojectprep)) {
            $this->saleProjectPreps->add($saleprojectprep);
        }

        $saleprojectprep->setSaleproject($this);

        return $this;
    }

    /**
     * @param InfoSaleprojectprep $saleprojectprep
     * @return self
     */
    public function removeSaleProjectPrep(InfoSaleprojectprep $saleprojectprep): self
    {
        if ($this->saleProjectPreps->contains($saleprojectprep)) {
            $this->saleProjectPreps->removeElement($saleprojectprep);
            $saleprojectprep->setSaleproject(null);
        }

        return $this;
    }


    /**
     * Set guid
     *
     * @param string $guid
     * @return $this
     */
    public function setGuid($guid)
    {
        $this->guid = $guid;

        return $this;
    }

    /**
     * Get guid
     *
     * @return string
     */
    public function getGuid()
    {
        return $this->guid;
    }

    /**
     * Set currenttime
     *
     * @param \DateTime $currenttime
     * @return $this
     */
    public function setCurrenttime(\DateTime $currenttime = null)
    {
        $this->currenttime = $currenttime;

        return $this;
    }

    /**
     * Get currenttime
     *
     * @return \DateTime
     */
    public function getCurrenttime()
    {
        return $this->currenttime;
    }

    /**
     * Set moduser
     *
     * @param string $moduser
     * @return $this
     */
    public function setModuser($moduser)
    {
        $this->moduser = $moduser;

        return $this;
    }

    /**
     * Get moduser
     *
     * @return string
     */
    public function getModuser()
    {
        return $this->moduser;
    }
}
