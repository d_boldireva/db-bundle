<?php

namespace TeamSoft\CrmRepositoryBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * InfoMcmfrom
 *
 * @ORM\Table(name="info_mcmfrom")
 * @ORM\Entity(repositoryClass="TeamSoft\CrmRepositoryBundle\Repository\InfoMcmfromRepository")
 */
class InfoMcmfrom implements ServiceFieldInterface
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="email", type="string", length=255)
     */
    private $email;

    /**
     * @var string
     *
     * @ORM\Column(name="replyto", type="string", length=255, nullable=true)
     */
    private $replyto;

    /**
     * @var string
     *
     * @ORM\Column(name="headerimage", type="string", length=255, nullable=true)
     */
    private $headerimage;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="currenttime", type="datetime", nullable=true)
     */
    private $currenttime;

    /**
     * @var string
     *
     * @ORM\Column(name="moduser", type="string", length=16, nullable=true)
     */
    private $moduser;

    /**
     * @var string
     *
     * @ORM\Column(name="guid", type="guid", nullable=true)
     */
    private $guid;

    /**
     * @var string|null
     *
     * @ORM\Column(name="type", type="string", length=255, nullable=true)
     */
    private $type;

    /**
     * @var int|null
     *
     * @ORM\Column(name="isdefault", type="integer", nullable=true)
     */
    private $isdefault;

    /**
     * @var string|null
     *
     * @ORM\Column(name="alphaname", type="string", length=255, nullable=true)
     */
    private $alphaName;

    /**
     * @var InfoCountry|null
     *
     * @ORM\ManyToOne(targetEntity="InfoCountry")
     * @ORM\JoinColumn(name="country_id", referencedColumnName="id")
     */
    private $country;

    /**
     * @var string|null
     *
     * @ORM\Column(name="provider_login", type="string", length=255, nullable=true)
     */
    private $providerLogin;

    /**
     * @var string|null
     *
     * @ORM\Column(name="provider_password", type="string", length=255, nullable=true)
     */
    private $providerPassword;

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string|null $name
     *
     * @return InfoMcmfrom
     */
    public function setName(?string $name): self
    {
        $this->name = $name;
    
        return $this;
    }

    /**
     * Get name
     *
     * @return string|null
     */
    public function getName(): ?string
    {
        return $this->name;
    }

    /**
     * Set email
     *
     * @param string|null $email
     *
     * @return InfoMcmfrom
     */
    public function setEmail(?string $email): self
    {
        $this->email = $email;
    
        return $this;
    }

    /**
     * Get email
     *
     * @return string|null
     */
    public function getEmail(): ?string
    {
        return $this->email;
    }

    /**
     * Set replyto
     *
     * @param string|null $replyto
     *
     * @return InfoMcmfrom
     */
    public function setReplyto($replyto): self
    {
        $this->replyto = $replyto;
    
        return $this;
    }

    /**
     * Get replyto
     *
     * @return string|null
     */
    public function getReplyto(): ?string
    {
        return $this->replyto;
    }

    /**
     * Set headerimage
     *
     * @param string|null $headerimage
     *
     * @return InfoMcmfrom
     */
    public function setHeaderimage(?string $headerimage): self
    {
        $this->headerimage = $headerimage;
    
        return $this;
    }

    /**
     * Get headerimage
     *
     * @return string|null
     */
    public function getHeaderimage(): ?string
    {
        return $this->headerimage;
    }

    /**
     * Set currenttime
     *
     * @param \DateTime $currenttime
     *
     * @return InfoMcmfrom
     */
    public function setCurrenttime(\DateTime $currenttime): self
    {
        $this->currenttime = $currenttime;
    
        return $this;
    }

    /**
     * Get currenttime
     *
     * @return \DateTime
     */
    public function getCurrenttime()
    {
        return $this->currenttime;
    }

    /**
     * Set moduser
     *
     * @param string $moduser
     *
     * @return InfoMcmfrom
     */
    public function setModuser($moduser): self
    {
        $this->moduser = $moduser;
    
        return $this;
    }

    /**
     * Get moduser
     *
     * @return string
     */
    public function getModuser(): string
    {
        return $this->moduser;
    }

    /**
     * Set guid
     *
     * @param string $guid
     *
     * @return InfoMcmfrom
     */
    public function setGuid($guid): self
    {
        $this->guid = $guid;
    
        return $this;
    }

    /**
     * Get guid
     *
     * @return string
     */
    public function getGuid(): string
    {
        return $this->guid;
    }

    /**
     * Set type
     *
     * @param string|null $type
     *
     * @return InfoMcmfrom
     */
    public function setType(?string $type): self
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type
     *
     * @return string|null
     */
    public function getType(): ?string
    {
        return $this->type;
    }

    /**
     * Set isdefault
     *
     * @param int|null $isdefault
     *
     * @return InfoMcmfrom
     */
    public function setIsdefault(?int $isdefault): self
    {
        $this->isdefault = $isdefault;

        return $this;
    }

    /**
     * Get isdefault
     *
     * @return int|null
     */
    public function getIsdefault(): ?int
    {
        return $this->isdefault;
    }

    /**
     * Set alphaName
     *
     * @param string|null $alphaName
     *
     * @return InfoMcmfrom
     */
    public function setAlphaName(?string $alphaName): self
    {
        $this->alphaName = $alphaName;

        return $this;
    }

    /**
     * Get alphaName
     *
     * @return string|null
     */
    public function getAlphaName(): ?string
    {
        return $this->alphaName;
    }

    /**
     * Set country
     *
     * @param InfoCountry $country|null
     *
     * @return InfoMcmfrom
     */
    public function setCountry(?InfoCountry $country): self
    {
        $this->country = $country;

        return $this;
    }

    /**
     * Get country
     *
     * @return InfoCountry|null
     */
    public function getCountry(): ?InfoCountry
    {
        return $this->country;
    }

    /**
     * Set providerLogin
     *
     * @param string|null $providerLogin
     *
     * @return InfoMcmfrom
     */
    public function setProviderLogin(?string $providerLogin): self
    {
        $this->providerLogin = $providerLogin;

        return $this;
    }

    /**
     * Get providerLogin
     *
     * @return string|null
     */
    public function getProviderLogin(): ?string
    {
        return $this->providerLogin;
    }

    /**
     * Set providerPassword
     *
     * @param string|null $providerPassword
     *
     * @return InfoMcmfrom
     */
    public function setProviderPassword(?string $providerPassword): self
    {
        $this->providerPassword = $providerPassword;

        return $this;
    }

    /**
     * Get providerPassword
     *
     * @return string|null
     */
    public function getProviderPassword(): ?string
    {
        return $this->providerPassword;
    }
}
