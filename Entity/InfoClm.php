<?php

namespace TeamSoft\CrmRepositoryBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use TeamSoft\CrmRepositoryBundle\Model\InfoFile;

/**
 * InfoClm
 *
 * @ORM\Table(name="info_clm")
 * @ORM\Entity(repositoryClass="TeamSoft\CrmRepositoryBundle\Repository\InfoClm")
 */
class InfoClm extends InfoFile implements ServiceFieldInterface
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="Name", type="string", length=255, nullable=true)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="filename", type="string", length=255, nullable=true)
     */
    private $filename;

    /**
     * @var string
     *
     * @ORM\Column(name="fileext", type="string", length=50, nullable=true)
     */
    private $fileext;

    /**
     * @var resource | string
     *
     * @ORM\Column(name="Content", type="blob_or_string", nullable=true)
     */
    private $content;

    /**
     * @var string
     *
     * @ORM\Column(name="guid", type="guid", nullable=true)
     */
    private $guid;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="currenttime", type="datetime", nullable=true)
     */
    private $currenttime;

    /**
     * @var string
     *
     * @ORM\Column(name="moduser", type="string", length=16, nullable=true)
     */
    private $moduser;

    /**
     * @var int
     *
     * @ORM\Column(name="isarchive", type="integer", nullable=true)
     */
    private $isarchive;

    /**
     * @var int
     *
     * @ORM\Column(name="fsize", type="integer", nullable=true)
     */
    private $fsize;

    /**
     * @var int
     *
     * @ORM\Column(name="issendmail", type="integer", nullable=true)
     */
    private $issendmail;

    /**
     * @ORM\ManyToMany(targetEntity="InfoClmgroup")
     * @ORM\JoinTable(name="info_clmingroup",
     *      joinColumns={@ORM\JoinColumn(name="clm_id", referencedColumnName="id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="group_id", referencedColumnName="id", unique=true)}
     *      )
     */
    private $clmGroups;

    /**
     * @ORM\OneToMany(targetEntity="InfoTaskpresentation", mappedBy="presentation", cascade={"persist"}, orphanRemoval=true)
     */
    private $taskPresentations;

    /**
     * @ORM\OneToMany(targetEntity="InfoTaskpresentationslide", mappedBy="presentation", cascade={"persist"}, orphanRemoval=true)
     */
    private $taskPresentationSlides;

    /**
     * @ORM\OneToMany(targetEntity="InfoClmviewlog", mappedBy="clm", cascade={"persist"}, orphanRemoval=true)
     */
    private $clmViewLogs;

    /**
     * @ORM\OneToMany(targetEntity="InfoClmanswer", mappedBy="clm", cascade={"persist"}, orphanRemoval=true)
     */
    private $clmAnswers;

    /**
     * @ORM\OneToMany(targetEntity="InfoClmingroup", mappedBy="clm", cascade={"persist"}, orphanRemoval=true)
     */
    private $clmInGroups;

    /**
     * @ORM\OneToMany(targetEntity="InfoClmmap", mappedBy="clm", cascade={"persist"}, orphanRemoval=true)
     */
    private $clmMaps;

    /**
     * @ORM\ManyToOne(targetEntity="InfoCustomdictionaryvalue")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="efile_id", referencedColumnName="id")
     * })
     */
    private $efile;

    /**
     * @var string|null
     * @ORM\Column(name="pmac_number", type="string", nullable=true)
     */
    private $pmacNumber;

    public function __construct()
    {
        $this->clmGroups = new ArrayCollection();
        $this->taskPresentations = new ArrayCollection();
        $this->taskPresentationSlides = new ArrayCollection();
        $this->clmViewLogs = new ArrayCollection();
        $this->clmAnswers = new ArrayCollection();
        $this->clmInGroups = new ArrayCollection();
        $this->clmMaps = new ArrayCollection();
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return InfoClm
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set filename
     *
     * @param string $filename
     *
     * @return InfoClm
     */
    public function setFilename($filename)
    {
        $this->filename = $filename;

        return $this;
    }

    /**
     * Get filename
     *
     * @return string
     */
    public function getFilename()
    {
        return $this->filename;
    }

    /**
     * Set fileext
     *
     * @param string $fileext
     *
     * @return InfoClm
     */
    public function setFileext($fileext)
    {
        $this->fileext = $fileext;

        return $this;
    }

    /**
     * Get fileext
     *
     * @return string
     */
    public function getFileext()
    {
        return $this->fileext;
    }

    /**
     * Set content
     *
     * @param string $content
     *
     * @return InfoClm
     */
    public function setContent($content)
    {
        $this->content = $content;

        return $this;
    }

    /**
     * Get content
     *
     * @return resource | string
     */
    public function getContent()
    {
        return $this->content;
    }

    /**
     * Set guid
     *
     * @param string $guid
     * @return InfoClm
     */
    public function setGuid($guid)
    {
        $this->guid = $guid;

        return $this;
    }

    /**
     * Get guid
     *
     * @return string
     */
    public function getGuid()
    {
        return $this->guid;
    }

    /**
     * Set currenttime
     *
     * @param \DateTime $currenttime
     * @return InfoClm
     */
    public function setCurrenttime(\DateTime $currenttime)
    {
        $this->currenttime = $currenttime;

        return $this;
    }

    /**
     * Get currenttime
     *
     * @return \DateTime|string
     */
    public function getCurrenttime()
    {
        return $this->currenttime;
    }

    /**
     * Set moduser
     *
     * @param string $moduser
     * @return InfoClm
     */
    public function setModuser($moduser)
    {
        $this->moduser = $moduser;

        return $this;
    }

    /**
     * Get moduser
     *
     * @return string
     */
    public function getModuser()
    {
        return $this->moduser;
    }

    /**
     * Set isarchive
     *
     * @param int $isarchive
     *
     * @return InfoClm
     */
    public function setIsarchive($isarchive)
    {
        $this->isarchive = $isarchive;

        return $this;
    }

    /**
     * Get isarchive
     *
     * @return int
     */
    public function getIsarchive()
    {
        return $this->isarchive;
    }

    /**
     * Set issendmail
     *
     * @param int $issendmail
     *
     * @return InfoClm
     */
    public function setIssendmail($issendmail)
    {
        $this->issendmail = $issendmail;

        return $this;
    }

    /**
     * Get issendmail
     *
     * @return int
     */
    public function getIssendmail()
    {
        return $this->issendmail;
    }

    /**
     * @return ArrayCollection|InfoClmgroup[]|Collection
     */
    public function getClmGroups(): Collection
    {
        return $this->clmGroups;
    }

    /**
     * Add $infoClmgroup
     *
     * @param InfoClmgroup $infoClmgroup
     *
     * @return self
     */
    public function addClmGroup(InfoClmgroup $infoClmgroup): self
    {
        if (!$this->clmGroups->contains($infoClmgroup)) {
            $this->clmGroups->add($infoClmgroup);
        }

        return $this;
    }

    /**
     * Remove $infoClmgroup
     *
     * @param InfoClmgroup $infoClmgroup
     */
    public function removeClmGroup(InfoClmgroup $infoClmgroup)
    {
        $this->clmGroups->removeElement($infoClmgroup);
    }

    /**
     * @return ArrayCollection|InfoTaskpresentation[]|Collection
     */
    public function getTaskPresentations(): Collection
    {
        return $this->taskPresentations;
    }

    /**
     * Add $taskPresentation
     *
     * @param InfoTaskpresentation $taskPresentation
     *
     * @return self
     */
    public function addTaskPresentation(InfoTaskpresentation $taskPresentation): self
    {
        if (!$this->taskPresentations->contains($taskPresentation)) {
            $this->taskPresentations->add($taskPresentation);
        }

        return $this;
    }

    /**
     * Remove $taskPresentation
     *
     * @param InfoTaskpresentation $taskPresentation
     */
    public function removeTaskPresentation(InfoTaskpresentation $taskPresentation)
    {
        $this->taskPresentations->removeElement($taskPresentation);
    }

    /**
     * @return ArrayCollection|InfoTaskpresentationslide[]|Collection
     */
    public function getTaskPresentationSlides(): Collection
    {
        return $this->taskPresentationSlides;
    }

    /**
     * Add $taskPresentationSlide
     *
     * @param InfoTaskpresentationslide $taskPresentationSlide
     *
     * @return self
     */
    public function addTaskPresentationSlide(InfoTaskpresentationslide $taskPresentationSlide): self
    {
        if (!$this->taskPresentationSlides->contains($taskPresentationSlide)) {
            $this->taskPresentationSlides->add($taskPresentationSlide);
        }

        return $this;
    }

    /**
     * Remove $taskPresentationSlide
     *
     * @param InfoTaskpresentationslide $taskPresentationSlide
     */
    public function removeTaskPresentationSlide(InfoTaskpresentationslide $taskPresentationSlide)
    {
        $this->taskPresentationSlides->removeElement($taskPresentationSlide);
    }

    /**
     * @return ArrayCollection|InfoClmviewlog[]|Collection
     */
    public function getClmViewLogs(): Collection
    {
        return $this->clmViewLogs;
    }

    /**
     * Add $clmViewLog
     *
     * @param InfoClmviewlog $clmViewLog
     *
     * @return self
     */
    public function addClmViewLog(InfoClmviewlog $clmViewLog): self
    {
        if (!$this->clmViewLogs->contains($clmViewLog)) {
            $this->clmViewLogs->add($clmViewLog);
        }

        return $this;
    }

    /**
     * Remove $clmViewLog
     *
     * @param InfoClmviewlog $clmViewLog
     */
    public function removeClmViewLog(InfoClmviewlog $clmViewLog)
    {
        $this->clmViewLogs->removeElement($clmViewLog);
    }

    /**
     * @return ArrayCollection|InfoClmanswer[]|Collection
     */
    public function getClmanswers(): Collection
    {
        return $this->clmAnswers;
    }

    /**
     * Add $clmAnswer
     *
     * @param InfoClmanswer $clmAnswer
     *
     * @return self
     */
    public function addClmAnswer(InfoClmanswer $clmAnswer): self
    {
        if (!$this->clmAnswers->contains($clmAnswer)) {
            $this->clmAnswers->add($clmAnswer);
        }

        return $this;
    }

    /**
     * Remove $clmAnswer
     *
     * @param InfoClmanswer $clmAnswer
     */
    public function removeClmAnswer(InfoClmanswer $clmAnswer)
    {
        $this->clmAnswers->removeElement($clmAnswer);
    }

    /**
     * @return ArrayCollection|InfoClmingroup[]|Collection
     */
    public function getClmInGroups(): Collection
    {
        return $this->clmInGroups;
    }

    /**
     * Add $clmInGroup
     *
     * @param InfoClmingroup $clmInGroup
     *
     * @return self
     */
    public function addClmInGroup(InfoClmingroup $clmInGroup): self
    {
        if (!$this->clmInGroups->contains($clmInGroup)) {
            $this->clmInGroups->add($clmInGroup);
        }

        return $this;
    }

    /**
     * Remove $clmInGroup
     *
     * @param InfoClmingroup $clmInGroup
     */
    public function removeClmInGroup(InfoClmingroup $clmInGroup)
    {
        $this->clmInGroups->removeElement($clmInGroup);
    }

    /**
     * @return ArrayCollection|InfoClmmap[]|Collection
     */
    public function getClmMaps(): Collection
    {
        return $this->clmMaps;
    }

    /**
     * Add $clmMap
     *
     * @param InfoClmmap $clmMap
     *
     * @return self
     */
    public function addClmMap(InfoClmmap $clmMap): self
    {
        if (!$this->clmMaps->contains($clmMap)) {
            $this->clmMaps->add($clmMap);
        }

        return $this;
    }

    /**
     * Remove $clmMap
     *
     * @param InfoClmmap $clmMap
     */
    public function removeClmMap(InfoClmmap $clmMap)
    {
        $this->clmMaps->removeElement($clmMap);
    }

    /**
     * Set fsize.
     *
     * @param int|null $fsize
     *
     * @return InfoClmgroup
     */
    public function setFsize($fsize = null)
    {
        $this->fsize = $fsize;

        return $this;
    }

    /**
     * Get fsize.
     *
     * @return int|null
     */
    public function getFsize()
    {
        return $this->fsize;
    }

    public function getEfile(): ?InfoCustomdictionaryvalue
    {
        return $this->efile;
    }

    public function setEfile(?InfoCustomdictionaryvalue $efile): self
    {
        $this->efile = $efile;
        return $this;
    }

    public function getPmacNumber(): ?string
    {
        return $this->pmacNumber;
    }

    public function setPmacNumber(?string $pmacNumber): self
    {
        $this->pmacNumber = $pmacNumber;
        return $this;
    }

    public function getEfileId(): ?int
    {
        return $this->efile ? $this->efile->id : null;
    }
}
