<?php

namespace TeamSoft\CrmRepositoryBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * McmWebinarcontact
 *
 * @ORM\Table(name="mcm_webinarcontact")
 * @ORM\Entity
 */
class McmWebinarcontact
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var int|null
     *
     * @ORM\Column(name="morionid", type="integer", nullable=true)
     */
    private $morionid;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="currenttime", type="datetime", nullable=true)
     */
    private $currenttime = 'CURRENT_TIMESTAMP';

    /**
     * @var string|null
     *
     * @ORM\Column(name="moduser", type="string", length=16, nullable=true)
     */
    private $moduser = '[dbo].[Get_CurrentCode]()';

    /**
     * @var string|null
     *
     * @ORM\Column(name="type", type="string", length=255, nullable=true)
     */
    private $type;

    /**
     * @var string|null
     *
     * @ORM\Column(name="link", type="string", length=255, nullable=true)
     */
    private $link;

    /**
     * @var \McmWebinar
     *
     * @ORM\ManyToOne(targetEntity="McmWebinar")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="mcm_webinar_id", referencedColumnName="id")
     * })
     */
    private $mcmWebinar;

    /**
     * @var int|null
     *
     * @ORM\Column(name="status_new", type="integer", nullable=true)
     */
    private $statusNew;

    /**
     * @var int|null
     *
     * @ORM\Column(name="status_send", type="integer", nullable=true)
     */
    private $statusSend;

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set morionid.
     *
     * @param int|null $morionid
     *
     * @return McmWebinarcontact
     */
    public function setMorionid($morionid = null)
    {
        $this->morionid = $morionid;

        return $this;
    }

    /**
     * Get morionid.
     *
     * @return int|null
     */
    public function getMorionid()
    {
        return $this->morionid;
    }

    /**
     * Set currenttime.
     *
     * @param \DateTime|null $currenttime
     *
     * @return McmWebinarcontact
     */
    public function setCurrenttime($currenttime = null)
    {
        $this->currenttime = $currenttime;

        return $this;
    }

    /**
     * Get currenttime.
     *
     * @return \DateTime|null
     */
    public function getCurrenttime()
    {
        return $this->currenttime;
    }

    /**
     * Set moduser.
     *
     * @param string|null $moduser
     *
     * @return McmWebinarcontact
     */
    public function setModuser($moduser = null)
    {
        $this->moduser = $moduser;

        return $this;
    }

    /**
     * Get moduser.
     *
     * @return string|null
     */
    public function getModuser()
    {
        return $this->moduser;
    }

    /**
     * Set type.
     *
     * @param string|null $type
     *
     * @return McmWebinarcontact
     */
    public function setType($type = null)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type.
     *
     * @return string|null
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set link.
     *
     * @param string|null $link
     *
     * @return McmWebinarcontact
     */
    public function setLink($link = null)
    {
        $this->link = $link;

        return $this;
    }

    /**
     * Get link.
     *
     * @return string|null
     */
    public function getLink()
    {
        return $this->link;
    }

    /**
     * Set mcmWebinar.
     *
     * @param \TeamSoft\CrmRepositoryBundle\Entity\McmWebinar|null $mcmWebinar
     *
     * @return McmWebinarcontact
     */
    public function setMcmWebinar(\TeamSoft\CrmRepositoryBundle\Entity\McmWebinar $mcmWebinar = null)
    {
        $this->mcmWebinar = $mcmWebinar;

        return $this;
    }

    /**
     * Get mcmWebinar.
     *
     * @return \TeamSoft\CrmRepositoryBundle\Entity\McmWebinar|null
     */
    public function getMcmWebinar()
    {
        return $this->mcmWebinar;
    }

    /**
     * Set statusNew.
     *
     * @param int|null $statusNew
     *
     * @return McmWebinarcontact
     */
    public function setStatusNew($statusNew = null)
    {
        $this->statusNew = $statusNew;

        return $this;
    }

    /**
     * Get statusNew.
     *
     * @return int|null
     */
    public function getStatusNew()
    {
        return $this->statusNew;
    }

    /**
     * Set statusSend.
     *
     * @param int|null $statusSend
     *
     * @return McmWebinarcontact
     */
    public function setStatusSend($statusSend = null)
    {
        $this->statusSend = $statusSend;

        return $this;
    }

    /**
     * Get statusSend.
     *
     * @return int|null
     */
    public function getStatusSend()
    {
        return $this->statusSend;
    }
}
