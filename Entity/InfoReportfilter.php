<?php

namespace TeamSoft\CrmRepositoryBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * InfoReportfilter
 *
 * @ORM\Table(name="info_reportfilter")
 * @ORM\Entity
 */
class InfoReportfilter implements ServiceFieldInterface
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="Name", type="string", length=100, nullable=false)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="Filter", type="string", length=255, nullable=true)
     */
    private $filter;

    /**
     * @var int
     *
     * @ORM\Column(name="FieldDataType", type="integer", nullable=true)
     */
    private $fielddatatype;

    /**
     * @var string
     *
     * @ORM\Column(name="guid", type="guid", nullable=true)
     */
    private $guid;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="currenttime", type="datetime", nullable=true)
     */
    private $currenttime;

    /**
     * @var string
     *
     * @ORM\Column(name="moduser", type="string", length=16, nullable=true)
     */
    private $moduser;

    /**
     * @var string
     *
     * @ORM\Column(name="customWhere", type="string", length=255, nullable=true)
     */
    private $customwhere;

    /**
     * @var InfoCustomdictionary
     *
     * @ORM\ManyToOne(targetEntity="InfoCustomdictionary")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="CustomDictionary_id", referencedColumnName="id")
     * })
     */
    private $customdictionary;

    /**
     * @var InfoSystemdictionary
     *
     * @ORM\ManyToOne(targetEntity="InfoSystemdictionary")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="systemdictionary_id", referencedColumnName="id")
     * })
     */
    private $systemdictionary;



    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return InfoReportfilter
     */
    public function setName($name)
    {
        $this->name = $name;
    
        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set filter
     *
     * @param string $filter
     *
     * @return InfoReportfilter
     */
    public function setFilter($filter)
    {
        $this->filter = $filter;
    
        return $this;
    }

    /**
     * Get filter
     *
     * @return string
     */
    public function getFilter()
    {
        return $this->filter;
    }

    /**
     * Set fielddatatype
     *
     * @param int $fielddatatype
     *
     * @return InfoReportfilter
     */
    public function setFielddatatype($fielddatatype)
    {
        $this->fielddatatype = $fielddatatype;
    
        return $this;
    }

    /**
     * Get fielddatatype
     *
     * @return int
     */
    public function getFielddatatype()
    {
        return $this->fielddatatype;
    }

    /**
     * Set guid
     *
     * @param string $guid
     *
     * @return InfoReportfilter
     */
    public function setGuid($guid)
    {
        $this->guid = $guid;
    
        return $this;
    }

    /**
     * Get guid
     *
     * @return string
     */
    public function getGuid()
    {
        return $this->guid;
    }

    /**
     * Set currenttime
     *
     * @param \DateTime $currenttime
     *
     * @return InfoReportfilter
     */
    public function setCurrenttime(\DateTime $currenttime)
    {
        $this->currenttime = $currenttime;
    
        return $this;
    }

    /**
     * Get currenttime
     *
     * @return \DateTime
     */
    public function getCurrenttime()
    {
        return $this->currenttime;
    }

    /**
     * Set moduser
     *
     * @param string $moduser
     *
     * @return InfoReportfilter
     */
    public function setModuser($moduser)
    {
        $this->moduser = $moduser;
    
        return $this;
    }

    /**
     * Get moduser
     *
     * @return string
     */
    public function getModuser()
    {
        return $this->moduser;
    }

    /**
     * Set customwhere
     *
     * @param string $customwhere
     *
     * @return InfoReportfilter
     */
    public function setCustomwhere($customwhere)
    {
        $this->customwhere = $customwhere;
    
        return $this;
    }

    /**
     * Get customwhere
     *
     * @return string
     */
    public function getCustomwhere()
    {
        return $this->customwhere;
    }

    /**
     * Set customdictionary
     *
     * @param \TeamSoft\CrmRepositoryBundle\Entity\InfoCustomdictionary $customdictionary
     *
     * @return InfoReportfilter
     */
    public function setCustomdictionary(InfoCustomdictionary $customdictionary = null)
    {
        $this->customdictionary = $customdictionary;
    
        return $this;
    }

    /**
     * Get customdictionary
     *
     * @return \TeamSoft\CrmRepositoryBundle\Entity\InfoCustomdictionary
     */
    public function getCustomdictionary()
    {
        return $this->customdictionary;
    }

    /**
     * Set systemdictionary
     *
     * @param \TeamSoft\CrmRepositoryBundle\Entity\InfoSystemdictionary $systemdictionary
     *
     * @return InfoReportfilter
     */
    public function setSystemdictionary(InfoSystemdictionary $systemdictionary = null)
    {
        $this->systemdictionary = $systemdictionary;
    
        return $this;
    }

    /**
     * Get systemdictionary
     *
     * @return \TeamSoft\CrmRepositoryBundle\Entity\InfoSystemdictionary
     */
    public function getSystemdictionary()
    {
        return $this->systemdictionary;
    }
}
