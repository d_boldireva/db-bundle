<?php

declare(strict_types=1);

namespace TeamSoft\CrmRepositoryBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="info_skladinquiry")
 */
class InfoStorageInquiry
{
    /**
     * @var int|null
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private ?int $id;

    /**
     * @var \TeamSoft\CrmRepositoryBundle\Entity\InfoStorage|null
     *
     * @ORM\ManyToOne(targetEntity="InfoStorage")
     * @ORM\JoinColumn(name="sklad_id", referencedColumnName="id")
     */
    private ?InfoStorage $storage;

    /**
     * @var \TeamSoft\CrmRepositoryBundle\Entity\InfoPromomaterial|null
     *
     * @ORM\ManyToOne(targetEntity="InfoPromomaterial")
     * @ORM\JoinColumn(name="promo_id", referencedColumnName="id")
     */
    private ?InfoPromomaterial $promoMaterial;

    /**
     * @var int|null
     *
     * @ORM\Column(name="cnt", type="integer", nullable=true)
     */
    private ?int $quantity;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="date1", type="date", nullable=true)
     */
    private ?\DateTime $requestDate;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="date2", type="date", nullable=true)
     */
    private ?\DateTime $receiveDate;

    /**
     * @var string|null
     *
     * @ORM\Column(name="descr", type="text", nullable=true)
     */
    private ?string $description;

    /**
     * @var int|null
     *
     * @ORM\Column(name="ismove", type="integer", nullable=true)
     */
    private ?int $isMoved;

    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return \TeamSoft\CrmRepositoryBundle\Entity\InfoStorage|null
     */
    public function getStorage(): ?InfoStorage
    {
        return $this->storage;
    }

    /**
     * @param  \TeamSoft\CrmRepositoryBundle\Entity\InfoStorage|null  $storage
     *
     * @return $this
     */
    public function setStorage(?InfoStorage $storage): self
    {
        $this->storage = $storage;

        return $this;
    }

    /**
     * @return \TeamSoft\CrmRepositoryBundle\Entity\InfoPromomaterial|null
     */
    public function getPromoMaterial(): ?InfoPromomaterial
    {
        return $this->promoMaterial;
    }

    /**
     * @param  \TeamSoft\CrmRepositoryBundle\Entity\InfoPromomaterial|null  $promoMaterial
     *
     * @return $this
     */
    public function setPromoMaterial(?InfoPromomaterial $promoMaterial): self
    {
        $this->promoMaterial = $promoMaterial;

        return $this;
    }

    /**
     * @return int|null
     */
    public function getQuantity(): ?int
    {
        return $this->quantity;
    }

    /**
     * @param  int|null  $quantity
     *
     * @return $this
     */
    public function setQuantity(?int $quantity): self
    {
        $this->quantity = $quantity;

        return $this;
    }

    /**
     * @return \DateTime|null
     */
    public function getRequestDate(): ?\DateTime
    {
        return $this->requestDate;
    }

    /**
     * @param  \DateTime|null  $requestDate
     *
     * @return $this
     */
    public function setRequestDate(?\DateTime $requestDate): self
    {
        $this->requestDate = $requestDate;

        return $this;
    }

    /**
     * @return \DateTime|null
     */
    public function getReceiveDate(): ?\DateTime
    {
        return $this->receiveDate;
    }

    /**
     * @param  \DateTime|null  $receiveDate
     *
     * @return $this
     */
    public function setReceiveDate(?\DateTime $receiveDate): self
    {
        $this->receiveDate = $receiveDate;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getDescription(): ?string
    {
        return $this->description;
    }

    /**
     * @param  string|null  $description
     *
     * @return $this
     */
    public function setDescription(?string $description): self
    {
        $this->description = $description;

        return $this;
    }

    /**
     * @return int|null
     */
    public function getIsMoved(): ?int
    {
        return $this->isMoved;
    }

    /**
     * @param  int|null  $isMoved
     *
     * @return $this
     */
    public function setIsMoved(?int $isMoved): self
    {
        $this->isMoved = $isMoved;

        return $this;
    }
}
