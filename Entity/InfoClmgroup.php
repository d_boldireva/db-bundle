<?php

namespace TeamSoft\CrmRepositoryBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use TeamSoft\CrmRepositoryBundle\Entity\ServiceFieldInterface;

/**
 * InfoClmgroup
 *
 * @ORM\Table(name="info_clmgroup")
 * @ORM\Entity(repositoryClass="TeamSoft\CrmRepositoryBundle\Repository\InfoClmgroup")
 */
class InfoClmgroup implements ServiceFieldInterface
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255, nullable=false)
     */
    private $name;

    /**
     * @var int
     *
     * @ORM\Column(name="root", type="integer", nullable=true)
     */
    private $root;

    /**
     * @var guid
     *
     * @ORM\Column(name="guid", type="guid", nullable=true)
     */
    private $guid;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="currenttime", type="datetime", nullable=true)
     */
    private $currenttime;

    /**
     * @var string
     *
     * @ORM\Column(name="moduser", type="string", length=16, nullable=true)
     */
    private $moduser;

    /**
     * @var int
     *
     * @ORM\Column(name="flags", type="integer", nullable=true)
     */
    private $flags;

    /**
     * @var int|null
     *
     * @ORM\Column(name="isarchive", type="integer", nullable=true)
     */
    private $isarchive;

    /**
     * @var InfoClmgroup
     *
     * @ORM\ManyToOne(targetEntity="InfoClmgroup", cascade={"remove"})
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="parent_id", referencedColumnName="id")
     * })
     */
    private $parent;


    /**
     * @ORM\OneToMany(targetEntity="InfoClmgroupdirection", mappedBy="clmgroup")
     */
    private $clmGroupdirection;

    /**
     * @ORM\ManyToMany(targetEntity="InfoDirection")
     * @ORM\JoinTable(name="info_clmgroupdirection",
     *   joinColumns={@ORM\JoinColumn(name="clmgroup_id", referencedColumnName="id")},
     *   inverseJoinColumns={@ORM\JoinColumn(name="direction_id", referencedColumnName="id")}
     * )
     */
    private $directions;

    /**
     * @var InfoClm
     *
     * @ORM\ManyToMany(targetEntity="\TeamSoft\CrmRepositoryBundle\Entity\InfoClm")
     * @ORM\JoinTable(
     *   name="info_clmingroup",
     *   joinColumns={@ORM\JoinColumn(name="group_id", referencedColumnName="id")},
     *   inverseJoinColumns={@ORM\JoinColumn(name="clm_id", referencedColumnName="id")}
     * )
     */
    private $clmCollection;

    /**
     * @var InfoClmingroup
     *
     * @ORM\OneToMany(targetEntity="InfoClmingroup", mappedBy="group")
     */
    private $clmInGroupCollection;


    public function __construct()
    {
        $this->clmCollection = new ArrayCollection();
        $this->clmInGroupCollection = new ArrayCollection();
        $this->directions = new ArrayCollection();
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return InfoClmgroup
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set root
     *
     * @param int $root
     *
     * @return InfoClmgroup
     */
    public function setRoot($root)
    {
        $this->root = $root;

        return $this;
    }

    /**
     * Get root
     *
     * @return int
     */
    public function getRoot()
    {
        return $this->root;
    }

    /**
     * Set guid
     *
     * @param guid $guid
     *
     * @return InfoClmgroup
     */
    public function setGuid($guid)
    {
        $this->guid = $guid;

        return $this;
    }

    /**
     * Get guid
     *
     * @return guid
     */
    public function getGuid()
    {
        return $this->guid;
    }

    /**
     * Set currenttime
     *
     * @param \DateTime $currenttime
     *
     * @return InfoClmgroup
     */
    public function setCurrenttime(\DateTime $currenttime)
    {
        $this->currenttime = $currenttime;

        return $this;
    }

    /**
     * Get currenttime
     *
     * @return \DateTime
     */
    public function getCurrenttime()
    {
        return $this->currenttime;
    }

    /**
     * Set moduser
     *
     * @param string $moduser
     *
     * @return InfoClmgroup
     */
    public function setModuser($moduser)
    {
        $this->moduser = $moduser;

        return $this;
    }

    /**
     * Get moduser
     *
     * @return string
     */
    public function getModuser()
    {
        return $this->moduser;
    }

    /**
     * Set flags
     *
     * @param int $flags
     *
     * @return InfoClmgroup
     */
    public function setFlags($flags)
    {
        $this->flags = $flags;

        return $this;
    }

    /**
     * Get flags
     *
     * @return int
     */
    public function getFlags()
    {
        return $this->flags;
    }

    /**
     * Set parent
     *
     * @param \TeamSoft\CrmRepositoryBundle\Entity\InfoClmgroup $parent
     *
     * @return InfoClmgroup
     */
    public function setParent(\TeamSoft\CrmRepositoryBundle\Entity\InfoClmgroup $parent = null)
    {
        $this->parent = $parent;

        return $this;
    }

    /**
     * Get parent
     *
     * @return \TeamSoft\CrmRepositoryBundle\Entity\InfoClmgroup
     */
    public function getParent()
    {
        return $this->parent;
    }

    /**
     * Set clmdirection
     *
     * @param \TeamSoft\CrmRepositoryBundle\Entity\InfoClmgroupdirection $clmGroupdirection
     *
     * @return InfoClmgroup
     */
    public function setClmgroupdirection(\TeamSoft\CrmRepositoryBundle\Entity\InfoClmgroupdirection $clmGroupdirection = null)
    {
        $this->clmGroupdirection = $clmGroupdirection;

        return $this;
    }

    /**
     * Get clmGroupdirection
     *
     * @return \TeamSoft\CrmRepositoryBundle\Entity\InfoClmgroupdirection
     */
    public function getClmgroupdirection()
    {
        return $this->clmGroupdirection;
    }

    /**
     * Add clmCollection
     *
     * @param \TeamSoft\CrmRepositoryBundle\Entity\InfoClm $clm
     *
     * @return InfoClmgroup
     */
    public function addClmCollection(InfoClm $clm)
    {
        $this->clmCollection[] = $clm;

        return $this;
    }

    /**
     * Remove clmCollection
     *
     * @param \TeamSoft\CrmRepositoryBundle\Entity\InfoClm $clm
     */
    public function removeDirectionCollection(InfoClm $clm)
    {
        $this->clmCollection->removeElement($clm);
    }

    /**
     * Get clmCollection
     *
     * @return ArrayCollection
     */
    public function getClmCollection()
    {
        return $this->clmCollection;
    }

    /**
     * Add direction
     *
     * @param InfoDirection $direction
     *
     * @return InfoClmgroup
     */
    public function addDirection(InfoDirection $direction)
    {
        $this->directions->add($direction);
        return $this;
    }

    /**
     * Remove direction
     *
     * @param InfoDirection $direction
     */
    public function removeDirection(InfoDirection $direction)
    {
        $this->directions->removeElement($direction);
    }

    /**
     * Get directions
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getDirections()
    {
        return $this->directions;
    }

    /**
     * Add InfoClmingroup
     *
     * @param InfoClmingroup $clmInGroup
     *
     * @return InfoClmgroup
     */
    public function addClmInGroupCollection(InfoClmingroup $clmInGroup)
    {
        $this->clmInGroupCollection[] = $clmInGroup;

        return $this;
    }

    /**
     * Get clmInGoupCollection
     *
     * @return ArrayCollection
     */
    public function getClmInGroupCollection()
    {
        return $this->clmInGroupCollection;
    }

    /**
     * @return int|null
     */
    public function getIsarchive(): ?int
    {
        return $this->isarchive;
    }

    /**
     * @param int $isarchive
     * @return self
     */
    public function setIsarchive(?int $isarchive): self
    {
        $this->isarchive = $isarchive;
        return $this;
    }

    public function isArchive(): bool
    {
        return (bool)$this->getIsarchive();
    }

    public function isRoot(): bool
    {
        return (bool)$this->getRoot();
    }
}
