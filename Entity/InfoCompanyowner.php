<?php

namespace TeamSoft\CrmRepositoryBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * InfoCompanyowner
 *
 * @ORM\Table(name="info_companyowner")
 * @ORM\Entity
 */
class InfoCompanyowner implements ServiceFieldInterface
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="guid", type="guid", nullable=true)
     */
    private $guid;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="currenttime", type="datetime", nullable=true)
     */
    private $currenttime;

    /**
     * @var string
     *
     * @ORM\Column(name="moduser", type="string", length=16, nullable=true)
     */
    private $moduser;

    /**
     * @var integer
     *
     * @ORM\Column(name="isuse", type="integer", nullable=true)
     */
    private $isuse;

    /**
     * @var InfoCompany
     *
     * @ORM\ManyToOne(targetEntity="InfoCompany", inversedBy="companyOwnerCollection")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="company_id", referencedColumnName="id")
     * })
     */
    private $company;

    /**
     * @var InfoUser
     *
     * @ORM\ManyToOne(targetEntity="InfoUser")
     * @ORM\JoinColumn(name="owner_id", referencedColumnName="id")
     */
    private $owner;

    /**
     * @var integer
     *
     * @ORM\Column(name="ismust", type="integer", nullable=true)
     */
    private $ismust;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set guid
     *
     * @param string $guid
     * @return InfoCompanyowner
     */
    public function setGuid($guid)
    {
        $this->guid = $guid;

        return $this;
    }

    /**
     * Get guid
     *
     * @return string
     */
    public function getGuid()
    {
        return $this->guid;
    }

    /**
     * Set currenttime
     *
     * @param \DateTime $currenttime
     * @return InfoCompanyowner
     */
    public function setCurrenttime(\DateTime $currenttime)
    {
        $this->currenttime = $currenttime;

        return $this;
    }

    /**
     * Get currenttime
     *
     * @return \DateTime
     */
    public function getCurrenttime()
    {
        return $this->currenttime;
    }

    /**
     * Set moduser
     *
     * @param string $moduser
     * @return InfoCompanyowner
     */
    public function setModuser($moduser)
    {
        $this->moduser = $moduser;

        return $this;
    }

    /**
     * Get moduser
     *
     * @return string
     */
    public function getModuser()
    {
        return $this->moduser;
    }

    /**
     * Set isuse
     *
     * @param integer $isuse
     * @return InfoCompanyowner
     */
    public function setIsuse($isuse)
    {
        $this->isuse = $isuse;

        return $this;
    }

    /**
     * Get isuse
     *
     * @return integer
     */
    public function getIsuse()
    {
        return $this->isuse;
    }

    /**
     * Set company
     *
     * @param \TeamSoft\CrmRepositoryBundle\Entity\InfoCompany $company
     * @return InfoCompanyowner
     */
    public function setCompany(InfoCompany $company = null)
    {
        $this->company = $company;

        return $this;
    }

    /**
     * Get company
     *
     * @return \TeamSoft\CrmRepositoryBundle\Entity\InfoCompany
     */
    public function getCompany()
    {
        return $this->company;
    }

    /**
     * Set owner
     *
     * @param \TeamSoft\CrmRepositoryBundle\Entity\InfoUser $owner
     * @return InfoCompanyowner
     */
    public function setOwner(InfoUser $owner = null)
    {
        $this->owner = $owner;

        return $this;
    }

    /**
     * Get owner
     *
     * @return \TeamSoft\CrmRepositoryBundle\Entity\InfoUser
     */
    public function getOwner()
    {
        return $this->owner;
    }

    /**
    * Set ismust
    *
    * @param integer $ismust
    * @return InfoCompanyowner
    */
    public function setIsmust($ismust)
    {
        $this->ismust = $ismust;

        return $this;
    }

    /**
     * Get ismust
     *
     * @return integer
     */
    public function getIsmust()
    {
        return $this->ismust;
    }
}
