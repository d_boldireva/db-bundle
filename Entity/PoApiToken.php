<?php

namespace TeamSoft\CrmRepositoryBundle\Entity;

use Symfony\Component\Security\Core\User\UserInterface;
use TeamSoft\CrmRepositoryBundle\Repository\PoApiTokenRepository;
use Doctrine\ORM\Mapping as ORM;

//#[ORM\Table(name: "po_apitoken")]
//#[ORM\Entity(repositoryClass: PoApiTokenRepository::class)]
/**
 * @ORM\Table(name="po_apitoken")
 * @ORM\Entity(repositoryClass=PoApiTokenRepository::class)
 */
class PoApiToken implements ServiceFieldInterface
{
//    #[ORM\Id]
//    #[ORM\GeneratedValue]
//    #[ORM\Column(type: "integer")]
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

//    #[ORM\Column(type: "string", length: 255)]
    /**
     * @ORM\Column(type="string", length=255)
     */
    private $token;

//    #[ORM\Column(name: "expires_at", type: "datetime")]
    /**
     * @ORM\Column(name="expires_at", type="datetime")
     */
    private $expiresAt;

//    #[ORM\ManyToOne(targetEntity: InfoUser::class, inversedBy: "poApiTokens")]
//    #[ORM\JoinColumn(name: "user_id", referencedColumnName: "id")]
    /**
     * @ORM\ManyToOne(targetEntity=InfoUser::class, inversedBy="poApiTokens")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     */
    private $user;

//    #[ORM\Column(type: "datetime")]
    /**
     * @ORM\Column(type="datetime")
     */
    private $currenttime;

//    #[ORM\Column(type: "string", length: 16)]
    /**
     * @ORM\Column(type="string", length=16)
     */
    private $moduser;

//    #[ORM\Column(name: "guid", type: "guid")]
    /**
     * @ORM\Column(name="guid", type="guid")
     */
    private $guid;

    public function __construct(UserInterface $user, \DateTime $expiresAt = null)
    {
        $this->token = bin2hex(random_bytes(25));
        $this->user = $user;
        $this->expiresAt = $expiresAt ?? new \DateTime('+1 hour');
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getToken(): ?string
    {
        return $this->token;
    }

    public function setToken(string $token): self
    {
        $this->token = $token;

        return $this;
    }

    public function getExpiresAt(): ?\DateTimeInterface
    {
        return $this->expiresAt;
    }

    public function setExpiresAt(\DateTimeInterface $expiresAt): self
    {
        $this->expiresAt = $expiresAt;

        return $this;
    }

    public function getUser(): ?UserInterface
    {
        return $this->user;
    }

    public function setUser(?UserInterface $infoUser): self
    {
        $this->user = $infoUser;

        return $this;
    }

    public function setCurrenttime(\DateTime $currenttime): self
    {
        $this->currenttime = $currenttime;

        return $this;
    }

    public function getCurrenttime(): ?\DateTimeInterface
    {
        return $this->currenttime;
    }

    public function setModuser($moduser): self
    {
        $this->moduser = $moduser;

        return $this;
    }

    public function getModuser(): ?string
    {
        return $this->moduser;
    }

    public function setGuid($guid): self
    {
        $this->guid = $guid;

        return $this;
    }

    public function getGuid(): ?string
    {
        return $this->guid;
    }

    public function isExpired(): bool
    {
        $currentDateTime = new \DateTime();
        return $this->getExpiresAt() <= $currentDateTime;
    }
}
