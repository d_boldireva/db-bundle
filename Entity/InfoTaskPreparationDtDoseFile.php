<?php

namespace TeamSoft\CrmRepositoryBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use TeamSoft\CrmRepositoryBundle\Model\InfoFile;

/**
 * InfoTaskPreparationDtDoseFile
 *
 * @ORM\Table(name="info_taskpreparationdtdosefile")
 * @ORM\Entity
 */
class InfoTaskPreparationDtDoseFile extends InfoFile implements ServiceFieldInterface
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var InfoTaskPreparationDtDose|null
     *
     * @ORM\OneToOne(targetEntity="InfoTaskPreparationDtDose", inversedBy="preparationDtDose")
     * @ORM\JoinColumn(name="taskpreparationdtdose_id", referencedColumnName="id")
     */
    private $taskPreparationDtDose;

    /**
     * @var string|null
     * @ORM\Column(name="filetype", type="string", length=16, nullable=true)
     */
    private $fileType;

    /**
     * @var mixed
     * @ORM\Column(name="request", type="blob_or_string", nullable=true)
     */
    private $request;

    /**
     * @var string|null
     *
     * @ORM\Column(name="guid", type="guid", nullable=true)
     */
    private $guid;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="currenttime", type="datetime", nullable=true)
     */
    private $currenttime;

    /**
     * @var string|null
     *
     * @ORM\Column(name="moduser", type="string", length=16, nullable=true)
     */
    private $moduser;

    /**
     * @return int
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return InfoTaskPreparationDtDose|null
     */
    public function getTaskPreparationDtDose(): ?InfoTaskPreparationDtDose
    {
        return $this->taskPreparationDtDose;
    }

    /**
     * @param InfoTaskPreparationDtDose|null $taskPreparationDtDose
     * @return self
     */
    public function setTaskPreparationDtDose(?InfoTaskPreparationDtDose $taskPreparationDtDose): self
    {
        $this->taskPreparationDtDose = $taskPreparationDtDose;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getFileType(): ?string
    {
        return $this->fileType;
    }

    /**
     * @param string|null $fileType
     * @return self
     */
    public function setFileType(?string $fileType): self
    {
        $this->fileType = $fileType;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getRequest()
    {
        return $this->request;
    }

    /**
     * @param mixed $request
     * @return self
     */
    public function setRequest($request): self
    {
        $this->request = $request;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getGuid(): ?string
    {
        return $this->guid;
    }

    /**
     * @param string|null $guid
     * @return self
     */
    public function setGuid($guid): self
    {
        $this->guid = $guid;
        return $this;
    }

    /**
     * @return \DateTime|null
     */
    public function getCurrenttime(): ?\DateTime
    {
        return $this->currenttime;
    }

    /**
     * @param \DateTime|null $currenttime
     * @return self
     */
    public function setCurrenttime(?\DateTime $currenttime): self
    {
        $this->currenttime = $currenttime;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getModuser(): ?string
    {
        return $this->moduser;
    }

    /**
     * @param string|null $moduser
     * @return self
     */
    public function setModuser($moduser): self
    {
        $this->moduser = $moduser;
        return $this;
    }

    public function getName()
    {
        return '';
    }

    public function getContent()
    {
        return $this->getRequest();
    }

    public function setContent($content)
    {
        $this->setRequest($content);

        return $this;
    }
}
