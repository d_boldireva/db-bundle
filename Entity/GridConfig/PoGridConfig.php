<?php
declare(strict_types=1);

namespace TeamSoft\CrmRepositoryBundle\Entity\GridConfig;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use TeamSoft\CrmRepositoryBundle\Entity\ServiceFieldInterface;

/**
 * PoGridConfig
 *
 * @ORM\Table(name="po_gridconfig")
 * @ORM\Entity(repositoryClass="TeamSoft\CrmRepositoryBundle\Repository\PoGridConfig\PoGridConfigRepository")
 */
class PoGridConfig implements ServiceFieldInterface
{

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var int|null
     *
     * @ORM\Column(name="page", type="integer", nullable=true)
     */
    private $page;

    /**
     * @var int|null
     *
     * @ORM\Column(name="subpage", type="integer", nullable=true)
     */
    private $subpage;

    /**
     * @var string|null
     *
     * @ORM\Column(name="field_code", type="string", length=255, nullable=true)
     */
    private $fieldCode;

    /**
     * @var string|null
     *
     * @ORM\Column(name="field_name", type="string", length=255, nullable=true)
     */
    private $fieldName;

    /**
     * @var int|null
     *
     * @ORM\Column(name="sort_order", type="integer", nullable=true)
     */
    private $sortOrder;

    /**
     * @var int|null
     *
     * @ORM\Column(name="is_shown", type="integer", nullable=true)
     */
    private $isShown;

    /**
     * @var int|null
     *
     * @ORM\Column(name="is_shown_dcr", type="integer", nullable=true)
     */
    private $isShownDcr;

    /**
     * @var string|null
     *
     * @ORM\Column(name="serializer_groups", type="json", nullable=true)
     */
    private $serializerGroups;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="currenttime", type="datetime", nullable=true)
     */
    private $currenttime;

    /**
     * @var string|null
     *
     * @ORM\Column(
     *     name="moduser",
     *     type="string",
     *     length=16,
     *     nullable=true
     * )
     */
    private $moduser;

    /**
     * @var string|null
     *
     * @ORM\Column(name="guid", type="guid", nullable=true)
     */
    private $guid;

    /**
     * @var PoGridConfigByRole[]|Collection
     * @ORM\OneToMany(targetEntity="TeamSoft\CrmRepositoryBundle\Entity\GridConfig\PoGridConfigByRole", mappedBy="gridConfig")
     */
    private $gridConfigByRoles;

    /**
     * @var PoGridConfigByUser[]|Collection
     * @ORM\OneToMany(targetEntity="TeamSoft\CrmRepositoryBundle\Entity\GridConfig\PoGridConfigByUser", mappedBy="gridConfig")
     */
    private $gridConfigByUsers;

    /**
     * @var integer|null
     * @ORM\Column(name="field_width", type="integer", nullable=true)
     */
    private $fieldWidth;

    public function __construct()
    {
        $this->gridConfigByRoles = new ArrayCollection();
        $this->gridConfigByUsers = new ArrayCollection();
    }

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set page.
     *
     * @param int|null $page
     *
     * @return self
     */
    public function setPage(?int $page): self
    {
        $this->page = $page;

        return $this;
    }

    /**
     * Get page.
     *
     * @return int
     */
    public function getPage()
    {
        return $this->page;
    }

    /**
     * Set subpage.
     *
     * @param int|null $subpage
     *
     * @return self
     */
    public function setSubpage(?int $subpage): self
    {
        $this->subpage = $subpage;

        return $this;
    }

    /**
     * Get subpage.
     *
     * @return int
     */
    public function getSubpage()
    {
        return $this->subpage;
    }

    /**
     * Set fieldCode.
     *
     * @param string|null $fieldCode
     *
     * @return self
     */
    public function setFieldCode(?string $fieldCode): self
    {
        $this->fieldCode = $fieldCode;

        return $this;
    }

    /**
     * Get fieldCode.
     *
     * @return string|null
     */
    public function getFieldCode()
    {
        return $this->fieldCode;
    }

    /**
     * Set fieldName.
     *
     * @param string|null $fieldName
     *
     * @return self
     */
    public function setFieldName(?string $fieldName): self
    {
        $this->fieldName = $fieldName;

        return $this;
    }

    /**
     * Get fieldName.
     *
     * @return string|null
     */
    public function getFieldName()
    {
        return $this->fieldName;
    }

    /**
     * Set sortOrder.
     *
     * @param int|null $sortOrder
     *
     * @return self
     */
    public function setSortOrder(?string $sortOrder): self
    {
        $this->sortOrder = $sortOrder;

        return $this;
    }

    /**
     * Get order.
     *
     * @return int
     */
    public function getSortOrder()
    {
        return $this->sortOrder;
    }

    /**
     * Set isShown.
     *
     * @param int|null $isShown
     *
     * @return self
     */
    public function setIsShown(?int $isShown): self
    {
        $this->isShown = $isShown;

        return $this;
    }

    /**
     * Get isShown.
     *
     * @return int|null
     */
    public function getIsShown(): ?int
    {
        return $this->isShown;
    }

    /**
     * Set serializerGroups.
     *
     * @param string|null $serializerGroups
     *
     * @return self
     */
    public function setSerializerGroups(?string $serializerGroups): self
    {
        $this->serializerGroups = $serializerGroups;

        return $this;
    }

    /**
     * Get serializerGroups.
     *
     * @return string|null
     */
    public function getSerializerGroups()
    {
        return $this->serializerGroups;
    }

    /**
     * Set currenttime.
     *
     * @param \DateTime|null $currenttime
     *
     * @return self
     */
    public function setCurrenttime(?\DateTime $currenttime): self
    {
        $this->currenttime = $currenttime;

        return $this;
    }

    /**
     * Get currenttime.
     *
     * @return \DateTime|null
     */
    public function getCurrenttime()
    {
        return $this->currenttime;
    }

    /**
     * Set moduser.
     *
     * @param string|null $moduser
     *
     * @return self
     */
    public function setModuser($moduser = null): self
    {
        $this->moduser = $moduser;

        return $this;
    }

    /**
     * Get moduser.
     *
     * @return string|null
     */
    public function getModuser()
    {
        return $this->moduser;
    }

    /**
     * Set guid.
     *
     * @param string|null $guid
     *
     * @return self
     */
    public function setGuid($guid = null): self
    {
        $this->guid = $guid;

        return $this;
    }

    /**
     * Get guid.
     *
     * @return string|null
     */
    public function getGuid()
    {
        return $this->guid;
    }

    /**
     * @return Collection|PoGridConfigByRole[]
     */
    public function getGridConfigByRoles(): Collection
    {
        return $this->gridConfigByRoles;
    }

    public function addGridConfigByRole(PoGridConfigByRole $gridConfigByRole): self
    {
        if (!$this->gridConfigByRoles->contains($gridConfigByRole)) {
            $this->gridConfigByRoles->add($gridConfigByRole);

            $gridConfigByRole->setGridConfig($this);
        }

        return $this;
    }

    public function removeGridConfigByRole(PoGridConfigByRole $gridConfigByRole): self
    {
        if ($this->gridConfigByRoles->removeElement($gridConfigByRole)) {
            $gridConfigByRole->setGridConfig(null);
        }

        return $this;
    }

    public function addGridConfigByUser(PoGridConfigByUser $gridConfigByUser): self
    {
        if (!$this->gridConfigByUsers->contains($gridConfigByUser)) {
            $this->gridConfigByUsers->add($gridConfigByUser);

            $gridConfigByUser->setGridConfig($this);
        }

        return $this;
    }

    public function removeGridConfigByUser(PoGridConfigByUser $gridConfigByUser): self
    {
        if ($this->gridConfigByUsers->removeElement($gridConfigByUser)) {
            $gridConfigByUser->setGridConfig(null);
        }

        return $this;
    }

    /**
     * @return Collection|PoGridConfigByUser[]
     */
    public function getGridConfigByUsers(): Collection
    {
        return $this->gridConfigByUsers;
    }

    public function getFieldWidth(): ?int
    {
        return $this->fieldWidth;
    }

    public function setFieldWidth(?int $fieldWidth): self
    {
        $this->fieldWidth = $fieldWidth;
        return $this;
    }

    public function getIsShownDcr(): ?int
    {
        return $this->isShownDcr;
    }

    public function setIsShownDcr(?int $isShownDcr): self
    {
        $this->isShownDcr = $isShownDcr;
        return $this;
    }
}
