<?php
declare(strict_types=1);

namespace TeamSoft\CrmRepositoryBundle\Entity\GridConfig;


use Doctrine\ORM\Mapping as ORM;
use TeamSoft\CrmRepositoryBundle\Entity\InfoRole;
use TeamSoft\CrmRepositoryBundle\Entity\ServiceFieldInterface;

/**
 * PoGridConfigByRole
 *
 * @ORM\Table(name="po_gridconfigbyrole")
 * @ORM\Entity(repositoryClass="TeamSoft\CrmRepositoryBundle\Repository\PoGridConfig\PoGridConfigByRoleRepository")
 */
class PoGridConfigByRole implements ServiceFieldInterface
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="currenttime", type="datetime", nullable=true)
     */
    private $currenttime;

    /**
     * @var string|null
     *
     * @ORM\Column(
     *     name="moduser",
     *     type="string",
     *     length=16,
     *     nullable=true
     * )
     */
    private $moduser;

    /**
     * @var string|null
     *
     * @ORM\Column(name="guid", type="guid", nullable=true)
     */
    private $guid;

    /**
     * @var PoGridConfig|null
     * @ORM\ManyToOne(targetEntity="PoGridConfig", inversedBy="gridConfigByRoles")
     * @ORM\JoinColumn(name="gridconfig_id", referencedColumnName="id")
     */
    public $gridConfig;

    /**
     * @var InfoRole|null
     * @ORM\ManyToOne(targetEntity="TeamSoft\CrmRepositoryBundle\Entity\InfoRole", inversedBy="gridConfigByRoles")
     * @ORM\JoinColumn(name="role_id", referencedColumnName="id")
     */
    public $role;

    /**
     * @var integer|null
     *
     * @ORM\Column(name="sort_order", type="integer", nullable=true)
     */
    private $sortOrder;

    /**
     * @var integer|null
     *
     * @ORM\Column(name="is_shown", type="integer", nullable=true)
     */
    private $isShown;

    /**
     * @var integer|null
     *
     * @ORM\Column(name="is_shown_dcr", type="integer", nullable=true)
     */
    private $isShownDcr;


    /**
     * @var integer|null
     * @ORM\Column(name="field_width", type="integer", nullable=true)
     */
    private $fieldWidth;

    public function __clone()
    {
        $this->id = null;
        $this->guid = null;
        $this->currenttime = null;
    }

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set currenttime.
     *
     * @param \DateTime|null $currenttime
     *
     * @return self
     */
    public function setCurrenttime(\DateTime $currenttime = null): self
    {
        $this->currenttime = $currenttime;

        return $this;
    }

    /**
     * Get currenttime.
     *
     * @return \DateTime|null
     */
    public function getCurrenttime()
    {
        return $this->currenttime;
    }

    /**
     * Set moduser.
     *
     * @param string|null $moduser
     *
     * @return self
     */
    public function setModuser($moduser = null): self
    {
        $this->moduser = $moduser;

        return $this;
    }

    /**
     * Get moduser.
     *
     * @return string|null
     */
    public function getModuser()
    {
        return $this->moduser;
    }

    /**
     * Set guid.
     *
     * @param string|null $guid
     *
     * @return self
     */
    public function setGuid($guid = null): self
    {
        $this->guid = $guid;

        return $this;
    }

    /**
     * Get guid.
     *
     * @return string|null
     */
    public function getGuid()
    {
        return $this->guid;
    }

    /**
     * @return PoGridConfig|null
     */
    public function getGridConfig(): ?PoGridConfig
    {
        return $this->gridConfig;
    }

    /**
     * @param PoGridConfig|null $gridConfig
     * @return self
     */
    public function setGridConfig(?PoGridConfig $gridConfig): self
    {
        $this->gridConfig = $gridConfig;
        return $this;
    }

    /**
     * @return InfoRole|null
     */
    public function getRole(): ?InfoRole
    {
        return $this->role;
    }

    /**
     * @param InfoRole|null $role
     * @return self
     */
    public function setRole(?InfoRole $role): self
    {
        $this->role = $role;
        return $this;
    }

    /**
     * @return int|null
     */
    public function getSortOrder(): ?int
    {
        return $this->sortOrder;
    }

    /**
     * @param int|null $sortOrder
     * @return self
     */
    public function setSortOrder(?int $sortOrder): self
    {
        $this->sortOrder = $sortOrder;
        return $this;
    }

    /**
     * @return int|null
     */
    public function getIsShown(): ?int
    {
        return $this->isShown;
    }

    /**
     * @param int|null $isShown
     * @return self
     */
    public function setIsShown(?int $isShown): self
    {
        $this->isShown = $isShown;
        return $this;
    }

    public function getIsShownDcr(): ?int
    {
        return $this->isShownDcr;
    }

    public function setIsShownDcr(?int $isShownDcr): self
    {
        $this->isShownDcr = $isShownDcr;
        return $this;
    }

    /**
     * @return int|null
     */
    public function getRoleId(): ?int
    {
        return $this->getRole() ? $this->getRole()->getId() : null;
    }

    /**
     * @return int|null
     */
    public function getGridConfigId(): ?int
    {
        return $this->getGridConfig() ? $this->getGridConfig()->getId() : null;
    }

    public function getFieldWidth(): ?int
    {
        return $this->fieldWidth;
    }

    public function setFieldWidth(?int $fieldWidth): self
    {
        $this->fieldWidth = $fieldWidth;
        return $this;
    }
}
