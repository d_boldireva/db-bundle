<?php

namespace TeamSoft\CrmRepositoryBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * InfoMcmconferencedetail
 *
 * @ORM\Table(name="info_mcmconferencedetail")
 * @ORM\Entity
 */
class InfoMcmconferencedetail implements ServiceFieldInterface
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="promocode", type="string", length=255, nullable=true)
     */
    private $promocode;

    /**
     * @var string
     *
     * @ORM\Column(name="url", type="string", length=255, nullable=true)
     */
    private $url;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="currenttime", type="datetime", nullable=true)
     */
    private $currenttime;

    /**
     * @var string
     *
     * @ORM\Column(name="moduser", type="string", length=16, nullable=true)
     */
    private $moduser;

    /**
     * @var string
     *
     * @ORM\Column(name="guid", type="guid", nullable=true)
     */
    private $guid;

    /**
     * @var InfoMcmconference
     *
     * @ORM\ManyToOne(targetEntity="InfoMcmconference", inversedBy="details")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="conference_id", referencedColumnName="id", nullable=true)
     * })
     */
    private $conference;

    /**
     * @var InfoCustomdictionaryvalue
     *
     * @ORM\ManyToOne(targetEntity="InfoCustomdictionaryvalue")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="country_id", referencedColumnName="id")
     * })
     */
    private $country;

    /**
     * @var ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="InfoMcmconferencedetailformat", mappedBy="detail", cascade={"persist"})
     */
    private $formats;

    public function __construct()
    {
        $this->formats = new ArrayCollection();
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set promocode
     *
     * @param string $promocode
     *
     * @return InfoMcmconferencedetail
     */
    public function setPromocode($promocode)
    {
        $this->promocode = $promocode;

        return $this;
    }

    /**
     * Get promocode
     *
     * @return string
     */
    public function getPromocode()
    {
        return $this->promocode;
    }

    /**
     * Set url
     *
     * @param string $url
     *
     * @return InfoMcmconferencedetail
     */
    public function setUrl($url)
    {
        $this->url = $url;

        return $this;
    }

    /**
     * Get url
     *
     * @return string
     */
    public function getUrl()
    {
        return $this->url;
    }

    /**
     * Set conference
     *
     * @param InfoMcmconference|null $conference
     *
     * @return InfoMcmconferencedetail
     */
    public function setConference($conference)
    {
        $this->conference = $conference;

        return $this;
    }

    /**
     * Get conference
     *
     * @return InfoMcmconference
     */
    public function getConference()
    {
        return $this->conference;
    }

    /**
     * Set currenttime
     *
     * @param \DateTime $currenttime
     *
     * @return InfoMcmconferencedetail
     */
    public function setCurrenttime(\DateTime $currenttime)
    {
        $this->currenttime = $currenttime;

        return $this;
    }

    /**
     * Get currenttime
     *
     * @return \DateTime
     */
    public function getCurrenttime()
    {
        return $this->currenttime;
    }

    /**
     * Set moduser
     *
     * @param string $moduser
     *
     * @return InfoMcmconferencedetail
     */
    public function setModuser($moduser)
    {
        $this->moduser = $moduser;

        return $this;
    }

    /**
     * Get moduser
     *
     * @return string
     */
    public function getModuser()
    {
        return $this->moduser;
    }

    /**
     * Set guid
     *
     * @param string $guid
     *
     * @return InfoMcmconferencedetail
     */
    public function setGuid($guid)
    {
        $this->guid = $guid;

        return $this;
    }

    /**
     * Get guid
     *
     * @return string
     */
    public function getGuid()
    {
        return $this->guid;
    }

    /**
     * Add format
     *
     * @param InfoMcmconferencedetailformat $format
     *
     * @return InfoMcmconference
     */
    public function addFormat(InfoMcmconferencedetailformat $format)
    {
        $this->formats->add($format);
        $format->setDetail($this);

        return $this;
    }

    /**
     * Remove format
     *
     * @param InfoMcmconferencedetailformat $format
     */
    public function removeFormat(InfoMcmconferencedetailformat $format)
    {
        $this->formats->removeElement($format);
    }

    /**
     * Get formats
     *
     * @return ArrayCollection
     */
    public function getFormats()
    {
        return $this->formats;
    }

    /**
     * Set formats
     *
     * @return InfoMcmconference
     */
    public function setFormats(ArrayCollection $formats)
    {
        $this->formats = $formats;

        return $this;
    }

    /**
     * Set country
     *
     * @param integer $country
     *
     * @return InfoMcmconference
     */
    public function setCountry($country)
    {
        $this->country = $country;

        return $this;
    }

    /**
     * Get country
     *
     * @return integer
     */
    public function getCountry()
    {
        return $this->country;
    }
}
