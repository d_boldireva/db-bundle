<?php

namespace TeamSoft\CrmRepositoryBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * InfoContact
 *
 * @ORM\Table(name="info_contactblog")
 * @ORM\Entity(repositoryClass="TeamSoft\CrmRepositoryBundle\Repository\InfoContactBlog")
 * @UniqueEntity("link")
 */
class InfoContactBlog implements ServiceFieldInterface
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var InfoContact|null
     * @ORM\ManyToOne(targetEntity="InfoContact", inversedBy="contactBlogCollection")
     * @ORM\JoinColumn(name="contact_id", referencedColumnName="id")
     */
    private $contact;

    /**
     * @var string|null
     * @ORM\Column(name="name", type="string", length=50, nullable=false)
     */
    private $name;

    /**
     * @var string|null
     * @ORM\Column(name="link", type="string", length=50, nullable=false, unique=true)
     */
    private $link;

    /**
     * @var integer|null
     * @ORM\Column(name="subscribers", type="integer", nullable=true)
     */
    private $subscribers;

    /**
     * @var string
     *
     * @ORM\Column(name="guid", type="guid", nullable=true)
     */
    private $guid;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="currenttime", type="datetime", nullable=true)
     */
    private $currenttime;

    /**
     * @var string
     *
     * @ORM\Column(name="moduser", type="string", length=16, nullable=true)
     */
    private $moduser;

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return InfoContact
     */
    public function getContact(): ?InfoContact
    {
        return $this->contact;
    }

    /**
     * @param InfoContact $contact
     * @return self
     */
    public function setContact(InfoContact $contact): self
    {
        $this->contact = $contact;
        return $this;
    }

    /**
     * @return string
     */
    public function getName(): ?string
    {
        return $this->name;
    }

    /**
     * @param string $name
     * @return self
     */
    public function setName(string $name): self
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return string
     */
    public function getLink(): ?string
    {
        return $this->link;
    }

    /**
     * @param string $link
     * @return self
     */
    public function setLink(string $link): self
    {
        $this->link = mb_strtolower(trim($link));
        return $this;
    }

    /**
     * @return int
     */
    public function getSubscribers(): ?int
    {
        return $this->subscribers;
    }

    /**
     * @param int $subscribers
     * @return self
     */
    public function setSubscribers(?int $subscribers): self
    {
        $this->subscribers = $subscribers;
        return $this;
    }

    /**
     * @return string
     */
    public function getGuid()
    {
        return $this->guid;
    }

    /**
     * @param string $guid
     * @return self
     */
    public function setGuid($guid): self
    {
        $this->guid = $guid;
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getCurrenttime()
    {
        return $this->currenttime;
    }

    /**
     * @param \DateTime $currenttime
     * @return self
     */
    public function setCurrenttime(\DateTime $currenttime): self
    {
        $this->currenttime = $currenttime;
        return $this;
    }

    /**
     * @return string
     */
    public function getModuser()
    {
        return $this->moduser;
    }

    /**
     * @param string $moduser
     * @return self
     */
    public function setModuser($moduser): self
    {
        $this->moduser = $moduser;
        return $this;
    }

}
