<?php

namespace TeamSoft\CrmRepositoryBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\Common\Collections\Criteria;
use Doctrine\ORM\Mapping as ORM;

/**
 * InfoSurveyQuestion
 *
 * @ORM\Table(name="info_surveyquestion")
 * @ORM\Entity
 */
class InfoSurveyQuestion implements ServiceFieldInterface
{
    public const ANSWER_TYPE_TEXT = 1;
    public const ANSWER_TYPE_NUMBER = 2;
    public const ANSWER_TYPE_FIXED = 3;
    public const ANSWER_TYPE_LIST = 4;
    public const ANSWER_TYPE_MULTIPLE_LIST = 5;

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255, nullable=true)
     */
    private $name;

    /**
     * @var int
     *
     * @ORM\Column(name="type_ans", type="integer", nullable=true)
     */
    private $answerType;

    /**
     * @var int
     *
     * @ORM\Column(name="showcomment", type="integer", nullable=true)
     */
    private $showComment;

    /**
     * @var int
     *
     * @ORM\Column(name="is_required", type="integer", nullable=true)
     */
    private $isRequired;

    /**
     * @var string
     *
     * @ORM\Column(name="guid", type="guid", nullable=true)
     */
    private $guid;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="currenttime", type="datetime", nullable=true)
     */
    private $currenttime;

    /**
     * @var string
     *
     * @ORM\Column(name="moduser", type="string", length=16, nullable=true)
     */
    private $moduser;

    /**
     * @var ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="InfoSurveyQuestionAnswer", mappedBy="question", cascade={"persist"}, orphanRemoval=true)
     */
    private $answerCollection;

    /**
     * @var ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="InfoSurveyResult", mappedBy="question", cascade={"persist"}, orphanRemoval=true)
     */
    private $resultCollection;

    /**
     * @ORM\ManyToMany(targetEntity="InfoSurvey", inversedBy="surveyQuestionCollection")
     * @ORM\JoinTable(
     *     name="info_surveyquestionjunction",
     *     joinColumns={@ORM\JoinColumn(name="question_id", referencedColumnName="id")},
     *     inverseJoinColumns={@ORM\JoinColumn(name="survey_id", referencedColumnName="id")}
     * )
     */
    private $surveyCollection;

    /**
     * @var ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="InfoSurveyResultAction", mappedBy="question", cascade={"persist"}, orphanRemoval=true)
     */
    private $resultActions;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->answerCollection = new ArrayCollection();
        $this->resultCollection = new ArrayCollection();
        $this->surveyCollection = new ArrayCollection();
        $this->resultActions = new ArrayCollection();
    }

    public function __clone()
    {
        $this->id = null;
        $this->guid = null;
        $this->currenttime = null;
        $this->moduser = null;

        $this->answerCollection = new ArrayCollection();
        $this->resultCollection = new ArrayCollection();
        $this->surveyCollection = new ArrayCollection();
        $this->resultActions = new ArrayCollection();
    }

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name.
     *
     * @param string|null $name
     *
     * @return InfoSurveyQuestion
     */
    public function setName($name = null)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name.
     *
     * @return string|null
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set answerType.
     *
     * @param int|null $answerType
     *
     * @return InfoSurveyQuestion
     */
    public function setAnswerType($answerType = null)
    {
        $this->answerType = $answerType;

        return $this;
    }

    /**
     * Get answerType.
     *
     * @return int|null
     */
    public function getAnswerType()
    {
        return $this->answerType;
    }

    /**
     * Set showComment.
     *
     * @param int|null $showComment
     *
     * @return InfoSurveyQuestion
     */
    public function setShowComment($showComment = null)
    {
        $this->showComment = $showComment;

        return $this;
    }

    /**
     * Get showComment.
     *
     * @return int|null
     */
    public function getShowComment()
    {
        return $this->showComment;
    }

    /**
     * Set isRequired.
     *
     * @param int|null $isRequired
     *
     * @return InfoSurveyQuestion
     */
    public function setIsRequired($isRequired = null)
    {
        $this->isRequired = $isRequired;

        return $this;
    }

    /**
     * Get isRequired.
     *
     * @return int|null
     */
    public function getIsRequired()
    {
        return $this->isRequired;
    }

    /**
     * Set guid.
     *
     * @param string|null $guid
     *
     * @return InfoSurveyQuestion
     */
    public function setGuid($guid = null)
    {
        $this->guid = $guid;

        return $this;
    }

    /**
     * Get guid.
     *
     * @return string|null
     */
    public function getGuid()
    {
        return $this->guid;
    }

    /**
     * Set currenttime.
     *
     * @param \DateTime|null $currenttime
     *
     * @return InfoSurveyQuestion
     */
    public function setCurrenttime(\DateTime $currenttime = null)
    {
        $this->currenttime = $currenttime;

        return $this;
    }

    /**
     * Get currenttime.
     *
     * @return \DateTime|null
     */
    public function getCurrenttime()
    {
        return $this->currenttime;
    }

    /**
     * Set moduser.
     *
     * @param string|null $moduser
     *
     * @return InfoSurveyQuestion
     */
    public function setModuser($moduser = null)
    {
        $this->moduser = $moduser;

        return $this;
    }

    /**
     * Get moduser.
     *
     * @return string|null
     */
    public function getModuser()
    {
        return $this->moduser;
    }

    /**
     * Add answerCollection.
     *
     * @param InfoSurveyQuestionAnswer $answerCollection
     *
     * @return InfoSurveyQuestion
     */
    public function addAnswerCollection(InfoSurveyQuestionAnswer $answerCollection)
    {
        $answerCollection->setQuestion($this);
        $this->answerCollection[] = $answerCollection;

        return $this;
    }

    /**
     * Remove answerCollection.
     *
     * @param InfoSurveyQuestionAnswer $answerCollection
     *
     * @return boolean TRUE if this collection contained the specified element, FALSE otherwise.
     */
    public function removeAnswerCollection(InfoSurveyQuestionAnswer $answerCollection)
    {
        return $this->answerCollection->removeElement($answerCollection);
    }

    /**
     * Get answerCollection.
     *
     * @return Collection
     */
    public function getAnswerCollection()
    {
        return $this->answerCollection;
    }

    /**
     * Add resultCollection.
     *
     * @param InfoSurveyResult $resultCollection
     *
     * @return InfoSurveyQuestion
     */
    public function addResultCollection(InfoSurveyResult $resultCollection)
    {
        $resultCollection->setQuestion($this);
        $this->resultCollection[] = $resultCollection;

        return $this;
    }

    /**
     * Remove resultCollection.
     *
     * @param InfoSurveyResult $resultCollection
     *
     * @return boolean TRUE if this collection contained the specified element, FALSE otherwise.
     */
    public function removeResultCollection(InfoSurveyResult $resultCollection)
    {
        return $this->resultCollection->removeElement($resultCollection);
    }

    /**
     * Get resultCollection.
     *
     * @return Collection
     */
    public function getResultCollection()
    {
        return $this->resultCollection;
    }

    /**
     * Add surveyCollection.
     *
     * @param InfoSurvey $survey
     *
     * @return InfoSurveyQuestion
     */
    public function addSurveyCollection(InfoSurvey $survey)
    {
        if (!$this->surveyCollection->contains($survey)) {
            $this->surveyCollection->add($survey);
        }

        return $this;
    }

    /**
     * Remove surveyCollection.
     *
     * @param InfoSurvey $survey
     *
     * @return InfoSurveyQuestion
     */
    public function removeSurveyCollection(InfoSurvey $survey)
    {
        if ($this->surveyCollection->contains($survey)) {
            $this->surveyCollection->removeElement($survey);
        }

        return $this;
    }

    /**
     * Get surveyCollection.
     *
     * @return Collection
     */
    public function getSurveyCollection()
    {
        return $this->surveyCollection;
    }

    /**
     * Add resultCollection.
     *
     * @param InfoSurveyResultAction $resultAction
     *
     * @return InfoSurveyQuestion
     */
    public function addResultAction(InfoSurveyResultAction $resultAction)
    {
        if (!$this->resultActions->contains($resultAction)) {
            $resultAction->setQuestion($this);
            $this->resultActions[] = $resultAction;
        }

        return $this;
    }

    /**
     * Remove resultCollection.
     *
     * @param InfoSurveyResultAction $resultAction
     *
     * @return InfoSurveyQuestion
     */
    public function removeResultAction(InfoSurveyResultAction $resultAction)
    {
        if ($this->resultActions->contains($resultAction)) {
            $this->resultActions->removeElement($resultAction);
        }

        return $this;
    }

    /**
     * Get resultCollection.
     *
     * @return Collection
     */
    public function getResultActions()
    {
        return $this->resultActions;
    }
}
