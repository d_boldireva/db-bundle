<?php

namespace TeamSoft\CrmRepositoryBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * InfoTabcontroldictionary
 *
 * @ORM\Table(name="info_tabcontroldictionary")
 * @ORM\Entity()
 */
class InfoTabcontroldictionary implements ServiceFieldInterface
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var int
     *
     * @ORM\Column(name="type", type="integer", nullable=true)
     */
    private $type;

    /**
     * @var int
     *
     * @ORM\Column(name="parent_type", type="integer", nullable=true)
     */
    private $parentType;

    /**
     * @var string
     *
     * @ORM\Column(name="type_name", type="string", length=255, nullable=true)
     */
    private $typeName;

    /**
     * @var string
     *
     * @ORM\Column(name="parent_type_name", type="string", length=255, nullable=true)
     */
    private $parentTypeName;

    /**
     * @var string
     *
     * @ORM\Column(name="guid", type="guid", nullable=true)
     */
    private $guid;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="currenttime", type="datetime", nullable=true)
     */
    private $currenttime;

    /**
     * @var string
     *
     * @ORM\Column(name="moduser", type="string", length=16, nullable=true)
     */
    private $moduser;

    public function __clone()
    {
        $this->id = null;
        $this->guid = null;
        $this->currenttime = null;
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set typeName
     *
     * @param string $typeName
     *
     * @return InfoTabcontroldictionary
     */
    public function setTypeName($typeName)
    {
        $this->typeName = $typeName;
    
        return $this;
    }

    /**
     * Get typeName
     *
     * @return string
     */
    public function getTypeName()
    {
        return $this->typeName;
    }

    /**
     * Set parentTypeName
     *
     * @param string $parentTypeName
     *
     * @return InfoTabcontroldictionary
     */
    public function setParentTypeName($parentTypeName)
    {
        $this->parentTypeName = $parentTypeName;
    
        return $this;
    }

    /**
     * Get parentParentTypeName
     *
     * @return string
     */
    public function getParentTypeName()
    {
        return $this->parentTypeName;
    }

    /**
     * Set type
     *
     * @param string $type
     *
     * @return InfoTabcontroldictionary
     */
    public function setType($type)
    {
        $this->type = $type;
    
        return $this;
    }

    /**
     * Get type
     *
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set parentType
     *
     * @param string $parentType
     *
     * @return InfoTabcontroldictionary
     */
    public function setParentType($parentType)
    {
        $this->parentType = $parentType;
    
        return $this;
    }

    /**
     * Get parentType
     *
     * @return string
     */
    public function getParentType()
    {
        return $this->parentType;
    }

    /**
     * Set guid
     *
     * @param string $guid
     * @return InfoTabcontroldictionary
     */
    public function setGuid($guid)
    {
        $this->guid = $guid;
    
        return $this;
    }

    /**
     * Get guid
     *
     * @return string
     */
    public function getGuid()
    {
        return $this->guid;
    }

    /**
     * Set currenttime
     *
     * @param \DateTime $currenttime
     * @return InfoTabcontroldictionary
     */
    public function setCurrenttime(\DateTime $currenttime)
    {
        $this->currenttime = $currenttime;
    
        return $this;
    }

    /**
     * Get currenttime
     *
     * @return \DateTime|string
     */
    public function getCurrenttime()
    {
        return $this->currenttime;
    }

    /**
     * Set moduser
     *
     * @param string $moduser
     * @return InfoTabcontroldictionary
     */
    public function setModuser($moduser)
    {
        $this->moduser = $moduser;
    
        return $this;
    }

    /**
     * Get moduser
     *
     * @return string
     */
    public function getModuser()
    {
        return $this->moduser;
    }
}
