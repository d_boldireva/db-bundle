<?php

namespace TeamSoft\CrmRepositoryBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * InfoEntityTypeWorkflowLog
 *
 * @ORM\Table(name="info_entitytypeworkflowlog")
 * @ORM\Entity(repositoryClass="TeamSoft\CrmRepositoryBundle\Repository\InfoEntityTypeWorkflowLog")
 */
class InfoEntityTypeWorkflowLog implements ServiceFieldInterface
{

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var InfoEntityTypeWorkflow
     *
     * @ORM\ManyToOne(targetEntity="InfoEntityTypeWorkflow")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="workflow_id", referencedColumnName="id")
     * })
     */
    private $workflow;

    /**
     * @var InfoAction
     *
     * @ORM\ManyToOne(targetEntity="InfoAction")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="action_id", referencedColumnName="id")
     * })
     */
    private $action;

    /**
     * @var InfoUser
     *
     * @ORM\ManyToOne(targetEntity="InfoUser")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     * })
     */
    private $user;

    /**
     * @var integer
     *
     * @ORM\Column(name="status", type="integer", length=255, nullable=true)
     */
    private $status;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date", type="datetime", length=255, nullable=true)
     */
    private $date;

    /**
     * @var integer
     *
     * @ORM\Column(name="guid", type="guid", nullable=true)
     */
    private $guid;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="currenttime", type="datetime", nullable=true)
     */
    private $currenttime;

    /**
     * @var string
     *
     * @ORM\Column(name="moduser", type="string", length=16, nullable=true)
     */
    private $moduser;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * Set workflow
     *
     * @param InfoEntitytypeworkflow $workflow
     * @return InfoEntityTypeWorkflowLog
     */
    public function setWorkflow(?InfoEntitytypeworkflow $workflow): self
    {
        $this->workflow = $workflow;

        return $this;
    }

    /**
     * Get workflow
     *
     * @return InfoEntitytypeworkflow
     */
    public function getWorkflow(): ?InfoEntitytypeworkflow
    {
        return $this->workflow;
    }

    /**
     * Set action
     *
     * @param InfoAction $action
     * @return InfoEntityTypeWorkflowLog
     */
    public function setAction(?InfoAction $action): self
    {
        $this->action = $action;

        return $this;
    }

    /**
     * Get action
     *
     * @return InfoAction
     */
    public function getAction(): ?InfoAction
    {
        return $this->action;
    }

    /**
     * Set user
     *
     * @param InfoUser $user
     * @return InfoEntityTypeWorkflowLog
     */
    public function setUser(?InfoUser $user): self
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return InfoUser
     */
    public function getUser(): ?InfoUser
    {
        return $this->user;
    }

    /**
     * Set date
     *
     * @param \DateTime $date
     * @return InfoEntityTypeWorkflowLog
     */
    public function setDate(?\DateTime $date): self
    {
        $this->date = $date;

        return $this;
    }

    /**
     * Get $date
     *
     * @return \DateTime
     */
    public function getDate(): ?\DateTime
    {
        return $this->date;
    }

    /**
     * Set status
     *
     * @param integer $status
     * @return InfoEntityTypeWorkflowLog
     */
    public function setStatus(?int $status): self
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return integer
     */
    public function getStatus(): ?int
    {
        return $this->status;
    }

    /**
     * Set guid
     *
     * @param string $guid
     *
     * @return InfoEntityTypeWorkflowLog
     */
    public function setGuid($guid)
    {
        $this->guid = $guid;

        return $this;
    }

    /**
     * Get guid
     *
     * @return integer
     */
    public function getGuid(): ?int
    {
        return $this->guid;
    }

    /**
     * Set currenttime
     *
     * @param \DateTime $currenttime
     * @return InfoEntityTypeWorkflowLog
     */
    public function setCurrenttime(?\DateTime $currenttime): self
    {
        $this->currenttime = $currenttime;

        return $this;
    }

    /**
     * Get currenttime
     *
     * @return \DateTime
     */
    public function getCurrenttime(): ?\DateTime
    {
        return $this->currenttime;
    }

    /**
     * Set moduser
     *
     * @param string $moduser
     *
     * @return InfoEntityTypeWorkflowLog
     */
    public function setModuser($moduser)
    {
        $this->moduser = $moduser;

        return $this;
    }

    /**
     * Get moduser
     *
     * @return string
     */
    public function getModuser(): ?string
    {
        return $this->moduser;
    }
}
