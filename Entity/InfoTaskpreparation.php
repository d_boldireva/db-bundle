<?php

namespace TeamSoft\CrmRepositoryBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * InfoTaskpreparation
 *
 * @ORM\Table(name="info_taskpreparation")
 * @ORM\Entity(repositoryClass="TeamSoft\CrmRepositoryBundle\Repository\InfoTaskpreparation")
 */
class InfoTaskpreparation implements ServiceFieldInterface
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var int
     *
     * @ORM\Column(name="ispromo", type="integer", nullable=true)
     */
    private $ispromo = '0';

    /**
     * @var string
     *
     * @ORM\Column(name="guid", type="guid", nullable=true)
     */
    private $guid;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="currenttime", type="datetime", nullable=true)
     */
    private $currenttime;

    /**
     * @var string
     *
     * @ORM\Column(name="moduser", type="string", length=16, nullable=true)
     */
    private $moduser;

    /**
     * @var int
     *
     * @ORM\Column(name="ismanual", type="integer", nullable=true)
     */
    private $ismanual;

    /**
     * @var int
     *
     * @ORM\Column(name="plancnt", type="integer", nullable=true)
     */
    private $plancnt;

    /**
     * @var string
     *
     * @ORM\Column(name="descr", type="varchar_or_n_varchar_image_or_string", nullable=true)
     */
    private $descr;

    /**
     * @var InfoPreparationbrend
     *
     * @ORM\ManyToOne(targetEntity="InfoPreparationbrend")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="brend_id", referencedColumnName="id")
     * })
     */
    private $brend;

    /**
     * @var InfoPrepline
     *
     * @ORM\ManyToOne(targetEntity="InfoPrepline")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="prepline_id", referencedColumnName="id")
     * })
     */
    private $prepline;

    /**
     * @var InfoPrepresult
     *
     * @ORM\ManyToOne(targetEntity="InfoPrepresult")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="prepresult_id", referencedColumnName="id")
     * })
     */
    private $prepresult;

    /**
     * @var InfoPreprespons
     *
     * @ORM\ManyToOne(targetEntity="InfoPreprespons")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="preprespons_id", referencedColumnName="id")
     * })
     */
    private $preprespons;

    /**
     * @var InfoTask
     *
     * @ORM\ManyToOne(targetEntity="InfoTask", inversedBy="taskPreparationCollection")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="task_id", referencedColumnName="id")
     * })
     */
    private $task;

    /**
     * @var InfoSmile
     *
     * @ORM\ManyToOne(targetEntity="InfoSmile")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="smile_id", referencedColumnName="id")
     * })
     */
    private $smile;



    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set ispromo
     *
     * @param int $ispromo
     *
     * @return InfoTaskpreparation
     */
    public function setIspromo($ispromo)
    {
        $this->ispromo = $ispromo;
    
        return $this;
    }

    /**
     * Get ispromo
     *
     * @return int
     */
    public function getIspromo()
    {
        return $this->ispromo;
    }

    /**
     * Set guid
     *
     * @param guid $guid
     *
     * @return InfoTaskpreparation
     */
    public function setGuid($guid)
    {
        $this->guid = $guid;
    
        return $this;
    }

    /**
     * Get guid
     *
     * @return guid
     */
    public function getGuid()
    {
        return $this->guid;
    }

    /**
     * Set currenttime
     *
     * @param \DateTime $currenttime
     *
     * @return InfoTaskpreparation
     */
    public function setCurrenttime(\DateTime $currenttime)
    {
        $this->currenttime = $currenttime;
    
        return $this;
    }

    /**
     * Get currenttime
     *
     * @return \DateTime
     */
    public function getCurrenttime()
    {
        return $this->currenttime;
    }

    /**
     * Set moduser
     *
     * @param string $moduser
     *
     * @return InfoTaskpreparation
     */
    public function setModuser($moduser)
    {
        $this->moduser = $moduser;
    
        return $this;
    }

    /**
     * Get moduser
     *
     * @return string
     */
    public function getModuser()
    {
        return $this->moduser;
    }

    /**
     * Set ismanual
     *
     * @param int $ismanual
     *
     * @return InfoTaskpreparation
     */
    public function setIsmanual($ismanual)
    {
        $this->ismanual = $ismanual;
    
        return $this;
    }

    /**
     * Get ismanual
     *
     * @return int
     */
    public function getIsmanual()
    {
        return $this->ismanual;
    }

    /**
     * Set plancnt
     *
     * @param int $plancnt
     *
     * @return InfoTaskpreparation
     */
    public function setPlancnt($plancnt)
    {
        $this->plancnt = $plancnt;
    
        return $this;
    }

    /**
     * Get plancnt
     *
     * @return int
     */
    public function getPlancnt()
    {
        return $this->plancnt;
    }

    /**
     * Set descr
     *
     * @param string $descr
     *
     * @return InfoTaskpreparation
     */
    public function setDescr($descr)
    {
        $this->descr = $descr;
    
        return $this;
    }

    /**
     * Get descr
     *
     * @return string
     */
    public function getDescr()
    {
        return $this->descr;
    }

    /**
     * Set brend
     *
     * @param \TeamSoft\CrmRepositoryBundle\Entity\InfoPreparationbrend $brend
     *
     * @return InfoTaskpreparation
     */
    public function setBrend(\TeamSoft\CrmRepositoryBundle\Entity\InfoPreparationbrend $brend = null)
    {
        $this->brend = $brend;

        return $this;
    }

    /**
     * Get brend
     *
     * @return \TeamSoft\CrmRepositoryBundle\Entity\InfoPreparationbrend
     */
    public function getBrend()
    {
        return $this->brend;
    }

    /**
     * Set prepline
     *
     * @param \TeamSoft\CrmRepositoryBundle\Entity\InfoPrepline $prepline
     *
     * @return InfoTaskpreparation
     */
    public function setPrepline(\TeamSoft\CrmRepositoryBundle\Entity\InfoPrepline $prepline = null)
    {
        $this->prepline = $prepline;

        return $this;
    }

    /**
     * Get prepline
     *
     * @return \TeamSoft\CrmRepositoryBundle\Entity\InfoPrepline
     */
    public function getPrepline()
    {
        return $this->prepline;
    }

    /**
     * Set prepresult
     *
     * @param \TeamSoft\CrmRepositoryBundle\Entity\InfoPrepresult $prepresult
     *
     * @return InfoTaskpreparation
     */
    public function setPrepresult(\TeamSoft\CrmRepositoryBundle\Entity\InfoPrepresult $prepresult = null)
    {
        $this->prepresult = $prepresult;

        return $this;
    }

    /**
     * Get prepresult
     *
     * @return \TeamSoft\CrmRepositoryBundle\Entity\InfoPrepresult
     */
    public function getPrepresult()
    {
        return $this->prepresult;
    }

    /**
     * Set preprespons
     *
     * @param \TeamSoft\CrmRepositoryBundle\Entity\InfoPreprespons $preprespons
     *
     * @return InfoTaskpreparation
     */
    public function setPreprespons(\TeamSoft\CrmRepositoryBundle\Entity\InfoPreprespons $preprespons = null)
    {
        $this->preprespons = $preprespons;

        return $this;
    }

    /**
     * Get preprespons
     *
     * @return \TeamSoft\CrmRepositoryBundle\Entity\InfoPreprespons
     */
    public function getPreprespons()
    {
        return $this->preprespons;
    }

    /**
     * Set task
     *
     * @param \TeamSoft\CrmRepositoryBundle\Entity\InfoTask $task
     *
     * @return InfoTaskpreparation
     */
    public function setTask(\TeamSoft\CrmRepositoryBundle\Entity\InfoTask $task = null)
    {
        $this->task = $task;

        return $this;
    }

    /**
     * Get task
     *
     * @return \TeamSoft\CrmRepositoryBundle\Entity\InfoTask
     */
    public function getTask()
    {
        return $this->task;
    }

    /**
     * Set smile
     *
     * @param \TeamSoft\CrmRepositoryBundle\Entity\InfoSmile $smile
     *
     * @return InfoTaskpreparation
     */
    public function setSmile(\TeamSoft\CrmRepositoryBundle\Entity\InfoSmile $smile = null)
    {
        $this->smile = $smile;

        return $this;
    }

    /**
     * Get smile
     *
     * @return \TeamSoft\CrmRepositoryBundle\Entity\InfoSmile
     */
    public function getSmile()
    {
        return $this->smile;
    }

    public function getBrandId()
    {
        return $this->getBrend() ? $this->getBrend()->getId() : null;
    }

    public function getTaskId()
    {
        return $this->getTask() ? $this->getTask()->getId() : null;
    }

    public function getPreplineId()
    {
        return $this->getPrepline() ? $this->getPrepline()->getId() : null;
    }
}
