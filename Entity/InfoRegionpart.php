<?php

namespace TeamSoft\CrmRepositoryBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * InfoRegionpart
 *
 * @ORM\Table(name="info_regionpart")
 * @ORM\Entity(repositoryClass="TeamSoft\CrmRepositoryBundle\Repository\InfoRegionpart")
 */
class InfoRegionpart implements ServiceFieldInterface
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="Name", type="string", length=255, nullable=false)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="guid", type="guid", nullable=true)
     */
    private $guid;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="currenttime", type="datetime", nullable=true)
     */
    private $currenttime;

    /**
     * @var string
     *
     * @ORM\Column(name="moduser", type="string", length=16, nullable=true)
     */
    private $moduser;

    /**
     * @ORM\ManyToMany(targetEntity="InfoRegion", mappedBy="regionparts")
     * @ORM\JoinTable(name="info_regioninregionpart",
     *     joinColumns={@ORM\JoinColumn(name="regionpart_id", referencedColumnName="id")},
     *     inverseJoinColumns={@ORM\JoinColumn(name="region_id", referencedColumnName="id")}
     * )
     * @ORM\OrderBy({"name" = "ASC"})
     */
    private $regionCollection;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->regionCollection = new ArrayCollection();
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return InfoRegionpart
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set guid
     *
     * @param string $guid
     *
     * @return InfoRegionpart
     */
    public function setGuid($guid)
    {
        $this->guid = $guid;

        return $this;
    }

    /**
     * Get guid
     *
     * @return string
     */
    public function getGuid()
    {
        return $this->guid;
    }

    /**
     * Set currenttime
     *
     * @param \DateTime $currenttime
     *
     * @return InfoRegionpart
     */
    public function setCurrenttime(\DateTime $currenttime)
    {
        $this->currenttime = $currenttime;

        return $this;
    }

    /**
     * Get currenttime
     *
     * @return \DateTime
     */
    public function getCurrenttime()
    {
        return $this->currenttime;
    }

    /**
     * Set moduser
     *
     * @param string $moduser
     *
     * @return InfoRegionpart
     */
    public function setModuser($moduser)
    {
        $this->moduser = $moduser;

        return $this;
    }

    /**
     * Get moduser
     *
     * @return string
     */
    public function getModuser()
    {
        return $this->moduser;
    }

    /**
     * Add regionCollection
     *
     * @param \TeamSoft\CrmRepositoryBundle\Entity\InfoRegion $regionCollection
     *
     * @return InfoRegionpart
     */
    public function addRegionCollection(InfoRegion $regionCollection)
    {
        $this->regionCollection[] = $regionCollection;

        return $this;
    }

    /**
     * Remove regionCollection
     *
     * @param \TeamSoft\CrmRepositoryBundle\Entity\InfoRegion $regionCollection
     */
    public function removeRegionCollection(InfoRegion $regionCollection)
    {
        $this->regionCollection->removeElement($regionCollection);
    }

    /**
     * Get regionCollection
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getRegionCollection()
    {
        return $this->regionCollection;
    }
}
