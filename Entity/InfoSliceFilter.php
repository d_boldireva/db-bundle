<?php

namespace TeamSoft\CrmRepositoryBundle\Entity;

use DateTime;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * InfoSliceFilter
 *
 * @ORM\Table(name="info_slicefilter")
 * @ORM\Entity()
 */
class InfoSliceFilter implements ServiceFieldInterface
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string|null
     *
     * @ORM\Column(name="client_filter", type="string", length=255, nullable=true)
     */
    private $clientFilter;

    /**
     * @var string|null
     *
     * @ORM\Column(name="server_filter", type="string", length=255, nullable=true)
     */
    private $serverFilter;

    /**
     * @var string|null
     *
     * @ORM\Column(name="client_default_filter", type="string", length=255, nullable=true)
     */
    private $clientDefaultFilter;

    /**
     * @var string|null
     *
     * @ORM\Column(name="server_default_filter", type="string", length=255, nullable=true)
     */
    private $serverDefaultFilter;

    /**
     * @var DateTime|null
     *
     * @ORM\Column(name="currenttime", type="datetime", nullable=true)
     */
    private $currenttime;

    /**
     * @var string|null
     *
     * @ORM\Column(name="moduser", type="string", length=16, nullable=true)
     * )
     */
    private $moduser;

    /**
     * @var string|null
     *
     * @ORM\Column(name="guid", type="guid", nullable=true)
     */
    private $guid;

    /**
     * Many slices have one filter
     * @ORM\ManyToOne(targetEntity="InfoSlice", inversedBy="id")
     * @ORM\JoinColumn(name="slice_id", referencedColumnName="id")
     */
    private $slice;

    /**
     * Many filter have one filter
     * @ORM\ManyToOne(targetEntity="InfoSliceFilterDictionary", inversedBy="id")
     * @ORM\JoinColumn(name="filter_id", referencedColumnName="id")
     */
    private $filter;


    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set slice
     *
     * @param InfoSlice $slice
     *
     * @return InfoSliceFilter
     */
    public function setSlice($slice = null)
    {
        $this->slice = $slice;

        return $this;
    }

    /**
     * Get slice
     *
     * @return InfoSlice
     */
    public function getSlice()
    {
        return $this->slice;
    }

    /**
     * Set filter
     *
     * @param InfoSliceFilterDictionary $filter
     *
     * @return InfosliceFilter
     */
    public function setFilter($filter = null)
    {
        $this->filter = $filter;

        return $this;
    }

    /**
     * Get filter
     *
     * @return InfoSliceFilterDictionary
     */
    public function getFilter()
    {
        return $this->filter;
    }

    /**
     * Set clientFilter.
     *
     * @param string|null $clientFilter
     *
     * @return InfoSliceFilter
     */
    public function setClientFilter($clientFilter = null)
    {
        $this->clientFilter = $clientFilter;

        return $this;
    }

    /**
     * Get clientFilter.
     *
     * @return string|null
     */
    public function getClientFilter()
    {
        return $this->clientFilter;
    }

    /**
     * Set serverFilter.
     *
     * @param string|null $serverFilter
     *
     * @return InfoSliceFilter
     */
    public function setServerFilter($serverFilter = null)
    {
        $this->serverFilter = $serverFilter;

        return $this;
    }

    /**
     * Get serverFilter.
     *
     * @return string|null
     */
    public function getServerFilter()
    {
        return $this->serverFilter;
    }

    /**
     * Set clientDefaultFilter.
     *
     * @param string|null $clientDefaultFilter
     *
     * @return InfoSliceFilter
     */
    public function setClientDefaultFilter($clientDefaultFilter = null)
    {
        $this->clientDefaultFilter = $clientDefaultFilter;

        return $this;
    }

    /**
     * Get clientDefaultFilter.
     *
     * @return string|null
     */
    public function getClientDefaultFilter()
    {
        return $this->clientDefaultFilter;
    }

    /**
     * Set serverDefaultFilter.
     *
     * @param string|null $serverDefaultFilter
     *
     * @return InfoSliceFilter
     */
    public function setServerDefaultFilter($serverDefaultFilter = null)
    {
        $this->serverDefaultFilter = $serverDefaultFilter;

        return $this;
    }

    /**
     * Get serverDefaultFilter.
     *
     * @return string|null
     */
    public function getServerDefaultFilter()
    {
        return $this->serverDefaultFilter;
    }

    /**
     * Set currenttime.
     *
     * @param DateTime|null $currenttime
     *
     * @return InfoSliceFilter
     */
    public function setCurrenttime(DateTime $currenttime = null)
    {
        $this->currenttime = $currenttime;

        return $this;
    }

    /**
     * Get currenttime.
     *
     * @return DateTime|null
     */
    public function getCurrenttime()
    {
        return $this->currenttime;
    }

    /**
     * Set moduser.
     *
     * @param string|null $moduser
     *
     * @return InfoSliceFilter
     */
    public function setModuser($moduser = null)
    {
        $this->moduser = $moduser;

        return $this;
    }

    /**
     * Get moduser.
     *
     * @return string|null
     */
    public function getModuser()
    {
        return $this->moduser;
    }

    /**
     * Set guid.
     *
     * @param string|null $guid
     *
     * @return InfoSliceFilter
     */
    public function setGuid($guid = null)
    {
        $this->guid = $guid;

        return $this;
    }

    /**
     * Get guid.
     *
     * @return string|null
     */
    public function getGuid()
    {
        return $this->guid;
    }
}
