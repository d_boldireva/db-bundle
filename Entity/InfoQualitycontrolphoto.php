<?php

namespace TeamSoft\CrmRepositoryBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * InfoQualitycontrolphoto
 *
 * @ORM\Table(name="info_qualitycontrolphoto")
 * @ORM\Entity
 */
class InfoQualitycontrolphoto implements ServiceFieldInterface
{
    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255, nullable=true)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="content", type="blob", nullable=true)
     */
    private $content;

    /**
     * @var int
     *
     * @ORM\Column(name="size", type="integer", nullable=true)
     */
    private $size;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="currenttime", type="datetime", nullable=true)
     */
    private $currenttime;

    /**
     * @var string
     *
     * @ORM\Column(name="moduser", type="string", length=16, nullable=true)
     */
    private $moduser;

    /**
     * @var guid
     *
     * @ORM\Column(name="guid", type="guid", nullable=true)
     */
    private $guid;

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var InfoQualitycontrol
     *
     * @ORM\ManyToOne(targetEntity="InfoQualitycontrol", inversedBy="photo")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="qualitycontrol_id", referencedColumnName="id")
     * })
     */
    private $qualitycontrol;


    /**
     * Set name
     *
     * @param string $name
     *
     * @return InfoQualitycontrolphoto
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set content
     *
     * @param string $content
     *
     * @return InfoQualitycontrolphoto
     */
    public function setContent($content)
    {
        $this->content = $content;

        return $this;
    }

    /**
     * Get content
     *
     * @return string
     */
    public function getContent()
    {
        return $this->content;
    }

    /**
     * Get base64 encoded content
     *
     * @return string
     */
    public function getBase64EncodedContent()
    {
        return base64_encode(stream_get_contents($this->content));
    }

    /**
     * Set size
     *
     * @param int $size
     *
     * @return InfoQualitycontrolphoto
     */
    public function setSize($size)
    {
        $this->size = $size;

        return $this;
    }

    /**
     * Get size
     *
     * @return int
     */
    public function getSize()
    {
        return $this->size;
    }

    /**
     * Set currenttime
     *
     * @param \DateTime $currenttime
     *
     * @return InfoQualitycontrolphoto
     */
    public function setCurrenttime(\DateTime $currenttime)
    {

        $this->currenttime = $currenttime;

        return $this;
    }

    /**
     * Get currenttime
     *
     * @return \DateTime
     */
    public function getCurrenttime()
    {
        return $this->currenttime;
    }

    /**
     * Set moduser
     *
     * @param string $moduser
     *
     * @return InfoQualitycontrolphoto
     */
    public function setModuser($moduser)
    {
        $this->moduser = $moduser;

        return $this;
    }

    /**
     * Get moduser
     *
     * @return string
     */
    public function getModuser()
    {
        return $this->moduser;
    }

    /**
     * Set guid
     *
     * @param guid $guid
     *
     * @return InfoQualitycontrolphoto
     */
    public function setGuid($guid)
    {
        $this->guid = $guid;

        return $this;
    }

    /**
     * Get guid
     *
     * @return guid
     */
    public function getGuid()
    {
        return $this->guid;
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set qualitycontrol
     *
     * @param \TeamSoft\CrmRepositoryBundle\Entity\InfoQualitycontrol $qualitycontrol
     *
     * @return InfoQualitycontrolphoto
     */
    public function setQualitycontrol(\TeamSoft\CrmRepositoryBundle\Entity\InfoQualitycontrol $qualitycontrol = null)
    {
        $this->qualitycontrol = $qualitycontrol;

        return $this;
    }

    /**
     * Get qualitycontrol
     *
     * @return \TeamSoft\CrmRepositoryBundle\Entity\InfoQualitycontrol
     */
    public function getQualitycontrol()
    {
        return $this->qualitycontrol;
    }
}
