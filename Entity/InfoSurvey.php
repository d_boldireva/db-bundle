<?php

namespace TeamSoft\CrmRepositoryBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * InfoSurvey
 *
 * @ORM\Table(name="info_survey")
 * @ORM\Entity(repositoryClass="TeamSoft\CrmRepositoryBundle\Repository\InfoSurvey")
 * @ORM\EntityListeners({"TeamSoft\CrmRepositoryBundle\EventListener\InfoSurveyListener"})
 */
class InfoSurvey implements ServiceFieldInterface
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255, nullable=true)
     */
    private $name;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="datefrom", type="datetime", nullable=true)
     */
    private $datefrom;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="datetill", type="datetime", nullable=true)
     */
    private $datetill;

    /**
     * @var int
     *
     * @ORM\Column(name="isrequired", type="integer", nullable=true)
     */
    private $isRequired;

    /**
     * @var int
     *
     * @ORM\Column(name="page", type="integer", nullable=true)
     */
    private $page;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="varchar_or_n_varchar_image_or_string", nullable=true)
     */
    private $description;

    /**
     * @var int
     *
     * @ORM\Column(name="isarchive", type="integer", nullable=true)
     */
    private $isArchive;

    /**
     * @var int
     *
     * @ORM\Column(name="ismultiuse", type="integer", nullable=true)
     */
    private $isMultiuse;

    /**
     * @var int
     *
     * @ORM\Column(name="isgetprevanswers", type="integer", nullable=true)
     */
    private $isGetPrevAnswers;

    /**
     * @var string
     *
     * @ORM\Column(name="guid", type="guid", nullable=true)
     */
    private $guid;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="currenttime", type="datetime", nullable=true)
     */
    private $currenttime;

    /**
     * @var string
     *
     * @ORM\Column(name="moduser", type="string", length=16, nullable=true)
     */
    private $moduser;

    /**
     * @var InfoUser
     *
     * @ORM\ManyToOne(targetEntity="InfoUser")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="creator_id", referencedColumnName="id")
     * })
     */
    private $creator;

    /**
     * @var ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="InfoSurveyFilter", mappedBy="survey", cascade={"persist"}, orphanRemoval=true)
     */
    private $surveyFilterCollection;

    /**
     * @var ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="InfoSurveyResult", mappedBy="survey", cascade={"persist"}, orphanRemoval=true)
     */
    private $surveyResultCollection;

    /**
     * @var ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="InfoSurveyResultAction", mappedBy="survey", cascade={"persist"}, orphanRemoval=true)
     */
    private $surveyResultActions;

    /**
     * @ORM\ManyToMany(targetEntity="InfoSurveyQuestion", inversedBy="surveyCollection")
     * @ORM\JoinTable(
     *     name="info_surveyquestionjunction",
     *     joinColumns={@ORM\JoinColumn(name="survey_id", referencedColumnName="id")},
     *     inverseJoinColumns={@ORM\JoinColumn(name="question_id", referencedColumnName="id")}
     * )
     */
    private $surveyQuestionCollection;

    /**
     *
     * @ORM\OneToMany(targetEntity="InfoSurveyQuestionJunction", mappedBy="survey", cascade={"persist"}, orphanRemoval=true)
     */
    private $surveyQuestionJunctionCollection;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->surveyFilterCollection = new ArrayCollection();
        $this->surveyQuestionCollection = new ArrayCollection();
        $this->surveyQuestionJunctionCollection = new ArrayCollection();
        $this->surveyResultCollection = new ArrayCollection();
        $this->surveyResultActions = new ArrayCollection();
    }

    public function __clone()
    {
        $this->id = null;
        $this->guid = null;
        $this->currenttime = null;
        $this->moduser = null;

        $this->surveyFilterCollection = new ArrayCollection();
        $this->surveyQuestionCollection = new ArrayCollection();
        $this->surveyQuestionJunctionCollection = new ArrayCollection();
        $this->surveyResultCollection = new ArrayCollection();
        $this->surveyResultActions = new ArrayCollection();
    }


    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name.
     *
     * @param string|null $name
     *
     * @return InfoSurvey
     */
    public function setName($name = null)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name.
     *
     * @return string|null
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set datefrom.
     *
     * @param \DateTime|null $datefrom
     *
     * @return InfoSurvey
     */
    public function setDatefrom($datefrom = null)
    {
        $this->datefrom = $datefrom;

        return $this;
    }

    /**
     * Get datefrom.
     *
     * @return \DateTime|null
     */
    public function getDatefrom()
    {
        return $this->datefrom;
    }

    /**
     * Set datetill.
     *
     * @param \DateTime|null $datetill
     *
     * @return InfoSurvey
     */
    public function setDatetill($datetill = null)
    {
        $this->datetill = $datetill;

        return $this;
    }

    /**
     * Get datetill.
     *
     * @return \DateTime|null
     */
    public function getDatetill()
    {
        return $this->datetill;
    }

    /**
     * Set page.
     *
     * @param int|null $page
     *
     * @return InfoSurvey
     */
    public function setPage($page = null)
    {
        $this->page = $page;

        return $this;
    }

    /**
     * Get page.
     *
     * @return int|null
     */
    public function getPage()
    {
        return $this->page;
    }

    /**
     * Set description.
     *
     * @param string|null $description
     *
     * @return InfoSurvey
     */
    public function setDescription($description = null)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description.
     *
     * @return string|null
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set isArchive.
     *
     * @param int|null $isArchive
     *
     * @return InfoSurvey
     */
    public function setIsArchive($isArchive = null)
    {
        $this->isArchive = $isArchive;

        return $this;
    }

    /**
     * Get isArchive.
     *
     * @return int|null
     */
    public function getIsArchive()
    {
        return $this->isArchive;
    }

    /**
     * Set isMultiuse.
     *
     * @param int|null $isMultiuse
     *
     * @return InfoSurvey
     */
    public function setIsMultiuse($isMultiuse = null)
    {
        $this->isMultiuse = $isMultiuse;

        return $this;
    }

    /**
     * Get isMultiuse.
     *
     * @return int|null
     */
    public function getIsMultiuse()
    {
        return $this->isMultiuse;
    }

    /**
     * Set isRequired.
     *
     * @param int|null $isRequired
     *
     * @return InfoSurvey
     */
    public function setIsRequired($isRequired = null)
    {
        $this->isRequired = $isRequired;

        return $this;
    }

    /**
     * Get isRequired.
     *
     * @return int|null
     */
    public function getIsRequired()
    {
        return $this->isRequired;
    }

    /**
     * Set isGetPrevAnswers.
     *
     * @param int|null $isGetPrevAnswers
     *
     * @return InfoSurvey
     */
    public function setIsGetPrevAnswers($isGetPrevAnswers = null)
    {
        $this->isGetPrevAnswers = $isGetPrevAnswers;

        return $this;
    }

    /**
     * Get isGetPrevAnswers.
     *
     * @return int|null
     */
    public function getIsGetPrevAnswers()
    {
        return $this->isGetPrevAnswers;
    }

    /**
     * Set guid.
     *
     * @param string|null $guid
     *
     * @return InfoSurvey
     */
    public function setGuid($guid = null)
    {
        $this->guid = $guid;

        return $this;
    }

    /**
     * Get guid.
     *
     * @return string|null
     */
    public function getGuid()
    {
        return $this->guid;
    }

    /**
     * Set currenttime.
     *
     * @param \DateTime|null $currenttime
     *
     * @return InfoSurvey
     */
    public function setCurrenttime(\DateTime $currenttime = null)
    {
        $this->currenttime = $currenttime;

        return $this;
    }

    /**
     * Get currenttime.
     *
     * @return \DateTime|null
     */
    public function getCurrenttime()
    {
        return $this->currenttime;
    }

    /**
     * Set moduser.
     *
     * @param string|null $moduser
     *
     * @return InfoSurvey
     */
    public function setModuser($moduser = null)
    {
        $this->moduser = $moduser;

        return $this;
    }

    /**
     * Get moduser.
     *
     * @return string|null
     */
    public function getModuser()
    {
        return $this->moduser;
    }

    /**
     * Set creator
     *
     * @param InfoUser|null $creator
     * @return InfoSurvey
     */
    public function setCreator(InfoUser $creator = null)
    {
        $this->creator = $creator;

        return $this;
    }

    /**
     * Get creator
     *
     * @return InfoUser
     */
    public function getCreator()
    {
        return $this->creator;
    }

    /**
     * @param InfoSurveyFilter $filter
     * @return $this
     */
    public function addSurveyFilterCollection(InfoSurveyFilter $filter)
    {
        if (!$this->surveyFilterCollection->contains($filter)) {
            $this->surveyFilterCollection->add($filter);
            $filter->setSurvey($this);
        }

        return $this;
    }

    /**
     * @param InfoSurveyFilter $filter
     * @return $this
     */
    public function removeSurveyFilterCollection(InfoSurveyFilter $filter)
    {
        if ($this->surveyFilterCollection->contains($filter)) {
            $this->surveyFilterCollection->removeElement($filter);
        }

        return $this;
    }

    /**
     * @return ArrayCollection
     */
    public function getSurveyFilterCollection()
    {
        return $this->surveyFilterCollection;
    }

    /**
     * @param InfoSurveyQuestion $question
     * @return $this
     */
    public function addSurveyQuestionCollection(InfoSurveyQuestion $question)
    {
        if (!$this->surveyQuestionCollection->contains($question)) {
            $this->surveyQuestionCollection->add($question);
        }

        return $this;
    }

    /**
     * @param InfoSurveyQuestion $question
     * @return $this
     */
    public function removeSurveyQuestionCollection(InfoSurveyQuestion $question)
    {
        if ($this->surveyQuestionCollection->contains($question)) {
            $this->surveyQuestionCollection->removeElement($question);
        }

        return $this;
    }

    /**
     * @return ArrayCollection
     */
    public function getSurveyQuestionCollection()
    {
        return $this->surveyQuestionCollection;
    }

    /**
     * @param InfoSurveyQuestionJunction $question
     * @return $this
     */
    public function addSurveyQuestionJunctionCollection(InfoSurveyQuestionJunction $question)
    {
        if (!$this->surveyQuestionJunctionCollection->contains($question)) {
            $this->surveyQuestionJunctionCollection->add($question);
        }

        return $this;
    }

    /**
     * @param InfoSurveyQuestionJunction $question
     * @return $this
     */
    public function removeSurveyQuestionJunctionCollection(InfoSurveyQuestionJunction $question)
    {
        if ($this->surveyQuestionJunctionCollection->contains($question)) {
            $this->surveyQuestionJunctionCollection->removeElement($question);
        }

        return $this;
    }

    /**
     * @return ArrayCollection
     */
    public function getSurveyQuestionJunctionCollection()
    {
        return $this->surveyQuestionJunctionCollection;
    }

    /**
     * @return ArrayCollection
     */
    public function getSurveyResultCollection()
    {
        return $this->surveyResultCollection;
    }

    /**
     * @return ArrayCollection
     */
    public function getSurveyResultActions()
    {
        return $this->surveyResultActions;
    }

    /**
     * Add surveyResultAction.
     *
     * @param \TeamSoft\CrmRepositoryBundle\Entity\InfoSurveyResultAction $surveyResultAction
     *
     * @return InfoSurvey
     */
    public function addSurveyResultAction(\TeamSoft\CrmRepositoryBundle\Entity\InfoSurveyResultAction $surveyResultAction)
    {
        if (!$this->surveyResultActions->contains($surveyResultAction)) {
            $surveyResultAction->setAction($this);
            $this->surveyResultActions[] = $surveyResultAction;
        }

        return $this;
    }

    /**
     * Remove surveyResultAction.
     *
     * @param \TeamSoft\CrmRepositoryBundle\Entity\InfoSurveyResultAction $surveyResultAction
     *
     */
    public function removeSurveyResultAction(\TeamSoft\CrmRepositoryBundle\Entity\InfoSurveyResultAction $surveyResultAction)
    {
        if ($this->surveyResultActions->contains($surveyResultAction)) {
            $this->surveyResultActions->removeElement($surveyResultAction);
        }
    }
}
