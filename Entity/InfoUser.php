<?php

namespace TeamSoft\CrmRepositoryBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Symfony\Component\PasswordHasher\Hasher\PasswordHasherAwareInterface;
use Symfony\Component\Security\Core\User\PasswordAuthenticatedUserInterface;
use TeamSoft\CrmRepositoryBundle\Entity\CarReport\InfoCar;
use TeamSoft\CrmRepositoryBundle\Entity\CarReport\InfoCarReportSheet;
use TeamSoft\CrmRepositoryBundle\Entity\GridConfig\PoGridConfigByUser;
use TeamSoft\CrmRepositoryBundle\Entity\RTC\InfoRtcRoom;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * InfoUser
 *
 * @ORM\Table(name="info_user")
 * @ORM\Entity(repositoryClass="TeamSoft\CrmRepositoryBundle\Repository\InfoUser")
 * @UniqueEntity(fields={"poLogin"})
 * @UniqueEntity(fields={"email"})
 */
class InfoUser implements ServiceFieldInterface, AdvancedUserInterface, PasswordHasherAwareInterface, PasswordAuthenticatedUserInterface
{

    const FLAG_ALLOW_EXPORT = 0b0100;
    const FLAG_CAN_CHANGE_ACTIVE = 0b0100000;
    const FLAG_CAN_SET_OWNER = 0b01000000;
    public const PASSWORD_REGEX_PATTERN = '/^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)[A-Za-z\d]{8,}$/';

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255, nullable=true)
     */
    private $name;

    /**
     * path to image (typically /api/uploads/a49fc1b6f80b593ddf0c6a9f371d84e6.png)
     *
     * @var string
     *
     * @ORM\Column(name="avatar", type="string", length=255, nullable=true)
     */
    private $avatar;

    /**
     * @var string
     *
     * @ORM\Column(name="password", type="string", length=255, nullable=true)
     */
    private $password;

    /**
     * @var string
     *
     * @ORM\Column(name="profile", type="text", length=16, nullable=true)
     */
    private $profile;

    /**
     * @var integer
     *
     * @ORM\Column(name="IsRep", type="integer", nullable=true)
     */
    private $isrep;

    /**
     * @var integer
     *
     * @ORM\Column(name="IsReg", type="integer", nullable=true)
     */
    private $isreg;

    /**
     * @var integer
     *
     * @ORM\Column(name="isKAM", type="integer", nullable=true)
     */
    private $iskam;

    /**
     * @var integer
     *
     * @ORM\Column(name="isholiday", type="integer", nullable=true)
     */
    private $isholiday;

    /**
     * @var integer
     *
     * @ORM\Column(name="adjcol", type="integer", nullable=true)
     */
    private $adjcol;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="BirthDate", type="datetime", nullable=true)
     */
    private $birthdate;

    /**
     * @var string
     *
     * @ORM\Column(name="phonehome", type="string", length=255, nullable=true)
     */
    private $phonehome;

    /**
     * @var string
     *
     * @ORM\Column(name="phonemob", type="string", length=255, nullable=true)
     */
    private $phonemob;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="po_password_modified", type="datetime", nullable=true)
     */
    private $poPasswordModified;

    /**
     * @var string
     *
     * @ORM\Column(name="email", type="string", length=255, nullable=true)
     */
    private $email;

    /**
     * @var integer
     *
     * @ORM\Column(name="koef", type="integer", nullable=true)
     */
    private $koef;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="varchar_or_n_varchar_image_or_string", nullable=true)
     */
    private $description;

    /**
     * @var string
     *
     * @ORM\Column(name="guid", type="guid", nullable=true)
     */
    private $guid;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="currenttime", type="datetime", nullable=true)
     */
    private $currenttime;

    /**
     * @var string
     *
     * @ORM\Column(name="moduser", type="string", length=16, nullable=true)
     */
    private $moduser;

    /**
     * @var string
     *
     * @ORM\Column(name="phone2", type="string", length=255, nullable=true)
     */
    private $phone2;

    /**
     * @var integer
     *
     * @ORM\Column(name="iscitydep", type="integer", nullable=true)
     */
    private $iscitydep;

    /**
     * @var integer
     *
     * @ORM\Column(name="isnowork", type="integer", nullable=true)
     */
    private $isnowork;

    /**
     * @var integer
     *
     * @ORM\Column(name="flags", type="integer", nullable=true)
     */
    private $flags;

    /**
     * @var string
     *
     * @ORM\Column(name="ncard", type="string", length=255, nullable=true)
     */
    private $ncard;

    /**
     * @var string
     *
     * @ORM\Column(name="clientAppPlatform", type="string", length=20, nullable=true)
     */
    private $appPlatform;

    /**
     * @var string
     *
     * @ORM\Column(name="car", type="string", length=255, nullable=true)
     */
    private $car;

    /**
     * @var string
     *
     * @ORM\Column(name="po_login", type="string", length=255, nullable=true)
     */
    private $poLogin;

    /**
     * @var string
     *
     * @ORM\Column(name="po_password", type="string", length=255, nullable=true)
     */
    private $poPassword;

    /**
     * @var integer
     *
     * @ORM\Column(name="export", type="integer", nullable=true)
     */
    private $export;

    /**
     * @var string
     *
     * @ORM\Column(name="primeryIMEI", type="string", length=50, nullable=true, unique=true)
     */
    private $primeryimei;

    /**
     * @var string
     *
     * @ORM\Column(name="alterIMEI", type="string", length=50, nullable=true, unique=true)
     */
    private $alterimei;

    /**
     * @var string
     *
     * @ORM\Column(name="GMT", type="string")
     */
    private $gmt = '2';

    /**
     * @var InfoDictionary
     *
     * @ORM\ManyToOne(targetEntity="InfoDictionary")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="position_id", referencedColumnName="id")
     * })
     */
    private $position;

    /**
     * @var InfoRole
     *
     * @ORM\ManyToOne(targetEntity="InfoRole")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="role_id", referencedColumnName="id")
     * })
     */
    private $role;

    /**
     * @var InfoLanguage
     *
     * @ORM\OneToOne(targetEntity="InfoLanguage")
     * @ORM\JoinColumn(name="language_id", referencedColumnName="id")
     */
    private $language;

    /**
     * @var InfoCountry|null
     *
     * @ORM\OneToOne(targetEntity="InfoCountry")
     * @ORM\JoinColumn(name="country_id", referencedColumnName="id")
     */
    private $country;

    /**
     * @var InfoCity
     *
     * @ORM\OneToOne(targetEntity="InfoCity")
     * @ORM\JoinColumn(name="city_id", referencedColumnName="id")
     */
    private $city;

    /**
     * @var InfoUser
     *
     * @ORM\OneToOne(targetEntity="InfoUser", cascade={"remove"})
     * @ORM\JoinColumn(name="inheritance_id", referencedColumnName="id")
     */
    private $inheritance;

    /**
     * @var InfoUser
     *
     * @ORM\ManyToOne(targetEntity="InfoUser", inversedBy="directSubmissions")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="submission_id", referencedColumnName="id")
     * })
     */
    private $submission;

    /**
     * @ORM\OneToMany(targetEntity="InfoUser", mappedBy="submission")
     */
    private $directSubmissions;

    /**
     * @var InfoUser
     *
     * @ORM\ManyToMany(targetEntity="InfoUser")
     * @ORM\JoinTable(name="info_contactsubmission",
     *     joinColumns={@ORM\JoinColumn(name="child_id", referencedColumnName="id")},
     *     inverseJoinColumns={@ORM\JoinColumn(name="subj_id", referencedColumnName="id")}
     * )
     */
    private $submissions;

    /**
     * @var InfoUser[]
     *
     * @ORM\ManyToMany(targetEntity="InfoUser")
     * @ORM\JoinTable(name="info_contactsubmission",
     *     joinColumns={@ORM\JoinColumn(name="subj_id", referencedColumnName="id")},
     *     inverseJoinColumns={@ORM\JoinColumn(name="child_id", referencedColumnName="id")}
     * )
     */
    private $subordinates;

    /**
     * @var InfoMobil
     *
     * @ORM\ManyToOne(targetEntity="InfoMobil")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="mobil_id", referencedColumnName="id")
     * })
     */
    private $mobil;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="flags_due_date", type="datetime", nullable=true)
     */
    private $flagsDueDate;

    /**
     * @var integer
     *
     * @ORM\Column(name="is_gps_disabled", type="integer", nullable=true)
     */
    private $isGpsDisabled;

    /**
     * @var integer
     *
     * @ORM\Column(name="hexcolor", type="string", length=20, nullable=true)
     */
    private $color;

    /**
     * @ORM\ManyToMany(targetEntity="InfoTesting")
     * @ORM\JoinTable(name="info_testinguser",
     *   joinColumns={@ORM\JoinColumn(name="user_id", referencedColumnName="id")},
     *   inverseJoinColumns={@ORM\JoinColumn(name="testing_id", referencedColumnName="id")}
     * )
     */
    private $testingCollection;

    /**
     * @ORM\ManyToMany(targetEntity="InfoRegion")
     * @ORM\JoinTable(name="info_regioninuser",
     *     joinColumns={@ORM\JoinColumn(name="user_id", referencedColumnName="id")},
     *     inverseJoinColumns={@ORM\JoinColumn(name="region_id", referencedColumnName="id")}
     * )
     */
    private $regionCollection;

    /**
     * @ORM\ManyToMany(targetEntity="InfoCity")
     * @ORM\JoinTable(name="info_cityinuser",
     *     joinColumns={@ORM\JoinColumn(name="user_id", referencedColumnName="id")},
     *     inverseJoinColumns={@ORM\JoinColumn(name="city_id", referencedColumnName="id")}
     * )
     */
    private $cities;

    /**
     * @ORM\ManyToMany(targetEntity="InfoDirection")
     * @ORM\JoinTable(name="info_userdirection",
     *     joinColumns={@ORM\JoinColumn(name="user_id", referencedColumnName="id")},
     *     inverseJoinColumns={@ORM\JoinColumn(name="direction_id", referencedColumnName="id")}
     * )
     */
    private $directionCollection;

    /**
     * @ORM\ManyToMany(targetEntity="InfoSubdirection")
     * @ORM\JoinTable(name="info_usersubdirection",
     *     joinColumns={@ORM\JoinColumn(name="user_id", referencedColumnName="id")},
     *     inverseJoinColumns={@ORM\JoinColumn(name="subdirection_id", referencedColumnName="id")}
     * )
     */
    private $subdirections;

    /**
     * @ORM\OneToMany(targetEntity="TeamSoft\CrmRepositoryBundle\Entity\Etms\InfoPolygon", mappedBy="user")
     */
    private $polygonCollection;

    /**
     * @ORM\OneToMany(targetEntity="TeamSoft\CrmRepositoryBundle\Entity\Etms\InfoEtmsuserapprove", mappedBy="user")
     */
    private $approvedCollection;

    /**
     * @ORM\OneToOne(targetEntity="User", mappedBy="infoUser", cascade={"persist"})
     */
    private $poUser;

    /**
     * @var integer
     *
     * @ORM\Column(name="area_approved", type="integer", nullable=true)
     */
    private $approved;

    /**
     * @var string
     *
     * @ORM\Column(name="gpspoint", type="geometry", nullable=true)
     */
    private $gpsPoint;

    /**
     * @var string
     *
     * @ORM\Column(name="external_id", type="string", length=100, nullable=true)
     */
    private $externalId;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="workstartdate", type="datetime", nullable=true)
     */
    private $workstartdate;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="workenddate", type="datetime", nullable=true)
     */
    private $workenddate;

    /**
     * @var integer
     *
     * @ORM\Column(name="iscontactsignenabled", type="integer", nullable=true)
     */
    private $isContactSignEnabled;

    /**
     * @var string
     *
     * @ORM\Column(name="ad_identifier", type="string", length=255, nullable=true)
     */
    private $adIdentifier;

    /**
     * @var string
     *
     * @ORM\Column(name="ad_city", type="string", length=255, nullable=true)
     */
    private $adCity;

    /**
     * @var string
     *
     * @ORM\Column(name="webinarlink", type="string", length=512, nullable=true)
     */
    private $webinarlink;

    /**
     * @var ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="\TeamSoft\CrmRepositoryBundle\Entity\RTC\InfoRtcRoom", mappedBy="responsible")
     */
    private $rtcRooms;

    /**
     * @var string
     * @ORM\Column(name="siplogin", type="string", length=255, nullable=true)
     */
    private $sipLogin;

    /**
     * @var string
     * @ORM\Column(name="sippass", type="string", length=255, nullable=true)
     */
    private $sipPass;

    /**
     * @var ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="InfoMcmedetailing", mappedBy="user")
     */
    private $mcmedetailingCollection;

    /**
     * @var int
     *
     * @ORM\Column(name="edetailing_enabled", type="integer", nullable=true)
     */
    private $edetailingEnabled;

    /**
     * @ORM\ManyToOne(targetEntity="TeamSoft\CrmRepositoryBundle\Entity\CarReport\InfoCar", inversedBy="users")
     * @ORM\JoinColumn(name="carnum_id", referencedColumnName="id")
     */
    private $carModel;

    /**
     * @ORM\OneToMany(targetEntity="TeamSoft\CrmRepositoryBundle\Entity\CarReport\InfoCarReportSheet", mappedBy="user")
     */
    private $carReportSheets;

    /**
     * @ORM\OneToMany(targetEntity="InfoPlan", mappedBy="owner")
     */
    private $plans;

    /**
     * @var string|null
     * @ORM\Column(name="passportnumber", type="string", length=20, nullable=true)
     */
    private $passportNumber;

    /**
     * @var PoGridConfigByUser[]|Collection
     * @ORM\OneToMany(targetEntity="TeamSoft\CrmRepositoryBundle\Entity\GridConfig\PoGridConfigByUser", mappedBy="user")
     */
    private $gridConfigByUsers;

    /**
     * @var integer|null
     * @ORM\Column(name="hide_me_in_geo_force", type="integer", nullable=true)
     */
    private $hideMeInGeoForce;

    /**
     * @ORM\OneToMany(targetEntity=PoApiToken::class, mappedBy="user")
     */
    private $poApiTokens;

    /**
     * @var InfoStorage|null
     *
     * @ORM\OneToOne(targetEntity="InfoStorage", mappedBy="user")
     */
    private $storage;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->testingCollection = new ArrayCollection();
        $this->regionCollection = new ArrayCollection();
        $this->cities = new ArrayCollection();
        $this->directionCollection = new ArrayCollection();
        $this->subdirections = new ArrayCollection();
        $this->polygonCollection = new ArrayCollection();
        $this->approvedCollection = new ArrayCollection();
        $this->submissions = new ArrayCollection();
        $this->subordinates = new ArrayCollection();
        $this->rtcRooms = new ArrayCollection();
        $this->mcmedetailingCollection = new ArrayCollection();
        $this->carReportSheets = new ArrayCollection();
        $this->plans = new ArrayCollection();
        $this->gridConfigByUsers = new ArrayCollection();
        $this->poApiTokens = new ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return InfoUser
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set avatar
     *
     * @param string $avatar
     * @return InfoUser
     */
    public function setAvatar($avatar)
    {
        $this->avatar = $avatar;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getAvatar()
    {
        return $this->avatar;
    }

    /**
     * Set password
     *
     * @param string $password
     * @return InfoUser
     */
    public function setPassword($password)
    {
        $this->password = $password;

        return $this;
    }

    /**
     * Set profile
     *
     * @param string $profile
     * @return InfoUser
     */
    public function setProfile($profile)
    {
        $this->profile = $profile;

        return $this;
    }

    /**
     * Get profile
     *
     * @return string
     */
    public function getProfile()
    {
        return $this->profile;
    }

    /**
     * Get directSubmission
     *
     * @return InfoUser[]|Collection
     */
    public function getDirectSubmissions()
    {
        return $this->directSubmissions;
    }

    /**
     * Set isrep
     *
     * @param integer $isrep
     * @return InfoUser
     */
    public function setIsrep($isrep)
    {
        $this->isrep = $isrep;

        return $this;
    }

    /**
     * Get isrep
     *
     * @return integer
     */
    public function getIsrep()
    {
        return $this->isrep;
    }

    /**
     * Set iskam
     *
     * @param integer $iskam
     * @return InfoUser
     */
    public function setIskam($iskam)
    {
        $this->iskam = $iskam;

        return $this;
    }

    /**
     * Get iskam
     *
     * @return integer
     */
    public function getIskam()
    {
        return $this->iskam;
    }

    /**
     * Set isreg
     *
     * @param integer $isreg
     * @return InfoUser
     */
    public function setIsreg($isreg)
    {
        $this->isreg = $isreg;

        return $this;
    }

    /**
     * Get isreg
     *
     * @return integer
     */
    public function getIsreg()
    {
        return $this->isreg;
    }

    /**
     * Set isholiday
     *
     * @param integer $isholiday
     * @return InfoUser
     */
    public function setIsholiday($isholiday)
    {
        $this->isholiday = $isholiday;

        return $this;
    }

    /**
     * Get isholiday
     *
     * @return integer
     */
    public function getIsholiday()
    {
        return $this->isholiday;
    }

    /**
     * Set adjcol
     *
     * @param integer $adjcol
     * @return InfoUser
     */
    public function setAdjcol($adjcol)
    {
        $this->adjcol = $adjcol;

        return $this;
    }

    /**
     * Get adjcol
     *
     * @return integer
     */
    public function getAdjcol()
    {
        return $this->adjcol;
    }

    /**
     * Set birthdate
     *
     * @param \DateTime $birthdate
     * @return InfoUser
     */
    public function setBirthdate($birthdate)
    {
        $this->birthdate = $birthdate;

        return $this;
    }

    /**
     * Get birthdate
     *
     * @return \DateTime
     */
    public function getBirthdate()
    {
        return $this->birthdate;
    }

    /**
     * Get birthdate
     *
     * @return string
     */
    public function getFormattedBirthdate()
    {
        return $this->birthdate ? $this->birthdate->format('d.m.Y') : null;
    }

    /**
     * Set phonehome
     *
     * @param string $phonehome
     * @return InfoUser
     */
    public function setPhonehome($phonehome)
    {
        $this->phonehome = $phonehome;

        return $this;
    }

    /**
     * Get phonehome
     *
     * @return string
     */
    public function getPhonehome()
    {
        return $this->phonehome;
    }

    /**
     * Set phonemob
     *
     * @param string $phonemob
     * @return InfoUser
     */
    public function setPhonemob($phonemob)
    {
        $this->phonemob = $phonemob;

        return $this;
    }

    /**
     * Get phonemob
     *
     * @return string
     */
    public function getPhonemob()
    {
        return $this->phonemob;
    }

    /**
     * Set po_password_modified
     *
     * @param \DateTime $poPasswordModified
     * @return InfoUser
     */
    public function setPoPasswordModified(?\DateTime $poPasswordModified): self
    {
        $this->poPasswordModified = $poPasswordModified;

        return $this;
    }

    /**
     * Get po_password_modified
     *
     * @return \DateTime|null
     */
    public function getPoPasswordModified(): ?\DateTime
    {
        return $this->poPasswordModified;
    }

    /**
     * Set email
     *
     * @param string $email
     * @return InfoUser
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Get email
     *
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set koef
     *
     * @param integer $koef
     * @return InfoUser
     */
    public function setKoef($koef)
    {
        $this->koef = $koef;

        return $this;
    }

    /**
     * Get koef
     *
     * @return integer
     */
    public function getKoef()
    {
        return $this->koef;
    }

    /**
     * Set description
     *
     * @param string $description
     * @return InfoUser
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set guid
     *
     * @param string $guid
     * @return InfoUser
     */
    public function setGuid($guid)
    {
        $this->guid = $guid;

        return $this;
    }

    /**
     * Get guid
     *
     * @return string
     */
    public function getGuid()
    {
        return $this->guid;
    }

    /**
     * Set currenttime
     *
     * @param \DateTime $currenttime
     * @return InfoUser
     */
    public function setCurrenttime(\DateTime $currenttime)
    {
        $this->currenttime = $currenttime;

        return $this;
    }

    /**
     * Get currenttime
     *
     * @return \DateTime
     */
    public function getCurrenttime()
    {
        return $this->currenttime;
    }

    /**
     * Set moduser
     *
     * @param string $moduser
     * @return InfoUser
     */
    public function setModuser($moduser)
    {
        $this->moduser = $moduser;

        return $this;
    }

    /**
     * Get moduser
     *
     * @return string
     */
    public function getModuser()
    {
        return $this->moduser;
    }

    /**
     * Set phone2
     *
     * @param string $phone2
     * @return InfoUser
     */
    public function setPhone2($phone2)
    {
        $this->phone2 = $phone2;

        return $this;
    }

    /**
     * Get phone2
     *
     * @return string
     */
    public function getPhone2()
    {
        return $this->phone2;
    }

    /**
     * Set iscitydep
     *
     * @param integer $iscitydep
     * @return InfoUser
     */
    public function setIscitydep($iscitydep)
    {
        $this->iscitydep = $iscitydep;

        return $this;
    }

    /**
     * Get iscitydep
     *
     * @return integer
     */
    public function getIscitydep()
    {
        return $this->iscitydep;
    }

    /**
     * Set isnowork
     *
     * @param integer $isnowork
     * @return InfoUser
     */
    public function setIsnowork($isnowork)
    {
        $this->isnowork = $isnowork;

        return $this;
    }

    /**
     * Get isnowork
     *
     * @return integer
     */
    public function getIsnowork()
    {
        return $this->isnowork;
    }

    /**
     * Set ncard
     *
     * @param string $ncard
     * @return InfoUser
     */
    public function setNcard($ncard)
    {
        $this->ncard = $ncard;

        return $this;
    }

    /**
     * Get ncard
     *
     * @return string
     */
    public function getNcard()
    {
        return $this->ncard;
    }

    /**
     * Set app platform
     *
     * @param string $platform
     * @return InfoUser
     */
    public function setAppPlatform($platform)
    {
        $this->appPlatform = $platform;

        return $this;
    }

    /**
     * Get app platform
     *
     * @return string
     */
    public function getAppPlatform()
    {
        return $this->appPlatform;
    }

    /**
     * Set car
     *
     * @param string $car
     * @return InfoUser
     */
    public function setCar($car)
    {
        $this->car = $car;

        return $this;
    }

    /**
     * Get car
     *
     * @return string
     */
    public function getCar()
    {
        return $this->car;
    }

    /**
     * Set poLogin
     *
     * @param string $poLogin
     * @return InfoUser
     */
    public function setPoLogin($poLogin)
    {
        $this->poLogin = $poLogin;

        return $this;
    }

    /**
     * Get poLogin
     *
     * @return string
     */
    public function getPoLogin()
    {
        return $this->poLogin;
    }

    /**
     * Set poPassword
     *
     * @param string $poPassword
     * @return InfoUser
     */
    public function setPoPassword($poPassword)
    {
        $this->poPassword = $poPassword;

        return $this;
    }

    /**
     * Get poPassword
     *
     * @return string
     */
    public function getPoPassword()
    {
        return $this->poPassword;
    }

    /**
     * Set export
     *
     * @param integer $export
     * @return InfoUser
     */
    public function setExport($export)
    {
        $this->export = $export;

        return $this;
    }

    /**
     * Get export
     *
     * @return integer
     */
    public function getExport()
    {
        return $this->export;
    }

    /**
     * Set primeryimei
     *
     * @param string $primeryimei
     * @return InfoUser
     */
    public function setPrimeryimei($primeryimei)
    {
        $this->primeryimei = $primeryimei;

        return $this;
    }

    /**
     * Get primeryimei
     *
     * @return string
     */
    public function getPrimeryimei()
    {
        return $this->primeryimei;
    }

    /**
     * Set alterimei
     *
     * @param string $alterimei
     * @return InfoUser
     */
    public function setAlterimei($alterimei)
    {
        $this->alterimei = $alterimei;

        return $this;
    }

    /**
     * Get alterimei
     *
     * @return string
     */
    public function getAlterimei()
    {
        return $this->alterimei;
    }

    /**
     * Set gmt
     *
     * @param string $gmt
     * @return InfoUser
     */
    public function setGmt($gmt)
    {
        $this->gmt = $gmt;

        return $this;
    }

    /**
     * Get gmt
     *
     * @return string
     */
    public function getGmt()
    {
        return $this->gmt;
    }

    /**
     * Set position
     *
     * @param InfoDictionary $position
     * @return InfoUser
     */
    public function setPosition(InfoDictionary $position = null)
    {
        $this->position = $position;

        return $this;
    }

    /**
     * Get position
     *
     * @return InfoDictionary
     */
    public function getPosition()
    {
        return $this->position;
    }

    /**
     * Set role
     *
     * @param InfoRole $role
     * @return InfoUser
     */
    public function setRole(InfoRole $role = null)
    {
        $this->role = $role;

        return $this;
    }

    /**
     * Get role
     *
     * @return InfoRole
     */
    public function getRole()
    {
        return $this->role;
    }

    /**
     * Set inheritance
     *
     * @param InfoUser $inheritance
     * @return InfoUser
     */
    public function setInheritance(InfoUser $inheritance = null)
    {
        $this->inheritance = $inheritance;

        return $this;
    }

    /**
     * Get inheritance
     *
     * @return InfoUser
     */
    public function getInheritance()
    {
        return $this->inheritance;
    }

    /**
     * Set city
     *
     * @param InfoCity $city
     * @return InfoUser
     */
    public function setCity(InfoCity $city = null)
    {
        $this->city = $city;

        return $this;
    }

    /**
     * Get city
     *
     * @return InfoCity
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
     * Set submission
     *
     * @param InfoUser $submission
     * @return InfoUser
     */
    public function setSubmission(InfoUser $submission = null)
    {
        $this->submission = $submission;

        return $this;
    }

    /**
     * Get submission
     *
     * @return InfoUser
     */
    public function getSubmission()
    {
        return $this->submission;
    }

    /**
     * Set mobil
     *
     * @param InfoMobil $mobil
     * @return InfoUser
     */
    public function setMobil(InfoMobil $mobil = null)
    {
        $this->mobil = $mobil;

        return $this;
    }

    /**
     *
     * @param \DateTime $flagsDueDate
     * @return InfoUser
     */
    public function setflagsDueDate(?\DateTime $flagsDueDate): self
    {
        $this->flagsDueDate = $flagsDueDate;

        return $this;
    }

    /**
     *
     * @return \DateTime
     */
    public function getflagsDueDate(): ?\DateTime
    {
        return $this->flagsDueDate;
    }

    /**
     * Set isGpsDisabled
     *
     * @param integer $isGpsDisabled
     * @return InfoUser
     */
    public function setIsGpsDisabled($isGpsDisabled)
    {
        $this->isGpsDisabled = $isGpsDisabled;

        return $this;
    }

    /**
     * Get isGpsDisabled
     *
     * @return integer
     */
    public function getIsGpsDisabled()
    {
        return $this->isGpsDisabled;
    }

    /**
     * Get username
     *
     * @return string
     */
    public function getUsername()
    {
        return $this->getPoLogin();
    }

    public function getUserIdentifier(): string
    {
        return $this->getPoLogin();
    }

    /**
     * Get password
     *
     * @return string
     */
    public function getPassword(): ?string
    {
        return $this->getPoUser() && $this->getPoUser()->getPassword()
            ? $this->getPoUser()->getPassword()
            : $this->getPoPassword();
    }

    /**
     * Get salt
     *
     * @return string
     */
    public function getSalt()
    {
        return $this->getPoUser() ? $this->getPoUser()->getSalt() : null;
    }

    public function getRoles(): array
    {
        $roles = [];

        if ($role = $this->getRole()) {
            if ($roleCode = $role->getCode()) {
                $roles[] = $roleCode;
            }

            $privileges = $role->getPrivileges();

            /**
             * @var InfoServiceprivilege $privilege
             */
            foreach ($privileges as $privilege) {
                if ($privilegeCode = $privilege->getCode()) {
                    $roles[] = $privilegeCode;
                }
            }

        }
        return $roles;
    }

    public function setRoles($roles)
    {
        return $this;
    }

    public function eraseCredentials()
    {
    }

    public function isEnabled(): bool
    {
        return $this->getPoUser() === null || $this->getPoUser()->getIsActive() !== 0;
    }

    public function isWork(): bool
    {
        return !$this->getIsnowork();
    }

    public function allowedHost(): ?string
    {
        $url = $this->getCountry() ? $this->getCountry()->getPharmaUrl() : null;
        return $url ? parse_url($url, PHP_URL_HOST) : null;
    }

    /**
     * @return bool
     */
    public function isAdmin()
    {
        return ($this->profile === 'lpa0HlFGfiF7L0YU+lE=' ||
            $this->profile === 'lpWlGlRcEg==' ||
            $this->getRole() && in_array($this->getRole()->getCode(), ['ROLE_ADMIN', 'ROLE_SUPER_ADMIN']));
    }

    /**
     * @return bool
     */
    public function isSuperAdmin()
    {
        //TODO: Replace on $this->isGranted('ROLE_SUPER_ADMIN')
        return $this->getRole() && $this->getRole()->getCode() === 'ROLE_SUPER_ADMIN';
    }

    /*
     * @return bool
     */
    public function checkFlags($flag)
    {
        return (bool)($this->flags & $flag);
    }

    /*
     * @return bool
     */
    public function getProcessedFlags()
    {
        return [
            'ALLOW_EXPORT' => $this->checkFlags($this::FLAG_ALLOW_EXPORT),
        ];
    }

    public function __serialize(): array
    {
        return [
            $this->id,
            $this->poLogin,
            $this->poPassword,
            $this->poUser,
        ];
    }

    public function __unserialize(array $serialized)
    {
        [
            $this->id,
            $this->poLogin,
            $this->poPassword,
            $this->poUser,
        ] = $serialized;
    }

    public function getFirstName()
    {
        $name = $this->name;
        $nameArr = preg_split('/\s+/', $name);
        if (count($nameArr) == 2 || count($nameArr) == 3) {
            $name = $nameArr[1];
        }
        return $name;
    }

    /**
     * Get mobil
     *
     * @return InfoMobil
     */
    public function getMobil()
    {
        return $this->mobil;
    }

    /**
     * Add testingCollection
     *
     * @param InfoTesting $testingCollection
     *
     * @return InfoUser
     */
    public function addTestingCollection(InfoTesting $testingCollection)
    {
        $this->testingCollection[] = $testingCollection;

        return $this;
    }

    /**
     * Remove testingCollection
     *
     * @param InfoTesting $testingCollection
     */
    public function removeTestingCollection(InfoTesting $testingCollection)
    {
        $this->testingCollection->removeElement($testingCollection);
    }

    /**
     * Get testingCollection
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getTestingCollection()
    {
        return $this->testingCollection;
    }

    /**
     * Add regionCollection
     *
     * @param InfoRegion $regionCollection
     *
     * @return InfoUser
     */
    public function addRegionCollection(InfoRegion $regionCollection)
    {
        $this->regionCollection->add($regionCollection);

        return $this;
    }

    /**
     * Remove regionCollection
     *
     * @param InfoRegion $regionCollection
     */
    public function removeRegionCollection(InfoRegion $regionCollection)
    {
        $this->regionCollection->removeElement($regionCollection);
    }

    /**
     * Get regionCollection
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getRegionCollection()
    {
        return $this->regionCollection;
    }

    /**
     * Set poUser
     *
     * @param User $poUser
     *
     * @return InfoUser
     */
    public function setPoUser(User $poUser = null)
    {
        if ($poUser) {
            $poUser->setInfoUser($this);
            $this->poUser = $poUser;
        } else {
            $poUser->setInfoUser(null);
            $this->poUser = null;
        }

        return $this;
    }

    /**
     * Get poUser
     *
     * @return User
     */
    public function getPoUser()
    {
        return $this->poUser;
    }

    public function getPasswordHasherName(): ?string
    {
        return $this->getPoUser() && $this->getPoUser()->getSalt() ? 'sha512' : null;
    }

    /**
     * Add directionCollection
     *
     * @param InfoDirection $directionCollection
     *
     * @return InfoUser
     */
    public function addDirectionCollection(InfoDirection $directionCollection)
    {
        $this->directionCollection[] = $directionCollection;

        return $this;
    }

    /**
     * Remove directionCollection
     *
     * @param InfoDirection $directionCollection
     */
    public function removeDirectionCollection(InfoDirection $directionCollection)
    {
        $this->directionCollection->removeElement($directionCollection);
    }

    /**
     * Get directionCollection
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getDirectionCollection()
    {
        return $this->directionCollection;
    }

    /**
     * Add subdirection
     *
     * @param InfoDirection $subdirection
     *
     * @return InfoUser
     */
    public function addSubdirection(InfoDirection $subdirection)
    {
        $this->subdirections[] = $subdirection;

        return $this;
    }

    /**
     * Remove subdirection
     *
     * @param InfoDirection $subdirection
     */
    public function removeSubdirection(InfoDirection $subdirection)
    {
        $this->subdirections->removeElement($subdirection);
    }

    /**
     * Get subdirections
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getSubdirections()
    {
        return $this->subdirections;
    }

    /**
     * Set color
     *
     * @param string $color
     *
     * @return InfoUser
     */
    public function setColor($color)
    {
        $this->color = $color;

        return $this;
    }

    /**
     * Get color
     *
     * @return string
     */
    public function getColor()
    {
        return $this->color;
    }

    /**
     * Add polygonCollection
     *
     * @param Etms\InfoPolygon $polygonCollection
     *
     * @return InfoUser
     */
//    public function addPolygonCollection(InfoPolygon $polygonCollection)
//    {
//        $this->polygonCollection[] = $polygonCollection;
//
//        return $this;
//    }

    /**
     * Remove polygonCollection
     *
     * @param InfoPolygon $polygonCollection
     */
//    public function removePolygonCollection(InfoPolygon $polygonCollection)
//    {
//        $this->polygonCollection->removeElement($polygonCollection);
//    }

    /**
     * Get polygonCollection
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getPolygonCollection()
    {
        return $this->polygonCollection;
    }

    /**
     * Add submission
     *
     * @param InfoUser $submission
     *
     * @return InfoUser
     */
    public function addSubmission(InfoUser $submission)
    {
        $this->submissions[] = $submission;

        return $this;
    }

    /**
     * Remove submission
     *
     * @param InfoUser $submission
     */
    public function removeSubmission(InfoUser $submission)
    {
        $this->submissions->removeElement($submission);
    }

    /**
     * Get submissions
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getSubmissions()
    {
        return $this->submissions;
    }

    /**
     * Add subordinate
     *
     * @param InfoUser $subordinate
     *
     * @return InfoUser
     */
    public function addSubordinate(InfoUser $subordinate)
    {
        $this->subordinates[] = $subordinate;

        return $this;
    }

    /**
     * Remove subordinate
     *
     * @param InfoUser $subordinate
     */
    public function removeSubordinate(InfoUser $subordinate)
    {
        $this->subordinates->removeElement($subordinate);
    }

    /**
     * Get subordinates
     *
     * @return \Doctrine\Common\Collections\ArrayCollection
     */
    public function getSubordinates()
    {
        return $this->subordinates;
    }

    /**
     * Set flags.
     *
     * @param int|null $flags
     *
     * @return InfoUser
     */
    public function setFlags($flags = null)
    {
        $this->flags = $flags;

        return $this;
    }

    /**
     * Get flags.
     *
     * @return int|null
     */
    public function getFlags()
    {
        return $this->flags;
    }

    /**
     * Set approved.
     *
     * @param int|null $approved
     *
     * @return InfoUser
     */
    public function setApproved($approved = null)
    {
        $this->approved = $approved;

        return $this;
    }

    /**
     * Get approved.
     *
     * @return int|null
     */
    public function getApproved()
    {
        return $this->approved;
    }

    /**
     * Returns cloned entity with optionally reset fields.
     * We are not using magic method __clone() because of 'clone' used in Symfony's engine classes.
     * @param array $fields fields to be cloned
     * @param InfoUser $clone
     * @return InfoUser
     */
    public function getClone($fields = [], InfoUser $clone = null)
    {
        if (!$clone) {
            $clone = new InfoUser();
        }

        if (is_array($fields) && count($fields) > 0) {
            foreach ($fields as $field) {
                $setter = 'set' . ucfirst($field);
                $getter = 'get' . ucfirst($field);
                if (method_exists($this, $setter)) {
                    $clone->{$setter}($this->{$getter}());
                }
            }
        }

        return $clone;
    }

    /**
     * Add approvedCollection.
     *
     * @param Etms\InfoEtmsuserapprove $approvedCollection
     *
     * @return InfoUser
     */
    public function addApprovedCollection(Etms\InfoEtmsuserapprove $approvedCollection)
    {
        $this->approvedCollection[] = $approvedCollection;

        return $this;
    }

    /**
     * Remove approvedCollection.
     *
     * @param Etms\InfoEtmsuserapprove $approvedCollection
     *
     * @return boolean TRUE if this collection contained the specified element, FALSE otherwise.
     */
    public function removeApprovedCollection(Etms\InfoEtmsuserapprove $approvedCollection)
    {
        return $this->approvedCollection->removeElement($approvedCollection);
    }

    /**
     * Get approvedCollection.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getApprovedCollection()
    {
        return $this->approvedCollection;
    }

    /**
     * Set gpsPoint.
     *
     * @param string|null $gpsPoint
     *
     * @return InfoUser
     */
    public function setGpsPoint($gpsPoint = null)
    {
        $this->gpsPoint = $gpsPoint;

        return $this;
    }

    /**
     * Get gpsPoint.
     *
     * @return string|null
     */
    public function getGpsPoint()
    {
        return $this->gpsPoint;
    }

    /**
     * Add city.
     *
     * @param \TeamSoft\CrmRepositoryBundle\Entity\InfoCity $city
     *
     * @return InfoUser
     */
    public function addCity(InfoCity $city)
    {
        $this->cities->add($city);

        return $this;
    }

    /**
     * Remove city.
     *
     * @param \TeamSoft\CrmRepositoryBundle\Entity\InfoCity $city
     *
     * @return boolean TRUE if this collection contained the specified element, FALSE otherwise.
     */
    public function removeCity(InfoCity $city)
    {
        return $this->cities->removeElement($city);
    }

    /**
     * @param InfoLanguage $language
     *
     * @return InfoUser
     */
    public function setLanguage(InfoLanguage $language)
    {
        $this->language = $language;
        return $this;
    }

    /**
     * @return InfoLanguage
     */
    public function getLanguage()
    {
        return $this->language;
    }

    /**
     * Get cities.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getCities()
    {
        return $this->cities;
    }

    /**
     * Set externalId
     *
     * @param string $externalId
     * @return InfoUser
     */
    public function setExternalId($externalId)
    {
        $this->externalId = $externalId;

        return $this;
    }

    /**
     * Get externalId
     *
     * @return string
     */
    public function getExternalId()
    {
        return $this->externalId;
    }

    /**
     * Set workstartdate
     *
     * @param \DateTime $workstartdate
     * @return InfoUser
     */
    public function setWorkstartdate($workstartdate)
    {
        $this->workstartdate = $workstartdate;

        return $this;
    }

    /**
     * Get workstartdate
     *
     * @return \DateTime
     */
    public function getWorkstartdate()
    {
        return $this->workstartdate;
    }

    /**
     * Get workstartdate
     *
     * @return string
     */
    public function getFormattedWorkstartdate()
    {
        return $this->workstartdate ? $this->workstartdate->format('d.m.Y') : null;
    }

    /**
     * Set workenddate
     *
     * @param \DateTime $workenddate
     * @return InfoUser
     */
    public function setWorkenddate($workenddate)
    {
        $this->workenddate = $workenddate;

        return $this;
    }

    /**
     * Get workenddate
     *
     * @return \DateTime
     */
    public function getWorkenddate()
    {
        return $this->workenddate;
    }

    /**
     * Set isContactSignEnabled.
     *
     * @param int|null $isContactSignEnabled
     *
     * @return InfoUser
     */
    public function setIsContactSignEnabled($isContactSignEnabled = null)
    {
        $this->isContactSignEnabled = $isContactSignEnabled;

        return $this;
    }

    /**
     * Get isContactSignEnabled.
     *
     * @return int|null
     */
    public function getIsContactSignEnabled()
    {
        return $this->isContactSignEnabled;
    }

    /**
     * Get workenddate
     *
     * @return string
     */
    public function getFormattedWorkenddate()
    {
        return $this->workenddate ? $this->workenddate->format('d.m.Y') : null;
    }

    /**
     * @param InfoCountry $country
     *
     * @return InfoUser
     */
    public function setCountry(?InfoCountry $country)
    {
        $this->country = $country;
        return $this;
    }

    /**
     * @return InfoCountry
     */
    public function getCountry()
    {
        return $this->country;
    }

    /**
     * Set adIdentifier
     *
     * @param string $adIdentifier
     * @return InfoUser
     */
    public function setAdIdentifier($adIdentifier)
    {
        $this->adIdentifier = $adIdentifier;

        return $this;
    }

    /**
     * Get adIdentifier
     *
     * @return string
     */
    public function getAdIdentifier()
    {
        return $this->adIdentifier;
    }

    /**
     * Set adCity
     *
     * @param string $adCity
     * @return InfoUser
     */
    public function setAdCity($adCity)
    {
        $this->adCity = $adCity;

        return $this;
    }

    /**
     * Get adCity
     *
     * @return string
     */
    public function getAdCity()
    {
        return $this->adCity;
    }

    /**
     * Set webinarlink
     *
     * @param string $webinarlink
     * @return InfoUser
     */
    public function setWebinarlink($webinarlink)
    {
        $this->webinarlink = $webinarlink;

        return $this;
    }

    /**
     * Get webinarlink
     *
     * @return string
     */
    public function getWebinarlink()
    {
        return $this->webinarlink;
    }

    public function getRoleId(): ?int
    {
        return $this->getRole() ? $this->getRole()->getId() : null;
    }

    public function getPositionId(): ?int
    {
        return $this->getPosition() ? $this->getPosition()->getId() : null;
    }

    public function getPositionName(): ?string
    {
        return $this->getPosition() ? $this->getPosition()->getName() : null;
    }

    public function getCountryLanguageId(): ?int
    {
        return $this->getCountry() ? $this->getCountry()->getLanguageId() : null;
    }

    public function getCountryId(): ?int
    {
        return $this->getCountry() ? $this->getCountry()->getId() : null;
    }

    /**
     * @return ArrayCollection
     */
    public function getRtcRooms()
    {
        return $this->rtcRooms;
    }

    /**
     * @param InfoRtcRoom $RTCRoom
     * @return $this
     */
    public function addRtcRoom(InfoRtcRoom $RTCRoom)
    {
        if (!$this->rtcRooms->contains($RTCRoom)) {
            $this->rtcRooms->add($RTCRoom);
        }
        $RTCRoom->setResponsible($this);
        return $this;
    }

    /**
     * @param InfoRtcRoom $RTCRoom
     * @return $this
     */
    public function removeRtcRoom(InfoRtcRoom $RTCRoom)
    {
        if ($this->rtcRooms->contains($RTCRoom)) {
            $this->rtcRooms->removeElement($RTCRoom);
        }
        $RTCRoom->setResponsible(null);
        return $this;
    }

    /**
     * @return $this
     */
    public function clearRtcRooms()
    {
        $this->rtcRooms->clear();
        return $this;
    }

    /**
     * @return string
     */
    public function getSipLogin()
    {
        return $this->sipLogin;
    }

    /**
     * @param $sipLogin
     * @return $this
     */
    public function setSipLogin($sipLogin)
    {
        $this->sipLogin = $sipLogin;
        return $this;
    }

    /**
     * @return string
     */
    public function getSipPass()
    {
        return $this->sipPass;
    }

    /**
     * @param $sipPass
     * @return $this
     */
    public function setSipPass($sipPass)
    {
        $this->sipPass = $sipPass;
        return $this;
    }

    /**
     * @return ArrayCollection
     */
    public function getMcmedetailingCollection()
    {
        return $this->mcmedetailingCollection;
    }

    /**
     * @param InfoMcmedetailing $mcmedetailing
     * @return $this
     */
    public function addMcmedetailingCollection(InfoMcmedetailing $mcmedetailing)
    {
        if (!$this->mcmedetailingCollection->contains($mcmedetailing)) {
            $this->mcmedetailingCollection->add($mcmedetailing);
        }
        $mcmedetailing->setUser($this);
        return $this;
    }

    /**
     * @param InfoMcmedetailing $mcmedetailing
     * @return $this
     */
    public function removeMcmedetailingCollection(InfoMcmedetailing $mcmedetailing)
    {
        if ($this->mcmedetailingCollection->contains($mcmedetailing)) {
            $this->mcmedetailingCollection->removeElement($mcmedetailing);
        }
        $mcmedetailing->setUser(null);
        return $this;
    }

    /**
     * @return $this
     */
    public function clearMcmedetailingCollection()
    {
        $this->mcmedetailingCollection->clear();
        return $this;
    }

    /**
     * @return int
     */
    public function getEdetailingEnabled()
    {
        return $this->edetailingEnabled;
    }

    /**
     * @param $edetailingEnabled
     * @return $this
     */
    public function setEdetailingEnabled($edetailingEnabled)
    {
        $this->edetailingEnabled = $edetailingEnabled;
        return $this;
    }

    /**
     * @return bool
     */
    public function isEdetailingEnabled()
    {
        return !!$this->edetailingEnabled;
    }

    /**
     * @return InfoCar|null
     */
    public function getCarModel(): ?InfoCar
    {
        return $this->carModel;
    }

    /**
     * @param InfoCar|null $carModel
     * @return self
     */
    public function setCarModel(?InfoCar $carModel): self
    {
        $this->carModel = $carModel;
        return $this;
    }

    /**
     * Get plans
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getPlans()
    {
        return $this->plans;
    }

    /**
     * @param InfoPlan $plan
     * @return $this
     */
    public function addPlan(InfoPlan $plan): self
    {
        if (!$this->plans->contains($plan)) {
            $plan->setOwner($this);
            $this->plans->add($plan);
        }

        return $this;
    }

    /**
     * @param InfoPlan $plan
     * @return $this
     */
    public function removePlan(InfoPlan $plan): self
    {
        $plan->setOwner(null);
        $this->plans->removeElement($plan);

        return $this;
    }


    public function addCarReportSheet(InfoCarReportSheet $carReportSheet): self
    {
        if (!$this->carReportSheets->contains($carReportSheet)) {
            $carReportSheet->setUser($this);
            $this->carReportSheets->add($carReportSheet);
        }

        return $this;
    }

    public function removeCarReportSheet(InfoCarReportSheet $carReportSheet): self
    {
        $this->carReportSheets->removeElement($carReportSheet);

        return $this;
    }

    /**
     * @return string|null
     */
    public function getPassportNumber(): ?string
    {
        return $this->passportNumber;
    }

    /**
     * @param string|null $passportNumber
     * @return self
     */
    public function setPassportNumber(?string $passportNumber): self
    {
        $this->passportNumber = $passportNumber;
        return $this;
    }

    /**
     * @return Collection|PoGridConfigByUser[]
     */
    public function getGridConfigByUsers(): Collection
    {
        return $this->gridConfigByUsers;
    }

    public function setGridConfigByUser(PoGridConfigByUser $poGridConfigByUser): self
    {
        if (!$this->gridConfigByUsers->contains($poGridConfigByUser)) {
            $this->gridConfigByUsers->add($poGridConfigByUser);
            $poGridConfigByUser->setUser($this);
        }

        return $this;
    }

    public function removeGridConfigByUser(PoGridConfigByUser $poGridConfigByUser): self
    {
        if ($this->gridConfigByUsers->removeElement($poGridConfigByUser)) {
            $poGridConfigByUser->setUser(null);
        }

        return $this;
    }

    public function hasRole(string $role): bool
    {
        return $this->hasRoles([$role]);
    }

    public function hasRoles(array $roles): bool
    {
        return (bool)array_intersect($roles, $this->getRoles());
    }

    /**
     * @return mixed
     */
    public function getHideMeInGeoForce()
    {
        return $this->hideMeInGeoForce;
    }

    /**
     * @param mixed $hideMeInGeoForce
     * @return self
     */
    public function setHideMeInGeoForce($hideMeInGeoForce): self
    {
        $this->hideMeInGeoForce = $hideMeInGeoForce;

        return $this;
    }

    /**
     * @return Collection|PoApiToken[]
     */
    public function getPoApiTokens(): Collection
    {
        return $this->poApiTokens;
    }

    public function addPoApiToken(PoApiToken $poApiToken): self
    {
        if (!$this->poApiTokens->contains($poApiToken)) {
            $this->poApiTokens[] = $poApiToken;
            $poApiToken->setUser($this);
        }

        return $this;
    }

    public function removePoApiToken(PoApiToken $poApiToken): self
    {
        if ($this->poApiTokens->removeElement($poApiToken)) {
            // set the owning side to null (unless already changed)
            if ($poApiToken->getUser() === $this) {
                $poApiToken->setUser(null);
            }
        }

        return $this;
    }

    /**
     * @return \TeamSoft\CrmRepositoryBundle\Entity\InfoStorage|null
     */
    public function getStorage(): ?InfoStorage
    {
        return $this->storage;
    }

    /**
     * @param  \TeamSoft\CrmRepositoryBundle\Entity\InfoStorage|null  $storage
     *
     * @return  $this
     */
    public function setStorage(?InfoStorage $storage): self
    {
        $this->storage = $storage;

        return $this;
    }

    public function isAccountNonLocked(): bool
    {
        return true;
    }

    public function isAccountNonExpired(): bool
    {
        return true;
    }

    public function isCredentialsNonExpired(int $daysIntervalForPasswordChange): bool
    {
        if ($this->getPoPasswordModified()) {
            $dateInterval =  ((new \DateTime('now'))->diff($this->poPasswordModified));

            return !($daysIntervalForPasswordChange > 0 && $dateInterval->days >= $daysIntervalForPasswordChange);
        }
        return true;
    }
}
