<?php

namespace TeamSoft\CrmRepositoryBundle\Entity;

use DateTime;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\Common\Collections\Criteria;
use Doctrine\ORM\Mapping as ORM;

/**
 * InfoReportTab
 *
 * @ORM\Table(name="info_reporttab")
 * @ORM\Entity()
 */
class InfoReportTab implements ServiceFieldInterface
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string|null
     *
     * @ORM\Column(name="name", type="string", length=255, nullable=true)
     */
    private $name;

    /**
     * @var string|null
     *
     * @ORM\Column(name="position", type="integer", length=10, nullable=true)
     */
    private $position;

    /**
     * @var DateTime|null
     *
     * @ORM\Column(name="currenttime", type="datetime", nullable=true)
     */
    private $currenttime;

    /**
     * @var string|null
     *
     * @ORM\Column(name="moduser", type="string", length=16, nullable=true)
     * )
     */
    private $moduser;

    /**
     * @var string|null
     *
     * @ORM\Column(name="guid", type="guid", nullable=true)
     */
    private $guid;

    /**
     * Many tabs belongs to one report
     * @ORM\ManyToOne(targetEntity="InfoReport", inversedBy="id")
     * @ORM\JoinColumn(name="report_id", referencedColumnName="id")
     */
    private $report;

    /**
     * Many tabs have one slice
     * @ORM\ManyToOne(targetEntity="InfoSlice", inversedBy="id")
     * @ORM\JoinColumn(name="slice_id", referencedColumnName="id")
     */
    private $slice;

    /**
     * @var InfoUser
     *
     * @ORM\ManyToMany(targetEntity="InfoSlice", cascade={"persist"})
     * @ORM\JoinTable(name="info_reporttabslice",
     *     joinColumns={@ORM\JoinColumn(name="reporttab_id", referencedColumnName="id")},
     *     inverseJoinColumns={@ORM\JoinColumn(name="slice_id", referencedColumnName="id")}
     * )
     */
    private $additionalSlices;

    /**
     * @var InfoUser

    /**
     * @ORM\OneToMany(targetEntity="InfoReportTabFilter", mappedBy="reportTab", cascade={"persist", "remove"}, orphanRemoval=true)
     */
    private $filters;

    /**
     * @ORM\OneToMany(targetEntity="InfoReportTabField", mappedBy="reportTab", cascade={"persist", "remove"}, orphanRemoval=true)
     */
    private $fields;

    public function __construct()
    {
        $this->fields = new ArrayCollection();
        $this->filters = new ArrayCollection();
        $this->additionalSlices = new ArrayCollection();
    }


    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name.
     *
     * @param string|null $name
     *
     * @return InfoReportTab
     */
    public function setName($name = null)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name.
     *
     * @return string|null
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set position.
     *
     * @param string|null $position
     *
     * @return InfoReportTab
     */
    public function setPosition($position = null)
    {
        $this->position = $position;

        return $this;
    }

    /**
     * Get position.
     *
     * @return string|null
     */
    public function getPosition()
    {
        return $this->position;
    }

    /**
     * Set currenttime.
     *
     * @param DateTime|null $currenttime
     *
     * @return InfoReportTab
     */
    public function setCurrenttime(DateTime $currenttime = null)
    {
        $this->currenttime = $currenttime;

        return $this;
    }

    /**
     * Get currenttime.
     *
     * @return DateTime|null
     */
    public function getCurrenttime()
    {
        return $this->currenttime;
    }

    /**
     * Set moduser.
     *
     * @param string|null $moduser
     *
     * @return InfoReportTab
     */
    public function setModuser($moduser = null)
    {
        $this->moduser = $moduser;

        return $this;
    }

    /**
     * Get moduser.
     *
     * @return string|null
     */
    public function getModuser()
    {
        return $this->moduser;
    }

    /**
     * Set guid.
     *
     * @param string|null $guid
     *
     * @return InfoReportTab
     */
    public function setGuid($guid = null)
    {
        $this->guid = $guid;

        return $this;
    }

    /**
     * Get guid.
     *
     * @return string|null
     */
    public function getGuid()
    {
        return $this->guid;
    }

    /**
     * Set report
     *
     * @param InfoReport $report
     *
     * @return InfoReportTab
     */
    public function setReport($report = null)
    {
        $this->report = $report;

        return $this;
    }

    /**
     * Get report
     *
     * @return InfoReport
     */
    public function getReport()
    {
        return $this->report;
    }

    /**
     * Set slice
     *
     * @param InfoSlice $slice
     *
     * @return InfoReportTab
     */
    public function setSlice($slice = null)
    {
        $this->slice = $slice;

        return $this;
    }

    /**
     * Get slice
     *
     * @return InfoSlice
     */
    public function getSlice()
    {
        return $this->slice;
    }

    /**
     * Get slice id
     *
     * @return InfoSlice
     */
    public function getSliceId()
    {
        return $this->slice ? $this->slice->getId() : null;
    }

    /**
     * Add additionalSlice
     *
     * @param InfoSlice $additionalSlice
     *
     * @return InfoReportTab
     */
    public function addAdditionalSlice(InfoSlice $additionalSlice)
    {
        $this->additionalSlices[] = $additionalSlice;

        return $this;
    }

    /**
     * Remove additionalSlice
     *
     * @param InfoSlice $additionalSlice
     */
    public function removeAdditionalSlice(InfoSlice $additionalSlice)
    {
        $this->additionalSlices->removeElement($additionalSlice);
    }

    /**

     * Get additionalSlices
     *
     * @return Collection
     */
    public function getAdditionalSlices()
    {
        return $this->additionalSlices;
    }

    /**
     * Get additionalSlices
     *
     * @return number[]
     */
    public function getAdditionalSlicesIds()
    {
        $ids = [];

        if ($this->additionalSlices->count()) {
            $ids = array_map(function (InfoSlice $slice) {
                return $slice->getId();
            }, $this->additionalSlices->toArray());
        }

        return $ids;
    }

    /**
     * Add field
     *
     * @param InfoReportTabField $field
     * @return InfoReportTab
     */
    public function addField(InfoReportTabField $field)
    {
        $field->setReportTab($this);
        $this->fields->add($field);
        return $this;
    }

    /**
     * Remove field
     *
     * @param InfoReportTabField $field
     */
    public function removeField(InfoReportTabField $field)
    {
        $this->fields->removeElement($field);
    }

    /**
     * Get field
     *
     * @return Collection
     */
    public function getFields()
    {
        return $this->fields;
    }

    /**
     * Add filter
     *
     * @param InfoReportTabFilter $filter
     * @return InfoReportTab
     */
    public function addFilter(InfoReportTabFilter $filter)
    {
        if (!$this->filters->contains($filter)) {
            $filter->setReportTab($this);
            $this->filters[] = $filter;
        }
        return $this;
    }

    /**
     * Remove filter
     *
     * @param InfoReportTabFilter $filter
     */
    public function removeFilter(InfoReportTabFilter $filter)
    {
        if ($this->filters->contains($filter)) {
            $this->filters->removeElement($filter);
        }
    }

    /**
     * Get filter
     *
     * @return Collection
     */
    public function getFilters()
    {
        return $this->filters;
    }
}
