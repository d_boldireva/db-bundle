<?php

namespace TeamSoft\CrmRepositoryBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Context\ExecutionContextInterface;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * InfoPlan
 *
 * @ORM\Table(name="info_plan")
 * @ORM\Entity(repositoryClass="TeamSoft\CrmRepositoryBundle\Repository\InfoPlan")
 */
class InfoPlan implements ServiceFieldInterface
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="DateFrom", type="datetime", nullable=true)
     */
    private $datefrom;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="DateTill", type="datetime", nullable=true)
     */
    private $datetill;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="CreateDate", type="datetime", nullable=true)
     */
    private $createdate;

    /**
     * @var string
     *
     * @ORM\Column(name="Description", type="text", length=16, nullable=true)
     */
    private $description;

    /**
     * @var string
     *
     * @ORM\Column(name="Name", type="string", length=255, nullable=false)
     */
    private $name;

    /**
     * @var integer
     *
     * @ORM\Column(name="CountAction", type="integer", nullable=true)
     */
    private $countaction;

    /**
     * @var integer
     *
     * @ORM\Column(name="workday", type="integer", nullable=true)
     */
    private $workday;

    /**
     * @var integer
     *
     * @ORM\Column(name="taskinday", type="integer", nullable=true)
     */
    private $taskinday;

    /**
     * @var integer
     *
     * @ORM\Column(name="review", type="integer", nullable=true)
     */
    private $review;

    /**
     * @var guid
     *
     * @ORM\Column(name="guid", type="guid", nullable=true)
     */
    private $guid;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="currenttime", type="datetime", nullable=true)
     */
    private $currenttime;

    /**
     * @var string
     *
     * @ORM\Column(name="moduser", type="string", length=16, nullable=true)
     */
    private $moduser;

    /**
     * @var \InfoUser
     *
     * @ORM\ManyToOne(targetEntity="InfoUser")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="Creator_id", referencedColumnName="id")
     * })
     */
    private $creator;

    /**
     * @var \InfoUser
     *
     * @ORM\ManyToOne(targetEntity="InfoUser", inversedBy="plans")
     * @ORM\JoinColumn(name="owner_id", referencedColumnName="id")
     */
    private $owner;

    /**
     * @var \Doctrine\Common\Collections\ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="InfoPlandetail", mappedBy="plan")
     *
     */
    private $plandetails;


    /**
     * @var string
     */
    private $directionName;

    /**
     * @var string
     */
    private $subDirectionName;


    /**
     * Constructor
     */
    public function __construct()
    {
        $this->plandetails = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set datefrom
     *
     * @param \DateTime $datefrom
     *
     * @return InfoPlan
     */
    public function setDatefrom($datefrom)
    {
        $this->datefrom = $datefrom;

        return $this;
    }

    /**
     * Get datefrom
     *
     * @return \DateTime
     */
    public function getDatefrom()
    {
        return $this->datefrom;
    }

    /**
     * Set datetill
     *
     * @param \DateTime $datetill
     *
     * @return InfoPlan
     */
    public function setDatetill($datetill)
    {
        $this->datetill = $datetill;

        return $this;
    }

    /**
     * Get datetill
     *
     * @return \DateTime
     */
    public function getDatetill()
    {
        return $this->datetill;
    }

    /**
     * Set createdate
     *
     * @param \DateTime $createdate
     *
     * @return InfoPlan
     */
    public function setCreatedate(\DateTime $createdate)
    {
        $this->createdate = $createdate;

        return $this;
    }

    /**
     * Get createdate
     *
     * @return \DateTime
     */
    public function getCreatedate()
    {
        return $this->createdate;
    }

    /**
     * Set description
     *
     * @param string $description
     *
     * @return InfoPlan
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return InfoPlan
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set countaction
     *
     * @param int $countaction
     *
     * @return InfoPlan
     */
    public function setCountaction($countaction)
    {
        $this->countaction = $countaction;

        return $this;
    }

    /**
     * Get countaction
     *
     * @return int
     */
    public function getCountaction()
    {
        return $this->countaction;
    }

    /**
     * Set workday
     *
     * @param int $workday
     *
     * @return InfoPlan
     */
    public function setWorkday($workday)
    {
        $this->workday = $workday;

        return $this;
    }

    /**
     * Get workday
     *
     * @return int
     */
    public function getWorkday()
    {
        return $this->workday;
    }

    /**
     * Set taskinday
     *
     * @param int $taskinday
     *
     * @return InfoPlan
     */
    public function setTaskinday($taskinday)
    {
        $this->taskinday = $taskinday;

        return $this;
    }

    /**
     * Get taskinday
     *
     * @return int
     */
    public function getTaskinday()
    {
        return $this->taskinday;
    }

    /**
     * Set review
     *
     * @param int $review
     *
     * @return InfoPlan
     */
    public function setReview($review)
    {
        $this->review = $review;

        return $this;
    }

    /**
     * Get review
     *
     * @return int
     */
    public function getReview()
    {
        return $this->review;
    }

    /**
     * Set guid
     *
     * @param guid $guid
     *
     * @return InfoPlan
     */
    public function setGuid($guid)
    {
        $this->guid = $guid;

        return $this;
    }

    /**
     * Get guid
     *
     * @return guid
     */
    public function getGuid()
    {
        return $this->guid;
    }

    /**
     * Set currenttime
     *
     * @param \DateTime $currenttime
     *
     * @return InfoPlan
     */
    public function setCurrenttime(\DateTime $currenttime)
    {
        $this->currenttime = $currenttime;

        return $this;
    }

    /**
     * Get currenttime
     *
     * @return \DateTime
     */
    public function getCurrenttime()
    {
        return $this->currenttime;
    }

    /**
     * Set moduser
     *
     * @param string $moduser
     *
     * @return InfoPlan
     */
    public function setModuser($moduser)
    {
        $this->moduser = $moduser;

        return $this;
    }

    /**
     * Get moduser
     *
     * @return string
     */
    public function getModuser()
    {
        return $this->moduser;
    }

    /**
     * Set creator
     *
     * @param \TeamSoft\CrmRepositoryBundle\Entity\InfoUser $creator
     *
     * @return InfoPlan
     */
    public function setCreator(\TeamSoft\CrmRepositoryBundle\Entity\InfoUser $creator = null)
    {
        $this->creator = $creator;

        return $this;
    }

    /**
     * Get creator
     *
     * @return \TeamSoft\CrmRepositoryBundle\Entity\InfoUser
     */
    public function getCreator()
    {
        return $this->creator;
    }

    /**
     * Set owner
     *
     * @param \TeamSoft\CrmRepositoryBundle\Entity\InfoUser $owner
     *
     * @return InfoPlan
     */
    public function setOwner(\TeamSoft\CrmRepositoryBundle\Entity\InfoUser $owner = null)
    {
        $this->owner = $owner;

        return $this;
    }

    /**
     * Get owner
     *
     * @return \TeamSoft\CrmRepositoryBundle\Entity\InfoUser
     */
    public function getOwner()
    {
        return $this->owner;
    }

    /**
     * Add plandetail
     *
     * @param \TeamSoft\CrmRepositoryBundle\Entity\InfoPlandetail $plandetail
     *
     * @return InfoPlan
     */
    public function addPlandetail(\TeamSoft\CrmRepositoryBundle\Entity\InfoPlandetail $plandetail)
    {
        $this->plandetails[] = $plandetail;

        return $this;
    }

    /**
     * Remove plandetail
     *
     * @param \TeamSoft\CrmRepositoryBundle\Entity\InfoPlandetail $plandetail
     */
    public function removePlandetail(\TeamSoft\CrmRepositoryBundle\Entity\InfoPlandetail $plandetail)
    {
        $this->plandetails->removeElement($plandetail);
    }

    /**
     * Get plandetails
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getPlandetails()
    {
        return $this->plandetails;
    }

    /**
     * @return int|null
     */
    public function getPlanDetailTaskCountSum()
    {
        $sum = 0;
        if ($this->plandetails) {
            foreach ($this->plandetails as $planDetail) {
                /**
                 * @var $planDetail InfoPlandetail
                 */
                $sum += $planDetail->getTaskcount();
            }
        }

        return $sum;
    }

    /**
     * @return int
     */
    public function getPlanDetailTaskPromoSum()
    {
        $sum = 0;
        if ($this->plandetails) {
            foreach ($this->plandetails as $planDetail) {
                /**
                 * @var $planDetail InfoPlandetail
                 */
                $sum += $planDetail->getTaskPromo();
            }
        }

        return $sum;
    }


    /**
     * @param string $directionName
     */
    public function setDirectionName($directionName)
    {
        $this->directionName = $directionName;
    }

    /**
     * @param string $subDirectionName
     */
    public function setSubDirectionName($subDirectionName)
    {
        $this->subDirectionName = $subDirectionName;
    }

    /**
     * @return int|null
     */
    public function getOwnerId()
    {
        return $this->getOwner() ? $this->getOwner()->getId() : null;
    }

    /**
     * @return int|null
     */
    public function getCreatorId()
    {
        return $this->getCreator() ? $this->getCreator()->getId() : null;
    }

    /**
     * @param ExecutionContextInterface $context
     * @Assert\Callback
     */
    public function validateDateTill(ExecutionContextInterface $context)
    {
        if ($this->datefrom instanceof \DateTime && $this->datetill instanceof \DateTime && $this->datefrom >= $this->datetill) {
            $context->buildViolation('This value should be greater than {{ compared_value }}.')
                ->atPath('datetill')
                ->setParameter('{{ compared_value }}', $this->datefrom->format('Y-m-d H:i'))
                ->addViolation();
        }
    }

    public function regenerateName(): self
    {
        $owner = $this->getOwner();
        $creator = $this->getCreator();
        $dateFrom = $this->getDatefrom();
        $dateTime = $this->getDatetill();
        if ($owner->getId() == $creator->getId()) {
            $name = $creator->getName();
        } else {
            $name = $owner->getName() . "/" . $creator->getName();
        }
        $name .= " " . $dateFrom->format("d.m.Y") . " - " . $dateTime->format("d.m.Y");

        $this->setName($name);

        return $this;
    }
}
