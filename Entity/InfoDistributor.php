<?php

namespace TeamSoft\CrmRepositoryBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * InfoDistributor
 *
 * @ORM\Table(name="info_distributor")
 * @ORM\Entity(repositoryClass="TeamSoft\CrmRepositoryBundle\Repository\InfoDistributorRepository")
 */
class InfoDistributor
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255, nullable=true)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="address", type="string", length=100, nullable=true)
     */
    private $address;

    /**
     * @var string
     *
     * @ORM\Column(name="phone", type="string", length=20, nullable=true)
     */
    private $phone;

    /**
     * @var string
     *
     * @ORM\Column(name="code", type="string", length=20, nullable=true)
     */
    private $code;

    /**
     * @var string
     *
     * @ORM\Column(name="guid", type="guid", nullable=true)
     */
    private $guid;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="currenttime", type="datetime", nullable=true)
     */
    private $currenttime;

    /**
     * @var string
     *
     * @ORM\Column(name="moduser", type="string", length=16, nullable=true)
     */
    private $moduser;

    /**
     * @var string
     *
     * @ORM\Column(name="guid2", type="guid", nullable=true)
     */
    private $guid2;

    /**
     * @var string
     *
     * @ORM\Column(name="contract", type="string", length=50, nullable=true)
     */
    private $contract;

    /**
     * @var InfoDictionary
     *
     * @ORM\ManyToOne(targetEntity="InfoDictionary")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="p_id", referencedColumnName="id")
     * })
     */
    private $p;

    /**
     * @var InfoRegion
     *
     * @ORM\ManyToOne(targetEntity="InfoRegion")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="region_id", referencedColumnName="id")
     * })
     */
    private $region;

    /**
     * @var InfoDistributor
     *
     * @ORM\ManyToOne(targetEntity="InfoDistributor")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="distributor_id", referencedColumnName="id")
     * })
     */
    private $distributor;

    /**
     * @var InfoCity
     *
     * @ORM\ManyToOne(targetEntity="InfoCity")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="city_id", referencedColumnName="id")
     * })
     */
    private $city;



    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return InfoDistributor
     */
    public function setName($name)
    {
        $this->name = $name;
    
        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set address
     *
     * @param string $address
     *
     * @return InfoDistributor
     */
    public function setAddress($address)
    {
        $this->address = $address;
    
        return $this;
    }

    /**
     * Get address
     *
     * @return string
     */
    public function getAddress()
    {
        return $this->address;
    }

    /**
     * Set phone
     *
     * @param string $phone
     *
     * @return InfoDistributor
     */
    public function setPhone($phone)
    {
        $this->phone = $phone;
    
        return $this;
    }

    /**
     * Get phone
     *
     * @return string
     */
    public function getPhone()
    {
        return $this->phone;
    }

    /**
     * Set code
     *
     * @param string $code
     *
     * @return InfoDistributor
     */
    public function setCode($code)
    {
        $this->code = $code;
    
        return $this;
    }

    /**
     * Get code
     *
     * @return string
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * Set guid
     *
     * @param string $guid
     *
     * @return InfoDistributor
     */
    public function setGuid($guid)
    {
        $this->guid = $guid;
    
        return $this;
    }

    /**
     * Get guid
     *
     * @return string
     */
    public function getGuid()
    {
        return $this->guid;
    }

    /**
     * Set currenttime
     *
     * @param \DateTime $currenttime
     *
     * @return InfoDistributor
     */
    public function setCurrenttime($currenttime)
    {
        $this->currenttime = $currenttime;
    
        return $this;
    }

    /**
     * Get currenttime
     *
     * @return \DateTime
     */
    public function getCurrenttime()
    {
        return $this->currenttime;
    }

    /**
     * Set moduser
     *
     * @param string $moduser
     *
     * @return InfoDistributor
     */
    public function setModuser($moduser)
    {
        $this->moduser = $moduser;
    
        return $this;
    }

    /**
     * Get moduser
     *
     * @return string
     */
    public function getModuser()
    {
        return $this->moduser;
    }

    /**
     * Set guid2
     *
     * @param string $guid2
     *
     * @return InfoDistributor
     */
    public function setGuid2($guid2)
    {
        $this->guid2 = $guid2;
    
        return $this;
    }

    /**
     * Get guid2
     *
     * @return string
     */
    public function getGuid2()
    {
        return $this->guid2;
    }

    /**
     * Set contract
     *
     * @param string $contract
     *
     * @return InfoDistributor
     */
    public function setContract($contract)
    {
        $this->contract = $contract;
    
        return $this;
    }

    /**
     * Get contract
     *
     * @return string
     */
    public function getContract()
    {
        return $this->contract;
    }

    /**
     * Set p
     *
     * @param \TeamSoft\CrmRepositoryBundle\Entity\InfoDictionary $p
     *
     * @return InfoDistributor
     */
    public function setP(\TeamSoft\CrmRepositoryBundle\Entity\InfoDictionary $p = null)
    {
        $this->p = $p;
    
        return $this;
    }

    /**
     * Get p
     *
     * @return \TeamSoft\CrmRepositoryBundle\Entity\InfoDictionary
     */
    public function getP()
    {
        return $this->p;
    }

    /**
     * Set region
     *
     * @param \TeamSoft\CrmRepositoryBundle\Entity\InfoRegion $region
     *
     * @return InfoDistributor
     */
    public function setRegion(\TeamSoft\CrmRepositoryBundle\Entity\InfoRegion $region = null)
    {
        $this->region = $region;
    
        return $this;
    }

    /**
     * Get region
     *
     * @return \TeamSoft\CrmRepositoryBundle\Entity\InfoRegion
     */
    public function getRegion()
    {
        return $this->region;
    }

    /**
     * Set distributor
     *
     * @param \TeamSoft\CrmRepositoryBundle\Entity\InfoDistributor $distributor
     *
     * @return InfoDistributor
     */
    public function setDistributor(\TeamSoft\CrmRepositoryBundle\Entity\InfoDistributor $distributor = null)
    {
        $this->distributor = $distributor;
    
        return $this;
    }

    /**
     * Get distributor
     *
     * @return \TeamSoft\CrmRepositoryBundle\Entity\InfoDistributor
     */
    public function getDistributor()
    {
        return $this->distributor;
    }

    /**
     * Set city
     *
     * @param \TeamSoft\CrmRepositoryBundle\Entity\InfoCity $city
     *
     * @return InfoDistributor
     */
    public function setCity(\TeamSoft\CrmRepositoryBundle\Entity\InfoCity $city = null)
    {
        $this->city = $city;
    
        return $this;
    }

    /**
     * Get city
     *
     * @return \TeamSoft\CrmRepositoryBundle\Entity\InfoCity
     */
    public function getCity()
    {
        return $this->city;
    }
}
