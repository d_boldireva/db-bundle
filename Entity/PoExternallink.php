<?php

namespace TeamSoft\CrmRepositoryBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * PoExternallink
 *
 * @ORM\Table(name="po_externallink")
 * @ORM\Entity
 */
class PoExternallink
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string|null
     *
     * @ORM\Column(name="name_ru", type="string", length=255, nullable=true)
     */
    private $nameRu;

    /**
     * @var string|null
     *
     * @ORM\Column(name="name_en", type="string", length=255, nullable=true)
     */
    private $nameEn;

    /**
     * @var string|null
     *
     * @ORM\Column(name="url", type="string", length=255, nullable=true)
     */
    private $url;

    /**
     * @var boolean|null
     *
     * @ORM\Column(name="enable", type="boolean", nullable=true)
     */
    private $enable;

    /**
     * @var boolean|null
     *
     * @ORM\Column(name="target_blank", type="boolean", nullable=true)
     */
    private $targetBlank;

    /**
     * @var string|null
     *
     * @ORM\Column(name="description_ru", type="string", length=255, nullable=true)
     */
    private $descriptionRu;

    /**
     * @var string|null
     *
     * @ORM\Column(name="description_en", type="string", length=255, nullable=true)
     */
    private $descriptionEn;

    /**
     * @var string|null
     *
     * @ORM\Column(name="image", type="blob", nullable=true)
     */
    private $image;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="currenttime", type="datetime", nullable=true)
     */
    private $currenttime;

    /**
     * @var string|null
     *
     * @ORM\Column(name="moduser", type="string", length=16, nullable=true)
     */
    private $moduser;

    /**
     * @var string|null
     *
     * @ORM\Column(name="guid", type="guid", nullable=true)
     */
    private $guid;


    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Get name
     *
     * @return string|null
     */
    public function getName($locale = null)
    {
        if (strtolower($locale) === 'en') {
            return $this->nameEn;
        } else if (strtolower($locale) === 'ru') {
            return $this->nameRu;
        } else {
            return $this->nameEn;
        }
    }

    /**
     * Get description
     *
     * @return string|null
     */
    public function getDescription($locale = null)
    {
        if (strtolower($locale) === 'en') {
            return $this->descriptionEn;
        } else if (strtolower($locale) === 'ru') {
            return $this->descriptionRu;
        } else {
            return $this->descriptionEn;
        }
    }

    /**
     * Set nameRu.
     *
     * @param string|null $nameRu
     *
     * @return PoExternallink
     */
    public function setNameRu($nameRu = null)
    {
        $this->nameRu = $nameRu;

        return $this;
    }

    /**
     * Get nameRu.
     *
     * @return string|null
     */
    public function getNameRu()
    {
        return $this->nameRu;
    }

    /**
     * Set nameEn.
     *
     * @param string|null $nameEn
     *
     * @return PoExternallink
     */
    public function setNameEn($nameEn = null)
    {
        $this->nameEn = $nameEn;

        return $this;
    }

    /**
     * Get nameEn.
     *
     * @return string|null
     */
    public function getNameEn()
    {
        return $this->nameEn;
    }

    /**
     * Set url.
     *
     * @param string|null $url
     *
     * @return PoExternallink
     */
    public function setUrl($url = null)
    {
        $this->url = $url;

        return $this;
    }

    /**
     * Get url.
     *
     * @return string|null
     */
    public function getUrl()
    {
        return $this->url;
    }

    /**
     * Set enable.
     *
     * @param int|null $enable
     *
     * @return PoExternallink
     */
    public function setEnable($enable = null)
    {
        $this->enable = $enable;

        return $this;
    }

    /**
     * Get enable.
     *
     * @return int|null
     */
    public function getEnable()
    {
        return $this->enable;
    }

    /**
     * Set targetBlank.
     *
     * @param int|null $targetBlank
     *
     * @return PoExternallink
     */
    public function setTargetBlank($targetBlank = null)
    {
        $this->targetBlank = $targetBlank;

        return $this;
    }

    /**
     * Get targetBlank.
     *
     * @return int|null
     */
    public function getTargetBlank()
    {
        return $this->targetBlank;
    }

    /**
     * Set descriptionRu.
     *
     * @param string|null $descriptionRu
     *
     * @return PoExternallink
     */
    public function setDescriptionRu($descriptionRu = null)
    {
        $this->descriptionRu = $descriptionRu;

        return $this;
    }

    /**
     * Get descriptionRu.
     *
     * @return string|null
     */
    public function getDescriptionRu()
    {
        return $this->descriptionRu;
    }

    /**
     * Set descriptionEn.
     *
     * @param string|null $descriptionEn
     *
     * @return PoExternallink
     */
    public function setDescriptionEn($descriptionEn = null)
    {
        $this->descriptionEn = $descriptionEn;

        return $this;
    }

    /**
     * Get descriptionEn.
     *
     * @return string|null
     */
    public function getDescriptionEn()
    {
        return $this->descriptionEn;
    }

    /**
     * Set image.
     *
     * @param string|null $image
     *
     * @return PoExternallink
     */
    public function setImage($image = null)
    {
        $this->image = $image;

        return $this;
    }

    /**
     * Get image.
     *
     * @return string|null
     */
    public function getImage()
    {
        return $this->image;
    }

    /**
     * Set currenttime.
     *
     * @param \DateTime|null $currenttime
     *
     * @return PoExternallink
     */
    public function setCurrenttime($currenttime = null)
    {
        $this->currenttime = $currenttime;

        return $this;
    }

    /**
     * Get currenttime.
     *
     * @return \DateTime|null
     */
    public function getCurrenttime()
    {
        return $this->currenttime;
    }

    /**
     * Set moduser.
     *
     * @param string|null $moduser
     *
     * @return PoExternallink
     */
    public function setModuser($moduser = null)
    {
        $this->moduser = $moduser;

        return $this;
    }

    /**
     * Get moduser.
     *
     * @return string|null
     */
    public function getModuser()
    {
        return $this->moduser;
    }

    /**
     * Set guid.
     *
     * @param string|null $guid
     *
     * @return PoExternallink
     */
    public function setGuid($guid = null)
    {
        $this->guid = $guid;

        return $this;
    }

    /**
     * Get guid.
     *
     * @return string|null
     */
    public function getGuid()
    {
        return $this->guid;
    }
}
