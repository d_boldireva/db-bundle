<?php

namespace TeamSoft\CrmRepositoryBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * InfoTarget
 *
 * @ORM\Table(name="info_target", indexes={@ORM\Index(name="ix_info_target_guid", columns={"guid"}), @ORM\Index(name="ix_info_target_time", columns={"time"})})
 * @ORM\Entity(repositoryClass="TeamSoft\CrmRepositoryBundle\Repository\InfoTarget")
 */
class InfoTarget
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255, nullable=true)
     */
    private $name;

    /**
     * @var integer
     *
     * @ORM\Column(name="isnocontact", type="integer", nullable=true)
     */
    private $isnocontact;

    /**
     * @var guid
     *
     * @ORM\Column(name="guid", type="guid", nullable=true)
     */
    private $guid;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="currenttime", type="datetime", nullable=true)
     */
    private $currenttime;

    /**
     * @var string
     *
     * @ORM\Column(name="moduser", type="string", length=16, nullable=true)
     */
    private $moduser;

    /**
     * @var InfoDirection
     *
     * @ORM\ManyToOne(targetEntity="InfoDirection")
     * @ORM\JoinColumn(name="direction_id", referencedColumnName="id")
     */
    private $direction;



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return InfoTarget
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set isnocontact
     *
     * @param integer $isnocontact
     * @return InfoTarget
     */
    public function setIsnocontact($isnocontact)
    {
        $this->isnocontact = $isnocontact;

        return $this;
    }

    /**
     * Get isnocontact
     *
     * @return integer 
     */
    public function getIsnocontact()
    {
        return $this->isnocontact;
    }

    /**
     * Set guid
     *
     * @param guid $guid
     * @return InfoTarget
     */
    public function setGuid($guid)
    {
        $this->guid = $guid;

        return $this;
    }

    /**
     * Get guid
     *
     * @return guid 
     */
    public function getGuid()
    {
        return $this->guid;
    }

    /**
     * Set currenttime
     *
     * @param \DateTime $currenttime
     * @return InfoTarget
     */
    public function setCurrenttime($currenttime)
    {
        $this->currenttime = $currenttime;

        return $this;
    }

    /**
     * Get currenttime
     *
     * @return \DateTime 
     */
    public function getCurrenttime()
    {
        return $this->currenttime;
    }

    /**
     * Set moduser
     *
     * @param string $moduser
     * @return InfoTarget
     */
    public function setModuser($moduser)
    {
        $this->moduser = $moduser;

        return $this;
    }

    /**
     * Get moduser
     *
     * @return string 
     */
    public function getModuser()
    {
        return $this->moduser;
    }

    public function getDoctorsCount(){
        return 0;
    }

    public function getVisitsCount(){
        return 0;
    }

    /**
     * @param InfoDirection $direction
     * @return InfoDirection
     */
    public function setDirection(InfoDirection $direction){
        $this->direction = $direction;
        return $direction;
    }

    /**
     * @return InfoDirection
     */
    public function getDirection() {
        return $this->direction;
    }
}
