<?php

namespace TeamSoft\CrmRepositoryBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * InfoTestingquestion
 *
 * @ORM\Table(name="info_testingquestion")
 * @ORM\Entity
 */
class InfoTestingquestion implements ServiceFieldInterface
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var int
     *
     * @ORM\Column(name="groupname", type="integer", nullable=true)
     */
    private $groupname;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255, nullable=true)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="string", length=255, nullable=true)
     */
    private $description;

    /**
     * @var string
     *
     * @ORM\Column(name="type", type="string", length=50, nullable=true)
     */
    private $type;

    /**
     * @var string
     *
     * @ORM\Column(name="picture", type="string", length=50, nullable=true)
     */
    private $picture;

    /**
     * @var int
     *
     * @ORM\Column(name="timelimit", type="integer", nullable=true)
     */
    private $timelimit;

    /**
     * @var guid
     *
     * @ORM\Column(name="guid", type="guid", nullable=true)
     */
    private $guid;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="currenttime", type="datetime", nullable=true)
     */
    private $currenttime;

    /**
     * @var string
     *
     * @ORM\Column(name="moduser", type="string", length=16, nullable=true)
     */
    private $moduser;

    /**
     * @var InfoTesting
     *
     * @ORM\ManyToOne(targetEntity="InfoTesting")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="testing_id", referencedColumnName="id")
     * })
     */
    private $testing;

    /**
     * @var InfoTestinganswer
     *
     * @ORM\OneToMany(targetEntity="InfoTestinganswer", mappedBy="question")
     * @ORM\OrderBy({"rowNum" = "ASC"})
     */
    private $answers;



    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set groupname
     *
     * @param int $groupname
     *
     * @return InfoTestingquestion
     */
    public function setGroupname($groupname)
    {
        $this->groupname = $groupname;
    
        return $this;
    }

    /**
     * Get groupname
     *
     * @return int
     */
    public function getGroupname()
    {
        return $this->groupname;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return InfoTestingquestion
     */
    public function setName($name)
    {
        $this->name = $name;
    
        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set description
     *
     * @param string $description
     *
     * @return InfoTestingquestion
     */
    public function setDescription($description)
    {
        $this->description = $description;
    
        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set type
     *
     * @param string $type
     *
     * @return InfoTestingquestion
     */
    public function setType($type)
    {
        $this->type = $type;
    
        return $this;
    }

    /**
     * Get type
     *
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set picture
     *
     * @param string $picture
     *
     * @return InfoTestingquestion
     */
    public function setPicture($picture)
    {
        $this->picture = $picture;
    
        return $this;
    }

    /**
     * Get picture
     *
     * @return string
     */
    public function getPicture()
    {
        return $this->picture;
    }

    /**
     * Set timelimit
     *
     * @param int $timelimit
     *
     * @return InfoTestingquestion
     */
    public function setTimelimit($timelimit)
    {
        $this->timelimit = $timelimit;
    
        return $this;
    }

    /**
     * Get timelimit
     *
     * @return int
     */
    public function getTimelimit()
    {
        return $this->timelimit;
    }

    /**
     * Set guid
     *
     * @param guid $guid
     *
     * @return InfoTestingquestion
     */
    public function setGuid($guid)
    {
        $this->guid = $guid;
    
        return $this;
    }

    /**
     * Get guid
     *
     * @return guid
     */
    public function getGuid()
    {
        return $this->guid;
    }

    /**
     * Set currenttime
     *
     * @param \DateTime $currenttime
     *
     * @return InfoTestingquestion
     */
    public function setCurrenttime(\DateTime $currenttime)
    {
        $this->currenttime = $currenttime;
    
        return $this;
    }

    /**
     * Get currenttime
     *
     * @return \DateTime
     */
    public function getCurrenttime()
    {
        return $this->currenttime;
    }

    /**
     * Set moduser
     *
     * @param string $moduser
     *
     * @return InfoTestingquestion
     */
    public function setModuser($moduser)
    {
        $this->moduser = $moduser;
    
        return $this;
    }

    /**
     * Get moduser
     *
     * @return string
     */
    public function getModuser()
    {
        return $this->moduser;
    }

    /**
     * Set testing
     *
     * @param \TeamSoft\CrmRepositoryBundle\Entity\InfoTesting $testing
     *
     * @return InfoTestingquestion
     */
    public function setTesting(\TeamSoft\CrmRepositoryBundle\Entity\InfoTesting $testing = null)
    {
        $this->testing = $testing;
    
        return $this;
    }

    /**
     * Get testing
     *
     * @return \TeamSoft\CrmRepositoryBundle\Entity\InfoTesting
     */
    public function getTesting()
    {
        return $this->testing;
    }

    /**
     * Set answers
     *
     * @param \TeamSoft\CrmRepositoryBundle\Entity\InfoTestinganswer $answers
     *
     * @return InfoTestinganswer
     */
    public function setAnswer(\TeamSoft\CrmRepositoryBundle\Entity\InfoTestinganswer $answers = null)
    {
        $this->answers = $answers;

        return $this;
    }

    /**
     * Get answer
     *
     * @return \TeamSoft\CrmRepositoryBundle\Entity\InfoTestinganswer
     */
    public function getAnswer()
    {
        return $this->answers;
    }
}
