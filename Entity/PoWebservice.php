<?php

namespace TeamSoft\CrmRepositoryBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * PoWebservice
 *
 * @ORM\Table(name="po_webservice")
 * @ORM\Entity
 */
class PoWebservice implements ServiceFieldInterface
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var int|null
     *
     * @ORM\Column(name="`order`", type="integer", nullable=true)
     */
    private $order;

    /**
     * @var string|null
     *
     * @ORM\Column(name="allow_if", type="string", length=255, nullable=true)
     */
    private $allowIf;

    /**
     * @var string|null
     *
     * @ORM\Column(name="route", type="string", length=50, nullable=true)
     */
    private $route;

    /**
     * @var string|null
     *
     * @ORM\Column(name="url", type="string", length=50, nullable=true)
     */
    private $url;

    /**
     * @var int|null
     *
     * @ORM\Column(name="target_blank", type="integer", nullable=true)
     */
    private $targetBlank;

    /**
     * @var string|null
     *
     * @ORM\Column(name="guid", type="guid", nullable=true)
     */
    private $guid;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="currenttime", type="datetime", nullable=true)
     */
    private $currenttime;

    /**
     * @var string|null
     *
     * @ORM\Column(name="moduser", type="string", length=16, nullable=true)
     */
    private $moduser;

    /**
     * @var string|null
     *
     * @ORM\Column(name="`group`", type="string", length=20, nullable=true)
     */
    private $group;

    /**
     * @ORM\OneToOne(targetEntity="InfoService", inversedBy="webService")
     * @ORM\JoinColumn(name="service_id", referencedColumnName="id")
     */
    private $service;

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set order.
     *
     * @param int|null $order
     *
     * @return PoWebservice
     */
    public function setOrder($order = null)
    {
        $this->order = $order;

        return $this;
    }

    /**
     * Get order.
     *
     * @return int|null
     */
    public function getOrder()
    {
        return $this->order;
    }

    /**
     * Set allowIf.
     *
     * @param string|null $allowIf
     *
     * @return PoWebservice
     */
    public function setAllowIf($allowIf = null)
    {
        $this->allowIf = $allowIf;

        return $this;
    }

    /**
     * Get allowIf.
     *
     * @return string|null
     */
    public function getAllowIf()
    {
        return $this->allowIf;
    }

    /**
     * Set route.
     *
     * @param string|null $route
     *
     * @return PoWebservice
     */
    public function setRoute($route = null)
    {
        $this->route = $route;

        return $this;
    }

    /**
     * Get route.
     *
     * @return string|null
     */
    public function getRoute()
    {
        return $this->route;
    }

    /**
     * Set url.
     *
     * @param string|null $url
     *
     * @return PoWebservice
     */
    public function setUrl($url = null)
    {
        $this->url = $url;

        return $this;
    }

    /**
     * Get url.
     *
     * @return string|null
     */
    public function getUrl()
    {
        return $this->url;
    }

    /**
     * Set targetBlank.
     *
     * @param int|null $targetBlank
     *
     * @return PoWebservice
     */
    public function setTargetBlank($targetBlank = null)
    {
        $this->targetBlank = $targetBlank;

        return $this;
    }

    /**
     * Get targetBlank.
     *
     * @return int|null
     */
    public function getTargetBlank()
    {
        return $this->targetBlank;
    }

    /**
     * Set guid.
     *
     * @param string|null $guid
     *
     * @return PoWebservice
     */
    public function setGuid($guid = null)
    {
        $this->guid = $guid;

        return $this;
    }

    /**
     * Get guid.
     *
     * @return string|null
     */
    public function getGuid()
    {
        return $this->guid;
    }

    /**
     * Set currenttime.
     *
     * @param \DateTime|null $currenttime
     *
     * @return PoWebservice
     */
    public function setCurrenttime(\DateTime $currenttime = null)
    {
        $this->currenttime = $currenttime;

        return $this;
    }

    /**
     * Get currenttime.
     *
     * @return \DateTime|null
     */
    public function getCurrenttime()
    {
        return $this->currenttime;
    }

    /**
     * Set moduser.
     *
     * @param string|null $moduser
     *
     * @return PoWebservice
     */
    public function setModuser($moduser = null)
    {
        $this->moduser = $moduser;

        return $this;
    }

    /**
     * Get moduser.
     *
     * @return string|null
     */
    public function getModuser()
    {
        return $this->moduser;
    }

    /**
     * Set group.
     *
     * @param string|null $group
     *
     * @return PoWebservice
     */
    public function setGroup($group = null)
    {
        $this->group = $group;

        return $this;
    }

    /**
     * Get group.
     *
     * @return string|null
     */
    public function getGroup()
    {
        return $this->group;
    }

    /**
     * Set service
     *
     * @param InfoService $service
     *
     * @return PoWebservice
     */
    public function setService(InfoService $service = null)
    {
        $this->service = $service;

        return $this;
    }

    /**
     * Get service
     *
     * @return InfoService
     */
    public function getService()
    {
        return $this->service;
    }
}
