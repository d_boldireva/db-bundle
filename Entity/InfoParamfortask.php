<?php

namespace TeamSoft\CrmRepositoryBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * InfoParamfortask
 *
 * @ORM\Table(name="info_paramfortask")
 * @ORM\Entity
 */
class InfoParamfortask implements ServiceFieldInterface
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string|null
     *
     * @ORM\Column(name="group_name", type="string", length=255, nullable=true)
     */
    private $groupName;

    /**
     * @var string|null
     *
     * @ORM\Column(name="name", type="string", length=255, nullable=true)
     */
    private $name;

    /**
     * @var string|null
     *
     * @ORM\Column(name="guid", type="guid", nullable=true)
     */
    private $guid;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="currenttime", type="datetime", nullable=true)
     */
    private $currenttime;

    /**
     * @var string|null
     *
     * @ORM\Column(name="moduser", type="string", length=16, nullable=true)
     */
    private $moduser;

    /**
     * @var string|null
     *
     * @ORM\Column(name="company", type="string", length=255, nullable=true)
     */
    private $company;

    /**
     * @var int|null
     *
     * @ORM\Column(name="isGoal", type="integer", nullable=true)
     */
    private $isgoal;

    /**
     * @var string|null
     *
     * @ORM\Column(name="block_name", type="string", length=255, nullable=true)
     */
    private $blockName;

    /**
     * @var int|null
     *
     * @ORM\Column(name="position", type="integer", nullable=true)
     */
    private $position;

    /**
     * @var int|null
     *
     * @ORM\Column(name="isshowprev", type="integer", nullable=true)
     */
    private $isshowprev;

    /**
     * @var int|null
     *
     * @ORM\Column(name="ishidden", type="integer", nullable=true)
     */
    private $ishidden;

    /**
     * @var int|null
     *
     * @ORM\Column(name="isrequired", type="integer", nullable=true)
     */
    private $isrequired;

    /**
     * @var int|null
     *
     * @ORM\Column(name="isshowdescr", type="integer", nullable=true)
     */
    private $isshowdescr;

    /**
     * @var int|null
     *
     * @ORM\Column(name="isrequireddescr", type="integer", nullable=true)
     */
    private $isrequireddescr;

    /**
     * @var int|null
     *
     * @ORM\Column(name="mark", type="float", nullable=true)
     */
    private $mark;

    /**
     * @var int|null
     *
     * @ORM\Column(name="isforbrends", type="integer", nullable=true)
     */
    private $isforbrends;

    /**
     * @ORM\ManyToMany(targetEntity="InfoTasktype")
     * @ORM\JoinTable(name="info_paramfortasktype",
     *   joinColumns={@ORM\JoinColumn(name="param_id", referencedColumnName="id")},
     *   inverseJoinColumns={@ORM\JoinColumn(name="tasktype_id", referencedColumnName="id")}
     * )
     */
    private $taskTypes;

    /**
     * @ORM\ManyToMany(targetEntity="InfoDictionary")
     * @ORM\JoinTable(name="info_paramfortaskposition",
     *   joinColumns={@ORM\JoinColumn(name="param_id", referencedColumnName="id")},
     *   inverseJoinColumns={@ORM\JoinColumn(name="position_id", referencedColumnName="id")}
     * )
     */
    private $positions;

    /**
     * @var InfoParamfortaskvalue
     *
     * @ORM\OneToMany(targetEntity="InfoParamfortaskvalue", mappedBy="paramForTask")
     */
    private $values;

    public function __construct()
    {
        $this->taskTypes = new ArrayCollection();
        $this->positions = new ArrayCollection();
        $this->values = new ArrayCollection();
    }


    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set groupName.
     *
     * @param string|null $groupName
     *
     * @return InfoParamfortask
     */
    public function setGroupName($groupName = null)
    {
        $this->groupName = $groupName;

        return $this;
    }

    /**
     * Get groupName.
     *
     * @return string|null
     */
    public function getGroupName()
    {
        return $this->groupName;
    }

    /**
     * Set name.
     *
     * @param string|null $name
     *
     * @return InfoParamfortask
     */
    public function setName($name = null)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name.
     *
     * @return string|null
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set guid.
     *
     * @param string|null $guid
     *
     * @return InfoParamfortask
     */
    public function setGuid($guid = null)
    {
        $this->guid = $guid;

        return $this;
    }

    /**
     * Get guid.
     *
     * @return string|null
     */
    public function getGuid()
    {
        return $this->guid;
    }

    /**
     * Set currenttime.
     *
     * @param \DateTime|null $currenttime
     *
     * @return InfoParamfortask
     */
    public function setCurrenttime(\DateTime $currenttime)
    {
        $this->currenttime = $currenttime;

        return $this;
    }

    /**
     * Get currenttime.
     *
     * @return \DateTime|null
     */
    public function getCurrenttime()
    {
        return $this->currenttime;
    }

    /**
     * Set moduser.
     *
     * @param string|null $moduser
     *
     * @return InfoParamfortask
     */
    public function setModuser($moduser = null)
    {
        $this->moduser = $moduser;

        return $this;
    }

    /**
     * Get moduser.
     *
     * @return string|null
     */
    public function getModuser()
    {
        return $this->moduser;
    }

    /**
     * Set company.
     *
     * @param string|null $company
     *
     * @return InfoParamfortask
     */
    public function setCompany($company = null)
    {
        $this->company = $company;

        return $this;
    }

    /**
     * Get company.
     *
     * @return string|null
     */
    public function getCompany()
    {
        return $this->company;
    }

    /**
     * Set isgoal.
     *
     * @param int|null $isgoal
     *
     * @return InfoParamfortask
     */
    public function setIsgoal($isgoal = null)
    {
        $this->isgoal = $isgoal;

        return $this;
    }

    /**
     * Get isgoal.
     *
     * @return int|null
     */
    public function getIsgoal()
    {
        return $this->isgoal;
    }

    /**
     * Set blockName.
     *
     * @param string|null $blockName
     *
     * @return InfoParamfortask
     */
    public function setBlockName($blockName = null)
    {
        $this->blockName = $blockName;

        return $this;
    }

    /**
     * Get blockName.
     *
     * @return string|null
     */
    public function getBlockName()
    {
        return $this->blockName;
    }

    /**
     * Set position.
     *
     * @param int|null $position
     *
     * @return InfoParamfortask
     */
    public function setPosition($position = null)
    {
        $this->position = $position;

        return $this;
    }

    /**
     * Get position.
     *
     * @return int|null
     */
    public function getPosition()
    {
        return $this->position;
    }

    /**
     * Set ishidden.
     *
     * @param int|null $ishidden
     *
     * @return InfoParamfortask
     */
    public function setIshidden($ishidden = null)
    {
        $this->ishidden = $ishidden;

        return $this;
    }

    /**
     * Get ishidden.
     *
     * @return int|null
     */
    public function getIshidden()
    {
        return $this->ishidden;
    }

    /**
     * Set isshowprev.
     *
     * @param int|null $isshowprev
     *
     * @return InfoParamfortask
     */
    public function setIsshowprev($isshowprev = null)
    {
        $this->isshowprev = $isshowprev;

        return $this;
    }

    /**
     * Get isshowprev.
     *
     * @return int|null
     */
    public function getIsshowprev()
    {
        return $this->isshowprev;
    }

    /**
     * Set isrequired.
     *
     * @param int|null $isrequired
     *
     * @return InfoParamfortask
     */
    public function setIsrequired($isrequired = null)
    {
        $this->isrequired = $isrequired;

        return $this;
    }

    /**
     * Get isrequired.
     *
     * @return int|null
     */
    public function getIsrequired()
    {
        return $this->isrequired;
    }

    /**
     * Set isshowdescr.
     *
     * @param int|null $isshowdescr
     *
     * @return InfoParamfortask
     */
    public function setIsshowdescr($isshowdescr = null)
    {
        $this->isshowdescr = $isshowdescr;

        return $this;
    }

    /**
     * Get isshowdescr.
     *
     * @return int|null
     */
    public function getIsshowdescr()
    {
        return $this->isshowdescr;
    }

    /**
     * Set isrequireddescr.
     *
     * @param int|null $isrequireddescr
     *
     * @return InfoParamfortask
     */
    public function setIsrequireddescr($isrequireddescr = null)
    {
        $this->isrequireddescr = $isrequireddescr;

        return $this;
    }

    /**
     * Get isrequireddescr.
     *
     * @return int|null
     */
    public function getIsrequireddescr()
    {
        return $this->isrequireddescr;
    }

    /**
     * Set mark
     *
     * @param float|null $mark
     *
     * @return InfoParamfortask
     */
    public function setMark($mark = null)
    {
        $this->mark = $mark;

        return $this;
    }

    /**
     * Get mark
     *
     * @return float|null
     */
    public function getMark()
    {
        return $this->mark;
    }

    /**
     * Set isforbrends.
     *
     * @param int|null $isforbrends
     *
     * @return InfoParamfortask
     */
    public function setIsforbrends($isforbrends = null)
    {
        $this->isforbrends = $isforbrends;

        return $this;
    }

    /**
     * Get isforbrends.
     *
     * @return int|null
     */
    public function getIsforbrends()
    {
        return $this->isforbrends;
    }

    /**
     * Add taskType
     *
     * @param InfoTasktype $taskType
     *
     * @return InfoParamfortask
     */
    public function addTaskType(InfoTasktype $taskType)
    {
        if (!$this->taskTypes->contains($taskType)) {
            $this->taskTypes->add($taskType);
        }

        return $this;
    }

    /**
     * Remove taskType
     *
     * @param InfoTasktype $taskType
     */
    public function removeTaskType(InfoTasktype $taskType)
    {
        $this->taskTypes->removeElement($taskType);
    }

    /**
     * Get taskTypes
     *
     * @return ArrayCollection
     */
    public function getTaskTypes()
    {
        return $this->taskTypes;
    }

    /**
     * Add value
     *
     * @param InfoTasktype $value
     *
     * @return InfoParamfortask
     */
    public function addValue(InfoTasktype $value)
    {
        if (!$this->values->contains($value)) {
            $this->values->add($value);
        }

        return $this;
    }

    /**
     * Remove value
     *
     * @param InfoTasktype $value
     */
    public function removeValue(InfoTasktype $value)
    {
        $this->values->removeElement($value);
    }

    /**
     * Get values
     *
     * @return ArrayCollection
     */
    public function getValues()
    {
        return $this->values;
    }

    /**
     * Add position
     *
     * @param InfoTasktype $position
     *
     * @return InfoParamfortask
     */
    public function addPosition(InfoTasktype $position)
    {
        if (!$this->positions->contains($position)) {
            $this->positions->add($position);
        }

        return $this;
    }

    /**
     * Remove position
     *
     * @param InfoTasktype $position
     */
    public function removePosition(InfoTasktype $position)
    {
        $this->positions->removeElement($position);
    }

    /**
     * Get positions
     *
     * @return ArrayCollection
     */
    public function getPositions()
    {
        return $this->positions;
    }
}
