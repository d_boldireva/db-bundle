<?php

namespace TeamSoft\CrmRepositoryBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * InfoCompanyrequest
 *
 * @ORM\Table(name="info_companyrequest")
 * @ORM\Entity
 */
class InfoCompanyrequest implements ServiceFieldInterface
{
    const COMPANY_VERIFICATION_WITHOUT_DCR = 1;
    const COMPANY_VERIFICATION_WITH_DCR = 2;

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="modified", type="datetime", nullable=true)
     */
    private $modified;

    /**
     * @var string
     *
     * @ORM\Column(name="field", type="string", length=255, nullable=true)
     */
    private $field;

    /**
     * @var string
     *
     * @ORM\Column(name="oldvalue", type="string", length=255, nullable=true)
     */
    private $oldvalue;

    /**
     * @var string
     *
     * @ORM\Column(name="newvalue", type="string", length=255, nullable=true)
     */
    private $newvalue;

    /**
     * @var string
     *
     * @ORM\Column(name="comment", type="string", length=255, nullable=true)
     */
    private $comment;

    /**
     * @var string
     *
     * @ORM\Column(name="guid", type="guid", nullable=true)
     */
    private $guid;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="currenttime", type="datetime", nullable=true)
     */
    private $currenttime;

    /**
     * @var string
     *
     * @ORM\Column(name="moduser", type="string", length=16, nullable=true)
     */
    private $moduser;

    /**
     * @var int
     *
     * @ORM\Column(name="statusverification", type="integer", nullable=true)
     */
    private $statusverification;

    /**
     * @var int
     *
     * @ORM\Column(name="statusverification2", type="string", nullable=true)
     */
    private $statusverification2;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="sendverification", type="datetime", nullable=true)
     */
    private $sendverification;

    /**
     * @var InfoArchivereason
     *
     * @ORM\ManyToOne(targetEntity="InfoArchivereason")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="archivereason_id_old", referencedColumnName="id")
     * })
     */
    private $archivereasonOld;

    /**
     * @var InfoArchivereason
     *
     * @ORM\ManyToOne(targetEntity="InfoArchivereason")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="archivereason_id_new", referencedColumnName="id")
     * })
     */
    private $archivereasonNew;

    /**
     * @var InfoCity
     *
     * @ORM\ManyToOne(targetEntity="InfoCity")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="city_id_old", referencedColumnName="id")
     * })
     */
    private $cityOld;

    /**
     * @var InfoCity
     *
     * @ORM\ManyToOne(targetEntity="InfoCity")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="city_id_new", referencedColumnName="id")
     * })
     */
    private $cityNew;

    /**
     * @var InfoCompany
     *
     * @ORM\ManyToOne(targetEntity="InfoCompany", cascade={"persist"})
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="company_id", referencedColumnName="id")
     * })
     */
    private $company;

    /**
     * @var InfoCompany
     *
     * @ORM\ManyToOne(targetEntity="InfoCompany", cascade={"persist"})
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="additionalcompany_id_old", referencedColumnName="id")
     * })
     */
    private $additionalcompanyOld;

    /**
     * @var InfoCompany
     *
     * @ORM\ManyToOne(targetEntity="InfoCompany", cascade={"persist"})
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="additionalcompany_id_new", referencedColumnName="id")
     * })
     */
    private $additionalcompanyNew;

    /**
     * @var InfoCompanytype
     *
     * @ORM\ManyToOne(targetEntity="InfoCompanytype")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="companytype_id_old", referencedColumnName="id")
     * })
     */
    private $companytypeOld;

    /**
     * @var InfoCompanytype
     *
     * @ORM\ManyToOne(targetEntity="InfoCompanytype")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="companytype_id_new", referencedColumnName="id")
     * })
     */
    private $companytypeNew;

    /**
     * @var InfoCompanycategory
     *
     * @ORM\ManyToOne(targetEntity="InfoCompanycategory")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="category_id_old", referencedColumnName="id")
     * })
     */
    private $companycategoryOld;

    /**
     * @var InfoCompanycategory
     *
     * @ORM\ManyToOne(targetEntity="InfoCompanycategory")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="category_id_new", referencedColumnName="id")
     * })
     */
    private $companycategoryNew;

    /**
     * @var InfoDictionary
     *
     * @ORM\ManyToOne(targetEntity="InfoDictionary")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="streettype_id_old", referencedColumnName="id")
     * })
     */
    private $streettypeOld;

    /**
     * @var InfoDictionary
     *
     * @ORM\ManyToOne(targetEntity="InfoDictionary")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="streettype_id_new", referencedColumnName="id")
     * })
     */
    private $streettypeNew;

    /**
     * @var InfoUser
     *
     * @ORM\ManyToOne(targetEntity="InfoUser")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="modifier_id", referencedColumnName="id")
     * })
     */
    private $modifier;

    /**
     * @var InfoRegion
     *
     * @ORM\ManyToOne(targetEntity="InfoRegion")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="region_id_old", referencedColumnName="id")
     * })
     */
    private $regionOld;

    /**
     * @var InfoRegion
     *
     * @ORM\ManyToOne(targetEntity="InfoRegion")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="region_id_new", referencedColumnName="id")
     * })
     */
    private $regionNew;

    /**
     * @var InfoCompany
     *
     * @ORM\ManyToOne(targetEntity="InfoCompany")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="main_id_old", referencedColumnName="id")
     * })
     */
    private $mainOld;

    /**
     * @var InfoCompany
     *
     * @ORM\ManyToOne(targetEntity="InfoCompany")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="main_id_new", referencedColumnName="id")
     * })
     */
    private $mainNew;

    /**
     * @var InfoUser
     *
     * @ORM\ManyToOne(targetEntity="InfoUser")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="owner_id_new", referencedColumnName="id")
     * })
     */
    private $ownerNew;

    /**
     * @var InfoUser
     *
     * @ORM\ManyToOne(targetEntity="InfoUser")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="owner_id_old", referencedColumnName="id")
     * })
     */
    private $ownerOld;


    /**
     * @var InfoUser
     *
     * @ORM\ManyToOne(targetEntity="InfoUser")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="additionalowner_id_old", referencedColumnName="id")
     * })
     */
    private $additionalownerOld;

    /**
     * @var InfoUser
     *
     * @ORM\ManyToOne(targetEntity="InfoUser")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="additionalowner_id_new", referencedColumnName="id")
     * })
     */
    private $additionalownerNew;

    /**
     * @var int
     *
     * @ORM\Column(name="requesttype", type="integer", nullable=true)
     */
    private $requesttype;

    /**
     * @var InfoAddinfotype $addinfotype
     *
     * @ORM\ManyToOne(targetEntity="InfoAddinfotype")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="addinfotype_id", referencedColumnName="id")
     * })
     */
    private $addinfotype;

    /**
     * @var InfoDcrtype
     *
     * @ORM\ManyToOne(targetEntity="InfoDcrtype")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="dcrtype_id", referencedColumnName="id")
     * })
     */
    private $dcrType;

    /**
     * @var InfoRole
     *
     * @ORM\ManyToOne(targetEntity="InfoRole")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="role_id", referencedColumnName="id")
     * })
     */
    private $role;

    /**
     * @var InfoUser
     *
     * @ORM\ManyToOne(targetEntity="InfoUser")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     * })
     */
    private $user;



    /**
     * @var integer
     *
     * @ORM\Column(name="chain_num", type="integer", nullable=true)
     */
    private $chainNum;

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set modified
     *
     * @param \DateTime $modified
     *
     * @return InfoCompanyrequest
     */
    public function setModified($modified)
    {
        $this->modified = $modified;

        return $this;
    }

    /**
     * Get modified
     *
     * @return \DateTime
     */
    public function getModified()
    {
        return $this->modified;
    }

    /**
     * Set field
     *
     * @param string $field
     *
     * @return InfoCompanyrequest
     */
    public function setField($field)
    {
        $this->field = $field;

        return $this;
    }

    /**
     * Get field
     *
     * @return string
     */
    public function getField()
    {
        return $this->field;
    }

    /**
     * Set oldvalue
     *
     * @param string $oldvalue
     *
     * @return InfoCompanyrequest
     */
    public function setOldvalue($oldvalue)
    {
        $this->oldvalue = $oldvalue;

        return $this;
    }

    /**
     * Get oldvalue
     *
     * @return string
     */
    public function getOldvalue()
    {
        return $this->oldvalue;
    }

    /**
     * Set newvalue
     *
     * @param string $newvalue
     *
     * @return InfoCompanyrequest
     */
    public function setNewvalue($newvalue)
    {
        $this->newvalue = $newvalue;

        return $this;
    }

    /**
     * Get newvalue
     *
     * @return string
     */
    public function getNewvalue()
    {
        return $this->newvalue;
    }

    /**
     * Set comment
     *
     * @param string $comment
     *
     * @return InfoCompanyrequest
     */
    public function setComment($comment)
    {
        $this->comment = $comment;

        return $this;
    }

    /**
     * Get comment
     *
     * @return string
     */
    public function getComment()
    {
        return $this->comment;
    }

    /**
     * Set guid
     *
     * @param string $guid
     *
     * @return InfoCompanyrequest
     */
    public function setGuid($guid)
    {
        $this->guid = $guid;

        return $this;
    }

    /**
     * Get guid
     *
     * @return string
     */
    public function getGuid()
    {
        return $this->guid;
    }

    /**
     * Set currenttime
     *
     * @param \DateTime $currenttime
     *
     * @return InfoCompanyrequest
     */
    public function setCurrenttime(\DateTime $currenttime)
    {
        $this->currenttime = $currenttime;

        return $this;
    }

    /**
     * Get currenttime
     *
     * @return \DateTime
     */
    public function getCurrenttime()
    {
        return $this->currenttime;
    }

    /**
     * Set moduser
     *
     * @param string $moduser
     *
     * @return InfoCompanyrequest
     */
    public function setModuser($moduser)
    {
        $this->moduser = $moduser;

        return $this;
    }

    /**
     * Get moduser
     *
     * @return string
     */
    public function getModuser()
    {
        return $this->moduser;
    }

    /**
     * Set statusverification
     *
     * @param int $statusverification
     *
     * @return InfoCompanyrequest
     */
    public function setStatusverification($statusverification)
    {
        $this->statusverification = $statusverification;

        return $this;
    }

    /**
     * Get statusverification
     *
     * @return int
     */
    public function getStatusverification()
    {
        return $this->statusverification;
    }

    /**
     * Set statusverification2
     *
     * @param int $statusverification2
     *
     * @return InfoCompanyrequest
     */
    public function setStatusverification2($statusverification2)
    {
        $this->statusverification2 = $statusverification2;

        return $this;
    }

    /**
     * Get statusverification2
     *
     * @return int
     */
    public function getStatusverification2()
    {
        return $this->statusverification2;
    }

    /**
     * Set sendverification
     *
     * @param \DateTime $sendverification
     *
     * @return InfoCompanyrequest
     */
    public function setSendverification($sendverification)
    {
        $this->sendverification = $sendverification;

        return $this;
    }

    /**
     * Get sendverification
     *
     * @return \DateTime
     */
    public function getSendverification()
    {
        return $this->sendverification;
    }

    /**
     * Set archivereasonOld
     *
     * @param \TeamSoft\CrmRepositoryBundle\Entity\InfoArchivereason $archivereasonOld
     *
     * @return InfoCompanyrequest
     */
    public function setArchivereasonOld(\TeamSoft\CrmRepositoryBundle\Entity\InfoArchivereason $archivereasonOld = null)
    {
        $this->archivereasonOld = $archivereasonOld;

        return $this;
    }

    /**
     * Get archivereasonOld
     *
     * @return \TeamSoft\CrmRepositoryBundle\Entity\InfoArchivereason
     */
    public function getArchivereasonOld()
    {
        return $this->archivereasonOld;
    }

    /**
     * Set archivereasonNew
     *
     * @param \TeamSoft\CrmRepositoryBundle\Entity\InfoArchivereason $archivereasonNew
     *
     * @return InfoCompanyrequest
     */
    public function setArchivereasonNew(\TeamSoft\CrmRepositoryBundle\Entity\InfoArchivereason $archivereasonNew = null)
    {
        $this->archivereasonNew = $archivereasonNew;

        return $this;
    }

    /**
     * Get archivereasonNew
     *
     * @return \TeamSoft\CrmRepositoryBundle\Entity\InfoArchivereason
     */
    public function getArchivereasonNew()
    {
        return $this->archivereasonNew;
    }

    /**
     * Set cityOld
     *
     * @param \TeamSoft\CrmRepositoryBundle\Entity\InfoCity $cityOld
     *
     * @return InfoCompanyrequest
     */
    public function setCityOld(\TeamSoft\CrmRepositoryBundle\Entity\InfoCity $cityOld = null)
    {
        $this->cityOld = $cityOld;

        return $this;
    }

    /**
     * Get cityOld
     *
     * @return \TeamSoft\CrmRepositoryBundle\Entity\InfoCity
     */
    public function getCityOld()
    {
        return $this->cityOld;
    }

    /**
     * Set cityNew
     *
     * @param \TeamSoft\CrmRepositoryBundle\Entity\InfoCity $cityNew
     *
     * @return InfoCompanyrequest
     */
    public function setCityNew(\TeamSoft\CrmRepositoryBundle\Entity\InfoCity $cityNew = null)
    {
        $this->cityNew = $cityNew;

        return $this;
    }

    /**
     * Get cityNew
     *
     * @return \TeamSoft\CrmRepositoryBundle\Entity\InfoCity
     */
    public function getCityNew()
    {
        return $this->cityNew;
    }

    /**
     * Set company
     *
     * @param \TeamSoft\CrmRepositoryBundle\Entity\InfoCompany $company
     *
     * @return InfoCompanyrequest
     */
    public function setCompany(\TeamSoft\CrmRepositoryBundle\Entity\InfoCompany $company = null)
    {
        $this->company = $company;

        return $this;
    }

    /**
     * Get company
     *
     * @return \TeamSoft\CrmRepositoryBundle\Entity\InfoCompany
     */
    public function getCompany()
    {
        return $this->company;
    }

    /**
     * Set companycategoryOld
     *
     * @param InfoCompanycategory $companycategory
     *
     * @return InfoCompanyrequest
     */
    public function setCompanycategoryOld(InfoCompanycategory $companycategory = null)
    {
        $this->companycategoryOld = $companycategory;

        return $this;
    }

    /**
     * Get companycategoryOld
     *
     * @return InfoCompanycategory
     */
    public function getCompanycategoryOld()
    {
        return $this->companycategoryOld;
    }

    /**
     * Set companycategoryNew
     *
     * @param InfoCompanycategory $companycategory
     *
     * @return InfoCompanyrequest
     */
    public function setCompanycategoryNew(InfoCompanycategory $companycategory = null)
    {
        $this->companycategoryNew = $companycategory;

        return $this;
    }

    /**
     * Get companycategoryNew
     *
     * @return InfoCompanycategory
     */
    public function getCompanycategoryNew()
    {
        return $this->companycategoryNew;
    }

    /**
     * Set additionalcompanyOld
     *
     * @param InfoCompany $company
     *
     * @return InfoCompanyrequest
     */
    public function setAdditionalcompanyOld(InfoCompany $additionalcompanyOld = null)
    {
        $this->additionalcompanyOld = $additionalcompanyOld;

        return $this;
    }

    /**
     * Get additionalcompanyOld
     *
     * @return InfoCompany
     */
    public function getAdditionalcompanyOld()
    {
        return $this->additionalcompanyOld;
    }

    /**
     * Set additionalcompanyNew
     *
     * @param InfoCompany $company
     *
     * @return InfoCompanyrequest
     */
    public function setAdditionalcompanyNew(InfoCompany $additionalcompanyNew = null)
    {
        $this->additionalcompanyNew = $additionalcompanyNew;

        return $this;
    }

    /**
     * Get additionalcompanyNew
     *
     * @return InfoCompany
     */
    public function getAdditionalcompanyNew()
    {
        return $this->additionalcompanyNew;
    }

    /**
     * Set streettypeOld
     *
     * @param InfoDictionary $streettypeOld
     *
     * @return InfoCompanyrequest
     */
    public function setStreettypeOld(InfoDictionary $streettypeOld = null)
    {
        $this->streettypeOld = $streettypeOld;

        return $this;
    }

    /**
     * Get streettypeOld
     *
     * @return InfoDictionary
     */
    public function getStreettypeOld()
    {
        return $this->streettypeOld;
    }

    /**
     * Set streettypeNew
     *
     * @param InfoDictionary $streettypeNew
     *
     * @return InfoCompanyrequest
     */
    public function setStreettypeNew(InfoDictionary $streettypeNew = null)
    {
        $this->streettypeNew = $streettypeNew;

        return $this;
    }

    /**
     * Get streettypeNew
     *
     * @return InfoDictionary
     */
    public function getStreettypeNew()
    {
        return $this->streettypeNew;
    }

    /**
     * Set companytypeOld
     *
     * @param \TeamSoft\CrmRepositoryBundle\Entity\InfoCompanytype $companytypeOld
     *
     * @return InfoCompanyrequest
     */
    public function setCompanytypeOld(\TeamSoft\CrmRepositoryBundle\Entity\InfoCompanytype $companytypeOld = null)
    {
        $this->companytypeOld = $companytypeOld;

        return $this;
    }

    /**
     * Get companytypeOld
     *
     * @return \TeamSoft\CrmRepositoryBundle\Entity\InfoCompanytype
     */
    public function getCompanytypeOld()
    {
        return $this->companytypeOld;
    }

    /**
     * Set companytypeNew
     *
     * @param \TeamSoft\CrmRepositoryBundle\Entity\InfoCompanytype $companytypeNew
     *
     * @return InfoCompanyrequest
     */
    public function setCompanytypeNew(\TeamSoft\CrmRepositoryBundle\Entity\InfoCompanytype $companytypeNew = null)
    {
        $this->companytypeNew = $companytypeNew;

        return $this;
    }

    /**
     * Get companytypeNew
     *
     * @return \TeamSoft\CrmRepositoryBundle\Entity\InfoCompanytype
     */
    public function getCompanytypeNew()
    {
        return $this->companytypeNew;
    }

    /**
     * Set modifier
     *
     * @param \TeamSoft\CrmRepositoryBundle\Entity\InfoUser $modifier
     *
     * @return InfoCompanyrequest
     */
    public function setModifier(\TeamSoft\CrmRepositoryBundle\Entity\InfoUser $modifier = null)
    {
        $this->modifier = $modifier;

        return $this;
    }

    /**
     * Get modifier
     *
     * @return \TeamSoft\CrmRepositoryBundle\Entity\InfoUser
     */
    public function getModifier()
    {
        return $this->modifier;
    }

    /**
     * Set regionOld
     *
     * @param \TeamSoft\CrmRepositoryBundle\Entity\InfoRegion $regionOld
     *
     * @return InfoCompanyrequest
     */
    public function setRegionOld(\TeamSoft\CrmRepositoryBundle\Entity\InfoRegion $regionOld = null)
    {
        $this->regionOld = $regionOld;

        return $this;
    }

    /**
     * Get regionOld
     *
     * @return \TeamSoft\CrmRepositoryBundle\Entity\InfoRegion
     */
    public function getRegionOld()
    {
        return $this->regionOld;
    }

    /**
     * Set regionNew
     *
     * @param \TeamSoft\CrmRepositoryBundle\Entity\InfoRegion $regionNew
     *
     * @return InfoCompanyrequest
     */
    public function setRegionNew(\TeamSoft\CrmRepositoryBundle\Entity\InfoRegion $regionNew = null)
    {
        $this->regionNew = $regionNew;

        return $this;
    }

    /**
     * Get regionNew
     *
     * @return \TeamSoft\CrmRepositoryBundle\Entity\InfoRegion
     */
    public function getRegionNew()
    {
        return $this->regionNew;
    }

    /**
     * Set mainOld
     *
     * @param \TeamSoft\CrmRepositoryBundle\Entity\InfoCompany $mainOld
     *
     * @return InfoCompanyrequest
     */
    public function setMainOld(\TeamSoft\CrmRepositoryBundle\Entity\InfoCompany $mainOld = null)
    {
        $this->mainOld = $mainOld;

        return $this;
    }

    /**
     * Get mainOld
     *
     * @return \TeamSoft\CrmRepositoryBundle\Entity\InfoCompany
     */
    public function getMainOld()
    {
        return $this->mainOld;
    }

    /**
     * Set mainNew
     *
     * @param \TeamSoft\CrmRepositoryBundle\Entity\InfoCompany $mainNew
     *
     * @return InfoCompanyrequest
     */
    public function setMainNew(\TeamSoft\CrmRepositoryBundle\Entity\InfoCompany $mainNew = null)
    {
        $this->mainNew = $mainNew;

        return $this;
    }

    /**
     * Get mainNew
     *
     * @return \TeamSoft\CrmRepositoryBundle\Entity\InfoCompany
     */
    public function getMainNew()
    {
        return $this->mainNew;
    }

    /**
     * Set requesttype
     *
     * @param int $requesttype
     *
     * @return InfoCompanyrequest
     */
    public function setRequesttype($requesttype)
    {
        $this->requesttype = $requesttype;

        return $this;
    }

    /**
     * Get requesttype
     *
     * @return int
     */
    public function getRequesttype()
    {
        return $this->requesttype;
    }

    /**
     * Set dcrType
     *
     * @param int $dcrType
     *
     * @return InfoCompanyrequest
     */
    public function setDcrType($dcrType)
    {
        $this->dcrType = $dcrType;

        return $this;
    }

    /**
     * Get dcrType
     *
     * @return InfoDcrtype
     */
    public function getDcrType()
    {
        return $this->dcrType;
    }

    /**
     * Set role
     *
     * @param InfoRole $role
     *
     * @return InfoCompanyrequest
     */
    public function setRole($role)
    {
        $this->role = $role;

        return $this;
    }

    /**
     * Get role
     *
     * @return InfoRole
     */
    public function getRole()
    {
        return $this->role;
    }

    /**
     * Set addinfotype
     *
     * @param \TeamSoft\CrmRepositoryBundle\Entity\InfoAddinfotype $addinfotype
     *
     * @return InfoCompanyrequest
     */
    public function setAddinfotype(\TeamSoft\CrmRepositoryBundle\Entity\InfoAddinfotype $addinfotype = null)
    {
        $this->addinfotype = $addinfotype;

        return $this;
    }

    /**
     * Get addinfotype
     *
     * @return \TeamSoft\CrmRepositoryBundle\Entity\InfoAddinfotype
     */
    public function getAddinfotype()
    {
        return $this->addinfotype;
    }

    /**
     * Set ownerNew.
     *
     * @param \TeamSoft\CrmRepositoryBundle\Entity\InfoUser|null $ownerNew
     *
     * @return InfoCompanyrequest
     */
    public function setOwnerNew(\TeamSoft\CrmRepositoryBundle\Entity\InfoUser $ownerNew = null)
    {
        $this->ownerNew = $ownerNew;

        return $this;
    }

    /**
     * Get ownerNew.
     *
     * @return \TeamSoft\CrmRepositoryBundle\Entity\InfoUser|null
     */
    public function getOwnerNew()
    {
        return $this->ownerNew;
    }

    /**
     * Set ownerOld.
     *
     * @param \TeamSoft\CrmRepositoryBundle\Entity\InfoUser|null $ownerOld
     *
     * @return InfoCompanyrequest
     */
    public function setOwnerOld(\TeamSoft\CrmRepositoryBundle\Entity\InfoUser $ownerOld = null)
    {
        $this->ownerOld = $ownerOld;

        return $this;
    }

    /**
     * Get ownerOld.
     *
     * @return \TeamSoft\CrmRepositoryBundle\Entity\InfoUser|null
     */
    public function getOwnerOld()
    {
        return $this->ownerOld;
    }

    /**
     * Set additionalownerOld
     *
     * @param InfoUser|null additionalownerOld
     *
     * @return InfoCompanyrequest
     */
    public function setAdditionalownerOld(InfoUser $additionalownerOld = null)
    {
        $this->additionalownerOld = $additionalownerOld;

        return $this;
    }

    /**
     * Get additionalownerOld
     *
     * @return InfoUser|null
     */
    public function getAdditionalownerOld()
    {
        return $this->additionalownerOld;
    }

    /**
     * Set ownerOld.
     *
     * @param InfoUser|null $ownerOld
     *
     * @return InfoCompanyrequest
     */
    public function setAdditionalownerNew(InfoUser $additionalownerNew = null)
    {
        $this->additionalownerNew = $additionalownerNew;

        return $this;
    }

    /**
     * Get additionalownerNew.
     *
     * @return InfoUser|null
     */
    public function getAdditionalownerNew()
    {
        return $this->additionalownerNew;
    }

    /**
     * Set user.
     *
     * @param InfoUser|null $user
     *
     * @return InfoCompanyrequest
     */
    public function setUser(?InfoUser $user): self
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user.
     *
     * @return InfoUser|null
     */
    public function getUser(): ?InfoUser
    {
        return $this->user;
    }

    /**
     * Set chainNum
     *
     * @param integer $chainNum
     * @return InfoCompanyrequest
     */
    public function setChainNum(?int $chainNum): self
    {
        $this->chainNum = $chainNum;

        return $this;
    }

    /**
     * Get chainNum
     *
     * @return integer
     */
    public function getChainNum(): ?int
    {
        return $this->chainNum;
    }
}
