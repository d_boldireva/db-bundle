<?php

namespace TeamSoft\CrmRepositoryBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * InfoMcmcontentlanguage
 *
 * @ORM\Table(name="info_mcmcontentlanguage")
 * @ORM\Entity
 * @UniqueEntity("code")
 */
class InfoMcmcontentlanguage implements ServiceFieldInterface
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="`default`", type="integer", nullable=true)
     */
    private $default;

    /**
     * @var string
     *
     * @ORM\Column(name="body", type="string", nullable=true)
     */
    private $body;

    /**
     * @var string
     *
     * @ORM\Column(name="subject", type="string", nullable=true)
     */
    private $subject;

    /**
     * @var string
     *
     * @ORM\Column(name="caption", type="string", nullable=true)
     */
    private $caption;

    /**
     * @var string
     *
     * @ORM\Column(name="alternative_body", type="string", nullable=true)
     */
    private $alternativeBody;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="currenttime", type="datetime", nullable=true)
     */
    private $currenttime;

    /**
     * @var string
     *
     * @ORM\Column(name="moduser", type="string", length=16, nullable=true)
     */
    private $moduser;

    /**
     * @var string
     *
     * @ORM\Column(name="guid", type="guid", nullable=true)
     */
    private $guid;

    /**
     * @var InfoLanguage
     *
     * @ORM\ManyToOne(targetEntity="InfoLanguage")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="language_id", referencedColumnName="id")
     * })
     */
    private $language;

    /**
     * @var InfoMcmcontent
     *
     * @ORM\ManyToOne(targetEntity="InfoMcmcontent", inversedBy="contentCollection", cascade={"persist", "remove"})
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="content_id", referencedColumnName="id")
     * })
     */
    private $content;

    /**
     * @var string
     *
     * @ORM\Column(name="json", type="string", nullable=true)
     */
    private $json;

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set body
     *
     * @param string $body
     *
     * @return InfoMcmcontentlanguage
     */
    public function setBody(?string $body)
    {
        $this->body = $body;

        return $this;
    }

    /**
     * Get body
     *
     * @return string
     */
    public function getBody()
    {
        return $this->body;
    }

    /**
     * Set caption
     *
     * @param string $caption
     *
     * @return InfoMcmcontentlanguage
     */
    public function setCaption(?string $caption)
    {
        $this->caption = $caption;

        return $this;
    }

    /**
     * Get caption
     *
     * @return string
     */
    public function getCaption()
    {
        return $this->caption;
    }

    /**
     * Set subject
     *
     * @param string|null $subject
     *
     * @return InfoMcmcontentlanguage
     */
    public function setSubject(?string $subject)
    {
        $this->subject = $subject;

        return $this;
    }

    /**
     * Get subject
     *
     * @return string
     */
    public function getSubject()
    {
        return $this->subject;
    }

    /**
     * Set alternativeBody
     *
     * @param string $alternativeBody
     *
     * @return InfoMcmcontentlanguage
     */
    public function setAlternativeBody(?string $alternativeBody)
    {
        $this->alternativeBody = $alternativeBody;

        return $this;
    }

    /**
     * Get alternativeBody
     *
     * @return string
     */
    public function getAlternativeBody()
    {
        return $this->alternativeBody;
    }

    /**
     * Set default
     *
     * @param string $default
     *
     * @return InfoMcmcontentlanguage
     */
    public function setDefault($default)
    {
        $this->default = $default;

        return $this;
    }

    /**
     * Get default
     *
     * @return string
     */
    public function getDefault()
    {
        return $this->default;
    }

    /**
     * Set currenttime
     *
     * @param \DateTime $currenttime
     *
     * @return InfoMcmcontentlanguage
     */
    public function setCurrenttime(\DateTime $currenttime)
    {
        $this->currenttime = $currenttime;

        return $this;
    }

    /**
     * Get currenttime
     *
     * @return \DateTime
     */
    public function getCurrenttime()
    {
        return $this->currenttime;
    }

    /**
     * Set moduser
     *
     * @param string $moduser
     *
     * @return InfoMcmcontentlanguage
     */
    public function setModuser($moduser)
    {
        $this->moduser = $moduser;

        return $this;
    }

    /**
     * Get moduser
     *
     * @return string
     */
    public function getModuser()
    {
        return $this->moduser;
    }

    /**
     * Set guid
     *
     * @param string $guid
     *
     * @return InfoMcmcontentlanguage
     */
    public function setGuid($guid)
    {
        $this->guid = $guid;

        return $this;
    }

    /**
     * Get guid
     *
     * @return string
     */
    public function getGuid()
    {
        return $this->guid;
    }

    /**
     * Set language
     *
     * @param InfoMcmcontent $content
     * @return InfoMcmcontentlanguage
     */
    public function setLanguage(?InfoLanguage $language)
    {
        $this->language = $language;

        return $this;
    }

    /**
     * Get language
     *
     * @return InfoLanguage
     */
    public function getLanguage()
    {
        return $this->language;
    }

    /**
     * Set content
     *
     * @param InfoMcmcontent $content
     * @return InfoMcmcontentlanguage
     */
    public function setContent(?InfoMcmcontent $content)
    {
        $this->content = $content;

        return $this;
    }

    /**
     * Get content
     *
     * @return InfoMcmcontent
     */
    public function getContent()
    {
        return $this->content;
    }

    /**
     * @param null|string $json
     */
    public function setJson(?string $json): self
    {
        $this->json = $json;

        return $this;
    }

    /**
     * @return null|string
     */
    public function getJson(): ?string
    {
        return $this->json;
    }
}
