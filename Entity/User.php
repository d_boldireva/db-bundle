<?php

namespace TeamSoft\CrmRepositoryBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * PoUser
 *
 * @ORM\Table(name="po_user")
 * @ORM\Entity(repositoryClass="TeamSoft\CrmRepositoryBundle\Repository\User")
 */
class User implements ServiceFieldInterface
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="password", type="string", length=255, nullable=true)
     */
    private $password;

    /**
     * @var string
     *
     * @ORM\Column(name="salt", type="string", length=255, nullable=true)
     */
    private $salt;

    /**
     * @var array
     *
     * @ORM\Column(name="roles", type="json", nullable=true)
     */
    private $userRoles;

    /**
     * @ORM\ManyToMany(targetEntity="UserGroup")
     * @ORM\JoinTable(name="po_user_user_group",
     *   joinColumns={@ORM\JoinColumn(name="user_id", referencedColumnName="id")},
     *   inverseJoinColumns={@ORM\JoinColumn(name="user_group_id", referencedColumnName="id")}
     * )
     */
    private $groups;

    /**
     * @var integer
     *
     * @ORM\Column(name="is_active", type="integer", nullable=true)
     */
    private $isActive;

    /**
     * @var guid
     *
     * @ORM\Column(name="guid", type="guid", nullable=true)
     */
    private $guid;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="currenttime", type="datetime", nullable=true)
     */
    private $currenttime;

    /**
     * @var string
     *
     * @ORM\Column(name="moduser", type="string", length=16, nullable=true)
     */
    private $moduser;

    /**
     * @var \TeamSoft\CrmRepositoryBundle\Entity\InfoUser;
     *
     * @ORM\OneToOne(targetEntity="InfoUser", inversedBy="poUser")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="info_user_id", referencedColumnName="id")
     * })
     */
    private $infoUser;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->groups = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set password
     *
     * @param string $password
     *
     * @return User
     */
    public function setPassword($password)
    {
        $this->password = $password;

        return $this;
    }

    /**
     * Get password
     *
     * @return string
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * Set salt
     *
     * @param string $salt
     *
     * @return User
     */
    public function setSalt($salt)
    {
        $this->salt = $salt;

        return $this;
    }

    /**
     * Get salt
     *
     * @return string
     */
    public function getSalt()
    {
        return $this->salt;
    }

    /**
     * Set userRoles
     *
     * @param array $userRoles
     *
     * @return User
     */
    public function setUserRoles($userRoles)
    {
        $this->userRoles = $userRoles;

        return $this;
    }

    /**
     * Get userRoles
     *
     * @return array
     */
    public function getUserRoles()
    {
        return $this->userRoles;
    }

    /**
     * Get roles
     *
     * @return array
     */
    public function getRoles()
    {
        $roles = [];
        if (is_array($this->userRoles)) {
            $roles = array_merge($roles, $this->userRoles);
        }
        /**
         * @var UserGroup $group
         */
        foreach ($this->getGroups() as $group) {
            if (is_array($group->getRoles())) {
                $roles = array_merge($roles, $group->getRoles());
            }
        }
        return array_unique($roles);
    }

    /**
     * Add group
     *
     * @param UserGroup $group
     *
     * @return User
     */
    public function addGroup(UserGroup $group)
    {
        $this->groups->add($group);

        return $this;
    }

    /**
     * Remove group
     *
     * @param UserGroup $group
     */
    public function removeGroup(UserGroup $group)
    {
        $this->groups->removeElement($group);
    }

    /**
     * Get groups
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getGroups()
    {
        return $this->groups;
    }

    /**
     * Set guid
     *
     * @param guid $guid
     *
     * @return User
     */
    public function setGuid($guid)
    {
        $this->guid = $guid;

        return $this;
    }

    /**
     * Get guid
     *
     * @return guid
     */
    public function getGuid()
    {
        return $this->guid;
    }

    /**
     * Set currenttime
     *
     * @param \DateTime $currenttime
     *
     * @return User
     */
    public function setCurrenttime(\DateTime $currenttime)
    {
        $this->currenttime = $currenttime;

        return $this;
    }

    /**
     * Get currenttime
     *
     * @return \DateTime
     */
    public function getCurrenttime()
    {
        return $this->currenttime;
    }

    /**
     * Set moduser
     *
     * @param string $moduser
     *
     * @return User
     */
    public function setModuser($moduser)
    {
        $this->moduser = $moduser;

        return $this;
    }

    /**
     * Get moduser
     *
     * @return string
     */
    public function getModuser()
    {
        return $this->moduser;
    }

    /**
     * Set user
     *
     * @param \TeamSoft\CrmRepositoryBundle\Entity\InfoUser $user
     *
     * @return User
     */
    public function setInfoUser(\TeamSoft\CrmRepositoryBundle\Entity\InfoUser $user = null)
    {
        $this->infoUser = $user;

        return $this;
    }

    /**
     * Get infoUser
     *
     * @return \TeamSoft\CrmRepositoryBundle\Entity\InfoUser
     */
    public function getInfoUser()
    {
        return $this->infoUser;
    }

    /**
     * Set isActive
     *
     * @param int $isActive
     *
     * @return User
     */
    public function setIsActive($isActive)
    {
        $this->isActive = $isActive;

        return $this;
    }

    /**
     * Get isActive
     *
     * @return int
     */
    public function getIsActive()
    {
        return $this->isActive;
    }

    public function __serialize(): array
    {
        return [
            $this->id,
            $this->password,
            $this->salt,
            $this->isActive,
        ];
    }

    public function __unserialize( array $serialized)
    {
        [
            $this->id,
            $this->password,
            $this->salt,
            $this->isActive,
        ] = $serialized;
    }
}
