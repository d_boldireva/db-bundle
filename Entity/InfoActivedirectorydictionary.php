<?php

namespace TeamSoft\CrmRepositoryBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * InfoActivedirectorydictionary
 *
 * @ORM\Table(name="info_activedirectorydictionary")
 * @ORM\Entity
 */
class InfoActivedirectorydictionary implements ServiceFieldInterface
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string|null
     *
     * @ORM\Column(name="direction_value", type="string", length=255, nullable=true)
     */
    private $directionValue;

    /**
     * @var string|null
     *
     * @ORM\Column(name="department_value", type="string", length=255, nullable=true)
     */
    private $departmentValue;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="currenttime", type="datetime", nullable=true)
     */
    private $currenttime = 'CURRENT_TIMESTAMP';

    /**
     * @var string|null
     *
     * @ORM\Column(name="moduser", type="string", length=16, nullable=true)
     */
    private $moduser = '[dbo].[Get_CurrentCode]()';

    /**
     * @var string|null
     *
     * @ORM\Column(name="guid", type="guid", nullable=true)
     */
    private $guid = 'newid()';

    /**
     * @var InfoDirection
     *
     * @ORM\ManyToOne(targetEntity="InfoDirection")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="direction_id", referencedColumnName="id")
     * })
     */
    private $direction;



    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set directionValue.
     *
     * @param string|null $directionValue
     *
     * @return InfoActivedirectorydictionary
     */
    public function setDirectionValue($directionValue = null)
    {
        $this->directionValue = $directionValue;

        return $this;
    }

    /**
     * Get directionValue.
     *
     * @return string|null
     */
    public function getDirectionValue()
    {
        return $this->directionValue;
    }

    /**
     * Set departmentValue.
     *
     * @param string|null $departmentValue
     *
     * @return InfoActivedirectorydictionary
     */
    public function setDepartmentValue($departmentValue = null)
    {
        $this->departmentValue = $departmentValue;

        return $this;
    }

    /**
     * Get departmentValue.
     *
     * @return string|null
     */
    public function getDepartmentValue()
    {
        return $this->departmentValue;
    }

    /**
     * Set currenttime.
     *
     * @param \DateTime|null $currenttime
     *
     * @return InfoActivedirectorydictionary
     */
    public function setCurrenttime(\DateTime $currenttime = null)
    {
        $this->currenttime = $currenttime;

        return $this;
    }

    /**
     * Get currenttime.
     *
     * @return \DateTime|null
     */
    public function getCurrenttime()
    {
        return $this->currenttime;
    }

    /**
     * Set moduser.
     *
     * @param string|null $moduser
     *
     * @return InfoActivedirectorydictionary
     */
    public function setModuser($moduser = null)
    {
        $this->moduser = $moduser;

        return $this;
    }

    /**
     * Get moduser.
     *
     * @return string|null
     */
    public function getModuser()
    {
        return $this->moduser;
    }

    /**
     * Set guid.
     *
     * @param string|null $guid
     *
     * @return InfoActivedirectorydictionary
     */
    public function setGuid($guid = null)
    {
        $this->guid = $guid;

        return $this;
    }

    /**
     * Get guid.
     *
     * @return string|null
     */
    public function getGuid()
    {
        return $this->guid;
    }

    /**
     * Set direction.
     *
     * @param \TeamSoft\CrmRepositoryBundle\Entity\InfoDirection|null $direction
     *
     * @return InfoActivedirectorydictionary
     */
    public function setDirection(\TeamSoft\CrmRepositoryBundle\Entity\InfoDirection $direction = null)
    {
        $this->direction = $direction;

        return $this;
    }

    /**
     * Get direction.
     *
     * @return \TeamSoft\CrmRepositoryBundle\Entity\InfoDirection|null
     */
    public function getDirection()
    {
        return $this->direction;
    }
}
