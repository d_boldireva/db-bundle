<?php

namespace TeamSoft\CrmRepositoryBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * InfoRegioninuser
 *
 * @ORM\Table(name="info_regioninuser")
 * @ORM\Entity(repositoryClass="TeamSoft\CrmRepositoryBundle\Repository\InfoRegioninuser")
 */
class InfoRegioninuser implements ServiceFieldInterface
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="ismain", type="integer", nullable=true)
     */
    private $ismain;

    /**
     * @var InfoRegion
     *
     * @ORM\ManyToOne(targetEntity="InfoRegion")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="region_id", referencedColumnName="id")
     * })
     */
    private $region;

    /**
     * @var InfoUser
     *
     * @ORM\ManyToOne(targetEntity="InfoUser")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     * })
     */
    private $user;

    /**
     * @var string
     *
     * @ORM\Column(name="guid", type="guid", nullable=true)
     */
    private $guid;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="currenttime", type="datetime", nullable=true)
     */
    private $currenttime;

    /**
     * @var string
     *
     * @ORM\Column(name="moduser", type="string", length=16, nullable=true)
     */
    private $moduser;

    /**
     * Get region
     *
     * @return \TeamSoft\CrmRepositoryBundle\Entity\InfoRegion
     */
    public function getRegion()
    {
        return $this->region;
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set ismain
     *
     * @param int $ismain
     *
     * @return InfoRegioninuser
     */
    public function setIsmain($ismain)
    {
        $this->ismain = $ismain;
    
        return $this;
    }

    /**
     * Get ismain
     *
     * @return int
     */
    public function getIsmain()
    {
        return $this->ismain;
    }

    /**
     * Set region
     *
     * @param \TeamSoft\CrmRepositoryBundle\Entity\InfoRegion $region
     *
     * @return InfoRegioninuser
     */
    public function setRegion(InfoRegion $region = null)
    {
        $this->region = $region;
    
        return $this;
    }

    /**
     * Set user
     *
     * @param \TeamSoft\CrmRepositoryBundle\Entity\InfoUser $user
     *
     * @return InfoRegioninuser
     */
    public function setUser(InfoUser $user = null)
    {
        $this->user = $user;
    
        return $this;
    }

    /**
     * Get user
     *
     * @return \TeamSoft\CrmRepositoryBundle\Entity\InfoUser
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Set guid
     *
     * @param guid $guid
     *
     * @return InfoRegioninuser
     */
    public function setGuid($guid)
    {
        $this->guid = $guid;
    
        return $this;
    }

    /**
     * Get guid
     *
     * @return guid
     */
    public function getGuid()
    {
        return $this->guid;
    }

    /**
     * Set currenttime
     *
     * @param \DateTime $currenttime
     *
     * @return InfoRegioninuser
     */
    public function setCurrenttime(\DateTime $currenttime)
    {
        $this->currenttime = $currenttime;
    
        return $this;
    }

    /**
     * Get currenttime
     *
     * @return \DateTime
     */
    public function getCurrenttime()
    {
        return $this->currenttime;
    }

    /**
     * Set moduser
     *
     * @param string $moduser
     *
     * @return InfoRegioninuser
     */
    public function setModuser($moduser)
    {
        $this->moduser = $moduser;
    
        return $this;
    }

    /**
     * Get moduser
     *
     * @return string
     */
    public function getModuser()
    {
        return $this->moduser;
    }
}
