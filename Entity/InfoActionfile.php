<?php

namespace TeamSoft\CrmRepositoryBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use TeamSoft\CrmRepositoryBundle\Model\InfoFile;

/**
 * InfoActionfile
 *
 * @ORM\Table(name="info_actionfile")
 * @ORM\Entity
 */
class InfoActionfile extends InfoFile implements ServiceFieldInterface
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255, nullable=true)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="content", type="blob_or_string", nullable=true)
     */
    private $content;

    /**
     * @var integer
     *
     * @ORM\Column(name="fsize", type="integer", nullable=true)
     */
    private $fsize;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="fmodified", type="datetime", nullable=true)
     */
    private $fmodified;

    /**
     * @var guid
     *
     * @ORM\Column(name="guid", type="guid", nullable=true)
     */
    private $guid;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="currenttime", type="datetime", nullable=true)
     */
    private $currenttime;

    /**
     * @var string
     *
     * @ORM\Column(name="moduser", type="string", length=16, nullable=true)
     */
    private $moduser;

    /**
     * @var InfoActionfilegroup
     *
     * @ORM\ManyToOne(targetEntity="InfoActionfilegroup", inversedBy="actionFileCollection")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="subj_id", referencedColumnName="id")
     * })
     */
    private $subj;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return InfoActionfile
     */
    public function setName($name)
    {
        $this->name = $name;
    
        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set content
     *
     * @param string $content
     *
     * @return InfoActionfile
     */
    public function setContent($content)
    {
        $this->content = $content;
    
        return $this;
    }

    /**
     * Get content
     *
     * @return string
     */
    public function getContent()
    {
        return $this->content;
    }

    /**
     * Set fsize
     *
     * @param integer $fsize
     *
     * @return InfoActionfile
     */
    public function setFsize($fsize)
    {
        $this->fsize = $fsize;
    
        return $this;
    }

    /**
     * Get fsize
     *
     * @return integer
     */
    public function getFsize()
    {
        return $this->fsize;
    }

    /**
     * Set fmodified
     *
     * @param \DateTime $fmodified
     *
     * @return InfoActionfile
     */
    public function setFmodified($fmodified)
    {
        $this->fmodified = $fmodified;
    
        return $this;
    }

    /**
     * Get fmodified
     *
     * @return \DateTime
     */
    public function getFmodified()
    {
        return $this->fmodified;
    }

    /**
     * Set guid
     *
     * @param guid $guid
     *
     * @return InfoActionfile
     */
    public function setGuid($guid)
    {
        $this->guid = $guid;
    
        return $this;
    }

    /**
     * Get guid
     *
     * @return guid
     */
    public function getGuid()
    {
        return $this->guid;
    }

    /**
     * Set currenttime
     *
     * @param \DateTime $currenttime
     *
     * @return InfoActionfile
     */
    public function setCurrenttime(\DateTime $currenttime)
    {
        $this->currenttime = $currenttime;
    
        return $this;
    }

    /**
     * Get currenttime
     *
     * @return \DateTime
     */
    public function getCurrenttime()
    {
        return $this->currenttime;
    }

    /**
     * Set moduser
     *
     * @param string $moduser
     *
     * @return InfoActionfile
     */
    public function setModuser($moduser)
    {
        $this->moduser = $moduser;
    
        return $this;
    }

    /**
     * Get moduser
     *
     * @return string
     */
    public function getModuser()
    {
        return $this->moduser;
    }

    /**
     * Set subj
     *
     * @param \TeamSoft\CrmRepositoryBundle\Entity\InfoActionfilegroup $subj
     *
     * @return InfoActionfile
     */
    public function setSubj(\TeamSoft\CrmRepositoryBundle\Entity\InfoActionfilegroup $subj = null)
    {
        $this->subj = $subj;
    
        return $this;
    }

    /**
     * Get subj
     *
     * @return \TeamSoft\CrmRepositoryBundle\Entity\InfoActionfilegroup
     */
    public function getSubj()
    {
        return $this->subj;
    }
}
