<?php

namespace TeamSoft\CrmRepositoryBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * InfoMcmcontactdistributionmsg
 *
 * @ORM\Table(name="info_mcmcontactdistributionmsg")
 * @ORM\Entity
 */
class InfoMcmcontactdistributionmsg implements ServiceFieldInterface
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var InfoMcmdistribution
     *
     * @ORM\ManyToOne(targetEntity="InfoMcmdistribution")
     * @ORM\JoinColumn(name="distribution_id", referencedColumnName="id")
     */
    private $distribution;

    /**
     * @var InfoContact
     * @ORM\ManyToOne(targetEntity="InfoContact")
     * @ORM\JoinColumn(name="contact_id", referencedColumnName="id")
     */
    private $contact;

    /**
     * @var InfoUser
     *
     * @ORM\ManyToOne(targetEntity="InfoUser")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     * })
     */
    private $user;

    /**
     * @var InfoMcmdistributionaction
     *
     * @ORM\ManyToOne(targetEntity="InfoMcmdistributionaction")
     * @ORM\JoinColumn(name="distributionaction_id", referencedColumnName="id")
     */
    private $distributionAction;

    /**
     * @var InfoMcmcontent
     * @ORM\ManyToOne(targetEntity="InfoMcmcontent")
     * @ORM\JoinColumn(name="content_id", referencedColumnName="id")
     */
    private $content;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="currenttime", type="datetime", nullable=true)
     */
    private $currenttime;

    /**
     * @var string
     *
     * @ORM\Column(name="moduser", type="string", length=16, nullable=true)
     */
    private $moduser;

    /**
     * @var string
     *
     * @ORM\Column(name="guid", type="guid", nullable=true)
     */
    private $guid;

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set currenttime
     *
     * @param \DateTime $currenttime
     *
     * @return InfoMcmcontactdistributionmsg
     */
    public function setCurrenttime(\DateTime $currenttime)
    {
        $this->currenttime = $currenttime;

        return $this;
    }

    /**
     * Get currenttime
     *
     * @return \DateTime
     */
    public function getCurrenttime()
    {
        return $this->currenttime;
    }

    /**
     * Set moduser
     *
     * @param string $moduser
     *
     * @return InfoMcmcontactdistributionmsg
     */
    public function setModuser($moduser)
    {
        $this->moduser = $moduser;

        return $this;
    }

    /**
     * Get moduser
     *
     * @return string
     */
    public function getModuser()
    {
        return $this->moduser;
    }

    /**
     * Set guid
     *
     * @param string $guid
     *
     * @return InfoMcmcontactdistributionmsg
     */
    public function setGuid($guid)
    {
        $this->guid = $guid;

        return $this;
    }

    /**
     * Get guid
     *
     * @return string
     */
    public function getGuid()
    {
        return $this->guid;
    }

    /**
     * Set distribution
     *
     * @param InfoMcmdistribution $distribution
     *
     * @return InfoMcmcontactdistributionmsg
     */
    public function setDistribution(InfoMcmdistribution $distribution = null)
    {
        $this->distribution = $distribution;

        return $this;
    }

    /**
     * Get distribution
     *
     * @return InfoMcmdistribution
     */
    public function getDistribution()
    {
        return $this->distribution;
    }

    /**
     * Set distributionaction
     *
     * @param InfoMcmdistributionaction $distributionAction
     *
     * @return InfoMcmcontactdistributionmsg
     */
    public function setDistributionAction(InfoMcmdistributionaction $distributionAction = null)
    {
        $this->distributionAction = $distributionAction;

        return $this;
    }

    /**
     * Get distributionaction
     *
     * @return InfoMcmdistributionaction
     */
    public function getDistributionAction()
    {
        return $this->distributionAction;
    }

    /**
     * Get contact
     *
     * @return InfoContact
     */
    public function getContact()
    {
        return $this->contact;
    }

    /**
     * Set contact
     *
     * @param InfoContact $contact
     *
     * @return InfoMcmcontactdistributionmsg
     */
    public function setContact(InfoContact $contact = null)
    {
        $this->contact = $contact;

        return $this;
    }

    /**
     * Get content
     *
     * @return InfoMcmcontent
     */
    public function getContent()
    {
        return $this->content;
    }

    /**
     * Set content
     *
     * @param InfoMcmcontent $content
     *
     * @return InfoMcmcontactdistributionmsg
     */
    public function setContent(InfoMcmcontent $content = null)
    {
        $this->content = $content;

        return $this;
    }

    /**
     * Set user
     *
     * @param InfoUser $user
     *
     * @return InfoMcmcontactdistributionmsg
     */
    public function setUser(InfoUser $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return InfoUser
     */
    public function getUser()
    {
        return $this->user;
    }
}
