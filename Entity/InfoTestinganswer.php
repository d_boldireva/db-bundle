<?php

namespace TeamSoft\CrmRepositoryBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * InfoTestinganswer
 *
 * @ORM\Table(name="info_testinganswer")
 * @ORM\Entity
 */
class InfoTestinganswer implements ServiceFieldInterface
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255, nullable=true)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="string", length=255, nullable=true)
     */
    private $description;

    /**
     * @var int
     *
     * @ORM\Column(name="is_correct", type="integer", nullable=true)
     */
    private $isCorrect;

    /**
     * @var guid
     *
     * @ORM\Column(name="guid", type="guid", nullable=true)
     */
    private $guid;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="currenttime", type="datetime", nullable=true)
     */
    private $currenttime;

    /**
     * @var string
     *
     * @ORM\Column(name="moduser", type="string", length=16, nullable=true)
     */
    private $moduser;

    /**
     * @var InfoTestingquestion
     *
     * @ORM\ManyToOne(targetEntity="InfoTestingquestion")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="question_id", referencedColumnName="id")
     * })
     */
    private $question;

    /**
     * @var int
     *
     * @ORM\Column(name="row_num", type="integer", nullable=true)
     */
    private $rowNum;



    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return InfoTestinganswer
     */
    public function setName($name)
    {
        $this->name = $name;
    
        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set description
     *
     * @param string $description
     *
     * @return InfoTestinganswer
     */
    public function setDescription($description)
    {
        $this->description = $description;
    
        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set isCorrect
     *
     * @param int $isCorrect
     *
     * @return InfoTestinganswer
     */
    public function setIsCorrect($isCorrect)
    {
        $this->isCorrect = $isCorrect;
    
        return $this;
    }

    /**
     * Get isCorrect
     *
     * @return int
     */
    public function getIsCorrect()
    {
        return $this->isCorrect;
    }

    /**
     * Set guid
     *
     * @param guid $guid
     *
     * @return InfoTestinganswer
     */
    public function setGuid($guid)
    {
        $this->guid = $guid;
    
        return $this;
    }

    /**
     * Get guid
     *
     * @return guid
     */
    public function getGuid()
    {
        return $this->guid;
    }

    /**
     * Set currenttime
     *
     * @param \DateTime $currenttime
     *
     * @return InfoTestinganswer
     */
    public function setCurrenttime(\DateTime $currenttime)
    {
        $this->currenttime = $currenttime;
    
        return $this;
    }

    /**
     * Get currenttime
     *
     * @return \DateTime
     */
    public function getCurrenttime()
    {
        return $this->currenttime;
    }

    /**
     * Set moduser
     *
     * @param string $moduser
     *
     * @return InfoTestinganswer
     */
    public function setModuser($moduser)
    {
        $this->moduser = $moduser;
    
        return $this;
    }

    /**
     * Get moduser
     *
     * @return string
     */
    public function getModuser()
    {
        return $this->moduser;
    }

    /**
     * Set question
     *
     * @param \TeamSoft\CrmRepositoryBundle\Entity\InfoTestingquestion $question
     *
     * @return InfoTestinganswer
     */
    public function setQuestion(\TeamSoft\CrmRepositoryBundle\Entity\InfoTestingquestion $question = null)
    {
        $this->question = $question;
    
        return $this;
    }

    /**
     * Get question
     *
     * @return \TeamSoft\CrmRepositoryBundle\Entity\InfoTestingquestion
     */
    public function getQuestion()
    {
        return $this->question;
    }

    /**
     * Set rowNum
     *
     * @param int $rowNum
     *
     * @return InfoTestinganswer
     */
    public function setRowNum($rowNum)
    {
        $this->rowNum = $rowNum;
    
        return $this;
    }

    /**
     * Get rowNum
     *
     * @return int
     */
    public function getRowNum()
    {
        return $this->rowNum;
    }
}
