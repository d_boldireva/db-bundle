<?php

namespace TeamSoft\CrmRepositoryBundle\Entity;

use DateTime;
use Doctrine\ORM\Mapping as ORM;

/**
 * InfoDcrtyperoleworkflow
 *
 * @ORM\Table(name="info_dcrtyperoleworkflow")
 * @ORM\Entity()
 */
class InfoDcrtyperoleworkflow
{

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var InfoDcrtype
     *
     * @ORM\ManyToOne(targetEntity="InfoDcrtype")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="dcrtype_id", referencedColumnName="id")
     * })
     */
    private $dcrType;

    /**
     * @var InfoRole
     *
     * @ORM\ManyToOne(targetEntity="InfoRole")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="role_id", referencedColumnName="id")
     * })
     */
    private $role;

    /**
     * @var string
     *
     * @ORM\Column(name="chain_type", type="string", length=255, nullable=true)
     */
    private $chainType;

    /**
     * @var integer
     *
     * @ORM\Column(name="chain_num", type="integer", nullable=true)
     */
    private $chainNum;

    /**
     * @var string
     *
     * @ORM\Column(name="guid", type="guid", nullable=true)
     */
    private $guid;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="currenttime", type="datetime", nullable=true)
     */
    private $currenttime;

    /**
     * @var string
     *
     * @ORM\Column(name="moduser", type="string", length=16, nullable=true)
     */
    private $moduser;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * Set dcrType
     *
     * @param InfoDcrtype $dcrType
     * @return InfoDcrtyperoleworkflow
     */
    public function setDcrType(?InfoDcrtype $dcrType): self
    {
        $this->dcrType = $dcrType;

        return $this;
    }

    /**
     * Get dcrType
     *
     * @return InfoDcrtype
     */
    public function getDcrType(): ?InfoDcrtype
    {
        return $this->dcrType;
    }

    /**
     * Set role
     *
     * @param InfoRole $role
     * @return InfoDcrtyperoleworkflow
     */
    public function setRole(?InfoRole $role): self
    {
        $this->role = $role;

        return $this;
    }

    /**
     * Get role
     *
     * @return InfoRole
     */
    public function getRole(): ?InfoRole
    {
        return $this->role;
    }

    /**
     * Set chainType
     *
     * @param string $chainType
     * @return InfoDcrtyperoleworkflow
     */
    public function setChainType(?string $chainType): self
    {
        $this->chainType = $chainType;

        return $this;
    }

    /**
     * Get chainType
     *
     * @return string
     */
    public function getChainType(): ?string
    {
        return $this->chainType;
    }

    /**
     * Set chainNum
     *
     * @param integer $chainNum
     * @return InfoDcrtyperoleworkflow
     */
    public function setChainNum(?int $chainNum): self
    {
        $this->chainNum = $chainNum;

        return $this;
    }

    /**
     * Get chainNum
     *
     * @return integer
     */
    public function getChainNum(): ?int
    {
        return $this->chainNum;
    }

    /**
     * Set guid
     *
     * @param string $guid
     * @return InfoDcrtype
     */
    public function setGuid(?string $guid): self
    {
        $this->guid = $guid;

        return $this;
    }

    /**
     * Get guid
     *
     * @return string
     */
    public function getGuid(): ?string
    {
        return $this->guid;
    }

    /**
     * Set currenttime
     *
     * @param DateTime $currenttime
     * @return InfoDcrtype
     */
    public function setCurrenttime(?DateTime $currenttime): self
    {
        $this->currenttime = $currenttime;

        return $this;
    }

    /**
     * Get currenttime
     *
     * @return DateTime
     */
    public function getCurrenttime(): ?DateTime
    {
        return $this->currenttime;
    }

    /**
     * Set moduser
     *
     * @param string $moduser
     * @return InfoDcrtype
     */
    public function setModuser(?string $moduser): self
    {
        $this->moduser = $moduser;

        return $this;
    }

    /**
     * Get moduser
     *
     * @return string
     */
    public function getModuser(): ?string
    {
        return $this->moduser;
    }
}
