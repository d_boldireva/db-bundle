<?php

namespace TeamSoft\CrmRepositoryBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * InfoEventlocation
 *
 * @ORM\Table(name="info_eventlocation")
 * @ORM\Entity
 */
class InfoEventlocation
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var \InfoEvent
     *
     * @ORM\ManyToOne(targetEntity="InfoEvent")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="event_id", referencedColumnName="id")
     * })
     */
    private $event;

    /**
     * @var \InfoRegionpart
     *
     * @ORM\ManyToOne(targetEntity="InfoRegionpart")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="regionpart_id", referencedColumnName="id")
     * })
     */
    private $regionpart;

    /**
     * @var \InfoRegion
     *
     * @ORM\ManyToOne(targetEntity="InfoRegion")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="region_id", referencedColumnName="id")
     * })
     */
    private $region;

    /**
     * @var \InfoCity
     *
     * @ORM\ManyToOne(targetEntity="InfoCity")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="city_id", referencedColumnName="id")
     * })
     */
    private $city;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set event
     *
     * @param \TeamSoft\CrmRepositoryBundle\Entity\InfoEvent $event
     *
     * @return InfoEventlocation
     */
    public function setEvent(\TeamSoft\CrmRepositoryBundle\Entity\InfoEvent $event = null)
    {
        $this->event = $event;

        return $this;
    }

    /**
     * Get event
     *
     * @return \TeamSoft\CrmRepositoryBundle\Entity\InfoEvent
     */
    public function getEvent()
    {
        return $this->event;
    }

    /**
     * Set regionpart
     *
     * @param \TeamSoft\CrmRepositoryBundle\Entity\InfoRegionpart $regionpart
     *
     * @return InfoEventlocation
     */
    public function setRegionpart(\TeamSoft\CrmRepositoryBundle\Entity\InfoRegionpart $regionpart = null)
    {
        $this->regionpart = $regionpart;

        return $this;
    }

    /**
     * Get regionpart
     *
     * @return \TeamSoft\CrmRepositoryBundle\Entity\InfoRegionpart
     */
    public function getRegionpart()
    {
        return $this->regionpart;
    }

    /**
     * Set region
     *
     * @param \TeamSoft\CrmRepositoryBundle\Entity\InfoRegion $region
     *
     * @return InfoEventlocation
     */
    public function setRegion(\TeamSoft\CrmRepositoryBundle\Entity\InfoRegion $region = null)
    {
        $this->region = $region;

        return $this;
    }

    /**
     * Get region
     *
     * @return \TeamSoft\CrmRepositoryBundle\Entity\InfoRegion
     */
    public function getRegion()
    {
        return $this->region;
    }

    /**
     * Set city
     *
     * @param \TeamSoft\CrmRepositoryBundle\Entity\InfoCity $city
     *
     * @return InfoEventlocation
     */
    public function setCity(\TeamSoft\CrmRepositoryBundle\Entity\InfoCity $city = null)
    {
        $this->city = $city;

        return $this;
    }

    /**
     * Get city
     *
     * @return \TeamSoft\CrmRepositoryBundle\Entity\InfoCity
     */
    public function getCity()
    {
        return $this->city;
    }
}
