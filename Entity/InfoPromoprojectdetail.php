<?php

namespace TeamSoft\CrmRepositoryBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * InfoPromoprojectdetail
 *
 * @ORM\Table(name="info_promoprojectdetail")
 * @ORM\Entity
 */
class InfoPromoprojectdetail implements ServiceFieldInterface
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="currenttime", type="datetime", nullable=true)
     */
    private $currenttime;

    /**
     * @var string|null
     *
     * @ORM\Column(name="moduser", type="string", length=16, nullable=true)
     */
    private $moduser;

    /**
     * @var string|null
     *
     * @ORM\Column(name="guid", type="guid", nullable=true)
     */
    private $guid;

    /**
     * @var int|null
     *
     * @ORM\Column(name="ntask", type="integer", nullable=true)
     */
    private $ntask;

    /**
     * @var string|null
     *
     * @ORM\Column(name="msg", type="string", length=255, nullable=true)
     */
    private $msg;

    /**
     * @var int|null
     *
     * @ORM\Column(name="nquarter", type="integer", nullable=true)
     */
    private $nquarter;

    /**
     * @var InfoPromoproject
     *
     * @ORM\ManyToOne(targetEntity="InfoPromoproject")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="promoproject_id", referencedColumnName="id")
     * })
     */
    private $promoproject;

    /**
     * @var \Doctrine\Common\Collections\ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="InfoPromoprojectmaterial", mappedBy="promoprojectdetail", cascade={"persist"}, orphanRemoval=true)
     */
    private $materialCollection;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->materialCollection = new \Doctrine\Common\Collections\ArrayCollection();
    }
    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set currenttime.
     *
     * @param \DateTime|null $currenttime
     *
     * @return InfoPromoprojectdetail
     */
    public function setCurrenttime(\DateTime $currenttime = null)
    {
        $this->currenttime = $currenttime;

        return $this;
    }

    /**
     * Get currenttime.
     *
     * @return \DateTime|null
     */
    public function getCurrenttime()
    {
        return $this->currenttime;
    }

    /**
     * Set moduser.
     *
     * @param string|null $moduser
     *
     * @return InfoPromoprojectdetail
     */
    public function setModuser($moduser = null)
    {
        $this->moduser = $moduser;

        return $this;
    }

    /**
     * Get moduser.
     *
     * @return string|null
     */
    public function getModuser()
    {
        return $this->moduser;
    }

    /**
     * Set guid.
     *
     * @param string|null $guid
     *
     * @return InfoPromoprojectdetail
     */
    public function setGuid($guid = null)
    {
        $this->guid = $guid;

        return $this;
    }

    /**
     * Get guid.
     *
     * @return string|null
     */
    public function getGuid()
    {
        return $this->guid;
    }

    /**
     * Set ntask.
     *
     * @param int|null $ntask
     *
     * @return InfoPromoprojectdetail
     */
    public function setNtask($ntask = null)
    {
        $this->ntask = $ntask;

        return $this;
    }

    /**
     * Get ntask.
     *
     * @return int|null
     */
    public function getNtask()
    {
        return $this->ntask;
    }

    /**
     * Set msg.
     *
     * @param string|null $msg
     *
     * @return InfoPromoprojectdetail
     */
    public function setMsg($msg = null)
    {
        $this->msg = $msg;

        return $this;
    }

    /**
     * Get msg.
     *
     * @return string|null
     */
    public function getMsg()
    {
        return $this->msg;
    }

    /**
     * Set nquarter.
     *
     * @param int|null $nquarter
     *
     * @return InfoPromoprojectdetail
     */
    public function setNquarter($nquarter = null)
    {
        $this->nquarter = $nquarter;

        return $this;
    }

    /**
     * Get nquarter.
     *
     * @return int|null
     */
    public function getNquarter()
    {
        return $this->nquarter;
    }

    /**
     * Set promoproject.
     *
     * @param \TeamSoft\CrmRepositoryBundle\Entity\InfoPromoproject|null $promoproject
     *
     * @return InfoPromoprojectdetail
     */
    public function setPromoproject(\TeamSoft\CrmRepositoryBundle\Entity\InfoPromoproject $promoproject = null)
    {
        $this->promoproject = $promoproject;

        return $this;
    }

    /**
     * Get promoproject.
     *
     * @return \TeamSoft\CrmRepositoryBundle\Entity\InfoPromoproject|null
     */
    public function getPromoproject()
    {
        return $this->promoproject;
    }

    /**
     * Add materialCollection.
     *
     * @param \TeamSoft\CrmRepositoryBundle\Entity\InfoPromoprojectmaterial $material
     *
     * @return InfoPromoprojectdetail
     */
    public function addMaterialCollection(\TeamSoft\CrmRepositoryBundle\Entity\InfoPromoprojectmaterial $material)
    {
        $material->setPromoproject($this->getPromoproject());
        $material->setPromoprojectdetail($this);
        $this->materialCollection->add($material);

        return $this;
    }

    /**
     * Remove materialCollection.
     *
     * @param \TeamSoft\CrmRepositoryBundle\Entity\InfoPromoprojectmaterial $material
     *
     * @return boolean TRUE if this collection contained the specified element, FALSE otherwise.
     */
    public function removeMaterialCollection(\TeamSoft\CrmRepositoryBundle\Entity\InfoPromoprojectmaterial $material)
    {
        $material->setPromoproject(null);
        $material->setPromoprojectdetail(null);
        return $this->materialCollection->removeElement($material);
    }

    /**
     * Get materialCollection.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getMaterialCollection()
    {
        return $this->materialCollection;
    }
}
