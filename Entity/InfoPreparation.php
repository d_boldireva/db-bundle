<?php

namespace TeamSoft\CrmRepositoryBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * InfoPreparation
 *
 * @ORM\Table(name="info_preparation")
 * @ORM\Entity(repositoryClass="TeamSoft\CrmRepositoryBundle\Repository\InfoPreparation")
 */
class InfoPreparation
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="Name", type="string", length=255, nullable=false)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="Dose", type="string", length=255, nullable=true)
     */
    private $dose;

    /**
     * @var string
     *
     * @ORM\Column(name="Description", type="blob", nullable=true)
     */
    private $description;

    /**
     * @var string
     *
     * @ORM\Column(name="NameEng", type="string", length=255, nullable=true)
     */
    private $nameeng;

    /**
     * @var int
     *
     * @ORM\Column(name="ispromo", type="integer", nullable=true)
     */
    private $ispromo;

    /**
     * @var float|null
     *
     * @ORM\Column(name="price", type="float", nullable=true)
     */
    private $price;

    /**
     * @var string
     *
     * @ORM\Column(name="guid", type="guid", nullable=true)
     */
    private $guid;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="currenttime", type="datetime", nullable=true)
     */
    private $currenttime;

    /**
     * @var string
     *
     * @ORM\Column(name="moduser", type="string", length=16, nullable=true)
     */
    private $moduser;

    /**
     * @var string
     *
     * @ORM\Column(name="guid2", type="guid", nullable=true)
     */
    private $guid2;

    /**
     * @var int
     *
     * @ORM\Column(name="ismer", type="integer", nullable=true)
     */
    private $ismer;

    /**
     * @var string
     *
     * @ORM\Column(name="code", type="string", length=50, nullable=true)
     */
    private $code;

    /**
     * @var int
     *
     * @ORM\Column(name="row_num", type="integer", nullable=true)
     */
    private $rowNum;

    /**
     * @var InfoFormpart
     *
     * @ORM\ManyToOne(targetEntity="InfoFormpart")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="FormPart_id", referencedColumnName="id")
     * })
     */
    private $formpart;

    /**
     * @var InfoPreparationgroup
     *
     * @ORM\ManyToOne(targetEntity="InfoPreparationgroup")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="Group_id", referencedColumnName="id")
     * })
     */
    private $group;

    /**
     * @var InfoPreparationbrend
     *
     * @ORM\ManyToOne(targetEntity="InfoPreparationbrend")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="Brend_id", referencedColumnName="id")
     * })
     */
    private $brend;

    /**
     * @var int
     *
     * @ORM\Column(name="morionid", type="integer", nullable=true)
     */
    private $morionId;

    /**
     * ARTERIUMCA-140
     *
     * @var int
     *
     * @ORM\Column(name="externalcode", type="integer", nullable=true, unique=true)
     */
    private $externalcode;

    /**
     * @var ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="InfoCompanypreparation2", mappedBy="preparation")
     */
    private $companyPreparations2;

    /**
     * @ORM\ManyToMany(targetEntity="InfoDirection")
     * @ORM\JoinTable(name="info_preparationdirection",
     *     joinColumns={@ORM\JoinColumn(name="preparation_id", referencedColumnName="id")},
     *     inverseJoinColumns={@ORM\JoinColumn(name="direction_id", referencedColumnName="id")}
     * )
     */
    private $directionCollection;

    /**
     * @ORM\OneToMany(targetEntity="InfoPreparationdirection", mappedBy="preparation")
     */
    private $directionCollection2;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->companyPreparations2 = new ArrayCollection();
        $this->directionCollection = new ArrayCollection();
        $this->directionCollection2 = new ArrayCollection();
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return InfoPreparation
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set dose
     *
     * @param string $dose
     *
     * @return InfoPreparation
     */
    public function setDose($dose)
    {
        $this->dose = $dose;

        return $this;
    }

    /**
     * Get dose
     *
     * @return string
     */
    public function getDose()
    {
        return $this->dose;
    }

    /**
     * Set description
     *
     * @param string $description
     *
     * @return InfoPreparation
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set nameeng
     *
     * @param string $nameeng
     *
     * @return InfoPreparation
     */
    public function setNameeng($nameeng)
    {
        $this->nameeng = $nameeng;

        return $this;
    }

    /**
     * Get nameeng
     *
     * @return string
     */
    public function getNameeng()
    {
        return $this->nameeng;
    }

    /**
     * Set ispromo
     *
     * @param int $ispromo
     *
     * @return InfoPreparation
     */
    public function setIspromo($ispromo)
    {
        $this->ispromo = $ispromo;

        return $this;
    }

    /**
     * Get ispromo
     *
     * @return int
     */
    public function getIspromo()
    {
        return $this->ispromo;
    }

    /**
     * Set price
     *
     * @param float|null $price
     *
     * @return InfoPreparation
     */
    public function setPrice(float $price = null)
    {
        $this->price = $price;

        return $this;
    }

    /**
     * Get price
     *
     * @return float|null
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * Set guid
     *
     * @param string $guid
     *
     * @return InfoPreparation
     */
    public function setGuid($guid)
    {
        $this->guid = $guid;

        return $this;
    }

    /**
     * Get guid
     *
     * @return string
     */
    public function getGuid()
    {
        return $this->guid;
    }

    /**
     * Set currenttime
     *
     * @param \DateTime $currenttime
     *
     * @return InfoPreparation
     */
    public function setCurrenttime($currenttime)
    {
        $this->currenttime = $currenttime;

        return $this;
    }

    /**
     * Get currenttime
     *
     * @return \DateTime
     */
    public function getCurrenttime()
    {
        return $this->currenttime;
    }

    /**
     * Set moduser
     *
     * @param string $moduser
     *
     * @return InfoPreparation
     */
    public function setModuser($moduser)
    {
        $this->moduser = $moduser;

        return $this;
    }

    /**
     * Get moduser
     *
     * @return string
     */
    public function getModuser()
    {
        return $this->moduser;
    }

    /**
     * Set guid2
     *
     * @param string $guid2
     *
     * @return InfoPreparation
     */
    public function setGuid2($guid2)
    {
        $this->guid2 = $guid2;

        return $this;
    }

    /**
     * Get guid2
     *
     * @return string
     */
    public function getGuid2()
    {
        return $this->guid2;
    }

    /**
     * Set ismer
     *
     * @param int $ismer
     *
     * @return InfoPreparation
     */
    public function setIsmer($ismer)
    {
        $this->ismer = $ismer;

        return $this;
    }

    /**
     * Get ismer
     *
     * @return int
     */
    public function getIsmer()
    {
        return $this->ismer;
    }

    /**
     * Set code
     *
     * @param string $code
     *
     * @return InfoPreparation
     */
    public function setCode($code)
    {
        $this->code = $code;

        return $this;
    }

    /**
     * Get code
     *
     * @return string
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * Set rowNum
     *
     * @param int $rowNum
     *
     * @return InfoPreparation
     */
    public function setRowNum($rowNum)
    {
        $this->rowNum = $rowNum;

        return $this;
    }

    /**
     * Get rowNum
     *
     * @return int
     */
    public function getRowNum()
    {
        return $this->rowNum;
    }

    /**
     * Set formpart
     *
     * @param InfoFormpart $formpart
     *
     * @return InfoPreparation
     */
    public function setFormpart(InfoFormpart $formpart = null)
    {
        $this->formpart = $formpart;

        return $this;
    }

    /**
     * Get formpart
     *
     * @return InfoFormpart
     */
    public function getFormpart()
    {
        return $this->formpart;
    }

    /**
     * Set group
     *
     * @param InfoPreparationgroup $group
     *
     * @return InfoPreparation
     */
    public function setGroup(InfoPreparationgroup $group = null)
    {
        $this->group = $group;

        return $this;
    }

    /**
     * Get group
     *
     * @return InfoPreparationgroup
     */
    public function getGroup()
    {
        return $this->group;
    }

    /**
     * Set brend
     *
     * @param InfoPreparationbrend $brend
     *
     * @return InfoPreparation
     */
    public function setBrend(InfoPreparationbrend $brend = null)
    {
        $this->brend = $brend;

        return $this;
    }

    /**
     * Get brend
     *
     * @return InfoPreparationbrend
     */
    public function getBrend()
    {
        return $this->brend;
    }

    /**
     * Add companyPreparation2
     *
     * @param InfoCompanypreparation2 $companyPreparation2
     *
     * @return InfoPreparation
     */
    public function addCompanyPreparation2(InfoCompanypreparation2 $companyPreparation2)
    {
        $this->companyPreparations2[] = $companyPreparation2;

        return $this;
    }

    /**
     * Remove companyPreparation2
     *
     * @param InfoCompanypreparation2 $companyPreparation2
     */
    public function removeCompanyPreparation2(InfoCompanypreparation2 $companyPreparation2)
    {
        $this->companyPreparations2->removeElement($companyPreparation2);
    }

    /**
     * Get companyPreparation2
     *
     * @return Collection
     */
    public function getCompanyPreparations2()
    {
        return $this->companyPreparations2;
    }

    public function getCompanyPreparation2()
    {
        return $this->companyPreparations2[0];
    }

    /**
     * Set morionId
     *
     * @param int $morionId
     *
     * @return InfoPreparation
     */
    public function setMorionId($morionId)
    {
        $this->morionId = $morionId;

        return $this;
    }

    /**
     * Get morionId
     *
     * @return int
     */
    public function getMorionId()
    {
        return $this->morionId;
    }

    /**
     * Add companyPreparations2
     *
     * @param \TeamSoft\CrmRepositoryBundle\Entity\InfoCompanypreparation2 $companyPreparations2
     *
     * @return InfoPreparation
     */
    public function addCompanyPreparations2(InfoCompanypreparation2 $companyPreparations2)
    {
        $this->companyPreparations2[] = $companyPreparations2;

        return $this;
    }

    /**
     * Remove companyPreparations2
     *
     * @param \TeamSoft\CrmRepositoryBundle\Entity\InfoCompanypreparation2 $companyPreparations2
     */
    public function removeCompanyPreparations2(InfoCompanypreparation2 $companyPreparations2)
    {
        $this->companyPreparations2->removeElement($companyPreparations2);
    }

    /**
     * Add directionCollection
     *
     * @param \TeamSoft\CrmRepositoryBundle\Entity\InfoDirection $directionCollection
     *
     * @return InfoPreparation
     */
    public function addDirectionCollection(InfoDirection $directionCollection)
    {
        $this->directionCollection[] = $directionCollection;

        return $this;
    }

    /**
     * Remove directionCollection
     *
     * @param \TeamSoft\CrmRepositoryBundle\Entity\InfoDirection $directionCollection
     */
    public function removeDirectionCollection(InfoDirection $directionCollection)
    {
        $this->directionCollection->removeElement($directionCollection);
    }

    /**
     * Get directionCollection
     *
     * @return Collection
     */
    public function getDirectionCollection()
    {
        return $this->directionCollection;
    }

    /**
     * Set external code
     *
     * ARTERIUMCA-140
     *
     * @param null|string $externalcode
     * @return $this
     */
    public function setExternalCode($externalcode = null){
        $this->externalcode = $externalcode;
        return $this;
    }

    /**
     * Get external code
     *
     * ARTERIUMCA-140
     *
     * @return int|null
     */
    public function getExternalCode(){
        return $this->externalcode;
    }
}
