<?php

namespace TeamSoft\CrmRepositoryBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * InfoMcmjson
 *
 * @ORM\Table(name="info_mcmjson")
 * @ORM\Entity
 */
class InfoMcmjson implements ServiceFieldInterface
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="morionid", type="integer", nullable=false)
     */
    private $morionId;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="createdate", type="datetime", nullable=false)
     */
    private $createdate;

    /**
     * @var string|null
     *
     * @ORM\Column(name="name", type="string", length=255, nullable=false)
     */
    private $name;

    /**
     * @var string|null
     *
     * @ORM\Column(name="metric", type="json", nullable=false)
     */
    private $metric;

    /**
     * @var string|null
     *
     * @ORM\Column(name="guid", type="guid", nullable=true)
     */
    private $guid;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="currenttime", type="datetime", nullable=true)
     */
    private $currenttime;

    /**
     * @var string|null
     *
     * @ORM\Column(name="moduser", type="string", length=16, nullable=true)
     */
    private $moduser;

    /**
     * @var InfoMcmdistributionaction
     *
     * @ORM\ManyToOne(targetEntity="InfoMcmdistributionaction")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="mcmdistributionaction_id", referencedColumnName="id")
     * })
     */
    private $mcmDistributionAction;

    /**
     * @var InfoContact|null
     *
     * @ORM\ManyToOne(targetEntity="InfoContact", inversedBy="mcmJsons")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="contact_id", referencedColumnName="id")
     * })
     */
    private $contact;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getMorionId(): ?int
    {
        return $this->morionId;
    }

    public function setMorionId(int $morionId): InfoMcmjson
    {
        $this->morionId = $morionId;

        return $this;
    }

    public function setCreatedate(?\DateTime $createdate): self
    {
        $this->createdate = $createdate;

        return $this;
    }


    public function getCreatedate(): ?\DateTime
    {
        return $this->createdate;
    }

    public function setName(?string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setMetric(?object $metric): self
    {
        $this->metric = $metric;

        return $this;
    }

    public function getMetric(): ?object
    {
        return $this->metric;
    }

    public function setGuid($guid = null): self
    {
        $this->guid = $guid;

        return $this;
    }

    public function getGuid(): ?string
    {
        return $this->guid;
    }

    public function setCurrenttime(?\DateTime $currenttime): self
    {
        $this->currenttime = $currenttime;

        return $this;
    }

    public function getCurrenttime(): ?\DateTime
    {
        return $this->currenttime;
    }

    public function setModuser($moduser = null): self
    {
        $this->moduser = $moduser;

        return $this;
    }

    public function getModuser(): ?string
    {
        return $this->moduser;
    }

    public function getMcmDistributionAction(): ?InfoMcmdistributionaction
    {
        return $this->mcmDistributionAction;
    }

    public function setMcmDistributionAction(?InfoMcmdistributionaction $mcmDistributionAction): self
    {
        $this->mcmDistributionAction = $mcmDistributionAction;
        return $this;
    }

    public function getContact(): ?InfoContact
    {
        return $this->contact;
    }

    public function setContact(?InfoContact $contact): self
    {
        $this->contact = $contact;
        return $this;
    }
}
