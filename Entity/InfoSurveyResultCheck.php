<?php

namespace TeamSoft\CrmRepositoryBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Criteria;
use Doctrine\ORM\Mapping as ORM;

/**
 * InfoSurveyResultCheck
 *
 * @ORM\Table(name="info_surveyresultcheck")
 * @ORM\Entity
 */
class InfoSurveyResultCheck implements ServiceFieldInterface
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="guid", type="guid", nullable=true)
     */
    private $guid;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="currenttime", type="datetime", nullable=true)
     */
    private $currenttime;

    /**
     * @var string
     *
     * @ORM\Column(name="moduser", type="string", length=16, nullable=true)
     */
    private $moduser;

    /**
     * @var \InfoSurveyResult
     *
     * @ORM\ManyToOne(targetEntity="InfoSurveyResult")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="result_id", referencedColumnName="id")
     * })
     */
    private $result;

    /**
     * @var \InfoSurveyQuestionAnswer
     *
     * @ORM\ManyToOne(targetEntity="InfoSurveyQuestionAnswer")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="answer_id", referencedColumnName="id")
     * })
     */
    private $answer;

    public function __clone()
    {
        $this->id = null;
        $this->guid = null;
        $this->currenttime = null;
        $this->moduser = null;
    }

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set guid.
     *
     * @param string|null $guid
     *
     * @return InfoSurveyResultCheck
     */
    public function setGuid($guid = null)
    {
        $this->guid = $guid;

        return $this;
    }

    /**
     * Get guid.
     *
     * @return string|null
     */
    public function getGuid()
    {
        return $this->guid;
    }

    /**
     * Set currenttime.
     *
     * @param \DateTime|null $currenttime
     *
     * @return InfoSurveyResultCheck
     */
    public function setCurrenttime(\DateTime $currenttime = null)
    {
        $this->currenttime = $currenttime;

        return $this;
    }

    /**
     * Get currenttime.
     *
     * @return \DateTime|null
     */
    public function getCurrenttime()
    {
        return $this->currenttime;
    }

    /**
     * Set moduser.
     *
     * @param string|null $moduser
     *
     * @return InfoSurveyResultCheck
     */
    public function setModuser($moduser = null)
    {
        $this->moduser = $moduser;

        return $this;
    }

    /**
     * Get moduser.
     *
     * @return string|null
     */
    public function getModuser()
    {
        return $this->moduser;
    }

    /**
     * Set result.
     *
     * @param \TeamSoft\CrmRepositoryBundle\Entity\InfoSurveyResult|null $result
     *
     * @return InfoSurveyResultCheck
     */
    public function setResult(\TeamSoft\CrmRepositoryBundle\Entity\InfoSurveyResult $result = null)
    {
        $this->result = $result;

        return $this;
    }

    /**
     * Get result.
     *
     * @return \TeamSoft\CrmRepositoryBundle\Entity\InfoSurveyResult|null
     */
    public function getResult()
    {
        return $this->result;
    }

    /**
     * Set answer.
     *
     * @param \TeamSoft\CrmRepositoryBundle\Entity\InfoSurveyQuestionAnswer|null $answer
     *
     * @return InfoSurveyResultCheck
     */
    public function setAnswer(\TeamSoft\CrmRepositoryBundle\Entity\InfoSurveyQuestionAnswer $answer = null)
    {
        $this->answer = $answer;

        return $this;
    }

    /**
     * Get answer.
     *
     * @return \TeamSoft\CrmRepositoryBundle\Entity\InfoSurveyQuestionAnswer|null
     */
    public function getAnswer()
    {
        return $this->answer;
    }

    public function getAnswerId(): ?int
    {
        return $this->answer ? $this->answer->getId() : null;
    }
}
