<?php

namespace TeamSoft\CrmRepositoryBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * InfoContactphone
 *
 * @ORM\Table(name="info_contactphone")
 * @ORM\Entity
 * @ORM\EntityListeners({"TeamSoft\CrmRepositoryBundle\EventListener\InfoContactPhoneListener"})
 */
class InfoContactphone implements ServiceFieldInterface
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="phone", type="string", length=255, nullable=true)
     */
    private $phone;

    /**
     * @var string
     *
     * @ORM\Column(name="guid", type="guid", nullable=true)
     */
    private $guid;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="currenttime", type="datetime", nullable=true)
     */
    private $currenttime;

    /**
     * @var string
     *
     * @ORM\Column(name="moduser", type="string", length=16, nullable=true)
     */
    private $moduser;

    /**
     * @var integer
     *
     * @ORM\Column(name="morionid", type="integer", nullable=true)
     */
    private $morionid;

    /**
     * @var InfoContact
     *
     * @ORM\ManyToOne(targetEntity="InfoContact", inversedBy="contactPhoneCollection")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="contact_id", referencedColumnName="id")
     * })
     */
    private $contact;

    /**
     * @var int
     *
     * @ORM\Column(name="isviber", type="integer", nullable=true)
     */
    private $isviber;

    /**
     * @var int
     *
     * @ORM\Column(name="isvibermcm", type="integer", nullable=false)
     */
    private $isvibermcm;

    /**
     * @var int
     *
     * @ORM\Column(name="issmsmcm", type="integer", nullable=false)
     */
    private $issmsmcm;

    /**
     * @var int
     *
     * @ORM\Column(name="country_phone_mask", type="integer", nullable=true)
     */
    private $countryPhoneMask;

    /**
     * @var int
     *
     * @ORM\Column(name="from_site", type="integer", nullable=true)
     */
    private $fromSite;

    /**
     * @var integer|null
     *
     * @ORM\Column(name="isvisibleincrm", type="integer", nullable=true)
     */
    private $isVisibleInCrm;

    /**
     * @var InfoPhonetype|null
     * @ORM\ManyToOne(targetEntity="InfoPhonetype")
     * @ORM\JoinColumn(name="phonetype_id", referencedColumnName="id")
     */
    private $phoneType;

    /**
     * @var int
     *
     * @ORM\Column(name="for1stsms", type="integer", nullable=true)
     */
    private $for1stsms;

    /**
     * @var int
     *
     * @ORM\Column(name="istelegrammcm", type="integer", nullable=true)
     */
    private $isTelegramMcm;

    /**
     * @var int
     *
     * @ORM\Column(name="iswhatsappmcm", type="integer", nullable=true)
     */
    private $isWhatsappMcm;

    /**
     * @var int
     *
     * @ORM\Column(name="ismcmcall", type="integer", nullable=true)
     */
    private $isMcmCall;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set phone
     *
     * @param string $phone
     *
     * @return InfoContactphone
     */
    public function setPhone($phone)
    {
        $this->phone = $phone;

        return $this;
    }

    /**
     * Get phone
     *
     * @return string
     */
    public function getPhone()
    {
        return $this->phone;
    }

    /**
     * Set guid
     *
     * @param string $guid
     *
     * @return InfoContactphone
     */
    public function setGuid($guid)
    {
        $this->guid = $guid;

        return $this;
    }

    /**
     * Get guid
     *
     * @return string
     */
    public function getGuid()
    {
        return $this->guid;
    }

    /**
     * Set currenttime
     *
     * @param \DateTime $currenttime
     *
     * @return InfoContactphone
     */
    public function setCurrenttime($currenttime)
    {
        $this->currenttime = $currenttime;

        return $this;
    }

    /**
     * Get currenttime
     *
     * @return \DateTime
     */
    public function getCurrenttime()
    {
        return $this->currenttime;
    }

    /**
     * Set moduser
     *
     * @param string $moduser
     *
     * @return InfoContactphone
     */
    public function setModuser($moduser)
    {
        $this->moduser = $moduser;

        return $this;
    }

    /**
     * Get moduser
     *
     * @return string
     */
    public function getModuser()
    {
        return $this->moduser;
    }

    /**
     * Set morionid
     *
     * @param integer $morionid
     *
     * @return InfoContactphone
     */
    public function setMorionid($morionid)
    {
        $this->morionid = $morionid;

        return $this;
    }

    /**
     * Get morionid
     *
     * @return integer
     */
    public function getMorionid()
    {
        return $this->morionid;
    }

    /**
     * Set contact
     *
     * @param \TeamSoft\CrmRepositoryBundle\Entity\InfoContact $contact
     *
     * @return InfoContactphone
     */
    public function setContact(\TeamSoft\CrmRepositoryBundle\Entity\InfoContact $contact = null)
    {
        $this->contact = $contact;

        return $this;
    }

    /**
     * Get contact
     *
     * @return \TeamSoft\CrmRepositoryBundle\Entity\InfoContact
     */
    public function getContact()
    {
        return $this->contact;
    }

    /**
     * Set isviber
     *
     * @param int $isviber
     *
     * @return InfoMcmtarget
     */
    public function setIsviber($isviber)
    {
        $this->isviber = $isviber;

        return $this;
    }

    /**
     * Get isviber
     *
     * @return int
     */
    public function getIsviber()
    {
        return $this->isviber;
    }

    /**
     * Get isvibermcm
     *
     * @return int
     */
    public function getIsvibermcm()
    {
        return $this->isvibermcm;
    }

    /**
     * Get issmsmcm
     *
     * @return int
     */
    public function getIssmsmcm()
    {
        return $this->issmsmcm;
    }

    /**
     * Set countryPhoneMask
     *
     * @param int $countryPhoneMask
     *
     * @return InfoContactphone
     */
    public function setCountryPhoneMask($countryPhoneMask)
    {
        $this->countryPhoneMask = $countryPhoneMask;

        return $this;
    }

    /**
     * Get countryPhoneMask
     *
     * @return int
     */
    public function getCountryPhoneMask()
    {
        return $this->countryPhoneMask;
    }

    /**
     * Set isvibermcm
     *
     * @param int $isvibermcm
     *
     * @return InfoContactphone
     */
    public function setIsViberMcm($isvibermcm)
    {
        $this->isvibermcm = $isvibermcm;

        return $this;
    }

    /**
     * Set issmsmcm
     *
     * @param int $issmsmcm
     *
     * @return InfoContactphone
     */
    public function setIsSmsMcm($issmsmcm)
    {
        $this->issmsmcm = $issmsmcm;

        return $this;
    }

    /**
     * Set fromSite
     *
     * @param int $fromSite
     *
     * @return InfoContactphone
     */
    public function setFromSite($fromSite)
    {
        $this->fromSite = $fromSite;

        return $this;
    }

    /**
     * Get fromSite
     *
     * @return int
     */
    public function getFromSite()
    {
        return $this->fromSite;
    }

    public function isViberForMcm(): bool
    {
        return (bool)$this->getIsvibermcm();
    }

    public function isSmsForMcm(): bool
    {
        return (bool)$this->getIssmsmcm();
    }

    /**
     * @return InfoPhonetype|null
     */
    public function getPhoneType(): ?InfoPhonetype
    {
        return $this->phoneType;
    }

    /**
     * @param InfoPhonetype|null $phoneType
     * @return self
     */
    public function setPhoneType(?InfoPhonetype $phoneType): self
    {
        $this->phoneType = $phoneType;

        return $this;
    }

    /**
     * @return int|null
     */
    public function getIsVisibleInCrm(): ?int
    {
        return $this->isVisibleInCrm;
    }

    /**
     * @param int|null $isVisibleInCrm
     * @return self
     */
    public function setIsVisibleInCrm(?int $isVisibleInCrm): self
    {
        $this->isVisibleInCrm = $isVisibleInCrm;

        return $this;
    }

    /**
     * @return int|null
     */
    public function getContactPhoneTypeId(): ?int
    {
        return $this->getPhoneType() ? $this->getPhoneType()->getId() : null;
    }

    /**
     * Set for1stsms
     *
     * @param int $for1stsms
     *
     * @return InfoContactphone
     */
    public function setFor1stsms($for1stsms)
    {
        $this->for1stsms = $for1stsms;

        return $this;
    }

    /**
     * Get for1stsms
     *
     * @return int
     */
    public function getFor1stsms()
    {
        return $this->for1stsms;
    }

    /**
     * Set isTelegramMcm
     *
     * @param null|int $isTelegramMcm
     *
     * @return InfoContactphone
     */
    public function setIsTelegramMcm(?int $isTelegramMcm): self
    {
        $this->isTelegramMcm = $isTelegramMcm;

        return $this;
    }

    /**
     * Get isTelegramMcm
     *
     * @return null|int
     */
    public function getIsTelegramMcm(): ?int
    {
        return $this->isTelegramMcm;
    }

    /**
     * Set isWhatsappMcm
     *
     * @param null|int $isWhatsappMcm
     *
     * @return InfoContactphone
     */
    public function setIsWhatsappMcm(?int $isWhatsappMcm): self
    {
        $this->isWhatsappMcm = $isWhatsappMcm;

        return $this;
    }

    /**
     * Get isWhatsappMcm
     *
     * @return null|int
     */
    public function getIsWhatsappMcm(): ?int
    {
        return $this->isWhatsappMcm;
    }

    /**
     * Set isMcmCall
     *
     * @param null|int $isMcmCall
     *
     * @return InfoContactphone
     */
    public function setIsMcmCall(?int $isMcmCall): self
    {
        $this->isMcmCall = $isMcmCall;

        return $this;
    }

    /**
     * Get isMcmCall
     *
     * @return null|int
     */
    public function getIsMcmCall(): ?int
    {
        return $this->isMcmCall;
    }
}
