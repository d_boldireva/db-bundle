<?php

namespace TeamSoft\CrmRepositoryBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * InfoContactpotential
 *
 * @ORM\Table(name="info_contactpotential")
 * @ORM\Entity(repositoryClass="TeamSoft\CrmRepositoryBundle\Repository\InfoContactpotential")
 */
class InfoContactpotential implements ServiceFieldInterface
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="Date", type="datetime", nullable=true)
     */
    private $date;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="Date2", type="datetime", nullable=true)
     */
    private $date2;

    /**
     * @var int
     *
     * @ORM\Column(name="Potential", type="float", nullable=true)
     */
    private $potential;

    /**
     * @var int
     *
     * @ORM\Column(name="PlanCount", type="float", nullable=true)
     */
    private $plancount;

    /**
     * @var int
     *
     * @ORM\Column(name="FactCount", type="float", nullable=true)
     */
    private $factcount;

    /**
     * @var string
     *
     * @ORM\Column(name="guid", type="guid", nullable=true)
     */
    private $guid;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="currenttime", type="datetime", nullable=true)
     */
    private $currenttime;

    /**
     * @var string
     *
     * @ORM\Column(name="moduser", type="string", length=16, nullable=true)
     */
    private $moduser;

    /**
     * @var int
     *
     * @ORM\Column(name="control", type="integer", nullable=true)
     */
    private $control;

    /**
     * @var InfoRivalpreparation
     *
     * @ORM\ManyToOne(targetEntity="InfoRivalpreparation")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="rivalprep_id", referencedColumnName="id")
     * })
     */
    private $rivalprep;

    /**
     * @var InfoPreparation
     *
     * @ORM\ManyToOne(targetEntity="InfoPreparation")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="prep_id", referencedColumnName="id")
     * })
     */
    private $prep;

    /**
     * @var InfoTask
     *
     * @ORM\ManyToOne(targetEntity="InfoTask", inversedBy="contactPotentialCollection")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="task_id", referencedColumnName="id")
     * })
     */
    private $task;

    /**
     * @var InfoContact
     *
     * @ORM\ManyToOne(targetEntity="InfoContact")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="Contact_id", referencedColumnName="id")
     * })
     */
    private $contact;

    /**
     * @var InfoDictionary
     *
     * @ORM\ManyToOne(targetEntity="InfoDictionary")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="dictionary_id", referencedColumnName="id")
     * })
     */
    private $dictionary;

    /**
     * @var InfoPatalog
     *
     * @ORM\ManyToOne(targetEntity="InfoPatalog")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="patalog_id", referencedColumnName="id")
     * })
     */
    private $patalog;

    /**
     * @var InfoPreparationbrend
     *
     * @ORM\ManyToOne(targetEntity="InfoPreparationbrend")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="brend_id", referencedColumnName="id")
     * })
     */
    private $brend;



    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set date
     *
     * @param \DateTime $date
     *
     * @return InfoContactpotential
     */
    public function setDate($date)
    {
        $this->date = $date;
    
        return $this;
    }

    /**
     * Get date
     *
     * @return \DateTime
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * Set date2
     *
     * @param \DateTime $date2
     *
     * @return InfoContactpotential
     */
    public function setDate2($date2)
    {
        $this->date2 = $date2;
    
        return $this;
    }

    /**
     * Get date2
     *
     * @return \DateTime
     */
    public function getDate2()
    {
        return $this->date2;
    }

    /**
     * Set potential
     *
     * @param int $potential
     *
     * @return InfoContactpotential
     */
    public function setPotential($potential)
    {
        $this->potential = $potential;
    
        return $this;
    }

    /**
     * Get potential
     *
     * @return int
     */
    public function getPotential()
    {
        return $this->potential;
    }

    /**
     * Set plancount
     *
     * @param int $plancount
     *
     * @return InfoContactpotential
     */
    public function setPlancount($plancount)
    {
        $this->plancount = $plancount;
    
        return $this;
    }

    /**
     * Get plancount
     *
     * @return int
     */
    public function getPlancount()
    {
        return $this->plancount;
    }

    /**
     * Set factcount
     *
     * @param int $factcount
     *
     * @return InfoContactpotential
     */
    public function setFactcount($factcount)
    {
        $this->factcount = $factcount;
    
        return $this;
    }

    /**
     * Get factcount
     *
     * @return int
     */
    public function getFactcount()
    {
        return $this->factcount;
    }

    /**
     * Set guid
     *
     * @param guid $guid
     *
     * @return InfoContactpotential
     */
    public function setGuid($guid)
    {
        $this->guid = $guid;
    
        return $this;
    }

    /**
     * Get guid
     *
     * @return guid
     */
    public function getGuid()
    {
        return $this->guid;
    }

    /**
     * Set currenttime
     *
     * @param \DateTime $currenttime
     *
     * @return InfoContactpotential
     */
    public function setCurrenttime(\DateTime $currenttime)
    {
        $this->currenttime = $currenttime;
    
        return $this;
    }

    /**
     * Get currenttime
     *
     * @return \DateTime
     */
    public function getCurrenttime()
    {
        return $this->currenttime;
    }

    /**
     * Set moduser
     *
     * @param string $moduser
     *
     * @return InfoContactpotential
     */
    public function setModuser($moduser)
    {
        $this->moduser = $moduser;
    
        return $this;
    }

    /**
     * Get moduser
     *
     * @return string
     */
    public function getModuser()
    {
        return $this->moduser;
    }

    /**
     * Set control
     *
     * @param int $control
     *
     * @return InfoContactpotential
     */
    public function setControl($control)
    {
        $this->control = $control;
    
        return $this;
    }

    /**
     * Get control
     *
     * @return int
     */
    public function getControl()
    {
        return $this->control;
    }

    /**
     * Set rivalprep
     *
     * @param \TeamSoft\CrmRepositoryBundle\Entity\InfoRivalpreparation $rivalprep
     *
     * @return InfoContactpotential
     */
    public function setRivalprep(\TeamSoft\CrmRepositoryBundle\Entity\InfoRivalpreparation $rivalprep = null)
    {
        $this->rivalprep = $rivalprep;
    
        return $this;
    }

    /**
     * Get rivalprep
     *
     * @return \TeamSoft\CrmRepositoryBundle\Entity\InfoRivalpreparation
     */
    public function getRivalprep()
    {
        return $this->rivalprep;
    }

    /**
     * Set prep
     *
     * @param \TeamSoft\CrmRepositoryBundle\Entity\InfoPreparation $prep
     *
     * @return InfoContactpotential
     */
    public function setPrep(\TeamSoft\CrmRepositoryBundle\Entity\InfoPreparation $prep = null)
    {
        $this->prep = $prep;
    
        return $this;
    }

    /**
     * Get prep
     *
     * @return \TeamSoft\CrmRepositoryBundle\Entity\InfoPreparation
     */
    public function getPrep()
    {
        return $this->prep;
    }

    /**
     * Set task
     *
     * @param \TeamSoft\CrmRepositoryBundle\Entity\InfoTask $task
     *
     * @return InfoContactpotential
     */
    public function setTask(\TeamSoft\CrmRepositoryBundle\Entity\InfoTask $task = null)
    {
        $this->task = $task;
    
        return $this;
    }

    /**
     * Get task
     *
     * @return \TeamSoft\CrmRepositoryBundle\Entity\InfoTask
     */
    public function getTask()
    {
        return $this->task;
    }

    /**
     * Set contact
     *
     * @param \TeamSoft\CrmRepositoryBundle\Entity\InfoContact $contact
     *
     * @return InfoContactpotential
     */
    public function setContact(\TeamSoft\CrmRepositoryBundle\Entity\InfoContact $contact = null)
    {
        $this->contact = $contact;
    
        return $this;
    }

    /**
     * Get contact
     *
     * @return \TeamSoft\CrmRepositoryBundle\Entity\InfoContact
     */
    public function getContact()
    {
        return $this->contact;
    }

    /**
     * Set dictionary
     *
     * @param \TeamSoft\CrmRepositoryBundle\Entity\InfoDictionary $dictionary
     *
     * @return InfoContactpotential
     */
    public function setDictionary(\TeamSoft\CrmRepositoryBundle\Entity\InfoDictionary $dictionary = null)
    {
        $this->dictionary = $dictionary;
    
        return $this;
    }

    /**
     * Get dictionary
     *
     * @return \TeamSoft\CrmRepositoryBundle\Entity\InfoDictionary
     */
    public function getDictionary()
    {
        return $this->dictionary;
    }

    /**
     * Set patalog
     *
     * @param \TeamSoft\CrmRepositoryBundle\Entity\InfoPatalog $patalog
     *
     * @return InfoContactpotential
     */
    public function setPatalog(\TeamSoft\CrmRepositoryBundle\Entity\InfoPatalog $patalog = null)
    {
        $this->patalog = $patalog;
    
        return $this;
    }

    /**
     * Get patalog
     *
     * @return \TeamSoft\CrmRepositoryBundle\Entity\InfoPatalog
     */
    public function getPatalog()
    {
        return $this->patalog;
    }

    /**
     * Set brend
     *
     * @param \TeamSoft\CrmRepositoryBundle\Entity\InfoPreparationbrend $brend
     *
     * @return InfoContactpotential
     */
    public function setBrend(\TeamSoft\CrmRepositoryBundle\Entity\InfoPreparationbrend $brend = null)
    {
        $this->brend = $brend;
    
        return $this;
    }

    /**
     * Get brend
     *
     * @return \TeamSoft\CrmRepositoryBundle\Entity\InfoPreparationbrend
     */
    public function getBrend()
    {
        return $this->brend;
    }

    public function getTaskId () {
        return $this->getTask() ? $this->getTask()->getId() : null;
    }

    public function getBrandId () {
        return $this->getBrend() ? $this->getBrend()->getId() : null;
    }

    public function getPreparationId () {
        return $this->getPrep() ? $this->getPrep()->getId() : null;
    }

    public function getContactId () {
        return $this->getContact() ? $this->getContact()->getId() : null;
    }
}
