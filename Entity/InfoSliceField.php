<?php

namespace TeamSoft\CrmRepositoryBundle\Entity;

use DateTime;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * InfoSliceField
 *
 * @ORM\Table(name="info_slicefield")
 * @ORM\Entity(repositoryClass="TeamSoft\CrmRepositoryBundle\Repository\Reports\InfoSliceFieldRepository")
 */
class InfoSliceField implements ServiceFieldInterface
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string|null
     *
     * @ORM\Column(name="name", type="string", length=255, nullable=true)
     */
    private $name;

    /**
     * @var string|null
     *
     * @ORM\Column(name="field", type="string", length=255, nullable=true)
     */
    private $field;

    /**
     * @var DateTime|null
     *
     * @ORM\Column(name="currenttime", type="datetime", nullable=true)
     */
    private $currenttime;

    /**
     * @var string|null
     *
     * @ORM\Column(name="moduser", type="string", length=16, nullable=true)
     * )
     */
    private $moduser;

    /**
     * @var string|null
     *
     * @ORM\Column(name="guid", type="guid", nullable=true)
     */
    private $guid;

    /**
     * Many fields has one slice
     * @ORM\ManyToOne(targetEntity="InfoSlice", inversedBy="id")
     * @ORM\JoinColumn(name="slice_id", referencedColumnName="id")
     */
    private $slice;

    /**
     * One slice fields has many report fields
     * @ORM\OneToMany(targetEntity="InfoReportTabField", mappedBy="field")
     * @ORM\JoinColumn(name="id", referencedColumnName="field")
     */
    private $reportFields;

    public function __construct()
    {
        $this->reportFields = new ArrayCollection();
    }


    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set slice
     *
     * @param InfoSlice $slice
     *
     * @return InfoSliceField
     */
    public function setSlice($slice = null)
    {
        $this->slice = $slice;

        return $this;
    }

    /**
     * Get slice
     *
     * @return InfoSlice
     */
    public function getSlice()
    {
        return $this->slice;
    }

    /**
     * Set name.
     *
     * @param string|null $name
     *
     * @return InfoSliceField
     */
    public function setName($name = null)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name.
     *
     * @return string|null
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set field.
     *
     * @param string|null $field
     *
     * @return InfoSliceField
     */
    public function setField($field = null)
    {
        $this->field = $field;

        return $this;
    }

    /**
     * Get field.
     *
     * @return string|null
     */
    public function getField()
    {
        return $this->field;
    }

    /**
     * Set currenttime.
     *
     * @param DateTime|null $currenttime
     *
     * @return InfoSliceField
     */
    public function setCurrenttime(DateTime $currenttime = null)
    {
        $this->currenttime = $currenttime;

        return $this;
    }

    /**
     * Get currenttime.
     *
     * @return DateTime|null
     */
    public function getCurrenttime()
    {
        return $this->currenttime;
    }

    /**
     * Set moduser.
     *
     * @param string|null $moduser
     *
     * @return InfoSliceField
     */
    public function setModuser($moduser = null)
    {
        $this->moduser = $moduser;

        return $this;
    }

    /**
     * Get moduser.
     *
     * @return string|null
     */
    public function getModuser()
    {
        return $this->moduser;
    }

    /**
     * Set guid.
     *
     * @param string|null $guid
     *
     * @return InfoSliceField
     */
    public function setGuid($guid = null)
    {
        $this->guid = $guid;

        return $this;
    }

    /**
     * Get guid.
     *
     * @return string|null
     */
    public function getGuid()
    {
        return $this->guid;
    }

    /**
     * Add reportField
     *
     * @param InfoReportTabField $reportField
     *
     * @return InfoSliceField
     */
    public function addField(InfoReportTabField $reportField)
    {
        $reportField->setField($this);
        $this->reportFields->add($reportField);

        return $this;
    }

    /**
     * Remove reportField
     *
     * @param InfoReportTabField $reportField
     */
    public function removeField(InfoReportTabField $reportField)
    {
        $reportField->setField(null);
        $this->reportFields->removeElement($reportField);
    }

    /**
     * Get reportFields
     *
     * @return Collection
     */
    public function getFields()
    {
        return $this->reportFields;
    }
}
