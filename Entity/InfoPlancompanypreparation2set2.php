<?php

namespace TeamSoft\CrmRepositoryBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * InfoPlancompanypreparation2set2
 *
 * @ORM\Table(name="info_plancompanypreparation2set2")
 * @ORM\Entity()
 */
class InfoPlancompanypreparation2set2 implements ServiceFieldInterface
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var guid
     *
     * @ORM\Column(name="guid", type="guid", nullable=true)
     */
    private $guid;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="currenttime", type="datetime", nullable=true)
     */
    private $currenttime;

    /**
     * @var string
     *
     * @ORM\Column(name="moduser", type="string", length=16, nullable=true)
     */
    private $moduser;

    /**
     * @var string
     *
     * @ORM\Column(name="field", type="string", nullable=true)
     */
    private $field;

    /**
     * @var \InfoPlan
     *
     * @ORM\ManyToOne(targetEntity="InfoPlan")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="plan_id", referencedColumnName="id")
     * })
     */
    private $plan;

    /**
     * @var \InfoPlandt
     *
     * @ORM\ManyToOne(targetEntity="InfoPlandt")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="plandt_id", referencedColumnName="id")
     * })
     */
    private $planDt;

    /**
     * @var \InfoPreparation
     *
     * @ORM\ManyToOne(targetEntity="InfoPreparation")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="prep_id", referencedColumnName="id")
     * })
     */
    private $prep;



    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set guid
     *
     * @param guid $guid
     *
     * @return InfoPlancompanypreparation2set2
     */
    public function setGuid($guid)
    {
        $this->guid = $guid;
    
        return $this;
    }

    /**
     * Get guid
     *
     * @return guid
     */
    public function getGuid()
    {
        return $this->guid;
    }

    /**
     * Set currenttime
     *
     * @param \DateTime $currenttime
     *
     * @return InfoPlancompanypreparation2set2
     */
    public function setCurrenttime(\DateTime $currenttime)
    {
        $this->currenttime = $currenttime;
    
        return $this;
    }

    /**
     * Get currenttime
     *
     * @return \DateTime
     */
    public function getCurrenttime()
    {
        return $this->currenttime;
    }

    /**
     * Set moduser
     *
     * @param string $moduser
     *
     * @return InfoPlancompanypreparation2set2
     */
    public function setModuser($moduser)
    {
        $this->moduser = $moduser;
    
        return $this;
    }

    /**
     * Get moduser
     *
     * @return string
     */
    public function getModuser()
    {
        return $this->moduser;
    }

    /**
     * Set field
     *
     * @param string $field
     *
     * @return InfoPlancompanypreparation2set2
     */
    public function setField($field)
    {
        $this->field = $field;
    
        return $this;
    }

    /**
     * Get field
     *
     * @return string
     */
    public function getField()
    {
        return $this->field;
    }

    /**
     * Set plan
     *
     * @param InfoPlan $plan
     *
     * @return InfoPlancompanypreparation2set2
     */
    public function setPlan(InfoPlan $plan = null)
    {
        $this->plan = $plan;
    
        return $this;
    }

    /**
     * Get plan
     *
     * @return InfoPlan
     */
    public function getPlan()
    {
        return $this->plan;
    }

    /**
     * Set planDt
     *
     * @param InfoPlandt $planDt
     *
     * @return InfoPlancompanypreparation2set2
     */
    public function setPlanDt(InfoPlanDt $planDt = null)
    {
        $this->planDt = $planDt;

        return $this;
    }

    /**
     * Get planDt
     *
     * @return InfoPlandt
     */
    public function getPlanDt()
    {
        return $this->planDt;
    }

    /**
     * Set prep
     *
     * @param InfoPreparation $prep
     *
     * @return InfoPlancompanypreparation2set2
     */
    public function setPrep(InfoPreparation $prep = null)
    {
        $this->prep = $prep;
    
        return $this;
    }

    /**
     * Get prep
     *
     * @return InfoPreparation
     */
    public function getPrep()
    {
        return $this->prep;
    }

    public function getPreparationId ()
    {
        return $this->getPrep() ? $this->getPrep()->getId() : null;
    }

    public function getPlanId()
    {
        return $this->getPlan() ? $this->getPlan()->getId() : null;
    }

    public function getPlanDtId()
    {
        return $this->getPlanDt() ? $this->getPlanDt()->getId() : null;
    }
}
