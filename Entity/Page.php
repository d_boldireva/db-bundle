<?php

namespace TeamSoft\CrmRepositoryBundle\Entity;

class Page
{
    const COMPANY = 1;
    const CONTACT = 2;
    const TASK = 4;
    const ACTION = 8;
    const TARGETING_GRID = 100;
    const COMPANY_WEB = 101;
    const CONTACT_WEB = 102;
    const TASK_WEB = 104;
    const ACTION_WEB = 108;
    const GRANUAL_PLAN_SUMMARY_GRID = 109;
    const GRANUAL_PLAN_WORK_GRID = 110;
    const UNSUBSCRIBE = 301;
}
