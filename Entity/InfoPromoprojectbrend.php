<?php

namespace TeamSoft\CrmRepositoryBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * InfoPromoprojectbrend
 *
 * @ORM\Table(name="info_promoprojectbrend")
 * @ORM\Entity
 */
class InfoPromoprojectbrend implements ServiceFieldInterface
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="currenttime", type="datetime", nullable=true)
     */
    private $currenttime;

    /**
     * @var string|null
     *
     * @ORM\Column(name="moduser", type="string", length=16, nullable=true)
     */
    private $moduser;

    /**
     * @var string|null
     *
     * @ORM\Column(name="guid", type="guid", nullable=true)
     */
    private $guid;

    /**
     * @var int|null
     *
     * @ORM\Column(name="cnt", type="integer", nullable=true)
     */
    private $cnt;

    /**
     * @var InfoPromoproject
     *
     * @ORM\ManyToOne(targetEntity="InfoPromoproject")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="promoproject_id", referencedColumnName="id")
     * })
     */
    private $promoproject;

    /**
     * @var InfoPreparationbrend
     *
     * @ORM\ManyToOne(targetEntity="InfoPreparationbrend")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="brend_id", referencedColumnName="id")
     * })
     */
    private $brend;

    /**
     * @var InfoPreparation
     *
     * @ORM\ManyToOne(targetEntity="InfoPreparation")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="prep_id", referencedColumnName="id")
     * })
     */
    private $prep;

    /**
     * @var InfoPrepline
     *
     * @ORM\ManyToOne(targetEntity="InfoPrepline")
     * @ORM\JoinColumn(name="prepline_id", referencedColumnName="id")
     */
    private $prepline;



    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set currenttime.
     *
     * @param \DateTime|null $currenttime
     *
     * @return InfoPromoprojectbrend
     */
    public function setCurrenttime(\DateTime $currenttime = null)
    {
        $this->currenttime = $currenttime;
    
        return $this;
    }

    /**
     * Get currenttime.
     *
     * @return \DateTime|null
     */
    public function getCurrenttime()
    {
        return $this->currenttime;
    }

    /**
     * Set moduser.
     *
     * @param string|null $moduser
     *
     * @return InfoPromoprojectbrend
     */
    public function setModuser($moduser = null)
    {
        $this->moduser = $moduser;
    
        return $this;
    }

    /**
     * Get moduser.
     *
     * @return string|null
     */
    public function getModuser()
    {
        return $this->moduser;
    }

    /**
     * Set guid.
     *
     * @param string|null $guid
     *
     * @return InfoPromoprojectbrend
     */
    public function setGuid($guid = null)
    {
        $this->guid = $guid;
    
        return $this;
    }

    /**
     * Get guid.
     *
     * @return string|null
     */
    public function getGuid()
    {
        return $this->guid;
    }

    /**
     * Set cnt.
     *
     * @param int|null $cnt
     *
     * @return InfoPromoprojectbrend
     */
    public function setCnt($cnt = null)
    {
        $this->cnt = $cnt;
    
        return $this;
    }

    /**
     * Get cnt.
     *
     * @return int|null
     */
    public function getCnt()
    {
        return $this->cnt;
    }

    /**
     * @return int|null
     */
    public function getPromoprojectId(){
        return $this->promoproject ? $this->promoproject->getId() : null;
    }

    /**
     * Set promoproject.
     *
     * @param \TeamSoft\CrmRepositoryBundle\Entity\InfoPromoproject|null $promoproject
     *
     * @return InfoPromoprojectbrend
     */
    public function setPromoproject(\TeamSoft\CrmRepositoryBundle\Entity\InfoPromoproject $promoproject = null)
    {
        $this->promoproject = $promoproject;
    
        return $this;
    }

    /**
     * Get promoproject.
     *
     * @return \TeamSoft\CrmRepositoryBundle\Entity\InfoPromoproject|null
     */
    public function getPromoproject()
    {
        return $this->promoproject;
    }

    /**
     * Set brend.
     *
     * @param \TeamSoft\CrmRepositoryBundle\Entity\InfoPreparationbrend|null $brend
     *
     * @return InfoPromoprojectbrend
     */
    public function setBrend(\TeamSoft\CrmRepositoryBundle\Entity\InfoPreparationbrend $brend = null)
    {
        $this->brend = $brend;
    
        return $this;
    }

    /**
     * Get brend.
     *
     * @return \TeamSoft\CrmRepositoryBundle\Entity\InfoPreparationbrend|null
     */
    public function getBrend()
    {
        return $this->brend;
    }

    /**
     * @return int|null
     */
    public function getPrepId(){
        return $this->prep ? $this->prep->getId() : null;
    }

    /**
     * Set prep.
     *
     * @param \TeamSoft\CrmRepositoryBundle\Entity\InfoPreparation|null $prep
     *
     * @return InfoPromoprojectbrend
     */
    public function setPrep(\TeamSoft\CrmRepositoryBundle\Entity\InfoPreparation $prep = null)
    {
        $this->prep = $prep;
    
        return $this;
    }

    /**
     * Get prep.
     *
     * @return \TeamSoft\CrmRepositoryBundle\Entity\InfoPreparation|null
     */
    public function getPrep()
    {
        return $this->prep;
    }

    public function getBrandId()
    {
        return $this->getBrend() ? $this->getBrend()->getId() : null;
    }

    /**
     * @return int|null
     */
    public function getPreplineId()
    {
        return $this->prepline ? $this->prepline->getId() : null;
    }

    /**
     * @param InfoPrepline|null $prepline
     * @return $this
     */
    public function setPrepline(\TeamSoft\CrmRepositoryBundle\Entity\InfoPrepline $prepline = null)
    {
        $this->prepline = $prepline;

        return $this;
    }

    /**
     * @return InfoPrepline
     */
    public function getPrepline(){
        return $this->prepline;
    }
}
