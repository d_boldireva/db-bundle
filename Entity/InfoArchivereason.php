<?php

namespace TeamSoft\CrmRepositoryBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * InfoArchivereason
 *
 * @ORM\Table(name="info_archivereason", indexes={@ORM\Index(name="ix_info_archivereason_guid", columns={"guid"})})
 * @ORM\Entity
 */
class InfoArchivereason
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="type", type="integer", nullable=true)
     */
    private $type;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255, nullable=true)
     */
    private $name;

    /**
     * @var integer
     *
     * @ORM\Column(name="iswork", type="integer", nullable=true)
     */
    private $iswork;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="currenttime", type="datetime", nullable=true)
     */
    private $currenttime;

    /**
     * @var guid
     *
     * @ORM\Column(name="guid", type="guid", nullable=true)
     */
    private $guid;

    /**
     * @var string
     *
     * @ORM\Column(name="moduser", type="string", length=16, nullable=true)
     */
    private $moduser;

    /**
     * @var integer
     *
     * @ORM\Column(name="morionid", type="integer", nullable=true)
     */
    private $morionid;



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set type
     *
     * @param integer $type
     * @return InfoArchivereason
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type
     *
     * @return integer 
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return InfoArchivereason
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set iswork
     *
     * @param integer $iswork
     * @return InfoArchivereason
     */
    public function setIswork($iswork)
    {
        $this->iswork = $iswork;

        return $this;
    }

    /**
     * Get iswork
     *
     * @return integer 
     */
    public function getIswork()
    {
        return $this->iswork;
    }

    /**
     * Set currenttime
     *
     * @param \DateTime $currenttime
     * @return InfoArchivereason
     */
    public function setCurrenttime($currenttime)
    {
        $this->currenttime = $currenttime;

        return $this;
    }

    /**
     * Get currenttime
     *
     * @return \DateTime 
     */
    public function getCurrenttime()
    {
        return $this->currenttime;
    }

    /**
     * Set guid
     *
     * @param guid $guid
     * @return InfoArchivereason
     */
    public function setGuid($guid)
    {
        $this->guid = $guid;

        return $this;
    }

    /**
     * Get guid
     *
     * @return guid 
     */
    public function getGuid()
    {
        return $this->guid;
    }

    /**
     * Set moduser
     *
     * @param string $moduser
     * @return InfoArchivereason
     */
    public function setModuser($moduser)
    {
        $this->moduser = $moduser;

        return $this;
    }

    /**
     * Get moduser
     *
     * @return string 
     */
    public function getModuser()
    {
        return $this->moduser;
    }

    /**
     * Set morionid
     *
     * @param integer $morionid
     * @return InfoArchivereason
     */
    public function setMorionid($morionid)
    {
        $this->morionid = $morionid;

        return $this;
    }

    /**
     * Get morionid
     *
     * @return integer 
     */
    public function getMorionid()
    {
        return $this->morionid;
    }
}
