<?php

namespace TeamSoft\CrmRepositoryBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * InfoCountry
 *
 * @ORM\Table(name="info_country", indexes={@ORM\Index(name="ix_info_country_guid", columns={"guid"})})
 * @ORM\Entity
 */
class InfoCountry
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="Name", type="string", length=255, nullable=false)
     */
    private $name;

    /**
     * @var guid
     *
     * @ORM\Column(name="guid", type="guid", nullable=true)
     */
    private $guid;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="currenttime", type="datetime", nullable=true)
     */
    private $currenttime;

    /**
     * @var string
     *
     * @ORM\Column(name="moduser", type="string", length=16, nullable=true)
     */
    private $moduser;

    /**
     * @var guid
     *
     * @ORM\Column(name="guid2", type="guid", nullable=true)
     */
    private $guid2;

    /**
     * @var string
     *
     * @ORM\Column(name="name2", type="string", length=255, nullable=true)
     */
    private $name2;

    /**
     * @var string
     *
     * @ORM\Column(name="code", type="string", length=255, nullable=true)
     */
    private $code;

    /**
     * @var string
     *
     * @ORM\Column(name="phonecode", type="string", length=255, nullable=false)
     */
    private $phonecode;

    /**
     * @var string
     *
     * @ORM\Column(name="pharma_url", type="string", length=255, nullable=true)
     */
    private $pharmaUrl;

    /**
     * @var string
     *
     * @ORM\Column(name="phonemask", type="string", length=255, nullable=true)
     */
    private $phonemask;

    /**
     * @var string
     *
     * @ORM\Column(name="crm_phonemask", type="string", length=255, nullable=true)
     */
    private $crmPhonemask;

    /**
     * @var float
     *
     * @ORM\Column(name="gps_la", type="float", nullable=true)
     */
    private $gpsLa;

    /**
     * @var float
     *
     * @ORM\Column(name="gps_lo", type="float", nullable=true)
     */
    private $gpsLo;

    /**
     * @var integer
     *
     * @ORM\Column(name="zoom_level", type="integer", nullable=true)
     */
    private $zoomLevel;

    /**
     * @var integer
     *
     * @ORM\Column(name="min_zoom", type="integer", nullable=true)
     */
    private $minZoom;

    /**
     * @var integer
     *
     * @ORM\Column(name="max_zoom", type="integer", nullable=true)
     */
    private $maxZoom;

    /**
     * @var InfoLanguage
     *
     * @ORM\ManyToOne(targetEntity="InfoLanguage")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="language_id", referencedColumnName="id")
     * })
     */
    private $language;

    /**
     * @var InfoRegion[]|Collection
     * @ORM\OneToMany(targetEntity="InfoRegion", mappedBy="country")
     */
    private $regions;

    /**
     * @var InfoCurrency|null
     * @ORM\OneToOne(targetEntity="InfoCurrency", inversedBy="country")
     * @ORM\JoinColumn(name="currency_id", referencedColumnName="id")
     */
    private $currency;

    /**
     * @var string
     * @ORM\Column(name="colorhex", type="string", nullable=true)
     */
    private $colorhex;

    public function __construct()
    {
        $this->regions = new ArrayCollection();
    }

    /**
     * Get code
     *
     * @return string
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * Set code
     *
     * @param string $code
     * @return InfoCountry
     */
    public function setCode($code)
    {
        $this->code = $code;
        return $this;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return InfoCountry
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set guid
     *
     * @param guid $guid
     * @return InfoCountry
     */
    public function setGuid($guid)
    {
        $this->guid = $guid;

        return $this;
    }

    /**
     * Get guid
     *
     * @return guid
     */
    public function getGuid()
    {
        return $this->guid;
    }

    /**
     * Set currenttime
     *
     * @param \DateTime $currenttime
     * @return InfoCountry
     */
    public function setCurrenttime($currenttime)
    {
        $this->currenttime = $currenttime;

        return $this;
    }

    /**
     * Get currenttime
     *
     * @return \DateTime
     */
    public function getCurrenttime()
    {
        return $this->currenttime;
    }

    /**
     * Set moduser
     *
     * @param string $moduser
     * @return InfoCountry
     */
    public function setModuser($moduser)
    {
        $this->moduser = $moduser;

        return $this;
    }

    /**
     * Get moduser
     *
     * @return string
     */
    public function getModuser()
    {
        return $this->moduser;
    }

    /**
     * Set guid2
     *
     * @param guid $guid2
     * @return InfoCountry
     */
    public function setGuid2($guid2)
    {
        $this->guid2 = $guid2;

        return $this;
    }

    /**
     * Get guid2
     *
     * @return guid
     */
    public function getGuid2()
    {
        return $this->guid2;
    }

    /**
     * Set name2
     *
     * @param string $name2
     * @return InfoCountry
     */
    public function setName2($name2)
    {
        $this->name2 = $name2;

        return $this;
    }

    /**
     * Get name2
     *
     * @return string
     */
    public function getName2()
    {
        return $this->name2;
    }

    /**
     * Set phonecode
     *
     * @param string $phonecode
     * @return InfoCountry
     */
    public function setPhonecode($phonecode)
    {
        $this->phonecode = $phonecode;

        return $this;
    }

    /**
     * Get phonecode
     *
     * @return string
     */
    public function getPhonecode()
    {
        return $this->phonecode;
    }

    /**
     * Set pharmaUrl
     *
     * @param string $pharmaUrl
     * @return InfoCountry
     */
    public function setPharmaUrl($pharmaUrl)
    {
        $this->pharmaUrl = $pharmaUrl;

        return $this;
    }

    /**
     * Get pharmaUrl
     *
     * @return string
     */
    public function getPharmaUrl()
    {
        return $this->pharmaUrl;
    }

    /**
     * Set phonemask
     *
     * @param string $phonemask
     * @return InfoCountry
     */
    public function setPhonemask($phonemask)
    {
        $this->phonemask = $phonemask;

        return $this;
    }

    /**
     * Get phonemask
     *
     * @return string
     */
    public function getPhonemask()
    {
        return $this->phonemask;
    }

    /**
     * Set crmPhonemask
     *
     * @param string $crmPhonemask
     * @return InfoCountry
     */
    public function setCrmPhonemask(?string $crmPhonemask): self
    {
        $this->crmPhonemask = $crmPhonemask;

        return $this;
    }

    /**
     * Get crmPhonemask
     *
     * @return string
     */
    public function getCrmPhonemask(): ?string
    {
        return $this->crmPhonemask;
    }

    /**
     * Set gpsLa
     *
     * @param float $gpsLa
     * @return InfoCountry
     */
    public function setGpsLa(?float $gpsLa): self
    {
        $this->gpsLa = $gpsLa;

        return $this;
    }

    /**
     * Get gpsLa
     *
     * @return float|null
     */
    public function getGpsLa(): ?float
    {
        return $this->gpsLa;
    }

    /**
     * Set gpsLo
     *
     * @param float $gpsLo
     * @return InfoCountry
     */
    public function setGpsLo(?float $gpsLo): self
    {
        $this->gpsLo = $gpsLo;

        return $this;
    }

    /**
     * Get gpsLo
     *
     * @return float|null
     */
    public function getGpsLo(): ?float
    {
        return $this->gpsLo;
    }

    /**
     * Set zoomLevel
     *
     * @param int $zoomLevel
     * @return InfoCountry
     */
    public function setZoomLevel(?int $zoomLevel): self
    {
        $this->zoomLevel = $zoomLevel;

        return $this;
    }

    /**
     * Get zoomLevel
     *
     * @return int|null
     */
    public function getZoomLevel(): ?int
    {
        return $this->zoomLevel;
    }

    /**
     * Set minZoom
     *
     * @param int $minZoom
     * @return InfoCountry
     */
    public function setMinZoom(?int $minZoom): self
    {
        $this->minZoom = $minZoom;

        return $this;
    }

    /**
     * Get minZoom
     *
     * @return int|null
     */
    public function getMinZoom(): ?int
    {
        return $this->minZoom;
    }

    /**
     * Set maxZoom
     *
     * @param int $maxZoom
     * @return InfoCountry
     */
    public function setMaxZoom(?int $maxZoom): self
    {
        $this->maxZoom = $maxZoom;

        return $this;
    }

    /**
     * Get maxZoom
     *
     * @return int|null
     */
    public function getMaxZoom(): ?int
    {
        return $this->maxZoom;
    }

    /**
     * @param $colorhex
     * @return InfoCountry
     */
    public function setColorhex($colorhex)
    {
        $this->colorhex = $colorhex;

        return $this;
    }

    /**
     * Get colorhex
     *
     * @return string
     */
    public function getColorhex()
    {
        return $this->colorhex;
    }

    /**
     * Set language
     *
     * @param InfoLanguage|null $content
     * @return InfoCountry
     */
    public function setLanguage(InfoLanguage $language = null)
    {
        $this->language = $language;

        return $this;
    }

    /**
     * Get language
     *
     * @return InfoLanguage
     */
    public function getLanguage()
    {
        return $this->language;
    }

    public function getLanguageId(): ?int
    {
        return $this->getLanguage() ? $this->getLanguage()->getId() : null;
    }

    /**
     * @return InfoRegion[]|Collection
     */
    public function getRegions(): Collection
    {
        return $this->regions;
    }

    /**
     * @param InfoRegion $region
     * @return $this
     */
    public function addRegion(InfoRegion $region): self
    {
        $region->setCountry($this);

        if (!$this->regions->contains($region)) {
            $this->regions->add($region);
        }

        return $this;
    }

    /**
     * @param InfoRegion $region
     */
    public function removeRegion(InfoRegion $region): void
    {
        $this->regions->removeElement($region);
    }

    /**
     * @return InfoCurrency|null
     */
    public function getCurrency(): ?InfoCurrency
    {
        return $this->currency;
    }

    /**
     * @param InfoCurrency|null $currency
     * @return self
     */
    public function setCurrency(?InfoCurrency $currency): self
    {
        $this->currency = $currency;
        return $this;
    }
}
