<?php

namespace TeamSoft\CrmRepositoryBundle\Entity;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\User\UserInterface;

interface SecureEntityInterface
{
    /**
     * @return InfoRole[]
     */
    public function getRoles();
}
