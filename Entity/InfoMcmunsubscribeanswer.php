<?php

namespace TeamSoft\CrmRepositoryBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * InfoMcmunsubscribeanswer
 *
 * @ORM\Table(name="Info_mcmunsubscribeanswer")
 * @ORM\Entity
 */
class InfoMcmunsubscribeanswer implements ServiceFieldInterface
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var InfoContact $contact
     * @ORM\ManyToOne(targetEntity="InfoContact")
     * @ORM\JoinColumn(name="contact_id", referencedColumnName="id")
     */
    private $contact;

    /**
     * @var InfoSurveyQuestion $surveyquestion
     * @ORM\ManyToOne(targetEntity="InfoSurveyQuestion")
     * @ORM\JoinColumn(name="surveyquestion_id", referencedColumnName="id")
     */
    private $surveyQuestion;

    /**
     * @var InfoSurveyQuestionAnswer $surveyquestionanswer
     * @ORM\ManyToOne(targetEntity="InfoSurveyQuestionAnswer")
     * @ORM\JoinColumn(name="surveyquestionanswer_id", referencedColumnName="id")
     */
    private $surveyQuestionAnswer;

    /**
     * @var string|null
     *
     * @ORM\Column(name="answer", type="string", nullable=true)
     */
    private $answer;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="currenttime", type="datetime", nullable=true)
     */
    private $currenttime;

    /**
     * @var string
     *
     * @ORM\Column(name="moduser", type="string", length=16, nullable=true)
     */
    private $moduser;

    /**
     * @var string
     *
     * @ORM\Column(name="guid", type="guid", nullable=true)
     */
    private $guid;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Get contact
     *
     * @return null|InfoContact
     */
    public function getContact(): ?InfoContact
    {
        return $this->contact;
    }

    /**
     * Set contact
     *
     * @param null|InfoContact $contact
     *
     * @return InfoMcmunsubscribeanswer
     */
    public function setContact(?InfoContact $contact): self
    {
        $this->contact = $contact;

        return $this;
    }

    /**
     * Get surveyQuestion
     *
     * @return null|InfoSurveyQuestion
     */
    public function getSurveyQuestion(): ?InfoSurveyQuestion
    {
        return $this->surveyQuestion;
    }

    /**
     * Set surveyQuestion
     *
     * @param null|InfoSurveyQuestion $surveyQuestion
     *
     * @return InfoMcmunsubscribeanswer
     */
    public function setSurveyQuestion(?InfoSurveyQuestion $surveyQuestion): self
    {
        $this->surveyQuestion = $surveyQuestion;

        return $this;
    }

    /**
     * Get surveyQuestionAnswer
     *
     * @return null|InfoSurveyQuestionAnswer
     */
    public function getSurveyQuestionAnswer(): ?InfoSurveyQuestionAnswer
    {
        return $this->surveyQuestionAnswer;
    }

    /**
     * Set surveyQuestionAnswer
     *
     * @param null|InfoSurveyQuestionAnswer $surveyQuestionAnswer
     *
     * @return InfoMcmunsubscribeanswer
     */
    public function setSurveyQuestionAnswer(?InfoSurveyQuestionAnswer $surveyQuestionAnswer): self
    {
        $this->surveyQuestionAnswer = $surveyQuestionAnswer;

        return $this;
    }

    /**
     * Set answer
     *
     * @param string|null $answer
     *
     * @return InfoMcmunsubscribeanswer
     */
    public function setAnswer(?string $answer): self
    {
        $this->answer = $answer;

        return $this;
    }

    /**
     * Get answer
     *
     * @return string|null
     */
    public function getAnswer(): ?string
    {
        return $this->answer;
    }

    /**
     * Set currenttime
     *
     * @param null|\DateTime $currenttime
     *
     * @return InfoMcmunsubscribeanswer
     */
    public function setCurrenttime(?\DateTime $currenttime): self
    {
        $this->currenttime = $currenttime;

        return $this;
    }

    /**
     * Get currenttime
     *
     * @return null|\DateTime
     */
    public function getCurrenttime(): ?\DateTime
    {
        return $this->currenttime;
    }

    /**
     * Set moduser
     *
     * @param null|string $moduser
     *
     * @return InfoMcmunsubscribeanswer
     */
    public function setModuser($moduser): self
    {
        $this->moduser = $moduser;

        return $this;
    }

    /**
     * Get moduser
     *
     * @return string
     */
    public function getModuser()
    {
        return $this->moduser;
    }

    /**
     * Set guid
     *
     * @param string $guid
     *
     * @return InfoMcmunsubscribeanswer
     */
    public function setGuid($guid): self
    {
        $this->guid = $guid;

        return $this;
    }

    /**
     * Get guid
     *
     * @return string
     */
    public function getGuid()
    {
        return $this->guid;
    }
}
