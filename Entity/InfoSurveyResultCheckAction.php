<?php


namespace TeamSoft\CrmRepositoryBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * InfoSurveyResultCheckAction
 *
 * @ORM\Table(name="info_surveyresultcheckaction")
 * @ORM\Entity
 */
class InfoSurveyResultCheckAction implements ServiceFieldInterface
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="guid", type="guid", nullable=true)
     */
    private $guid;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="currenttime", type="datetime", nullable=true)
     */
    private $currenttime;

    /**
     * @var string
     *
     * @ORM\Column(name="moduser", type="string", length=16, nullable=true)
     */
    private $moduser;

    /**
     * @var InfoSurveyResultAction
     *
     * @ORM\ManyToOne(targetEntity="InfoSurveyResultAction")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="result_id", referencedColumnName="id")
     * })
     */
    private $result;

    /**
     * @var InfoSurveyQuestionAnswer
     *
     * @ORM\ManyToOne(targetEntity="InfoSurveyQuestionAnswer")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="answer_id", referencedColumnName="id")
     * })
     */
    private $answer;

    public function __clone()
    {
        $this->id = null;
        $this->guid = null;
        $this->currenttime = null;
        $this->moduser = null;
    }

    /**
     * Get id.
     *
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * Set guid.
     *
     * @param string|null $guid
     *
     * @return InfoSurveyResultCheckAction
     */
    public function setGuid($guid = null): InfoSurveyResultCheckAction
    {
        $this->guid = $guid;

        return $this;
    }

    /**
     * Get guid.
     *
     * @return string|null
     */
    public function getGuid(): ?string
    {
        return $this->guid;
    }

    /**
     * Set currenttime.
     *
     * @param \DateTime|null $currenttime
     *
     * @return InfoSurveyResultCheckAction
     */
    public function setCurrenttime(\DateTime $currenttime = null): InfoSurveyResultCheckAction
    {
        $this->currenttime = $currenttime;

        return $this;
    }

    /**
     * Get currenttime.
     *
     * @return \DateTime|null
     */
    public function getCurrenttime(): ?\DateTime
    {
        return $this->currenttime;
    }

    /**
     * Set moduser.
     *
     * @param string|null $moduser
     *
     * @return InfoSurveyResultCheckAction
     */
    public function setModuser($moduser = null): InfoSurveyResultCheckAction
    {
        $this->moduser = $moduser;

        return $this;
    }

    /**
     * Get moduser.
     *
     * @return string|null
     */
    public function getModuser(): ?string
    {
        return $this->moduser;
    }

    /**
     * Set result.
     *
     * @param \TeamSoft\CrmRepositoryBundle\Entity\InfoSurveyResult|null $result
     *
     * @return InfoSurveyResultCheckAction
     */
    public function setResult(\TeamSoft\CrmRepositoryBundle\Entity\InfoSurveyResultAction $result = null): InfoSurveyResultCheckAction
    {
        $this->result = $result;

        return $this;
    }

    /**
     * Get result.
     *
     * @return \TeamSoft\CrmRepositoryBundle\Entity\InfoSurveyResultAction|null
     */
    public function getResult(): ?InfoSurveyResultAction
    {
        return $this->result;
    }

    /**
     * Set answer.
     *
     * @param \TeamSoft\CrmRepositoryBundle\Entity\InfoSurveyQuestionAnswer|null $answer
     *
     * @return InfoSurveyResultCheckAction
     */
    public function setAnswer(\TeamSoft\CrmRepositoryBundle\Entity\InfoSurveyQuestionAnswer $answer = null): InfoSurveyResultCheckAction
    {
        $this->answer = $answer;

        return $this;
    }

    /**
     * Get answer.
     *
     * @return \TeamSoft\CrmRepositoryBundle\Entity\InfoSurveyQuestionAnswer|null
     */
    public function getAnswer(): ?InfoSurveyQuestionAnswer
    {
        return $this->answer;
    }

    public function getAnswerId(): ?int
    {
        return $this->answer ? $this->answer->getId() : null;
    }
}