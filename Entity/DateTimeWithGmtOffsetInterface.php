<?php
namespace TeamSoft\CrmRepositoryBundle\Entity;

interface DateTimeWithGmtOffsetInterface
{

    /**
     * @return array
     */
    public function getDateTimeWithGmtOffsetPropertyNameList();

    /**
     * @return integer
     */
    public function getGmtOffset();

    /**
     * @param int $gmtOffset
     * @return mixed
     */
    public function setGmtOffset(int $gmtOffset);
}
