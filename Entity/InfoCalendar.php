<?php declare(strict_types=1);

namespace TeamSoft\CrmRepositoryBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Class InfoCalendar
 * @package TeamSoft\CrmRepositoryBundle\Entity
 * @ORM\Table(name="info_calendar")
 * @ORM\Entity(repositoryClass="TeamSoft\CrmRepositoryBundle\Repository\InfoCalendar")
 */
class InfoCalendar implements ServiceFieldInterface
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id()
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="currenttime", type="datetime", nullable=true)
     */
    private $currenttime;

    /**
     * @var string|null
     *
     * @ORM\Column(name="moduser", type="string", length=16, nullable=true)
     */
    private $moduser;

    /**
     * @var string
     *
     * @ORM\Column(name="guid", type="guid", nullable=true)
     */
    private $guid;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="datefrom", type="datetime")
     */
    private $dateFrom;


    /**
     * @var integer|null
     *
     * @ORM\Column(name="workday", type="integer", nullable=true)
     */
    private $workday;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param \DateTime $currenttime
     * @return $this|ServiceFieldInterface
     */
    public function setCurrenttime(\DateTime $currenttime)
    {
        $this->currenttime = $currenttime;
        return $this;
    }

    /**
     * @return \DateTime|string|null
     */
    public function getCurrenttime()
    {
        return $this->currenttime;
    }

    /**
     * @param string $moduser
     * @return $this|ServiceFieldInterface
     */
    public function setModuser($moduser)
    {
        $this->moduser = $moduser;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getModuser()
    {
        return $this->moduser;
    }

    /**
     * @param int $guid
     * @return $this|ServiceFieldInterface
     */
    public function setGuid($guid)
    {
        $this->guid = $guid;
        return $this;
    }

    /**
     * @return int|string
     */
    public function getGuid()
    {
        return $this->guid;
    }

    /**
     * @return \DateTime|null
     */
    public function getDateFrom(): ?\DateTime
    {
        return $this->dateFrom;
    }

    /**
     * @param \DateTime|null $dateFrom
     * @return self
     */
    public function setDateFrom(?\DateTime $dateFrom): self
    {
        $this->dateFrom = $dateFrom;
        return $this;
    }

    /**
     * @return int|null
     */
    public function getWorkday(): ?int
    {
        return $this->workday;
    }

    /**
     * @param int|null $workday
     * @return self
     */
    public function setWorkday(?int $workday): self
    {
        $this->workday = $workday;
        return $this;
    }
}
