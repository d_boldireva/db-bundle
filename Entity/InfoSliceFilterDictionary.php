<?php

namespace TeamSoft\CrmRepositoryBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * InfoSliceFilterDictionary
 *
 * @ORM\Table(name="info_slicefilterdictionary")
 * @ORM\Entity
 */
class InfoSliceFilterDictionary implements ServiceFieldInterface
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=100, nullable=false)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="is_required", type="integer", length=1, nullable=false)
     */
    private $isRequired;

    /**
     * @var int
     *
     * @ORM\Column(name="field_data_type", type="integer", nullable=true)
     */
    private $fieldDataType;

    /**
     * @var string
     *
     * @ORM\Column(name="guid", type="guid", nullable=true)
     */
    private $guid;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="currenttime", type="datetime", nullable=true)
     */
    private $currenttime;

    /**
     * @var string
     *
     * @ORM\Column(name="moduser", type="string", length=16, nullable=true)
     */
    private $moduser;

    /**
     * @var string
     *
     * @ORM\Column(name="client_custom_where", type="string", length=255, nullable=true)
     */
    private $clientCustomWhere;

    /**
     * @var string
     *
     * @ORM\Column(name="server_custom_where", type="string", length=255, nullable=true)
     */
    private $serverCustomWhere;

    /**
     * @var InfoCustomDictionary
     *
     * @ORM\ManyToOne(targetEntity="InfoCustomdictionary")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="customdictionary_id", referencedColumnName="id")
     * })
     */
    private $customDictionary;

    /**
     * @var InfoSystemDictionary
     *
     * @ORM\ManyToOne(targetEntity="InfoSystemdictionary")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="systemdictionary_id", referencedColumnName="id")
     * })
     */
    private $systemDictionary;

    /**
     * One filter has many sliceFilters
     * @ORM\OneToMany(targetEntity="InfoSliceFilter", mappedBy="slice")
     * @ORM\JoinColumn(name="id", referencedColumnName="slice")
     */
    private $sliceFilters;

    public function __construct()
    {
        $this->sliceFilters = new ArrayCollection();
    }


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return InfoSliceFilterDictionary
     */
    public function setName($name)
    {
        $this->name = $name;
    
        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set isRequired
     *
     * @param string $isRequired
     *
     * @return InfoSliceFilterDictionary
     */
    public function setIsRequired($isRequired)
    {
        $this->isRequired = $isRequired;
    
        return $this;
    }

    /**
     * Get isRequired
     *
     * @return string
     */
    public function getIsRequired()
    {
        return $this->isRequired;
    }

    /**
     * Set fieldDataType
     *
     * @param int $fieldDataType
     *
     * @return InfoSliceFilterDictionary
     */
    public function setFieldDataType($fieldDataType)
    {
        $this->fieldDataType = $fieldDataType;
    
        return $this;
    }

    /**
     * Get fieldDataType
     *
     * @return int
     */
    public function getFieldDataType()
    {
        return $this->fieldDataType;
    }

    /**
     * Set guid
     *
     * @param string $guid
     *
     * @return InfoSliceFilterDictionary
     */
    public function setGuid($guid)
    {
        $this->guid = $guid;
    
        return $this;
    }

    /**
     * Get guid
     *
     * @return string
     */
    public function getGuid()
    {
        return $this->guid;
    }

    /**
     * Set currenttime
     *
     * @param \DateTime $currenttime
     *
     * @return InfoSliceFilterDictionary
     */
    public function setCurrenttime(\DateTime $currenttime)
    {
        $this->currenttime = $currenttime;
    
        return $this;
    }

    /**
     * Get currenttime
     *
     * @return \DateTime
     */
    public function getCurrenttime()
    {
        return $this->currenttime;
    }

    /**
     * Set moduser
     *
     * @param string $moduser
     *
     * @return InfoSliceFilterDictionary
     */
    public function setModuser($moduser)
    {
        $this->moduser = $moduser;
    
        return $this;
    }

    /**
     * Get moduser
     *
     * @return string
     */
    public function getModuser()
    {
        return $this->moduser;
    }

    /**
     * Set clientCustomWhere
     *
     * @param string $clientCustomWhere
     *
     * @return InfoSliceFilterDictionary
     */
    public function setClientCustomWhere($clientCustomWhere)
    {
        $this->clientCustomWhere = $clientCustomWhere;
    
        return $this;
    }

    /**
     * Get clientCustomWhere
     *
     * @return string
     */
    public function getClientCustomWhere()
    {
        return $this->clientCustomWhere;
    }

    /**
     * Set serverCustomWhere
     *
     * @param string $serverCustomWhere
     *
     * @return InfoSliceFilterDictionary
     */
    public function setServerCustomWhere($serverCustomWhere)
    {
        $this->serverCustomWhere = $serverCustomWhere;
    
        return $this;
    }

    /**
     * Get serverCustomWhere
     *
     * @return string
     */
    public function getServerCustomWhere()
    {
        return $this->serverCustomWhere;
    }

    /**
     * Set customDictionary
     *
     * @param InfoCustomDictionary $customDictionary
     *
     * @return InfoSliceFilterDictionary
     */
    public function setCustomDictionary(InfoCustomDictionary $customDictionary = null)
    {
        $this->customDictionary = $customDictionary;
    
        return $this;
    }

    /**
     * Get customDictionary
     *
     * @return InfoCustomDictionary
     */
    public function getCustomDictionary()
    {
        return $this->customDictionary;
    }

    /**
     * Set systemDictionary
     *
     * @param InfoSystemDictionary $systemDictionary
     *
     * @return InfoSliceFilterDictionary
     */
    public function setSystemDictionary(InfoSystemDictionary $systemDictionary = null)
    {
        $this->systemDictionary = $systemDictionary;
    
        return $this;
    }

    /**
     * Get systemDictionary
     *
     * @return InfoSystemDictionary
     */
    public function getSystemDictionary()
    {
        return $this->systemDictionary;
    }

    /**
     * Add sliceFilter
     *
     * @param InfoSliceFilter $sliceFilter
     *
     * @return InfoSliceFilterDictionary
     */
    public function addSliceFilter(InfoSliceFilter $sliceFilter)
    {
        $sliceFilter->setSlice($this);
        $this->sliceFilters->add($sliceFilter);

        return $this;
    }

    /**
     * Remove sliceFilter
     *
     * @param InfoSliceFilter $sliceFilter
     */
    public function removeSliceFilter(InfoSliceFilter $sliceFilter)
    {
        $sliceFilter->setSlice(null);
        $this->sliceFilters->removeElement($sliceFilter);
    }

    /**
     * Get sliceFilters
     *
     * @return Collection
     */
    public function getSliceFilters()
    {
        return $this->sliceFilters;
    }
}
