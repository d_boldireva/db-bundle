<?php

namespace TeamSoft\CrmRepositoryBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * InfoCompany
 *
 * @ORM\Table(name="info_company")
 * @ORM\Entity(repositoryClass="TeamSoft\CrmRepositoryBundle\Repository\Company\InfoCompanyRepository")
 * @ORM\EntityListeners({"TeamSoft\CrmRepositoryBundle\EventListener\InfoCompanyListener"})
 */
class InfoCompany implements ServiceFieldInterface
{
    /**
     *  Ожидает верификацию
     */
    public const STATUS_WAIT_VERIFICATION = 1;

    /**
     * Отправлено на верификацию
     */
    public const STATUS_NOT_VERIFICATION = 2;

    /**
     * Верифицировано
     */
    public const STATUS_VERIFIED = 3;

    /**
     * Отклонено
     */
    public const STATUS_REJECTED = 4;

    /**
     * Требует повторной верификации
     */
    public const STATUS_NEED_VERIFICATION_AGAIN = 5;

    /**
     * Не подлежит верификации
     */
    public const STATUS_NOT_SUBJECT_VERIFICATION = 6;

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="Name", type="string", length=255, nullable=true)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="Code", type="string", length=20, nullable=true)
     */
    private $code;

    /**
     * @var string
     *
     * @ORM\Column(name="Description", type="varchar_or_n_varchar_image_or_string", nullable=true)
     */
    private $description;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="Modified", type="datetime", nullable=true)
     */
    private $modified;

    /**
     * @var string
     *
     * @ORM\Column(name="phone1", type="string", length=255, nullable=true)
     */
    private $phone1;

    /**
     * @var string
     *
     * @ORM\Column(name="phone2", type="string", length=255, nullable=true)
     */
    private $phone2;

    /**
     * @var string
     *
     * @ORM\Column(name="practiceno", type="string", length=255, nullable=true)
     */
    private $practiceno;

    /**
     * @var string
     *
     * @ORM\Column(name="eaddr", type="string", length=255, nullable=true)
     */
    private $eaddr;

    /**
     * @var string
     *
     * @ORM\Column(name="okrug", type="string", length=255, nullable=true)
     */
    private $okrug;

    /**
     * @var string
     *
     * @ORM\Column(name="PostCode", type="string", length=20, nullable=true)
     */
    private $postcode;

    /**
     * @var string
     *
     * @ORM\Column(name="Street", type="string", length=255, nullable=true)
     */
    private $street;

    /**
     * @var string
     *
     * @ORM\Column(name="Building", type="string", length=20, nullable=true)
     */
    private $building;

    /**
     * @var string
     *
     * @ORM\Column(name="Apartment", type="string", length=20, nullable=true)
     */
    private $apartment;

    /**
     * @var string
     *
     * @ORM\Column(name="waddr", type="string", length=255, nullable=true)
     */
    private $waddr;

    /**
     * @var string
     *
     * @ORM\Column(name="StreetUkr", type="string", length=255, nullable=true)
     */
    private $streetukr;

    /**
     * @var string
     *
     * @ORM\Column(name="NameUkr", type="string", length=255, nullable=true)
     */
    private $nameukr;

    /**
     * @var integer
     *
     * @ORM\Column(name="IsArchive", type="integer", nullable=true)
     */
    private $isarchive;

    /**
     * @var string
     *
     * @ORM\Column(name="guid", type="guid", nullable=true)
     */
    private $guid;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="currenttime", type="datetime", nullable=true)
     */
    private $currenttime;

    /**
     * @var string
     *
     * @ORM\Column(name="moduser", type="string", length=16, nullable=true)
     */
    private $moduser;

    /**
     * @var string
     *
     * @ORM\Column(name="Suburb", type="string", length=255, nullable=true)
     */
    private $suburb;

    /**
     * @var string
     *
     * @ORM\Column(name="blddop", type="string", length=10, nullable=true)
     */
    private $blddop;

    /**
     * @var integer
     *
     * @ORM\Column(name="statusverification", type="integer", nullable=true)
     */
    private $statusverification;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="sendverification", type="datetime", nullable=true)
     */
    private $sendverification;

    /**
     * @var string
     *
     * @ORM\Column(name="name2", type="string", length=255, nullable=true)
     */
    private $name2;

    /**
     * @var string
     *
     * @ORM\Column(name="code2", type="string", length=255, nullable=true)
     */
    private $code2;

    /**
     * @var integer
     *
     * @ORM\Column(name="id2", type="integer", nullable=true)
     */
    private $id2;

    /**
     * @var integer
     *
     * @ORM\Column(name="dragstore_cnt", type="integer", nullable=true)
     */
    private $dragstoreCnt;

    /**
     * @var integer
     *
     * @ORM\Column(name="id_hrm", type="integer", nullable=true)
     */
    private $idHrm;

    /**
     * @var integer
     *
     * @ORM\Column(name="updname", type="integer", nullable=true)
     */
    private $updname;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="CreateDate", type="datetime", nullable=true)
     */
    private $createdate;

    /**
     * @var string
     *
     * @ORM\Column(name="gps_la", type="string", length=255, nullable=true)
     */
    private $gpsLa;

    /**
     * @var string
     *
     * @ORM\Column(name="gps_lo", type="string", length=255, nullable=true)
     */
    private $gpsLo;

    /**zcy
     * @var integer
     *
     * @ORM\Column(name="IsActive", type="integer", nullable=true)
     */
    private $isactive;

    /**
     * @var integer
     *
     * @ORM\Column(name="ismain", type="integer", nullable=true)
     */
    private $ismain;

    /**
     * @var integer
     *
     * @ORM\Column(name="morionid", type="integer", nullable=true)
     */
    private $morionid;

    /**
     * @var integer
     *
     * @ORM\Column(name="isshop2", type="integer", nullable=true)
     */
    private $isshop2;

    /**
     * @var InfoUser
     *
     * @ORM\ManyToOne(targetEntity="InfoUser")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="Owner_id", referencedColumnName="id")
     * })
     */
    private $owner;

    /**
     * @var InfoUser
     *
     * @ORM\ManyToOne(targetEntity="InfoUser")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="Modifier_id", referencedColumnName="id")
     * })
     */
    private $modifier;

    /**
     * @var InfoArchivereason
     *
     * @ORM\ManyToOne(targetEntity="InfoArchivereason")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="ArchiveReason_id", referencedColumnName="id")
     * })
     */
    private $archivereason;

    /**
     * @var InfoCompany
     *
     * @ORM\ManyToOne(targetEntity="InfoCompany")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="main_id", referencedColumnName="id")
     * })
     */
    private $main;

    /**
     * @var InfoCompany
     *
     * @ORM\ManyToOne(targetEntity="InfoCompany")
     * @ORM\JoinColumn(name="CENTER_ID", referencedColumnName="id")
     */
    private $centerId;

    /**
     * @var InfoDictionary
     *
     * @ORM\ManyToOne(targetEntity="InfoDictionary")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="streettypeukr_id", referencedColumnName="id")
     * })
     */
    private $streettypeukr;

    /**
     * @var InfoCompanycategory
     *
     * @ORM\ManyToOne(targetEntity="InfoCompanycategory")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="Category_id", referencedColumnName="id")
     * })
     */
    private $category;

    /**
     * @var InfoPhonetype
     *
     * @ORM\ManyToOne(targetEntity="InfoPhonetype")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="phonetype1_id", referencedColumnName="id")
     * })
     */
    private $phonetype1;

    /**
     * @var InfoCompany
     *
     * @ORM\ManyToOne(targetEntity="InfoCompany")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="nearcompany_id", referencedColumnName="id")
     * })
     */
    private $nearcompany;

    /**
     * @var InfoPhonetype
     *
     * @ORM\ManyToOne(targetEntity="InfoPhonetype")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="phonetype2_id", referencedColumnName="id")
     * })
     */
    private $phonetype2;

    /**
     * @var InfoRegion
     *
     * @ORM\ManyToOne(targetEntity="InfoRegion")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="Region_Id", referencedColumnName="id")
     * })
     */
    private $region;

    /**
     * @var InfoCountry
     *
     * @ORM\ManyToOne(targetEntity="InfoCountry")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="country_id", referencedColumnName="id")
     * })
     */
    private $country;

    /**
     * @var InfoCity
     *
     * @ORM\ManyToOne(targetEntity="InfoCity")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="City_id", referencedColumnName="id")
     * })
     */
    private $city;

    /**
     * @var InfoSuburb
     *
     * @ORM\ManyToOne(targetEntity="InfoSuburb")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="Suburb_id", referencedColumnName="id")
     * })
     */
    private $suburb2;

    /**
     * @var InfoCompanytype
     *
     * @ORM\ManyToOne(targetEntity="InfoCompanytype")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="CompanyType_id", referencedColumnName="id")
     * })
     */
    private $companytype;

    /**
     * @var InfoDistrict
     *
     * @ORM\ManyToOne(targetEntity="InfoDistrict")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="district_id", referencedColumnName="id")
     * })
     */
    private $district;

    /**
     * @var InfoDictionary
     *
     * @ORM\ManyToOne(targetEntity="InfoDictionary")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="streettype_id", referencedColumnName="id")
     * })
     */
    private $streettype;

    /**
     * @var ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="InfoCompanyinfo", mappedBy="subj", cascade={"persist"}, orphanRemoval=true)
     */
    private $companyInfoCollection;


    /**
     * @var string
     *
     * @ORM\Column(name="tax_number", type="string", length=255, nullable=true)
     */
    private $taxNumber;

    /**
     * @var ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="InfoContact", mappedBy="company")
     */
    private $contactCollection;

    /**
     * @var ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="InfoCompanyowner", mappedBy="company")
     */
    private $companyOwnerCollection;

    /**
     * @var ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="InfoCompanycateg", mappedBy="company", cascade={"persist"}, orphanRemoval=true)
     */
    private $companyCategoryCollection;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="getverification", type="datetime", nullable=true)
     */
    private $getVerification;

    /**
     * @var string
     *
     * @ORM\Column(name="gpspoint", type="geometry", nullable=true)
     */
    private $gpspoint;

    /**
     * @var InfoCompanySign|null
     * @ORM\OneToOne(
     *     targetEntity="InfoCompanySign",
     *     mappedBy="company",
     *     cascade={"persist"}
     * )
     */
    private $companySign;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->companyInfoCollection = new ArrayCollection();
        $this->contactCollection = new ArrayCollection();
        $this->companyOwnerCollection = new ArrayCollection();
        $this->companyCategoryCollection = new ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return InfoCompany
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set code
     *
     * @param string $code
     * @return InfoCompany
     */
    public function setCode($code)
    {
        $this->code = $code;

        return $this;
    }

    /**
     * Get code
     *
     * @return string
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * Set description
     *
     * @param string $description
     * @return InfoCompany
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set modified
     *
     * @param \DateTime $modified
     * @return InfoCompany
     */
    public function setModified(\DateTime $modified)
    {
        $this->modified = $modified;

        return $this;
    }

    /**
     * Get modified
     *
     * @return \DateTime
     */
    public function getModified()
    {
        return $this->modified;
    }

    /**
     * Set phone1
     *
     * @param string $phone1
     * @return InfoCompany
     */
    public function setPhone1($phone1)
    {
        $this->phone1 = $phone1;

        return $this;
    }

    /**
     * Get phone1
     *
     * @return string
     */
    public function getPhone1()
    {
        return $this->phone1;
    }

    /**
     * Set phone2
     *
     * @param string $phone2
     * @return InfoCompany
     */
    public function setPhone2($phone2)
    {
        $this->phone2 = $phone2;

        return $this;
    }

    /**
     * Get phone2
     *
     * @return string
     */
    public function getPhone2()
    {
        return $this->phone2;
    }

    /**
     * Set practiceno
     *
     * @param string $practiceno
     * @return InfoCompany
     */
    public function setPracticeno($practiceno)
    {
        $this->practiceno = $practiceno;

        return $this;
    }

    /**
     * Get practiceno
     *
     * @return string
     */
    public function getPracticeno()
    {
        return $this->practiceno;
    }

    /**
     * Set eaddr
     *
     * @param string $eaddr
     * @return InfoCompany
     */
    public function setEaddr($eaddr)
    {
        $this->eaddr = $eaddr;

        return $this;
    }

    /**
     * Get eaddr
     *
     * @return string
     */
    public function getEaddr()
    {
        return $this->eaddr;
    }

    /**
     * Set okrug
     *
     * @param string $okrug
     * @return InfoCompany
     */
    public function setOkrug($okrug)
    {
        $this->okrug = $okrug;

        return $this;
    }

    /**
     * Get okrug
     *
     * @return string
     */
    public function getOkrug()
    {
        return $this->okrug;
    }

    /**
     * Set postcode
     *
     * @param string $postcode
     * @return InfoCompany
     */
    public function setPostcode($postcode)
    {
        $this->postcode = $postcode;

        return $this;
    }

    /**
     * Get postcode
     *
     * @return string
     */
    public function getPostcode()
    {
        return $this->postcode;
    }

    /**
     * Set street
     *
     * @param string $street
     * @return InfoCompany
     */
    public function setStreet($street)
    {
        $this->street = $street;

        return $this;
    }

    /**
     * Get street
     *
     * @return string
     */
    public function getStreet()
    {
        return $this->street;
    }

    /**
     * Set building
     *
     * @param string $building
     * @return InfoCompany
     */
    public function setBuilding($building)
    {
        $this->building = $building;

        return $this;
    }

    /**
     * Get building
     *
     * @return string
     */
    public function getBuilding()
    {
        return $this->building;
    }

    /**
     * Set apartment
     *
     * @param string $apartment
     * @return InfoCompany
     */
    public function setApartment($apartment)
    {
        $this->apartment = $apartment;

        return $this;
    }

    /**
     * Get apartment
     *
     * @return string
     */
    public function getApartment()
    {
        return $this->apartment;
    }

    /**
     * Set waddr
     *
     * @param string $waddr
     * @return InfoCompany
     */
    public function setWaddr($waddr)
    {
        $this->waddr = $waddr;

        return $this;
    }

    /**
     * Get waddr
     *
     * @return string
     */
    public function getWaddr()
    {
        return $this->waddr;
    }

    /**
     * Set streetukr
     *
     * @param string $streetukr
     * @return InfoCompany
     */
    public function setStreetukr($streetukr)
    {
        $this->streetukr = $streetukr;

        return $this;
    }

    /**
     * Get streetukr
     *
     * @return string
     */
    public function getStreetukr()
    {
        return $this->streetukr;
    }

    /**
     * Set nameukr
     *
     * @param string $nameukr
     * @return InfoCompany
     */
    public function setNameukr($nameukr)
    {
        $this->nameukr = $nameukr;

        return $this;
    }

    /**
     * Get nameukr
     *
     * @return string
     */
    public function getNameukr()
    {
        return $this->nameukr;
    }

    /**
     * Set isarchive
     *
     * @param integer $isarchive
     * @return InfoCompany
     */
    public function setIsarchive($isarchive)
    {
        $this->isarchive = $isarchive;

        return $this;
    }

    /**
     * Get isarchive
     *
     * @return integer
     */
    public function getIsarchive()
    {
        return $this->isarchive;
    }

    /**
     * Set guid
     *
     * @param string $guid
     * @return InfoCompany
     */
    public function setGuid($guid)
    {
        $this->guid = $guid;

        return $this;
    }

    /**
     * Get guid
     *
     * @return string
     */
    public function getGuid()
    {
        return $this->guid;
    }

    /**
     * Set currenttime
     *
     * @param \DateTime $currenttime
     * @return InfoCompany
     */
    public function setCurrenttime(\DateTime $currenttime)
    {
        $this->currenttime = $currenttime;

        return $this;
    }

    /**
     * Get currenttime
     *
     * @return \DateTime
     */
    public function getCurrenttime()
    {
        return $this->currenttime;
    }

    /**
     * Set moduser
     *
     * @param string $moduser
     * @return InfoCompany
     */
    public function setModuser($moduser)
    {
        $this->moduser = $moduser;

        return $this;
    }

    /**
     * Get moduser
     *
     * @return string
     */
    public function getModuser()
    {
        return $this->moduser;
    }

    /**
     * Set suburb
     *
     * @param string $suburb
     * @return InfoCompany
     */
    public function setSuburb($suburb)
    {
        $this->suburb = $suburb;

        return $this;
    }

    /**
     * Get suburb
     *
     * @return string
     */
    public function getSuburb()
    {
        return $this->suburb;
    }

    /**
     * Set blddop
     *
     * @param string $blddop
     * @return InfoCompany
     */
    public function setBlddop($blddop)
    {
        $this->blddop = $blddop;

        return $this;
    }

    /**
     * Get blddop
     *
     * @return string
     */
    public function getBlddop()
    {
        return $this->blddop;
    }

    /**
     * Set statusverification
     *
     * @param integer $statusverification
     * @return InfoCompany
     */
    public function setStatusverification($statusverification)
    {
        $this->statusverification = $statusverification;

        return $this;
    }

    /**
     * Get statusverification
     *
     * @return integer
     */
    public function getStatusverification()
    {
        return $this->statusverification;
    }

    /**
     * Set sendverification
     *
     * @param \DateTime $sendverification
     * @return InfoCompany
     */
    public function setSendverification($sendverification)
    {
        $this->sendverification = $sendverification;

        return $this;
    }

    /**
     * Get sendverification
     *
     * @return \DateTime
     */
    public function getSendverification()
    {
        return $this->sendverification;
    }

    /**
     * Set name2
     *
     * @param string $name2
     * @return InfoCompany
     */
    public function setName2($name2)
    {
        $this->name2 = $name2;

        return $this;
    }

    /**
     * Get name2
     *
     * @return string
     */
    public function getName2()
    {
        return $this->name2;
    }

    /**
     * Set code2
     *
     * @param string $code2
     * @return InfoCompany
     */
    public function setCode2($code2)
    {
        $this->code2 = $code2;

        return $this;
    }

    /**
     * Get code2
     *
     * @return string
     */
    public function getCode2()
    {
        return $this->code2;
    }

    /**
     * Set id2
     *
     * @param integer $id2
     * @return InfoCompany
     */
    public function setId2($id2)
    {
        $this->id2 = $id2;

        return $this;
    }

    /**
     * Get id2
     *
     * @return integer
     */
    public function getId2()
    {
        return $this->id2;
    }

    /**
     * Set dragstoreCnt
     *
     * @param integer $dragstoreCnt
     * @return InfoCompany
     */
    public function setDragstoreCnt($dragstoreCnt)
    {
        $this->dragstoreCnt = $dragstoreCnt;

        return $this;
    }

    /**
     * Get dragstoreCnt
     *
     * @return integer
     */
    public function getDragstoreCnt()
    {
        return $this->dragstoreCnt;
    }

    /**
     * Set idHrm
     *
     * @param integer $idHrm
     * @return InfoCompany
     */
    public function setIdHrm($idHrm)
    {
        $this->idHrm = $idHrm;

        return $this;
    }

    /**
     * Get idHrm
     *
     * @return integer
     */
    public function getIdHrm()
    {
        return $this->idHrm;
    }

    /**
     * Set updname
     *
     * @param integer $updname
     * @return InfoCompany
     */
    public function setUpdname($updname)
    {
        $this->updname = $updname;

        return $this;
    }

    /**
     * Get updname
     *
     * @return integer
     */
    public function getUpdname()
    {
        return $this->updname;
    }

    /**
     * Set createdate
     *
     * @param \DateTime $createdate
     * @return InfoCompany
     */
    public function setCreatedate(\DateTime $createdate)
    {
        $this->createdate = $createdate;

        return $this;
    }

    /**
     * Get createdate
     *
     * @return \DateTime
     */
    public function getCreatedate()
    {
        return $this->createdate;
    }

    /**
     * Set gpsLa
     *
     * @param string $gpsLa
     * @return InfoCompany
     */
    public function setGpsLa($gpsLa)
    {
        $this->gpsLa = $gpsLa;

        return $this;
    }

    /**
     * Get gpsLa
     *
     * @return string
     */
    public function getGpsLa()
    {
        return $this->gpsLa;
    }

    /**
     * Get gpsLa
     *
     * @return float
     */
    public function getGpsLaFloat()
    {
        return floatval(str_replace(",", ".", $this->gpsLa));
    }

    /**
     * Set gpsLo
     *
     * @param string $gpsLo
     * @return InfoCompany
     */
    public function setGpsLo($gpsLo)
    {
        $this->gpsLo = $gpsLo;

        return $this;
    }

    /**
     * Get gpsLo
     *
     * @return string
     */
    public function getGpsLo()
    {
        return $this->gpsLo;
    }

    /**
     * Get gpsLo
     *
     * @return float
     */
    public function getGpsLoFloat()
    {
        return floatval(str_replace(",", ".", $this->gpsLo));
    }

    /**
     * Set isactive
     *
     * @param integer $isactive
     * @return InfoCompany
     */
    public function setIsactive($isactive)
    {
        $this->isactive = $isactive;

        return $this;
    }

    /**
     * Get isactive
     *
     * @return integer
     */
    public function getIsactive()
    {
        return $this->isactive;
    }

    /**
     * Set morionid
     *
     * @param integer $morionid
     * @return InfoCompany
     */
    public function setMorionid($morionid)
    {
        $this->morionid = $morionid;

        return $this;
    }

    /**
     * Get morionid
     *
     * @return integer
     */
    public function getMorionid()
    {
        return $this->morionid;
    }

    /**
     * Set ismain
     *
     * @param integer $ismain
     * @return InfoCompany
     */
    public function setIsmain($ismain)
    {
        $this->ismain = $ismain;

        return $this;
    }

    /**
     * Get ismain
     *
     * @return integer
     */
    public function getIsmain()
    {
        return $this->ismain;
    }

    /**
     * Set isshop2
     *
     * @param integer $isshop2
     * @return InfoCompany
     */
    public function setIsshop2($isshop2)
    {
        $this->isshop2 = $isshop2;

        return $this;
    }

    /**
     * Get isshop2
     *
     * @return integer
     */
    public function getIsshop2()
    {
        return $this->isshop2;
    }

    /**
     * Set owner
     *
     * @param \TeamSoft\CrmRepositoryBundle\Entity\InfoUser $owner
     * @return InfoCompany
     */
    public function setOwner(InfoUser $owner = null)
    {
        $this->owner = $owner;

        return $this;
    }

    /**
     * Get owner
     *
     * @return InfoUser
     */
    public function getOwner()
    {
        return $this->owner;
    }

    /**
     * Set modifier
     *
     * @param InfoUser $modifier
     * @return InfoCompany
     */
    public function setModifier(InfoUser $modifier = null)
    {
        $this->modifier = $modifier;

        return $this;
    }

    /**
     * Get modifier
     *
     * @return InfoUser
     */
    public function getModifier()
    {
        return $this->modifier;
    }

    /**
     * Set archivereason
     *
     * @param InfoArchivereason $archivereason
     * @return InfoCompany
     */
    public function setArchivereason(InfoArchivereason $archivereason = null)
    {
        $this->archivereason = $archivereason;

        return $this;
    }

    /**
     * Get archivereason
     *
     * @return InfoArchivereason
     */
    public function getArchivereason()
    {
        return $this->archivereason;
    }

    /**
     * Set main
     *
     * @param InfoCompany $main
     * @return InfoCompany
     */
    public function setMain(InfoCompany $main = null)
    {
        $this->main = $main;

        return $this;
    }

    /**
     * Get main
     *
     * @return InfoCompany
     */
    public function getMain()
    {
        return $this->main;
    }

    /**
     * Set streettypeukr
     *
     * @param InfoDictionary $streettypeukr
     * @return InfoCompany
     */
    public function setStreettypeukr(InfoDictionary $streettypeukr = null)
    {
        $this->streettypeukr = $streettypeukr;

        return $this;
    }

    /**
     * Get streettypeukr
     *
     * @return InfoDictionary
     */
    public function getStreettypeukr()
    {
        return $this->streettypeukr;
    }

    /**
     * Set category
     *
     * @param InfoCompanycategory $category
     * @return InfoCompany
     */
    public function setCategory(InfoCompanycategory $category = null)
    {
        $this->category = $category;

        return $this;
    }

    /**
     * Get category
     *
     * @return InfoCompanycategory
     */
    public function getCategory()
    {
        return $this->category;
    }

    /**
     * Set phonetype1
     *
     * @param InfoPhonetype $phonetype1
     * @return InfoCompany
     */
    public function setPhonetype1(InfoPhonetype $phonetype1 = null)
    {
        $this->phonetype1 = $phonetype1;

        return $this;
    }

    /**
     * Get phonetype1
     *
     * @return InfoPhonetype
     */
    public function getPhonetype1()
    {
        return $this->phonetype1;
    }

    /**
     * Set nearcompany
     *
     * @param InfoCompany $nearcompany
     * @return InfoCompany
     */
    public function setNearcompany(InfoCompany $nearcompany = null)
    {
        $this->nearcompany = $nearcompany;

        return $this;
    }

    /**
     * Get nearcompany
     *
     * @return InfoCompany
     */
    public function getNearcompany()
    {
        return $this->nearcompany;
    }

    /**
     * Set phonetype2
     *
     * @param InfoPhonetype $phonetype2
     * @return InfoCompany
     */
    public function setPhonetype2(InfoPhonetype $phonetype2 = null)
    {
        $this->phonetype2 = $phonetype2;

        return $this;
    }

    /**
     * Get phonetype2
     *
     * @return InfoPhonetype
     */
    public function getPhonetype2()
    {
        return $this->phonetype2;
    }

    /**
     * Set region
     *
     * @param InfoRegion $region
     * @return InfoCompany
     */
    public function setRegion(InfoRegion $region = null)
    {
        $this->region = $region;

        return $this;
    }

    /**
     * Get region
     *
     * @return InfoRegion
     */
    public function getRegion()
    {
        return $this->region;
    }

    /**
     * Set country
     *
     * @param InfoCountry $country
     * @return InfoCompany
     */
    public function setCountry(InfoCountry $country = null)
    {
        $this->country = $country;

        return $this;
    }

    /**
     * Get country
     *
     * @return InfoCountry
     */
    public function getCountry()
    {
        return $this->country;
    }

    /**
     * Set city
     *
     * @param InfoCity $city
     * @return InfoCompany
     */
    public function setCity(InfoCity $city = null)
    {
        $this->city = $city;

        return $this;
    }

    /**
     * Get city
     *
     * @return InfoCity
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
     * Set suburb2
     *
     * @param InfoSuburb $suburb2
     * @return InfoCompany
     */
    public function setSuburb2(InfoSuburb $suburb2 = null)
    {
        $this->suburb2 = $suburb2;

        return $this;
    }

    /**
     * Get suburb2
     *
     * @return InfoSuburb
     */
    public function getSuburb2()
    {
        return $this->suburb2;
    }

    /**
     * Set companytype
     *
     * @param InfoCompanytype $companytype
     * @return InfoCompany
     */
    public function setCompanytype(InfoCompanytype $companytype = null)
    {
        $this->companytype = $companytype;

        return $this;
    }

    /**
     * Get companytype
     *
     * @return InfoCompanytype
     */
    public function getCompanytype()
    {
        return $this->companytype;
    }

    /**
     * Set district
     *
     * @param InfoDistrict $district
     * @return InfoCompany
     */
    public function setDistrict(InfoDistrict $district = null)
    {
        $this->district = $district;

        return $this;
    }

    /**
     * Get district
     *
     * @return InfoDistrict
     */
    public function getDistrict()
    {
        return $this->district;
    }

    /**
     * Set streettype
     *
     * @param InfoDictionary $streettype
     * @return InfoCompany
     */
    public function setStreettype(InfoDictionary $streettype = null)
    {
        $this->streettype = $streettype;

        return $this;
    }

    /**
     * Get streettype
     *
     * @return InfoDictionary
     */
    public function getStreettype()
    {
        return $this->streettype;
    }

    public function getAddressFull() {
        $address = '';
        $streetType = $this->getStreettype();
        if ($streetType && $streetType->getName()) {
            $address .= mb_convert_case($streetType->getName(), MB_CASE_LOWER, "UTF-8");
        }
        if ($this->street) {
            $address .= $address !== '' ? ' ' . $this->street : $this->street;
        }
        if ($this->building) {
            $address .= $address !== '' ? ', ' . $this->building : $this->building;
        }
        $city = $this->getCity();
        if ($city && $city->getName()) {
            $address .= $address !== '' ? ', ' . $city->getName() : $city->getName();
        }
        return $address ? $address : null;
    }

    public function getShortAddress() {
        $address = '';
        if ($this->street) {
            $address .= $this->street;
        }
        if ($this->building) {
            $address .= $address !== '' ? ', ' . $this->building : $this->building;
        }
        return $address ? $address : null;
    }

    public function getShortAddressUa() {
        $address = '';
        if ($this->street) {
            $address .= 'ул.' . $this->street;
        }
        if ($this->building) {
            $address .= $address !== '' ? ', б.' . $this->building : $this->building;
        }
        return $address ? $address : null;
    }

    public function getCountryId () {
        return $this->getCountry() ? $this->getCountry()->getId() : null;
    }

    public function getRegionId () {
        return $this->getRegion() ? $this->getRegion()->getId() : null;
    }

    public function getCityId () {
        return $this->getCity() ? $this->getCity()->getId() : null;
    }

    public function getStreettypeId () {
        return $this->getStreettype() ? $this->getStreettype()->getId() : null;
    }

    public function getCompanytypeId () {
        return $this->getCompanytype() ? $this->getCompanytype()->getId() : null;
    }

    public function getCategoryId () {
        return $this->getCategory() ? $this->getCategory()->getId() : null;
    }

    public function getCompanytypeName () {
        return $this->getCompanytype() ? $this->getCompanytype()->getName() : null;
    }

    public function getCategoryName () {
        return $this->getCategory() ? $this->getCategory()->getName() : null;
    }

    public function getOwnerId () {
        return $this->getOwner() ? $this->getOwner()->getId() : null;
    }

    public function getArchivereasonId () {
        return $this->getArchivereason() ? $this->getArchivereason()->getId() : null;
    }

    public function getAddress () {
        $addressParts = array();

        if ($this->city !== null) {
            $addressParts[] = $this->city->getName();
        }
        if (!empty($this->street)) {
            $street = $this->street;
            if ($this->streettype !== null) {
                $street = $this->streettype->getName() . ' ' . $street;
            }
            $addressParts[] = $street;
        }
        if (!empty($this->building)) {
            $addressParts[] = $this->building;
        }
        return implode(', ', $addressParts);
    }

    //ToDo: dublicate method getAddressFull. leave one
    public function getFullAddress () {
        $addressParts = array();

        if ($this->getPostcode() !== null) {
            $addressParts[] = $this->getPostcode();
        }
        if ($this->country !== null) {
            $addressParts[] = $this->country->getName();
        }
        if ($this->region !== null) {
            $addressParts[] = $this->region->getName();
        }
        if ($this->city !== null) {
            $addressParts[] = $this->city->getName();
        }
        if (!empty($this->street)) {
            $street = $this->street;
            if ($this->streettype !== null) {
                $street = $this->streettype->getName() . ' ' . $street;
            }
            $addressParts[] = $street;
        }
        if (!empty($this->building)) {
            $addressParts[] = $this->building;
        }
        return implode(', ', $addressParts);
    }

    public function getStatusVerificationDescription () {
        return $this->statusverification;
    }

    public function getVisitsCountOnStrategy(){
        return rand(0,100);
    }

    public function getVisitsCountAll(){
        return rand(0,100);
    }

    public function getMainId () {
        return $this->getMain() ? $this->getMain()->getId() : null;
    }

    /**
     * Add companyInfoCollection
     *
     * @param InfoCompanyinfo $companyInfo
     *
     * @return InfoCompany
     */
    public function addCompanyInfoCollection(InfoCompanyinfo $companyInfo)
    {
        $companyInfo->setSubj($this);
        $this->companyInfoCollection->add($companyInfo);

        return $this;
    }

    /**
     * Remove companyInfoCollection
     *
     * @param InfoCompanyinfo $companyInfo
     */
    public function removeCompanyInfoCollection(InfoCompanyinfo $companyInfo)
    {
        $companyInfo->setSubj(null);
        $this->companyInfoCollection->removeElement($companyInfo);
    }

    /**
     * Get companyInfoCollection
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getCompanyInfoCollection()
    {
        return $this->companyInfoCollection;
    }

    /**
     * Set centerId.
     *
     * @param InfoCompany|null $centerId
     *
     * @return InfoCompany
     */
    public function setCenterId(InfoCompany $centerId = null)
    {
        $this->centerId = $centerId;

        return $this;
    }

    /**
     * Get centerId.
     *
     * @return InfoCompany|null
     */
    public function getCenterId()
    {
        return $this->centerId;
    }

    /**
     * Add contactCollection.
     *
     * @param InfoContact $contactCollection
     *
     * @return InfoCompany
     */
    public function addContactCollection(InfoContact $contactCollection)
    {
        $this->contactCollection[] = $contactCollection;

        return $this;
    }

    /**
     * Remove contactCollection.
     *
     * @param InfoContact $contactCollection
     *
     * @return boolean TRUE if this collection contained the specified element, FALSE otherwise.
     */
    public function removeContactCollection(InfoContact $contactCollection)
    {
        return $this->contactCollection->removeElement($contactCollection);
    }

    /**
     * Get contactCollection.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getContactCollection()
    {
        return $this->contactCollection;
    }

    /**
     * @return string
     */
    public function getTaxNumber(): ?string
    {
        return $this->taxNumber;
    }

    /**
     * @param string $taxNumber
     *
     * @return InfoCompany
     */
    public function setTaxNumber(?string $taxNumber): InfoCompany
    {
        $this->taxNumber = $taxNumber;

        return $this;
    }

    /**
     * @return ArrayCollection
     */
    public function getCompanyCategoryCollection()
    {
        return $this->companyCategoryCollection;
    }

    /**
     * Add contactCategory.
     *
     * @param \TeamSoft\CrmRepositoryBundle\Entity\InfoCompanycateg $contactCategory
     *
     * @return InfoCompany
     */
    public function addCompanyCategoryCollection(\TeamSoft\CrmRepositoryBundle\Entity\InfoCompanycateg $companyCategory): InfoCompany
    {
        $companyCategory->setCompany($this);
        $this->companyCategoryCollection[] = $companyCategory;

        return $this;
    }

    /**
     * Remove companyCategory.
     *
     * @param InfoCompanycateg $companyCategory
     *
     * @return boolean TRUE if this collection contained the specified element, FALSE otherwise.
     */
    public function removeCompanyCategoryCollection(InfoCompanycateg $companyCategory)
    {
        return $this->companyCategoryCollection->removeElement($companyCategory);
    }

    /**
     * @return ArrayCollection
     */
    public function getCompanyOwnerCollection()
    {
        return $this->companyOwnerCollection;
    }

    /**
     * Add contactCategory.
     *
     * @param InfoCompanyowner $companyOwner
     *
     * @return InfoCompany
     */
    public function addCompanyOwnerCollection(InfoCompanyowner $companyOwner): InfoCompany
    {
        $companyOwner->setCompany($this);
        $this->companyOwnerCollection[] = $companyOwner;

        return $this;
    }

    /**
     * Remove companyOwner.
     *
     * @param InfoCompanyowner $companyOwner
     *
     * @return boolean TRUE if this collection contained the specified element, FALSE otherwise.
     */
    public function removeCompanyOwnerCollection(InfoCompanyowner $companyOwner)
    {
        return $this->companyOwnerCollection->removeElement($companyOwner);
    }

    /**
     * Set getVerification
     *
     * @param \DateTime $getVerification
     * @return InfoCompany
     */
    public function setGetVerification(\DateTime $getVerification)
    {
        $this->getVerification = $getVerification;

        return $this;
    }

    /**
     * Get getVerification
     *
     * @return \DateTime
     */
    public function getGetVerification()
    {
        return $this->getVerification;
    }

    /**
     * @return array
     */
    public function getGeoJSON(){
        return [
            'type' => 'Feature',
            'geometry' => [
                'type' => 'Point',
                'coordinates' => [$this->getGpsLoFloat(), $this->getGpsLaFloat()]
            ],
            'properties'=> [
                'id' => $this->id,
                'name' => $this->name,
                'region_id' => $this->getRegionId(),
                'companytype_id' => $this->getCompanytypeId(),
                'isArchive' => $this->isarchive
            ]
        ];
    }

    /**
     * @return bool
     */
    public function isRegionVerification(): bool
    {
        return $this->getRegion() && $this->getRegion()->isVerification();
    }

    /**
     * @return int|null
     */
    public function getCategoryDirectionId(): ?int
    {
        return $this->getCategory() ? $this->getCategory()->getDirectionId() : null;
    }

    /**
     * @return InfoCompanySign|null
     */
    public function getCompanySign(): ?InfoCompanySign
    {
        return $this->companySign;
    }

    /**
     * @param InfoCompanySign|null $companySign
     * @return self
     */
    public function setCompanySign(?InfoCompanySign $companySign): self
    {
        $this->companySign = $companySign;

        return $this;
    }
}
