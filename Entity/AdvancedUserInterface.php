<?php

namespace TeamSoft\CrmRepositoryBundle\Entity;

use Symfony\Component\Security\Core\User\UserInterface;

interface AdvancedUserInterface extends UserInterface
{
    public function isEnabled(): bool;

    public function isWork(): bool;

    public function allowedHost(): ?string;

    public function isAccountNonLocked(): bool;

    public function isAccountNonExpired(): bool;

    public function isCredentialsNonExpired(int $daysIntervalForPasswordChange): bool;
}
