<?php

namespace TeamSoft\CrmRepositoryBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * InfoReportfiltersql
 *
 * @ORM\Table(name="info_reportfiltersql")
 * @ORM\Entity
 */
class InfoReportfiltersql implements ServiceFieldInterface
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var int
     *
     * @ORM\Column(name="reportsql_id", type="integer", nullable=true)
     */
    private $reportsqlId;

    /**
     * @var string
     *
     * @ORM\Column(name="guid", type="guid", nullable=true)
     */
    private $guid;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="currenttime", type="datetime", nullable=true)
     */
    private $currenttime;

    /**
     * @var string
     *
     * @ORM\Column(name="moduser", type="string", length=16, nullable=true)
     */
    private $moduser;

    /**
     * @var InfoReportfilter
     *
     * @ORM\ManyToOne(targetEntity="InfoReportfilter")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="reportfilter_id", referencedColumnName="id")
     * })
     */
    private $reportfilter;



    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set reportsqlId
     *
     * @param int $reportsqlId
     *
     * @return InfoReportfiltersql
     */
    public function setReportsqlId($reportsqlId)
    {
        $this->reportsqlId = $reportsqlId;
    
        return $this;
    }

    /**
     * Get reportsqlId
     *
     * @return int
     */
    public function getReportsqlId()
    {
        return $this->reportsqlId;
    }

    /**
     * Set guid
     *
     * @param string $guid
     *
     * @return InfoReportfiltersql
     */
    public function setGuid($guid)
    {
        $this->guid = $guid;
    
        return $this;
    }

    /**
     * Get guid
     *
     * @return string
     */
    public function getGuid()
    {
        return $this->guid;
    }

    /**
     * Set currenttime
     *
     * @param \DateTime $currenttime
     *
     * @return InfoReportfiltersql
     */
    public function setCurrenttime(\DateTime $currenttime)
    {
        $this->currenttime = $currenttime;
    
        return $this;
    }

    /**
     * Get currenttime
     *
     * @return \DateTime
     */
    public function getCurrenttime()
    {
        return $this->currenttime;
    }

    /**
     * Set moduser
     *
     * @param string $moduser
     *
     * @return InfoReportfiltersql
     */
    public function setModuser($moduser)
    {
        $this->moduser = $moduser;
    
        return $this;
    }

    /**
     * Get moduser
     *
     * @return string
     */
    public function getModuser()
    {
        return $this->moduser;
    }

    /**
     * Set reportfilter
     *
     * @param \TeamSoft\CrmRepositoryBundle\Entity\InfoReportfilter $reportfilter
     *
     * @return InfoReportfiltersql
     */
    public function setReportfilter(InfoReportfilter $reportfilter = null)
    {
        $this->reportfilter = $reportfilter;
    
        return $this;
    }

    /**
     * Get reportfilter
     *
     * @return \TeamSoft\CrmRepositoryBundle\Entity\InfoReportfilter
     */
    public function getReportfilter()
    {
        return $this->reportfilter;
    }
}
