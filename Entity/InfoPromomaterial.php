<?php

namespace TeamSoft\CrmRepositoryBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * InfoPromomaterial
 *
 * @ORM\Table(name="info_promomaterial", indexes={@ORM\Index(name="ix_info_promomaterial_guid", columns={"guid"}), @ORM\Index(name="ix_info_promomaterial_time", columns={"time"}), @ORM\Index(name="ix_info_promomaterial_promomaterialtype_id", columns={"promomaterialtype_id"}), @ORM\Index(name="ix_info_promomaterial_unit_id", columns={"unit_id"}), @ORM\Index(name="ix_info_promomaterial_group_id", columns={"group_id"})})
 * @ORM\Entity
 */
class InfoPromomaterial implements ServiceFieldInterface
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255, nullable=true)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="blob", nullable=true)
     */
    private $description;

    /**
     * @var float
     *
     * @ORM\Column(name="price", type="decimal", precision=18, scale=4, nullable=true)
     */
    private $price;

    /**
     * @var guid
     *
     * @ORM\Column(name="guid", type="guid", nullable=true)
     */
    private $guid;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="currenttime", type="datetime", nullable=true)
     */
    private $currenttime;

    /**
     * @var string
     *
     * @ORM\Column(name="moduser", type="string", length=16, nullable=true)
     */
    private $moduser;

    /**
     * @var int
     *
     * @ORM\Column(name="iscontrol", type="integer", nullable=true)
     */
    private $iscontrol;

    /**
     * @var \InfoDictionary
     *
     * @ORM\ManyToOne(targetEntity="InfoDictionary")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="unit_id", referencedColumnName="id")
     * })
     */
    private $unit;

    /**
     * @var \InfoPromomaterialgroup
     *
     * @ORM\ManyToOne(targetEntity="InfoPromomaterialgroup")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="group_id", referencedColumnName="id")
     * })
     */
    private $group;

    /**
     * @var \InfoDictionary
     *
     * @ORM\ManyToOne(targetEntity="InfoDictionary")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="promomaterialtype_id", referencedColumnName="id")
     * })
     */
    private $promomaterialtype;



    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return InfoPromomaterial
     */
    public function setName($name)
    {
        $this->name = $name;
    
        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set description
     *
     * @param string $description
     *
     * @return InfoPromomaterial
     */
    public function setDescription($description)
    {
        $this->description = $description;
    
        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set price
     *
     * @param int $price
     *
     * @return InfoPromomaterial
     */
    public function setPrice($price)
    {
        $this->price = $price;
    
        return $this;
    }

    /**
     * Get price
     *
     * @return int
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * Set guid
     *
     * @param guid $guid
     *
     * @return InfoPromomaterial
     */
    public function setGuid($guid)
    {
        $this->guid = $guid;
    
        return $this;
    }

    /**
     * Get guid
     *
     * @return guid
     */
    public function getGuid()
    {
        return $this->guid;
    }

    /**
     * Set currenttime
     *
     * @param \DateTime $currenttime
     *
     * @return InfoPromomaterial
     */
    public function setCurrenttime(\DateTime $currenttime)
    {
        $this->currenttime = $currenttime;
    
        return $this;
    }

    /**
     * Get currenttime
     *
     * @return \DateTime
     */
    public function getCurrenttime()
    {
        return $this->currenttime;
    }

    /**
     * Set moduser
     *
     * @param string $moduser
     *
     * @return InfoPromomaterial
     */
    public function setModuser($moduser)
    {
        $this->moduser = $moduser;
    
        return $this;
    }

    /**
     * Get moduser
     *
     * @return string
     */
    public function getModuser()
    {
        return $this->moduser;
    }

    /**
     * Set iscontrol
     *
     * @param int $iscontrol
     *
     * @return InfoPromomaterial
     */
    public function setIscontrol($iscontrol)
    {
        $this->iscontrol = $iscontrol;
    
        return $this;
    }

    /**
     * Get iscontrol
     *
     * @return int
     */
    public function getIscontrol()
    {
        return $this->iscontrol;
    }

    /**
     * Set unit
     *
     * @param \TeamSoft\CrmRepositoryBundle\Entity\InfoDictionary $unit
     *
     * @return InfoPromomaterial
     */
    public function setUnit(\TeamSoft\CrmRepositoryBundle\Entity\InfoDictionary $unit = null)
    {
        $this->unit = $unit;
    
        return $this;
    }

    /**
     * Get unit
     *
     * @return \TeamSoft\CrmRepositoryBundle\Entity\InfoDictionary
     */
    public function getUnit()
    {
        return $this->unit;
    }

    /**
     * Set group
     *
     * @param \TeamSoft\CrmRepositoryBundle\Entity\InfoPromomaterialgroup $group
     *
     * @return InfoPromomaterial
     */
    public function setGroup(\TeamSoft\CrmRepositoryBundle\Entity\InfoPromomaterialgroup $group = null)
    {
        $this->group = $group;
    
        return $this;
    }

    /**
     * Get group
     *
     * @return \TeamSoft\CrmRepositoryBundle\Entity\InfoPromomaterialgroup
     */
    public function getGroup()
    {
        return $this->group;
    }

    /**
     * Set promomaterialtype
     *
     * @param \TeamSoft\CrmRepositoryBundle\Entity\InfoDictionary $promomaterialtype
     *
     * @return InfoPromomaterial
     */
    public function setPromomaterialtype(\TeamSoft\CrmRepositoryBundle\Entity\InfoDictionary $promomaterialtype = null)
    {
        $this->promomaterialtype = $promomaterialtype;
    
        return $this;
    }

    /**
     * Get promomaterialtype
     *
     * @return \TeamSoft\CrmRepositoryBundle\Entity\InfoDictionary
     */
    public function getPromomaterialtype()
    {
        return $this->promomaterialtype;
    }
}
