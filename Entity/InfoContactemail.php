<?php

namespace TeamSoft\CrmRepositoryBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * InfoContactemail
 *
 * @ORM\Table(name="info_contactemail")
 * @ORM\Entity
 * @ORM\EntityListeners({"TeamSoft\CrmRepositoryBundle\EventListener\InfoContactEmailListener"})
 */
class InfoContactemail implements ServiceFieldInterface
{

    public const STATUS_VALID = 1;
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="email", type="string", length=255, nullable=true)
     */
    private $email;

    /**
     * @var string
     *
     * @ORM\Column(name="guid", type="guid", nullable=true)
     */
    private $guid;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="currenttime", type="datetime", nullable=true)
     */
    private $currenttime;

    /**
     * @var string
     *
     * @ORM\Column(name="moduser", type="string", length=16, nullable=true)
     */
    private $moduser;

    /**
     * @var integer
     *
     * @ORM\Column(name="morionid", type="integer", nullable=true)
     */
    private $morionid;

    /**
     * @var string
     *
     * @ORM\Column(name="response", type="string", length=1000, nullable=true)
     */
    private $response;

    /**
     * @var integer
     *
     * @ORM\Column(name="status", type="integer", nullable=true)
     */
    private $status;

    /**
     * @var integer|null
     *
     * @ORM\Column(name="isvisibleincrm", type="integer", nullable=true)
     */
    private $isVisibleInCrm;

    /**
     * @var InfoContact
     *
     * @ORM\ManyToOne(targetEntity="InfoContact", inversedBy="contactEmailCollection")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="contact_id", referencedColumnName="id")
     * })
     */
    private $contact;

    /**
     * @var integer|null
     *
     * @ORM\Column(name="from_site", type="integer", nullable=true)
     */
    private $fromSite;

    /**
     * @var integer|null
     *
     * @ORM\Column(name="priority", type="integer", nullable=true)
     */
    private $priority;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set email
     *
     * @param string $email
     *
     * @return InfoContactemail
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Get email
     *
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set guid
     *
     * @param string $guid
     *
     * @return InfoContactemail
     */
    public function setGuid($guid)
    {
        $this->guid = $guid;

        return $this;
    }

    /**
     * Get guid
     *
     * @return string
     */
    public function getGuid()
    {
        return $this->guid;
    }

    /**
     * Set currenttime
     *
     * @param \DateTime $currenttime
     *
     * @return InfoContactemail
     */
    public function setCurrenttime($currenttime)
    {
        $this->currenttime = $currenttime;

        return $this;
    }

    /**
     * Get currenttime
     *
     * @return \DateTime
     */
    public function getCurrenttime()
    {
        return $this->currenttime;
    }

    /**
     * Set moduser
     *
     * @param string $moduser
     *
     * @return InfoContactemail
     */
    public function setModuser($moduser)
    {
        $this->moduser = $moduser;

        return $this;
    }

    /**
     * Get moduser
     *
     * @return string
     */
    public function getModuser()
    {
        return $this->moduser;
    }

    /**
     * Set morionid
     *
     * @param integer $morionid
     *
     * @return InfoContactemail
     */
    public function setMorionid($morionid)
    {
        $this->morionid = $morionid;

        return $this;
    }

    /**
     * Get morionid
     *
     * @return integer
     */
    public function getMorionid()
    {
        return $this->morionid;
    }

    /**
     * Set status
     *
     * @param int $status
     *
     * @return InfoContactemail
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return int
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set response
     *
     * @param string $response
     *
     * @return InfoContactemail
     */
    public function setResponse($response)
    {
        $this->response = $response;

        return $this;
    }

    /**
     * Get response
     *
     * @return string
     */
    public function getResponse()
    {
        return $this->response;
    }

    /**
     * Set contact
     *
     * @param \TeamSoft\CrmRepositoryBundle\Entity\InfoContact $contact
     *
     * @return InfoContactemail
     */
    public function setContact(\TeamSoft\CrmRepositoryBundle\Entity\InfoContact $contact = null)
    {
        $this->contact = $contact;

        return $this;
    }

    /**
     * Get contact
     *
     * @return \TeamSoft\CrmRepositoryBundle\Entity\InfoContact
     */
    public function getContact()
    {
        return $this->contact;
    }

    public function isEmailValid(): bool
    {
        return $this->getStatus() === self::STATUS_VALID;
    }

    /**
     * @return int|null
     */
    public function getIsVisibleInCrm(): ?int
    {
        return $this->isVisibleInCrm;
    }

    /**
     * @param int|null $isVisibleInCrm
     * @return self
     */
    public function setIsVisibleInCrm(?int $isVisibleInCrm): self
    {
        $this->isVisibleInCrm = $isVisibleInCrm;

        return $this;
    }

    /**
     * Set fromSite
     *
     * @param int $fromSite
     *
     * @return InfoContactemail
     */
    public function setFromSite($fromSite)
    {
        $this->fromSite = $fromSite;

        return $this;
    }

    /**
     * Get fromSite
     *
     * @return int
     */
    public function getFromSite()
    {
        return $this->fromSite;
    }

    /**
     * Set priority
     *
     * @param int $priority
     *
     * @return InfoContactemail
     */
    public function setPriority($priority)
    {
        $this->priority = $priority;

        return $this;
    }

    /**
     * Get priority
     *
     * @return int
     */
    public function getPriority()
    {
        return $this->priority;
    }
}
