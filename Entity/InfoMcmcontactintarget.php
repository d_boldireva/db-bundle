<?php

namespace TeamSoft\CrmRepositoryBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * InfoMcmcontactintarget
 *
 * @ORM\Table(name="info_mcmcontactintarget")
 * @ORM\Entity
 */
class InfoMcmcontactintarget implements ServiceFieldInterface
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var \TeamSoft\CrmRepositoryBundle\Entity\InfoMcmtarget
     *
     * @ORM\ManyToOne(targetEntity="\TeamSoft\CrmRepositoryBundle\Entity\InfoMcmtarget")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="target_id", referencedColumnName="id")
     * })
     */
    private $target;

    /**
     * @var \TeamSoft\CrmRepositoryBundle\Entity\InfoContact
     *
     * @ORM\ManyToOne(targetEntity="\TeamSoft\CrmRepositoryBundle\Entity\InfoContact")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="contact_id", referencedColumnName="id")
     * })
     */
    private $contact;

    /**
     * @var int
     *
     * @ORM\Column(name="isactive", type="integer", nullable=true)
     */
    private $isactive;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="datefrom", type="datetime", nullable=true)
     */
    private $datefrom;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="datetill", type="datetime", nullable=true)
     */
    private $datetill;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="currenttime", type="datetime", nullable=true)
     */
    private $currenttime;

    /**
     * @var string
     *
     * @ORM\Column(name="moduser", type="string", length=16, nullable=true)
     */
    private $moduser;

    /**
     * @var string
     *
     * @ORM\Column(name="guid", type="guid", nullable=true)
     */
    private $guid;

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set target
     *
     * @param \TeamSoft\CrmRepositoryBundle\Entity\InfoMcmtarget $target
     *
     * @return InfoMcmcontactintarget
     */
    public function setTarget(\TeamSoft\CrmRepositoryBundle\Entity\InfoMcmtarget $target)
    {
        $this->target = $target;
    
        return $this;
    }

    /**
     * Get target
     *
     * @return \TeamSoft\CrmRepositoryBundle\Entity\InfoMcmtarget
     */
    public function getTarget()
    {
        return $this->target;
    }

    /**
     * Set contact
     *
     * @param \TeamSoft\CrmRepositoryBundle\Entity\InfoContact $contact
     *
     * @return InfoMcmcontactintarget
     */
    public function setContact(\TeamSoft\CrmRepositoryBundle\Entity\InfoContact $contact)
    {
        $this->contact = $contact;
    
        return $this;
    }

    /**
     * Get contact
     *
     * @return int
     */
    public function getContact()
    {
        return $this->contact;
    }

    /**
     * Set isactive
     *
     * @param int $isactive
     *
     * @return InfoMcmcontactintarget
     */
    public function setIsactive($isactive)
    {
        $this->isactive = $isactive;
    
        return $this;
    }

    /**
     * Get isactive
     *
     * @return int
     */
    public function getIsactive()
    {
        return $this->isactive;
    }

    /**
     * Set datefrom
     *
     * @param \DateTime $datefrom
     *
     * @return InfoMcmcontactintarget
     */
    public function setDatefrom($datefrom)
    {
        $this->datefrom = $datefrom;
    
        return $this;
    }

    /**
     * Get datefrom
     *
     * @return \DateTime
     */
    public function getDatefrom()
    {
        return $this->datefrom;
    }

    /**
     * Set datetill
     *
     * @param \DateTime $datetill
     *
     * @return InfoMcmcontactintarget
     */
    public function setDatetill($datetill)
    {
        $this->datetill = $datetill;
    
        return $this;
    }

    /**
     * Get datetill
     *
     * @return \DateTime
     */
    public function getDatetill()
    {
        return $this->datetill;
    }

    /**
     * Set currenttime
     *
     * @param \DateTime $currenttime
     *
     * @return InfoMcmcontactintarget
     */
    public function setCurrenttime(\DateTime $currenttime)
    {
        $this->currenttime = $currenttime;
    
        return $this;
    }

    /**
     * Get currenttime
     *
     * @return \DateTime
     */
    public function getCurrenttime()
    {
        return $this->currenttime;
    }

    /**
     * Set moduser
     *
     * @param string $moduser
     *
     * @return InfoMcmcontactintarget
     */
    public function setModuser($moduser)
    {
        $this->moduser = $moduser;
    
        return $this;
    }

    /**
     * Get moduser
     *
     * @return string
     */
    public function getModuser()
    {
        return $this->moduser;
    }

    /**
     * Set guid
     *
     * @param string $guid
     *
     * @return InfoMcmcontactintarget
     */
    public function setGuid($guid)
    {
        $this->guid = $guid;
    
        return $this;
    }

    /**
     * Get guid
     *
     * @return string
     */
    public function getGuid()
    {
        return $this->guid;
    }
}
