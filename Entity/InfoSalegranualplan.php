<?php

namespace TeamSoft\CrmRepositoryBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * InfoSalegranualplan
 *
 * @ORM\Table(name="info_salegranualplan")
 * @ORM\Entity(repositoryClass="TeamSoft\CrmRepositoryBundle\Repository\InfoSalegranualplanRepository")
 */
class InfoSalegranualplan
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var float|null
     *
     * @ORM\Column(name="cnt", type="float", nullable=true)
     */
    private $cnt;

    /**
     * @var float|null
     *
     * @ORM\Column(name="cnt_auto", type="float", nullable=true)
     */
    private $cnt_auto;

    /**
     * @var string|null
     *
     * @ORM\Column(name="guid", type="guid", nullable=true)
     */
    private $guid;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="currenttime", type="datetime", nullable=true)
     */
    private $currenttime;

    /**
     * @var string|null
     *
     * @ORM\Column(name="moduser", type="string", length=16, nullable=true)
     */
    private $moduser;

    /**
     * @var InfoCompany
     *
     * @ORM\ManyToOne(targetEntity="InfoCompany")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="company_id", referencedColumnName="id")
     * })
     */
    private $company;

    /**
     * @var InfoPreparation
     *
     * @ORM\ManyToOne(targetEntity="InfoPreparation")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="preparation_id", referencedColumnName="id")
     * })
     */
    private $preparation;

    /**
     * @var InfoSaleperiod
     *
     * @ORM\ManyToOne(targetEntity="InfoSaleperiod")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="saleperiod_id", referencedColumnName="id")
     * })
     */
    private $saleperiod;

    /**
     * @var InfoUser
     *
     * @ORM\ManyToOne(targetEntity="InfoUser")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     * })
     */
    private $user;

    /**
     * @var InfoDirection
     *
     * @ORM\ManyToOne(targetEntity="InfoDirection")
     * @ORM\JoinColumn(name="direction_id", referencedColumnName="id")
     */
    private $direction;



    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set cnt.
     *
     * @param float|null $cnt
     *
     * @return InfoSalegranualplan
     */
    public function setCnt($cnt = null)
    {
        $this->cnt = $cnt;

        return $this;
    }

    /**
     * Get cnt.
     *
     * @return float|null
     */
    public function getCnt()
    {
        return $this->cnt;
    }

    /**
     * @param float $cnt_auto
     * @return $this
     */
    public function setCntAuto($cnt_auto = null){
        $this->cnt_auto = $cnt_auto;
        return $this;
    }

    /**
     * @return float|null
     */
    public function getCntAuto(){
        return $this->cnt_auto;
    }

    /**
     * Set guid.
     *
     * @param string|null $guid
     *
     * @return InfoSalegranualplan
     */
    public function setGuid($guid = null)
    {
        $this->guid = $guid;

        return $this;
    }

    /**
     * Get guid.
     *
     * @return string|null
     */
    public function getGuid()
    {
        return $this->guid;
    }

    /**
     * Set currenttime.
     *
     * @param \DateTime|null $currenttime
     *
     * @return InfoSalegranualplan
     */
    public function setCurrenttime($currenttime = null)
    {
        $this->currenttime = $currenttime;

        return $this;
    }

    /**
     * Get currenttime.
     *
     * @return \DateTime|null
     */
    public function getCurrenttime()
    {
        return $this->currenttime;
    }

    /**
     * Set moduser.
     *
     * @param string|null $moduser
     *
     * @return InfoSalegranualplan
     */
    public function setModuser($moduser = null)
    {
        $this->moduser = $moduser;

        return $this;
    }

    /**
     * Get moduser.
     *
     * @return string|null
     */
    public function getModuser()
    {
        return $this->moduser;
    }

    /**
     * Set company.
     *
     * @param InfoCompany|null $company
     *
     * @return InfoSalegranualplan
     */
    public function setCompany(InfoCompany $company = null)
    {
        $this->company = $company;

        return $this;
    }

    /**
     * Get company.
     *
     * @return \TeamSoft\CrmRepositoryBundle\Entity\InfoCompany|null
     */
    public function getCompany()
    {
        return $this->company;
    }

    /**
     * Set preparation.
     *
     * @param InfoPreparation|null $preparation
     *
     * @return InfoSalegranualplan
     */
    public function setPreparation(InfoPreparation $preparation = null)
    {
        $this->preparation = $preparation;

        return $this;
    }

    /**
     * Get preparation.
     *
     * @return InfoPreparation|null
     */
    public function getPreparation()
    {
        return $this->preparation;
    }

    /**
     * Set saleperiod.
     *
     * @param InfoSaleperiod|null $saleperiod
     *
     * @return InfoSalegranualplan
     */
    public function setSaleperiod(InfoSaleperiod $saleperiod = null)
    {
        $this->saleperiod = $saleperiod;

        return $this;
    }

    /**
     * Get saleperiod.
     *
     * @return InfoSaleperiod|null
     */
    public function getSaleperiod()
    {
        return $this->saleperiod;
    }

    /**
     * Set user.
     *
     * @param InfoUser|null $user
     *
     * @return InfoSalegranualplan
     */
    public function setUser(InfoUser $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user.
     *
     * @return InfoUser|null
     */
    public function getUser()
    {
        return $this->user;
    }
}
