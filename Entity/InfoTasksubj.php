<?php

namespace TeamSoft\CrmRepositoryBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * InfoTasksubj
 *
 * @ORM\Table(name="info_tasksubj")
 * @ORM\Entity
 */
class InfoTasksubj implements ServiceFieldInterface{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var InfoTask
     *
     * @ORM\ManyToOne(targetEntity="InfoTask")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="task_id", referencedColumnName="id")
     * })
     */
    private $task;

    /**
     * @var InfoTasksubject
     *
     * @ORM\ManyToOne(targetEntity="InfoTasksubject")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="tasksubject_id", referencedColumnName="id")
     * })
     */
    private $tasksubject;

    /**
     * @var \TeamSoft\CrmRepositoryBundle\Entity\InfoSmile
     *
     * @ORM\ManyToOne(targetEntity="InfoSmile")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="smile_id", referencedColumnName="id")
     * })
     */
    private $smile;

    /**
     * @var string
     *
     * @ORM\Column(name="guid", type="guid", nullable=true)
     */
    private $guid;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="currenttime", type="datetime", nullable=true)
     */
    private $currenttime;

    /**
     * @var string
     *
     * @ORM\Column(name="moduser", type="string", length=16, nullable=true)
     */
    private $moduser;

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Get task
     *
     * @return InfoTask
     */
    public function getTask(){
        return $this->task;
    }

    /**
     * Set Task
     *
     * @param \TeamSoft\CrmRepositoryBundle\Entity\InfoTask $task
     *
     * @return InfoTasksubj
     */
    public function setTask(\TeamSoft\CrmRepositoryBundle\Entity\InfoTask $task = null){
        $this->task = $task;

        return $this;
    }

    /**
     *  Get Tasksubject
     *
     * @return InfoTasksubject
     */
    public function getTasksubject(){
        return $this->tasksubject;
    }

    /**
     * Set tasksubject
     *
     * @param InfoTasksubject $tasksubject
     *
     * @return InfoTasksubj
     */
    public function setTasksubject(\TeamSoft\CrmRepositoryBundle\Entity\InfoTasksubject $tasksubject = null){
        $this->tasksubject = $tasksubject;

        return $this;
    }

    /**
     * Get smile
     *
     * @return string
     */
    public function getSmile()
    {
        return $this->smile;
    }

    /**
     * Set smile
     *
     * @param \TeamSoft\CrmRepositoryBundle\Entity\InfoSmile $smile
     *
     * @return InfoTasksubj
     */
    public function setSmile(\TeamSoft\CrmRepositoryBundle\Entity\InfoSmile $smile = null)
    {
        $this->smile = $smile;

        return $this;
    }

    /**
     * Get guid
     *
     * @return string
     */
    public function getGuid()
    {
        return $this->guid;
    }

    /**
     * Set guid
     *
     * @param string $guid
     *
     * @return InfoTasksubj
     */
    public function setGuid($guid)
    {
        $this->guid = $guid;

        return $this;
    }

    /**
     * Get currenttime
     *
     * @return \DateTime
     */
    public function getCurrenttime()
    {
        return $this->currenttime;
    }

    /**
     * Set currenttime
     *
     * @param \DateTime $currenttime
     *
     * @return InfoTasksubj
     */
    public function setCurrenttime(\DateTime $currenttime)
    {
        $this->currenttime = $currenttime;

        return $this;
    }

    /**
     * Get moduser
     *
     * @return string
     */
    public function getModuser()
    {
        return $this->moduser;
    }

    /**
     * Set moduser
     *
     * @param string $moduser
     *
     * @return InfoTasksubj
     */
    public function setModuser($moduser)
    {
        $this->moduser = $moduser;

        return $this;
    }

    /**
     * Get task id
     *
     * @return int
     */
    public function getTaskId(){
        return $this->task->getId();
    }

    /**
     * Get tasksubject id
     *
     * @return int
     */
    public function getTaskSubjectId(){
        return $this->getTasksubject() ? $this->getTasksubject()->getId() : null;
    }
}