<?php

namespace TeamSoft\CrmRepositoryBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use TeamSoft\CrmRepositoryBundle\Entity\SaleProject\InfoPricesaleperiod;

/**
 * InfoSaleperiod
 *
 * @ORM\Table(name="info_saleperiod")
 * @ORM\Entity(repositoryClass="TeamSoft\CrmRepositoryBundle\Repository\InfoSaleperiod")
 */
class InfoSaleperiod implements ServiceFieldInterface
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="datefrom", type="datetime", nullable=true)
     */
    private $datefrom;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="datetill", type="datetime", nullable=true)
     */
    private $datetill;

    /**
     * @var int|null
     *
     * @ORM\Column(name="is_discount", type="integer", nullable=true)
     */
    private $isDiscount;

    /**
     * @var ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="TeamSoft\CrmRepositoryBundle\Entity\SaleProject\InfoPricesaleperiod", mappedBy="saleperiod", cascade={"persist","remove"})
     */
    private $priceSalePeriods;

    /**
     * @var string|null
     *
     * @ORM\Column(name="guid", type="guid", nullable=true)
     */
    private $guid;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="currenttime", type="datetime", nullable=true)
     */
    private $currenttime;

    /**
     * @var string|null
     *
     * @ORM\Column(name="moduser", type="string", length=16, nullable=true)
     */
    private $moduser;

    /**
     * @var integer|null
     *
     * @ORM\Column(name="is_for_granual", type="integer", nullable=true)
     */
    private $isForGranual;

    public function __construct()
    {
        $this->priceSalePeriods = new ArrayCollection();
    }

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set datefrom.
     *
     * @param \DateTime|null $datefrom
     *
     * @return InfoSaleperiod
     */
    public function setDatefrom($datefrom = null)
    {
        $this->datefrom = $datefrom;

        return $this;
    }

    /**
     * Get datefrom.
     *
     * @return \DateTime|null
     */
    public function getDatefrom()
    {
        return $this->datefrom;
    }

    /**
     * Set datetill.
     *
     * @param \DateTime|null $datetill
     *
     * @return InfoSaleperiod
     */
    public function setDatetill($datetill = null)
    {
        $this->datetill = $datetill;

        return $this;
    }

    /**
     * Get datetill.
     *
     * @return \DateTime|null
     */
    public function getDatetill()
    {
        return $this->datetill;
    }

    /**
     * Set is discount
     *
     * @param int|null $isDiscount
     * @return $this
     */
    public function setIsDiscount($isDiscount)
    {
        $this->isDiscount = $isDiscount;
        return $this;
    }

    /**
     * Get is discount
     *
     * @return int|null
     */
    public function getIsDiscount()
    {
        return $this->isDiscount;
    }

    /**
     * Is discount
     *
     * @return bool
     */
    public function IsDiscount()
    {
        return intval($this->isDiscount) > 0;
    }

    /**
     * @return ArrayCollection
     */
    public function getPriceSalePeriods()
    {
        return $this->priceSalePeriods;
    }

    /**
     * @param InfoPricesaleperiod $priceSalePeriod
     * @return self
     */
    public function addPriceSalePeriod(InfoPricesaleperiod $priceSalePeriod): self
    {
        if (!$this->priceSalePeriods->contains($priceSalePeriod)) {
            $this->priceSalePeriods->add($priceSalePeriod);
            $priceSalePeriod->setSaleperiod($this);
        }

        return $this;
    }

    /**
     * @param InfoPricesaleperiod $priceSalePeriod
     * @return self
     */
    public function removePriceSalePeriod(InfoPricesaleperiod $priceSalePeriod): self
    {
        if ($this->priceSalePeriods->contains($priceSalePeriod)) {
            $this->priceSalePeriods->removeElement($priceSalePeriod);
            $priceSalePeriod->setSaleperiod(null);
        }

        return $this;
    }

    /**
     * Set guid.
     *
     * @param string|null $guid
     *
     * @return InfoSaleperiod
     */
    public function setGuid($guid = null)
    {
        $this->guid = $guid;

        return $this;
    }

    /**
     * Get guid.
     *
     * @return string|null
     */
    public function getGuid()
    {
        return $this->guid;
    }

    /**
     * Set currenttime.
     *
     * @param \DateTime|null $currenttime
     *
     * @return InfoSaleperiod
     */
    public function setCurrenttime(\DateTime $currenttime = null)
    {
        $this->currenttime = $currenttime;

        return $this;
    }

    /**
     * Get currenttime.
     *
     * @return \DateTime|null
     */
    public function getCurrenttime()
    {
        return $this->currenttime;
    }

    /**
     * Set moduser.
     *
     * @param string|null $moduser
     *
     * @return InfoSaleperiod
     */
    public function setModuser($moduser = null)
    {
        $this->moduser = $moduser;

        return $this;
    }

    /**
     * Get moduser.
     *
     * @return string|null
     */
    public function getModuser()
    {
        return $this->moduser;
    }

    public function getIsForGranual(): ?int
    {
        return $this->isForGranual;
    }

    public function setIsForGranual(?int $isForGranual): self
    {
        $this->isForGranual = $isForGranual;
        return $this;
    }
}
