<?php

namespace TeamSoft\CrmRepositoryBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * InfoContactowner
 *
 * @ORM\Table(name="info_contactowner")
 * @ORM\Entity(repositoryClass="TeamSoft\CrmRepositoryBundle\Repository\InfoContactowner")
 */
class InfoContactowner implements ServiceFieldInterface
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="guid", type="guid", nullable=true)
     */
    private $guid;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="currenttime", type="datetime", nullable=true)
     */
    private $currenttime;

    /**
     * @var string
     *
     * @ORM\Column(name="moduser", type="string", length=16, nullable=true)
     */
    private $moduser;

    /**
     * @var integer
     *
     * @ORM\Column(name="isuse", type="integer", nullable=true)
     */
    private $isuse;

    /**
     * @var integer
     *
     * @ORM\Column(name="ismust", type="integer", nullable=true)
     */
    private $ismust;

    /**
     * @var integer
     *
     * @ORM\Column(name="taskmultiplicity", type="integer", nullable=true)
     */
    private $taskmultiplicity;

    /**
     * @var InfoContact
     *
     * @ORM\ManyToOne(targetEntity="InfoContact", inversedBy="contactOwnerCollection")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="contact_id", referencedColumnName="id")
     * })
     */
    private $contact;

    /**
     * @var InfoUser
     *
     * @ORM\ManyToOne(targetEntity="InfoUser")
     * @ORM\JoinColumn(name="owner_id", referencedColumnName="id")
     */
    private $owner;

    /**
     * @var integer
     *
     * @ORM\Column(name="is_main_owner", type="integer", nullable=true)
     */
    private $isMainOwner;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set guid
     *
     * @param string $guid
     * @return InfoContactowner
     */
    public function setGuid($guid)
    {
        $this->guid = $guid;

        return $this;
    }

    /**
     * Get guid
     *
     * @return string
     */
    public function getGuid()
    {
        return $this->guid;
    }

    /**
     * Set currenttime
     *
     * @param \DateTime $currenttime
     * @return InfoContactowner
     */
    public function setCurrenttime(\DateTime $currenttime)
    {
        $this->currenttime = $currenttime;

        return $this;
    }

    /**
     * Get currenttime
     *
     * @return \DateTime
     */
    public function getCurrenttime()
    {
        return $this->currenttime;
    }

    /**
     * Set moduser
     *
     * @param string $moduser
     * @return InfoContactowner
     */
    public function setModuser($moduser)
    {
        $this->moduser = $moduser;

        return $this;
    }

    /**
     * Get moduser
     *
     * @return string
     */
    public function getModuser()
    {
        return $this->moduser;
    }

    /**
     * Set isuse
     *
     * @param integer $isuse
     * @return InfoContactowner
     */
    public function setIsuse($isuse)
    {
        $this->isuse = $isuse;

        return $this;
    }

    /**
     * Get isuse
     *
     * @return integer
     */
    public function getIsuse()
    {
        return $this->isuse;
    }

    /**
     * Set ismust
     *
     * @param integer $ismust
     * @return InfoContactowner
     */
    public function setIsmust($ismust)
    {
        $this->ismust = $ismust;

        return $this;
    }

    /**
     * Get ismust
     *
     * @return integer
     */
    public function getIsmust()
    {
        return $this->ismust;
    }

    /**
     * Set taskmultiplicity
     *
     * @param integer $taskmultiplicity
     * @return InfoContactowner
     */
    public function setTaskmultiplicity($taskmultiplicity)
    {
        $this->taskmultiplicity = $taskmultiplicity;

        return $this;
    }

    /**
     * Get taskmultiplicity
     *
     * @return integer
     */
    public function getTaskmultiplicity()
    {
        return $this->taskmultiplicity;
    }

    /**
     * Set contact
     *
     * @param \TeamSoft\CrmRepositoryBundle\Entity\InfoContact $contact
     * @return InfoContactowner
     */
    public function setContact(InfoContact $contact = null)
    {
        $this->contact = $contact;

        return $this;
    }

    /**
     * Get contact
     *
     * @return \TeamSoft\CrmRepositoryBundle\Entity\InfoContact
     */
    public function getContact()
    {
        return $this->contact;
    }

    /**
     * Set owner
     *
     * @param \TeamSoft\CrmRepositoryBundle\Entity\InfoUser $owner
     * @return InfoContactowner
     */
    public function setOwner(InfoUser $owner = null)
    {
        $this->owner = $owner;

        return $this;
    }

    /**
     * Get owner
     *
     * @return \TeamSoft\CrmRepositoryBundle\Entity\InfoUser
     */
    public function getOwner()
    {
        return $this->owner;
    }

    /**
     * Set isMainOwner
     *
     * @param integer $isMainOwner
     * @return InfoContactowner
     */
    public function setIsMainOwner($isMainOwner)
    {
        $this->isMainOwner = $isMainOwner;

        return $this;
    }

    /**
     * Get isMainOwner
     *
     * @return integer
     */
    public function getIsMainOwner()
    {
        return $this->isMainOwner;
    }
}
