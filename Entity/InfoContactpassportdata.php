<?php

namespace TeamSoft\CrmRepositoryBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\JoinColumn;
use Doctrine\ORM\Mapping\OneToMany;
use Doctrine\ORM\Mapping\OneToOne;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * InfoContactpassportdata
 *
 * @ORM\Table(name="info_Contactpassportdata")
 * @ORM\Entity
 */
class InfoContactpassportdata
{

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     *
     * @var InfoContact
     *
     * @ORM\OneToOne(targetEntity="InfoContact")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="contact_id", referencedColumnName="id")
     * })
     */
    private $contact;

    /**
     * @var string
     *
     * @ORM\Column(name="articletitles", type="string", nullable=true)
     */
    private $articleTitles;

    /**
     * @var string
     *
     * @ORM\Column(name="communityname", type="string", nullable=true)
     */
    private $communityName;

    /**
     * @var string
     *
     * @ORM\Column(name="guid", type="guid", nullable=true)
     */
    private $guid;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="currenttime", type="datetime", nullable=true)
     */
    private $currenttime;

    /**
     * @var string
     *
     * @ORM\Column(name="moduser", type="string", length=16, nullable=true)
     */
    private $moduser;

    /**
     * @var InfoCustomdictionaryvalue
     *
     * @ORM\ManyToOne(targetEntity="InfoCustomdictionaryvalue")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="academicrank_id", referencedColumnName="id")
     * })
     */
    private $academicRank;

    /**
     * @var InfoCustomdictionaryvalue
     *
     * @ORM\ManyToOne(targetEntity="InfoCustomdictionaryvalue")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="academicdegree_id", referencedColumnName="id")
     * })
     */
    private $academicDegree;

    /**
     * @var InfoCustomdictionaryvalue
     *
     * @ORM\ManyToOne(targetEntity="InfoCustomdictionaryvalue")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="stateemployee_id", referencedColumnName="id")
     * })
     */
    private $isStateEmployee;

    /**
     * @var InfoPreparation
     *
     * @ORM\ManyToOne(targetEntity="InfoPreparation")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="preparation_id", referencedColumnName="id")
     * })
     */
    private $preparation;

    /**
     * @var InfoCustomdictionaryvalue
     *
     * @ORM\ManyToOne(targetEntity="InfoCustomdictionaryvalue")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="appointmentexperience_id", referencedColumnName="id")
     * })
     */
    private $appointmentExperience;

    /**
     *
     *
     * @ORM\Column(name="patientamount", type="integer", nullable=true)
     * })
     */
    private $patientAmount;

    /**
     * @var InfoCustomdictionaryvalue
     *
     * @ORM\ManyToOne(targetEntity="InfoCustomdictionaryvalue")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="lecturingexperience_id", referencedColumnName="id")
     * })
     */
    private $lecturingExperience;

    /**
     * @var InfoCustomdictionaryvalue
     *
     * @ORM\ManyToOne(targetEntity="InfoCustomdictionaryvalue")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="webinarexperience_id", referencedColumnName="id")
     * })
     */
    private $webinarHostExperience;

    /**
     * @var InfoCustomdictionaryvalue
     *
     * @ORM\ManyToOne(targetEntity="InfoCustomdictionaryvalue")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="articleexperience_id", referencedColumnName="id")
     * })
     */
    private $articleWritingExperience;

    /**
     * @var InfoCustomdictionaryvalue
     *
     * @ORM\ManyToOne(targetEntity="InfoCustomdictionaryvalue")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="communitymember_id", referencedColumnName="id")
     * })
     */
    private $communityMember;

    /**
     * @var InfoContactpassportdatavalue[]
     * @OneToMany(targetEntity="InfoContactpassportdatavalue", mappedBy="passportData", cascade={"persist"}, orphanRemoval=true)
     */
    private $contactPassportDataValues;

    /**
     *
     *
     * @ORM\Column(name="hobby", type="string", nullable=true)
     */
    private $hobby;

    /**
     *
     *
     * @ORM\Column(name="advisedbycolleagues", type="string", nullable=true)
     */
    private $advisedByColleagues;

    /**
     *
     *
     * @ORM\Column(name="territory", type="string", nullable=true)
     */
    private $territory;

    /**
     * @var InfoCustomdictionaryvalue
     *
     * @ORM\ManyToOne(targetEntity="InfoCustomdictionaryvalue")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="recomendation_id", referencedColumnName="id")
     * })
     */
    private $recomendations;

    /**
     * @return InfoCustomdictionaryvalue
     */
    public function getRecomendations()
    {
        return $this->recomendations;
    }

    /**
     * @param InfoCustomdictionaryvalue $recomendations
     *
     * @return InfoContactpassportdata
     */
    public function setRecomendations(InfoCustomdictionaryvalue $recomendations): self
    {
        $this->recomendations = $recomendations;

        return $this;
    }

    /**
     * @return string
     */
    public function getHobby()
    {
        return $this->hobby;
    }

    /**
     *
     */
    public function setHobby($hobby): self
    {
        $this->hobby = $hobby;

        return $this;
    }

    /**
     * @return string
     */
    public function getAdvisedByColleagues()
    {
        return $this->advisedByColleagues;
    }

    /**
     * @param string $advisedByColleagues
     *
     * @return InfoContactpassportdata
     */
    public function setAdvisedByColleagues($advisedByColleagues): self
    {
        $this->advisedByColleagues = $advisedByColleagues;

        return $this;
    }


    public function getContactId()
    {
        return $this->contact ? $this->contact->getId() : null;
    }

    /**
     * @return InfoContact
     */
    public function getContact(): ?InfoContact
    {
        return $this->contact;
    }

    /**
     * @param InfoContact $contact
     *
     * @return InfoContactpassportdata
     */
    public function setContact(InfoContact $contact): self
    {
        $this->contact = $contact;

        return $this;
    }

    /**
     * @return ArrayCollection
     */
    public function getContactPassportDataValues()
    {
        return $this->contactPassportDataValues;
    }

    /**
     * Add $contactPassportDataValue
     *
     * @param \TeamSoft\CrmRepositoryBundle\Entity\InfoContactpassportdatavalue $contactPassportDataValue
     *
     * @return InfoContactpassportdata
     */
    public function addContactPassportDataValue(\TeamSoft\CrmRepositoryBundle\Entity\InfoContactpassportdatavalue $contactPassportDataValue): self
    {
        $this->contactPassportDataValues[] = $contactPassportDataValue;
        $contactPassportDataValue->setPassportData($this);

        return $this;
    }

    /**
     * Remove $contactPassportDataValue
     *
     * @param \TeamSoft\CrmRepositoryBundle\Entity\ $contactPassportDataValue $contactPassportDataValue
     *
     */
    public function removeContactPassportDataValue(\TeamSoft\CrmRepositoryBundle\Entity\InfoContactpassportdatavalue $contactPassportDataValue)
    {
        $this->contactPassportDataValues->removeElement($contactPassportDataValue);
    }


    /**
     * Constructor
     */
    public function __construct()
    {
        $this->contactPassportDataValues = new ArrayCollection();
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getArticleTitles(): string
    {
        return $this->articleTitles;
    }

    /**
     * @param string $articleTitles
     *
     * @return InfoContactpassportdata
     */
    public function setArticleTitles($articleTitles): self
    {
        $this->articleTitles = $articleTitles;

        return $this;
    }

    /**
     * @return string
     */
    public function getCommunityName(): string
    {
        return $this->communityName;
    }

    /**
     * @param string $communityName
     *
     * @return InfoContactpassportdata
     */
    public function setCommunityName($communityName): self
    {
        $this->communityName = $communityName;

        return $this;
    }

    /**
     * @return string
     */
    public function getGuid()
    {
        return $this->guid;
    }

    /**
     * @param string $guid
     *
     * @return InfoContactpassportdata
     */
    public function setGuid(string $guid): self
    {
        $this->guid = $guid;

        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getCurrenttime()
    {
        return $this->currenttime;
    }

    /**
     * @param \DateTime $currenttime
     *
     * @return InfoContactpassportdata
     */
    public function setCurrenttime(\DateTime $currenttime): self
    {
        $this->currenttime = $currenttime;

        return $this;
    }

    /**
     * @return string
     */
    public function getModuser(): string
    {
        return $this->moduser;
    }

    /**
     * @param string $moduser
     *
     * @return InfoContactpassportdata
     */
    public function setModuser(string $moduser): self
    {
        $this->moduser = $moduser;

        return $this;
    }

    /**
     * @return InfoCustomdictionaryvalue
     */
    public function getAcademicRank()
    {
        return $this->academicRank;
    }

    /**
     * @param InfoCustomdictionaryvalue $academicRank
     *
     * @return InfoContactpassportdata
     */
    public function setAcademicRank(InfoCustomdictionaryvalue $academicRank): self
    {
        $this->academicRank = $academicRank;

        return $this;
    }

    /**
     * @return InfoCustomdictionaryvalue
     */
    public function getAcademicDegree()
    {
        return $this->academicDegree;
    }

    /**
     * @param InfoCustomdictionaryvalue $academicDegree
     *
     * @return InfoContactpassportdata
     */
    public function setAcademicDegree(InfoCustomdictionaryvalue $academicDegree): self
    {
        $this->academicDegree = $academicDegree;

        return $this;
    }

    /**
     * @return InfoCustomdictionaryvalue
     */
    public function getIsStateEmployee()
    {
        return $this->isStateEmployee;
    }

    /**
     * @param InfoCustomdictionaryvalue $isStateEmployee
     *
     * @return InfoContactpassportdata
     */
    public function setIsStateEmployee(InfoCustomdictionaryvalue $isStateEmployee): self
    {
        $this->isStateEmployee = $isStateEmployee;

        return $this;
    }

    /**
     * @return InfoPreparation
     */
    public function getPreparation()
    {
        return $this->preparation;
    }

    /**
     * @param InfoPreparation $preparation
     *
     * @return InfoContactpassportdata
     */
    public function setPreparation(InfoPreparation $preparation): self
    {
        $this->preparation = $preparation;

        return $this;
    }

    /**
     * @return InfoCustomdictionaryvalue
     */
    public function getAppointmentExperience()
    {
        return $this->appointmentExperience;
    }

    /**
     * @param InfoCustomdictionaryvalue $appointmentExperience
     *
     * @return InfoContactpassportdata
     */
    public function setAppointmentExperience(InfoCustomdictionaryvalue $appointmentExperience): self
    {
        $this->appointmentExperience = $appointmentExperience;

        return $this;
    }

    /**
     * @return int
     */
    public function getPatientAmount()
    {
        return $this->patientAmount;
    }

    /**
     * @param int $patientAmount
     *
     * @return InfoContactpassportdata
     */
    public function setPatientAmount($patientAmount): self
    {
        $this->patientAmount = $patientAmount;

        return $this;
    }

    /**
     * @return InfoCustomdictionaryvalue
     */
    public function getLecturingExperience()
    {
        return $this->lecturingExperience;
    }

    /**
     * @param InfoCustomdictionaryvalue $lecturingExperience
     *
     * @return InfoContactpassportdata
     */
    public function setLecturingExperience(InfoCustomdictionaryvalue $lecturingExperience): self
    {
        $this->lecturingExperience = $lecturingExperience;

        return $this;
    }

    /**
     * @return InfoCustomdictionaryvalue
     */
    public function getWebinarHostExperience()
    {
        return $this->webinarHostExperience;
    }

    /**
     * @param InfoCustomdictionaryvalue $webinarHostExperience
     *
     * @return InfoContactpassportdata
     */
    public function setWebinarHostExperience(InfoCustomdictionaryvalue $webinarHostExperience): self
    {
        $this->webinarHostExperience = $webinarHostExperience;

        return $this;
    }

    /**
     * @return InfoCustomdictionaryvalue
     */
    public function getArticleWritingExperience()
    {
        return $this->articleWritingExperience;
    }

    /**
     * @param InfoCustomdictionaryvalue $articleWritingExperience
     *
     * @return InfoContactpassportdata
     */
    public function setArticleWritingExperience(InfoCustomdictionaryvalue $articleWritingExperience): self
    {
        $this->articleWritingExperience = $articleWritingExperience;

        return $this;
    }

    /**
     * @return InfoCustomdictionaryvalue
     */
    public function getCommunityMember()
    {
        return $this->communityMember;
    }

    /**
     * @param InfoCustomdictionaryvalue $communityMember
     *
     * @return InfoContactpassportdata
     */
    public function setCommunityMember(InfoCustomdictionaryvalue $communityMember): self
    {
        $this->communityMember = $communityMember;

        return $this;
    }

    /**
     * @return string
     */
    public function getTerritory()
    {
        return $this->territory;
    }

    /**
     * @param  $territory
     *
     * @return InfoContactpassportdata
     */
    public function setTerritory($territory): self
    {
        $this->territory = $territory;

        return $this;
    }
}
