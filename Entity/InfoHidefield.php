<?php

namespace TeamSoft\CrmRepositoryBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * InfoHidefield
 *
 * @ORM\Table(name="info_hidefield")
 * @ORM\Entity(repositoryClass="TeamSoft\CrmRepositoryBundle\Repository\InfoHidefield")
 */
class InfoHidefield implements ServiceFieldInterface
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var int|null
     *
     * @ORM\Column(name="page", type="integer", nullable=true)
     */
    private $page;

    /**
     * @var string|null
     *
     * @ORM\Column(name="name", type="string", length=255, nullable=true)
     */
    private $name;

    /**
     * @var string|null
     *
     * @ORM\Column(name="entitytype", type="string", length=255, nullable=true)
     */
    private $entitytype;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="currenttime", type="datetime", nullable=true)
     */
    private $currenttime;

    /**
     * @var string|null
     *
     * @ORM\Column(
     *     name="moduser",
     *     type="string",
     *     length=16,
     *     nullable=true,
     *     options={"default"="[dbo].[Get_CurrentCode]()"}
     * )
     */
    private $moduser;

    /**
     * @var string|null
     *
     * @ORM\Column(name="guid", type="guid", nullable=true)
     */
    private $guid;

    /**
     * @var InfoRole
     *
     * @ORM\ManyToOne(targetEntity="InfoRole", inversedBy="hiddenFields")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="role_id", referencedColumnName="id")
     * })
     */
    private $role;

    public function __clone()
    {
        $this->id = null;
        $this->guid = null;
        $this->currenttime = null;
    }

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set page.
     *
     * @param int|null $page
     *
     * @return InfoHidefield
     */
    public function setPage($page = null)
    {
        $this->page = $page;
    
        return $this;
    }

    /**
     * Get page.
     *
     * @return int|null
     */
    public function getPage()
    {
        return $this->page;
    }

    /**
     * Set name.
     *
     * @param string|null $name
     *
     * @return InfoHidefield
     */
    public function setName($name = null)
    {
        $this->name = $name;
    
        return $this;
    }

    /**
     * Get name.
     *
     * @return string|null
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set entitytype.
     *
     * @param string|null $entitytype
     *
     * @return InfoHidefield
     */
    public function setEntitytype($entitytype = null)
    {
        $this->entitytype = strtolower($entitytype);
    
        return $this;
    }

    /**
     * Get entitytype.
     *
     * @return string|null
     */
    public function getEntitytype()
    {
        return strtolower($this->entitytype);
    }

    /**
     * Set currenttime.
     *
     * @param \DateTime|null $currenttime
     *
     * @return InfoHidefield
     */
    public function setCurrenttime(\DateTime $currenttime = null)
    {
        $this->currenttime = $currenttime;
    
        return $this;
    }

    /**
     * Get currenttime.
     *
     * @return \DateTime|null
     */
    public function getCurrenttime()
    {
        return $this->currenttime;
    }

    /**
     * Set moduser.
     *
     * @param string|null $moduser
     *
     * @return InfoHidefield
     */
    public function setModuser($moduser = null)
    {
        $this->moduser = $moduser;
    
        return $this;
    }

    /**
     * Get moduser.
     *
     * @return string|null
     */
    public function getModuser()
    {
        return $this->moduser;
    }

    /**
     * Set guid.
     *
     * @param string|null $guid
     *
     * @return InfoHidefield
     */
    public function setGuid($guid = null)
    {
        $this->guid = $guid;
    
        return $this;
    }

    /**
     * Get guid.
     *
     * @return string|null
     */
    public function getGuid()
    {
        return $this->guid;
    }

    /**
     * Set role
     *
     * @param InfoRole $role
     * @return InfoHidefield
     */
    public function setRole(InfoRole $role = null)
    {
        $this->role = $role;

        return $this;
    }

    /**
     * Get role
     *
     * @return InfoRole
     */
    public function getRole()
    {
        return $this->role;
    }
}
