<?php

namespace TeamSoft\CrmRepositoryBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * InfoMcmdistributionaction
 *
 * @ORM\Table(name="info_mcmdistributionaction")
 * @ORM\Entity(repositoryClass="TeamSoft\CrmRepositoryBundle\Repository\InfoMcmdistributionaction")
 */
class InfoMcmdistributionaction implements ServiceFieldInterface, DateTimeWithGmtOffsetInterface
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var \TeamSoft\CrmRepositoryBundle\Entity\InfoMcmdistribution
     *
     * @ORM\ManyToOne(targetEntity="InfoMcmdistribution")
     * @ORM\JoinColumn(name="distribution_id", referencedColumnName="id")
     */
    private $distribution;

    /**
     * @var \TeamSoft\CrmRepositoryBundle\Entity\InfoMcmtrigger
     *
     * @ORM\ManyToOne(targetEntity="InfoMcmtrigger")
     * @ORM\JoinColumn(name="trigger_id", referencedColumnName="id")
     */
    private $trigger;

    /**
     * @var \TeamSoft\CrmRepositoryBundle\Entity\InfoContact $contact
     * @ORM\ManyToOne(targetEntity="InfoContact")
     * @ORM\JoinColumn(name="contact_id", referencedColumnName="id")
     */
    private $contact;

    /**
     * @var \TeamSoft\CrmRepositoryBundle\Entity\InfoMcmconferencecontact $conferenceContact
     * @ORM\ManyToOne(targetEntity="InfoMcmconferencecontact")
     * @ORM\JoinColumn(name="conferencecontact_id", referencedColumnName="id")
     */
    private $conferenceContact;

    ////// https://teamsoft.atlassian.net/browse/MCM-417
    /**
     * @var \TeamSoft\CrmRepositoryBundle\Entity\InfoTask $task
     *
     * @ORM\ManyToOne(targetEntity="\TeamSoft\CrmRepositoryBundle\Entity\InfoTask")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="task_id", referencedColumnName="id")
     * })
     */
    ////// temporarily
    /**
     * @var int
     *
     * @ORM\Column(name="task_id", type="integer", nullable=true)
     */
    private $task;

    /**
     * @var string
     *
     * @ORM\Column(name="status", type="string", length=255, nullable=true)
     */
    private $status;

    /**
     * @var string
     *
     * @ORM\Column(name="reason", type="string", length=255, nullable=true)
     */
    private $reason;

    /**
     * @var string
     *
     * @ORM\Column(name="recipient", type="string", length=255, nullable=true)
     */
    private $recipient;

    /**
     * @var string
     *
     * @ORM\Column(name="request_id", type="string", length=255, nullable=true)
     */
    private $requestId;

    /**
     * @var int
     *
     * @ORM\Column(name="failed", type="integer", nullable=true)
     */
    private $failed;

    /**
     * @var int
     *
     * @ORM\Column(name="delivered", type="integer", nullable=true)
     */
    private $delivered;

    /**
     * @var int
     *
     * @ORM\Column(name="messenger_id", type="integer", nullable=true)
     */
    private $messenger;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="createdate", type="datetime", nullable=true)
     */
    private $createdate;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="senddate", type="datetime", nullable=true)
     */
    private $senddate;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="currenttime", type="datetime", nullable=true)
     */
    private $currenttime;

    /**
     * @var string
     *
     * @ORM\Column(name="moduser", type="string", length=16, nullable=true)
     */
    private $moduser;

    /**
     * @var string
     *
     * @ORM\Column(name="guid", type="guid", nullable=true)
     */
    private $guid;

    /**
     * @var \TeamSoft\CrmRepositoryBundle\Entity\InfoUser
     *
     * @ORM\ManyToOne(targetEntity="\TeamSoft\CrmRepositoryBundle\Entity\InfoUser")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     * })
     */
    private $user;

    /**
     * @var float|null
     *
     * @ORM\Column(name="price", type="float", nullable=true)
     */
    private $price;

    /**
     * @var integer
     *
     * @ORM\Column(name="gmt_offset", type="integer", nullable=true)
     */
    private $gmtOffset;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="final_status_date", type="datetime", nullable=true)
     */
    private $finalStatusDate;

    /**
     * @var InfoMcmcontent
     *
     * @ORM\ManyToOne(targetEntity="InfoMcmcontent")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="content_id", referencedColumnName="id")
     * })
     */
    private $content;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="del_status_at", type="datetime", nullable=true)
     */
    private $delStatusAt;

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set status
     *
     * @param string $status
     *
     * @return InfoMcmdistributionaction
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return string
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set reason
     *
     * @param string $reason
     *
     * @return InfoMcmdistributionaction
     */
    public function setReason($reason)
    {

        $this->reason = strlen($reason) > 255 ? substr($reason, 0, 255) : $reason;

        return $this;
    }

    /**
     * Get reason
     *
     * @return string
     */
    public function getReason()
    {
        return $this->reason;
    }

    /**
     * Set recipient
     *
     * @param string $recipient
     *
     * @return InfoMcmdistributionaction
     */
    public function setRecipient($recipient)
    {

        $this->recipient = strlen($recipient) > 255 ? substr($recipient, 0, 255) : $recipient;

        return $this;
    }

    /**
     * Get recipient
     *
     * @return string
     */
    public function getRecipient()
    {
        return $this->recipient;
    }

    /**
     * Set requestId
     *
     * @param string $requestId
     *
     * @return InfoMcmdistributionaction
     */
    public function setRequestId($requestId)
    {
        $this->requestId = $requestId;

        return $this;
    }

    /**
     * Get requestId
     *
     * @return string
     */
    public function getRequestId()
    {
        return $this->requestId;
    }

    /**
     * Set failed
     *
     * @param boolean $failed
     *
     * @return InfoMcmdistributionaction
     */
    public function setFailed($failed)
    {
        $this->failed = $failed;

        return $this;
    }

    /**
     * Get failed
     *
     * @return boolean
     */
    public function getFailed()
    {
        return $this->failed;
    }

    /**
     * Set delivered
     *
     * @param boolean $delivered
     *
     * @return InfoMcmdistributionaction
     */
    public function setDelivered($delivered)
    {
        $this->delivered = $delivered;

        return $this;
    }

    /**
     * Get delivered
     *
     * @return boolean
     */
    public function getDelivered()
    {
        return $this->delivered;
    }

    /**
     * Set messenger
     *
     * @param int $messenger
     *
     * @return InfoMcmdistributionaction
     */
    public function setMessenger($messenger)
    {
        $this->messenger = $messenger;

        return $this;
    }

    /**
     * Get messenger
     *
     * @return int
     */
    public function getMessenger()
    {
        return $this->messenger;
    }

    /**
     * Set createdate
     *
     * @param \DateTime $createdate
     *
     * @return InfoMcmdistributionaction
     */
    public function setCreatedate($createdate)
    {
        $this->createdate = $createdate;

        return $this;
    }

    /**
     * Get createdate
     *
     * @return \DateTime
     */
    public function getCreatedate()
    {
        return $this->createdate;
    }

    /**
     * Set senddate
     *
     * @param \DateTime $senddate
     *
     * @return InfoMcmdistributionaction
     */
    public function setSenddate($senddate)
    {
        $this->senddate = $senddate;

        return $this;
    }

    /**
     * Get senddate
     *
     * @return \DateTime
     */
    public function getSenddate()
    {
        return $this->senddate;
    }

    /**
     * Set currenttime
     *
     * @param \DateTime $currenttime
     *
     * @return InfoMcmdistributionaction
     */
    public function setCurrenttime(\DateTime $currenttime)
    {
        $this->currenttime = $currenttime;

        return $this;
    }

    /**
     * Get currenttime
     *
     * @return \DateTime
     */
    public function getCurrenttime()
    {
        return $this->currenttime;
    }

    /**
     * Set moduser
     *
     * @param string $moduser
     *
     * @return InfoMcmdistributionaction
     */
    public function setModuser($moduser)
    {
        $this->moduser = $moduser;

        return $this;
    }

    /**
     * Get moduser
     *
     * @return string
     */
    public function getModuser()
    {
        return $this->moduser;
    }

    /**
     * Set guid
     *
     * @param string $guid
     *
     * @return InfoMcmdistributionaction
     */
    public function setGuid($guid)
    {
        $this->guid = $guid;

        return $this;
    }

    /**
     * Get guid
     *
     * @return string
     */
    public function getGuid()
    {
        return $this->guid;
    }

    /**
     * Set distribution
     *
     * @param InfoMcmdistribution $distribution
     *
     * @return InfoMcmdistributionaction
     */
    public function setDistribution(InfoMcmdistribution $distribution = null)
    {
        $this->distribution = $distribution;

        return $this;
    }

    /**
     * Get distribution
     *
     * @return InfoMcmdistribution
     */
    public function getDistribution()
    {
        return $this->distribution;
    }

    /**
     * @return int|null
     */
    public function getDistributionId(){
        return $this->distribution ? $this->distribution->getId() : null;
    }

    /**
     * Set trigger
     *
     * @param InfoMcmtrigger $trigger
     *
     * @return InfoMcmdistributionaction
     */
    public function setTrigger(InfoMcmtrigger $trigger = null)
    {
        $this->trigger = $trigger;

        return $this;
    }

    /**
     * Get trigger
     *
     * @return InfoMcmtrigger
     */
    public function getTrigger()
    {
        return $this->trigger;
    }

    /**
     * Get task
     *
     * @return int //\TeamSoft\CrmRepositoryBundle\Entity\InfoTask
     */
    public function getTask()
    {
        return $this->task;
    }

    /**
     * Set task
     *
     * @param int $taskId //\TeamSoft\CrmRepositoryBundle\Entity\InfoTask $task
     *
     * @return InfoMcmdistributionaction
     */
    public function setTask(int $taskId)
    {
        $this->task = $taskId;

        return $this;
    }

    /**
     * Get contact
     *
     * @return InfoContact
     */
    public function getContact()
    {
        return $this->contact;
    }

    /**
     * Set contact
     *
     * @param InfoContact $contact
     *
     * @return InfoMcmdistributionaction
     */
    public function setContact(InfoContact $contact = null)
    {
        $this->contact = $contact;

        return $this;
    }

    /**
     * Get conferenceContact
     *
     * @return InfoMcmconferencecontact
     */
    public function getConferenceContact()
    {
        return $this->conferenceContact;
    }

    /**
     * Set conferenceContact
     *
     * @param InfoMcmconferencecontact $conferenceContact
     *
     * @return InfoMcmdistributionaction
     */
    public function setConferenceContact(InfoMcmconferencecontact $conferenceContact = null)
    {
        $this->conferenceContact = $conferenceContact;

        return $this;
    }

    /**
     * Set user
     *
     * @param InfoUser $user
     *
     * @return InfoMcmdistributionaction
     */
    public function setUser(InfoUser $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return InfoUser
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Set price
     *
     * @param float $price
     *
     * @return InfoMcmdistributionaction
     */
    public function setPrice($price)
    {
        $this->price = $price;

        return $this;
    }

    /**
     * Get price
     *
     * @return float
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * Set gmtOffset.
     *
     * @param int|null $gmtOffset
     *
     * @return InfoMcmdistributionaction
     */
    public function setGmtOffset(int $gmtOffset = null)
    {
        $this->gmtOffset = $gmtOffset;

        return $this;
    }

    /**
     * Get gmtOffset.
     *
     * @return int|null
     */
    public function getGmtOffset()
    {
        return $this->gmtOffset;
    }

    public function getDateTimeWithGmtOffsetPropertyNameList()
    {
        return ['senddate'];
    }

    /**
     * Set finalStatusDate
     *
     * @param \DateTime $finalStatusDate
     *
     * @return InfoMcmdistributionaction
     */
    public function setFinalStatusDate($finalStatusDate)
    {
        $this->finalStatusDate = $finalStatusDate;

        return $this;
    }

    /**
     * Get finalStatusDate
     *
     * @return \DateTime
     */
    public function getFinalStatusDate()
    {
        return $this->finalStatusDate;
    }

    /**
     * Set content
     *
     * @param InfoMcmcontent $content
     *
     * @return InfoMcmdistributionaction
     */
    public function setMcmcontent(InfoMcmcontent $content = null)
    {
        $this->content = $content;

        return $this;
    }

    /**
     * Get content
     *
     * @return InfoMcmcontent
     */
    public function getMcmcontent()
    {
        return $this->content;
    }

    /**
     * Set delStatusAt
     *
     * @param \DateTime $delStatusAt
     *
     * @return InfoMcmdistributionaction
     */
    public function setDelStatusAt($delStatusAt)
    {
        $this->delStatusAt = $delStatusAt;

        return $this;
    }

    /**
     * Get delStatusAt
     *
     * @return \DateTime
     */
    public function getDelStatusAt()
    {
        return $this->delStatusAt;
    }

    public function getContactRegion(): ?InfoRegion
    {
        return $this->contact ? $this->contact->getRegion() : null;
    }

    public function getContactRegionGmt(): ?string
    {
        return $this->getContactRegion() ? $this->getContactRegion()->getGmt() : null;
    }
}
