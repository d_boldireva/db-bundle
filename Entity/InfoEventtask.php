<?php

namespace TeamSoft\CrmRepositoryBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * InfoEventtask
 *
 * @ORM\Table(name="info_eventtask")
 * @ORM\Entity
 */
class InfoEventtask
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="currenttime", type="datetime", nullable=true)
     */
    private $currenttime;

    /**
     * @var \InfoEvent
     *
     * @ORM\ManyToOne(targetEntity="InfoEvent")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="event_id", referencedColumnName="id")
     * })
     */
    private $event;

    /**
     * @var \InfoTask
     *
     * @ORM\ManyToOne(targetEntity="InfoTask")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="task_id", referencedColumnName="id")
     * })
     */
    private $task;

    /**
     * @var \InfoAction
     *
     * @ORM\ManyToOne(targetEntity="InfoAction")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="action_id", referencedColumnName="id")
     * })
     */
    private $action;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set currenttime
     *
     * @param \DateTime $currenttime
     *
     * @return InfoEventtask
     */
    public function setCurrenttime($currenttime)
    {
        $this->currenttime = $currenttime;
    
        return $this;
    }

    /**
     * Get currenttime
     *
     * @return \DateTime
     */
    public function getCurrenttime()
    {
        return $this->currenttime;
    }

    /**
     * Set event
     *
     * @param \TeamSoft\CrmRepositoryBundle\Entity\InfoEvent $event
     *
     * @return InfoEventtask
     */
    public function setEvent(\TeamSoft\CrmRepositoryBundle\Entity\InfoEvent $event = null)
    {
        $this->event = $event;
    
        return $this;
    }

    /**
     * Get event
     *
     * @return \TeamSoft\CrmRepositoryBundle\Entity\InfoEvent
     */
    public function getEvent()
    {
        return $this->event;
    }

    /**
     * Set task
     *
     * @param \TeamSoft\CrmRepositoryBundle\Entity\InfoTask $task
     *
     * @return InfoEventtask
     */
    public function setTask(\TeamSoft\CrmRepositoryBundle\Entity\InfoTask $task = null)
    {
        $this->task = $task;
    
        return $this;
    }

    /**
     * Get task
     *
     * @return \TeamSoft\CrmRepositoryBundle\Entity\InfoTask
     */
    public function getTask()
    {
        return $this->task;
    }

    /**
     * Set action
     *
     * @param \TeamSoft\CrmRepositoryBundle\Entity\InfoAction $action
     *
     * @return InfoEventtask
     */
    public function setAction(\TeamSoft\CrmRepositoryBundle\Entity\InfoAction $action = null)
    {
        $this->action = $action;
    
        return $this;
    }

    /**
     * Get action
     *
     * @return \TeamSoft\CrmRepositoryBundle\Entity\InfoAction
     */
    public function getAction()
    {
        return $this->action;
    }
}
