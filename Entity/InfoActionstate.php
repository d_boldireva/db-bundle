<?php

namespace TeamSoft\CrmRepositoryBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * InfoActionstate
 *
 * @ORM\Table(name="info_actionstate")
 * @ORM\Entity
 */
class InfoActionstate
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255, nullable=false)
     */
    private $name;

    /**
     * @var integer
     *
     * @ORM\Column(name="isclosed", type="integer", nullable=true)
     */
    private $isclosed;

    /**
     * @var integer
     *
     * @ORM\Column(name="color", type="integer", nullable=true)
     */
    private $color;

    /**
     * @var guid
     *
     * @ORM\Column(name="guid", type="guid", nullable=true)
     */
    private $guid;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="currenttime", type="datetime", nullable=true)
     */
    private $currenttime;

    /**
     * @var string
     *
     * @ORM\Column(name="moduser", type="string", length=16, nullable=true)
     */
    private $moduser;

    /**
     * @var integer
     *
     * @ORM\Column(name="isstart", type="integer", nullable=true)
     */
    private $isstart;

    /**
     * @var string
     *
     * @ORM\Column(name="colorhex", type="string", length=255, nullable=true)
     */
    private $colorhex;

    /**
     * @var InfoLanguage
     *
     * @ORM\ManyToOne(targetEntity="InfoLanguage")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="language_id", referencedColumnName="id")
     * })
     */
    private $language;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return InfoActionstate
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set isclosed
     *
     * @param integer $isclosed
     * @return InfoActionstate
     */
    public function setIsclosed($isclosed)
    {
        $this->isclosed = $isclosed;

        return $this;
    }

    /**
     * Get isclosed
     *
     * @return integer
     */
    public function getIsclosed()
    {
        return $this->isclosed;
    }

    /**
     * Set color
     *
     * @param integer $color
     * @return InfoActionstate
     */
    public function setColor($color)
    {
        $this->color = $color;

        return $this;
    }

    /**
     * Get color
     *
     * @return integer
     */
    public function getColor()
    {
        return $this->color;
    }

    /**
     * Set guid
     *
     * @param guid $guid
     * @return InfoActionstate
     */
    public function setGuid($guid)
    {
        $this->guid = $guid;

        return $this;
    }

    /**
     * Get guid
     *
     * @return guid
     */
    public function getGuid()
    {
        return $this->guid;
    }

    /**
     * Set currenttime
     *
     * @param \DateTime $currenttime
     * @return InfoActionstate
     */
    public function setCurrenttime($currenttime)
    {
        $this->currenttime = $currenttime;

        return $this;
    }

    /**
     * Get currenttime
     *
     * @return \DateTime
     */
    public function getCurrenttime()
    {
        return $this->currenttime;
    }

    /**
     * Set moduser
     *
     * @param string $moduser
     * @return InfoActionstate
     */
    public function setModuser($moduser)
    {
        $this->moduser = $moduser;

        return $this;
    }

    /**
     * Get moduser
     *
     * @return string
     */
    public function getModuser()
    {
        return $this->moduser;
    }

    /**
     * Set isstart
     *
     * @param integer $isstart
     * @return InfoActionstate
     */
    public function setIsstart($isstart)
    {
        $this->isstart = $isstart;

        return $this;
    }

    /**
     * Get isstart
     *
     * @return integer
     */
    public function getIsstart()
    {
        return $this->isstart;
    }

    /**
     * Set colorhex
     *
     * @param string $colorhex
     * @return InfoActionstate
     */
    public function setColorhex($colorhex)
    {
        $this->colorhex = $colorhex;

        return $this;
    }

    /**
     * Get colorhex
     *
     * @return string
     */
    public function getColorhex()
    {
        return $this->colorhex;
    }

    /**
     * Set language
     *
     * @param InfoLanguage|null $content
     * @return InfoActionstate
     */
    public function setLanguage(InfoLanguage $language = null)
    {
        $this->language = $language;

        return $this;
    }

    /**
     * Get language
     *
     * @return InfoLanguage
     */
    public function getLanguage()
    {
        return $this->language;
    }
}
