<?php

namespace TeamSoft\CrmRepositoryBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * InfoTaskparam
 *
 * @ORM\Table(name="info_taskparam")
 * @ORM\Entity
 */
class InfoTaskparam implements ServiceFieldInterface
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var int|null
     *
     * @ORM\Column(name="mark", type="integer", nullable=true)
     */
    private $mark;

    /**
     * @var string|null
     *
     * @ORM\Column(name="guid", type="guid", nullable=true)
     */
    private $guid;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="currenttime", type="datetime", nullable=true)
     */
    private $currenttime;

    /**
     * @var string|null
     *
     * @ORM\Column(name="moduser", type="string", length=16, nullable=true)
     */
    private $moduser;

    /**
     * @var int|null
     *
     * @ORM\Column(name="mark2", type="integer", nullable=true)
     */
    private $mark2;

    /**
     * @var int|null
     *
     * @ORM\Column(name="mark3", type="integer", nullable=true)
     */
    private $mark3;

    /**
     * @var int|null
     *
     * @ORM\Column(name="mark4", type="integer", nullable=true)
     */
    private $mark4;

    /**
     * @var int|null
     *
     * @ORM\Column(name="tasksubjectbrend_id", type="integer", nullable=true)
     */
    private $tasksubjectbrendId;

    /**
     * @var int|null
     *
     * @ORM\Column(name="isimportant", type="integer", nullable=true)
     */
    private $isimportant;

    /**
     * @var string|null
     *
     * @ORM\Column(name="mark5", type="string", length=255, nullable=true)
     */
    private $mark5;

    /**
     * @var InfoParamfortask
     *
     * @ORM\ManyToOne(targetEntity="InfoParamfortask")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="param_id", referencedColumnName="id")
     * })
     */
    private $param;

    /**
     * @var InfoParamfortaskvalue
     *
     * @ORM\ManyToOne(targetEntity="InfoParamfortaskvalue")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="paramfortaskvalue_id", referencedColumnName="id")
     * })
     */
    private $paramForTaskValue;

    /**
     * @var InfoTask
     *
     * @ORM\ManyToOne(targetEntity="InfoTask")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="task_id", referencedColumnName="id")
     * })
     */
    private $task;

    /**
     * @var InfoUser
     *
     * @ORM\ManyToOne(targetEntity="InfoUser")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     * })
     */
    private $user;



    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set mark.
     *
     * @param int|null $mark
     *
     * @return InfoTaskparam
     */
    public function setMark($mark = null)
    {
        $this->mark = $mark;

        return $this;
    }

    /**
     * Get mark.
     *
     * @return int|null
     */
    public function getMark()
    {
        return $this->mark;
    }

    /**
     * Set guid.
     *
     * @param string|null $guid
     *
     * @return InfoTaskparam
     */
    public function setGuid($guid = null)
    {
        $this->guid = $guid;

        return $this;
    }

    /**
     * Get guid.
     *
     * @return string|null
     */
    public function getGuid()
    {
        return $this->guid;
    }

    /**
     * Set currenttime.
     *
     * @param \DateTime|null $currenttime
     *
     * @return InfoTaskparam
     */
    public function setCurrenttime(\DateTime $currenttime = null)
    {
        $this->currenttime = $currenttime;

        return $this;
    }

    /**
     * Get currenttime.
     *
     * @return \DateTime|null
     */
    public function getCurrenttime()
    {
        return $this->currenttime;
    }

    /**
     * Set moduser.
     *
     * @param string|null $moduser
     *
     * @return InfoTaskparam
     */
    public function setModuser($moduser = null)
    {
        $this->moduser = $moduser;

        return $this;
    }

    /**
     * Get moduser.
     *
     * @return string|null
     */
    public function getModuser()
    {
        return $this->moduser;
    }

    /**
     * Set mark2.
     *
     * @param int|null $mark2
     *
     * @return InfoTaskparam
     */
    public function setMark2($mark2 = null)
    {
        $this->mark2 = $mark2;

        return $this;
    }

    /**
     * Get mark2.
     *
     * @return int|null
     */
    public function getMark2()
    {
        return $this->mark2;
    }

    /**
     * Set mark3.
     *
     * @param int|null $mark3
     *
     * @return InfoTaskparam
     */
    public function setMark3($mark3 = null)
    {
        $this->mark3 = $mark3;

        return $this;
    }

    /**
     * Get mark3.
     *
     * @return int|null
     */
    public function getMark3()
    {
        return $this->mark3;
    }

    /**
     * Set mark4.
     *
     * @param int|null $mark4
     *
     * @return InfoTaskparam
     */
    public function setMark4($mark4 = null)
    {
        $this->mark4 = $mark4;

        return $this;
    }

    /**
     * Get mark4.
     *
     * @return int|null
     */
    public function getMark4()
    {
        return $this->mark4;
    }

    /**
     * Set tasksubjectbrendId.
     *
     * @param int|null $tasksubjectbrendId
     *
     * @return InfoTaskparam
     */
    public function setTasksubjectbrendId($tasksubjectbrendId = null)
    {
        $this->tasksubjectbrendId = $tasksubjectbrendId;

        return $this;
    }

    /**
     * Get tasksubjectbrendId.
     *
     * @return int|null
     */
    public function getTasksubjectbrendId()
    {
        return $this->tasksubjectbrendId;
    }

    /**
     * Set isimportant.
     *
     * @param int|null $isimportant
     *
     * @return InfoTaskparam
     */
    public function setIsimportant($isimportant = null)
    {
        $this->isimportant = $isimportant;

        return $this;
    }

    /**
     * Get isimportant.
     *
     * @return int|null
     */
    public function getIsimportant()
    {
        return $this->isimportant;
    }

    /**
     * Set mark5.
     *
     * @param string|null $mark5
     *
     * @return InfoTaskparam
     */
    public function setMark5($mark5 = null)
    {
        $this->mark5 = $mark5;

        return $this;
    }

    /**
     * Get mark5.
     *
     * @return string|null
     */
    public function getMark5()
    {
        return $this->mark5;
    }

    /**
     * Set param.
     *
     * @param InfoParamfortask|null $param
     *
     * @return InfoTaskparam
     */
    public function setParam(InfoParamfortask $param = null)
    {
        $this->param = $param;

        return $this;
    }

    /**
     * Get param.
     *
     * @return InfoParamfortask|null
     */
    public function getParam()
    {
        return $this->param;
    }

    /**
     * Get param.
     *
     * @return int|null
     */
    public function getParamId()
    {
        return $this->param ? $this->param->getId() : null;
    }

    /**
     * Set paramForTaskValue
     *
     * @param InfoParamfortaskvalue|null $paramForTaskValue
     *
     * @return InfoTaskparam
     */
    public function setParamForTaskValue(InfoParamfortaskvalue $paramForTaskValue = null)
    {
        $this->paramForTaskValue = $paramForTaskValue;

        return $this;
    }

    /**
     * Get paramForTaskValue
     *
     * @return InfoParamfortaskvalue
     */
    public function getParamForTaskValue()
    {
        return $this->paramForTaskValue;
    }

    /**
     * Get paramForTaskValue
     *
     * @return int
     */
    public function getParamForTaskValueId()
    {
        return $this->paramForTaskValue ? $this->paramForTaskValue->getId() : null;
    }

    /**
     * Set task.
     *
     * @param InfoTask|null $task
     *
     * @return InfoTaskparam
     */
    public function setTask(InfoTask $task = null)
    {
        $this->task = $task;

        return $this;
    }

    /**
     * Get task.
     *
     * @return InfoTask|null
     */
    public function getTask()
    {
        return $this->task;
    }

    /**
     * Set user.
     *
     * @param InfoUser|null $user
     *
     * @return InfoTaskparam
     */
    public function setUser(InfoUser $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user.
     *
     * @return InfoUser|null
     */
    public function getUser()
    {
        return $this->user;
    }
}
