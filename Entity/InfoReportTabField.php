<?php

namespace TeamSoft\CrmRepositoryBundle\Entity;

use DateTime;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * InfoReportTabField
 *
 * @ORM\Table(name="info_reporttabfield")
 * @ORM\Entity()
 */
class InfoReportTabField implements ServiceFieldInterface
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string|null
     *
     * @ORM\Column(name="name", type="string", length=255, nullable=true)
     */
    private $name;

    /**
     * @var string|null
     *
     * @ORM\Column(name="sorting", type="string", length=255, nullable=true)
     */
    private $sorting;

    /**
     * @var string|null
     *
     * @ORM\Column(name="aggregation", type="string", length=255, nullable=true)
     */
    private $aggregation;

    /**
     * @var string|null
     *
     * @ORM\Column(name="formula", type="string", length=255, nullable=true)
     */
    private $formula;

    /**
     * @var string|null
     *
     * @ORM\Column(name="action", type="string", length=255, nullable=true)
     */
    private $action;

    /**
     * @var string|null
     *
     * @ORM\Column(name="action_text", type="string", length=255, nullable=true)
     */
    private $actionText;

    /**
     * @var string|null
     *
     * @ORM\Column(name="action_reference", type="string", length=255, nullable=true)
     */
    private $actionReference;

    /**
     * @var string|null
     *
     * @ORM\Column(name="style_color", type="string", length=255, nullable=true)
     */
    private $styleColor;

    /**
     * @var string|null
     *
     * @ORM\Column(name="style_background", type="string", length=255, nullable=true)
     */
    private $styleBackground;

    /**
     * @var string|null
     *
     * @ORM\Column(name="style_condition", type="string", length=255, nullable=true)
     */
    private $styleCondition;

    /**
     * @var string|null
     *
     * @ORM\Column(name="style_value1", type="string", length=255, nullable=true)
     */
    private $styleValue1;

    /**
     * @var string|null
     *
     * @ORM\Column(name="style_value2", type="string", length=255, nullable=true)
     */
    private $styleValue2;

    /**
     * @var string|null
     *
     * @ORM\Column(name="position", type="integer", length=10, nullable=true)
     */
    private $position;

    /**
     * @var string|null
     *
     * @ORM\Column(name="group_by", type="integer", length=10, nullable=true)
     */
    private $groupBy;

    /**
     * @var string|null
     *
     * @ORM\Column(name="is_pivot", type="integer", length=10, nullable=true)
     */
    private $pivot;

    /**
     * @var string|null
     *
     * @ORM\Column(name="width", type="integer", length=10, nullable=true)
     */
    private $width;

    /**
     * @var string|null
     *
     * @ORM\Column(name="visible", type="integer", length=10, nullable=true)
     */
    private $visible;

    /**
     * @var DateTime|null
     *
     * @ORM\Column(name="currenttime", type="datetime", nullable=true)
     */
    private $currenttime;

    /**
     * @var string|null
     *
     * @ORM\Column(name="moduser", type="string", length=16, nullable=true)
     * )
     */
    private $moduser;

    /**
     * @var string|null
     *
     * @ORM\Column(name="guid", type="guid", nullable=true)
     */
    private $guid;

    /**
     * Many fields belongs to one reportTab
     * @ORM\ManyToOne(targetEntity="InfoReportTab", inversedBy="id")
     * @ORM\JoinColumn(name="reporttab_id", referencedColumnName="id")
     */
    private $reportTab;

    /**
     * @var InfoSliceField $field
     * Many fields have one field
     * @ORM\ManyToOne(targetEntity="InfoSliceField", inversedBy="id")
     * @ORM\JoinColumn(name="field_id", referencedColumnName="id")
     */
    private $field;

    /**
     * @var string|null
     *
     * @ORM\Column(name="filter", type="string", length=255, nullable=true)
     */
    private $filter;


    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set reportTab
     *
     * @param InfoReportTab $reportTab
     *
     * @return InfoReportTabField
     */
    public function setReportTab($reportTab = null)
    {
        $this->reportTab = $reportTab;

        return $this;
    }

    /**
     * Get reportTab
     *
     * @return InfoReportTab
     */
    public function getReportTab()
    {
        return $this->reportTab;
    }

    /**
     * Set field
     *
     * @param InfoSliceField $field
     *
     * @return InfoReportTabField
     */
    public function setField($field = null)
    {
        $this->field = $field;

        return $this;
    }

    /**
     * Get field
     *
     * @return InfoSliceField
     */
    public function getField()
    {
        return $this->field;
    }

    /**
     * Set name.
     *
     * @param string|null $name
     *
     * @return InfoReportTabField
     */
    public function setName($name = null)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name.
     *
     * @return string|null
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set sorting.
     *
     * @param string|null $sorting
     *
     * @return InfoReportTabField
     */
    public function setSorting($sorting = null)
    {
        $this->sorting = $sorting;

        return $this;
    }

    /**
     * Get sorting.
     *
     * @return string|null
     */
    public function getSorting()
    {
        return $this->sorting;
    }

    /**
     * Set position.
     *
     * @param string|null $position
     *
     * @return InfoReportTabField
     */
    public function setPosition($position = null)
    {
        $this->position = $position;

        return $this;
    }

    /**
     * Get position.
     *
     * @return string|null
     */
    public function getPosition()
    {
        return $this->position;
    }

    /**
     * Set groupBy.
     *
     * @param string|null $groupBy
     *
     * @return InfoReportTabField
     */
    public function setGroupBy($groupBy = null)
    {
        $this->groupBy = $groupBy;

        return $this;
    }

    /**
     * Get groupBy.
     *
     * @return string|null
     */
    public function getGroupBy()
    {
        return $this->groupBy;
    }

    /**
     * Set pivot.
     *
     * @param string|null $pivot
     *
     * @return InfoReportTabField
     */
    public function setPivot($pivot = null)
    {
        $this->pivot = $pivot;

        return $this;
    }

    /**
     * Get pivot.
     *
     * @return string|null
     */
    public function getPivot()
    {
        return $this->pivot;
    }

    /**
     * Set visible.
     *
     * @param string|null $visible
     *
     * @return InfoReportTabField
     */
    public function setVisible($visible = null)
    {
        $this->visible = $visible;

        return $this;
    }

    /**
     * Get visible.
     *
     * @return string|null
     */
    public function getVisible()
    {
        return $this->visible;
    }

    /**
     * Set aggregation.
     *
     * @param string|null $aggregation
     *
     * @return InfoReportTabField
     */
    public function setAggregation($aggregation = null)
    {
        $this->aggregation = $aggregation;

        return $this;
    }

    /**
     * Get aggregation.
     *
     * @return string|null
     */
    public function getAggregation()
    {
        return $this->aggregation;
    }

    /**
     * Set formula.
     *
     * @param string|null $formula
     *
     * @return InfoReportTabField
     */
    public function setFormula($formula = null)
    {
        $this->formula = $formula;

        return $this;
    }

    /**
     * Get formula.
     *
     * @return string|null
     */
    public function getFormula()
    {
        return $this->formula;
    }

    /**
     * Set action.
     *
     * @param string|null $action
     *
     * @return InfoReportTabField
     */
    public function setAction($action = null)
    {
        $this->action = $action;

        return $this;
    }

    /**
     * Get action.
     *
     * @return string|null
     */
    public function getAction()
    {
        return $this->action;
    }

    /**
     * Set actionText.
     *
     * @param string|null $actionText
     *
     * @return InfoReportTabField
     */
    public function setActionText($actionText = null)
    {
        $this->actionText = $actionText;

        return $this;
    }

    /**
     * Get actionText.
     *
     * @return string|null
     */
    public function getActionText()
    {
        return $this->actionText;
    }

    /**
     * Set actionReference.
     *
     * @param string|null $actionReference
     *
     * @return InfoReportTabField
     */
    public function setActionReference($actionReference = null)
    {
        $this->actionReference = $actionReference;

        return $this;
    }

    /**
     * Get actionReference.
     *
     * @return string|null
     */
    public function getActionReference()
    {
        return $this->actionReference;
    }

    /**
     * Set width.
     *
     * @param string|null $width
     *
     * @return InfoReportTabField
     */
    public function setWidth($width = null)
    {
        $this->width = $width;

        return $this;
    }

    /**
     * Get width.
     *
     * @return string|null
     */
    public function getWidth()
    {
        return $this->width;
    }

    /**
     * Set styleColor.
     *
     * @param string|null $styleColor
     *
     * @return InfoReportTabField
     */
    public function setStyleColor($styleColor = null)
    {
        $this->styleColor = $styleColor;

        return $this;
    }

    /**
     * Get styleColor.
     *
     * @return string|null
     */
    public function getStyleColor()
    {
        return $this->styleColor;
    }

    /**
     * Set styleBackground.
     *
     * @param string|null $styleBackground
     *
     * @return InfoReportTabField
     */
    public function setStyleBackground($styleBackground = null)
    {
        $this->styleBackground = $styleBackground;

        return $this;
    }

    /**
     * Get styleBackground.
     *
     * @return string|null
     */
    public function getStyleBackground()
    {
        return $this->styleBackground;
    }

    /**
     * Set styleCondition.
     *
     * @param string|null $styleCondition
     *
     * @return InfoReportTabField
     */
    public function setStyleCondition($styleCondition = null)
    {
        $this->styleCondition = $styleCondition;

        return $this;
    }

    /**
     * Get styleCondition.
     *
     * @return string|null
     */
    public function getStyleCondition()
    {
        return $this->styleCondition;
    }

    /**
     * Set styleValue1.
     *
     * @param string|null $styleValue1
     *
     * @return InfoReportTabField
     */
    public function setStyleValue1($styleValue1 = null)
    {
        $this->styleValue1 = $styleValue1;

        return $this;
    }

    /**
     * Get styleValue1.
     *
     * @return string|null
     */
    public function getStyleValue1()
    {
        return $this->styleValue1;
    }

    /**
     * Set styleValue2.
     *
     * @param string|null $styleValue2
     *
     * @return InfoReportTabField
     */
    public function setStyleValue2($styleValue2 = null)
    {
        $this->styleValue2 = $styleValue2;

        return $this;
    }

    /**
     * Get styleValue2.
     *
     * @return string|null
     */
    public function getStyleValue2()
    {
        return $this->styleValue2;
    }

    /**
     * Set currenttime.
     *
     * @param DateTime|null $currenttime
     *
     * @return InfoReportTabField
     */
    public function setCurrenttime(DateTime $currenttime = null)
    {
        $this->currenttime = $currenttime;

        return $this;
    }

    /**
     * Get currenttime.
     *
     * @return DateTime|null
     */
    public function getCurrenttime()
    {
        return $this->currenttime;
    }

    /**
     * Set moduser.
     *
     * @param string|null $moduser
     *
     * @return InfoReportTabField
     */
    public function setModuser($moduser = null)
    {
        $this->moduser = $moduser;

        return $this;
    }

    /**
     * Get moduser.
     *
     * @return string|null
     */
    public function getModuser()
    {
        return $this->moduser;
    }

    /**
     * Set guid.
     *
     * @param string|null $guid
     *
     * @return InfoReportTabField
     */
    public function setGuid($guid = null)
    {
        $this->guid = $guid;

        return $this;
    }

    /**
     * Get guid.
     *
     * @return string|null
     */
    public function getGuid()
    {
        return $this->guid;
    }

    /**
     * @return number
     */
    public function getFieldId()
    {
        return $this->field ? $this->field->getId() : null;
    }

    /**
     * Set filter.
     *
     * @param string|null $filter
     *
     * @return InfoReportTabField
     */
    public function setFilter($filter = null)
    {
        $this->filter = $filter;

        return $this;
    }

    /**
     * Get filter.
     *
     * @return string|null
     */
    public function getFilter()
    {
        return $this->filter;
    }
}
