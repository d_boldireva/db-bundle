<?php

namespace TeamSoft\CrmRepositoryBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use TeamSoft\CrmRepositoryBundle\Model\InfoFile;

/**
 * InfoCompanySign
 *
 * @ORM\Table(name="info_companysign")
 * @ORM\Entity
 */
class InfoCompanySign implements ServiceFieldInterface
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="currenttime", type="datetime", length=255, nullable=true)
     */
    private $currenttime;

    /**
     * @var string
     *
     * @ORM\Column(name="moduser", type="string", length=16, nullable=true)
     */
    private $moduser;

    /**
     * @var string
     *
     * @ORM\Column(name="guid", type="guid", nullable=true)
     */
    private $guid;

    /**
     * @var InfoCompany
     *
     * @ORM\OneToOne(targetEntity="InfoCompany", inversedBy="companySign")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="company_id", referencedColumnName="id")
     * })
     */
    private $company;

    /**
     * @var InfoCompanySignFile[]|ArrayCollection
     *
     * @ORM\OneToMany (
     *     targetEntity="InfoCompanySignFile",
     *     mappedBy="companySign",
     *     cascade={"persist"}
     * )
     */
    private $companySignFiles;


    /**
     * @var \DateTime
     *
     * @ORM\Column(name="createdate", type="datetime", nullable=true)
     */
    private $createdate;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="modified", type="datetime", nullable=true)
     */
    private $modified;

    public function __construct()
    {
        $this->companySignFiles = new ArrayCollection();
    }

    /**
     * Set guid
     *
     * @param guid $guid
     * @return self
     */
    public function setGuid($guid)
    {
        $this->guid = $guid;

        return $this;
    }

    /**
     * Get guid
     *
     * @return guid
     */
    public function getGuid()
    {
        return $this->guid;
    }

    /**
     * Set currenttime
     *
     * @param \DateTime $currenttime
     * @return self
     */
    public function setCurrenttime(\DateTime $currenttime)
    {
        $this->currenttime = $currenttime;

        return $this;
    }

    /**
     * Get currenttime
     *
     * @return \DateTime
     */
    public function getCurrenttime()
    {
        return $this->currenttime;
    }

    /**
     * Set moduser
     *
     * @param string $moduser
     * @return InfoContactsign
     */
    public function setModuser($moduser)
    {
        $this->moduser = $moduser;

        return $this;
    }

    /**
     * Get moduser
     *
     * @return string
     */
    public function getModuser()
    {
        return $this->moduser;
    }

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Get contact
     *
     * @return InfoCompany|null
     */
    public function getCompany()
    {
        return $this->company;
    }

    /**
     * Set contact
     * @param InfoCompany $contact
     * @return self
     */
    public function setCompany(InfoCompany $contact): self
    {
        $this->company = $contact;

        return $this;
    }


    /**
     * Set createdate.
     *
     * @param \DateTime|null $createdate
     *
     * @return self
     */
    public function setCreatedate($createdate = null): self
    {
        $this->createdate = $createdate;

        return $this;
    }

    /**
     * Get createdate.
     *
     * @return \DateTime|null
     */
    public function getCreatedate()
    {
        return $this->createdate;
    }

    /**
     * Set modified.
     *
     * @param \DateTime|null $modified
     *
     * @return InfoContactsign
     */
    public function setModified($modified = null)
    {
        $this->modified = $modified;

        return $this;
    }

    /**
     * Get modified.
     *
     * @return \DateTime|null
     */
    public function getModified()
    {
        return $this->modified;
    }

    /**
     * Get companySignFiles
     *
     * @return InfoContactsignfile[]|Collection|ArrayCollection
     */
    public function getCompanySignFiles(): Collection
    {
        return $this->companySignFiles;
    }

    /**
     * @param InfoCompanySignFile $companySignFile
     * @return $this
     */
    public function addCompanySignFile(InfoCompanySignFile $companySignFile): self
    {
        if (!$this->companySignFiles->contains($companySignFile)) {
            $this->companySignFiles->add($companySignFile);
        }

        return $this;
    }

    /**
     * @param InfoCompanySignFile $companySignFile
     * @return $this
     */
    public function removeContactSignFile(InfoCompanySignFile $companySignFile): self
    {
        $this->companySignFiles->removeElement($companySignFile);

        return $this;
    }
}
