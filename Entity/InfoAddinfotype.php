<?php

namespace TeamSoft\CrmRepositoryBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * InfoAddinfotype
 *
 * @ORM\Table(name="info_AddInfoType")
 * @ORM\Entity(repositoryClass="TeamSoft\CrmRepositoryBundle\Repository\InfoAddinfotype")
 */
class InfoAddinfotype implements ServiceFieldInterface
{

    public const STRING_VALUE = 1;
    public const INT_VALUE = 2;
    public const FLOAT_VALUE = 3;
    public const DATE_VALUE = 4;
    public const DICTIONARY_VALUE = 5;
    public const SYSTEM_DICTIONARY_VALUE = 6;

    /**
     * CODE for turnover type
     * @var string
     */
    public const CODE_TURNOVER = 'tovarooborot';

    /**
     *
     */
    public const CODE_DISTANCE_VISIT = 'distance_visit';

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="Name", type="string", length=100, nullable=false)
     */
    private $name;

    private $nameTranslation;


    /**
     * @var string
     *
     * @ORM\Column(name="Code", type="string", length=50, nullable=true)
     */
    private $code;

    /**
     * @var int
     *
     * @ORM\Column(name="DataType", type="integer", nullable=true)
     */
    private $datatype = self::STRING_VALUE;

    /**
     * @var int
     *
     * @ORM\Column(name="additionaltype", type="integer", nullable=true)
     */
    private $additionaltype;

    /**
     * @var int
     *
     * @ORM\Column(name="FieldDataType", type="integer", nullable=true)
     */
    private $fielddatatype;

    /**
     * @var int
     *
     * @ORM\Column(name="IsDefault", type="integer", nullable=true)
     */
    private $isdefault;

    /**
     * @var int
     *
     * @ORM\Column(name="disableedit", type="integer", nullable=true)
     */
    private $disableedit;

    /**
     * @var int
     *
     * @ORM\Column(name="XPos", type="integer", nullable=true)
     */
    private $xpos;

    /**
     * @var int
     *
     * @ORM\Column(name="YPos", type="integer", nullable=true)
     */
    private $ypos;

    /**
     * @var string
     *
     * @ORM\Column(name="guid", type="guid", nullable=true)
     */
    private $guid;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="currenttime", type="datetime", nullable=true)
     */
    private $currenttime;

    /**
     * @var string
     *
     * @ORM\Column(name="moduser", type="string", length=16, nullable=true)
     */
    private $moduser;

    /**
     * @var int
     *
     * @ORM\Column(name="isshowhint", type="integer", nullable=true)
     */
    private $isshowhint;

    /**
     * @var InfoCustomdictionary
     *
     * @ORM\ManyToOne(targetEntity="InfoCustomdictionary")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="CustomDictionary_id", referencedColumnName="id")
     * })
     */
    private $customdictionary;

    /**
     * @var InfoSystemdictionary
     *
     * @ORM\ManyToOne(targetEntity="InfoSystemdictionary")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="systemdictionary_id", referencedColumnName="id")
     * })
     */
    private $systemdictionary;

    /**
     * @var ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="InfoAddinfocompanytype", mappedBy="addinfotype", cascade={"persist"})
     */
    private $addInfoCompanyTypes;

    /**
     * @var ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="InfoAddinfocontacttype", mappedBy="addinfotype", cascade={"persist"})
     */
    private $addInfoContactTypes;

    /**
     * @var ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="InfoAddinfotasktype", mappedBy="addinfotype", cascade={"persist"})
     */
    private $addInfoTaskTypes;

    /**
     * @var ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="InfoAddinfoactiontype", mappedBy="addinfotype", cascade={"persist"})
     */
    private $addInfoActionTypes;

    /**
     * @ORM\ManyToMany(targetEntity="InfoRole")
     * @ORM\JoinTable(name="info_addinfotypebyrole",
     *   joinColumns={@ORM\JoinColumn(name="addinfotype_id", referencedColumnName="id")},
     *   inverseJoinColumns={@ORM\JoinColumn(name="role_id", referencedColumnName="id")}
     * )
     */
    private $roles;

    /**
     * @ORM\ManyToMany(targetEntity="InfoTasktype")
     * @ORM\JoinTable(name="info_addinfotasktype",
     *   joinColumns={@ORM\JoinColumn(name="addinfotype_id", referencedColumnName="id")},
     *   inverseJoinColumns={@ORM\JoinColumn(name="subjtype_id", referencedColumnName="id")}
     * )
     */
    private $taskTypes;

    /**
     * @ORM\ManyToMany(targetEntity="InfoActiontype")
     * @ORM\JoinTable(name="info_addinfoactiontype",
     *   joinColumns={@ORM\JoinColumn(name="addinfotype_id", referencedColumnName="id")},
     *   inverseJoinColumns={@ORM\JoinColumn(name="subjtype_id", referencedColumnName="id")}
     * )
     */
    private $actionTypes;

    /**
     * @ORM\ManyToMany(targetEntity="InfoCompanytype")
     * @ORM\JoinTable(name="info_addinfocompanytype",
     *   joinColumns={@ORM\JoinColumn(name="addinfotype_id", referencedColumnName="id")},
     *   inverseJoinColumns={@ORM\JoinColumn(name="subjtype_id", referencedColumnName="id")}
     * )
     */
    private $companyTypes;

    /**
     * @ORM\ManyToMany(targetEntity="InfoContacttype")
     * @ORM\JoinTable(name="info_addinfocontacttype",
     *   joinColumns={@ORM\JoinColumn(name="addinfotype_id", referencedColumnName="id")},
     *   inverseJoinColumns={@ORM\JoinColumn(name="subjtype_id", referencedColumnName="id")}
     * )
     */
    private $contactTypes;

    /**
     * @var InfoDirection|null
     *
     * @ORM\ManyToOne(targetEntity="InfoDirection")
     * @ORM\JoinColumn(name="direction_id", referencedColumnName="id")
     */
    private $direction;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->addInfoCompanyTypes = new ArrayCollection();
        $this->addInfoContactTypes = new ArrayCollection();
        $this->addInfoTaskTypes = new ArrayCollection();
        $this->addInfoActionTypes = new ArrayCollection();
        $this->roles = new ArrayCollection();
        $this->taskTypes = new ArrayCollection();
        $this->actionTypes = new ArrayCollection();
        $this->companyTypes = new ArrayCollection();
        $this->contactTypes = new ArrayCollection();
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return InfoAddinfotype
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Get nameTranslation
     *
     * @return string
     */
    public function getNameTranslation()
    {
        return $this->nameTranslation;
    }

    /**
     * Set nameTranslation
     *
     * @param string $nameTranslation
     *
     * @return InfoAddinfotype
     */
    public function setNameTranslation($nameTranslation)
    {
        $this->nameTranslation = $nameTranslation;

        return $this;
    }

    /**
     * Set code
     *
     * @param string $code
     *
     * @return InfoAddinfotype
     */
    public function setCode($code)
    {
        $this->code = $code;

        return $this;
    }

    /**
     * Get code
     *
     * @return string
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * Set datatype
     *
     * @param int $datatype
     *
     * @return InfoAddinfotype
     */
    public function setDatatype($datatype)
    {
        $this->datatype = $datatype;

        return $this;
    }

    /**
     * Get datatype
     *
     * @return int
     */
    public function getDatatype()
    {
        return $this->datatype;
    }

    /**
     * @return int|null
     */
    public function getAdditionaltype(): ?int
    {
        return $this->additionaltype;
    }

    /**
     * @param  int|null  $additionaltype
     *
     * @return $this
     */
    public function setAdditionaltype(?int $additionaltype): self
    {
        $this->additionaltype = $additionaltype;

        return $this;
    }

    /**
     * Set fielddatatype
     *
     * @param int $fielddatatype
     *
     * @return InfoAddinfotype
     */
    public function setFielddatatype($fielddatatype)
    {
        $this->fielddatatype = $fielddatatype;

        return $this;
    }

    /**
     * Get fielddatatype
     *
     * @return int
     */
    public function getFielddatatype()
    {
        return $this->fielddatatype;
    }

    /**
     * Set isdefault
     *
     * @param int $isdefault
     *
     * @return InfoAddinfotype
     */
    public function setIsdefault($isdefault)
    {
        $this->isdefault = $isdefault;

        return $this;
    }

    /**
     * Get isdefault
     *
     * @return int
     */
    public function getIsdefault()
    {
        return $this->isdefault;
    }

    /**
     * Set disableedit
     *
     * @param int $disableedit
     *
     * @return InfoAddinfotype
     */
    public function setDisableedit($disableedit)
    {
        $this->disableedit = $disableedit;

        return $this;
    }

    /**
     * Get disableedit
     *
     * @return int
     */
    public function getDisableedit()
    {
        return $this->disableedit;
    }

    /**
     * Set xpos
     *
     * @param int $xpos
     *
     * @return InfoAddinfotype
     */
    public function setXpos($xpos)
    {
        $this->xpos = $xpos;

        return $this;
    }

    /**
     * Get xpos
     *
     * @return int
     */
    public function getXpos()
    {
        return $this->xpos;
    }

    /**
     * Set ypos
     *
     * @param int $ypos
     *
     * @return InfoAddinfotype
     */
    public function setYpos($ypos)
    {
        $this->ypos = $ypos;

        return $this;
    }

    /**
     * Get ypos
     *
     * @return int
     */
    public function getYpos()
    {
        return $this->ypos;
    }

    /**
     * Set guid
     *
     * @param guid $guid
     *
     * @return InfoAddinfotype
     */
    public function setGuid($guid)
    {
        $this->guid = $guid;

        return $this;
    }

    /**
     * Get guid
     *
     * @return string
     */
    public function getGuid()
    {
        return $this->guid;
    }

    /**
     * Set currenttime
     *
     * @param \DateTime $currenttime
     *
     * @return InfoAddinfotype
     */
    public function setCurrenttime(\DateTime $currenttime)
    {
        $this->currenttime = $currenttime;

        return $this;
    }

    /**
     * Get currenttime
     *
     * @return \DateTime
     */
    public function getCurrenttime()
    {
        return $this->currenttime;
    }

    /**
     * Set moduser
     *
     * @param string $moduser
     *
     * @return InfoAddinfotype
     */
    public function setModuser($moduser)
    {
        $this->moduser = $moduser;

        return $this;
    }

    /**
     * Get moduser
     *
     * @return string
     */
    public function getModuser()
    {
        return $this->moduser;
    }

    /**
     * Set isshowhint
     *
     * @param int $isshowhint
     *
     * @return InfoAddinfotype
     */
    public function setIsshowhint($isshowhint)
    {
        $this->isshowhint = $isshowhint;

        return $this;
    }

    /**
     * Get isshowhint
     *
     * @return int
     */
    public function getIsshowhint()
    {
        return $this->isshowhint;
    }

    /**
     * Set customdictionary
     *
     * @param InfoCustomdictionary $customdictionary
     *
     * @return InfoAddinfotype
     */
    public function setCustomdictionary(InfoCustomdictionary $customdictionary = null)
    {
        $this->customdictionary = $customdictionary;

        return $this;
    }

    /**
     * Get customdictionary
     *
     * @return InfoCustomdictionary
     */
    public function getCustomdictionary()
    {
        return $this->customdictionary;
    }

    /**
     * Set systemdictionary
     *
     * @param InfoSystemdictionary $systemdictionary
     *
     * @return InfoAddinfotype
     */
    public function setSystemdictionary(InfoSystemdictionary $systemdictionary = null)
    {
        $this->systemdictionary = $systemdictionary;

        return $this;
    }

    /**
     * Get systemdictionary
     *
     * @return InfoSystemdictionary
     */
    public function getSystemdictionary()
    {
        return $this->systemdictionary;
    }

    /**
     * Add addInfoCompanyType
     *
     * @param InfoAddinfocompanytype $addInfoCompanyType
     *
     * @return InfoAddinfotype
     */
    public function addAddInfoCompanyType(InfoAddinfocompanytype $addInfoCompanyType)
    {
        if (!$this->addInfoCompanyTypes->contains($addInfoCompanyType)) {
            $addInfoCompanyType->setAddinfotype($this);
            $this->addInfoCompanyTypes->add($addInfoCompanyType);
        }

        return $this;
    }

    /**
     * Remove addInfoCompanyType
     *
     * @param InfoAddinfocompanytype $addInfoCompanyType
     */
    public function removeAddInfoCompanyType(InfoAddinfocompanytype $addInfoCompanyType)
    {
        $this->addInfoCompanyTypes->removeElement($addInfoCompanyType);
    }

    /**
     * Get addInfoCompanyTypes
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getAddInfoCompanyTypes()
    {
        return $this->addInfoCompanyTypes;
    }

    public function getAddInfoCompanyTypeSubjTypes()
    {
        $subjTypes = [];
        /**
         * @var InfoAddinfocompanytype $addInfoCompanyType
         */
        foreach ($this->addInfoCompanyTypes as $addInfoCompanyType) {
            $subjTypes[] = $addInfoCompanyType->getSubjtype()->getId();
        }
        return $subjTypes;
    }

    /**
     * Add addInfoContactType
     *
     * @param InfoAddinfocontacttype $addInfoContactType
     *
     * @return InfoAddinfotype
     */
    public function addAddInfoContactType(InfoAddinfocontacttype $addInfoContactType)
    {
        if (!$this->addInfoContactTypes->contains($addInfoContactType)) {
            $addInfoContactType->setAddinfotype($this);
            $this->addInfoContactTypes->add($addInfoContactType);
        }

        return $this;
    }

    /**
     * Remove addInfoContactType
     *
     * @param InfoAddinfocontacttype $addInfoContactType
     */
    public function removeAddInfoContactType(InfoAddinfocontacttype $addInfoContactType)
    {
        $this->addInfoContactTypes->removeElement($addInfoContactType);
    }

    /**
     * Get addInfoContactTypes
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getAddInfoContactTypes()
    {
        return $this->addInfoContactTypes;
    }

    public function getAddInfoContactTypeSubjTypes()
    {
        $subjTypes = [];
        /**
         * @var InfoAddinfocontacttype $addInfoContactType
         */
        foreach ($this->addInfoContactTypes as $addInfoContactType) {
            $subjTypes[] = $addInfoContactType->getSubjtype()->getId();
        }
        return $subjTypes;
    }

    /**
     * Add addInfoTaskType
     *
     * @param InfoAddinfotasktype $addInfoTaskType
     *
     * @return InfoAddinfotype
     */
    public function addAddInfoTaskType(InfoAddinfotasktype $addInfoTaskType)
    {
        if (!$this->addInfoTaskTypes->contains($addInfoTaskType)) {
            $addInfoTaskType->setAddinfotype($this);
            $this->addInfoTaskTypes->add($addInfoTaskType);
        }

        return $this;
    }

    /**
     * Remove addInfoTaskType
     *
     * @param InfoAddinfotasktype $addInfoTaskType
     */
    public function removeAddInfoTaskType(InfoAddinfotasktype $addInfoTaskType)
    {
        $this->addInfoTaskTypes->removeElement($addInfoTaskType);
    }

    /**
     * Get addInfoTaskTypes
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getAddInfoTaskTypes()
    {
        return $this->addInfoTaskTypes;
    }

    public function getAddInfoTaskTypeSubjTypes()
    {
        $subjTypes = [];
        /**
         * @var InfoAddinfotasktype $addInfoTaskType
         */
        foreach ($this->addInfoTaskTypes as $addInfoTaskType) {
            $subjTypes[] = $addInfoTaskType->getSubjtype()->getId();
        }
        return $subjTypes;
    }

    /**
     * Add addInfoActionType
     *
     * @param InfoAddinfoactiontype $addInfoActionType
     *
     * @return InfoAddinfotype
     */
    public function addAddInfoActionType(InfoAddinfoactiontype $addInfoActionType)
    {
        if (!$this->addInfoActionTypes->contains($addInfoActionType)) {
            $addInfoActionType->setAddinfotype($this);
            $this->addInfoActionTypes->add($addInfoActionType);
        }

        return $this;
    }

    /**
     * Remove addInfoActionType
     *
     * @param InfoAddinfoactiontype $addInfoActionType
     */
    public function removeAddInfoActionType(InfoAddinfoactiontype $addInfoActionType)
    {
        $this->addInfoActionTypes->removeElement($addInfoActionType);
    }

    /**
     * Get addInfoActionTypes
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getAddInfoActionTypes()
    {
        return $this->addInfoActionTypes;
    }

    public function getAddInfoActionTypeSubjTypes()
    {
        $subjTypes = [];
        /**
         * @var InfoAddinfoactiontype $addInfoActionType
         */
        foreach ($this->addInfoActionTypes as $addInfoActionType) {
            $subjTypes[] = $addInfoActionType->getSubjtype()->getId();
        }
        return $subjTypes;
    }

    /**
     * @param Collection $subjTypes
     * @return InfoAddinfotype
     */
    public function setAddInfoTypeSubjTypes(Collection $subjTypes)
    {
        switch ($this->getDatatype()) {
            case 1:
                $this->setCompanyTypes($subjTypes);
                break;
            case 2:
                $this->setContactTypes($subjTypes);
                break;
            case 4:
                $this->setTaskTypes($subjTypes);
                break;
            case 8:
                $this->setActionTypes($subjTypes);
                break;
        }
        return $this;
    }

    public function getAddInfoTypeSubjTypes()
    {
        $subjTypes = [];
        switch ($this->getDatatype()) {
            case 1:
                $subjTypes = $this->getAddInfoCompanyTypeSubjTypes();
                break;
            case 2:
                $subjTypes = $this->getAddInfoContactTypeSubjTypes();
                break;
            case 4:
                $subjTypes = $this->getAddInfoTaskTypeSubjTypes();
                break;
            case 8:
                $subjTypes = $this->getAddInfoActionTypeSubjTypes();
                break;
        }
        return $subjTypes;
    }

    public function getSubjectTypesFlat()
    {
        /**
         * @var InfoAddinfocompanytype[]|InfoAddinfocontacttype[]|InfoAddinfotasktype[]|InfoAddinfoactiontype[] $subjTypes
         */
        $subjTypes = [];
        switch ($this->getDatatype()) {
            case 1:
                $subjTypes = $this->getAddInfoCompanyTypes();
                break;
            case 2:
                $subjTypes = $this->getAddInfoContactTypes();
                break;
            case 4:
                $subjTypes = $this->getAddInfoTaskTypes();
                break;
            case 8:
                $subjTypes = $this->getAddInfoActionTypes();
                break;
        }

        return array_map(function ($subjectType) {
            return [
                'role_id' => $subjectType->getRole() ? $subjectType->getRole()->getId() : null,
                'subjtype_id' => $subjectType->getSubjtype()->getId(),
                'addinfotype_id' => $subjectType->getAddinfotype()->getId(),
            ];
        }, $subjTypes->toArray() ?? []);
    }

    public function getCustomDictionaryId()
    {
        return $this->getCustomdictionary() ? $this->getCustomdictionary()->getId() : null;
    }

    public function getSystemDictionaryId()
    {
        return $this->getSystemdictionary() ? $this->getSystemdictionary()->getId() : null;
    }

    /**
     * Add role
     *
     * @param InfoRole $role
     *
     * @return InfoAddinfotype
     */
    public function addRole(InfoRole $role)
    {
        if (!$this->roles->contains($role)) {
            $this->roles[] = $role;
            $role->addAddInfoType($this);
        }

        return $this;
    }

    /**
     * Remove role
     *
     * @param InfoRole $role
     */
    public function removeRole(InfoRole $role)
    {
        if ($this->roles->contains($role)) {
            $this->roles->removeElement($role);
            $role->removeAddInfoType($this);
        }
    }

    /**
     * Get roles
     *
     * @return ArrayCollection
     */
    public function getRoles()
    {
        return $this->roles;
    }

    /**
     * Set roles
     *
     * @return ArrayCollection
     */
    public function setRoles(ArrayCollection $roles)
    {
        $this->roles = $roles;

        return $this;
    }

    public function getAddInfoTypeRoles()
    {
        $rolesIds = [];
        /**
         * @var InfoRole $role
         */
        foreach ($this->roles as $role) {
            $rolesIds[] = $role->getId();
        }
        return $rolesIds;
    }

    /**
     * Add taskType
     *
     * @param InfoTasktype $taskType
     *
     * @return InfoAddinfotype
     */
    public function addTaskType(InfoTasktype $taskType)
    {
        if (!$this->taskTypes->contains($taskType)) {
            $this->taskTypes->add($taskType);
        }

        return $this;
    }

    /**
     * Remove taskType
     *
     * @param InfoTasktype $taskType
     */
    public function removeTaskType(InfoTasktype $taskType)
    {
        $this->taskTypes->removeElement($taskType);
    }

    /**
     * Get taskTypes
     *
     * @return ArrayCollection
     */
    public function getTaskTypes()
    {
        return $this->taskTypes;
    }

    /**
     * Set actionTypes
     *
     * @return ArrayCollection
     */
    public function setActionTypes(Collection $actionTypes)
    {
        $this->actionTypes = $actionTypes;

        return $this;
    }

    /**
     * Add actionType
     *
     * @param InfoActiontype $actionType
     *
     * @return InfoAddinfotype
     */
    public function addActiontype(InfoActiontype $actionType)
    {
        if (!$this->actionTypes->contains($actionType)) {
            $this->actionTypes->add($actionType);
        }

        return $this;
    }

    /**
     * Remove actionType
     *
     * @param InfoActiontype $actionType
     */
    public function removeActionType(InfoActiontype $actionType)
    {
        $this->actionTypes->removeElement($actionType);
    }

    /**
     * Get actionTypes
     *
     * @return ArrayCollection
     */
    public function getActionTypes()
    {
        return $this->actionTypes;
    }

    /**
     * Set taskTypes
     *
     * @return ArrayCollection
     */
    public function setTaskTypes(Collection $taskTypes)
    {
        $this->taskTypes = $taskTypes;

        return $this;
    }

    /**
     * Add companyType
     *
     * @param InfoCompanytype $companyType
     *
     * @return InfoAddinfotype
     */
    public function addCompanyType(InfoCompanytype $companyType)
    {
        if (!$this->companyTypes->contains($companyType)) {
            $this->companyTypes->add($companyType);
        }

        return $this;
    }

    /**
     * Remove companyType
     *
     * @param InfoCompanytype $companyType
     */
    public function removeCompanyType(InfoCompanytype $companyType)
    {
        $this->companyTypes->removeElement($companyType);
    }

    /**
     * Get companyTypes
     *
     * @return ArrayCollection
     */
    public function getCompanyTypes()
    {
        return $this->companyTypes;
    }

    /**
     * Set companyTypes
     *
     * @return ArrayCollection
     */
    public function setCompanyTypes(Collection $companyTypes)
    {
        $this->companyTypes = $companyTypes;

        return $this;
    }

    /**
     * Add contactType
     *
     * @param InfoContacttype $contactType
     *
     * @return InfoAddinfotype
     */
    public function addContactType(InfoContacttype $contactType)
    {
        if (!$this->contactTypes->contains($contactType)) {
            $this->contactTypes->add($contactType);
        }

        return $this;
    }

    /**
     * Remove contactType
     *
     * @param InfoContacttype $contactType
     */
    public function removeContactType(InfoContacttype $contactType)
    {
        $this->contactTypes->removeElement($contactType);
    }

    /**
     * Set contactTypes
     *
     * @return self
     */
    public function setContactTypes(Collection $contactTypes)
    {
        $this->contactTypes = $contactTypes;

        return $this;
    }

    /**
     * Get contactTypes
     *
     * @return ArrayCollection
     */
    public function getContactTypes()
    {
        return $this->contactTypes;
    }

    /**
     * @return InfoDirection|null
     */
    public function getDirection(): ?InfoDirection
    {
        return $this->direction;
    }

    /**
     * @param InfoDirection|null $direction
     * @return self
     */
    public function setDirection(?InfoDirection $direction): self
    {
        $this->direction = $direction;

        return $this;
    }

    public function getDirectionId(): ?int
    {
        return $this->getDirection() ? $this->getDirection()->getId() : null;
    }
}
