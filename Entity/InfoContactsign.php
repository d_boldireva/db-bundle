<?php

namespace TeamSoft\CrmRepositoryBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use TeamSoft\CrmRepositoryBundle\Model\InfoFile;

/**
 * InfoContactSign
 *
 * @ORM\Table(name="info_contactsign")
 * @ORM\Entity(repositoryClass="TeamSoft\CrmRepositoryBundle\Repository\InfoContactsignRepository")
 */
class InfoContactsign extends InfoFile implements ServiceFieldInterface
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="currenttime", type="datetime", length=255, nullable=true)
     */
    private $currenttime;

    /**
     * @var string
     *
     * @ORM\Column(name="moduser", type="string", length=16, nullable=true)
     */
    private $moduser;

    /**
     * @var string
     *
     * @ORM\Column(name="guid", type="guid", nullable=true)
     */
    private $guid;

    /**
     * @var \TeamSoft\CrmRepositoryBundle\Entity\InfoContact
     *
     * @ORM\ManyToOne(targetEntity="InfoContact", inversedBy="contactSigns")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="contact_id", referencedColumnName="id")
     * })
     */
    private $contact;

    /**
     * @var ArrayCollection
     *
     * @ORM\OneToMany (
     *     targetEntity="InfoContactsignfile",
     *     mappedBy="contactSign",
     *     cascade={"persist"}
     * )
     */
    private $contactSignFiles;

    /**
     * @var resource | string
     *
     * @ORM\Column(name="sign", type="blob_or_string", nullable=true)
     */
    private $sign;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="createdate", type="datetime", nullable=true)
     */
    private $createdate;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="modified", type="datetime", nullable=true)
     */
    private $modified;

    /**
     * @var string
     *
     * @ORM\Column(name="signtype", type="string", nullable=true)
     */
    private $signType;

    /**
     * @var InfoDictionaryremote
     *
     * @ORM\OneToOne(targetEntity="InfoDictionaryremote")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="agreement_id", referencedColumnName="id")
     * })
     */
    private $agreement;

    /**
     * @var int|null
     *
     * @ORM\Column(name="isarchive", type="integer", nullable=true)
     */
    private $isArchive;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="archive_date", type="datetime", nullable=true)
     */
    private $archiveDate;

    public function __construct()
    {
        $this->contactSignFiles = new ArrayCollection();
    }

    /**
     * Set guid
     *
     * @param guid $guid
     * @return InfoContactsign
     */
    public function setGuid($guid)
    {
        $this->guid = $guid;

        return $this;
    }

    /**
     * Get guid
     *
     * @return guid
     */
    public function getGuid()
    {
        return $this->guid;
    }

    /**
     * Set currenttime
     *
     * @param \DateTime $currenttime
     * @return InfoContactsign
     */
    public function setCurrenttime(\DateTime $currenttime)
    {
        $this->currenttime = $currenttime;

        return $this;
    }

    /**
     * Get currenttime
     *
     * @return \DateTime
     */
    public function getCurrenttime()
    {
        return $this->currenttime;
    }

    /**
     * Set moduser
     *
     * @param string $moduser
     * @return InfoContactsign
     */
    public function setModuser($moduser)
    {
        $this->moduser = $moduser;

        return $this;
    }

    /**
     * Get moduser
     *
     * @return string
     */
    public function getModuser()
    {
        return $this->moduser;
    }

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Get contact
     *
     * @return InfoContact
     */
    public function getContact()
    {
        return $this->contact;
    }

    /**
     * Set contact
     * @param InfoContact $contact
     * @return InfoContactsign
     */
    public function setContact(?InfoContact $contact)
    {
        $this->contact = $contact;

        return $this;
    }

    /**
     * Set sign.
     *
     * @param blob_or_string|null $sign
     *
     * @return InfoContactsign
     */
    public function setSign($sign = null)
    {
        $this->sign = $sign;

        return $this;
    }

    /**
     * Get sign.
     *
     * @return blob_or_string|null
     */
    public function getSign()
    {
        return $this->sign;
    }

    /**
     * Set createdate.
     *
     * @param \DateTime|null $createdate
     *
     * @return InfoContactsign
     */
    public function setCreatedate($createdate = null)
    {
        $this->createdate = $createdate;

        return $this;
    }

    /**
     * Get createdate.
     *
     * @return \DateTime|null
     */
    public function getCreatedate()
    {
        return $this->createdate;
    }

    /**
     * Set modified.
     *
     * @param \DateTime|null $modified
     *
     * @return InfoContactsign
     */
    public function setModified($modified = null)
    {
        $this->modified = $modified;

        return $this;
    }

    /**
     * Get modified.
     *
     * @return \DateTime|null
     */
    public function getModified()
    {
        return $this->modified;
    }

    public function getName()
    {
        return '';
    }

    /**
     * Set content
     *
     * @param string $content
     *
     * @return InfoContactsign
     */
    public function setContent($content)
    {
        return $this->setSign($content);
    }

    /**
     * Get content
     *
     * @return resource | string
     */
    public function getContent()
    {
        return $this->getSign();
    }

    /**
     * Get contactSignFiles
     *
     * @return InfoContactsignfile[]|Collection|ArrayCollection
     */
    public function getContactSignFiles(): Collection
    {
        return $this->contactSignFiles;
    }

    /**
     * @param InfoContactsignfile $contactsign
     * @return $this
     */
    public function addContactSignFile(InfoContactsignfile $contactsignfile): self
    {
        if (!$this->contactSignFiles->contains($contactsignfile)) {
            $contactsignfile->setContactSign($this);
            $this->contactSignFiles->add($contactsignfile);
        }

        return $this;
    }

    /**
     * @param InfoContactsignfile $contactsign
     * @return $this
     */
    public function removeContactSignFile(InfoContactsignfile $contactsignfile): self
    {
        if (!$this->contactSignFiles->contains($contactsignfile)) {
            $contactsignfile->setContactSign(null);
            $this->contactSignFiles->removeElement($contactsignfile);
        }

        return $this;
    }

    /**
     * Set agreement
     *
     * @param InfoDictionaryremote|null
     *
     * @return self
     */
    public function setAgreement(?InfoDictionaryremote $agreement): self
    {
        $this->agreement = $agreement;

        return $this;
    }

    /**
     * Get agreement
     *
     * @return InfoDictionaryremote|null
     */
    public function getAgreement(): ?InfoDictionaryremote
    {
        return $this->agreement;
    }

    /**
     * @return string|null
     */
    public function getSignType(): ?string
    {
        return $this->signType;
    }

    /**
     * @param string|null $signType
     * @return $this
     */
    public function setSignType(?string $signType): self
    {
        $this->signType = $signType;
        return $this;
    }

    public function getIsArchive(): ?int
    {
        return $this->isArchive;
    }

    public function setIsArchive(?int $isArchive): self
    {
        $this->isArchive = $isArchive;
        return $this;
    }

    public function getArchiveDate(): ?\DateTime
    {
        return $this->archiveDate;
    }

    public function setArchiveDate(?\DateTime $archiveDate): self
    {
        $this->archiveDate = $archiveDate;
        return $this;
    }
}
