<?php

namespace TeamSoft\CrmRepositoryBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * InfoContactspec
 *
 * @ORM\Table(name="info_contactspec")
 * @ORM\Entity(repositoryClass="TeamSoft\CrmRepositoryBundle\Repository\InfoContactspec")
 */
class InfoContactspec implements ServiceFieldInterface
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var guid
     *
     * @ORM\Column(name="guid", type="guid", nullable=true)
     */
    private $guid;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="currenttime", type="datetime", nullable=true)
     */
    private $currenttime;

    /**
     * @var string
     *
     * @ORM\Column(name="moduser", type="string", length=16, nullable=true)
     */
    private $moduser;

    /**
     * @var string
     *
     * @ORM\Column(name="Description", type="blob", nullable=true)
     */
    private $description;

    /**
     * @var int
     *
     * @ORM\Column(name="statusverification", type="integer", nullable=true)
     */
    private $statusverification;

    /**
     * @var InfoDictionary
     *
     * @ORM\ManyToOne(targetEntity="InfoDictionary")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="Department_id", referencedColumnName="id")
     * })
     */
    private $department;

    /**
     * @var InfoDictionary
     *
     * @ORM\ManyToOne(targetEntity="InfoDictionary")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="Position_id", referencedColumnName="id")
     * })
     */
    private $position;

    /**
     * @var InfoContact
     *
     * @ORM\ManyToOne(targetEntity="InfoContact", inversedBy="contactSpecializationCollection")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="contact_id", referencedColumnName="id")
     * })
     */
    private $contact;

    /**
     * @var InfoDictionary
     *
     * @ORM\ManyToOne(targetEntity="InfoDictionary")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="spec_id", referencedColumnName="id")
     * })
     */
    private $spec;



    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set guid
     *
     * @param guid $guid
     *
     * @return InfoContactspec
     */
    public function setGuid($guid)
    {
        $this->guid = $guid;
    
        return $this;
    }

    /**
     * Get guid
     *
     * @return guid
     */
    public function getGuid()
    {
        return $this->guid;
    }

    /**
     * Set currenttime
     *
     * @param \DateTime $currenttime
     *
     * @return InfoContactspec
     */
    public function setCurrenttime(\DateTime $currenttime)
    {
        $this->currenttime = $currenttime;
    
        return $this;
    }

    /**
     * Get currenttime
     *
     * @return \DateTime
     */
    public function getCurrenttime()
    {
        return $this->currenttime;
    }

    /**
     * Set moduser
     *
     * @param string $moduser
     *
     * @return InfoContactspec
     */
    public function setModuser($moduser)
    {
        $this->moduser = $moduser;
    
        return $this;
    }

    /**
     * Get moduser
     *
     * @return string
     */
    public function getModuser()
    {
        return $this->moduser;
    }

    /**
     * Set description
     *
     * @param string $description
     *
     * @return InfoContactspec
     */
    public function setDescription($description)
    {
        $this->description = $description;
    
        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set statusverification
     *
     * @param int $statusverification
     *
     * @return InfoContactspec
     */
    public function setStatusverification($statusverification)
    {
        $this->statusverification = $statusverification;
    
        return $this;
    }

    /**
     * Get statusverification
     *
     * @return int
     */
    public function getStatusverification()
    {
        return $this->statusverification;
    }

    /**
     * Set department
     *
     * @param \TeamSoft\CrmRepositoryBundle\Entity\InfoDictionary $department
     *
     * @return InfoContactspec
     */
    public function setDepartment(\TeamSoft\CrmRepositoryBundle\Entity\InfoDictionary $department = null)
    {
        $this->department = $department;
    
        return $this;
    }

    /**
     * Get department
     *
     * @return \TeamSoft\CrmRepositoryBundle\Entity\InfoDictionary
     */
    public function getDepartment()
    {
        return $this->department;
    }

    /**
     * Set position
     *
     * @param \TeamSoft\CrmRepositoryBundle\Entity\InfoDictionary $position
     *
     * @return InfoContactspec
     */
    public function setPosition(\TeamSoft\CrmRepositoryBundle\Entity\InfoDictionary $position = null)
    {
        $this->position = $position;
    
        return $this;
    }

    /**
     * Get position
     *
     * @return \TeamSoft\CrmRepositoryBundle\Entity\InfoDictionary
     */
    public function getPosition()
    {
        return $this->position;
    }

    /**
     * Set contact
     *
     * @param \TeamSoft\CrmRepositoryBundle\Entity\InfoContact $contact
     *
     * @return InfoContactspec
     */
    public function setContact(\TeamSoft\CrmRepositoryBundle\Entity\InfoContact $contact = null)
    {
        $this->contact = $contact;
    
        return $this;
    }

    /**
     * Get contact
     *
     * @return \TeamSoft\CrmRepositoryBundle\Entity\InfoContact
     */
    public function getContact()
    {
        return $this->contact;
    }

    /**
     * Set spec
     *
     * @param \TeamSoft\CrmRepositoryBundle\Entity\InfoDictionary $spec
     *
     * @return InfoContactspec
     */
    public function setSpec(\TeamSoft\CrmRepositoryBundle\Entity\InfoDictionary $spec = null)
    {
        $this->spec = $spec;
    
        return $this;
    }

    /**
     * Get spec
     *
     * @return \TeamSoft\CrmRepositoryBundle\Entity\InfoDictionary
     */
    public function getSpec()
    {
        return $this->spec;
    }

    public function getContactId()
    {
        return $this->getContact() ? $this->getContact()->getId() : null;
    }

    public function getSpecId()
    {
        return $this->getSpec() ? $this->getSpec()->getId() : null;
    }
}
