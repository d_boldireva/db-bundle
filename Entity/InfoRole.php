<?php

namespace TeamSoft\CrmRepositoryBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use TeamSoft\CrmRepositoryBundle\Entity\GridConfig\PoGridConfigByRole;

/**
 * InfoRole
 *
 * @ORM\Table(name="info_role")
 * @ORM\Entity()
 */
class InfoRole implements ServiceFieldInterface
{
    const SUPER_ADMIN_RANG = 1;
    const ADMIN_RANG = 2;

    public const ROLE_ADMIN = 'ROLE_ADMIN';
    public const ROLE_SUPER_ADMIN = 'ROLE_SUPER_ADMIN';
    public const ROLE_ACCESS_TO_CAR_REPORT_USER = 'ROLE_ACCESS_TO_CAR_REPORT_USER';
    public const ROLE_ACCESS_TO_CAR_REPORT_ADMIN = 'ROLE_ACCESS_TO_CAR_REPORT_ADMIN';

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255, nullable=true)
     */
    private $name;

    /**
     * @var int
     *
     * @ORM\Column(name="is_archived", type="integer", nullable=true)
     */
    private $isArchived;

    /**
     * @var int
     *
     * @ORM\Column(name="rang", type="integer", nullable=true)
     */
    private $rang;

    /**
     * @var string
     *
     * @ORM\Column(name="guid", type="guid", nullable=true)
     */
    private $guid;

    /**
     * @var string
     *
     * @ORM\Column(name="code", type="string", nullable=true)
     */
    private $code;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="currenttime", type="datetime", nullable=true)
     */
    private $currenttime;

    /**
     * @var string
     *
     * @ORM\Column(name="moduser", type="string", length=16, nullable=true)
     */
    private $moduser;

    /**
     * @ORM\OneToMany(targetEntity="InfoOptionsbyrole", mappedBy="role", cascade={"persist", "remove"}, orphanRemoval=true)
     */
    private $options;

    /**
     * @ORM\OneToMany(targetEntity="InfoEntityMap", mappedBy="role", cascade={"persist", "remove"}, orphanRemoval=true)
     */
    private $entityMaps;

    /**
     * @ORM\OneToMany(targetEntity="InfoTabcontrol", mappedBy="role", cascade={"persist", "remove"}, orphanRemoval=true)
     */
    private $tabControls;

    /**
     * @ORM\OneToMany(targetEntity="InfoRequired", mappedBy="role", cascade={"persist", "remove"}, orphanRemoval=true)
     */
    private $requiredFields;

    /**
     * @ORM\OneToMany(targetEntity="InfoReadonlyfield", mappedBy="role", cascade={"persist", "remove"}, orphanRemoval=true)
     */
    private $readonlyFields;

    /**
     * @ORM\OneToMany(targetEntity="InfoHidefield", mappedBy="role", cascade={"persist", "remove"}, orphanRemoval=true)
     */
    private $hiddenFields;

    /**
     * @ORM\ManyToMany(targetEntity="InfoDcrtype", cascade={"persist", "remove"})
     * @ORM\JoinTable(name="info_dcrtyperoleworkflow",
     *   joinColumns={@ORM\JoinColumn(name="role_id", referencedColumnName="id")},
     *   inverseJoinColumns={@ORM\JoinColumn(name="dcrtype_id", referencedColumnName="id")}
     * )
     */
    private $dcrTypes;

    /**
     * @ORM\ManyToMany(targetEntity="InfoServiceprivilege", cascade={"persist", "remove"})
     * @ORM\JoinTable(name="info_roleprivilege",
     *   joinColumns={@ORM\JoinColumn(name="role_id", referencedColumnName="id")},
     *   inverseJoinColumns={@ORM\JoinColumn(name="privilege_id", referencedColumnName="id")}
     * )
     */
    private $privileges;

    /**
     * @ORM\ManyToMany(targetEntity="InfoAddinfotype", cascade={"persist", "remove"})
     * @ORM\JoinTable(name="info_addinfotypebyrole",
     *   joinColumns={@ORM\JoinColumn(name="role_id", referencedColumnName="id")},
     *   inverseJoinColumns={@ORM\JoinColumn(name="addInfoType_id", referencedColumnName="id")}
     * )
     */
    private $addInfoTypes;

    /**
     * @ORM\ManyToMany(targetEntity="InfoTasktype", cascade={"persist", "remove"})
     * @ORM\JoinTable(name="info_tasktypebyrole",
     *   joinColumns={@ORM\JoinColumn(name="role_id", referencedColumnName="id")},
     *   inverseJoinColumns={@ORM\JoinColumn(name="tasktype_id", referencedColumnName="id")}
     * )
     */
    private $taskTypes;

    /**
     * @ORM\ManyToMany(targetEntity="InfoActiontype", cascade={"persist", "remove"})
     * @ORM\JoinTable(name="info_actiontypebyrole",
     *   joinColumns={@ORM\JoinColumn(name="role_id", referencedColumnName="id")},
     *   inverseJoinColumns={@ORM\JoinColumn(name="actiontype_id", referencedColumnName="id")}
     * )
     */
    private $actionTypes;

    /**
     * @ORM\ManyToMany(targetEntity="InfoCompanytype", cascade={"persist", "remove"})
     * @ORM\JoinTable(name="info_companytypebyrole",
     *   joinColumns={@ORM\JoinColumn(name="role_id", referencedColumnName="id")},
     *   inverseJoinColumns={@ORM\JoinColumn(name="companytype_id", referencedColumnName="id")}
     * )
     */
    private $companyTypes;

    /**
     * @ORM\ManyToMany(targetEntity="InfoContacttype")
     * @ORM\JoinTable(name="info_contacttypebyrole",
     *   joinColumns={@ORM\JoinColumn(name="role_id", referencedColumnName="id")},
     *   inverseJoinColumns={@ORM\JoinColumn(name="contacttype_id", referencedColumnName="id")}
     * )
     */
    private $contactTypes;

    /**
     * @ORM\ManyToOne(targetEntity="InfoRole")
     * @ORM\JoinColumn(name="parent_id", referencedColumnName="id")
     */
    private $parent;

    /**
     * @var PoGridConfigByRole[]|Collection|ArrayCollection
     * @ORM\OneToMany(targetEntity="TeamSoft\CrmRepositoryBundle\Entity\GridConfig\PoGridConfigByRole", mappedBy="role", cascade={"persist", "remove"}, orphanRemoval=true)
     */
    private $gridConfigByRoles;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->options = new ArrayCollection();
        $this->entityMaps = new ArrayCollection();
        $this->tabControls = new ArrayCollection();
        $this->requiredFields = new ArrayCollection();
        $this->readonlyFields = new ArrayCollection();
        $this->hiddenFields = new ArrayCollection();
        $this->addInfoTypes = new ArrayCollection();
        $this->dcrTypes = new ArrayCollection();
        $this->privileges = new ArrayCollection();
        $this->taskTypes = new ArrayCollection();
        $this->actionTypes = new ArrayCollection();
        $this->companyTypes = new ArrayCollection();
        $this->contactTypes = new ArrayCollection();
        $this->gridConfigByRoles = new ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return InfoRole
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set code
     *
     * @param string $code
     * @return InfoRole
     */
    public function setCode($code)
    {
        $this->code = $code;

        return $this;
    }

    /**
     * Get code
     *
     * @return string
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * @return int|null
     */
    public function getIsArchived(): ?int
    {
        return $this->isArchived;
    }

    /**
     * @param  int|null  $isArchived
     *
     * @return InfoRole
     */
    public function setIsArchived(?int $isArchived): self
    {
        $this->isArchived = $isArchived;

        return $this;
    }

    /**
     * Set rang
     *
     * @param string $rang
     * @return InfoRole
     */
    public function setRang($rang)
    {
        $this->rang = $rang;

        return $this;
    }

    /**
     * Get rang
     *
     * @return number
     */
    public function getRang()
    {
        return $this->getCode() === 'ROLE_SUPER_ADMIN' ? self::SUPER_ADMIN_RANG : $this->rang;
    }

    /**
     * Set guid
     *
     * @param string $guid
     * @return InfoRole
     */
    public function setGuid($guid)
    {
        $this->guid = $guid;

        return $this;
    }

    /**
     * Get guid
     *
     * @return string
     */
    public function getGuid()
    {
        return $this->guid;
    }

    /**
     * Set currenttime
     *
     * @param \DateTime $currenttime
     * @return InfoRole
     */
    public function setCurrenttime(\DateTime $currenttime)
    {
        $this->currenttime = $currenttime;

        return $this;
    }

    /**
     * Get currenttime
     *
     * @return \DateTime
     */
    public function getCurrenttime()
    {
        return $this->currenttime;
    }

    /**
     * Set moduser
     *
     * @param string $moduser
     * @return InfoRole
     */
    public function setModuser($moduser)
    {
        $this->moduser = $moduser;

        return $this;
    }

    /**
     * Get moduser
     *
     * @return string
     */
    public function getModuser()
    {
        return $this->moduser;
    }

    /**
     * Add dcrType
     *
     * @param InfoDcrtype $dcrType
     *
     * @return InfoRole
     */
    public function addDcrType(InfoDcrtype $dcrType)
    {
        $this->dcrTypes->add($dcrType);

        return $this;
    }

    /**
     * Remove dcrType
     *
     * @param InfoDcrtype $dcrType
     */
    public function removeDcrType(InfoDcrtype $dcrType)
    {
        $this->dcrTypes->removeElement($dcrType);
    }

    /**
     * Get dcrTypes
     *
     * @return ArrayCollection
     */
    public function getDcrTypes()
    {
        return $this->dcrTypes;
    }

    /**
     * Set dcrTypes
     *
     * @return InfoRole
     */
    public function setDcrTypes(ArrayCollection $dcrTypes)
    {
        $this->dcrTypes = $dcrTypes;

        return $this;
    }

    /**
     * Add privilege
     *
     * @param InfoServiceprivilege $privilege
     *
     * @return InfoRole
     */
    public function addPrivilege(InfoServiceprivilege $privilege)
    {
        $this->privileges->add($privilege);

        return $this;
    }

    /**
     * Remove privilege
     *
     * @param InfoServiceprivilege $privilege
     */
    public function removePrivilege(InfoServiceprivilege $privilege)
    {
        $this->privileges->removeElement($privilege);
    }

    /**
     * Get privileges
     *
     * @return ArrayCollection
     */
    public function getPrivileges()
    {
        return $this->privileges;
    }

    /**
     * Set privileges
     *
     * @return InfoRole
     */
    public function setPrivileges(ArrayCollection $privileges)
    {
        $this->privileges = $privileges;

        return $this;
    }

    /**
     * Add addInfoType
     *
     * @param InfoAddinfotype $addInfoType
     *
     * @return InfoRole
     */
    public function addAddInfoType(InfoAddinfotype $addInfoType)
    {
        $this->addInfoTypes->add($addInfoType);

        return $this;
    }

    /**
     * Remove addInfoType
     *
     * @param InfoAddinfotype $addInfoType
     */
    public function removeAddInfoType(InfoAddinfotype $addInfoType)
    {
        $this->addInfoTypes->removeElement($addInfoType);
    }

    /**
     * Get addInfoTypes
     *
     * @return ArrayCollection
     */
    public function getAddInfoTypes()
    {
        return $this->addInfoTypes;
    }

    /**
     * Set addInfoTypes
     *
     * @return InfoRole
     */
    public function setAddInfoTypes(ArrayCollection $addInfoTypes)
    {
        $this->addInfoTypes = $addInfoTypes;

        return $this;
    }

    /**
     * Add taskType
     *
     * @param InfoTasktype $taskType
     *
     * @return InfoRole
     */
    public function addTaskType(InfoTasktype $taskType)
    {
        if (!$this->taskTypes->contains($taskType)) {
            $this->taskTypes->add($taskType);
        }

        return $this;
    }

    /**
     * Remove taskType
     *
     * @param InfoTasktype $taskType
     */
    public function removeTaskType(InfoTasktype $taskType)
    {
        $this->taskTypes->removeElement($taskType);
    }

    /**
     * Get taskTypes
     *
     * @return ArrayCollection
     */
    public function getTaskTypes()
    {
        return $this->taskTypes;
    }

    /**
     * Set actionTypes
     *
     * @return InfoRole
     */
    public function setActionTypes(ArrayCollection $actionTypes)
    {
        $this->actionTypes = $actionTypes;

        return $this;
    }

    /**
     * Add actionType
     *
     * @param InfoActiontype $actionType
     *
     * @return InfoRole
     */
    public function addActiontype(InfoActiontype $actionType)
    {
        if (!$this->actionTypes->contains($actionType)) {
            $this->actionTypes->add($actionType);
        }

        return $this;
    }

    /**
     * Remove actionType
     *
     * @param InfoActiontype $actionType
     */
    public function removeActionType(InfoActiontype $actionType)
    {
        $this->actionTypes->removeElement($actionType);
    }

    /**
     * Get actionTypes
     *
     * @return ArrayCollection
     */
    public function getActionTypes()
    {
        return $this->actionTypes;
    }

    /**
     * Set taskTypes
     *
     * @return InfoRole
     */
    public function setTaskTypes(ArrayCollection $taskTypes)
    {
        $this->taskTypes = $taskTypes;

        return $this;
    }

    /**
     * Add companyType
     *
     * @param InfoCompanytype $companyType
     *
     * @return InfoRole
     */
    public function addCompanyType(InfoCompanytype $companyType)
    {
        if (!$this->companyTypes->contains($companyType)) {
            $this->companyTypes->add($companyType);
        }

        return $this;
    }

    /**
     * Remove companyType
     *
     * @param InfoCompanytype $companyType
     */
    public function removeCompanyType(InfoCompanytype $companyType)
    {
        $this->companyTypes->removeElement($companyType);
    }

    /**
     * Get companyTypes
     *
     * @return ArrayCollection
     */
    public function getCompanyTypes()
    {
        return $this->companyTypes;
    }

    /**
     * Set companyTypes
     *
     * @return InfoRole
     */
    public function setCompanyTypes(ArrayCollection $companyTypes)
    {
        $this->companyTypes = $companyTypes;

        return $this;
    }

    /**
     * Add contactType
     *
     * @param InfoContacttype $contactType
     *
     * @return InfoRole
     */
    public function addContactType(InfoContacttype $contactType)
    {
        if (!$this->contactTypes->contains($contactType)) {
            $this->contactTypes->add($contactType);
        }

        return $this;
    }

    /**
     * Remove contactType
     *
     * @param InfoContacttype $contactType
     */
    public function removeContactType(InfoContacttype $contactType)
    {
        $this->contactTypes->removeElement($contactType);
    }

    /**
     * Set contactTypes
     *
     * @return InfoRole
     */
    public function setContactTypes(ArrayCollection $contactTypes)
    {
        $this->contactTypes = $contactTypes;

        return $this;
    }

    /**
     * Get contactTypes
     *
     * @return ArrayCollection
     */
    public function getContactTypes()
    {
        return $this->contactTypes;
    }

    /**
     * Add option
     *
     * @param InfoOptions $option
     *
     * @return InfoRole
     */
    public function addOption(InfoOptionsbyrole $option)
    {
        if (!$this->options->contains($option)) {
            $option->setRole($this);
            $this->options->add($option);
        }

        return $this;
    }

    /**
     * Remove option
     *
     * @param InfoOptions $option
     */
    public function removeOption(InfoOptionsbyrole $option)
    {
        $this->options->removeElement($option);
    }

    /**
     * Get options
     *
     * @return ArrayCollection
     */
    public function getOptions()
    {
        return $this->options;
    }

    /**
     * Set options
     *
     * @return InfoRole
     */
    public function setOptions(ArrayCollection $options)
    {
        $this->options = $options;

        return $this;
    }

    /**
     * Add entityMap
     *
     * @param InfoEntityMap $entityMap
     *
     * @return InfoRole
     */
    public function addEntityMap(InfoEntityMap $entityMap)
    {
        if (!$this->entityMaps->contains($entityMap)) {
            $entityMap->setRole($this);
            $this->entityMaps->add($entityMap);
        }

        return $this;
    }

    /**
     * Remove entityMap
     *
     * @param InfoEntityMap $entityMap
     */
    public function removeEntityMap(InfoEntityMap $entityMap)
    {
        $this->entityMaps->removeElement($entityMap);
    }

    /**
     * Get entityMaps
     *
     * @return ArrayCollection
     */
    public function getEntityMaps()
    {
        return $this->entityMaps;
    }

    /**
     * Set entityMaps
     *
     * @return InfoRole
     */
    public function setEntityMaps(ArrayCollection $entityMaps)
    {
        $this->entityMaps = $entityMaps;

        return $this;
    }

    /**
     * Add tabControl
     *
     * @param InfoTabcontrol $tabControl
     *
     * @return InfoRole
     */
    public function addTabControl(InfoTabcontrol $tabControl)
    {
        if (!$this->tabControls->contains($tabControl)) {
            $tabControl->setRole($this);
            $this->tabControls->add($tabControl);
        }

        return $this;
    }

    /**
     * Remove tabControl
     *
     * @param InfoTabcontrol $tabControl
     */
    public function removeTabControl(InfoTabcontrol $tabControl)
    {
        $this->tabControls->removeElement($tabControl);
    }

    /**
     * Get tabControls
     *
     * @return ArrayCollection
     */
    public function getTabControls()
    {
        return $this->tabControls;
    }

    /**
     * Add requiredField
     *
     * @param InfoRequired $requiredField
     *
     * @return InfoRole
     */
    public function addRequiredField(InfoRequired $requiredField)
    {
        if (!$this->requiredFields->contains($requiredField)) {
            $requiredField->setRole($this);
            $this->requiredFields->add($requiredField);
        }

        return $this;
    }

    /**
     * Remove requiredField
     *
     * @param InfoRequired $requiredField
     */
    public function removeRequiredField(InfoRequired $requiredField)
    {
        $this->requiredFields->removeElement($requiredField);
    }

    /**
     * Get requiredFields
     *
     * @return ArrayCollection
     */
    public function getRequiredFields()
    {
        return $this->requiredFields;
    }

    /**
     * Set requiredFields
     *
     * @return InfoRole
     */
    public function setRequiredFields(ArrayCollection $requiredFields)
    {
        $this->requiredFields = $requiredFields;

        return $this;
    }

    /**
     * Add readonlyField
     *
     * @param InfoReadonlyField $readonlyField
     *
     * @return InfoRole
     */
    public function addReadonlyField(InfoReadonlyField $readonlyField)
    {
        if (!$this->readonlyFields->contains($readonlyField)) {
            $readonlyField->setRole($this);
            $this->readonlyFields->add($readonlyField);
        }

        return $this;
    }

    /**
     * Remove readonlyField
     *
     * @param InfoReadonlyField $readonlyField
     */
    public function removeReadonlyField(InfoReadonlyField $readonlyField)
    {
        $this->readonlyFields->removeElement($readonlyField);
    }

    /**
     * Get readonlyFields
     *
     * @return ArrayCollection
     */
    public function getReadonlyFields()
    {
        return $this->readonlyFields;
    }

    /**
     * Set readonlyFields
     *
     * @return InfoRole
     */
    public function setReadonlyFields(ArrayCollection $readonlyFields)
    {
        $this->readonlyFields = $readonlyFields;

        return $this;
    }

    /**
     * Add hiddenField
     *
     * @param InfoHidefield $hiddenField
     *
     * @return InfoRole
     */
    public function addHiddenField(InfoHidefield $hiddenField)
    {
        if (!$this->hiddenFields->contains($hiddenField)) {
            $hiddenField->setRole($this);
            $this->hiddenFields->add($hiddenField);
        }

        return $this;
    }

    /**
     * Remove hiddenField
     *
     * @param InfoHidefield $hiddenField
     */
    public function removeHiddenField(InfoHidefield $hiddenField)
    {
        $this->hiddenFields->removeElement($hiddenField);
    }

    /**
     * Get hiddenFields
     *
     * @return ArrayCollection
     */
    public function getHiddenFields()
    {
        return $this->hiddenFields;
    }

    /**
     * Set hiddenFields
     *
     * @return InfoRole
     */
    public function setHiddenFields(ArrayCollection $hiddenFields)
    {
        $this->hiddenFields = $hiddenFields;

        return $this;
    }

    /**
     * Set parent
     *
     * @param InfoRole $parent
     * @return InfoRole
     */
    public function setParent(InfoRole $parent)
    {
        $this->parent = $parent;

        return $this;
    }

    /**
     * Get parent
     *
     * @return InfoRole
     */
    public function getParent()
    {
        return $this->parent;
    }

    /**
     * @return Collection|PoGridConfigByRole[]
     */
    public function getGridConfigByRoles(): Collection
    {
        return $this->gridConfigByRoles;
    }

    /**
     * @param PoGridConfigByRole $gridConfigByRole
     * @return $this
     */
    public function addGridConfigByRole(PoGridConfigByRole $gridConfigByRole): self
    {
        if (!$this->gridConfigByRoles->contains($gridConfigByRole)) {
            $this->gridConfigByRoles->add($gridConfigByRole);
            $gridConfigByRole->setRole($this);
        }

        return $this;
    }

    /**
     * @param PoGridConfigByRole $gridConfigByRole
     * @return $this
     */
    public function removeGridConfigByRole(PoGridConfigByRole $gridConfigByRole): self
    {
        if ($this->gridConfigByRoles->remove($gridConfigByRole)) {
            $gridConfigByRole->setRole(null);
        }

        return $this;
    }
}
