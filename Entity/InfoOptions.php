<?php

namespace TeamSoft\CrmRepositoryBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * InfoOptions
 *
 * @ORM\Table(name="info_options", indexes={@ORM\Index(name="ix_info_options_guid", columns={"guid"})})
 * @ORM\Entity
 */
class InfoOptions implements ServiceFieldInterface
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=100, nullable=true)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="value", type="string", length=100, nullable=true)
     */
    private $value;

    /**
     * @var guid
     *
     * @ORM\Column(name="guid", type="guid", nullable=true)
     */
    private $guid;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="currenttime", type="datetime", nullable=true)
     */
    private $currenttime;

    /**
     * @var string
     *
     * @ORM\Column(name="moduser", type="string", length=16, nullable=true)
     */
    private $moduser;

    /**
     * @ORM\OneToMany(targetEntity="InfoOptionsbyrole", mappedBy="option", cascade={"persist"}, orphanRemoval=true)
     */
    private $optionsByRole;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->optionsByRole = new ArrayCollection();
    }


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return InfoOptions
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set value
     *
     * @param string $value
     * @return InfoOptions
     */
    public function setValue($value)
    {
        $this->value = $value;

        return $this;
    }

    /**
     * Get value
     *
     * @return string
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     * Set guid
     *
     * @param guid $guid
     * @return InfoOptions
     */
    public function setGuid($guid)
    {
        $this->guid = $guid;

        return $this;
    }

    /**
     * Get guid
     *
     * @return guid
     */
    public function getGuid()
    {
        return $this->guid;
    }

    /**
     * Set currenttime
     *
     * @param \DateTime $currenttime
     * @return InfoOptions
     */
    public function setCurrenttime(\DateTime $currenttime)
    {
        $this->currenttime = $currenttime;

        return $this;
    }

    /**
     * Get currenttime
     *
     * @return \DateTime
     */
    public function getCurrenttime()
    {
        return $this->currenttime;
    }

    /**
     * Set moduser
     *
     * @param string $moduser
     * @return InfoOptions
     */
    public function setModuser($moduser)
    {
        $this->moduser = $moduser;

        return $this;
    }

    /**
     * Get moduser
     *
     * @return string
     */
    public function getModuser()
    {
        return $this->moduser;
    }

    /**
     * Add optionsByRoleByRole
     *
     * @param InfoOptionsbyrole $optionsByRole
     *
     * @return InfoOptions
     */
    public function addOptionByRole(InfoOptionsbyrole $optionsByRole)
    {
        if (!$this->optionsByRole->contains($optionsByRole)) {
            $this->optionsByRole[] = $optionsByRole;
            $optionsByRole->setOption($this);
        }

        return $this;
    }

    /**
     * Remove optionsByRoleByRole
     *
     * @param InfoOptionsbyrole $optionsByRole
     */
    public function removeOptionByRole(InfoOptionsbyrole $optionsByRole)
    {
        if ($this->optionsByRole->contains($optionsByRole)) {
            $this->optionsByRole->removeElement($optionsByRole);
            $optionsByRole->setOption(null);
        }
    }

    /**
     * Get optionsByRole
     *
     * @return ArrayCollection
     */
    public function getOptionByRoles()
    {
        return $this->optionsByRole;
    }

    /**
     * Set optionsByRole
     *
     * @return InfoOptions
     */
    public function setOptionByRoles(ArrayCollection $optionsByRole)
    {
        $this->optionsByRole = $optionsByRole;

        return $this;
    }

    public function getRolesIds()
    {
        $rolesIds = array();
        /**
         * @var InfoRole $role
         */
        foreach ($this->roles as $role) {
            $rolesIds[] = $role->getId();
        }
        return $rolesIds;
    }
}
