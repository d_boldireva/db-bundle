<?php

namespace TeamSoft\CrmRepositoryBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\PersistentCollection;
use TeamSoft\CrmRepositoryBundle\Entity\RTC\InfoRtcRoomReserve;

/**
 * InfoContact
 *
 * @see vendor/teamsoft/db-bundle/Resources/config/serializer/Entity.InfoContact.yml
 *
 * @ORM\Table(name="info_contact")
 * @ORM\Entity(repositoryClass="TeamSoft\CrmRepositoryBundle\Repository\Contact\InfoContactRepository")
 * @ORM\EntityListeners({"TeamSoft\CrmRepositoryBundle\EventListener\InfoContactListener"})
 */
class InfoContact implements ServiceFieldInterface, LanguageInterface
{
    /**
     *  Ожидает верификацию
     */
    public const STATUS_WAIT_VERIFICATION = 1;

    /**
     * Отправлено на верификацию
     */
    public const STATUS_NOT_VERIFICATION = 2;

    /**
     * Верифицировано
     */
    public const STATUS_VERIFIED = 3;

    /**
     * Отклонено
     */
    public const STATUS_REJECTED = 4;

    /**
     * Требует повторной верификации
     */
    public const STATUS_NEED_VERIFICATION_AGAIN = 5;

    /**
     * Не подлежит верификации
     */
    public const STATUS_NOT_SUBJECT_VERIFICATION = 6;

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="InfoContactsign", mappedBy="contact", cascade={"persist"}, orphanRemoval=false)
     */
    private $contactSigns;

    /**
     * @var string
     *
     * @ORM\Column(name="LastName", type="string", length=255, nullable=true)
     */
    private $lastname;

    /**
     * @var string
     *
     * @ORM\Column(name="FirstName", type="string", length=255, nullable=true)
     */
    private $firstname;


    /**
     * @var integer
     *
     * @ORM\Column(name="practiceno", type="integer", length=255, nullable=true)
     */
    private $practiceno;

    /**
     * @var string
     *
     * @ORM\Column(name="MiddleName", type="string", length=255, nullable=true)
     */
    private $middlename;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="BirthDate", type="datetime", nullable=true)
     */
    private $birthdate;

    /**
     * @var string
     *
     * @ORM\Column(name="Phone1", type="string", length=255, nullable=true)
     */
    private $phone1;

    /**
     * @var string
     *
     * @ORM\Column(name="Phone2", type="string", length=255, nullable=true)
     */
    private $phone2;

    /**
     * @var string
     *
     * @ORM\Column(name="eaddr", type="string", length=255, nullable=true)
     */
    private $eaddr;

    /**
     * @var string
     *
     * @ORM\Column(name="PostCode", type="string", length=20, nullable=true)
     */
    private $postcode;

    /**
     * @var string
     *
     * @ORM\Column(name="Street", type="string", length=255, nullable=true)
     */
    private $street;

    /**
     * @var string
     *
     * @ORM\Column(name="Building", type="string", length=255, nullable=true)
     */
    private $building;

    /**
     * @var string
     *
     * @ORM\Column(name="Apartment", type="string", length=20, nullable=true)
     */
    private $apartment;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="Modified", type="datetime", nullable=true)
     */
    private $modified;

    /**
     * @var string
     *
     * @ORM\Column(name="StreetUkr", type="string", length=255, nullable=true)
     */
    private $streetukr;

    /**
     * @var string
     *
     * @ORM\Column(name="Description", type="varchar_or_n_varchar_image_or_string", nullable=true)
     */
    private $description;

    /**
     * @var string
     *
     * @ORM\Column(name="Sex", type="string", length=20, nullable=true)
     */
    private $sex;

    /**
     * @var integer
     *
     * @ORM\Column(name="IsArchive", type="integer", nullable=true)
     */
    private $isarchive;

    /**
     * @var string
     *
     * @ORM\Column(name="DepartmentText", type="string", length=255, nullable=true)
     */
    private $departmenttext;

    /**
     * @var string
     *
     * @ORM\Column(name="guid", type="guid", nullable=true)
     */
    private $guid;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="currenttime", type="datetime", nullable=true)
     */
    private $currenttime;

    /**
     * @var string
     *
     * @ORM\Column(name="moduser", type="string", length=16, nullable=true)
     */
    private $moduser;

    /**
     * @var string
     *
     * @ORM\Column(name="categoryprep", type="string", length=50, nullable=true)
     */
    private $categoryprep;

    /**
     * @var string
     *
     * @ORM\Column(name="Suburb", type="string", length=255, nullable=true)
     */
    private $suburb;

    /**
     * @var string
     *
     * @ORM\Column(name="blddop", type="string", length=10, nullable=true)
     */
    private $blddop;

    /**
     * @var integer
     *
     * @ORM\Column(name="isact", type="integer", nullable=true)
     */
    private $isact;

    /**
     * @var integer
     *
     * @ORM\Column(name="statusverification", type="integer", nullable=true)
     */
    private $statusverification;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="sendverification", type="datetime", nullable=true)
     */
    private $sendverification;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="CreateDate", type="datetime", nullable=true)
     */
    private $createdate;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="enable_date_sms", type="datetime", nullable=true)
     */
    private $enableDateSms;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="enable_date_email", type="datetime", nullable=true)
     */
    private $enableDateEmail;

    /**
     * @var integer
     *
     * @ORM\Column(name="isactive", type="integer", nullable=true)
     */
    private $isactive;

    /**
     * @var integer
     *
     * @ORM\Column(name="morionid", type="integer", nullable=true)
     */
    private $morionid;

    /**
     * @var string
     *
     * @ORM\Column(name="case_number", type="string", length=255, nullable=true)
     */
    private $caseNumber;

    /**
     * @var InfoCustomdictionaryvalue|null
     * @ORM\OneToOne(targetEntity="InfoCustomdictionaryvalue")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="legal_status", referencedColumnName="id")
     * })
     */
    private $legalStatus;

    /**
     * @var InfoCustomdictionaryvalue|null
     * @ORM\OneToOne(targetEntity="InfoCustomdictionaryvalue")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="interaction", referencedColumnName="id")
     * })
     */
    private $interaction;

    /**
     * @var string|null
     *
     * @ORM\Column(name="legal_name", type="string", length=255, nullable=true)
     */
    private $legalName;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="passport_date", type="datetime", length=255, nullable=true)
     */
    private $passportDate;

    /**
     * @var string|null
     *
     * @ORM\Column(name="passport_seriesnumber", type="string", length=255, nullable=true)
     */
    private $passportSeriesNumber;

    /**
     * @var string|null
     *
     * @ORM\Column(name="passport_issued", type="string", length=255, nullable=true)
     */
    private $passportIssued;

    /**
     * @var string|null
     *
     * @ORM\Column(name="regaddress", type="string", length=255, nullable=true)
     */
    private $regAddress;

    /**
     * @var string|null
     *
     * @ORM\Column(name="insurance_certificate", type="string", length=255, nullable=true)
     */
    private $insuranceCertificate;

    /**
     * @var string|null
     *
     * @ORM\Column(name="identification_number", type="string", length=255, nullable=true)
     */
    private $identificationNumber;

    /**
     * @var string|null
     *
     * @ORM\Column(name="checking_account", type="string", length=255, nullable=true)
     */
    private $checkingAccount;

    /**
     * @var string|null
     *
     * @ORM\Column(name="bank", type="string", length=255, nullable=true)
     */
    private $bank;

    /**
     * @var string|null
     *
     * @ORM\Column(name="bic", type="string", length=255, nullable=true)
     */
    private $bic;

    /**
     * @var string|null
     *
     * @ORM\Column(name="correspondent_account", type="string", length=255, nullable=true)
     */
    private $correspondentAccount;

    /**
     * @var string|null
     *
     * @ORM\Column(name="snils", type="string", length=255, nullable=true)
     */
    private $snils;

    /**
     * @var string|null
     *
     * @ORM\Column(name="kpp", type="string", length=255, nullable=true)
     */
    private $kpp;

    /**
     * @var InfoPhonetype
     *
     * @ORM\ManyToOne(targetEntity="InfoPhonetype")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="PhoneType1_id", referencedColumnName="id")
     * })
     */
    private $phonetype1;

    /**
     * @var InfoRegion
     *
     * @ORM\ManyToOne(targetEntity="InfoRegion")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="Region_id", referencedColumnName="id")
     * })
     */
    private $region;

    /**
     * @var InfoPhonetype
     *
     * @ORM\ManyToOne(targetEntity="InfoPhonetype")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="PhoneType2_id", referencedColumnName="id")
     * })
     */
    private $phonetype2;

    /**
     * @var InfoContactcateg
     *
     * @ORM\ManyToOne(targetEntity="InfoContactcateg")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="Category_id", referencedColumnName="id")
     * })
     */
    private $category;

    /**
     * @var InfoCompany
     *
     * @ORM\ManyToOne(targetEntity="InfoCompany", inversedBy="contactCollection")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="Company_id", referencedColumnName="id")
     * })
     */
    private $company;

    /**
     * @var InfoCity
     *
     * @ORM\ManyToOne(targetEntity="InfoCity")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="City_id", referencedColumnName="id")
     * })
     */
    private $city;

    /**
     * @var InfoCountry
     *
     * @ORM\ManyToOne(targetEntity="InfoCountry")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="country_id", referencedColumnName="id")
     * })
     */
    private $country;

    /**
     * @var InfoUser
     *
     * @ORM\ManyToOne(targetEntity="InfoUser")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="Owner_id", referencedColumnName="id")
     * })
     */
    private $owner;

    /**
     * @var InfoUser
     *
     * @ORM\ManyToOne(targetEntity="InfoUser")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="Modifier_id", referencedColumnName="id")
     * })
     */
    private $modifier;

    /**
     * @var InfoArchivereason
     *
     * @ORM\ManyToOne(targetEntity="InfoArchivereason")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="ArchiveReason_id", referencedColumnName="id")
     * })
     */
    private $archivereason;

    /**
     * @var InfoContacttype
     *
     * @ORM\ManyToOne(targetEntity="InfoContacttype")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="ContactType_id", referencedColumnName="id")
     * })
     */
    private $contacttype;

    /**
     * @var InfoDictionary
     *
     * @ORM\ManyToOne(targetEntity="InfoDictionary")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="streettype_id", referencedColumnName="id")
     * })
     */
    private $streettype;

    /**
     * @var InfoDictionary
     *
     * @ORM\ManyToOne(targetEntity="InfoDictionary")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="Department_id", referencedColumnName="id")
     * })
     */
    private $department;

    /**
     * @var InfoDictionary
     *
     * @ORM\ManyToOne(targetEntity="InfoDictionary")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="Position_id", referencedColumnName="id")
     * })
     */
    private $position;

    /**
     * @var InfoDictionary
     *
     * @ORM\ManyToOne(targetEntity="InfoDictionary")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="Specialization_id", referencedColumnName="id")
     * })
     */
    private $specialization;

    /**
     * @var InfoSuburb
     *
     * @ORM\ManyToOne(targetEntity="InfoSuburb")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="Suburb_id", referencedColumnName="id")
     * })
     */
    private $suburb2;

    /**
     * @var InfoDistrict
     *
     * @ORM\ManyToOne(targetEntity="InfoDistrict")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="district_id", referencedColumnName="id")
     * })
     */
    private $district;

    /**
     * @var \TeamSoft\CrmRepositoryBundle\Entity\McmContactemails
     *
     * @ORM\OneToOne(targetEntity="McmContactemails", mappedBy="contact")
     */
    private $contactEmail;

    /**
     * @var ArrayCollection
     *
     * @ORM\OneToMany(
     *     targetEntity="InfoMcmcontactvariables", mappedBy="contact", cascade={"persist"}, orphanRemoval=true
     * )
     */
    private $mcmContactVariablesCollection;

    /**
     * @var ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="InfoContactinfo", mappedBy="subj", cascade={"persist"}, orphanRemoval=true)
     */
    private $contactInfoCollection;

    /**
     * @var ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="InfoContactspec", mappedBy="contact", cascade={"persist"}, orphanRemoval=true)
     */
    private $contactSpecializationCollection;

    /**
     * @var ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="InfoContactworkplace", mappedBy="contact", cascade={"persist"}, orphanRemoval=true)
     */
    private $contactWorkplaceCollection;

    /**
     * @var ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="InfoContactphone", mappedBy="contact", cascade={"persist"}, orphanRemoval=true)
     */
    private $contactPhoneCollection;

    /**
     * @var ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="InfoContactemail", mappedBy="contact", cascade={"persist"}, orphanRemoval=true)
     */
    private $contactEmailCollection;

    /**
     * @var ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="InfoContactcategory", mappedBy="contact", cascade={"persist"}, orphanRemoval=true)
     */
    private $contactCategoryCollection;

    /**
     * @var ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="InfoContactowner", mappedBy="contact")
     */
    private $contactOwnerCollection;

    /**
     * @ORM\ManyToMany(targetEntity="InfoTarget")
     * @ORM\JoinTable(name="info_targetspec",
     *     joinColumns={@ORM\JoinColumn(name="spec_id", referencedColumnName="id")},
     *     inverseJoinColumns={@ORM\JoinColumn(name="target_id", referencedColumnName="id")}
     * )
     */
    private $targetCollection;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="getverification", type="datetime", nullable=true)
     */
    private $getVerification;

    /**
     * @var ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="InfoContactBlog", mappedBy="contact", cascade={"persist"}, orphanRemoval=true)
     */
    private $contactBlogCollection;

    /**
     * @var InfoLanguage
     *
     * @ORM\OneToOne(targetEntity="InfoLanguage")
     * @ORM\JoinColumn(name="language_id", referencedColumnName="id")
     */
    private $language;

    /**
     * @var string
     *
     * @ORM\Column(name="lastname_ukr", type="string", length=255, nullable=true)
     */
    private $lastnameUkr;

    /**
     * @var string
     *
     * @ORM\Column(name="firstname_ukr", type="string", length=255, nullable=true)
     */
    private $firstnameUkr;

    /**
     * @var string
     *
     * @ORM\Column(name="middlename_ukr", type="string", length=255, nullable=true)
     */
    private $middlenameUkr;

    /**
     * @var string
     *
     * @ORM\Column(name="lastname_eng", type="string", length=255, nullable=true)
     */
    private $lastnameEng;

    /**
     * @var string
     *
     * @ORM\Column(name="firstname_eng", type="string", length=255, nullable=true)
     */
    private $firstnameEng;

    /**
     * @var string
     *
     * @ORM\Column(name="middlename_eng", type="string", length=255, nullable=true)
     */
    private $middlenameEng;

    /**
     * @var InfoContactComment|null
     *
     * @ORM\OneToOne(targetEntity="InfoContactComment", mappedBy="contact", cascade={"persist"})
     */
    private $contactComment;

    /**
     * @var ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="\TeamSoft\CrmRepositoryBundle\Entity\RTC\InfoRtcRoomReserve", mappedBy="contact")
     */
    private $rtcRoomReserves;

    /**
     * @var ArrayCollection
     *
     * @ORM\OneToMany(
     *     targetEntity="InfoMcmjson", mappedBy="contact", cascade={"persist"}, orphanRemoval=true
     * )
     */
    private $mcmJsons;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->contactOwnerCollection = new ArrayCollection();
        $this->contactInfoCollection = new ArrayCollection();
        $this->contactWorkplaceCollection = new ArrayCollection();
        $this->mcmContactVariablesCollection = new ArrayCollection();
        $this->contactSpecializationCollection = new ArrayCollection();
        $this->contactPhoneCollection = new ArrayCollection();
        $this->contactEmailCollection = new ArrayCollection();
        $this->targetCollection = new ArrayCollection();
        $this->contactCategoryCollection = new ArrayCollection();
        $this->contactBlogCollection = new ArrayCollection();
        $this->rtcRoomReserves = new ArrayCollection();
        $this->contactSigns = new ArrayCollection();
        $this->mcmJsons = new ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set lastname
     *
     * @param string $lastname
     * @return InfoContact
     */
    public function setLastname($lastname)
    {
        $this->lastname = $lastname;

        return $this;
    }

    /**
     * Get lastname
     *
     * @return string
     */
    public function getLastname()
    {
        return $this->lastname;
    }

    /**
     * Set firstname
     *
     * @param string $firstname
     * @return InfoContact
     */
    public function setFirstname($firstname)
    {
        $this->firstname = $firstname;

        return $this;
    }

    /**
     * Get firstname
     *
     * @return string
     */
    public function getFirstname()
    {
        return $this->firstname;
    }

    /**
     * Set middlename
     *
     * @param string $middlename
     * @return InfoContact
     */
    public function setMiddlename($middlename)
    {
        $this->middlename = $middlename;

        return $this;
    }

    /**
     * Get middlename
     *
     * @return string
     */
    public function getMiddlename()
    {
        return $this->middlename;
    }

    /**
     * Set birthdate
     *
     * @param \DateTime $birthdate
     * @return InfoContact
     */
    public function setBirthdate($birthdate)
    {
        $this->birthdate = $birthdate;

        return $this;
    }

    /**
     * Get birthdate
     *
     * @return \DateTime
     */
    public function getBirthdate()
    {
        return $this->birthdate;
    }

    /**
     * Set phone1
     *
     * @param string $phone1
     * @return InfoContact
     */
    public function setPhone1($phone1)
    {
        $this->phone1 = $phone1;

        return $this;
    }

    /**
     * Get phone1
     *
     * @return string
     */
    public function getPhone1()
    {
        return $this->phone1;
    }

    /**
     * Set phone2
     *
     * @param string $phone2
     * @return InfoContact
     */
    public function setPhone2($phone2)
    {
        $this->phone2 = $phone2;

        return $this;
    }

    /**
     * Get phone2
     *
     * @return string
     */
    public function getPhone2()
    {
        return $this->phone2;
    }

    /**
     * Set eaddr
     *
     * @param string $eaddr
     * @return InfoContact
     */
    public function setEaddr($eaddr)
    {
        $this->eaddr = $eaddr;

        return $this;
    }

    /**
     * Get eaddr
     *
     * @return string
     */
    public function getEaddr()
    {
        return $this->eaddr;
    }

    /**
     * Set postcode
     *
     * @param string $postcode
     * @return InfoContact
     */
    public function setPostcode($postcode)
    {
        $this->postcode = $postcode;

        return $this;
    }

    /**
     * Get postcode
     *
     * @return string
     */
    public function getPostcode()
    {
        return $this->postcode;
    }

    /**
     * Set street
     *
     * @param string $street
     * @return InfoContact
     */
    public function setStreet($street)
    {
        $this->street = $street;

        return $this;
    }

    /**
     * Get street
     *
     * @return string
     */
    public function getStreet()
    {
        return $this->street;
    }

    /**
     * Set building
     *
     * @param string $building
     * @return InfoContact
     */
    public function setBuilding($building)
    {
        $this->building = $building;

        return $this;
    }

    /**
     * Get building
     *
     * @return string
     */
    public function getBuilding()
    {
        return $this->building;
    }

    /**
     * Set apartment
     *
     * @param string $apartment
     * @return InfoContact
     */
    public function setApartment($apartment)
    {
        $this->apartment = $apartment;

        return $this;
    }

    /**
     * Get apartment
     *
     * @return string
     */
    public function getApartment()
    {
        return $this->apartment;
    }

    /**
     * Set modified
     *
     * @param \DateTime $modified
     * @return InfoContact
     */
    public function setModified(\DateTime $modified)
    {
        $this->modified = $modified;

        return $this;
    }

    /**
     * Get modified
     *
     * @return \DateTime
     */
    public function getModified()
    {
        return $this->modified;
    }

    /**
     * Set streetukr
     *
     * @param string $streetukr
     * @return InfoContact
     */
    public function setStreetukr($streetukr)
    {
        $this->streetukr = $streetukr;

        return $this;
    }

    /**
     * Get streetukr
     *
     * @return string
     */
    public function getStreetukr()
    {
        return $this->streetukr;
    }

    /**
     * Set description
     *
     * @param string $description
     * @return InfoContact
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set sex
     *
     * @param string $sex
     * @return InfoContact
     */
    public function setSex($sex)
    {
        $this->sex = $sex;

        return $this;
    }

    /**
     * Get sex
     *
     * @return string
     */
    public function getSex()
    {
        return $this->sex;
    }

    /**
     * Set isarchive
     *
     * @param integer $isarchive
     * @return InfoContact
     */
    public function setIsarchive($isarchive)
    {
        $this->isarchive = $isarchive;

        return $this;
    }

    /**
     * Get isarchive
     *
     * @return integer
     */
    public function getIsarchive()
    {
        return $this->isarchive;
    }

    /**
     * Set departmenttext
     *
     * @param string $departmenttext
     * @return InfoContact
     */
    public function setDepartmenttext($departmenttext)
    {
        $this->departmenttext = $departmenttext;

        return $this;
    }

    /**
     * Get departmenttext
     *
     * @return string
     */
    public function getDepartmenttext()
    {
        return $this->departmenttext;
    }

    /**
     * Set guid
     *
     * @param string $guid
     * @return InfoContact
     */
    public function setGuid($guid)
    {
        $this->guid = $guid;

        return $this;
    }

    /**
     * Get guid
     *
     * @return string
     */
    public function getGuid()
    {
        return $this->guid;
    }

    /**
     * Set currenttime
     *
     * @param \DateTime $currenttime
     * @return InfoContact
     */
    public function setCurrenttime(\DateTime $currenttime)
    {
        $this->currenttime = $currenttime;

        return $this;
    }

    /**
     * Get currenttime
     *
     * @return \DateTime
     */
    public function getCurrenttime()
    {
        return $this->currenttime;
    }

    /**
     * Set moduser
     *
     * @param string $moduser
     * @return InfoContact
     */
    public function setModuser($moduser)
    {
        $this->moduser = $moduser;

        return $this;
    }

    /**
     * Get moduser
     *
     * @return string
     */
    public function getModuser()
    {
        return $this->moduser;
    }

    /**
     * Get contactSigns
     *
     * @return resource | string
     */
    public function getContactSigns()
    {
        return $this->contactSigns;
    }

    /**
     * Add contactSign
     *
     * @param $contactSign
     * @return InfoContact
     */
    public function addContactSign(InfoContactsign $contactSign)
    {
        if(!$this->contactSigns->contains($contactSign)) {
            $contactSign->setContact($this);
            $this->contactSigns->add($contactSign);
        }

        return $this;
    }

    /**
     * Remove contactSign
     *
     * @param $contactSign
     * @return InfoContact
     */
    public function removeContactSign(InfoContactsign $contactSign)
    {
        if($this->contactSigns->contains($contactSign)) {
            $contactSign->setContact(null);
            $this->contactSigns->removeElement($contactSign);
        }

        return $this;
    }


    /**
     * Set categoryprep
     *
     * @param string $categoryprep
     * @return InfoContact
     */
    public function setCategoryprep($categoryprep)
    {
        $this->categoryprep = $categoryprep;

        return $this;
    }

    /**
     * Get categoryprep
     *
     * @return string
     */
    public function getCategoryprep()
    {
        return $this->categoryprep;
    }

    /**
     * Set suburb
     *
     * @param string $suburb
     * @return InfoContact
     */
    public function setSuburb($suburb)
    {
        $this->suburb = $suburb;

        return $this;
    }

    /**
     * Get suburb
     *
     * @return string
     */
    public function getSuburb()
    {
        return $this->suburb;
    }

    /**
     * Set blddop
     *
     * @param string $blddop
     * @return InfoContact
     */
    public function setBlddop($blddop)
    {
        $this->blddop = $blddop;

        return $this;
    }

    /**
     * Get blddop
     *
     * @return string
     */
    public function getBlddop()
    {
        return $this->blddop;
    }

    /**
     * Set isact
     *
     * @param integer $isact
     * @return InfoContact
     */
    public function setIsact($isact)
    {
        $this->isact = $isact;

        return $this;
    }

    /**
     * Get isact
     *
     * @return integer
     */
    public function getIsact()
    {
        return $this->isact;
    }

    /**
     * Set statusverification
     *
     * @param integer $statusverification
     * @return InfoContact
     */
    public function setStatusverification($statusverification)
    {
        $this->statusverification = $statusverification;

        return $this;
    }

    /**
     * Get statusverification
     *
     * @return integer
     */
    public function getStatusverification()
    {
        return $this->statusverification;
    }

    /**
     * Set sendverification
     *
     * @param \DateTime $sendverification
     * @return InfoContact
     */
    public function setSendverification($sendverification)
    {
        $this->sendverification = $sendverification;

        return $this;
    }

    /**
     * Get sendverification
     *
     * @return \DateTime
     */
    public function getSendverification()
    {
        return $this->sendverification;
    }

    /**
     * Set createdate
     *
     * @param \DateTime $createdate
     * @return InfoContact
     */
    public function setCreatedate(\DateTime $createdate)
    {
        $this->createdate = $createdate;

        return $this;
    }

    /**
     * Get createdate
     *
     * @return \DateTime
     */
    public function getCreatedate()
    {
        return $this->createdate;
    }

    /**
     * Set enableDateSms
     *
     * @param \DateTime $enableDateSms
     * @return InfoContact
     */
    public function setEmableDateSms(\DateTime $enableDateSms)
    {
        $this->enableDateSms = $enableDateSms;

        return $this;
    }

    /**
     * Get enableDateSms
     *
     * @return \DateTime
     */
    public function getEnableDateSms()
    {
        return $this->enableDateSms;
    }

    /**
     * Set enableDateEmail
     *
     * @param \DateTime $enableDateEmail
     * @return InfoContact
     */
    public function setEmableDateEmail(\DateTime $enableDateEmail)
    {
        $this->enableDateEmail = $enableDateEmail;

        return $this;
    }

    /**
     * Get enableDateEmail
     *
     * @return \DateTime
     */
    public function getEnableDateEmail()
    {
        return $this->enableDateEmail;
    }

    /**
     * Set isactive
     *
     * @param integer $isactive
     * @return InfoContact
     */
    public function setIsactive($isactive)
    {
        $this->isactive = $isactive;

        return $this;
    }

    /**
     * Get isactive
     *
     * @return integer
     */
    public function getIsactive()
    {
        return $this->isactive;
    }

    /**
     * Set morionid
     *
     * @param integer $morionid
     * @return InfoContact
     */
    public function setMorionid($morionid)
    {
        $this->morionid = $morionid;

        return $this;
    }

    /**
     * Get morionid
     *
     * @return integer
     */
    public function getMorionid()
    {
        return $this->morionid;
    }

    /**
     * Set phonetype1
     *
     * @param \TeamSoft\CrmRepositoryBundle\Entity\InfoPhonetype $phonetype1
     * @return InfoContact
     */
    public function setPhonetype1(InfoPhonetype $phonetype1 = null)
    {
        $this->phonetype1 = $phonetype1;

        return $this;
    }

    /**
     * Get phonetype1
     *
     * @return \TeamSoft\CrmRepositoryBundle\Entity\InfoPhonetype
     */
    public function getPhonetype1()
    {
        return $this->phonetype1;
    }

    /**
     * Set region
     *
     * @param \TeamSoft\CrmRepositoryBundle\Entity\InfoRegion $region
     * @return InfoContact
     */
    public function setRegion(InfoRegion $region = null)
    {
        $this->region = $region;

        return $this;
    }

    /**
     * Get region
     *
     * @return \TeamSoft\CrmRepositoryBundle\Entity\InfoRegion
     */
    public function getRegion()
    {
        return $this->region;
    }

    /**
     * Set phonetype2
     *
     * @param \TeamSoft\CrmRepositoryBundle\Entity\InfoPhonetype $phonetype2
     * @return InfoContact
     */
    public function setPhonetype2(InfoPhonetype $phonetype2 = null)
    {
        $this->phonetype2 = $phonetype2;

        return $this;
    }

    /**
     * Get phonetype2
     *
     * @return \TeamSoft\CrmRepositoryBundle\Entity\InfoPhonetype
     */
    public function getPhonetype2()
    {
        return $this->phonetype2;
    }

    /**
     * Set category
     *
     * @param \TeamSoft\CrmRepositoryBundle\Entity\InfoContactcateg $category
     * @return InfoContact
     */
    public function setCategory(InfoContactcateg $category = null)
    {
        $this->category = $category;

        return $this;
    }

    /**
     * Get category
     *
     * @return \TeamSoft\CrmRepositoryBundle\Entity\InfoContactcateg
     */
    public function getCategory()
    {
        return $this->category;
    }

    /**
     * Set company
     *
     * @param \TeamSoft\CrmRepositoryBundle\Entity\InfoCompany $company
     * @return InfoContact
     */
    public function setCompany(InfoCompany $company = null)
    {
        $this->company = $company;

        return $this;
    }

    /**
     * Get company
     *
     * @return \TeamSoft\CrmRepositoryBundle\Entity\InfoCompany
     */
    public function getCompany()
    {
        return $this->company;
    }

    /**
     * Set city
     *
     * @param \TeamSoft\CrmRepositoryBundle\Entity\InfoCity $city
     * @return InfoContact
     */
    public function setCity(InfoCity $city = null)
    {
        $this->city = $city;

        return $this;
    }

    /**
     * Get city
     *
     * @return \TeamSoft\CrmRepositoryBundle\Entity\InfoCity
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
     * Set country
     *
     * @param \TeamSoft\CrmRepositoryBundle\Entity\InfoCountry $country
     * @return InfoContact
     */
    public function setCountry(InfoCountry $country = null)
    {
        $this->country = $country;

        return $this;
    }

    /**
     * Get country
     *
     * @return \TeamSoft\CrmRepositoryBundle\Entity\InfoCountry
     */
    public function getCountry()
    {
        return $this->country;
    }

    /**
     * Set owner
     *
     * @param \TeamSoft\CrmRepositoryBundle\Entity\InfoUser $owner
     * @return InfoContact
     */
    public function setOwner(InfoUser $owner = null)
    {
        $this->owner = $owner;

        return $this;
    }

    /**
     * Get owner
     *
     * @return \TeamSoft\CrmRepositoryBundle\Entity\InfoUser
     */
    public function getOwner()
    {
        return $this->owner;
    }

    /**
     * Set modifier
     *
     * @param \TeamSoft\CrmRepositoryBundle\Entity\InfoUser $modifier
     * @return InfoContact
     */
    public function setModifier(InfoUser $modifier = null)
    {
        $this->modifier = $modifier;

        return $this;
    }

    /**
     * Get modifier
     *
     * @return \TeamSoft\CrmRepositoryBundle\Entity\InfoUser
     */
    public function getModifier()
    {
        return $this->modifier;
    }

    /**
     * Set archivereason
     *
     * @param \TeamSoft\CrmRepositoryBundle\Entity\InfoArchivereason $archivereason
     * @return InfoContact
     */
    public function setArchivereason(InfoArchivereason $archivereason = null)
    {
        $this->archivereason = $archivereason;

        return $this;
    }

    /**
     * Get archivereason
     *
     * @return \TeamSoft\CrmRepositoryBundle\Entity\InfoArchivereason
     */
    public function getArchivereason()
    {
        return $this->archivereason;
    }

    /**
     * Set contacttype
     *
     * @param \TeamSoft\CrmRepositoryBundle\Entity\InfoContacttype $contacttype
     * @return InfoContact
     */
    public function setContacttype(InfoContacttype $contacttype = null)
    {
        $this->contacttype = $contacttype;

        return $this;
    }

    /**
     * Get contacttype
     *
     * @return \TeamSoft\CrmRepositoryBundle\Entity\InfoContacttype
     */
    public function getContacttype()
    {
        return $this->contacttype;
    }

    /**
     * Set streettype
     *
     * @param \TeamSoft\CrmRepositoryBundle\Entity\InfoDictionary $streettype
     * @return InfoContact
     */
    public function setStreettype(InfoDictionary $streettype = null)
    {
        $this->streettype = $streettype;

        return $this;
    }

    /**
     * Get streettype
     *
     * @return \TeamSoft\CrmRepositoryBundle\Entity\InfoDictionary
     */
    public function getStreettype()
    {
        return $this->streettype;
    }

    /**
     * Set department
     *
     * @param \TeamSoft\CrmRepositoryBundle\Entity\InfoDictionary $department
     * @return InfoContact
     */
    public function setDepartment(InfoDictionary $department = null)
    {
        $this->department = $department;

        return $this;
    }

    /**
     * Get department
     *
     * @return \TeamSoft\CrmRepositoryBundle\Entity\InfoDictionary
     */
    public function getDepartment()
    {
        return $this->department;
    }

    /**
     * Set position
     *
     * @param \TeamSoft\CrmRepositoryBundle\Entity\InfoDictionary $position
     * @return InfoContact
     */
    public function setPosition(InfoDictionary $position = null)
    {
        $this->position = $position;

        return $this;
    }

    /**
     * Get position
     *
     * @return \TeamSoft\CrmRepositoryBundle\Entity\InfoDictionary
     */
    public function getPosition()
    {
        return $this->position;
    }

    /**
     * Set specialization
     *
     * @param \TeamSoft\CrmRepositoryBundle\Entity\InfoDictionary $specialization
     * @return InfoContact
     */
    public function setSpecialization(InfoDictionary $specialization = null)
    {
        $this->specialization = $specialization;

        return $this;
    }

    /**
     * Get specialization
     *
     * @return \TeamSoft\CrmRepositoryBundle\Entity\InfoDictionary
     */
    public function getSpecialization()
    {
        return $this->specialization;
    }

    /**
     * Set suburb2
     *
     * @param \TeamSoft\CrmRepositoryBundle\Entity\InfoSuburb $suburb2
     * @return InfoContact
     */
    public function setSuburb2(InfoSuburb $suburb2 = null)
    {
        $this->suburb2 = $suburb2;

        return $this;
    }

    /**
     * Get suburb2
     *
     * @return \TeamSoft\CrmRepositoryBundle\Entity\InfoSuburb
     */
    public function getSuburb2()
    {
        return $this->suburb2;
    }

    /**
     * Set district
     *
     * @param \TeamSoft\CrmRepositoryBundle\Entity\InfoDistrict $district
     * @return InfoContact
     */
    public function setDistrict(InfoDistrict $district = null)
    {
        $this->district = $district;

        return $this;
    }

    /**
     * Get district
     *
     * @return \TeamSoft\CrmRepositoryBundle\Entity\InfoDistrict
     */
    public function getDistrict()
    {
        return $this->district;
    }

    /**
     * Get name
     * @return string
     */
    public function getName()
    {
        return $this->getLastname() . ' ' . $this->getFirstname() . ' ' . $this->getMiddlename();
    }

    public function getRegionId()
    {
        return $this->getRegion() ? $this->getRegion()->getId() : null;
    }

    public function getCityId()
    {
        return $this->getCity() ? $this->getCity()->getId() : null;
    }

    public function getStreettypeId()
    {
        return $this->getStreettype() ? $this->getStreettype()->getId() : null;
    }

    public function getOwnerId()
    {
        return $this->getOwner() ? $this->getOwner()->getId() : null;
    }

    public function getSpecializationId()
    {
        return $this->getSpecialization() ? $this->getSpecialization()->getId() : null;
    }

    public function getContacttypeId()
    {
        return $this->getContacttype() ? $this->getContacttype()->getId() : null;
    }

    public function getCompanyId()
    {
        return $this->getCompany() ? $this->getCompany()->getId() : null;
    }

    public function getCategoryId()
    {
        return $this->getCategory() ? $this->getCategory()->getId() : null;
    }

    public function getArchivereasonId()
    {
        return $this->getArchivereason() ? $this->getArchivereason()->getId() : null;
    }

    public function getPositionId()
    {
        return $this->getPosition() ? $this->getPosition()->getId() : null;
    }

    public function getPositionName()
    {
        return $this->getPosition() ? $this->getPosition()->getName() : null;
    }

    public function getAddress()
    {
        $addressParts = array();

        if ($this->city !== null) {
            $addressParts[] = $this->city->getName();
        }
        if (!empty($this->street)) {
            $street = $this->street;
            if ($this->streettype !== null) {
                $street = $this->streettype->getName() . ' ' . $street;
            }
            $addressParts[] = $street;
        }
        if (!empty($this->building)) {
            $addressParts[] = $this->building;
        }
        return implode(', ', $addressParts);
    }

    public function getFullAddress()
    {
        $addressParts = array();

        if ($this->country !== null) {
            $addressParts[] = $this->country->getName();
        }
        if ($this->company !== null) {
            $addressParts[] = $this->company->getPostcode();
        }
        if ($this->region !== null) {
            $addressParts[] = $this->region->getName();
        }
        if ($this->city !== null) {
            $addressParts[] = $this->city->getName();
        }
        if (!empty($this->street)) {
            $street = $this->street;
            if ($this->streettype !== null) {
                $street = $this->streettype->getName() . ' ' . $street;
            }
            $addressParts[] = $street;
        }
        if (!empty($this->building)) {
            $addressParts[] = $this->building;
        }
        return implode(', ', $addressParts);
    }

    public function getPhoneString()
    {
        $phoneParts = [];
        if ($this->phone1) {
            $phoneParts[] = $this->phone1;
        }

        if ($this->phone2) {
            $phoneParts[] = $this->phone2;
        }

        foreach ($this->contactPhoneCollection as $phone) {
            $phoneParts[] = $phone->getPhone();
        }

        return join(', ', $phoneParts);
    }

    public function getShortAddress()
    {
        $addressParts = array();

        if (!empty($this->street)) {
            $street = $this->street;
            if ($this->streettype !== null) {
                $street = $this->streettype->getName() . ' ' . $street;
            }
            $addressParts[] = $street;
        }
        if (!empty($this->building)) {
            $addressParts[] = $this->building;
        }
        return implode(', ', $addressParts);
    }

    public function getCompanyName()
    {
        return $this->getCompany() ? $this->getCompany()->getName() : null;
    }

    public function getCategoryName()
    {
        return $this->getCategory() ? $this->getCategory()->getName() : null;
    }

    public function getSpecializationName()
    {
        return $this->getSpecialization() ? $this->getSpecialization()->getName() : null;
    }

    /**
     * Add contactInfoCollection
     *
     * @param \TeamSoft\CrmRepositoryBundle\Entity\InfoContactinfo $contactInfo
     *
     * @return InfoContact
     */
    public function addContactInfoCollection(InfoContactinfo $contactInfo)
    {
        $contactInfo->setSubj($this);
        $this->contactInfoCollection->add($contactInfo);

        return $this;
    }

    /**
     * Remove contactInfoCollection
     *
     * @param \TeamSoft\CrmRepositoryBundle\Entity\InfoContactinfo $contactInfo
     */
    public function removeContactInfoCollection(InfoContactinfo $contactInfo)
    {
        $contactInfo->setSubj(null);
        $this->contactInfoCollection->removeElement($contactInfo);
    }

    /**
     * Get contactInfoCollection
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getContactInfoCollection()
    {
        return $this->contactInfoCollection;
    }

    /**
     * Get mcmContactVariablesCollection
     *
     * @return \Doctrine\Common\Collections\ArrayCollection
     */
    public function getMcmContactVariablesCollection()
    {
        return $this->mcmContactVariablesCollection;
    }

    /**
     * Add contactWorkplaceCollection
     *
     * @param \TeamSoft\CrmRepositoryBundle\Entity\InfoContactworkplace $contactWorkplace
     *
     * @return InfoContact
     */
    public function addContactWorkplaceCollection(InfoContactworkplace $contactWorkplace)
    {
        $contactWorkplace->setContact($this);
        $this->contactWorkplaceCollection->add($contactWorkplace);

        return $this;
    }

    /**
     * Remove contactWorkplaceCollection
     *
     * @param \TeamSoft\CrmRepositoryBundle\Entity\InfoContactworkplace $contactWorkplace
     */
    public function removeContactWorkplaceCollection(InfoContactworkplace $contactWorkplace)
    {
        $contactWorkplace->setContact(null);
        $this->contactWorkplaceCollection->removeElement($contactWorkplace);
    }

    /**
     * Get contactWorkplaceCollection
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getContactWorkplaceCollection()
    {
        return $this->contactWorkplaceCollection;
    }

    /**
     * Add contactSpecializationCollection
     *
     * @param \TeamSoft\CrmRepositoryBundle\Entity\InfoContactspec $contactSpecialization
     *
     * @return InfoContact
     */
    public function addContactSpecializationCollection(InfoContactspec $contactSpecialization)
    {
        $contactSpecialization->setContact($this);
        $this->contactSpecializationCollection->add($contactSpecialization);

        return $this;
    }

    /**
     * Remove contactSpecializationCollection
     *
     * @param \TeamSoft\CrmRepositoryBundle\Entity\InfoContactspec $contactSpecialization
     */
    public function removeContactSpecializationCollection(InfoContactspec $contactSpecialization)
    {
        $contactSpecialization->setContact(null);
        $this->contactSpecializationCollection->removeElement($contactSpecialization);
    }

    /**
     * Get contactSpecializationCollection
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getContactSpecializationCollection()
    {
        return $this->contactSpecializationCollection;
    }

    /**
     * Get contactSpecializationCollection
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getContact()
    {
        return $this->contactSpecializationCollection;
    }

    /**
     * Get contactPhoneCollection
     *
     * @return \Doctrine\Common\Collections\Collection|InfoContactphone[]
     */
    public function getContactPhoneCollection()
    {
        return $this->contactPhoneCollection;
    }

    /**
     * Get contactEmailCollection
     *
     * @return \Doctrine\Common\Collections\Collection|McmContactemails[]
     */
    public function getContactEmailCollection()
    {
        return $this->contactEmailCollection;
    }

    /**
     * Set practiceno.
     *
     * @param int|null $practiceno
     *
     * @return InfoContact
     */
    public function setPracticeno($practiceno = null)
    {
        $this->practiceno = $practiceno;

        return $this;
    }

    /**
     * Get practiceno.
     *
     * @return int|null
     */
    public function getPracticeno()
    {
        return $this->practiceno;
    }

    /**
     * Set enableDateSms.
     *
     * @param \DateTime|null $enableDateSms
     *
     * @return InfoContact
     */
    public function setEnableDateSms($enableDateSms = null)
    {
        $this->enableDateSms = $enableDateSms;

        return $this;
    }

    /**
     * Set enableDateEmail.
     *
     * @param \DateTime|null $enableDateEmail
     *
     * @return InfoContact
     */
    public function setEnableDateEmail($enableDateEmail = null)
    {
        $this->enableDateEmail = $enableDateEmail;

        return $this;
    }

    /**
     * Set contactEmail.
     *
     * @param \TeamSoft\CrmRepositoryBundle\Entity\McmContactemails|null $contactEmail
     *
     * @return InfoContact
     */
    public function setContactEmail(\TeamSoft\CrmRepositoryBundle\Entity\McmContactemails $contactEmail = null)
    {
        $this->contactEmail = $contactEmail;

        return $this;
    }

    /**
     * Get contactEmail.
     *
     * @return \TeamSoft\CrmRepositoryBundle\Entity\McmContactemails|null
     */
    public function getContactEmail()
    {
        return $this->contactEmail;
    }


    /**
     * @return string
     */
    public function getCaseNumber(): ?string
    {
        return $this->caseNumber;
    }

    /**
     * @param string $caseNumber
     *
     * @return InfoContact
     */
    public function setCaseNumber(?string $caseNumber): InfoContact
    {
        $this->caseNumber = $caseNumber;

        return $this;
    }


    /**
     * Add mcmContactVariablesCollection.
     *
     * @param \TeamSoft\CrmRepositoryBundle\Entity\InfoMcmcontactvariables $mcmContactVariablesCollection
     *
     * @return InfoContact
     */
    public function addMcmContactVariablesCollection(\TeamSoft\CrmRepositoryBundle\Entity\InfoMcmcontactvariables $mcmContactVariablesCollection)
    {
        $this->mcmContactVariablesCollection[] = $mcmContactVariablesCollection;

        return $this;
    }

    /**
     * Remove mcmContactVariablesCollection.
     *
     * @param \TeamSoft\CrmRepositoryBundle\Entity\InfoMcmcontactvariables $mcmContactVariablesCollection
     */
    public function removeMcmContactVariablesCollection(\TeamSoft\CrmRepositoryBundle\Entity\InfoMcmcontactvariables $mcmContactVariablesCollection)
    {
        $this->mcmContactVariablesCollection->removeElement($mcmContactVariablesCollection);
    }

    /**
     * Add contactPhoneCollection.
     *
     * @param \TeamSoft\CrmRepositoryBundle\Entity\InfoContactphone $contactPhoneCollection
     *
     * @return InfoContact
     */
    public function addContactPhoneCollection(\TeamSoft\CrmRepositoryBundle\Entity\InfoContactphone $contactPhoneCollection)
    {
        $this->contactPhoneCollection[] = $contactPhoneCollection;
        $contactPhoneCollection->setContact($this);

        return $this;
    }

    /**
     * Remove contactPhoneCollection.
     *
     * @param \TeamSoft\CrmRepositoryBundle\Entity\InfoContactphone $contactPhoneCollection
     *
     */
    public function removeContactPhoneCollection(\TeamSoft\CrmRepositoryBundle\Entity\InfoContactphone $contactPhoneCollection)
    {
        $this->contactPhoneCollection->removeElement($contactPhoneCollection);
    }

    /**
     * Add contactEmailCollection.
     *
     * @param \TeamSoft\CrmRepositoryBundle\Entity\InfoContactemail $contactEmailCollection
     *
     * @return InfoContact
     */
    public function addContactEmailCollection(\TeamSoft\CrmRepositoryBundle\Entity\InfoContactemail $contactEmailCollection)
    {
        $this->contactEmailCollection[] = $contactEmailCollection;
        $contactEmailCollection->setContact($this);

        return $this;
    }

    /**
     * Remove contactEmailCollection.
     *
     * @param \TeamSoft\CrmRepositoryBundle\Entity\InfoContactemail $contactEmailCollection
     *
     */
    public function removeContactEmailCollection(\TeamSoft\CrmRepositoryBundle\Entity\InfoContactemail $contactEmailCollection)
    {
        $this->contactEmailCollection->removeElement($contactEmailCollection);
    }

    /**
     * Add targetCollection.
     *
     * @param \TeamSoft\CrmRepositoryBundle\Entity\InfoTarget $targetCollection
     *
     * @return InfoContact
     */
    public function addTargetCollection(\TeamSoft\CrmRepositoryBundle\Entity\InfoTarget $targetCollection)
    {
        $this->targetCollection[] = $targetCollection;

        return $this;
    }

    /**
     * Remove targetCollection.
     *
     * @param \TeamSoft\CrmRepositoryBundle\Entity\InfoTarget $targetCollection
     *
     */
    public function removeTargetCollection(\TeamSoft\CrmRepositoryBundle\Entity\InfoTarget $targetCollection)
    {
        $this->targetCollection->removeElement($targetCollection);
    }

    /**
     * Get targetCollection.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getTargetCollection()
    {
        return $this->targetCollection;
    }

    /**
     * @return ArrayCollection
     */
    public function getContactCategoryCollection()
    {
        return $this->contactCategoryCollection;
    }

    /**
     * Add contactCategory.
     *
     * @param \TeamSoft\CrmRepositoryBundle\Entity\InfoContactcategory $contactCategory
     *
     * @return InfoContact
     */
    public function addContactCategoryCollection(\TeamSoft\CrmRepositoryBundle\Entity\InfoContactcategory $contactCategory)
    {
        $contactCategory->setContact($this);
        $this->contactCategoryCollection[] = $contactCategory;

        return $this;
    }

    /**
     * Remove contactCategory.
     *
     * @param \TeamSoft\CrmRepositoryBundle\Entity\InfoContactcategory $contactCategory
     *
     */
    public function removeContactCategoryCollection(\TeamSoft\CrmRepositoryBundle\Entity\InfoContactcategory $contactCategory)
    {
        $this->contactCategoryCollection->removeElement($contactCategory);
    }

    /**
     * Add contactOwnerCollection.
     *
     * @param InfoContactOwner $contactOwnerCollection
     *
     * @return self
     */
    public function addContactOwnerCollection(InfoContactOwner $contactOwnerCollection)
    {
        $this->contactOwnerCollection[] = $contactOwnerCollection;

        return $this;
    }

    /**
     * Remove contactOwnerCollection.
     *
     * @param InfoContactOwner $contactOwnerCollection
     *
     */
    public function removeContactOwnerCollection(InfoContactOwner $contactOwnerCollection)
    {
        $this->contactOwnerCollection->removeElement($contactOwnerCollection);
    }

    /**
     * Get contactOwnerCollection.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getContactOwnerCollection()
    {
        return $this->contactOwnerCollection;
    }

    /**
     * Set getVerification
     *
     * @param \DateTime $getVerification
     * @return InfoContact
     */
    public function setGetVerification(\DateTime $getVerification)
    {
        $this->getVerification = $getVerification;

        return $this;
    }

    /**
     * Get getVerification
     *
     * @return \DateTime
     */
    public function getGetVerification()
    {
        return $this->getVerification;
    }

    /**
     * @return bool
     */
    public function isSpecializationVerification(): bool
    {
        return $this->getSpecialization() && $this->getSpecialization()->getIsverification();
    }

    /**
     * @return bool
     */
    public function isRegionVerification(): bool
    {
        return $this->getRegion() && $this->getRegion()->isVerification();
    }

    /**
     * @return InfoCustomdictionaryvalue|null
     */
    public function getLegalStatus(): ?InfoCustomdictionaryvalue
    {
        return $this->legalStatus;
    }

    /**
     * @param InfoCustomdictionaryvalue|null $legalStatus
     * @return self
     */
    public function setLegalStatus(?InfoCustomdictionaryvalue $legalStatus): self
    {
        $this->legalStatus = $legalStatus;
        return $this;
    }

    /**
     * @return InfoCustomdictionaryvalue|null
     */
    public function getInteraction(): ?InfoCustomdictionaryvalue
    {
        return $this->interaction;
    }

    /**
     * @param InfoCustomdictionaryvalue|null $interaction
     * @return self
     */
    public function setInteraction(?InfoCustomdictionaryvalue $interaction): self
    {
        $this->interaction = $interaction;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getLegalName(): ?string
    {
        return $this->legalName;
    }

    /**
     * @param string|null $legalName
     * @return self
     */
    public function setLegalName(?string $legalName): self
    {
        $this->legalName = $legalName;
        return $this;
    }

    /**
     * @return \DateTime|null
     */
    public function getPassportDate(): ?\DateTime
    {
        return $this->passportDate;
    }

    /**
     * @param \DateTime|null $passportDate
     * @return self
     */
    public function setPassportDate(?\DateTime $passportDate): self
    {
        $this->passportDate = $passportDate;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getPassportSeriesNumber(): ?string
    {
        return $this->passportSeriesNumber;
    }

    /**
     * @param string|null $passportSeriesNumber
     * @return self
     */
    public function setPassportSeriesNumber(?string $passportSeriesNumber): self
    {
        $this->passportSeriesNumber = $passportSeriesNumber;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getPassportIssued(): ?string
    {
        return $this->passportIssued;
    }

    /**
     * @param string|null $passportIssued
     * @return self
     */
    public function setPassportIssued(?string $passportIssued): self
    {
        $this->passportIssued = $passportIssued;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getRegAddress(): ?string
    {
        return $this->regAddress;
    }

    /**
     * @param string|null $regAddress
     * @return self
     */
    public function setRegAddress(?string $regAddress): self
    {
        $this->regAddress = $regAddress;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getInsuranceCertificate(): ?string
    {
        return $this->insuranceCertificate;
    }

    /**
     * @param string|null $insuranceCertificate
     * @return self
     */
    public function setInsuranceCertificate(?string $insuranceCertificate): self
    {
        $this->insuranceCertificate = $insuranceCertificate;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getIdentificationNumber(): ?string
    {
        return $this->identificationNumber;
    }

    /**
     * @param string|null $identificationNumber
     * @return self
     */
    public function setIdentificationNumber(?string $identificationNumber): self
    {
        $this->identificationNumber = $identificationNumber;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getCheckingAccount(): ?string
    {
        return $this->checkingAccount;
    }

    /**
     * @param string|null $checkingAccount
     * @return self
     */
    public function setCheckingAccount(?string $checkingAccount): self
    {
        $this->checkingAccount = $checkingAccount;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getBank(): ?string
    {
        return $this->bank;
    }

    /**
     * @param string|null $bank
     * @return self
     */
    public function setBank(?string $bank): self
    {
        $this->bank = $bank;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getBic(): ?string
    {
        return $this->bic;
    }

    /**
     * @param string|null $bic
     * @return self
     */
    public function setBic(?string $bic): self
    {
        $this->bic = $bic;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getCorrespondentAccount(): ?string
    {
        return $this->correspondentAccount;
    }

    /**
     * @param string|null $correspondentAccount
     * @return self
     */
    public function setCorrespondentAccount(?string $correspondentAccount): self
    {
        $this->correspondentAccount = $correspondentAccount;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getSnils(): ?string
    {
        return $this->snils;
    }

    /**
     * @param string|null $snils
     * @return self
     */
    public function setSnils(?string $snils): self
    {
        $this->snils = $snils;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getKpp(): ?string
    {
        return $this->kpp;
    }

    /**
     * @param string|null $kpp
     * @return self
     */
    public function setKpp(?string $kpp): self
    {
        $this->kpp = $kpp;
        return $this;
    }


    public function getInteractionId(): ?int
    {
        return $this->getInteraction() ? $this->getInteraction()->getId() : null;
    }

    public function getLegalStatusId(): ?int
    {
        return $this->getLegalStatus() ? $this->getLegalStatus()->getId() : null;
    }

    /**
     * @return ArrayCollection|PersistentCollection|Collection
     */
    public function getContactBlogCollection(): Collection
    {
        return $this->contactBlogCollection;
    }

    /**
     * @param InfoContactBlog $contactBlog
     */
    public function addContactBlogCollection(InfoContactBlog $contactBlog): void
    {
        if (!$this->contactBlogCollection->contains($contactBlog)) {
            $contactBlog->setContact($this);
            $this->contactBlogCollection->add($contactBlog);
        }
    }

    public function removeContactBlogCollection(InfoContactBlog $contactBlog)
    {
        $this->contactBlogCollection->removeElement($contactBlog);
    }

    public function hasValidEmail(): bool
    {
        foreach ($this->getContactEmailCollection() as $item) {
            if ($item->getEmail() && $item->isEmailValid()) {
                return true;
            }
        }

        return false;
    }

    public function hasValidPhoneNumberForType(string $type): bool
    {
        if ($type === InfoMcmcontent::TYPE_VIBER) {
            foreach ($this->getContactPhoneCollection() as $item) {
                if ($item->isViberForMcm()) {
                    return true;
                }
            }
        } elseif ($type === InfoMcmcontent::TYPE_SMS) {
            foreach ($this->getContactPhoneCollection() as $item) {
                if ($item->isSmsForMcm()) {
                    return true;
                }
            }
        }

        return false;
    }

    /**
     * @param InfoLanguage $language
     *
     * @return InfoContact
     */
    public function setLanguage(InfoLanguage $language)
    {
        $this->language = $language;
        return $this;
    }

    /**
     * @return InfoLanguage
     */
    public function getLanguage()
    {
        return $this->language;
    }

    /**
     * Set lastnameUkr
     *
     * @param string $lastnameUkr
     * @return InfoContact
     */
    public function setLastnameUkr($lastnameUkr)
    {
        $this->lastnameUkr = $lastnameUkr;

        return $this;
    }

    /**
     * Get lastnameUkr
     *
     * @return string
     */
    public function getLastnameUkr()
    {
        return $this->lastnameUkr;
    }

    /**
     * Set firstnameUkr
     *
     * @param string $firstnameUkr
     * @return InfoContact
     */
    public function setFirstnameUkr($firstnameUkr)
    {
        $this->firstnameUkr = $firstnameUkr;

        return $this;
    }

    /**
     * Get firstnameUkr
     *
     * @return string
     */
    public function getFirstnameUkr()
    {
        return $this->firstnameUkr;
    }

    /**
     * Set middlenameUkr
     *
     * @param string $middlenameUkr
     * @return InfoContact
     */
    public function setMiddlenameUkr($middlenameUkr)
    {
        $this->middlenameUkr = $middlenameUkr;

        return $this;
    }

    /**
     * Get middlenameUkr
     *
     * @return string
     */
    public function getMiddlenameUkr()
    {
        return $this->middlenameUkr;
    }

    /**
     * Set lastnameeng
     *
     * @param string $lastnameEng
     * @return InfoContact
     */
    public function setLastnameEng($lastnameEng)
    {
        $this->lastnameeng = $lastnameEng;

        return $this;
    }

    /**
     * Get lastnameEng
     *
     * @return string
     */
    public function getLastnameEng()
    {
        return $this->lastnameEng;
    }

    /**
     * Set firstnameeng
     *
     * @param string $firstnameEng
     * @return InfoContact
     */
    public function setFirstnameEng($firstnameEng)
    {
        $this->firstnameeng = $firstnameEng;

        return $this;
    }

    /**
     * Get firstnameEng
     *
     * @return string
     */
    public function getFirstnameEng()
    {
        return $this->firstnameEng;
    }

    /**
     * Set middlenameeng
     *
     * @param string $middlenameEng
     * @return InfoContact
     */
    public function setMiddlenameEng($middlenameEng)
    {
        $this->middlenameeng = $middlenameEng;

        return $this;
    }

    /**
     * Get middlenameeng
     *
     * @return string
     */
    public function getMiddlenameEng()
    {
        return $this->middlenameeng;
    }

    /**
     * @return InfoContactComment|null
     */
    public function getContactComment(): ?InfoContactComment
    {
        return $this->contactComment;
    }

    /**
     * @param InfoContactComment|null $contactComment
     * @return self
     */
    public function setContactComment(?InfoContactComment $contactComment): self
    {
        $this->contactComment = $contactComment;
        $contactComment->setContact($this);

        return $this;
    }

    /**
     * @return ArrayCollection
     */
    public function getRtcRoomReserves()
    {
        return $this->rtcRoomReserves;
    }

    /**
     * @param InfoRtcRoomReserve $RTCRoomReserve
     * @return $this
     */
    public function addRtcRoomReserve(InfoRtcRoomReserve $RTCRoomReserve){
        if(!$this->rtcRoomReserves->contains($RTCRoomReserve)){
            $this->rtcRoomReserves->add($RTCRoomReserve);
        }
        $RTCRoomReserve->setRtcRoom($this);
        return $this;
    }

    /**
     * @param InfoRtcRoomReserve $RTCRoomReserve
     * @return $this
     */
    public function removeRtcRoomReserve(InfoRtcRoomReserve $RTCRoomReserve){
        if($this->rtcRoomReserves->contains($RTCRoomReserve)){
            $this->rtcRoomReserves->removeElement($RTCRoomReserve);
        }
        $RTCRoomReserve->setRtcRoom(null);
        return $this;
    }

    /**
     * @return $this
     */
    public function clearRtcRoomReserves(){
        $this->rtcRoomReserves->clear();
        return $this;
    }

    /**
     * Get mcmContactVariablesCollection
     *
     * @return \Doctrine\Common\Collections\ArrayCollection
     */
    public function getMcmJsons()
    {
        return $this->mcmJsons;
    }

    public function addMcmJson(InfoMcmjson $mcmJson): self
    {
        if(!$this->mcmJsons->contains($mcmJson)) {
            $mcmJson->setContact($this);
            $this->mcmJsons->add($mcmJson);
        }

        return $this;
    }

    public function removeMcmJson(InfoMcmjson $mcmJson): self
    {
        if(!$this->mcmJsons->contains($mcmJson)) {
            $mcmJson->setContact(null);
            $this->mcmJsons->removeElement($mcmJson);
        }

        return $this;
    }

    public function getContactSignsNotArchive(): ?ArrayCollection
    {
        $contactSingsNotArchive = $this->contactSigns->filter(function($contactSign){
           return $contactSign->getIsArchive() != 1;
        })->getValues();

        return new ArrayCollection($contactSingsNotArchive);
    }
}
