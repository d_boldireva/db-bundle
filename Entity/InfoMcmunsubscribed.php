<?php

namespace TeamSoft\CrmRepositoryBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * InfoMcmunsubscribed
 *
 * @ORM\Table(name="info_mcmunsubscribed")
 * @ORM\Entity(repositoryClass="TeamSoft\CrmRepositoryBundle\Repository\InfoMcmunsubscribed")
 */
class InfoMcmunsubscribed implements ServiceFieldInterface
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var \TeamSoft\CrmRepositoryBundle\Entity\InfoContact
     *
     * @ORM\ManyToOne(targetEntity="\TeamSoft\CrmRepositoryBundle\Entity\InfoContact")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="contact_id", referencedColumnName="id")
     * })
     */
    private $contact;

    /**
     * @var \TeamSoft\CrmRepositoryBundle\Entity\InfoCustomdictionaryvalue
     *
     * @ORM\ManyToOne(targetEntity="\TeamSoft\CrmRepositoryBundle\Entity\InfoCustomdictionaryvalue")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="reason_id", referencedColumnName="id", nullable=true)
     * })
     */
    private $reason;

    /**
     * @var string
     *
     * @ORM\Column(name="reason_text", type="string", length=255, nullable=true)
     */
    private $reasonText;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="currenttime", type="datetime", nullable=true)
     */
    private $currenttime;

    /**
     * @var string
     *
     * @ORM\Column(name="moduser", type="string", length=16, nullable=true)
     */
    private $moduser;

    /**
     * @var string
     *
     * @ORM\Column(name="guid", type="guid", nullable=true)
     */
    private $guid;

    /**
     * @var \TeamSoft\CrmRepositoryBundle\Entity\InfoMcmdistribution
     *
     * @ORM\ManyToOne(targetEntity="\TeamSoft\CrmRepositoryBundle\Entity\InfoMcmdistribution")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="distribution_id", referencedColumnName="id", nullable=true)
     * })
     */
    private $distribution;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set currenttime
     *
     * @param \DateTime $currenttime
     *
     * @return InfoMcmunsubscribed
     */
    public function setCurrenttime(\DateTime $currenttime)
    {
        $this->currenttime = $currenttime;

        return $this;
    }

    /**
     * Get currenttime
     *
     * @return \DateTime
     */
    public function getCurrenttime()
    {
        return $this->currenttime;
    }

    /**
     * Set reasonText
     *
     * @param string $reasonText
     *
     * @return InfoMcmunsubscribed
     */
    public function setReasonText($reasonText)
    {
        $this->reasonText = $reasonText;

        return $this;
    }

    /**
     * Get reasonText
     *
     * @return string
     */
    public function getReasonText()
    {
        return $this->reasonText;
    }

    /**
     * Set moduser
     *
     * @param string $moduser
     *
     * @return InfoMcmunsubscribed
     */
    public function setModuser($moduser)
    {
        $this->moduser = $moduser;

        return $this;
    }

    /**
     * Get moduser
     *
     * @return string
     */
    public function getModuser()
    {
        return $this->moduser;
    }

    /**
     * Set guid
     *
     * @param string $guid
     *
     * @return InfoMcmunsubscribed
     */
    public function setGuid($guid)
    {
        $this->guid = $guid;

        return $this;
    }

    /**
     * Get guid
     *
     * @return string
     */
    public function getGuid()
    {
        return $this->guid;
    }

    /**
     * Set reason
     *
     * @param \TeamSoft\CrmRepositoryBundle\Entity\InfoCustomdictionaryvalue $reason
     *
     * @return InfoMcmunsubscribed
     */
    public function setReason(InfoCustomdictionaryvalue $reason = null)
    {
        $this->reason = $reason;

        return $this;
    }

    /**
     * Get reason
     *
     * @return \TeamSoft\CrmRepositoryBundle\Entity\InfoCustomdictionaryvalue
     */
    public function getReason()
    {
        return $this->reason;
    }

    /**
     * Set contact
     *
     * @param \TeamSoft\CrmRepositoryBundle\Entity\InfoContact $contact
     *
     * @return InfoMcmunsubscribed
     */
    public function setContact(\TeamSoft\CrmRepositoryBundle\Entity\InfoContact $contact = null)
    {
        $this->contact = $contact;

        return $this;
    }

    /**
     * Get contact
     *
     * @return \TeamSoft\CrmRepositoryBundle\Entity\InfoContact
     */
    public function getContact()
    {
        return $this->contact;
    }

    /**
     * Set distribution
     *
     * @param \TeamSoft\CrmRepositoryBundle\Entity\InfoMcmdistribution $distribution
     *
     * @return InfoMcmunsubscribed
     */
    public function setDistribution(InfoMcmdistribution $distribution = null): self
    {
        $this->distribution = $distribution;

        return $this;
    }

    /**
     * Get distribution
     *
     * @return \TeamSoft\CrmRepositoryBundle\Entity\InfoMcmdistribution|null
     */
    public function getDistribution(): ?InfoMcmdistribution
    {
        return $this->distribution;
    }
}
