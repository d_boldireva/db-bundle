<?php

namespace TeamSoft\CrmRepositoryBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * InfoEventmsg
 *
 * @ORM\Table(name="info_eventmsg")
 * @ORM\Entity
 */
class InfoEventmsg
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="msg", type="string", length=255, nullable=true)
     */
    private $msg;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="createdate", type="datetime", nullable=true)
     */
    private $createdate;

    /**
     * @var \InfoEvent
     *
     * @ORM\ManyToOne(targetEntity="InfoEvent")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="event_id", referencedColumnName="id")
     * })
     */
    private $event;

    /**
     * @var \InfoUser
     *
     * @ORM\ManyToOne(targetEntity="InfoUser")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     * })
     */
    private $user;

    /**
     * @ORM\ManyToMany(targetEntity="InfoUser")
     * @ORM\JoinTable(name="info_eventmsguser",
     *     joinColumns={@ORM\JoinColumn(name="eventmsg_id", referencedColumnName="id")},
     *     inverseJoinColumns={@ORM\JoinColumn(name="user_id", referencedColumnName="id")}
     * )
     */
    private $userCollection;


    /**
     * InfoEventmsg constructor.
     */
    function __construct()
    {
        $this->setCreatedate(new \DateTime());
        $this->userCollection = new ArrayCollection();
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set msg
     *
     * @param string $msg
     *
     * @return InfoEventmsg
     */
    public function setMsg($msg)
    {
        $this->msg = $msg;

        return $this;
    }

    /**
     * Get msg
     *
     * @return string
     */
    public function getMsg()
    {
        return $this->msg;
    }

    /**
     * Set event
     *
     * @param \TeamSoft\CrmRepositoryBundle\Entity\InfoEvent $event
     *
     * @return InfoEventmsg
     */
    public function setEvent(\TeamSoft\CrmRepositoryBundle\Entity\InfoEvent $event = null)
    {
        $this->event = $event;

        return $this;
    }

    /**
     * Get event
     *
     * @return \TeamSoft\CrmRepositoryBundle\Entity\InfoEvent
     */
    public function getEvent()
    {
        return $this->event;
    }

    /**
     * Set user
     *
     * @param \TeamSoft\CrmRepositoryBundle\Entity\InfoUser $user
     *
     * @return InfoEventmsg
     */
    public function setUser(\TeamSoft\CrmRepositoryBundle\Entity\InfoUser $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \TeamSoft\CrmRepositoryBundle\Entity\InfoUser
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Add infoUser
     *
     * @param \TeamSoft\CrmRepositoryBundle\Entity\InfoUser $infoUser
     *
     * @return InfoEventmsg
     */
    public function addUserCollection(InfoUser $infoUser)
    {
        $this->userCollection[] = $infoUser;

        return $this;
    }

    /**
     * Remove infoUser
     *
     * @param \TeamSoft\CrmRepositoryBundle\Entity\InfoUser $infoUser
     */
    public function removeUserCollection(InfoUser $infoUser)
    {
        $this->userCollection->removeElement($infoUser);
    }

    /**
     * Get userCollection
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getUserCollection()
    {
        return $this->userCollection;
    }

    /**
     * @param \DateTime $createdate
     * @return InfoEventmsg
     */
    public function setCreatedate(\DateTime $createdate)
    {
        $this->createdate = $createdate;

        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getCreatedate(): ?\DateTime
    {
        return $this->createdate;
    }
}
