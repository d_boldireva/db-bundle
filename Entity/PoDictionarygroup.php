<?php

namespace TeamSoft\CrmRepositoryBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use TeamSoft\CrmRepositoryBundle\Entity\ServiceFieldInterface;

/**
 * PoDictionarygroup
 *
 * @ORM\Table(name="po_dictionarygroup")
 * @ORM\Entity
 */
class PoDictionarygroup implements ServiceFieldInterface
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string|null
     *
     * @ORM\Column(name="name", type="string", length=255, nullable=true)
     */
    private $name;

    /**
     * @var int|null
     *
     * @ORM\Column(name="`order`", type="integer", nullable=true)
     */
    private $order;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="currenttime", type="datetime", nullable=true)
     */
    private $currenttime;

    /**
     * @var string|null
     *
     * @ORM\Column(
     *     name="moduser",
     *     type="string",
     *     length=16,
     *     nullable=true,
     *     options={"default"="[dbo].[Get_CurrentCode]()"}
     * )
     */
    private $moduser;

    /**
     * @var string|null
     *
     * @ORM\Column(name="guid", type="guid", nullable=true)
     */
    private $guid;

    /**
     * One Group has Many Dictionaries
     * @ORM\OneToMany(targetEntity="PoDictionary", mappedBy="group")
     * @ORM\JoinColumn(name="id", referencedColumnName="group")
     */
    private $dictionaries;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->dictionaries = new ArrayCollection();
    }


    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name.
     *
     * @param string|null $name
     *
     * @return PoDictionarygroup
     */
    public function setName($name = null)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name.
     *
     * @return string|null
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set order.
     *
     * @param int|null $order
     *
     * @return PoDictionarygroup
     */
    public function setOrder($order = null)
    {
        $this->order = $order;

        return $this;
    }

    /**
     * Get order.
     *
     * @return int|null
     */
    public function getOrder()
    {
        return $this->order;
    }

    /**
     * Set currenttime.
     *
     * @param \DateTime|null $currenttime
     *
     * @return PoDictionarygroup
     */
    public function setCurrenttime(\DateTime $currenttime = null)
    {
        $this->currenttime = $currenttime;

        return $this;
    }

    /**
     * Get currenttime.
     *
     * @return \DateTime|null
     */
    public function getCurrenttime()
    {
        return $this->currenttime;
    }

    /**
     * Set moduser.
     *
     * @param string|null $moduser
     *
     * @return PoDictionarygroup
     */
    public function setModuser($moduser = null)
    {
        $this->moduser = $moduser;

        return $this;
    }

    /**
     * Get moduser.
     *
     * @return string|null
     */
    public function getModuser()
    {
        return $this->moduser;
    }

    /**
     * Set guid.
     *
     * @param string|null $guid
     *
     * @return PoDictionarygroup
     */
    public function setGuid($guid = null)
    {
        $this->guid = $guid;

        return $this;
    }

    /**
     * Get guid.
     *
     * @return string|null
     */
    public function getGuid()
    {
        return $this->guid;
    }


    /**
     * Add dictionary
     *
     * @param PoDictionary $dictionary
     *
     * @return PoDictionarygroup
     */
    public function addDictionary(PoDictionary $dictionary)
    {
        $dictionary->setGroup($this);
        $this->dictionaries->add($dictionary);

        return $this;
    }

    /**
     * Remove dictionary
     *
     * @param PoDictionary $dictionary
     */
    public function removeDictionary(PoDictionary $dictionary)
    {
        $dictionary->setGroup(null);
        $this->dictionaries->removeElement($dictionary);
    }

    /**
     * Get dictionaries
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getDictionaries()
    {
        return $this->dictionaries;
    }

    /**
     * set dictionaries
     *
     * @param \Doctrine\Common\Collections\Collection $poDictionaries
     *
     * @return PoDictionarygroup
     */
    public function setDictionaries(\Doctrine\Common\Collections\Collection $poDictionaries)
    {
        $this->dictionaries = $poDictionaries;

        return $this;
    }
}
