<?php

namespace TeamSoft\CrmRepositoryBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * InfoUserbrick
 *
 * @ORM\Table(name="info_userbrick")
 * @ORM\Entity(repositoryClass="TeamSoft\CrmRepositoryBundle\Repository\InfoUserBrick")
 */
class InfoUserbrick
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="currenttime", type="datetime", nullable=true)
     */
    private $currenttime;

    /**
     * @var string|null
     *
     * @ORM\Column(name="moduser", type="string", length=16, nullable=true)
     */
    private $moduser;

    /**
     * @var string|null
     *
     * @ORM\Column(name="guid", type="guid", nullable=true)
     */
    private $guid;

    /**
     * @var \InfoUser
     *
     * @ORM\ManyToOne(targetEntity="\TeamSoft\CrmRepositoryBundle\Entity\InfoUser")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     * })
     */
    private $user;

    /**
     * @var \InfoCompany
     *
     * @ORM\ManyToOne(targetEntity="\TeamSoft\CrmRepositoryBundle\Entity\InfoCompany")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="company_id", referencedColumnName="id")
     * })
     */
    private $company;

    /**
     * @var \InfoContact
     *
     * @ORM\ManyToOne(targetEntity="\TeamSoft\CrmRepositoryBundle\Entity\InfoContact")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="contact_id", referencedColumnName="id")
     * })
     */
    private $contact;



    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set currenttime.
     *
     * @param \DateTime|null $currenttime
     *
     * @return InfoUserbrick
     */
    public function setCurrenttime($currenttime = null)
    {
        $this->currenttime = $currenttime;

        return $this;
    }

    /**
     * Get currenttime.
     *
     * @return \DateTime|null
     */
    public function getCurrenttime()
    {
        return $this->currenttime;
    }

    /**
     * Set moduser.
     *
     * @param string|null $moduser
     *
     * @return InfoUserbrick
     */
    public function setModuser($moduser = null)
    {
        $this->moduser = $moduser;

        return $this;
    }

    /**
     * Get moduser.
     *
     * @return string|null
     */
    public function getModuser()
    {
        return $this->moduser;
    }

    /**
     * Set guid.
     *
     * @param string|null $guid
     *
     * @return InfoUserbrick
     */
    public function setGuid($guid = null)
    {
        $this->guid = $guid;

        return $this;
    }

    /**
     * Get guid.
     *
     * @return string|null
     */
    public function getGuid()
    {
        return $this->guid;
    }

    /**
     * Set user.
     *
     * @param \TeamSoft\CrmRepositoryBundle\Entity\InfoUser|null $user
     *
     * @return InfoUserbrick
     */
    public function setUser(\TeamSoft\CrmRepositoryBundle\Entity\InfoUser $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user.
     *
     * @return \TeamSoft\CrmRepositoryBundle\Entity\InfoUser|null
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Set company.
     *
     * @param \\TeamSoft\CrmRepositoryBundle\Entity\InfoCompany|null $company
     *
     * @return InfoUserbrick
     */
    public function setCompany(\TeamSoft\CrmRepositoryBundle\Entity\InfoCompany $company = null)
    {
        $this->company = $company;

        return $this;
    }

    /**
     * Get company.
     *
     * @return \TeamSoft\CrmRepositoryBundle\Entity\InfoCompany|null
     */
    public function getCompany()
    {
        return $this->company;
    }

    /**
     * Set contact.
     *
     * @param \TeamSoft\CrmRepositoryBundle\Entity\InfoContact|null $contact
     *
     * @return InfoUserbrick
     */
    public function setContact(\TeamSoft\CrmRepositoryBundle\Entity\InfoContact $contact = null)
    {
        $this->contact = $contact;

        return $this;
    }

    /**
     * Get contact.
     *
     * @return \TeamSoft\CrmRepositoryBundle\Entity\InfoContact|null
     */
    public function getContact()
    {
        return $this->contact;
    }
}
