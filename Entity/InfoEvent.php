<?php

namespace TeamSoft\CrmRepositoryBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\Context\ExecutionContextInterface;
use Doctrine\ORM\Mapping as ORM;

/**
 * InfoEvent
 *
 * @ORM\Table(name="info_event")
 * @ORM\Entity
 */
class InfoEvent
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="event_desc", type="text", length=-1, nullable=true)
     */
    private $eventDesc;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="createdate", type="datetime", nullable=true)
     */
    private $createdate;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="donedate", type="datetime", nullable=true)
     */
    private $donedate;

    /**
     * @var \DateTime
     *
     *
     * @ORM\Column(name="modified", type="datetime", nullable=true)
     */
    private $modified;

    /**
     * @var \InfoCustomdictionaryvalue
     *
     * @ORM\ManyToOne(targetEntity="InfoCustomdictionaryvalue")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="status_id", referencedColumnName="id")
     * })
     */
    private $status;

    /**
     * @var \InfoTasktype
     *
     * @ORM\ManyToOne(targetEntity="InfoTasktype")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="tasktype_id", referencedColumnName="id")
     * })
     */
    private $tasktype;

    /**
     * @var \InfoActiontype
     *
     * @ORM\ManyToOne(targetEntity="InfoActiontype")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="actiontype_id", referencedColumnName="id")
     * })
     */
    private $actiontype;

    /**
     * @var \InfoEvent
     *
     * @ORM\ManyToOne(targetEntity="InfoEvent")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="parent_id", referencedColumnName="id")
     * })
     */
    private $parent;

    /**
     * @var \InfoEvent
     *
     * @ORM\ManyToOne(targetEntity="InfoEvent")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="mainparent_id", referencedColumnName="id")
     * })
     */
    private $mainparent;

    /**
     * @var \InfoUser
     *
     * @ORM\ManyToOne(targetEntity="InfoUser")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     * })
     */
    private $user;

    /**
     * @var ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="InfoEventlocation", mappedBy="event", cascade={"persist"})
     */
    private $eventlocations;

    /**
     * @var ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="InfoEventmsg", mappedBy="event", cascade={"persist", "remove"})
     */
    private $eventmsgCollection;

    /**
     * @ORM\ManyToMany(targetEntity="InfoUser")
     * @ORM\JoinTable(name="info_eventuser",
     *     joinColumns={@ORM\JoinColumn(name="event_id", referencedColumnName="id")},
     *     inverseJoinColumns={@ORM\JoinColumn(name="user_id", referencedColumnName="id")}
     * )
     */
    private $eventusers;

    /**
     * @var ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="InfoEvent", mappedBy="parent")
     */
    private $childrens;

    /**
     * @ORM\ManyToMany(targetEntity="InfoTask", inversedBy="events")
     * @ORM\JoinTable(name="info_eventtask",
     *     joinColumns={@ORM\JoinColumn(name="event_id", referencedColumnName="id")},
     *     inverseJoinColumns={@ORM\JoinColumn(name="task_id", referencedColumnName="id")}
     * )
     */
    private $tasks;

    /**
     * @ORM\ManyToMany(targetEntity="InfoAction", inversedBy="events")
     * @ORM\JoinTable(name="info_eventtask",
     *     joinColumns={@ORM\JoinColumn(name="event_id", referencedColumnName="id")},
     *     inverseJoinColumns={@ORM\JoinColumn(name="action_id", referencedColumnName="id")}
     * )
     */
    private $actions;

    /**
     * @ORM\ManyToMany(targetEntity="InfoUser", inversedBy="observedEvents")
     * @ORM\JoinTable(name="info_eventobserver",
     *     joinColumns={@ORM\JoinColumn(name="event_id",referencedColumnName="id")},
     *     inverseJoinColumns={@ORM\JoinColumn(name="user_id",referencedColumnName="id")}
     * )
     */
    private $observers;


    /**
     * InfoEvent constructor.
     */
    public function __construct()
    {
        $this->setCreatedate(new \DateTime('now'));
        $this->eventlocations = new ArrayCollection();
        $this->eventusers = new ArrayCollection();
        $this->eventmsgCollection = new ArrayCollection();
        $this->observers = new ArrayCollection();
    }

    /**
     * __clone()
     */
    public function __clone()
    {
        $this->id = null;
        $this->eventusers = new ArrayCollection();
        $this->eventmsgCollection = new ArrayCollection();
        $this->eventlocations = new ArrayCollection();
        $this->observers = new ArrayCollection();
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set eventDesc
     *
     * @param string $eventDesc
     *
     * @return InfoEvent
     */
    public function setEventDesc($eventDesc)
    {
        $this->eventDesc = $eventDesc;

        return $this;
    }

    /**
     * Get eventDesc
     *
     * @return string
     */
    public function getEventDesc()
    {
        return $this->eventDesc;
    }

    /**
     * Set status
     *
     * @param InfoCustomdictionaryvalue $status
     *
     * @return InfoEvent
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return InfoCustomdictionaryvalue
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set createdate
     *
     * @param \DateTime $createdate
     *
     * @return InfoEvent
     */
    public function setCreatedate($createdate)
    {
        $this->createdate = $createdate;

        return $this;
    }

    /**
     * Get createdate
     *
     * @return \DateTime
     */
    public function getCreatedate()
    {
        return $this->createdate;
    }

    /**
     * Set tasktype
     *
     * @param \TeamSoft\CrmRepositoryBundle\Entity\InfoTasktype $tasktype
     *
     * @return InfoEvent
     */
    public function setTasktype(\TeamSoft\CrmRepositoryBundle\Entity\InfoTasktype $tasktype = null)
    {
        $this->tasktype = $tasktype;

        return $this;
    }

    /**
     * Get tasktype
     *
     * @return \TeamSoft\CrmRepositoryBundle\Entity\InfoTasktype
     */
    public function getTasktype()
    {
        return $this->tasktype;
    }

    /**
     * Set actiontype
     *
     * @param \TeamSoft\CrmRepositoryBundle\Entity\InfoActiontype $actiontype
     *
     * @return InfoEvent
     */
    public function setActiontype(\TeamSoft\CrmRepositoryBundle\Entity\InfoActiontype $actiontype = null)
    {
        $this->actiontype = $actiontype;

        return $this;
    }

    /**
     * Get actiontype
     *
     * @return \TeamSoft\CrmRepositoryBundle\Entity\InfoActiontype
     */
    public function getActiontype()
    {
        return $this->actiontype;
    }

    /**
     * Set parent
     *
     * @param \TeamSoft\CrmRepositoryBundle\Entity\InfoEvent $parent
     *
     * @return InfoEvent
     */
    public function setParent(\TeamSoft\CrmRepositoryBundle\Entity\InfoEvent $parent = null)
    {
        $this->parent = $parent;

        return $this;
    }

    /**
     * Get parent
     *
     * @return \TeamSoft\CrmRepositoryBundle\Entity\InfoEvent
     */
    public function getParent()
    {
        return $this->parent;
    }

    /**
     * Set mainparent
     *
     * @param \TeamSoft\CrmRepositoryBundle\Entity\InfoEvent $mainparent
     *
     * @return InfoEvent
     */
    public function setMainparent(\TeamSoft\CrmRepositoryBundle\Entity\InfoEvent $mainparent = null)
    {
        $this->mainparent = $mainparent;

        return $this;
    }

    /**
     * Get mainparent
     *
     * @return \TeamSoft\CrmRepositoryBundle\Entity\InfoEvent
     */
    public function getMainparent()
    {
        return $this->mainparent;
    }

    /**
     * Set user
     *
     * @param \TeamSoft\CrmRepositoryBundle\Entity\InfoUser $user
     *
     * @return InfoEvent
     */
    public function setUser(\TeamSoft\CrmRepositoryBundle\Entity\InfoUser $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \InfoUser
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @param $users
     * @return InfoEvent
     */
    public function setUsers($users): InfoEvent
    {
        $this->users = $users;

        return $this;
    }

    /**
     * @param InfoEventlocation $eventlocation
     * @return InfoEvent
     */
    public function addEventlocation(InfoEventlocation $eventlocation)
    {
        $eventlocation->setEvent($this);
        $this->eventlocations->add($eventlocation);

        return $this;
    }

    /**
     * @param InfoEventlocation $eventlocation
     */
    public function removeEventlocation(InfoEventlocation $eventlocation)
    {
        $this->eventlocations->removeElement($eventlocation);
    }

    /**
     * @return mixed
     */
    public function getEventlocations()
    {
        return $this->eventlocations;
    }

    /**
     * @param InfoUser $user
     * @return InfoEvent
     */
    public function addEventuser(InfoUser $user)
    {
        $this->eventusers->add($user);

        return $this;
    }

    /**
     * @param InfoUser $eventuser
     */
    public function removeEventuser(InfoUser $eventuser)
    {
        $this->eventusers->remove($eventuser);
    }

    /**
     * @return ArrayCollection
     */
    public function getEventusers()
    {
        return $this->eventusers;
    }

    /**
     * @return InfoEvent
     */
    public function setEventusers(?ArrayCollection $eventusers)
    {
        $this->eventusers = $eventusers;

        return $this;
    }

    /**
     * @param InfoUser $observer
     * @return InfoEvent
     */
    public function addObserver(InfoUser $observer){
        $this->observers->add($observer);

        return $this;
    }

    /**
     * @param InfoUser $observer
     * @return InfoEvent
     */
    public function removeObserver(InfoUser $observer){
        $this->observers->removeElement($observer);

        return $this;
    }

    /**
     * @return ArrayCollection
     */
    public function getObservers(){
        return $this->observers;
    }

    /**
     * @param ArrayCollection|null $observers
     * @return InfoEvent
     */
    public function setObservers(?ArrayCollection $observers){
        if($observers){
            $this->observers = $observers;
        } else {
            foreach ($this->observers as $observer){
                $this->observers->removeElement($observer);
            }
        }

        return $this;
    }

    /**
     * @param InfoEventmsg $eventmsg
     * @return InfoEvent
     */
    public function addEventmsgCollection(InfoEventmsg $eventmsg)
    {
        $eventmsg->setEvent($this);
        $this->eventmsgCollection->add($eventmsg);

        return $this;
    }

    /**
     * @param InfoEventmsg $eventmsg
     */
    public function removeEventmsgCollection(InfoEventmsg $eventmsg)
    {
        $this->eventmsgCollection->removeElement($eventmsg);
    }

    /**
     * @return ArrayCollection
     */
    public function getEventmsgCollection()
    {
        return $this->eventmsgCollection;
    }

    /**
     * @param ExecutionContextInterface $context
     * @param $payload
     * @Assert\Callback
     */
    public function validate(ExecutionContextInterface $context, $payload)
    {
        if (!$this->getActiontype() && !$this->getTasktype()) {
            $context->buildViolation('This value should not be blank.')
                ->atPath('tasktype')
                ->addViolation();
        }
    }

    /**
     * @return ArrayCollection
     */
    public function getChildrens(): ?ArrayCollection
    {
        return $this->childrens;
    }

    /**
     * @param ArrayCollection $childrens
     */
    public function setChildrens(?ArrayCollection $childrens)
    {
        $this->childrens = $childrens;
    }

    /**
     * @return \DateTime
     */
    public function getDonedate(): ?\DateTime
    {
        return $this->donedate;
    }

    /**
     * @param \DateTime $donedate
     */
    public function setDonedate(?\DateTime $donedate)
    {
        $this->donedate = $donedate;
    }

    /**
     * @return mixed
     */
    public function getTasks()
    {
        return $this->tasks;
    }

    /**
     * @param mixed $tasks
     */
    public function setTasks($tasks)
    {
        $this->tasks = $tasks;
    }

    /**
     * @return mixed
     */
    public function getActions()
    {
        return $this->actions;
    }

    /**
     * @param mixed $actions
     */
    public function setActions($actions)
    {
        $this->actions = $actions;
    }

    /**
     * @return bool
     */
    public function hasMain()
    {
        return $this->getParent() !== null;
    }

    /**
     * @return bool
     */
    public function isDone()
    {
        return $this->getStatus() && $this->getStatus()->getPos() === 3;
    }

    /**
     * @return \DateTime
     */
    public function getModified(): \DateTime
    {
        return $this->modified;
    }

    /**
     * @param \DateTime $modified
     */
    public function setModified(\DateTime $modified)
    {
        $this->modified = $modified;
    }

    /**
     * @return \InfoUser
     */
    public function getParentUser()
    {
        return $this->getParent() ? $this->getParent()->getUser() : $this->getUser();
    }

    /**
     * @return array
     */
    public function getDoneActions()
    {
        $actions = [];

        /**
         * @var $action InfoAction
         */
        foreach (($this->getActions() ?: []) as $action) {
            if ($action->getActionstate()->getIsclosed()) {
                $actions[] = $action;
            }
        }

        return $actions;
    }

    /**
     * @return array
     */
    public function getDoneTasks()
    {
        $tasks = [];

        /**
         * @var $task InfoTask
         */
        foreach ($this->getTasks() ?: [] as $task) {
            if ($task->isDone()) {
                $tasks[] = $task;
            }
        }

        return $tasks;
    }
}
