<?php
namespace TeamSoft\CrmRepositoryBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * InfoSaleplan
 *
 * @ORM\Table(name="info_saleplan")
 * @ORM\Entity
 */
class InfoSaleplan implements ServiceFieldInterface
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="guid", type="guid", nullable=true)
     */
    private $guid;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="currenttime", type="datetime", nullable=true)
     */
    private $currenttime;

    /**
     * @var string
     *
     * @ORM\Column(name="moduser", type="string", length=16, nullable=true)
     */
    private $moduser;

    /**
     * @var InfoSaleperiod
     *
     * @ORM\ManyToOne(targetEntity="InfoSaleperiod")
     * @ORM\JoinColumn(name="saleperiod_id", referencedColumnName="id")
     */
    private $saleperiod;

    /**
     * @var InfoUser
     *
     * @ORM\ManyToOne(targetEntity="InfoUser")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     */
    private $user;

    /**
     * @var string
     *
     * @ORM\Column(name="Description", type="varchar_or_n_varchar_image_or_string", nullable=true)
     */
    private $description;

    /**
     * @var ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="InfoSaleplanpreparation", mappedBy="saleplan", cascade={"persist", "remove"})
     */
    private $saleplanpreparationCollection;

    /**
     * InfoSaleplan constructor.
     */
    public function __construct()
    {
        $this->saleplanpreparationCollection = new ArrayCollection();
    }

    /**
     * @return int
     */
    public function getId(){
        return $this->id;
    }

    /**
     * Set guid
     *
     * @param string $guid
     *
     * @return $this
     */
    public function setGuid($guid)
    {
        $this->guid = $guid;

        return $this;
    }

    /**
     * Get guid
     *
     * @return string
     */
    public function getGuid()
    {
        return $this->guid;
    }

    /**
     * Set currenttime
     *
     * @param \DateTime $currenttime
     *
     * @return $this
     */
    public function setCurrenttime(\DateTime $currenttime)
    {
        $this->currenttime = $currenttime;

        return $this;
    }

    /**
     * Get currenttime
     *
     * @return \DateTime
     */
    public function getCurrenttime()
    {
        return $this->currenttime;
    }

    /**
     * Set moduser
     *
     * @param string $moduser
     *
     * @return $this
     */
    public function setModuser($moduser)
    {
        $this->moduser = $moduser;

        return $this;
    }

    /**
     * Get moduser
     *
     * @return string
     */
    public function getModuser()
    {
        return $this->moduser;
    }

    /**
     * @param InfoSaleperiod $saleperiod
     * @return $this
     */
    public function setSaleperiod(InfoSaleperiod $saleperiod){
        $this->saleperiod = $saleperiod;
        return $this;
    }

    /**
     * @return InfoSaleperiod
     */
    public function getSaleperiod(){
        return $this->saleperiod;
    }

    /**
     * @param InfoUser $user
     * @return $this
     */
    public function setUser(InfoUser $user){
        $this->user = $user;
        return $this;
    }

    /**
     * @return InfoUser
     */
    public function getUser(){
        return $this->user;
    }

    /**
     * @param $description
     * @return $this
     */
    public function setDescription($description){
        $this->description = $description;
        return $this;
    }

    /**
     * @return string
     */
    public function getDescription(){
        return $this->description;
    }

    /**
     * @return ArrayCollection
     */
    public function getSaleplanpreparationCollection(){
        return $this->saleplanpreparationCollection;
    }

    /**
     * @param InfoSaleplanpreparation $preparation
     * @return $this
     */
    public function addSaleplanpreparationCollection(InfoSaleplanpreparation $preparation){
        $preparation->setPlan($this);
        $this->saleplanpreparationCollection->add($preparation);
        return $this;
    }

    /**
     * @param InfoSaleplanpreparation $preparation
     * @return $this
     */
    public function removeSaleplanpreparationCollection(InfoSaleplanpreparation $preparation){
        $preparation->setPlan(null);
        $this->saleplanpreparationCollection->removeElement($preparation);
        return $this;
    }

    /**
     * @return $this
     */
    public function clearSaleplanpreparationCollection(){
        $this->saleplanpreparationCollection->clear();
        return $this;
    }
}