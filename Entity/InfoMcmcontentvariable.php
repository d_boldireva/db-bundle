<?php

namespace TeamSoft\CrmRepositoryBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * InfoMcmcontentvariable
 *
 * @ORM\Table(name="info_mcmcontentvariable")
 * @ORM\Entity
 * @UniqueEntity("code")
 */
class InfoMcmcontentvariable implements ServiceFieldInterface
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255, nullable=true)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="code", type="string", length=255, unique=true)
     */
    private $code;

    /**
     * @var string
     *
     * @ORM\Column(name="isdefault", type="integer", length=1, nullable=true)
     */
    private $isdefault;

    /**
     * @var string
     *
     * @ORM\Column(name="forUrl", type="integer", length=1, nullable=true)
     */
    private $forUrl;

    /**
     * @var string
     *
     * @ORM\Column(name="textForUrl", type="string", length=255, nullable=true)
     */
    private $textForUrl;

    /**
     * @var string
     *
     * @ORM\Column(name="defaultValue", type="string", length=255, nullable=true)
     */
    private $defaultValue;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="currenttime", type="datetime", nullable=true)
     */
    private $currenttime;

    /**
     * @var string
     *
     * @ORM\Column(name="moduser", type="string", length=16, nullable=true)
     */
    private $moduser;

    /**
     * @var string
     *
     * @ORM\Column(name="guid", type="guid", nullable=true)
     */
    private $guid;

    /**
     * @var InfoLanguage
     *
     * @ORM\ManyToOne(targetEntity="InfoLanguage")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="language_id", referencedColumnName="id")
     * })
     */
    private $language;

    /**
     * @var string
     *
     * @ORM\Column(name="male_value", type="string", length=255, nullable=true)
     */
    private $maleValue;

    /**
     * @var string
     *
     * @ORM\Column(name="female_value", type="string", length=255, nullable=true)
     */
    private $femaleValue;

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return InfoMcmcontentvariable
     */
    public function setName($name)
    {
        $this->name = $name;
    
        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set code
     *
     * @param string $code
     *
     * @return InfoMcmcontentvariable
     */
    public function setCode($code)
    {
        $this->code = $code;
    
        return $this;
    }

    /**
     * Get code
     *
     * @return string
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * Set defaultvalue
     *
     * @param string $defaultvalue
     *
     * @return InfoMcmcontentvariable
     */
    public function setDefaultValue($defaultValue)
    {
        $this->defaultValue = $defaultValue;
    
        return $this;
    }

    /**
     * Get defaultvalue
     *
     * @return string
     */
    public function getDefaultValue()
    {
        return $this->defaultValue;
    }

    /**
     * Get isdefault
     *
     * @return integer
     */
    public function getIsdefault()
    {
        return $this->isdefault;
    }

    /**
     * Get forUrl
     *
     * @return integer
     */
    public function getForUrl()
    {
        return $this->forUrl;
    }

    /**
     * Get textForUrl
     *
     * @return string
     */
    public function getTextForUrl()
    {
        return $this->textForUrl;
    }

    /**
     * Set currenttime
     *
     * @param \DateTime $currenttime
     *
     * @return InfoMcmcontentvariable
     */
    public function setCurrenttime(\DateTime $currenttime)
    {
        $this->currenttime = $currenttime;
    
        return $this;
    }

    /**
     * Get currenttime
     *
     * @return \DateTime
     */
    public function getCurrenttime()
    {
        return $this->currenttime;
    }

    /**
     * Set moduser
     *
     * @param string $moduser
     *
     * @return InfoMcmcontentvariable
     */
    public function setModuser($moduser)
    {
        $this->moduser = $moduser;
    
        return $this;
    }

    /**
     * Get moduser
     *
     * @return string
     */
    public function getModuser()
    {
        return $this->moduser;
    }

    /**
     * Set guid
     *
     * @param string $guid
     *
     * @return InfoMcmcontentvariable
     */
    public function setGuid($guid)
    {
        $this->guid = $guid;
    
        return $this;
    }

    /**
     * Get guid
     *
     * @return string
     */
    public function getGuid()
    {
        return $this->guid;
    }

    /**
     * Set language
     *
     * @param InfoLanguage $language
     * @return InfoMcmcontentvariable
     */
    public function setLanguage(InfoLanguage $language = null)
    {
        $this->language = $language;

        return $this;
    }

    /**
     * Get language
     *
     * @return InfoLanguage
     */
    public function getLanguage()
    {
        return $this->language;
    }

    /**
     * Set malevalue
     *
     * @param string $maleValue
     *
     * @return InfoMcmcontentvariable
     */
    public function setMaleValue($maleValue)
    {
        $this->maleValue = $maleValue;

        return $this;
    }

    /**
     * Get malevalue
     *
     * @return string
     */
    public function getMaleValue()
    {
        return $this->maleValue;
    }

    /**
     * Set femalevalue
     *
     * @param string $femaleValue
     *
     * @return InfoMcmcontentvariable
     */
    public function setFemaleValue($femaleValue)
    {
        $this->femaleValue = $femaleValue;

        return $this;
    }

    /**
     * Get femalevalue
     *
     * @return string
     */
    public function getFemaleValue()
    {
        return $this->femaleValue;
    }
}
