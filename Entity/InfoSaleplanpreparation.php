<?php
namespace TeamSoft\CrmRepositoryBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * InfoSaleplanpreparation
 *
 * @ORM\Table(name="info_saleplanpreparation")
 * @ORM\Entity
 */
class InfoSaleplanpreparation implements ServiceFieldInterface
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="guid", type="guid", nullable=true)
     */
    private $guid;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="currenttime", type="datetime", nullable=true)
     */
    private $currenttime;

    /**
     * @var string
     *
     * @ORM\Column(name="moduser", type="string", length=16, nullable=true)
     */
    private $moduser;

    /**
     * @var InfoSaleplan
     *
     * @ORM\ManyToOne(targetEntity="InfoSaleplan", inversedBy="saleplanpreparationCollection")
     * @ORM\JoinColumn(name="saleplan_id", referencedColumnName="id")
     */
    private $saleplan;

    /**
     * @var InfoPreparation
     *
     * @ORM\ManyToOne(targetEntity="InfoPreparation")
     * @ORM\JoinColumn(name="preparation_id", referencedColumnName="id")
     */
    private $preparation;

    /**
     * @var int
     *
     * @ORM\Column(name="count", type="integer", nullable=true)
     */
    private $count;

    /**
     * @var float
     *
     * @ORM\Column(name="amount", type="float", nullable=true)
     */
    private $amount;

    /**
     * @var int
     *
     * @ORM\Column(name="countcurrent", type="integer", nullable=true)
     */
    private $countcurrent;

    /**
     * @var InfoPreparationbrend
     *
     * @ORM\Column(name="InfoPreparationbrend")
     * @ORM\JoinColumn(name="brend_id", referencedColumnName="id")
     */
    private $brend;



    /**
     * @return int
     */
    public function getId(){
        return $this->id;
    }

    /**
     * Set guid
     *
     * @param string $guid
     *
     * @return $this
     */
    public function setGuid($guid)
    {
        $this->guid = $guid;

        return $this;
    }

    /**
     * Get guid
     *
     * @return string
     */
    public function getGuid()
    {
        return $this->guid;
    }

    /**
     * Set currenttime
     *
     * @param \DateTime $currenttime
     *
     * @return $this
     */
    public function setCurrenttime(\DateTime $currenttime)
    {
        $this->currenttime = $currenttime;

        return $this;
    }

    /**
     * Get currenttime
     *
     * @return \DateTime
     */
    public function getCurrenttime()
    {
        return $this->currenttime;
    }

    /**
     * Set moduser
     *
     * @param string $moduser
     *
     * @return $this
     */
    public function setModuser($moduser)
    {
        $this->moduser = $moduser;

        return $this;
    }

    /**
     * Get moduser
     *
     * @return string
     */
    public function getModuser()
    {
        return $this->moduser;
    }

    /**
     * @param InfoSaleplan $saleplan
     * @return $this
     */
    public function setSaleplan(InfoSaleplan $saleplan){
        $this->saleplan = $saleplan;
        return $this;
    }

    /**
     * @return InfoSaleplan
     */
    public function getSaleplan(){
        return $this->saleplan;
    }

    /**
     * @param InfoPreparation $preparation
     * @return $this
     */
    public function setPreparation(InfoPreparation $preparation){
        $this->preparation = $preparation;
        return $this;
    }

    /**
     * @return InfoPreparation
     */
    public function getPreparation(){
        return $this->preparation;
    }

    /**
     * @param int $count
     * @return $this
     */
    public function setCount($count){
        $this->count = $count;
        return $this;
    }

    /**
     * @return int
     */
    public function getCount(){
        return $this->count;
    }

    /**
     * @param float $amount
     * @return $this
     */
    public function setAmount($amount){
        $this->amount = $amount;
        return $this;
    }

    /**
     * @return float
     */
    public function getAmount(){
        return $this->amount;
    }

    /**
     * @param int $count
     * @return $this
     */
    public function setCountCurrent($count){
        $this->countcurrent = $count;
        return $this;
    }

    /**
     * @return int
     */
    public function getCountCurrent(){
        return $this->countcurrent;
    }

    /**
     * @param InfoPreparationbrend $brend
     * @return $this
     */
    public function setBrend(InfoPreparationbrend $brend){
        $this->brend = $brend;
        return $this;
    }

    /**
     * @return InfoPreparationbrend
     */
    public function getBrend(){
        return $this->brend;
    }
}