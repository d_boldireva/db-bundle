<?php

namespace TeamSoft\CrmRepositoryBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * InfoCompanytype
 *
 * @ORM\Table(name="info_companytype", indexes={@ORM\Index(name="ix_info_companytype_guid", columns={"guid"})})
 * @ORM\Entity(repositoryClass="TeamSoft\CrmRepositoryBundle\Repository\Company\InfoCompanyTypeRepository")
 */
class InfoCompanytype implements ServiceFieldInterface
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="Name", type="string", length=255, nullable=true)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="NameUkr", type="string", length=255, nullable=true)
     */
    private $nameukr;

    /**
     * @var integer
     *
     * @ORM\Column(name="IsShop", type="integer", nullable=true)
     */
    private $isshop;

    /**
     * @var string
     *
     * @ORM\Column(name="Description", type="text", length=16, nullable=true)
     */
    private $description;

    /**
     * @var string
     *
     * @ORM\Column(name="geodataenable", type="integer", nullable=true)
     */
    private $geodataenable;


    /**
     * @var string
     *
     * @ORM\Column(name="guid", type="guid", nullable=true)
     */
    private $guid;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="currenttime", type="datetime", nullable=true)
     */
    private $currenttime;

    /**
     * @var string
     *
     * @ORM\Column(name="moduser", type="string", length=16, nullable=true)
     */
    private $moduser;

    /**
     * @var integer
     *
     * @ORM\Column(name="morionid", type="integer", nullable=true)
     */
    private $morionid;

    /**
     * @var string
     *
     * @ORM\Column(name="code", type="string", length=255, nullable=true)
     */
    private $code;

    /**
     * @ORM\ManyToMany(targetEntity="InfoCompanycategory", inversedBy="companytypes")
     * @ORM\JoinTable(
     *   name="info_companycategorytype",
     *   joinColumns={@ORM\JoinColumn(name="companytype_id", referencedColumnName="id")},
     *   inverseJoinColumns={@ORM\JoinColumn(name="companycategory_id", referencedColumnName="id")}
     * )
     */
    private $companycategories;

    /**
     * @var string
     *
     * @ORM\Column(name="svg", type="string", length=255, nullable=true)
     */
    private $svg;

    /**
     * @var InfoLanguage
     *
     * @ORM\ManyToOne(targetEntity="InfoLanguage")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="language_id", referencedColumnName="id")
     * })
     */
    private $language;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->companycategories = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return InfoCompanytype
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set nameukr
     *
     * @param string $nameukr
     * @return InfoCompanytype
     */
    public function setNameukr($nameukr)
    {
        $this->nameukr = $nameukr;

        return $this;
    }

    /**
     * Get nameukr
     *
     * @return string
     */
    public function getNameukr()
    {
        return $this->nameukr;
    }

    /**
     * Set isshop
     *
     * @param integer $isshop
     * @return InfoCompanytype
     */
    public function setIsshop($isshop)
    {
        $this->isshop = $isshop;

        return $this;
    }

    /**
     * Get isshop
     *
     * @return integer
     */
    public function getIsshop()
    {
        return $this->isshop;
    }

    /**
     * Set description
     *
     * @param string $description
     * @return InfoCompanytype
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set guid
     *
     * @param string $guid
     * @return InfoCompanytype
     */
    public function setGuid($guid)
    {
        $this->guid = $guid;

        return $this;
    }

    /**
     * Get guid
     *
     * @return string
     */
    public function getGuid()
    {
        return $this->guid;
    }

    /**
     * Set currenttime
     *
     * @param \DateTime $currenttime
     * @return InfoCompanytype
     */
    public function setCurrenttime(\DateTime $currenttime)
    {
        $this->currenttime = $currenttime;

        return $this;
    }

    /**
     * Get currenttime
     *
     * @return \DateTime
     */
    public function getCurrenttime()
    {
        return $this->currenttime;
    }

    /**
     * Set moduser
     *
     * @param string $moduser
     * @return InfoCompanytype
     */
    public function setModuser($moduser)
    {
        $this->moduser = $moduser;

        return $this;
    }

    /**
     * Get moduser
     *
     * @return string
     */
    public function getModuser()
    {
        return $this->moduser;
    }

    /**
     * Set morionid
     *
     * @param integer $morionid
     * @return InfoCompanytype
     */
    public function setMorionid($morionid)
    {
        $this->morionid = $morionid;

        return $this;
    }

    /**
     * Get morionid
     *
     * @return integer
     */
    public function getMorionid()
    {
        return $this->morionid;
    }

    /**
     * Add companycategory
     *
     * @param InfoCompanycategory $companycategory
     *
     * @return InfoCompanytype
     */
    public function addCompanycategory(InfoCompanycategory $companycategory)
    {
        $this->companycategories[] = $companycategory;

        return $this;
    }

    /**
     * Remove companycategory
     *
     * @param InfoCompanycategory $companycategory
     */
    public function removeCompanycategory(InfoCompanycategory $companycategory)
    {
        $this->companycategories->removeElement($companycategory);
    }

    /**
     * Get companycategories
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getCompanycategories()
    {
        return $this->companycategories;
    }

    /**
     * Set geodataenable.
     *
     * @param int|null $geodataenable
     *
     * @return InfoCompanytype
     */
    public function setGeodataenable($geodataenable = null)
    {
        $this->geodataenable = $geodataenable;

        return $this;
    }

    /**
     * Get geodataenable.
     *
     * @return int|null
     */
    public function getGeodataenable()
    {
        return $this->geodataenable;
    }

    /**
     * Set code.
     *
     * @param string|null $code
     *
     * @return InfoCompanytype
     */
    public function setCode($code = null)
    {
        $this->code = $code;

        return $this;
    }

    /**
     * Get code.
     *
     * @return string|null
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * @param string|null $svg
     * @return $this
     */
    public function setSvg(?string $svg)
    {
        $this->svg = $svg;
        return $this;
    }

    /**
     * @return string
     */
    public function getSvg()
    {
        return $this->svg;
    }

    /**
     * Set language
     *
     * @param InfoLanguage|null $content
     * @return InfoCompanytype
     */
    public function setLanguage(InfoLanguage $language = null)
    {
        $this->language = $language;

        return $this;
    }

    /**
     * Get language
     *
     * @return InfoLanguage
     */
    public function getLanguage()
    {
        return $this->language;
    }
}
