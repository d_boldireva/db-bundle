<?php

namespace TeamSoft\CrmRepositoryBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * InfoMcmfilter
 *
 * @ORM\Table(name="info_mcmfilter")
 * @ORM\Entity(repositoryClass="TeamSoft\CrmRepositoryBundle\Repository\InfoMcmfilter")
 */
class InfoMcmfilter implements ServiceFieldInterface
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255, nullable=true)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="name_ru", type="string", length=255, nullable=true)
     */
    private $nameRu;

    /**
     * @var string
     *
     * @ORM\Column(name="type", type="string", length=50, nullable=true)
     */
    private $type;

    /**
     * @var string
     *
     * @ORM\Column(name="filter", type="string", length=255, nullable=true)
     */
    private $filter;

    /**
     * @var InfoCustomdictionary
     *
     * @ORM\ManyToOne(targetEntity="\TeamSoft\CrmRepositoryBundle\Entity\InfoCustomdictionary")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="customdictionary_id", referencedColumnName="id")
     * })
     */
    private $customdictionary;

    /**
     * @var InfoSystemdictionary
     *
     * @ORM\ManyToOne(targetEntity="\TeamSoft\CrmRepositoryBundle\Entity\InfoSystemdictionary")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="systemdictionary_id", referencedColumnName="id")
     * })
     */
    private $systemdictionary;

    /**
     * @var string
     *
     * @ORM\Column(name="customWhere", type="string", length=4000, nullable=true)
     */
    private $customwhere;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="currenttime", type="datetime", nullable=true)
     */
    private $currenttime;

    /**
     * @var string
     *
     * @ORM\Column(name="moduser", type="string", length=16, nullable=true)
     */
    private $moduser;

    /**
     * @var string
     *
     * @ORM\Column(name="guid", type="guid", nullable=true)
     */
    private $guid;

    /**
     * @var ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="InfoMcmfilterrelation", mappedBy="child", cascade={"persist"})
     */
    private $parents;

    /**
     * @var ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="InfoMcmfilterrelation", mappedBy="subj", cascade={"persist"})
     */
    private $children;

    /**
     * @var string
     *
     * @ORM\Column(name="subtitle", type="string", nullable=true)
     */
    private $subtitle;

    /**
     * @var string
     *
     * @ORM\Column(name="additional_dict", type="string", nullable=true)
     */
    private $additionalDict;

    /**
     * @var string
     *
     * @ORM\Column(name="depend_field", type="string", nullable=true)
     */
    private $dependField;


    /**
     * Constructor
     */
    public function __construct()
    {
        $this->parents = new ArrayCollection();
        $this->children = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setNameRu(string $nameRu): self
    {
        $this->nameRu = $nameRu;

        return $this;
    }

    public function getNameRu(): ?string
    {
        return $this->nameRu;
    }

    public function setType(string $type): self
    {
        $this->type = $type;

        return $this;
    }

    public function getType(): ?string
    {
        return $this->type;
    }

    public function setFilter(string $filter): self
    {
        $this->filter = $filter;

        return $this;
    }

    public function getFilter(): ?string
    {
        return $this->filter;
    }

    public function setCustomwhere(string $customWhere): self
    {
        $this->customwhere = $customWhere;

        return $this;
    }

    public function getCustomwhere(): ?string
    {
        return $this->customwhere;
    }

    public function setCustomDictionary(InfoCustomdictionary $customdictionary): self
    {
        $this->customdictionary = $customdictionary;

        return $this;
    }

    public function getCustomDictionary(): ?InfoCustomdictionary
    {
        return $this->customdictionary;
    }

    public function setSystemdictionary(InfoSystemdictionary $systemdictionary): self
    {
        $this->systemdictionary = $systemdictionary;

        return $this;
    }

    public function getSystemdictionary(): ?InfoSystemdictionary
    {
        return $this->systemdictionary;
    }

    public function setCurrenttime(\DateTime $currenttime): self
    {
        $this->currenttime = $currenttime;

        return $this;
    }

    public function getCurrenttime(): ?\DateTime
    {
        return $this->currenttime;
    }

    public function setModuser($moduser): self
    {
        $this->moduser = $moduser;

        return $this;
    }

    public function getModuser(): ?string
    {
        return $this->moduser;
    }

    public function setGuid($guid): self
    {
        $this->guid = $guid;

        return $this;
    }

    public function getGuid(): ?string
    {
        return $this->guid;
    }

    /**
     * Get parents
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getParents()
    {
        return $this->parents;
    }

    public function addParent(InfoMcmfilterrelation $parent): self
    {
        if (!$this->parents->contains($parent)) {
            $this->parents->add($parent);
        }
        return $this;
    }

    public function removeParent(InfoMcmfilterrelation $parent): self
    {
        if ($this->parents->contains($parent)) {
            $this->parents->removeElement($parent);
        }
        return $this;
    }


    /**
     * Get children
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getChildren()
    {
        return $this->children;
    }

    public function addChild(InfoMcmfilterrelation $child): self
    {
        if (!$this->children->contains($child)) {
            $this->children->add($child);
        }
        return $this;
    }

    public function removeChild(InfoMcmfilterrelation $children): self
    {
        if ($this->children->contains($children)) {
            $this->children->removeElement($children);
        }
        return $this;
    }

    public function setSubtitle(string $subtitle): self
    {
        $this->subtitle = $subtitle;

        return $this;
    }

    public function getSubtitle(): ?string
    {
        return $this->subtitle;
    }

    public function setAdditionalDict(string $additionalDict): self
    {
        $this->additionalDict = $additionalDict;

        return $this;
    }

    public function getAdditionalDict(): ?string
    {
        return $this->additionalDict;
    }

    public function setDependField(string $dependField): self
    {
        $this->dependField = $dependField;

        return $this;
    }

    public function getDependField(): ?string
    {
        return $this->dependField;
    }
}
