<?php

namespace TeamSoft\CrmRepositoryBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * InfoCompanypreparation2
 *
 * @ORM\Table(name="info_companypreparation2")
 * @ORM\Entity(repositoryClass="TeamSoft\CrmRepositoryBundle\Repository\InfoCompanypreparation2")
 */
class InfoCompanypreparation2 implements ServiceFieldInterface
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @deprecated use instead task.fromdate
     * @var \DateTime
     *
     * @ORM\Column(name="Date", type="datetime", nullable=true)
     */
    private $date;

    /**
     * @var int
     *
     * @ORM\Column(name="flag1", type="integer", nullable=true)
     */
    private $flag1;

    /**
     * @var int
     *
     * @ORM\Column(name="flag2", type="integer", nullable=true)
     */
    private $flag2;

    /**
     * @var int
     *
     * @ORM\Column(name="flag3", type="integer", nullable=true)
     */
    private $flag3;

    /**
     * @var int
     *
     * @ORM\Column(name="flag4", type="integer", nullable=true)
     */
    private $flag4;

    /**
     * @var int
     *
     * @ORM\Column(name="flag5", type="integer", nullable=true)
     */
    private $flag5;

    /**
     * @var int
     *
     * @ORM\Column(name="int1", type="integer", nullable=true)
     */
    private $int1;

    /**
     * @var int
     *
     * @ORM\Column(name="int2", type="integer", nullable=true)
     */
    private $int2;

    /**
     * @var int
     *
     * @ORM\Column(name="int3", type="integer", nullable=true)
     */
    private $int3;

    /**
     * @var string
     *
     * @ORM\Column(name="str1", type="string", length=255, nullable=true)
     */
    private $str1;

    /**
     * @var string
     *
     * @ORM\Column(name="str2", type="string", length=255, nullable=true)
     */
    private $str2;

    /**
     * @var string
     *
     * @ORM\Column(name="str3", type="string", length=255, nullable=true)
     */
    private $str3;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date1", type="datetime", nullable=true)
     */
    private $date1;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date2", type="datetime", nullable=true)
     */
    private $date2;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date3", type="datetime", nullable=true)
     */
    private $date3;

    /**
     * @var int
     *
     * @ORM\Column(name="float1", type="decimal", precision=7, scale=2)
     */
    private $float1;

    /**
     * @var int
     *
     * @ORM\Column(name="float2", type="decimal", precision=7, scale=2)
     */
    private $float2;

    /**
     * @var int
     *
     * @ORM\Column(name="float3", type="decimal", precision=7, scale=2)
     */
    private $float3;

    /**
     * @var guid
     *
     * @ORM\Column(name="guid", type="guid", nullable=true)
     */
    private $guid;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="currenttime", type="datetime", nullable=true)
     */
    private $currenttime;

    /**
     * @var string
     *
     * @ORM\Column(name="moduser", type="string", length=16, nullable=true)
     */
    private $moduser;

    /**
     * @var InfoTask
     *
     * @ORM\ManyToOne(targetEntity="InfoTask", inversedBy="companyPreparation2Collection")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="task_id", referencedColumnName="id")
     * })
     */
    private $task;

    /**
     * @deprecated use instead task.company
     * @var InfoCompany
     *
     * @ORM\ManyToOne(targetEntity="InfoCompany")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="Company_id", referencedColumnName="id")
     * })
     */
    private $company;

    /**
     * @var InfoPreparation
     *
     * @ORM\ManyToOne(targetEntity="InfoPreparation", inversedBy="companyPreparations2")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="Preparation_id", referencedColumnName="id")
     * })
     */
    private $preparation;

    /**
     * @var \InfoRivalpreparation
     *
     * @ORM\ManyToOne(targetEntity="InfoRivalpreparation")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="rivalprep_id", referencedColumnName="id")
     * })
     */
    private $rivalprep;



    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set date
     *
     * @deprecated
     *
     * @param \DateTime $date
     *
     * @return InfoCompanypreparation2
     */
    public function setDate($date)
    {
        $this->date = $date;
    
        return $this;
    }

    /**
     * Get date
     * @deprecated use instead $this->getTask()->getFromdate()
     *
     * @return \DateTime
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * Set flag1
     *
     * @param int $flag1
     *
     * @return InfoCompanypreparation2
     */
    public function setFlag1($flag1)
    {
        $this->flag1 = $flag1;
    
        return $this;
    }

    /**
     * Get flag1
     *
     * @return int
     */
    public function getFlag1()
    {
        return $this->flag1;
    }

    /**
     * Set flag2
     *
     * @param int $flag2
     *
     * @return InfoCompanypreparation2
     */
    public function setFlag2($flag2)
    {
        $this->flag2 = $flag2;
    
        return $this;
    }

    /**
     * Get flag2
     *
     * @return int
     */
    public function getFlag2()
    {
        return $this->flag2;
    }

    /**
     * Set flag3
     *
     * @param int $flag3
     *
     * @return InfoCompanypreparation2
     */
    public function setFlag3($flag3)
    {
        $this->flag3 = $flag3;
    
        return $this;
    }

    /**
     * Get flag3
     *
     * @return int
     */
    public function getFlag3()
    {
        return $this->flag3;
    }

    /**
     * Set flag4
     *
     * @param int $flag4
     *
     * @return InfoCompanypreparation2
     */
    public function setFlag4($flag4)
    {
        $this->flag4 = $flag4;
    
        return $this;
    }

    /**
     * Get flag4
     *
     * @return int
     */
    public function getFlag4()
    {
        return $this->flag4;
    }

    /**
     * Set flag5
     *
     * @param int $flag5
     *
     * @return InfoCompanypreparation2
     */
    public function setFlag5($flag5)
    {
        $this->flag5 = $flag5;
    
        return $this;
    }

    /**
     * Get flag5
     *
     * @return int
     */
    public function getFlag5()
    {
        return $this->flag5;
    }

    /**
     * Set int1
     *
     * @param int $int1
     *
     * @return InfoCompanypreparation2
     */
    public function setInt1($int1)
    {
        $this->int1 = $int1;
    
        return $this;
    }

    /**
     * Get int1
     *
     * @return int
     */
    public function getInt1()
    {
        return $this->int1;
    }

    /**
     * Set int2
     *
     * @param int $int2
     *
     * @return InfoCompanypreparation2
     */
    public function setInt2($int2)
    {
        $this->int2 = $int2;
    
        return $this;
    }

    /**
     * Get int2
     *
     * @return int
     */
    public function getInt2()
    {
        return $this->int2;
    }

    /**
     * Set int3
     *
     * @param int $int3
     *
     * @return InfoCompanypreparation2
     */
    public function setInt3($int3)
    {
        $this->int3 = $int3;
    
        return $this;
    }

    /**
     * Get int3
     *
     * @return int
     */
    public function getInt3()
    {
        return $this->int3;
    }

    /**
     * Set str1
     *
     * @param string $str1
     *
     * @return InfoCompanypreparation2
     */
    public function setStr1($str1)
    {
        $this->str1 = $str1;
    
        return $this;
    }

    /**
     * Get str1
     *
     * @return string
     */
    public function getStr1()
    {
        return $this->str1;
    }

    /**
     * Set str2
     *
     * @param string $str2
     *
     * @return InfoCompanypreparation2
     */
    public function setStr2($str2)
    {
        $this->str2 = $str2;
    
        return $this;
    }

    /**
     * Get str2
     *
     * @return string
     */
    public function getStr2()
    {
        return $this->str2;
    }

    /**
     * Set str3
     *
     * @param string $str3
     *
     * @return InfoCompanypreparation2
     */
    public function setStr3($str3)
    {
        $this->str3 = $str3;
    
        return $this;
    }

    /**
     * Get str3
     *
     * @return string
     */
    public function getStr3()
    {
        return $this->str3;
    }

    /**
     * Set date1
     *
     * @param \DateTime $date1
     *
     * @return InfoCompanypreparation2
     */
    public function setDate1($date1)
    {
        $this->date1 = $date1;
    
        return $this;
    }

    /**
     * Get date1
     *
     * @return \DateTime
     */
    public function getDate1()
    {
        return $this->date1;
    }

    /**
     * Set date2
     *
     * @param \DateTime $date2
     *
     * @return InfoCompanypreparation2
     */
    public function setDate2($date2)
    {
        $this->date2 = $date2;
    
        return $this;
    }

    /**
     * Get date2
     *
     * @return \DateTime
     */
    public function getDate2()
    {
        return $this->date2;
    }

    /**
     * Set date3
     *
     * @param \DateTime $date3
     *
     * @return InfoCompanypreparation2
     */
    public function setDate3($date3)
    {
        $this->date3 = $date3;
    
        return $this;
    }

    /**
     * Get date3
     *
     * @return \DateTime
     */
    public function getDate3()
    {
        return $this->date3;
    }

    /**
     * Set float1
     *
     * @param int $float1
     *
     * @return InfoCompanypreparation2
     */
    public function setFloat1($float1)
    {
        $this->float1 = $float1;
    
        return $this;
    }

    /**
     * Get float1
     *
     * @return int
     */
    public function getFloat1()
    {
        return $this->float1;
    }

    /**
     * Set float2
     *
     * @param int $float2
     *
     * @return InfoCompanypreparation2
     */
    public function setFloat2($float2)
    {
        $this->float2 = $float2;
    
        return $this;
    }

    /**
     * Get float2
     *
     * @return int
     */
    public function getFloat2()
    {
        return $this->float2;
    }

    /**
     * Set float3
     *
     * @param int $float3
     *
     * @return InfoCompanypreparation2
     */
    public function setFloat3($float3)
    {
        $this->float3 = $float3;
    
        return $this;
    }

    /**
     * Get float3
     *
     * @return int
     */
    public function getFloat3()
    {
        return $this->float3;
    }

    /**
     * Set guid
     *
     * @param guid $guid
     *
     * @return InfoCompanypreparation2
     */
    public function setGuid($guid)
    {
        $this->guid = $guid;
    
        return $this;
    }

    /**
     * Get guid
     *
     * @return guid
     */
    public function getGuid()
    {
        return $this->guid;
    }

    /**
     * Set currenttime
     *
     * @param \DateTime $currenttime
     *
     * @return InfoCompanypreparation2
     */
    public function setCurrenttime(\DateTime $currenttime)
    {
        $this->currenttime = $currenttime;
    
        return $this;
    }

    /**
     * Get currenttime
     *
     * @return \DateTime
     */
    public function getCurrenttime()
    {
        return $this->currenttime;
    }

    /**
     * Set moduser
     *
     * @param string $moduser
     *
     * @return InfoCompanypreparation2
     */
    public function setModuser($moduser)
    {
        $this->moduser = $moduser;
    
        return $this;
    }

    /**
     * Get moduser
     *
     * @return string
     */
    public function getModuser()
    {
        return $this->moduser;
    }

    /**
     * Set task
     *
     * @param \TeamSoft\CrmRepositoryBundle\Entity\InfoTask $task
     *
     * @return InfoCompanypreparation2
     */
    public function setTask(\TeamSoft\CrmRepositoryBundle\Entity\InfoTask $task = null)
    {
        $this->task = $task;
    
        return $this;
    }

    /**
     * Get task
     *
     * @return \TeamSoft\CrmRepositoryBundle\Entity\InfoTask
     */
    public function getTask()
    {
        return $this->task;
    }

    /**
     * Set company
     * @deprecated see $this->getTask()->getCompany()
     *
     * @param \TeamSoft\CrmRepositoryBundle\Entity\InfoCompany $company
     *
     * @return InfoCompanypreparation2
     */
    public function setCompany(\TeamSoft\CrmRepositoryBundle\Entity\InfoCompany $company = null)
    {
        $this->company = $company;
    
        return $this;
    }

    /**
     * Get company
     * @deprecated see $this->getTask()->getCompany()
     *
     * @return \TeamSoft\CrmRepositoryBundle\Entity\InfoCompany
     */
    public function getCompany()
    {
        return $this->company;
    }

    /**
     * Set preparation
     *
     * @param \TeamSoft\CrmRepositoryBundle\Entity\InfoPreparation $preparation
     *
     * @return InfoCompanypreparation2
     */
    public function setPreparation(\TeamSoft\CrmRepositoryBundle\Entity\InfoPreparation $preparation = null)
    {
        $this->preparation = $preparation;
    
        return $this;
    }

    /**
     * Get preparation
     *
     * @return \TeamSoft\CrmRepositoryBundle\Entity\InfoPreparation
     */
    public function getPreparation()
    {
        return $this->preparation;
    }

    /**
     * Set rivalprep
     *
     * @param \TeamSoft\CrmRepositoryBundle\Entity\InfoRivalpreparation $rivalprep
     *
     * @return InfoCompanypreparation2
     */
    public function setRivalprep(\TeamSoft\CrmRepositoryBundle\Entity\InfoRivalpreparation $rivalprep = null)
    {
        $this->rivalprep = $rivalprep;
    
        return $this;
    }

    /**
     * Get rivalprep
     *
     * @return \TeamSoft\CrmRepositoryBundle\Entity\InfoRivalpreparation
     */
    public function getRivalprep()
    {
        return $this->rivalprep;
    }

    public function getTaskId () {
        return $this->getTask() ? $this->getTask()->getId() : null;
    }

    public function getCompanyId () {
        return $this->getCompany() ? $this->getCompany()->getId() : null;
    }

    public function getPreparationId () {
        return $this->getPreparation() ? $this->getPreparation()->getId() : null;
    }
}
