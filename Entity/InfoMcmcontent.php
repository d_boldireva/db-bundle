<?php

namespace TeamSoft\CrmRepositoryBundle\Entity;

use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use phpDocumentor\Reflection\Types\Integer;

/**
 * InfoMcmcontent
 *
 * @ORM\Table(name="info_mcmcontent")
 * @ORM\Entity(repositoryClass="TeamSoft\CrmRepositoryBundle\Repository\InfoMcmcontent")
 */
class InfoMcmcontent implements ServiceFieldInterface
{
    public const TYPE_SMS = 'sms';
    public const TYPE_VIBER = 'viber';
    public const TYPE_EMAIL = 'email';
    public const TYPE_WHATSAPP = 'whatsapp';
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="type", type="string", length=255, nullable=true)
     */
    private $type;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255, nullable=true)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="subject", type="string", length=255, nullable=true)
     */
    private $subject;

    /**
     * @var string
     *
     * @ORM\Column(name="caption", type="string", length=255, nullable=true)
     */
    private $caption;

    /**
     * @var string
     *
     * @ORM\Column(name="image", type="string", length=1024, nullable=true)
     */
    private $image;

    /**
     * @var string
     *
     * @ORM\Column(name="url", type="string", length=1024, nullable=true)
     */
    private $url;

    /**
     * @var string
     *
     * @ORM\Column(name="body", type="string", nullable=true)
     */
    private $body;

    /**
     * @var string
     *
     * @ORM\Column(name="json", type="string", nullable=true)
     */
    private $json;

    /**
     * @var string
     *
     * @ORM\Column(name="alternate_body", type="string", nullable=true)
     */
    private $alternateBody;

    /**
     * @var integer
     *
     * @ORM\Column(name="row_num", type="integer", nullable=false)
     */
    private $rownum;

    /**
     * @var boolean
     *
     * @ORM\Column(name="israw", type="integer", nullable=false)
     */
    private $isRaw;

    /**
     * @var integer
     *
     * @ORM\Column(name="isapproved", type="integer", nullable=false)
     */
    private $isapproved;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="approved_date", type="datetime", nullable=true)
     */
    private $approvedDate;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created", type="datetime", nullable=true)
     */
    private $created;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="modified", type="datetime", nullable=true)
     */
    private $modified;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="currenttime", type="datetime", nullable=true)
     */
    private $currenttime;

    /**
     * @var string
     *
     * @ORM\Column(name="moduser", type="string", nullable=true)
     */
    private $moduser;

    /**
     * @var string
     */
    private $guid;

    /**
     * @var int
     *
     * @ORM\Column(name="isarchive", type="integer", nullable=true)
     */
    private $isarchive;

    /**
     * @var InfoUser
     *
     * @ORM\ManyToOne(targetEntity="InfoUser")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     * })
     */
    private $user;

    /**
     * @var InfoUser
     *
     * @ORM\ManyToOne(targetEntity="InfoUser")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="approved_by", referencedColumnName="id")
     * })
     */
    private $approvedBy;

    /**
     * @var InfoMcmdistribution
     *
     * @ORM\OneToMany(targetEntity="InfoMcmdistribution", mappedBy="mcmcontent")
     */
    private $distributionCollection;

    /**
     * @ORM\ManyToMany(targetEntity="InfoMcmfile")
     * @ORM\JoinTable(name="info_mcmcontentfile",
     *     joinColumns={@ORM\JoinColumn(name="mcmcontent_id", referencedColumnName="id")},
     *     inverseJoinColumns={@ORM\JoinColumn(name="mcmfile_id", referencedColumnName="id")}
     * )
     */
    private $fileCollection;

    /**
     * @var InfoMcmcontentlanguage
     *
     * @ORM\OneToMany(targetEntity="InfoMcmcontentlanguage", mappedBy="content", cascade={"persist", "remove"})
     */
    private $contentCollection;

    /**
     * @ORM\ManyToMany(targetEntity="InfoMcmconference")
     * @ORM\JoinTable(name="info_mcmconferencecontent",
     *   joinColumns={@ORM\JoinColumn(name="content_id", referencedColumnName="id")},
     *   inverseJoinColumns={@ORM\JoinColumn(name="conference_id", referencedColumnName="id")}
     * )
     */
    private $conferences;

    /**
     * @var InfoMcmfile|null
     *
     * @ORM\OneToOne(targetEntity="InfoMcmfile", cascade={"persist"})
     * @ORM\JoinColumn(name="certificate_id", referencedColumnName="id")
     */
    private $certificate;

    /**
     * @var string
     *
     * @ORM\Column(name="header_type", type="string", length=255, nullable=true)
     */
    private $headerType;

    /**
     * @var string
     *
     * @ORM\Column(name="header_body", type="string", length=255, nullable=true)
     */
    private $headerBody;

    /**
     * @var InfoMcmcontentbutton
     *
     * @ORM\OneToMany(targetEntity="InfoMcmcontentbutton", mappedBy="content", cascade={"persist"}, orphanRemoval=true)
     */
    private $buttons;

    /**
     * @var InfoMcmcontent|null
     *
     * @ORM\ManyToOne(targetEntity="InfoMcmcontent", inversedBy="children")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="parent_id", referencedColumnName="id")
     * })
     */
    private $parent;

    /**
     * @var int
     *
     * @ORM\Column(name="is_buttons_action", type="integer", nullable=true)
     */
    private $isButtonsAction;

    /**
     * @var int
     *
     * @ORM\Column(name="is_buttons_text", type="integer", nullable=true)
     */
    private $isButtonsText;

    /**
     * @var InfoCustomdictionaryvalue|null
     * @ORM\ManyToOne(targetEntity="InfoCustomdictionaryvalue")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="contenttype_id", referencedColumnName="id")
     * })
     */
    private $contentType;

    /**
     * @var InfoCustomdictionaryvalue|null
     * @ORM\ManyToOne(targetEntity="InfoCustomdictionaryvalue")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="contenttopic_id", referencedColumnName="id")
     * })
     */
    private $contentTopic;

    /**
     * @var InfoCustomdictionaryvalue|null
     * @ORM\ManyToOne(targetEntity="InfoCustomdictionaryvalue")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="funnelstage_id", referencedColumnName="id")
     * })
     */
    private $funnelStage;

    /**
     * @var InfoCustomdictionaryvalue|null
     * @ORM\ManyToOne(targetEntity="InfoCustomdictionaryvalue")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="subcontenttopic_id", referencedColumnName="id")
     * })
     */
    private $subContentTopic;

    /**
     * @var InfoMcmcontent
     * @ORM\OneToMany(targetEntity="InfoMcmcontent", mappedBy="parent", cascade={"persist", "remove"})
     * @ORM\OrderBy({"created" = "DESC"})
     */
    private $children;

    /**
     * @var int
     *
     * @ORM\Column(name="is_greeting", type="integer", nullable=true)
     */
    private $isGreeting;

    /**
     * @var int
     *
     * @ORM\Column(name="is_request_phone", type="integer", nullable=true)
     */
    private $isRequestPhone;

    /**
     * @var string
     *
     * @ORM\Column(name="pmac_number", type="string", length=255, nullable=true)
     */
    private $pmacNumber;

    /**
     * @var InfoCustomdictionaryvalue|null
     * @ORM\ManyToOne(targetEntity="InfoCustomdictionaryvalue")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="efile_id", referencedColumnName="id")
     * })
     */
    private $efile;

    /**
     * @var int
     *
     * @ORM\Column(name="is_landing", type="integer", nullable=true)
     */
    private $isLanding;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->distributionCollection = new ArrayCollection();
        $this->contentCollection = new ArrayCollection();
        $this->fileCollection = new ArrayCollection();
        $this->conferences = new ArrayCollection();
        $this->buttons = new ArrayCollection();
        $this->children = new ArrayCollection();
    }

    /**
     * Get distributionCollection
     *
     * @return Collection|InfoMcmdistribution
     */
    public function getDistributionCollection()
    {
        return $this->distributionCollection;
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set created
     *
     * @param \DateTime $created
     * @return InfoMcmcontent
     */
    public function setCreated($created): self
    {
        $this->created = $created;

        return $this;
    }

    /**
     * Get created
     *
     * @return \DateTime
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * Set approvedDate
     *
     * @param \DateTime $approvedDate
     * @return InfoMcmcontent
     */
    public function setApprovedDate(\DateTime $approvedDate): self
    {
        $this->created = $approvedDate;

        return $this;
    }

    /**
     * Get approvedDate
     *
     * @return \DateTime
     */
    public function getApprovedDate()
    {
        return $this->approvedDate;
    }

    /**
     * Set modified
     *
     * @param \DateTime $modified
     * @return InfoMcmcontent
     */
    public function setModified(\DateTime $modified): self
    {
        $this->modified = $modified;

        return $this;
    }

    /**
     * Get modified
     *
     * @return \DateTime
     */
    public function getModified()
    {
        return $this->modified;
    }

    /**
     * Set type
     *
     * @param string $type
     *
     * @return InfoMcmcontent
     */
    public function setType($type): self
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type
     *
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return InfoMcmcontent
     */
    public function setName($name): self
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set subject
     *
     * @param null|string $subject
     *
     * @return InfoMcmcontent
     */
    public function setSubject(?string $subject): self
    {
        $this->subject = $subject;

        return $this;
    }

    /**
     * Get subject
     *
     * @return string
     */
    public function getSubject()
    {
        return $this->subject;
    }

    /**
     * Set caption
     *
     * @param null|string $caption
     *
     * @return InfoMcmcontent
     */
    public function setCaption(?string $caption): self
    {
        $this->caption = $caption;

        return $this;
    }

    /**
     * Get caption
     *
     * @return string
     */
    public function getCaption()
    {
        return $this->caption;
    }

    /**
     * Set image
     *
     * @param string $image
     *
     * @return InfoMcmcontent
     */
    public function setImage($image): self
    {
        $this->image = $image;

        return $this;
    }

    /**
     * Get image
     *
     * @return string
     */
    public function getImage()
    {
        return $this->image;
    }

    /**
     * Set url
     *
     * @param string $url
     *
     * @return InfoMcmcontent
     */
    public function setUrl($url): self
    {
        $this->url = $url;

        return $this;
    }

    /**
     * Get url
     *
     * @return string
     */
    public function getUrl()
    {
        return $this->url;
    }

    /**
     * Set body
     *
     * @param string $body
     *
     * @return InfoMcmcontent
     */
    public function setBody($body): self
    {
        $this->body = $body;

        return $this;
    }

    /**
     * Get body
     *
     * @return string
     */
    public function getBody()
    {
        return $this->body;
    }

    /**
     * Set alternateBody
     *
     * @param string $alternateBody
     *
     * @return InfoMcmcontent
     */
    public function setAlternateBody($alternateBody): self
    {
        $this->alternateBody = $alternateBody;

        return $this;
    }

    /**
     * Get alternateBody
     *
     * @return string
     */
    public function getAlternateBody()
    {
        return $this->alternateBody;
    }

    /**
     * Get message for SMS
     *
     * @return string
     */
    public function getAlternativeBody()
    {
        return $this->alternateBody;
    }

    /**
     * Get rownum
     *
     * @return int
     */
    public function getRownum()
    {
        return $this->rownum;
    }

    /**
     * Set rownum
     *
     * @param integer $rownum
     *
     * @return InfoMcmcontent
     */
    public function setRownum($rownum): self
    {
        $this->rownum = $rownum;

        return $this;
    }

    /**
     * Get isRaw
     *
     * @return boolean
     */
    public function getIsRaw()
    {
        return $this->isRaw;
    }

    /**
     * Set isRaw
     *
     * @param integer $isRaw
     *
     * @return InfoMcmcontent
     */
    public function setIsRaw($isRaw): self
    {
        $this->isRaw = $isRaw;

        return $this;
    }

    /**
     * Get isapproved
     *
     * @return int
     */
    public function getIsapproved()
    {
        return $this->isapproved;
    }

    /**
     * Set isapproved
     *
     * @param null|int $isapproved
     *
     * @return InfoMcmcontent
     */
    public function setIsapproved(?int $isapproved): self
    {
        $this->isapproved = $isapproved;

        return $this;
    }

    /**
     * Set currenttime
     *
     * @param \DateTime $currenttime
     *
     * @return InfoMcmcontent
     */
    public function setCurrenttime(\DateTime $currenttime): self
    {
        $this->currenttime = $currenttime;

        return $this;
    }

    /**
     * Get currenttime
     *
     * @return \DateTime
     */
    public function getCurrenttime()
    {
        return $this->currenttime;
    }

    /**
     * Set moduser
     *
     * @param string $moduser
     *
     * @return InfoMcmcontent
     */
    public function setModuser($moduser): self
    {
        $this->moduser = $moduser;

        return $this;
    }

    /**
     * Get moduser
     *
     * @return string
     */
    public function getModuser()
    {
        return $this->moduser;
    }

    /**
     * Set guid
     *
     * @param string $guid
     *
     * @return InfoMcmcontent
     */
    public function setGuid($guid): self
    {
        $this->guid = $guid;

        return $this;
    }

    /**
     * Get guid
     *
     * @return string
     */
    public function getGuid()
    {
        return $this->guid;
    }

    /**
     * Set isarchive
     *
     * @param null|int $isarchive
     *
     * @return InfoMcmcontent
     */
    public function setIsarchive(?int $isarchive): self
    {
        $this->isarchive = $isarchive;

        return $this;
    }

    /**
     * Get isarchive
     *
     * @return int
     */
    public function getIsarchive()
    {
        return $this->isarchive;
    }

    /**
     * Set user
     *
     * @param null|InfoUser $user
     *
     * @return InfoMcmcontent
     */
    public function setUser(?InfoUser $user): self
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return InfoUser
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Set approvedBy
     *
     * @param null|InfoUser $approvedBy
     *
     * @return InfoMcmcontent
     */
    public function setapprovedBy(?InfoUser $approvedBy): self
    {
        $this->approvedBy = $approvedBy;

        return $this;
    }

    /**
     * Get approvedBy
     *
     * @return InfoUser
     */
    public function getapprovedBy()
    {
        return $this->approvedBy;
    }

    /**
     * Add file
     *
     * @param InfoMcmfile $MCMcontentfile
     *
     * @return InfoMcmcontent
     */
    public function addFileCollection(InfoMcmfile $MCMcontentfile): self
    {
        $this->fileCollection->add($MCMcontentfile);

        return $this;
    }

    /**
     * Remove file
     *
     * @param InfoMcmfile $MCMcontentfile
     */
    public function removeFileCollection(InfoMcmfile $MCMcontentfile)
    {
        $this->fileCollection->removeElement($MCMcontentfile);
    }

    /**
     * Get files
     *
     * @return Collection
     */
    public function getFileCollection()
    {
        return $this->fileCollection;
    }

    public static function getAllowedTypes()
    {
        return [self::TYPE_SMS, self::TYPE_VIBER, self::TYPE_EMAIL, self::TYPE_WHATSAPP];
    }

    public function isTypeEmail(): bool
    {
        return $this->type === self::TYPE_EMAIL;
    }

    /**
     * Add contentCollection
     *
     * @param InfoMcmcontentlanguage $content
     *
     * @return InfoMcmcontent
     */
    public function addContentCollection(InfoMcmcontentlanguage $content)
    {
        if (!$this->contentCollection->contains($content)) {
            $this->contentCollection->add($content);
            $content->setContent($this);
        }

        return $this;
    }

    /**
     * Remove contentCollection
     *
     * @param InfoMcmcontentlanguage $content
     */
    public function removeContentCollection(InfoMcmcontentlanguage $content)
    {
        if ($this->contentCollection->contains($content)) {
            $this->contentCollection->removeElement($content);
        }
    }

    /**
     * Get contentCollections
     *
     * @return Collection
     */
    public function getContentCollection()
    {
        return $this->contentCollection;
    }

    /**
     * Add conference
     *
     * @param InfoMcmconference $conference
     *
     * @return InfoMcmcontent
     */
    public function addConference(InfoMcmconference $conference): self
    {
        $this->conferences->add($conference);

        return $this;
    }

    /**
     * Remove conference
     *
     * @param InfoMcmconference $conference
     */
    public function removeConference(InfoMcmconference $conference)
    {
        $this->conferences->removeElement($conference);
    }

    /**
     * Get conferences
     *
     * @return ArrayCollection
     */
    public function getConferences()
    {
        return $this->conferences;
    }

    /**
     * @return InfoMcmfile|null
     */
    public function getCertificate(): ?InfoMcmfile
    {
        return $this->certificate;
    }

    /**
     * @param InfoMcmfile|null $certificate
     */
    public function setCertificate(?InfoMcmfile $certificate): self
    {
        $this->certificate = $certificate;

        return $this;
    }

    /**
     * @return null|string
     */
    public function getJson(): ?string
    {
        return $this->json;
    }

    /**
     * @param null|string $json
     */
    public function setJson(?string $json): self
    {
        $this->json = $json;

        return $this;
    }

    /**
     * Set headerType
     *
     * @param string|null $headerType
     *
     * @return InfoMcmcontent
     */
    public function setHeaderType(?string $headerType): self
    {
        $this->headerType = $headerType;

        return $this;
    }

    /**
     * Get headerType
     *
     * @return null|string
     */
    public function getHeaderType(): ?string
    {
        return $this->headerType;
    }

    /**
     * Set headerBody
     *
     * @param string|null $headerBody
     *
     * @return InfoMcmcontent
     */
    public function setHeaderBody(?string $headerBody): self
    {
        $this->headerBody = $headerBody;

        return $this;
    }

    /**
     * Get headerBody
     *
     * @return null|string
     */
    public function getHeaderBody(): ?string
    {
        return $this->headerBody;
    }

    /**
     * Add button
     *
     * @param null|InfoMcmcontentbutton $button
     *
     * @return InfoMcmcontent
     */
    public function addButton(?InfoMcmcontentbutton $button): self
    {
        if (!$this->buttons->contains($button)) {
            $this->buttons->add($button);
            $button->setContent($this);
        }

        return $this;
    }

    /**
     * Remove button
     *
     * @param InfoMcmcontentbutton $content
     */
    public function removeButton(InfoMcmcontentbutton $button)
    {
        if ($this->buttons->contains($button)) {
            $this->buttons->removeElement($button);
            $button->setContent(null);
        }
    }

    /**
     * Get buttons
     *
     * @return ArrayCollection
     */
    public function getButtons()
    {
        return $this->buttons;
    }

    /**
     * Set parent
     *
     * @param InfoMcmcontent|null $parent
     *
     * @return InfoMcmcontent
     */
    public function setParent(?InfoMcmcontent $parent): self
    {
        $this->parent = $parent;

        return $this;
    }

    /**
     * Get parent
     *
     * @return InfoMcmcontent|null
     */
    public function getParent()
    {
        return $this->parent;
    }

    /**
     * Set isButtonsAction
     *
     * @param int|null $isButtonsAction
     *
     * @return InfoMcmcontent
     */
    public function setIsButtonsAction(?int $isButtonsAction): self
    {
        $this->isButtonsAction = $isButtonsAction;

        return $this;
    }

    /**
     * Get isButtonsAction
     *
     * @return boolean
     */
    public function getIsButtonsAction()
    {
        return $this->isButtonsAction;
    }

    /**
     * Set isButtonsText
     *
     * @param int|null $isButtonsText
     *
     * @return InfoMcmcontent
     */
    public function setIsButtonsText(?int $isButtonsText): self
    {
        $this->isButtonsText = $isButtonsText;

        return $this;
    }

    /**
     * Get isButtonsText
     *
     * @return boolean
     */
    public function getIsButtonsText()
    {
        return $this->isButtonsText;
    }

    /**
     * Set content type
     *
     * @param null|InfoCustomdictionaryvalue $contentType
     *
     * @return InfoMcmcontent
     */
    public function setContentType(?InfoCustomdictionaryvalue $contentType): self
    {
        $this->contentType = $contentType;

        return $this;
    }

    /**
     * Get content type
     *
     * @return InfoCustomdictionaryvalue
     */
    public function getContentType()
    {
        return $this->contentType;
    }

    /**
     * Set content topic
     *
     * @param null|InfoCustomdictionaryvalue $contentTopic
     *
     * @return InfoMcmcontent
     */
    public function setContentTopic(?InfoCustomdictionaryvalue $contentTopic): self
    {
        $this->contentTopic = $contentTopic;

        return $this;
    }

    /**
     * Get content topic
     *
     * @return InfoCustomdictionaryvalue
     */
    public function getContentTopic()
    {
        return $this->contentTopic;
    }

    /**
     * Set funnel stage
     *
     * @param null|InfoCustomdictionaryvalue $funnelStage
     *
     * @return InfoMcmcontent
     */
    public function setFunnelStage(?InfoCustomdictionaryvalue $funnelStage): self
    {
        $this->funnelStage = $funnelStage;

        return $this;
    }

    /**
     * Get funnel stage
     *
     * @return InfoCustomdictionaryvalue
     */
    public function getFunnelStage()
    {
        return $this->funnelStage;
    }

    /**
     * Set content subtopic
     *
     * @param null|InfoCustomdictionaryvalue $subContentTopic
     *
     * @return InfoMcmcontent
     */
    public function setSubContentTopic(?InfoCustomdictionaryvalue $subContentTopic): self
    {
        $this->subContentTopic = $subContentTopic;

        return $this;
    }

    /**
     * Get content subtopic
     *
     * @return InfoCustomdictionaryvalue
     */
    public function getSubContentTopic()
    {
        return $this->subContentTopic;
    }

    /**
     * Get children
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getChildren()
    {
        return $this->children;
    }

    /**
     * Set isGreeting
     *
     * @param int|null $isGreeting
     *
     * @return InfoMcmcontent
     */
    public function setIsGreeting(?int $isGreeting): self
    {
        $this->isGreeting = $isGreeting;

        return $this;
    }

    /**
     * Get isGreeting
     *
     * @return int|null
     */
    public function getIsGreeting(): ?int
    {
        return $this->isGreeting;
    }

    /**
     * Set isRequestPhone
     *
     * @param int|null $isRequestPhone
     *
     * @return InfoMcmcontent
     */
    public function setIsRequestPhone(?int $isRequestPhone): self
    {
        $this->isRequestPhone = $isRequestPhone;

        return $this;
    }

    /**
     * Get isRequestPhone
     *
     * @return int|null
     */
    public function getIsRequestPhone(): ?int
    {
        return $this->isRequestPhone;
    }

    /**
     * Set pmacNumber
     *
     * @param string|null $pmacNumber
     *
     * @return InfoMcmcontent
     */
    public function setPmacNumber(?string $pmacNumber): self
    {
        $this->pmacNumber = $pmacNumber;

        return $this;
    }

    /**
     * Get pmacNumber
     *
     * @return null|string
     */
    public function getPmacNumber(): ?string
    {
        return $this->pmacNumber;
    }

    /**
     * Set efile
     *
     * @param null|InfoCustomdictionaryvalue $efile
     *
     * @return InfoMcmcontent
     */
    public function setEfile(?InfoCustomdictionaryvalue $efile): self
    {
        $this->efile = $efile;

        return $this;
    }

    /**
     * Get efile
     *
     * @return InfoCustomdictionaryvalue
     */
    public function getEfile(): ?InfoCustomdictionaryvalue
    {
        return $this->efile;
    }

    /**
     * Set isLanding
     *
     * @param int|null $isLanding
     *
     * @return InfoMcmcontent
     */
    public function setIsLanding(?int $isLanding): self
    {
        $this->isLanding = $isLanding;

        return $this;
    }

    /**
     * Get isLanding
     *
     * @return int|null
     */
    public function getIsLanding(): ?int
    {
        return $this->isLanding;
    }
}
