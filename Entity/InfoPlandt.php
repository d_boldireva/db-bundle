<?php

namespace TeamSoft\CrmRepositoryBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Context\ExecutionContextInterface;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * InfoPlandt
 *
 * @ORM\Table(name="info_plandt")
 * @ORM\Entity(repositoryClass="TeamSoft\CrmRepositoryBundle\Repository\InfoPlandt")
 */
class InfoPlandt implements ServiceFieldInterface
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dt1", type="datetime", nullable=true)
     */
    private $dt1;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dt2", type="datetime", nullable=true)
     */
    private $dt2;

    /**
     * @var guid
     *
     * @ORM\Column(name="guid", type="guid", nullable=true)
     */
    private $guid;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="currenttime", type="datetime", nullable=true)
     */
    private $currenttime;

    /**
     * @var string
     *
     * @ORM\Column(name="moduser", type="string", length=16, nullable=true)
     */
    private $moduser;

    /**
     * @var InfoPlan
     *
     * @ORM\ManyToOne(targetEntity="InfoPlan")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="plan_id", referencedColumnName="id")
     * })
     */
    private $plan;

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set guid
     *
     * @param string $guid
     *
     * @return InfoPlandt
     */
    public function setGuid($guid)
    {
        $this->guid = $guid;

        return $this;
    }

    /**
     * Get guid
     *
     * @return string
     */
    public function getGuid()
    {
        return $this->guid;
    }

    /**
     * Set currenttime
     *
     * @param \DateTime $currenttime
     *
     * @return InfoPlandt
     */
    public function setCurrenttime(\DateTime $currenttime = null)
    {
        $this->currenttime = $currenttime;

        return $this;
    }

    /**
     * Get currenttime
     *
     * @return \DateTime
     */
    public function getCurrenttime()
    {
        return $this->currenttime;
    }

    /**
     * Set moduser
     *
     * @param string $moduser
     *
     * @return InfoPlandt
     */
    public function setModuser($moduser)
    {
        $this->moduser = $moduser;

        return $this;
    }

    /**
     * Get moduser
     *
     * @return string
     */
    public function getModuser()
    {
        return $this->moduser;
    }

    /**
     * Set plan
     *
     * @param InfoPlan $plan
     *
     * @return InfoPlandt
     */
    public function setPlan(InfoPlan $plan = null)
    {
        $this->plan = $plan;

        return $this;
    }

    /**
     * Get plan
     *
     * @return InfoPlan
     */
    public function getPlan()
    {
        return $this->plan;
    }

    /**
     * Set dt1
     *
     * @param \DateTime $dt1
     *
     * @return InfoPlandt
     */
    public function setDt1($dt1)
    {
        $this->dt1 = $dt1;

        return $this;
    }

    /**
     * Get dt1
     *
     * @return \DateTime
     */
    public function getDt1()
    {
        return $this->dt1;
    }

    /**
     * Set dt2
     *
     * @param \DateTime $dt2
     *
     * @return InfoPlandt
     */
    public function setDt2($dt2)
    {
        $this->dt2 = $dt2;

        return $this;
    }

    /**
     * Get dt2
     *
     * @return \DateTime
     */
    public function getDt2()
    {
        return $this->dt2;
    }

    /**
     * @return int
     */
    public function getPlanId()
    {
        return $this->getPlan()->getId();
    }
}
