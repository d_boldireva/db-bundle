<?php

namespace TeamSoft\CrmRepositoryBundle\Entity;

use DateTime;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * InfoSliceRelation
 *
 * @ORM\Table(name="info_slicerelation")
 * @ORM\Entity()
 */
class InfoSliceRelation implements ServiceFieldInterface
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string|null
     *
     * @ORM\Column(name="parent_field", type="string", length=255, nullable=true)
     */
    private $parentField;

    /**
     * @var string|null
     *
     * @ORM\Column(name="child_field", type="string", length=255, nullable=true)
     */
    private $childField;

    /**
     * @var DateTime|null
     *
     * @ORM\Column(name="currenttime", type="datetime", nullable=true)
     */
    private $currenttime;

    /**
     * @var string|null
     *
     * @ORM\Column(name="moduser", type="string", length=16, nullable=true)
     * )
     */
    private $moduser;

    /**
     * @var string|null
     *
     * @ORM\Column(name="guid", type="guid", nullable=true)
     */
    private $guid;

    /**
     * @ORM\ManyToOne(targetEntity="InfoSlice", inversedBy="id")
     * @ORM\JoinColumn(name="parent_id", referencedColumnName="id")
     */
    private $parent;

    /**
     * @ORM\ManyToOne(targetEntity="InfoSlice", inversedBy="id")
     * @ORM\JoinColumn(name="child_id", referencedColumnName="id")
     */
    private $child;


    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set parent
     *
     * @param InfoSlice $parent
     *
     * @return InfoSliceRelation
     */
    public function setParent($parent = null)
    {
        $this->parent = $parent;

        return $this;
    }

    /**
     * Get parent
     *
     * @return InfoSlice
     */
    public function getParent()
    {
        return $this->parent;
    }

    /**
     * Set child
     *
     * @param InfoSlice $child
     *
     * @return InfoSliceRelation
     */
    public function setChild($child = null)
    {
        $this->child = $child;

        return $this;
    }

    /**
     * Get child
     *
     * @return InfoSlice
     */
    public function getChild()
    {
        return $this->child;
    }

    /**
     * Set parentField.
     *
     * @param string|null $parentField
     *
     * @return InfoSliceRelation
     */
    public function setParentField($parentField = null)
    {
        $this->parentField = $parentField;

        return $this;
    }

    /**
     * Get parentField.
     *
     * @return string|null
     */
    public function getParentField()
    {
        return $this->parentField;
    }

    /**
     * Set childField.
     *
     * @param string|null $childField
     *
     * @return InfoSliceRelation
     */
    public function setChildField($childField = null)
    {
        $this->childField = $childField;

        return $this;
    }

    /**
     * Get childField.
     *
     * @return string|null
     */
    public function getChildField()
    {
        return $this->childField;
    }

    /**
     * Set currenttime.
     *
     * @param DateTime|null $currenttime
     *
     * @return InfoSliceRelation
     */
    public function setCurrenttime(DateTime $currenttime = null)
    {
        $this->currenttime = $currenttime;

        return $this;
    }

    /**
     * Get currenttime.
     *
     * @return DateTime|null
     */
    public function getCurrenttime()
    {
        return $this->currenttime;
    }

    /**
     * Set moduser.
     *
     * @param string|null $moduser
     *
     * @return InfoSliceRelation
     */
    public function setModuser($moduser = null)
    {
        $this->moduser = $moduser;

        return $this;
    }

    /**
     * Get moduser.
     *
     * @return string|null
     */
    public function getModuser()
    {
        return $this->moduser;
    }

    /**
     * Set guid.
     *
     * @param string|null $guid
     *
     * @return InfoSliceRelation
     */
    public function setGuid($guid = null)
    {
        $this->guid = $guid;

        return $this;
    }

    /**
     * Get guid.
     *
     * @return string|null
     */
    public function getGuid()
    {
        return $this->guid;
    }
}
