<?php

namespace TeamSoft\CrmRepositoryBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * InfoMcmtelegramuser
 *
 * @ORM\Table(name="info_mcmtelegramuser")
 * @ORM\Entity
 */
class InfoMcmtelegramuser implements ServiceFieldInterface
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var InfoContact
     *
     * @ORM\ManyToOne(targetEntity="InfoContact", cascade={"persist", "remove"})
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="contact_id", referencedColumnName="id")
     * })
     */
    private $contact;

    /**
     * @var string|null
     *
     * @ORM\Column(name="user_identifier", type="string", nullable=true)
     */
    private $telegramUser;

    /**
     * @var string|null
     *
     * @ORM\Column(name="chat_identifier", type="string", nullable=true)
     */
    private $telegramChat;

    /**
     * @var string|null
     *
     * @ORM\Column(name="phone", type="string", length=255, nullable=true)
     */
    private $phone;

    /**
     * @var string|null
     *
     * @ORM\Column(name="first_name", type="string", length=255, nullable=true)
     */
    private $firstName;

    /**
     * @var string|null
     *
     * @ORM\Column(name="last_name", type="string", length=255, nullable=true)
     */
    private $lastName;

    /**
     * @var string|null
     *
     * @ORM\Column(name="user_name", type="string", length=255, nullable=true)
     */
    private $userName;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="authorization_date", type="datetime", nullable=true)
     */
    private $authorizationDate;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="currenttime", type="datetime", nullable=true)
     */
    private $currenttime;

    /**
     * @var string
     *
     * @ORM\Column(name="moduser", type="string", length=16, nullable=true)
     */
    private $moduser;

    /**
     * @var string
     *
     * @ORM\Column(name="guid", type="guid", nullable=true)
     */
    private $guid;


    /**
     * Get id
     *
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * Set contact
     *
     * @param InfoContact|null $contact
     *
     * @return InfoMcmtelegramuser
     */
    public function setContact(?InfoContact $contact): self
    {
        $this->contact = $contact;

        return $this;
    }

    /**
     * Get contact
     *
     * @return InfoContact
     */
    public function getContact(): ?InfoContact
    {
        return $this->contact;
    }

    /**
     * Set telegramUser
     *
     * @param string $telegramUser
     *
     * @return InfoMcmtelegramuser
     */
    public function setTelegramUser(?string $telegramUser): self
    {
        $this->telegramUser = $telegramUser;

        return $this;
    }

    /**
     * Get telegramUser
     *
     * @return string
     */
    public function getTelegramUser(): ?string
    {
        return $this->telegramUser;
    }

    /**
     * Set telegramChat
     *
     * @param string $telegramChat
     *
     * @return InfoMcmtelegramuser
     */
    public function setTelegramChat(?string $telegramChat): self
    {
        $this->telegramChat = $telegramChat;

        return $this;
    }

    /**
     * Get telegramChat
     *
     * @return string
     */
    public function getTelegramChat(): ?string
    {
        return $this->telegramChat;
    }

    /**
     * Set phone
     *
     * @param string|null $phone
     *
     * @return InfoMcmtelegramuser
     */
    public function setPhone(?string $phone): self
    {
        $this->phone = $phone;

        return $this;
    }

    /**
     * Get phone
     *
     * @return string
     */
    public function getPhone(): ?string
    {
        return $this->phone;
    }

    /**
     * Set firstName
     *
     * @param string|null $firstName
     *
     * @return InfoMcmtelegramuser
     */
    public function setFirstName(?string $firstName): self
    {
        $this->firstName = $firstName;

        return $this;
    }

    /**
     * Get firstName
     *
     * @return string
     */
    public function getFirstName(): ?string
    {
        return $this->firstName;
    }

    /**
     * Set lastName
     *
     * @param string|null $lastName
     *
     * @return InfoMcmtelegramuser
     */
    public function setLastName(?string $lastName): self
    {
        $this->lastName = $lastName;

        return $this;
    }

    /**
     * Get lastName
     *
     * @return string
     */
    public function getLastName(): ?string
    {
        return $this->lastName;
    }

    /**
     * Set userName
     *
     * @param string|null $userName
     *
     * @return InfoMcmtelegramuser
     */
    public function setUserName(?string $userName): self
    {
        $this->userName = $userName;

        return $this;
    }

    /**
     * Get userName
     *
     * @return string
     */
    public function getUserName(): ?string
    {
        return $this->userName;
    }

    /**
     * Set authorizationDate
     *
     * @param \DateTime $authorizationDate
     *
     * @return InfoMcmtelegramuser
     */
    public function setAuthorizationDate(?\DateTime $authorizationDate): self
    {
        $this->authorizationDate = $authorizationDate;

        return $this;
    }

    /**
     * Get authorizationDate
     *
     * @return \DateTime|null
     */
    public function getAuthorizationDate(): ?\DateTime
    {
        return $this->authorizationDate;
    }

    /**
     * Set currenttime
     *
     * @param null|\DateTime $currenttime
     *
     * @return InfoMcmtelegramuser
     */
    public function setCurrenttime(?\DateTime $currenttime): self
    {
        $this->currenttime = $currenttime;

        return $this;
    }

    /**
     * Get currenttime
     *
     * @return null|\DateTime
     */
    public function getCurrenttime(): ?\DateTime
    {
        return $this->currenttime;
    }

    /**
     * Set moduser
     *
     * @param null|string $moduser
     *
     * @return InfoMcmtelegramuser
     */
    public function setModuser($moduser = null): self
    {
        $this->moduser = $moduser;

        return $this;
    }

    /**
     * Get moduser
     *
     * @return null|string
     */
    public function getModuser(): ?string
    {
        return $this->moduser;
    }

    /**
     * Set guid
     *
     * @param null|string $guid
     *
     * @return InfoMcmtelegramuser
     */
    public function setGuid($guid = null): self
    {
        $this->guid = $guid;

        return $this;
    }

    /**
     * Get guid
     *
     * @return string|null
     */
    public function getGuid(): ?string
    {
        return $this->guid;
    }
}
