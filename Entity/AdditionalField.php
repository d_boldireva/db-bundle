<?php

namespace TeamSoft\CrmRepositoryBundle\Entity;

abstract class AdditionalField implements AdditionalFieldInterface
{
    public function getValue()
    {
        switch ($this->getFielddatatype()) {
            case InfoAddinfotype::STRING_VALUE:
                return $this->getStringvalue();
            case InfoAddinfotype::INT_VALUE:
                return $this->getIntvalue();
            case InfoAddinfotype::FLOAT_VALUE:
                return $this->getFloatvalue();
            case InfoAddinfotype::DATE_VALUE:
                return $this->getDatevalue();
            case InfoAddinfotype::DICTIONARY_VALUE:
                return $this->getDictvalue();
            case InfoAddinfotype::SYSTEM_DICTIONARY_VALUE:
                return $this->getSysdictvalueId();
        }
    }

    private function getFielddatatype()
    {
        return $this->getInfotype() ? $this->getInfotype()->getFielddatatype() : null;
    }

}
