<?php

namespace TeamSoft\CrmRepositoryBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * InfoContactrequest
 *
 * @ORM\Table(name="info_contactrequest")
 * @ORM\Entity
 */
class InfoContactrequest implements ServiceFieldInterface
{
    const CONTACT_VERIFICATION_WITHOUT_DCR = 1;
    const CONTACT_VERIFICATION_WITH_DCR = 2;

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="modified", type="datetime", nullable=true)
     */
    private $modified;

    /**
     * @var string
     *
     * @ORM\Column(name="field", type="string", length=255, nullable=true)
     */
    private $field;

    /**
     * @var string
     *
     * @ORM\Column(name="oldvalue", type="string", length=255, nullable=true)
     */
    private $oldvalue;

    /**
     * @var string
     *
     * @ORM\Column(name="newvalue", type="string", length=255, nullable=true)
     */
    private $newvalue;

    /**
     * @var string
     *
     * @ORM\Column(name="comment", type="string", length=255, nullable=true)
     */
    private $comment;

    /**
     * @var string
     *
     * @ORM\Column(name="guid", type="guid", nullable=true)
     */
    private $guid;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="currenttime", type="datetime", nullable=true)
     */
    private $currenttime;

    /**
     * @var string
     *
     * @ORM\Column(name="moduser", type="string", length=16, nullable=true)
     */
    private $moduser;

    /**
     * @var int
     *
     * @ORM\Column(name="statusverification", type="integer", nullable=true)
     */
    private $statusverification;

    /**
     * @var int
     *
     * @ORM\Column(name="statusverification2", type="string", nullable=true)
     */
    private $statusverification2;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="sendverification", type="datetime", nullable=true)
     */
    private $sendverification;

    /**
     * @var InfoArchivereason
     *
     * @ORM\ManyToOne(targetEntity="InfoArchivereason")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="archivereason_id_old", referencedColumnName="id")
     * })
     */
    private $archivereasonOld;

    /**
     * @var InfoArchivereason
     *
     * @ORM\ManyToOne(targetEntity="InfoArchivereason")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="archivereason_id_new", referencedColumnName="id")
     * })
     */
    private $archivereasonNew;

    /**
     * @var InfoCompany
     *
     * @ORM\ManyToOne(targetEntity="InfoCompany")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="company_id_old", referencedColumnName="id")
     * })
     */
    private $companyOld;

    /**
     * @var InfoCompany
     *
     * @ORM\ManyToOne(targetEntity="InfoCompany")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="company_id_new", referencedColumnName="id")
     * })
     */
    private $companyNew;

    /**
     * @var InfoCompany
     *
     * @ORM\ManyToOne(targetEntity="InfoCompany")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="additionalcompany_id_old", referencedColumnName="id")
     * })
     */
    private $additionalcompanyOld;

    /**
     * @var InfoCompany
     *
     * @ORM\ManyToOne(targetEntity="InfoCompany")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="additionalcompany_id_new", referencedColumnName="id")
     * })
     */
    private $additionalcompanyNew;

    /**
     * @var InfoContact
     *
     * @ORM\ManyToOne(targetEntity="InfoContact", cascade={"persist"})
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="contact_id", referencedColumnName="id")
     * })
     */
    private $contact;

    /**
     * @var InfoContactcateg
     *
     * @ORM\ManyToOne(targetEntity="InfoContactcateg")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="category_id_old", referencedColumnName="id")
     * })
     */
    private $contactcategoryOld;

    /**
     * @var InfoContactcateg
     *
     * @ORM\ManyToOne(targetEntity="InfoContactcateg")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="category_id_new", referencedColumnName="id")
     * })
     */
    private $contactcategoryNew;

    /**
     * @var InfoDictionary
     *
     * @ORM\ManyToOne(targetEntity="InfoDictionary")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="spec_id_old", referencedColumnName="id")
     * })
     */
    private $specOld;

    /**
     * @var InfoDictionary
     *
     * @ORM\ManyToOne(targetEntity="InfoDictionary")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="spec_id_new", referencedColumnName="id")
     * })
     */
    private $specNew;

    /**
     * @var InfoDictionary
     *
     * @ORM\ManyToOne(targetEntity="InfoDictionary")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="additionalspec_id_old", referencedColumnName="id")
     * })
     */
    private $additionalSpecOld;

    /**
     * @var InfoDictionary
     *
     * @ORM\ManyToOne(targetEntity="InfoDictionary")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="additionalspec_id_new", referencedColumnName="id")
     * })
     */
    private $additionalSpecNew;

    /**
     * @var InfoDictionary
     *
     * @ORM\ManyToOne(targetEntity="InfoDictionary")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="position_id_old", referencedColumnName="id")
     * })
     */
    private $positionOld;

    /**
     * @var InfoDictionary
     *
     * @ORM\ManyToOne(targetEntity="InfoDictionary")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="position_id_new", referencedColumnName="id")
     * })
     */
    private $positionNew;

    /**
     * @var InfoContacttype
     *
     * @ORM\ManyToOne(targetEntity="InfoContacttype")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="contacttype_id_old", referencedColumnName="id")
     * })
     */
    private $contacttypeOld;

    /**
     * @var InfoContacttype
     *
     * @ORM\ManyToOne(targetEntity="InfoContacttype")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="contacttype_id_new", referencedColumnName="id")
     * })
     */
    private $contacttypeNew;

    /**
     * @var InfoUser
     *
     * @ORM\ManyToOne(targetEntity="InfoUser")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="owner_id_old", referencedColumnName="id")
     * })
     */
    private $ownerOld;

    /**
     * @var InfoUser
     *
     * @ORM\ManyToOne(targetEntity="InfoUser")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="owner_id_new", referencedColumnName="id")
     * })
     */
    private $ownerNew;

    /**
     * @var InfoUser
     *
     * @ORM\ManyToOne(targetEntity="InfoUser")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="additionalowner_id_old", referencedColumnName="id")
     * })
     */
    private $additionalownerOld;

    /**
     * @var InfoUser
     *
     * @ORM\ManyToOne(targetEntity="InfoUser")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="additionalowner_id_new", referencedColumnName="id")
     * })
     */
    private $additionalownerNew;

    /**
     * @var InfoUser
     *
     * @ORM\ManyToOne(targetEntity="InfoUser")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="modifier_id", referencedColumnName="id")
     * })
     */
    private $modifier;

    /**
     * @var InfoRegion
     *
     * @ORM\ManyToOne(targetEntity="InfoRegion")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="region_id_old", referencedColumnName="id")
     * })
     */
    private $regionOld;

    /**
     * @var InfoRegion
     *
     * @ORM\ManyToOne(targetEntity="InfoRegion")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="region_id_new", referencedColumnName="id")
     * })
     */
    private $regionNew;

    /**
     * @var InfoCity
     *
     * @ORM\ManyToOne(targetEntity="InfoCity")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="city_id_old", referencedColumnName="id")
     * })
     */
    private $cityOld;

    /**
     * @var InfoCity
     *
     * @ORM\ManyToOne(targetEntity="InfoCity")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="city_id_new", referencedColumnName="id")
     * })
     */
    private $cityNew;

    /**
     * @var int
     *
     * @ORM\Column(name="requesttype", type="integer", nullable=true)
     */
    private $requesttype;

    /**
     * @var InfoAddinfotype $addinfotype
     *
     * @ORM\ManyToOne(targetEntity="InfoAddinfotype")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="addinfotype_id", referencedColumnName="id")
     * })
     */
    private $addinfotype;

    /**
     * @var InfoDcrtype
     *
     * @ORM\ManyToOne(targetEntity="InfoDcrtype")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="dcrtype_id", referencedColumnName="id")
     * })
     */
    private $dcrType;

    /**
     * @var InfoRole
     *
     * @ORM\ManyToOne(targetEntity="InfoRole")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="role_id", referencedColumnName="id")
     * })
     */
    private $role;

    /**
     * @var InfoCustomdictionaryvalue
     *
     * @ORM\ManyToOne(targetEntity="InfoCustomdictionaryvalue")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="dictvalue_id_old", referencedColumnName="id")
     * })
     */
    private $dictvalueOld;

    /**
     * @var InfoCustomdictionaryvalue
     *
     * @ORM\ManyToOne(targetEntity="InfoCustomdictionaryvalue")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="dictvalue_id_new", referencedColumnName="id")
     * })
     */
    private $dictvalueNew;

    /**
     * @var InfoUser
     *
     * @ORM\ManyToOne(targetEntity="InfoUser")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     * })
     */
    private $user;

    /**
     * @var integer
     *
     * @ORM\Column(name="chain_num", type="integer", nullable=true)
     */
    private $chainNum;

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set modified
     *
     * @param \DateTime $modified
     *
     * @return InfoContactrequest
     */
    public function setModified(\DateTime $modified)
    {
        $this->modified = $modified;

        return $this;
    }

    /**
     * Get modified
     *
     * @return \DateTime
     */
    public function getModified()
    {
        return $this->modified;
    }

    /**
     * Set field
     *
     * @param string $field
     *
     * @return InfoContactrequest
     */
    public function setField($field)
    {
        $this->field = $field;

        return $this;
    }

    /**
     * Get field
     *
     * @return string
     */
    public function getField()
    {
        return $this->field;
    }

    /**
     * Set oldvalue
     *
     * @param string $oldvalue
     *
     * @return InfoContactrequest
     */
    public function setOldvalue($oldvalue)
    {
        $this->oldvalue = $oldvalue;

        return $this;
    }

    /**
     * Get oldvalue
     *
     * @return string
     */
    public function getOldvalue()
    {
        return $this->oldvalue;
    }

    /**
     * Set newvalue
     *
     * @param string $newvalue
     *
     * @return InfoContactrequest
     */
    public function setNewvalue($newvalue)
    {
        $this->newvalue = $newvalue;

        return $this;
    }

    /**
     * Get newvalue
     *
     * @return string
     */
    public function getNewvalue()
    {
        return $this->newvalue;
    }

    /**
     * Set comment
     *
     * @param string $comment
     *
     * @return InfoContactrequest
     */
    public function setComment($comment)
    {
        $this->comment = $comment;

        return $this;
    }

    /**
     * Get comment
     *
     * @return string
     */
    public function getComment()
    {
        return $this->comment;
    }

    /**
     * Set guid
     *
     * @param string $guid
     *
     * @return InfoContactrequest
     */
    public function setGuid($guid)
    {
        $this->guid = $guid;

        return $this;
    }

    /**
     * Get guid
     *
     * @return string
     */
    public function getGuid()
    {
        return $this->guid;
    }

    /**
     * Set currenttime
     *
     * @param \DateTime $currenttime
     *
     * @return InfoContactrequest
     */
    public function setCurrenttime(\DateTime $currenttime)
    {
        $this->currenttime = $currenttime;

        return $this;
    }

    /**
     * Get currenttime
     *
     * @return \DateTime
     */
    public function getCurrenttime()
    {
        return $this->currenttime;
    }

    /**
     * Set moduser
     *
     * @param string $moduser
     *
     * @return InfoContactrequest
     */
    public function setModuser($moduser)
    {
        $this->moduser = $moduser;

        return $this;
    }

    /**
     * Get moduser
     *
     * @return string
     */
    public function getModuser()
    {
        return $this->moduser;
    }

    /**
     * Set statusverification
     *
     * @param int $statusverification
     *
     * @return InfoContactrequest
     */
    public function setStatusverification($statusverification)
    {
        $this->statusverification = $statusverification;

        return $this;
    }

    /**
     * Get statusverification
     *
     * @return int
     */
    public function getStatusverification()
    {
        return $this->statusverification;
    }

    /**
     * Set statusverification2
     *
     * @param int $statusverification2
     *
     * @return InfoContactrequest
     */
    public function setStatusverification2($statusverification2)
    {
        $this->statusverification2 = $statusverification2;

        return $this;
    }

    /**
     * Get statusverification2
     *
     * @return int
     */
    public function getStatusverification2()
    {
        return $this->statusverification2;
    }

    /**
     * Set sendverification
     *
     * @param \DateTime $sendverification
     *
     * @return InfoContactrequest
     */
    public function setSendverification($sendverification)
    {
        $this->sendverification = $sendverification;

        return $this;
    }

    /**
     * Get sendverification
     *
     * @return \DateTime
     */
    public function getSendverification()
    {
        return $this->sendverification;
    }



    /**
     * Set contactcategoryOld
     *
     * @param InfoContactcateg $contactcategory
     *
     * @return InfoContactrequest
     */
    public function setContactcategoryOld(InfoContactcateg $contactcategory = null)
    {
        $this->contactcategoryOld = $contactcategory;

        return $this;
    }

    /**
     * Get contactcategoryOld
     *
     * @return InfoContactcateg
     */
    public function getContactcategoryOld()
    {
        return $this->contactcategoryOld;
    }

    /**
     * Set contactcategoryNew
     *
     * @param InfoContactcateg $contactcategory
     *
     * @return InfoContactrequest
     */
    public function setContactcategoryNew(InfoContactcateg $contactcategory = null)
    {
        $this->contactcategoryNew = $contactcategory;

        return $this;
    }

    /**
     * Get contactcategoryNew
     *
     * @return InfoContactcateg
     */
    public function getContactcategoryNew()
    {
        return $this->contactcategoryNew;
    }

    /**
     * Set archivereasonOld
     *
     * @param InfoArchivereason $archivereasonOld
     *
     * @return InfoContactrequest
     */
    public function setArchivereasonOld(InfoArchivereason $archivereasonOld = null)
    {
        $this->archivereasonOld = $archivereasonOld;

        return $this;
    }

    /**
     * Get archivereasonOld
     *
     * @return InfoArchivereason
     */
    public function getArchivereasonOld()
    {
        return $this->archivereasonOld;
    }

    /**
     * Set archivereasonNew
     *
     * @param InfoArchivereason $archivereasonNew
     *
     * @return InfoContactrequest
     */
    public function setArchivereasonNew(InfoArchivereason $archivereasonNew = null)
    {
        $this->archivereasonNew = $archivereasonNew;

        return $this;
    }

    /**
     * Get archivereasonNew
     *
     * @return InfoArchivereason
     */
    public function getArchivereasonNew()
    {
        return $this->archivereasonNew;
    }

    /**
     * Set companyOld
     *
     * @param InfoCompany $companyOld
     *
     * @return InfoContactrequest
     */
    public function setCompanyOld(InfoCompany $companyOld = null)
    {
        $this->companyOld = $companyOld;

        return $this;
    }

    /**
     * Get companyOld
     *
     * @return InfoCompany
     */
    public function getCompanyOld()
    {
        return $this->companyOld;
    }

    /**
     * Set companyNew
     *
     * @param InfoCompany $companyNew
     *
     * @return InfoContactrequest
     */
    public function setCompanyNew(InfoCompany $companyNew = null)
    {
        $this->companyNew = $companyNew;

        return $this;
    }

    /**
     * Get companyNew
     *
     * @return InfoCompany
     */
    public function getCompanyNew()
    {
        return $this->companyNew;
    }

    /**
     * Set additionalcompanyOld
     *
     * @param InfoCompany $additionalcompanyOld
     *
     * @return InfoContactrequest
     */
    public function setAdditionalcompanyOld(InfoCompany $additionalcompanyOld = null)
    {
        $this->additionalcompanyOld = $additionalcompanyOld;

        return $this;
    }

    /**
     * Get additionalcompanyOld
     *
     * @return InfoCompany
     */
    public function getAdditionalcompanyOld()
    {
        return $this->additionalcompanyOld;
    }

    /**
     * Set additionalcompanyNew
     *
     * @param InfoCompany $additionalcompanyNew
     *
     * @return InfoContactrequest
     */
    public function setAdditionalcompanyNew(InfoCompany $additionalcompanyNew = null)
    {
        $this->additionalcompanyNew = $additionalcompanyNew;

        return $this;
    }

    /**
     * Get additionalcompanyNew
     *
     * @return InfoCompany
     */
    public function getAdditionalcompanyNew()
    {
        return $this->additionalcompanyNew;
    }

    /**
     * Set contact
     *
     * @param InfoContact $contact
     *
     * @return InfoContactrequest
     */
    public function setContact(InfoContact $contact = null)
    {
        $this->contact = $contact;

        return $this;
    }

    /**
     * Get contact
     *
     * @return InfoContact
     */
    public function getContact()
    {
        return $this->contact;
    }

    /**
     * Set specOld
     *
     * @param InfoDictionary $specOld
     *
     * @return InfoContactrequest
     */
    public function setSpecOld(InfoDictionary $specOld = null)
    {
        $this->specOld = $specOld;

        return $this;
    }

    /**
     * Get specOld
     *
     * @return InfoDictionary
     */
    public function getSpecOld()
    {
        return $this->specOld;
    }

    /**
     * Set specNew
     *
     * @param InfoDictionary $specNew
     *
     * @return InfoContactrequest
     */
    public function setSpecNew(InfoDictionary $specNew = null)
    {
        $this->specNew = $specNew;

        return $this;
    }

    /**
     * Get specNew
     *
     * @return InfoDictionary
     */
    public function getSpecNew()
    {
        return $this->specNew;
    }

    /**
     * Set additionalSpecOld
     *
     * @param InfoDictionary $additionalSpecOld
     *
     * @return InfoContactrequest
     */
    public function setAdditionalspecOld(InfoDictionary $additionalSpecOld = null)
    {
        $this->additionalSpecOld = $additionalSpecOld;

        return $this;
    }

    /**
     * Get additionalSpecOld
     *
     * @return InfoDictionary
     */
    public function getAdditionalspecOld()
    {
        return $this->additionalSpecOld;
    }

    /**
     * Set additionalSpecNew
     *
     * @param InfoDictionary $additionalSpecNew
     *
     * @return InfoContactrequest
     */
    public function setAdditionalspecNew(InfoDictionary $additionalSpecNew = null)
    {
        $this->additionalSpecNew = $additionalSpecNew;

        return $this;
    }

    /**
     * Get additionalSpecNew
     *
     * @return InfoDictionary
     */
    public function getAdditionalspecNew()
    {
        return $this->additionalSpecNew;
    }

    /**
     * Set positionOld
     *
     * @param InfoDictionary $positionOld
     *
     * @return InfoContactrequest
     */
    public function setPositionOld(InfoDictionary $positionOld = null)
    {
        $this->positionOld = $positionOld;

        return $this;
    }

    /**
     * Get positionOld
     *
     * @return InfoDictionary
     */
    public function getPositionOld()
    {
        return $this->positionOld;
    }

    /**
     * Set positionNew
     *
     * @param InfoDictionary $positionNew
     *
     * @return InfoContactrequest
     */
    public function setPositionNew(InfoDictionary $positionNew = null)
    {
        $this->positionNew = $positionNew;

        return $this;
    }

    /**
     * Get positionNew
     *
     * @return InfoDictionary
     */
    public function getPositionNew()
    {
        return $this->positionNew;
    }

    /**
     * Set contacttypeOld
     *
     * @param InfoContacttype $contacttypeOld
     *
     * @return InfoContactrequest
     */
    public function setContactTypeOld(InfoContacttype $contacttypeOld = null)
    {
        $this->contacttypeOld = $contacttypeOld;

        return $this;
    }

    /**
     * Get contacttypeOld
     *
     * @return InfoContacttype
     */
    public function getContactTypeOld()
    {
        return $this->contacttypeOld;
    }

    /**
     * Set contacttypeNew
     *
     * @param InfoContacttype $contacttypeNew
     *
     * @return InfoContactrequest
     */
    public function setContactTypeNew(InfoContacttype $contacttypeNew = null)
    {
        $this->contacttypeNew = $contacttypeNew;

        return $this;
    }

    /**
     * Get contacttypeNew
     *
     * @return InfoContacttype
     */
    public function getContactTypeNew()
    {
        return $this->contacttypeNew;
    }

    /**
     * Set modifier
     *
     * @param InfoUser $modifier
     *
     * @return InfoContactrequest
     */
    public function setModifier(InfoUser $modifier = null)
    {
        $this->modifier = $modifier;

        return $this;
    }

    /**
     * Get modifier
     *
     * @return InfoUser
     */
    public function getModifier()
    {
        return $this->modifier;
    }

    /**
     * Set regionOld
     *
     * @param InfoRegion $regionOld
     *
     * @return InfoContactrequest
     */
    public function setRegionOld(InfoRegion $regionOld = null)
    {
        $this->regionOld = $regionOld;

        return $this;
    }

    /**
     * Get regionOld
     *
     * @return InfoRegion
     */
    public function getRegionOld()
    {
        return $this->regionOld;
    }

    /**
     * Set regionNew
     *
     * @param InfoRegion $regionNew
     *
     * @return InfoContactrequest
     */
    public function setRegionNew(InfoRegion $regionNew = null)
    {
        $this->regionNew = $regionNew;

        return $this;
    }

    /**
     * Get regionNew
     *
     * @return InfoRegion
     */
    public function getRegionNew()
    {
        return $this->regionNew;
    }

    /**
     * Set cityOld
     *
     * @param InfoCity $cityOld
     *
     * @return InfoContactrequest
     */
    public function setCityOld(InfoCity $cityOld = null)
    {
        $this->cityOld = $cityOld;

        return $this;
    }

    /**
     * Get cityOld
     *
     * @return InfoCity
     */
    public function getCityOld()
    {
        return $this->cityOld;
    }

    /**
     * Set cityNew
     *
     * @param InfoCity $cityNew
     *
     * @return InfoContactrequest
     */
    public function setCityNew(InfoCity $cityNew = null)
    {
        $this->cityNew = $cityNew;

        return $this;
    }

    /**
     * Get cityNew
     *
     * @return InfoCity
     */
    public function getCityNew()
    {
        return $this->cityNew;
    }

    /**
     * Set requesttype
     *
     * @param int $requesttype
     *
     * @return InfoContactrequest
     */
    public function setRequesttype($requesttype)
    {
        $this->requesttype = $requesttype;

        return $this;
    }

    /**
     * Get requesttype
     *
     * @return int
     */
    public function getRequesttype()
    {
        return $this->requesttype;
    }

    /**
     * Set dcrType
     *
     * @param int $dcrType
     *
     * @return InfoContactrequest
     */
    public function setDcrType($dcrType)
    {
        $this->dcrType = $dcrType;

        return $this;
    }

    /**
     * Get dcrType
     *
     * @return InfoDcrtype
     */
    public function getDcrType()
    {
        return $this->dcrType;
    }

    /**
     * Set role
     *
     * @param InfoRole $role
     *
     * @return InfoContactrequest
     */
    public function setRole($role)
    {
        $this->role = $role;

        return $this;
    }

    /**
     * Get role
     *
     * @return InfoRole
     */
    public function getRole()
    {
        return $this->role;
    }

    /**
     * Set addinfotype
     *
     * @param InfoAddinfotype $addinfotype
     *
     * @return InfoContactrequest
     */
    public function setAddinfotype(InfoAddinfotype $addinfotype = null)
    {
        $this->addinfotype = $addinfotype;

        return $this;
    }

    /**
     * Get addinfotype
     *
     * @return InfoAddinfotype
     */
    public function getAddinfotype()
    {
        return $this->addinfotype;
    }

    /**
     * Set ownerNew.
     *
     * @param InfoUser|null $ownerNew
     *
     * @return InfoContactrequest
     */
    public function setOwnerNew(InfoUser $ownerNew = null)
    {
        $this->ownerNew = $ownerNew;

        return $this;
    }

    /**
     * Get ownerNew.
     *
     * @return InfoUser|null
     */
    public function getOwnerNew()
    {
        return $this->ownerNew;
    }

    /**
     * Set ownerOld.
     *
     * @param InfoUser|null $ownerOld
     *
     * @return InfoContactrequest
     */
    public function setOwnerOld(InfoUser $ownerOld = null)
    {
        $this->ownerOld = $ownerOld;

        return $this;
    }

    /**
     * Get ownerOld.
     *
     * @return InfoUser|null
     */
    public function getOwnerOld()
    {
        return $this->ownerOld;
    }

    /**
     * Set additionalownerOld.
     *
     * @param InfoUser|null $additionalownerOld
     *
     * @return InfoContactrequest
     */
    public function setAdditionalownerOld(InfoUser $additionalownerOld = null)
    {
        $this->additionalownerOld = $additionalownerOld;

        return $this;
    }

    /**
     * Get additionalownerOld.
     *
     * @return InfoUser|null
     */
    public function getAdditionalownerOld()
    {
        return $this->additionalownerOld;
    }

    /**
     * Set additionalownerNew.
     *
     * @param InfoUser|null $additionalownerNew
     *
     * @return InfoContactrequest
     */
    public function setAdditionalownerNew(InfoUser $additionalownerNew = null)
    {
        $this->additionalownerNew = $additionalownerNew;

        return $this;
    }

    /**
     * Get additionalownerNew.
     *
     * @return InfoUser|null
     */
    public function getAdditionalownerNew()
    {
        return $this->additionalownerNew;
    }

    /**
     * Set dictvalueOld.
     *
     * @param InfoCustomdictionaryvalue|null $dictvalueOld
     *
     * @return InfoContactrequest
     */
    public function setDictvalueOld(InfoCustomdictionaryvalue $dictvalueOld = null)
    {
        $this->dictvalueOld = $dictvalueOld;

        return $this;
    }

    /**
     * Get dictvalueOld.
     *
     * @return InfoCustomdictionaryvalue|null
     */
    public function getDictvalueOld()
    {
        return $this->dictvalueOld;
    }

    /**
     * Set dictvalueNew.
     *
     * @param InfoCustomdictionaryvalue|null $dictvalueNew
     *
     * @return InfoContactrequest
     */
    public function setDictvalueNew(InfoCustomdictionaryvalue $dictvalueNew = null)
    {
        $this->dictvalueNew = $dictvalueNew;

        return $this;
    }

    /**
     * Get dictvalueNew.
     *
     * @return InfoCustomdictionaryvalue|null
     */
    public function getDictvalueNew()
    {
        return $this->dictvalueNew;
    }

    /**
     * Set user.
     *
     * @param InfoUser|null $user
     *
     * @return InfoContactrequest
     */
    public function setUser(?InfoUser $user): self
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user.
     *
     * @return InfoUser|null
     */
    public function getUser(): ?InfoUser
    {
        return $this->user;
    }

    /**
     * Set chainNum
     *
     * @param integer $chainNum
     * @return InfoContactrequest
     */
    public function setChainNum(?int $chainNum): self
    {
        $this->chainNum = $chainNum;

        return $this;
    }

    /**
     * Get chainNum
     *
     * @return integer
     */
    public function getChainNum(): ?int
    {
        return $this->chainNum;
    }
}
