<?php

namespace TeamSoft\CrmRepositoryBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * PoPhotoreportfilterrelation
 *
 * @ORM\Table(name="po_photoreportfilterrelation")
 * @ORM\Entity()
 */
class PoPhotoreportfilterrelation implements ServiceFieldInterface
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var PoPhotoreportfilter
     *
     * @ORM\ManyToOne(targetEntity="PoPhotoreportfilter", inversedBy="parentsCollection")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="child_id", referencedColumnName="id")
     * })
     */
    private $child;

    /**
     * @var PoPhotoreportfilter
     *
     * @ORM\ManyToOne(targetEntity="PoPhotoreportfilter")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="subj_id", referencedColumnName="id")
     * })
     */
    private $subj;

    /**
     * @var string
     *
     * @ORM\Column(name="custom_where", type="string", length=255, nullable=true)
     */
    private $customWhere;

    /**
     * @var DateTime|null
     *
     * @ORM\Column(name="currenttime", type="datetime", nullable=true)
     */
    private $currenttime;

    /**
     * @var string|null
     *
     * @ORM\Column(name="moduser", type="string", length=16, nullable=true)
     * )
     */
    private $moduser;

    /**
     * @var string|null
     *
     * @ORM\Column(name="guid", type="guid", nullable=true)
     */
    private $guid;


    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set customWhere
     *
     * @param string $customWhere
     *
     * @return self
     */
    public function setCustomWhere($customWhere)
    {
        $this->customWhere = $customWhere;

        return $this;
    }

    /**
     * Get customWhere
     *
     * @return string
     */
    public function getCustomWhere()
    {
        return $this->customWhere;
    }

    /**
     * Set currenttime.
     *
     * @param DateTime|null $currenttime
     *
     * @return self
     */
    public function setCurrenttime(\DateTime $currenttime = null)
    {
        $this->currenttime = $currenttime;

        return $this;
    }

    /**
     * Get currenttime.
     *
     * @return DateTime|null
     */
    public function getCurrenttime()
    {
        return $this->currenttime;
    }

    /**
     * Set moduser.
     *
     * @param string|null $moduser
     *
     * @return self
     */
    public function setModuser($moduser = null)
    {
        $this->moduser = $moduser;

        return $this;
    }

    /**
     * Get moduser.
     *
     * @return string|null
     */
    public function getModuser()
    {
        return $this->moduser;
    }

    /**
     * Set guid.
     *
     * @param string|null $guid
     *
     * @return self
     */
    public function setGuid($guid = null)
    {
        $this->guid = $guid;

        return $this;
    }

    /**
     * Get guid.
     *
     * @return string|null
     */
    public function getGuid()
    {
        return $this->guid;
    }

    /**
     * Get subj.
     *
     * @return PoPhotoreportfilter
     */
    public function getSubj()
    {
        return $this->subj;
    }

    /**
     * Set subj.
     *
     * @param PoPhotoreportfilter $subj
     *
     * @return self
     */
    public function setSubj(PoPhotoreportfilter $subj = null)
    {
        $this->subj = $subj;

        return $this;
    }
}
