<?php

namespace TeamSoft\CrmRepositoryBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * InfoPromoproject
 *
 * @ORM\Table(name="info_promoproject")
 * @ORM\Entity(repositoryClass="TeamSoft\CrmRepositoryBundle\Repository\InfoPromoprojectRepository")
 */
class InfoPromoproject implements ServiceFieldInterface
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string|null
     *
     * @ORM\Column(name="title", type="string", length=255, nullable=true)
     */
    private $title;

    /**
     * @var string|null
     *
     * @ORM\Column(name="msg", type="string", length=255, nullable=true)
     */
    private $msg;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="datefrom", type="datetime", nullable=true)
     */
    private $datefrom;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="datetill", type="datetime", nullable=true)
     */
    private $datetill;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="dateclose", type="datetime", nullable=true)
     */
    private $dateclose;

    /**
     * @var InfoPromoproject
     *
     * @ORM\ManyToOne(targetEntity="InfoPromoproject", inversedBy="childrenCollection")
     * @ORM\JoinColumn(name="parent_id", referencedColumnName="id")
     */
    private $parent;

    /**
     * @var int
     *
     * @ORM\Column(name="isarchive", type="integer", nullable=true)
     */
    private $isArchive;

    /**
     * @var \Doctrine\Common\Collections\ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="InfoPromoproject", mappedBy="parent", cascade={"persist", "remove"})
     */
    private $childrenCollection;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="currenttime", type="datetime", nullable=true)
     */
    private $currenttime;

    /**
     * @var string|null
     *
     * @ORM\Column(name="moduser", type="string", length=1, nullable=true)
     */
    private $moduser;

    /**
     * @var string|null
     *
     * @ORM\Column(name="guid", type="guid", nullable=true)
     */
    private $guid;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="datestart", type="datetime", nullable=true)
     */
    private $datestart;

    /**
     * @ORM\ManyToMany(targetEntity="InfoDirection", fetch="EAGER")
     * @ORM\JoinTable(name="info_promoprojectdirection",
     *     joinColumns={@ORM\JoinColumn(name="promoproject_id", referencedColumnName="id")},
     *     inverseJoinColumns={@ORM\JoinColumn(name="direction_id", referencedColumnName="id")}
     * )
     */
    private $directionCollection;

    /**
     * @ORM\ManyToMany(targetEntity="InfoDictionary", fetch="EAGER")
     * @ORM\JoinTable(name="info_promoprojectfilter",
     *     joinColumns={@ORM\JoinColumn(name="promoproject_id", referencedColumnName="id")},
     *     inverseJoinColumns={@ORM\JoinColumn(name="specialization_id", referencedColumnName="id")}
     * )
     */
    private $specializationCollection;

    /**
     * @var \Doctrine\Common\Collections\ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="InfoPromoprojectbrend", mappedBy="promoproject", cascade={"persist"}, orphanRemoval=true, fetch="EAGER")
     */
    private $brandCollection;

    /**
     * @var \Doctrine\Common\Collections\ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="InfoPromoprojectdetail", mappedBy="promoproject", cascade={"persist"}, orphanRemoval=true, fetch="EAGER")
     */
    private $detailCollection;

    /**
     * @var int
     *
     * @ORM\Column(name="isformcm", type="integer", nullable=true)
     */
    private $isformcm;


    /**
     * Constructor
     */
    public function __construct()
    {
        $this->childrenCollection = new \Doctrine\Common\Collections\ArrayCollection();
        $this->directionCollection = new \Doctrine\Common\Collections\ArrayCollection();
        $this->specializationCollection = new \Doctrine\Common\Collections\ArrayCollection();
        $this->brandCollection = new \Doctrine\Common\Collections\ArrayCollection();
        $this->detailCollection = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set title.
     *
     * @param string|null $title
     *
     * @return InfoPromoproject
     */
    public function setTitle($title = null)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title.
     *
     * @return string|null
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set msg.
     *
     * @param string|null $msg
     *
     * @return InfoPromoproject
     */
    public function setMsg($msg = null)
    {
        $this->msg = $msg;

        return $this;
    }

    /**
     * Get msg.
     *
     * @return string|null
     */
    public function getMsg()
    {
        return $this->msg;
    }

    /**
     * Set datefrom.
     *
     * @param \DateTime|null $datefrom
     *
     * @return InfoPromoproject
     */
    public function setDatefrom($datefrom = null)
    {
        $this->datefrom = $datefrom;

        return $this;
    }

    /**
     * Get datefrom.
     *
     * @return \DateTime|null
     */
    public function getDatefrom()
    {
        return $this->datefrom;
    }

    /**
     * Set datetill.
     *
     * @param \DateTime|null $datetill
     *
     * @return InfoPromoproject
     */
    public function setDatetill($datetill = null)
    {
        $this->datetill = $datetill;

        return $this;
    }

    /**
     * Get datetill.
     *
     * @return \DateTime|null
     */
    public function getDatetill()
    {
        return $this->datetill;
    }

    /**
     * Set dateclose.
     *
     * @param \DateTime|null $dateclose
     *
     * @return InfoPromoproject
     */
    public function setDateclose($dateclose = null)
    {
        $this->dateclose = $dateclose;

        return $this;
    }

    /**
     * Get dateclose.
     *
     * @return \DateTime|null
     */
    public function getDateclose()
    {
        return $this->dateclose;
    }

    /**
     * Set parentId.
     *
     * @param int|null $parentId
     *
     * @return InfoPromoproject
     */
    public function setParentId($parentId = null)
    {
        $this->parentId = $parentId;

        return $this;
    }

    /**
     * Get parentId.
     *
     * @return int|null
     */
    public function getParentId()
    {
        return $this->parentId;
    }

    /**
     * Set isArchive
     *
     * @param int $isArchive
     *
     * @return InfoPromoproject
     */
    public function setIsArchive(?int $isArchive): self
    {
        $this->isArchive = $isArchive;

        return $this;
    }

    /**
     * Get isArchive
     *
     * @return int
     */
    public function getIsArchive(): ?int
    {
        return $this->isArchive;
    }

    /**
     * Set currenttime.
     *
     * @param \DateTime $currenttime
     *
     * @return InfoPromoproject
     */
    public function setCurrenttime(\DateTime $currenttime)
    {
        $this->currenttime = $currenttime;

        return $this;
    }

    /**
     * Get currenttime.
     *
     * @return \DateTime|null
     */
    public function getCurrenttime()
    {
        return $this->currenttime;
    }

    /**
     * Set moduser.
     *
     * @param string|null $moduser
     *
     * @return InfoPromoproject
     */
    public function setModuser($moduser = null)
    {
        $this->moduser = $moduser;

        return $this;
    }

    /**
     * Get moduser.
     *
     * @return string|null
     */
    public function getModuser()
    {
        return $this->moduser;
    }

    /**
     * Set guid.
     *
     * @param string|null $guid
     *
     * @return InfoPromoproject
     */
    public function setGuid($guid = null)
    {
        $this->guid = $guid;

        return $this;
    }

    /**
     * Get guid.
     *
     * @return string|null
     */
    public function getGuid()
    {
        return $this->guid;
    }

    /**
     * Set datestart.
     *
     * @param \DateTime|null $datestart
     *
     * @return InfoPromoproject
     */
    public function setDatestart($datestart = null)
    {
        $this->datestart = $datestart;

        return $this;
    }

    /**
     * Get datestart.
     *
     * @return \DateTime|null
     */
    public function getDatestart()
    {
        return $this->datestart;
    }

    /**
     * Set parent.
     *
     * @param \TeamSoft\CrmRepositoryBundle\Entity\InfoPromoproject|null $parent
     *
     * @return InfoPromoproject
     */
    public function setParent(\TeamSoft\CrmRepositoryBundle\Entity\InfoPromoproject $parent = null)
    {
        $this->parent = $parent;

        return $this;
    }

    /**
     * Get parent.
     *
     * @return \TeamSoft\CrmRepositoryBundle\Entity\InfoPromoproject|null
     */
    public function getParent()
    {
        return $this->parent;
    }

    /**
     * Add childrenCollection.
     *
     * @param \TeamSoft\CrmRepositoryBundle\Entity\InfoPromoproject $childrenCollection
     *
     * @return InfoPromoproject
     */
    public function addChildrenCollection(\TeamSoft\CrmRepositoryBundle\Entity\InfoPromoproject $childrenCollection)
    {
        $this->childrenCollection->add($childrenCollection);

        return $this;
    }

    /**
     * Remove childrenCollection.
     *
     * @param \TeamSoft\CrmRepositoryBundle\Entity\InfoPromoproject $childrenCollection
     *
     * @return boolean TRUE if this collection contained the specified element, FALSE otherwise.
     */
    public function removeChildrenCollection(\TeamSoft\CrmRepositoryBundle\Entity\InfoPromoproject $childrenCollection)
    {
        return $this->childrenCollection->removeElement($childrenCollection);
    }

    /**
     * Get childrenCollection.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getChildrenCollection()
    {
        return $this->childrenCollection;
    }

    /**
     * Add directionCollection.
     *
     * @param \TeamSoft\CrmRepositoryBundle\Entity\InfoDirection $direction
     *
     * @return InfoPromoproject
     */
    public function addDirectionCollection(\TeamSoft\CrmRepositoryBundle\Entity\InfoDirection $direction)
    {
        $this->directionCollection->add($direction);

        return $this;
    }

    /**
     * Remove directionCollection.
     *
     * @param \TeamSoft\CrmRepositoryBundle\Entity\InfoDirection $direction
     *
     * @return boolean TRUE if this collection contained the specified element, FALSE otherwise.
     */
    public function removeDirectionCollection(\TeamSoft\CrmRepositoryBundle\Entity\InfoDirection $direction)
    {
        return $this->directionCollection->removeElement($direction);
    }

    /**
     * Get directionCollection.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getDirectionCollection()
    {
        return $this->directionCollection;
    }

    /**
     * Add specializationCollection.
     *
     * @param \TeamSoft\CrmRepositoryBundle\Entity\InfoDictionary $specialization
     *
     * @return InfoPromoproject
     */
    public function addSpecializationCollection(\TeamSoft\CrmRepositoryBundle\Entity\InfoDictionary $specialization)
    {
        $this->specializationCollection->add($specialization);

        return $this;
    }

    /**
     * Remove specializationCollection.
     *
     * @param \TeamSoft\CrmRepositoryBundle\Entity\InfoDictionary $specialization
     *
     * @return boolean TRUE if this collection contained the specified element, FALSE otherwise.
     */
    public function removeSpecializationCollection(\TeamSoft\CrmRepositoryBundle\Entity\InfoDictionary $specialization)
    {
        return $this->specializationCollection->removeElement($specialization);
    }

    /**
     * Get specializationCollection.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getSpecializationCollection()
    {
        return $this->specializationCollection;
    }

    /**
     * Add brandCollection.
     *
     * @param \TeamSoft\CrmRepositoryBundle\Entity\InfoPromoprojectbrend $brand
     *
     * @return InfoPromoproject
     */
    public function addBrandCollection(\TeamSoft\CrmRepositoryBundle\Entity\InfoPromoprojectbrend $brand)
    {
        $brand->setPromoproject($this);
        $this->brandCollection->add($brand);

        return $this;
    }

    /**
     * Remove brandCollection.
     *
     * @param \TeamSoft\CrmRepositoryBundle\Entity\InfoPromoprojectbrend $brand
     *
     * @return boolean TRUE if this collection contained the specified element, FALSE otherwise.
     */
    public function removeBrandCollection(\TeamSoft\CrmRepositoryBundle\Entity\InfoPromoprojectbrend $brand)
    {
        $brand->setPromoproject(null);
        return $this->brandCollection->removeElement($brand);
    }

    /**
     * Get brandCollection.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getBrandCollection()
    {
        return $this->brandCollection;
    }

    /**
     * Add detailCollection.
     *
     * @param \TeamSoft\CrmRepositoryBundle\Entity\InfoPromoprojectdetail $detail
     *
     * @return InfoPromoproject
     */
    public function addDetailCollection(\TeamSoft\CrmRepositoryBundle\Entity\InfoPromoprojectdetail $detail)
    {
        $detail->setPromoproject($this);
        $this->detailCollection->add($detail);

        return $this;
    }

    /**
     * Remove detailCollection.
     *
     * @param \TeamSoft\CrmRepositoryBundle\Entity\InfoPromoprojectdetail $detail
     *
     * @return boolean TRUE if this collection contained the specified element, FALSE otherwise.
     */
    public function removeDetailCollection(\TeamSoft\CrmRepositoryBundle\Entity\InfoPromoprojectdetail $detail)
    {
        $detail->setPromoproject(null);
        return $this->detailCollection->removeElement($detail);
    }

    /**
     * Get detailCollection.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getDetailCollection()
    {
        return $this->detailCollection;
    }

    /**
     * Add companyCollection.
     *
     * @param \TeamSoft\CrmRepositoryBundle\Entity\InfoPromoprojectcompany $company
     *
     * @return InfoPromoproject
     */
    public function addCompanyCollection(\TeamSoft\CrmRepositoryBundle\Entity\InfoPromoprojectcompany $company)
    {
        $company->setPromoproject($this);
        $this->companyCollection->add($company);

        return $this;
    }


    public function getQuarterNumber()
    {
        /**
         * @var \TeamSoft\CrmRepositoryBundle\Entity\InfoPromoprojectdetail $detail
         */
        $quarterNumber = null;
        foreach ($this->getDetailCollection() as $detail) {
            if ($detail->getNquarter() > $quarterNumber) {
                $quarterNumber = $detail->getNquarter();
            }
        }
        return $quarterNumber;
    }

    public function getStatus()
    {
        $date = new \DateTime('00:00');
        if ($this->datefrom !== null && $this->datetill !== null && $this->datefrom <= $date && $date <= $this->datetill) {
            return 1;
        } else if ($this->datefrom !== null && $this->datefrom > $date) {
            return 2;
        } else if ($this->datetill !== null && $this->datetill < $date) {
            return 0;
        } else if ($this->datefrom !== null && $this->datefrom <= $date) {
            return 1;
        } else if ($this->datetill !== null && $this->datetill >= $date) {
            return 1;
        } else {
            return -1;
        }
    }

    public function getCurrentQuarter()
    {
        $getQuarterDates = function ($year, $quarterNumber) {
            switch ($quarterNumber) {
                case 1:
                    return [
                        \DateTime::createFromFormat('Y-m-d', "$year-04-01"),
                        \DateTime::createFromFormat('Y-m-d', "$year-06-30")
                    ];
                case 2:
                    return [
                        \DateTime::createFromFormat('Y-m-d', "$year-07-01"),
                        \DateTime::createFromFormat('Y-m-d', "$year-09-30")
                    ];
                case 3:
                    return [
                        \DateTime::createFromFormat('Y-m-d', "$year-10-01"),
                        \DateTime::createFromFormat('Y-m-d', "$year-12-31")
                    ];
                case 4:
                    $year += 1;
                    return [
                        \DateTime::createFromFormat('Y-m-d', "$year-01-01"),
                        \DateTime::createFromFormat('Y-m-d', "$year-03-31")
                    ];
            }
        };
        $date = new \DateTime('00:00');
        $year = $this->datefrom !== null ? (int)$this->datefrom->format('Y') : null;
        for ($quarterNumber = 1; $quarterNumber <= 4; $quarterNumber++) {
            list($startDate, $endDate) = $getQuarterDates($year, $quarterNumber);
            if ($startDate <= $date && $date <= $endDate) {
                return $quarterNumber;
            }
        }
        return null;
    }

    /**
     * Set isformcm
     *
     * @param int $isformcm
     *
     * @return InfoPromoproject
     */
    public function setIsformcm($isformcm)
    {
        $this->isformcm = $isformcm;

        return $this;
    }

    /**
     * Get isformcm
     *
     * @return int
     */
    public function getIsformcm()
    {
        return $this->isformcm;
    }
}
