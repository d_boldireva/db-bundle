<?php

namespace TeamSoft\CrmRepositoryBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use TeamSoft\CrmRepositoryBundle\Model\InfoFile;

/**
 * InfoTaskfile
 *
 * @ORM\Table(name="info_taskfile")
 * @ORM\Entity
 */
class InfoTaskfile extends InfoFile implements ServiceFieldInterface
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255, nullable=true)
     */
    private $name;

    /**
     * @var resource | string
     *
     * @ORM\Column(name="content", type="blob_or_string", nullable=true)
     */
    private $content;

    /**
     * @var integer
     *
     * @ORM\Column(name="fsize", type="integer", nullable=true)
     */
    private $fsize;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="fmodified", type="datetime", nullable=true)
     */
    private $fmodified;

    /**
     * @var guid
     *
     * @ORM\Column(name="guid", type="guid", nullable=true)
     */
    private $guid;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="currenttime", type="datetime", nullable=true)
     */
    private $currenttime;

    /**
     * @var string
     *
     * @ORM\Column(name="moduser", type="string", length=16, nullable=true)
     */
    private $moduser;

    /**
     * @var \TeamSoft\CrmRepositoryBundle\Entity\InfoTaskfilegroup
     *
     * @ORM\ManyToOne(targetEntity="InfoTaskfilegroup")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="subj_id", referencedColumnName="id")
     * })
     */
    protected $subj;

    /**
     * @var int
     *
     * @ORM\Column(name="photostate", type="integer", nullable=true)
     */
    private $photostateCustom;

    /**
     * @var InfoCustomdictionaryvalue
     *
     * @ORM\OneToOne(targetEntity="InfoCustomdictionaryvalue")
     * @ORM\JoinColumn(name="photostate_id", referencedColumnName="id")
     */
    private $photostate;

    /**
     * @var InfoCustomdictionaryvalue
     *
     * @ORM\OneToOne(targetEntity="InfoCustomdictionaryvalue")
     * @ORM\JoinColumn(name="whatsinside", referencedColumnName="id")
     */
    private $whatsinside;

    /**
     * @var InfoPreparationbrend
     *
     * @ORM\OneToOne(targetEntity="InfoPreparationbrend")
     * @ORM\JoinColumn(name="photobrand", referencedColumnName="id")
     */
    private $photobrand;

    /**
     * @var InfoCustomdictionaryvalue
     *
     * @ORM\OneToOne(targetEntity="InfoCustomdictionaryvalue")
     * @ORM\JoinColumn(name="photoservicetype", referencedColumnName="id")
     */
    private $photoservicetype;

    /**
     * @var string
     *
     * @ORM\Column(name="comment", type="string", length=2500, nullable=true)
     */
    private $comment;

    /**
     * @var InfoPreparation
     *
     * @ORM\ManyToOne(targetEntity="InfoPreparation")
     * @ORM\JoinColumn(name="photoprep", referencedColumnName="id")
     */
    private $photoprep;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return InfoTaskfile
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set content
     *
     * @param string $content
     *
     * @return InfoTaskfile
     */
    public function setContent($content)
    {
        $this->content = $content;

        return $this;
    }

    /**
     * Get content
     *
     * @return resource | string
     */
    public function getContent()
    {
        return $this->content;
    }

    /**
     * Set fsize
     *
     * @param integer $fsize
     *
     * @return InfoTaskfile
     */
    public function setFsize($fsize)
    {
        $this->fsize = $fsize;

        return $this;
    }

    /**
     * Get fsize
     *
     * @return integer
     */
    public function getFsize()
    {
        return $this->fsize;
    }

    /**
     * Set fmodified
     *
     * @param \DateTime $fmodified
     *
     * @return InfoTaskfile
     */
    public function setFmodified($fmodified)
    {
        $this->fmodified = $fmodified;

        return $this;
    }

    /**
     * Get fmodified
     *
     * @return \DateTime
     */
    public function getFmodified()
    {
        return $this->fmodified;
    }

    /**
     * Set guid
     *
     * @param guid $guid
     *
     * @return InfoTaskfile
     */
    public function setGuid($guid)
    {
        $this->guid = $guid;

        return $this;
    }

    /**
     * Get guid
     *
     * @return guid
     */
    public function getGuid()
    {
        return $this->guid;
    }

    /**
     * Set currenttime
     *
     * @param \DateTime $currenttime
     *
     * @return InfoTaskfile
     */
    public function setCurrenttime(\DateTime $currenttime)
    {
        $this->currenttime = $currenttime;

        return $this;
    }

    /**
     * Get currenttime
     *
     * @return \DateTime
     */
    public function getCurrenttime()
    {
        return $this->currenttime;
    }

    /**
     * Set moduser
     *
     * @param string $moduser
     *
     * @return InfoTaskfile
     */
    public function setModuser($moduser)
    {
        $this->moduser = $moduser;

        return $this;
    }

    /**
     * Get moduser
     *
     * @return string
     */
    public function getModuser()
    {
        return $this->moduser;
    }

    /**
     * Set subj
     *
     * @param \TeamSoft\CrmRepositoryBundle\Entity\InfoTaskfilegroup $subj
     *
     * @return InfoTaskfile
     */
    public function setSubj(\TeamSoft\CrmRepositoryBundle\Entity\InfoTaskfilegroup $subj = null)
    {
        $this->subj = $subj;

        return $this;
    }

    /**
     * Get subj
     *
     * @return \TeamSoft\CrmRepositoryBundle\Entity\InfoTaskfilegroup
     */
    public function getSubj()
    {
        return $this->subj;
    }

    /**
     * Get photostateCustom
     *
     * @return null|int
     */
    public function getPhotostateCustom(): ?int
    {
        return $this->photostateCustom;
    }

    /**
     * Set photostateCustom
     *
     * @param null|int $photostateCustom
     * @return self
     */
    public function setPhotostateCustom(?int $photostateCustom): self
    {
        $this->photostateCustom = $photostateCustom;

        return $this;
    }

    /**
     * Get what's inside
     *
     * @return null|InfoCustomdictionaryvalue
     */
    public function getWhatsInside(): ?InfoCustomdictionaryvalue
    {
        return $this->whatsinside;
    }

    /**
     * Set what's inside
     *
     * @param null|InfoCustomdictionaryvalue $whatsinside
     * @return self
     */
    public function setWhatsInside(?InfoCustomdictionaryvalue $whatsinside): self
    {
        $this->whatsinside = $whatsinside;

        return $this;
    }

    /**
     * Get photostate
     *
     * @return null|InfoCustomdictionaryvalue
     */
    public function getPhotostate(): ?InfoCustomdictionaryvalue
    {
        return $this->photostate;
    }

    /**
     * Set photostate
     *
     * @param null|InfoCustomdictionaryvalue $photostate
     * @return self
     */
    public function setPhotostate(?InfoCustomdictionaryvalue $photostate): self
    {
        $this->photostate = $photostate;

        return $this;
    }

    /**
     * Get Brand
     *
     * @return null|InfoPreparationbrend
     */
    public function getPhotoBrand() : ?InfoPreparationbrend{
        return $this->photobrand;
    }

    /**
     * Set brand
     *
     * @param null|InfoPreparationbrend $photobrand
     * @return InfoTaskfile
     */
    public function setPhotoBrand(?InfoPreparationbrend $photobrand) : InfoTaskfile{
        $this->photobrand = $photobrand;

        return $this;
    }

    /**
     * Get service type
     *
     * @return null|InfoCustomdictionaryvalue
     */
    public function getServiceType() : ?InfoCustomdictionaryvalue{
        return $this->photoservicetype;
    }

    /**
     * Set service type
     *
     * @param null|InfoCustomdictionaryvalue $serviceType
     * @return InfoTaskfile
     */
    public function setServiceType(?InfoCustomdictionaryvalue $serviceType) : InfoTaskfile{
        $this->photoservicetype = $serviceType;

        return $this;
    }

    /**
     * Get comment
     *
     * @return null|string
     */
    public function getComment() : ?string{
        return $this->comment;
    }

    /**
     * Set comment
     *
     * @param null|string $comment
     * @return InfoTaskfile
     */
    public function setComment(?string $comment) : InfoTaskfile{
        $this->comment = $comment;

        return $this;
    }

    /**
     * Get photo prep
     *
     * @return InfoPreparation|null
     */
    public function getPhotoPrep() {
        return $this->photoprep;
    }

    /**
     * Set photo prep
     *
     * @param InfoPreparation $prep
     * @return $this
     */
    public function setPhotoPrep(?InfoPreparation $prep) {
        $this->photoprep = $prep;
        return $this;
    }
}
