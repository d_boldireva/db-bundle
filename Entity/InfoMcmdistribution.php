<?php

namespace TeamSoft\CrmRepositoryBundle\Entity;

use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * InfoMcmdistribution
 *
 * @ORM\Table(name="info_mcmdistribution")
 * @ORM\Entity(repositoryClass="TeamSoft\CrmRepositoryBundle\Repository\InfoMcmdistribution")
 */
class InfoMcmdistribution implements ServiceFieldInterface, DateTimeWithGmtOffsetInterface
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255, nullable=true)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="locale", type="string", length=2, nullable=true)
     */
    private $locale;

    /**
     * @var boolean
     *
     * @ORM\Column(name="resend", type="integer", length=1, nullable=true)
     */
    private $resend;

    /**
     * @var boolean
     *
     * @ORM\Column(name="isformp", type="integer", length=1, nullable=true)
     */
    private $isformp;

    /**
     * @var boolean
     *
     * @ORM\Column(name="iswebinar", type="integer", length=1, nullable=true)
     */
    private $iswebinar;

    /**
     * @var boolean
     *
     * @ORM\Column(name="isactive", type="integer", length=1, nullable=true)
     */
    private $isactive;

    /**
     * @var string
     *
     * @ORM\Column(name="footer_language", type="string", length=16, nullable=true)
     */
    private $footerLanguage;

    /**
     * @var boolean
     *
     * @ORM\Column(name="shortify", type="integer", length=1, nullable=true)
     */
    private $shortify;

    /**
     * One distribution has one content template
     *
     * @var InfoMcmcontent
     *
     * @ORM\ManyToOne(targetEntity="InfoMcmcontent", inversedBy="distributionCollection")
     * @ORM\JoinColumn(name="mcmcontent_id", referencedColumnName="id")
     */
    private $mcmcontent;

    /**
     * One distribution has one sender name
     *
     * @var InfoMcmfrom
     *
     * @ORM\OneToOne(targetEntity="InfoMcmfrom")
     * @ORM\JoinColumn(name="mcmfrom_id", referencedColumnName="id")
     */
    private $mcmfrom;

    /**
     * One distribution has one sender name
     *
     * @var InfoMcmfrom
     *
     * @ORM\OneToOne(targetEntity="InfoMcmfrom")
     * @ORM\JoinColumn(name="resend_mcmfrom_id", referencedColumnName="id")
     *
     */
    private $resendMcmfrom;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="accept_datefrom", type="datetime", nullable=true)
     */
    private $acceptDatefrom;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="reminder", type="datetime", nullable=true)
     */
    private $reminder;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="accept_datetill", type="datetime", nullable=true)
     */
    private $acceptDatetill;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="datefrom", type="datetime", nullable=true)
     */
    private $datefrom;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="datetill", type="datetime", nullable=true)
     */
    private $datetill;

    /**
     * @var string
     *
     * @ORM\Column(name="accept_type", type="string", length=255, nullable=true)
     */
    private $acceptType;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="createdate", type="datetime", nullable=true)
     */
    private $createdate;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dt", type="datetime", nullable=true)
     */
    private $dt;

    /**
     * Many Features have One Product.
     * @ORM\ManyToOne(targetEntity="InfoMcmdistribution", inversedBy="children")
     * @ORM\JoinColumn(name="parent_id", referencedColumnName="id")
     */
    private $parent;

    /**
     * One distribution has one conference
     *
     * @var InfoMcmconference
     *
     * @ORM\ManyToOne(targetEntity="InfoMcmconference")
     * @ORM\JoinColumn(name="conference_id", referencedColumnName="id")
     */
    private $conference;

    /**
     * @var string
     *
     * @ORM\Column(name="state", type="string", length=255, nullable=true)
     */
    private $state;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="currenttime", type="datetime", nullable=true)
     */
    private $currenttime;

    /**
     * @var string
     *
     * @ORM\Column(name="moduser", type="string", length=16, nullable=true)
     */
    private $moduser;

    /**
     * @var string
     *
     * @ORM\Column(name="guid", type="guid", nullable=true)
     */
    private $guid;

    /**
     * @var InfoUser
     *
     * @ORM\ManyToOne(targetEntity="InfoUser")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="owner_id", referencedColumnName="id")
     * })
     */
    private $owner;

    /**
     * @var InfoMcmdistribution
     * @ORM\OneToMany(targetEntity="InfoMcmdistribution", mappedBy="parent", cascade={"persist", "remove"})
     * @ORM\OrderBy({"dt" = "DESC"})
     */
    private $children;

    /**
     * Many distributions has many target audiences
     *
     * @var InfoMcmtarget
     *
     * @ORM\ManyToMany(targetEntity="InfoMcmtarget")
     * @ORM\JoinTable(
     *   name="info_mcmdistributiontarget",
     *   joinColumns={@ORM\JoinColumn(name="mcmdistribution_id", referencedColumnName="id")},
     *   inverseJoinColumns={@ORM\JoinColumn(name="mcmtarget_id", referencedColumnName="id")}
     * )
     */
    private $mcmTargets;

    /**
     * @var \DateTime
     */
    private $minSendDate;

    /**
     * @var integer
     *
     * @ORM\Column(name="gmt_offset", type="integer", nullable=true)
     */
    private $gmtOffset;

    /**
     * @var int
     *
     * @ORM\Column(name="countsms", type="integer", nullable=true)
     */
    private $countSms;

    /**
     * @var int
     *
     * @ORM\Column(name="isarchive", type="integer", nullable=true)
     */
    private $isArchive;

    /**
     * @var int
     *
     * @ORM\Column(name="for_site", type="integer", nullable=true)
     */
    private $forSite;

    /**
     * Many distributions has many directions
     *
     * @var InfoDirection
     *
     * @ORM\ManyToMany(targetEntity="InfoDirection")
     * @ORM\JoinTable(
     *   name="info_mcmdistributiondirection",
     *   joinColumns={@ORM\JoinColumn(name="distribution_id", referencedColumnName="id")},
     *   inverseJoinColumns={@ORM\JoinColumn(name="direction_id", referencedColumnName="id")}
     * )
     */
    private $directions;

    /**
     * Many distributions has many regions
     *
     * @var InfoRegion
     *
     * @ORM\ManyToMany(targetEntity="InfoRegion")
     * @ORM\JoinTable(
     *   name="info_mcmdistributiondirection",
     *   joinColumns={@ORM\JoinColumn(name="distribution_id", referencedColumnName="id")},
     *   inverseJoinColumns={@ORM\JoinColumn(name="region_id", referencedColumnName="id")}
     * )
     */
    private $regions;

    /**
     * Many distributions has many targets
     *
     * @var InfoTarget
     *
     * @ORM\ManyToMany(targetEntity="InfoTarget")
     * @ORM\JoinTable(
     *   name="info_mcmdistributiondirection",
     *   joinColumns={@ORM\JoinColumn(name="distribution_id", referencedColumnName="id")},
     *   inverseJoinColumns={@ORM\JoinColumn(name="target_id", referencedColumnName="id")}
     * )
     */
    private $targets;

    /**
     * Many distributions has many users
     *
     * @var InfoUser
     *
     * @ORM\ManyToMany(targetEntity="InfoUser")
     * @ORM\JoinTable(
     *   name="info_mcmdistributiondirection",
     *   joinColumns={@ORM\JoinColumn(name="distribution_id", referencedColumnName="id")},
     *   inverseJoinColumns={@ORM\JoinColumn(name="user_id", referencedColumnName="id")}
     * )
     */
    private $users;

    /**
     * Many distributions has many submissions
     *
     * @var InfoUser
     *
     * @ORM\ManyToMany(targetEntity="InfoUser")
     * @ORM\JoinTable(
     *   name="info_mcmdistributiondirection",
     *   joinColumns={@ORM\JoinColumn(name="distribution_id", referencedColumnName="id")},
     *   inverseJoinColumns={@ORM\JoinColumn(name="submission_id", referencedColumnName="id")}
     * )
     */
    private $submissions;

    /**
     * One distribution has one brand name
     *
     * @var InfoBrand
     *
     * @ORM\OneToOne(targetEntity="InfoBrand")
     * @ORM\JoinColumn(name="brand_id", referencedColumnName="id")
     */
    private $brand;

    /**
     *
     * @ORM\ManyToOne(targetEntity="InfoMcmdistribution")
     * @ORM\JoinColumn(name="trigger_parent_id", referencedColumnName="id")
     */
    private $triggerParent;

    /**
     * One distribution has one trigger
     *
     * @var InfoMcmtrigger
     *
     * @ORM\ManyToOne(targetEntity="InfoMcmtrigger", cascade={"persist"})
     * @ORM\JoinColumn(name="trigger_id", referencedColumnName="id")
     */
    private $trigger;

    /**
     * @var int
     *
     * @ORM\Column(name="invite", type="integer", nullable=true)
     */
    private $invite;

    /**
     * @var int
     *
     * @ORM\Column(name="is_link", type="integer", nullable=true)
     */
    private $isLink;

    /**
     * @var int
     *
     * @ORM\Column(name="accept_filter", type="integer", nullable=true)
     */
    private $acceptFilter;

    /**
     * @var string
     *
     * @ORM\Column(name="tag", type="string", length=255, nullable=true)
     */
    private $tag;

    /**
     * One distribution may have one landing page
     *
     * @var InfoMcmcontent
     *
     * @ORM\ManyToOne(targetEntity="InfoMcmcontent")
     * @ORM\JoinColumn(name="landing_content_id", referencedColumnName="id")
     */
    private $landingContent;


    /**
     * Constructor
     */
    public function __construct()
    {
        $this->children = new ArrayCollection();
        $this->mcmTargets = new ArrayCollection();
        $this->directions = new ArrayCollection();
        $this->regions = new ArrayCollection();
        $this->targets = new ArrayCollection();
        $this->users = new ArrayCollection();
        $this->submissions = new ArrayCollection();
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return InfoMcmdistribution
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set locale
     *
     * @param string $locale
     *
     * @return InfoMcmdistribution
     */
    public function setLocale($locale)
    {
        $this->locale = $locale;

        return $this;
    }

    /**
     * Get locale
     *
     * @return string
     */
    public function getLocale()
    {
        return $this->locale;
    }

    /**
     * Set resend
     *
     * @param boolean $resend
     *
     * @return InfoMcmdistribution
     */
    public function setResend($resend)
    {
        $this->resend = $resend;

        return $this;
    }

    /**
     * Get resend
     *
     * @return boolean
     */
    public function getResend()
    {
        return $this->resend;
    }

    /**
     * Set isformp
     *
     * @param boolean $isformp
     *
     * @return InfoMcmdistribution
     */
    public function setIsformp($isformp)
    {
        $this->isformp = $isformp;

        return $this;
    }

    /**
     * Get isformp
     *
     * @return boolean
     */
    public function getIsformp()
    {
        return $this->isformp;
    }

    /**
     * Set iswebinar
     *
     * @param boolean $iswebinar
     *
     * @return InfoMcmdistribution
     */
    public function setIswebinar($iswebinar)
    {
        $this->iswebinar = $iswebinar;

        return $this;
    }

    /**
     * Get iswebinar
     *
     * @return boolean
     */
    public function getIswebinar()
    {
        return $this->iswebinar;
    }

    /**
     * Set isactive
     *
     * @param boolean $isactive
     *
     * @return InfoMcmdistribution
     */
    public function setIsactive($isactive)
    {
        $this->isactive = $isactive;

        return $this;
    }

    /**
     * Get isactive
     *
     * @return boolean
     */
    public function getIsactive()
    {
        return $this->isactive;
    }

    /**
     * Set footerLanguage
     *
     * @param string $footerLanguage
     *
     * @return InfoMcmdistribution
     */
    public function setFooterLanguage($footerLanguage)
    {
        $this->footerLanguage = $footerLanguage;

        return $this;
    }

    /**
     * Get footerLanguage
     *
     * @return string
     */
    public function getFooterLanguage()
    {
        return $this->footerLanguage;
    }

    /**
     * Set shortify
     *
     * @param boolean $shortify
     *
     * @return InfoMcmdistribution
     */
    public function setShortify($shortify)
    {
        $this->shortify = $shortify;

        return $this;
    }

    /**
     * Get shortify
     *
     * @return boolean
     */
    public function getShortify()
    {
        return $this->shortify;
    }

    /**
     * Set mcmcontent
     *
     * @param InfoMcmcontent $mcmcontent
     *
     * @return InfoMcmdistribution
     */
    public function setMcmcontent(InfoMcmcontent $mcmcontent = null)
    {
        $this->mcmcontent = $mcmcontent;

        return $this;
    }

    /**
     * Get mcmcontent
     *
     * @return InfoMcmcontent
     */
    public function getMcmcontent()
    {
        return $this->mcmcontent;
    }

    /**
     * Set mcmfrom
     *
     * @param InfoMcmfrom $mcmfrom
     *
     * @return InfoMcmdistribution
     */
    public function setMcmfrom(InfoMcmfrom $mcmfrom = null)
    {
        $this->mcmfrom = $mcmfrom;

        return $this;
    }

    /**
     * Get mcmfrom
     *
     * @return InfoMcmfrom
     */
    public function getMcmfrom()
    {
        return $this->mcmfrom;
    }

    /**
     * Set resendMcmfrom
     *
     * @param InfoMcmfrom $resendMcmfrom
     *
     * @return InfoMcmdistribution
     */
    public function setResendMcmfrom(InfoMcmfrom $resendMcmfrom = null)
    {
        $this->resendMcmfrom = $resendMcmfrom;

        return $this;
    }

    /**
     * Get resendMcmfrom
     *
     * @return InfoMcmfrom
     */
    public function getResendMcmfrom()
    {
        return $this->resendMcmfrom;
    }

    /**
     * Set conference
     *
     * @param InfoMcmconference $conference
     *
     * @return InfoMcmdistribution
     */
    public function setConference(InfoMcmconference $conference = null)
    {
        $this->conference = $conference;

        return $this;
    }

    /**
     * Get conference
     *
     * @return InfoMcmconference
     */
    public function getConference()
    {
        return $this->conference;
    }

    /**
     * Set createdate
     *
     * @param \DateTime $createdate
     *
     * @return InfoMcmdistribution
     */
    public function setCreatedate(\DateTime $createdate)
    {
        $this->createdate = $createdate;

        return $this;
    }

    /**
     * Get createdate
     *
     * @return \DateTime
     */
    public function getCreatedate()
    {
        return $this->createdate;
    }

    /**
     * Setdt
     *
     * @param \DateTime $dt
     *
     * @return InfoMcmdistribution
     */
    public function setDt(\DateTime $dt = null)
    {
        $this->dt = $dt;

        return $this;
    }

    /**
     * Get dt
     *
     * @return \DateTime
     */
    public function getDt()
    {
        return $this->dt;
    }

    /**
     * set acceptDatefrom
     *
     * @param \DateTime $acceptDatefrom
     *
     * @return InfoMcmdistribution
     */
    public function setAcceptDatefrom(\DateTime $acceptDatefrom = null)
    {
        $this->acceptDatefrom = $acceptDatefrom;

        return $this;
    }

    /**
     * Get acceptDatefrom
     *
     * @return \DateTime
     */
    public function getAcceptDatefrom()
    {
        return $this->acceptDatefrom;
    }

    /**
     * Set reminder
     *
     * @param \DateTime $reminder
     *
     * @return InfoMcmdistribution
     */
    public function setReminder(\DateTime $reminder = null)
    {
        $this->reminder = $reminder;

        return $this;
    }

    /**
     * Get reminder
     *
     * @return \DateTime
     */
    public function getReminder()
    {
        return $this->reminder;
    }

    /**
     * set datefrom
     *
     * @param \DateTime $datefrom
     *
     * @return InfoMcmdistribution
     */
    public function setDatefrom(\DateTime $datefrom = null)
    {
        $this->datefrom = $datefrom;

        return $this;
    }

    /**
     * Get datefrom
     *
     * @return \DateTime
     */
    public function getDatefrom()
    {
        return $this->datefrom;
    }

    /**
     * set acceptDatetill
     *
     * @param \DateTime $acceptDatetill
     *
     * @return InfoMcmdistribution
     */
    public function setAcceptDatetill(\DateTime $acceptDatetill = null)
    {
        $this->acceptDatetill = $acceptDatetill;

        return $this;
    }

    /**
     * Get acceptDatetill
     *
     * @return \DateTime
     */
    public function getAcceptDatetill()
    {
        return $this->acceptDatetill;
    }

    /**
     * set datetill
     *
     * @param \DateTime $datetill
     *
     * @return InfoMcmdistribution
     */
    public function setDatetill(\DateTime $datetill = null)
    {
        $this->datetill = $datetill;

        return $this;
    }

    /**
     * Get datetill
     *
     * @return \DateTime
     */
    public function getDatetill()
    {
        return $this->datetill;
    }

    /**
     * Set acceptType
     *
     * @param string $acceptType
     *
     * @return InfoMcmdistribution
     */
    public function setAcceptType($acceptType)
    {
        if (is_array($acceptType)) {
            $acceptType = implode(',', $acceptType);
        }

        $this->acceptType = $acceptType;

        return $this;
    }

    /**
     * Get acceptType
     *
     * @return array
     */
    public function getAcceptType()
    {
        return $this->acceptType ? explode(',', $this->acceptType) : null;
    }

    /**
     * Get acceptType
     *
     * @return array
     */
    public function getAcceptTypesCollection()
    {
        return $this->acceptType ? explode(',', $this->acceptType) : null;
    }

    /**
     * Set parent
     *
     * @param InfoMcmdistribution $parent
     *
     * @return InfoMcmdistribution
     */
    public function setParent($parent)
    {
        $this->parent = $parent;

        return $this;
    }

    /**
     * Get parent
     *
     * @return InfoMcmdistribution
     */
    public function getParent()
    {
        return $this->parent;
    }

    /**
     * Set state
     *
     * @param string $state
     *
     * @return InfoMcmdistribution
     */
    public function setState($state)
    {
        $this->state = $state;

        return $this;
    }

    /**
     * Get state
     *
     * @return string
     */
    public function getState()
    {
        return $this->state;
    }

    /**
     * Set currenttime
     *
     * @param \DateTime $currenttime
     *
     * @return InfoMcmdistribution
     */
    public function setCurrenttime(\DateTime $currenttime)
    {
        $this->currenttime = $currenttime;

        return $this;
    }

    /**
     * Get currenttime
     *
     * @return \DateTime
     */
    public function getCurrenttime()
    {
        return $this->currenttime;
    }

    /**
     * Set moduser
     *
     * @param string $moduser
     *
     * @return InfoMcmdistribution
     */
    public function setModuser($moduser)
    {
        $this->moduser = $moduser;

        return $this;
    }

    /**
     * Get moduser
     *
     * @return string
     */
    public function getModuser()
    {
        return $this->moduser;
    }

    /**
     * Set guid
     *
     * @param string $guid
     *
     * @return InfoMcmdistribution
     */
    public function setGuid($guid)
    {
        $this->guid = $guid;

        return $this;
    }

    /**
     * Get guid
     *
     * @return string
     */
    public function getGuid()
    {
        return $this->guid;
    }

    /**
     * Add children
     *
     * @param InfoMcmdistribution $child
     * @return InfoMcmtarget
     */
    public function addChildren(InfoMcmdistribution $child)
    {
        $child->setParent($this);
        $this->children->add($child);
        return $this;
    }

    /**
     * Remove children
     *
     * @param InfoMcmdistribution $child
     */
    public function removeChildren(InfoMcmdistribution $child)
    {
        $child->setParent(null);
        $this->children->removeElement($child);
    }

    /**
     * Get children
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getChildren()
    {
        return $this->children;
    }

    /**
     * Add mcmTargets
     *
     * @param InfoMcmtarget $mcmTargets
     * @return InfoMcmtarget
     */
    public function addMcmTargets(InfoMcmtarget $mcmTargets)
    {
        $this->mcmTargets->add($mcmTargets);
        return $this;
    }

    /**
     * Remove mcmTargets
     *
     * @param InfoMcmtarget $mcmtarget
     */
    public function removeMcmTargets(InfoMcmtarget $mcmTarget)
    {
        $this->mcmTargets->removeElement($mcmTarget);
    }

    /**
     * Get mcmTargets
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getMcmTargets()
    {
        return $this->mcmTargets;
    }

    /**
     * Set minSendDate
     *
     * @param \DateTime $minSendDate
     *
     * @return InfoMcmdistribution
     */
    public function setMinSendDate($minSendDate)
    {
        $this->minSendDate = $minSendDate;

        return $this;
    }

    /**
     * Get minSendDate
     *
     * @return \DateTime
     */
    public function getMinSendDate()
    {
        return $this->minSendDate;
    }

    /**
     * Set gmtOffset.
     *
     * @param int|null $gmtOffset
     *
     * @return InfoMcmdistribution
     */
    public function setGmtOffset(int $gmtOffset = null)
    {
        $this->gmtOffset = $gmtOffset;

        return $this;
    }

    /**
     * Get gmtOffset.
     *
     * @return int|null
     */
    public function getGmtOffset()
    {
        return $this->gmtOffset;
    }

    public function getDateTimeWithGmtOffsetPropertyNameList()
    {
        return ['datefrom', 'datetill'];
    }

    /**
     * Get countSms
     *
     * @return int
     */
    public function getCountSms()
    {
        return $this->countSms;
    }

    /**
     * Set countSms
     *
     * @param int $countSms
     *
     * @return InfoMcmdistribution
     */
    public function setCountSms($countSms)
    {
        $this->countSms = $countSms;

        return $this;
    }

    /**
     * Set isArchive
     *
     * @param int $isArchive
     *
     * @return InfoMcmdistribution
     */
    public function setIsArchive($isArchive)
    {
        $this->isArchive = $isArchive;

        return $this;
    }

    /**
     * Get isArchive
     *
     * @return int
     */
    public function getIsArchive()
    {
        return $this->isArchive;
    }

    /**
     * Set forSite
     *
     * @param int $forSite
     *
     * @return InfoMcmdistribution
     */
    public function setForSite($forSite)
    {
        $this->forSite = $forSite;

        return $this;
    }

    /**
     * Get forSite
     *
     * @return int
     */
    public function getForSite()
    {
        return $this->forSite;
    }

    /**
     * Add direction
     *
     * @param InfoDirection $direction
     *
     * @return InfoMcmdistribution
     */
    public function addDirection(InfoDirection $direction)
    {
        $this->directions->add($direction);

        return $this;
    }

    /**
     * Remove direction
     *
     * @param InfoDirection $direction
     */
    public function removeDirection(InfoDirection $direction)
    {
        $this->directions->removeElement($direction);
    }

    /**
     * Get directions
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getDirections(): Collection
    {
        return $this->directions;
    }

    /**
     * Add region
     *
     * @param InfoRegion $direction
     *
     * @return InfoMcmdistribution
     */
    public function addRegion(InfoRegion $region): self
    {
        $this->regions->add($region);

        return $this;
    }

    /**
     * Remove region
     *
     * @param InfoDirection $region
     */
    public function removeRegion(InfoRegion $region): self
    {
        $this->regions->removeElement($region);

        return $this;
    }

    /**
     * Get regions
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getRegions()
    {
        return $this->regions;
    }

    /**
     * Add target
     *
     * @param InfoTarget $direction
     *
     * @return InfoMcmdistribution
     */
    public function addTarget(InfoTarget $target): self
    {
        $this->targets->add($target);

        return $this;
    }

    /**
     * Remove target
     *
     * @param InfoDirection $target
     */
    public function removeTarget(InfoTarget $target): self
    {
        $this->targets->removeElement($target);

        return $this;
    }

    /**
     * Get targets
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getTargets(): Collection
    {
        return $this->targets;
    }

    /**
     * Add user
     *
     * @param InfoUser $user
     *
     * @return InfoMcmdistribution
     */
    public function addUser(InfoUser $user)
    {
        $this->users->add($user);

        return $this;
    }

    /**
     * Remove user
     *
     * @param InfoUser $user
     */
    public function removeUser(InfoUser $user)
    {
        $this->users->removeElement($user);
    }

    /**
     * Get users
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getUsers()
    {
        return $this->users;
    }

    /**
    * Add submission
    *
    * @param InfoUser $submission
    *
    * @return InfoMcmdistribution
    */
    public function addSubmission(InfoUser $submission)
    {
        $this->submissions->add($submission);

        return $this;
    }

    /**
     * Remove submission
     *
     * @param InfoUser $submission
     */
    public function removeSubmission(InfoUser $submission)
    {
        $this->submissions->removeElement($submission);
    }

    /**
     * Get submissions
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getSubmissions()
    {
        return $this->submissions;
    }

    /**
     * Set owner
     *
     * @param InfoUser $owner
     *
     * @return InfoMcmdistribution
     */
    public function setOwner(InfoUser $owner = null)
    {
        $this->owner = $owner;

        return $this;
    }

    /**
     * Get owner
     *
     * @return InfoUser
     */
    public function getOwner()
    {
        return $this->owner;
    }

    /**
     * Set brand
     *
     * @param InfoBrand $brand
     *
     * @return InfoMcmdistribution
     */
    public function setBrand(InfoBrand $brand = null)
    {
        $this->brand = $brand;

        return $this;
    }

    /**
     * Get brand
     *
     * @return InfoBrand
     */
    public function getBrand()
    {
        return $this->brand;
    }

    /**
     * Set triggerParent
     *
     * @param InfoMcmdistribution $triggerParent
     *
     * @return InfoMcmdistribution
     */
    public function setTriggerParent(InfoMcmdistribution $triggerParent = null)
    {
        $this->triggerParent = $triggerParent;

        return $this;
    }

    /**
     * Get triggerParent
     *
     * @return InfoMcmdistribution
     */
    public function getTriggerParent()
    {
        return $this->triggerParent;
    }

    /**
     * Set trigger
     *
     * @param InfoMcmtrigger $trigger
     *
     * @return InfoMcmdistribution
     */
    public function setTrigger(InfoMcmtrigger $trigger = null)
    {
        $this->trigger = $trigger;

        return $this;
    }

    /**
     * Get trigger
     *
     * @return InfoMcmtrigger
     */
    public function getTrigger()
    {
        return $this->trigger;
    }

    /**
     * Set invite
     *
     * @param int $invite
     *
     * @return InfoMcmdistribution
     */
    public function setInvite($invite)
    {
        $this->invite = $invite;

        return $this;
    }

    /**
     * Get invite
     *
     * @return int
     */
    public function getInvite()
    {
        return $this->invite;
    }

    /**
     * Set isLink
     *
     * @param int $isLink
     *
     * @return InfoMcmdistribution
     */
    public function setIsLink($isLink)
    {
        $this->isLink = $isLink;

        return $this;
    }

    /**
     * Get isLink
     *
     * @return int
     */
    public function getIsLink()
    {
        return $this->isLink;
    }

    /**
     * @return int
     */
    public function getAcceptFilter()
    {
        return $this->acceptFilter;
    }

    /**
     * @param int $acceptFilter
     */
    public function setAcceptFilter(?int $acceptFilter)
    {
        $this->acceptFilter = $acceptFilter;
        return $this;
    }

    /**
     * Set tag
     *
     * @param string|null $tag
     *
     * @return InfoMcmdistribution
     */
    public function setTag($tag = null)
    {
        $this->tag = $tag;

        return $this;
    }

    /**
     * Get tag
     *
     * @return string|null
     */
    public function getTag()
    {
        return $this->tag;
    }

    /**
     * Set landingContent
     *
     * @param null|InfoMcmcontent $landingContent
     *
     * @return InfoMcmdistribution
     */
    public function setLandingContent(?InfoMcmcontent $landingContent): self
    {
        $this->landingContent = $landingContent;

        return $this;
    }

    /**
     * Get landingContent
     *
     * @return InfoMcmcontent|null
     */
    public function getLandingContent(): ?InfoMcmcontent
    {
        return $this->landingContent;
    }
}
