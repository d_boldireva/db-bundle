<?php

namespace TeamSoft\CrmRepositoryBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * InfoTaskmaterial
 *
 * @ORM\Table(name="info_taskmaterial")
 * @ORM\Entity
 */
class InfoTaskmaterial implements ServiceFieldInterface
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var int
     *
     * @ORM\Column(name="quantity", type="integer", nullable=true)
     */
    private $quantity;

    /**
     * @var string
     *
     * @ORM\Column(name="guid", type="guid", nullable=true)
     */
    private $guid;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="currenttime", type="datetime", nullable=true)
     */
    private $currenttime;

    /**
     * @var string
     *
     * @ORM\Column(name="moduser", type="string", length=16, nullable=true)
     */
    private $moduser;

    /**
     * @var InfoPromomaterial
     *
     * @ORM\ManyToOne(targetEntity="InfoPromomaterial")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="Promomaterial_id", referencedColumnName="id")
     * })
     */
    private $promomaterial;

    /**
     * @var InfoTask
     *
     * @ORM\ManyToOne(targetEntity="InfoTask", inversedBy="taskMaterialCollection")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="Task_id", referencedColumnName="id")
     * })
     */
    private $task;



    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set quantity
     *
     * @param int $quantity
     *
     * @return InfoTaskmaterial
     */
    public function setQuantity($quantity)
    {
        $this->quantity = $quantity;
    
        return $this;
    }

    /**
     * Get quantity
     *
     * @return int
     */
    public function getQuantity()
    {
        return $this->quantity;
    }

    /**
     * Set guid
     *
     * @param guid $guid
     *
     * @return InfoTaskmaterial
     */
    public function setGuid($guid)
    {
        $this->guid = $guid;
    
        return $this;
    }

    /**
     * Get guid
     *
     * @return guid
     */
    public function getGuid()
    {
        return $this->guid;
    }

    /**
     * Set currenttime
     *
     * @param \DateTime $currenttime
     *
     * @return InfoTaskmaterial
     */
    public function setCurrenttime(\DateTime $currenttime)
    {
        $this->currenttime = $currenttime;
    
        return $this;
    }

    /**
     * Get currenttime
     *
     * @return \DateTime
     */
    public function getCurrenttime()
    {
        return $this->currenttime;
    }

    /**
     * Set moduser
     *
     * @param string $moduser
     *
     * @return InfoTaskmaterial
     */
    public function setModuser($moduser)
    {
        $this->moduser = $moduser;
    
        return $this;
    }

    /**
     * Get moduser
     *
     * @return string
     */
    public function getModuser()
    {
        return $this->moduser;
    }

    /**
     * Set promomaterial
     *
     * @param \TeamSoft\CrmRepositoryBundle\Entity\InfoPromomaterial $promomaterial
     *
     * @return InfoTaskmaterial
     */
    public function setPromomaterial(\TeamSoft\CrmRepositoryBundle\Entity\InfoPromomaterial $promomaterial = null)
    {
        $this->promomaterial = $promomaterial;
    
        return $this;
    }

    /**
     * Get promomaterial
     *
     * @return \TeamSoft\CrmRepositoryBundle\Entity\InfoPromomaterial
     */
    public function getPromomaterial()
    {
        return $this->promomaterial;
    }

    /**
     * Set task
     *
     * @param \TeamSoft\CrmRepositoryBundle\Entity\InfoTask $task
     *
     * @return InfoTaskmaterial
     */
    public function setTask(\TeamSoft\CrmRepositoryBundle\Entity\InfoTask $task = null)
    {
        $this->task = $task;
    
        return $this;
    }

    /**
     * Get task
     *
     * @return \TeamSoft\CrmRepositoryBundle\Entity\InfoTask
     */
    public function getTask()
    {
        return $this->task;
    }

    public function getTaskId()
    {
        return $this->getTask() ? $this->getTask()->getId() : null;
    }

    public function getPromoMaterialId()
    {
        return $this->getPromomaterial() ? $this->getPromomaterial()->getId() : null;
    }
}
