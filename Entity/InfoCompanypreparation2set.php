<?php

namespace TeamSoft\CrmRepositoryBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * InfoCompanypreparation2set
 *
 * @ORM\Table(name="info_companypreparation2set")
 * @ORM\Entity
 */
class InfoCompanypreparation2set
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="flag1", type="string", length=255, nullable=true)
     */
    private $flag1;

    /**
     * @var int
     *
     * @ORM\Column(name="isflag1", type="integer", nullable=true)
     */
    private $isflag1;

    /**
     * @var int
     *
     * @ORM\Column(name="nflag1", type="integer", nullable=true)
     */
    private $nflag1;

    /**
     * @var string
     *
     * @ORM\Column(name="flag2", type="string", length=255, nullable=true)
     */
    private $flag2;

    /**
     * @var int
     *
     * @ORM\Column(name="isflag2", type="integer", nullable=true)
     */
    private $isflag2;

    /**
     * @var int
     *
     * @ORM\Column(name="nflag2", type="integer", nullable=true)
     */
    private $nflag2;

    /**
     * @var string
     *
     * @ORM\Column(name="flag3", type="string", length=255, nullable=true)
     */
    private $flag3;

    /**
     * @var int
     *
     * @ORM\Column(name="isflag3", type="integer", nullable=true)
     */
    private $isflag3;

    /**
     * @var int
     *
     * @ORM\Column(name="nflag3", type="integer", nullable=true)
     */
    private $nflag3;

    /**
     * @var string
     *
     * @ORM\Column(name="flag4", type="string", length=255, nullable=true)
     */
    private $flag4;

    /**
     * @var int
     *
     * @ORM\Column(name="isflag4", type="integer", nullable=true)
     */
    private $isflag4;

    /**
     * @var int
     *
     * @ORM\Column(name="nflag4", type="integer", nullable=true)
     */
    private $nflag4;

    /**
     * @var string
     *
     * @ORM\Column(name="flag5", type="string", length=255, nullable=true)
     */
    private $flag5;

    /**
     * @var int
     *
     * @ORM\Column(name="isflag5", type="integer", nullable=true)
     */
    private $isflag5;

    /**
     * @var int
     *
     * @ORM\Column(name="nflag5", type="integer", nullable=true)
     */
    private $nflag5;

    /**
     * @var string
     *
     * @ORM\Column(name="int1", type="string", length=255, nullable=true)
     */
    private $int1;

    /**
     * @var int
     *
     * @ORM\Column(name="isint1", type="integer", nullable=true)
     */
    private $isint1;

    /**
     * @var int
     *
     * @ORM\Column(name="nint1", type="integer", nullable=true)
     */
    private $nint1;

    /**
     * @var string
     *
     * @ORM\Column(name="int2", type="string", length=255, nullable=true)
     */
    private $int2;

    /**
     * @var int
     *
     * @ORM\Column(name="isint2", type="integer", nullable=true)
     */
    private $isint2;

    /**
     * @var int
     *
     * @ORM\Column(name="nint2", type="integer", nullable=true)
     */
    private $nint2;

    /**
     * @var string
     *
     * @ORM\Column(name="int3", type="string", length=255, nullable=true)
     */
    private $int3;

    /**
     * @var int
     *
     * @ORM\Column(name="isint3", type="integer", nullable=true)
     */
    private $isint3;

    /**
     * @var int
     *
     * @ORM\Column(name="nint3", type="integer", nullable=true)
     */
    private $nint3;

    /**
     * @var string
     *
     * @ORM\Column(name="str1", type="string", length=255, nullable=true)
     */
    private $str1;

    /**
     * @var int
     *
     * @ORM\Column(name="isstr1", type="integer", nullable=true)
     */
    private $isstr1;

    /**
     * @var int
     *
     * @ORM\Column(name="nstr1", type="integer", nullable=true)
     */
    private $nstr1;

    /**
     * @var string
     *
     * @ORM\Column(name="str2", type="string", length=255, nullable=true)
     */
    private $str2;

    /**
     * @var int
     *
     * @ORM\Column(name="isstr2", type="integer", nullable=true)
     */
    private $isstr2;

    /**
     * @var int
     *
     * @ORM\Column(name="nstr2", type="integer", nullable=true)
     */
    private $nstr2;

    /**
     * @var string
     *
     * @ORM\Column(name="str3", type="string", length=255, nullable=true)
     */
    private $str3;

    /**
     * @var int
     *
     * @ORM\Column(name="isstr3", type="integer", nullable=true)
     */
    private $isstr3;

    /**
     * @var int
     *
     * @ORM\Column(name="nstr3", type="integer", nullable=true)
     */
    private $nstr3;

    /**
     * @var string
     *
     * @ORM\Column(name="date1", type="string", length=255, nullable=true)
     */
    private $date1;

    /**
     * @var int
     *
     * @ORM\Column(name="isdate1", type="integer", nullable=true)
     */
    private $isdate1;

    /**
     * @var int
     *
     * @ORM\Column(name="ndate1", type="integer", nullable=true)
     */
    private $ndate1;

    /**
     * @var string
     *
     * @ORM\Column(name="date2", type="string", length=255, nullable=true)
     */
    private $date2;

    /**
     * @var int
     *
     * @ORM\Column(name="isdate2", type="integer", nullable=true)
     */
    private $isdate2;

    /**
     * @var int
     *
     * @ORM\Column(name="ndate2", type="integer", nullable=true)
     */
    private $ndate2;

    /**
     * @var string
     *
     * @ORM\Column(name="date3", type="string", length=255, nullable=true)
     */
    private $date3;

    /**
     * @var int
     *
     * @ORM\Column(name="isdate3", type="integer", nullable=true)
     */
    private $isdate3;

    /**
     * @var int
     *
     * @ORM\Column(name="ndate3", type="integer", nullable=true)
     */
    private $ndate3;

    /**
     * @var string
     *
     * @ORM\Column(name="float1", type="string", length=255, nullable=true)
     */
    private $float1;

    /**
     * @var int
     *
     * @ORM\Column(name="isfloat1", type="integer", nullable=true)
     */
    private $isfloat1;

    /**
     * @var int
     *
     * @ORM\Column(name="nfloat1", type="integer", nullable=true)
     */
    private $nfloat1;

    /**
     * @var string
     *
     * @ORM\Column(name="float2", type="string", length=255, nullable=true)
     */
    private $float2;

    /**
     * @var int
     *
     * @ORM\Column(name="isfloat2", type="integer", nullable=true)
     */
    private $isfloat2;

    /**
     * @var int
     *
     * @ORM\Column(name="nfloat2", type="integer", nullable=true)
     */
    private $nfloat2;

    /**
     * @var string
     *
     * @ORM\Column(name="float3", type="string", length=255, nullable=true)
     */
    private $float3;

    /**
     * @var int
     *
     * @ORM\Column(name="isfloat3", type="integer", nullable=true)
     */
    private $isfloat3;

    /**
     * @var int
     *
     * @ORM\Column(name="nfloat3", type="integer", nullable=true)
     */
    private $nfloat3;

    /**
     * @var guid
     *
     * @ORM\Column(name="guid", type="guid", nullable=true)
     */
    private $guid;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="currenttime", type="datetime", nullable=true)
     */
    private $currenttime;

    /**
     * @var string
     *
     * @ORM\Column(name="moduser", type="string", length=16, nullable=true)
     */
    private $moduser;

    /**
     * @var string
     *
     * @ORM\Column(name="rf1", type="string", length=255, nullable=true)
     */
    private $rf1;

    /**
     * @var string
     *
     * @ORM\Column(name="rf2", type="string", length=255, nullable=true)
     */
    private $rf2;

    /**
     * @var string
     *
     * @ORM\Column(name="rf3", type="string", length=255, nullable=true)
     */
    private $rf3;

    /**
     * @var string
     *
     * @ORM\Column(name="rf4", type="string", length=255, nullable=true)
     */
    private $rf4;

    /**
     * @var string
     *
     * @ORM\Column(name="rf5", type="string", length=255, nullable=true)
     */
    private $rf5;

    /**
     * @var string
     *
     * @ORM\Column(name="rf6", type="string", length=255, nullable=true)
     */
    private $rf6;

    /**
     * @var string
     *
     * @ORM\Column(name="rf7", type="string", length=255, nullable=true)
     */
    private $rf7;

    /**
     * @var string
     *
     * @ORM\Column(name="rf8", type="string", length=255, nullable=true)
     */
    private $rf8;

    /**
     * @var string
     *
     * @ORM\Column(name="rf9", type="string", length=255, nullable=true)
     */
    private $rf9;

    /**
     * @var string
     *
     * @ORM\Column(name="rf10", type="string", length=255, nullable=true)
     */
    private $rf10;

    /**
     * @var string
     *
     * @ORM\Column(name="rf1f", type="string", length=255, nullable=true)
     */
    private $rf1f;

    /**
     * @var string
     *
     * @ORM\Column(name="rf2f", type="string", length=255, nullable=true)
     */
    private $rf2f;

    /**
     * @var string
     *
     * @ORM\Column(name="rf3f", type="string", length=255, nullable=true)
     */
    private $rf3f;

    /**
     * @var string
     *
     * @ORM\Column(name="rf4f", type="string", length=255, nullable=true)
     */
    private $rf4f;

    /**
     * @var string
     *
     * @ORM\Column(name="rf5f", type="string", length=255, nullable=true)
     */
    private $rf5f;

    /**
     * @var string
     *
     * @ORM\Column(name="rf6f", type="string", length=255, nullable=true)
     */
    private $rf6f;

    /**
     * @var string
     *
     * @ORM\Column(name="rf7f", type="string", length=255, nullable=true)
     */
    private $rf7f;

    /**
     * @var string
     *
     * @ORM\Column(name="rf8f", type="string", length=255, nullable=true)
     */
    private $rf8f;

    /**
     * @var string
     *
     * @ORM\Column(name="rf9f", type="string", length=255, nullable=true)
     */
    private $rf9f;

    /**
     * @var string
     *
     * @ORM\Column(name="rf10f", type="string", length=255, nullable=true)
     */
    private $rf10f;

    /**
     * @var int
     *
     * @ORM\Column(name="rflag1", type="integer", nullable=true)
     */
    private $rflag1;

    /**
     * @var int
     *
     * @ORM\Column(name="rflag2", type="integer", nullable=true)
     */
    private $rflag2;

    /**
     * @var int
     *
     * @ORM\Column(name="rflag3", type="integer", nullable=true)
     */
    private $rflag3;

    /**
     * @var int
     *
     * @ORM\Column(name="rflag4", type="integer", nullable=true)
     */
    private $rflag4;

    /**
     * @var int
     *
     * @ORM\Column(name="rflag5", type="integer", nullable=true)
     */
    private $rflag5;

    /**
     * @var int
     *
     * @ORM\Column(name="rint1", type="integer", nullable=true)
     */
    private $rint1;

    /**
     * @var int
     *
     * @ORM\Column(name="rint2", type="integer", nullable=true)
     */
    private $rint2;

    /**
     * @var int
     *
     * @ORM\Column(name="rint3", type="integer", nullable=true)
     */
    private $rint3;

    /**
     * @var int
     *
     * @ORM\Column(name="rstr1", type="integer", nullable=true)
     */
    private $rstr1;

    /**
     * @var int
     *
     * @ORM\Column(name="rstr2", type="integer", nullable=true)
     */
    private $rstr2;

    /**
     * @var int
     *
     * @ORM\Column(name="rstr3", type="integer", nullable=true)
     */
    private $rstr3;

    /**
     * @var int
     *
     * @ORM\Column(name="rdate1", type="integer", nullable=true)
     */
    private $rdate1;

    /**
     * @var int
     *
     * @ORM\Column(name="rdate2", type="integer", nullable=true)
     */
    private $rdate2;

    /**
     * @var int
     *
     * @ORM\Column(name="rdate3", type="integer", nullable=true)
     */
    private $rdate3;

    /**
     * @var int
     *
     * @ORM\Column(name="rfloat1", type="integer", nullable=true)
     */
    private $rfloat1;

    /**
     * @var int
     *
     * @ORM\Column(name="rfloat2", type="integer", nullable=true)
     */
    private $rfloat2;

    /**
     * @var int
     *
     * @ORM\Column(name="rfloat3", type="integer", nullable=true)
     */
    private $rfloat3;

    /**
     * @var int
     *
     * @ORM\Column(name="prevflag1", type="integer", nullable=true)
     */
    private $prevflag1;

    /**
     * @var int
     *
     * @ORM\Column(name="prevflag2", type="integer", nullable=true)
     */
    private $prevflag2;

    /**
     * @var int
     *
     * @ORM\Column(name="prevflag3", type="integer", nullable=true)
     */
    private $prevflag3;

    /**
     * @var int
     *
     * @ORM\Column(name="prevflag4", type="integer", nullable=true)
     */
    private $prevflag4;

    /**
     * @var int
     *
     * @ORM\Column(name="prevflag5", type="integer", nullable=true)
     */
    private $prevflag5;

    /**
     * @var int
     *
     * @ORM\Column(name="prevint1", type="integer", nullable=true)
     */
    private $prevint1;

    /**
     * @var int
     *
     * @ORM\Column(name="prevint2", type="integer", nullable=true)
     */
    private $prevint2;

    /**
     * @var int
     *
     * @ORM\Column(name="prevint3", type="integer", nullable=true)
     */
    private $prevint3;

    /**
     * @var int
     *
     * @ORM\Column(name="prevstr1", type="integer", nullable=true)
     */
    private $prevstr1;

    /**
     * @var int
     *
     * @ORM\Column(name="prevstr2", type="integer", nullable=true)
     */
    private $prevstr2;

    /**
     * @var int
     *
     * @ORM\Column(name="prevstr3", type="integer", nullable=true)
     */
    private $prevstr3;

    /**
     * @var int
     *
     * @ORM\Column(name="prevdate1", type="integer", nullable=true)
     */
    private $prevdate1;

    /**
     * @var int
     *
     * @ORM\Column(name="prevdate2", type="integer", nullable=true)
     */
    private $prevdate2;

    /**
     * @var int
     *
     * @ORM\Column(name="prevdate3", type="integer", nullable=true)
     */
    private $prevdate3;

    /**
     * @var int
     *
     * @ORM\Column(name="prevfloat1", type="integer", nullable=true)
     */
    private $prevfloat1;

    /**
     * @var int
     *
     * @ORM\Column(name="prevfloat2", type="integer", nullable=true)
     */
    private $prevfloat2;

    /**
     * @var int
     *
     * @ORM\Column(name="prevfloat3", type="integer", nullable=true)
     */
    private $prevfloat3;

    /**
     * @var InfoRole
     *
     * @ORM\ManyToOne(targetEntity="InfoRole")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="role_id", referencedColumnName="id")
     * })
     */
    private $role;



    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set flag1
     *
     * @param string $flag1
     *
     * @return InfoCompanypreparation2set
     */
    public function setFlag1($flag1)
    {
        $this->flag1 = $flag1;
    
        return $this;
    }

    /**
     * Get flag1
     *
     * @return string
     */
    public function getFlag1()
    {
        return $this->flag1;
    }

    /**
     * Set isflag1
     *
     * @param int $isflag1
     *
     * @return InfoCompanypreparation2set
     */
    public function setIsflag1($isflag1)
    {
        $this->isflag1 = $isflag1;
    
        return $this;
    }

    /**
     * Get isflag1
     *
     * @return int
     */
    public function getIsflag1()
    {
        return $this->isflag1;
    }

    /**
     * Set nflag1
     *
     * @param int $nflag1
     *
     * @return InfoCompanypreparation2set
     */
    public function setNflag1($nflag1)
    {
        $this->nflag1 = $nflag1;
    
        return $this;
    }

    /**
     * Get nflag1
     *
     * @return int
     */
    public function getNflag1()
    {
        return $this->nflag1;
    }

    /**
     * Set flag2
     *
     * @param string $flag2
     *
     * @return InfoCompanypreparation2set
     */
    public function setFlag2($flag2)
    {
        $this->flag2 = $flag2;
    
        return $this;
    }

    /**
     * Get flag2
     *
     * @return string
     */
    public function getFlag2()
    {
        return $this->flag2;
    }

    /**
     * Set isflag2
     *
     * @param int $isflag2
     *
     * @return InfoCompanypreparation2set
     */
    public function setIsflag2($isflag2)
    {
        $this->isflag2 = $isflag2;
    
        return $this;
    }

    /**
     * Get isflag2
     *
     * @return int
     */
    public function getIsflag2()
    {
        return $this->isflag2;
    }

    /**
     * Set nflag2
     *
     * @param int $nflag2
     *
     * @return InfoCompanypreparation2set
     */
    public function setNflag2($nflag2)
    {
        $this->nflag2 = $nflag2;
    
        return $this;
    }

    /**
     * Get nflag2
     *
     * @return int
     */
    public function getNflag2()
    {
        return $this->nflag2;
    }

    /**
     * Set flag3
     *
     * @param string $flag3
     *
     * @return InfoCompanypreparation2set
     */
    public function setFlag3($flag3)
    {
        $this->flag3 = $flag3;
    
        return $this;
    }

    /**
     * Get flag3
     *
     * @return string
     */
    public function getFlag3()
    {
        return $this->flag3;
    }

    /**
     * Set isflag3
     *
     * @param int $isflag3
     *
     * @return InfoCompanypreparation2set
     */
    public function setIsflag3($isflag3)
    {
        $this->isflag3 = $isflag3;
    
        return $this;
    }

    /**
     * Get isflag3
     *
     * @return int
     */
    public function getIsflag3()
    {
        return $this->isflag3;
    }

    /**
     * Set nflag3
     *
     * @param int $nflag3
     *
     * @return InfoCompanypreparation2set
     */
    public function setNflag3($nflag3)
    {
        $this->nflag3 = $nflag3;
    
        return $this;
    }

    /**
     * Get nflag3
     *
     * @return int
     */
    public function getNflag3()
    {
        return $this->nflag3;
    }

    /**
     * Set flag4
     *
     * @param string $flag4
     *
     * @return InfoCompanypreparation2set
     */
    public function setFlag4($flag4)
    {
        $this->flag4 = $flag4;
    
        return $this;
    }

    /**
     * Get flag4
     *
     * @return string
     */
    public function getFlag4()
    {
        return $this->flag4;
    }

    /**
     * Set isflag4
     *
     * @param int $isflag4
     *
     * @return InfoCompanypreparation2set
     */
    public function setIsflag4($isflag4)
    {
        $this->isflag4 = $isflag4;
    
        return $this;
    }

    /**
     * Get isflag4
     *
     * @return int
     */
    public function getIsflag4()
    {
        return $this->isflag4;
    }

    /**
     * Set nflag4
     *
     * @param int $nflag4
     *
     * @return InfoCompanypreparation2set
     */
    public function setNflag4($nflag4)
    {
        $this->nflag4 = $nflag4;
    
        return $this;
    }

    /**
     * Get nflag4
     *
     * @return int
     */
    public function getNflag4()
    {
        return $this->nflag4;
    }

    /**
     * Set flag5
     *
     * @param string $flag5
     *
     * @return InfoCompanypreparation2set
     */
    public function setFlag5($flag5)
    {
        $this->flag5 = $flag5;
    
        return $this;
    }

    /**
     * Get flag5
     *
     * @return string
     */
    public function getFlag5()
    {
        return $this->flag5;
    }

    /**
     * Set isflag5
     *
     * @param int $isflag5
     *
     * @return InfoCompanypreparation2set
     */
    public function setIsflag5($isflag5)
    {
        $this->isflag5 = $isflag5;
    
        return $this;
    }

    /**
     * Get isflag5
     *
     * @return int
     */
    public function getIsflag5()
    {
        return $this->isflag5;
    }

    /**
     * Set nflag5
     *
     * @param int $nflag5
     *
     * @return InfoCompanypreparation2set
     */
    public function setNflag5($nflag5)
    {
        $this->nflag5 = $nflag5;
    
        return $this;
    }

    /**
     * Get nflag5
     *
     * @return int
     */
    public function getNflag5()
    {
        return $this->nflag5;
    }

    /**
     * Set int1
     *
     * @param string $int1
     *
     * @return InfoCompanypreparation2set
     */
    public function setInt1($int1)
    {
        $this->int1 = $int1;
    
        return $this;
    }

    /**
     * Get int1
     *
     * @return string
     */
    public function getInt1()
    {
        return $this->int1;
    }

    /**
     * Set isint1
     *
     * @param int $isint1
     *
     * @return InfoCompanypreparation2set
     */
    public function setIsint1($isint1)
    {
        $this->isint1 = $isint1;
    
        return $this;
    }

    /**
     * Get isint1
     *
     * @return int
     */
    public function getIsint1()
    {
        return $this->isint1;
    }

    /**
     * Set nint1
     *
     * @param int $nint1
     *
     * @return InfoCompanypreparation2set
     */
    public function setNint1($nint1)
    {
        $this->nint1 = $nint1;
    
        return $this;
    }

    /**
     * Get nint1
     *
     * @return int
     */
    public function getNint1()
    {
        return $this->nint1;
    }

    /**
     * Set int2
     *
     * @param string $int2
     *
     * @return InfoCompanypreparation2set
     */
    public function setInt2($int2)
    {
        $this->int2 = $int2;
    
        return $this;
    }

    /**
     * Get int2
     *
     * @return string
     */
    public function getInt2()
    {
        return $this->int2;
    }

    /**
     * Set isint2
     *
     * @param int $isint2
     *
     * @return InfoCompanypreparation2set
     */
    public function setIsint2($isint2)
    {
        $this->isint2 = $isint2;
    
        return $this;
    }

    /**
     * Get isint2
     *
     * @return int
     */
    public function getIsint2()
    {
        return $this->isint2;
    }

    /**
     * Set nint2
     *
     * @param int $nint2
     *
     * @return InfoCompanypreparation2set
     */
    public function setNint2($nint2)
    {
        $this->nint2 = $nint2;
    
        return $this;
    }

    /**
     * Get nint2
     *
     * @return int
     */
    public function getNint2()
    {
        return $this->nint2;
    }

    /**
     * Set int3
     *
     * @param string $int3
     *
     * @return InfoCompanypreparation2set
     */
    public function setInt3($int3)
    {
        $this->int3 = $int3;
    
        return $this;
    }

    /**
     * Get int3
     *
     * @return string
     */
    public function getInt3()
    {
        return $this->int3;
    }

    /**
     * Set isint3
     *
     * @param int $isint3
     *
     * @return InfoCompanypreparation2set
     */
    public function setIsint3($isint3)
    {
        $this->isint3 = $isint3;
    
        return $this;
    }

    /**
     * Get isint3
     *
     * @return int
     */
    public function getIsint3()
    {
        return $this->isint3;
    }

    /**
     * Set nint3
     *
     * @param int $nint3
     *
     * @return InfoCompanypreparation2set
     */
    public function setNint3($nint3)
    {
        $this->nint3 = $nint3;
    
        return $this;
    }

    /**
     * Get nint3
     *
     * @return int
     */
    public function getNint3()
    {
        return $this->nint3;
    }

    /**
     * Set str1
     *
     * @param string $str1
     *
     * @return InfoCompanypreparation2set
     */
    public function setStr1($str1)
    {
        $this->str1 = $str1;
    
        return $this;
    }

    /**
     * Get str1
     *
     * @return string
     */
    public function getStr1()
    {
        return $this->str1;
    }

    /**
     * Set isstr1
     *
     * @param int $isstr1
     *
     * @return InfoCompanypreparation2set
     */
    public function setIsstr1($isstr1)
    {
        $this->isstr1 = $isstr1;
    
        return $this;
    }

    /**
     * Get isstr1
     *
     * @return int
     */
    public function getIsstr1()
    {
        return $this->isstr1;
    }

    /**
     * Set nstr1
     *
     * @param int $nstr1
     *
     * @return InfoCompanypreparation2set
     */
    public function setNstr1($nstr1)
    {
        $this->nstr1 = $nstr1;
    
        return $this;
    }

    /**
     * Get nstr1
     *
     * @return int
     */
    public function getNstr1()
    {
        return $this->nstr1;
    }

    /**
     * Set str2
     *
     * @param string $str2
     *
     * @return InfoCompanypreparation2set
     */
    public function setStr2($str2)
    {
        $this->str2 = $str2;
    
        return $this;
    }

    /**
     * Get str2
     *
     * @return string
     */
    public function getStr2()
    {
        return $this->str2;
    }

    /**
     * Set isstr2
     *
     * @param int $isstr2
     *
     * @return InfoCompanypreparation2set
     */
    public function setIsstr2($isstr2)
    {
        $this->isstr2 = $isstr2;
    
        return $this;
    }

    /**
     * Get isstr2
     *
     * @return int
     */
    public function getIsstr2()
    {
        return $this->isstr2;
    }

    /**
     * Set nstr2
     *
     * @param int $nstr2
     *
     * @return InfoCompanypreparation2set
     */
    public function setNstr2($nstr2)
    {
        $this->nstr2 = $nstr2;
    
        return $this;
    }

    /**
     * Get nstr2
     *
     * @return int
     */
    public function getNstr2()
    {
        return $this->nstr2;
    }

    /**
     * Set str3
     *
     * @param string $str3
     *
     * @return InfoCompanypreparation2set
     */
    public function setStr3($str3)
    {
        $this->str3 = $str3;
    
        return $this;
    }

    /**
     * Get str3
     *
     * @return string
     */
    public function getStr3()
    {
        return $this->str3;
    }

    /**
     * Set isstr3
     *
     * @param int $isstr3
     *
     * @return InfoCompanypreparation2set
     */
    public function setIsstr3($isstr3)
    {
        $this->isstr3 = $isstr3;
    
        return $this;
    }

    /**
     * Get isstr3
     *
     * @return int
     */
    public function getIsstr3()
    {
        return $this->isstr3;
    }

    /**
     * Set nstr3
     *
     * @param int $nstr3
     *
     * @return InfoCompanypreparation2set
     */
    public function setNstr3($nstr3)
    {
        $this->nstr3 = $nstr3;
    
        return $this;
    }

    /**
     * Get nstr3
     *
     * @return int
     */
    public function getNstr3()
    {
        return $this->nstr3;
    }

    /**
     * Set date1
     *
     * @param string $date1
     *
     * @return InfoCompanypreparation2set
     */
    public function setDate1($date1)
    {
        $this->date1 = $date1;
    
        return $this;
    }

    /**
     * Get date1
     *
     * @return string
     */
    public function getDate1()
    {
        return $this->date1;
    }

    /**
     * Set isdate1
     *
     * @param int $isdate1
     *
     * @return InfoCompanypreparation2set
     */
    public function setIsdate1($isdate1)
    {
        $this->isdate1 = $isdate1;
    
        return $this;
    }

    /**
     * Get isdate1
     *
     * @return int
     */
    public function getIsdate1()
    {
        return $this->isdate1;
    }

    /**
     * Set ndate1
     *
     * @param int $ndate1
     *
     * @return InfoCompanypreparation2set
     */
    public function setNdate1($ndate1)
    {
        $this->ndate1 = $ndate1;
    
        return $this;
    }

    /**
     * Get ndate1
     *
     * @return int
     */
    public function getNdate1()
    {
        return $this->ndate1;
    }

    /**
     * Set date2
     *
     * @param string $date2
     *
     * @return InfoCompanypreparation2set
     */
    public function setDate2($date2)
    {
        $this->date2 = $date2;
    
        return $this;
    }

    /**
     * Get date2
     *
     * @return string
     */
    public function getDate2()
    {
        return $this->date2;
    }

    /**
     * Set isdate2
     *
     * @param int $isdate2
     *
     * @return InfoCompanypreparation2set
     */
    public function setIsdate2($isdate2)
    {
        $this->isdate2 = $isdate2;
    
        return $this;
    }

    /**
     * Get isdate2
     *
     * @return int
     */
    public function getIsdate2()
    {
        return $this->isdate2;
    }

    /**
     * Set ndate2
     *
     * @param int $ndate2
     *
     * @return InfoCompanypreparation2set
     */
    public function setNdate2($ndate2)
    {
        $this->ndate2 = $ndate2;
    
        return $this;
    }

    /**
     * Get ndate2
     *
     * @return int
     */
    public function getNdate2()
    {
        return $this->ndate2;
    }

    /**
     * Set date3
     *
     * @param string $date3
     *
     * @return InfoCompanypreparation2set
     */
    public function setDate3($date3)
    {
        $this->date3 = $date3;
    
        return $this;
    }

    /**
     * Get date3
     *
     * @return string
     */
    public function getDate3()
    {
        return $this->date3;
    }

    /**
     * Set isdate3
     *
     * @param int $isdate3
     *
     * @return InfoCompanypreparation2set
     */
    public function setIsdate3($isdate3)
    {
        $this->isdate3 = $isdate3;
    
        return $this;
    }

    /**
     * Get isdate3
     *
     * @return int
     */
    public function getIsdate3()
    {
        return $this->isdate3;
    }

    /**
     * Set ndate3
     *
     * @param int $ndate3
     *
     * @return InfoCompanypreparation2set
     */
    public function setNdate3($ndate3)
    {
        $this->ndate3 = $ndate3;
    
        return $this;
    }

    /**
     * Get ndate3
     *
     * @return int
     */
    public function getNdate3()
    {
        return $this->ndate3;
    }

    /**
     * Set float1
     *
     * @param string $float1
     *
     * @return InfoCompanypreparation2set
     */
    public function setFloat1($float1)
    {
        $this->float1 = $float1;
    
        return $this;
    }

    /**
     * Get float1
     *
     * @return string
     */
    public function getFloat1()
    {
        return $this->float1;
    }

    /**
     * Set isfloat1
     *
     * @param int $isfloat1
     *
     * @return InfoCompanypreparation2set
     */
    public function setIsfloat1($isfloat1)
    {
        $this->isfloat1 = $isfloat1;
    
        return $this;
    }

    /**
     * Get isfloat1
     *
     * @return int
     */
    public function getIsfloat1()
    {
        return $this->isfloat1;
    }

    /**
     * Set nfloat1
     *
     * @param int $nfloat1
     *
     * @return InfoCompanypreparation2set
     */
    public function setNfloat1($nfloat1)
    {
        $this->nfloat1 = $nfloat1;
    
        return $this;
    }

    /**
     * Get nfloat1
     *
     * @return int
     */
    public function getNfloat1()
    {
        return $this->nfloat1;
    }

    /**
     * Set float2
     *
     * @param string $float2
     *
     * @return InfoCompanypreparation2set
     */
    public function setFloat2($float2)
    {
        $this->float2 = $float2;
    
        return $this;
    }

    /**
     * Get float2
     *
     * @return string
     */
    public function getFloat2()
    {
        return $this->float2;
    }

    /**
     * Set isfloat2
     *
     * @param int $isfloat2
     *
     * @return InfoCompanypreparation2set
     */
    public function setIsfloat2($isfloat2)
    {
        $this->isfloat2 = $isfloat2;
    
        return $this;
    }

    /**
     * Get isfloat2
     *
     * @return int
     */
    public function getIsfloat2()
    {
        return $this->isfloat2;
    }

    /**
     * Set nfloat2
     *
     * @param int $nfloat2
     *
     * @return InfoCompanypreparation2set
     */
    public function setNfloat2($nfloat2)
    {
        $this->nfloat2 = $nfloat2;
    
        return $this;
    }

    /**
     * Get nfloat2
     *
     * @return int
     */
    public function getNfloat2()
    {
        return $this->nfloat2;
    }

    /**
     * Set float3
     *
     * @param string $float3
     *
     * @return InfoCompanypreparation2set
     */
    public function setFloat3($float3)
    {
        $this->float3 = $float3;
    
        return $this;
    }

    /**
     * Get float3
     *
     * @return string
     */
    public function getFloat3()
    {
        return $this->float3;
    }

    /**
     * Set isfloat3
     *
     * @param int $isfloat3
     *
     * @return InfoCompanypreparation2set
     */
    public function setIsfloat3($isfloat3)
    {
        $this->isfloat3 = $isfloat3;
    
        return $this;
    }

    /**
     * Get isfloat3
     *
     * @return int
     */
    public function getIsfloat3()
    {
        return $this->isfloat3;
    }

    /**
     * Set nfloat3
     *
     * @param int $nfloat3
     *
     * @return InfoCompanypreparation2set
     */
    public function setNfloat3($nfloat3)
    {
        $this->nfloat3 = $nfloat3;
    
        return $this;
    }

    /**
     * Get nfloat3
     *
     * @return int
     */
    public function getNfloat3()
    {
        return $this->nfloat3;
    }

    /**
     * Set guid
     *
     * @param guid $guid
     *
     * @return InfoCompanypreparation2set
     */
    public function setGuid($guid)
    {
        $this->guid = $guid;
    
        return $this;
    }

    /**
     * Get guid
     *
     * @return guid
     */
    public function getGuid()
    {
        return $this->guid;
    }

    /**
     * Set currenttime
     *
     * @param \DateTime $currenttime
     *
     * @return InfoCompanypreparation2set
     */
    public function setCurrenttime($currenttime)
    {
        $this->currenttime = $currenttime;
    
        return $this;
    }

    /**
     * Get currenttime
     *
     * @return \DateTime
     */
    public function getCurrenttime()
    {
        return $this->currenttime;
    }

    /**
     * Set moduser
     *
     * @param string $moduser
     *
     * @return InfoCompanypreparation2set
     */
    public function setModuser($moduser)
    {
        $this->moduser = $moduser;
    
        return $this;
    }

    /**
     * Get moduser
     *
     * @return string
     */
    public function getModuser()
    {
        return $this->moduser;
    }

    /**
     * Set rf1
     *
     * @param string $rf1
     *
     * @return InfoCompanypreparation2set
     */
    public function setRf1($rf1)
    {
        $this->rf1 = $rf1;
    
        return $this;
    }

    /**
     * Get rf1
     *
     * @return string
     */
    public function getRf1()
    {
        return $this->rf1;
    }

    /**
     * Set rf2
     *
     * @param string $rf2
     *
     * @return InfoCompanypreparation2set
     */
    public function setRf2($rf2)
    {
        $this->rf2 = $rf2;
    
        return $this;
    }

    /**
     * Get rf2
     *
     * @return string
     */
    public function getRf2()
    {
        return $this->rf2;
    }

    /**
     * Set rf3
     *
     * @param string $rf3
     *
     * @return InfoCompanypreparation2set
     */
    public function setRf3($rf3)
    {
        $this->rf3 = $rf3;
    
        return $this;
    }

    /**
     * Get rf3
     *
     * @return string
     */
    public function getRf3()
    {
        return $this->rf3;
    }

    /**
     * Set rf4
     *
     * @param string $rf4
     *
     * @return InfoCompanypreparation2set
     */
    public function setRf4($rf4)
    {
        $this->rf4 = $rf4;
    
        return $this;
    }

    /**
     * Get rf4
     *
     * @return string
     */
    public function getRf4()
    {
        return $this->rf4;
    }

    /**
     * Set rf5
     *
     * @param string $rf5
     *
     * @return InfoCompanypreparation2set
     */
    public function setRf5($rf5)
    {
        $this->rf5 = $rf5;
    
        return $this;
    }

    /**
     * Get rf5
     *
     * @return string
     */
    public function getRf5()
    {
        return $this->rf5;
    }

    /**
     * Set rf6
     *
     * @param string $rf6
     *
     * @return InfoCompanypreparation2set
     */
    public function setRf6($rf6)
    {
        $this->rf6 = $rf6;
    
        return $this;
    }

    /**
     * Get rf6
     *
     * @return string
     */
    public function getRf6()
    {
        return $this->rf6;
    }

    /**
     * Set rf7
     *
     * @param string $rf7
     *
     * @return InfoCompanypreparation2set
     */
    public function setRf7($rf7)
    {
        $this->rf7 = $rf7;
    
        return $this;
    }

    /**
     * Get rf7
     *
     * @return string
     */
    public function getRf7()
    {
        return $this->rf7;
    }

    /**
     * Set rf8
     *
     * @param string $rf8
     *
     * @return InfoCompanypreparation2set
     */
    public function setRf8($rf8)
    {
        $this->rf8 = $rf8;
    
        return $this;
    }

    /**
     * Get rf8
     *
     * @return string
     */
    public function getRf8()
    {
        return $this->rf8;
    }

    /**
     * Set rf9
     *
     * @param string $rf9
     *
     * @return InfoCompanypreparation2set
     */
    public function setRf9($rf9)
    {
        $this->rf9 = $rf9;
    
        return $this;
    }

    /**
     * Get rf9
     *
     * @return string
     */
    public function getRf9()
    {
        return $this->rf9;
    }

    /**
     * Set rf10
     *
     * @param string $rf10
     *
     * @return InfoCompanypreparation2set
     */
    public function setRf10($rf10)
    {
        $this->rf10 = $rf10;
    
        return $this;
    }

    /**
     * Get rf10
     *
     * @return string
     */
    public function getRf10()
    {
        return $this->rf10;
    }

    /**
     * Set rf1f
     *
     * @param string $rf1f
     *
     * @return InfoCompanypreparation2set
     */
    public function setRf1f($rf1f)
    {
        $this->rf1f = $rf1f;
    
        return $this;
    }

    /**
     * Get rf1f
     *
     * @return string
     */
    public function getRf1f()
    {
        return $this->rf1f;
    }

    /**
     * Set rf2f
     *
     * @param string $rf2f
     *
     * @return InfoCompanypreparation2set
     */
    public function setRf2f($rf2f)
    {
        $this->rf2f = $rf2f;
    
        return $this;
    }

    /**
     * Get rf2f
     *
     * @return string
     */
    public function getRf2f()
    {
        return $this->rf2f;
    }

    /**
     * Set rf3f
     *
     * @param string $rf3f
     *
     * @return InfoCompanypreparation2set
     */
    public function setRf3f($rf3f)
    {
        $this->rf3f = $rf3f;
    
        return $this;
    }

    /**
     * Get rf3f
     *
     * @return string
     */
    public function getRf3f()
    {
        return $this->rf3f;
    }

    /**
     * Set rf4f
     *
     * @param string $rf4f
     *
     * @return InfoCompanypreparation2set
     */
    public function setRf4f($rf4f)
    {
        $this->rf4f = $rf4f;
    
        return $this;
    }

    /**
     * Get rf4f
     *
     * @return string
     */
    public function getRf4f()
    {
        return $this->rf4f;
    }

    /**
     * Set rf5f
     *
     * @param string $rf5f
     *
     * @return InfoCompanypreparation2set
     */
    public function setRf5f($rf5f)
    {
        $this->rf5f = $rf5f;
    
        return $this;
    }

    /**
     * Get rf5f
     *
     * @return string
     */
    public function getRf5f()
    {
        return $this->rf5f;
    }

    /**
     * Set rf6f
     *
     * @param string $rf6f
     *
     * @return InfoCompanypreparation2set
     */
    public function setRf6f($rf6f)
    {
        $this->rf6f = $rf6f;
    
        return $this;
    }

    /**
     * Get rf6f
     *
     * @return string
     */
    public function getRf6f()
    {
        return $this->rf6f;
    }

    /**
     * Set rf7f
     *
     * @param string $rf7f
     *
     * @return InfoCompanypreparation2set
     */
    public function setRf7f($rf7f)
    {
        $this->rf7f = $rf7f;
    
        return $this;
    }

    /**
     * Get rf7f
     *
     * @return string
     */
    public function getRf7f()
    {
        return $this->rf7f;
    }

    /**
     * Set rf8f
     *
     * @param string $rf8f
     *
     * @return InfoCompanypreparation2set
     */
    public function setRf8f($rf8f)
    {
        $this->rf8f = $rf8f;
    
        return $this;
    }

    /**
     * Get rf8f
     *
     * @return string
     */
    public function getRf8f()
    {
        return $this->rf8f;
    }

    /**
     * Set rf9f
     *
     * @param string $rf9f
     *
     * @return InfoCompanypreparation2set
     */
    public function setRf9f($rf9f)
    {
        $this->rf9f = $rf9f;
    
        return $this;
    }

    /**
     * Get rf9f
     *
     * @return string
     */
    public function getRf9f()
    {
        return $this->rf9f;
    }

    /**
     * Set rf10f
     *
     * @param string $rf10f
     *
     * @return InfoCompanypreparation2set
     */
    public function setRf10f($rf10f)
    {
        $this->rf10f = $rf10f;
    
        return $this;
    }

    /**
     * Get rf10f
     *
     * @return string
     */
    public function getRf10f()
    {
        return $this->rf10f;
    }

    /**
     * Set rflag1
     *
     * @param int $rflag1
     *
     * @return InfoCompanypreparation2set
     */
    public function setRflag1($rflag1)
    {
        $this->rflag1 = $rflag1;
    
        return $this;
    }

    /**
     * Get rflag1
     *
     * @return int
     */
    public function getRflag1()
    {
        return $this->rflag1;
    }

    /**
     * Set rflag2
     *
     * @param int $rflag2
     *
     * @return InfoCompanypreparation2set
     */
    public function setRflag2($rflag2)
    {
        $this->rflag2 = $rflag2;
    
        return $this;
    }

    /**
     * Get rflag2
     *
     * @return int
     */
    public function getRflag2()
    {
        return $this->rflag2;
    }

    /**
     * Set rflag3
     *
     * @param int $rflag3
     *
     * @return InfoCompanypreparation2set
     */
    public function setRflag3($rflag3)
    {
        $this->rflag3 = $rflag3;
    
        return $this;
    }

    /**
     * Get rflag3
     *
     * @return int
     */
    public function getRflag3()
    {
        return $this->rflag3;
    }

    /**
     * Set rflag4
     *
     * @param int $rflag4
     *
     * @return InfoCompanypreparation2set
     */
    public function setRflag4($rflag4)
    {
        $this->rflag4 = $rflag4;
    
        return $this;
    }

    /**
     * Get rflag4
     *
     * @return int
     */
    public function getRflag4()
    {
        return $this->rflag4;
    }

    /**
     * Set rflag5
     *
     * @param int $rflag5
     *
     * @return InfoCompanypreparation2set
     */
    public function setRflag5($rflag5)
    {
        $this->rflag5 = $rflag5;
    
        return $this;
    }

    /**
     * Get rflag5
     *
     * @return int
     */
    public function getRflag5()
    {
        return $this->rflag5;
    }

    /**
     * Set rint1
     *
     * @param int $rint1
     *
     * @return InfoCompanypreparation2set
     */
    public function setRint1($rint1)
    {
        $this->rint1 = $rint1;
    
        return $this;
    }

    /**
     * Get rint1
     *
     * @return int
     */
    public function getRint1()
    {
        return $this->rint1;
    }

    /**
     * Set rint2
     *
     * @param int $rint2
     *
     * @return InfoCompanypreparation2set
     */
    public function setRint2($rint2)
    {
        $this->rint2 = $rint2;
    
        return $this;
    }

    /**
     * Get rint2
     *
     * @return int
     */
    public function getRint2()
    {
        return $this->rint2;
    }

    /**
     * Set rint3
     *
     * @param int $rint3
     *
     * @return InfoCompanypreparation2set
     */
    public function setRint3($rint3)
    {
        $this->rint3 = $rint3;
    
        return $this;
    }

    /**
     * Get rint3
     *
     * @return int
     */
    public function getRint3()
    {
        return $this->rint3;
    }

    /**
     * Set rstr1
     *
     * @param int $rstr1
     *
     * @return InfoCompanypreparation2set
     */
    public function setRstr1($rstr1)
    {
        $this->rstr1 = $rstr1;
    
        return $this;
    }

    /**
     * Get rstr1
     *
     * @return int
     */
    public function getRstr1()
    {
        return $this->rstr1;
    }

    /**
     * Set rstr2
     *
     * @param int $rstr2
     *
     * @return InfoCompanypreparation2set
     */
    public function setRstr2($rstr2)
    {
        $this->rstr2 = $rstr2;
    
        return $this;
    }

    /**
     * Get rstr2
     *
     * @return int
     */
    public function getRstr2()
    {
        return $this->rstr2;
    }

    /**
     * Set rstr3
     *
     * @param int $rstr3
     *
     * @return InfoCompanypreparation2set
     */
    public function setRstr3($rstr3)
    {
        $this->rstr3 = $rstr3;
    
        return $this;
    }

    /**
     * Get rstr3
     *
     * @return int
     */
    public function getRstr3()
    {
        return $this->rstr3;
    }

    /**
     * Set rdate1
     *
     * @param int $rdate1
     *
     * @return InfoCompanypreparation2set
     */
    public function setRdate1($rdate1)
    {
        $this->rdate1 = $rdate1;
    
        return $this;
    }

    /**
     * Get rdate1
     *
     * @return int
     */
    public function getRdate1()
    {
        return $this->rdate1;
    }

    /**
     * Set rdate2
     *
     * @param int $rdate2
     *
     * @return InfoCompanypreparation2set
     */
    public function setRdate2($rdate2)
    {
        $this->rdate2 = $rdate2;
    
        return $this;
    }

    /**
     * Get rdate2
     *
     * @return int
     */
    public function getRdate2()
    {
        return $this->rdate2;
    }

    /**
     * Set rdate3
     *
     * @param int $rdate3
     *
     * @return InfoCompanypreparation2set
     */
    public function setRdate3($rdate3)
    {
        $this->rdate3 = $rdate3;
    
        return $this;
    }

    /**
     * Get rdate3
     *
     * @return int
     */
    public function getRdate3()
    {
        return $this->rdate3;
    }

    /**
     * Set rfloat1
     *
     * @param int $rfloat1
     *
     * @return InfoCompanypreparation2set
     */
    public function setRfloat1($rfloat1)
    {
        $this->rfloat1 = $rfloat1;
    
        return $this;
    }

    /**
     * Get rfloat1
     *
     * @return int
     */
    public function getRfloat1()
    {
        return $this->rfloat1;
    }

    /**
     * Set rfloat2
     *
     * @param int $rfloat2
     *
     * @return InfoCompanypreparation2set
     */
    public function setRfloat2($rfloat2)
    {
        $this->rfloat2 = $rfloat2;
    
        return $this;
    }

    /**
     * Get rfloat2
     *
     * @return int
     */
    public function getRfloat2()
    {
        return $this->rfloat2;
    }

    /**
     * Set rfloat3
     *
     * @param int $rfloat3
     *
     * @return InfoCompanypreparation2set
     */
    public function setRfloat3($rfloat3)
    {
        $this->rfloat3 = $rfloat3;
    
        return $this;
    }

    /**
     * Get rfloat3
     *
     * @return int
     */
    public function getRfloat3()
    {
        return $this->rfloat3;
    }

    /**
     * Set prevflag1
     *
     * @param int $prevflag1
     *
     * @return InfoCompanypreparation2set
     */
    public function setPrevflag1($prevflag1)
    {
        $this->prevflag1 = $prevflag1;
    
        return $this;
    }

    /**
     * Get prevflag1
     *
     * @return int
     */
    public function getPrevflag1()
    {
        return $this->prevflag1;
    }

    /**
     * Set prevflag2
     *
     * @param int $prevflag2
     *
     * @return InfoCompanypreparation2set
     */
    public function setPrevflag2($prevflag2)
    {
        $this->prevflag2 = $prevflag2;
    
        return $this;
    }

    /**
     * Get prevflag2
     *
     * @return int
     */
    public function getPrevflag2()
    {
        return $this->prevflag2;
    }

    /**
     * Set prevflag3
     *
     * @param int $prevflag3
     *
     * @return InfoCompanypreparation2set
     */
    public function setPrevflag3($prevflag3)
    {
        $this->prevflag3 = $prevflag3;
    
        return $this;
    }

    /**
     * Get prevflag3
     *
     * @return int
     */
    public function getPrevflag3()
    {
        return $this->prevflag3;
    }

    /**
     * Set prevflag4
     *
     * @param int $prevflag4
     *
     * @return InfoCompanypreparation2set
     */
    public function setPrevflag4($prevflag4)
    {
        $this->prevflag4 = $prevflag4;
    
        return $this;
    }

    /**
     * Get prevflag4
     *
     * @return int
     */
    public function getPrevflag4()
    {
        return $this->prevflag4;
    }

    /**
     * Set prevflag5
     *
     * @param int $prevflag5
     *
     * @return InfoCompanypreparation2set
     */
    public function setPrevflag5($prevflag5)
    {
        $this->prevflag5 = $prevflag5;
    
        return $this;
    }

    /**
     * Get prevflag5
     *
     * @return int
     */
    public function getPrevflag5()
    {
        return $this->prevflag5;
    }

    /**
     * Set prevint1
     *
     * @param int $prevint1
     *
     * @return InfoCompanypreparation2set
     */
    public function setPrevint1($prevint1)
    {
        $this->prevint1 = $prevint1;
    
        return $this;
    }

    /**
     * Get prevint1
     *
     * @return int
     */
    public function getPrevint1()
    {
        return $this->prevint1;
    }

    /**
     * Set prevint2
     *
     * @param int $prevint2
     *
     * @return InfoCompanypreparation2set
     */
    public function setPrevint2($prevint2)
    {
        $this->prevint2 = $prevint2;
    
        return $this;
    }

    /**
     * Get prevint2
     *
     * @return int
     */
    public function getPrevint2()
    {
        return $this->prevint2;
    }

    /**
     * Set prevint3
     *
     * @param int $prevint3
     *
     * @return InfoCompanypreparation2set
     */
    public function setPrevint3($prevint3)
    {
        $this->prevint3 = $prevint3;
    
        return $this;
    }

    /**
     * Get prevint3
     *
     * @return int
     */
    public function getPrevint3()
    {
        return $this->prevint3;
    }

    /**
     * Set prevstr1
     *
     * @param int $prevstr1
     *
     * @return InfoCompanypreparation2set
     */
    public function setPrevstr1($prevstr1)
    {
        $this->prevstr1 = $prevstr1;
    
        return $this;
    }

    /**
     * Get prevstr1
     *
     * @return int
     */
    public function getPrevstr1()
    {
        return $this->prevstr1;
    }

    /**
     * Set prevstr2
     *
     * @param int $prevstr2
     *
     * @return InfoCompanypreparation2set
     */
    public function setPrevstr2($prevstr2)
    {
        $this->prevstr2 = $prevstr2;
    
        return $this;
    }

    /**
     * Get prevstr2
     *
     * @return int
     */
    public function getPrevstr2()
    {
        return $this->prevstr2;
    }

    /**
     * Set prevstr3
     *
     * @param int $prevstr3
     *
     * @return InfoCompanypreparation2set
     */
    public function setPrevstr3($prevstr3)
    {
        $this->prevstr3 = $prevstr3;
    
        return $this;
    }

    /**
     * Get prevstr3
     *
     * @return int
     */
    public function getPrevstr3()
    {
        return $this->prevstr3;
    }

    /**
     * Set prevdate1
     *
     * @param int $prevdate1
     *
     * @return InfoCompanypreparation2set
     */
    public function setPrevdate1($prevdate1)
    {
        $this->prevdate1 = $prevdate1;
    
        return $this;
    }

    /**
     * Get prevdate1
     *
     * @return int
     */
    public function getPrevdate1()
    {
        return $this->prevdate1;
    }

    /**
     * Set prevdate2
     *
     * @param int $prevdate2
     *
     * @return InfoCompanypreparation2set
     */
    public function setPrevdate2($prevdate2)
    {
        $this->prevdate2 = $prevdate2;
    
        return $this;
    }

    /**
     * Get prevdate2
     *
     * @return int
     */
    public function getPrevdate2()
    {
        return $this->prevdate2;
    }

    /**
     * Set prevdate3
     *
     * @param int $prevdate3
     *
     * @return InfoCompanypreparation2set
     */
    public function setPrevdate3($prevdate3)
    {
        $this->prevdate3 = $prevdate3;
    
        return $this;
    }

    /**
     * Get prevdate3
     *
     * @return int
     */
    public function getPrevdate3()
    {
        return $this->prevdate3;
    }

    /**
     * Set prevfloat1
     *
     * @param int $prevfloat1
     *
     * @return InfoCompanypreparation2set
     */
    public function setPrevfloat1($prevfloat1)
    {
        $this->prevfloat1 = $prevfloat1;
    
        return $this;
    }

    /**
     * Get prevfloat1
     *
     * @return int
     */
    public function getPrevfloat1()
    {
        return $this->prevfloat1;
    }

    /**
     * Set prevfloat2
     *
     * @param int $prevfloat2
     *
     * @return InfoCompanypreparation2set
     */
    public function setPrevfloat2($prevfloat2)
    {
        $this->prevfloat2 = $prevfloat2;
    
        return $this;
    }

    /**
     * Get prevfloat2
     *
     * @return int
     */
    public function getPrevfloat2()
    {
        return $this->prevfloat2;
    }

    /**
     * Set prevfloat3
     *
     * @param int $prevfloat3
     *
     * @return InfoCompanypreparation2set
     */
    public function setPrevfloat3($prevfloat3)
    {
        $this->prevfloat3 = $prevfloat3;
    
        return $this;
    }

    /**
     * Get prevfloat3
     *
     * @return int
     */
    public function getPrevfloat3()
    {
        return $this->prevfloat3;
    }

    /**
     * Set role
     *
     * @param InfoRole $role
     * @return InfoCompanypreparation2set
     */
    public function setRole(InfoRole $role = null)
    {
        $this->role = $role;

        return $this;
    }

    /**
     * Get role
     *
     * @return InfoRole
     */
    public function getRole()
    {
        return $this->role;
    }
}
