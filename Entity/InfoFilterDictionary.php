<?php

namespace TeamSoft\CrmRepositoryBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use TeamSoft\CrmRepositoryBundle\Repository\InfoFilterDictionaryRepository;

/**
 * InfoFilterDictionary
 *
 * @ORM\Table(name="info_filterdictionary")
 * @ORM\Entity(repositoryClass=InfoFilterDictionaryRepository::class)
 */
class InfoFilterDictionary
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $page;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $field;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $sql;

    /**
     * @var string
     *
     * @ORM\Column(name="empty_sql", type="string", length=255, nullable=true)
     */
    private $emptySql;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $sqlite;

    /**
     * @var string
     *
     * @ORM\Column(name="empty_sqlite", type="string", length=255, nullable=true)
     */
    private $emptySqlite;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $subtitle;

    /**
     * @var string
     *
     * @ORM\Column(name="additional_dict", type="string", length=255, nullable=true)
     */
    private $additionalDict;

    /**
     * @var string
     *
     * @ORM\Column(name="depend_field", type="string", length=255, nullable=true)
     */
    private $dependField;

    /**
     * @var string
     *
     * @ORM\Column(name="guid", type="guid", nullable=true)
     */
    private $guid;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="currenttime", type="datetime", nullable=true)
     */
    private $currenttime;

    /**
     * @var string
     *
     * @ORM\Column(name="moduser", type="string", length=16, nullable=true)
     */
    private $moduser;

    /**
     * @ORM\ManyToOne(targetEntity="PoDictionary", inversedBy="id")
     * @ORM\JoinColumn(name="dictionary_id", referencedColumnName="id")
     */
    private $dictionary;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return InfoFilterDictionary
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set page
     *
     * @param string $page
     * @return InfoFilterDictionary
     */
    public function setPage($page)
    {
        $this->page = $page;

        return $this;
    }

    /**
     * Get page
     *
     * @return string
     */
    public function getPage()
    {
        return $this->page;
    }

    /**
     * Set field
     *
     * @param string $field
     * @return InfoFilterDictionary
     */
    public function setField($field)
    {
        $this->field = $field;

        return $this;
    }

    /**
     * Get field
     *
     * @return string
     */
    public function getField()
    {
        return $this->field;
    }

    /**
     * Set sql
     *
     * @param string $sql
     * @return InfoFilterDictionary
     */
    public function setSql($sql)
    {
        $this->sql = $sql;

        return $this;
    }

    /**
     * Get sql
     *
     * @return string
     */
    public function getSql()
    {
        return $this->sql;
    }

    /**
     * Set emptySql
     *
     * @param string $emptySql
     * @return InfoFilterDictionary
     */
    public function setEmptySql(string $emptySql)
    {
        $this->emptySql = $emptySql;

        return $this;
    }

    /**
     * Get emptySql
     *
     * @return string
     */
    public function getEmptySql()
    {
        return $this->emptySql;
    }

    /**
     * Set sqlite
     *
     * @param string $sqlite
     * @return InfoFilterDictionary
     */
    public function setSqlite(string $sqlite)
    {
        $this->sqlite = $sqlite;

        return $this;
    }

    /**
     * Get sqlite
     *
     * @return string
     */
    public function getSqlite()
    {
        return $this->sqlite;
    }

    /**
     * Set emptySqlite
     *
     * @param string $emptySqlite
     * @return InfoFilterDictionary
     */
    public function setEmptySqlite(string $emptySqlite)
    {
        $this->emptySqlite = $emptySqlite;

        return $this;
    }

    /**
     * Get emptySqlite
     *
     * @return string
     */
    public function getEmptySqlite()
    {
        return $this->emptySqlite;
    }

    /**
     * Set subtitle
     *
     * @param string|null $subtitle
     * @return InfoFilterDictionary
     */
    public function setSubtitle(?string $subtitle): self
    {
        $this->subtitle = $subtitle;

        return $this;
    }

    /**
     * Get subtitle
     *
     * @return string|null
     */
    public function getSubtitle(): ?string
    {
        return $this->subtitle;
    }

    /**
     * Set additionalDict
     *
     * @param string|null $additionalDict
     * @return InfoFilterDictionary
     */
    public function setAdditionalDict(?string $additionalDict): self
    {
        $this->additionalDict = $additionalDict;

        return $this;
    }

    /**
     * Get additionalDict
     *
     * @return string|null
     */
    public function getAdditionalDict(): ?string
    {
        return $this->additionalDict;
    }

    /**
     * Set dependField
     *
     * @param string $dependField
     * @return InfoFilterDictionary
     */
    public function setDependField(?string $dependField): self
    {
        $this->dependField = $dependField;

        return $this;
    }

    /**
     * Get dependField
     *
     * @return string|null
     */
    public function getDependField(): ?string
    {
        return $this->dependField;
    }

    /**
     * Set guid
     *
     * @param string $guid
     * @return InfoFilterDictionary
     */
    public function setGuid($guid)
    {
        $this->guid = $guid;

        return $this;
    }

    /**
     * Get guid
     *
     * @return string
     */
    public function getGuid()
    {
        return $this->guid;
    }

    /**
     * Set currenttime
     *
     * @param \DateTime $currenttime
     * @return InfoFilterDictionary
     */
    public function setCurrenttime($currenttime)
    {
        $this->currenttime = $currenttime;

        return $this;
    }

    /**
     * Get currenttime
     *
     * @return \DateTime
     */
    public function getCurrenttime()
    {
        return $this->currenttime;
    }

    /**
     * Set moduser
     *
     * @param string $moduser
     * @return InfoFilterDictionary
     */
    public function setModuser($moduser)
    {
        $this->moduser = $moduser;

        return $this;
    }

    /**
     * Get moduser
     *
     * @return string
     */
    public function getModuser()
    {
        return $this->moduser;
    }

    /**
     * Set dictionary
     *
     * @param PoDictionary $dictionary
     * @return InfoFilterDictionary
     */
    public function setDictionary(PoDictionary $dictionary = null)
    {
        $this->dictionary = $dictionary;

        return $this;
    }

    /**
     * Get dictionary
     *
     * @return PoDictionary
     */
    public function getDictionary()
    {
        return $this->dictionary;
    }
}
