<?php

namespace TeamSoft\CrmRepositoryBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * InfoUsersubdirection
 *
 * @ORM\Table(name="info_usersubdirection")
 * @ORM\Entity
 */
class InfoUsersubdirection implements ServiceFieldInterface
{
    /**
     * @var string
     *
     * @ORM\Column(name="guid", type="guid", nullable=true)
     */
    private $guid;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="currenttime", type="datetime", nullable=true)
     */
    private $currenttime;

    /**
     * @var string
     *
     * @ORM\Column(name="moduser", type="string", length=16, nullable=true)
     */
    private $moduser;

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var \TeamSoft\CrmRepositoryBundle\Entity\InfoDirection
     *
     * @ORM\ManyToOne(targetEntity="InfoDirection")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="subdirection_id", referencedColumnName="id")
     * })
     */
    private $subdirection;

    /**
     * @var \TeamSoft\CrmRepositoryBundle\Entity\InfoUser
     *
     * @ORM\ManyToOne(targetEntity="InfoUser")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     * })
     */
    private $user;

    /**
     * Set guid
     *
     * @param string $guid
     *
     * @return InfoUsersubdirection
     */
    public function setGuid($guid)
    {
        $this->guid = $guid;

        return $this;
    }

    /**
     * Get guid
     *
     * @return string
     */
    public function getGuid()
    {
        return $this->guid;
    }

    /**
     * Set currenttime
     *
     * @param \DateTime $currenttime
     *
     * @return InfoUsersubdirection
     */
    public function setCurrenttime(\DateTime $currenttime)
    {
        $this->currenttime = $currenttime;

        return $this;
    }

    /**
     * Get currenttime
     *
     * @return \DateTime
     */
    public function getCurrenttime()
    {
        return $this->currenttime;
    }

    /**
     * Set moduser
     *
     * @param string $moduser
     *
     * @return InfoUsersubdirection
     */
    public function setModuser($moduser)
    {
        $this->moduser = $moduser;

        return $this;
    }

    /**
     * Get moduser
     *
     * @return string
     */
    public function getModuser()
    {
        return $this->moduser;
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set subdirection
     *
     * @param InfoDirection $subdirection
     *
     * @return InfoUsersubdirection
     */
    public function setSubdirection(InfoDirection $subdirection = null)
    {
        $this->subdirection = $subdirection;

        return $this;
    }

    /**
     * Get direction
     *
     * @return \TeamSoft\CrmRepositoryBundle\Entity\InfoDirection
     */
    public function getSubdirection()
    {
        return $this->subdirection;
    }

    /**
     * Set user
     *
     * @param \TeamSoft\CrmRepositoryBundle\Entity\InfoUser $user
     *
     * @return InfoUsersubdirection
     */
    public function setUser(\TeamSoft\CrmRepositoryBundle\Entity\InfoUser $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \TeamSoft\CrmRepositoryBundle\Entity\InfoUser
     */
    public function getUser()
    {
        return $this->user;
    }
}
