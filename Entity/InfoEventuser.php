<?php

namespace TeamSoft\CrmRepositoryBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * InfoEventuser
 *
 * @ORM\Table(name="info_eventuser")
 * @ORM\Entity
 */
class InfoEventuser
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var \InfoEvent
     *
     * @ORM\ManyToOne(targetEntity="InfoEvent")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="event_id", referencedColumnName="id")
     * })
     */
    private $event;

    /**
     * @var \InfoUser
     *
     * @ORM\ManyToOne(targetEntity="InfoUser")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     * })
     */
    private $user;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set event
     *
     * @param \TeamSoft\CrmRepositoryBundle\Entity\InfoEvent $event
     *
     * @return InfoEventuser
     */
    public function setEvent(\TeamSoft\CrmRepositoryBundle\Entity\InfoEvent $event = null)
    {
        $this->event = $event;

        return $this;
    }

    /**
     * Get event
     *
     * @return \TeamSoft\CrmRepositoryBundle\Entity\InfoEvent
     */
    public function getEvent()
    {
        return $this->event;
    }

    /**
     * Set user
     *
     * @param \TeamSoft\CrmRepositoryBundle\Entity\InfoUser $user
     *
     * @return InfoEventuser
     */
    public function setUser(\TeamSoft\CrmRepositoryBundle\Entity\InfoUser $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \TeamSoft\CrmRepositoryBundle\Entity\InfoUser
     */
    public function getUser()
    {
        return $this->user;
    }
}
