<?php

namespace TeamSoft\CrmRepositoryBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * InfoSaleType
 *
 * @ORM\Table(name="info_saletype")
 * @ORM\Entity
 */
class InfoSaleType
{

    const CODE_PURCHASES = 'zakupki';
    const CODE_REMAINS = 'ostatki';
    const CODE_SALES = 'prodazhi';
    const CODE_BEFORE = 'do';
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string|null
     *
     * @ORM\Column(name="name", type="string", length=255, nullable=true)
     */
    private $name;

    /**
     * @var string|null
     *
     * @ORM\Column(name="code", type="string", length=255, nullable=true)
     */
    private $code;

    /**
     * @var int|null
     *
     * @ORM\Column(name="is_discount", type="integer", nullable=true)
     */
    private $isDiscount;

    /**
     * @var integer|null
     *
     * @ORM\Column(name="is_for_granual", type="integer", nullable=true)
     */
    private $isForGranual;

    /**
     * Get id.
     *
     * @return int
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * Get name.
     *
     * @return string|null
     */
    public function getName(): ?string
    {
        return $this->name;
    }

    /**
     * Set name.
     *
     * @param string|null $name
     *
     * @return InfoSaleType
     */
    public function setName(?string $name): self
    {
        $this->name = $name;
        return $this;
    }

    /**
     * Get code.
     *
     * @return string|null
     */
    public function getCode(): ?string
    {
        return $this->code;
    }

    /**
     * Set code.
     *
     * @param string|null $code
     *
     * @return InfoSaleType
     */
    public function setCode(?string $code): self
    {
        $this->code = $code;
        return $this;
    }

    /**
     * @return int|null
     */
    public function getIsDiscount(): ?int
    {
        return $this->isDiscount;
    }

    /**
     * @param int|null $isDiscount
     * @return self
     */
    public function setIsDiscount(?int $isDiscount): self
    {
        $this->isDiscount = $isDiscount;
        return $this;
    }

    /**
     * @return int|null
     */
    public function getIsForGranual(): ?int
    {
        return $this->isDiscount;
    }

    /**
     * @param int|null $isForGranual
     * @return self
     */
    public function setIsForGranual(?int $isForGranual): self
    {
        $this->isDiscount = $isForGranual;
        return $this;
    }
}
