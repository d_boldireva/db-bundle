<?php

namespace TeamSoft\CrmRepositoryBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * InfoEntityMap
 *
 * @ORM\Table(name="info_entitymap")
 * @ORM\Entity(repositoryClass="TeamSoft\CrmRepositoryBundle\Repository\InfoEntityMap")
 */
class InfoEntityMap implements ServiceFieldInterface
{
    public const ENTITY_TYPE_PDK = 'pdk';
    public const ENTITY_TYPE_AGREEMENT = 'agreement';
    public const ENTITY_TYPE_FIRST_MAN = 'firstman';
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var int
     *
     * @ORM\Column(name="isshow", type="integer", nullable=true)
     */
    private $isShow;

    /**
     * @var int
     *
     * @ORM\Column(name="issearch", type="integer", nullable=true)
     */
    private $isSearch;

    /**
     * @var int
     *
     * @ORM\Column(name="position", type="integer", nullable=true)
     */
    private $position;

    /**
     * @var int
     *
     * @ORM\Column(name="colwidth", type="integer", nullable=true)
     */
    private $colWidth;

    /**
     * @var int
     *
     * @ORM\Column(name="isrequired", type="integer", nullable=true)
     */
    private $isRequired;

    /**
     * @var int
     *
     * @ORM\Column(name="isgetprev", type="integer", nullable=true)
     */
    private $isGetPrev;

    /**
     * @var int
     *
     * @ORM\Column(name="isfillfromprev", type="integer", nullable=true)
     */
    private $isFillFromPrev;

    /**
     * @var int
     *
     * @ORM\Column(name="isverify", type="integer", nullable=true)
     */
    private $isVerify;

    /**
     * @var string
     *
     * values: 'pdk', 'agreement', 'firstman'
     *
     * @ORM\Column(
     *     name="entitytype",
     *     type="string",
     *     length=255,
     *     nullable=true
     * )
     */
    private $entityType;

    /**
     * @var string
     *
     * values: '=', '!=', '>', '<', '>=', '<='
     *
     * @ORM\Column(
     *     name="verifycondition",
     *     type="string",
     *     length=255,
     *     nullable=true
     * )
     */
    private $verifyCondition;

    /**
     * @var string
     *
     * @ORM\Column(name="formula", type="string", length=255, nullable=true)
     */
    private $formula;

    /**
     * @var string
     *
     * @ORM\Column(name="dictcode", type="string", length=255, nullable=true)
     */
    private $dictCode;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255, nullable=true)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="fieldname", type="string", length=255, nullable=true)
     */
    private $fieldName;

    /**
     * @var string
     *
     * @ORM\Column(name="guid", type="guid", nullable=true)
     */
    private $guid;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="currenttime", type="datetime", nullable=true)
     */
    private $currenttime;

    /**
     * @var string
     *
     * @ORM\Column(name="moduser", type="string", length=16, nullable=true)
     */
    private $moduser;

    /**
     * @var InfoRole
     *
     * @ORM\ManyToOne(targetEntity="InfoRole", inversedBy="entityMaps")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="role_id", referencedColumnName="id")
     * })
     */
    private $role;

    /**
     * @var InfoTasktype
     *
     * @ORM\ManyToOne(targetEntity="InfoTasktype")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="tasktype_id", referencedColumnName="id")
     * })
     */
    private $taskType;

    /**
     * @ORM\ManyToMany(targetEntity="InfoEntityMapParam")
     * @ORM\JoinTable(name="info_entitymapparamjoin",
     *   joinColumns={@ORM\JoinColumn(name="entitymap_id", referencedColumnName="id")},
     *   inverseJoinColumns={@ORM\JoinColumn(name="entitymapparam_id", referencedColumnName="id")}
     * )
     */
    private $entityMapParams;

    public function __clone()
    {
        $this->id = null;
        $this->guid = null;
        $this->currenttime = null;
    }

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->entityMapParams = new ArrayCollection();
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set verifyCondition
     *
     * @param string $verifyCondition
     *
     * @return InfoEntityMap
     */
    public function setVerifyCondition($verifyCondition)
    {
        $this->verifyCondition = $verifyCondition;

        return $this;
    }

    /**
     * Get verifyCondition
     *
     * @return string
     */
    public function getVerifyCondition()
    {
        return $this->verifyCondition;
    }

    /**
     * Set dictCode
     *
     * @param string $dictCode
     *
     * @return InfoEntityMap
     */
    public function setDictCode($dictCode)
    {
        $this->dictCode = $dictCode;

        return $this;
    }

    /**
     * Get dictCode
     *
     * @return string
     */
    public function getDictCode()
    {
        return $this->dictCode;
    }

    /**
     * Set formula
     *
     * @param string $formula
     *
     * @return InfoEntityMap
     */
    public function setFormula($formula)
    {
        $this->formula = $formula;

        return $this;
    }

    /**
     * Get formula
     *
     * @return string
     */
    public function getFormula()
    {
        return $this->formula;
    }

    /**
     * Set isVerify
     *
     * @param string $isVerify
     *
     * @return InfoEntityMap
     */
    public function setIsVerify($isVerify)
    {
        $this->isVerify = $isVerify;

        return $this;
    }

    /**
     * Get isVerify
     *
     * @return string
     */
    public function getIsVerify()
    {
        return $this->isVerify;
    }

    /**
     * Set isSearch
     *
     * @param string $isSearch
     *
     * @return InfoEntityMap
     */
    public function setIsSearch($isSearch)
    {
        $this->isSearch = $isSearch;

        return $this;
    }

    /**
     * Get isSearch
     *
     * @return string
     */
    public function getIsSearch()
    {
        return $this->isSearch;
    }

    /**
     * Set isFillFromPrev
     *
     * @param string $isFillFromPrev
     *
     * @return InfoEntityMap
     */
    public function setIsFillFromPrev($isFillFromPrev)
    {
        $this->isFillFromPrev = $isFillFromPrev;

        return $this;
    }

    /**
     * Get isFillFromPrev
     *
     * @return string
     */
    public function getIsFillFromPrev()
    {
        return $this->isFillFromPrev;
    }

    /**
     * Set isGetPrev
     *
     * @param string $isGetPrev
     *
     * @return InfoEntityMap
     */
    public function setIsGetPrev($isGetPrev)
    {
        $this->isGetPrev = $isGetPrev;

        return $this;
    }

    /**
     * Get isGetPrev
     *
     * @return string
     */
    public function getIsGetPrev()
    {
        return $this->isGetPrev;
    }

    /**
     * Set isRequired
     *
     * @param string $isRequired
     *
     * @return InfoEntityMap
     */
    public function setIsRequired($isRequired)
    {
        $this->isRequired = $isRequired;

        return $this;
    }

    /**
     * Get isRequired
     *
     * @return string
     */
    public function getIsRequired()
    {
        return $this->isRequired;
    }

    /**
     * Set colWidth
     *
     * @param string $colWidth
     *
     * @return InfoEntityMap
     */
    public function setColWidth($colWidth)
    {
        $this->colWidth = $colWidth;

        return $this;
    }

    /**
     * Get colWidth
     *
     * @return string
     */
    public function getColWidth()
    {
        return $this->colWidth;
    }

    /**
     * Set position
     *
     * @param string $position
     *
     * @return InfoEntityMap
     */
    public function setPosition($position)
    {
        $this->position = $position;

        return $this;
    }

    /**
     * Get position
     *
     * @return string
     */
    public function getPosition()
    {
        return $this->position;
    }

    /**
     * Set isShow
     *
     * @param string $isShow
     *
     * @return InfoEntityMap
     */
    public function setIsShow($isShow)
    {
        $this->isShow = $isShow;

        return $this;
    }

    /**
     * Get isShow
     *
     * @return string
     */
    public function getIsShow()
    {
        return $this->isShow;
    }

    /**
     * Set entityType
     *
     * @param string $entityType
     *
     * @return InfoEntityMap
     */
    public function setEntityType(?string $entityType)
    {
        $this->entityType = $entityType;

        return $this;
    }

    /**
     * Get entityType
     *
     * @return string
     */
    public function getEntityType()
    {
        return $this->entityType;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return InfoEntityMap
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set fieldName
     *
     * @param string $fieldName
     *
     * @return InfoEntityMap
     */
    public function setFieldName($fieldName)
    {
        $this->fieldName = $fieldName;

        return $this;
    }

    /**
     * Get fieldName
     *
     * @return string
     */
    public function getFieldName()
    {
        return $this->fieldName;
    }

    /**
     * Set guid
     *
     * @param string $guid
     *
     * @return InfoEntityMap
     */
    public function setGuid($guid)
    {
        $this->guid = $guid;

        return $this;
    }

    /**
     * Get guid
     *
     * @return string
     */
    public function getGuid()
    {
        return $this->guid;
    }

    /**
     * Set currenttime
     *
     * @param \DateTime $currenttime
     *
     * @return InfoEntityMap
     */
    public function setCurrenttime(\DateTime $currenttime)
    {
        $this->currenttime = $currenttime;

        return $this;
    }

    /**
     * Get currenttime
     *
     * @return \DateTime
     */
    public function getCurrenttime()
    {
        return $this->currenttime;
    }

    /**
     * Set moduser
     *
     * @param string $moduser
     *
     * @return InfoEntityMap
     */
    public function setModuser($moduser)
    {
        $this->moduser = $moduser;

        return $this;
    }

    /**
     * Get moduser
     *
     * @return string
     */
    public function getModuser()
    {
        return $this->moduser;
    }

    /**
     * Set role
     *
     * @param InfoRole $role
     * @return InfoEntityMap
     */
    public function setRole(InfoRole $role = null)
    {
        $this->role = $role;

        return $this;
    }

    /**
     * Get role
     *
     * @return InfoRole
     */
    public function getRole()
    {
        return $this->role;
    }

    /**
     * Set taskType
     *
     * @param InfoTasktype $taskType
     * @return InfoEntityMap
     */
    public function setTaskType(InfoTasktype $taskType = null)
    {
        $this->taskType = $taskType;

        return $this;
    }

    /**
     * Get taskType
     *
     * @return InfoTasktype
     */
    public function getTaskType()
    {
        return $this->taskType;
    }

    /**
     * Add entityMapParam
     *
     * @param InfoEntityMapParam $entityMapParam
     *
     * @return InfoEntityMap
     */
    public function addEntityMapParam(InfoEntityMapParam $entityMapParam)
    {
        if (!$this->entityMapParams->contains($entityMapParam)) {
            $this->entityMapParams->add($entityMapParam);
        }

        return $this;
    }

    /**
     * Remove entityMapParam
     *
     * @param InfoEntityMapParam $entityMapParam
     */
    public function removeEntityMapParam(InfoEntityMapParam $entityMapParam)
    {
        $this->entityMapParams->removeElement($entityMapParam);
    }

    /**
     * Set entityMapParams
     *
     * @return self
     */
    public function setEntityMapParams(ArrayCollection $entityMapParams)
    {
        $this->entityMapParams = $entityMapParams;

        return $this;
    }

    /**
     * Get entityMapParams
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getEntityMapParams()
    {
        return $this->entityMapParams;
    }

    /**
     * @return array|string[]
     */
    public static function getEntityTypes(): array
    {
        return [
            self::ENTITY_TYPE_PDK,
            self::ENTITY_TYPE_AGREEMENT,
            self::ENTITY_TYPE_FIRST_MAN
        ];
    }

    public function isShow(): bool
    {
        return (bool) $this->getIsShow();
    }

    public function isRequired(): bool
    {
        return (bool) $this->getIsRequired();
    }
}
