<?php

namespace TeamSoft\CrmRepositoryBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * InfoFiltersetmobile
 *
 * @ORM\Table(name="info_filtersetmobile")
 * @ORM\Entity(repositoryClass="TeamSoft\CrmRepositoryBundle\Repository\InfoFiltersetmobile")
 */
class InfoFiltersetmobile
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var int
     *
     * @ORM\Column(name="page", type="integer", nullable=true)
     */
    private $page;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255, nullable=true)
     */
    private $name;


    /**
     * @var string
     *
     * @ORM\Column(name="name_en", type="string", length=255, nullable=true)
     */
    private $name_en;

    /**
     * @var string
     *
     * @ORM\Column(name="filter", type="string", length=8000, nullable=true)
     */
    private $filter;

    /**
     * @var int
     *
     * @ORM\Column(name="isdefault", type="integer", nullable=true)
     */
    private $isdefault;

    /**
     * @var guid
     *
     * @ORM\Column(name="guid", type="guid", nullable=true)
     */
    private $guid;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="currenttime", type="datetime", nullable=true)
     */
    private $currenttime;

    /**
     * @var string
     *
     * @ORM\Column(name="moduser", type="string", length=16, nullable=true)
     */
    private $moduser;

    /**
     * @var \InfoDirection
     *
     * @ORM\ManyToOne(targetEntity="InfoDirection")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="direction_id", referencedColumnName="id")
     * })
     */
    private $direction;

    /**
     * @var \InfoUser
     *
     * @ORM\ManyToOne(targetEntity="InfoUser")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="owner_id", referencedColumnName="id")
     * })
     */
    private $owner;

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set page
     *
     * @param int $page
     *
     * @return InfoFiltersetmobile
     */
    public function setPage($page)
    {
        $this->page = $page;

        return $this;
    }

    /**
     * Get page
     *
     * @return int
     */
    public function getPage()
    {
        return $this->page;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return InfoFiltersetmobile
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set filter
     *
     * @param string $filter
     *
     * @return InfoFiltersetmobile
     */
    public function setFilter($filter)
    {
        $this->filter = $filter;

        return $this;
    }

    /**
     * Get filter
     *
     * @return string
     */
    public function getFilter()
    {
        return $this->filter;
    }

    /**
     * Set isdefault
     *
     * @param int $isdefault
     *
     * @return InfoFiltersetmobile
     */
    public function setIsdefault($isdefault)
    {
        $this->isdefault = $isdefault;

        return $this;
    }

    /**
     * Get isdefault
     *
     * @return int
     */
    public function getIsdefault()
    {
        return $this->isdefault;
    }

    /**
     * Set guid
     *
     * @param guid $guid
     *
     * @return InfoFiltersetmobile
     */
    public function setGuid($guid)
    {
        $this->guid = $guid;

        return $this;
    }

    /**
     * Get guid
     *
     * @return guid
     */
    public function getGuid()
    {
        return $this->guid;
    }

    /**
     * Set currenttime
     *
     * @param \DateTime $currenttime
     *
     * @return InfoFiltersetmobile
     */
    public function setCurrenttime($currenttime)
    {
        $this->currenttime = $currenttime;

        return $this;
    }

    /**
     * Get currenttime
     *
     * @return \DateTime
     */
    public function getCurrenttime()
    {
        return $this->currenttime;
    }

    /**
     * Set moduser
     *
     * @param string $moduser
     *
     * @return InfoFiltersetmobile
     */
    public function setModuser($moduser)
    {
        $this->moduser = $moduser;

        return $this;
    }

    /**
     * Get moduser
     *
     * @return string
     */
    public function getModuser()
    {
        return $this->moduser;
    }

    /**
     * Set direction
     *
     * @param \TeamSoft\CrmRepositoryBundle\Entity\InfoDirection $direction
     *
     * @return InfoFiltersetmobile
     */
    public function setDirection(\TeamSoft\CrmRepositoryBundle\Entity\InfoDirection $direction = null)
    {
        $this->direction = $direction;

        return $this;
    }

    /**
     * Get direction
     *
     * @return \TeamSoft\CrmRepositoryBundle\Entity\InfoDirection
     */
    public function getDirection()
    {
        return $this->direction;
    }

    /**
     * Set owner
     *
     * @param \TeamSoft\CrmRepositoryBundle\Entity\InfoUser $owner
     *
     * @return InfoFiltersetmobile
     */
    public function setOwner(\TeamSoft\CrmRepositoryBundle\Entity\InfoUser $owner = null)
    {
        $this->owner = $owner;

        return $this;
    }

    /**
     * Get owner
     *
     * @return \TeamSoft\CrmRepositoryBundle\Entity\InfoUser
     */
    public function getOwner()
    {
        return $this->owner;
    }

    public function isFlag()
    {
        return strpos($this->getFilter(), '#FLAG#') !== false;
    }

    public function isSingle()
    {
        return strpos($this->getFilter(), '#SINGLE#') !== false;
    }

    /**
     * Set nameEn.
     *
     * @param string|null $nameEn
     *
     * @return InfoFiltersetmobile
     */
    public function setNameEn($nameEn = null)
    {
        $this->name_en = $nameEn;

        return $this;
    }

    /**
     * Get nameEn.
     *
     * @return string|null
     */
    public function getNameEn()
    {
        return $this->name_en;
    }


    public function getTranslations()
    {
        return [
            'ru' => [
                'name' => $this->name
            ],
            'en' => [
                'name' => $this->name_en
            ]
        ];
    }
}
