<?php

namespace TeamSoft\CrmRepositoryBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * InfoContactHistory
 *
 * @ORM\Table(name="info_contacthist")
 * @ORM\Entity
 */
class InfoContactHistory implements ServiceFieldInterface
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="contact_id", type="integer", nullable=true)
     */
    private $contactId;

    /**
     * @var integer
     *
     * @ORM\Column(name="modifier_id", type="integer", nullable=true)
     */
    private $modifierId;


    /**
     * @ORM\Column(type="datetime")
     */
    private $modified;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $field;

    /**
     * @ORM\Column(name="oldvalue", type="string")
     */
    private $oldValue;

    /**
     * @ORM\Column(name="newvalue", type="string", length=255, nullable=true)
     */
    private $newValue;


    /**
     * @ORM\Column(name="archivereason_id_old", type="integer", nullable=true)
     */
    private $archiveReasonIdOld;

    /**
     * @ORM\Column(name="archivereason_id_new", type="integer", nullable=true)
     */
    private $archiveReasonIdNew;

    /**
     * @ORM\Column(name="company_id_old", type="integer", nullable=true)
     */
    private $companyIdOld;

    /**
     * @ORM\Column(name="company_id_new", type="integer", nullable=true)
     */
    private $companyIdNew;

    /**
     * @ORM\Column(name="spec_id_old", type="integer", nullable=true)
     */
    private $specializationIdOld;

    /**
     * @ORM\Column(name="spec_id_new", type="integer", nullable=true)
     */
    private $specializationIdNew;

    /**
     * @ORM\Column(name="position_id_old", type="integer", nullable=true)
     */
    private $positionIdOld;

    /**
     * @ORM\Column(name="position_id_new", type="integer", nullable=true)
     */
    private $positionIdNew;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $comment;

    /**
     * @ORM\Column(type="guid", nullable=true)
     */
    private $guid;

    /**
     * @ORM\Column(name="currenttime", type="datetime", nullable=true)
     */
    private $currentTime;

    /**
     * @var string
     *
     * @ORM\Column(name="moduser", type="string", length=16, nullable=true)
     */
    private $modUser;

    /**
     * @ORM\Column(name="statusverification", type="integer", nullable=true)
     */
    private $statusVerification;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="sendverification", type="datetime", nullable=true)
     */
    private $sendVerification;

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return int
     */
    public function getContactId(): int
    {
        return $this->contactId;
    }

    /**
     * @param int $contactId
     */
    public function setContactId(int $contactId): void
    {
        $this->contactId = $contactId;
    }

    /**
     * @return int
     */
    public function getModifierId(): int
    {
        return $this->modifierId;
    }

    /**
     * @param int $modifierId
     */
    public function setModifierId(int $modifierId): void
    {
        $this->modifierId = $modifierId;
    }

    /**
     * @return mixed
     */
    public function getModified()
    {
        return $this->modified;
    }

    /**
     * @param mixed $modified
     */
    public function setModified($modified): void
    {
        $this->modified = $modified;
    }

    /**
     * @return mixed
     */
    public function getField()
    {
        return $this->field;
    }

    /**
     * @param mixed $field
     */
    public function setField($field): void
    {
        $this->field = $field;
    }

    /**
     * @return mixed
     */
    public function getOldValue()
    {
        return $this->oldValue;
    }

    /**
     * @param mixed $oldValue
     */
    public function setOldValue($oldValue): void
    {
        $this->oldValue = $oldValue;
    }

    /**
     * @return mixed
     */
    public function getNewValue()
    {
        return $this->newValue;
    }

    /**
     * @param mixed $newValue
     */
    public function setNewValue($newValue): void
    {
        $this->newValue = $newValue;
    }

    /**
     * @return mixed
     */
    public function getArchiveReasonIdOld()
    {
        return $this->archiveReasonIdOld;
    }

    /**
     * @param mixed $archiveReasonIdOld
     */
    public function setArchiveReasonIdOld($archiveReasonIdOld): void
    {
        $this->archiveReasonIdOld = $archiveReasonIdOld;
    }

    /**
     * @return mixed
     */
    public function getArchiveReasonIdNew()
    {
        return $this->archiveReasonIdNew;
    }

    /**
     * @param mixed $archiveReasonIdNew
     */
    public function setArchiveReasonIdNew($archiveReasonIdNew): void
    {
        $this->archiveReasonIdNew = $archiveReasonIdNew;
    }

    /**
     * @return mixed
     */
    public function getCompanyIdOld()
    {
        return $this->companyIdOld;
    }

    /**
     * @param mixed $companyIdOld
     */
    public function setCompanyIdOld($companyIdOld): void
    {
        $this->companyIdOld = $companyIdOld;
    }

    /**
     * @return mixed
     */
    public function getCompanyIdNew()
    {
        return $this->companyIdNew;
    }

    /**
     * @param mixed $companyIdNew
     */
    public function setCompanyIdNew($companyIdNew): void
    {
        $this->companyIdNew = $companyIdNew;
    }

    /**
     * @return mixed
     */
    public function getSpecializationIdOld()
    {
        return $this->specializationIdOld;
    }

    /**
     * @param mixed $specializationIdOld
     */
    public function setSpecializationIdOld($specializationIdOld): void
    {
        $this->specializationIdOld = $specializationIdOld;
    }

    /**
     * @return mixed
     */
    public function getSpecializationIdNew()
    {
        return $this->specializationIdNew;
    }

    /**
     * @param mixed $specializationIdNew
     */
    public function setSpecializationIdNew($specializationIdNew): void
    {
        $this->specializationIdNew = $specializationIdNew;
    }

    /**
     * @return mixed
     */
    public function getPositionIdOld()
    {
        return $this->positionIdOld;
    }

    /**
     * @param mixed $positionIdOld
     */
    public function setPositionIdOld($positionIdOld): void
    {
        $this->positionIdOld = $positionIdOld;
    }

    /**
     * @return mixed
     */
    public function getPositionIdNew()
    {
        return $this->positionIdNew;
    }

    /**
     * @param mixed $positionIdNew
     */
    public function setPositionIdNew($positionIdNew): void
    {
        $this->positionIdNew = $positionIdNew;
    }

    /**
     * @return mixed
     */
    public function getComment()
    {
        return $this->comment;
    }

    /**
     * @param mixed $comment
     */
    public function setComment($comment): void
    {
        $this->comment = $comment;
    }

    /**
     * Set guid
     *
     * @param string $guid
     * @return InfoContactHistory
     */
    public function setGuid($guid)
    {
        $this->guid = $guid;

        return $this;
    }

    /**
     * Get guid
     *
     * @return string
     */
    public function getGuid()
    {
        return $this->guid;
    }

    /**
     * Set currenttime
     *
     * @param \DateTime $currenttime
     * @return InfoContactHistory
     */
    public function setCurrenttime(\DateTime $currenttime)
    {
        $this->currentTime = $currenttime;

        return $this;
    }

    /**
     * Get currenttime
     *
     * @return \DateTime
     */
    public function getCurrenttime()
    {
        return $this->currentTime;
    }

    /**
     * Set moduser
     *
     * @param string $moduser
     * @return InfoContactHistory
     */
    public function setModuser($moduser)
    {
        $this->modUser = $moduser;

        return $this;
    }

    /**
     * Get moduser
     *
     * @return string
     */
    public function getModuser()
    {
        return $this->modUser;
    }

    /**
     * @return mixed
     */
    public function getStatusVerification()
    {
        return $this->statusVerification;
    }

    /**
     * @param mixed $statusVerification
     */
    public function setStatusVerification($statusVerification): void
    {
        $this->statusVerification = $statusVerification;
    }

    /**
     * @return mixed
     */
    public function getSendVerification()
    {
        return $this->sendVerification;
    }

    /**
     * @param mixed $sendVerification
     */
    public function setSendVerification($sendVerification): void
    {
        $this->sendVerification = $sendVerification;
    }
}
