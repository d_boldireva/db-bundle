<?php

namespace TeamSoft\CrmRepositoryBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * InfoEventobserver
 *
 * @ORM\Table(name="info_eventobserver")
 * @ORM\Entity
 */
class InfoEventobserver
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var \InfoEvent
     *
     * @ORM\ManyToOne(targetEntity="InfoEvent")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="event_id", referencedColumnName="id")
     * })
     */
    private $event;

    /**
     * @var \InfoUser
     *
     * @ORM\ManyToOne(targetEntity="InfoUser")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     * })
     */
    private $observer;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set event
     *
     * @param \TeamSoft\CrmRepositoryBundle\Entity\InfoEvent $event
     *
     * @return InfoEventobserver
     */
    public function setEvent(\TeamSoft\CrmRepositoryBundle\Entity\InfoEvent $event = null)
    {
        $this->event = $event;

        return $this;
    }

    /**
     * Get event
     *
     * @return \TeamSoft\CrmRepositoryBundle\Entity\InfoEvent
     */
    public function getEvent()
    {
        return $this->event;
    }

    /**
     * Set observer
     *
     * @param \TeamSoft\CrmRepositoryBundle\Entity\InfoUser $observer
     *
     * @return InfoEventobserver
     */
    public function setObserver(\TeamSoft\CrmRepositoryBundle\Entity\InfoUser $observer = null)
    {
        $this->observer = $observer;

        return $this;
    }

    /**
     * Get observer
     *
     * @return \TeamSoft\CrmRepositoryBundle\Entity\InfoUser
     */
    public function getObserver()
    {
        return $this->observer;
    }
}
