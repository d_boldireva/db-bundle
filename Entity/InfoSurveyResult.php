<?php

namespace TeamSoft\CrmRepositoryBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Criteria;
use Doctrine\ORM\Mapping as ORM;

/**
 * InfoSurveyResult
 *
 * @ORM\Table(name="info_surveyresult")
 * @ORM\Entity(repositoryClass="TeamSoft\CrmRepositoryBundle\Repository\InfoSurveyResult")
 */
class InfoSurveyResult implements ServiceFieldInterface
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="ans_text", type="string", length=255, nullable=true)
     */
    private $textAnswer;

    /**
     * @var float
     *
     * @ORM\Column(name="ans_float", type="float", precision=53, scale=0, nullable=true)
     */
    private $floatAnswer;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="fill_date", type="datetime", nullable=true)
     */
    private $fillDate;

    /**
     * @var string
     *
     * @ORM\Column(name="comment", type="string", length=255, nullable=true)
     */
    private $comment;

    /**
     * @var int
     *
     * @ORM\Column(name="isrejected", type="integer", nullable=true)
     */
    private $isRejected;

    /**
     * @var string
     *
     * @ORM\Column(name="guid", type="guid", nullable=true)
     */
    private $guid;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="currenttime", type="datetime", nullable=true)
     */
    private $currenttime;

    /**
     * @var string
     *
     * @ORM\Column(name="moduser", type="string", length=16, nullable=true)
     */
    private $moduser;

    /**
     * @var InfoTask
     *
     * @ORM\ManyToOne(targetEntity="InfoTask")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="task_id", referencedColumnName="id")
     * })
     */
    private $task;

    /**
     * @var InfoSurvey
     *
     * @ORM\ManyToOne(targetEntity="InfoSurvey")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="survey_id", referencedColumnName="id")
     * })
     */
    private $survey;

    /**
     * @var InfoSurveyQuestion
     *
     * @ORM\ManyToOne(targetEntity="InfoSurveyQuestion")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="question_id", referencedColumnName="id")
     * })
     */
    private $question;

    /**
     * @var InfoSurveyQuestionAnswer
     *
     * @ORM\ManyToOne(targetEntity="InfoSurveyQuestionAnswer")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="ans_list_id", referencedColumnName="id")
     * })
     */
    private $fixedAnswer;

    /**
     * @var ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="InfoSurveyResultCheck", mappedBy="result", cascade={"persist"}, orphanRemoval=true)
     */
    private $resultCheckCollection;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->resultCheckCollection = new ArrayCollection();
    }

    public function __clone()
    {
        $this->id = null;
        $this->guid = null;
        $this->currenttime = null;
        $this->moduser = null;
    }

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set textAnswer.
     *
     * @param string|null $textAnswer
     *
     * @return InfoSurveyResult
     */
    public function setTextAnswer($textAnswer = null)
    {
        $this->textAnswer = $textAnswer;

        return $this;
    }

    /**
     * Get textAnswer.
     *
     * @return string|null
     */
    public function getTextAnswer()
    {
        return $this->textAnswer;
    }

    /**
     * Set floatAnswer.
     *
     * @param float|null $floatAnswer
     *
     * @return InfoSurveyResult
     */
    public function setFloatAnswer($floatAnswer = null)
    {
        $this->floatAnswer = $floatAnswer;

        return $this;
    }

    /**
     * Get floatAnswer.
     *
     * @return float|null
     */
    public function getFloatAnswer()
    {
        return $this->floatAnswer;
    }

    /**
     * Set fillDate.
     *
     * @param \DateTime|null $fillDate
     *
     * @return InfoSurveyResult
     */
    public function setFillDate($fillDate = null)
    {
        $this->fillDate = $fillDate;

        return $this;
    }

    /**
     * Get fillDate.
     *
     * @return \DateTime|null
     */
    public function getFillDate()
    {
        return $this->fillDate;
    }

    /**
     * Set comment.
     *
     * @param string|null $comment
     *
     * @return InfoSurveyResult
     */
    public function setComment($comment = null)
    {
        $this->comment = $comment;

        return $this;
    }

    /**
     * Get comment.
     *
     * @return string|null
     */
    public function getComment()
    {
        return $this->comment;
    }

    /**
     * Set isRejected.
     *
     * @param int|null $isRejected
     *
     * @return InfoSurveyResult
     */
    public function setIsRejected($isRejected = null)
    {
        $this->isRejected = $isRejected;

        return $this;
    }

    /**
     * Get isRejected.
     *
     * @return int|null
     */
    public function getIsRejected()
    {
        return $this->isRejected;
    }

    /**
     * Set guid.
     *
     * @param string|null $guid
     *
     * @return InfoSurveyResult
     */
    public function setGuid($guid = null)
    {
        $this->guid = $guid;

        return $this;
    }

    /**
     * Get guid.
     *
     * @return string|null
     */
    public function getGuid()
    {
        return $this->guid;
    }

    /**
     * Set currenttime.
     *
     * @param \DateTime|null $currenttime
     *
     * @return InfoSurveyResult
     */
    public function setCurrenttime(\DateTime $currenttime = null)
    {
        $this->currenttime = $currenttime;

        return $this;
    }

    /**
     * Get currenttime.
     *
     * @return \DateTime|null
     */
    public function getCurrenttime()
    {
        return $this->currenttime;
    }

    /**
     * Set moduser.
     *
     * @param string|null $moduser
     *
     * @return InfoSurveyResult
     */
    public function setModuser($moduser = null)
    {
        $this->moduser = $moduser;

        return $this;
    }

    /**
     * Get moduser.
     *
     * @return string|null
     */
    public function getModuser()
    {
        return $this->moduser;
    }

    /**
     * Set task.
     *
     * @param \TeamSoft\CrmRepositoryBundle\Entity\InfoTask|null $task
     *
     * @return InfoSurveyResult
     */
    public function setTask(\TeamSoft\CrmRepositoryBundle\Entity\InfoTask $task = null)
    {
        $this->task = $task;

        return $this;
    }

    /**
     * Get task.
     *
     * @return \TeamSoft\CrmRepositoryBundle\Entity\InfoTask|null
     */
    public function getTask()
    {
        return $this->task;
    }

    /**
     * Set survey.
     *
     * @param \TeamSoft\CrmRepositoryBundle\Entity\InfoSurvey|null $survey
     *
     * @return InfoSurveyResult
     */
    public function setSurvey(\TeamSoft\CrmRepositoryBundle\Entity\InfoSurvey $survey = null)
    {
        $this->survey = $survey;

        return $this;
    }

    /**
     * Get survey.
     *
     * @return \TeamSoft\CrmRepositoryBundle\Entity\InfoSurvey|null
     */
    public function getSurvey()
    {
        return $this->survey;
    }

    /**
     * Set question.
     *
     * @param \TeamSoft\CrmRepositoryBundle\Entity\InfoSurveyQuestion|null $question
     *
     * @return InfoSurveyResult
     */
    public function setQuestion(\TeamSoft\CrmRepositoryBundle\Entity\InfoSurveyQuestion $question = null)
    {
        $this->question = $question;

        return $this;
    }

    /**
     * Get question.
     *
     * @return \TeamSoft\CrmRepositoryBundle\Entity\InfoSurveyQuestion|null
     */
    public function getQuestion()
    {
        return $this->question;
    }

    /**
     * Set fixedAnswer.
     *
     * @param \TeamSoft\CrmRepositoryBundle\Entity\InfoSurveyQuestionAnswer|null $fixedAnswer
     *
     * @return InfoSurveyResult
     */
    public function setFixedAnswer(\TeamSoft\CrmRepositoryBundle\Entity\InfoSurveyQuestionAnswer $fixedAnswer = null)
    {
        $this->fixedAnswer = $fixedAnswer;

        return $this;
    }

    /**
     * Get fixedAnswer.
     *
     * @return \TeamSoft\CrmRepositoryBundle\Entity\InfoSurveyQuestionAnswer|null
     */
    public function getFixedAnswer()
    {
        return $this->fixedAnswer;
    }

    /**
     * Add resultCheckCollection.
     *
     * @param \TeamSoft\CrmRepositoryBundle\Entity\InfoSurveyResultCheck $resultCheckCollection
     *
     * @return InfoSurveyResult
     */
    public function addResultCheckCollection(\TeamSoft\CrmRepositoryBundle\Entity\InfoSurveyResultCheck $resultCheckCollection)
    {
        $resultCheckCollection->setResult($this);
        $this->resultCheckCollection[] = $resultCheckCollection;

        return $this;
    }

    /**
     * Remove resultCheckCollection.
     *
     * @param \TeamSoft\CrmRepositoryBundle\Entity\InfoSurveyResultCheck $resultCheckCollection
     *
     * @return boolean TRUE if this collection contained the specified element, FALSE otherwise.
     */
    public function removeResultCheckCollection(\TeamSoft\CrmRepositoryBundle\Entity\InfoSurveyResultCheck $resultCheckCollection)
    {
        return $this->resultCheckCollection->removeElement($resultCheckCollection);
    }

    /**
     * Get resultCheckCollection.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getResultCheckCollection()
    {
        return $this->resultCheckCollection;
    }

    public function getSurveyId(): ?int
    {
        return $this->survey ? $this->survey->getId() : null;
    }

    public function getQuestionId(): ?int
    {
        return $this->question ? $this->question->getId() : null;
    }

    public function getFixedAnswerId(): ?int
    {
        return $this->fixedAnswer ? $this->fixedAnswer->getId() : null;
    }
}
