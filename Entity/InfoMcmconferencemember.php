<?php

namespace TeamSoft\CrmRepositoryBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * InfoMcmconferencemember
 *
 * @ORM\Table(name="info_mcmconferencemember")
 * @ORM\Entity
 */
class InfoMcmconferencemember implements ServiceFieldInterface
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="price", type="integer", nullable=true)
     */
    private $price;

    /**
     * @var string
     *
     * @ORM\Column(name="currency", type="string", length=255, nullable=true)
     */
    private $currency;

    /**
     * @var InfoCustomdictionaryvalue
     *
     * @ORM\ManyToOne(targetEntity="InfoCustomdictionaryvalue")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="member_role_id", referencedColumnName="id")
     * })
     */
    private $memberRole;

    /**
     * @var string
     *
     * @ORM\Column(name="last_date", type="datetime", nullable=true)
     */
    private $lastDate;

    /**
     * @var string
     *
     * @ORM\Column(name="last_subject", type="string", length=255, nullable=true)
     */
    private $lastSubject;

    /**
     * @var string
     *
     * @ORM\Column(name="token_received", type="integer", nullable=true)
     */
    private $tokenReceived;

    /**
     * @var string
     *
     * @ORM\Column(name="access_conference", type="integer", nullable=true)
     */
    private $accessConference;

    /**
     * @var string
     *
     * @ORM\Column(name="prev_access_conference", type="integer", nullable=true)
     */
    private $prevAccessConference;

    /**
     * @var string
     *
     * @ORM\Column(name="access_video", type="integer", nullable=true)
     */
    private $accessVideo;

    /**
     * @var string
     *
     * @ORM\Column(name="access_pdf", type="integer", nullable=true)
     */
    private $accessPdf;

    /**
     * @var string
     *
     * @ORM\Column(name="code_conf", type="string", length=255, nullable=true)
     */
    private $codeConf;

    /**
     * @var string
     *
     * @ORM\Column(name="certificate", type="integer", nullable=true)
     */
    private $certificate;

    /**
     * @var string
     *
     * @ORM\Column(name="user_name", type="string", length=255, nullable=true)
     */
    private $userName;

    /**
     * @var string
     *
     * @ORM\Column(name="user_pass", type="string", length=255, nullable=true)
     */
    private $userPass;

    /**
     * @var string
     *
     * @ORM\Column(name="user_link", type="string", length=255, nullable=true)
     */
    private $userLink;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="currenttime", type="datetime", nullable=true)
     */
    private $currenttime;

    /**
     * @var string
     *
     * @ORM\Column(name="moduser", type="string", length=16, nullable=true)
     */
    private $moduser;

    /**
     * @var string
     *
     * @ORM\Column(name="guid", type="guid", nullable=true)
     */
    private $guid;

    /**
     * @var InfoMcmconferencecontact
     *
     * @ORM\ManyToOne(targetEntity="InfoMcmconferencecontact")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="conferencecontact_id", referencedColumnName="id")
     * })
     */
    private $conferenceContact;

    /**
     * @var InfoContact
     *
     * @ORM\ManyToOne(targetEntity="InfoContact")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="contact_id", referencedColumnName="id")
     * })
     */
    private $contact;

    /**
     * @var InfoCustomdictionaryvalue
     *
     * @ORM\ManyToOne(targetEntity="InfoCustomdictionaryvalue")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="format_id", referencedColumnName="id")
     * })
     */
    private $format;

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set price
     *
     * @param string $price
     *
     * @return InfoMcmconferencemember
     */
    public function setPrice($price)
    {
        $this->price = $price;

        return $this;
    }

    /**
     * Get price
     *
     * @return string
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * Set currency
     *
     * @param string $currency
     *
     * @return InfoMcmconferencemember
     */
    public function setCurrency($currency)
    {
        $this->currency = $currency;

        return $this;
    }

    /**
     * Get currency
     *
     * @return string
     */
    public function getCurrency()
    {
        return $this->currency;
    }

    /**
     * Set contact
     *
     * @param string $contact
     *
     * @return InfoMcmconferencemember
     */
    public function setContact($contact)
    {
        $this->contact = $contact;

        return $this;
    }

    /**
     * Get contact
     *
     * @return string
     */
    public function getContact()
    {
        return $this->contact;
    }

    /**
     * Set conferenceContact
     *
     * @param string $conferenceContact
     *
     * @return InfoMcmconferencemember
     */
    public function setConferenceContact($conferenceContact)
    {
        $this->conferenceContact = $conferenceContact;

        return $this;
    }

    /**
     * Get conferenceContact
     *
     * @return string
     */
    public function getConferenceContact()
    {
        return $this->conferenceContact;
    }

    /**
     * Set member memberRole
     *
     * @param InfoCustomdictionaryvalue $memberRole
     *
     * @return InfoMcmconferencemember
     */
    public function setMemberRole($memberRole)
    {
        $this->memberRole = $memberRole;

        return $this;
    }

    /**
     * Get member memberRole
     *
     * @return InfoCustomdictionaryvalue
     */
    public function getMemberRole()
    {
        return $this->memberRole;
    }

    /**
     * Set format
     *
     * @param InfoCustomdictionaryvalue $format
     *
     * @return InfoMcmconferencemember
     */
    public function setFormat($format)
    {
        $this->format = $format;

        return $this;
    }

    /**
     * Get format
     *
     * @return InfoCustomdictionaryvalue
     */
    public function getFormat()
    {
        return $this->format;
    }

    /**
     * Set lastDate
     *
     * @param string $lastDate
     *
     * @return InfoMcmconferencemember
     */
    public function setLastDate($lastDate)
    {
        $this->lastDate = $lastDate;

        return $this;
    }

    /**
     * Get lastDate
     *
     * @return string
     */
    public function getLastDate()
    {
        return $this->lastDate;
    }

    /**
     * Set lastSubject
     *
     * @param string $lastSubject
     *
     * @return InfoMcmconferencemember
     */
    public function setLastSubject($lastSubject)
    {
        $this->lastSubject = $lastSubject;

        return $this;
    }

    /**
     * Get lastSubject
     *
     * @return string
     */
    public function getLastSubject()
    {
        return $this->lastSubject;
    }

    /**
     * Set tokenReceived
     *
     * @param string $tokenReceived
     *
     * @return InfoMcmconferencemember
     */
    public function setTokenReceived($tokenReceived)
    {
        $this->tokenReceived = $tokenReceived;

        return $this;
    }

    /**
     * Get tokenReceived
     *
     * @return string
     */
    public function getTokenReceived()
    {
        return $this->tokenReceived;
    }

    /**
     * Set accessConference
     *
     * @param string $accessConference
     *
     * @return InfoMcmconferencemember
     */
    public function setAccessConference($accessConference)
    {
        $this->accessConference = $accessConference;

        return $this;
    }

    /**
     * Get accessConference
     *
     * @return string
     */
    public function getAccessConference()
    {
        return $this->accessConference;
    }

    /**
     * Set prevAccessConference
     *
     * @param string $prevAccessConference
     *
     * @return InfoMcmconferencemember
     */
    public function setPrevAccessConference($prevAccessConference)
    {
        $this->prevAccessConference = $prevAccessConference;

        return $this;
    }

    /**
     * Get prevAccessConference
     *
     * @return string
     */
    public function getPrevAccessConference()
    {
        return $this->prevAccessConference;
    }

    /**
     * Set accessVideo
     *
     * @param string $accessVideo
     *
     * @return InfoMcmconferencemember
     */
    public function setAccessVideo($accessVideo)
    {
        $this->accessVideo = $accessVideo;

        return $this;
    }

    /**
     * Get accessVideo
     *
     * @return string
     */
    public function getAccessVideo()
    {
        return $this->accessVideo;
    }

    /**
     * Set accessPdf
     *
     * @param string $accessPdf
     *
     * @return InfoMcmconferencemember
     */
    public function setAccessPdf($accessPdf)
    {
        $this->accessPdf = $accessPdf;

        return $this;
    }

    /**
     * Get accessPdf
     *
     * @return string
     */
    public function getAccessPdf()
    {
        return $this->accessPdf;
    }

    /**
     * Set certificate
     *
     * @param string $certificate
     *
     * @return InfoMcmconferencemember
     */
    public function setCertificate($certificate)
    {
        $this->certificate = $certificate;

        return $this;
    }

    /**
     * Get certificate
     *
     * @return string
     */
    public function getCertificate()
    {
        return $this->certificate;
    }

    /**
     * Set codeConf
     *
     * @param string $codeConf
     *
     * @return InfoMcmconferencemember
     */
    public function setCodeConf($codeConf)
    {
        $this->codeConf = $codeConf;

        return $this;
    }

    /**
     * Get codeConf
     *
     * @return string
     */
    public function getCodeConf()
    {
        return $this->codeConf;
    }

    /**
     * Set userName
     *
     * @param string $userName
     *
     * @return InfoMcmconferencemember
     */
    public function setUserName($userName)
    {
        $this->userName = $userName;

        return $this;
    }

    /**
     * Get userName
     *
     * @return string
     */
    public function getUserName()
    {
        return $this->userName;
    }

    /**
     * Set userPass
     *
     * @param string $userPass
     *
     * @return InfoMcmconferencemember
     */
    public function setUserPass($userPass)
    {
        $this->userPass = $userPass;

        return $this;
    }

    /**
     * Get userPass
     *
     * @return string
     */
    public function getUserPass()
    {
        return $this->userPass;
    }

    /**
     * Set userLink
     *
     * @param string $userLink
     *
     * @return InfoMcmconferencemember
     */
    public function setUserLink($userLink)
    {
        $this->userLink = $userLink;

        return $this;
    }

    /**
     * Get userLink
     *
     * @return string
     */
    public function getUserLink()
    {
        return $this->userLink;
    }

    /**
     * Set currenttime
     *
     * @param \DateTime $currenttime
     *
     * @return InfoMcmconferencemember
     */
    public function setCurrenttime(\DateTime $currenttime)
    {
        $this->currenttime = $currenttime;

        return $this;
    }

    /**
     * Get currenttime
     *
     * @return \DateTime
     */
    public function getCurrenttime()
    {
        return $this->currenttime;
    }

    /**
     * Set moduser
     *
     * @param string $moduser
     *
     * @return InfoMcmconferencemember
     */
    public function setModuser($moduser)
    {
        $this->moduser = $moduser;

        return $this;
    }

    /**
     * Get moduser
     *
     * @return string
     */
    public function getModuser()
    {
        return $this->moduser;
    }

    /**
     * Set guid
     *
     * @param string $guid
     *
     * @return InfoMcmconferencemember
     */
    public function setGuid($guid)
    {
        $this->guid = $guid;

        return $this;
    }

    /**
     * Get guid
     *
     * @return string
     */
    public function getGuid()
    {
        return $this->guid;
    }
}
