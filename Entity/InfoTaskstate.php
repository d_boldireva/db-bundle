<?php

namespace TeamSoft\CrmRepositoryBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * InfoTaskstate
 *
 * @ORM\Table(name="info_taskstate", indexes={@ORM\Index(name="ix_info_taskstate_guid", columns={"guid"}), @ORM\Index(name="ix_info_taskstate_time", columns={"time"})})
 * @ORM\Entity(repositoryClass="TeamSoft\CrmRepositoryBundle\Repository\InfoTaskstate")
 */
class InfoTaskstate
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="Name", type="string", length=255, nullable=false)
     */
    private $name;

    /**
     * @var integer
     *
     * @ORM\Column(name="isclosed", type="integer", nullable=true)
     */
    private $isclosed;

    /**
     * @var integer
     *
     * @ORM\Column(name="color", type="integer", nullable=true)
     */
    private $color;

    /**
     * @var integer
     *
     * @ORM\Column(name="isfinish", type="integer", nullable=true)
     */
    private $isfinish;

    /**
     * @var string
     *
     * @ORM\Column(name="guid", type="guid", nullable=true)
     */
    private $guid;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="currenttime", type="datetime", nullable=true)
     */
    private $currenttime;

    /**
     * @var string
     *
     * @ORM\Column(name="moduser", type="string", length=16, nullable=true)
     */
    private $moduser;

    /**
     * @var string
     *
     * @ORM\Column(name="colorhex", type="string", length=255, nullable=true)
     */
    private $colorhex;

    /**
     * @var InfoLanguage
     *
     * @ORM\ManyToOne(targetEntity="InfoLanguage")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="language_id", referencedColumnName="id")
     * })
     */
    private $language;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return InfoTaskstate
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set isclosed
     *
     * @param integer $isclosed
     * @return InfoTaskstate
     */
    public function setIsclosed($isclosed)
    {
        $this->isclosed = $isclosed;

        return $this;
    }

    /**
     * Get isclosed
     *
     * @return integer
     */
    public function getIsclosed()
    {
        return $this->isclosed;
    }

    /**
     * Set color
     *
     * @param integer $color
     * @return InfoTaskstate
     */
    public function setColor($color)
    {
        $this->color = $color;

        return $this;
    }

    /**
     * Get color
     *
     * @return integer
     */
    public function getColor()
    {
        return $this->color;
    }

    /**
     * Set isfinish
     *
     * @param integer $isfinish
     * @return InfoTaskstate
     */
    public function setIsfinish($isfinish)
    {
        $this->isfinish = $isfinish;

        return $this;
    }

    /**
     * Get isfinish
     *
     * @return integer
     */
    public function getIsfinish()
    {
        return $this->isfinish;
    }

    /**
     * Set guid
     *
     * @param string $guid
     * @return InfoTaskstate
     */
    public function setGuid($guid)
    {
        $this->guid = $guid;

        return $this;
    }

    /**
     * Get guid
     *
     * @return string
     */
    public function getGuid()
    {
        return $this->guid;
    }

    /**
     * Set currenttime
     *
     * @param \DateTime $currenttime
     * @return InfoTaskstate
     */
    public function setCurrenttime($currenttime)
    {
        $this->currenttime = $currenttime;

        return $this;
    }

    /**
     * Get currenttime
     *
     * @return \DateTime
     */
    public function getCurrenttime()
    {
        return $this->currenttime;
    }

    /**
     * Set moduser
     *
     * @param string $moduser
     * @return InfoTaskstate
     */
    public function setModuser($moduser)
    {
        $this->moduser = $moduser;

        return $this;
    }

    /**
     * Get moduser
     *
     * @return string
     */
    public function getModuser()
    {
        return $this->moduser;
    }

    /**
     * Set colorhex
     *
     * @param string $colorhex
     * @return InfoTaskstate
     */
    public function setColorhex($colorhex)
    {
        $this->colorhex = $colorhex;

        return $this;
    }

    /**
     * Get colorhex
     *
     * @return string
     */
    public function getColorhex()
    {
        return $this->colorhex;
    }

    /**
     * Set language
     *
     * @param InfoLanguage|null $content
     * @return InfoTaskstate
     */
    public function setLanguage(InfoLanguage $language = null)
    {
        $this->language = $language;

        return $this;
    }

    /**
     * Get language
     *
     * @return InfoLanguage
     */
    public function getLanguage()
    {
        return $this->language;
    }

    /**
     * @return bool
     */
    public function isDone(): bool
    {
        return $this->getIsclosed() && $this->getIsfinish();
    }
}
