<?php

namespace TeamSoft\CrmRepositoryBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use TeamSoft\CrmRepositoryBundle\Entity\ServiceFieldInterface;

/**
 * InfoClmmapslide
 *
 * @ORM\Table(name="info_clmmapslide")
 * @ORM\Entity
 */
class InfoClmmapslide implements ServiceFieldInterface
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var int
     *
     * @ORM\Column(name="position", type="integer", nullable=true)
     */
    private $position;

    /**
     * @var string
     *
     * @ORM\Column(name="guid", type="guid", nullable=true)
     */
    private $guid;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="currenttime", type="datetime", nullable=true)
     */
    private $currenttime;

    /**
     * @var string
     *
     * @ORM\Column(name="moduser", type="string", length=16, nullable=true)
     */
    private $moduser;

    /**
     * @var \TeamSoft\CrmRepositoryBundle\Entity\InfoClmmap
     *
     * @ORM\ManyToOne(targetEntity="InfoClmmap", cascade={"remove"})
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="clmmap_id", referencedColumnName="id")
     * })
     */
    private $clmmap;

    /**
     * @var \TeamSoft\CrmRepositoryBundle\Entity\InfoClmslide
     *
     * @ORM\ManyToOne(targetEntity="InfoClmslide", cascade={"remove"})
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="clmslide_id", referencedColumnName="id")
     * })
     */
    private $clmslide;



    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set position
     *
     * @param int $position
     *
     * @return InfoClmmapslide
     */
    public function setPosition($position)
    {
        $this->position = $position;
    
        return $this;
    }

    /**
     * Get position
     *
     * @return int
     */
    public function getPosition()
    {
        return $this->position;
    }

    /**
     * Set guid
     *
     * @param string $guid
     *
     * @return InfoClmmapslide
     */
    public function setGuid($guid)
    {
        $this->guid = $guid;
    
        return $this;
    }

    /**
     * Get guid
     *
     * @return string
     */
    public function getGuid()
    {
        return $this->guid;
    }

    /**
     * Set currenttime
     *
     * @param \DateTime $currenttime
     *
     * @return InfoClmmapslide
     */
    public function setCurrenttime(\DateTime $currenttime)
    {
        $this->currenttime = $currenttime;
    
        return $this;
    }

    /**
     * Get currenttime
     *
     * @return \DateTime
     */
    public function getCurrenttime()
    {
        return $this->currenttime;
    }

    /**
     * Set moduser
     *
     * @param string $moduser
     *
     * @return InfoClmmapslide
     */
    public function setModuser($moduser)
    {
        $this->moduser = $moduser;
    
        return $this;
    }

    /**
     * Get moduser
     *
     * @return string
     */
    public function getModuser()
    {
        return $this->moduser;
    }

    /**
     * Set clmmap
     *
     * @param \TeamSoft\CrmRepositoryBundle\Entity\InfoClmmap $clmmap
     *
     * @return InfoClmmapslide
     */
    public function setClmmap(\TeamSoft\CrmRepositoryBundle\Entity\InfoClmmap $clmmap = null)
    {
        $this->clmmap = $clmmap;
    
        return $this;
    }

    /**
     * Get clmmap
     *
     * @return \TeamSoft\CrmRepositoryBundle\Entity\InfoClmmap
     */
    public function getClmmap()
    {
        return $this->clmmap;
    }

    /**
     * Set clmslide
     *
     * @param \TeamSoft\CrmRepositoryBundle\Entity\InfoClmslide $clmslide
     *
     * @return InfoClmmapslide
     */
    public function setClmslide(\TeamSoft\CrmRepositoryBundle\Entity\InfoClmslide $clmslide = null)
    {
        $this->clmslide = $clmslide;
    
        return $this;
    }

    /**
     * Get clmslide
     *
     * @return \TeamSoft\CrmRepositoryBundle\Entity\InfoClmslide
     */
    public function getClmslide()
    {
        return $this->clmslide;
    }
}
