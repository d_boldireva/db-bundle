<?php

namespace TeamSoft\CrmRepositoryBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * InfoReport
 *
 * @ORM\Table(name="info_report")
 * @ORM\Entity
 */
class InfoReport implements ServiceFieldInterface
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255, nullable=true)
     */
    private $name;

    /**
     * @var bool
     *
     * @ORM\Column(name="is_web", type="integer", nullable=true)
     */
    private $isWeb;

    /**
     * @var bool
     *
     * @ORM\Column(name="is_mcm", type="integer", nullable=true)
     */
    private $isMcm;

    /**
     * @var bool
     *
     * @ORM\Column(name="version", type="integer", nullable=true)
     */
    private $version;

    /**
     * @var int
     *
     * @ORM\Column(name="`limit`", type="integer", nullable=true)
     */
    private $limit;

    /**
     * @var bool
     *
     * @ORM\Column(name="is_mobile", type="integer", nullable=true)
     */
    private $isMobile;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="string", length=255, nullable=true)
     */
    private $description;

    /**
     * @var string
     *
     * @ORM\Column(name="report", type="blob", nullable=true)
     */
    private $report;

    /**
     * @var string
     *
     * @ORM\Column(name="guid", type="guid", nullable=true)
     */
    private $guid;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="currenttime", type="datetime", nullable=true)
     */
    private $currenttime;

    /**
     * @var string
     *
     * @ORM\Column(name="moduser", type="string", length=16, nullable=true)
     */
    private $moduser;

    /**
     * @ORM\OneToMany(targetEntity="InfoReportaccess", mappedBy="report", cascade={"persist", "remove"}, orphanRemoval=true)
     */
    private $reportAccesses;

    /**
     * @ORM\OneToMany(targetEntity="InfoReportTab", mappedBy="report", cascade={"persist", "remove"}, orphanRemoval=true)
     */
    private $tabs;

    /**
     * @ORM\OneToMany(targetEntity="InfoReportsql", mappedBy="report", cascade={"persist", "remove"}, orphanRemoval=true)
     */
    private $reportSqls;


    public function __construct()
    {
        $this->reportAccesses = new ArrayCollection();
        $this->tabs = new ArrayCollection();
        $this->reportSqls = new ArrayCollection();
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return InfoReport
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set isWeb
     *
     * @param bool $isWeb
     *
     * @return InfoReport
     */
    public function setIsWeb($isWeb)
    {
        $this->isWeb = $isWeb;

        return $this;
    }

    /**
     * Get bool
     *
     * @return bool
     */
    public function getIsWeb()
    {
        return $this->isWeb;
    }

    /**
     * Set isMcm
     *
     * @param bool $isMcm
     *
     * @return InfoReport
     */
    public function setIsMcm($isMcm)
    {
        $this->isMcm = $isMcm;

        return $this;
    }

    /**
     * Get bool
     *
     * @return bool
     */
    public function getIsMcm()
    {
        return $this->isMcm;
    }

    /**
     * Set isMobile
     *
     * @param number $isMobile
     *
     * @return InfoReport
     */
    public function setIsMobile($isMobile)
    {
        $this->isMobile = $isMobile;

        return $this;
    }

    /**
     * Get isMobile
     *
     * @return bool
     */
    public function getIsMobile()
    {
        return $this->isMobile;
    }

    /**
     * Set version
     *
     * @param number $version
     *
     * @return InfoReport
     */
    public function setVersion($version)
    {
        $this->version = $version;

        return $this;
    }

    /**
     * Get version
     *
     * @return bool
     */
    public function getVersion()
    {
        return $this->version;
    }

    /**
     * Set limit
     *
     * @param number $limit
     *
     * @return InfoReport
     */
    public function setLimit($limit)
    {
        $this->limit = $limit;

        return $this;
    }

    /**
     * Get limit
     *
     * @return bool
     */
    public function getLimit()
    {
        return $this->limit;
    }

    /**
     * Set description
     *
     * @param string $description
     *
     * @return InfoReport
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set report
     *
     * @param string $report
     *
     * @return InfoReport
     */
    public function setReport($report)
    {
        $this->report = $report;

        return $this;
    }

    /**
     * Get report
     *
     * @return string
     */
    public function getReport()
    {
        return $this->report;
    }

    /**
     * Set guid
     *
     * @param string $guid
     *
     * @return InfoReport
     */
    public function setGuid($guid)
    {
        $this->guid = $guid;

        return $this;
    }

    /**
     * Get guid
     *
     * @return string
     */
    public function getGuid()
    {
        return $this->guid;
    }

    /**
     * Set currenttime
     *
     * @param \DateTime $currenttime
     *
     * @return InfoReport
     */
    public function setCurrenttime(\DateTime $currenttime)
    {
        $this->currenttime = $currenttime;

        return $this;
    }

    /**
     * Get currenttime
     *
     * @return \DateTime
     */
    public function getCurrenttime()
    {
        return $this->currenttime;
    }

    /**
     * Set moduser
     *
     * @param string $moduser
     *
     * @return InfoReport
     */
    public function setModuser($moduser)
    {
        $this->moduser = $moduser;

        return $this;
    }

    /**
     * Get moduser
     *
     * @return string
     */
    public function getModuser()
    {
        return $this->moduser;
    }

    /**
     * Add reportAccess
     *
     * @param InfoReportaccess $reportAccess
     * @return InfoReport
     */
    public function addReportAccess(InfoReportaccess $reportAccess)
    {
        $reportAccess->setReport($this);
        $this->reportAccesses->add($reportAccess);
        return $this;
    }

    /**
     * Remove reportAccess
     *
     * @param InfoReportaccess $reportAccess
     */
    public function removeReportAccess(InfoReportaccess $reportAccess)
    {
        $this->reportAccesses->removeElement($reportAccess);
        $reportAccess->setReport(null);
    }

    /**
     * Get reportAccess
     *
     * @return Collection
     */
    public function getReportAccesses()
    {
        return $this->reportAccesses;
    }

    /**
     * Add tab
     *
     * @param InfoReportTab $tab
     * @return InfoReport
     */
    public function addTab(InfoReportTab $tab)
    {
        $tab->setReport($this);
        $this->tabs->add($tab);
        return $this;
    }

    /**
     * Remove tab
     *
     * @param InfoReportTab $tab
     */
    public function removeTab(InfoReportTab $tab)
    {
        $tab->setReport(null);
        $this->tabs->removeElement($tab);
    }

    /**
     * Get tab
     *
     * @return Collection
     */
    public function getTabs()
    {
        return $this->tabs;
    }

    /**
     * Add reportSql
     *
     * @param InfoReportsql $reportSql
     * @return InfoReport
     */
    public function addReportSql(InfoReportsql $reportSql)
    {
        $reportSql->setReport($this);
        $this->reportSqls->add($reportSql);
        return $this;
    }

    /**
     * Remove reportSql
     *
     * @param InfoReportsql $reportSql
     */
    public function removeReportSql(InfoReportsql $reportSql)
    {
        $reportSql->setReport(null);
        $this->reportSqls->removeElement($reportSql);
    }

    /**
     * Get reportSql
     *
     * @return Collection
     */
    public function getReportSqls()
    {
        return $this->reportSqls;
    }

    /**
     * Set reportSql
     *
     * @param ArrayCollection $reportSqls
     * @return InfoReport
     */
    public function setReportSqls(ArrayCollection $reportSqls)
    {
        $this->reportSqls = $reportSqls;

        return $this;
    }
}
