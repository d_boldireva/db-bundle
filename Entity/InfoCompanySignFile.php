<?php

namespace TeamSoft\CrmRepositoryBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use TeamSoft\CrmRepositoryBundle\Model\InfoFile;

/**
 * InfoCompanySignFile
 *
 * @ORM\Table(name="info_companysignfile")
 * @ORM\Entity
 */
class InfoCompanySignFile extends InfoFile implements ServiceFieldInterface
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="currenttime", type="datetime", nullable=true)
     */
    private $currenttime;

    /**
     * @var string|null
     *
     * @ORM\Column(name="moduser", type="string", length=16, nullable=true)
     */
    private $moduser;

    /**
     * @var string|null
     *
     * @ORM\Column(name="guid", type="guid", nullable=true)
     */
    private $guid;

    /**
     * @var string|null
     *
     * @ORM\Column(name="agreement", type="blob_or_string", nullable=true)
     */
    private $agreement;

    /**
     * @var InfoCompanySign
     *
     * @ORM\ManyToOne (targetEntity="InfoCompanySign", inversedBy="companySignFiles")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="companysign_id", referencedColumnName="id")
     * })
     */
    private $companySign;

    /**
     * @var string|null
     *
     * @ORM\Column(name="filetype", type="string", length=16, nullable=true)
     */
    private $filetype;


    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set currenttime.
     *
     * @param \DateTime|null $currenttime
     *
     * @return self
     */
    public function setCurrenttime(\DateTime $currenttime = null)
    {
        $this->currenttime = $currenttime;

        return $this;
    }

    /**
     * Get currenttime.
     *
     * @return \DateTime|null
     */
    public function getCurrenttime()
    {
        return $this->currenttime;
    }

    /**
     * Set moduser.
     *
     * @param string|null $moduser
     *
     * @return self
     */
    public function setModuser($moduser = null)
    {
        $this->moduser = $moduser;

        return $this;
    }

    /**
     * Get moduser.
     *
     * @return string|null
     */
    public function getModuser()
    {
        return $this->moduser;
    }

    /**
     * Set guid.
     *
     * @param string|null $guid
     *
     * @return self
     */
    public function setGuid($guid = null)
    {
        $this->guid = $guid;

        return $this;
    }

    /**
     * Get guid.
     *
     * @return string|null
     */
    public function getGuid()
    {
        return $this->guid;
    }

    /**
     * Set agreement.
     *
     * @param string|null $agreement
     *
     * @return self
     */
    public function setAgreement($agreement = null)
    {
        $this->agreement = $agreement;

        return $this;
    }

    /**
     * Get agreement.
     *
     * @return string|null
     */
    public function getAgreement()
    {
        return $this->agreement;
    }

    /**
     * Set filetype.
     *
     * @param string|null $filetype
     *
     * @return self
     */
    public function setFiletype($filetype = null)
    {
        $this->filetype = $filetype;

        return $this;
    }

    /**
     * Get filetype.
     *
     * @return string|null
     */
    public function getFiletype(): ?string
    {
        return $this->filetype;
    }

    /**
     * Set contactSign.
     *
     * @param InfoCompanySign|null $companySign
     *
     * @return self
     */
    public function setCompanySign(?InfoCompanySign $companySign)
    {
        $this->companySign = $companySign;

        return $this;
    }

    /**
     * Get contactSign.
     *
     * @return InfoCompanySign|null
     */
    public function getCompanySign()
    {
        return $this->companySign;
    }

    public function getName()
    {
        return '';
    }

    /**
     * Set content
     *
     * @param string $content
     *
     * @return self
     */
    public function setContent($content)
    {
        return $this->setAgreement($content);
    }

    /**
     * Get content
     *
     * @return resource | string
     */
    public function getContent()
    {
        return $this->getAgreement();
    }
}
