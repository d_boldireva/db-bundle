<?php

namespace TeamSoft\CrmRepositoryBundle\Entity\Sales;

use Doctrine\ORM\Mapping as ORM;
use TeamSoft\CrmRepositoryBundle\Entity;
use Doctrine\Common\Collections\ArrayCollection;
use TeamSoft\CrmRepositoryBundle\Entity\InfoCity;
use TeamSoft\CrmRepositoryBundle\Entity\InfoCompany;
use TeamSoft\CrmRepositoryBundle\Entity\InfoRegion;
use TeamSoft\CrmRepositoryBundle\Entity\ServiceFieldInterface;
use TeamSoft\CrmRepositoryBundle\Entity\Sales\SynonymInterface;
use TeamSoft\CrmRepositoryBundle\Entity\Sales\SynonymTrait;

/**
 * VwSynOrg
 *
 * @ORM\Table(name="vw_syn_org")
 * @ORM\Entity
 */
class VwSynOrg implements SynonymInterface, ServiceFieldInterface
{
    use SynonymTrait;

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=1000, nullable=true)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="address", type="string", length=255, nullable=true)
     */
    private $address;

    /**
     * @var string
     *
     * @ORM\Column(name="region", type="string", length=255, nullable=true)
     */
    private $region;

    /**
     * @var string
     *
     * @ORM\Column(name="city", type="string", length=255, nullable=true)
     */
    private $city;

    /**
     * @var int
     *
     * @ORM\Column(name="org_id_morion", type="integer", nullable=true)
     */
    private $orgIdMorion;

    /**
     * @var int
     *
     * @ORM\Column(name="edr_code", type="integer", nullable=true)
     */
    private $edrCode;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="currenttime", type="datetime", nullable=true)
     */
    private $currenttime = 'CURRENT_TIMESTAMP';

    /**
     * @var InfoCity
     *
     * @ORM\ManyToOne(targetEntity="TeamSoft\CrmRepositoryBundle\Entity\InfoCity")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="city_id", referencedColumnName="id")
     * })
     */
    private $infoCity;

    /**
     * @var InfoRegion
     *
     * @ORM\ManyToOne(targetEntity="TeamSoft\CrmRepositoryBundle\Entity\InfoRegion")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="region_id", referencedColumnName="id")
     * })
     */
    private $infoRegion;

    /**
     * @var InfoCompany
     *
     * @ORM\ManyToOne(targetEntity="TeamSoft\CrmRepositoryBundle\Entity\InfoCompany")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="company_id", referencedColumnName="id")
     * })
     */
    private $company;

    /**
     * @var SynCity
     *
     * @ORM\ManyToOne(targetEntity="TeamSoft\CrmRepositoryBundle\Entity\Sales\SynCity")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="syncity_id", referencedColumnName="id")
     * })
     */
    private $synCity;

    /**
     * @var SynRegion
     *
     * @ORM\ManyToOne(targetEntity="TeamSoft\CrmRepositoryBundle\Entity\Sales\SynRegion")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="synregion_id", referencedColumnName="id")
     * })
     */
    private $synRegion;

    /**
     * @var PsUploadedfiles
     *
     * @ORM\ManyToMany(targetEntity="TeamSoft\CrmRepositoryBundle\Entity\Sales\PsUploadedfiles")
     * @ORM\JoinTable(
     *     name="ps_synorg_uploadfile",
     *     joinColumns={@ORM\JoinColumn(name="company_hash", referencedColumnName="company_hash")},
     *     inverseJoinColumns={@ORM\JoinColumn(name="uploadedfile_id", referencedColumnName="id")}
     * )
     */
    private $psUploadedfiles;

    /**
     * @var int
     *
     * @ORM\Column(name="is_reference", type="integer", nullable=true)
     */
    private $is_reference;

    /**
     * @var float
     *
     * @ORM\Column(name="count", type="float", nullable=true)
     */
    private $count;

    /**
     * @var string
     *
     * @ORM\Column(name="company_hash", type="string", length=32, nullable=true)
     */
    private $company_hash;


    public function __construct(){
        $this->psUploadedfiles = new ArrayCollection();
    }


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return VwSynOrg
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set name
     *
     * @param string $address
     *
     * @return VwSynOrg
     */
    public function setAddress($address)
    {
        $this->address = $address;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getAddress()
    {
        return $this->address;
    }

    /**
     * Set orgIdMorion
     *
     * @param int $orgIdMorion
     *
     * @return VwSynOrg
     */
    public function setOrgIdMorion($orgIdMorion)
    {
        $this->orgIdMorion = $orgIdMorion;

        return $this;
    }

    /**
     * Get orgIdMorion
     *
     * @return int
     */
    public function getOrgIdMorion()
    {
        return $this->orgIdMorion;
    }

    /**
     * Set edrCode
     *
     * @param int $edrCode
     *
     * @return VwSynOrg
     */
    public function setEdrCode($edrCode)
    {
        $this->edrCode = $edrCode;

        return $this;
    }

    /**
     * Get edrCode
     *
     * @return int
     */
    public function getEdrCode()
    {
        return $this->edrCode;
    }

    /**
     * Set currenttime
     *
     * @param \DateTime $currenttime
     *
     * @return VwSynOrg
     */
    public function setCurrenttime(\DateTime $currenttime)
    {
        $this->currenttime = $currenttime;

        return $this;
    }

    /**
     * Get currenttime
     *
     * @return \DateTime
     */
    public function getCurrenttime()
    {
        return $this->currenttime;
    }

    /**
     * Set city
     *
     * @param string|null $city
     *
     * @return VwSynOrg
     */
    public function setCity(?string $city = null)
    {
        $this->city = $city;

        return $this;
    }

    /**
     * Get city
     *
     * @return string|null
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
     * Set region
     *
     * @param string|null $region
     *
     * @return VwSynOrg
     */
    public function setRegion(?string $region = null)
    {
        $this->region = $region;

        return $this;
    }

    /**
     * Get region
     *
     * @return string
     */
    public function getRegion()
    {
        return $this->region;
    }

    /**
     * Set company
     *
     * @param Entity\InfoCompany $company
     *
     * @return VwSynOrg
     */
    public function setCompany(?Entity\InfoCompany $company = null)
    {
        $this->company = $company;

        return $this;
    }

    /**
     * Get company
     *
     * @return Entity\InfoCompany
     */
    public function getCompany()
    {
        return $this->company;
    }

    /**
     * @param Entity\InfoCity $infoCity
     */
    public function setInfoCity(?Entity\InfoCity $infoCity)
    {
        $this->infoCity = $infoCity;
    }

    /**
     * @return Entity\InfoCity
     */
    public function getInfoCity(): ?Entity\InfoCity
    {
        return $this->infoCity;
    }

    /**
     * @param Entity\InfoRegion $infoRegion
     */
    public function setInfoRegion(?Entity\InfoRegion $infoRegion)
    {
        $this->infoRegion = $infoRegion;
    }

    /**
     * @return Entity\InfoRegion
     */
    public function getInfoRegion(): ?Entity\InfoRegion
    {
        return $this->infoRegion;
    }

    /**
     * @return SynCity
     */
    public function getSynCity(): ?SynCity
    {
        return $this->synCity;
    }

    /**
     * @param SynCity $synCity
     */
    public function setSynCity(SynCity $synCity)
    {
        $this->synCity = $synCity;
    }

    /**
     * @return SynRegion
     */
    public function getSynRegion(): ?SynRegion
    {
        return $this->synRegion;
    }

    /**
     * @param SynRegion $synRegion
     */
    public function setSynRegion(SynRegion $synRegion)
    {
        $this->synRegion = $synRegion;
    }

    public function getGuid()
    {
        // TODO: Implement getGuid() method.
    }

    public function setGuid($value)
    {
        // TODO: Implement setGuid() method.
    }

    public function getModuser()
    {
        // TODO: Implement getModuser() method.
    }

    public function setModuser($value)
    {
        // TODO: Implement setModuser() method.
    }

    /**
     * Get is_reference
     *
     * @return int
     */
    public function getIsReference(){
        return $this->is_reference ?? 0;
    }

    /**
     * Add psUploadedFilesCollection
     *
     * @param PsUploadedfiles $psUploadedFilesCollection
     *
     * @return VwSynOrg
     */
    public function addPsUploadedFilesCollection(PsUploadedfiles $psUploadedFilesCollection)
    {
        $this->psUploadedfiles[] = $psUploadedFilesCollection;

        return $this;
    }

    /**
     * Remove psUploadedFilesCollection
     *
     * @param PsUploadedfiles $psUploadedFilesCollection
     */
    public function removePsUploadedFilesCollection(PsUploadedfiles $psUploadedFilesCollection)
    {
        $this->psUploadedfiles->removeElement($psUploadedFilesCollection);
    }

    /**
     * Get psUploadedFilesCollection
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getPsUploadedFilesCollection()
    {
        return $this->psUploadedfiles;
    }

    /**
     * Get company_hash
     *
     * @return null|string
     */
    public function getCompanyHash() : ?string {
        return $this->company_hash;
    }

    /**
     * Set company_hash
     *
     * @param null|string $company_hash
     * @return VwSynOrg
     */
    public function setCompanyHash(?string $company_hash) : VwSynOrg {
        $this->company_hash = $company_hash;

        return $this;
    }

    /**
     * @param float $count
     * @return VwSynOrg
     */
    public function setCount(float $count = 0) : VwSynOrg {
        $this->count = $count;

        return $this;
    }

    /**
     * @return float
     */
    public function getCount() : float {
        return $this->count;
    }
}
