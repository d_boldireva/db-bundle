<?php

namespace TeamSoft\CrmRepositoryBundle\Entity\Sales;

use Doctrine\ORM\Mapping as ORM;

/**
 * PsPatternSkippedRows
 *
 * @ORM\Table(name="ps_pattern_skipped_rows")
 * @ORM\Entity
 */
class PsPatternSkippedRows
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="row_num", type="integer", nullable=true)
     */
    private $rowNum;

    /**
     * @var \PsPattern
     *
     * @ORM\ManyToOne(targetEntity="PsPattern")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="pattern_id", referencedColumnName="id")
     * })
     */
    private $pattern;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set rowNum
     *
     * @param integer $rowNum
     *
     * @return PsPatternSkippedRows
     */
    public function setRowNum($rowNum)
    {
        $this->rowNum = $rowNum;

        return $this;
    }

    /**
     * Get rowNum
     *
     * @return integer
     */
    public function getRowNum()
    {
        return $this->rowNum;
    }

    /**
     * Set pattern
     *
     * @param PsPattern $pattern
     *
     * @return PsPatternSkippedRows
     */
    public function setPattern(?PsPattern $pattern = null)
    {
        $this->pattern = $pattern;

        return $this;
    }

    /**
     * Get pattern
     *
     * @return \PsPattern
     */
    public function getPattern()
    {
        return $this->pattern;
    }
}
