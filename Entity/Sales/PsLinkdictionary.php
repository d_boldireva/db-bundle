<?php

namespace TeamSoft\CrmRepositoryBundle\Entity\Sales;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * SynOrg
 *
 * @ORM\Table(name="ps_linkdictionary")
 * @ORM\Entity
 */
class PsLinkdictionary
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="morion_id", type="integer", nullable=false)
     */
    private $morionId;

    /**
     * @var integer
     *
     * @ORM\Column(name="id_link", type="integer", nullable=false)
     */
    private $idLink;

    /**
     * @var string
     *
     * @ORM\Column(name="company_hash", type="string", length=32, nullable=false)
     */
    private $companyHash;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=1000, nullable=false)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="addr", type="string", length=1000, nullable=false)
     */
    private $addr;

    /**
     * Get id
     *
     * @return int
     */
    public function getID() : int {
        return $this->id;
    }

    /**
     * Get morion id
     *
     * @return int
     */
    public function getMorionId() : int {
        return $this->morionId;
    }

    /**
     * Set morion id
     *
     * @param int $morionId
     * @return PsLinkdictionary
     */
    public function setMorionId(int $morionId) : PsLinkdictionary {
        $this->morionId = $morionId;

        return $this;
    }

    /**
     * Get idLink
     *
     * @return int
     */
    public function getIdLink() : int {
        return $this->idLink;
    }

    /**
     * Set id link
     *
     * @param int $idLink
     * @return PsLinkdictionary
     */
    public function setIdLink(int $idLink) : PsLinkdictionary {
        $this->idLink = $idLink;

        return $this;
    }

    /**
     * Get company hash
     *
     * @return string
     */
    public function getCompanyHash() : string {
        return $this->companyHash;
    }

    /**
     * Set company hash
     *
     * @param string $companyHash
     * @return PsLinkdictionary
     */
    public function setCompanyHash(string $companyHash) : PsLinkdictionary {
        $this->companyHash = $companyHash;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName() : string {
        return $this->name;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return PsLinkdictionary
     */
    public function setName(string $name) : PsLinkdictionary {
        $this->name = $name;

        return $this;
    }

    /**
     * get Address
     *
     * @return string
     */
    public function getAddr() : string {
        return $this->addr;
    }

    /**
     * set Address
     *
     * @param string $addr
     * @return PsLinkdictionary
     */
    public function setAddr(string $addr) : PsLinkdictionary {
        $this->addr = $addr;

        return $this;
    }

    /**
     * get nameaddr
     *
     * @return string
     */
    public function getNameAddr() : string {
        return $this->name.($this->addr ? " {$this->addr}" : '');
    }
}
