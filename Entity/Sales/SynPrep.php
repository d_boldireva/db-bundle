<?php

namespace TeamSoft\CrmRepositoryBundle\Entity\Sales;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use TeamSoft\CrmRepositoryBundle\Entity\ServiceFieldInterface;
use TeamSoft\CrmRepositoryBundle\Entity\Sales\SynonymInterface;
use TeamSoft\CrmRepositoryBundle\Entity\Sales\SynonymTrait;
use TeamSoft\CrmRepositoryBundle\Entity\InfoPreparation;

/**
 * SynPrep
 *
 * @ORM\Table(name="syn_prep")
 * @ORM\Entity(repositoryClass="TeamSoft\CrmRepositoryBundle\Repository\SynPrep")
 */
class SynPrep implements SynonymInterface, ServiceFieldInterface
{
    use SynonymTrait;

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=250, nullable=true)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="coef", type="float", nullable=false)
     */
    private $coef = 1;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="currenttime", type="datetime", nullable=true)
     */
    private $currenttime = 'CURRENT_TIMESTAMP';

    /**
     * @var InfoPreparation
     *
     * @ORM\ManyToOne(targetEntity="TeamSoft\CrmRepositoryBundle\Entity\InfoPreparation")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="prep_id", referencedColumnName="id")
     * })
     */
    private $prep;

    /**
     * @var float
     *
     * @ORM\Column(name="count", type="float", nullable=true)
     */
    private $count;

    /**
     * @var PsUploadedfiles
     *
     * @ORM\ManyToMany(targetEntity="TeamSoft\CrmRepositoryBundle\Entity\Sales\PsUploadedfiles")
     * @ORM\JoinTable(
     *     name="ps_synprep_uploadfile",
     *     joinColumns={@ORM\JoinColumn(name="synprep_id", referencedColumnName="id")},
     *     inverseJoinColumns={@ORM\JoinColumn(name="uploadedfile_id", referencedColumnName="id")}
     * )
     */
    private $psUploadedfiles;

    /**
     * @var int
     *
     * ARTERIUMCA-140
     *
     * @ORM\Column(name="externalcode", type="string", length=32, nullable=true)
     */
    private $externalcode;

    public function __construct()
    {
        $this->psUploadedfiles = new ArrayCollection();
    }


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return SynPrep
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set currenttime
     *
     * @param \DateTime $currenttime
     *
     * @return SynPrep
     */
    public function setCurrenttime(\DateTime $currenttime)
    {
        $this->currenttime = $currenttime;

        return $this;
    }

    /**
     * Get currenttime
     *
     * @return \DateTime
     */
    public function getCurrenttime()
    {
        return $this->currenttime;
    }

    /**
     * Set prep
     *
     * @param InfoPreparation $prep
     *
     * @return SynPrep
     */
    public function setPrep(InfoPreparation $prep = null)
    {
        $this->prep = $prep;

        return $this;
    }

    /**
     * Get prep
     *
     * @return InfoPreparation
     */
    public function getPrep()
    {
        return $this->prep;
    }

    public function getPrepId()
    {
        return $this->getPrep() ? $this->getPrep()->getId() : null;
    }

    public function getGuid()
    {
        // TODO: Implement getGuid() method.
    }

    public function setGuid($value)
    {
        // TODO: Implement setGuid() method.
    }

    public function getModuser()
    {
        // TODO: Implement getModuser() method.
    }

    public function setModuser($value)
    {
        // TODO: Implement setModuser() method.
    }

    /**
     * @return string
     */
    public function getCoef(): ?float
    {
        return $this->coef ?: 1;
    }

    /**
     * @param string $coef
     */
    public function setCoef(?float $coef)
    {
        $this->coef = $coef;
    }

    /**
     * @param float $count
     * @return SynPrep
     */
    public function setCount(float $count = 0): SynPrep
    {
        $this->count = $count;

        return $this;
    }

    /**
     * @return float|null
     */
    public function getCount(): ?float
    {
        return $this->count;
    }

    /**
     * Add psUploadedFilesCollection
     *
     * @param PsUploadedfiles $psUploadedFilesCollection
     *
     * @return SynPrep
     */
    public function addPsUploadedFilesCollection(PsUploadedfiles $psUploadedFilesCollection)
    {
        $this->psUploadedfiles[] = $psUploadedFilesCollection;

        return $this;
    }

    /**
     * Remove psUploadedFilesCollection
     *
     * @param PsUploadedfiles $psUploadedFilesCollection
     *
     * @return SynPrep
     */
    public function removePsUploadedFilesCollection(PsUploadedfiles $psUploadedFilesCollection)
    {
        $this->psUploadedfiles->removeElement($psUploadedFilesCollection);

        return $this;
    }

    /**
     * Get psUploadedFilesCollection
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getPsUploadedFilesCollection()
    {
        return $this->psUploadedfiles;
    }

    /**
     * Set external code
     *
     * ARTERIUMCA-140
     *
     * @param $externalcode
     * @return $this
     */
    public function setExternalCode($externalcode)
    {
        $this->externalcode = $externalcode;
        return $this;
    }

    /**
     * Get external code
     *
     * ARTERIUMCA-140
     *
     * @return int
     */
    public function getExternalCode()
    {
        return $this->externalcode;
    }
}
