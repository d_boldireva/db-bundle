<?php

namespace TeamSoft\CrmRepositoryBundle\Entity\Sales;


use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\JoinColumn;
use Doctrine\ORM\Mapping\OneToOne;
use TeamSoft\CrmRepositoryBundle\Entity\ServiceFieldInterface;

/**
 * PsProject
 *
 * @ORM\Table(name="ps_projectfield")
 * @ORM\Entity
 */
class PsProjectField implements ServiceFieldInterface
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;


    /**
     * @OneToOne(targetEntity="PsProject")
     * @JoinColumn(name="project_id", referencedColumnName="id")
     */
    private $project;

    /**
     *
     * @ORM\ManyToOne(targetEntity="PsProjectFieldDictionary")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="field_id", referencedColumnName="id")
     * })
     */
    private $field;

    /**
     * @var string
     *
     * @ORM\Column(name="guid", type="guid", nullable=true)
     */
    private $guid;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="currenttime", type="datetime", nullable=true)
     */
    private $currenttime;


    /**
     * @var string
     *
     * @ORM\Column(name="moduser", type="string", length=16, nullable=true)
     */
    private $moduser;

    /**
     * @return int
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getProject(): ?PsProject
    {
        return $this->project;
    }

    /**
     * @param mixed $project
     *
     * @return PsProjectField
     */
    public function setProject(?PsProject $project): self
    {
        $this->project = $project;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getField(): ?PsProjectFieldDictionary
    {
        return $this->field;
    }

    /**
     * @param mixed $field
     *
     * @return PsProjectField
     */
    public function setField(?PsProjectFieldDictionary $field): self
    {
        $this->field = $field;

        return $this;
    }

    /**
     * @return string
     */
    public function getGuid(): ?string
    {
        return $this->guid;
    }

    /**
     * @param string $guid
     *
     * @return PsProjectField
     */
    public function setGuid($guid): self
    {
        $this->guid = $guid;

        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getCurrenttime(): ?\DateTime
    {
        return $this->currenttime;
    }

    /**
     * @param \DateTime $currenttime
     *
     * @return PsProjectField
     */
    public function setCurrenttime(?\DateTime $currenttime): self
    {
        $this->currenttime = $currenttime;

        return $this;
    }

    /**
     * @return string
     */
    public function getModuser(): ?string
    {
        return $this->moduser;
    }

    /**
     * @param string $moduser
     *
     * @return PsProjectField
     */
    public function setModuser($moduser): self
    {
        $this->moduser = $moduser;

        return $this;
    }
}
