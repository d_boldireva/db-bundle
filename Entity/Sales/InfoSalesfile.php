<?php

namespace TeamSoft\CrmRepositoryBundle\Entity\Sales;

use Symfony\Component\HttpFoundation\File\UploadedFile;
use TeamSoft\CrmRepositoryBundle\Entity\ServiceFieldInterface;

use Doctrine\ORM\Mapping as ORM;

/**
 * Class InfoSalesfile
 *
 * @ORM\Table(name="info_sales_file")
 * @ORM\Entity()
 * @ORM\EntityListeners({"TeamSoft\CrmRepositoryBundle\EventListener\InfoSalesfileListener"})
 */
class InfoSalesfile implements ServiceFieldInterface
{
    /**
     * @var int
     *
     * @ORM\Id()
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    protected $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255, nullable=true)
     */
    protected $name;

    /**
     * @var string
     *
     * @ORM\Column(name="original_name", type="string", length=512, nullable=true)
     */
    protected $originalName;

    /**
     * @var int
     *
     * @ORM\Column(name="size", type="integer", nullable=true)
     */
    protected $size = '0';

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="create_date", type="datetime", nullable=true)
     */
    protected $createDate;

    /**
     * @var string
     *
     * @ORM\Column(name="table_name", type="string", length=255, nullable=true)
     */
    protected $tableName;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="currenttime", type="datetime", nullable=true)
     */
    protected $currenttime;

    /**
     * @var string
     *
     * @ORM\Column(name="moduser", type="string", length=16, nullable=true)
     */
    protected $moduser;

    /**
     * @var string
     *
     * @ORM\Column(name="guid", type="guid", nullable=true)
     */
    protected $guid;


    /**
     * @var UploadedFile $file
     */
    protected $file;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string|null $name
     * @return $this
     */
    public function setName(?string $name = null)
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return string
     */
    public function getOriginalName()
    {
        return $this->originalName;
    }

    /**
     * @param string|null $name
     * @return $this
     */
    public function setOriginalName(?string $name = null)
    {
        $this->originalName = $name;
        return $this;
    }

    /**
     * @return int
     */
    public function getSize()
    {
        return $this->size;
    }

    /**
     * @param int $size
     * @return $this
     */
    public function setSize(int $size = 0)
    {
        $this->size = $size;
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getCreateDate()
    {
        return $this->createDate;
    }

    /**
     * @param \DateTime|null $createDate
     * @return $this
     */
    public function setCreateDate(\DateTime $createDate = null)
    {
        $this->createDate = $createDate;
        return $this;
    }

    /**
     * @return string
     */
    public function getTableName()
    {
        return $this->tableName;
    }

    /**
     * @param string|null $tableName
     * @return $this
     */
    public function setTableName(?string $tableName = null)
    {
        $this->tableName = $tableName;
        return $this;
    }


    /**
     * @return \DateTime|string
     */
    public function getCurrenttime()
    {
        return $this->currenttime;
    }

    /**
     * @param \DateTime $value
     * @return $this|ServiceFieldInterface
     */
    public function setCurrenttime(\DateTime $value)
    {
        $this->currenttime = $value;
        return $this;
    }

    /**
     * @return string
     */
    public function getModuser()
    {
        return $this->moduser;
    }

    /**
     * @param string $value
     * @return $this|ServiceFieldInterface
     */
    public function setModuser($value)
    {
        $this->moduser = $value;
        return $this;
    }

    /**
     * @return int|string
     */
    public function getGuid()
    {
        return $this->guid;
    }

    /**
     * @param int $value
     * @return $this|ServiceFieldInterface
     */
    public function setGuid($value)
    {
        $this->guid = $value;

        return $this;
    }

    /**
     * @return UploadedFile
     */
    public function getFile()
    {
        return $this->file;
    }

    /**
     * @param UploadedFile $file
     * @return $this
     */
    public function setFile(UploadedFile $file)
    {
        $this->file = $file;

        return $this;
    }

    /**
     * Return true if model used for upload patterns
     * @return bool
     */
    public function isPattern(): bool
    {
        return !$this->getTableName();
    }
}
