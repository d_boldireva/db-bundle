<?php

namespace TeamSoft\CrmRepositoryBundle\Entity\Sales;

use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use TeamSoft\CrmRepositoryBundle\Entity\InfoCity;
use TeamSoft\CrmRepositoryBundle\Entity\InfoRegion;
use TeamSoft\CrmRepositoryBundle\Entity\ServiceFieldInterface;
use TeamSoft\CrmRepositoryBundle\Entity\Sales\SynonymInterface;
use TeamSoft\CrmRepositoryBundle\Entity\Sales\SynonymTrait;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * SynCity
 *
 * @ORM\Table(name="syn_city")
 * @ORM\Entity
 */
class SynCity implements SynonymInterface, ServiceFieldInterface
{
    use SynonymTrait;

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=250, nullable=true)
     */
    private $name;

    /**
     * @var int
     *
     * @ORM\Column(name="morionid", type="integer", nullable=true)
     */
    private $morionid;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="currenttime", type="datetime", nullable=true)
     */
    private $currenttime = 'CURRENT_TIMESTAMP';

    /**
     * @var InfoCity
     *
     * @ORM\ManyToOne(targetEntity="TeamSoft\CrmRepositoryBundle\Entity\InfoCity")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="city_id", referencedColumnName="id")
     * })
     */
    private $city;

    /**
     * @var InfoRegion
     *
     * @ORM\ManyToOne(targetEntity="TeamSoft\CrmRepositoryBundle\Entity\InfoRegion")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="region_id", referencedColumnName="id")
     * })
     */
    private $region;

    /**
     * @var PsUploadedfiles
     *
     * @ORM\ManyToMany(targetEntity="TeamSoft\CrmRepositoryBundle\Entity\Sales\PsUploadedfiles")
     * @ORM\JoinTable(
     *     name="ps_syncity_uploadfile",
     *     joinColumns={@ORM\JoinColumn(name="syncity_id", referencedColumnName="id")},
     *     inverseJoinColumns={@ORM\JoinColumn(name="uploadedfile_id", referencedColumnName="id")}
     * )
     */
    private $psUploadedfiles;

    /**
     * @var SynRegion
     *
     * @ORM\ManyToOne(targetEntity="TeamSoft\CrmRepositoryBundle\Entity\Sales\SynRegion")
     * @ORM\JoinColumn(name="synregion_id", referencedColumnName="id")
     */
    private $synregion;

    /**
     * @var float
     *
     * @ORM\Column(name="count", type="float", nullable=true)
     */
    private $count;

    public function __construct()
    {
        $this->psUploadedfiles = new ArrayCollection();
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return SynCity
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set morionid
     *
     * @param int $morionid
     *
     * @return SynCity
     */
    public function setMorionid($morionid)
    {
        $this->morionid = $morionid;

        return $this;
    }

    /**
     * Get morionid
     *
     * @return int
     */
    public function getMorionid()
    {
        return $this->morionid;
    }

    /**
     * Set currenttime
     *
     * @param \DateTime $currenttime
     *
     * @return SynCity
     */
    public function setCurrenttime(\DateTime $currenttime)
    {
        $this->currenttime = $currenttime;

        return $this;
    }

    /**
     * Get currenttime
     *
     * @return \DateTime
     */
    public function getCurrenttime()
    {
        return $this->currenttime;
    }

    /**
     * Set city
     *
     * @param InfoCity $city
     *
     * @return SynCity
     */
    public function setCity(InfoCity $city = null)
    {
        $this->city = $city;

        return $this;
    }

    /**
     * Get city
     *
     * @return InfoCity
     */
    public function getCity()
    {
        return $this->city;
    }

    public function getRegionId()
    {
        return $this->getSynRegion() ? $this->getSynRegion()->getRegionId() : null;
    }

    public function getCityId()
    {
        return $this->getCity() ? $this->getCity()->getId() : null;
    }

    public function getGuid()
    {
        // TODO: Implement getGuid() method.
    }

    public function setGuid($value)
    {
        // TODO: Implement setGuid() method.
    }

    public function getModuser()
    {
        // TODO: Implement getModuser() method.
    }

    public function setModuser($value)
    {
        // TODO: Implement setModuser() method.
    }

    /**
     * @return InfoRegion
     */
    public function getRegion(): InfoRegion
    {
        return $this->region;
    }

    /**
     * @param InfoRegion $region
     */
    public function setRegion(InfoRegion $region)
    {
        $this->region = $region;
    }

    /**
     * Add psUploadedFilesCollection
     *
     * @param PsUploadedfiles $psUploadedFilesCollection
     *
     * @return SynCity
     */
    public function addPsUploadedFilesCollection(PsUploadedfiles $psUploadedFilesCollection)
    {
        $this->psUploadedfiles[] = $psUploadedFilesCollection;

        return $this;
    }

    /**
     * Remove psUploadedFilesCollection
     *
     * @param PsUploadedfiles $psUploadedFilesCollection
     *
     * @return SynCity
     */
    public function removePsUploadedFilesCollection(PsUploadedfiles $psUploadedFilesCollection)
    {
        $this->psUploadedfiles->removeElement($psUploadedFilesCollection);

        return $this;
    }

    /**
     * Get psUploadedFilesCollection
     *
     * @return Collection
     */
    public function getPsUploadedFilesCollection()
    {
        return $this->psUploadedfiles;
    }

    /**
     * Get synRegion
     *
     * @return null|SynRegion
     */
    public function getSynRegion() : ?SynRegion{
        return $this->synregion;
    }

    /**
     * Set synRegion
     *
     * @param null|SynRegion $synRegion
     * @return null|SynCity
     */
    public function setSynRegion(?SynRegion $synRegion) : ?SynCity{
        $this->synregion = $synRegion;

        return $this;
    }

    /**
     * Get public name
     *
     * @return null|string
     */
    public function getPublicName() : ?string{
        $regionName = null;
        if($this->synregion){
            if($this->synregion->getRegion()){
                $regionName = $this->synregion->getRegion()->getName();
            } else {
                $regionName = $this->synregion->getName();
            }

            $regionName = " ({$regionName})";
        }

        return $this->name.$regionName;
    }

    /**
     * Get package count
     *
     * @return float
     */
    public function getCount(): float
    {
        return $this->count;
    }

    /**
     * Set package count
     *
     * @param float $count
     * @return SynCity
     */
    public function setCount(float $count = 0): SynCity
    {
        $this->count = $count;

        return $this;
    }
}
