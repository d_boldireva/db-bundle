<?php

namespace TeamSoft\CrmRepositoryBundle\Entity\Sales;

use Doctrine\ORM\Mapping as ORM;
use TeamSoft\CrmRepositoryBundle\Entity\InfoRegion;
use TeamSoft\CrmRepositoryBundle\Entity\ServiceFieldInterface;
use TeamSoft\CrmRepositoryBundle\Entity\Sales\SynonymInterface;
use TeamSoft\CrmRepositoryBundle\Entity\Sales\SynonymTrait;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * SynRegion
 *
 * @ORM\Table(name="syn_region")
 * @ORM\Entity(repositoryClass="TeamSoft\CrmRepositoryBundle\Repository\SynRegion")
 */
class SynRegion implements SynonymInterface, ServiceFieldInterface
{
    use SynonymTrait;

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=250, nullable=true)
     */
    private $name;

    /**
     * @var int
     *
     * @ORM\Column(name="morionid", type="integer", nullable=true)
     */
    private $morionid;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="currenttime", type="datetime", nullable=true)
     */
    private $currenttime = 'CURRENT_TIMESTAMP';

    /**
     * @var InfoRegion
     *
     * @ORM\ManyToOne(targetEntity="TeamSoft\CrmRepositoryBundle\Entity\InfoRegion")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="region_id", referencedColumnName="id")
     * })
     */
    private $region;

    /**
     * @var PsUploadedfiles
     *
     * @ORM\ManyToMany(targetEntity="TeamSoft\CrmRepositoryBundle\Entity\Sales\PsUploadedfiles")
     * @ORM\JoinTable(
     *     name="ps_synregion_uploadfile",
     *     joinColumns={@ORM\JoinColumn(name="synregion_id", referencedColumnName="id")},
     *     inverseJoinColumns={@ORM\JoinColumn(name="uploadedfile_id", referencedColumnName="id")}
     * )
     */
    private $psUploadedfiles;

    /**
     * @var float
     *
     * @ORM\Column(name="count",type="float",nullable=true)
     */
    private $count;

    public function __construct()
    {
        $this->psUploadedfiles = new ArrayCollection();
    }



    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return SynRegion
     */
    public function setName($name)
    {
        $this->name = $name;
    
        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set morionid
     *
     * @param int $morionid
     *
     * @return SynRegion
     */
    public function setMorionid($morionid)
    {
        $this->morionid = $morionid;
    
        return $this;
    }

    /**
     * Get morionid
     *
     * @return int
     */
    public function getMorionid()
    {
        return $this->morionid;
    }

    /**
     * Set currenttime
     *
     * @param \DateTime $currenttime
     *
     * @return SynRegion
     */
    public function setCurrenttime(\DateTime $currenttime)
    {
        $this->currenttime = $currenttime;
    
        return $this;
    }

    /**
     * Get currenttime
     *
     * @return \DateTime
     */
    public function getCurrenttime()
    {
        return $this->currenttime;
    }

    /**
     * Set region
     *
     * @param InfoRegion $region
     *
     * @return SynRegion
     */
    public function setRegion(InfoRegion $region = null)
    {
        $this->region = $region;
    
        return $this;
    }

    /**
     * Get region
     *
     * @return InfoRegion
     */
    public function getRegion()
    {
        return $this->region;
    }

    public function getRegionId()
    {
        return $this->getRegion() ? $this->getRegion()->getId() : null;
    }

    public function getGuid()
    {
        // TODO: Implement getGuid() method.
    }

    public function setGuid($value)
    {
        // TODO: Implement setGuid() method.
    }

    public function getModuser()
    {
        // TODO: Implement getModuser() method.
    }

    public function setModuser($value)
    {
        // TODO: Implement setModuser() method.
    }

    /**
     * Add psUploadedFilesCollection
     *
     * @param PsUploadedfiles $psUploadedFilesCollection
     *
     * @return SynRegion
     */
    public function addPsUploadedFilesCollection(PsUploadedfiles $psUploadedFilesCollection)
    {
        $this->psUploadedfiles[] = $psUploadedFilesCollection;

        return $this;
    }

    /**
     * Remove psUploadedFilesCollection
     *
     * @param PsUploadedfiles $psUploadedFilesCollection
     *
     * @return SynRegion
     */
    public function removePsUploadedFilesCollection(PsUploadedfiles $psUploadedFilesCollection)
    {
        $this->psUploadedfiles->removeElement($psUploadedFilesCollection);

        return $this;
    }

    /**
     * Get psUploadedFilesCollection
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getPsUploadedFilesCollection()
    {
        return $this->psUploadedfiles;
    }

    /**
     * Get packages count
     *
     * @return float
     */
    public function getCount(): float
    {
        return $this->count;
    }

    /**
     * Set packages count
     *
     * @param float $count
     * @return $this|SynonymInterface
     */
    public function setCount(float $count = 0) : SynRegion
    {
        $this->count = $count;

        return $this;
    }
}
