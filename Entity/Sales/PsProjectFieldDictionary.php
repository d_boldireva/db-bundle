<?php

namespace TeamSoft\CrmRepositoryBundle\Entity\Sales;

use Doctrine\ORM\Mapping as ORM;
use TeamSoft\CrmRepositoryBundle\Entity\InfoCustomdictionary;
use TeamSoft\CrmRepositoryBundle\Entity\InfoSystemdictionary;
use TeamSoft\CrmRepositoryBundle\Entity\ServiceFieldInterface;

/**
 * PsProject
 *
 * @ORM\Table(name="ps_projectfield_dictionary")
 * @ORM\Entity
 */
class PsProjectFieldDictionary implements ServiceFieldInterface
{
    public const STRING_VALUE = 1;
    public const INT_VALUE = 2;
    public const FLOAT_VALUE = 3;
    public const DATE_VALUE = 4;
    public const DICTIONARY_VALUE = 5;
    public const SYSTEM_DICTIONARY_VALUE = 6;

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255, nullable=false)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="code", type="string", length=50, nullable=true)
     */
    private $code;

    /**
     * @var int
     *
     * @ORM\Column(name="type", type="integer", nullable=true)
     */
    private $type = self::STRING_VALUE;

    /**
     * @var int
     *
     * @ORM\Column(name="is_required", type="integer", nullable=true)
     */
    private $isRequired;

    /**
     * @var InfoCustomdictionary
     *
     * @ORM\ManyToOne(targetEntity="TeamSoft\CrmRepositoryBundle\Entity\InfoCustomdictionary")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="CustomDictionary_id", referencedColumnName="id")
     * })
     */
    private $customDictionary;

    /**
     * @var InfoSystemdictionary
     *
     * @ORM\ManyToOne(targetEntity="TeamSoft\CrmRepositoryBundle\Entity\InfoSystemdictionary")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="systemdictionary_id", referencedColumnName="id")
     * })
     */
    private $systemDictionary;

    /**
     * @var string
     *
     * @ORM\Column(name="guid", type="guid", nullable=true)
     */
    private $guid;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="currenttime", type="datetime", nullable=true)
     */
    private $currenttime;

    /**
     * @var string
     *
     * @ORM\Column(name="moduser", type="string", length=16, nullable=true)
     */
    private $moduser;

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string|null $name
     *
     * @return PsProjectFieldDictionary
     */
    public function setName(string $name = null)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set code
     *
     * @param string|null $code
     *
     * @return PsProjectFieldDictionary
     */
    public function setCode(string $code = null)
    {
        $this->code = $code;

        return $this;
    }

    /**
     * Get code
     *
     * @return string
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * Set type
     *
     * @param int|null $type
     *
     * @return PsProjectFieldDictionary
     */
    public function setType(int $type = null)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type
     *
     * @return int
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set guid
     *
     * @param string $guid
     *
     * @return PsProjectFieldDictionary
     */
    public function setGuid($guid)
    {
        $this->guid = $guid;

        return $this;
    }

    /**
     * Get guid
     *
     * @return string
     */
    public function getGuid()
    {
        return $this->guid;
    }

    /**
     * Set currenttime
     *
     * @param \DateTime $currenttime
     *
     * @return PsProject
     */
    public function setCurrenttime(\DateTime $currenttime)
    {
        $this->currenttime = $currenttime;

        return $this;
    }

    /**
     * Get currenttime
     *
     * @return \DateTime
     */
    public function getCurrenttime()
    {
        return $this->currenttime;
    }

    /**
     * Set moduser
     *
     * @param string $moduser
     *
     * @return PsProjectFieldDictionary
     */
    public function setModuser($moduser)
    {
        $this->moduser = $moduser;

        return $this;
    }

    /**
     * Set isRequired
     *
     * @param int|null $isRequired
     *
     * @return PsProjectFieldDictionary
     */
    public function setIsRequired(int $isRequired = null)
    {
        $this->isRequired = $isRequired;

        return $this;
    }

    /**
     * Get isRequired
     *
     * @return int
     */
    public function getIsRequired()
    {
        return $this->isRequired;
    }

    /**
     * Get moduser
     *
     * @return string
     */
    public function getModuser()
    {
        return $this->moduser;
    }

    /**
     * Set customDictionary
     *
     * @param InfoCustomdictionary|null $customDictionary
     *
     * @return PsProjectFieldDictionary
     */
    public function setCustomDictionary(InfoCustomdictionary $customDictionary = null)
    {
        $this->customDictionary = $customDictionary;

        return $this;
    }

    /**
     * Get customDictionary
     *
     * @return InfoCustomdictionary
     */
    public function getCustomDictionary()
    {
        return $this->customDictionary;
    }

    /**
     * Set systemDictionary
     *
     * @param InfoSystemdictionary|null $systemDictionary
     *
     * @return PsProjectFieldDictionary
     */
    public function setSystemDictionary(InfoSystemdictionary $systemDictionary = null)
    {
        $this->systemDictionary = $systemDictionary;

        return $this;
    }

    /**
     * Get systemDictionary
     *
     * @return InfoSystemdictionary
     */
    public function getSystemDictionary()
    {
        return $this->systemDictionary;
    }
}
