<?php

namespace TeamSoft\CrmRepositoryBundle\Entity\Sales;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use TeamSoft\CrmRepositoryBundle\Entity\ServiceFieldInterface;
use TeamSoft\CrmRepositoryBundle\Entity\Sales\PsPattern;
use TeamSoft\CrmRepositoryBundle\Entity\Sales\SynOrg;

/**
 * PsUploadedfiles
 *
 * @ORM\Table(name="ps_uploadedfiles")
 * @ORM\Entity(repositoryClass="TeamSoft\CrmRepositoryBundle\Repository\PsUploadedFiles")
 */
class PsUploadedfiles implements ServiceFieldInterface
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255, nullable=true)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="original", type="string", length=255, nullable=true)
     */
    private $original;

    /**
     * @var string
     *
     * @ORM\Column(name="report_name", type="string", length=255, nullable=true)
     */
    private $reportName;

    /**
     * @var int
     *
     * @ORM\Column(name="packages", type="integer", nullable=true)
     */
    private $packages = 0;

    /**
     * @var int
     *
     * @ORM\Column(name="packages_count", type="integer", nullable=true)
     */
    private $packagesCount = 0;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="saledate", type="datetime", nullable=true)
     */
    private $saledate;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="createdate", type="datetime", nullable=true)
     */
    private $createdate;

    /**
     * @var int
     *
     * @ORM\Column(name="isimported", type="integer", nullable=true)
     */
    private $isimported;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="currenttime", type="datetime", nullable=true)
     */
    private $currenttime;

    /**
     * @var string
     *
     * @ORM\Column(name="moduser", type="string", length=16, nullable=true)
     */
    private $moduser;

    /**
     * @var string
     *
     * @ORM\Column(name="guid", type="guid", nullable=true)
     */
    private $guid;

    /**
     * @var PsPattern
     *
     * @ORM\ManyToOne(targetEntity="PsPattern")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="pattern_id", referencedColumnName="id")
     * })
     */
    private $pattern;

    /**
     * One type has Many Patterns
     * @ORM\ManyToOne(targetEntity="PsPatternType")
     * @ORM\JoinColumn(name="type_id", referencedColumnName="id")
     */
    private $type;

    /**
     * @var string
     *
     * @ORM\Column(name="table_name", type="string", nullable=true)
     */
    private $tableName;

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return PsUploadedfiles
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set original
     *
     * @param string $original
     *
     * @return PsUploadedfiles
     */
    public function setOriginal($original)
    {
        $this->original = $original;

        return $this;
    }

    /**
     * Get original
     *
     * @return string
     */
    public function getOriginal()
    {
        return $this->original;
    }

    /**
     * Set saledate
     *
     * @param \DateTime $datefrom
     *
     * @return PsUploadedfiles
     */
    public function setSaledate($saledate)
    {
        $this->saledate = $saledate;

        return $this;
    }

    /**
     * Get saledate
     *
     * @return \DateTime
     */
    public function getSaledate()
    {
        return $this->saledate;
    }

    /**
     * Set createdate
     *
     * @param \DateTime $createdate
     *
     * @return PsUploadedfiles
     */
    public function setCreatedate($createdate)
    {
        $this->createdate = $createdate;

        return $this;
    }

    /**
     * Get createdate
     *
     * @return \DateTime
     */
    public function getCreatedate()
    {
        return $this->createdate;
    }

    /**
     * Set isimported
     *
     * @param int $isimported
     *
     * @return PsUploadedfiles
     */
    public function setIsimported($isimported)
    {
        $this->isimported = $isimported;

        return $this;
    }

    /**
     * Get isimported
     *
     * @return int
     */
    public function getIsimported()
    {
        return $this->isimported;
    }

    /**
     * Set currenttime
     *
     * @param \DateTime $currenttime
     *
     * @return PsUploadedfiles
     */
    public function setCurrenttime(\DateTime $currenttime)
    {
        $this->currenttime = $currenttime;

        return $this;
    }

    /**
     * Get currenttime
     *
     * @return \DateTime
     */
    public function getCurrenttime()
    {
        return $this->currenttime;
    }

    /**
     * Set moduser
     *
     * @param string $moduser
     *
     * @return PsUploadedfiles
     */
    public function setModuser($moduser)
    {
        $this->moduser = $moduser;

        return $this;
    }

    /**
     * Get moduser
     *
     * @return string
     */
    public function getModuser()
    {
        return $this->moduser;
    }

    /**
     * Set guid
     *
     * @param string $guid
     *
     * @return PsUploadedfiles
     */
    public function setGuid($guid)
    {
        $this->guid = $guid;

        return $this;
    }

    /**
     * Get guid
     *
     * @return string
     */
    public function getGuid()
    {
        return $this->guid;
    }

    /**
     * Set pattern
     *
     * @param PsPattern $pattern
     *
     * @return PsUploadedfiles
     */
    public function setPattern(PsPattern $pattern = null)
    {
        $this->pattern = $pattern;

        return $this;
    }

    /**
     * Get pattern
     *
     * @return PsPattern
     */
    public function getPattern()
    {
        return $this->pattern;
    }

    /**
     * @return string
     */
    public function getReportName(): ?string
    {
        return $this->reportName;
    }

    /**
     * @param string $reportName
     */
    public function setReportName(string $reportName)
    {
        $this->reportName = $reportName;
    }

    /**
     * @return int
     */
    public function getPackages(): int
    {
        return $this->packages;
    }

    /**
     * @param int $packages
     * @return PsUploadedfiles
     */
    public function setPackages(?int $packages)
    {
        $this->packages = $packages ?: 0;

        return $this;
    }

    /**
     * @return int
     */
    public function getPackagesCount(): int
    {
        return $this->packagesCount;
    }

    /**
     * @param int $packagesCount
     * @return PsUploadedfiles
     */
    public function setPackagesCount(?int $packagesCount)
    {
        $this->packagesCount = $packagesCount ?: 0;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param mixed $type
     * @return PsUploadedfiles
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    public function getNiceName(){
        return $this->getOriginal() ?? $this->getReportName() ?? $this->getName();
    }

    /**
     * @param string $tableName
     * @return PsUploadedfiles
     */
    public function setTableName(?string $tableName){
        $this->tableName = $tableName;

        return $this;
    }

    /**
     * @return string
     */
    public function getTableName(){
        return $this->tableName;
    }
}
