<?php

namespace TeamSoft\CrmRepositoryBundle\Entity\Sales;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use TeamSoft\CrmRepositoryBundle\Entity\InfoUser;
use TeamSoft\CrmRepositoryBundle\Entity\ServiceFieldInterface;
use TeamSoft\CrmRepositoryBundle\Entity\Sales\PsPattern;
use TeamSoft\CrmRepositoryBundle\Entity\Sales\SynOrg;

/**
 * PsDeletedfiles
 *
 * @ORM\Table(name="ps_deletedfiles")
 * @ORM\Entity()
 */
class PsDeletedfiles implements ServiceFieldInterface
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255, nullable=true)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="original", type="string", length=255, nullable=true)
     */
    private $original;

    /**
     * @var string
     *
     * @ORM\Column(name="table_name", type="string", length=255, nullable=true)
     */
    private $tableName;

    /**
     * @var int
     *
     * @ORM\Column(name="isimported", type="integer", nullable=true)
     */
    private $isimported;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="currenttime", type="datetime", nullable=true)
     */
    private $currenttime;

    /**
     * @var string
     *
     * @ORM\Column(name="moduser", type="string", length=16, nullable=true)
     */
    private $moduser;

    /**
     * @var string
     *
     * @ORM\Column(name="guid", type="guid", nullable=true)
     */
    private $guid;

    /**
     * @var PsPattern
     *
     * @ORM\ManyToOne(targetEntity="PsPattern")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="pattern_id", referencedColumnName="id")
     * })
     */
    private $pattern;

    /**
     * @var InfoUser
     *
     * @ORM\ManyToOne(targetEntity="\TeamSoft\CrmRepositoryBundle\Entity\InfoUser")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     * })
     */
    private $user;

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return PsDeletedfiles
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set original
     *
     * @param string $original
     *
     * @return PsDeletedfiles
     */
    public function setOriginal(string $original)
    {
        $this->original = $original;

        return $this;
    }

    /**
     * Get original
     *
     * @return string
     */
    public function getOriginal()
    {
        return $this->original;
    }

    /**
     * Set createdate
     *
     * @param \DateTime $createdate
     *
     * @return PsDeletedfiles
     */
    public function setCreatedate($createdate)
    {
        $this->createdate = $createdate;

        return $this;
    }

    /**
     * Get createdate
     *
     * @return \DateTime
     */
    public function getCreatedate()
    {
        return $this->createdate;
    }

    /**
     * Set isimported
     *
     * @param int $isimported
     *
     * @return PsDeletedfiles
     */
    public function setIsimported($isimported)
    {
        $this->isimported = $isimported;

        return $this;
    }

    /**
     * Get isimported
     *
     * @return int
     */
    public function getIsimported()
    {
        return $this->isimported;
    }

    /**
     * Set currenttime
     *
     * @param \DateTime $currenttime
     *
     * @return PsDeletedfiles
     */
    public function setCurrenttime(\DateTime $currenttime)
    {
        $this->currenttime = $currenttime;

        return $this;
    }

    /**
     * Get currenttime
     *
     * @return \DateTime
     */
    public function getCurrenttime()
    {
        return $this->currenttime;
    }

    /**
     * Set moduser
     *
     * @param string $moduser
     *
     * @return PsDeletedfiles
     */
    public function setModuser($moduser)
    {
        $this->moduser = $moduser;

        return $this;
    }

    /**
     * Get moduser
     *
     * @return string
     */
    public function getModuser()
    {
        return $this->moduser;
    }

    /**
     * Set guid
     *
     * @param string $guid
     *
     * @return PsDeletedfiles
     */
    public function setGuid($guid)
    {
        $this->guid = $guid;

        return $this;
    }

    /**
     * Get guid
     *
     * @return string
     */
    public function getGuid()
    {
        return $this->guid;
    }

    /**
     * Set pattern
     *
     * @param PsPattern $pattern
     *
     * @return PsDeletedfiles
     */
    public function setPattern(PsPattern $pattern = null)
    {
        $this->pattern = $pattern;

        return $this;
    }

    /**
     * Get pattern
     *
     * @return PsPattern
     */
    public function getPattern()
    {
        return $this->pattern;
    }

    /**
     * @return string
     */
    public function getTableName(): ?string
    {
        return $this->tableName;
    }

    /**
     * @param string $tableName
     * @return PsDeletedfiles
     */
    public function setTableName(?string $tableName)
    {
        $this->tableName = $tableName;

        return $this;
    }

    /**
     * @return InfoUser
     */
    public function getUser(): InfoUser
    {
        return $this->user;
    }

    /**
     * @param InfoUser $user
     * @return PsDeletedfiles
     */
    public function setUser($user)
    {
        $this->user = $user;

        return $this;
    }
}
