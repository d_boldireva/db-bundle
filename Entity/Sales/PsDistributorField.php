<?php

namespace TeamSoft\CrmRepositoryBundle\Entity\Sales;

use Doctrine\ORM\Mapping as ORM;
use TeamSoft\CrmRepositoryBundle\Entity\ServiceFieldInterface;

/**
 * PsDistributorField
 *
 * @ORM\Table(name="ps_distributor_field")
 * @ORM\Entity
 */
class PsDistributorField implements ServiceFieldInterface
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255, nullable=false)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="field_name", type="string", length=255, nullable=true, unique=true)
     */
    private $fieldName;

    /**
     * @var integer
     *
     * @ORM\Column(name="is_required", type="integer", nullable=true)
     */
    private $isRequired;

    /**
     * @var string
     *
     * @ORM\Column(name="guid", type="guid", nullable=true)
     */
    private $guid = 'newid()';

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="currenttime", type="datetime", nullable=true)
     */
    private $currenttime = 'CURRENT_TIMESTAMP';

    /**
     * @var string
     *
     * @ORM\Column(name="moduser", type="string", length=16, nullable=true)
     */
    private $moduser = '[dbo].[Get_CurrentCode]()';

    /**
     * One type has Many Patterns
     * @ORM\ManyToOne(targetEntity="PsPatternType")
     * @ORM\JoinColumn(name="type_id", referencedColumnName="id")
     */
    private $type;

    /**
     * @var int
     *
     * @ORM\Column(name="required_group", type="integer", nullable=true)
     */
    private $requiredGroup;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return PsDistributorField
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set fieldName
     *
     * @param string $fieldName
     *
     * @return PsDistributorField
     */
    public function setFieldName($fieldName)
    {
        $this->fieldName = $fieldName;

        return $this;
    }

    /**
     * Get fieldName
     *
     * @return string
     */
    public function getFieldName()
    {
        return $this->fieldName;
    }

    /**
     * Set isRequired
     *
     * @param $isRequired $isRequired
     *
     * @return PsDistributorField
     */
    public function setIsRequired($isRequired)
    {
        $this->isRequired = $isRequired;

        return $this;
    }

    /**
     * Get isRequired
     *
     * @return integer
     */
    public function getIsRequired()
    {
        return $this->isRequired;
    }

    /**
     * Set guid
     *
     * @param string $guid
     *
     * @return PsDistributorField
     */
    public function setGuid($guid)
    {
        $this->guid = $guid;

        return $this;
    }

    /**
     * Get guid
     *
     * @return string
     */
    public function getGuid()
    {
        return $this->guid;
    }

    /**
     * Set currenttime
     *
     * @param \DateTime $currenttime
     *
     * @return PsDistributorField
     */
    public function setCurrenttime(\DateTime $currenttime)
    {
        $this->currenttime = $currenttime;

        return $this;
    }

    /**
     * Get currenttime
     *
     * @return \DateTime
     */
    public function getCurrenttime()
    {
        return $this->currenttime;
    }

    /**
     * Set moduser
     *
     * @param string $moduser
     *
     * @return PsDistributorField
     */
    public function setModuser($moduser)
    {
        $this->moduser = $moduser;

        return $this;
    }

    /**
     * Get moduser
     *
     * @return string
     */
    public function getModuser()
    {
        return $this->moduser;
    }

    /**
     * @return PsPatternType|null
     */
    public function getType(): ?PsPatternType
    {
        return $this->type;
    }

    /**
     * @param PsPatternType|null $type
     */
    public function setType(?PsPatternType $type)
    {
        $this->type = $type;
    }

    /**
     * Set required group
     *
     * @param $requiredGroup
     * @return $this
     */
    public function setRequiredGroup($requiredGroup)
    {
        $this->requiredGroup = $requiredGroup;
        return $this;
    }

    /**
     * @return int
     */
    public function getRequiredGroup()
    {
        return $this->requiredGroup;
    }
}
