<?php

namespace TeamSoft\CrmRepositoryBundle\Entity\Sales;

use Doctrine\ORM\Mapping as ORM;

/**
 * PsFilenamePattern
 *
 * @ORM\Table(name="ps_filename_pattern")
 * @ORM\Entity
 */
class PsFilenamePattern
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="filename", type="string", length=255, nullable=false)
     */
    private $filename;

    /**
     * @var PsPattern
     *
     * @ORM\ManyToOne(targetEntity="PsPattern")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="pattern_id", referencedColumnName="id")
     * })
     */
    private $pattern;



    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set filename
     *
     * @param string $filename
     *
     * @return PsFilenamePattern
     */
    public function setFilename($filename)
    {
        $this->filename = $filename;
    
        return $this;
    }

    /**
     * Get filename
     *
     * @return string
     */
    public function getFilename()
    {
        return $this->filename;
    }

    /**
     * Set pattern
     *
     * @param PsPattern $pattern
     *
     * @return PsFilenamePattern
     */
    public function setPattern(PsPattern $pattern = null)
    {
        $this->pattern = $pattern;
    
        return $this;
    }

    /**
     * Get pattern
     *
     * @return PsPattern
     */
    public function getPattern()
    {
        return $this->pattern;
    }
}
