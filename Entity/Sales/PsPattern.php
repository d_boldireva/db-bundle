<?php

namespace TeamSoft\CrmRepositoryBundle\Entity\Sales;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use TeamSoft\CrmRepositoryBundle\Entity\InfoDistributor;
use TeamSoft\CrmRepositoryBundle\Entity\Sales\PsProject;
use TeamSoft\CrmRepositoryBundle\Entity\ServiceFieldInterface;

/**
 * PsPattern
 *
 * @ORM\Table(name="ps_pattern")
 * @ORM\Entity
 */
class PsPattern implements ServiceFieldInterface
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255, nullable=false)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="filename", type="string", length=255, nullable=false)
     */
    private $filename;

    /**
     * @var string
     *
     * @ORM\Column(name="sheet", type="string", length=255, nullable=true)
     */
    private $sheet;

    /**
     * @var string
     *
     * @ORM\Column(name="guid", type="guid", nullable=true)
     */
    private $guid;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="currenttime", type="datetime", nullable=true)
     */
    private $currenttime;

    /**
     * @var string
     *
     * @ORM\Column(name="moduser", type="string", length=16, nullable=true)
     */
    private $moduser;

    /**
     * One distributor has Many Patterns
     * @ORM\ManyToOne(targetEntity="\TeamSoft\CrmRepositoryBundle\Entity\InfoDistributor")
     * @ORM\JoinColumn(name="distributor_id", referencedColumnName="id")
     */
    private $distributor;

    /**
     * One project has Many Patterns
     * @ORM\ManyToOne(targetEntity="\TeamSoft\CrmRepositoryBundle\Entity\Sales\PsProject")
     * @ORM\JoinColumn(name="project_id", referencedColumnName="id")
     */
    private $project;

    /**
     * One type has Many Patterns
     * @ORM\ManyToOne(targetEntity="TeamSoft\CrmRepositoryBundle\Entity\Sales\PsPatternType")
     * @ORM\JoinColumn(name="type_id", referencedColumnName="id")
     */
    private $type;

    /**
     * One Pattern has Many Fields.
     * @ORM\OneToMany(targetEntity="PsPatternField", mappedBy="pattern", cascade={"persist"})
     */
    private $fieldsCollection;

    /**
     * One Pattern has Many Skipped Rows.
     * @ORM\OneToMany(targetEntity="PsPatternSkippedRows", mappedBy="pattern", orphanRemoval=true, cascade={"persist"})
     */
    private $skippedRowsCollection;

    /**
     * One Pattern has Many Uploadedfiles.
     * @ORM\OneToMany(targetEntity="PsUploadedfiles", mappedBy="pattern", cascade={"persist"})
     */
    private $uploadedfilesCollection;


    /**
     * PsPattern constructor.
     */
    public function __construct()
    {
        $this->fieldsCollection = new ArrayCollection();
        $this->uploadedfilesCollection = new ArrayCollection();
        $this->skippedRowsCollection = new ArrayCollection();
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return PsPattern
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set sheet
     *
     * @param string $sheet
     *
     * @return PsPattern
     */
    public function setSheet($sheet)
    {
        $this->sheet = $sheet;

        return $this;
    }

    /**
     * Get sheet
     *
     * @return string
     */
    public function getSheet()
    {
        return $this->sheet;
    }

    /**
     * Set guid
     *
     * @param string $guid
     *
     * @return PsPattern
     */
    public function setGuid($guid)
    {
        $this->guid = $guid;

        return $this;
    }

    /**
     * Get guid
     *
     * @return string
     */
    public function getGuid()
    {
        return $this->guid;
    }

    /**
     * Set currenttime
     *
     * @param \DateTime $currenttime
     *
     * @return PsPattern
     */
    public function setCurrenttime(\DateTime $currenttime)
    {
        $this->currenttime = $currenttime;

        return $this;
    }

    /**
     * Get currenttime
     *
     * @return \DateTime
     */
    public function getCurrenttime()
    {
        return $this->currenttime;
    }

    /**
     * Set moduser
     *
     * @param string $moduser
     *
     * @return PsPattern
     */
    public function setModuser($moduser)
    {
        $this->moduser = $moduser;

        return $this;
    }

    /**
     * Get moduser
     *
     * @return string
     */
    public function getModuser()
    {
        return $this->moduser;
    }

    /**
     * Set distributor
     *
     * @param InfoDistributor $distributor
     *
     * @return PsPattern
     */
    public function setDistributor(InfoDistributor $distributor = null)
    {
        $this->distributor = $distributor;

        return $this;
    }

    /**
     * Get distributor
     *
     * @return InfoDistributor
     */
    public function getDistributor()
    {
        return $this->distributor;
    }

    /**
     * Set project
     *
     * @param PsProject $project
     *
     * @return PsPattern
     */
    public function setProject(PsProject $project = null)
    {
        $this->project = $project;

        return $this;
    }

    /**
     * Get project
     *
     * @return PsProject
     */
    public function getProject()
    {
        return $this->project;
    }

    /**
     * Add field
     *
     * @param PsPatternField $field
     *
     * @return PsPattern
     */
    public function addFieldsCollection(PsPatternField $field)
    {
        $field->setPattern($this);
        $this->fieldsCollection->add($field);

        return $this;
    }

    /**
     * Remove field
     *
     * @param PsPatternField $field
     */
    public function removeFieldsCollection(PsPatternField $field)
    {
        $this->fieldsCollection->removeElement($field);
    }

    /**
     * Get fields
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getFieldsCollection()
    {
        return $this->fieldsCollection;
    }

    /**
     * Add uploadedfilesCollection
     *
     * @param PsUploadedfiles
     *
     * @return PsPattern
     */
    public function addUploadedfilesCollection(PsUploadedfiles $uploadedfiles)
    {
        $uploadedfiles->setPattern($this);
        $this->uploadedfilesCollection->add($uploadedfiles);

        return $this;
    }

    /**
     * Remove uploadedfilesCollection
     *
     * @param PsUploadedfiles
     */
    public function removeUploadedfilesCollection(PsUploadedfiles $uploadedfiles)
    {
        $this->uploadedfilesCollection->removeElement($uploadedfiles);
    }

    /**
     * Get uploadedfilesCollection
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getUploadedfilesCollection()
    {
        return $this->uploadedfilesCollection;
    }

    /**
     * Set filename
     *
     * @param string $filename
     *
     * @return PsPattern
     */
    public function setFilename($filename)
    {
        $this->filename = $filename;

        return $this;
    }

    /**
     * Get filename
     *
     * @return string
     */
    public function getFilename()
    {
        return $this->filename;
    }

    /**
     * @return mixed
     */
    public function getSkippedRowsCollection()
    {
        return $this->skippedRowsCollection;
    }

    /**
     * Add field
     *
     * @param PsPatternSkippedRows $row
     *
     * @return PsPattern
     */
    public function addSkippedRowsCollection(PsPatternSkippedRows $row)
    {
        $row->setPattern($this);
        $this->skippedRowsCollection->add($row);

        return $this;
    }

    /**
     * Remove field
     *
     * @param PsPatternSkippedRows $row
     */
    public function removeSkippedRowsCollection(PsPatternSkippedRows $row)
    {
        $row->setPattern(null);
        $this->skippedRowsCollection->removeElement($row);
    }

    /**
     * @return PsPatternType
     */
    public function getType(): ?PsPatternType
    {
        return $this->type;
    }

    /**
     * @param PsPatternType $type
     * @return PsPattern
     */
    public function setType(PsPatternType $type)
    {
        $this->type = $type;

        return $this;
    }
}
