<?php

namespace TeamSoft\CrmRepositoryBundle\Entity\Sales;

use Doctrine\ORM\Mapping as ORM;

/**
 * PsPatternType
 *
 * @ORM\Table(name="ps_pattern_types")
 * @ORM\Entity
 */
class PsPatternType
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255, nullable=false)
     */
    private $name;

    /**
     * @var int
     *
     * @ORM\Column(name="hidden", type="integer", nullable=true)
     */
    private $hidden;

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return PsPatternType
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set hidden
     *
     * @param int $hidden
     * @return $this
     */
    public function setHidden($hidden = 0)
    {
        $this->hidden = $hidden;
        return $this;
    }

    /**
     * Get hidden
     *
     * @return int
     */
    public function getHidden()
    {
        return $this->hidden;
    }

    /**
     * Is hidden
     *
     * @return bool
     */
    public function IsHidden()
    {
        return !!$this->hidden;
    }
}
