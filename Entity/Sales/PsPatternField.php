<?php

namespace TeamSoft\CrmRepositoryBundle\Entity\Sales;

use Doctrine\ORM\Mapping as ORM;
use TeamSoft\CrmRepositoryBundle\Entity\ServiceFieldInterface;
use TeamSoft\CrmRepositoryBundle\Entity\Sales\PsDistributorField;

/**
 * PsPatternField
 *
 * @ORM\Table(name="ps_pattern_field")
 * @ORM\Entity
 */
class PsPatternField implements ServiceFieldInterface
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="`column`", type="string", length=255, nullable=false)
     */
    private $column;

    /**
     * @var string
     *
     * @ORM\Column(name="guid", type="guid", nullable=true)
     */
    private $guid;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="currenttime", type="datetime", nullable=true)
     */
    private $currenttime;

    /**
     * @var string
     *
     * @ORM\Column(name="moduser", type="string", length=16, nullable=true)
     */
    private $moduser;

    /**
     * @var PsPattern
     *
     * @ORM\ManyToOne(targetEntity="PsPattern", cascade={"persist", "remove"})
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="pattern_id", referencedColumnName="id")
     * })
     */
    private $pattern;

    /**
     * @var PsDistributorField
     *
     * @ORM\ManyToOne(targetEntity="PsDistributorField")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="distributor_field_id", referencedColumnName="id")
     * })
     */
    private $distributorField;

    /**
     * @var PsProjectFieldDictionary
     *
     * @ORM\ManyToOne(targetEntity="PsProjectFieldDictionary")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="project_field_id", referencedColumnName="id")
     * })
     */
    private $projectField;

    /**
     * @var string
     *
     * @ORM\Column(name="reg_template",type="string", nullable=true, length=1000)
     */
    private $regTemplate;

    /**
     * @var integer
     *
     * @ORM\Column(name="required", type="integer", nullable=true)
     */
    private $required;

    /**
     * @var integer
     *
     * @ORM\Column(name="position", type="integer", nullable=false)
     */
    private $position;

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set column
     *
     * @param string $column
     *
     * @return PsPatternField
     */
    public function setColumn($column)
    {
        $this->column = $column;

        return $this;
    }

    /**
     * Get column
     *
     * @return string
     */
    public function getColumn()
    {
        return $this->column;
    }

    /**
     * Set guid
     *
     * @param string $guid
     *
     * @return PsPatternField
     */
    public function setGuid($guid)
    {
        $this->guid = $guid;

        return $this;
    }

    /**
     * Get guid
     *
     * @return string
     */
    public function getGuid()
    {
        return $this->guid;
    }

    /**
     * Set currenttime
     *
     * @param \DateTime $currenttime
     *
     * @return PsPatternField
     */
    public function setCurrenttime(\DateTime $currenttime)
    {
        $this->currenttime = $currenttime;

        return $this;
    }

    /**
     * Get currenttime
     *
     * @return \DateTime
     */
    public function getCurrenttime()
    {
        return $this->currenttime;
    }

    /**
     * Set moduser
     *
     * @param string $moduser
     *
     * @return PsPatternField
     */
    public function setModuser($moduser)
    {
        $this->moduser = $moduser;

        return $this;
    }

    /**
     * Get moduser
     *
     * @return string
     */
    public function getModuser()
    {
        return $this->moduser;
    }

    /**
     * Set pattern
     *
     * @param PsPattern $pattern
     *
     * @return PsPatternField
     */
    public function setPattern(PsPattern $pattern = null)
    {
        $this->pattern = $pattern;

        return $this;
    }

    /**
     * Get pattern
     *
     * @return PsPattern
     */
    public function getPattern()
    {
        return $this->pattern;
    }

    /**
     * Set distributorField
     *
     * @param PsDistributorField $distributorField
     *
     * @return PsPatternField
     */
    public function setDistributorField(PsDistributorField $distributorField = null)
    {
        $this->distributorField = $distributorField;

        return $this;
    }

    /**
     * Get distributorField
     *
     * @return PsDistributorField
     */
    public function getDistributorField()
    {
        return $this->distributorField;
    }

    /**
     * Get pattern regex template
     *
     * @return null|string
     */
    public function getRegTemplate(): ?string
    {
        return $this->regTemplate;
    }

    /**
     * Set pattern regex template
     *
     * @param null|string $regTemplate
     * @return PsPatternField
     */
    public function setRegTemplate(?string $regTemplate): PsPatternField
    {
        $this->regTemplate = $regTemplate;

        return $this;
    }

    /**
     * Is field required
     *
     * @return int
     */
    public function isRequired(): int
    {
        return intval($this->required ?: 0);
    }

    /**
     * Set field required
     *
     * @param bool $required
     * @return PsPatternField
     */
    public function setRequired($required): self
    {
        $this->required = $required ? 1 : 0;

        return $this;
    }

    public function getPosition(): ?int
    {
        return $this->position;
    }

    /**
     * @param int $position
     * @return self
     */
    public function setPosition(int $position): self
    {
        $this->position = $position;

        return $this;
    }

    /**
     * @return PsProjectFieldDictionary
     */
    public function getProjectField(): ?PsProjectFieldDictionary
    {
        return $this->projectField;
    }

    /**
     * @param PsProjectFieldDictionary $projectField
     */
    public function setProjectField(?PsProjectFieldDictionary $projectField): void
    {
        $this->projectField = $projectField;
    }
}
