<?php

namespace TeamSoft\CrmRepositoryBundle\Entity\Sales;

interface SynonymInterface
{
    /**
     * Set ignore
     *
     * @return boolean
     */
    public function isSkip();

    /**
     * Get ignore
     *
     * @return boolean
     */
    public function isIgnore();

    /**
     * Set skip
     *
     * @param boolean $skip
     *
     * @return $this
     */
    public function setSkip($skip);

    /**
     * Get skip
     *
     * @return boolean
     */
    public function getSkip();

    /**
     * @return float|null
     */
    public function getCount() : ?float;

    /**
     * @param float $count
     * @return SynonymInterface
     */
    public function setCount(float $count = 0);
}
