<?php

namespace TeamSoft\CrmRepositoryBundle\Entity\Sales;

use Doctrine\ORM\Mapping as ORM;

trait SynonymTrait
{
    /**
     * @var \DateTime
     *
     * @ORM\Column(name="skipdate", type="datetime", nullable=true)
     */
    protected $skipdate;

    /**
     * @var int
     *
     * @ORM\Column(name="skip", type="integer", nullable=true)
     */
    protected $skip;

    /**
     * @var int
     *
     * @ORM\Column(name="ignore", type="integer", nullable=true)
     */
    protected $ignore;

    /**
     * Set skip
     *
     * @param boolean $skip
     * @return $this
     */
    public function setSkip($skip)
    {
        $this->skip = $skip;

        return $this;
    }

    /**
     * Get skip
     *
     * @return boolean
     */
    public function getSkip()
    {
        return $this->skip;
    }

    /**
     * @param ?int $ignore
     * @return $this
     */
    public function setIgnore($ignore){
        $this->ignore = $ignore;

        return $this;
    }

    /**
     * @return int
     */
    public function getIgnore(){
        return $this->ignore;
    }

    /**
     * @return \DateTime
     */
    public function getSkipdate(): ?\DateTime
    {
        return $this->skipdate;
    }

    /**
     * @param \DateTime $skipdate
     */
    public function setSkipdate(?\DateTime $skipdate)
    {
        $this->skipdate = $skipdate;
    }

    /**
     * @return bool
     * @throws \Exception
     */
    public function isSkip()
    {
        $now = new \DateTime();

        return $this->getSkip() && $this->getSkipdate() && ($this->getSkipdate() > $now->format('Y-m-d H:i:s'));
    }

    /**
     * @return bool
     */
    public function isIgnore()
    {
        return $this->getIgnore() == true;
    }
}
