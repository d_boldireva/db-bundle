<?php

namespace TeamSoft\CrmRepositoryBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use TeamSoft\CrmRepositoryBundle\Entity\ServiceFieldInterface;

/**
 * InfoTaskpresentation
 *
 * @ORM\Table(name="info_taskpresentation")
 * @ORM\Entity(repositoryClass="TeamSoft\CrmRepositoryBundle\Repository\InfoTaskpresentation")
 */
class InfoTaskpresentation implements ServiceFieldInterface
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var guid
     *
     * @ORM\Column(name="guid", type="guid", nullable=true)
     */
    private $guid;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="currenttime", type="datetime", nullable=true)
     */
    private $currenttime;

    /**
     * @var string
     *
     * @ORM\Column(name="moduser", type="string", length=16, nullable=true)
     */
    private $moduser;

    /**
     * @var \TeamSoft\CrmRepositoryBundle\Entity\InfoTask
     *
     * @ORM\ManyToOne(targetEntity="\TeamSoft\CrmRepositoryBundle\Entity\InfoTask", cascade={"persist", "remove"})
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="task_id", referencedColumnName="id")
     * })
     */
    private $task;

    /**
     * @var \TeamSoft\CrmRepositoryBundle\Entity\InfoClm
     *
     * @ORM\ManyToOne(targetEntity="InfoClm", inversedBy="taskPresentations", cascade={"persist", "remove"})
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="presentation_id", referencedColumnName="id")
     * })
     */
    private $presentation;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set guid
     *
     * @param guid $guid
     *
     * @return InfoTaskpresentation
     */
    public function setGuid($guid)
    {
        $this->guid = $guid;

        return $this;
    }

    /**
     * Get guid
     *
     * @return guid
     */
    public function getGuid()
    {
        return $this->guid;
    }

    /**
     * Set currenttime
     *
     * @param \DateTime $currenttime
     *
     * @return InfoTaskpresentation
     */
    public function setCurrenttime(\DateTime $currenttime)
    {
        $this->currenttime = $currenttime;

        return $this;
    }

    /**
     * Get currenttime
     *
     * @return \DateTime
     */
    public function getCurrenttime()
    {
        return $this->currenttime;
    }

    /**
     * Set moduser
     *
     * @param string $moduser
     *
     * @return InfoTaskpresentation
     */
    public function setModuser($moduser)
    {
        $this->moduser = $moduser;

        return $this;
    }

    /**
     * Get moduser
     *
     * @return string
     */
    public function getModuser()
    {
        return $this->moduser;
    }

    /**
     * Set task
     *
     * @param \TeamSoft\CrmRepositoryBundle\Entity\InfoTask $task
     *
     * @return InfoTaskpresentation
     */
    public function setTask(\TeamSoft\CrmRepositoryBundle\Entity\InfoTask $task = null)
    {
        $this->task = $task;

        return $this;
    }

    /**
     * Get task
     *
     * @return \TeamSoft\CrmRepositoryBundle\Entity\InfoTask
     */
    public function getTask()
    {
        return $this->task;
    }

    /**
     * Set presentation
     *
     * @param \TeamSoft\CrmRepositoryBundle\Entity\InfoClm $presentation
     *
     * @return InfoTaskpresentation
     */
    public function setPresentation(\TeamSoft\CrmRepositoryBundle\Entity\InfoClm $presentation = null)
    {
        $this->presentation = $presentation;

        return $this;
    }

    /**
     * Get presentation
     *
     * @return \TeamSoft\CrmRepositoryBundle\Entity\InfoClm
     */
    public function getPresentation()
    {
        return $this->presentation;
    }
}

