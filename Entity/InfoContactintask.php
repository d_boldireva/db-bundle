<?php

namespace TeamSoft\CrmRepositoryBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * InfoContactintask
 *
 * @ORM\Table(name="info_contactintask")
 * @ORM\Entity
 */
class InfoContactintask implements ServiceFieldInterface
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="Description", type="blob", nullable=true)
     */
    private $description;

    /**
     * @var guid
     *
     * @ORM\Column(name="guid", type="guid", nullable=true)
     */
    private $guid;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="currenttime", type="datetime", nullable=true)
     */
    private $currenttime;

    /**
     * @var string
     *
     * @ORM\Column(name="moduser", type="string", length=16, nullable=true)
     */
    private $moduser;

    /**
     * @var string
     *
     * @ORM\Column(name="tasktarget", type="blob", nullable=true)
     */
    private $tasktarget;

    /**
     * @var string
     *
     * @ORM\Column(name="expectresult", type="blob", nullable=true)
     */
    private $expectresult;

    /**
     * @var string
     *
     * @ORM\Column(name="factresult", type="blob", nullable=true)
     */
    private $factresult;

    /**
     * @var InfoContact
     *
     * @ORM\ManyToOne(targetEntity="InfoContact")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="Contact_id", referencedColumnName="id")
     * })
     */
    private $contact;

    /**
     * @var InfoDictionary
     *
     * @ORM\ManyToOne(targetEntity="InfoDictionary")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="Role_id", referencedColumnName="id")
     * })
     */
    private $role;

    /**
     * @var InfoTask
     *
     * @ORM\ManyToOne(targetEntity="InfoTask")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="Task_id", referencedColumnName="id")
     * })
     */
    private $task;



    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set description
     *
     * @param string $description
     *
     * @return InfoContactintask
     */
    public function setDescription($description)
    {
        $this->description = $description;
    
        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set guid
     *
     * @param guid $guid
     *
     * @return InfoContactintask
     */
    public function setGuid($guid)
    {
        $this->guid = $guid;
    
        return $this;
    }

    /**
     * Get guid
     *
     * @return guid
     */
    public function getGuid()
    {
        return $this->guid;
    }

    /**
     * Set currenttime
     *
     * @param \DateTime $currenttime
     *
     * @return InfoContactintask
     */
    public function setCurrenttime(\DateTime $currenttime)
    {
        $this->currenttime = $currenttime;
    
        return $this;
    }

    /**
     * Get currenttime
     *
     * @return \DateTime
     */
    public function getCurrenttime()
    {
        return $this->currenttime;
    }

    /**
     * Set moduser
     *
     * @param string $moduser
     *
     * @return InfoContactintask
     */
    public function setModuser($moduser)
    {
        $this->moduser = $moduser;
    
        return $this;
    }

    /**
     * Get moduser
     *
     * @return string
     */
    public function getModuser()
    {
        return $this->moduser;
    }

    /**
     * Set tasktarget
     *
     * @param string $tasktarget
     *
     * @return InfoContactintask
     */
    public function setTasktarget($tasktarget)
    {
        $this->tasktarget = $tasktarget;
    
        return $this;
    }

    /**
     * Get tasktarget
     *
     * @return string
     */
    public function getTasktarget()
    {
        return $this->tasktarget;
    }

    /**
     * Set expectresult
     *
     * @param string $expectresult
     *
     * @return InfoContactintask
     */
    public function setExpectresult($expectresult)
    {
        $this->expectresult = $expectresult;
    
        return $this;
    }

    /**
     * Get expectresult
     *
     * @return string
     */
    public function getExpectresult()
    {
        return $this->expectresult;
    }

    /**
     * Set factresult
     *
     * @param string $factresult
     *
     * @return InfoContactintask
     */
    public function setFactresult($factresult)
    {
        $this->factresult = $factresult;
    
        return $this;
    }

    /**
     * Get factresult
     *
     * @return string
     */
    public function getFactresult()
    {
        return $this->factresult;
    }

    /**
     * Set contact
     *
     * @param \TeamSoft\CrmRepositoryBundle\Entity\InfoContact $contact
     *
     * @return InfoContactintask
     */
    public function setContact(\TeamSoft\CrmRepositoryBundle\Entity\InfoContact $contact = null)
    {
        $this->contact = $contact;
    
        return $this;
    }

    /**
     * Get contact
     *
     * @return \TeamSoft\CrmRepositoryBundle\Entity\InfoContact
     */
    public function getContact()
    {
        return $this->contact;
    }

    /**
     * Set role
     *
     * @param \TeamSoft\CrmRepositoryBundle\Entity\InfoDictionary $role
     *
     * @return InfoContactintask
     */
    public function setRole(\TeamSoft\CrmRepositoryBundle\Entity\InfoDictionary $role = null)
    {
        $this->role = $role;
    
        return $this;
    }

    /**
     * Get role
     *
     * @return \TeamSoft\CrmRepositoryBundle\Entity\InfoDictionary
     */
    public function getRole()
    {
        return $this->role;
    }

    /**
     * Set task
     *
     * @param \TeamSoft\CrmRepositoryBundle\Entity\InfoTask $task
     *
     * @return InfoContactintask
     */
    public function setTask(\TeamSoft\CrmRepositoryBundle\Entity\InfoTask $task = null)
    {
        $this->task = $task;
    
        return $this;
    }

    /**
     * Get task
     *
     * @return \TeamSoft\CrmRepositoryBundle\Entity\InfoTask
     */
    public function getTask()
    {
        return $this->task;
    }

    public function getTaskId()
    {
        return $this->getTask() ? $this->getTask()->getId() : null;
    }

    public function getContactId()
    {
        return $this->getContact() ? $this->getContact()->getId() : null;
    }
}
