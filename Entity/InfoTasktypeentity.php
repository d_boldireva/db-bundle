<?php

namespace TeamSoft\CrmRepositoryBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * InfoTasktypeentity
 *
 * @ORM\Table(name="info_tasktypeentity")
 * @ORM\Entity
 */
class InfoTasktypeentity
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string|null
     *
     * @ORM\Column(name="entity", type="string", length=255, nullable=true)
     */
    private $entity;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="currenttime", type="datetime", nullable=true)
     */
    private $currenttime;

    /**
     * @var string|null
     *
     * @ORM\Column(name="moduser", type="string", length=16, nullable=true)
     */
    private $moduser;

    /**
     * @var string|null
     *
     * @ORM\Column(name="guid", type="guid", nullable=true)
     */
    private $guid;

    /**
     * @var \InfoTasktype
     *
     * @ORM\ManyToOne(targetEntity="InfoTasktype")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="tasktype_id", referencedColumnName="id")
     * })
     */
    private $tasktype;

    /**
     * @var \InfoReport
     *
     * @ORM\ManyToOne(targetEntity="InfoReport")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="report_id", referencedColumnName="id")
     * })
     */
    private $report;



    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set entity.
     *
     * @param string|null $entity
     *
     * @return InfoTasktypeentity
     */
    public function setEntity($entity = null)
    {
        $this->entity = $entity;

        return $this;
    }

    /**
     * Get entity.
     *
     * @return string|null
     */
    public function getEntity()
    {
        return $this->entity;
    }

    /**
     * Set currenttime.
     *
     * @param \DateTime|null $currenttime
     *
     * @return InfoTasktypeentity
     */
    public function setCurrenttime($currenttime = null)
    {
        $this->currenttime = $currenttime;

        return $this;
    }

    /**
     * Get currenttime.
     *
     * @return \DateTime|null
     */
    public function getCurrenttime()
    {
        return $this->currenttime;
    }

    /**
     * Set moduser.
     *
     * @param string|null $moduser
     *
     * @return InfoTasktypeentity
     */
    public function setModuser($moduser = null)
    {
        $this->moduser = $moduser;

        return $this;
    }

    /**
     * Get moduser.
     *
     * @return string|null
     */
    public function getModuser()
    {
        return $this->moduser;
    }

    /**
     * Set guid.
     *
     * @param string|null $guid
     *
     * @return InfoTasktypeentity
     */
    public function setGuid($guid = null)
    {
        $this->guid = $guid;

        return $this;
    }

    /**
     * Get guid.
     *
     * @return string|null
     */
    public function getGuid()
    {
        return $this->guid;
    }

    /**
     * Set tasktype.
     *
     * @param \TeamSoft\CrmRepositoryBundle\Entity\InfoTasktype|null $tasktype
     *
     * @return InfoTasktypeentity
     */
    public function setTasktype(\TeamSoft\CrmRepositoryBundle\Entity\InfoTasktype $tasktype = null)
    {
        $this->tasktype = $tasktype;

        return $this;
    }

    /**
     * Get tasktype.
     *
     * @return \TeamSoft\CrmRepositoryBundle\Entity\InfoTasktype|null
     */
    public function getTasktype()
    {
        return $this->tasktype;
    }

    /**
     * Set report.
     *
     * @param \TeamSoft\CrmRepositoryBundle\Entity\InfoReport|null $report
     *
     * @return InfoTasktypeentity
     */
    public function setReport(\TeamSoft\CrmRepositoryBundle\Entity\InfoReport $report = null)
    {
        $this->report = $report;

        return $this;
    }

    /**
     * Get report.
     *
     * @return \TeamSoft\CrmRepositoryBundle\Entity\InfoReport|null
     */
    public function getReport()
    {
        return $this->report;
    }

    public function getTasktypeId()
    {
        return $this->getTasktype() ? $this->getTasktype()->getId() : null;
    }
}
