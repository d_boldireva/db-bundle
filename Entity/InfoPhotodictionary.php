<?php

namespace TeamSoft\CrmRepositoryBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * InfoPhotodictionary
 *
 * @ORM\Table(name="info_photodictionary")
 * @ORM\Entity()
 */
class InfoPhotodictionary
{

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", nullable=true)
     */
    private $name;

    /**
     * @var int
     *
     * @ORM\Column(name="page", type="integer", nullable=true)
     */
    private $page;

    /**
     * @var guid
     *
     * @ORM\Column(name="guid", type="guid", nullable=true)
     */
    private $guid;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="currenttime", type="datetime", nullable=true)
     */
    private $currenttime;

    /**
     * @var string
     *
     * @ORM\Column(name="moduser", type="string", length=16, nullable=true)
     */
    private $moduser;


    /**
     * Get id
     *
     * @return int
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName(): ?string
    {
        return $this->name;
    }

    /**
     * Set name
     *
     * @param string|null $name
     *
     * @return self
     */
    public function setName(?string $name): self
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get page
     *
     * @return integer
     */
    public function getPage(): ?int
    {
        return $this->page;
    }

    /**
     * Set page
     *
     * @param int|null $page
     *
     * @return self
     */
    public function setPage(?int $page): ?int
    {
        $this->page = $page;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getGuid(): ?string
    {
        return $this->guid;
    }

    /**
     * @param string $guid
     * @return self
     */
    public function setGuid($guid): self
    {
        $this->guid = $guid;
        return $this;
    }

    /**
     * @return \DateTime|null
     */
    public function getCurrenttime(): ?\DateTime
    {
        return $this->currenttime;
    }

    /**
     * @param \DateTime $currenttime
     * @return self
     */
    public function setCurrenttime(\DateTime $currenttime = null): self
    {
        $this->currenttime = $currenttime;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getModuser(): ?string
    {
        return $this->moduser;
    }

    /**
     * @param string $moduser
     * @return self
     */
    public function setModuser($moduser): self
    {
        $this->moduser = $moduser;
        return $this;
    }
}
