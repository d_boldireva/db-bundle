<?php

namespace TeamSoft\CrmRepositoryBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * PoEntityfields
 *
 * @ORM\Table(name="po_entityfields")
 * @ORM\Entity
 */
class PoEntityfields implements ServiceFieldInterface
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var int|null
     *
     * @ORM\Column(name="page", type="integer", nullable=true)
     */
    private $page;

    /**
     * @var string
     *
     * @ORM\Column(name="field", type="string", length=255, nullable=true)
     */
    private $field;

    /**
     * @var string
     *
     * @ORM\Column(name="`group`", type="string", length=255, nullable=true)
     */
    private $group;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255, nullable=true)
     */
    private $name;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="currenttime", type="datetime", nullable=true)
     */
    private $currenttime;

    /**
     * @var string
     *
     * @ORM\Column(
     *     name="moduser",
     *     type="string",
     *     length=16,
     *     nullable=true,
     *     options={"default"="[dbo].[Get_CurrentCode]()"}
     * )
     */
    private $moduser;

    /**
     * @var string
     *
     * @ORM\Column(name="guid", type="guid", nullable=true)
     */
    private $guid;



    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set page.
     *
     * @param int|null $page
     *
     * @return PoEntityfields
     */
    public function setPage($page = null)
    {
        $this->page = $page;
    
        return $this;
    }

    /**
     * Get page.
     *
     * @return int|null
     */
    public function getPage()
    {
        return $this->page;
    }

    /**
     * Set field.
     *
     * @param string $field
     *
     * @return PoEntityfields
     */
    public function setField($field = null)
    {
        $this->field = $field;
    
        return $this;
    }

    /**
     * Get field.
     *
     * @return string
     */
    public function getField()
    {
        return $this->field;
    }

    /**
     * Set group.
     *
     * @param string $group
     *
     * @return PoEntityfields
     */
    public function setGroup($group = null)
    {
        $this->group = $group;
    
        return $this;
    }

    /**
     * Get group.
     *
     * @return string
     */
    public function getGroup()
    {
        return $this->group;
    }

    /**
     * Set name.
     *
     * @param string $name
     *
     * @return PoEntityfields
     */
    public function setName($name = null)
    {
        $this->name = $name;
    
        return $this;
    }

    /**
     * Get name.
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set currenttime.
     *
     * @param \DateTime|null $currenttime
     *
     * @return PoEntityfields
     */
    public function setCurrenttime(\DateTime $currenttime = null)
    {
        $this->currenttime = $currenttime;
    
        return $this;
    }

    /**
     * Get currenttime.
     *
     * @return \DateTime|null
     */
    public function getCurrenttime()
    {
        return $this->currenttime;
    }

    /**
     * Set moduser.
     *
     * @param string $moduser
     *
     * @return PoEntityfields
     */
    public function setModuser($moduser = null)
    {
        $this->moduser = $moduser;
    
        return $this;
    }

    /**
     * Get moduser.
     *
     * @return string
     */
    public function getModuser()
    {
        return $this->moduser;
    }

    /**
     * Set guid.
     *
     * @param string $guid
     *
     * @return PoEntityfields
     */
    public function setGuid($guid = null)
    {
        $this->guid = $guid;
    
        return $this;
    }

    /**
     * Get guid.
     *
     * @return string
     */
    public function getGuid()
    {
        return $this->guid;
    }
}
