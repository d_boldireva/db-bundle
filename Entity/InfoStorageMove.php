<?php

declare(strict_types=1);

namespace TeamSoft\CrmRepositoryBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Table(name="info_skladmove")
 * @ORM\Entity(repositoryClass="TeamSoft\CrmRepositoryBundle\Repository\InfoStorageMoveRepository")
 */
class InfoStorageMove
{
    public const TYPE_RECEIVE = 1;
    public const TYPE_SEND = 2;

    /**
     * @var int|null
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private ?int $id;

    /**
     * @var \TeamSoft\CrmRepositoryBundle\Entity\InfoStorage
     *
     * @ORM\ManyToOne(targetEntity="InfoStorage", inversedBy="moves")
     * @ORM\JoinColumn(name="sklad_id", referencedColumnName="id")
     */
    private InfoStorage $storage;

    /**
     * @var \TeamSoft\CrmRepositoryBundle\Entity\InfoUser|null
     *
     * @ORM\ManyToOne(targetEntity="InfoUser")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     */
    private ?InfoUser $user;

    /**
     * @var \TeamSoft\CrmRepositoryBundle\Entity\InfoPromomaterial
     *
     * @ORM\ManyToOne(targetEntity="InfoPromomaterial")
     * @ORM\JoinColumn(name="promo_id", referencedColumnName="id")
     */
    private InfoPromomaterial $promoMaterial;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="date", type="date", nullable=true)
     */
    private ?\DateTime $date;

    /**
     * @var int|null
     *
     * @ORM\Column(name="cnt", type="integer", nullable=true)
     */
    private ?int $quantity;

    /**
     * @var float|null
     *
     * @ORM\Column(name="summ", type="float", nullable=true)
     */
    private ?float $totalCost;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="date2", type="date", nullable=true)
     */
    private ?\DateTime $dateReceived;

    /**
     * @var int|null
     *
     * @ORM\Column(name="cnt2", type="integer", nullable=true)
     */
    private ?int $quantityReceived;

    public function __construct()
    {
        $this->storage = new InfoStorage();
        $this->promoMaterial = new InfoPromomaterial();
    }

    /**
     * @param  \TeamSoft\CrmRepositoryBundle\Entity\InfoStorage  $storage
     * @param  \TeamSoft\CrmRepositoryBundle\Entity\InfoUser  $user
     * @param  \TeamSoft\CrmRepositoryBundle\Entity\InfoPromomaterial  $promoMaterial
     * @param  \DateTime  $date
     * @param  int  $quantity
     * @param  float  $total
     *
     * @return self
     */
    public static function create(
        InfoStorage $storage,
        InfoUser $user,
        InfoPromomaterial $promoMaterial,
        \DateTime $date,
        int $quantity,
        float $total
    ): self {
        return (new self())
            ->setStorage($storage)
            ->setUser($user)
            ->setPromoMaterial($promoMaterial)
            ->setDate($date)
            ->setDateReceived($date)
            ->setQuantity($quantity)
            ->setQuantityReceived($quantity)
            ->setTotalCost($total);
    }

    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return \TeamSoft\CrmRepositoryBundle\Entity\InfoStorage
     */
    public function getStorage(): InfoStorage
    {
        return $this->storage;
    }

    /**
     * @param  \TeamSoft\CrmRepositoryBundle\Entity\InfoStorage  $storage
     *
     * @return $this
     */
    public function setStorage(InfoStorage $storage): self
    {
        $this->storage = $storage;

        return $this;
    }

    /**
     * @return \TeamSoft\CrmRepositoryBundle\Entity\InfoUser|null
     */
    public function getUser(): ?InfoUser
    {
        return $this->user;
    }

    /**
     * @param  \TeamSoft\CrmRepositoryBundle\Entity\InfoUser|null  $user
     *
     * @return $this
     */
    public function setUser(?InfoUser $user): self
    {
        $this->user = $user;

        return $this;
    }

    /**
     * @return \TeamSoft\CrmRepositoryBundle\Entity\InfoPromomaterial
     */
    public function getPromoMaterial(): InfoPromomaterial
    {
        return $this->promoMaterial;
    }

    /**
     * @param  \TeamSoft\CrmRepositoryBundle\Entity\InfoPromomaterial  $promoMaterial
     *
     * @return $this
     */
    public function setPromoMaterial(InfoPromomaterial $promoMaterial): self
    {
        $this->promoMaterial = $promoMaterial;

        return $this;
    }

    /**
     * @return \DateTime|null
     */
    public function getDate(): ?\DateTime
    {
        return $this->date;
    }

    /**
     * @param  \DateTime|null  $date
     *
     * @return $this
     */
    public function setDate(?\DateTime $date): self
    {
        $this->date = $date;

        return $this;
    }

    /**
     * @return int|null
     */
    public function getQuantity(): ?int
    {
        return $this->quantity;
    }

    /**
     * @param  int|null  $quantity
     *
     * @return $this
     */
    public function setQuantity(?int $quantity): self
    {
        $this->quantity = $quantity;

        return $this;
    }

    /**
     * @return float|null
     */
    public function getTotalCost(): ?float
    {
        return $this->totalCost;
    }

    /**
     * @param  float|null  $totalCost
     *
     * @return $this
     */
    public function setTotalCost(?float $totalCost): self
    {
        $this->totalCost = $totalCost;

        return $this;
    }

    /**
     * @return \DateTime|null
     */
    public function getDateReceived(): ?\DateTime
    {
        return $this->dateReceived;
    }

    /**
     * @param  \DateTime|null  $dateReceived
     *
     * @return $this
     */
    public function setDateReceived(?\DateTime $dateReceived): self
    {
        $this->dateReceived = $dateReceived;

        return $this;
    }

    /**
     * @return int|null
     */
    public function getQuantityReceived(): ?int
    {
        return $this->quantityReceived;
    }

    /**
     * @param  int|null  $quantityReceived
     *
     * @return $this
     */
    public function setQuantityReceived(?int $quantityReceived): self
    {
        $this->quantityReceived = $quantityReceived;

        return $this;
    }

    /**
     * @return int
     * @psalm-return InfoStorageMove::TYPE_SEND|InfoStorageMove::TYPE_RECEIVE
     */
    public function getType(): int
    {
        if ($this->quantity < 0) {
            return self::TYPE_SEND;
        }

        return self::TYPE_RECEIVE;
    }
}
