<?php

namespace TeamSoft\CrmRepositoryBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * InfoPlanpreparation2
 *
 * @ORM\Table(name="info_planpreparation2")
 * @ORM\Entity(repositoryClass="TeamSoft\CrmRepositoryBundle\Repository\InfoPlanpreparation2")
 */
class InfoPlanpreparation2 implements ServiceFieldInterface
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var guid
     *
     * @ORM\Column(name="guid", type="guid", nullable=true)
     */
    private $guid;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="currenttime", type="datetime", nullable=true)
     */
    private $currenttime;

    /**
     * @var string
     *
     * @ORM\Column(name="moduser", type="string", length=16, nullable=true)
     */
    private $moduser;

    /**
     * @var int
     *
     * @ORM\Column(name="cnt_a", type="integer", nullable=true)
     */
    private $cntA;

    /**
     * @var int
     *
     * @ORM\Column(name="cnt_b", type="integer", nullable=true)
     */
    private $cntB;

    /**
     * @var int
     *
     * @ORM\Column(name="cnt_c", type="integer", nullable=true)
     */
    private $cntC;

    /**
     * @var int
     *
     * @ORM\Column(name="restcnt", type="integer", nullable=true)
     */
    private $restcnt;

    /**
     * @var \InfoPlan
     *
     * @ORM\ManyToOne(targetEntity="InfoPlan")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="plan_id", referencedColumnName="id")
     * })
     */
    private $plan;

    /**
     * @var \InfoPreparation
     *
     * @ORM\ManyToOne(targetEntity="InfoPreparation")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="prep_id", referencedColumnName="id")
     * })
     */
    private $prep;



    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set guid
     *
     * @param guid $guid
     *
     * @return InfoPlanpreparation2
     */
    public function setGuid($guid)
    {
        $this->guid = $guid;
    
        return $this;
    }

    /**
     * Get guid
     *
     * @return guid
     */
    public function getGuid()
    {
        return $this->guid;
    }

    /**
     * Set currenttime
     *
     * @param \DateTime $currenttime
     *
     * @return InfoPlanpreparation2
     */
    public function setCurrenttime(\DateTime $currenttime)
    {
        $this->currenttime = $currenttime;
    
        return $this;
    }

    /**
     * Get currenttime
     *
     * @return \DateTime
     */
    public function getCurrenttime()
    {
        return $this->currenttime;
    }

    /**
     * Set moduser
     *
     * @param string $moduser
     *
     * @return InfoPlanpreparation2
     */
    public function setModuser($moduser)
    {
        $this->moduser = $moduser;
    
        return $this;
    }

    /**
     * Get moduser
     *
     * @return string
     */
    public function getModuser()
    {
        return $this->moduser;
    }

    /**
     * Set cntA
     *
     * @param int $cntA
     *
     * @return InfoPlanpreparation2
     */
    public function setCntA($cntA)
    {
        $this->cntA = $cntA;
    
        return $this;
    }

    /**
     * Get cntA
     *
     * @return int
     */
    public function getCntA()
    {
        return $this->cntA;
    }

    /**
     * Set cntB
     *
     * @param int $cntB
     *
     * @return InfoPlanpreparation2
     */
    public function setCntB($cntB)
    {
        $this->cntB = $cntB;
    
        return $this;
    }

    /**
     * Get cntB
     *
     * @return int
     */
    public function getCntB()
    {
        return $this->cntB;
    }

    /**
     * Set cntC
     *
     * @param int $cntC
     *
     * @return InfoPlanpreparation2
     */
    public function setCntC($cntC)
    {
        $this->cntC = $cntC;
    
        return $this;
    }

    /**
     * Get cntC
     *
     * @return int
     */
    public function getCntC()
    {
        return $this->cntC;
    }

    /**
     * Set restcnt
     *
     * @param int $restcnt
     *
     * @return InfoPlanpreparation2
     */
    public function setRestcnt($restcnt)
    {
        $this->restcnt = $restcnt;
    
        return $this;
    }

    /**
     * Get restcnt
     *
     * @return int
     */
    public function getRestcnt()
    {
        return $this->restcnt;
    }

    /**
     * Set plan
     *
     * @param \TeamSoft\CrmRepositoryBundle\Entity\InfoPlan $plan
     *
     * @return InfoPlanpreparation2
     */
    public function setPlan(\TeamSoft\CrmRepositoryBundle\Entity\InfoPlan $plan = null)
    {
        $this->plan = $plan;
    
        return $this;
    }

    /**
     * Get plan
     *
     * @return \TeamSoft\CrmRepositoryBundle\Entity\InfoPlan
     */
    public function getPlan()
    {
        return $this->plan;
    }

    /**
     * Set prep
     *
     * @param \TeamSoft\CrmRepositoryBundle\Entity\InfoPreparation $prep
     *
     * @return InfoPlanpreparation2
     */
    public function setPrep(\TeamSoft\CrmRepositoryBundle\Entity\InfoPreparation $prep = null)
    {
        $this->prep = $prep;
    
        return $this;
    }

    /**
     * Get prep
     *
     * @return \TeamSoft\CrmRepositoryBundle\Entity\InfoPreparation
     */
    public function getPrep()
    {
        return $this->prep;
    }

    public function getPreparationId ()
    {
        return $this->getPrep() ? $this->getPrep()->getId() : null;
    }

    public function getPlanId()
    {
        return $this->getPlan() ? $this->getPlan()->getId() : null;
    }
}
