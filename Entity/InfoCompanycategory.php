<?php

namespace TeamSoft\CrmRepositoryBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * InfoCompanycategory
 *
 * @ORM\Table(name="info_companycategory", indexes={@ORM\Index(name="ix_info_companycategory_guid", columns={"guid"})})
 * @ORM\Entity(repositoryClass="TeamSoft\CrmRepositoryBundle\Repository\Company\InfoCompanyCategoryRepository")
 */
class InfoCompanycategory implements ServiceFieldInterface
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255, nullable=true)
     */
    private $name;

    /**
     * @var integer
     *
     * @ORM\Column(name="value1", type="integer", nullable=true)
     */
    private $value1;

    /**
     * @var integer
     *
     * @ORM\Column(name="value2", type="integer", nullable=true)
     */
    private $value2;

    /**
     * @var guid
     *
     * @ORM\Column(name="guid", type="guid", nullable=true)
     */
    private $guid;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="currenttime", type="datetime", nullable=true)
     */
    private $currenttime;

    /**
     * @var string
     *
     * @ORM\Column(name="moduser", type="string", length=16, nullable=true)
     */
    private $moduser;

    /**
     * @var integer
     *
     * @ORM\Column(name="flag", type="integer", nullable=true)
     */
    private $flag;

    /**
     * @var integer
     *
     * @ORM\Column(name="sale", type="integer", nullable=true)
     */
    private $sale;

    /**
     * @var string
     *
     * @ORM\Column(name="Description", type="text", length=16, nullable=true)
     */
    private $description;

    /**
     * @ORM\ManyToMany(targetEntity="InfoCompanytype", mappedBy="companycategories")
     */
    private $companytypes;

    /**
     * @var InfoDirection
     *
     * @ORM\ManyToOne(targetEntity="InfoDirection")
     * @ORM\JoinColumn(name="direction_id", referencedColumnName="id")
     */
    private $direction;

    /**
     * @var integer
     *
     * @ORM\Column(name="targeting_cnt", type="integer", nullable=true)
     */
    private $targetingCnt;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->companytypes = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return InfoCompanycategory
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set value1
     *
     * @param integer $value1
     * @return InfoCompanycategory
     */
    public function setValue1($value1)
    {
        $this->value1 = $value1;

        return $this;
    }

    /**
     * Get value1
     *
     * @return integer
     */
    public function getValue1()
    {
        return $this->value1;
    }

    /**
     * Set value2
     *
     * @param integer $value2
     * @return InfoCompanycategory
     */
    public function setValue2($value2)
    {
        $this->value2 = $value2;

        return $this;
    }

    /**
     * Get value2
     *
     * @return integer
     */
    public function getValue2()
    {
        return $this->value2;
    }

    /**
     * Set guid
     *
     * @param guid $guid
     * @return InfoCompanycategory
     */
    public function setGuid($guid)
    {
        $this->guid = $guid;

        return $this;
    }

    /**
     * Get guid
     *
     * @return guid
     */
    public function getGuid()
    {
        return $this->guid;
    }

    /**
     * Set currenttime
     *
     * @param \DateTime $currenttime
     * @return InfoCompanycategory
     */
    public function setCurrenttime(\DateTime $currenttime)
    {
        $this->currenttime = $currenttime;

        return $this;
    }

    /**
     * Get currenttime
     *
     * @return \DateTime
     */
    public function getCurrenttime()
    {
        return $this->currenttime;
    }

    /**
     * Set moduser
     *
     * @param string $moduser
     * @return InfoCompanycategory
     */
    public function setModuser($moduser)
    {
        $this->moduser = $moduser;

        return $this;
    }

    /**
     * Get moduser
     *
     * @return string
     */
    public function getModuser()
    {
        return $this->moduser;
    }

    /**
     * Set flag
     *
     * @param integer $flag
     * @return InfoCompanycategory
     */
    public function setFlag($flag)
    {
        $this->flag = $flag;

        return $this;
    }

    /**
     * Get flag
     *
     * @return integer
     */
    public function getFlag()
    {
        return $this->flag;
    }

    /**
     * Set sale
     *
     * @param integer $sale
     * @return InfoCompanycategory
     */
    public function setSale($sale)
    {
        $this->sale = $sale;

        return $this;
    }

    /**
     * Get sale
     *
     * @return integer
     */
    public function getSale()
    {
        return $this->sale;
    }

    /**
     * Set description
     *
     * @param string $description
     * @return InfoCompanycategory
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Add companytype
     *
     * @param \TeamSoft\CrmRepositoryBundle\Entity\InfoCompanytype $companytype
     *
     * @return InfoCompanycategory
     */
    public function addCompanytype(\TeamSoft\CrmRepositoryBundle\Entity\InfoCompanytype $companytype)
    {
        $this->companytypes[] = $companytype;

        return $this;
    }

    /**
     * Remove companytype
     *
     * @param \TeamSoft\CrmRepositoryBundle\Entity\InfoCompanytype $companytype
     */
    public function removeCompanytype(\TeamSoft\CrmRepositoryBundle\Entity\InfoCompanytype $companytype)
    {
        $this->companytypes->removeElement($companytype);
    }

    /**
     * Get companytypes
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getCompanytypes()
    {
        return $this->companytypes;
    }

    /**
     * Set direction
     *
     * @param InfoDirection $direction
     *
     * @return $this
     */
    public function setDirection(InfoDirection $direction = null)
    {
        $this->direction = $direction;

        return $this;
    }

    /**
     * Set targetingCnt
     *
     * @param int $targetingCnt
     * @return $this
     */
    public function setTargetingCnt(int $targetingCnt): self
    {
        $this->targetingCnt = $targetingCnt;

        return $this;
    }

    /**
     * Get targetingCnt
     * @return int
     */
    public function getTargetingCnt(): int
    {
        return $this->targetingCnt;
    }

    /**
     * Get direction
     *
     * @return InfoDirection
     */
    public function getDirection()
    {
        return $this->direction;
    }

    /**
     * @return int|null
     */
    public function getDirectionId(): ?int
    {
        return $this->getDirection() ? $this->getDirection()->getId() : null;
    }
}
