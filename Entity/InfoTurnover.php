<?php

namespace TeamSoft\CrmRepositoryBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * InfoTurnover
 *
 * @ORM\Table(name="info_turnover")
 * @ORM\Entity
 */
class InfoTurnover
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="dt", type="datetime", nullable=true)
     */
    private $dt;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="dt2", type="datetime", nullable=true)
     */
    private $dt2;

    /**
     * @var int|null
     *
     * @ORM\Column(name="turnover", type="integer", nullable=true)
     */
    private $turnover;

    /**
     * @var int|null
     *
     * @ORM\Column(name="morionid", type="integer", nullable=true)
     */
    private $morionid;

    /**
     * @var int|null
     *
     * @ORM\Column(name="DATA_YEAR", type="integer", nullable=true)
     */
    private $dataYear;

    /**
     * @var int|null
     *
     * @ORM\Column(name="DATA_QUARTER", type="integer", nullable=true)
     */
    private $dataQuarter;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="currenttime", type="datetime", nullable=true)
     */
    private $currenttime;

    /**
     * @var int|null
     *
     * @ORM\Column(name="DATA_MONTH", type="integer", nullable=true)
     */
    private $dataMonth;

    /**
     * @var InfoCompany
     *
     * @ORM\ManyToOne(targetEntity="InfoCompany")
     * @ORM\JoinColumn(name="company_id", referencedColumnName="id")
     */
    private $company;

    /**
     * @var string|null
     * @ORM\Column(name="category", type="string", length=50, nullable=true)
     */
    private $category;

    /**
     * @var string|null
     * @ORM\Column(name="rx_otc", type="string", length=50, nullable=true)
     */
    private $rx_otc;



    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set dt.
     *
     * @param \DateTime|null $dt
     *
     * @return InfoTurnover
     */
    public function setDt($dt = null)
    {
        $this->dt = $dt;
    
        return $this;
    }

    /**
     * Get dt.
     *
     * @return \DateTime|null
     */
    public function getDt()
    {
        return $this->dt;
    }

    /**
     * Set dt2.
     *
     * @param \DateTime|null $dt2
     *
     * @return InfoTurnover
     */
    public function setDt2($dt2 = null)
    {
        $this->dt2 = $dt2;
    
        return $this;
    }

    /**
     * Get dt2.
     *
     * @return \DateTime|null
     */
    public function getDt2()
    {
        return $this->dt2;
    }

    /**
     * Set turnover.
     *
     * @param int|null $turnover
     *
     * @return InfoTurnover
     */
    public function setTurnover($turnover = null)
    {
        $this->turnover = $turnover;
    
        return $this;
    }

    /**
     * Get turnover.
     *
     * @return int|null
     */
    public function getTurnover()
    {
        return $this->turnover;
    }

    /**
     * Set morionid.
     *
     * @param int|null $morionid
     *
     * @return InfoTurnover
     */
    public function setMorionid($morionid = null)
    {
        $this->morionid = $morionid;
    
        return $this;
    }

    /**
     * Get morionid.
     *
     * @return int|null
     */
    public function getMorionid()
    {
        return $this->morionid;
    }

    /**
     * Set dataYear.
     *
     * @param int|null $dataYear
     *
     * @return InfoTurnover
     */
    public function setDataYear($dataYear = null)
    {
        $this->dataYear = $dataYear;
    
        return $this;
    }

    /**
     * Get dataYear.
     *
     * @return int|null
     */
    public function getDataYear()
    {
        return $this->dataYear;
    }

    /**
     * Set dataQuarter.
     *
     * @param int|null $dataQuarter
     *
     * @return InfoTurnover
     */
    public function setDataQuarter($dataQuarter = null)
    {
        $this->dataQuarter = $dataQuarter;
    
        return $this;
    }

    /**
     * Get dataQuarter.
     *
     * @return int|null
     */
    public function getDataQuarter()
    {
        return $this->dataQuarter;
    }

    /**
     * Set currenttime.
     *
     * @param \DateTime|null $currenttime
     *
     * @return InfoTurnover
     */
    public function setCurrenttime($currenttime = null)
    {
        $this->currenttime = $currenttime;
    
        return $this;
    }

    /**
     * Get currenttime.
     *
     * @return \DateTime|null
     */
    public function getCurrenttime()
    {
        return $this->currenttime;
    }

    /**
     * Set dataMonth.
     *
     * @param int|null $dataMonth
     *
     * @return InfoTurnover
     */
    public function setDataMonth($dataMonth = null)
    {
        $this->dataMonth = $dataMonth;
    
        return $this;
    }

    /**
     * Get dataMonth.
     *
     * @return int|null
     */
    public function getDataMonth()
    {
        return $this->dataMonth;
    }

    /**
     * Set company.
     *
     * @param InfoCompany|null $company
     *
     * @return InfoTurnover
     */
    public function setCompany(InfoCompany $company = null)
    {
        $this->company = $company;
    
        return $this;
    }

    /**
     * Get company.
     *
     * @return InfoCompany|null
     */
    public function getCompany()
    {
        return $this->company;
    }

    /**
     * @return string|null
     *
     * GEODATA-534
     */
    public function getCategory(){
        return $this->category;
    }

    /**
     * @param string|null $category
     * @return $this
     *
     * GEODATA-534
     */
    public function setCategory($category = null){
        $this->category = $category;
        return $this;
    }

    /**
     * @return string|null
     *
     * GEODATA-534
     */
    public function getRxOTC(){
        return $this->rx_otc;
    }

    /**
     * @param string|null $RxOTC
     * @return $this
     *
     * GEODATA-534
     */
    public function setRxOTC($RxOTC = null){
        $this->rx_otc = $RxOTC;
        return $this;
    }
}
