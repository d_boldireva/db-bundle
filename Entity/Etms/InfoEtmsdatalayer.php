<?php

namespace TeamSoft\CrmRepositoryBundle\Entity\Etms;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use TeamSoft\CrmRepositoryBundle\Entity\InfoDirection;
use TeamSoft\CrmRepositoryBundle\Entity\InfoUser;
use TeamSoft\CrmRepositoryBundle\Entity\ServiceFieldInterface;

/**
 * Class InfoEtmsdatalayer
 * @package TeamSoft\CrmRepositoryBundle\Entity\Etms
 *
 * @ORM\Table(name="info_etmsdatalayer")
 * @ORM\Entity(repositoryClass="\TeamSoft\CrmRepositoryBundle\Repository\Etms\InfoEtmsdatalayer")
 */
class InfoEtmsdatalayer
{
    /**
     * @var int|null
     *
     * @ORM\GeneratedValue(strategy="IDENTITY")
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id()
     */
    private $id;
    /**
     * @var string|null
     * @ORM\Column(name="name", type="string", length=255, nullable=true)
     */
    private $name;
    /**
     * @var string|null
     * @ORM\Column(name="description", type="string", length=512, nullable=true)
     */
    private $description;

    /**
     * @var integer|null
     * @ORM\Column(name="enable", type="integer", nullable=true)
     */
    private $enable;

    /**
     * @var InfoUser
     * @ORM\ManyToOne(targetEntity="TeamSoft\CrmRepositoryBundle\Entity\InfoUser")
     * @ORM\JoinColumn(name="owner_id", referencedColumnName="id")
     */
    private $owner;
    /**
     * @var integer
     *
     * @ORM\Column(name="is_datepicker", type="integer", nullable=true)
     */
    private $is_datepicker;
    /**
     * @var integer
     * @ORM\Column(name="is_userlist", type="integer", nullable=true)
     */
    private $is_userlist;
    /**
     * @var integer
     * @ORM\Column(name="is_labellist", type="integer", nullable=true)
     */
    private $is_labellist;

    /**
     * @var \DateTime|null
     * @ORM\Column(name="currenttime", type="datetime", nullable=true)
     */
    private $currenttime;
    /**
     * @var string|null
     * @ORM\Column(name="moduser", type="string", length=16, nullable=true)
     */
    private $moduser;
    /**
     * @var string|null
     * @ORM\Column(name="guid", type="guid", nullable=true)
     */
    private $guid;

    /**
     * @var ArrayCollection|InfoEtmsdatalayervalue[]
     *
     * @ORM\OneToMany(targetEntity="InfoEtmsdatalayervalue", mappedBy="datalayer")
     */
    private $valueCollection;
    /**
     * @var ArrayCollection|InfoDirection[]
     *
     * @ORM\ManyToMany(targetEntity="TeamSoft\CrmRepositoryBundle\Entity\InfoDirection")
     * @ORM\JoinTable(
     *     name="info_etmsdatalayerdirection",
     *     joinColumns={
     *          @ORM\JoinColumn(name="datalayer_id", referencedColumnName="id")
     *     },
     *     inverseJoinColumns={
     *          @ORM\JoinColumn(name="direction_id", referencedColumnName="id")
     *     }
     * )
     */
    private $directionCollection;
    /**
     * @var ArrayCollection|InfoEtmsdatalayertag[]
     * @ORM\ManyToMany(targetEntity="InfoEtmsdatalayertag", inversedBy="dataLayerCollection")
     * @ORM\JoinTable(
     *     name="info_etmsdatalayertags",
     *     joinColumns={
     *          @ORM\JoinColumn(name="datalayer_id", referencedColumnName="id")
     *     },
     *     inverseJoinColumns={
     *          @ORM\JoinColumn(name="tag_id", referencedColumnName="id")
     *     }
     * )
     */
    private $tagCollection;

    /**
     * @var InfoEtmscustomcolorrange
     *
     * @ORM\ManyToOne(targetEntity="InfoEtmscustomcolorrange", inversedBy="layerCollection")
     * @ORM\JoinColumn(name="customcolorrange_id", referencedColumnName="id")
     */
    private $colorRange;

    /**
     * @var ArrayCollection|InfoEtmsdatalayerparam[]
     * @ORM\OneToMany(targetEntity="TeamSoft\CrmRepositoryBundle\Entity\Etms\InfoEtmsdatalayerparam", mappedBy="datalayer")
     */
    private $paramCollection;

    /**
     * InfoEtmsdatalayer constructor.
     */
    public function __construct()
    {
        $this->valueCollection = new ArrayCollection();
        $this->directionCollection = new ArrayCollection();
        $this->tagCollection = new ArrayCollection();
        $this->paramCollection = new ArrayCollection();
    }

    /**
     * Get id
     *
     * @return int|null
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string|null $name
     * @return $this
     */
    public function setName(?string $name = null): self
    {
        $this->name = $name;
        return $this;
    }

    /**
     * Get name
     *
     * @return string|null
     */
    public function getName(): ?string
    {
        return $this->name;
    }

    /**
     * Set description
     *
     * @param string|null $description
     * @return InfoEtmsdatalayer
     */
    public function setDescription(?string $description = null): self
    {
        $this->description = $description;
        return $this;
    }

    /**
     * Get description
     *
     * @return string|null
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set enable
     *
     * @param null $enable
     * @return InfoEtmsdatalayer
     */
    public function setEnable($enable = null): self
    {
        $this->enable = $enable;
        return $this;
    }

    /**
     * Get enable
     *
     * @return int|null
     */
    public function getEnable(): ?int
    {
        return $this->enable;
    }

    /**
     * Set owner
     *
     * @param InfoUser|null $user
     * @return InfoEtmsdatalayer
     */
    public function setOwner(InfoUser $user = null): self
    {
        $this->owner = $user;
        return $this;
    }

    /**
     * Get owner
     *
     * @return InfoUser
     */
    public function getOwner(): InfoUser
    {
        return $this->owner;
    }

    /**
     * Set currenttime
     *
     * @param \DateTime $value
     * @return $this|ServiceFieldInterface
     */
    public function setCurrenttime(\DateTime $value)
    {
        $this->currenttime = $value;
        return $this;
    }

    /**
     * Get currenttime
     *
     * @return \DateTime|string|null
     */
    public function getCurrenttime()
    {
        return $this->currenttime;
    }

    /**
     * Set moduser
     *
     * @param string $value
     * @return $this|ServiceFieldInterface
     */
    public function setModuser($value)
    {
        $this->moduser = $value;
        return $this;
    }

    /**
     * Get moduser
     *
     * @return string|null
     */
    public function getModuser()
    {
        return $this->moduser;
    }

    /**
     * Set guid
     *
     * @param int $value
     * @return $this|ServiceFieldInterface
     */
    public function setGuid($value)
    {
        $this->guid = $value;
        return $this;
    }

    /**
     * Get guid
     *
     * @return int|string|null
     */
    public function getGuid()
    {
        return $this->guid;
    }

    /**
     * @return ArrayCollection|InfoEtmsdatalayervalue[]
     */
    public function getValueCollection()
    {
        return $this->valueCollection;
    }

    /**
     * @param InfoEtmsdatalayervalue $value
     * @return $this
     */
    public function addValueCollection(InfoEtmsdatalayervalue $value)
    {
        if (!$this->valueCollection->contains($value)) {
            $this->valueCollection->add($value);
            $value->setDatalayer($this);
        }

        return $this;
    }

    /**
     * @param InfoEtmsdatalayervalue $value
     * @return $this
     */
    public function removeValueCollection(InfoEtmsdatalayervalue $value)
    {
        if ($this->valueCollection->contains($value)) {
            $this->valueCollection->removeElement($value);
        }

        $value->setDatalayer(null);

        return $this;
    }

    /**
     * @return $this
     */
    public function clearValueCollection()
    {
        $this->valueCollection->clear();
        return $this;
    }

    /**
     * @return ArrayCollection|InfoDirection[]
     */
    public function getDirectionCollection()
    {
        return $this->directionCollection;
    }

    /**
     * @param InfoDirection $direction
     * @return $this
     */
    public function addDirectionCollection(InfoDirection $direction)
    {
        if (!$this->directionCollection->contains($direction)) {
            $this->directionCollection->add($direction);
        }

        return $this;
    }

    /**
     * @param InfoDirection $direction
     * @return $this
     */
    public function removeDirectionCollection(InfoDirection $direction)
    {
        if ($this->directionCollection->contains($direction)) {
            $this->directionCollection->removeElement($direction);
        }

        return $this;
    }

    /**
     * @return $this
     */
    public function clearDirectionCollection()
    {
        $this->directionCollection->clear();
        return $this;
    }

    /**
     * @return ArrayCollection|InfoEtmsdatalayertag[]
     */
    public function getTagCollection()
    {
        return $this->tagCollection;
    }

    /**
     * @param InfoEtmsdatalayertag $tag
     * @return $this
     */
    public function addTagCollection(InfoEtmsdatalayertag $tag)
    {
        if (!$this->tagCollection->contains($tag)) {
            $this->tagCollection->add($tag);
        }

        return $this;
    }

    /**
     * @param InfoEtmsdatalayertag $tag
     * @return $this
     */
    public function removeTagCollection(InfoEtmsdatalayertag $tag)
    {
        if ($this->tagCollection->contains($tag)) {
            $this->tagCollection->removeElement($tag);
        }

        return $this;
    }

    /**
     * @return $this
     */
    public function clearTagCollection()
    {
        $this->tagCollection->clear();

        return $this;
    }

    /**
     * @return int|null
     */
    public function getOwnerId()
    {
        return $this->owner ? $this->owner->getId() : null;
    }

    /**
     * @return array|string[]
     */
    public function getLayerCodes()
    {
        $codes = [];
        foreach ($this->tagCollection as $tag) {
            $codes[] = $tag->getCode();
        }
        return $codes;
    }

    /**
     * @param $is_datepicker
     * @return $this
     */
    public function setIsDatepicker($is_datepicker)
    {
        $this->is_datepicker = $is_datepicker ? 1 : 0;
        return $this;
    }

    /**
     * @return int
     */
    public function getIsDatepicker()
    {
        return $this->is_datepicker ? 1 : 0;
    }

    /**
     * @param $is_labellist
     * @return $this
     */
    public function setIsLabellist($is_labellist)
    {
        $this->is_labellist = $is_labellist;
        return $this;
    }

    /**
     * @return int
     */
    public function getIsLabellist()
    {
        return $this->is_labellist ? 1 : 0;
    }

    /**
     * @param int $is_userlist
     * @return $this
     */
    public function setIsUserlist($is_userlist = 0)
    {
        $this->is_userlist = $is_userlist;
        return $this;
    }

    /**
     * @return int
     */
    public function getIsUserlist()
    {
        return $this->is_userlist;
    }

    /**
     * @param InfoEtmscustomcolorrange $etmscustomcolorrange
     * @return $this
     */
    public function setColorrange(InfoEtmscustomcolorrange $etmscustomcolorrange)
    {
        $this->colorRange = $etmscustomcolorrange;
        return $this;
    }

    /**
     * @return InfoEtmscustomcolorrange
     */
    public function getColorrange()
    {
        return $this->colorRange;
    }

    /**
     * @return array|null
     */
    public function getPalette()
    {
        if ($this->colorRange) {
            $palette = [];

            /**
             * @var InfoEtmscustomcolorrangecolor $color
             */
            foreach ($this->colorRange->getColorCollection() as $color) {
                $palette['' . $color->getTopBound()] = $color->getColor();
            }

            return $palette;
        }

        return null;
    }

    /**
     * @return string|null
     */
    public function getMainColor()
    {
        if ($palette = $this->getPalette()) {
            $top = max(array_keys($palette));

            return $palette[$top];
        }

        return null;
    }

    /**
     * @return ArrayCollection|InfoEtmsdatalayerparam[]
     */
    public function getParamCollection()
    {
        return $this->paramCollection;
    }

    /**
     * @param InfoEtmsdatalayerparam $param
     * @return $this
     */
    public function addParamCollection(InfoEtmsdatalayerparam $param)
    {
        if (!$this->paramCollection->contains($param)) {
            $param->setDatalayer($this);
            $this->paramCollection->add($param);
        }

        return $this;
    }

    /**
     * @param InfoEtmsdatalayerparam $param
     * @return $this
     */
    public function removeParamCollection(InfoEtmsdatalayerparam $param)
    {
        if ($this->paramCollection->contains($param)) {
            $this->paramCollection->removeElement($param);
            $param->setDatalayer(null);
        }

        return $this;
    }

    /**
     * @return $this
     */
    public function clearParamCollection()
    {
        foreach ($this->paramCollection as $param) {
            $this->paramCollection->removeElement($param);
            $param->setDatalayer(null);
        }
        return $this;
    }
}
