<?php

namespace TeamSoft\CrmRepositoryBundle\Entity\Etms;

use TeamSoft\CrmRepositoryBundle\Entity\InfoDirection;
use TeamSoft\CrmRepositoryBundle\Entity\InfoUser;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Table(name="info_etmsmapsversion")
 * @ORM\Entity()
 *
 * Class InfoEtmsmapsversion
 * @package TeamSoft\CrmRepositoryBundle\Entity\Etms
 */
class InfoEtmsmapsversion
{
    /**
     * @var int
     *
     * @ORM\GeneratedValue(strategy="IDENTITY")
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id()
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", nullable=true, length=255)
     */
    private $name;

    /**
     * @var InfoUser
     *
     * @ORM\ManyToOne(targetEntity="TeamSoft\CrmRepositoryBundle\Entity\InfoUser")
     * @ORM\JoinColumn(name="creator_id", referencedColumnName="id")
     */
    private $creator;

    /**
     * @var InfoDirection
     *
     * @ORM\ManyToOne(targetEntity="TeamSoft\CrmRepositoryBundle\Entity\InfoDirection", inversedBy="mapVersionCollection")
     * @ORM\JoinColumn(name="direction_id", referencedColumnName="id")
     */
    private $direction;

    /**
     * @var int
     *
     * @ORM\Column(name="isAPI", type="integer", nullable=true)
     */
    private $isAPI;

    /**
     * @var int
     *
     * @ORM\Column(name="isArchive", type="integer", nullable=true)
     */
    private $isArchive;

    /**
     * @var int
     *
     * @ORM\Column(name="isTargeting", type="integer", nullable=true)
     */
    private $isTargeting;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="currenttime", type="datetime", nullable=true)
     */
    private $currenttime;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set version name
     *
     * @param $name
     * @return $this
     */
    public function setName($name)
    {
        $this->name = $name;
        return $this;
    }

    /**
     * Get version name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set version creator
     *
     * @param InfoUser $creator
     * @return $this
     */
    public function setCreator(InfoUser $creator)
    {
        $this->creator = $creator;
        return $this;
    }

    /**
     * Get version creator
     *
     * @return InfoUser
     */
    public function getCreator()
    {
        return $this->creator;
    }

    /**
     * @return int|null
     */
    public function getCreatorId()
    {
        return $this->creator ? $this->creator->getId() : null;
    }

    /**
     * Set version direction
     *
     * @param InfoDirection $direction
     * @return $this
     */
    public function setDirection(InfoDirection $direction)
    {
        $this->direction = $direction;
        return $this;
    }

    /**
     * Get version direction
     *
     * @return InfoDirection
     */
    public function getDirection()
    {
        return $this->direction;
    }

    /**
     * @return int|null
     */
    public function getDirectionId()
    {
        return $this->direction ? $this->direction->getId() : null;
    }

    /**
     * Set IS_API
     *
     * @param $isAPI
     * @return $this
     */
    public function setIsAPI($isAPI)
    {
        $this->isAPI = $isAPI;
        return $this;
    }

    /**
     * Get IS_API
     *
     * @return int
     */
    public function getIsAPI()
    {
        return $this->isAPI;
    }

    /**
     * Is version API
     *
     * @return bool
     */
    public function IsAPI()
    {
        return !!$this->isAPI;
    }

    /**
     * Set IS_ARCHIVE
     *
     * @param $isArchive
     * @return $this
     */
    public function setIsArchive($isArchive)
    {
        $this->isArchive = $isArchive;
        return $this;
    }

    /**
     * Get IS_ARCHIVE
     *
     * @return int
     */
    public function getIsArchive()
    {
        return $this->isArchive;
    }

    /**
     * Is version ARCHIVE
     *
     * @return bool
     */
    public function IsArchive()
    {
        return !!$this->isArchive;
    }

    /**
     * Set IS_QLIKVIEW
     *
     * @param $isTargeting
     * @return $this
     */
    public function setisTargeting($isTargeting)
    {
        $this->isTargeting = $isTargeting;
        return $this;
    }

    /**
     * Get IS_QLIKVIEW
     *
     * @return int
     */
    public function getisTargeting()
    {
        return $this->isTargeting;
    }

    /**
     * Is version QLIKVIEW
     *
     * @return bool
     */
    public function isTargeting()
    {
        return !!$this->isTargeting;
    }

    /**
     * Set currenttime
     *
     * @param \DateTime $dateTime
     * @return $this
     */
    public function setCurrenttime(\DateTime $dateTime)
    {
        $this->currenttime = $dateTime;
        return $this;
    }

    /**
     * Get currenttime
     *
     * @return \DateTime
     */
    public function getCurrenttime()
    {
        return $this->currenttime;
    }
}
