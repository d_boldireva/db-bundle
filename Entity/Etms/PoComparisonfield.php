<?php

namespace TeamSoft\CrmRepositoryBundle\Entity\Etms;

use Doctrine\ORM\Mapping as ORM;

/**
 * PoComparisonfield
 *
 * @ORM\Table(name="po_comparisonfield")
 * @ORM\Entity
 */
class PoComparisonfield
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255, nullable=true)
     */
    private $name;

    /**
     * @var int
     *
     * @ORM\Column(name="enable", type="integer", nullable=true)
     */
    private $enable;

    /**
     * @var int
     *
     * @ORM\Column(name="is_histogram", type="integer", nullable=true)
     */
    private $isHistogram;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return PoComparisonfield
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set enable
     *
     * @param int $enable
     *
     * @return PoComparisonfield
     */
    public function setEnable($enable)
    {
        $this->enable = $enable;

        return $this;
    }

    /**
     * Get enable
     *
     * @return int
     */
    public function getEnable()
    {
        return $this->enable;
    }

    /**
     * Set isHistogram.
     *
     * @param int|null $isHistogram
     *
     * @return PoComparisonfield
     */
    public function setIsHistogram($isHistogram = null)
    {
        $this->isHistogram = $isHistogram;

        return $this;
    }

    /**
     * Get isHistogram.
     *
     * @return int|null
     */
    public function getIsHistogram()
    {
        return $this->isHistogram;
    }
}
