<?php

namespace TeamSoft\CrmRepositoryBundle\Entity\Etms;

use Doctrine\ORM\Mapping as ORM;
use TeamSoft\CrmRepositoryBundle\Entity\ServiceFieldInterface;

/**
 * InfoPolygonlayerdirectionparam
 *
 * @ORM\Table(name="info_polygonlayerdirectionparam")
 * @ORM\Entity
 */
class InfoPolygonlayerdirectionparam implements ServiceFieldInterface
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string|null
     *
     * @ORM\Column(name="param_name", type="string", length=255, nullable=true)
     */
    private $paramName;

    /**
     * @var string|null
     *
     * @ORM\Column(name="param_value", type="text", length=-1, nullable=true)
     */
    private $paramValue;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="currenttime", type="datetime", nullable=true)
     */
    private $currenttime;

    /**
     * @var string|null
     *
     * @ORM\Column(name="moduser", type="string", length=16, nullable=true)
     */
    private $moduser;

    /**
     * @var string|null
     *
     * @ORM\Column(name="guid", type="guid", nullable=true)
     */
    private $guid;

    /**
     * @var InfoPolygonlayerdirection
     *
     * @ORM\ManyToOne(targetEntity="InfoPolygonlayerdirection", inversedBy="params")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="polygonlayerdirection_id", referencedColumnName="id")
     * })
     */
    private $polygonlayerdirection;


    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set paramName.
     *
     * @param string|null $paramName
     *
     * @return InfoPolygonlayerdirectionparam
     */
    public function setParamName($paramName = null)
    {
        $this->paramName = $paramName;

        return $this;
    }

    /**
     * Get paramName.
     *
     * @return string|null
     */
    public function getParamName()
    {
        return $this->paramName;
    }

    /**
     * Set paramValue.
     *
     * @param string|null $paramValue
     *
     * @return InfoPolygonlayerdirectionparam
     */
    public function setParamValue($paramValue = null)
    {
        $this->paramValue = $paramValue;

        return $this;
    }

    /**
     * Get paramValue.
     *
     * @return string|null
     */
    public function getParamValue()
    {
        return $this->paramValue;
    }

    /**
     * Set currenttime.
     *
     * @param \DateTime|null $currenttime
     *
     * @return InfoPolygonlayerdirectionparam
     */
    public function setCurrenttime(\DateTime $currenttime)
    {
        $this->currenttime = $currenttime;

        return $this;
    }

    /**
     * Get currenttime.
     *
     * @return \DateTime|null
     */
    public function getCurrenttime()
    {
        return $this->currenttime;
    }

    /**
     * Set moduser.
     *
     * @param string|null $moduser
     *
     * @return InfoPolygonlayerdirectionparam
     */
    public function setModuser($moduser = null)
    {
        $this->moduser = $moduser;

        return $this;
    }

    /**
     * Get moduser.
     *
     * @return string|null
     */
    public function getModuser()
    {
        return $this->moduser;
    }

    /**
     * Set guid.
     *
     * @param string|null $guid
     *
     * @return InfoPolygonlayerdirectionparam
     */
    public function setGuid($guid = null)
    {
        $this->guid = $guid;

        return $this;
    }

    /**
     * Get guid.
     *
     * @return string|null
     */
    public function getGuid()
    {
        return $this->guid;
    }

    /**
     * Set polygonlayerdirection.
     *
     * @param InfoPolygonlayerdirection|null $polygonlayerdirection
     *
     * @return InfoPolygonlayerdirectionparam
     */
    public function setPolygonlayerdirection(InfoPolygonlayerdirection $polygonlayerdirection = null)
    {
        $this->polygonlayerdirection = $polygonlayerdirection;

        return $this;
    }

    /**
     * Get polygonlayerdirection.
     *
     * @return InfoPolygonlayerdirection|null
     */
    public function getPolygonlayerdirection()
    {
        return $this->polygonlayerdirection;
    }
}
