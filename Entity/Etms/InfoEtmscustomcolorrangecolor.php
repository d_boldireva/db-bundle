<?php

namespace TeamSoft\CrmRepositoryBundle\Entity\Etms;

use Doctrine\ORM\Mapping as ORM;

/**
 * Class InfoEtmscustomcolorrangecolor
 * @package TeamSoft\CrmRepositoryBundle\Entity\Etms
 *
 * @ORM\Table(name="info_etmscustomcolorrangecolor")
 * @ORM\Entity()
 */
class InfoEtmscustomcolorrangecolor
{
    /**
     * @var integer
     *
     * @ORM\GeneratedValue(strategy="IDENTITY")
     * @ORM\Column(name="id", type="integer", nullable=true)
     * @ORM\Id()
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="color", type="string", length=16, nullable=true)
     */
    private $color;

    /**
     * @var float
     *
     * @ORM\Column(name="top_bound", type="float", nullable=true)
     */
    private $top_bound;

    /**
     * @var InfoEtmscustomcolorrange
     *
     * @ORM\ManyToOne(targetEntity="TeamSoft\CrmRepositoryBundle\Entity\Etms\InfoEtmscustomcolorrange")
     * @ORM\JoinColumn(name="customcolorrange_id", referencedColumnName="id")
     */
    private $customcolorrange;

    /**
     * @var \DateTime|null
     * @ORM\Column(name="currenttime", type="datetime", nullable=true)
     */
    private $currenttime;
    /**
     * @var string|null
     * @ORM\Column(name="moduser", type="string", length=16, nullable=true)
     */
    private $moduser;
    /**
     * @var string|null
     * @ORM\Column(name="guid", type="guid", nullable=true)
     */
    private $guid;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param string $color
     * @return $this
     */
    public function setColor($color = '#ffffff')
    {
        $this->color = $color;
        return $this;
    }

    /**
     * @return string
     */
    public function getColor()
    {
        return $this->color;
    }

    /**
     * @param float $top_bound
     * @return $this
     */
    public function setTopBound($top_bound = 0.1)
    {
        $this->top_bound = $top_bound;
        return $this;
    }

    /**
     * @return float
     */
    public function getTopBound()
    {
        return $this->top_bound;
    }

    /**
     * @param InfoEtmscustomcolorrange $etmscustomcolorrange
     * @return $this
     */
    public function setCustomcolorrange(InfoEtmscustomcolorrange $etmscustomcolorrange)
    {
        $this->customcolorrange = $etmscustomcolorrange;
        return $this;
    }

    /**
     * @return InfoEtmscustomcolorrange
     */
    public function getCustomcolorrange()
    {
        return $this->customcolorrange;
    }

    /**
     * @param null $currenttime
     * @return $this
     */
    public function setCurrenttime($currenttime = null)
    {
        $this->currenttime = $currenttime;
        return $this;
    }

    /**
     * @return \DateTime|null
     */
    public function getCurrenttime()
    {
        return $this->currenttime;
    }

    /**
     * @param null $moduser
     * @return $this
     */
    public function setModuser($moduser = null)
    {
        $this->moduser = $moduser;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getModuser()
    {
        return $this->moduser;
    }

    /**
     * @param null $guid
     * @return $this
     */
    public function setGuid($guid = null)
    {
        $this->guid = $guid;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getGuid()
    {
        return $this->guid;
    }

    /**
     * @return int|null
     */
    public function getCustomColorRangeId()
    {
        return $this->customcolorrange ? $this->customcolorrange->getId() : null;
    }
}
