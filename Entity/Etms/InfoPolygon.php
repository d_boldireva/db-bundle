<?php

namespace TeamSoft\CrmRepositoryBundle\Entity\Etms;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Criteria;
use Doctrine\ORM\Mapping as ORM;
use TeamSoft\CrmRepositoryBundle\Entity\InfoCompany;
use TeamSoft\CrmRepositoryBundle\Entity\InfoDirection;
use TeamSoft\CrmRepositoryBundle\Entity\InfoRegion;
use TeamSoft\CrmRepositoryBundle\Entity\InfoUser;
use TeamSoft\CrmRepositoryBundle\Entity\ServiceFieldInterface;

/**
 * InfoPolygon
 *
 * @ORM\Table(name="info_polygon")
 * @ORM\Entity(repositoryClass="TeamSoft\CrmRepositoryBundle\Repository\Etms\InfoPolygon")
 */
class InfoPolygon implements ServiceFieldInterface
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255, nullable=true)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="color_hex", type="string", length=255, nullable=true)
     */
    private $color;

    /**
     * @var string
     *
     * @ORM\Column(name="encoded_polyline", type="text", length=-1, nullable=true)
     */
    private $encodedPolyline;

    /**
     * @var InfoUser
     *
     * @ORM\ManyToOne(targetEntity="TeamSoft\CrmRepositoryBundle\Entity\InfoUser")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="creator_id", referencedColumnName="id")
     * })
     */
    private $creator;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="currenttime", type="datetime", nullable=true)
     */
    private $currenttime;

    /**
     * @var string
     *
     * @ORM\Column(name="guid", type="guid", nullable=true)
     */
    private $guid;

    /**
     * @var string
     *
     * @ORM\Column(name="moduser", type="string", length=16, nullable=true)
     */
    private $moduser;

    /**
     * @var InfoUser
     *
     * @ORM\ManyToOne(targetEntity="TeamSoft\CrmRepositoryBundle\Entity\InfoUser", inversedBy="polygonCollection")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     * })
     */
    private $user;

    /**
     * @var float
     *
     * @ORM\Column(name="center_lat", type="float", precision=53, scale=0, nullable=true)
     */
    private $centerLat;

    /**
     * @var float
     *
     * @ORM\Column(name="center_lng", type="float", precision=53, scale=0, nullable=true)
     */
    private $centerLng;

    /**
     * @var int
     *
     * @ORM\Column(name="isWrongPolygon", type="integer", nullable=true)
     */
    private $iswrongpolygon;

    /**
     * @var float
     *
     * @ORM\Column(name="area", type="float", nullable=true)
     */
    private $area;

    /**
     * @var float
     *
     * @ORM\Column(name="perimeter", type="float", nullable=true)
     */
    private $perimeter;

    /**
     * @var InfoPolygoncompany
     *
     * @ORM\OneToMany(targetEntity="InfoPolygoncompany", mappedBy="polygon", fetch="EXTRA_LAZY")
     */
    private $companies;

    /**
     * @var InfoPolygonlayer
     *
     * @ORM\ManyToOne(targetEntity="InfoPolygonlayer")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="polygonlayer_id", referencedColumnName="id")
     * })
     */
    private $polygonLayer;

    /**
     * @var InfoDirection
     *
     * @ORM\ManyToOne(targetEntity="TeamSoft\CrmRepositoryBundle\Entity\InfoDirection")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="direction_id", referencedColumnName="id")
     * })
     */
    private $direction;

    /**
     * @var InfoCompany
     *
     * @ORM\ManyToOne(targetEntity="TeamSoft\CrmRepositoryBundle\Entity\InfoCompany")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="company_id", referencedColumnName="id")
     * })
     */
    private $company;

    /**
     * @var InfoRegion
     *
     * @ORM\ManyToOne(targetEntity="TeamSoft\CrmRepositoryBundle\Entity\InfoRegion")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="region_id", referencedColumnName="id")
     * })
     */
    private $region;

    /**
     * @var ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="InfoPolygonuser", mappedBy="polygon", cascade={"persist"})
     */
    private $polygonUser;

    /**
     * @var ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="InfoPolygonuserbalancing", mappedBy="polygon", cascade={"persist"})
     */
    private $polygonUserBalancing;

    /**
     * @var string
     *
     * @ORM\Column(name="polygon", type="polygon", nullable=true)
     */
    private $polygon;

    /**
     * @var string|null
     *
     * @ORM\Column(name="polygon_buffer", type="polygon", nullable=true)
     */
    private $polygonBuffer;

    /**
     * @var int
     *
     * @ORM\Column(name="isEnclave", type="integer", nullable=true)
     */
    private $isEnclave = '0';

    /**
     * @var int|null
     *
     * @ORM\Column(name="population", type="integer", nullable=true)
     */
    private $population;

    /**
     * @var int|null
     *
     * @ORM\Column(name="morionid", type="integer", nullable=true)
     */
    private $morionId;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return InfoPolygon
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set color
     *
     * @param string $color
     *
     * @return InfoPolygon
     */
    public function setColor($color)
    {
        $this->color = $color;

        return $this;
    }

    /**
     * Get color
     *
     * @return string
     */
    public function getColor()
    {
        return $this->color;
    }

    /**
     * Set encodedPolyline
     *
     * @param string $encodedPolyline
     *
     * @return InfoPolygon
     */
    public function setEncodedPolyline($encodedPolyline)
    {
        $this->encodedPolyline = $encodedPolyline;

        return $this;
    }

    /**
     * Get encodedPolyline
     *
     * @return string
     */
    public function getEncodedPolyline()
    {
        return $this->encodedPolyline;
    }

    /**
     * Set creator
     *
     * @param InfoUser $creator
     *
     * @return InfoPolygon
     */
    public function setCreator(InfoUser $creator)
    {
        $this->creator = $creator;

        return $this;
    }

    /**
     * Get creator
     *
     * @return InfoUser
     */
    public function getCreator()
    {
        return $this->creator;
    }

    /**
     * Set currenttime
     *
     * @param \DateTime $currenttime
     *
     * @return InfoPolygon
     */
    public function setCurrenttime(\DateTime $currenttime)
    {
        $this->currenttime = $currenttime;

        return $this;
    }

    /**
     * Get currenttime
     *
     * @return \DateTime
     */
    public function getCurrenttime()
    {
        return $this->currenttime;
    }

    /**
     * Set guid
     *
     * @param string $guid
     *
     * @return InfoPolygon
     */
    public function setGuid($guid)
    {
        $this->guid = $guid;

        return $this;
    }

    /**
     * Get guid
     *
     * @return string
     */
    public function getGuid()
    {
        return $this->guid;
    }

    /**
     * Set moduser
     *
     * @param string $moduser
     *
     * @return InfoPolygon
     */
    public function setModuser($moduser)
    {
        $this->moduser = $moduser;

        return $this;
    }

    /**
     * Get moduser
     *
     * @return string
     */
    public function getModuser()
    {
        return $this->moduser;
    }

    /**
     * Set user
     *
     * @param InfoUser $user
     *
     * @return InfoPolygon
     */
    public function setUser($user)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return InfoUser
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Set centerLat
     *
     * @param float $centerLat
     *
     * @return InfoPolygon
     */
    public function setCenterLat($centerLat)
    {
        $this->centerLat = $centerLat;

        return $this;
    }

    /**
     * Get centerLat
     *
     * @return float
     */
    public function getCenterLat()
    {
        return $this->centerLat;
    }

    /**
     * Set centerLng
     *
     * @param float $centerLng
     *
     * @return InfoPolygon
     */
    public function setCenterLng($centerLng)
    {
        $this->centerLng = $centerLng;

        return $this;
    }

    /**
     * Get centerLng
     *
     * @return float
     */
    public function getCenterLng()
    {
        return $this->centerLng;
    }

    /**
     * Set iswrongpolygon
     *
     * @param int $iswrongpolygon
     *
     * @return InfoPolygon
     */
    public function setIswrongpolygon($iswrongpolygon)
    {
        $this->iswrongpolygon = $iswrongpolygon;

        return $this;
    }

    /**
     * Get iswrongpolygon
     *
     * @return int
     */
    public function getIswrongpolygon()
    {
        return $this->iswrongpolygon;
    }

    public function getCompaniesCount()
    {
        $criteria = Criteria::create();
        $expr = Criteria::expr();

        $criteria->where($expr->isNull('isdeleted'));
        $criteria->orWhere($expr->eq('isdeleted', 0));

        $polygonCompany = $this->getCompanies()->matching($criteria);
        return $polygonCompany->count();
    }

    /**
     * Add company
     *
     * @param \TeamSoft\CrmRepositoryBundle\Entity\Etms\InfoPolygoncompany $company
     *
     * @return InfoPolygon
     */
    public function addCompany(InfoPolygoncompany $company)
    {
        $this->companies[] = $company;

        return $this;
    }

    /**
     * Remove company
     *
     * @param \TeamSoft\CrmRepositoryBundle\Entity\Etms\InfoPolygoncompany $company
     */
    public function removeCompany(InfoPolygoncompany $company)
    {
        $this->companies->removeElement($company);
    }

    /**
     * Get companies
     *
     * @return \Doctrine\Common\Collections\ArrayCollection
     */
    public function getCompanies()
    {
        return $this->companies;
    }

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->companies = new ArrayCollection();
    }

    /**
     * Set polygonLayer
     *
     * @param \TeamSoft\CrmRepositoryBundle\Entity\Etms\InfoPolygonlayer $polygonLayer
     *
     * @return InfoPolygon
     */
    public function setPolygonLayer(InfoPolygonlayer $polygonLayer = null)
    {
        $this->polygonLayer = $polygonLayer;

        return $this;
    }

    /**
     * Get polygonLayer
     *
     * @return \TeamSoft\CrmRepositoryBundle\Entity\Etms\InfoPolygonlayer
     */
    public function getPolygonLayer()
    {
        return $this->polygonLayer;
    }

    /**
     * Set direction
     *
     * @param \TeamSoft\CrmRepositoryBundle\Entity\InfoDirection $direction
     *
     * @return InfoPolygon
     */
    public function setDirection(InfoDirection $direction = null)
    {
        $this->direction = $direction;

        return $this;
    }

    /**
     * Get direction
     *
     * @return \TeamSoft\CrmRepositoryBundle\Entity\InfoDirection
     */
    public function getDirection()
    {
        return $this->direction;
    }

    /**
     * Set company
     *
     * @param \TeamSoft\CrmRepositoryBundle\Entity\InfoCompany $company
     *
     * @return InfoPolygon
     */
    public function setCompany(InfoCompany $company = null)
    {
        $this->company = $company;

        return $this;
    }

    /**
     * Get company
     *
     * @return \TeamSoft\CrmRepositoryBundle\Entity\InfoCompany
     */
    public function getCompany()
    {
        return $this->company;
    }

    /**
     * Set region
     *
     * @param \TeamSoft\CrmRepositoryBundle\Entity\InfoRegion $region
     *
     * @return InfoPolygon
     */
    public function setRegion(InfoRegion $region = null)
    {
        $this->region = $region;

        return $this;
    }

    /**
     * Get region
     *
     * @return \TeamSoft\CrmRepositoryBundle\Entity\InfoRegion
     */
    public function getRegion()
    {
        return $this->region;
    }

    /**
     * Add polygonUser
     *
     * @param \TeamSoft\CrmRepositoryBundle\Entity\Etms\InfoPolygonuser $polygonUser
     *
     * @return InfoPolygon
     */
    public function addPolygonUser(InfoPolygonuser $polygonUser)
    {
        $this->polygonUser[] = $polygonUser;

        return $this;
    }

    /**
     * Remove polygonUser
     *
     * @param \TeamSoft\CrmRepositoryBundle\Entity\Etms\InfoPolygonuser $polygonUser
     */
    public function removePolygonUser(InfoPolygonuser $polygonUser)
    {
        $this->polygonUser->removeElement($polygonUser);
    }

    /**
     * Get polygonUser
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getPolygonUser()
    {
        return $this->polygonUser;
    }

    /**
     * Set polygon.
     *
     * @param string|null $polygon
     *
     * @return InfoPolygon
     */
    public function setPolygon($polygon = null)
    {
        $this->polygon = $polygon;

        return $this;
    }

    /**
     * Get polygon.
     *
     * @return string|null
     */
    public function getPolygon()
    {
        return $this->polygon;
    }

    /**
     * Add polygonUserBalancing.
     *
     * @param \TeamSoft\CrmRepositoryBundle\Entity\Etms\InfoPolygonuserbalancing $polygonUserBalancing
     *
     * @return InfoPolygon
     */
    public function addPolygonUserBalancing(\TeamSoft\CrmRepositoryBundle\Entity\Etms\InfoPolygonuserbalancing $polygonUserBalancing)
    {
        $this->polygonUserBalancing[] = $polygonUserBalancing;

        return $this;
    }

    /**
     * Remove polygonUserBalancing.
     *
     * @param \TeamSoft\CrmRepositoryBundle\Entity\Etms\InfoPolygonuserbalancing $polygonUserBalancing
     *
     * @return boolean TRUE if this collection contained the specified element, FALSE otherwise.
     */
    public function removePolygonUserBalancing(\TeamSoft\CrmRepositoryBundle\Entity\Etms\InfoPolygonuserbalancing $polygonUserBalancing)
    {
        return $this->polygonUserBalancing->removeElement($polygonUserBalancing);
    }

    /**
     * Get polygonUserBalancing.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getPolygonUserBalancing()
    {
        return $this->polygonUserBalancing;
    }

    /**
     * @param float $area
     * @return $this
     */
    public function setArea($area = 0.0)
    {
        $this->area = $area;
        return $this;
    }

    /**
     * @return float
     */
    public function getArea()
    {
        return $this->area;
    }

    /**
     * @param float $perimeter
     * @return $this
     */
    public function setPerimeter($perimeter = 0.0)
    {
        $this->perimeter = $perimeter;
        return $this;
    }

    /**
     * @return float
     */
    public function getPerimeter()
    {
        return $this->perimeter;
    }

    /**
     * @param int $isEnclave
     * @return $this
     */
    public function setIsEnclave($isEnclave = 0)
    {
        $this->isEnclave = $isEnclave;
        return $this;
    }

    /**
     * @return int
     */
    public function getIsEnclave()
    {
        return $this->isEnclave;
    }

    /**
     * @return bool
     */
    public function IsEnclave()
    {
        return !!$this->isEnclave;
    }

    /**
     * @return int|null
     */
    public function getRegionId()
    {
        return $this->region ? $this->region->getId() : null;
    }

    /**
     * @return int|null
     */
    public function getDirectionId()
    {
        return $this->direction ? $this->direction->getId() : null;
    }

    /**
     * @return int|null
     */
    public function getCreatorId()
    {
        return $this->creator ? $this->creator->getId() : null;
    }

    /**
     * @return string|null
     */
    public function getPolygonBuffer(): ?string
    {
        return $this->polygonBuffer;
    }

    /**
     * @param string|null $polygonBuffer
     * @return self
     */
    public function setPolygonBuffer(?string $polygonBuffer): self
    {
        $this->polygonBuffer = $polygonBuffer;

        return $this;
    }

    /**
     * @return int|null
     */
    public function getPopulation(): ?int
    {
        return $this->population;
    }

    /**
     * @param int|null $population
     * @return self
     */
    public function setPopulation(?int $population): self
    {
        $this->population = $population;

        return $this;
    }

    /**
     * @return int|null
     */
    public function getMorionId(): ?int
    {
        return $this->morionId;
    }

    /**
     * @param int|null $morionId
     * @return self
     */
    public function setMorionId(?int $morionId): self
    {
        $this->morionId = $morionId;

        return $this;
    }

    /**
     * @return array
     */
    public function getGeoJSON()
    {
        return [
            'type' => 'Feature',
            'geometry' => [
                'type' => 'Polygon',
                'coordinates' => $this->polygon,
            ],
            'properties' => [
                'id' => $this->id,
                'name' => $this->name,
                'area' => $this->area,
                'perimeter' => $this->perimeter,
                'region_id' => $this->getRegionId(),
                'creator_id' => $this->getCreatorId(),
                'direction_id' => $this->getDirectionId(),
                'population' => $this->getPopulation(),
                'center' => [
                    'lat' => $this->getCenterLat(),
                    'lng' => $this->getCenterLng(),
                ],
            ],
        ];
    }
}
