<?php

namespace TeamSoft\CrmRepositoryBundle\Entity\Etms;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use TeamSoft\CrmRepositoryBundle\Entity\InfoDirection;
use TeamSoft\CrmRepositoryBundle\Entity\InfoUser;
use TeamSoft\CrmRepositoryBundle\Entity\ServiceFieldInterface;

/**
 * InfoEtmsbalancing
 *
 * @ORM\Table(name="info_etmsbalancing")
 * @ORM\Entity
 */
class InfoEtmsbalancing implements ServiceFieldInterface
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string|null
     *
     * @ORM\Column(name="morion_guid", type="string", length=255, nullable=true)
     */
    private $morionGuid;

    /**
     * @var int|null
     *
     * @ORM\Column(name="coef_compactness", type="integer", nullable=true)
     */
    private $coefCompactness;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="currenttime", type="datetime", nullable=true)
     */
    private $currenttime;

    /**
     * @var string|null
     *
     * @ORM\Column(name="moduser", type="string", length=16, nullable=true)
     */
    private $moduser;

    /**
     * @var string|null
     *
     * @ORM\Column(name="guid", type="guid", nullable=true)
     */
    private $guid;

    /**
     * @var int|null
     *
     * @ORM\Column(name="status_code", type="integer", nullable=true)
     */
    private $statusCode;

    /**
     * @var string|null
     *
     * @ORM\Column(name="status_msg", type="string", length=255, nullable=true)
     */
    private $statusMsg;

    /**
     * @var InfoUser
     *
     * @ORM\ManyToOne(targetEntity="TeamSoft\CrmRepositoryBundle\Entity\InfoUser")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="creator_id", referencedColumnName="id")
     * })
     */
    private $creator;

    /**
     * @var InfoDirection
     *
     * @ORM\ManyToOne(targetEntity="TeamSoft\CrmRepositoryBundle\Entity\InfoDirection")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="direction_id", referencedColumnName="id")
     * })
     */
    private $direction;

    /**
     * @var InfoEtmsmapsversion
     *
     * @ORM\ManyToOne(targetEntity="TeamSoft\CrmRepositoryBundle\Entity\Etms\InfoEtmsmapsversion")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="version_id", referencedColumnName="id")
     * })
     */
    private $version;

    /**
     * @var InfoPolygonlayer
     *
     * @ORM\ManyToOne(targetEntity="TeamSoft\CrmRepositoryBundle\Entity\Etms\InfoPolygonlayer")
     * @ORM\JoinColumn(name="polygonlayer_id", referencedColumnName="id")
     */
    private $polygonlayer;

    /**
     * @var InfoEtmsbalancingparam
     *
     * @ORM\OneToMany(targetEntity="InfoEtmsbalancingparam", mappedBy="balancing")
     */
    private $balancingParam;

    /**
     * @var int|null
     *
     * @ORM\Column(name="coef_turnover", type="integer", nullable=true)
     */
    private $coefTurnover;

    /**
     * @var int
     *
     * @ORM\Column(name="solution_id", type="integer", nullable=true)
     */
    private $solution;

    /**
     * @var InfoPolygonuserbalancing[]|ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="InfoPolygonuserbalancing", mappedBy="balancing", cascade={"remove","persist"})
     */
    private $polygonUserBalancingCollection;


    /**
     * InfoEtmsbalancing constructor.
     */
    public function __construct()
    {
        $this->balancingParam = new ArrayCollection();
        $this->polygonUserBalancingCollection = new ArrayCollection();
    }

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set morionGuid.
     *
     * @param string|null $morionGuid
     *
     * @return InfoEtmsbalancing
     */
    public function setMorionGuid($morionGuid = null)
    {
        $this->morionGuid = $morionGuid;

        return $this;
    }

    /**
     * Get morionGuid.
     *
     * @return string|null
     */
    public function getMorionGuid()
    {
        return $this->morionGuid;
    }

    /**
     * Set coefCompactness.
     *
     * @param int|null $coefCompactness
     *
     * @return InfoEtmsbalancing
     */
    public function setCoefCompactness($coefCompactness = null)
    {
        $this->coefCompactness = $coefCompactness;

        return $this;
    }

    /**
     * Get coefCompactness.
     *
     * @return int|null
     */
    public function getCoefCompactness()
    {
        return $this->coefCompactness;
    }

    /**
     * Set currenttime.
     *
     * @param \DateTime|null $currenttime
     *
     * @return InfoEtmsbalancing
     */
    public function setCurrenttime(\DateTime $currenttime = null)
    {
        $this->currenttime = $currenttime;

        return $this;
    }

    /**
     * Get currenttime.
     *
     * @return \DateTime|null
     */
    public function getCurrenttime()
    {
        return $this->currenttime;
    }

    /**
     * Set moduser.
     *
     * @param string|null $moduser
     *
     * @return InfoEtmsbalancing
     */
    public function setModuser($moduser = null)
    {
        $this->moduser = $moduser;

        return $this;
    }

    /**
     * Get moduser.
     *
     * @return string|null
     */
    public function getModuser()
    {
        return $this->moduser;
    }

    /**
     * Set guid.
     *
     * @param string|null $guid
     *
     * @return InfoEtmsbalancing
     */
    public function setGuid($guid = null)
    {
        $this->guid = $guid;

        return $this;
    }

    /**
     * Get guid.
     *
     * @return string|null
     */
    public function getGuid()
    {
        return $this->guid;
    }

    /**
     * Set statusCode.
     *
     * @param int|null $statusCode
     *
     * @return InfoEtmsbalancing
     */
    public function setStatusCode($statusCode = null)
    {
        $this->statusCode = $statusCode;

        return $this;
    }

    /**
     * Get statusCode.
     *
     * @return int|null
     */
    public function getStatusCode()
    {
        return $this->statusCode;
    }

    /**
     * Set statusMsg.
     *
     * @param string|null $statusMsg
     *
     * @return InfoEtmsbalancing
     */
    public function setStatusMsg($statusMsg = null)
    {
        $this->statusMsg = $statusMsg;

        return $this;
    }

    /**
     * Get statusMsg.
     *
     * @return string|null
     */
    public function getStatusMsg()
    {
        return $this->statusMsg;
    }

    /**
     * Set creator.
     *
     * @param InfoUser|null $creator
     *
     * @return InfoEtmsbalancing
     */
    public function setCreator(InfoUser $creator = null)
    {
        $this->creator = $creator;

        return $this;
    }

    /**
     * Get creator.
     *
     * @return InfoUser|null
     */
    public function getCreator()
    {
        return $this->creator;
    }

    /**
     * Set direction.
     *
     * @param InfoDirection|null $direction
     *
     * @return InfoEtmsbalancing
     */
    public function setDirection(InfoDirection $direction = null)
    {
        $this->direction = $direction;

        return $this;
    }

    /**
     * Get direction.
     *
     * @return InfoDirection|null
     */
    public function getDirection()
    {
        return $this->direction;
    }

    /**
     * Set polygonlayer
     *
     * @param InfoPolygonlayer|null $polygonlayer
     * @return $this
     */
    public function setPolygonlayer(InfoPolygonlayer $polygonlayer = null)
    {
        $this->polygonlayer = $polygonlayer;
        return $this;
    }

    /**
     * Get polygonlayer
     *
     * @return InfoPolygonlayer
     */
    public function getPolygonlayer()
    {
        return $this->polygonlayer;
    }

    /**
     * Get polygonlayer id
     *
     * @return int|null
     */
    public function getPolygonlayerId()
    {
        return $this->polygonlayer ? $this->polygonlayer->getId() : null;
    }

    /**
     * Get polygonlayer tag
     *
     * @return string|null
     */
    public function getPolygonlayerTag()
    {
        return $this->polygonlayer ? $this->polygonlayer->getTag() : null;
    }

    /**
     * Add balancingParam.
     *
     * @param InfoEtmsbalancingparam $balancingParam
     *
     * @return InfoEtmsbalancing
     */
    public function addBalancingParam(InfoEtmsbalancingparam $balancingParam)
    {
        $this->balancingParam[] = $balancingParam;

        return $this;
    }

    /**
     * Remove balancingParam.
     *
     * @param InfoEtmsbalancingparam $balancingParam
     *
     * @return boolean TRUE if this collection contained the specified element, FALSE otherwise.
     */
    public function removeBalancingParam(InfoEtmsbalancingparam $balancingParam)
    {
        return $this->balancingParam->removeElement($balancingParam);
    }

    /**
     * Get balancingParam.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getBalancingParam()
    {
        return $this->balancingParam;
    }

    /**
     * Set coefTurnover.
     *
     * @param int|null $coefTurnover
     *
     * @return InfoEtmsbalancing
     */
    public function setCoefTurnover($coefTurnover = null)
    {
        $this->coefTurnover = $coefTurnover;

        return $this;
    }

    /**
     * Get coefTurnover.
     *
     * @return int|null
     */
    public function getCoefTurnover()
    {
        return $this->coefTurnover;
    }

    /**
     * Set version
     *
     * @param InfoEtmsmapsversion $version
     * @return $this
     */
    public function setVersion(InfoEtmsmapsversion $version)
    {
        $this->version = $version;
        return $this;
    }

    /**
     * Get version
     *
     * @return InfoEtmsmapsversion
     */
    public function getVersion()
    {
        return $this->version;
    }

    /**
     * @return int
     */
    public function getSolution()
    {
        return $this->solution;
    }

    /**
     * @param int $solution
     * @return $this
     */
    public function setSolution($solution = 0)
    {
        $this->solution = $solution;

        return $this;
    }

    /**
     * @return int
     */
    public function getVersionId()
    {
        return $this->version ? $this->version->getId() : null;
    }

    /**
     * @return int|null
     */
    public function getDirectionId()
    {
        $directionId = $this->version ? $this->version->getDirectionId() : null;

        if (!$directionId) {
            $directionId = $this->direction ? $this->direction->getId() : null;
        }

        return $directionId;
    }

    /**
     * @return array
     */
    public function getPolygonUserBalancingCollection()
    {
        return $this->polygonUserBalancingCollection->toArray();
    }

    /**
     * @param InfoPolygonuserbalancing $polygonuserbalancing
     * @return $this
     */
    public function addPolygonUserBalancingCollection(InfoPolygonuserbalancing $polygonuserbalancing)
    {
        if (!$this->polygonUserBalancingCollection->contains($polygonuserbalancing)) {
            $polygonuserbalancing->setBalancing($this);
            $this->polygonUserBalancingCollection->add($polygonuserbalancing);
        }

        return $this;
    }

    /**
     * @param InfoPolygonuserbalancing $polygonuserbalancing
     * @return $this
     */
    public function removePolygonUserBalancingCollection(InfoPolygonuserbalancing $polygonuserbalancing)
    {
        if ($this->polygonUserBalancingCollection->contains($polygonuserbalancing)) {
            $this->polygonUserBalancingCollection->removeElement($polygonuserbalancing);
            $polygonuserbalancing->setBalancing(null);
        }

        return $this;
    }

    public function clearPolygonUserBalancingCollection()
    {
        $this->polygonUserBalancingCollection = new ArrayCollection();
    }
}
