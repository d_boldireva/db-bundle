<?php

namespace TeamSoft\CrmRepositoryBundle\Entity\Etms;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use TeamSoft\CrmRepositoryBundle\Entity\InfoUser;
use TeamSoft\CrmRepositoryBundle\Entity\ServiceFieldInterface;

/**
 * Class InfoEtmsrepcluster
 * @package TeamSoft\CrmRepositoryBundle\Entity\Etms
 *
 * @ORM\Table(name="info_etmsrepcluster", indexes={@ORM\Index(name="ix_info_etmsrepcluster_owner_id", columns={"owner_id"})})
 * @ORM\Entity()
 */
class InfoEtmsrepcluster implements ServiceFieldInterface
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id()
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255, nullable=true)
     */
    private $name;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created", type="datetime", nullable=true)
     */
    private $created;

    /**
     * @var int
     *
     * @ORM\Column(name="company_visits", type="integer", nullable=true)
     */
    private $companyVisits;

    /**
     * @var int
     *
     * @ORM\Column(name="contact_visits", type="integer", nullable=true)
     */
    private $contactVisits;

    /**
     * @var InfoUser
     *
     *
     * @ORM\ManyToOne(targetEntity="TeamSoft\CrmRepositoryBundle\Entity\InfoUser")
     * @ORM\JoinColumn(name="owner_id", referencedColumnName="id", onDelete="SET NULL")
     */
    private $owner;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="currenttime", type="datetime", nullable=true)
     */
    private $currenttime;

    /**
     * @var string
     *
     * @ORM\Column(name="moduser", type="string", length=16, nullable=true)
     */
    private $moduser;

    /**
     * @var string
     *
     * @ORM\Column(name="guid", type="guid", nullable=true)
     */
    private $guid;

    /**
     * @var ArrayCollection|InfoEtmsrepclusterCluster[]
     *
     * @ORM\OneToMany(targetEntity="InfoEtmsrepclusterCluster", mappedBy="repcluster", cascade={"persist", "remove"})
     */
    private $clusterCollection;

    /**
     * @var ArrayCollection|InfoEtmsrepclusterCompanytype[]
     *
     * @ORM\OneToMany(targetEntity="InfoEtmsrepclusterCompanytype", mappedBy="repcluster", cascade={"persist", "remove"})
     */
    private $companyTypeCollection;

    /**
     * @var ArrayCollection|InfoEtmsrepclusterContactspec[]
     *
     * @ORM\OneToMany(targetEntity="InfoEtmsrepclusterContactspec", mappedBy="repcluster", cascade={"persist", "remove"})
     */
    private $contactSpecCollection;


    /**
     * @return \DateTime|string
     */
    public function getCurrenttime()
    {
        return $this->currenttime;
    }

    /**
     * @param \DateTime $value
     * @return $this|ServiceFieldInterface
     */
    public function setCurrenttime(\DateTime $value)
    {
        $this->currenttime = $value;
        return $this;
    }

    /**
     * @return int|string
     */
    public function getGuid()
    {
        return $this->guid;
    }

    /**
     * @param int $value
     * @return $this|ServiceFieldInterface
     */
    public function setGuid($value)
    {
        $this->guid = $value;
        return $this;
    }

    /**
     * @return string
     */
    public function getModuser()
    {
        return $this->moduser;
    }

    /**
     * @param string $value
     * @return $this|ServiceFieldInterface
     */
    public function setModuser($value)
    {
        $this->moduser = $value;
        return $this;
    }


    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     * @return $this
     */
    public function setName($name)
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * @param \DateTime $created
     * @return $this
     */
    public function setCreated(\DateTime $created)
    {
        $this->created = $created;
        return $this;
    }

    /**
     * @return int
     */
    public function getCompanyVisists()
    {
        return $this->companyVisits;
    }

    /**
     * @param int $value
     * @return $this
     */
    public function setCompanyVisits($value)
    {
        $this->companyVisits = $value;
        return $this;
    }

    /**
     * @return int
     */
    public function getContactVisits()
    {
        return $this->contactVisits;
    }

    /**
     * @param int $value
     * @return $this
     */
    public function setContactVisits($value)
    {
        $this->contactVisits = $value;
        return $this;
    }

    /**
     * @return InfoUser
     */
    public function getOwner()
    {
        return $this->owner;
    }

    /**
     * @param InfoUser $user
     * @return $this
     */
    public function setOwner(InfoUser $user)
    {
        $this->owner = $user;
        return $this;
    }

    /**
     * @return int|null
     */
    public function getOwnerId()
    {
        return $this->owner ? $this->owner->getId() : null;
    }

    /**
     * @return ArrayCollection|InfoEtmsrepclusterCluster[]
     */
    public function getClusterCollection()
    {
        return $this->clusterCollection;
    }

    /**
     * @param InfoEtmsrepclusterCluster $clusterCollection
     * @return $this
     */
    public function addClusterCollection(InfoEtmsrepclusterCluster $cluster)
    {
        if (!$this->clusterCollection->contains($cluster)) {
            $this->clusterCollection->add($cluster);
        }

        $cluster->setRepCluster($this);

        return $this;
    }

    /**
     * @param InfoEtmsrepclusterCluster $cluster
     * @return $this
     */
    public function removeClusterCollection(InfoEtmsrepclusterCluster $cluster)
    {
        if ($this->clusterCollection->contains($cluster)) {
            $this->clusterCollection->removeElement($cluster);
        }

        $cluster->setRepCluster(null);

        return $this;
    }

    /**
     * @return ArrayCollection|InfoEtmsrepclusterCompanytype[]
     */
    public function getCompanyTypeCollection()
    {
        return $this->companyTypeCollection;
    }

    /**
     * @param InfoEtmsrepclusterCompanytype $companyType
     * @return $this
     */
    public function addCompanyTypeCollection(InfoEtmsrepclusterCompanytype $companyType)
    {
        if (!$this->companyTypeCollection->contains($companyType)) {
            $this->companyTypeCollection->add($companyType);
        }

        $companyType->setRepCluster($this);

        return $this;
    }

    /**
     * @param InfoEtmsrepclusterCompanytype $companyType
     * @return $this
     */
    public function removeCompanyTypeCollection(InfoEtmsrepclusterCompanytype $companyType)
    {
        if ($this->companyTypeCollection->contains($companyType)) {
            $this->companyTypeCollection->removeElement($companyType);
        }

        $companyType->setRepCluster(null);

        return $this;
    }

    /**
     * @return ArrayCollection|InfoEtmsrepclusterContactspec[]
     */
    public function getContactSpecCollection()
    {
        return $this->contactSpecCollection;
    }

    /**
     * @param InfoEtmsrepclusterContactspec $contactSpecCollection
     * @return $this
     */
    public function addContactSpecCollection(InfoEtmsrepclusterContactspec $contactSpec)
    {
        if (!$this->contactSpecCollection->contains($contactSpec)) {
            $this->contactSpecCollection->add($contactSpec);
        }

        $contactSpec->setRepCluster($this);

        return $this;
    }

    /**
     * @param InfoEtmsrepclusterContactspec $contactSpecCollection
     * @return $this
     */
    public function removeContactSpecCollection(InfoEtmsrepclusterContactspec $contactSpec)
    {
        if (!$this->contactSpecCollection->contains($contactSpec)) {
            $this->contactSpecCollection->removeElement($contactSpec);
        }

        $contactSpec->setRepCluster(null);

        return $this;
    }
}
