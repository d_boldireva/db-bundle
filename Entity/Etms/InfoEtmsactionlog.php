<?php

namespace TeamSoft\CrmRepositoryBundle\Entity\Etms;

use Doctrine\ORM\Mapping as ORM;
use TeamSoft\CrmRepositoryBundle\Entity\InfoUser;
use TeamSoft\CrmRepositoryBundle\Entity\ServiceFieldInterface;

/**
 * Class InfoEtmsactionlog
 * @package TeamSoft\CrmRepositoryBundle\Entity\Etms
 *
 * @ORM\Table(name="info_etmsactionlog", uniqueConstraints={@ORM\UniqueConstraint(name="uq_info_etmsactionlog_batch_id_polygon_id", columns={"batch_id","polygon_id"})})
 * @ORM\Entity(repositoryClass="TeamSoft\CrmRepositoryBundle\Repository\Etms\InfoEtmsactionlog")
 */
class InfoEtmsactionlog implements ServiceFieldInterface
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id()
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var InfoEtmsactionlogbatch
     *
     * @ORM\ManyToOne(targetEntity="InfoEtmsactionlogbatch", inversedBy="actionLogCollection")
     * @ORM\JoinColumn(name="batch_id", referencedColumnName="id", onDelete="CASCADE")
     */
    private $batch;

    /**
     * @var InfoPolygon
     *
     * @ORM\ManyToOne(targetEntity="InfoPolygon")
     * @ORM\JoinColumn(name="polygon_id", referencedColumnName="id", onDelete="CASCADE")
     */
    private $polygon;

    /**
     * @var InfoEtmsactionlogprev
     *
     * @ORM\OneToOne(targetEntity="InfoEtmsactionlogprev", mappedBy="action", cascade={"persist", "remove"})
     */
    private $prev;

    /**
     * @var InfoEtmsactionlognew
     *
     * @ORM\OneToOne(targetEntity="InfoEtmsactionlognew", mappedBy="action", cascade={"persist", "remove"})
     */
    private $new;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="currenttime", type="datetime", nullable=true)
     */
    private $currenttime;

    /**
     * @var string|null
     *
     * @ORM\Column(name="moduser", type="string", length=16, nullable=true)
     */
    private $moduser;

    /**
     * @var string
     *
     * @ORM\Column(name="guid", type="guid", nullable=true)
     */
    private $guid;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param InfoEtmsactionlogbatch|null $batch
     * @return $this
     */
    public function setBatch(?InfoEtmsactionlogbatch $batch)
    {
        $this->batch = $batch;
        return $this;
    }

    /**
     * @return InfoEtmsactionlogbatch
     */
    public function getBatch()
    {
        return $this->batch;
    }

    /**
     * @param InfoPolygon|null $polygon
     * @return $this
     */
    public function setPolygon(?InfoPolygon $polygon)
    {
        $this->polygon = $polygon;
        return $this;
    }

    /**
     * @return InfoPolygon
     */
    public function getPolygon()
    {
        return $this->polygon;
    }

    /**
     * @param InfoEtmsactionlogprev|null $prev
     * @return $this
     */
    public function setPrev(?InfoEtmsactionlogprev $prev)
    {
        $prev->setAction($this);
        $this->prev = $prev;
        return $this;
    }

    /**
     * @return InfoEtmsactionlogprev
     */
    public function getPrev()
    {
        return $this->prev;
    }

    /**
     * @param InfoEtmsactionlognew|null $new
     * @return $this
     */
    public function setNew(?InfoEtmsactionlognew $new)
    {
        $new->setAction($this);
        $this->new = $new;
        return $this;
    }

    /**
     * @return InfoEtmsactionlognew
     */
    public function getNew()
    {
        return $this->new;
    }

    /**
     * @param \DateTime $currenttime
     * @return $this|ServiceFieldInterface
     */
    public function setCurrenttime(\DateTime $currenttime)
    {
        $this->currenttime = $currenttime;
        return $this;
    }

    /**
     * @return \DateTime|string|null
     */
    public function getCurrenttime()
    {
        return $this->currenttime;
    }

    /**
     * @param string $moduser
     * @return $this|ServiceFieldInterface
     */
    public function setModuser($moduser)
    {
        $this->moduser = $moduser;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getModuser()
    {
        return $this->moduser;
    }

    /**
     * @param int $guid
     * @return $this|ServiceFieldInterface
     */
    public function setGuid($guid)
    {
        $this->guid = $guid;
        return $this;
    }

    /**
     * @return int|string
     */
    public function getGuid()
    {
        return $this->guid;
    }

    /**
     * @param InfoUser|null $user
     * @return $this
     */
    public function setPrevUser(?InfoUser $user)
    {
        if (!$this->prev) {
            $this->prev = new InfoEtmsactionlogprev();
            $this->prev->setAction($this);
        }

        $this->prev->setUser($user);

        return $this;
    }

    /**
     * @return InfoUser|null
     */
    public function getPrevUser()
    {
        return $this->prev ? $this->prev->getUser() : null;
    }

    /**
     * @param InfoUser|null $user
     * @return $this
     */
    public function setNewUser(?InfoUser $user)
    {
        if (!$this->new) {
            $this->new = new InfoEtmsactionlognew();
            $this->new->setAction($this);
        }

        $this->new->setUser($user);

        return $this;
    }

    /**
     * @return InfoUser|null
     */
    public function getNewUser()
    {
        return $this->new ? $this->new->getUser() : null;
    }

    /**
     * @return int|null
     */
    public function getBatchId()
    {
        return $this->batch ? $this->batch->getId() : null;
    }

    /**
     * @return int|null
     */
    public function getPolygonId()
    {
        return $this->polygon ? $this->polygon->getId() : null;
    }

    /**
     * @return int|null
     */
    public function getPrevUserId()
    {
        return $this->prev ? $this->prev->getUserId() : null;
    }

    /**
     * @return int|null
     */
    public function getNewUserId()
    {
        return $this->new ? $this->new->getUserId() : null;
    }
}
