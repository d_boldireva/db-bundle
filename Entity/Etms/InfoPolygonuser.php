<?php

namespace TeamSoft\CrmRepositoryBundle\Entity\Etms;

use Doctrine\ORM\Mapping as ORM;
use TeamSoft\CrmRepositoryBundle\Entity\InfoDirection;
use TeamSoft\CrmRepositoryBundle\Entity\InfoUser;
use TeamSoft\CrmRepositoryBundle\Entity\ServiceFieldInterface;

/**
 * InfoPolygonuser
 *
 * @ORM\Table(name="info_polygonuser")
 * @ORM\Entity(repositoryClass="\TeamSoft\CrmRepositoryBundle\Repository\Etms\InfoPolygonuser")
 */
class InfoPolygonuser implements ServiceFieldInterface
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="currenttime", type="datetime", nullable=true)
     */
    private $currenttime;

    /**
     * @var string
     *
     * @ORM\Column(name="guid", type="guid", nullable=true)
     */
    private $guid;

    /**
     * @var string
     *
     * @ORM\Column(name="moduser", type="string", length=16, nullable=true)
     */
    private $moduser;

    /**
     * @var InfoPolygon
     *
     * @ORM\ManyToOne(targetEntity="InfoPolygon", inversedBy="polygonUser")
     * @ORM\JoinColumn(name="polygon_id", referencedColumnName="id")
     */
    private $polygon;

    /**
     * @var InfoUser
     *
     * @ORM\ManyToOne(targetEntity="TeamSoft\CrmRepositoryBundle\Entity\InfoUser")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     */
    private $user;

    /**
     * @var InfoDirection
     *
     * @ORM\ManyToOne(targetEntity="TeamSoft\CrmRepositoryBundle\Entity\InfoDirection")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="direction_id", referencedColumnName="id")
     * })
     */
    private $direction;

    /**
     * @var InfoEtmsmapsversion
     *
     * @ORM\ManyToOne(targetEntity="TeamSoft\CrmRepositoryBundle\Entity\Etms\InfoEtmsmapsversion")
     * @ORM\JoinColumn(name="version_id", referencedColumnName="id")
     */
    private $version;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set currenttime
     *
     * @param \DateTime $currenttime
     *
     * @return InfoPolygonuser
     */
    public function setCurrenttime(\DateTime $currenttime)
    {
        $this->currenttime = $currenttime;

        return $this;
    }

    /**
     * Get currenttime
     *
     * @return \DateTime
     */
    public function getCurrenttime()
    {
        return $this->currenttime;
    }

    /**
     * Set guid
     *
     * @param string $guid
     *
     * @return InfoPolygonuser
     */
    public function setGuid($guid)
    {
        $this->guid = $guid;

        return $this;
    }

    /**
     * Get guid
     *
     * @return string
     */
    public function getGuid()
    {
        return $this->guid;
    }

    /**
     * Set moduser
     *
     * @param string $moduser
     *
     * @return InfoPolygonuser
     */
    public function setModuser($moduser)
    {
        $this->moduser = $moduser;

        return $this;
    }

    /**
     * Get moduser
     *
     * @return string
     */
    public function getModuser()
    {
        return $this->moduser;
    }

    /**
     * Set polygon
     *
     * @param \TeamSoft\CrmRepositoryBundle\Entity\Etms\InfoPolygon $polygon
     *
     * @return InfoPolygonuser
     */
    public function setPolygon(InfoPolygon $polygon = null)
    {
        $this->polygon = $polygon;

        return $this;
    }

    /**
     * Get polygon
     *
     * @return \TeamSoft\CrmRepositoryBundle\Entity\Etms\InfoPolygon
     */
    public function getPolygon()
    {
        return $this->polygon;
    }

    /**
     * Set user
     *
     * @param \TeamSoft\CrmRepositoryBundle\Entity\InfoUser $user
     *
     * @return InfoPolygonuser
     */
    public function setUser(InfoUser $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \TeamSoft\CrmRepositoryBundle\Entity\InfoUser
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Set direction
     *
     * @param \TeamSoft\CrmRepositoryBundle\Entity\InfoDirection $direction
     *
     * @return InfoPolygonuser
     */
    public function setDirection(\TeamSoft\CrmRepositoryBundle\Entity\InfoDirection $direction = null)
    {
        $this->direction = $direction;

        return $this;
    }

    /**
     * Get direction
     *
     * @return \TeamSoft\CrmRepositoryBundle\Entity\InfoDirection
     */
    public function getDirection()
    {
        return $this->direction;
    }

    /**
     * Set version
     *
     * @param InfoEtmsmapsversion $version
     * @return $this
     */
    public function setVersion(InfoEtmsmapsversion $version)
    {
        $this->version = $version;
        return $this;
    }

    /**
     * Get version
     *
     * @return InfoEtmsmapsversion
     */
    public function getVersion()
    {
        return $this->version;
    }

    /**
     * @return int|null
     */
    public function getPolygonId()
    {
        return $this->polygon ? $this->polygon->getId() : null;
    }

    /**
     * @return int|null
     */
    public function getUserId()
    {
        return $this->user ? $this->user->getId() : null;
    }

    /**
     * @return int|null
     */
    public function getVersionId()
    {
        return $this->version ? $this->version->getId() : null;
    }

    /**
     * @return int|null
     */
    public function getDirectionId()
    {
        return $this->direction ? $this->direction->getId() : null;
    }
}
