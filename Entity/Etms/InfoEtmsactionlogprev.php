<?php

namespace TeamSoft\CrmRepositoryBundle\Entity\Etms;

use Doctrine\ORM\Mapping as ORM;
use TeamSoft\CrmRepositoryBundle\Entity\InfoUser;
use TeamSoft\CrmRepositoryBundle\Entity\ServiceFieldInterface;

/**
 * Class InfoEtmsactionlogprev
 * @package TeamSoft\CrmRepositoryBundle\Entity\Etms
 *
 * @ORM\Table(name="info_etmsactionlogprev", uniqueConstraints={@ORM\UniqueConstraint(name="uq_info_etmsactionlogprev_action_id_user_id", columns={"action_id","user_id"})})
 * @ORM\Entity()
 */
class InfoEtmsactionlogprev implements ServiceFieldInterface
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id()
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var InfoEtmsactionlog
     *
     * @ORM\OneToOne(targetEntity="InfoEtmsactionlog", inversedBy="prev")
     * @ORM\JoinColumn(name="action_id", referencedColumnName="id", onDelete="CASCADE")
     */
    private $action;

    /**
     * @var InfoUser
     *
     * @ORM\ManyToOne(targetEntity="TeamSoft\CrmRepositoryBundle\Entity\InfoUser")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id", onDelete="CASCADE")
     */
    private $user;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="currenttime", type="datetime", nullable=true)
     */
    private $currenttime;

    /**
     * @var string|null
     *
     * @ORM\Column(name="moduser", type="string", length=16, nullable=true)
     */
    private $moduser;

    /**
     * @var string
     *
     * @ORM\Column(name="guid", type="guid", nullable=true)
     */
    private $guid;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param InfoEtmsactionlog|null $action
     * @return $this
     */
    public function setAction(?InfoEtmsactionlog $action)
    {
        $this->action = $action;
        return $this;
    }

    /**
     * @return InfoEtmsactionlog
     */
    public function getAction()
    {
        return $this->action;
    }

    /**
     * @param InfoUser|null $user
     * @return $this
     */
    public function setUser(?InfoUser $user)
    {
        $this->user = $user;
        return $this;
    }

    /**
     * @return InfoUser
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @param \DateTime $currenttime
     * @return $this|ServiceFieldInterface
     */
    public function setCurrenttime(\DateTime $currenttime)
    {
        $this->currenttime = $currenttime;
        return $this;
    }

    /**
     * @return \DateTime|string|null
     */
    public function getCurrenttime()
    {
        return $this->currenttime;
    }

    /**
     * @param string $moduser
     * @return $this|ServiceFieldInterface
     */
    public function setModuser($moduser)
    {
        $this->moduser = $moduser;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getModuser()
    {
        return $this->moduser;
    }

    /**
     * @param int $guid
     * @return $this|ServiceFieldInterface
     */
    public function setGuid($guid)
    {
        $this->guid = $guid;
        return $this;
    }

    /**
     * @return int|string
     */
    public function getGuid()
    {
        return $this->guid;
    }

    /**
     * @return int|null
     */
    public function getActionId()
    {
        return $this->action ? $this->action->getId() : null;
    }

    /**
     * @return int|null
     */
    public function getUserId()
    {
        return $this->user ? $this->user->getId() : null;
    }
}
