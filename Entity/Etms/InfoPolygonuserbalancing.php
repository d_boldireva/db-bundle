<?php

namespace TeamSoft\CrmRepositoryBundle\Entity\Etms;

use Doctrine\ORM\Mapping as ORM;
use TeamSoft\CrmRepositoryBundle\Entity\InfoUser;
use TeamSoft\CrmRepositoryBundle\Entity\ServiceFieldInterface;

/**
 * InfoPolygonuserbalancing
 *
 * @ORM\Table(name="info_polygonuserbalancing")
 * @ORM\Entity(repositoryClass="\TeamSoft\CrmRepositoryBundle\Repository\Etms\InfoPolygonuserbalancing")
 */
class InfoPolygonuserbalancing implements ServiceFieldInterface
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var int|null
     *
     * @ORM\Column(name="solution_id", type="integer", nullable=true)
     */
    private $solutionId;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="currenttime", type="datetime", nullable=true)
     */
    private $currenttime;

    /**
     * @var string|null
     *
     * @ORM\Column(name="moduser", type="string", length=16, nullable=true)
     */
    private $moduser;

    /**
     * @var string|null
     *
     * @ORM\Column(name="guid", type="guid", nullable=true)
     */
    private $guid;

    /**
     * @var InfoPolygon
     *
     * @ORM\ManyToOne(targetEntity="InfoPolygon", inversedBy="polygonUserBalancing")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="polygon_id", referencedColumnName="id")
     * })
     */
    private $polygon;

    /**
     * @var InfoUser
     *
     * @ORM\ManyToOne(targetEntity="TeamSoft\CrmRepositoryBundle\Entity\InfoUser")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     * })
     */
    private $user;

    /**
     * @var InfoEtmsbalancing
     *
     * @ORM\ManyToOne(targetEntity="InfoEtmsbalancing", inversedBy="polygonUserBalancingCollection")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="balancing_id", referencedColumnName="id")
     * })
     */
    private $balancing;

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set solutionId.
     *
     * @param int|null $solutionId
     *
     * @return InfoPolygonuserbalancing
     */
    public function setSolutionId($solutionId = null)
    {
        $this->solutionId = $solutionId;

        return $this;
    }

    /**
     * Get solutionId.
     *
     * @return int|null
     */
    public function getSolutionId()
    {
        return $this->solutionId;
    }

    /**
     * Set currenttime.
     *
     * @param \DateTime|null $currenttime
     *
     * @return InfoPolygonuserbalancing
     */
    public function setCurrenttime(\DateTime $currenttime = null)
    {
        $this->currenttime = $currenttime;

        return $this;
    }

    /**
     * Get currenttime.
     *
     * @return \DateTime|null
     */
    public function getCurrenttime()
    {
        return $this->currenttime;
    }

    /**
     * Set moduser.
     *
     * @param string|null $moduser
     *
     * @return InfoPolygonuserbalancing
     */
    public function setModuser($moduser = null)
    {
        $this->moduser = $moduser;

        return $this;
    }

    /**
     * Get moduser.
     *
     * @return string|null
     */
    public function getModuser()
    {
        return $this->moduser;
    }

    /**
     * Set guid.
     *
     * @param string|null $guid
     *
     * @return InfoPolygonuserbalancing
     */
    public function setGuid($guid = null)
    {
        $this->guid = $guid;

        return $this;
    }

    /**
     * Get guid.
     *
     * @return string|null
     */
    public function getGuid()
    {
        return $this->guid;
    }

    /**
     * Set polygon.
     *
     * @param InfoPolygon|null $polygon
     *
     * @return InfoPolygonuserbalancing
     */
    public function setPolygon(InfoPolygon $polygon = null)
    {
        $this->polygon = $polygon;

        return $this;
    }

    /**
     * Get polygon.
     *
     * @return InfoPolygon|null
     */
    public function getPolygon()
    {
        return $this->polygon;
    }

    /**
     * Set user.
     *
     * @param InfoUser|null $user
     *
     * @return InfoPolygonuserbalancing
     */
    public function setUser(InfoUser $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user.
     *
     * @return InfoUser|null
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Set balancing.
     *
     * @param InfoEtmsbalancing|null $balancing
     *
     * @return InfoPolygonuserbalancing
     */
    public function setBalancing(InfoEtmsbalancing $balancing = null)
    {
        $this->balancing = $balancing;

        return $this;
    }

    /**
     * Get balancing.
     *
     * @return InfoEtmsbalancing|null
     */
    public function getBalancing()
    {
        return $this->balancing;
    }

    /**
     * @return int|null
     */
    public function getPolygonId()
    {
        return $this->polygon ? $this->polygon->getId() : null;
    }

    /**
     * @return int|null
     */
    public function getUserId()
    {
        return $this->user ? $this->user->getId() : null;
    }

    /**
     * @return int|null
     */
    public function getBalancingId()
    {
        return $this->balancing ? $this->balancing->getId() : null;
    }

    /**
     * @return int|null
     */
    public function getVersionId()
    {
        return $this->balancing ? $this->balancing->getVersionId() : null;
    }

    /**
     * @return int|null
     */
    public function getDIrectionId()
    {
        return $this->balancing ? $this->balancing->getDirectionId() : null;
    }
}
