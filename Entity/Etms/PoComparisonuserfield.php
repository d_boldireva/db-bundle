<?php

namespace TeamSoft\CrmRepositoryBundle\Entity\Etms;

use Doctrine\ORM\Mapping as ORM;
use TeamSoft\CrmRepositoryBundle\Entity\InfoDirection;
use TeamSoft\CrmRepositoryBundle\Entity\InfoUser;

/**
 * PoComparisonuserfield
 *
 * @ORM\Table(name="po_comparisonuserfield")
 * @ORM\Entity(repositoryClass="TeamSoft\CrmRepositoryBundle\Repository\Etms\PoComparisonuserfield")
 */
class PoComparisonuserfield
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var PoComparisonfield
     *
     * @ORM\ManyToOne(targetEntity="\TeamSoft\CrmRepositoryBundle\Entity\Etms\PoComparisonfield")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="field_id", referencedColumnName="id")
     * })
     */
    private $field;

    /**
     * @var InfoUser
     *
     * @ORM\ManyToOne(targetEntity="\TeamSoft\CrmRepositoryBundle\Entity\InfoUser")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     * })
     */
    private $user;

    /**
     * @var  int
     *
     * @ORM\Column(name="subitem_id", type="integer", nullable=true)
     */
    private $subitemId;

    /**
     * @var InfoDirection
     *
     * @ORM\ManyToOne(targetEntity="TeamSoft\CrmRepositoryBundle\Entity\InfoDirection")
     * @ORM\JoinColumn(name="direction_id", referencedColumnName="id")
     */
    private $direction;

    /**
     * @var  int
     *
     * @ORM\Column(name="checked", type="integer", nullable=true)
     */
    private $checked;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set field
     *
     * @param PoComparisonfield $field
     *
     * @return PoComparisonuserfield
     */
    public function setField(PoComparisonfield $field = null)
    {
        $this->field = $field;

        return $this;
    }

    /**
     * Get field
     *
     * @return PoComparisonfield
     */
    public function getField()
    {
        return $this->field;
    }

    /**
     * Set user
     *
     * @param InfoUser $user
     *
     * @return PoComparisonuserfield
     */
    public function setUser(InfoUser $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return InfoUser
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Set subitemId
     *
     * @param int $subitemId
     *
     * @return PoComparisonuserfield
     */
    public function setSubitemId($subitemId)
    {
        $this->subitemId = $subitemId;

        return $this;
    }

    /**
     * Get subitemId
     *
     * @return int
     */
    public function getSubitemId()
    {
        return $this->subitemId;
    }

    /**
     * Set direction.
     *
     * @param InfoDirection|null $direction
     *
     * @return PoComparisonuserfield
     */
    public function setDirection(InfoDirection $direction = null)
    {
        $this->direction = $direction;

        return $this;
    }

    /**
     * Get direction.
     *
     * @return InfoDirection|null
     */
    public function getDirection()
    {
        return $this->direction;
    }

    /**
     * Set checked.
     *
     * @param int|null $checked
     *
     * @return PoComparisonuserfield
     */
    public function setChecked($checked = null)
    {
        $this->checked = $checked;

        return $this;
    }

    /**
     * Get checked.
     *
     * @return int|null
     */
    public function getChecked()
    {
        return $this->checked;
    }
}
