<?php

namespace TeamSoft\CrmRepositoryBundle\Entity\Etms;

use Doctrine\ORM\Mapping as ORM;
use TeamSoft\CrmRepositoryBundle\Entity\InfoCompany;
use TeamSoft\CrmRepositoryBundle\Entity\ServiceFieldInterface;

/**
 * InfoPolygoncompany
 *
 * @ORM\Table(name="info_polygoncompany")
 * @ORM\Entity
 */
class InfoPolygoncompany implements ServiceFieldInterface
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="currenttime", type="datetime", nullable=true)
     */
    private $currenttime;

    /**
     * @var string
     *
     * @ORM\Column(name="guid", type="guid", nullable=true)
     */
    private $guid;

    /**
     * @var string
     *
     * @ORM\Column(name="moduser", type="string", length=16, nullable=true)
     */
    private $moduser;

    /**
     * @var int
     *
     * @ORM\Column(name="isadded", type="integer", nullable=true)
     */
    private $isadded;

    /**
     * @var int
     *
     * @ORM\Column(name="isdeleted", type="integer", nullable=true)
     */
    private $isdeleted;

    /**
     * @var InfoPolygon
     *
     * @ORM\ManyToOne(targetEntity="InfoPolygon", inversedBy="companies")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="polygon_id", referencedColumnName="id")
     * })
     */
    private $polygon;

    /**
     * @var InfoCompany
     *
     * @ORM\ManyToOne(targetEntity="TeamSoft\CrmRepositoryBundle\Entity\InfoCompany")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="company_id", referencedColumnName="id")
     * })
     */
    private $company;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set currenttime
     *
     * @param \DateTime $currenttime
     *
     * @return InfoPolygoncompany
     */
    public function setCurrenttime(\DateTime $currenttime)
    {
        $this->currenttime = $currenttime;

        return $this;
    }

    /**
     * Get currenttime
     *
     * @return \DateTime
     */
    public function getCurrenttime()
    {
        return $this->currenttime;
    }

    /**
     * Set guid
     *
     * @param string $guid
     *
     * @return InfoPolygoncompany
     */
    public function setGuid($guid)
    {
        $this->guid = $guid;

        return $this;
    }

    /**
     * Get guid
     *
     * @return string
     */
    public function getGuid()
    {
        return $this->guid;
    }

    /**
     * Set moduser
     *
     * @param string $moduser
     *
     * @return InfoPolygoncompany
     */
    public function setModuser($moduser)
    {
        $this->moduser = $moduser;

        return $this;
    }

    /**
     * Get moduser
     *
     * @return string
     */
    public function getModuser()
    {
        return $this->moduser;
    }

    /**
     * Set isadded
     *
     * @param int $isadded
     *
     * @return InfoPolygoncompany
     */
    public function setIsadded($isadded)
    {
        $this->isadded = $isadded;

        return $this;
    }

    /**
     * Get isadded
     *
     * @return int
     */
    public function getIsadded()
    {
        return $this->isadded;
    }

    /**
     * Set isdeleted
     *
     * @param int $isdeleted
     *
     * @return InfoPolygoncompany
     */
    public function setIsdeleted($isdeleted)
    {
        $this->isdeleted = $isdeleted;

        return $this;
    }

    /**
     * Get isdeleted
     *
     * @return int
     */
    public function getIsdeleted()
    {
        return $this->isdeleted;
    }

    /**
     * Set polygon
     *
     * @param \TeamSoft\CrmRepositoryBundle\Entity\Etms\InfoPolygon $polygon
     *
     * @return InfoPolygoncompany
     */
    public function setPolygon(InfoPolygon $polygon = null)
    {
        $this->polygon = $polygon;

        return $this;
    }

    /**
     * Get polygon
     *
     * @return \TeamSoft\CrmRepositoryBundle\Entity\Etms\InfoPolygon
     */
    public function getPolygon()
    {
        return $this->polygon;
    }

    /**
     * Set company
     *
     * @param \TeamSoft\CrmRepositoryBundle\Entity\InfoCompany $company
     *
     * @return InfoPolygoncompany
     */
    public function setCompany(InfoCompany $company = null)
    {
        $this->company = $company;

        return $this;
    }

    /**
     * Get company
     *
     * @return \TeamSoft\CrmRepositoryBundle\Entity\InfoCompany
     */
    public function getCompany()
    {
        return $this->company;
    }
}
