<?php

namespace TeamSoft\CrmRepositoryBundle\Entity\Etms;

use Doctrine\ORM\Mapping as ORM;
use TeamSoft\CrmRepositoryBundle\Entity\ServiceFieldInterface;

/**
 * InfoPolygonlayer
 *
 * @ORM\Table(name="info_polygonlayer")
 * @ORM\Entity(repositoryClass="TeamSoft\CrmRepositoryBundle\Repository\Etms\InfoPolygonlayer")
 */
class InfoPolygonlayer implements ServiceFieldInterface
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255, nullable=true)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="NameEng", type="string", length=255, nullable=true)
     */
    private $name_eng;

    /**
     * @var string
     *
     * @ORM\Column(name="tag", type="string", length=255, nullable=true)
     */
    private $tag;

    /**
     * @var integer
     *
     * @ORM\Column(name="isautoaddnewcompanies", type="integer", nullable=true)
     */
    private $isautoaddnewcompanies;

    /**
     * @var integer
     *
     * @ORM\OneToMany(targetEntity="InfoPolygonlayerdirection", mappedBy="polygonlayer")
     */
    private $polygonLayerDirection;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="currenttime", type="datetime", nullable=true)
     */
    private $currenttime;

    /**
     * @var string
     *
     * @ORM\Column(name="moduser", type="string", length=16, nullable=true)
     */
    private $moduser;

    /**
     * @var string
     *
     * @ORM\Column(name="guid", type="guid", nullable=true)
     */
    private $guid;

    /**
     * @var int
     *
     * @ORM\Column(name="priority", type="integer", nullable=true)
     */
    private $priority;

    /**
     * @var int
     *
     * @ORM\Column(name="row_num", type="integer", nullable=true)
     */
    private $row_num;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return InfoPolygonlayer
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     * @return $this
     */
    public function setNameEng($name = '')
    {
        $this->name_eng = $name;
        return $this;
    }

    /**
     * @return string
     */
    public function getNameEng()
    {
        return $this->name_eng;
    }

    /**
     * Set tag
     *
     * @param string $tag
     *
     * @return InfoPolygonlayer
     */
    public function setTag($tag)
    {
        $this->tag = $tag;

        return $this;
    }

    /**
     * Get tag
     *
     * @return string
     */
    public function getTag()
    {
        return $this->tag;
    }

    /**
     * Set isautoaddnewcompanies
     *
     * @param integer $isautoaddnewcompanies
     *
     * @return InfoPolygonlayer
     */
    public function setIsautoaddnewcompanies($isautoaddnewcompanies)
    {
        $this->isautoaddnewcompanies = $isautoaddnewcompanies;

        return $this;
    }

    /**
     * Get isautoaddnewcompanies
     *
     * @return integer
     */
    public function getIsautoaddnewcompanies()
    {
        return $this->isautoaddnewcompanies;
    }

    /**
     * Set currenttime
     *
     * @param \DateTime $currenttime
     *
     * @return InfoPolygonlayer
     */
    public function setCurrenttime(\DateTime $currenttime)
    {
        $this->currenttime = $currenttime;

        return $this;
    }

    /**
     * Get currenttime
     *
     * @return \DateTime
     */
    public function getCurrenttime()
    {
        return $this->currenttime;
    }

    /**
     * Set moduser
     *
     * @param string $moduser
     *
     * @return InfoPolygonlayer
     */
    public function setModuser($moduser)
    {
        $this->moduser = $moduser;

        return $this;
    }

    /**
     * Get moduser
     *
     * @return string
     */
    public function getModuser()
    {
        return $this->moduser;
    }

    /**
     * Set guid
     *
     * @param string $guid
     *
     * @return InfoPolygonlayer
     */
    public function setGuid($guid)
    {
        $this->guid = $guid;

        return $this;
    }

    /**
     * Get guid
     *
     * @return string
     */
    public function getGuid()
    {
        return $this->guid;
    }

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->polygonLayerDirection = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add polygonLayerDirection.
     *
     * @param InfoPolygonlayerdirection $polygonLayerDirection
     *
     * @return InfoPolygonlayer
     */
    public function addPolygonLayerDirection(InfoPolygonlayerdirection $polygonLayerDirection)
    {
        $this->polygonLayerDirection[] = $polygonLayerDirection;

        return $this;
    }

    /**
     * Remove polygonLayerDirection.
     *
     * @param InfoPolygonlayerdirection $polygonLayerDirection
     *
     * @return boolean TRUE if this collection contained the specified element, FALSE otherwise.
     */
    public function removePolygonLayerDirection(InfoPolygonlayerdirection $polygonLayerDirection)
    {
        return $this->polygonLayerDirection->removeElement($polygonLayerDirection);
    }

    /**
     * Get polygonLayerDirection.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getPolygonLayerDirection()
    {
        return $this->polygonLayerDirection;
    }

    /**
     * @return bool
     */
    public function isCustomLayer()
    {
        return in_array($this->getTag(), ['user_layer', 'voronoi_layer']);
    }

    /**
     * @param int $priority
     */
    public function setPriority($priority = 0)
    {
        $this->priority = $priority;
    }

    /**
     * @return int
     */
    public function getPriority()
    {
        return $this->priority;
    }

    /**
     * @param int|null $rowNum
     * @return $this
     */
    public function setRowNum($rowNum = null)
    {
        $this->row_num = $rowNum;
        return $this;
    }

    /**
     * @return int
     */
    public function getRowNum()
    {
        return $this->row_num;
    }
}
