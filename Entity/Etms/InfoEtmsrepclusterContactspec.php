<?php

namespace TeamSoft\CrmRepositoryBundle\Entity\Etms;

use TeamSoft\CrmRepositoryBundle\Entity\InfoCompanytype;
use TeamSoft\CrmRepositoryBundle\Entity\InfoDictionary;
use TeamSoft\CrmRepositoryBundle\Entity\ServiceFieldInterface;
use Doctrine\ORM\Mapping as ORM;

/**
 * Class InfoEtmsrepclusterContactspec
 * @package TeamSoft\CrmRepositoryBundle\Entity\Etms
 *
 * @ORM\Table(name="info_etmsrepcluster_contactspec", indexes={@ORM\Index(name="fk_info_etmsrepcluster_contactspec_spec_id", columns={"spec_id"}), @ORM\Index(name="fk_info_etmsrepcluster_contactspec_repcluster_id", columns={"repcluster_id"})})
 * @ORM\Entity()
 */
class InfoEtmsrepclusterContactspec implements ServiceFieldInterface
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id()
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var InfoEtmsrepcluster
     *
     * @ORM\ManyToOne(targetEntity="InfoEtmsrepcluster", inversedBy="contactSpecCollection")
     * @ORM\JoinColumn(name="repcluster_id", referencedColumnName="id", onDelete="CASCADE")
     */
    private $repcluster;

    /**
     * @var InfoDictionary
     *
     * @ORM\ManyToOne(targetEntity="TeamSoft\CrmRepositoryBundle\Entity\InfoDictionary")
     * @ORM\JoinColumn(name="spec_id", referencedColumnName="id", onDelete="CASCADE")
     */
    private $spec;

    /**
     * @var int
     *
     * @ORM\Column(name="value", type="integer", nullable=false)
     */
    private $value = 0;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="currenttime", type="datetime", nullable=true)
     */
    private $currenttime;

    /**
     * @var string
     *
     * @ORM\Column(name="moduser", type="string", length=16, nullable=true)
     */
    private $moduser;

    /**
     * @var string
     *
     * @ORM\Column(name="guid", type="guid", nullable=true)
     */
    private $guid;


    /**
     * @return \DateTime|string
     */
    public function getCurrenttime()
    {
        return $this->currenttime;
    }

    /**
     * @param \DateTime $value
     * @return $this|ServiceFieldInterface
     */
    public function setCurrenttime(\DateTime $value)
    {
        $this->currenttime = $value;
        return $this;
    }

    /**
     * @return int|string
     */
    public function getGuid()
    {
        return $this->guid;
    }

    /**
     * @param int $value
     * @return $this|ServiceFieldInterface
     */
    public function setGuid($value)
    {
        $this->guid = $value;
        return $this;
    }

    /**
     * @return string
     */
    public function getModuser()
    {
        return $this->moduser;
    }

    /**
     * @param string $value
     * @return $this|ServiceFieldInterface
     */
    public function setModuser($value)
    {
        $this->moduser = $value;
        return $this;
    }


    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return InfoEtmsrepcluster
     */
    public function getRepCluster()
    {
        return $this->repcluster;
    }

    /**
     * @param InfoEtmsrepcluster $etmsrepcluster
     * @return $this
     */
    public function setRepCluster(InfoEtmsrepcluster $etmsrepcluster)
    {
        $this->repcluster = $etmsrepcluster;
        return $this;
    }

    /**
     * @return InfoDictionary
     */
    public function getSpec()
    {
        return $this->spec;
    }

    /**
     * @param InfoCompanytype $companytype
     * @return $this
     */
    public function setSpec(InfoCompanytype $companytype)
    {
        $this->spec = $companytype;
        return $this;
    }

    /**
     * @return int
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     * @param int $value
     * @return $this
     */
    public function setValue($value)
    {
        $this->value = $value;
        return $this;
    }

    /**
     * @return int|null
     */
    public function getRepclusterId()
    {
        return $this->repcluster ? $this->repcluster->getId() : null;
    }

    /**
     * @return int|null
     */
    public function getSpecId()
    {
        return $this->spec ? $this->spec->getId() : null;
    }
}
