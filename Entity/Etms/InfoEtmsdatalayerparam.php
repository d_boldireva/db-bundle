<?php

namespace TeamSoft\CrmRepositoryBundle\Entity\Etms;

use Doctrine\ORM\Mapping as ORM;

/**
 * Class InfoEtmsdatalayerparam
 * @package TeamSoft\CrmRepositoryBundle\Entity\Etms
 *
 * @ORM\Table(name="info_etmsdatalayerparam",
 *     uniqueConstraints={@ORM\UniqueConstraint(name="uq_info_etmsdatalayerparam_datalayer_name", columns={"name","datalayer"})})
 * @ORM\Entity()
 */
class InfoEtmsdatalayerparam
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\GeneratedValue(strategy="IDENTITY")
     * @ORM\Id()
     */
    private $id;

    /**
     * @var string
     * @ORM\Column(name="name", type="string", length=255, nullable=false)
     */
    private $name;

    /**
     * @var string
     * @ORM\Column(name="value", type="string", length=2048, nullable=false)
     */
    private $value;

    /**
     * @var InfoEtmsdatalayer
     * @ORM\ManyToOne(targetEntity="TeamSoft\CrmRepositoryBundle\Entity\Etms\InfoEtmsdatalayer", inversedBy="paramCollection")
     * @ORM\JoinColumn(name="datalayer_id", referencedColumnName="id")
     */
    private $datalayer;

    /**
     * @var \DateTime
     * @ORM\Column(name="currenttime", type="datetime", nullable=true)
     */
    private $currenttime;

    /**
     * @var string
     * @ORM\Column(name="moduser", type="string", length=16)
     */
    private $moduser;

    /**
     * @var string
     * @ORM\Column(name="guid", type="guid", nullable=true)
     */
    private $guid;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     * @return $this
     */
    public function setName(string $name)
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return string
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     * @param $value
     * @return $this
     */
    public function setValue($value)
    {
        $this->value = $value;
        return $this;
    }

    /**
     * @return InfoEtmsdatalayer
     */
    public function getDatalayer()
    {
        return $this->datalayer;
    }

    /**
     * @param InfoEtmsdatalayer $datalayer
     * @return $this
     */
    public function setDatalayer(InfoEtmsdatalayer $datalayer)
    {
        $this->datalayer = $datalayer;
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getCurrenttime()
    {
        return $this->currenttime;
    }

    /**
     * @param $currenttime
     * @return $this
     */
    public function setCurrenttimr($currenttime)
    {
        $this->currenttime = $currenttime;
        return $this;
    }

    /**
     * @return string
     */
    public function getModuser()
    {
        return $this->moduser;
    }

    /**
     * @param $moduser
     * @return $this
     */
    public function setModuser($moduser)
    {
        $this->moduser = $moduser;
        return $this;
    }

    /**
     * @return string
     */
    public function getGuid()
    {
        return $this->guid;
    }

    /**
     * @param $guid
     * @return $this
     */
    public function setGuid($guid)
    {
        $this->guid = $guid;
        return $this;
    }

    /**
     * @return int|null
     */
    public function getDatalayerId()
    {
        return $this->datalayer ? $this->datalayer->getId() : null;
    }
}
