<?php

namespace TeamSoft\CrmRepositoryBundle\Entity\Etms;

use TeamSoft\CrmRepositoryBundle\Entity\InfoCompanytype;
use TeamSoft\CrmRepositoryBundle\Entity\ServiceFieldInterface;
use Doctrine\ORM\Mapping as ORM;

/**
 * Class InfoEtmsrepclusterCompanytype
 * @package TeamSoft\CrmRepositoryBundle\Entity\Etms
 *
 * @ORM\Table(name="info_etmsrepcluster_companytype", indexes={@ORM\Index(name="ix_info_etmsrepcluster_companytype_repcluster_id", columns={"repcluster_id"}), @ORM\Index(name="ix_info_etmsrepcluster_companytype_companytype_id", columns={"companytype_id"})})
 * @ORM\Entity()
 */
class InfoEtmsrepclusterCompanytype implements ServiceFieldInterface
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id()
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var InfoEtmsrepcluster
     *
     * @ORM\ManyToOne(targetEntity="InfoEtmsrepcluster", inversedBy="companyTypeCollection")
     * @ORM\JoinColumn(name="repcluster_id", referencedColumnName="id", onDelete="CASCADE")
     */
    private $repcluster;

    /**
     * @var InfoCompanytype
     *
     * @ORM\ManyToOne(targetEntity="TeamSoft\CrmRepositoryBundle\Entity\InfoCompanytype")
     * @ORM\JoinColumn(name="compantype_id", referencedColumnName="id", onDelete="CASCADE")
     */
    private $companytype;

    /**
     * @var int
     *
     * @ORM\Column(name="value", type="integer", nullable=false)
     */
    private $value = 0;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="currenttime", type="datetime", nullable=true)
     */
    private $currenttime;

    /**
     * @var string
     *
     * @ORM\Column(name="moduser", type="string", length=16, nullable=true)
     */
    private $moduser;

    /**
     * @var string
     *
     * @ORM\Column(name="guid", type="guid", nullable=true)
     */
    private $guid;


    /**
     * @return \DateTime|string
     */
    public function getCurrenttime()
    {
        return $this->currenttime;
    }

    /**
     * @param \DateTime $value
     * @return $this|ServiceFieldInterface
     */
    public function setCurrenttime(\DateTime $value)
    {
        $this->currenttime = $value;
        return $this;
    }

    /**
     * @return int|string
     */
    public function getGuid()
    {
        return $this->guid;
    }

    /**
     * @param int $value
     * @return $this|ServiceFieldInterface
     */
    public function setGuid($value)
    {
        $this->guid = $value;
        return $this;
    }

    /**
     * @return string
     */
    public function getModuser()
    {
        return $this->moduser;
    }

    /**
     * @param string $value
     * @return $this|ServiceFieldInterface
     */
    public function setModuser($value)
    {
        $this->moduser = $value;
        return $this;
    }


    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return InfoEtmsrepcluster
     */
    public function getRepCluster()
    {
        return $this->repcluster;
    }

    /**
     * @param InfoEtmsrepcluster $etmsrepcluster
     * @return $this
     */
    public function setRepCluster(InfoEtmsrepcluster $etmsrepcluster)
    {
        $this->repcluster = $etmsrepcluster;
        return $this;
    }

    /**
     * @return InfoCompanytype
     */
    public function getCompanyType()
    {
        return $this->companytype;
    }

    /**
     * @param InfoCompanytype $companytype
     * @return $this
     */
    public function setCompanyType(InfoCompanytype $companytype)
    {
        $this->companytype = $companytype;
        return $this;
    }

    /**
     * @return int
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     * @param int $value
     * @return $this
     */
    public function setValue($value)
    {
        $this->value = $value;
        return $this;
    }

    /**
     * @return int|null
     */
    public function getRepclusterId()
    {
        return $this->repcluster ? $this->repcluster->getId() : null;
    }

    /**
     * @return int|null
     */
    public function getCompanyTypeId()
    {
        return $this->companytype ? $this->companytype->getId() : null;
    }
}
