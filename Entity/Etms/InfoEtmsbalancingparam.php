<?php

namespace TeamSoft\CrmRepositoryBundle\Entity\Etms;

use Doctrine\ORM\Mapping as ORM;
use TeamSoft\CrmRepositoryBundle\Entity\InfoCompanycategory;
use TeamSoft\CrmRepositoryBundle\Entity\InfoCompanytype;
use TeamSoft\CrmRepositoryBundle\Entity\InfoContactcateg;
use TeamSoft\CrmRepositoryBundle\Entity\InfoDictionary;
use TeamSoft\CrmRepositoryBundle\Entity\InfoRegion;

/**
 * InfoEtmsbalancingparam
 *
 * @ORM\Table(name="info_etmsbalancingparam")
 * @ORM\Entity
 */
class InfoEtmsbalancingparam
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var int|null
     *
     * @ORM\Column(name="param_weight", type="integer", nullable=true)
     */
    private $paramWeight;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="currenttime", type="datetime", nullable=true)
     */
    private $currenttime;

    /**
     * @var string|null
     *
     * @ORM\Column(name="moduser", type="string", length=16, nullable=true)
     */
    private $moduser;

    /**
     * @var string|null
     *
     * @ORM\Column(name="guid", type="guid", nullable=true)
     */
    private $guid;

    /**
     * @var InfoEtmsbalancing
     *
     * @ORM\ManyToOne(targetEntity="InfoEtmsbalancing", inversedBy="balancingParam")
     * @ORM\JoinColumn(name="balancing_id", referencedColumnName="id")
     */
    private $balancing;

    /**
     * @var InfoCompanytype
     *
     * @ORM\ManyToOne(targetEntity="TeamSoft\CrmRepositoryBundle\Entity\InfoCompanytype")
     * @ORM\JoinColumn(name="companytype_id", referencedColumnName="id")
     */
    private $companytype;

    /**
     * @var InfoDictionary
     *
     * @ORM\ManyToOne(targetEntity="TeamSoft\CrmRepositoryBundle\Entity\InfoDictionary")
     * @ORM\JoinColumn(name="spec_id", referencedColumnName="id")
     */
    private $spec;

    /**
     * @var InfoRegion
     *
     * @ORM\ManyToOne(targetEntity="TeamSoft\CrmRepositoryBundle\Entity\InfoRegion")
     * @ORM\JoinColumn(name="region_id", referencedColumnName="id")
     */
    private $region;

    /**
     * @var InfoPolygon
     *
     * @ORM\ManyToOne(targetEntity="TeamSoft\CrmRepositoryBundle\Entity\Etms\InfoPolygon")
     * @ORM\JoinColumn(name="citypolygon_id", referencedColumnName="id")
     */
    private $citypolygon;

    /**
     * @var InfoPolygon
     *
     * @ORM\ManyToOne(targetEntity="TeamSoft\CrmRepositoryBundle\Entity\Etms\InfoPolygon")
     * @ORM\JoinColumn(name="regionpolygon_id", referencedColumnName="id")
     */
    private $regionpolygon;


    /**
     * @var InfoCompanycategory
     *
     * @ORM\ManyToOne (targetEntity="TeamSoft\CrmRepositoryBundle\Entity\InfoCompanycategory")
     * @ORM\JoinColumn (name="companycategory_id", referencedColumnName="id")
     */
    private $companycategory;

    /**
     * @var InfoContactcateg
     *
     * @ORM\ManyToOne (targetEntity="TeamSoft\CrmRepositoryBundle\Entity\InfoContactcateg")
     * @ORM\JoinColumn (name="contactcategory_id", referencedColumnName="id")
     */
    private $contactcategory;


    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set paramWeight.
     *
     * @param int|null $paramWeight
     *
     * @return InfoEtmsbalancingparam
     */
    public function setParamWeight($paramWeight = null)
    {
        $this->paramWeight = $paramWeight;

        return $this;
    }

    /**
     * Get paramWeight.
     *
     * @return int|null
     */
    public function getParamWeight()
    {
        return $this->paramWeight;
    }

    /**
     * Set currenttime.
     *
     * @param \DateTime|null $currenttime
     *
     * @return InfoEtmsbalancingparam
     */
    public function setCurrenttime($currenttime = null)
    {
        $this->currenttime = $currenttime;

        return $this;
    }

    /**
     * Get currenttime.
     *
     * @return \DateTime|null
     */
    public function getCurrenttime()
    {
        return $this->currenttime;
    }

    /**
     * Set moduser.
     *
     * @param string|null $moduser
     *
     * @return InfoEtmsbalancingparam
     */
    public function setModuser($moduser = null)
    {
        $this->moduser = $moduser;

        return $this;
    }

    /**
     * Get moduser.
     *
     * @return string|null
     */
    public function getModuser()
    {
        return $this->moduser;
    }

    /**
     * Set guid.
     *
     * @param string|null $guid
     *
     * @return InfoEtmsbalancingparam
     */
    public function setGuid($guid = null)
    {
        $this->guid = $guid;

        return $this;
    }

    /**
     * Get guid.
     *
     * @return string|null
     */
    public function getGuid()
    {
        return $this->guid;
    }

    /**
     * Set balancing.
     *
     * @param InfoEtmsbalancing|null $balancing
     *
     * @return InfoEtmsbalancingparam
     */
    public function setBalancing(InfoEtmsbalancing $balancing = null)
    {
        $this->balancing = $balancing;

        return $this;
    }

    /**
     * Get balancing.
     *
     * @return InfoEtmsbalancing|null
     */
    public function getBalancing()
    {
        return $this->balancing;
    }

    /**
     * Set companytype.
     *
     * @param InfoCompanytype|null $companytype
     *
     * @return InfoEtmsbalancingparam
     */
    public function setCompanytype(InfoCompanytype $companytype = null)
    {
        $this->companytype = $companytype;

        return $this;
    }

    /**
     * Get companytype.
     *
     * @return InfoCompanytype|null
     */
    public function getCompanytype()
    {
        return $this->companytype;
    }

    /**
     * Set spec.
     *
     * @param InfoDictionary|null $spec
     *
     * @return InfoEtmsbalancingparam
     */
    public function setSpec(InfoDictionary $spec = null)
    {
        $this->spec = $spec;

        return $this;
    }

    /**
     * Get spec.
     *
     * @return InfoDictionary|null
     */
    public function getSpec()
    {
        return $this->spec;
    }

    /**
     * Set region.
     *
     * @param InfoRegion|null $region
     *
     * @return InfoEtmsbalancingparam
     */
    public function setRegion(InfoRegion $region = null)
    {
        $this->region = $region;

        return $this;
    }

    /**
     * Get region.
     *
     * @return InfoRegion|null
     */
    public function getRegion()
    {
        return $this->region;
    }

    /**
     * Set city polygon
     *
     * @param InfoPolygon|null $polygon
     * @return $this
     */
    public function setCityPolygon(InfoPolygon $polygon = null)
    {
        $this->citypolygon = $polygon;

        return $this;
    }

    /**
     * Get city polygon
     *
     * @return InfoPolygon
     */
    public function getCityPolygon()
    {
        return $this->citypolygon;
    }

    /**
     * Set region polygon
     *
     * @param InfoPolygon $polygon
     * @return $this
     */
    public function setRegionPolygon(InfoPolygon $polygon)
    {
        $this->regionpolygon = $polygon;
        return $this;
    }

    /**
     * Get region polygon
     *
     * @return InfoPolygon
     */
    public function getRegionPolygon()
    {
        return $this->regionpolygon;
    }

    /**
     * @param InfoCompanycategory|null $companycategory
     * @return $this
     */
    public function setCompanyCategory(InfoCompanycategory $companycategory = null)
    {
        $this->companycategory = $companycategory;
        return $this;
    }

    /**
     * @return InfoCompanycategory
     */
    public function getCompanyCategory()
    {
        return $this->companycategory;
    }

    /**
     * @param InfoContactcateg|null $contactcategory
     * @return $this
     */
    public function setContactCategory(InfoContactcateg $contactcategory = null)
    {
        $this->contactcategory = $contactcategory;
        return $this;
    }

    /**
     * @return InfoContactcateg
     */
    public function getContactCategory()
    {
        return $this->contactcategory;
    }
}
