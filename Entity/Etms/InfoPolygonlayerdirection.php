<?php

namespace TeamSoft\CrmRepositoryBundle\Entity\Etms;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use TeamSoft\CrmRepositoryBundle\Entity\InfoDirection;
use TeamSoft\CrmRepositoryBundle\Entity\InfoUser;

/**
 * InfoPolygonlayerdirection
 *
 * @ORM\Table(name="info_polygonlayerdirection")
 * @ORM\Entity
 */
class InfoPolygonlayerdirection
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var InfoPolygonlayer
     *
     * @ORM\ManyToOne(targetEntity="InfoPolygonlayer", inversedBy="polygonLayerDirection")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="polygonlayer_id", referencedColumnName="id")
     * })
     */
    private $polygonlayer;

    /**
     * @var \TeamSoft\CrmRepositoryBundle\Entity\InfoDirection
     *
     * @ORM\ManyToOne(targetEntity="\TeamSoft\CrmRepositoryBundle\Entity\InfoDirection")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="direction_id", referencedColumnName="id")
     * })
     */
    private $direction;

    /**
     * @var \TeamSoft\CrmRepositoryBundle\Entity\InfoUser
     *
     * @ORM\ManyToOne(targetEntity="\TeamSoft\CrmRepositoryBundle\Entity\InfoUser")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="creator_id", referencedColumnName="id")
     * })
     */
    private $creator;

    /**
     * @var InfoPolygonlayer
     *
     * @ORM\ManyToOne(targetEntity="InfoPolygonlayer")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="parentlayer_id", referencedColumnName="id")
     * })
     */
    private $parentlayer;

    /**
     * @var InfoPolygonlayerdirectionparam
     *
     * @ORM\OneToMany(targetEntity="TeamSoft\CrmRepositoryBundle\Entity\Etms\InfoPolygonlayerdirectionparam", mappedBy="polygonlayerdirection")
     */
    private $params;

    /**
     * @var InfoEtmsmapsversion
     *
     * @ORM\ManyToOne(targetEntity="TeamSoft\CrmRepositoryBundle\Entity\Etms\InfoEtmsmapsversion")
     * @ORM\JoinColumn(name="version_id", referencedColumnName="id")
     */
    private $version;

    public function __construct()
    {
        $this->params = new ArrayCollection();
    }


    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }


    /**
     * Set polygonlayer.
     *
     * @param InfoPolygonlayer|null $polygonlayer
     *
     * @return InfoPolygonlayerdirection
     */
    public function setPolygonlayer(InfoPolygonlayer $polygonlayer = null)
    {
        $this->polygonlayer = $polygonlayer;

        return $this;
    }

    /**
     * Get polygonlayer.
     *
     * @return InfoPolygonlayer|null
     */
    public function getPolygonlayer()
    {
        return $this->polygonlayer;
    }

    /**
     * Set direction.
     *
     * @param \TeamSoft\CrmRepositoryBundle\Entity\InfoDirection|null $direction
     *
     * @return InfoPolygonlayerdirection
     */
    public function setDirection(InfoDirection $direction = null)
    {
        $this->direction = $direction;

        return $this;
    }

    /**
     * Get direction.
     *
     * @return \TeamSoft\CrmRepositoryBundle\Entity\InfoDirection|null
     */
    public function getDirection()
    {
        return $this->direction;
    }

    /**
     * Set creator.
     *
     * @param \TeamSoft\CrmRepositoryBundle\Entity\InfoUser|null $creator
     *
     * @return InfoPolygonlayerdirection
     */
    public function setCreator(InfoUser $creator = null)
    {
        $this->creator = $creator;

        return $this;
    }

    /**
     * Get creator.
     *
     * @return \TeamSoft\CrmRepositoryBundle\Entity\InfoUser|null
     */
    public function getCreator()
    {
        return $this->creator;
    }
    /**
     * Constructor
     */

    /**
     * Add param.
     *
     * @param InfoPolygonlayerdirectionparam $param
     *
     * @return InfoPolygonlayerdirection
     */
    public function addParam(InfoPolygonlayerdirectionparam $param)
    {
        $this->params[] = $param;

        return $this;
    }

    /**
     * Remove param.
     *
     * @param InfoPolygonlayerdirectionparam $param
     *
     * @return boolean TRUE if this collection contained the specified element, FALSE otherwise.
     */
    public function removeParam(InfoPolygonlayerdirectionparam $param)
    {
        return $this->params->removeElement($param);
    }

    /**
     * Get params.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getParams()
    {
        return $this->params;
    }

    /**
     * Set parentlayer.
     *
     * @param InfoPolygonlayer|null $parentlayer
     *
     * @return InfoPolygonlayerdirection
     */
    public function setParentlayer(InfoPolygonlayer $parentlayer = null)
    {
        $this->parentlayer = $parentlayer;

        return $this;
    }

    /**
     * Get parentlayer.
     *
     * @return InfoPolygonlayer|null
     */
    public function getParentlayer()
    {
        return $this->parentlayer;
    }

    /**
     * Set version
     *
     * @param InfoEtmsmapsversion $version
     * @return $this
     */
    public function setVersion(InfoEtmsmapsversion $version)
    {
        $this->version = $version;
        return $this;
    }

    /**
     * Get version
     *
     * @return InfoEtmsmapsversion
     */
    public function getVersion()
    {
        return $this->version;
    }
}
