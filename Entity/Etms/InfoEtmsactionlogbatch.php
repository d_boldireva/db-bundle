<?php

namespace TeamSoft\CrmRepositoryBundle\Entity\Etms;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use TeamSoft\CrmRepositoryBundle\Entity\InfoUser;
use TeamSoft\CrmRepositoryBundle\Entity\ServiceFieldInterface;

/**
 * Class InfoEtmsactionlogbatch
 * @package TeamSoft\CrmRepositoryBundle\Entity\Etms
 *
 * @ORM\Table(name="info_etmsactionlogbatch", indexes={@ORM\Index(name="ix_info_etmsactionlogbatch_version_id", columns={"version_id"})})
 * @ORM\Entity(repositoryClass="\TeamSoft\CrmRepositoryBundle\Repository\Etms\InfoEtmsactionlogbatch")
 */
class InfoEtmsactionlogbatch implements ServiceFieldInterface
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id()
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var InfoEtmsmapsversion
     *
     * @ORM\ManyToOne(targetEntity="InfoEtmsmapsversion")
     * @ORM\JoinColumn(name="version_id", referencedColumnName="id", onDelete="CASCADE")
     */
    private $version;

    /**
     * @var InfoEtmsactionlogbatchowner
     *
     * @ORM\OneToOne(targetEntity="InfoEtmsactionlogbatchowner", mappedBy="batch", cascade={"persist", "remove"})
     */
    private $owner;

    /**
     * @var ArrayCollection|InfoEtmsactionlog[]
     *
     * @ORM\OneToMany(targetEntity="InfoEtmsactionlog", mappedBy="batch", cascade={"persist", "remove"})
     */
    private $actionLogCollection;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="currenttime", type="datetime", nullable=true)
     */
    private $currenttime;

    /**
     * @var string|null
     *
     * @ORM\Column(name="moduser", type="string", length=16, nullable=true)
     */
    private $moduser;

    /**
     * @var string
     *
     * @ORM\Column(name="guid", type="guid", nullable=true)
     */
    private $guid;

    /**
     * InfoEtmsactionlogbatch constructor.
     */
    public function __construct()
    {
        $this->actionLogCollection = new ArrayCollection();
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param InfoEtmsmapsversion $version
     * @return $this
     */
    public function setVersion(InfoEtmsmapsversion $version)
    {
        $this->version = $version;
        return $this;
    }

    /**
     * @return InfoEtmsmapsversion
     */
    public function getVersion()
    {
        return $this->version;
    }

    /**
     * @param InfoEtmsactionlogbatchowner|null $owner
     * @return $this
     */
    public function setOwner(?InfoEtmsactionlogbatchowner $owner)
    {
        $owner->setBatch($this);
        $this->owner = $owner;

        return $this;
    }

    /**
     * @param \DateTime $currenttime
     * @return $this|ServiceFieldInterface
     */
    public function setCurrenttime(\DateTime $currenttime)
    {
        $this->currenttime = $currenttime;
        return $this;
    }

    /**
     * @return \DateTime|string|null
     */
    public function getCurrenttime()
    {
        return $this->currenttime;
    }

    /**
     * @param string $moduser
     * @return $this|ServiceFieldInterface
     */
    public function setModuser($moduser)
    {
        $this->moduser = $moduser;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getModuser()
    {
        return $this->moduser;
    }

    /**
     * @param int $guid
     * @return $this|ServiceFieldInterface
     */
    public function setGuid($guid)
    {
        $this->guid = $guid;
        return $this;
    }

    /**
     * @return int|string
     */
    public function getGuid()
    {
        return $this->guid;
    }

    /**
     * @return ArrayCollection|InfoEtmsactionlog[]
     */
    public function getActionLogCollection()
    {
        return $this->actionLogCollection;
    }

    /**
     * @param InfoEtmsactionlog|null $etmsactionlog
     * @return $this
     */
    public function addActionLogCollection(?InfoEtmsactionlog $etmsactionlog)
    {
        if (!$this->actionLogCollection->contains($etmsactionlog)) {
            $etmsactionlog->setBatch($this);
            $this->actionLogCollection->add($etmsactionlog);
        }

        return $this;
    }

    /**
     * @param InfoEtmsactionlog|null $etmsactionlog
     * @return $this
     */
    public function removeActionLogCollection(?InfoEtmsactionlog $etmsactionlog)
    {
        if ($this->actionLogCollection->contains($etmsactionlog)) {
            $this->actionLogCollection->removeElement($etmsactionlog);
            $etmsactionlog->setBatch(null);
        }

        return $this;
    }

    /**
     * @return $this
     */
    public function clearActionLogCollection()
    {
        foreach ($this->actionLogCollection as $action) {
            $this->actionLogCollection->removeElement($action);
            $action->setBatch(null);
        }

        return $this;
    }

    /**
     * @param InfoUser|null $user
     * @return $this
     */
    public function setOwnerUser(?InfoUser $user)
    {
        if (!$this->owner) {
            $this->owner = new InfoEtmsactionlogbatchowner();
            $this->owner->setBatch($this);
        }

        $this->owner->setOwner($user);

        return $this;
    }

    /**
     * @return InfoUser|null
     */
    public function getOwnerUser()
    {
        return $this->owner ? $this->owner->getOwner() : null;
    }

    /**
     * @return int|null
     */
    public function getVersionId()
    {
        return $this->version ? $this->version->getId() : null;
    }

    /**
     * @return int|null
     */
    public function getDirectionId()
    {
        return $this->version ? $this->version->getDirectionId() : null;
    }

    /**
     * @return int|null
     */
    public function getOwnerUserId()
    {
        return $this->owner ? $this->owner->getOwnerId() : null;
    }
}
