<?php

namespace TeamSoft\CrmRepositoryBundle\Entity\Etms;

use Doctrine\ORM\Mapping as ORM;
use phpDocumentor\Reflection\DocBlock\Tags\Return_;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Entity;
use TeamSoft\CrmRepositoryBundle\Entity\InfoCompany;
use TeamSoft\CrmRepositoryBundle\Entity\InfoDirection;
use TeamSoft\CrmRepositoryBundle\Entity\InfoUser;
use TeamSoft\CrmRepositoryBundle\Entity\ServiceFieldInterface;

/**
 * Class InfoEtmsdatalayervalues
 * @package TeamSoft\CrmRepositoryBundle\Entity\Etms
 *
 * @ORM\Table(name="info_etmsdatalayervalue")
 * @ORM\Entity()
 */
class InfoEtmsdatalayervalue implements ServiceFieldInterface
{
    /**
     * @var int
     *
     * @ORM\GeneratedValue(strategy="IDENTITY")
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id()
     */
    private $id;
    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="datefrom", type="datetime", nullable=true)
     */
    private $datefrom;
    /**
     * @var \DateTime|null
     * @ORM\Column(name="datetill", type="datetime", nullable=true)
     */
    private $datetill;
    /**
     * @var array
     * @ORM\Column(name="polygon", type="polygon", nullable=true)
     */
    private $polygon;
    /**
     * @var InfoDirection
     *
     * @ORM\ManyToOne(targetEntity="TeamSoft\CrmRepositoryBundle\Entity\InfoDirection")
     * @ORM\JoinColumn(name="direction_id", referencedColumnName="id")
     */
    private $direction;
    /**
     * @var string|null
     * @ORM\Column(name="color", type="string", length=32, nullable=true)
     */
    private $color;
    /**
     * @var string|null
     * @ORM\Column(name="label", type="string", length=255, nullable=true)
     */
    private $label;
    /**
     * @var InfoCompany
     * @ORM\ManyToOne(targetEntity="TeamSoft\CrmRepositoryBundle\Entity\InfoCompany")
     * @ORM\JoinColumn(referencedColumnName="id", name="company_id")
     */
    private $company;
    /**
     * @var InfoUser
     *
     * @ORM\ManyToOne(targetEntity="TeamSoft\CrmRepositoryBundle\Entity\InfoUser")
     * @ORM\JoinColumn(referencedColumnName="id", name="user_id")
     */
    private $user;
    /**
     * @var float|null
     * @ORM\Column(name="value", type="float", nullable=true)
     */
    private $value;
    /**
     * @var float|null
     * @ORM\Column(name="gps_la", type="float", nullable=true)
     */
    private $gpsLa;
    /**
     * @var float|null
     * @ORM\Column(name="gps_lo", type="float", nullable=true)
     */
    private $gpsLo;
    /**
     * @var int|null
     * @ORM\Column(name="row_num", type="integer", nullable=true)
     */
    private $rowNum;

    /**
     * @var InfoEtmsdatalayer
     *
     * @ORM\ManyToOne(targetEntity="InfoEtmsdatalayer", inversedBy="valueCollection")
     * @ORM\JoinColumn(referencedColumnName="id", name="datalayer_id")
     */
    private $datalayer;

    /**
     * @var string
     * @ORM\Column(name="svg", type="string", length=255, nullable=true)
     */
    private $svg;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="currenttime", type="datetime", nullable=true)
     */
    private $currenttime;
    /**
     * @var string|null
     * @ORM\Column(name="moduser", type="string", length=16, nullable=true)
     */
    private $moduser;
    /**
     * @var string
     * @ORM\Column(name="guid", type="guid", nullable=true)
     */
    private $guid;
    /**
     * @var string
     * @ORM\Column(name="gpspoint", type="geometry", nullable=true)
     */
    private $gpspoint;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return \DateTime
     */
    public function getDatefrom(): ?\DateTime
    {
        return $this->datefrom;
    }

    /**
     * @param \DateTime $datefrom
     * @return InfoEtmsdatalayervalue
     */
    public function setDatefrom(?\DateTime $datefrom): InfoEtmsdatalayervalue
    {
        $this->datefrom = $datefrom;
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getDatetill(): ?\DateTime
    {
        return $this->datetill;
    }

    /**
     * @param \DateTime $datetill
     * @return InfoEtmsdatalayervalue
     */
    public function setDatetill(?\DateTime $datetill): InfoEtmsdatalayervalue
    {
        $this->datetill = $datetill;
        return $this;
    }

    /**
     * @return array
     */
    public function getPolygon()
    {
        return $this->polygon;
    }

    /**
     * @param array $polygon
     * @return InfoEtmsdatalayervalue
     */
    public function setPolygon(array $polygon): InfoEtmsdatalayervalue
    {
        $this->polygon = $polygon;
        return $this;
    }

    /**
     * @return InfoDirection
     */
    public function getDirection()
    {
        return $this->direction;
    }

    /**
     * @param InfoDirection $direction
     * @return InfoEtmsdatalayervalue
     */
    public function setDirection(InfoDirection $direction): InfoEtmsdatalayervalue
    {
        $this->direction = $direction;
        return $this;
    }

    /**
     * @return string
     */
    public function getColor()
    {
        return $this->color;
    }

    /**
     * @param string $color
     * @return InfoEtmsdatalayervalue
     */
    public function setColor(?string $color): InfoEtmsdatalayervalue
    {
        $this->color = $color;
        return $this;
    }

    /**
     * @return string
     */
    public function getLabel()
    {
        return $this->label;
    }

    /**
     * @param string $label
     * @return InfoEtmsdatalayervalue
     */
    public function setLabel(?string $label): InfoEtmsdatalayervalue
    {
        $this->label = $label;
        return $this;
    }

    /**
     * @return InfoCompany
     */
    public function getCompany()
    {
        return $this->company;
    }

    /**
     * @param InfoCompany $company
     * @return InfoEtmsdatalayervalue
     */
    public function setCompany(?InfoCompany $company): InfoEtmsdatalayervalue
    {
        $this->company = $company;
        return $this;
    }

    /**
     * @return InfoUser
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @param InfoUser $user
     * @return InfoEtmsdatalayervalue
     */
    public function setUser(InfoUser $user): InfoEtmsdatalayervalue
    {
        $this->user = $user;
        return $this;
    }

    /**
     * @return float
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     * @param float $value
     * @return InfoEtmsdatalayervalue
     */
    public function setValue(?float $value): InfoEtmsdatalayervalue
    {
        $this->value = $value;
        return $this;
    }

    /**
     * @return float
     */
    public function getGpsLa()
    {
        return $this->gpsLa;
    }

    /**
     * @param float $gpsLa
     * @return InfoEtmsdatalayervalue
     */
    public function setGpsLa(?float $gpsLa): InfoEtmsdatalayervalue
    {
        $this->gpsLa = $gpsLa;
        return $this;
    }

    /**
     * @return float
     */
    public function getGpsLo()
    {
        return $this->gpsLo;
    }

    /**
     * @param float $gpsLo
     * @return InfoEtmsdatalayervalue
     */
    public function setGpsLo(?float $gpsLo): InfoEtmsdatalayervalue
    {
        $this->gpsLo = $gpsLo;
        return $this;
    }

    /**
     * @return int
     */
    public function getRowNum()
    {
        return $this->rowNum;
    }

    /**
     * @param int $rowNum
     * @return InfoEtmsdatalayervalue
     */
    public function setRowNum(?int $rowNum): InfoEtmsdatalayervalue
    {
        $this->rowNum = $rowNum;
        return $this;
    }

    /**
     * @return InfoEtmsdatalayer
     */
    public function getDatalayer(): InfoEtmsdatalayer
    {
        return $this->datalayer;
    }

    /**
     * @param InfoEtmsdatalayer $datalayer
     * @return InfoEtmsdatalayervalue
     */
    public function setDatalayer(InfoEtmsdatalayer $datalayer): InfoEtmsdatalayervalue
    {
        $this->datalayer = $datalayer;
        return $this;
    }

    /**
     * @return string
     */
    public function getSVG()
    {
        return $this->svg;
    }

    /**
     * @param string|null $svg
     * @return InfoEtmsdatalayervalue
     */
    public function setSVG($svg)
    {
        $this->svg = $svg;
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getCurrenttime(): \DateTime
    {
        return $this->currenttime;
    }

    /**
     * @param \DateTime $currenttime
     * @return InfoEtmsdatalayervalue
     */
    public function setCurrenttime(\DateTime $currenttime): InfoEtmsdatalayervalue
    {
        $this->currenttime = $currenttime;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getModuser()
    {
        return $this->moduser;
    }

    /**
     * @param mixed $moduser
     * @return InfoEtmsdatalayervalue
     */
    public function setModuser($moduser): InfoEtmsdatalayervalue
    {
        $this->moduser = $moduser;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getGuid()
    {
        return $this->guid;
    }

    /**
     * @param mixed $guid
     * @return InfoEtmsdatalayervalue
     */
    public function setGuid($guid): InfoEtmsdatalayervalue
    {
        $this->guid = $guid;
        return $this;
    }

    /**
     * Get direction id
     *
     * @return int|null
     */
    public function getDirectionId()
    {
        return $this->direction ? $this->direction->getId() : null;
    }

    /**
     * Get company id
     *
     * @return int|null
     */
    public function getCompanyId()
    {
        return $this->company ? $this->company->getId() : null;
    }

    /**
     * Get user id
     *
     * @return int|null
     */
    public function getUserId()
    {
        return $this->user ? $this->user->getId() : null;
    }

    /**
     * Get active color
     */
    public function getActiveColor()
    {
        return ($this->user ? $this->user->getColor() : null) ?: ($this->datalayer ? $this->datalayer->getMainColor() : $this->color);
    }

    /**
     * @param $gpspoint
     * @return $this
     */
    public function setGpspoint($gpspoint)
    {
        $this->gpspoint = $gpspoint;
        return $this;
    }

    /**
     * @return string
     */
    public function getGpspoint()
    {
        return $this->gpspoint;
    }
}
