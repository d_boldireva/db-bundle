<?php

namespace TeamSoft\CrmRepositoryBundle\Entity\Etms;

use Doctrine\Common\Collections\ArrayCollection;
use TeamSoft\CrmRepositoryBundle\Entity\ServiceFieldInterface;
use Doctrine\ORM\Mapping as ORM;

/**
 * Class InfoEtmsdatalayertag
 * @package TeamSoft\CrmRepositoryBundle\Entity\Etms
 *
 * @ORM\Table(name="info_etmsdatalayertag")
 * @ORM\Entity()
 */
class InfoEtmsdatalayertag implements ServiceFieldInterface
{
    /**
     * @var int|null
     *
     * @ORM\GeneratedValue(strategy="IDENTITY")
     * @ORM\Column(name="id", type="integer", nullable=true)
     * @ORM\Id()
     */
    private $id;

    /**
     * @var string
     * @ORM\Column(name="name", type="string", length=255, nullable=false)
     */
    private $name;

    /**
     * @var string
     * @ORM\Column(name="code", type="string", length=255, nullable=false, unique=true)
     */
    private $code;

    /**
     * @var ArrayCollection|InfoEtmsdatalayer[]
     * @ORM\ManyToMany(targetEntity="InfoEtmsdatalayer", mappedBy="tagCollection")
     */
    private $dataLayerCollection;

    /**
     * @var \DateTime|null
     * @ORM\Column(name="currenttime", type="datetime", nullable=true)
     */
    private $currenttime;
    /**
     * @var string|null
     * @ORM\Column(name="moduser", type="string", length=16, nullable=true)
     */
    private $moduser;
    /**
     * @var string|null
     * @ORM\Column(name="guid", type="guid", nullable=true)
     */
    private $guid;

    /**
     * InfoEtmsdatalayertag constructor.
     */
    public function __construct()
    {
        $this->dataLayerCollection = new ArrayCollection();
    }

    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     * @return InfoEtmsdatalayertag
     */
    public function setName(string $name): InfoEtmsdatalayertag
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @param string $code
     * @return $this
     */
    public function setCode(string $code)
    {
        $this->code = $code;
        return $this;
    }

    /**
     * @return string
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * @return ArrayCollection|InfoEtmsdatalayer[]
     */
    public function getDataLayerCollection()
    {
        return $this->dataLayerCollection;
    }

    /**
     * @param InfoEtmsdatalayer $etmsdatalayer
     * @return $this
     */
    public function addDataLayerCollection(InfoEtmsdatalayer $etmsdatalayer)
    {
        if (!$this->dataLayerCollection->contains($etmsdatalayer)) {
            $this->dataLayerCollection->add($etmsdatalayer);
        }
        $etmsdatalayer->addTagCollection($this);

        return $this;
    }

    /**
     * @param InfoEtmsdatalayer $etmsdatalayer
     * @return $this
     */
    public function removeDataLayerCollection(InfoEtmsdatalayer $etmsdatalayer)
    {
        if ($this->dataLayerCollection->contains($etmsdatalayer)) {
            $this->dataLayerCollection->remove($etmsdatalayer);
        }
        $etmsdatalayer->removeTagCollection($this);

        return $this;
    }

    /**
     * @return $this
     */
    public function clearDataLAyerCollection()
    {
        foreach ($this->dataLayerCollection as $etmsdatalayer) {
            $etmsdatalayer->removeTagCollection($this);
        }
        $this->dataLayerCollection->clear();

        return $this;
    }

    /**
     * @return \DateTime|null
     */
    public function getCurrenttime(): ?\DateTime
    {
        return $this->currenttime;
    }

    /**
     * @param \DateTime|null $currenttime
     * @return InfoEtmsdatalayertag
     */
    public function setCurrenttime(?\DateTime $currenttime): InfoEtmsdatalayertag
    {
        $this->currenttime = $currenttime;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getModuser(): ?string
    {
        return $this->moduser;
    }

    /**
     * @param string|null $moduser
     * @return InfoEtmsdatalayertag
     */
    public function setModuser($moduser): InfoEtmsdatalayertag
    {
        $this->moduser = $moduser;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getGuid(): ?string
    {
        return $this->guid;
    }

    /**
     * @param string|null $guid
     * @return InfoEtmsdatalayertag
     */
    public function setGuid($guid): InfoEtmsdatalayertag
    {
        $this->guid = $guid;
        return $this;
    }
}
