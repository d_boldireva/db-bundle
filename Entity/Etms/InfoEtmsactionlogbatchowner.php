<?php

namespace TeamSoft\CrmRepositoryBundle\Entity\Etms;

use Doctrine\ORM\Mapping as ORM;
use TeamSoft\CrmRepositoryBundle\Entity\InfoUser;
use TeamSoft\CrmRepositoryBundle\Entity\ServiceFieldInterface;

/**
 * Class InfoEtmsactionlogbatchowner
 * @package TeamSoft\CrmRepositoryBundle\Entity\Etms
 *
 * @ORM\Table(name="info_etmsactionlogbatchowner", uniqueConstraints={@ORM\UniqueConstraint(name="uq_info_etmsactionlogbatchowner_batch_id_owner_id", columns={"batch_id","owner_id"})})
 * @ORM\Entity()
 */
class InfoEtmsactionlogbatchowner implements ServiceFieldInterface
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id()
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var InfoEtmsactionlogbatch
     *
     * @ORM\OneToOne(targetEntity="InfoEtmsactionlogbatch", inversedBy="owner")
     * @ORM\JoinColumn(name="batch_id", referencedColumnName="id", onDelete="CASCADE")
     */
    private $batch;

    /**
     * @var InfoUser
     *
     * @ORM\ManyToOne(targetEntity="TeamSoft\CrmRepositoryBundle\Entity\InfoUser")
     * @ORM\JoinColumn(name="owner_id", referencedColumnName="id", onDelete="CASCADE")
     */
    private $owner;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="currenttime", type="datetime", nullable=true)
     */
    private $currenttime;

    /**
     * @var string|null
     *
     * @ORM\Column(name="moduser", type="string", length=16, nullable=true)
     */
    private $moduser;

    /**
     * @var string
     *
     * @ORM\Column(name="guid", type="guid", nullable=true)
     */
    private $guid;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param InfoEtmsactionlogbatch|null $batch
     * @return $this
     */
    public function setBatch(?InfoEtmsactionlogbatch $batch)
    {
        $this->batch = $batch;
        return $this;
    }

    /**
     * @return InfoEtmsactionlogbatch
     */
    public function getBatch()
    {
        return $this->batch;
    }

    /**
     * @param InfoUser|null $owner
     * @return $this
     */
    public function setOwner(?InfoUser $owner)
    {
        $this->owner = $owner;
        return $this;
    }

    /**
     * @return InfoUser
     */
    public function getOwner()
    {
        return $this->owner;
    }

    /**
     * @param \DateTime $currenttime
     * @return $this|ServiceFieldInterface
     */
    public function setCurrenttime(\DateTime $currenttime)
    {
        $this->currenttime = $currenttime;
        return $this;
    }

    /**
     * @return \DateTime|string|null
     */
    public function getCurrenttime()
    {
        return $this->currenttime;
    }

    /**
     * @param string $moduser
     * @return $this|ServiceFieldInterface
     */
    public function setModuser($moduser)
    {
        $this->moduser = $moduser;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getModuser()
    {
        return $this->moduser;
    }

    /**
     * @param int $guid
     * @return $this|ServiceFieldInterface
     */
    public function setGuid($guid)
    {
        $this->guid = $guid;
        return $this;
    }

    /**
     * @return int|string
     */
    public function getGuid()
    {
        return $this->guid;
    }

    /**
     * @return int|null
     */
    public function getBatchId()
    {
        return $this->batch ? $this->batch->getId() : null;
    }

    /**
     * @return int|null
     */
    public function getOwnerId()
    {
        return $this->owner ? $this->owner->getId() : null;
    }
}
