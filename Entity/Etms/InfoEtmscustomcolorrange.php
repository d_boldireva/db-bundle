<?php

namespace TeamSoft\CrmRepositoryBundle\Entity\Etms;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * Class InfoEtmscustomcolorrange
 * @package TeamSoft\CrmRepositoryBundle\Entity\Etms
 *
 * @ORM\Table(name="info_etmscustomcolorrange")
 * @ORM\Entity()
 */
class InfoEtmscustomcolorrange
{
    /**
     * @var integer
     *
     * @ORM\GeneratedValue(strategy="IDENTITY")
     * @ORM\Column(name="id", type="integer", nullable=true)
     * @ORM\Id()
     */
    private $id;

    /**
     * @var string|null
     *
     * @ORM\Column(name="name", type="string", length=255, nullable=true)
     */
    private $name;

    /**
     * @var \DateTime|null
     * @ORM\Column(name="currenttime", type="datetime", nullable=true)
     */
    private $currenttime;

    /**
     * @var string|null
     * @ORM\Column(name="moduser", type="string", length=16, nullable=true)
     */
    private $moduser;

    /**
     * @var string|null
     * @ORM\Column(name="guid", type="guid", nullable=true)
     */
    private $guid;

    /**
     * @var ArrayCollection|InfoEtmscustomcolorrangecolor[]
     *
     * @ORM\OneToMany(targetEntity="InfoEtmscustomcolorrangecolor", mappedBy="customcolorrange")
     */
    private $colorCollection;

    /**
     * @var ArrayCollection|InfoEtmsdatalayer[]
     *
     * @ORM\OneToMany(targetEntity="InfoEtmsdatalayer", mappedBy="colorRange")
     */
    private $layerCollection;

    /**
     * InfoEtmscustomcolorrange constructor.
     */
    public function __construct()
    {
        $this->colorCollection = new ArrayCollection();
        $this->layerCollection = new ArrayCollection();
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param string|null $name
     * @return $this
     */
    public function setName(?string $name)
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param $currenttime
     * @return $this
     */
    public function setCurrenttime($currenttime = null)
    {
        $this->currenttime = $currenttime;
        return $this;
    }

    /**
     * @return \DateTime|null
     */
    public function getCurrenttime()
    {
        return $this->currenttime;
    }

    /**
     * @param $moduser
     * @return $this
     */
    public function setModuser($moduser = null)
    {
        $this->moduser = $moduser;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getModuser()
    {
        return $this->moduser;
    }

    /**
     * @param $guid
     * @return $this
     */
    public function setGuid($guid = null)
    {
        $this->guid = $guid;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getGuid()
    {
        return $this->guid;
    }

    /**
     * @return ArrayCollection|InfoEtmscustomcolorrangecolor[]
     */
    public function getColorCollection()
    {
        return $this->colorCollection->toArray();
    }

    /**
     * @param InfoEtmscustomcolorrangecolor $color
     * @return $this
     */
    public function addColorCollection(InfoEtmscustomcolorrangecolor $color)
    {
        if (!$this->colorCollection->contains($color)) {
            $color->setCustomcolorrange($this);
            $this->colorCollection->add($color);
        }
        return $this;
    }

    /**
     * @param InfoEtmscustomcolorrangecolor $color
     * @return $this
     */
    public function removeColorCollection(InfoEtmscustomcolorrangecolor $color)
    {
        if ($this->colorCollection->contains($color)) {
            $this->colorCollection->removeElement($color);
            $color->setCustomcolorrange(null);
        }
        return $this;
    }

    /**
     * @return $this
     */
    public function clearColorCollection()
    {
        $this->colorCollection->clear();
        return $this;
    }

    /**
     * @return ArrayCollection|InfoEtmsdatalayer[]
     */
    public function getLayerCollection()
    {
        return $this->layerCollection;
    }

    /**
     * @param InfoEtmsdatalayer $etmsdatalayer
     * @return $this
     */
    public function addLayerCollection(InfoEtmsdatalayer $etmsdatalayer)
    {
        if (!$this->layerCollection->contains($etmsdatalayer)) {
            $etmsdatalayer->setColorrange($this);
            $this->layerCollection->add($etmsdatalayer);
        }

        return $this;
    }

    /**
     * @param InfoEtmsdatalayer $etmsdatalayer
     * @return $this
     */
    public function removeLayerCollection(InfoEtmsdatalayer $etmsdatalayer)
    {
        if ($this->layerCollection->contains($etmsdatalayer)) {
            $this->layerCollection->removeElement($etmsdatalayer);
            $etmsdatalayer->setColorrange(null);
        }

        return $this;
    }

    /**
     * @return $this
     */
    public function clearLayerCollection()
    {
        /**
         * @var InfoEtmsdatalayer $layer
         */
        foreach ($this->layerCollection->toArray() as $layer) {
            $layer->setColorrange(null);
        }

        $this->layerCollection->clear();

        return $this;
    }
}
