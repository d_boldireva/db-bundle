<?php

namespace TeamSoft\CrmRepositoryBundle\Entity\Etms;

use Doctrine\ORM\Mapping as ORM;
use TeamSoft\CrmRepositoryBundle\Entity\InfoUser;

/**
 * Class VwEtmsactionlog
 * @package TeamSoft\CrmRepositoryBundle\Entity\Etms
 *
 * @ORM\Table(name="vw_etmsactionlog")
 * @ORM\Entity(readOnly=true)
 */
class VwEtmsactionlog
{
    /**
     * @var int
     *
     * @ORM\Column(name="action_id", type="integer", nullable=false)
     * @ORM\Id()
     */
    private $action_id;

    /**
     * @var InfoEtmsactionlog
     *
     * @ORM\ManyToOne(targetEntity="InfoEtmsactionlog")
     * @ORM\JoinColumn(name="id", referencedColumnName="id")
     */
    private $action;

    /**
     * @var InfoEtmsactionlogbatch
     *
     * @ORM\ManyToOne(targetEntity="InfoEtmsactionlogbatch")
     * @ORM\JoinColumn(name="batch_id", referencedColumnName="id")
     */
    private $batch;

    /**
     * @var InfoUser|null
     *
     * @ORM\ManyToOne(targetEntity="TeamSoft\CrmRepositoryBundle\Entity\InfoUser")
     * @ORM\JoinColumn(name="owner_id", referencedColumnName="id")
     */
    private $owner;

    /**
     * @var InfoUser|null
     *
     * @ORM\ManyToOne(targetEntity="TeamSoft\CrmRepositoryBundle\Entity\InfoUser")
     * @ORM\JoinColumn(name="prev_user_id", referencedColumnName="id")
     */
    private $prev_user;

    /**
     * @var InfoUser|null
     *
     * @ORM\ManyToOne(targetEntity="TeamSoft\CrmRepositoryBundle\Entity\InfoUser")
     * @ORM\JoinColumn(name="new_user_id", referencedColumnName="id")
     */
    private $new_user;

    /**
     * @var InfoPolygon
     *
     * @ORM\ManyToOne(targetEntity="InfoPolygon")
     * @ORM\JoinColumn(name="polygon_id", referencedColumnName="id")
     */
    private $polygon;

    /**
     * @return InfoEtmsactionlog
     */
    public function getAction()
    {
        return $this->action;
    }

    /**
     * @return InfoEtmsactionlogbatch
     */
    public function getBatch()
    {
        return $this->batch;
    }

    /**
     * @return InfoUser|null
     */
    public function getOwner()
    {
        return $this->owner;
    }

    /**
     * @return InfoUser|null
     */
    public function getPrevUser()
    {
        return $this->prev_user;
    }

    /**
     * @return InfoUser|null
     */
    public function getNextUser()
    {
        return $this->new_user;
    }

    /**
     * @return InfoPolygon
     */
    public function getPolygon()
    {
        return $this->polygon;
    }

    /**
     * @return InfoEtmsmapsversion|null
     */
    public function getVersion()
    {
        return $this->batch ? $this->batch->getVersion() : null;
    }

    /**
     * @return \TeamSoft\CrmRepositoryBundle\Entity\InfoDirection|null
     */
    public function getDirection()
    {
        return $this->batch ? $this->batch->getVersion() ? $this->batch->getVersion()->getDirection() : null : null;
    }

    /**
     * @return int|null
     */
    public function getActionId()
    {
        return $this->action ? $this->action->getId() : null;
    }

    /**
     * @return int|null
     */
    public function getBatchId()
    {
        return $this->batch ? $this->batch->getId() : null;
    }

    /**
     * @return int|null
     */
    public function getOwnerId()
    {
        return $this->owner ? $this->owner->getId() : null;
    }

    /**
     * @return int|null
     */
    public function getPrevUserId()
    {
        return $this->prev_user ? $this->prev_user->getId() : null;
    }

    /**
     * @return int|null
     */
    public function getNewUserId()
    {
        return $this->new_user ? $this->new_user->getId() : null;
    }

    /**
     * @return int|null
     */
    public function getPolygonId()
    {
        return $this->polygon ? $this->polygon->getId() : null;
    }

    /**
     * @return int|null
     */
    public function getVersionId()
    {
        return $this->batch ? $this->batch->getVersionId() : null;
    }

    /**
     * @return int|null
     */
    public function getDirectionId()
    {
        return $this->batch ? $this->batch->getDirectionId() : null;
    }
}
