<?php

namespace TeamSoft\CrmRepositoryBundle\Entity\Etms;

use Doctrine\ORM\Mapping as ORM;

/**
 * Class InfoPolygonnearby
 * @package TeamSoft\CrmRepositoryBundle\Entity\Etms
 *
 * @ORM\Table(name="info_polygonnearby")
 * @ORM\Entity()
 */
class InfoPolygonnearby
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id()
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var InfoPolygon
     *
     * @ORM\ManyToOne(targetEntity="InfoPolygon", cascade={"persist"})
     * @ORM\JoinColumn(name="polygon_id", referencedColumnName="id", onDelete="CASCADE")
     */
    private $polygon;

    /**
     * @var InfoPolygon
     *
     * @ORM\ManyToOne(targetEntity="InfoPolygon", cascade={"persist"})
     * @ORM\JoinColumn(name="nearby_polygon_id", referencedColumnName="id", onDelete="CASCADE")
     */
    private $nearbyPolygon;

    /**
     * @var float
     *
     * @ORM\Column(name="intersection_length", type="float", nullable=true)
     */
    private $intersectionLength;

    /**
     * InfoPolygonnearby constructor.
     * @param InfoPolygon $polygon
     * @param InfoPolygon $nearbyPolygon
     * @param float $intersectionLength
     */
    function __construct(InfoPolygon $polygon, InfoPolygon $nearbyPolygon, $intersectionLength = 0.0)
    {
        $this->polygon = $polygon;
        $this->nearbyPolygon = $nearbyPolygon;
        $this->intersectionLength = $intersectionLength;
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param InfoPolygon $polygon
     * @return $this
     */
    public function setPolygon(InfoPolygon $polygon)
    {
        $this->polygon = $polygon;
        return $this;
    }

    /**
     * @return InfoPolygon
     */
    public function getPolygon()
    {
        return $this->polygon;
    }

    /**
     * @param InfoPolygon $polygon
     * @return $this
     */
    public function setNearbyPolygon(InfoPolygon $polygon)
    {
        $this->nearbyPolygon = $polygon;
        return $this;
    }

    /**
     * @return InfoPolygon
     */
    public function getNearbyPolygon()
    {
        return $this->nearbyPolygon;
    }

    /**
     * @param float $length
     * @return $this
     */
    public function setIntersectionLength($length = 0.0)
    {
        $this->intersectionLength = $length;
        return $this;
    }

    /**
     * @return float
     */
    public function getIntersectionLength()
    {
        return $this->intersectionLength;
    }
}
