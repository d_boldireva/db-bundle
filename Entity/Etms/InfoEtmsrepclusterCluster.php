<?php

namespace TeamSoft\CrmRepositoryBundle\Entity\Etms;

use Doctrine\Common\Collections\ArrayCollection;
use TeamSoft\CrmRepositoryBundle\Entity\InfoCompany;
use TeamSoft\CrmRepositoryBundle\Entity\InfoContact;
use TeamSoft\CrmRepositoryBundle\Entity\InfoRegion;
use TeamSoft\CrmRepositoryBundle\Entity\ServiceFieldInterface;
use Doctrine\ORM\Mapping as ORM;

/**
 * Class InfoEtmsrepclusterCluster
 * @package TeamSoft\CrmRepositoryBundle\Entity\Etms
 *
 * @ORM\Table(name="info_etmsrepcluster_cluster", indexes={@ORM\Index(name="ix_info_etmsrepcluster_cluster_repcluster_id", columns={"repcluster_id"}), @ORM\Index(name="ix_info_etmsrepcluster_cluster_region_id", columns={"region_id"})})
 * @ORM\Entity()
 */
class InfoEtmsrepclusterCluster implements ServiceFieldInterface
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id()
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255, nullable=true)
     */
    private $name;

    /**
     * @var InfoEtmsrepcluster
     *
     * @ORM\ManyToOne(targetEntity="InfoEtmsrepcluster", inversedBy="clusterCollection")
     * @ORM\JoinColumn(name="repcluster_id", referencedColumnName="id", onDelete="CASCADE")
     */
    private $repcluster;

    /**
     * @var InfoRegion
     *
     * @ORM\ManyToOne(targetEntity="TeamSoft\CrmRepositoryBundle\Entity\InfoRegion")
     * @ORM\JoinColumn(name="region_id", referencedColumnName="id", onDelete="SET NULL")
     */
    private $region;

    /**
     * @var string
     *
     * @ORM\Column(name="color", type="string", length=255, nullable=true)
     */
    private $color;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="currenttime", type="datetime", nullable=true)
     */
    private $currenttime;

    /**
     * @var string
     *
     * @ORM\Column(name="moduser", type="string", length=16, nullable=true)
     */
    private $moduser;

    /**
     * @var string
     *
     * @ORM\Column(name="guid", type="guid", nullable=true)
     */
    private $guid;

    /**
     * @var ArrayCollection|InfoCompany[]
     *
     * @ORM\ManyToMany(targetEntity="TeamSoft\CrmRepositoryBundle\Entity\InfoCompany", fetch="EXTRA_LAZY")
     * @ORM\JoinTable(
     *     name="info_etmsrepcluster_company",
     *     joinColumns={@ORM\JoinColumn(name="cluster_id", referencedColumnName="id", onDelete="CASCADE")},
     *     inverseJoinColumns={@ORM\JoinColumn(name="company_id", referencedColumnName="id", onDelete="CASCADE")}
     * )
     */
    private $companyCollection;

    /**
     * @var ArrayCollection|InfoContact[]
     *
     * @ORM\ManyToMany(targetEntity="TeamSoft\CrmRepositoryBundle\Entity\InfoContact", fetch="EXTRA_LAZY")
     * @ORM\JoinTable(
     *     name="info_etmsrepcluster_contact",
     *     joinColumns={@ORM\JoinColumn(name="cluster_id", referencedColumnName="id", onDelete="CASCADE")},
     *     inverseJoinColumns={@ORM\JoinColumn(name="contact_id", referencedColumnName="id", onDelete="CASCADE")}
     * )
     */
    private $contactCollection;

    /**
     * InfoEtmsrepclusterCluster constructor.
     * @param InfoEtmsrepcluster|null $cluster
     * @param InfoRegion|null $region
     * @param string|null $name
     * @param string|null $color
     */
    public function __construct(InfoEtmsrepcluster $cluster = null, InfoRegion $region = null, string $name = null, string $color = null)
    {
        if ($cluster) {
            $this->repcluster = $cluster;
        }
        if ($region) {
            $this->region = $region;
        }
        if ($name) {
            $this->name = $name;
        }
        if ($color) {
            $this->color = $color;
        }
        $this->companyCollection = new ArrayCollection();
        $this->contactCollection = new ArrayCollection();
    }


    /**
     * @return \DateTime|string
     */
    public function getCurrenttime()
    {
        return $this->currenttime;
    }

    /**
     * @param \DateTime $value
     * @return $this|ServiceFieldInterface
     */
    public function setCurrenttime(\DateTime $value)
    {
        $this->currenttime = $value;
        return $this;
    }

    /**
     * @return int|string
     */
    public function getGuid()
    {
        return $this->guid;
    }

    /**
     * @param int $value
     * @return $this|ServiceFieldInterface
     */
    public function setGuid($value)
    {
        $this->guid = $value;
        return $this;
    }

    /**
     * @return string
     */
    public function getModuser()
    {
        return $this->moduser;
    }

    /**
     * @param string $value
     * @return $this|ServiceFieldInterface
     */
    public function setModuser($value)
    {
        $this->moduser = $value;
        return $this;
    }


    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     * @return $this
     */
    public function setName($name)
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return InfoEtmsrepcluster
     */
    public function getRepCluster()
    {
        return $this->repcluster;
    }

    /**
     * @return int|null
     */
    public function getRepClusterId()
    {
        return $this->repcluster ? $this->repcluster->getId() : null;
    }

    /**
     * @param InfoEtmsrepcluster $etmsrepcluster
     * @return $this
     */
    public function setRepCluster(InfoEtmsrepcluster $etmsrepcluster)
    {
        $this->repcluster = $etmsrepcluster;
        return $this;
    }

    /**
     * @return InfoRegion
     */
    public function getRegion()
    {
        return $this->region;
    }

    /**
     * @return int|null
     */
    public function getRegionId()
    {
        return $this->region ? $this->region->getId() : null;
    }

    /**
     * @param InfoRegion $region
     * @return $this
     */
    public function setRegion(InfoRegion $region)
    {
        $this->region = $region;
        return $this;
    }

    /**
     * @return string
     */
    public function getColor()
    {
        return $this->color;
    }

    /**
     * @param $color
     * @return $this
     */
    public function setColor($color)
    {
        $this->color = $color;
        return $this;
    }

    /**
     * @return ArrayCollection|InfoCompany[]
     */
    public function getCompanyCollection()
    {
        return $this->companyCollection;
    }

    /**
     * @param InfoCompany $company
     * @return $this
     */
    public function addCompanyCollection(InfoCompany $company)
    {
        if (!$this->companyCollection->contains($company)) {
            $this->companyCollection->add($company);
        }

        return $this;
    }

    /**
     * @param InfoCompany $company
     * @return $this
     */
    public function removeCompanyCollection(InfoCompany $company)
    {
        if ($this->companyCollection->contains($company)) {
            $this->companyCollection->removeElement($company);
        }

        return $this;
    }

    /**
     * @return ArrayCollection|InfoContact[]
     */
    public function getContactCollection()
    {
        return $this->contactCollection;
    }

    /**
     * @param InfoContact $contact
     * @return $this
     */
    public function addContactCollection(InfoContact $contact)
    {
        if (!$this->contactCollection->contains($contact)) {
            $this->contactCollection->add($contact);
        }

        return $this;
    }

    /**
     * @param InfoContact $contact
     * @return $this
     */
    public function removeContactCollection(InfoContact $contact)
    {
        if ($this->contactCollection->contains($contact)) {
            $this->contactCollection->removeElement($contact);
        }

        return $this;
    }
}
