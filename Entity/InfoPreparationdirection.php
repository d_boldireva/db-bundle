<?php

namespace TeamSoft\CrmRepositoryBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * InfoPreparationdirection
 *
 * @ORM\Table(name="info_preparationdirection", indexes={@ORM\Index(name="ix_info_preparationdirection_guid", columns={"guid"}), @ORM\Index(name="ix_info_preparationdirection_time", columns={"time"}), @ORM\Index(name="ix_info_preparationdirection_direction_id", columns={"direction_id"}), @ORM\Index(name="ix_info_preparationdirection_preparation_id", columns={"preparation_id"})})
 * @ORM\Entity
 */
class InfoPreparationdirection
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string|null
     *
     * @ORM\Column(name="guid", type="guid", nullable=true)
     */
    private $guid;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="currenttime", type="datetime", nullable=true)
     */
    private $currenttime;

    /**
     * @var string|null
     *
     * @ORM\Column(name="moduser", type="string", length=16, nullable=true)
     */
    private $moduser;

    /**
     * @var InfoPreparation
     *
     * @ORM\ManyToOne(targetEntity="InfoPreparation", inversedBy="directionCollection2")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="preparation_id", referencedColumnName="id")
     * })
     */
    private $preparation;

    /**
     * @var InfoDirection
     *
     * @ORM\ManyToOne(targetEntity="InfoDirection")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="direction_id", referencedColumnName="id")
     * })
     */
    private $direction;



    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set guid.
     *
     * @param string|null $guid
     *
     * @return InfoPreparationdirection
     */
    public function setGuid($guid = null)
    {
        $this->guid = $guid;
    
        return $this;
    }

    /**
     * Get guid.
     *
     * @return string|null
     */
    public function getGuid()
    {
        return $this->guid;
    }

    /**
     * Set currenttime.
     *
     * @param \DateTime|null $currenttime
     *
     * @return InfoPreparationdirection
     */
    public function setCurrenttime($currenttime = null)
    {
        $this->currenttime = $currenttime;
    
        return $this;
    }

    /**
     * Get currenttime.
     *
     * @return \DateTime|null
     */
    public function getCurrenttime()
    {
        return $this->currenttime;
    }

    /**
     * Set moduser.
     *
     * @param string|null $moduser
     *
     * @return InfoPreparationdirection
     */
    public function setModuser($moduser = null)
    {
        $this->moduser = $moduser;
    
        return $this;
    }

    /**
     * Get moduser.
     *
     * @return string|null
     */
    public function getModuser()
    {
        return $this->moduser;
    }

    /**
     * Set preparation.
     *
     * @param InfoPreparation|null $preparation
     *
     * @return InfoPreparationdirection
     */
    public function setPreparation(InfoPreparation $preparation = null)
    {
        $this->preparation = $preparation;
    
        return $this;
    }

    /**
     * Get preparation.
     *
     * @return InfoPreparation|null
     */
    public function getPreparation()
    {
        return $this->preparation;
    }

    /**
     * Set direction.
     *
     * @param InfoDirection|null $direction
     *
     * @return InfoPreparationdirection
     */
    public function setDirection(InfoDirection $direction = null)
    {
        $this->direction = $direction;
    
        return $this;
    }

    /**
     * Get direction.
     *
     * @return \TeamSoft\CrmRepositoryBundle\Entity\InfoDirection|null
     */
    public function getDirection()
    {
        return $this->direction;
    }
}
