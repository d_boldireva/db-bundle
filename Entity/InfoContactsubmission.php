<?php

namespace TeamSoft\CrmRepositoryBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * InfoContactsubmission
 *
 * @ORM\Table(name="info_contactsubmission")
 * @ORM\Entity
 */
class InfoContactsubmission implements ServiceFieldInterface
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="guid", type="guid", nullable=true)
     */
    private $guid;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="currenttime", type="datetime", nullable=true)
     */
    private $currenttime;

    /**
     * @var string
     *
     * @ORM\Column(name="moduser", type="string", length=16, nullable=true)
     */
    private $moduser;

    /**
     * @var InfoUser
     *
     * @ORM\ManyToOne(targetEntity="InfoUser")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="subj_id", referencedColumnName="id")
     * })
     */
    private $subjId;

    /**
     * @var InfoUser
     *
     * @ORM\ManyToOne(targetEntity="InfoUser")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="child_id", referencedColumnName="id")
     * })
     */
    private $childId;

    /**
     * Set subjId
     *
     * @param \TeamSoft\CrmRepositoryBundle\Entity\InfoUser $subjId
     * @return InfoContactsubmission
     */
    public function setSubjId(InfoUser $subjId = null)
    {
        $this->subjId = $subjId;

        return $this;
    }

    /**
     * Get subjId
     *
     * @return InfoContactsubmission
     */
    public function getSubjId()
    {
        return $this->subjId;
    }

    /**
     * Set childId
     *
     * @param \TeamSoft\CrmRepositoryBundle\Entity\InfoUser $childId
     * @return InfoContactsubmission
     */
    public function setChildId(InfoUser $childId = null)
    {
        $this->childId = $childId;

        return $this;
    }

    /**
     * Get childId
     *
     * @return InfoContactsubmission
     */
    public function getChildId()
    {
        return $this->childId;
    }


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set guid
     *
     * @param string $guid
     *
     * @return InfoContactsubmission
     */
    public function setGuid($guid)
    {
        $this->guid = $guid;
    
        return $this;
    }

    /**
     * Get guid
     *
     * @return string
     */
    public function getGuid()
    {
        return $this->guid;
    }

    /**
     * Set currenttime
     *
     * @param \DateTime $currenttime
     *
     * @return InfoContactsubmission
     */
    public function setCurrenttime(\DateTime $currenttime)
    {
        $this->currenttime = $currenttime;
    
        return $this;
    }

    /**
     * Get currenttime
     *
     * @return \DateTime
     */
    public function getCurrenttime()
    {
        return $this->currenttime;
    }

    /**
     * Set moduser
     *
     * @param string $moduser
     *
     * @return InfoContactsubmission
     */
    public function setModuser($moduser)
    {
        $this->moduser = $moduser;
    
        return $this;
    }

    /**
     * Get moduser
     *
     * @return string
     */
    public function getModuser()
    {
        return $this->moduser;
    }

}
