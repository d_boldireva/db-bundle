<?php

namespace TeamSoft\CrmRepositoryBundle\Entity;

use Doctrine\ORM\Mapping as ORM;


/**
 * InfoTaskTypeByRole
 * 
 * @ORM\Table(name="info_tasktypebyrole")
 * @ORM\Entity
 */
class InfoTaskTypeByRole implements ServiceFieldInterface
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var InfoRole|null
     *
     * @ORM\ManyToOne(targetEntity="InfoRole")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="role_id", referencedColumnName="id")
     * })
     */
    private $role;

    /**
     * @var InfoTasktype|null
     *
     * @ORM\ManyToOne(targetEntity="InfoTasktype")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="tasktype_id", referencedColumnName="id")
     * })
     */
    private $taskType;

    /**
     * @var string|null
     *
     * @ORM\Column(name="guid", type="guid", nullable=true)
     */
    private $guid;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="currenttime", type="datetime", nullable=true)
     */
    private $currenttime;

    /**
     * @var string|null
     *
     * @ORM\Column(name="moduser", type="string", length=16, nullable=true)
     */
    private $moduser;

    /**
     * Set guid
     *
     * @param guid $guid
     *
     * @return self
     */
    public function setGuid($guid)
    {
        $this->guid = $guid;

        return $this;
    }

    /**
     * Get guid
     *
     * @return string
     */
    public function getGuid()
    {
        return $this->guid;
    }

    /**
     * Set currenttime.
     *
     * @param \DateTime|null $currenttime
     *
     * @return self
     */
    public function setCurrenttime($currenttime = null)
    {
        $this->currenttime = $currenttime;

        return $this;
    }

    /**
     * Get currenttime.
     *
     * @return \DateTime|null
     */
    public function getCurrenttime()
    {
        return $this->currenttime;
    }

    /**
     * Set moduser.
     *
     * @param string|null $moduser
     *
     * @return self
     */
    public function setModuser($moduser = null)
    {
        $this->moduser = $moduser;

        return $this;
    }

    /**
     * Get moduser.
     *
     * @return string|null
     */
    public function getModuser()
    {
        return $this->moduser;
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return InfoRole|null
     */
    public function getRole(): ?InfoRole
    {
        return $this->role;
    }

    /**
     * @param InfoRole|null $role
     * @return self
     */
    public function setRole(?InfoRole $role): self
    {
        $this->role = $role;
        return $this;
    }

    /**
     * @return InfoTasktype|null
     */
    public function getTaskType(): ?InfoTasktype
    {
        return $this->taskType;
    }

    /**
     * @param InfoTasktype|null $taskType
     * @return self
     */
    public function setTaskType(?InfoTasktype $taskType): self
    {
        $this->taskType = $taskType;
        return $this;
    }
}
