<?php

namespace TeamSoft\CrmRepositoryBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * InfoPhonetype
 *
 * @ORM\Table(name="info_phonetype", indexes={@ORM\Index(name="ix_info_phonetype_guid", columns={"guid"})})
 * @ORM\Entity
 */
class InfoPhonetype
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="Name", type="string", length=255, nullable=true)
     */
    private $name;

    /**
     * @var integer
     *
     * @ORM\Column(name="ishavecode", type="integer", nullable=true)
     */
    private $ishavecode;

    /**
     * @var guid
     *
     * @ORM\Column(name="guid", type="guid", nullable=true)
     */
    private $guid;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="currenttime", type="datetime", nullable=true)
     */
    private $currenttime;

    /**
     * @var string
     *
     * @ORM\Column(name="moduser", type="string", length=16, nullable=true)
     */
    private $moduser;

    /**
     * @var InfoContactphone[]|Collection
     * @ORM\OneToMany (targetEntity="InfoContactphone", mappedBy="type")
     */
    private $contactPhones;

    public function __construct()
    {
        $this->contactPhones = new ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return InfoPhonetype
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set ishavecode
     *
     * @param integer $ishavecode
     * @return InfoPhonetype
     */
    public function setIshavecode($ishavecode)
    {
        $this->ishavecode = $ishavecode;

        return $this;
    }

    /**
     * Get ishavecode
     *
     * @return integer 
     */
    public function getIshavecode()
    {
        return $this->ishavecode;
    }

    /**
     * Warning. Can be too many entities (10k+). Use with brain
     * @return Collection|InfoContactphone[]
     */
    public function getContactPhones(): Collection
    {
        return $this->contactPhones;
    }

    /**
     * Warning. Can be too many entities (10k+). Use with brain
     * @param InfoContactphone $contactPhone
     * @return self
     */
    public function addContactPhone(InfoContactphone $contactPhone): self
    {
        $contactPhone->setPhoneType($this);
        if (!$this->contactPhones->contains($contactPhone)) {
            $this->contactPhones->add($contactPhone);
        }

        return $this;
    }

    /**
     * Warning. Can be too many entities (10k+). Use with brain
     * @param InfoContactphone $contactPhone
     */
    public function removeContactPhone(InfoContactphone $contactPhone): void
    {
        $this->contactPhones->removeElement($contactPhone);
    }

    /**
     * Set guid
     *
     * @param guid $guid
     * @return InfoPhonetype
     */
    public function setGuid($guid)
    {
        $this->guid = $guid;

        return $this;
    }

    /**
     * Get guid
     *
     * @return guid 
     */
    public function getGuid()
    {
        return $this->guid;
    }

    /**
     * Set currenttime
     *
     * @param \DateTime $currenttime
     * @return InfoPhonetype
     */
    public function setCurrenttime($currenttime)
    {
        $this->currenttime = $currenttime;

        return $this;
    }

    /**
     * Get currenttime
     *
     * @return \DateTime 
     */
    public function getCurrenttime()
    {
        return $this->currenttime;
    }

    /**
     * Set moduser
     *
     * @param string $moduser
     * @return InfoPhonetype
     */
    public function setModuser($moduser)
    {
        $this->moduser = $moduser;

        return $this;
    }

    /**
     * Get moduser
     *
     * @return string 
     */
    public function getModuser()
    {
        return $this->moduser;
    }
}
