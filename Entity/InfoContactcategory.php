<?php

namespace TeamSoft\CrmRepositoryBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * InfoContactcategory
 *
 * @ORM\Table(name="info_contactcategory", indexes={@ORM\Index(name="ix_info_contactcategory_guid", columns={"guid"}), @ORM\Index(name="ix_info_contactcategory_time", columns={"time"}), @ORM\Index(name="ix_info_contactcategory_contact_id", columns={"contact_id"}), @ORM\Index(name="ix_info_contactcategory_category_id", columns={"category_id"}), @ORM\Index(name="ix_info_contactcategory_brend_id", columns={"brend_id"})})
 * @ORM\Entity(repositoryClass="TeamSoft\CrmRepositoryBundle\Repository\InfoContactcategory")
 */
class InfoContactcategory implements ServiceFieldInterface
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string|null
     *
     * @ORM\Column(name="guid", type="guid", nullable=true)
     */
    private $guid;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="currenttime", type="datetime", nullable=true)
     */
    private $currenttime;

    /**
     * @var string|null
     *
     * @ORM\Column(name="moduser", type="string", length=16, nullable=true)
     */
    private $moduser;

    /**
     * @var InfoContact
     *
     * @ORM\ManyToOne(targetEntity="InfoContact", inversedBy="contactCategoryCollection")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="contact_id", referencedColumnName="id")
     * })
     */
    private $contact;

    /**
     * @var InfoContactcateg
     *
     * @ORM\ManyToOne(targetEntity="InfoContactcateg")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="category_id", referencedColumnName="id")
     * })
     */
    private $category;

    /**
     * @var int|null
     *
     * @ORM\Column(name="category_id", type="integer", nullable=true)
     */
    private $categoryId;

    /**
     * @var InfoPreparationbrend
     *
     * @ORM\ManyToOne(targetEntity="InfoPreparationbrend")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="brend_id", referencedColumnName="id")
     * })
     */
    private $brend;


    /**
     * @var InfoDirection|null
     * @ORM\ManyToOne(targetEntity="InfoDirection")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="direction_id", referencedColumnName="id")
     * })
     */
    private $direction;



    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set guid.
     *
     * @param string|null $guid
     *
     * @return InfoContactcategory
     */
    public function setGuid($guid = null)
    {
        $this->guid = $guid;

        return $this;
    }

    /**
     * Get guid.
     *
     * @return string|null
     */
    public function getGuid()
    {
        return $this->guid;
    }

    /**
     * Set currenttime.
     *
     * @param \DateTime|null $currenttime
     *
     * @return InfoContactcategory
     */
    public function setCurrenttime(\DateTime $currenttime = null)
    {
        $this->currenttime = $currenttime;

        return $this;
    }

    /**
     * Get currenttime.
     *
     * @return \DateTime|null
     */
    public function getCurrenttime()
    {
        return $this->currenttime;
    }

    /**
     * Set moduser.
     *
     * @param string|null $moduser
     *
     * @return InfoContactcategory
     */
    public function setModuser($moduser = null)
    {
        $this->moduser = $moduser;

        return $this;
    }

    /**
     * Get moduser.
     *
     * @return string|null
     */
    public function getModuser()
    {
        return $this->moduser;
    }

    /**
     * Set contact.
     *
     * @param InfoContact|null $contact
     *
     * @return InfoContactcategory
     */
    public function setContact(InfoContact $contact = null)
    {
        $this->contact = $contact;

        return $this;
    }

    /**
     * Get contact.
     *
     * @return InfoContact|null
     */
    public function getContact()
    {
        return $this->contact;
    }

    /**
     * Set category.
     *
     * @param InfoContactcateg|null $category
     *
     * @return InfoContactcategory
     */
    public function setCategory(InfoContactcateg $category = null)
    {
        $this->category = $category;

        return $this;
    }

    /**
     * Get category.
     *
     * @return InfoContactcateg|null
     */
    public function getCategory()
    {
        return $this->category;
    }

    /**
     * Set brend.
     *
     * @param InfoPreparationbrend|null $brend
     *
     * @return InfoContactcategory
     */
    public function setBrend(InfoPreparationbrend $brend = null)
    {
        $this->brend = $brend;

        return $this;
    }

    /**
     * Get brend.
     *
     * @return InfoPreparationbrend|null
     */
    public function getBrend()
    {
        return $this->brend;
    }


    /**
     * @return InfoDirection|null
     */
    public function getDirection(): ?InfoDirection
    {
        return $this->direction;
    }

    /**
     * @param InfoDirection|null $direction
     *
     * @return InfoContactcategory
     */
    public function setDirection(?InfoDirection $direction): InfoContactcategory
    {
        $this->direction = $direction;

        return $this;
    }

    /**
     * @return int|null
     */
    public function getCategoryId(): ?int
    {
        return $this->getCategory() ? $this->getCategory()->getId() : null;
    }

    /**
     * @return int|null
     */
    public function getDirectionId(): ?int
    {
        return $this->getDirection() ? $this->getDirection()->getId() : null;
    }
}
