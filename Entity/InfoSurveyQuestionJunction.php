<?php

namespace TeamSoft\CrmRepositoryBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Criteria;
use Doctrine\ORM\Mapping as ORM;

/**
 * InfoSurveyQuestionJunction
 *
 * @ORM\Table(name="info_surveyquestionjunction")
 * @ORM\Entity
 */
class InfoSurveyQuestionJunction implements ServiceFieldInterface
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var int
     *
     * @ORM\Column(name="row_num", type="integer", nullable=true)
     */
    private $rowNumber;

    /**
     * @var string
     *
     * @ORM\Column(name="guid", type="guid", nullable=true)
     */
    private $guid;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="currenttime", type="datetime", nullable=true)
     */
    private $currenttime;

    /**
     * @var string
     *
     * @ORM\Column(name="moduser", type="string", length=16, nullable=true)
     */
    private $moduser;

    /**
     * @var InfoSurvey
     *
     * @ORM\ManyToOne(targetEntity="InfoSurvey")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="survey_id", referencedColumnName="id")
     * })
     */
    private $survey;

    /**
     * @var InfoSurveyQuestion
     *
     * @ORM\ManyToOne(targetEntity="InfoSurveyQuestion")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="question_id", referencedColumnName="id")
     * })
     */
    private $question;

    public function __clone()
    {
        $this->id = null;
        $this->guid = null;
        $this->currenttime = null;
        $this->moduser = null;
    }

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set rowNumber.
     *
     * @param int|null $rowNumber
     *
     * @return InfoSurveyQuestionJunction
     */
    public function setRowNumber($rowNumber = null)
    {
        $this->rowNumber = $rowNumber;

        return $this;
    }

    /**
     * Get rowNumber.
     *
     * @return int|null
     */
    public function getRowNumber()
    {
        return $this->rowNumber;
    }

    /**
     * Set guid.
     *
     * @param string|null $guid
     *
     * @return InfoSurveyQuestionJunction
     */
    public function setGuid($guid = null)
    {
        $this->guid = $guid;

        return $this;
    }

    /**
     * Get guid.
     *
     * @return string|null
     */
    public function getGuid()
    {
        return $this->guid;
    }

    /**
     * Set currenttime.
     *
     * @param \DateTime|null $currenttime
     *
     * @return InfoSurveyQuestionJunction
     */
    public function setCurrenttime(\DateTime $currenttime = null)
    {
        $this->currenttime = $currenttime;

        return $this;
    }

    /**
     * Get currenttime.
     *
     * @return \DateTime|null
     */
    public function getCurrenttime()
    {
        return $this->currenttime;
    }

    /**
     * Set moduser.
     *
     * @param string|null $moduser
     *
     * @return InfoSurveyQuestionJunction
     */
    public function setModuser($moduser = null)
    {
        $this->moduser = $moduser;

        return $this;
    }

    /**
     * Get moduser.
     *
     * @return string|null
     */
    public function getModuser()
    {
        return $this->moduser;
    }

    /**
     * Set survey.
     *
     * @param \TeamSoft\CrmRepositoryBundle\Entity\InfoSurvey|null $survey
     *
     * @return InfoSurveyQuestionJunction
     */
    public function setSurvey(\TeamSoft\CrmRepositoryBundle\Entity\InfoSurvey $survey = null)
    {
        $this->survey = $survey;

        return $this;
    }

    /**
     * Get survey.
     *
     * @return \TeamSoft\CrmRepositoryBundle\Entity\InfoSurvey|null
     */
    public function getSurvey()
    {
        return $this->survey;
    }

    /**
     * Set question.
     *
     * @param \TeamSoft\CrmRepositoryBundle\Entity\InfoSurveyQuestion|null $question
     *
     * @return InfoSurveyQuestionJunction
     */
    public function setQuestion(\TeamSoft\CrmRepositoryBundle\Entity\InfoSurveyQuestion $question = null)
    {
        $this->question = $question;

        return $this;
    }

    /**
     * Get question.
     *
     * @return \TeamSoft\CrmRepositoryBundle\Entity\InfoSurveyQuestion|null
     */
    public function getQuestion()
    {
        return $this->question;
    }
}
