<?php

namespace TeamSoft\CrmRepositoryBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * InfoTaskfilegroup
 *
 * @ORM\Table(name="info_taskfilegroup")
 * @ORM\Entity
 */
class InfoTaskfilegroup implements ServiceFieldInterface
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=50, nullable=true)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="guid", type="guid", nullable=true)
     */
    private $guid;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="currenttime", type="datetime", nullable=true)
     */
    private $currenttime;

    /**
     * @var string
     *
     * @ORM\Column(name="moduser", type="string", length=16, nullable=true)
     */
    private $moduser;

    /**
     * @var InfoTaskfilegroup
     *
     * @ORM\ManyToOne(targetEntity="InfoTaskfilegroup")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="parent", referencedColumnName="id")
     * })
     */
    private $parent;

    /**
     * @var InfoTask
     *
     * @ORM\ManyToOne(targetEntity="InfoTask", inversedBy="taskFileGroupCollection")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="subj_id", referencedColumnName="id")
     * })
     */
    private $subj;

    /**
     * @var ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="InfoTaskfile", mappedBy="subj", cascade={"persist"}, orphanRemoval=true)
     */
    private $taskFileCollection;

    /**
     * @ORM\ManyToMany(targetEntity="InfoCustomdictionaryvalue")
     * @ORM\JoinTable(name="info_taskfilegrouptag",
     *   joinColumns={@ORM\JoinColumn(name="taskfilegroup_id", referencedColumnName="id")},
     *   inverseJoinColumns={@ORM\JoinColumn(name="tag_id", referencedColumnName="id")}
     * )
     */
    private $tags;

    /**
     * @var ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="VwTaskfileimage", mappedBy="subj")
     */
    private $taskImageCollection;

    /**
     * @var InfoPhotodictionary
     *
     * @ORM\ManyToOne(targetEntity="InfoPhotodictionary")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="photodictionary_id", referencedColumnName="id")
     * })
     */
    private $photoDictionary;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->taskFileCollection = new ArrayCollection();
        $this->taskImageCollection = new ArrayCollection();
        $this->tags = new ArrayCollection();
    }


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return InfoTaskfilegroup
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set guid
     *
     * @param guid $guid
     *
     * @return InfoTaskfilegroup
     */
    public function setGuid($guid)
    {
        $this->guid = $guid;

        return $this;
    }

    /**
     * Get guid
     *
     * @return guid
     */
    public function getGuid()
    {
        return $this->guid;
    }

    /**
     * Set currenttime
     *
     * @param \DateTime $currenttime
     *
     * @return InfoTaskfilegroup
     */
    public function setCurrenttime(\DateTime $currenttime)
    {
        $this->currenttime = $currenttime;

        return $this;
    }

    /**
     * Get currenttime
     *
     * @return \DateTime
     */
    public function getCurrenttime()
    {
        return $this->currenttime;
    }

    /**
     * Set moduser
     *
     * @param string $moduser
     *
     * @return InfoTaskfilegroup
     */
    public function setModuser($moduser)
    {
        $this->moduser = $moduser;

        return $this;
    }

    /**
     * Get moduser
     *
     * @return string
     */
    public function getModuser()
    {
        return $this->moduser;
    }

    /**
     * Set parent
     *
     * @param \TeamSoft\CrmRepositoryBundle\Entity\InfoTaskfilegroup $parent
     *
     * @return InfoTaskfilegroup
     */
    public function setParent(\TeamSoft\CrmRepositoryBundle\Entity\InfoTaskfilegroup $parent = null)
    {
        $this->parent = $parent;

        return $this;
    }

    /**
     * Get parent
     *
     * @return \TeamSoft\CrmRepositoryBundle\Entity\InfoTaskfilegroup
     */
    public function getParent()
    {
        return $this->parent;
    }

    /**
     * Set subj
     *
     * @param \TeamSoft\CrmRepositoryBundle\Entity\InfoTask $subj
     *
     * @return InfoTaskfilegroup
     */
    public function setSubj(\TeamSoft\CrmRepositoryBundle\Entity\InfoTask $subj = null)
    {
        $this->subj = $subj;

        return $this;
    }

    /**
     * Get subj
     *
     * @return \TeamSoft\CrmRepositoryBundle\Entity\InfoTask
     */
    public function getSubj()
    {
        return $this->subj;
    }

    /**
     * Add taskFileCollection
     *
     * @param \TeamSoft\CrmRepositoryBundle\Entity\InfoTaskfile $taskFile
     *
     * @return InfoTaskfilegroup
     */
    public function addTaskFileCollection(\TeamSoft\CrmRepositoryBundle\Entity\InfoTaskfile $taskFile)
    {
        $taskFile->setSubj($this);
        $this->taskFileCollection->add($taskFile);

        return $this;
    }

    /**
     * Remove taskFileCollection
     *
     * @param \TeamSoft\CrmRepositoryBundle\Entity\InfoTaskfile $taskFile
     */
    public function removeTaskFileCollection(\TeamSoft\CrmRepositoryBundle\Entity\InfoTaskfile $taskFile)
    {
        $taskFile->setSubj(null);
        $this->taskFileCollection->removeElement($taskFile);
    }

    /**
     * Get taskFileCollection
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getTaskFileCollection()
    {
        return $this->taskFileCollection;
    }

    /**
     * @param VwTaskfileimage $taskfileimage
     * @return $this
     */
    public function addTaskImageCollection(VwTaskfileimage $taskfileimage){
        $this->taskImageCollection->add($taskfileimage);
        return $this;
    }

    /**
     * @param VwTaskfileimage $taskfileimage
     * @return $this
     */
    public function removeTaskImageCollection(VwTaskfileimage $taskfileimage){
        $this->taskImageCollection->removeElement($taskfileimage);
        return $this;
    }

    /**
     * @return ArrayCollection
     */
    public function getTaskImageCollection(){
        return $this->taskImageCollection;
    }

    /**
     * Add tag
     *
     * @param InfoCustomdictionaryvalue $tag
     *
     * @return InfoTaskfilegroup
     */
    public function addTag(InfoCustomdictionaryvalue $tag)
    {
        $this->tags->add($tag);
        return $this;
    }
    /**
     * Remove tag
     *
     * @param InfoCustomdictionaryvalue $tag
     */
    public function removeTag(InfoCustomdictionaryvalue $tag)
    {
        $this->tags->removeElement($tag);
    }

    /**
     * Get tags
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getTags()
    {
        return $this->tags;
    }

    /**
     * Set photoDictionary
     *
     * @param InfoPhotodictionary $photoDictionary
     *
     * @return self
     */
    public function setPhotoDictionary(?InfoPhotodictionary $photoDictionary): self
    {
        $this->photoDictionary = $photoDictionary;

        return $this;
    }

    /**
     * Get photoDictionary
     *
     * @return InfoPhotodictionary
     */
    public function getPhotoDictionary(): ?InfoPhotodictionary
    {
        return $this->photoDictionary;
    }
}
