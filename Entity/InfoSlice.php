<?php

namespace TeamSoft\CrmRepositoryBundle\Entity;

use DateTime;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * InfoSlice
 *
 * @ORM\Table(name="info_slice")
 * @ORM\Entity(repositoryClass="TeamSoft\CrmRepositoryBundle\Repository\InfoSlice")
 */
class InfoSlice implements ServiceFieldInterface
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string|null
     *
     * @ORM\Column(name="name", type="string", length=255, nullable=true)
     */
    private $name;

    /**
     * @var string|null
     *
     * @ORM\Column(name="code", type="string", length=255, nullable=true)
     */
    private $code;

    /**
     * @var string|null
     *
     * @ORM\Column(name="server_sql", type="string", length=8000, nullable=true)
     */
    private $serverSql;

    /**
     * @var string|null
     *
     * @ORM\Column(name="client_sql", type="string", length=8000, nullable=true)
     */
    private $clientSql;

    /**
     * @var DateTime|null
     *
     * @ORM\Column(name="currenttime", type="datetime", nullable=true)
     */
    private $currenttime;

    /**
     * @var string|null
     *
     * @ORM\Column(name="moduser", type="string", length=16, nullable=true)
     * )
     */
    private $moduser;

    /**
     * @var string|null
     *
     * @ORM\Column(name="guid", type="guid", nullable=true)
     */
    private $guid;

    /**
     * Many slices have one group
     * @ORM\ManyToOne(targetEntity="InfoSliceGroup", inversedBy="id")
     * @ORM\JoinColumn(name="group_id", referencedColumnName="id")
     */
    private $group;

    /**
     * One slice has many fields
     * @ORM\OneToMany(targetEntity="InfoSliceField", mappedBy="slice")
     */
    private $fields;

    /**
     * One slice has many filters
     * @ORM\OneToMany(targetEntity="InfoSliceFilter", mappedBy="slice")
     */
    private $filters;

    /**
     * One slice has many filters
     * @ORM\OneToMany(targetEntity="InfoReportTabFilter", mappedBy="slice")
     */
    private $conditions;

    /**
     * One slice has many relations
     * @ORM\OneToMany(targetEntity="InfoSliceRelation", mappedBy="parent")
     */
    private $relations;

    /**
     * @var InfoUser
     *
     * @ORM\ManyToMany(targetEntity="InfoSlice")
     * @ORM\JoinTable(name="info_slicerelation",
     *     joinColumns={@ORM\JoinColumn(name="parent_id", referencedColumnName="id")},
     *     inverseJoinColumns={@ORM\JoinColumn(name="child_id", referencedColumnName="id")}
     * )
     */
    private $childSlices;

    public function __construct()
    {
        $this->fields = new ArrayCollection();
        $this->relations = new ArrayCollection();
        $this->filters = new ArrayCollection();
        $this->conditions = new ArrayCollection();
        $this->childSlices = new ArrayCollection();
    }


    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set group
     *
     * @param InfoSliceGroup $group
     *
     * @return InfoSlice
     */
    public function setGroup($group = null)
    {
        $this->group = $group;

        return $this;
    }

    /**
     * Get group
     *
     * @return InfoSliceGroup
     */
    public function getGroup()
    {
        return $this->group;
    }

    /**
     * Set name.
     *
     * @param string|null $name
     *
     * @return InfoSlice
     */
    public function setName($name = null)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name.
     *
     * @return string|null
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set code.
     *
     * @param string|null $code
     *
     * @return InfoSlice
     */
    public function setCode($code = null)
    {
        $this->code = $code;

        return $this;
    }

    /**
     * Get code.
     *
     * @return string|null
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * Set serverSql.
     *
     * @param string|null $serverSql
     *
     * @return InfoSlice
     */
    public function setServerSql($serverSql = null)
    {
        $this->serverSql = $serverSql;

        return $this;
    }

    /**
     * Get serverSql.
     *
     * @return string|null
     */
    public function getServerSql()
    {
        return $this->serverSql;
    }

    /**
     * Set clientSql.
     *
     * @param string|null $clientSql
     *
     * @return InfoSlice
     */
    public function setClientSql($clientSql = null)
    {
        $this->clientSql = $clientSql;

        return $this;
    }

    /**
     * Get clientSql.
     *
     * @return string|null
     */
    public function getClientSql()
    {
        return $this->clientSql;
    }

    /**
     * Set currenttime.
     *
     * @param DateTime|null $currenttime
     *
     * @return InfoSlice
     */
    public function setCurrenttime(DateTime $currenttime = null)
    {
        $this->currenttime = $currenttime;

        return $this;
    }

    /**
     * Get currenttime.
     *
     * @return DateTime|null
     */
    public function getCurrenttime()
    {
        return $this->currenttime;
    }

    /**
     * Set moduser.
     *
     * @param string|null $moduser
     *
     * @return InfoSlice
     */
    public function setModuser($moduser = null)
    {
        $this->moduser = $moduser;

        return $this;
    }

    /**
     * Get moduser.
     *
     * @return string|null
     */
    public function getModuser()
    {
        return $this->moduser;
    }

    /**
     * Set guid.
     *
     * @param string|null $guid
     *
     * @return InfoSlice
     */
    public function setGuid($guid = null)
    {
        $this->guid = $guid;

        return $this;
    }

    /**
     * Get guid.
     *
     * @return string|null
     */
    public function getGuid()
    {
        return $this->guid;
    }

    /**
     * Add field
     *
     * @param InfoSliceField $field
     *
     * @return InfoSlice
     */
    public function addField(InfoSliceField $field)
    {
        $field->setSlice($this);
        $this->fields->add($field);

        return $this;
    }

    /**
     * Remove field
     *
     * @param InfoSliceField $field
     */
    public function removeField(InfoSliceField $field)
    {
        $field->setSlice(null);
        $this->fields->removeElement($field);
    }

    /**
     * Get fields
     *
     * @return Collection
     */
    public function getFields()
    {
        return $this->fields;
    }

    /**
     * Add filter
     *
     * @param InfoSliceFilter $filter
     *
     * @return InfoSlice
     */
    public function addFilter(InfoSliceFilter $filter)
    {
        $filter->setSlice($this);
        $this->filters->add($filter);

        return $this;
    }

    /**
     * Remove filter
     *
     * @param InfoSliceFilter $filter
     */
    public function removeFilter(InfoSliceFilter $filter)
    {
        $filter->setSlice(null);
        $this->filters->removeElement($filter);
    }

    /**
     * Get filters
     *
     * @return Collection
     */
    public function getFilters()
    {
        return $this->filters;
    }

    /**
     * Add childSlice
     *
     * @param InfoSlice $childSlice
     *
     * @return InfoSlice
     */
    public function addChildSlice(InfoSlice $childSlice)
    {
        $this->childSlices[] = $childSlice;

        return $this;
    }

    /**
     * Remove childSlice
     *
     * @param InfoSlice $childSlice
     */
    public function removeChildSlice(InfoSlice $childSlice)
    {
        $this->childSlices->removeElement($childSlice);
    }

    /**
     * Get childSlices
     *
     * @return Collection
     */
    public function getChildSlices()
    {
        return $this->childSlices;
    }

    /**
     * Get conditions
     *
     * @return Collection
     */
    public function getConditions()
    {
        return $this->conditions;
    }

    /**
     * Add relation
     *
     * @param InfoSlice $relation
     *
     * @return InfoSlice
     */
    public function addRelation(InfoSlice $relation)
    {
        $this->relations[] = $relation;

        return $this;
    }

    /**
     * Remove relation
     *
     * @param InfoSlice $relation
     */
    public function removeRelation(InfoSlice $relation)
    {
        $this->relations->removeElement($relation);
    }

    /**
     * Get relations
     *
     * @return Collection
     */
    public function getRelations()
    {
        return $this->relations;
    }
}
