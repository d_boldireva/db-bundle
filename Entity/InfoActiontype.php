<?php

namespace TeamSoft\CrmRepositoryBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * InfoActiontype
 *
 * @ORM\Table(name="info_actiontype", indexes={@ORM\Index(name="ix_info_actiontype_guid", columns={"guid"}), @ORM\Index(name="ix_info_actiontype_time", columns={"time"})})
 * @ORM\Entity(repositoryClass="TeamSoft\CrmRepositoryBundle\Repository\InfoActiontype")
 */
class InfoActiontype
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255, nullable=false)
     */
    private $name;

    /**
     * @var guid
     *
     * @ORM\Column(name="guid", type="guid", nullable=true)
     */
    private $guid;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="currenttime", type="datetime", nullable=true)
     */
    private $currenttime;

    /**
     * @var string
     *
     * @ORM\Column(name="moduser", type="string", length=16, nullable=true)
     */
    private $moduser;

    /**
     * @var integer
     *
     * @ORM\Column(name="groupaction", type="integer", nullable=true)
     */
    private $groupaction;

    /**
     * @var string
     *
     * @ORM\Column(name="colorhex", type="string", length=255, nullable=true)
     */
    private $colorhex;

    /**
     * @var string
     *
     * @ORM\Column(name="isinvisibly", type="integer", nullable=true)
     */
    private $isInvisibly;

    /**
     * @var InfoLanguage
     *
     * @ORM\ManyToOne(targetEntity="InfoLanguage")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="language_id", referencedColumnName="id")
     * })
     */
    private $language;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return InfoActiontype
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set guid
     *
     * @param guid $guid
     * @return InfoActiontype
     */
    public function setGuid($guid)
    {
        $this->guid = $guid;

        return $this;
    }

    /**
     * Get guid
     *
     * @return guid
     */
    public function getGuid()
    {
        return $this->guid;
    }

    /**
     * Set currenttime
     *
     * @param \DateTime $currenttime
     * @return InfoActiontype
     */
    public function setCurrenttime($currenttime)
    {
        $this->currenttime = $currenttime;

        return $this;
    }

    /**
     * Get currenttime
     *
     * @return \DateTime
     */
    public function getCurrenttime()
    {
        return $this->currenttime;
    }

    /**
     * Set moduser
     *
     * @param string $moduser
     * @return InfoActiontype
     */
    public function setModuser($moduser)
    {
        $this->moduser = $moduser;

        return $this;
    }

    /**
     * Get moduser
     *
     * @return string
     */
    public function getModuser()
    {
        return $this->moduser;
    }

    /**
     * Set groupaction
     *
     * @param integer $groupaction
     * @return InfoActiontype
     */
    public function setGroupaction($groupaction)
    {
        $this->groupaction = $groupaction;

        return $this;
    }

    /**
     * Get groupaction
     *
     * @return integer
     */
    public function getGroupaction()
    {
        return $this->groupaction;
    }

    /**
     * Set colorhex
     *
     * @param string $colorhex
     *
     * @return InfoActiontype
     */
    public function setColorhex($colorhex)
    {
        $this->colorhex = $colorhex;

        return $this;
    }

    /**
     * Get colorhex
     *
     * @return string
     */
    public function getColorhex()
    {
        return $this->colorhex;
    }

    /**
     * Set isInvisibly.
     *
     * @param int|null $isInvisibly
     *
     * @return InfoActiontype
     */
    public function setIsInvisibly($isInvisibly = null)
    {
        $this->isInvisibly = $isInvisibly;

        return $this;
    }

    /**
     * Get isInvisibly.
     *
     * @return int|null
     */
    public function getIsInvisibly()
    {
        return $this->isInvisibly;
    }

    /**
     * Set language
     *
     * @param InfoLanguage|null $content
     * @return InfoActiontype
     */
    public function setLanguage(InfoLanguage $language = null)
    {
        $this->language = $language;

        return $this;
    }

    /**
     * Get language
     *
     * @return InfoLanguage
     */
    public function getLanguage()
    {
        return $this->language;
    }
}
