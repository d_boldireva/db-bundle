<?php

namespace TeamSoft\CrmRepositoryBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * InfoTaskPreparationDtDose
 *
 * @ORM\Table(name="info_taskpreparationdtdose")
 * @ORM\Entity
 */
class InfoTaskPreparationDtDose implements ServiceFieldInterface
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var InfoPreparationDtDose|null
     *
     * @ORM\OneToOne(targetEntity="InfoPreparationDtDose", inversedBy="taskPreparationDtDose")
     * @ORM\JoinColumns({
     *      @ORM\JoinColumn(name="preparationdtdose_id", referencedColumnName="id")
     * })
     */
    private $preparationDtDose;

    /**
     * @var InfoTask|null
     *
     * @ORM\ManyToOne(targetEntity="InfoTask", inversedBy="taskPreparationDtDoses")
     * @ORM\JoinColumn(name="task_id", referencedColumnName="id")
     */
    private $task;

    /**
     * @var InfoTaskPreparationDtDoseFile|null
     * @ORM\OneToOne(targetEntity="InfoTaskPreparationDtDoseFile", mappedBy="taskPreparationDtDose")
     */
    private $file;

    /**
     * @var int|null
     * @ORM\Column(name="cnt", type="integer", nullable=true)
     */
    private $cnt;


    /**
     * @var \DateTime|null
     * @ORM\Column(name="createdate", type="datetime", nullable=true)
     */
    private $createDate;

    /**
     * @var string|null
     *
     * @ORM\Column(name="guid", type="guid", nullable=true)
     */
    private $guid;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="currenttime", type="datetime", nullable=true)
     */
    private $currenttime;

    /**
     * @var string|null
     *
     * @ORM\Column(name="moduser", type="string", length=16, nullable=true)
     */
    private $moduser;

    /**
     * @return int
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return InfoPreparationDtDose|null
     */
    public function getPreparationDtDose(): ?InfoPreparationDtDose
    {
        return $this->preparationDtDose;
    }

    /**
     * @param InfoPreparationDtDose|null $preparationDtDose
     * @return self
     */
    public function setPreparationDtDose(?InfoPreparationDtDose $preparationDtDose): self
    {
        $this->preparationDtDose = $preparationDtDose;

        return $this;
    }

    /**
     * @return InfoTask|null
     */
    public function getTask(): ?InfoTask
    {
        return $this->task;
    }

    /**
     * @param InfoTask|null $task
     * @return self
     */
    public function setTask(?InfoTask $task): self
    {
        $this->task = $task;
        return $this;
    }

    /**
     * @return int|null
     */
    public function getCnt(): ?int
    {
        return $this->cnt;
    }

    /**
     * @param int|null $cnt
     * @return self
     */
    public function setCnt(?int $cnt): self
    {
        $this->cnt = $cnt;
        return $this;
    }

    /**
     * @return \DateTime|null
     */
    public function getCreateDate(): ?\DateTime
    {
        return $this->createDate;
    }

    /**
     * @param \DateTime|null $createDate
     * @return self
     */
    public function setCreateDate(?\DateTime $createDate): self
    {
        $this->createDate = $createDate;
        return $this;
    }

    /**
     * @return InfoTaskPreparationDtDoseFile|null
     */
    public function getFile(): ?InfoTaskPreparationDtDoseFile
    {
        return $this->file;
    }

    /**
     * @param InfoTaskPreparationDtDoseFile|null $file
     * @return self
     */
    public function setFile(?InfoTaskPreparationDtDoseFile $file): self
    {
        $this->file = $file;
        $file->setTaskPreparationDtDose($this);

        return $this;
    }

    /**
     * @return string|null
     */
    public function getGuid(): ?string
    {
        return $this->guid;
    }

    /**
     * @param string|null $guid
     * @return self
     */
    public function setGuid($guid): self
    {
        $this->guid = $guid;
        return $this;
    }

    /**
     * @return \DateTime|null
     */
    public function getCurrenttime(): ?\DateTime
    {
        return $this->currenttime;
    }

    /**
     * @param \DateTime|null $currenttime
     * @return self
     */
    public function setCurrenttime(?\DateTime $currenttime): self
    {
        $this->currenttime = $currenttime;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getModuser(): ?string
    {
        return $this->moduser;
    }

    /**
     * @param string|null $moduser
     * @return self
     */
    public function setModuser($moduser): self
    {
        $this->moduser = $moduser;
        return $this;
    }
}
