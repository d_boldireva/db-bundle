<?php

namespace TeamSoft\CrmRepositoryBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * InfoMcmdistributiontarget
 *
 * @ORM\Table(name="`")
 * @ORM\Entity
 */
class InfoMcmdistributiontarget implements ServiceFieldInterface
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="\TeamSoft\CrmRepositoryBundle\Entity\InfoMcmdistribution", cascade={"remove"})
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="mcmdistribution_id", referencedColumnName="id")
     * })
     */
    private $mcmdistribution;

    /**
     * @ORM\ManyToOne(targetEntity="\TeamSoft\CrmRepositoryBundle\Entity\InfoMcmtarget", cascade={"remove"})
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="mcmtarget_id", referencedColumnName="id")
     * })
     */
    private $mcmtarget;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="currenttime", type="datetime", nullable=true)
     */
    private $currenttime;

    /**
     * @var string
     *
     * @ORM\Column(name="moduser", type="string", length=16, nullable=true)
     */
    private $moduser;

    /**
     * @var string
     *
     * @ORM\Column(name="guid", type="guid", nullable=true)
     */
    private $guid;



    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set mcmdistribution
     *
     * @param \TeamSoft\CrmRepositoryBundle\Entity\InfoMcmdistribution $mcmdistribution
     *
     * @return InfoMcmdistributiontarget
     */
    public function setMcmdistribution($mcmdistribution)
    {
        $this->mcmdistribution = $mcmdistribution;
    
        return $this;
    }

    /**
     * Get mcmdistribution
     *
     * @return \TeamSoft\CrmRepositoryBundle\Entity\InfoMcmdistribution
     */
    public function getMcmdistribution()
    {
        return $this->mcmdistribution;
    }

    /**
     * Set mcmtarget
     *
     * @param \TeamSoft\CrmRepositoryBundle\Entity\InfoMcmtarget $mcmtarget
     *
     * @return InfoMcmdistributiontarget
     */
    public function setMcmtarget($mcmtarget)
    {
        $this->mcmtarget = $mcmtarget;
    
        return $this;
    }

    /**
     * Get mcmtarget
     *
     * @return \TeamSoft\CrmRepositoryBundle\Entity\InfoMcmtarget
     */
    public function getMcmtarget()
    {
        return $this->mcmtarget;
    }

    /**
     * Set currenttime
     *
     * @param \DateTime $currenttime
     *
     * @return InfoMcmdistributiontarget
     */
    public function setCurrenttime(\DateTime $currenttime)
    {
        $this->currenttime = $currenttime;
    
        return $this;
    }

    /**
     * Get currenttime
     *
     * @return \DateTime
     */
    public function getCurrenttime()
    {
        return $this->currenttime;
    }

    /**
     * Set moduser
     *
     * @param string $moduser
     *
     * @return InfoMcmdistributiontarget
     */
    public function setModuser($moduser)
    {
        $this->moduser = $moduser;
    
        return $this;
    }

    /**
     * Get moduser
     *
     * @return string
     */
    public function getModuser()
    {
        return $this->moduser;
    }

    /**
     * Set guid
     *
     * @param string $guid
     *
     * @return InfoMcmdistributiontarget
     */
    public function setGuid($guid)
    {
        $this->guid = $guid;
    
        return $this;
    }

    /**
     * Get guid
     *
     * @return string
     */
    public function getGuid()
    {
        return $this->guid;
    }
}
