<?php

namespace TeamSoft\CrmRepositoryBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * InfoPlancompanytask
 *
 * @ORM\Table(name="info_plancompanytask")
 * @ORM\Entity(repositoryClass="TeamSoft\CrmRepositoryBundle\Repository\InfoPlancompanytask")
 */
class InfoPlancompanytask implements ServiceFieldInterface
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dt", type="datetime", nullable=true)
     */
    private $dt;

    /**
     * @var int
     *
     * @ORM\Column(name="tasks", type="integer", nullable=true)
     */
    private $tasks;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="currenttime", type="datetime", nullable=true)
     */
    private $currenttime;

    /**
     * @var guid
     *
     * @ORM\Column(name="guid", type="guid", nullable=true)
     */
    private $guid;

    /**
     * @var string
     *
     * @ORM\Column(name="moduser", type="string", length=16, nullable=true)
     */
    private $moduser;

    /**
     * @var \InfoCompany
     *
     * @ORM\ManyToOne(targetEntity="InfoCompany")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="company_id", referencedColumnName="id")
     * })
     */
    private $company;

    /**
     * @var \InfoUser
     *
     * @ORM\ManyToOne(targetEntity="InfoUser")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     * })
     */
    private $user;

    /**
     * @var int
     *
     * @ORM\Column(name="approvementstatus", type="integer", nullable=true)
     */
    private $approvementstatus;

    /**
     * @var string
     *
     * @ORM\Column(name="comment", type="string", length=255, nullable=true)
     */
    private $comment;

    /**
     * @var InfoCompanycategory
     *
     * @ORM\ManyToOne(targetEntity="InfoCompanycategory")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="category_id", referencedColumnName="id")
     * })
     */
    private $category;

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set dt
     *
     * @param \DateTime $dt
     *
     * @return InfoPlancompanytask
     */
    public function setDt($dt)
    {
        $this->dt = $dt;
    
        return $this;
    }

    /**
     * Get dt
     *
     * @return \DateTime
     */
    public function getDt()
    {
        return $this->dt;
    }

    /**
     * Set tasks
     *
     * @param int $tasks
     *
     * @return InfoPlancompanytask
     */
    public function setTasks($tasks)
    {
        $this->tasks = $tasks;
    
        return $this;
    }

    /**
     * Get tasks
     *
     * @return int
     */
    public function getTasks()
    {
        return $this->tasks;
    }

    /**
     * Set currenttime
     *
     * @param \DateTime $currenttime
     *
     * @return InfoPlancompanytask
     */
    public function setCurrenttime(\DateTime$currenttime)
    {
        $this->currenttime = $currenttime;
    
        return $this;
    }

    /**
     * Get currenttime
     *
     * @return \DateTime
     */
    public function getCurrenttime()
    {
        return $this->currenttime;
    }

    /**
     * Set guid
     *
     * @param guid $guid
     *
     * @return InfoPlancompanytask
     */
    public function setGuid($guid)
    {
        $this->guid = $guid;
    
        return $this;
    }

    /**
     * Get guid
     *
     * @return guid
     */
    public function getGuid()
    {
        return $this->guid;
    }

    /**
     * Set moduser
     *
     * @param string $moduser
     *
     * @return InfoPlancompanytask
     */
    public function setModuser($moduser)
    {
        $this->moduser = $moduser;
    
        return $this;
    }

    /**
     * Get moduser
     *
     * @return string
     */
    public function getModuser()
    {
        return $this->moduser;
    }

    /**
     * Set company
     *
     * @param \TeamSoft\CrmRepositoryBundle\Entity\InfoCompany $company
     *
     * @return InfoPlancompanytask
     */
    public function setCompany(\TeamSoft\CrmRepositoryBundle\Entity\InfoCompany $company = null)
    {
        $this->company = $company;
    
        return $this;
    }

    /**
     * Get company
     *
     * @return \TeamSoft\CrmRepositoryBundle\Entity\InfoCompany
     */
    public function getCompany()
    {
        return $this->company;
    }

    /**
     * Set user
     *
     * @param \TeamSoft\CrmRepositoryBundle\Entity\InfoUser $user
     *
     * @return InfoPlancompanytask
     */
    public function setUser(\TeamSoft\CrmRepositoryBundle\Entity\InfoUser $user = null)
    {
        $this->user = $user;
    
        return $this;
    }

    /**
     * Get user
     *
     * @return \TeamSoft\CrmRepositoryBundle\Entity\InfoUser
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Set approvementstatus
     *
     * @param int $approvementstatus
     *
     * @return InfoPlancompanytask
     */
    public function setApprovementstatus($approvementstatus)
    {
        $this->approvementstatus = $approvementstatus;
    
        return $this;
    }

    /**
     * Get approvementstatus
     *
     * @return int
     */
    public function getApprovementstatus()
    {
        return $this->approvementstatus;
    }

    /**
     * Set comment
     *
     * @param string $comment
     * @return InfoPlancompanytask
     */
    public function setComment($comment)
    {
        $this->comment = $comment;

        return $this;
    }

    /**
     * Get comment
     *
     * @return string
     */
    public function getComment()
    {
        return $this->comment;
    }

    /**
     * Set user
     *
     * @param InfoCompanycategory|null $category
     * @return InfoPlancompanytask
     */
    public function setCategory(InfoCompanycategory $category = null)
    {
        $this->category = $category;

        return $this;
    }

    /**
     * Get user
     *
     * @return \TeamSoft\CrmRepositoryBundle\Entity\InfoCompanycategory
     */
    public function getCategory()
    {
        return $this->category;
    }
}
