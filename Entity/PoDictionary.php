<?php

namespace TeamSoft\CrmRepositoryBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * PoDictionary
 *
 * @ORM\Table(name="po_dictionary")
 * @ORM\Entity(repositoryClass="TeamSoft\CrmRepositoryBundle\Repository\PoDictionary")
 */
class PoDictionary implements ServiceFieldInterface, SecureEntityInterface
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var int|null
     *
     * @ORM\Column(name="identifier", type="integer", nullable=true)
     */
    private $identifier;

    /**
     * Many dictionaries have one group
     * @ORM\ManyToOne(targetEntity="PoDictionarygroup", inversedBy="id")
     * @ORM\JoinColumn(name="group_id", referencedColumnName="id")
     */
    private $group;

    /**
     * @var string|null
     *
     * @ORM\Column(name="name", type="string", length=255, nullable=true)
     */
    private $name;

    private $nameTranslation;

    /**
     * @var string|null
     *
     * @ORM\Column(name="tablename", type="string", length=255, nullable=true)
     */
    private $tablename;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="currenttime", type="datetime", nullable=true)
     */
    private $currenttime;

    /**
     * @var string|null
     *
     * @ORM\Column(
     *     name="moduser",
     *     type="string",
     *     length=16,
     *     nullable=true,
     *     options={"default"="[dbo].[Get_CurrentCode]()"}
     * )
     */
    private $moduser;

    /**
     * @var string|null
     *
     * @ORM\Column(name="guid", type="guid", nullable=true)
     */
    private $guid;

    /**
     * @var InfoSystemdictionary
     *
     * @ORM\OneToOne(targetEntity="InfoSystemdictionary", inversedBy="poDictionary")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="systemdictionary_id", referencedColumnName="id")
     * })
     */
    private $systemDictionary;

    /**
     * @var InfoCustomdictionary
     *
     * @ORM\ManyToOne(targetEntity="InfoCustomdictionary")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="CustomDictionary_id", referencedColumnName="id")
     * })
     */
    private $customDictionary;

    /**
     * @var ArrayCollection|InfoRole[]
     *
     * @ORM\ManyToMany(targetEntity="InfoRole")
     * @ORM\JoinTable(name="po_dictionarybyrole",
     *   joinColumns={@ORM\JoinColumn(name="dictionary_id", referencedColumnName="id")},
     *   inverseJoinColumns={@ORM\JoinColumn(name="role_id", referencedColumnName="id")}
     * )
     */
    private $roles;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->roles = new ArrayCollection();
    }


    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set identifier.
     *
     * @param int|null $identifier
     *
     * @return PoDictionary
     */
    public function setIdentifier($identifier = null)
    {
        $this->identifier = $identifier;

        return $this;
    }

    /**
     * Get identifier.
     *
     * @return int|null
     */
    public function getIdentifier()
    {
        return $this->identifier;
    }

    /**
     * Set group
     *
     * @param PoDictionarygroup $group
     *
     * @return PoDictionary
     */
    public function setGroup($group = null)
    {
        $this->group = $group;

        return $this;
    }

    /**
     * Get group
     *
     * @return PoDictionarygroup
     */
    public function getGroup()
    {
        return $this->group;
    }

    /**
     * Set name.
     *
     * @param string|null $name
     *
     * @return PoDictionary
     */
    public function setName($name = null)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name.
     *
     * @return string|null
     */
    public function getName()
    {
        return $this->name;
    }

    public function setNameTranslation(?string $nameTranslation): self
    {
        $this->nameTranslation = $nameTranslation;

        return $this;
    }

    public function getNameTranslation(): ?string
    {
        return $this->nameTranslation;
    }

    /**
     * Set tablename.
     *
     * @param string|null $tablename
     *
     * @return PoDictionary
     */
    public function setTablename($tablename = null)
    {
        $this->tablename = $tablename;

        return $this;
    }

    /**
     * Get tablename.
     *
     * @return string|null
     */
    public function getTablename()
    {
        return $this->tablename;
    }

    /**
     * Set currenttime.
     *
     * @param \DateTime|null $currenttime
     *
     * @return PoDictionary
     */
    public function setCurrenttime(\DateTime $currenttime = null)
    {
        $this->currenttime = $currenttime;

        return $this;
    }

    /**
     * Get currenttime.
     *
     * @return \DateTime|null
     */
    public function getCurrenttime()
    {
        return $this->currenttime;
    }

    /**
     * Set moduser.
     *
     * @param string|null $moduser
     *
     * @return PoDictionary
     */
    public function setModuser($moduser = null)
    {
        $this->moduser = $moduser;

        return $this;
    }

    /**
     * Get moduser.
     *
     * @return string|null
     */
    public function getModuser()
    {
        return $this->moduser;
    }

    /**
     * Set guid.
     *
     * @param string|null $guid
     *
     * @return PoDictionary
     */
    public function setGuid($guid = null)
    {
        $this->guid = $guid;

        return $this;
    }

    /**
     * Get guid.
     *
     * @return string|null
     */
    public function getGuid()
    {
        return $this->guid;
    }

    /**
     * Set systemDictionary
     *
     * @param InfoSystemdictionary $systemDictionary
     * @return PoDictionary
     */
    public function setSystemDictionary(InfoSystemdictionary $systemDictionary = null)
    {
        $this->systemDictionary = $systemDictionary;

        return $this;
    }

    /**
     * Get systemDictionary
     *
     * @return InfoSystemdictionary
     */
    public function getSystemDictionary()
    {
        return $this->systemDictionary;
    }

    /**
     * Set customDictionary
     *
     * @param InfoCustomdictionary|null $customDictionary
     * @return PoDictionary
     */
    public function setCustomDictionary(?InfoCustomdictionary $customDictionary): self
    {
        $this->customDictionary = $customDictionary;

        return $this;
    }

    /**
     * Get customDictionary
     *
     * @return InfoCustomdictionary|null
     */
    public function getCustomDictionary(): ?InfoCustomdictionary
    {
        return $this->customDictionary;
    }

    /**
     * Get roles
     *
     * @return ArrayCollection|InfoRole[]
     */
    public function getRoles()
    {
        return $this->roles;
    }

    /**
     * Add role
     *
     * @param InfoRole $infoRole
     *
     * @return PoDictionary
     */
    public function addRole(InfoRole $infoRole)
    {
        $this->roles->add($infoRole);

        return $this;
    }
}
