<?php

namespace TeamSoft\CrmRepositoryBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * InfoAddinfocontacttype
 *
 * @ORM\Table(name="info_addinfocontacttype")
 * @ORM\Entity
 */
class InfoAddinfocontacttype implements ServiceFieldInterface
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="guid", type="guid", nullable=true)
     */
    private $guid;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="currenttime", type="datetime", nullable=true)
     */
    private $currenttime;

    /**
     * @var string
     *
     * @ORM\Column(name="moduser", type="string", length=16, nullable=true)
     */
    private $moduser;

    /**
     * @var InfoContacttype
     *
     * @ORM\ManyToOne(targetEntity="InfoContacttype")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="SubjType_id", referencedColumnName="id")
     * })
     */
    private $subjtype;

    /**
     * @var InfoAddinfotype
     *
     * @ORM\ManyToOne(targetEntity="InfoAddinfotype", inversedBy="addInfoContactTypes")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="AddInfoType_id", referencedColumnName="id")
     * })
     */
    private $addinfotype;

    /**
     * @var InfoRole
     *
     * @ORM\ManyToOne(targetEntity="InfoRole")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="role_id", referencedColumnName="id")
     * })
     */
    private $role;

    public function __clone()
    {
        $this->id = null;
        $this->guid = null;
        $this->currenttime = null;
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set guid
     *
     * @param string $guid
     *
     * @return InfoAddinfocontacttype
     */
    public function setGuid($guid)
    {
        $this->guid = $guid;

        return $this;
    }

    /**
     * Get guid
     *
     * @return string
     */
    public function getGuid()
    {
        return $this->guid;
    }

    /**
     * Set currenttime
     *
     * @param \DateTime $currenttime
     *
     * @return InfoAddinfocontacttype
     */
    public function setCurrenttime(\DateTime $currenttime)
    {
        $this->currenttime = $currenttime;

        return $this;
    }

    /**
     * Get currenttime
     *
     * @return \DateTime
     */
    public function getCurrenttime()
    {
        return $this->currenttime;
    }

    /**
     * Set moduser
     *
     * @param string $moduser
     *
     * @return InfoAddinfocontacttype
     */
    public function setModuser($moduser)
    {
        $this->moduser = $moduser;

        return $this;
    }

    /**
     * Get moduser
     *
     * @return string
     */
    public function getModuser()
    {
        return $this->moduser;
    }

    /**
     * Set subjtype
     *
     * @param \TeamSoft\CrmRepositoryBundle\Entity\InfoContacttype $subjtype
     *
     * @return InfoAddinfocontacttype
     */
    public function setSubjtype(\TeamSoft\CrmRepositoryBundle\Entity\InfoContacttype $subjtype = null)
    {
        $this->subjtype = $subjtype;

        return $this;
    }

    /**
     * Get subjtype
     *
     * @return \TeamSoft\CrmRepositoryBundle\Entity\InfoContacttype
     */
    public function getSubjtype()
    {
        return $this->subjtype;
    }

    /**
     * Set addinfotype
     *
     * @param \TeamSoft\CrmRepositoryBundle\Entity\InfoAddinfotype $addinfotype
     *
     * @return InfoAddinfocontacttype
     */
    public function setAddinfotype(\TeamSoft\CrmRepositoryBundle\Entity\InfoAddinfotype $addinfotype = null)
    {
        $this->addinfotype = $addinfotype;

        return $this;
    }

    /**
     * Get addinfotype
     *
     * @return \TeamSoft\CrmRepositoryBundle\Entity\InfoAddinfotype
     */
    public function getAddinfotype()
    {
        return $this->addinfotype;
    }

    /**
     * Set role
     *
     * @param InfoRole $role
     *
     * @return InfoAddinfocontacttype
     */
    public function setRole(InfoRole $role = null)
    {
        $this->role = $role;

        return $this;
    }

    /**
     * Get role
     *
     * @return InfoRole
     */
    public function getRole()
    {
        return $this->role;
    }
}
