<?php

namespace TeamSoft\CrmRepositoryBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use TeamSoft\CrmRepositoryBundle\Entity\Etms\InfoEtmsmapsversion;

/**
 * InfoDirection
 *
 * @ORM\Table(name="info_direction")
 * @ORM\Entity
 */
class InfoDirection
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255, nullable=true)
     */
    private $name;

    /**
     * @var guid
     *
     * @ORM\Column(name="guid", type="guid", nullable=true)
     */
    private $guid;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="currenttime", type="datetime", nullable=true)
     */
    private $currenttime;

    /**
     * @var string
     *
     * @ORM\Column(name="moduser", type="string", length=16, nullable=true)
     */
    private $moduser;

    /**
     * @var InfoUser
     *
     * @ORM\ManyToMany(targetEntity="InfoUser")
     * @ORM\JoinTable(name="info_userdirection",
     *     joinColumns={@ORM\JoinColumn(name="direction_id", referencedColumnName="id")},
     *     inverseJoinColumns={@ORM\JoinColumn(name="user_id", referencedColumnName="id")}
     * )
     */
    private $users;

    /**
     * Map version collection (ETMS)
     *
     * @var ArrayCollection<TeamSoft\CrmRepositoryBundle\Entity\Etms\InfoEtmsmapsversion>
     *
     * @ORM\OneToMany(targetEntity="TeamSoft\CrmRepositoryBundle\Entity\Etms\InfoEtmsmapsversion", mappedBy="direction")
     */
    private $mapVersionCollection;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->users = new ArrayCollection();
        $this->mapVersionCollection = new ArrayCollection(); //(ETMS)
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return InfoDirection
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set guid
     *
     * @param guid $guid
     *
     * @return InfoDirection
     */
    public function setGuid($guid)
    {
        $this->guid = $guid;

        return $this;
    }

    /**
     * Get guid
     *
     * @return guid
     */
    public function getGuid()
    {
        return $this->guid;
    }

    /**
     * Set currenttime
     *
     * @param \DateTime $currenttime
     *
     * @return InfoDirection
     */
    public function setCurrenttime($currenttime)
    {
        $this->currenttime = $currenttime;

        return $this;
    }

    /**
     * Get currenttime
     *
     * @return \DateTime
     */
    public function getCurrenttime()
    {
        return $this->currenttime;
    }

    /**
     * Set moduser
     *
     * @param string $moduser
     *
     * @return InfoDirection
     */
    public function setModuser($moduser)
    {
        $this->moduser = $moduser;

        return $this;
    }

    /**
     * Get moduser
     *
     * @return string
     */
    public function getModuser()
    {
        return $this->moduser;
    }

    /**
     * Add users
     *
     * @param \TeamSoft\CrmRepositoryBundle\Entity\InfoUser $user
     *
     * @return InfoDirection
     */

    public function addUser(\TeamSoft\CrmRepositoryBundle\Entity\InfoUser $user)
    {
        $this->users[] = $user;

        return $this;
    }

    /**
     * Remove users
     *
     * @param \TeamSoft\CrmRepositoryBundle\Entity\InfoUser $user
     */
    public function removeUser(\TeamSoft\CrmRepositoryBundle\Entity\InfoUser $user)
    {
        $this->users->removeElement($user);
    }

    /**
     * Get users
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getUsers()
    {
        return $this->users;
    }

    /**
     * Add map version collection (ETMS)
     *
     * @param TeamSoft\CrmRepositoryBundle\Entity\Etms\InfoEtmsmapsversion $etmsmapsversion
     * @return $this
     */
    public function addMapVersionCollection(InfoEtmsmapsversion $etmsmapsversion)
    {
        $this->mapVersionCollection->add($etmsmapsversion);
        $etmsmapsversion->setDirection($this);
        return $this;
    }

    /**
     * Remove map version collection (ETMS)
     *
     * @param TeamSoft\CrmRepositoryBundle\Entity\Etms\InfoEtmsmapsversion $etmsmapsversion
     * @return $this
     */
    public function removeMapVersionCollection(InfoEtmsmapsversion $etmsmapsversion)
    {
        $this->mapVersionCollection->removeElement($etmsmapsversion);
        $etmsmapsversion->setDirection(null);
        return $this;
    }

    /**
     * Get map version collection (ETMS)
     *
     * @return ArrayCollection<TeamSoft\CrmRepositoryBundle\Entity\Etms\InfoEtmsmapsversion>
     */
    public function getMapVersionCollection()
    {
        return $this->mapVersionCollection;
    }
}
