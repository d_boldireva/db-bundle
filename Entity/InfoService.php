<?php

namespace TeamSoft\CrmRepositoryBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * InfoService
 *
 * @ORM\Table(name="info_service")
 * @ORM\Entity
 */
class InfoService implements ServiceFieldInterface
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string|null
     *
     * @ORM\Column(name="identifier", type="string", length=50, nullable=true)
     */
    private $identifier;

    /**
     * @var int|null
     *
     * @ORM\Column(name="enable", type="integer", nullable=true)
     */
    private $enable;

    /**
     * @var string|null
     *
     * @ORM\Column(name="name", type="string", length=50, nullable=true)
     */
    private $name;

    /**
     * @var string|null
     *
     * @ORM\Column(name="description", type="string", length=255, nullable=true)
     */
    private $description;

    /**
     * @var string|null
     *
     * @ORM\Column(name="guid", type="guid", nullable=true)
     */
    private $guid;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="currenttime", type="datetime", nullable=true)
     */
    private $currenttime;

    /**
     * @var string|null
     *
     * @ORM\Column(name="moduser", type="string", length=16, nullable=true)
     */
    private $moduser;

    /**
     * @ORM\OneToOne(targetEntity="PoWebservice", mappedBy="service")
     */
    private $webService;

    /**
     * @ORM\OneToMany(targetEntity="InfoServiceprivilege", mappedBy="service")
     */
    private $privileges;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->privileges = new ArrayCollection();
    }

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set identifier.
     *
     * @param string|null $identifier
     *
     * @return InfoService
     */
    public function setIdentifier($identifier = null)
    {
        $this->identifier = $identifier;

        return $this;
    }

    /**
     * Get identifier.
     *
     * @return string|null
     */
    public function getIdentifier()
    {
        return $this->identifier;
    }

    /**
     * Set enable.
     *
     * @param int|null $enable
     *
     * @return InfoService
     */
    public function setEnable($enable = null)
    {
        $this->enable = $enable;

        return $this;
    }

    /**
     * Get enable.
     *
     * @return int|null
     */
    public function getEnable()
    {
        return $this->enable;
    }

    /**
     * Set name.
     *
     * @param string|null $name
     *
     * @return InfoService
     */
    public function setName($name = null)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name.
     *
     * @return string|null
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set description.
     *
     * @param string|null $description
     *
     * @return InfoService
     */
    public function setDescription($description = null)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description.
     *
     * @return string|null
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set guid.
     *
     * @param string|null $guid
     *
     * @return InfoService
     */
    public function setGuid($guid = null)
    {
        $this->guid = $guid;

        return $this;
    }

    /**
     * Get guid.
     *
     * @return string|null
     */
    public function getGuid()
    {
        return $this->guid;
    }

    /**
     * Set currenttime.
     *
     * @param \DateTime|null $currenttime
     *
     * @return InfoService
     */
    public function setCurrenttime(\DateTime $currenttime = null)
    {
        $this->currenttime = $currenttime;

        return $this;
    }

    /**
     * Get currenttime.
     *
     * @return \DateTime|null
     */
    public function getCurrenttime()
    {
        return $this->currenttime;
    }

    /**
     * Set moduser.
     *
     * @param string|null $moduser
     *
     * @return InfoService
     */
    public function setModuser($moduser = null)
    {
        $this->moduser = $moduser;

        return $this;
    }

    /**
     * Get moduser.
     *
     * @return string|null
     */
    public function getModuser()
    {
        return $this->moduser;
    }

    /**
     * Set webWebService
     *
     * @param PoWebservice $webService
     *
     * @return InfoService
     */
    public function setWebService(PoWebservice $webService = null)
    {
        $this->webService = $webService;

        return $this;
    }

    /**
     * Get webService
     *
     * @return PoWebservice
     */
    public function getWebService()
    {
        return $this->webService;
    }

    /**
     * Add privilege
     *
     * @param InfoServiceprivilege $privilege
     *
     * @return InfoRole
     */
    public function addPrivilege(InfoServiceprivilege $privilege)
    {
        $this->privileges->add($privilege);

        return $this;
    }

    /**
     * Remove privilege
     *
     * @param InfoServiceprivilege $privilege
     */
    public function removePrivilege(InfoServiceprivilege $privilege)
    {
        $this->privileges->removeElement($privilege);
    }

    /**
     * Get privileges
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getPrivileges()
    {
        return $this->privileges;
    }
}

