<?php

namespace TeamSoft\CrmRepositoryBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * InfoPlanpreparation
 *
 * @ORM\Table(name="info_planpreparation")
 * @ORM\Entity(repositoryClass="TeamSoft\CrmRepositoryBundle\Repository\InfoPlanpreparation")
 */
class InfoPlanpreparation implements ServiceFieldInterface
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var guid
     *
     * @ORM\Column(name="guid", type="guid", nullable=true)
     */
    private $guid;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="currenttime", type="datetime", nullable=true)
     */
    private $currenttime;

    /**
     * @var string
     *
     * @ORM\Column(name="moduser", type="string", length=16, nullable=true)
     */
    private $moduser;

    /**
     * @var string
     *
     * @ORM\Column(name="descr", type="varchar_or_n_varchar_image_or_string", nullable=true)
     */
    private $descr;

    /**
     * @var int
     *
     * @ORM\Column(name="Count", type="integer", nullable=true)
     */
    private $count;

    /**
     * @var int
     *
     * @ORM\Column(name="ntask", type="integer", nullable=true)
     */
    private $ntask;

    /**
     * @var int
     *
     * @ORM\Column(name="plancnt", type="integer", nullable=true)
     */
    private $plancnt;

    /**
     * @var \InfoPrepline
     *
     * @ORM\ManyToOne(targetEntity="InfoPrepline")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="prepline_id", referencedColumnName="id")
     * })
     */
    private $prepline;

    /**
     * @var \InfoPreparationbrend
     *
     * @ORM\ManyToOne(targetEntity="InfoPreparationbrend")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="brend_id", referencedColumnName="id")
     * })
     */
    private $brend;

    /**
     * @var InfoPlandetail
     *
     * @ORM\ManyToOne(targetEntity="InfoPlandetail", inversedBy="planPreparations")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="plandetail_id", referencedColumnName="id")
     * })
     */
    private $plandetail;

    /**
     * @var string
     *
     * @ORM\Column(name="strplancount", type="string", nullable=true)
     */
    private $strPlanCount;

    /**
     * @var string
     *
     * @ORM\Column(name="strpotential", type="string", nullable=true)
     */
    private $strPotential;

    /**
     * @var string
     *
     * @ORM\Column(name="strfactcount", type="string", nullable=true)
     */
    private $strFactCount;

    public function __clone()
    {
        $this->id = null;
        $this->guid = null;
        $this->currenttime = null;
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set guid
     *
     * @param guid $guid
     *
     * @return InfoPlanpreparation
     */
    public function setGuid($guid)
    {
        $this->guid = $guid;

        return $this;
    }

    /**
     * Get guid
     *
     * @return guid
     */
    public function getGuid()
    {
        return $this->guid;
    }

    /**
     * Set currenttime
     *
     * @param \DateTime $currenttime
     *
     * @return InfoPlanpreparation
     */
    public function setCurrenttime(\DateTime $currenttime = null)
    {
        $this->currenttime = $currenttime;

        return $this;
    }

    /**
     * Get currenttime
     *
     * @return \DateTime
     */
    public function getCurrenttime()
    {
        return $this->currenttime;
    }

    /**
     * Set moduser
     *
     * @param string $moduser
     *
     * @return InfoPlanpreparation
     */
    public function setModuser($moduser)
    {
        $this->moduser = $moduser;

        return $this;
    }

    /**
     * Get moduser
     *
     * @return string
     */
    public function getModuser()
    {
        return $this->moduser;
    }

    /**
     * Set descr
     *
     * @param string $descr
     *
     * @return InfoPlanpreparation
     */
    public function setDescr($descr)
    {
        $this->descr = $descr;

        return $this;
    }

    /**
     * Get descr
     *
     * @return string
     */
    public function getDescr()
    {
        return $this->descr;
    }

    /**
     * Set count
     *
     * @param int $count
     *
     * @return InfoPlanpreparation
     */
    public function setCount($count)
    {
        $this->count = $count;

        return $this;
    }

    /**
     * Get count
     *
     * @return int
     */
    public function getCount()
    {
        return $this->count;
    }

    /**
     * Set ntask
     *
     * @param int $ntask
     *
     * @return InfoPlanpreparation
     */
    public function setNtask($ntask)
    {
        $this->ntask = $ntask;

        return $this;
    }

    /**
     * Get ntask
     *
     * @return int
     */
    public function getNtask()
    {
        return $this->ntask;
    }

    /**
     * Set plancnt
     *
     * @param int $plancnt
     *
     * @return InfoPlanpreparation
     */
    public function setPlancnt($plancnt)
    {
        $this->plancnt = $plancnt;

        return $this;
    }

    /**
     * Get plancnt
     *
     * @return int
     */
    public function getPlancnt()
    {
        return $this->plancnt;
    }

    /**
     *
     * @param string $strPlanCount
     *
     * @return InfoPlanpreparation
     */
    public function setStrPlanCount(?string $strPlanCount): self
    {
        $this->strPlanCount = $strPlanCount;

        return $this;
    }

    /**
     *
     * @return string
     */
    public function getStrPlanCount(): ?string
    {
        return $this->strPlanCount;
    }

    /**
     *
     * @param string $strPotential
     *
     * @return InfoPlanpreparation
     */
    public function setStrPotential(?string $strPotential): self
    {
        $this->strPotential = $strPotential;

        return $this;
    }

    /**
     *
     * @return string
     */
    public function getStrPotential(): ?string
    {
        return $this->strPotential;
    }

    /**
     *
     * @param string $strFactCount
     *
     * @return InfoPlanpreparation
     */
    public function setStrFactCount(?string $strFactCount): self
    {
        $this->strFactCount = $strFactCount;

        return $this;
    }

    /**
     *
     * @return string
     */
    public function getStrFactCount(): ?string
    {
        return $this->strFactCount;
    }

    /**
     * Set prepline
     *
     * @param \TeamSoft\CrmRepositoryBundle\Entity\InfoPrepline $prepline
     *
     * @return InfoPlanpreparation
     */
    public function setPrepline(\TeamSoft\CrmRepositoryBundle\Entity\InfoPrepline $prepline = null)
    {
        $this->prepline = $prepline;

        return $this;
    }

    /**
     * Get prepline
     *
     * @return \TeamSoft\CrmRepositoryBundle\Entity\InfoPrepline
     */
    public function getPrepline()
    {
        return $this->prepline;
    }

    /**
     * Set brend
     *
     * @param \TeamSoft\CrmRepositoryBundle\Entity\InfoPreparationbrend $brend
     *
     * @return InfoPlanpreparation
     */
    public function setBrend(\TeamSoft\CrmRepositoryBundle\Entity\InfoPreparationbrend $brend = null)
    {
        $this->brend = $brend;

        return $this;
    }

    /**
     * Get brend
     *
     * @return \TeamSoft\CrmRepositoryBundle\Entity\InfoPreparationbrend
     */
    public function getBrend()
    {
        return $this->brend;
    }

    /**
     * Set plandetail
     *
     * @param \TeamSoft\CrmRepositoryBundle\Entity\InfoPlandetail $plandetail
     *
     * @return InfoPlanpreparation
     */
    public function setPlandetail(\TeamSoft\CrmRepositoryBundle\Entity\InfoPlandetail $plandetail = null)
    {
        $this->plandetail = $plandetail;

        return $this;
    }

    /**
     * Get plandetail
     *
     * @return \TeamSoft\CrmRepositoryBundle\Entity\InfoPlandetail
     */
    public function getPlandetail()
    {
        return $this->plandetail;
    }

    /**
     * @return int|null
     */
    public function getBrandId()
    {
        return $this->getBrend() ? $this->getBrend()->getId() : null;
    }

    /**
     * @return int|null
     */
    public function getPreplineId()
    {
        return $this->getPrepline() ? $this->getPrepline()->getId() : null;
    }

    /**
     * @return int|null
     */
    public function getPlanDetailId()
    {
        return $this->getPlandetail() ? $this->getPlandetail()->getId() : null;
    }

}
