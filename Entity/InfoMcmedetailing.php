<?php

namespace TeamSoft\CrmRepositoryBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Class InfoMcmedetailing
 * @package TeamSoft\CrmRepositoryBundle\Entity
 *
 * @ORM\Table(name="info_mcmedetailing")
 * @ORM\Entity()
 */
class InfoMcmedetailing {
    /**
     * @var string
     *
     * @ORM\Column(name="id", type="guid", nullable=false)
     * @ORM\Id()
     */
    private $id;

    /**
     * @var InfoContact
     *
     * @ORM\ManyToOne(targetEntity="InfoContact")
     * @ORM\JoinColumn(name="contact_id", referencedColumnName="id")
     */
    private $contact;

    /**
     * @var InfoUser
     *
     * @ORM\ManyToOne(targetEntity="InfoUser", inversedBy="mcmedetailingCollection")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     */
    private $user;

    /**
     * @var InfoTask
     *
     * @ORM\ManyToOne(targetEntity="InfoTask")
     * @ORM\JoinColumn(name="task_id", referencedColumnName="id")
     */
    private $task;

    /**
     * @var string
     *
     * @ORM\Column(name="userlink", type="string", length=255, nullable=true)
     */
    private $userlink;

    /**
     * @var string
     *
     * @ORM\Column(name="contactlink", type="string", length=255, nullable=true)
     */
    private $contactlink;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="datefrom", type="datetime", nullable=true)
     */
    private $datefrom;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="datetill", type="datetime", nullable=true)
     */
    private $datetill;

    /**
     * @var int
     *
     * @ORM\Column(name="hascontactjoined", type="integer", nullable=true)
     */
    private $hascontactjoined;

    /**
     * @return string
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param string $id
     * @return $this
     */
    public function setId(string $id)
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return int|null
     */
    public function getContactId(){
        return $this->contact ? $this->contact->getId() : null;
    }

    /**
     * @return InfoContact
     */
    public function getContact(){
        return $this->contact;
    }

    /**
     * @param InfoContact $contact
     * @return $this
     */
    public function setContact(InfoContact $contact){
        $this->contact = $contact;
        return $this;
    }

    /**
     * @return int|null
     */
    public function getUserId(){
        return $this->user ? $this->user->getId() : null;
    }

    /**
     * @return InfoUser
     */
    public function getUser(){
        return $this->user;
    }

    /**
     * @param InfoUser $user
     * @return $this
     */
    public function setUser(InfoUser $user){
        $this->user = $user;
        return $this;
    }

    /**
     * @return int|null
     */
    public function getTaskId(){
        return $this->task ? $this->task->getId() : null;
    }

    /**
     * @return InfoTask
     */
    public function getTask(){
        return $this->task;
    }

    /**
     * @param InfoTask $task
     * @return $this
     */
    public function setTask(InfoTask $task){
        $this->task = $task;
        return $this;
    }

    /**
     * @return string
     */
    public function getContactlink(){
        return $this->contactlink;
    }

    /**
     * @param string $contactlink
     * @return $this
     */
    public function setContactlink($contactlink){
        $this->contactlink = $contactlink;
        return $this;
    }

    /**
     * @return string
     */
    public function getUserlink(){
        return $this->userlink;
    }

    /**
     * @param $userlink
     * @return $this
     */
    public function setUserlink($userlink){
        $this->userlink = $userlink;
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getDatefrom(){
        return $this->datefrom;
    }

    /**
     * @param \DateTime $datefrom
     * @return $this
     */
    public function setDatefrom(\DateTime $datefrom){
        $this->datefrom = $datefrom;
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getDatetill(){
        return $this->datetill;
    }

    /**
     * @param \DateTime $datetill
     * @return $this
     */
    public function setDatetill(\DateTime $datetill) {
        $this->datetill = $datetill;
        return $this;
    }

    /**
     * @return int
     */
    public function getHascontactjoined(){
        return $this->hascontactjoined;
    }

    /**
     * @param $hascontactjoined
     * @return $this
     */
    public function setHascontactjoined($hascontactjoined){
        $this->hascontactjoined = $hascontactjoined ? 1 : 0;
        return $this;
    }
}

