<?php

namespace TeamSoft\CrmRepositoryBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use TeamSoft\CrmRepositoryBundle\Entity\ServiceFieldInterface;

/**
 * InfoClmgroupdirection
 *
 * @ORM\Table(name="info_clmgroupdirection")
 * @ORM\Entity(repositoryClass="TeamSoft\CrmRepositoryBundle\Repository\InfoClmgroupdirection")
 */
class InfoClmgroupdirection implements ServiceFieldInterface
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var guid
     *
     * @ORM\Column(name="guid", type="guid", nullable=true)
     */
    private $guid;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="currenttime", type="datetime", nullable=true)
     */
    private $currenttime;

    /**
     * @var string
     *
     * @ORM\Column(name="moduser", type="string", length=16, nullable=true)
     */
    private $moduser;

    /**
     * @var \TeamSoft\CrmRepositoryBundle\Entity\InfoClmgroup
     *
     * @ORM\ManyToOne(targetEntity="InfoClmgroup", cascade={"remove"}, inversedBy="clmGroupdirection")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="clmgroup_id", referencedColumnName="id")
     * })
     */
    private $clmgroup;

    /**
     * @var \TeamSoft\CrmRepositoryBundle\Entity\InfoDirection
     *
     * @ORM\ManyToOne(targetEntity="\TeamSoft\CrmRepositoryBundle\Entity\InfoDirection")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="direction_id", referencedColumnName="id")
     * })
     */
    private $direction;



    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set guid
     *
     * @param string $guid
     */
    public function setGuid($guid)
    {
        $this->guid = $guid;
    }

    /**
     * Get guid
     *
     * @return string
     */
    public function getGuid()
    {
        return $this->guid;
    }

    /**
     * Set currenttime
     *
     * @param \DateTime $currenttime
     */
    public function setCurrenttime(\DateTime $currenttime)
    {
        $this->currenttime = $currenttime;
    }

    /**
     * Get currenttime
     *
     * @return \DateTime
     */
    public function getCurrenttime()
    {
        return $this->currenttime;
    }

    /**
     * Set moduser
     *
     * @param string $moduser
     */
    public function setModuser($moduser)
    {
        $this->moduser = $moduser;
    }

    /**
     * Get moduser
     *
     * @return string
     */
    public function getModuser()
    {
        return $this->moduser;
    }

    /**
     * Set clmgroup
     *
     * @param \TeamSoft\CrmRepositoryBundle\Entity\InfoClmgroup $clmgroup
     */
    public function setClmgroup(\TeamSoft\CrmRepositoryBundle\Entity\InfoClmgroup $clmgroup = null)
    {
        $this->clmgroup = $clmgroup;
    }

    /**
     * Get clmgroup
     *
     * @return \TeamSoft\CrmRepositoryBundle\Entity\InfoClmgroup
     */
    public function getClmgroup()
    {
        return $this->clmgroup;
    }

    /**
     * Set direction
     *
     * @param \TeamSoft\CrmRepositoryBundle\Entity\InfoDirection $direction
     *
     * @return InfoClmgroupdirection
     */
    public function setDirection(\TeamSoft\CrmRepositoryBundle\Entity\InfoDirection $direction = null)
    {
        $this->direction = $direction;
    
        return $this;
    }

    /**
     * Get direction
     *
     * @return \TeamSoft\CrmRepositoryBundle\Entity\InfoDirection
     */
    public function getDirection()
    {
        return $this->direction;
    }
}
