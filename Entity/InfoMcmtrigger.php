<?php

namespace TeamSoft\CrmRepositoryBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * InfoMcmtrigger
 *
 * @ORM\Table(name="info_mcmtrigger")
 * @ORM\Entity(repositoryClass="TeamSoft\CrmRepositoryBundle\Repository\InfoMcmtrigger")
 */
class InfoMcmtrigger implements ServiceFieldInterface, DateTimeWithGmtOffsetInterface
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string|null
     *
     * @ORM\Column(name="name", type="string", length=255, nullable=true)
     */
    private $name;

    /**
     * @var string|null
     *
     * @ORM\Column(name="action_type", type="string", length=255, nullable=true)
     */
    private $actionType;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="dt", type="datetime", nullable=true)
     */
    private $dt;

    /**
     * @var int|null
     *
     * @ORM\Column(name="is_active", type="integer", nullable=true)
     */
    private $isActive;

    /**
     * @var int|null
     *
     * @ORM\Column(name="resend_sms", type="integer", nullable=true)
     */
    private $resendSms;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="datefrom", type="datetime", nullable=true)
     */
    private $datefrom;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="datetill", type="datetime", nullable=true)
     */
    private $datetill;

    /**
     * @var int|null
     *
     * @ORM\Column(name="datecorrection", type="integer", nullable=true)
     */
    private $datecorrection;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="currenttime", type="datetime", nullable=true)
     */
    private $currenttime;

    /**
     * @var string|null
     *
     * @ORM\Column(name="moduser", type="string", length=16, nullable=true)
     */
    private $moduser;

    /**
     * @var string|null
     *
     * @ORM\Column(name="guid", type="guid", nullable=true)
     */
    private $guid;

    /**
     * @var InfoMcmtriggertype
     *
     * @ORM\ManyToOne(targetEntity="InfoMcmtriggertype")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="type_id", referencedColumnName="id")
     * })
     */
    private $type;

    /**
     * @var InfoMcmcontent
     *
     * @ORM\ManyToOne(targetEntity="InfoMcmcontent")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="mcmcontent_id", referencedColumnName="id")
     * })
     */
    private $mcmcontent;

    /**
     * @var InfoMcmfrom
     *
     * @ORM\ManyToOne(targetEntity="InfoMcmfrom")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="mcmfrom_id", referencedColumnName="id")
     * })
     */
    private $mcmfrom;

    /**
     * @var InfoMcmfrom
     *
     * @ORM\ManyToOne(targetEntity="InfoMcmfrom")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="resend_mcmfrom_id", referencedColumnName="id")
     * })
     */
    private $resendMcmfrom;

    /**
     * @var InfoMcmdistribution
     *
     * @ORM\ManyToOne(targetEntity="InfoMcmdistribution")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="mcmdistribution_id", referencedColumnName="id")
     * })
     */
    private $mcmdistribution;

    /**
     * @var InfoMcmconference
     *
     * @ORM\ManyToOne(targetEntity="InfoMcmconference")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="conference_id", referencedColumnName="id")
     * })
     */
    private $conference;

    /**
     * @var string
     *
     * @ORM\Column(name="footer_language", type="string", length=16, nullable=true)
     */
    private $footerLanguage;

    /**
     * @var integer
     *
     * @ORM\Column(name="gmt_offset", type="integer", nullable=true)
     */
    private $gmtOffset;

    /**
     * @var InfoCustomdictionaryvalue|null
     * @ORM\ManyToOne(targetEntity="InfoCustomdictionaryvalue")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="funnelstage_id", referencedColumnName="id")
     * })
     */
    private $funnelStage;

    /**
     * @var InfoUser|null
     *
     * @ORM\ManyToOne(targetEntity="InfoUser")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="owner_id", referencedColumnName="id")
     * })
     */
    private $owner;

    /**
     * Get id
     *
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string|null $name
     *
     * @return InfoMcmtrigger
     */
    public function setName(?string $name): self
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string|null
     */
    public function getName(): ?string
    {
        return $this->name;
    }

    /**
     * Set actionType
     *
     * @param string|null $actionType
     *
     * @return InfoMcmtrigger
     */
    public function setActionType(?string $actionType): self
    {
        $this->actionType = $actionType;

        return $this;
    }

    /**
     * Get actionType
     *
     * @return string|null
     */
    public function getActionType(): ?string
    {
        return $this->actionType;
    }

    /**
     * Set dt
     *
     * @param \DateTime|null $dt
     *
     * @return InfoMcmtrigger
     */
    public function setDt(?\DateTime $dt): self
    {
        $this->dt = $dt;

        return $this;
    }

    /**
     * Get dt
     *
     * @return \DateTime|null
     */
    public function getDt(): ?\DateTime
    {
        return $this->dt;
    }

    /**
     * Set isActive
     *
     * @param int|null $isActive
     *
     * @return InfoMcmtrigger
     */
    public function setIsActive(?int $isActive): self
    {
        $this->isActive = $isActive;

        return $this;
    }

    /**
     * Get isActive
     *
     * @return int|null
     */
    public function getIsActive(): ?int
    {
        return $this->isActive;
    }

    /**
     * Set resendSms
     *
     * @param int|null $resendSms
     *
     * @return InfoMcmtrigger
     */
    public function setResendSms(?int $resendSms): self
    {
        $this->resendSms = $resendSms;

        return $this;
    }

    /**
     * Get resendSms
     *
     * @return int|null
     */
    public function getResendSms(): ?int
    {
        return $this->resendSms;
    }

    /**
     * Set datefrom
     *
     * @param \DateTime|null $datefrom
     *
     * @return InfoMcmtrigger
     */
    public function setDatefrom(?\DateTime $datefrom): self
    {
        $this->datefrom = $datefrom;

        return $this;
    }

    /**
     * Get datefrom
     *
     * @return \DateTime|null
     */
    public function getDatefrom(): ?\DateTime
    {
        return $this->datefrom;
    }

    /**
     * Set datetill
     *
     * @param \DateTime|null $datetill
     *
     * @return InfoMcmtrigger
     */
    public function setDatetill(?\DateTime $datetill): self
    {
        $this->datetill = $datetill;

        return $this;
    }

    /**
     * Get datetill
     *
     * @return \DateTime|null
     */
    public function getDatetill(): ?\DateTime
    {
        return $this->datetill;
    }

    /**
     * Set datecorrection
     *
     * @param int|null $datecorrection
     *
     * @return InfoMcmtrigger
     */
    public function setDatecorrection(?int $datecorrection): self
    {
        $this->datecorrection = $datecorrection;

        return $this;
    }

    /**
     * Get datecorrection
     *
     * @return int|null
     */
    public function getDatecorrection(): ?int
    {
        return $this->datecorrection;
    }

    /**
     * Set currenttime
     *
     * @param \DateTime $currenttime
     *
     * @return InfoMcmtrigger
     */
    public function setCurrenttime(\DateTime $currenttime): self
    {
        $this->currenttime = $currenttime;

        return $this;
    }

    /**
     * Get currenttime
     *
     * @return \DateTime
     */
    public function getCurrenttime(): \DateTime
    {
        return $this->currenttime;
    }

    /**
     * Set moduser
     *
     * @param string|null $moduser
     *
     * @return InfoMcmtrigger
     */
    public function setModuser($moduser = null): self
    {
        $this->moduser = $moduser;

        return $this;
    }

    /**
     * Get moduser
     *
     * @return string|null
     */
    public function getModuser(): ?string
    {
        return $this->moduser;
    }

    /**
     * Set guid
     *
     * @param string|null $guid
     *
     * @return InfoMcmtrigger
     */
    public function setGuid($guid = null): self
    {
        $this->guid = $guid;

        return $this;
    }

    /**
     * Get guid
     *
     * @return string|null
     */
    public function getGuid(): ?string
    {
        return $this->guid;
    }

    /**
     * Set footerLanguage
     *
     * @param string|null $footerLanguage
     *
     * @return InfoMcmtrigger
     */
    public function setFooterLanguage(?string $footerLanguage): self
    {
        $this->footerLanguage = $footerLanguage;

        return $this;
    }

    /**
     * Get footerLanguage
     *
     * @return string|null
     */
    public function getFooterLanguage(): ?string
    {
        return $this->footerLanguage;
    }

    /**
     * Set type
     *
     * @param InfoMcmtriggertype|null $type
     *
     * @return InfoMcmtrigger
     */
    public function setType(?InfoMcmtriggertype $type): self
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type
     *
     * @return InfoMcmtriggertype|null
     */
    public function getType(): ?InfoMcmtriggertype
    {
        return $this->type;
    }

    /**
     * Set mcmcontent
     *
     * @param InfoMcmcontent|null $mcmcontent
     *
     * @return InfoMcmtrigger
     */
    public function setMcmcontent(?InfoMcmcontent $mcmcontent): self
    {
        $this->mcmcontent = $mcmcontent;

        return $this;
    }

    /**
     * Get mcmcontent
     *
     * @return InfoMcmcontent|null
     */
    public function getMcmcontent(): ?InfoMcmcontent
    {
        return $this->mcmcontent;
    }

    /**
     * Set mcmfrom
     *
     * @param InfoMcmfrom|null $mcmfrom
     *
     * @return InfoMcmtrigger
     */
    public function setMcmfrom(?InfoMcmfrom $mcmfrom): self
    {
        $this->mcmfrom = $mcmfrom;

        return $this;
    }

    /**
     * Get mcmfrom
     *
     * @return InfoMcmfrom|null
     */
    public function getMcmfrom(): ?InfoMcmfrom
    {
        return $this->mcmfrom;
    }

    /**
     * Set resendMcmfrom
     *
     * @param InfoMcmfrom|null $resendMcmfrom
     *
     * @return InfoMcmtrigger
     */
    public function setResendMcmfrom(?InfoMcmfrom $resendMcmfrom): self
    {
        $this->resendMcmfrom = $resendMcmfrom;

        return $this;
    }

    /**
     * Get resendMcmfrom
     *
     * @return InfoMcmfrom|null
     */
    public function getResendMcmfrom(): ?InfoMcmfrom
    {
        return $this->resendMcmfrom;
    }

    /**
     * Set mcmdistribution
     *
     * @param InfoMcmdistribution|null $mcmdistribution
     *
     * @return InfoMcmtrigger
     */
    public function setMcmdistribution(?InfoMcmdistribution $mcmdistribution): self
    {
        $this->mcmdistribution = $mcmdistribution;

        return $this;
    }

    /**
     * Get mcmdistribution
     *
     * @return InfoMcmdistribution|null
     */
    public function getMcmdistribution(): ?InfoMcmdistribution
    {
        return $this->mcmdistribution;
    }

    /**
     * Set gmtOffset
     *
     * @param int|null $gmtOffset
     *
     * @return InfoMcmtrigger
     */
    public function setGmtOffset(?int $gmtOffset): self
    {
        $this->gmtOffset = $gmtOffset;

        return $this;
    }

    /**
     * Get gmtOffset
     *
     * @return int|null
     */
    public function getGmtOffset(): ?int
    {
        return $this->gmtOffset;
    }

    public function getDateTimeWithGmtOffsetPropertyNameList()
    {
        return ['datefrom', 'datetill'];
    }

    /**
     * Set conference
     *
     * @param InfoMcmconference|null $conference
     *
     * @return InfoMcmtrigger
     */
    public function setConference(?InfoMcmconference $conference): self
    {
        $this->conference = $conference;

        return $this;
    }

    /**
     * Get conference
     *
     * @return InfoMcmconference|null
     */
    public function getConference(): ?InfoMcmconference
    {
        return $this->conference;
    }

    /**
     * Set funnel stage
     *
     * @param InfoCustomdictionaryvalue|null $funnelStage
     *
     * @return InfoMcmtrigger
     */
    public function setFunnelStage(?InfoCustomdictionaryvalue $funnelStage): self
    {
        $this->funnelStage = $funnelStage;

        return $this;
    }

    /**
     * Get funnel stage
     *
     * @return InfoCustomdictionaryvalue|null
     */
    public function getFunnelStage(): ?InfoCustomdictionaryvalue
    {
        return $this->funnelStage;
    }

    /**
     * Set owner
     *
     * @param InfoUser|null $owner
     *
     * @return InfoMcmfrom
     */
    public function setOwner(?InfoUser $owner): self
    {
        $this->owner = $owner;

        return $this;
    }

    /**
     * Get owner
     *
     * @return InfoUser|null
     */
    public function getOwner(): ?InfoUser
    {
        return $this->owner;
    }
}
