<?php

namespace TeamSoft\CrmRepositoryBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * InfoTabcontrol
 *
 * @ORM\Table(name="info_tabcontrol")
 * @ORM\Entity(repositoryClass="TeamSoft\CrmRepositoryBundle\Repository\InfoTabcontrolRepository")
 */
class InfoTabcontrol implements ServiceFieldInterface
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="InfoTabcontroldictionary")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="type", referencedColumnName="type")
     * })
     *
     * @ORM\Column(name="type", type="integer", nullable=true)
     */
    private $type;

    /**
     * @var int
     *
     * @ORM\Column(name="rowId", type="integer", nullable=true)
     */
    private $rowId;

    /**
     * @var int
     *
     * @ORM\Column(name="tabOrder", type="integer", nullable=true)
     */
    private $tabOrder;

    /**
     * @var int
     *
     * @ORM\Column(name="isShow", type="integer", nullable=true)
     */
    private $isShow;

    /**
     * @var int
     *
     * @ORM\Column(name="parrentType", type="integer", nullable=true)
     */
    private $parrentType;

    /**
     * @var string
     *
     * @ORM\Column(name="Name", type="string", length=255, nullable=true)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="Name_en", type="string", length=255, nullable=true)
     */
    private $nameEn;

    /**
     * @var string
     *
     * @ORM\Column(name="guid", type="guid", nullable=true)
     */
    private $guid;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="currenttime", type="datetime", nullable=true)
     */
    private $currenttime;

    /**
     * @var string
     *
     * @ORM\Column(name="moduser", type="string", length=16, nullable=true)
     */
    private $moduser;

    /**
     * @var InfoRole
     *
     * @ORM\ManyToOne(targetEntity="InfoRole", inversedBy="tabControls")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="role_id", referencedColumnName="id")
     * })
     */
    private $role;

    /**
     * @var InfoReport
     *
     * @ORM\ManyToOne(targetEntity="InfoReport")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="report_id", referencedColumnName="id")
     * })
     */
    private $report;

    /**
     * @var InfoLanguage
     *
     * @ORM\ManyToOne(targetEntity="InfoLanguage")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="language_id", referencedColumnName="id")
     * })
     */
    private $language;

    public function __clone()
    {
        $this->id = null;
        $this->guid = null;
        $this->currenttime = null;
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return InfoTabcontrol
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set tabOrder
     *
     * @param string $tabOrder
     *
     * @return InfoTabcontrol
     */
    public function setTabOrder($tabOrder)
    {
        $this->tabOrder = $tabOrder;

        return $this;
    }

    /**
     * Get tabOrder
     *
     * @return string
     */
    public function getTabOrder()
    {
        return $this->tabOrder;
    }

    /**
     * Set isShow
     *
     * @param string $isShow
     *
     * @return InfoTabcontrol
     */
    public function setIsShow($isShow)
    {
        $this->isShow = $isShow;

        return $this;
    }

    /**
     * Get isShow
     *
     * @return string
     */
    public function getIsShow()
    {
        return $this->isShow;
    }

    /**
     * Set rowId
     *
     * @param string $rowId
     *
     * @return InfoTabcontrol
     */
    public function setRowId($rowId)
    {
        $this->rowId = $rowId;

        return $this;
    }

    /**
     * Get rowId
     *
     * @return string
     */
    public function getRowId()
    {
        return $this->rowId;
    }

    /**
     * Set parrentType
     *
     * @param string $parrentType
     *
     * @return InfoTabcontrol
     */
    public function setParrentType($parrentType)
    {
        $this->parrentType = $parrentType;

        return $this;
    }

    /**
     * Get parrentType
     *
     * @return string
     */
    public function getParrentType()
    {
        return $this->parrentType;
    }

    /**
     * Set type
     *
     * @param string $type
     *
     * @return InfoTabcontrol
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type
     *
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set nameEn
     *
     * @param string $nameEn
     *
     * @return InfoTabcontrol
     */
    public function setNameEn($nameEn)
    {
        $this->nameEn = $nameEn;

        return $this;
    }

    /**
     * Get nameEn
     *
     * @return string
     */
    public function getNameEn()
    {
        return $this->nameEn;
    }

    /**
     * Set guid
     *
     * @param string $guid
     * @return InfoTabcontrol
     */
    public function setGuid($guid)
    {
        $this->guid = $guid;

        return $this;
    }

    /**
     * Get guid
     *
     * @return string
     */
    public function getGuid()
    {
        return $this->guid;
    }

    /**
     * Set currenttime
     *
     * @param \DateTime $currenttime
     * @return InfoTabcontrol
     */
    public function setCurrenttime(\DateTime $currenttime)
    {
        $this->currenttime = $currenttime;

        return $this;
    }

    /**
     * Get currenttime
     *
     * @return \DateTime|string
     */
    public function getCurrenttime()
    {
        return $this->currenttime;
    }

    /**
     * Set moduser
     *
     * @param string $moduser
     * @return InfoTabcontrol
     */
    public function setModuser($moduser)
    {
        $this->moduser = $moduser;

        return $this;
    }

    /**
     * Get moduser
     *
     * @return string
     */
    public function getModuser()
    {
        return $this->moduser;
    }

    /**
     * Set role
     *
     * @param InfoRole $role
     * @return InfoTabcontrol
     */
    public function setRole(InfoRole $role = null)
    {
        $this->role = $role;

        return $this;
    }

    /**
     * Get role
     *
     * @return InfoRole
     */
    public function getRole()
    {
        return $this->role;
    }

    /**
     * Set report
     *
     * @param InfoReport $report
     * @return InfoTabcontrol
     */
    public function setReport(InfoReport $report = null)
    {
        $this->report = $report;

        return $this;
    }

    /**
     * Get report
     *
     * @return InfoReport
     */
    public function getReport()
    {
        return $this->report;
    }

    /**
     * Set language
     *
     * @param InfoLanguage|null $content
     * @return InfoTabcontrol
     */
    public function setLanguage(InfoLanguage $language = null)
    {
        $this->language = $language;

        return $this;
    }

    /**
     * Get language
     *
     * @return InfoLanguage
     */
    public function getLanguage()
    {
        return $this->language;
    }
}
