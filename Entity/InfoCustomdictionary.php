<?php

namespace TeamSoft\CrmRepositoryBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use TeamSoft\CrmRepositoryBundle\Entity\InfoCustomdictionaryvalue;
use Doctrine\ORM\Mapping as ORM;

/**
 * InfoCustomdictionary
 *
 * @ORM\Table(name="info_CustomDictionary")
 * @ORM\Entity(repositoryClass="TeamSoft\CrmRepositoryBundle\Repository\InfoCustomDictionary")
 * @ORM\Entity
 */
class InfoCustomdictionary
{
    public const CODE_CONTACT_LEGAL_STATUS = 'legal_status';
    public const CODE_CONTACT_INTERACTION = 'interaction';
    public const CODE_EXPENSE_TYPES = 'expense_types';

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="Name", type="string", length=100, nullable=true)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="code", type="string", length=100, nullable=true)
     */
    private $code;

    /**
     * @var string
     *
     * @ORM\Column(name="Description", type="blob", nullable=true)
     */
    private $description;

    /**
     * @var guid
     *
     * @ORM\Column(name="guid", type="guid", nullable=true)
     */
    private $guid;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="currenttime", type="datetime", nullable=true)
     */
    private $currenttime;

    /**
     * @var string
     *
     * @ORM\Column(name="moduser", type="string", length=16, nullable=true)
     */
    private $moduser;

    /**
     * @var ArrayCollection
     * @ORM\OneToMany(targetEntity="InfoCustomdictionaryvalue", mappedBy="customdictionary")
     */
    private $customDictionariesValues;

    public function __construct()
    {
        $this->customDictionariesValues = new ArrayCollection();
    }

    /**
     * @return mixed
     */
    public function getCustomDictionariesValues()
    {
        return $this->customDictionariesValues;
    }

    /**
     * Add customDictionariesValues
     *
     * @param InfoCustomdictionaryvalue $infoCustomdictionaryvalue
     *
     * @return InfoCustomdictionary
     */
    public function addCustomDictionariesValues(InfoCustomdictionaryvalue $infoCustomdictionaryvalue)
    {
        if (!$this->customDictionariesValues->contains($infoCustomdictionaryvalue)) {
            $this->customDictionariesValues->add($infoCustomdictionaryvalue);
        }
        return $this;
    }

    /**
     * Remove customDictionariesValues
     *
     * @param InfoCustomdictionaryvalue $infoCustomdictionaryvalue
     */
    public function removeCustomDictionariesValues(InfoCustomdictionaryvalue $infoCustomdictionaryvalue)
    {
        if ($this->customDictionariesValues->contains($infoCustomdictionaryvalue)) {
            $this->customDictionariesValues->removeElement($infoCustomdictionaryvalue);
        }
    }

//    /**
//     * TODO move remove method to base model
//     * @param \TeamSoft\CrmRepositoryBundle\Entity\InfoCustomdictionaryvalue $infoCustomdictionaryvalue
//     * @return $this
//     */
//    public function removeCustomDictionariesValues(InfoCustomdictionaryvalue $infoCustomdictionaryvalue)
//    {
//        //removeElement forking only if  InfoCustomdictionaryvalue getting from $this->getCustomDictionariesValues()
//        $indexOf = $this->customDictionariesValues->indexOf($infoCustomdictionaryvalue);
//        if (!$indexOf) {
//            foreach ($this->getCustomDictionariesValues() as $index => $customDictionariesValue) {
//                /** @var  InfoCustomdictionaryvalue $customDictionariesValue */
//                if ($customDictionariesValue->getId() === $infoCustomdictionaryvalue->getId()) {
//                    $indexOf = $index;
//                    break;
//                }
//            }
//        }
//
//        if ($indexOf) {
//            $this->customDictionariesValues->remove($indexOf);
//        }
//
//        return $this;
//    }


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return InfoCustomdictionary
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set description
     *
     * @param string $description
     *
     * @return InfoCustomdictionary
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set guid
     *
     * @param guid $guid
     *
     * @return InfoCustomdictionary
     */
    public function setGuid($guid)
    {
        $this->guid = $guid;

        return $this;
    }

    /**
     * Get guid
     *
     * @return guid
     */
    public function getGuid()
    {
        return $this->guid;
    }

    /**
     * Set currenttime
     *
     * @param \DateTime $currenttime
     *
     * @return InfoCustomdictionary
     */
    public function setCurrenttime($currenttime)
    {
        $this->currenttime = $currenttime;

        return $this;
    }

    /**
     * Get currenttime
     *
     * @return \DateTime
     */
    public function getCurrenttime()
    {
        return $this->currenttime;
    }

    /**
     * Set moduser
     *
     * @param string $moduser
     *
     * @return InfoCustomdictionary
     */
    public function setModuser($moduser)
    {
        $this->moduser = $moduser;

        return $this;
    }

    /**
     * Get moduser
     *
     * @return string
     */
    public function getModuser()
    {
        return $this->moduser;
    }

    /**
     * @return string
     */
    public function getCode(): string
    {
        return $this->code;
    }

    /**
     * @param string $code
     */
    public function setCode(string $code)
    {
        $this->code = $code;
    }
}
