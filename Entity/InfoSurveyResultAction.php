<?php


namespace TeamSoft\CrmRepositoryBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * InfoSurveyResultAction
 *
 * @ORM\Table(name="info_surveyresultaction")
 * @ORM\Entity(repositoryClass="TeamSoft\CrmRepositoryBundle\Repository\InfoSurveyResultAction")
 */
class InfoSurveyResultAction implements ServiceFieldInterface
{
    /**
     * @var integer
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="currenttime", type="datetime", nullable=true)
     */
    private $currenttime;

    /**
     * @var string
     *
     * @ORM\Column(name="moduser", type="string", length=16, nullable=true)
     */
    private $moduser;

    /**
     * @var string
     *
     * @ORM\Column(name="guid", type="guid", nullable=true)
     */
    private $guid;

    /**
     * @var InfoAction
     *
     * @ORM\ManyToOne(targetEntity="InfoAction")
     * @ORM\JoinColumns({
     *  @ORM\JoinColumn(name="action_id", referencedColumnName="id")
     * })
     */
    private $action;

    /**
     * @var InfoSurvey
     *
     * @ORM\ManyToOne(targetEntity="InfoSurvey")
     * @ORM\JoinColumns({
     *     @ORM\JoinColumn(name="survey_id", referencedColumnName="id")
     * })
     */
    private $survey;

    /**
     * @var InfoSurveyQuestion
     *
     * @ORM\ManyToOne(targetEntity="InfoSurveyQuestion")
     *
     * @ORM\JoinColumns({
     *    @ORM\JoinColumn(name="question_id", referencedColumnName="id")
     * })
     */
    private $question;

    /**
     * @var string
     *
     * @ORM\Column(name="ans_text", type="string", length=255, nullable=true)
     */
    private $textAnswer;

    /**
     * @var float
     *
     * @ORM\Column(name="ans_float", type="float", precision=53, scale=0, nullable=true)
     */
    private $floatAnswer;

    /**
     * @var InfoSurveyQuestionAnswer
     *
     * @ORM\ManyToOne(targetEntity="InfoSurveyQuestionAnswer")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="ans_list_id", referencedColumnName="id")
     * })
     */
    private $fixedAnswer;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="fill_date", type="datetime", nullable=true)
     */
    private $fillDate;

    /**
     * @var string
     *
     * @ORM\Column(name="comment", type="string", length=255, nullable=true)
     */
    private $comment;

    /**
     * @var int
     *
     * @ORM\Column(name="isrejected", type="integer", nullable=true)
     */
    private $isRejected;

    /**
     * @var ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="InfoSurveyResultCheckAction", mappedBy="result", cascade={"persist"}, orphanRemoval=true)
     */
    private $resultCheckActions;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->resultCheckActions = new ArrayCollection();
    }

    public function __clone()
    {
        $this->id = null;
        $this->guid = null;
        $this->currenttime = null;
        $this->moduser = null;
    }

    /**
     * Get isRejected.
     *
     * @param int|null $isRejected
     * @return $this
     */
    public function setIsRejected(int $isRejected = null): InfoSurveyResultAction
    {
        $this->isRejected = $isRejected;

        return $this;
    }

    /**
     * Get isRejected.
     *
     * @return int|null
     */
    public function getIsRejected(): ?int
    {
        return $this->isRejected;
    }

    /**
     * Set comment.
     *
     * @param string|null $comment
     * @return self
     */
    public function setComment(string $comment = null): InfoSurveyResultAction
    {
        $this->comment = $comment;

        return $this;
    }

    /**
     * Get comment.
     *
     * @return string|null
     */
    public function getComment(): ?string
    {
        return $this->comment;
    }

    /**
     * Set fillDate.
     *
     * @param \DateTime|null $fillDate
     * @return self
     */
    public function setFillDate(\DateTime $fillDate = null): InfoSurveyResultAction
    {
        $this->fillDate = $fillDate;

        return $this;
    }

    /**
     * Get fillDate.
     *
     * @return \DateTime|null
     */
    public function getFillDate(): ?\DateTime
    {
        return $this->fillDate;
    }

    /**
     * Set fixedAnswer.
     *
     * @param InfoSurveyQuestionAnswer|null $fixedAnswer
     * @return $this
     */
    public function setFixedAnswer(InfoSurveyQuestionAnswer $fixedAnswer = null): InfoSurveyResultAction
    {
        $this->fixedAnswer = $fixedAnswer;

        return $this;
    }

    /**
     * Get fixedAnswer.
     *
     * @return InfoSurveyQuestionAnswer|null
     */
    public function getFixedAnswer(): ?InfoSurveyQuestionAnswer
    {
        return $this->fixedAnswer;
    }

    /**
     * Get floatAnswer
     *
     * @param float|null $floatAnswer
     * @return self
     */
    public function setFloatAnswer(float $floatAnswer = null): InfoSurveyResultAction
    {
        $this->floatAnswer = $floatAnswer;

        return $this;
    }

    /**
     * Get floatAnswer
     *
     * @return float|null
     */
    public function getFloatAnswer(): ?float
    {
        return $this->floatAnswer;
    }

    /**
     * Set textAnswer.
     *
     * @param string|null $textAnswer
     * @return self
     */
    public function setTextAnswer(string $textAnswer = null): InfoSurveyResultAction
    {
        $this->textAnswer = $textAnswer;

        return $this;
    }

    /**
     * Get textAnswer
     *
     * @return string|null
     */
    public function getTextAnswer(): ?string
    {
        return $this->textAnswer;
    }

    /**
     * Set question.
     *
     * @param InfoSurveyQuestion|null $question
     * @return self
     */
    public function setQuestion(InfoSurveyQuestion $question = null): InfoSurveyResultAction
    {
        $this->question = $question;

        return $this;
    }

    /**
     * Get question.
     *
     * @return InfoSurveyQuestion|null
     */
    public function getQuestion(): ?InfoSurveyQuestion
    {
        return $this->question;
    }

    /**
     * Set survey.
     *
     * @param InfoSurvey|null $survey
     * @return self
     */
    public function setSurvey(InfoSurvey $survey = null): InfoSurveyResultAction
    {
        $this->survey = $survey;

        return $this;
    }

    /**
     * @return InfoSurvey|null
     */
    public function getSurvey(): ?InfoSurvey
    {
        return $this->survey;
    }

    /**
     * Set action.
     *
     * @param InfoAction|null $action
     * @return self
     */
    public function setAction(InfoAction $action = null): InfoSurveyResultAction
    {
        $this->action = $action;

        return $this;
    }

    /**
     * Get action.
     *
     * @return InfoAction
     */
    public function getAction(): InfoAction
    {
        return $this->action;
    }

    /**
     * Set currenttime.
     *
     * @param \DateTime|null $currenttime
     * @return self;
     */
    public function setCurrenttime(\DateTime $currenttime = null): InfoSurveyResultAction
    {
        $this->currenttime = $currenttime;

        return $this;
    }

    /**
     * Get currenttime.
     *
     * @return \DateTime|null
     */
    public function getCurrenttime(): ?\DateTime
    {
        return $this->currenttime;
    }

    /**
     * Set guid.
     *
     * @param string|null $guid
     *
     * @return self
     */
    public function setGuid($guid = null): InfoSurveyResultAction
    {
        $this->guid = $guid;

        return $this;
    }

    /**
     * Get guid.
     *
     * @return string|null
     */
    public function getGuid(): ?string
    {
        return $this->guid;
    }

    /**
     * Set moduser.
     *
     * @param string|null $moduser
     * @return self
     */
    public function setModuser($moduser = null): InfoSurveyResultAction
    {
        $this->moduser = $moduser;

        return $this;
    }

    /**
     * Add resultCheckAction.
     *
     * @param \TeamSoft\CrmRepositoryBundle\Entity\InfoSurveyResultCheckAction $resultCheckAction
     *
     * @return InfoSurveyResultAction
     */
    public function addResultCheckAction(\TeamSoft\CrmRepositoryBundle\Entity\InfoSurveyResultCheckAction $resultCheckAction): InfoSurveyResultAction
    {
        if (!$this->resultCheckActions->contains($resultCheckAction)) {
            $resultCheckAction->setResult($this);
            $this->resultCheckActions[] = $resultCheckAction;
        }

        return $this;
    }

    /**
     * Remove resultCheckAction.
     *
     * @param \TeamSoft\CrmRepositoryBundle\Entity\InfoSurveyResultCheckAction $resultCheckAction
     *
     * @return void
     */
    public function removeResultCheckAction(\TeamSoft\CrmRepositoryBundle\Entity\InfoSurveyResultCheckAction $resultCheckAction)
    {
        if ($this->resultCheckActions->contains($resultCheckAction)) {
            $this->resultCheckActions->removeElement($resultCheckAction);
        }
    }

    /**
     * Get resultCheckActionCollection.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getResultCheckActions()
    {
        return $this->resultCheckActions;
    }

    /**
     * Get moduser.
     *
     * @return string|null
     */
    public function getModuser(): ?string
    {
        return $this->moduser;
    }

    public function getSurveyId(): ?int
    {
        return $this->survey ? $this->survey->getId() : null;
    }

    public function getQuestionId(): ?int
    {
        return $this->question ? $this->question->getId() : null;
    }

    public function getFixedAnswerId(): ?int
    {
        return $this->fixedAnswer ? $this->fixedAnswer->getId() : null;
    }
}
