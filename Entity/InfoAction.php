<?php

namespace TeamSoft\CrmRepositoryBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * InfoAction
 *
 * @ORM\Table(name="info_action")
 * @ORM\Entity(repositoryClass="TeamSoft\CrmRepositoryBundle\Repository\InfoAction")
 */
class InfoAction implements ServiceFieldInterface, DateTimeWithGmtOffsetInterface
{
    public const STATUS_APPROVED = 'Approved';
    public const STATUS_REJECTED = 'Rejected';
    public const STATUS_PENDING = 'Pending for approval';

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255, nullable=true)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="varchar_or_n_varchar_image_or_string", nullable=true)
     */
    private $description;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="datefrom", type="datetime", nullable=true)
     */
    private $datefrom;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="datetill", type="datetime", nullable=true)
     */
    private $datetill;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="realdatetill", type="datetime", nullable=true)
     */
    private $realdatetill;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="duration", type="datetime", nullable=true)
     */
    private $duration;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="createdate", type="datetime", nullable=true)
     */
    private $createdate;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="modified", type="datetime", nullable=true)
     */
    private $modified;

    /**
     * @var integer
     *
     * @ORM\Column(name="review", type="integer", nullable=true)
     */
    private $review;

    /**
     * @var string
     *
     * @ORM\Column(name="guid", type="guid", nullable=true)
     */
    private $guid;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="currenttime", type="datetime", nullable=true)
     */
    private $currenttime;

    /**
     * @var string
     *
     * @ORM\Column(name="moduser", type="string", length=16, nullable=true)
     */
    private $moduser;

    /**
     * @var integer
     *
     * @ORM\Column(name="isnoshow", type="integer", nullable=true)
     */
    private $isnoshow;

    /**
     * @var float
     *
     * @ORM\Column(name="gps_lat", type="float", precision=53, scale=0, nullable=true)
     */
    private $gpsLat;

    /**
     * @var float
     *
     * @ORM\Column(name="gps_lng", type="float", precision=53, scale=0, nullable=true)
     */
    private $gpsLng;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="gps_date", type="datetime", nullable=true)
     */
    private $gpsDate;

    /**
     * @var string
     *
     * @ORM\Column(name="location", type="string", length=255, nullable=true)
     */
    private $location;

    /**
     * @var integer
     *
     * @ORM\Column(name="gps_accuracy", type="integer", nullable=true)
     */
    private $gpsAccuracy;

    /**
     * @var InfoRegion
     *
     * @ORM\ManyToOne(targetEntity="InfoRegion")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="region_id", referencedColumnName="id")
     * })
     */
    private $region;

    /**
     * @var InfoUser
     *
     * @ORM\ManyToOne(targetEntity="InfoUser")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="responsible2_id", referencedColumnName="id")
     * })
     */
    private $responsible2;

    /**
     * @var InfoCity
     *
     * @ORM\ManyToOne(targetEntity="InfoCity")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="city_id", referencedColumnName="id")
     * })
     */
    private $city;

    /**
     * @var InfoCompany
     *
     * @ORM\ManyToOne(targetEntity="InfoCompany")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="company_id", referencedColumnName="id")
     * })
     */
    private $company;

    /**
     * @var InfoContact
     *
     * @ORM\ManyToOne(targetEntity="InfoContact")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="contact_id", referencedColumnName="id")
     * })
     */
    private $contact;

    /**
     * @var InfoActiontype
     *
     * @ORM\ManyToOne(targetEntity="InfoActiontype")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="actiontype_id", referencedColumnName="id")
     * })
     */
    private $actiontype;

    /**
     * @var InfoActionstate
     *
     * @ORM\ManyToOne(targetEntity="InfoActionstate")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="actionstate_id", referencedColumnName="id")
     * })
     */
    private $actionstate;

    /**
     * @var InfoDistrict
     *
     * @ORM\ManyToOne(targetEntity="InfoDistrict")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="district_id", referencedColumnName="id")
     * })
     */
    private $district;

    /**
     * @var InfoSuburb
     *
     * @ORM\ManyToOne(targetEntity="InfoSuburb")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="suburb_id", referencedColumnName="id")
     * })
     */
    private $suburb;

    /**
     * @var InfoCountry
     *
     * @ORM\ManyToOne(targetEntity="InfoCountry")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="country_id", referencedColumnName="id")
     * })
     */
    private $country;

    /**
     * @var InfoUser
     *
     * @ORM\ManyToOne(targetEntity="InfoUser")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="owner_id", referencedColumnName="id")
     * })
     */
    private $owner;

    /**
     * @var InfoUser
     *
     * @ORM\ManyToOne(targetEntity="InfoUser")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="modifier_id", referencedColumnName="id")
     * })
     */
    private $modifier;

    /**
     * @var InfoUser
     *
     * @ORM\ManyToOne(targetEntity="InfoUser")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="responsible_id", referencedColumnName="id")
     * })
     */
    private $responsible;

    /**
     * @var ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="InfoActioninfo", mappedBy="subj", cascade={"persist"})
     */
    private $actionInfoCollection;

    /**
     * @var ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="InfoActionparamtext", mappedBy="action", cascade={"persist"}, orphanRemoval=true)
     */
    private $actionParamTextCollection;

    /**
     * @var ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="InfoActionfilegroup", mappedBy="subj", cascade={"persist"})
     */
    private $actionFileGroupCollection;

    /**
     * @var ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="InfoSurveyResultAction", mappedBy="action", cascade={"persist"}, orphanRemoval=true)
     */
    private $surveyResultActions;

    /**
     * @ORM\ManyToMany(targetEntity="InfoEvent", mappedBy="actions")
     */
    private $events;

    /**
     * @var integer
     *
     * @ORM\Column(name="gmt_offset", type="integer", nullable=true)
     */
    private $gmtOffset;

    /**
     * @var ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="InfoEntityTypeWorkflowLog", mappedBy="action", cascade={"persist"}, orphanRemoval=true)
     */
    private $entityTypeWorkflowLogs;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->actionInfoCollection = new ArrayCollection();
        $this->actionFileGroupCollection = new ArrayCollection();
        $this->events = new ArrayCollection();
        $this->actionParamTextCollection = new ArrayCollection();
        $this->surveyResultActions = new ArrayCollection();
        $this->entityTypeWorkflowLogs = new ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return InfoAction
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set description
     *
     * @param string $description
     * @return InfoAction
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set datefrom
     *
     * @param \DateTime $datefrom
     * @return InfoAction
     */
    public function setDatefrom($datefrom)
    {
        $this->datefrom = $datefrom;

        return $this;
    }

    /**
     * Get datefrom
     *
     * @return \DateTime
     */
    public function getDatefrom()
    {
        return $this->datefrom;
    }

    /**
     * Set datetill
     *
     * @param \DateTime $datetill
     * @return InfoAction
     */
    public function setDatetill($datetill)
    {
        $this->datetill = $datetill;

        return $this;
    }

    /**
     * Get datetill
     *
     * @return \DateTime
     */
    public function getDatetill()
    {
        return $this->datetill;
    }

    /**
     * Set realdatetill
     *
     * @param \DateTime $realdatetill
     * @return InfoAction
     */
    public function setRealdatetill($realdatetill)
    {
        $this->realdatetill = $realdatetill;

        return $this;
    }

    /**
     * Get realdatetill
     *
     * @return \DateTime
     */
    public function getRealdatetill()
    {
        return $this->realdatetill;
    }

    /**
     * Set duration
     *
     * @param \DateTime $duration
     * @return InfoAction
     */
    public function setDuration($duration)
    {
        $this->duration = $duration;

        return $this;
    }

    /**
     * Get duration
     *
     * @return \DateTime
     */
    public function getDuration()
    {
        return $this->duration;
    }

    /**
     * Set createdate
     *
     * @param \DateTime $createdate
     * @return InfoAction
     */
    public function setCreatedate(\DateTime $createdate)
    {
        $this->createdate = $createdate;

        return $this;
    }

    /**
     * Get createdate
     *
     * @return \DateTime
     */
    public function getCreatedate()
    {
        return $this->createdate;
    }

    /**
     * Set modified
     *
     * @param \DateTime $modified
     * @return InfoAction
     */
    public function setModified(\DateTime $modified)
    {
        $this->modified = $modified;

        return $this;
    }

    /**
     * Get modified
     *
     * @return \DateTime
     */
    public function getModified()
    {
        return $this->modified;
    }

    /**
     * Set review
     *
     * @param integer $review
     * @return InfoAction
     */
    public function setReview($review)
    {
        $this->review = $review;

        return $this;
    }

    /**
     * Get review
     *
     * @return integer
     */
    public function getReview()
    {
        return $this->review;
    }

    /**
     * Set guid
     *
     * @param string $guid
     * @return InfoAction
     */
    public function setGuid($guid)
    {
        $this->guid = $guid;

        return $this;
    }

    /**
     * Get guid
     *
     * @return string
     */
    public function getGuid()
    {
        return $this->guid;
    }

    /**
     * Set currenttime
     *
     * @param \DateTime $currenttime
     * @return InfoAction
     */
    public function setCurrenttime(\DateTime $currenttime)
    {
        $this->currenttime = $currenttime;

        return $this;
    }

    /**
     * Get currenttime
     *
     * @return \DateTime
     */
    public function getCurrenttime()
    {
        return $this->currenttime;
    }

    /**
     * Set moduser
     *
     * @param string $moduser
     * @return InfoAction
     */
    public function setModuser($moduser)
    {
        $this->moduser = $moduser;

        return $this;
    }

    /**
     * Get moduser
     *
     * @return string
     */
    public function getModuser()
    {
        return $this->moduser;
    }

    /**
     * Set isnoshow
     *
     * @param integer $isnoshow
     * @return InfoAction
     */
    public function setIsnoshow($isnoshow)
    {
        $this->isnoshow = $isnoshow;

        return $this;
    }

    /**
     * Get isnoshow
     *
     * @return integer
     */
    public function getIsnoshow()
    {
        return $this->isnoshow;
    }

    /**
     * Set gpsLat
     *
     * @param float $gpsLat
     * @return InfoAction
     */
    public function setGpsLat($gpsLat)
    {
        $this->gpsLat = $gpsLat;

        return $this;
    }

    /**
     * Get gpsLat
     *
     * @return float
     */
    public function getGpsLat()
    {
        return $this->gpsLat;
    }

    /**
     * Set gpsLng
     *
     * @param float $gpsLng
     * @return InfoAction
     */
    public function setGpsLng($gpsLng)
    {
        $this->gpsLng = $gpsLng;

        return $this;
    }

    /**
     * Get gpsLng
     *
     * @return float
     */
    public function getGpsLng()
    {
        return $this->gpsLng;
    }

    /**
     * Set gpsDate
     *
     * @param \DateTime $gpsDate
     * @return InfoAction
     */
    public function setGpsDate($gpsDate)
    {
        $this->gpsDate = $gpsDate;

        return $this;
    }

    /**
     * Get gpsDate
     *
     * @return \DateTime
     */
    public function getGpsDate()
    {
        return $this->gpsDate;
    }

    /**
     * Set location
     *
     * @param string $location
     * @return InfoAction
     */
    public function setLocation($location)
    {
        $this->location = $location;

        return $this;
    }

    /**
     * Get location
     *
     * @return string
     */
    public function getLocation()
    {
        return $this->location;
    }

    /**
     * Set gpsAccuracy
     *
     * @param integer $gpsAccuracy
     * @return InfoAction
     */
    public function setGpsAccuracy($gpsAccuracy)
    {
        $this->gpsAccuracy = $gpsAccuracy;

        return $this;
    }

    /**
     * Get gpsAccuracy
     *
     * @return integer
     */
    public function getGpsAccuracy()
    {
        return $this->gpsAccuracy;
    }

    /**
     * Set region
     *
     * @param \TeamSoft\CrmRepositoryBundle\Entity\InfoRegion $region
     * @return InfoAction
     */
    public function setRegion(\TeamSoft\CrmRepositoryBundle\Entity\InfoRegion $region = null)
    {
        $this->region = $region;

        return $this;
    }

    /**
     * Get region
     *
     * @return \TeamSoft\CrmRepositoryBundle\Entity\InfoRegion
     */
    public function getRegion()
    {
        return $this->region;
    }

    /**
     * Set responsible2
     *
     * @param \TeamSoft\CrmRepositoryBundle\Entity\InfoUser $responsible2
     * @return InfoAction
     */
    public function setResponsible2(\TeamSoft\CrmRepositoryBundle\Entity\InfoUser $responsible2 = null)
    {
        $this->responsible2 = $responsible2;

        return $this;
    }

    /**
     * Get responsible2
     *
     * @return \TeamSoft\CrmRepositoryBundle\Entity\InfoUser
     */
    public function getResponsible2()
    {
        return $this->responsible2;
    }

    /**
     * Set city
     *
     * @param \TeamSoft\CrmRepositoryBundle\Entity\InfoCity $city
     * @return InfoAction
     */
    public function setCity(\TeamSoft\CrmRepositoryBundle\Entity\InfoCity $city = null)
    {
        $this->city = $city;

        return $this;
    }

    /**
     * Get city
     *
     * @return \TeamSoft\CrmRepositoryBundle\Entity\InfoCity
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
     * Set company
     *
     * @param \TeamSoft\CrmRepositoryBundle\Entity\InfoCompany $company
     * @return InfoAction
     */
    public function setCompany(\TeamSoft\CrmRepositoryBundle\Entity\InfoCompany $company = null)
    {
        $this->company = $company;

        return $this;
    }

    /**
     * Get company
     *
     * @return \TeamSoft\CrmRepositoryBundle\Entity\InfoCompany
     */
    public function getCompany()
    {
        return $this->company;
    }

    /**
     * Set contact
     *
     * @param \TeamSoft\CrmRepositoryBundle\Entity\InfoContact $contact
     * @return InfoAction
     */
    public function setContact(\TeamSoft\CrmRepositoryBundle\Entity\InfoContact $contact = null)
    {
        $this->contact = $contact;

        return $this;
    }

    /**
     * Get contact
     *
     * @return \TeamSoft\CrmRepositoryBundle\Entity\InfoContact
     */
    public function getContact()
    {
        return $this->contact;
    }

    /**
     * Set actiontype
     *
     * @param \TeamSoft\CrmRepositoryBundle\Entity\InfoActiontype $actiontype
     * @return InfoAction
     */
    public function setActiontype(\TeamSoft\CrmRepositoryBundle\Entity\InfoActiontype $actiontype = null)
    {
        $this->actiontype = $actiontype;

        return $this;
    }

    /**
     * Get actiontype
     *
     * @return \TeamSoft\CrmRepositoryBundle\Entity\InfoActiontype
     */
    public function getActiontype()
    {
        return $this->actiontype;
    }

    /**
     * Set actionstate
     *
     * @param \TeamSoft\CrmRepositoryBundle\Entity\InfoActionstate $actionstate
     * @return InfoAction
     */
    public function setActionstate(\TeamSoft\CrmRepositoryBundle\Entity\InfoActionstate $actionstate = null)
    {
        $this->actionstate = $actionstate;

        return $this;
    }

    /**
     * Get actionstate
     *
     * @return \TeamSoft\CrmRepositoryBundle\Entity\InfoActionstate
     */
    public function getActionstate()
    {
        return $this->actionstate;
    }

    /**
     * Set district
     *
     * @param \TeamSoft\CrmRepositoryBundle\Entity\InfoDistrict $district
     * @return InfoAction
     */
    public function setDistrict(\TeamSoft\CrmRepositoryBundle\Entity\InfoDistrict $district = null)
    {
        $this->district = $district;

        return $this;
    }

    /**
     * Get district
     *
     * @return \TeamSoft\CrmRepositoryBundle\Entity\InfoDistrict
     */
    public function getDistrict()
    {
        return $this->district;
    }

    /**
     * Set suburb
     *
     * @param \TeamSoft\CrmRepositoryBundle\Entity\InfoSuburb $suburb
     * @return InfoAction
     */
    public function setSuburb(\TeamSoft\CrmRepositoryBundle\Entity\InfoSuburb $suburb = null)
    {
        $this->suburb = $suburb;

        return $this;
    }

    /**
     * Get suburb
     *
     * @return \TeamSoft\CrmRepositoryBundle\Entity\InfoSuburb
     */
    public function getSuburb()
    {
        return $this->suburb;
    }

    /**
     * Set country
     *
     * @param \TeamSoft\CrmRepositoryBundle\Entity\InfoCountry $country
     * @return InfoAction
     */
    public function setCountry(\TeamSoft\CrmRepositoryBundle\Entity\InfoCountry $country = null)
    {
        $this->country = $country;

        return $this;
    }

    /**
     * Get country
     *
     * @return \TeamSoft\CrmRepositoryBundle\Entity\InfoCountry
     */
    public function getCountry()
    {
        return $this->country;
    }

    /**
     * Set owner
     *
     * @param \TeamSoft\CrmRepositoryBundle\Entity\InfoUser $owner
     * @return InfoAction
     */
    public function setOwner(\TeamSoft\CrmRepositoryBundle\Entity\InfoUser $owner = null)
    {
        $this->owner = $owner;

        return $this;
    }

    /**
     * Get owner
     *
     * @return \TeamSoft\CrmRepositoryBundle\Entity\InfoUser
     */
    public function getOwner()
    {
        return $this->owner;
    }

    /**
     * Set modifier
     *
     * @param \TeamSoft\CrmRepositoryBundle\Entity\InfoUser $modifier
     * @return InfoAction
     */
    public function setModifier(\TeamSoft\CrmRepositoryBundle\Entity\InfoUser $modifier = null)
    {
        $this->modifier = $modifier;

        return $this;
    }

    /**
     * Get modifier
     *
     * @return \TeamSoft\CrmRepositoryBundle\Entity\InfoUser
     */
    public function getModifier()
    {
        return $this->modifier;
    }

    /**
     * Set responsible
     *
     * @param \TeamSoft\CrmRepositoryBundle\Entity\InfoUser $responsible
     * @return InfoAction
     */
    public function setResponsible(\TeamSoft\CrmRepositoryBundle\Entity\InfoUser $responsible = null)
    {
        $this->responsible = $responsible;

        return $this;
    }

    /**
     * Get responsible
     *
     * @return \TeamSoft\CrmRepositoryBundle\Entity\InfoUser
     */
    public function getResponsible()
    {
        return $this->responsible;
    }

    public function getActiontypeId () {
        return $this->getActiontype() ? $this->getActiontype()->getId() : null;
    }

    public function getActionstateId () {
        return $this->getActionstate() ? $this->getActionstate()->getId() : null;
    }

    public function getCompanyId () {
        return $this->getCompany() ? $this->getCompany()->getId() : null;
    }

    public function getContactId () {
        return $this->getContact() ? $this->getContact()->getId() : null;
    }

    public function getResponsibleId () {
        return $this->getResponsible() ? $this->getResponsible()->getId() : null;
    }

    public function getResponsible2Id()
    {
        return $this->getResponsible2() ? $this->getResponsible2()->getId() : null;
    }

    public function getCityId () {
        return $this->getCity() ? $this->getCity()->getId() : null;
    }

    /**
     * Add actionInfoCollection
     *
     * @param \TeamSoft\CrmRepositoryBundle\Entity\InfoActioninfo $actionInfo
     *
     * @return InfoAction
     */
    public function addActionInfoCollection(\TeamSoft\CrmRepositoryBundle\Entity\InfoActioninfo $actionInfo)
    {
        $actionInfo->setSubj($this);
        $this->actionInfoCollection->add($actionInfo);

        return $this;
    }

    /**
     * Remove actionInfoCollection
     *
     * @param \TeamSoft\CrmRepositoryBundle\Entity\InfoActioninfo $actionInfo
     */
    public function removeActionInfoCollection(\TeamSoft\CrmRepositoryBundle\Entity\InfoActioninfo $actionInfo)
    {
        $actionInfo->setSubj(null);
        $this->actionInfoCollection->removeElement($actionInfo);
    }

    /**
     * Get actionInfoCollection
     *
     * @return Collection
     */
    public function getActionInfoCollection()
    {
        return $this->actionInfoCollection;
    }

    /**
     * Add actionFileGroupCollection
     *
     * @param \TeamSoft\CrmRepositoryBundle\Entity\InfoActionfilegroup $actionFileGroup
     *
     * @return InfoAction
     */
    public function addActionFileGroupCollection(\TeamSoft\CrmRepositoryBundle\Entity\InfoActionfilegroup $actionFileGroup)
    {
        $actionFileGroup->setSubj($this);
        $this->actionFileGroupCollection->add($actionFileGroup);

        return $this;
    }

    /**
     * Remove actionFileGroupCollection
     *
     * @param \TeamSoft\CrmRepositoryBundle\Entity\InfoActionfilegroup $actionFileGroup
     */
    public function removeActionFileGroupCollection(\TeamSoft\CrmRepositoryBundle\Entity\InfoActionfilegroup $actionFileGroup)
    {
        $actionFileGroup->setSubj(null);
        $this->actionFileGroupCollection->removeElement($actionFileGroup);
    }

    /**
     * Add surveyResultAction.
     *
     * @param \TeamSoft\CrmRepositoryBundle\Entity\InfoSurveyResultAction $surveyResultAction
     *
     * @return InfoAction
     */
    public function addSurveyResultAction(\TeamSoft\CrmRepositoryBundle\Entity\InfoSurveyResultAction $surveyResultAction)
    {
        if(!$this->surveyResultActions->contains($surveyResultAction))
        {
            $surveyResultAction->setAction($this);
            $this->surveyResultActions[] = $surveyResultAction;
        }

        return $this;
    }

    /**
     * Remove surveyResultAction.
     *
     * @param \TeamSoft\CrmRepositoryBundle\Entity\InfoSurveyResultAction $surveyResultAction
     *
     * @return void
     */
    public function removeSurveyResultAction(\TeamSoft\CrmRepositoryBundle\Entity\InfoSurveyResultAction $surveyResultAction)
    {
        if($this->surveyResultActions->contains($surveyResultAction)) {
           $this->surveyResultActions->removeElement($surveyResultAction);
        }
    }

    /**
     * Get surveyResultActions.
     *
     * @return Collection
     */
    public function getSurveyResultActions()
    {
        return $this->surveyResultActions;
    }

    /**
     * Add entityTypeWorkflowLog.
     *
     * @param InfoEntityTypeWorkflowLog $entityTypeWorkflowLog
     *
     * @return InfoAction
     */
    public function addEntityTypeWorkflowLog(InfoEntityTypeWorkflowLog $entityTypeWorkflowLog): self
    {
        if(!$this->entityTypeWorkflowLogs->contains($entityTypeWorkflowLog))
        {
            $entityTypeWorkflowLog->setAction($this);
            $this->entityTypeWorkflowLogs[] = $entityTypeWorkflowLog;
        }

        return $this;
    }

    /**
     * Remove entityTypeWorkflowLog.
     *
     * @param InfoEntityTypeWorkflowLog $entityTypeWorkflowLog
     *
     * @return void
     */
    public function removeEntityTypeWorkflowLog(InfoEntityTypeWorkflowLog $entityTypeWorkflowLog): void
    {
        if($this->entityTypeWorkflowLogs->contains($entityTypeWorkflowLog)) {
            $entityTypeWorkflowLog->setAction(null);
            $this->entityTypeWorkflowLogs->removeElement($entityTypeWorkflowLog);
        }
    }

    /**
     * Get entityTypeWorkflowLogs.
     *
     * @return Collection
     */
    public function getEntityTypeWorkflowLogs(): Collection
    {
        return $this->entityTypeWorkflowLogs;
    }

    /**
     * Get actionFileGroupCollection
     *
     * @return Collection
     */
    public function getActionFileGroupCollection()
    {
        return $this->actionFileGroupCollection;
    }

    public function getAttachments()
    {
        return $this->getActionFileGroupCollection()->filter(function (\TeamSoft\CrmRepositoryBundle\Entity\InfoActionfilegroup $actionFileGroup) {
            return $actionFileGroup->getName() === 'attachments';
        });
    }

    /**
     * Add event
     *
     * @param \TeamSoft\CrmRepositoryBundle\Entity\InfoEvent $event
     *
     * @return InfoAction
     */
    public function addEvent(InfoEvent $event)
    {
        $this->events[] = $event;

        return $this;
    }

    /**
     * Remove event
     *
     * @param \TeamSoft\CrmRepositoryBundle\Entity\InfoEvent $event
     */
    public function removeEvent(InfoEvent $event)
    {
        $this->events->removeElement($event);
    }

    /**
     * Get events
     *
     * @return Collection
     */
    public function getEvents()
    {
        return $this->events;
    }

    /**
     * Add actionParamTextCollection.
     *
     * @param \TeamSoft\CrmRepositoryBundle\Entity\InfoActionparamtext $actionParamTextCollection
     *
     * @return InfoAction
     */
    public function addActionParamTextCollection(\TeamSoft\CrmRepositoryBundle\Entity\InfoActionparamtext $actionParamTextCollection)
    {
        $actionParamTextCollection->setAction($this);
        $this->actionParamTextCollection[] = $actionParamTextCollection;

        return $this;
    }

    /**
     * Remove actionParamTextCollection.
     *
     * @param \TeamSoft\CrmRepositoryBundle\Entity\InfoActionparamtext $actionParamTextCollection
     *
     * @return boolean TRUE if this collection contained the specified element, FALSE otherwise.
     */
    public function removeActionParamTextCollection(\TeamSoft\CrmRepositoryBundle\Entity\InfoActionparamtext $actionParamTextCollection)
    {
        return $this->actionParamTextCollection->removeElement($actionParamTextCollection);
    }

    /**
     * Get actionParamTextCollection.
     *
     * @return Collection
     */
    public function getActionParamTextCollection()
    {
        return $this->actionParamTextCollection;
    }

    /**
     * Set gmtOffset.
     *
     * @param int|null $gmtOffset
     *
     * @return InfoAction
     */
    public function setGmtOffset(int $gmtOffset = null)
    {
        $this->gmtOffset = $gmtOffset;

        return $this;
    }

    /**
     * Get gmtOffset.
     *
     * @return int|null
     */
    public function getGmtOffset()
    {
        return $this->gmtOffset;
    }

    public function getDateTimeWithGmtOffsetPropertyNameList()
    {
        return ['datefrom', 'datetill', 'gpsDate'];
    }

    public function getEntityTypeWorkflowLogStatus ()
    {
        $status = null;
        $statuses = [];

        foreach ($this->entityTypeWorkflowLogs as $entityTypeWorkflowLog) {
            array_push($statuses, $entityTypeWorkflowLog->getStatus());
        }

        if (in_array(NULL, $statuses)) {
            $status = self::STATUS_PENDING;
        } else if (in_array(0, $statuses)) {
            $status = self::STATUS_REJECTED;
        } else if (in_array(1, $statuses)) {
            $status = self::STATUS_APPROVED;
        }

        return $status;
    }

    public function getTotal ()
    {
        $total = 0;

        foreach ($this->actionInfoCollection as $actionInfo) {
            if (in_array($actionInfo->getInfotype()->getCode(), ['perdiem', 'livingexp', 'transpexp'])) {
                $total += $actionInfo->getFloatvalue();
            }
        }

        return $total;
    }
}
