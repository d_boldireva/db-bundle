<?php

namespace TeamSoft\CrmRepositoryBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * InfoDictionary
 *
 * @ORM\Table(name="info_dictionary")
 * @ORM\Entity(repositoryClass="TeamSoft\CrmRepositoryBundle\Repository\InfoDictionary")
 */
class InfoDictionary
{
    const POSITION = 4;
    const CONTACT_SPECIALIZATION = 10;
    const USER_POSITION = 35;
    const STREET_TYPE = 37;
    const USER_POSITION_FLAG_COACH = 1;
    /**
     * Position regional manager
     */
    const USER_POSITION_FLAG_RM = 2;
    /**
     * Position medical representative
     */
    const USER_POSITION_FLAG_MR = 4;

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="Name", type="string", length=255, nullable=false)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="Description", type="text", length=16, nullable=true)
     */
    private $description;

    /**
     * @var integer
     *
     * @ORM\Column(name="Identifier", type="integer", nullable=true)
     */
    private $identifier;

    /**
     * @var string
     *
     * @ORM\Column(name="NameUkr", type="string", length=255, nullable=true)
     */
    private $nameukr;

    /**
     * @var string
     *
     * @ORM\Column(name="flags", type="integer", nullable=false)
     */
    private $flags;

    /**
     * @var guid
     *
     * @ORM\Column(name="guid", type="guid", nullable=true)
     */
    private $guid;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="currenttime", type="datetime", nullable=true)
     */
    private $currenttime;

    /**
     * @var string
     *
     * @ORM\Column(name="moduser", type="string", length=16, nullable=true)
     */
    private $moduser;

    /**
     * @var integer
     *
     * @ORM\Column(name="morionid", type="integer", nullable=true)
     */
    private $morionid;

    /**
     * @var integer
     *
     * @ORM\Column(name="isverification", type="integer", nullable=true)
     */
    private $isverification;

    /**
     * @var integer
     *
     * @ORM\Column(name="fl", type="integer", nullable=true)
     */
    private $fl;

    /**
     * @ORM\ManyToMany(targetEntity="InfoTarget")
     * @ORM\JoinTable(name="info_targetspec",
     *   joinColumns={@ORM\JoinColumn(name="spec_id", referencedColumnName="id")},
     *   inverseJoinColumns={@ORM\JoinColumn(name="target_id", referencedColumnName="id")}
     * )
     */
    private $targetCollection;

    /**
     * @var InfoLanguage
     *
     * @ORM\ManyToOne(targetEntity="InfoLanguage")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="language_id", referencedColumnName="id")
     * })
     */
    private $language;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->targetCollection = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return InfoDictionary
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set description
     *
     * @param string $description
     * @return InfoDictionary
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set identifier
     *
     * @param integer $identifier
     * @return InfoDictionary
     */
    public function setIdentifier($identifier)
    {
        $this->identifier = $identifier;

        return $this;
    }

    /**
     * Get identifier
     *
     * @return integer
     */
    public function getIdentifier()
    {
        return $this->identifier;
    }

    /**
     * Set nameukr
     *
     * @param string $nameukr
     * @return InfoDictionary
     */
    public function setNameukr($nameukr)
    {
        $this->nameukr = $nameukr;

        return $this;
    }

    /**
     * Get nameukr
     *
     * @return string
     */
    public function getNameukr()
    {
        return $this->nameukr;
    }

    /**
     * Set guid
     *
     * @param guid $guid
     * @return InfoDictionary
     */
    public function setGuid($guid)
    {
        $this->guid = $guid;

        return $this;
    }

    /**
     * Get guid
     *
     * @return guid
     */
    public function getGuid()
    {
        return $this->guid;
    }

    /**
     * Set currenttime
     *
     * @param \DateTime $currenttime
     * @return InfoDictionary
     */
    public function setCurrenttime($currenttime)
    {
        $this->currenttime = $currenttime;

        return $this;
    }

    /**
     * Get currenttime
     *
     * @return \DateTime
     */
    public function getCurrenttime()
    {
        return $this->currenttime;
    }

    /**
     * Set moduser
     *
     * @param string $moduser
     * @return InfoDictionary
     */
    public function setModuser($moduser)
    {
        $this->moduser = $moduser;

        return $this;
    }

    /**
     * Get moduser
     *
     * @return string
     */
    public function getModuser()
    {
        return $this->moduser;
    }

    /**
     * Set morionid
     *
     * @param integer $morionid
     * @return InfoDictionary
     */
    public function setMorionid($morionid)
    {
        $this->morionid = $morionid;

        return $this;
    }

    /**
     * Get morionid
     *
     * @return integer
     */
    public function getMorionid()
    {
        return $this->morionid;
    }

    /**
     * Set isverification
     *
     * @param integer $isverification
     * @return InfoDictionary
     */
    public function setIsverification($isverification)
    {
        $this->isverification = $isverification;

        return $this;
    }

    /**
     * Get isverification
     *
     * @return integer
     */
    public function getIsverification()
    {
        return $this->isverification;
    }

    /**
     * Set fl
     *
     * @param integer $fl
     * @return InfoDictionary
     */
    public function setFl($fl)
    {
        $this->fl = $fl;

        return $this;
    }

    /**
     * Get fl
     *
     * @return integer
     */
    public function getFl()
    {
        return $this->fl;
    }

    /**
     * Add targetCollection
     *
     * @param \TeamSoft\CrmRepositoryBundle\Entity\InfoTarget $targetCollection
     * @return InfoDictionary
     */
    public function addTargetCollection(\TeamSoft\CrmRepositoryBundle\Entity\InfoTarget $targetCollection)
    {
        $this->targetCollection[] = $targetCollection;

        return $this;
    }

    /**
     * Remove targetCollection
     *
     * @param \TeamSoft\CrmRepositoryBundle\Entity\InfoTarget $targetCollection
     */
    public function removeTargetCollection(\TeamSoft\CrmRepositoryBundle\Entity\InfoTarget $targetCollection)
    {
        $this->targetCollection->removeElement($targetCollection);
    }

    /**
     * Get targetCollection
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getTargetCollection()
    {
        return $this->targetCollection;
    }

    /**
     * Set flags.
     *
     * @param int $flags
     *
     * @return InfoDictionary
     */
    public function setFlags($flags)
    {
        $this->flags = $flags;

        return $this;
    }

    /**
     * Get flags.
     *
     * @return int
     */
    public function getFlags()
    {
        return $this->flags;
    }

    /**
     * Set language
     *
     * @param InfoLanguage|null $content
     * @return InfoDictionary
     */
    public function setLanguage(InfoLanguage $language = null)
    {
        $this->language = $language;

        return $this;
    }

    /**
     * Get language
     *
     * @return InfoLanguage
     */
    public function getLanguage()
    {
        return $this->language;
    }
}
