<?php

declare(strict_types=1);

namespace TeamSoft\CrmRepositoryBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Table(name="info_sklad")
 * @ORM\Entity(repositoryClass="TeamSoft\CrmRepositoryBundle\Repository\InfoStorageRepository")
 */
class InfoStorage
{
    /**
     * @var int|null
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var int|null
     *
     * @ORM\Column(name="ismain", type="integer", nullable=true)
     */
    private $isMain;

    /**
     * @var string|null
     *
     * @ORM\Column(name="description", type="text", nullable=true)
     */
    private $description;

    /**
     * @var int|null
     *
     * @ORM\Column(name="is_archived", type="integer", nullable=true)
     */
    private $isArchived;

    /**
     * @var \TeamSoft\CrmRepositoryBundle\Entity\InfoUser|null
     *
     * @ORM\OneToOne(targetEntity="InfoUser", inversedBy="storage")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     */
    private $user;

    /**
     * @var \TeamSoft\CrmRepositoryBundle\Entity\InfoStorageMove[]|null
     *
     * @ORM\OneToMany(targetEntity="TeamSoft\CrmRepositoryBundle\Entity\InfoStorageMove", mappedBy="storage")
     */
    private $moves;

    public function __construct()
    {
        $this->isMain = 0;
    }

    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return int|null
     */
    public function getIsMain(): ?int
    {
        return $this->isMain;
    }

    /**
     * @param  int|null  $isMain
     *
     * @return $this
     */
    public function setIsMain(?int $isMain): self
    {
        $this->isMain = $isMain;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getDescription(): ?string
    {
        return $this->description;
    }

    /**
     * @param  string|null  $description
     *
     * @return $this
     */
    public function setDescription(?string $description): self
    {
        $this->description = $description;

        return $this;
    }

    /**
     * @return \TeamSoft\CrmRepositoryBundle\Entity\InfoUser|null
     */
    public function getUser(): ?InfoUser
    {
        return $this->user;
    }

    /**
     * @param  \TeamSoft\CrmRepositoryBundle\Entity\InfoUser|null  $user
     *
     * @return $this
     */
    public function setUser(?InfoUser $user): self
    {
        $this->user = $user;

        return $this;
    }

    /**
     * @return int|null
     */
    public function getIsArchived(): ?int
    {
        return $this->isArchived;
    }

    /**
     * @param  int|null  $isArchived
     *
     * @return $this
     */
    public function setIsArchived(?int $isArchived): self
    {
        $this->isArchived = $isArchived;

        return $this;
    }
}
