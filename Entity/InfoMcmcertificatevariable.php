<?php

namespace TeamSoft\CrmRepositoryBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * InfoMcmcertificatevariable
 *
 * @ORM\Table(name="info_mcmcertificatevariable")
 * @ORM\Entity
 */
class InfoMcmcertificatevariable implements ServiceFieldInterface
{
    /**
     * @var int
     *
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string|null
     *
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $color;

    /**
     * @var float|null
     *
     * @ORM\Column(name="position_left", type="float", nullable=true)
     */
    private $positionLeft;

    /**
     * @var float|null
     *
     * @ORM\Column(name="position_top", type="float", nullable=true)
     */
    private $positionTop;

    /**
     * @var int|null
     *
     * @ORM\Column(type="integer", nullable=true)
     */
    private $size;

    /**
     * @var string|null
     *
     * @ORM\Column(name="guid", type="guid", nullable=true)
     */
    private $guid;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="currenttime", type="datetime", nullable=true)
     */
    private $currenttime;

    /**
     * @var string|null
     *
     * @ORM\Column(name="moduser", type="string", length=16, nullable=true)
     */
    private $moduser;

    /**
     * @var InfoMcmfile
     *
     * @ORM\ManyToOne(targetEntity="InfoMcmfile", inversedBy="variableCollection")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="certificate_id", referencedColumnName="id")
     * })
     */
    private $certificate;

    /**
     * @var InfoMcmcontentvariable
     *
     * @ORM\ManyToOne(targetEntity="InfoMcmcontentvariable", inversedBy="variableCollection")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="variable_id", referencedColumnName="id")
     * })
     */
    private $variable;

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set guid.
     *
     * @param string|null $guid
     *
     * @return InfoMcmcertificatevariable
     */
    public function setGuid($guid = null)
    {
        $this->guid = $guid;

        return $this;
    }

    /**
     * Get guid.
     *
     * @return string|null
     */
    public function getGuid(): ?string
    {
        return $this->guid;
    }

    /**
     * Set currenttime.
     *
     * @param \DateTime|null $currenttime
     *
     * @return InfoMcmcertificatevariable
     */
    public function setCurrenttime(\DateTime $currenttime = null): InfoMcmcertificatevariable
    {
        $this->currenttime = $currenttime;

        return $this;
    }

    /**
     * Get currenttime.
     *
     * @return \DateTime|null
     */
    public function getCurrenttime()
    {
        return $this->currenttime;
    }

    /**
     * Set moduser.
     *
     * @param string|null $moduser
     *
     * @return InfoMcmcertificatevariable
     */
    public function setModuser($moduser = null): InfoMcmcertificatevariable
    {
        $this->moduser = $moduser;

        return $this;
    }

    /**
     * Get moduser.
     *
     * @return string|null
     */
    public function getModuser()
    {
        return $this->moduser;
    }

    /**
     * @return string|null
     */
    public function getColor(): ?string
    {
        return $this->color;
    }

    /**
     * @param string|null $color
     */
    public function setColor(?string $color): void
    {
        $this->color = $color;
    }

    /**
     * @return float|null
     */
    public function getPositionLeft(): ?float
    {
        return $this->positionLeft;
    }

    /**
     * @param float|null $positionLeft
     */
    public function setPositionLeft(?float $positionLeft): void
    {
        $this->positionLeft = $positionLeft;
    }

    /**
     * @return float|null
     */
    public function getPositionTop(): ?float
    {
        return $this->positionTop;
    }

    /**
     * @param float|null $positionTop
     */
    public function setPositionTop(?float $positionTop): void
    {
        $this->positionTop = $positionTop;
    }

    /**
     * @return int|null
     */
    public function getSize(): ?int
    {
        return $this->size;
    }

    /**
     * @param int|null $size
     */
    public function setSize(?int $size): void
    {
        $this->size = $size;
    }

    /**
     * @return InfoMcmfile|null
     */
    public function getCertificate(): ?InfoMcmfile
    {
        return $this->certificate;
    }

    /**
     * @param InfoMcmfile|null $certificate
     */
    public function setCertificate(?InfoMcmfile $certificate): void
    {
        $this->certificate = $certificate;
    }

    /**
     * @return InfoMcmcontentvariable|null
     */
    public function getVariable(): ?InfoMcmcontentvariable
    {
        return $this->variable;
    }

    /**
     * @param InfoMcmcontentvariable $variable
     */
    public function setVariable(InfoMcmcontentvariable $variable): void
    {
        $this->variable = $variable;
    }
}
