<?php
namespace TeamSoft\CrmRepositoryBundle\Entity;

interface AdditionalFieldInterface
{

    /**
     * @return string
     */
    public function getStringvalue();

    /**
     * @return int
     */
    public function getIntvalue();

    /**
     * @return float
     */
    public function getFloatvalue();

    /**
     * @return \DateTime
     */
    public function getDatevalue();

    /**
     * @return string
     */
    public function getDictvalue();

    /**
     * @return string
     */
    public function getSysdictvalue();

    /**
     * @return \TeamSoft\CrmRepositoryBundle\Entity\InfoAddinfotype
     */
    public function getInfotype();

    /**
     * @return mixed
     */
    public function getValue();




    /**
     * @param int $id
     */
    public function setSysdictvalueId($id);

    /**
     * @return int
     */
    public function getSysdictvalueId();

    /**
     * @param string $value
     */
    public function setSysdictvalue($value);

}
