<?php

namespace TeamSoft\CrmRepositoryBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Criteria;
use Doctrine\ORM\Mapping as ORM;

/**
 * InfoSurveyFilter
 *
 * @ORM\Table(name="info_surveyfilter")
 * @ORM\Entity
 */
class InfoSurveyFilter implements ServiceFieldInterface
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255, nullable=true)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="sqlfilter", type="varchar_image", nullable=true)
     */
    private $sqlFilter;

    /**
     * @var string
     *
     * @ORM\Column(name="sqlitefilter", type="varchar_image", nullable=true)
     */
    private $sqliteFilter;

    /**
     * @var string
     *
     * @ORM\Column(name="json_filter", type="string", nullable=true)
     */
    private $jsonFilter;

    /**
     * @var string
     *
     * @ORM\Column(name="guid", type="guid", nullable=true)
     */
    private $guid;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="currenttime", type="datetime", nullable=true)
     */
    private $currenttime;

    /**
     * @var string
     *
     * @ORM\Column(name="moduser", type="string", length=16, nullable=true)
     */
    private $moduser;

    /**
     * @var InfoSurvey
     *
     * @ORM\ManyToOne(targetEntity="InfoSurvey")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="survey_id", referencedColumnName="id")
     * })
     */
    private $survey;

    public function __clone()
    {
        $this->id = null;
        $this->guid = null;
        $this->currenttime = null;
        $this->moduser = null;
    }

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name.
     *
     * @param string|null $name
     *
     * @return InfoSurveyFilter
     */
    public function setName($name = null)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name.
     *
     * @return string|null
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set sqlFilter.
     *
     * @param string|null $sqlFilter
     *
     * @return InfoSurveyFilter
     */
    public function setSqlFilter($sqlFilter = null)
    {
        $this->sqlFilter = $sqlFilter;

        return $this;
    }

    /**
     * Get sqlFilter.
     *
     * @return string|null
     */
    public function getSqlFilter()
    {
        return $this->sqlFilter;
    }

    /**
     * Set sqliteFilter.
     *
     * @param string|null $sqliteFilter
     *
     * @return InfoSurveyFilter
     */
    public function setSqliteFilter($sqliteFilter = null)
    {
        $this->sqliteFilter = $sqliteFilter;

        return $this;
    }

    /**
     * Get sqliteFilter.
     *
     * @return string|null
     */
    public function getSqliteFilter()
    {
        return $this->sqliteFilter;
    }

    /**
     * Set jsonFilter.
     *
     * @param string|null $jsonFilter
     *
     * @return InfoSurveyFilter
     */
    public function setJsonFilter($jsonFilter = null)
    {
        $this->jsonFilter = $jsonFilter;

        return $this;
    }

    /**
     * Get jsonFilter.
     *
     * @return string|null
     */
    public function getJsonFilter()
    {
        return $this->jsonFilter;
    }

    /**
     * Set guid.
     *
     * @param string|null $guid
     *
     * @return InfoSurveyFilter
     */
    public function setGuid($guid = null)
    {
        $this->guid = $guid;

        return $this;
    }

    /**
     * Get guid.
     *
     * @return string|null
     */
    public function getGuid()
    {
        return $this->guid;
    }

    /**
     * Set currenttime.
     *
     * @param \DateTime|null $currenttime
     *
     * @return InfoSurveyFilter
     */
    public function setCurrenttime(\DateTime $currenttime = null)
    {
        $this->currenttime = $currenttime;

        return $this;
    }

    /**
     * Get currenttime.
     *
     * @return \DateTime|null
     */
    public function getCurrenttime()
    {
        return $this->currenttime;
    }

    /**
     * Set moduser.
     *
     * @param string|null $moduser
     *
     * @return InfoSurveyFilter
     */
    public function setModuser($moduser = null)
    {
        $this->moduser = $moduser;

        return $this;
    }

    /**
     * Get moduser.
     *
     * @return string|null
     */
    public function getModuser()
    {
        return $this->moduser;
    }

    /**
     * Set survey.
     *
     * @param InfoSurvey|null $survey
     *
     * @return InfoSurveyFilter
     */
    public function setSurvey(InfoSurvey $survey = null)
    {
        $this->survey = $survey;

        return $this;
    }

    /**
     * Get survey.
     *
     * @return InfoSurvey
     */
    public function getSurvey()
    {
        return $this->survey;
    }
}
