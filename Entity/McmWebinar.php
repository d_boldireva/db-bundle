<?php

namespace TeamSoft\CrmRepositoryBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * McmWebinar
 *
 * @ORM\Table(name="mcm_webinar")
 * @ORM\Entity
 */
class McmWebinar
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string|null
     *
     * @ORM\Column(name="name", type="string", length=255, nullable=true)
     */
    private $name;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="webinardate", type="datetime", nullable=true)
     */
    private $webinardate;

    /**
     * @var string|null
     *
     * @ORM\Column(name="webinarurl", type="text", length=-1, nullable=true)
     */
    private $webinarurl;

    /**
     * @var string|null
     *
     * @ORM\Column(name="webinarurlforconnect", type="text", length=-1, nullable=true)
     */
    private $webinarurlforconnect;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="createdate", type="datetime", nullable=true)
     */
    private $createdate = 'CURRENT_TIMESTAMP';

    /**
     * @var int|null
     *
     * @ORM\Column(name="isarchive", type="integer", nullable=true)
     */
    private $isarchive;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="currenttime", type="datetime", nullable=true)
     */
    private $currenttime = 'CURRENT_TIMESTAMP';

    /**
     * @var string|null
     *
     * @ORM\Column(name="moduser", type="string", length=16, nullable=true)
     */
    private $moduser = '[dbo].[Get_CurrentCode]()';

    /**
     * @var int|null
     *
     * @ORM\Column(name="morionid", type="integer", nullable=true)
     */
    private $morionid;

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name.
     *
     * @param string|null $name
     *
     * @return McmWebinar
     */
    public function setName($name = null)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name.
     *
     * @return string|null
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set webinardate.
     *
     * @param \DateTime|null $webinardate
     *
     * @return McmWebinar
     */
    public function setWebinardate($webinardate = null)
    {
        $this->webinardate = $webinardate;

        return $this;
    }

    /**
     * Get webinardate.
     *
     * @return \DateTime|null
     */
    public function getWebinardate()
    {
        return $this->webinardate;
    }

    /**
     * Set webinarurl.
     *
     * @param string|null $webinarurl
     *
     * @return McmWebinar
     */
    public function setWebinarurl($webinarurl = null)
    {
        $this->webinarurl = $webinarurl;

        return $this;
    }

    /**
     * Get webinarurl.
     *
     * @return string|null
     */
    public function getWebinarurl()
    {
        return $this->webinarurl;
    }

    /**
     * Set webinarurlforconnect.
     *
     * @param string|null $webinarurlforconnect
     *
     * @return McmWebinar
     */
    public function setWebinarurlforconnect($webinarurlforconnect = null)
    {
        $this->webinarurlforconnect = $webinarurlforconnect;

        return $this;
    }

    /**
     * Get webinarurlforconnect.
     *
     * @return string|null
     */
    public function getWebinarurlforconnect()
    {
        return $this->webinarurlforconnect;
    }

    /**
     * Set createdate.
     *
     * @param \DateTime|null $createdate
     *
     * @return McmWebinar
     */
    public function setCreatedate($createdate = null)
    {
        $this->createdate = $createdate;

        return $this;
    }

    /**
     * Get createdate.
     *
     * @return \DateTime|null
     */
    public function getCreatedate()
    {
        return $this->createdate;
    }

    /**
     * Set isarchive.
     *
     * @param int|null $isarchive
     *
     * @return McmWebinar
     */
    public function setIsarchive($isarchive = null)
    {
        $this->isarchive = $isarchive;

        return $this;
    }

    /**
     * Get isarchive.
     *
     * @return int|null
     */
    public function getIsarchive()
    {
        return $this->isarchive;
    }

    /**
     * Set currenttime.
     *
     * @param \DateTime|null $currenttime
     *
     * @return McmWebinar
     */
    public function setCurrenttime($currenttime = null)
    {
        $this->currenttime = $currenttime;

        return $this;
    }

    /**
     * Get currenttime.
     *
     * @return \DateTime|null
     */
    public function getCurrenttime()
    {
        return $this->currenttime;
    }

    /**
     * Set moduser.
     *
     * @param string|null $moduser
     *
     * @return McmWebinar
     */
    public function setModuser($moduser = null)
    {
        $this->moduser = $moduser;

        return $this;
    }

    /**
     * Get moduser.
     *
     * @return string|null
     */
    public function getModuser()
    {
        return $this->moduser;
    }

    /**
     * Set morionid.
     *
     * @param int|null $morionid
     *
     * @return McmWebinar
     */
    public function setMorionid($morionid = null)
    {
        $this->morionid = $morionid;

        return $this;
    }

    /**
     * Get morionid.
     *
     * @return int|null
     */
    public function getMorionid()
    {
        return $this->morionid;
    }
}
