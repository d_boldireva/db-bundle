<?php

namespace TeamSoft\CrmRepositoryBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use TeamSoft\CrmRepositoryBundle\Entity\ServiceFieldInterface;

/**
 * InfoDevice
 *
 * @ORM\Table(name="info_device")
 * @ORM\Entity
 */
class InfoDevice implements ServiceFieldInterface
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="model", type="string", length=255, nullable=true)
     */
    private $model;

    /**
     * @var string
     *
     * @ORM\Column(name="systemversion", type="string", length=255, nullable=true)
     */
    private $systemversion;

    /**
     * @var string
     *
     * @ORM\Column(name="imei", type="string", length=255, nullable=true)
     */
    private $imei;


    /**
     * @var string
     *
     * @ORM\Column(name="appversion", type="string", length=255, nullable=true)
     */
    private $appversion;


    /**
     * @var string
     *
     * @ORM\Column(name="installappversion", type="string", length=255, nullable=true)
     */
    private $installappversion;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dt", type="datetime", nullable=true)
     */
    private $dt;

    /**
     * @var integer
     *
     * @ORM\Column(name="isblocked", type="integer", nullable=true)
     */
    private $isblocked;


    /**
     * @var \DateTime
     *
     * @ORM\Column(name="installdate", type="datetime", nullable=true)
     */
    private $installdate;

    /**
     * @var string
     *
     * @ORM\Column(name="guid", type="guid", nullable=true)
     */
    private $guid;

    /**
     * @var \DateTime
     */
    private $currenttime;

    /**
     * @var string
     */
    private $moduser;

    /**
     * @var \TeamSoft\CrmRepositoryBundle\Entity\InfoUser
     *
     * @ORM\ManyToOne(targetEntity="\TeamSoft\CrmRepositoryBundle\Entity\InfoUser")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     * })
     */
    private $user;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set model
     *
     * @param string $model
     *
     * @return InfoDevice
     */
    public function setModel($model)
    {
        $this->model = $model;
    
        return $this;
    }

    /**
     * Get model
     *
     * @return string
     */
    public function getModel()
    {
        return $this->model;
    }

    /**
     * Set systemversion
     *
     * @param string $systemversion
     *
     * @return InfoDevice
     */
    public function setSystemversion($systemversion)
    {
        $this->systemversion = $systemversion;

        return $this;
    }

    /**
     * Get systemversion
     *
     * @return string
     */
    public function getSystemversion()
    {
        return $this->systemversion;
    }

    /**
     * Set imei
     *
     * @param string $imei
     *
     * @return InfoDevice
     */
    public function setImei($imei)
    {
        $this->imei = $imei;

        return $this;
    }

    /**
     * Get imei
     *
     * @return string
     */
    public function getImei()
    {
        return $this->imei;
    }

    /**
     * Set installappversion
     *
     * @param string $installappversion
     *
     * @return InfoDevice
     */
    public function setInstallappversion($installappversion)
    {
        $this->installappversion = $installappversion;

        return $this;
    }

    /**
     * Get installappversion
     *
     * @return string
     */
    public function getInstallappversion()
    {
        return $this->installappversion;
    }

    /**
     * Set guid
     *
     * @param string $guid
     *
     * @return InfoDevice
     */
    public function setGuid($guid)
    {
        $this->guid = $guid;

        return $this;
    }

    /**
     * Get guid
     *
     * @return string
     */
    public function getGuid()
    {
        return $this->guid;
    }

    /**
     * Set currenttime
     *
     * @param \DateTime $currenttime
     *
     * @return InfoDevice
     */
    public function setCurrenttime(\DateTime $currenttime)
    {
        $this->currenttime = $currenttime;
    
        return $this;
    }

    /**
     * Get currenttime
     *
     * @return \DateTime
     */
    public function getCurrenttime()
    {
        return $this->currenttime;
    }

    /**
     * Set dt
     *
     * @param \DateTime $dt
     *
     * @return InfoDevice
     */
    public function setDt(\DateTime $dt)
    {
        $this->dt = $dt;

        return $this;
    }

    /**
     * Get dt
     *
     * @return \DateTime
     */
    public function getDt()
    {
        return $this->dt;
    }

    /**
     * Set installdate
     *
     * @param \DateTime installdate
     *
     * @return InfoDevice
     */
    public function setInstalldate(\DateTime $installdate)
    {
        $this->installdate = $installdate;

        return $this;
    }

    /**
     * Get installdate
     *
     * @return \DateTime
     */
    public function getInstalldate()
    {
        return $this->installdate;
    }

    /**
     * Set moduser
     *
     * @param string $moduser
     *
     * @return InfoDevice
     */
    public function setModuser($moduser)
    {
        $this->moduser = $moduser;
    
        return $this;
    }

    /**
     * Get moduser
     *
     * @return string
     */
    public function getModuser()
    {
        return $this->moduser;
    }

    /**
     * Set isblocked
     *
     * @param string $isblocked
     *
     * @return InfoDevice
     */
    public function setIsblocked($isblocked)
    {
        $this->isblocked = $isblocked;

        return $this;
    }

    /**
     * Get isblocked
     *
     * @return string
     */
    public function getIsblocked()
    {
        return $this->isblocked;
    }

    /**
     * Set user
     *
     * @param \TeamSoft\CrmRepositoryBundle\Entity\InfoUser $user
     *
     * @return InfoDevice
     */
    public function setUser(\TeamSoft\CrmRepositoryBundle\Entity\InfoUser $user = null)
    {
        $this->user = $user;
    
        return $this;
    }

    /**
     * Get user
     *
     * @return \TeamSoft\CrmRepositoryBundle\Entity\InfoUser
     */
    public function getUser()
    {
        return $this->user;
    }
}

