<?php

namespace TeamSoft\CrmRepositoryBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * InfoSalefact1
 *
 * @ORM\Table(name="info_salefact1", indexes={@ORM\Index(name="ix_info_salefact1_guid", columns={"guid"}), @ORM\Index(name="ix_info_salefact1_time", columns={"time"}), @ORM\Index(name="ix_info_salefact1_company_id", columns={"company_id"}), @ORM\Index(name="ix_info_salefact1_country_id", columns={"country_id"}), @ORM\Index(name="ix_info_salefact1_Distributor_id", columns={"Distributor_id"}), @ORM\Index(name="ix_info_salefact1_Preparation_id", columns={"Preparation_id"}), @ORM\Index(name="ix_info_salefact1_Region_id", columns={"Region_id"}), @ORM\Index(name="ix_info_salefact1_SalePeriod_id", columns={"SalePeriod_id"})})
 * @ORM\Entity
 */
class InfoSalefact1
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var int|null
     *
     * @ORM\Column(name="Count", type="integer", nullable=true)
     */
    private $count;

    /**
     * @var int|null
     *
     * @ORM\Column(name="Amount", type="integer", nullable=true)
     */
    private $amount;

    /**
     * @var string|null
     *
     * @ORM\Column(name="guid", type="guid", nullable=true)
     */
    private $guid;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="currenttime", type="datetime", nullable=true)
     */
    private $currenttime;

    /**
     * @var string|null
     *
     * @ORM\Column(name="moduser", type="string", length=16, nullable=true)
     */
    private $moduser;

    /**
     * @var \InfoRegion
     *
     * @ORM\ManyToOne(targetEntity="InfoRegion")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="Region_id", referencedColumnName="id")
     * })
     */
    private $region;

    /**
     * @var \InfoCompany
     *
     * @ORM\ManyToOne(targetEntity="InfoCompany")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="company_id", referencedColumnName="id")
     * })
     */
    private $company;

    /**
     * @var \InfoDistributor
     *
     * @ORM\ManyToOne(targetEntity="InfoDistributor")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="Distributor_id", referencedColumnName="id")
     * })
     */
    private $distributor;

    /**
     * @var \InfoPreparation
     *
     * @ORM\ManyToOne(targetEntity="InfoPreparation")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="Preparation_id", referencedColumnName="id")
     * })
     */
    private $preparation;

    /**
     * @var \InfoSaleperiod
     *
     * @ORM\ManyToOne(targetEntity="InfoSaleperiod")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="SalePeriod_id", referencedColumnName="id")
     * })
     */
    private $saleperiod;

    /**
     * @var \InfoCountry
     *
     * @ORM\ManyToOne(targetEntity="InfoCountry")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="country_id", referencedColumnName="id")
     * })
     */
    private $country;



    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set count.
     *
     * @param int|null $count
     *
     * @return InfoSalefact1
     */
    public function setCount($count = null)
    {
        $this->count = $count;

        return $this;
    }

    /**
     * Get count.
     *
     * @return int|null
     */
    public function getCount()
    {
        return $this->count;
    }

    /**
     * Set amount.
     *
     * @param int|null $amount
     *
     * @return InfoSalefact1
     */
    public function setAmount($amount = null)
    {
        $this->amount = $amount;

        return $this;
    }

    /**
     * Get amount.
     *
     * @return int|null
     */
    public function getAmount()
    {
        return $this->amount;
    }

    /**
     * Set guid.
     *
     * @param string|null $guid
     *
     * @return InfoSalefact1
     */
    public function setGuid($guid = null)
    {
        $this->guid = $guid;

        return $this;
    }

    /**
     * Get guid.
     *
     * @return string|null
     */
    public function getGuid()
    {
        return $this->guid;
    }

    /**
     * Set currenttime.
     *
     * @param \DateTime|null $currenttime
     *
     * @return InfoSalefact1
     */
    public function setCurrenttime($currenttime = null)
    {
        $this->currenttime = $currenttime;

        return $this;
    }

    /**
     * Get currenttime.
     *
     * @return \DateTime|null
     */
    public function getCurrenttime()
    {
        return $this->currenttime;
    }

    /**
     * Set moduser.
     *
     * @param string|null $moduser
     *
     * @return InfoSalefact1
     */
    public function setModuser($moduser = null)
    {
        $this->moduser = $moduser;

        return $this;
    }

    /**
     * Get moduser.
     *
     * @return string|null
     */
    public function getModuser()
    {
        return $this->moduser;
    }

    /**
     * Set region.
     *
     * @param \TeamSoft\CrmRepositoryBundle\Entity\InfoRegion|null $region
     *
     * @return InfoSalefact1
     */
    public function setRegion(\TeamSoft\CrmRepositoryBundle\Entity\InfoRegion $region = null)
    {
        $this->region = $region;

        return $this;
    }

    /**
     * Get region.
     *
     * @return \TeamSoft\CrmRepositoryBundle\Entity\InfoRegion|null
     */
    public function getRegion()
    {
        return $this->region;
    }

    /**
     * Set company.
     *
     * @param \TeamSoft\CrmRepositoryBundle\Entity\InfoCompany|null $company
     *
     * @return InfoSalefact1
     */
    public function setCompany(\TeamSoft\CrmRepositoryBundle\Entity\InfoCompany $company = null)
    {
        $this->company = $company;

        return $this;
    }

    /**
     * Get company.
     *
     * @return \TeamSoft\CrmRepositoryBundle\Entity\InfoCompany|null
     */
    public function getCompany()
    {
        return $this->company;
    }

    /**
     * Set distributor.
     *
     * @param \TeamSoft\CrmRepositoryBundle\Entity\InfoDistributor|null $distributor
     *
     * @return InfoSalefact1
     */
    public function setDistributor(\TeamSoft\CrmRepositoryBundle\Entity\InfoDistributor $distributor = null)
    {
        $this->distributor = $distributor;

        return $this;
    }

    /**
     * Get distributor.
     *
     * @return \TeamSoft\CrmRepositoryBundle\Entity\InfoDistributor|null
     */
    public function getDistributor()
    {
        return $this->distributor;
    }

    /**
     * Set preparation.
     *
     * @param \TeamSoft\CrmRepositoryBundle\Entity\InfoPreparation|null $preparation
     *
     * @return InfoSalefact1
     */
    public function setPreparation(\TeamSoft\CrmRepositoryBundle\Entity\InfoPreparation $preparation = null)
    {
        $this->preparation = $preparation;

        return $this;
    }

    /**
     * Get preparation.
     *
     * @return \TeamSoft\CrmRepositoryBundle\Entity\InfoPreparation|null
     */
    public function getPreparation()
    {
        return $this->preparation;
    }

    /**
     * Set saleperiod.
     *
     * @param \TeamSoft\CrmRepositoryBundle\Entity\InfoSaleperiod|null $saleperiod
     *
     * @return InfoSalefact1
     */
    public function setSaleperiod(\TeamSoft\CrmRepositoryBundle\Entity\InfoSaleperiod $saleperiod = null)
    {
        $this->saleperiod = $saleperiod;

        return $this;
    }

    /**
     * Get saleperiod.
     *
     * @return \TeamSoft\CrmRepositoryBundle\Entity\InfoSaleperiod|null
     */
    public function getSaleperiod()
    {
        return $this->saleperiod;
    }

    /**
     * Set country.
     *
     * @param \TeamSoft\CrmRepositoryBundle\Entity\InfoCountry|null $country
     *
     * @return InfoSalefact1
     */
    public function setCountry(\TeamSoft\CrmRepositoryBundle\Entity\InfoCountry $country = null)
    {
        $this->country = $country;

        return $this;
    }

    /**
     * Get country.
     *
     * @return \TeamSoft\CrmRepositoryBundle\Entity\InfoCountry|null
     */
    public function getCountry()
    {
        return $this->country;
    }
}
