<?php

namespace TeamSoft\CrmRepositoryBundle\Entity\RTC;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use TeamSoft\CrmRepositoryBundle\Entity\InfoUser;
use TeamSoft\CrmRepositoryBundle\Entity\ServiceFieldInterface;

/**
 * InfoRtcRoom
 *
 * @ORM\Table(name="info_rtcroom")
 * @ORM\Entity(repositoryClass="TeamSoft\CrmRepositoryBundle\Repository\RTC\InfoRtcRoom")
 * @UniqueEntity(fields={"sipUser"})
 */
class InfoRtcRoom implements ServiceFieldInterface {
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id()
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="sip_user", type="string", length=255, nullable=false)
     */
    private $sipUser;

    /**
     * @var string
     *
     * @ORM\Column(name="sip_pass", type="string", length=255, nullable=false)
     */
    private $sipPass;

    /**
     * @var string
     *
     * @ORM\Column(name="guid", type="guid", nullable=true)
     */
    private $guid;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="currenttime", type="datetime", nullable=true)
     */
    private $currenttime;

    /**
     * @var string
     *
     * @ORM\Column(name="moduser", type="string", length=16, nullable=true)
     */
    private $moduser;

    /**
     * @var InfoUser
     *
     * @ORM\ManyToOne(targetEntity="\TeamSoft\CrmRepositoryBundle\Entity\InfoUser", inversedBy="rtcRooms")
     * @ORM\JoinColumn(name="responsible_id", referencedColumnName="id", onDelete="SET NULL")
     */
    private $responsible;

    /**
     * @var ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="InfoRtcRoomReserve", mappedBy="rtcRoom", cascade={"persist"})
     */
    private $rtcRoomReserves;

    /**
     * InfoRtcRoom constructor.
     */
    public function __construct()
    {
        $this->rtcRoomReserves = new ArrayCollection();
    }

    /**
     * @return int
     */
    public function getId(){
        return $this->id;
    }

    /**
     * @param string $sipUser
     * @return $this
     */
    public function setSipUser(string $sipUser){
        $this->sipUser = $sipUser;
        return $this;
    }

    /**
     * @return string
     */
    public function getSipUser(){
        return $this->sipUser;
    }

    /**
     * @param string $sipPass
     * @return $this
     */
    public function setSipPass(string $sipPass){
        $this->sipPass = $sipPass;
        return $this;
    }

    /**
     * @return string
     */
    public function getSipPass(){
        return $this->sipPass;
    }

    /**
     * Set guid
     *
     * @param string $guid
     * @return $this
     */
    public function setGuid($guid)
    {
        $this->guid = $guid;

        return $this;
    }

    /**
     * Get guid
     *
     * @return string
     */
    public function getGuid()
    {
        return $this->guid;
    }

    /**
     * Set currenttime
     *
     * @param \DateTime $currenttime
     * @return $this
     */
    public function setCurrenttime(\DateTime $currenttime = null)
    {
        $this->currenttime = $currenttime;

        return $this;
    }

    /**
     * Get currenttime
     *
     * @return \DateTime
     */
    public function getCurrenttime()
    {
        return $this->currenttime;
    }

    /**
     * Set moduser
     *
     * @param string $moduser
     * @return $this
     */
    public function setModuser($moduser)
    {
        $this->moduser = $moduser;

        return $this;
    }

    /**
     * Get moduser
     *
     * @return string
     */
    public function getModuser()
    {
        return $this->moduser;
    }

    /**
     * @return int|null
     */
    public function getResponsibleId(){
        return $this->responsible ? $this->responsible->getId() : null;
    }

    /**
     * @return InfoUser
     */
    public function getResponsible(): InfoUser{
        return $this->responsible;
    }

    /**
     * @param InfoUser $responsible
     * @return $this
     */
    public function setResponsible(InfoUser $responsible){
        $this->responsible = $responsible;
        return $this;
    }

    /**
     * @return ArrayCollection
     */
    public function getRtcRoomReserves()
    {
        return $this->rtcRoomReserves;
    }

    /**
     * @param InfoRtcRoomReserve $RTCRoomReserve
     * @return $this
     */
    public function addRtcRoomReserve(InfoRtcRoomReserve $RTCRoomReserve){
        if(!$this->rtcRoomReserves->contains($RTCRoomReserve)){
            $this->rtcRoomReserves->add($RTCRoomReserve);
        }
        $RTCRoomReserve->setRtcRoom($this);
        return $this;
    }

    /**
     * @param InfoRtcRoomReserve $RTCRoomReserve
     * @return $this
     */
    public function removeRtcRoomReserve(InfoRtcRoomReserve $RTCRoomReserve){
        if($this->rtcRoomReserves->contains($RTCRoomReserve)){
            $this->rtcRoomReserves->removeElement($RTCRoomReserve);
        }
        $RTCRoomReserve->setRtcRoom(null);
        return $this;
    }

    /**
     * @return $this
     */
    public function clearRtcRoomReserves(){
        $this->rtcRoomReserves->clear();
        return $this;
    }
}
