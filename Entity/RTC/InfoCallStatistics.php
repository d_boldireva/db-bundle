<?php

namespace TeamSoft\CrmRepositoryBundle\Entity\RTC;

use Doctrine\ORM\Mapping as ORM;
use TeamSoft\CrmRepositoryBundle\Entity\InfoCompany;
use TeamSoft\CrmRepositoryBundle\Entity\InfoContact;
use TeamSoft\CrmRepositoryBundle\Entity\InfoContactphone;
use TeamSoft\CrmRepositoryBundle\Entity\InfoTask;
use TeamSoft\CrmRepositoryBundle\Entity\InfoUser;
use TeamSoft\CrmRepositoryBundle\Entity\ServiceFieldInterface;

/**
 * InfoCallStatistics
 *
 * @ORM\Table(name="info_callstatistics")
 * @ORM\Entity
 */
class InfoCallStatistics implements ServiceFieldInterface {
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id()
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="datetime", type="datetime", nullable=false)
     */
    private $datetime;

    /**
     * @var int
     *
     * @ORM\Column(name="duration", type="integer", nullable=true)
     */
    private $duration;

    /**
     * @var string
     *
     * @ORM\Column(name="guid", type="guid", nullable=true)
     */
    private $guid;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="currenttime", type="datetime", nullable=true)
     */
    private $currenttime;

    /**
     * @var string
     *
     * @ORM\Column(name="moduser", type="string", length=16, nullable=true)
     */
    private $moduser;

    /**
     * @var InfoRtcRoomReserve
     *
     * @ORM\ManyToOne(targetEntity="InfoRtcRoomReserve", inversedBy="callStatisticsCollection")
     * @ORM\JoinColumn(name="rtcroomreserve_id", referencedColumnName="id", onDelete="SET NULL")
     */
    private $rtcRoomReserve;

    /**
     * @var InfoUser
     *
     * @ORM\ManyToOne(targetEntity="\TeamSoft\CrmRepositoryBundle\Entity\InfoUser")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id", onDelete="SET NULL")
     */
    private $user;

    /**
     * @var InfoContact
     *
     * @ORM\ManyToOne(targetEntity="\TeamSoft\CrmRepositoryBundle\Entity\InfoContact")
     * @ORM\JoinColumn(name="contact_id", referencedColumnName="id", onDelete="SET NULL")
     */
    private $contact;

    /**
     * @var InfoContactphone
     *
     * @ORM\ManyToOne(targetEntity="\TeamSoft\CrmRepositoryBundle\Entity\InfoContactphone")
     * @ORM\JoinColumn(name="contactphone_id", referencedColumnName="id", onDelete="SET NULL")
     */
    private $contactphone;

    /**
     * @var InfoTask
     *
     * @ORM\ManyToOne(targetEntity="\TeamSoft\CrmRepositoryBundle\Entity\InfoTask")
     * @ORM\JoinColumn(name="task_id", referencedColumnName="id", onDelete="SET NULL")
     */
    private $task;

    /**
     * @var InfoCompany
     *
     * @ORM\ManyToOne(targetEntity="\TeamSoft\CrmRepositoryBundle\Entity\InfoCompany")
     * @ORM\JoinColumn(name="company_id", referencedColumnName="id", onDelete="SET NULL")
     */
    private $company;

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return \DateTime
     */
    public function getDatetime()
    {
        return $this->datetime;
    }

    /**
     * @param \DateTime $datetime
     * @return InfoCallStatistics
     */
    public function setDatetime($datetime)
    {
        $this->datetime = $datetime;
        return $this;
    }

    /**
     * @return int|null
     */
    public function getDuration(): ?int
    {
        return $this->duration;
    }

    /**
     * @param int|null $duration
     * @return InfoCallStatistics
     */
    public function setDuration(?int $duration)
    {
        $this->duration = $duration;
        return $this;
    }


    /**
     * Set guid
     *
     * @param string $guid
     * @return $this
     */
    public function setGuid($guid)
    {
        $this->guid = $guid;

        return $this;
    }

    /**
     * Get guid
     *
     * @return string
     */
    public function getGuid()
    {
        return $this->guid;
    }

    /**
     * Set currenttime
     *
     * @param \DateTime $currenttime
     * @return $this
     */
    public function setCurrenttime(\DateTime $currenttime = null)
    {
        $this->currenttime = $currenttime;

        return $this;
    }

    /**
     * Get currenttime
     *
     * @return \DateTime
     */
    public function getCurrenttime()
    {
        return $this->currenttime;
    }

    /**
     * Set moduser
     *
     * @param string $moduser
     * @return $this
     */
    public function setModuser($moduser)
    {
        $this->moduser = $moduser;

        return $this;
    }

    /**
     * Get moduser
     *
     * @return string
     */
    public function getModuser()
    {
        return $this->moduser;
    }

    /**
     * @return int|null
     */
    public function getRtcRoomReserveId(){
        return $this->rtcRoomReserve ? $this->rtcRoomReserve->getId() : null;
    }

    /**
     * @return InfoRtcRoomReserve
     */
    public function getRtcRoomReserve()
    {
        return $this->rtcRoomReserve;
    }

    /**
     * @param InfoRtcRoomReserve $rtcRoomReserve
     * @return InfoCallStatistics
     */
    public function setRtcRoomReserve(?InfoRtcRoomReserve $rtcRoomReserve)
    {
        $this->rtcRoomReserve = $rtcRoomReserve;
        return $this;
    }

    /**
     * @return int|null
     */
    public function getUserId(){
        return $this->user ? $this->user->getId() : null;
    }

    /**
     * @return InfoUser
     */
    public function getUser(){
        return $this->user;
    }

    /**
     * @param InfoUser $user
     * @return $this
     */
    public function setUser(?InfoUser $user){
        $this->user = $user;
        return $this;
    }

    /**
     * @return int|null
     */
    public function getContactId(){
        return $this->contact ? $this->contact->getId() : null;
    }

    /**
     * @return InfoContact
     */
    public function getContact(){
        return $this->contact;
    }

    /**
     * @param InfoContact $contact
     * @return $this
     */
    public function setContact(?InfoContact $contact){
        $this->contact = $contact;
        return $this;
    }

    /**
     * @return int|null
     */
    public function getContactphoneId(){
        return $this->contactphone ? $this->contactphone->getId() : null;
    }

    /**
     * @return InfoContactphone
     */
    public function getContactphone(){
        return $this->contactphone;
    }

    /**
     * @param InfoContactphone $contactphone
     * @return $this
     */
    public function setContactphone(?InfoContactphone $contactphone){
        $this->contactphone = $contactphone;
        return $this;
    }

    /**
     * @return int|null
     */
    public function getTaskId(){
        return $this->task ? $this->task->getId() : null;
    }

    /**
     * @return InfoTask
     */
    public function getTask(){
        return $this->task;
    }

    /**
     * @param InfoTask $task
     * @return $this
     */
    public function setTask(InfoTask $task){
        $this->task = $task;
        return $this;
    }

    /**
     * @return int|null
     */
    public function getCompanyId(){
        return $this->company ? $this->company->getId() : null;
    }

    /**
     * @return InfoCompany
     */
    public function getCompany(){
        return $this->company;
    }

    /**
     * @param InfoCompany $company
     * @return $this
     */
    public function setCompany(?InfoCompany $company){
        $this->company = $company;
        return $this;
    }
}
