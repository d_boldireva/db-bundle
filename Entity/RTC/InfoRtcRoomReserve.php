<?php

namespace TeamSoft\CrmRepositoryBundle\Entity\RTC;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use TeamSoft\CrmRepositoryBundle\Entity\InfoContact;
use TeamSoft\CrmRepositoryBundle\Entity\InfoTask;
use TeamSoft\CrmRepositoryBundle\Entity\ServiceFieldInterface;

/**
 * InfoRtcRoomReserve
 *
 * @ORM\Table(name="info_rtcroomreserve")
 * @ORM\Entity(repositoryClass="TeamSoft\CrmRepositoryBundle\Repository\RTC\InfoRtcRoomReserve")
 */
class InfoRtcRoomReserve implements ServiceFieldInterface {
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id()
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="datefrom", type="datetime", nullable=false)
     */
    private $datefrom;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="datetill", type="datetime", nullable=false)
     */
    private $datetill;

    /**
     * @var string
     *
     * @ORM\Column(name="notes", type="text", nullable=true)
     */
    private $notes;

    /**
     * @var string
     *
     * @ORM\Column(name="guid", type="guid", nullable=true)
     */
    private $guid;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="currenttime", type="datetime", nullable=true)
     */
    private $currenttime;

    /**
     * @var string
     *
     * @ORM\Column(name="moduser", type="string", length=16, nullable=true)
     */
    private $moduser;

    /**
     * @var InfoRtcRoom
     *
     * @ORM\ManyToOne(targetEntity="InfoRtcRoom", inversedBy="rtcRoomReserves")
     * @ORM\JoinColumn(name="rtcroom_id", referencedColumnName="id", onDelete="CASCADE")
     */
    private $rtcRoom;

    /**
     * @var InfoContact
     *
     * @ORM\ManyToOne(targetEntity="\TeamSoft\CrmRepositoryBundle\Entity\InfoContact", inversedBy="rtcRoomReserves")
     * @ORM\JoinColumn(name="contact_id", referencedColumnName="id", onDelete="CASCADE")
     */
    private $contact;

    /**
     * @var InfoTask
     *
     * @ORM\OneToOne(targetEntity="\TeamSoft\CrmRepositoryBundle\Entity\InfoTask", inversedBy="rtcRoomReserve")
     * @ORM\JoinColumn(name="task_id", referencedColumnName="id")
     */
    private $task;

    /**
     * @var ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="InfoCallStatistics", mappedBy="rtcRoomReserve")
     */
    private $callStatisticsCollection;

    public function __construct()
    {
        $this->callStatisticsCollection = new ArrayCollection();
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return \DateTime
     */
    public function getDatefrom()
    {
        return $this->datefrom;
    }

    /**
     * @param \DateTime $datefrom
     * @return InfoRtcRoomReserve
     */
    public function setDatefrom(\DateTime $datefrom = null): InfoRtcRoomReserve
    {
        $this->datefrom = $datefrom;
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getDatetill()
    {
        return $this->datetill;
    }

    /**
     * @param \DateTime $datetill
     * @return InfoRtcRoomReserve
     */
    public function setDatetill(\DateTime $datetill = null): InfoRtcRoomReserve
    {
        $this->datetill = $datetill;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getNotes(): ?string
    {
        return $this->notes;
    }

    /**
     * @param string|null $notes
     * @return InfoRtcRoomReserve
     */
    public function setNotes(?string $notes = null): InfoRtcRoomReserve
    {
        $this->notes = $notes;
        return $this;
    }

    /**
     * Set guid
     *
     * @param string $guid
     * @return $this
     */
    public function setGuid($guid)
    {
        $this->guid = $guid;

        return $this;
    }

    /**
     * Get guid
     *
     * @return string
     */
    public function getGuid()
    {
        return $this->guid;
    }

    /**
     * Set currenttime
     *
     * @param \DateTime $currenttime
     * @return $this
     */
    public function setCurrenttime(\DateTime $currenttime = null)
    {
        $this->currenttime = $currenttime;

        return $this;
    }

    /**
     * Get currenttime
     *
     * @return \DateTime
     */
    public function getCurrenttime()
    {
        return $this->currenttime;
    }

    /**
     * Set moduser
     *
     * @param string $moduser
     * @return $this
     */
    public function setModuser($moduser)
    {
        $this->moduser = $moduser;

        return $this;
    }

    /**
     * Get moduser
     *
     * @return string
     */
    public function getModuser()
    {
        return $this->moduser;
    }

    /**
     * @return int|null
     */
    public function getResponsibleId(){
        return $this->rtcRoom ? $this->rtcRoom->getResponsibleId() : null;
    }

    /**
     * @return int|null
     */
    public function getRtcRoomId(){
        return $this->rtcRoom ? $this->rtcRoom->getId() : null;
    }

    /**
     * @return InfoRtcRoom
     */
    public function getRtcRoom()
    {
        return $this->rtcRoom;
    }

    /**
     * @param InfoRtcRoom $rtcRoom
     * @return InfoRtcRoomReserve
     */
    public function setRtcRoom(InfoRtcRoom $rtcRoom): InfoRtcRoomReserve
    {
        $this->rtcRoom = $rtcRoom;
        return $this;
    }

    /**
     * @return int|null
     */
    public function getContactId(){
        return $this->contact ? $this->contact->getId() : null;
    }

    /**
     * @return InfoContact
     */
    public function getContact()
    {
        return $this->contact;
    }

    /**
     * @param InfoContact $contact
     * @return InfoRtcRoomReserve
     */
    public function setContact(?InfoContact $contact): InfoRtcRoomReserve
    {
        $this->contact = $contact;
        return $this;
    }

    /**
     * @return ArrayCollection
     */
    public function getCallStatisticsCollection()
    {
        return $this->callStatisticsCollection;
    }

    /**
     * @param InfoCallStatistics $callStatistics
     * @return $this
     */
    public function addCallStatisticsCollection(InfoCallStatistics $callStatistics){
        if(!$this->callStatisticsCollection->contains($callStatistics)){
            $this->callStatisticsCollection->add($callStatistics);
        }
        $callStatistics->setRtcRoomReserve($this);
        return $this;
    }

    /**
     * @param InfoCallStatistics $callStatistics
     * @return $this
     */
    public function removeCallStatisticsCollection(InfoCallStatistics $callStatistics){
        if($this->callStatisticsCollection->contains($callStatistics)){
            $this->callStatisticsCollection->removeElement($callStatistics);
        }
        $callStatistics->setRtcRoomReserve(null);
        return $this;
    }

    /**
     * @return $this
     */
    public function clearCallStatisticsCollection(){
        $this->callStatisticsCollection->clear();
        return $this;
    }

    /**
     * @return string
     */
    public function getResponsibleName(){
        $name = [];

        if($this->rtcRoom && $this->rtcRoom->getResponsible()){
            $name[] = $this->rtcRoom->getResponsible()->getName();
        }

        if($this->contact){
            $name[] = $this->contact->getName();
        }

        return implode(' - ',$name);
    }

    /**
     * @return int|null
     */
    public function getTaskId(){
        return $this->task ? $this->task->getId() : null;
    }

    /**
     * @return InfoTask
     */
    public function getTask(){
        return $this->task;
    }

    /**
     * @param InfoTask $task
     * @return $this
     */
    public function setTask($task){
        $this->task = $task;
        return $this;
    }

    /**
     * @return \DateTime|null
     */
    public function getTaskDatefrom(){
        return $this->task ? $this->task->getDatefrom() : $this->datefrom;
    }

    /**
     * @return \DateTime|null
     */
    public function getTaskDatetill(){
        return $this->task ? $this->task->getDatetill() : $this->datetill;
    }

    /**
     * @return InfoContact|null
     */
    public function getTaskContact(){
        return $this->task ? $this->task->getContact() : $this->contact;
    }

    /**
     * @return int|null
     */
    public function getTaskContactId(){
        $taskContact = $this->getTaskContact();

        return $taskContact ? $taskContact->getId() : null;
    }
}
