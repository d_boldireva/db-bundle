<?php

namespace TeamSoft\CrmRepositoryBundle\Entity;

use DateTime;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * InfoReportTabFilter
 *
 * @ORM\Table(name="info_reporttabfilter")
 * @ORM\Entity()
 */
class InfoReportTabFilter implements ServiceFieldInterface
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string|null
     *
     * @ORM\Column(name="client_filter_sql", type="string", length=255, nullable=true)
     */
    private $clientFilterSql;

    /**
     * @var string|null
     *
     * @ORM\Column(name="server_filter_sql", type="string", length=255, nullable=true)
     */
    private $serverFilterSql;

    /**
     * @var DateTime|null
     *
     * @ORM\Column(name="currenttime", type="datetime", nullable=true)
     */
    private $currenttime;

    /**
     * @var string|null
     *
     * @ORM\Column(name="moduser", type="string", length=16, nullable=true)
     * )
     */
    private $moduser;

    /**
     * @var string|null
     *
     * @ORM\Column(name="guid", type="guid", nullable=true)
     */
    private $guid;

    /**
     * Many filters have one slice
     * @ORM\ManyToOne(targetEntity="InfoSlice", inversedBy="id")
     * @ORM\JoinColumn(name="slice_id", referencedColumnName="id")
     */
    private $slice;

    /**
     * Many filter have one filter
     * @ORM\ManyToOne(targetEntity="InfoSliceFilter", inversedBy="id")
     * @ORM\JoinColumn(name="filter_id", referencedColumnName="id")
     */
    private $filter;

    /**
     * Many filter have one report
     * @ORM\ManyToOne(targetEntity="InfoReportTab", inversedBy="id")
     * @ORM\JoinColumn(name="reporttab_id", referencedColumnName="id")
     */
    private $reportTab;


    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set reportTab
     *
     * @param InfoReportTab $reportTab
     *
     * @return InfoReportTabFilter
     */
    public function setReportTab($reportTab = null)
    {
        $this->reportTab = $reportTab;

        return $this;
    }

    /**
     * Get reportTab
     *
     * @return InfoReportTab
     */
    public function getReportTab()
    {
        return $this->reportTab;
    }

    /**
     * Set filter
     *
     * @param InfoSliceFilter $filter
     *
     * @return InfoReportTabFilter
     */
    public function setFilter($filter = null)
    {
        $this->filter = $filter;

        return $this;
    }

    /**
     * Get filter
     *
     * @return InfoSliceFilter
     */
    public function getFilter()
    {
        return $this->filter;
    }

    /**
     * Set slice
     *
     * @param InfoSlice $slice
     *
     * @return InfoReportTabFilter
     */
    public function setSlice($slice = null)
    {
        $this->slice = $slice;

        return $this;
    }

    /**
     * Get slice
     *
     * @return InfoSlice
     */
    public function getSlice()
    {
        return $this->slice;
    }

    /**
     * Set clientFilterSql.
     *
     * @param string|null $clientFilterSql
     *
     * @return InfoReportTabFilter
     */
    public function setClientFilterSql($clientFilterSql = null)
    {
        $this->clientFilterSql = $clientFilterSql;

        return $this;
    }

    /**
     * Get clientFilterSql.
     *
     * @return string|null
     */
    public function getClientFilterSql()
    {
        return $this->clientFilterSql;
    }

    /**
     * Set serverFilterSql.
     *
     * @param string|null $serverFilterSql
     *
     * @return InfoReportTabFilter
     */
    public function setServerFilterSql($serverFilterSql = null)
    {
        $this->serverFilterSql = $serverFilterSql;

        return $this;
    }

    /**
     * Get serverFilterSql.
     *
     * @return string|null
     */
    public function getServerFilterSql()
    {
        return $this->serverFilterSql;
    }

    /**
     * Set currenttime.
     *
     * @param DateTime|null $currentTime
     *
     * @return InfoReportTabFilter
     */
    public function setCurrenttime(DateTime $currentTime = null)
    {
        $this->currenttime = $currentTime;

        return $this;
    }

    /**
     * Get currenttime.
     *
     * @return DateTime|null
     */
    public function getCurrenttime()
    {
        return $this->currenttime;
    }

    /**
     * Set moduser.
     *
     * @param string|null $moduser
     *
     * @return InfoReportTabFilter
     */
    public function setModuser($moduser = null)
    {
        $this->moduser = $moduser;

        return $this;
    }

    /**
     * Get moduser.
     *
     * @return string|null
     */
    public function getModuser()
    {
        return $this->moduser;
    }

    /**
     * Set guid.
     *
     * @param string|null $guid
     *
     * @return InfoReportTabFilter
     */
    public function setGuid($guid = null)
    {
        $this->guid = $guid;

        return $this;
    }

    /**
     * Get guid.
     *
     * @return string|null
     */
    public function getGuid()
    {
        return $this->guid;
    }
}
