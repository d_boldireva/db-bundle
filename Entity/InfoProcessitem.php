<?php

namespace TeamSoft\CrmRepositoryBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * InfoProcessitem
 *
 * @ORM\Table(name="info_processitem", indexes={@ORM\Index(name="ix_info_processitem_guid", columns={"guid"}), @ORM\Index(name="ix_info_processitem_time", columns={"time"}), @ORM\Index(name="ix_info_processitem_process_id", columns={"process_id"}), @ORM\Index(name="ix_info_processitem_tasktype_id", columns={"tasktype_id"})})
 * @ORM\Entity
 */
class InfoProcessitem
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255, nullable=true)
     */
    private $name;

    /**
     * @var integer
     *
     * @ORM\Column(name="duration", type="integer", nullable=true)
     */
    private $duration;

    /**
     * @var integer
     *
     * @ORM\Column(name="x", type="integer", nullable=true)
     */
    private $x;

    /**
     * @var integer
     *
     * @ORM\Column(name="y", type="integer", nullable=true)
     */
    private $y;

    /**
     * @var integer
     *
     * @ORM\Column(name="w", type="integer", nullable=true)
     */
    private $w;

    /**
     * @var integer
     *
     * @ORM\Column(name="h", type="integer", nullable=true)
     */
    private $h;

    /**
     * @var integer
     *
     * @ORM\Column(name="isnew", type="integer", nullable=true)
     */
    private $isnew = '1';

    /**
     * @var guid
     *
     * @ORM\Column(name="guid", type="guid", nullable=true)
     */
    private $guid;

    /**
     * @var integer
     *
     * @ORM\Column(name="isfirst", type="integer", nullable=true)
     */
    private $isfirst = '0';

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="currenttime", type="datetime", nullable=true)
     */
    private $currenttime;

    /**
     * @var string
     *
     * @ORM\Column(name="moduser", type="string", length=16, nullable=true)
     */
    private $moduser;

    /**
     * @var \InfoProcess
     *
     * @ORM\ManyToOne(targetEntity="InfoProcess")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="process_id", referencedColumnName="id")
     * })
     */
    private $process;

    /**
     * @var \InfoTasktype
     *
     * @ORM\ManyToOne(targetEntity="InfoTasktype")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="tasktype_id", referencedColumnName="id")
     * })
     */
    private $tasktype;



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return InfoProcessitem
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set duration
     *
     * @param integer $duration
     * @return InfoProcessitem
     */
    public function setDuration($duration)
    {
        $this->duration = $duration;

        return $this;
    }

    /**
     * Get duration
     *
     * @return integer 
     */
    public function getDuration()
    {
        return $this->duration;
    }

    /**
     * Set x
     *
     * @param integer $x
     * @return InfoProcessitem
     */
    public function setX($x)
    {
        $this->x = $x;

        return $this;
    }

    /**
     * Get x
     *
     * @return integer 
     */
    public function getX()
    {
        return $this->x;
    }

    /**
     * Set y
     *
     * @param integer $y
     * @return InfoProcessitem
     */
    public function setY($y)
    {
        $this->y = $y;

        return $this;
    }

    /**
     * Get y
     *
     * @return integer 
     */
    public function getY()
    {
        return $this->y;
    }

    /**
     * Set w
     *
     * @param integer $w
     * @return InfoProcessitem
     */
    public function setW($w)
    {
        $this->w = $w;

        return $this;
    }

    /**
     * Get w
     *
     * @return integer 
     */
    public function getW()
    {
        return $this->w;
    }

    /**
     * Set h
     *
     * @param integer $h
     * @return InfoProcessitem
     */
    public function setH($h)
    {
        $this->h = $h;

        return $this;
    }

    /**
     * Get h
     *
     * @return integer 
     */
    public function getH()
    {
        return $this->h;
    }

    /**
     * Set isnew
     *
     * @param integer $isnew
     * @return InfoProcessitem
     */
    public function setIsnew($isnew)
    {
        $this->isnew = $isnew;

        return $this;
    }

    /**
     * Get isnew
     *
     * @return integer 
     */
    public function getIsnew()
    {
        return $this->isnew;
    }

    /**
     * Set guid
     *
     * @param guid $guid
     * @return InfoProcessitem
     */
    public function setGuid($guid)
    {
        $this->guid = $guid;

        return $this;
    }

    /**
     * Get guid
     *
     * @return guid 
     */
    public function getGuid()
    {
        return $this->guid;
    }

    /**
     * Set isfirst
     *
     * @param integer $isfirst
     * @return InfoProcessitem
     */
    public function setIsfirst($isfirst)
    {
        $this->isfirst = $isfirst;

        return $this;
    }

    /**
     * Get isfirst
     *
     * @return integer 
     */
    public function getIsfirst()
    {
        return $this->isfirst;
    }

    /**
     * Set currenttime
     *
     * @param \DateTime $currenttime
     * @return InfoProcessitem
     */
    public function setCurrenttime($currenttime)
    {
        $this->currenttime = $currenttime;

        return $this;
    }

    /**
     * Get currenttime
     *
     * @return \DateTime 
     */
    public function getCurrenttime()
    {
        return $this->currenttime;
    }

    /**
     * Set moduser
     *
     * @param string $moduser
     * @return InfoProcessitem
     */
    public function setModuser($moduser)
    {
        $this->moduser = $moduser;

        return $this;
    }

    /**
     * Get moduser
     *
     * @return string 
     */
    public function getModuser()
    {
        return $this->moduser;
    }

    /**
     * Set process
     *
     * @param \TeamSoft\CrmRepositoryBundle\Entity\InfoProcess $process
     * @return InfoProcessitem
     */
    public function setProcess(\TeamSoft\CrmRepositoryBundle\Entity\InfoProcess $process = null)
    {
        $this->process = $process;

        return $this;
    }

    /**
     * Get process
     *
     * @return \TeamSoft\CrmRepositoryBundle\Entity\InfoProcess 
     */
    public function getProcess()
    {
        return $this->process;
    }

    /**
     * Set tasktype
     *
     * @param \TeamSoft\CrmRepositoryBundle\Entity\InfoTasktype $tasktype
     * @return InfoProcessitem
     */
    public function setTasktype(\TeamSoft\CrmRepositoryBundle\Entity\InfoTasktype $tasktype = null)
    {
        $this->tasktype = $tasktype;

        return $this;
    }

    /**
     * Get tasktype
     *
     * @return \TeamSoft\CrmRepositoryBundle\Entity\InfoTasktype 
     */
    public function getTasktype()
    {
        return $this->tasktype;
    }
}
