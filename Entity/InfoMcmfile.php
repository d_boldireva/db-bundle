<?php

namespace TeamSoft\CrmRepositoryBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use TeamSoft\CrmRepositoryBundle\Model\InfoFile;

/**
 * InfoMcmfile
 *
 * @ORM\Table(name="info_mcmfile")
 * @ORM\Entity(repositoryClass="TeamSoft\CrmRepositoryBundle\Repository\MCM\InfoMcmfileRepository")
 */
class InfoMcmfile extends InfoFile implements ServiceFieldInterface
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string|null
     *
     * @ORM\Column(name="name", type="string", length=255, nullable=true)
     */
    private $name;

    /**
     * @var string|null
     *
     * @ORM\Column(name="content", type="blob_or_string", nullable=true)
     */
    private $content;

    /**
     * @var int
     *
     * @ORM\Column(type="integer", nullable=true)
     */
    private $width;

    /**
     * @var int
     *
     * @ORM\Column(type="integer", nullable=true)
     */
    private $height;

    /**
     * @var int|null
     *
     * @ORM\Column(name="fsize", type="integer", nullable=true)
     */
    private $fsize;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="fmodified", type="datetime", nullable=true)
     */
    private $fmodified;

    /**
     * @var string|null
     *
     * @ORM\Column(name="guid", type="guid", nullable=true)
     */
    private $guid;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="currenttime", type="datetime", nullable=true)
     */
    private $currenttime;

    /**
     * @var string|null
     *
     * @ORM\Column(name="moduser", type="string", length=16, nullable=true)
     */
    private $moduser;

    /**
     * @var ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="InfoMcmcertificatevariable", mappedBy="certificate", cascade={"persist", "remove"})
     */
    private $variableCollection;

    public function __construct()
    {
        $this->variableCollection = new ArrayCollection();
    }

    /**
     * Get id.
     *
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * Set name.
     *
     * @param string|null $name
     *
     * @return InfoMcmfile
     */
    public function setName($name = null): InfoMcmfile
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name.
     *
     * @return string|null
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set content.
     *
     * @param string|null $content
     *
     * @return InfoMcmfile
     */
    public function setContent($content = null)
    {
        $this->content = $content;

        return $this;
    }

    /**
     * Get content.
     *
     * @return string|null
     */
    public function getContent()
    {
        return $this->content;
    }

    /**
     * Set fsize.
     *
     * @param int|null $fsize
     *
     * @return InfoMcmfile
     */
    public function setFsize($fsize = null)
    {
        $this->fsize = $fsize;

        return $this;
    }

    /**
     * Get fsize.
     *
     * @return int|null
     */
    public function getFsize()
    {
        return $this->fsize;
    }

    /**
     * Set fmodified.
     *
     * @param \DateTime|null $fmodified
     *
     * @return InfoMcmfile
     */
    public function setFmodified($fmodified = null)
    {
        $this->fmodified = $fmodified;

        return $this;
    }

    /**
     * Get fmodified.
     *
     * @return \DateTime|null
     */
    public function getFmodified()
    {
        return $this->fmodified;
    }

    /**
     * Set guid.
     *
     * @param string|null $guid
     *
     * @return InfoMcmfile
     */
    public function setGuid($guid = null)
    {
        $this->guid = $guid;

        return $this;
    }

    /**
     * Get guid.
     *
     * @return string|null
     */
    public function getGuid()
    {
        return $this->guid;
    }

    /**
     * Set currenttime.
     *
     * @param \DateTime|null $currenttime
     *
     * @return InfoMcmfile
     */
    public function setCurrenttime(\DateTime $currenttime = null)
    {
        $this->currenttime = $currenttime;

        return $this;
    }

    /**
     * Get currenttime.
     *
     * @return \DateTime|null
     */
    public function getCurrenttime()
    {
        return $this->currenttime;
    }

    /**
     * Set moduser.
     *
     * @param string|null $moduser
     *
     * @return InfoMcmfile
     */
    public function setModuser($moduser = null)
    {
        $this->moduser = $moduser;

        return $this;
    }

    /**
     * Get moduser.
     *
     * @return string|null
     */
    public function getModuser()
    {
        return $this->moduser;
    }

    /**
     * @return int
     */
    public function getWidth(): int
    {
        return $this->width;
    }

    /**
     * @param int $width
     */
    public function setWidth(int $width): void
    {
        $this->width = $width;
    }

    /**
     * @return int
     */
    public function getHeight(): int
    {
        return $this->height;
    }

    /**
     * @param int $height
     */
    public function setHeight(int $height): void
    {
        $this->height = $height;
    }

    /**
     * Add variable
     *
     * @param InfoMcmcertificatevariable $mcmCertificateVariable
     * @return InfoMcmfile
     */
    public function addVariableCollection(InfoMcmcertificatevariable $mcmCertificateVariable): InfoMcmfile
    {
        $mcmCertificateVariable->setCertificate($this);

        if (!$this->variableCollection->contains($mcmCertificateVariable)) {
            $this->variableCollection->add($mcmCertificateVariable);
        }

        return $this;
    }

    /**
     * Remove variable
     *
     * @param InfoMcmcertificatevariable $mcmCertificateVariable
     */
    public function removeVariableCollection(InfoMcmcertificatevariable $mcmCertificateVariable)
    {
        if ($this->variableCollection->contains($mcmCertificateVariable)) {
            $this->variableCollection->removeElement($mcmCertificateVariable);
        }

        $mcmCertificateVariable->setCertificate(null);
    }

    /**
     * Get variables
     *
     * @return Collection
     */
    public function getVariableCollection()
    {
        return $this->variableCollection;
    }
}
