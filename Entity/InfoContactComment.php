<?php

namespace TeamSoft\CrmRepositoryBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * InfoContactComment
 *
 * @ORM\Table(name="info_contactcomment")
 * @ORM\Entity
 */
class InfoContactComment implements ServiceFieldInterface
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string|null
     *
     * @ORM\Column(name="comment_large", type="string", nullable=true)
     */
    private $commentLarge;

    /**
     * @var InfoContact
     *
     * @ORM\OneToOne (targetEntity="InfoContact", inversedBy="contactComment")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="contact_id", referencedColumnName="id")
     * })
     */
    private $contact;


    /**
     * @var string
     *
     * @ORM\Column(name="guid", type="guid", nullable=true)
     */
    private $guid;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="currenttime", type="datetime", nullable=true)
     */
    private $currenttime;

    /**
     * @var string
     *
     * @ORM\Column(name="moduser", type="string", length=16, nullable=true)
     */
    private $moduser;

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return string|null
     */
    public function getCommentLarge(): ?string
    {
        return $this->commentLarge;
    }

    /**
     * @param string|null $commentLarge
     * @return self
     */
    public function setCommentLarge(?string $commentLarge): self
    {
        $this->commentLarge = $commentLarge;
        return $this;
    }

    /**
     * @return InfoContact|null
     */
    public function getContact(): ?InfoContact
    {
        return $this->contact;
    }

    /**
     * @param InfoContact|null $contact
     * @return self
     */
    public function setContact(?InfoContact $contact): self
    {
        $this->contact = $contact;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getGuid(): ?string
    {
        return $this->guid;
    }

    /**
     * @param string $guid
     * @return self
     */
    public function setGuid($guid): self
    {
        $this->guid = $guid;
        return $this;
    }

    /**
     * @return \DateTime|null
     */
    public function getCurrenttime(): ?\DateTime
    {
        return $this->currenttime;
    }

    /**
     * @param \DateTime $currenttime
     * @return self
     */
    public function setCurrenttime(\DateTime $currenttime = null): self
    {
        $this->currenttime = $currenttime;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getModuser(): ?string
    {
        return $this->moduser;
    }

    /**
     * @param string $moduser
     * @return self
     */
    public function setModuser($moduser): self
    {
        $this->moduser = $moduser;
        return $this;
    }
}
