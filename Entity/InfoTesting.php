<?php

namespace TeamSoft\CrmRepositoryBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * InfoTesting
 *
 * @ORM\Table(name="info_testing")
 * @ORM\Entity
 */
class InfoTesting implements ServiceFieldInterface
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=50, nullable=true)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="string", length=255, nullable=true)
     */
    private $description;

    /**
     * @var string
     *
     * @ORM\Column(name="startpage", type="string", length=255, nullable=true)
     */
    private $startpage;

    /**
     * @var string
     *
     * @ORM\Column(name="finishpage", type="string", length=255, nullable=true)
     */
    private $finishpage;

    /**
     * @var string
     *
     * @ORM\Column(name="Content", type="blob", nullable=true)
     */
    private $content;

    /**
     * @var guid
     *
     * @ORM\Column(name="guid", type="guid", nullable=true)
     */
    private $guid;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="currenttime", type="datetime", nullable=true)
     */
    private $currenttime;

    /**
     * @var string
     *
     * @ORM\Column(name="moduser", type="string", length=16, nullable=true)
     */
    private $moduser;



    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return InfoTesting
     */
    public function setName($name)
    {
        $this->name = $name;
    
        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set description
     *
     * @param string $description
     *
     * @return InfoTesting
     */
    public function setDescription($description)
    {
        $this->description = $description;
    
        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set startpage
     *
     * @param string $startpage
     *
     * @return InfoTesting
     */
    public function setStartpage($startpage)
    {
        $this->startpage = $startpage;
    
        return $this;
    }

    /**
     * Get startpage
     *
     * @return string
     */
    public function getStartpage()
    {
        return $this->startpage;
    }

    /**
     * Set finishpage
     *
     * @param string $finishpage
     *
     * @return InfoTesting
     */
    public function setFinishpage($finishpage)
    {
        $this->finishpage = $finishpage;
    
        return $this;
    }

    /**
     * Get finishpage
     *
     * @return string
     */
    public function getFinishpage()
    {
        return $this->finishpage;
    }

    /**
     * Set content
     *
     * @param string $content
     *
     * @return InfoTesting
     */
    public function setContent($content)
    {
        $this->content = $content;
    
        return $this;
    }

    /**
     * Get content
     *
     * @return string
     */
    public function getContent()
    {
        return $this->content;
    }

    /**
     * Set guid
     *
     * @param guid $guid
     *
     * @return InfoTesting
     */
    public function setGuid($guid)
    {
        $this->guid = $guid;
    
        return $this;
    }

    /**
     * Get guid
     *
     * @return guid
     */
    public function getGuid()
    {
        return $this->guid;
    }

    /**
     * Set currenttime
     *
     * @param \DateTime $currenttime
     *
     * @return InfoTesting
     */
    public function setCurrenttime(\DateTime $currenttime)
    {
        $this->currenttime = $currenttime;
    
        return $this;
    }

    /**
     * Get currenttime
     *
     * @return \DateTime
     */
    public function getCurrenttime()
    {
        return $this->currenttime;
    }

    /**
     * Set moduser
     *
     * @param string $moduser
     *
     * @return InfoTesting
     */
    public function setModuser($moduser)
    {
        $this->moduser = $moduser;
    
        return $this;
    }

    /**
     * Get moduser
     *
     * @return string
     */
    public function getModuser()
    {
        return $this->moduser;
    }
}
