<?php

namespace TeamSoft\CrmRepositoryBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * InfoDictionaryidentifier
 *
 * @ORM\Table(name="info_dictionaryidentifier")
 * @ORM\Entity()
 */
class InfoDictionaryidentifier
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="tablename", type="string", length=255, nullable=false)
     */
    private $tablename;

    /**
     * @var string
     *
     * @ORM\Column(name="columnname", type="string", length=255, nullable=false)
     */
    private $columnname;

    /**
     * @var string
     *
     * @ORM\Column(name="value", type="string", length=255, nullable=false)
     */
    private $value;

    /**
     * @var string
     *
     * @ORM\Column(name="values_list", type="json", nullable=true)
     */
    private $valuesList;

    /**
     * @var string
     *
     * @ORM\Column(name="language_code", type="string", length=2, nullable=false)
     */
    private $languageCode;

    /**
     * @var integer
     *
     * @ORM\Column(name="identifier", type="integer", nullable=true)
     */
    private $identifier;

    /**
     * @var integer
     *
     * @ORM\Column(name="order_num", type="integer", nullable=true)
     */
    private $orderNum;

    /**
     * @var integer
     *
     * @ORM\Column(name="hidden", type="integer", nullable=true)
     */
    private $hidden;

    /**
     * @var string
     *
     * @ORM\Column(name="guid", type="guid", nullable=true)
     */
    private $guid;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="currenttime", type="datetime", nullable=true)
     */
    private $currenttime;

    /**
     * @var string
     *
     * @ORM\Column(name="moduser", type="string", length=16, nullable=true)
     */
    private $moduser;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set tablename
     *
     * @param string $tablename
     * @return InfoDictionaryidentifier
     */
    public function setTablename($tablename)
    {
        $this->tablename = $tablename;

        return $this;
    }

    /**
     * Get tablename
     *
     * @return string
     */
    public function getTablename()
    {
        return $this->tablename;
    }

    /**
     * Set columnname
     *
     * @param string $columnname
     * @return InfoDictionaryidentifier
     */
    public function setColumnname($columnname)
    {
        $this->columnname = $columnname;

        return $this;
    }

    /**
     * Get columnname
     *
     * @return string
     */
    public function getColumnname()
    {
        return $this->columnname;
    }

    /**
     * Set value
     *
     * @param string $value
     * @return InfoDictionaryidentifier
     */
    public function setValue($value)
    {
        $this->value = $value;

        return $this;
    }

    /**
     * Get value
     *
     * @return string
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     * @return string|null
     */
    public function getValuesList(): ?string
    {
        return $this->valuesList;
    }

    /**
     * @param  string|null  $valuesList
     *
     * @return $this
     */
    public function setValuesList(?string $valuesList): self
    {
        $this->valuesList = $valuesList;

        return $this;
    }

    /**
     * Set languageCode
     *
     * @param string $languageCode
     * @return InfoDictionaryidentifier
     */
    public function setLanguageCode($languageCode)
    {
        $this->languageCode = $languageCode;

        return $this;
    }

    /**
     * Get languageCode
     *
     * @return string
     */
    public function getLanguageCode()
    {
        return $this->languageCode;
    }

    /**
     * Set identifier
     *
     * @param integer $identifier
     * @return InfoDictionaryidentifier
     */
    public function setIdentifier($identifier)
    {
        $this->identifier = $identifier;

        return $this;
    }

    /**
     * Get identifier
     *
     * @return integer
     */
    public function getIdentifier()
    {
        return $this->identifier;
    }

    /**
     * Set orderNum
     *
     * @param integer $orderNum
     * @return InfoDictionaryidentifier
     */
    public function setOrderNum($orderNum)
    {
        $this->orderNum = $orderNum;

        return $this;
    }

    /**
     * Get orderNum
     *
     * @return integer
     */
    public function getOrderNum()
    {
        return $this->orderNum;
    }

    /**
     * Set hidden
     *
     * @param integer $hidden
     * @return InfoDictionaryidentifier
     */
    public function setHidden($hidden)
    {
        $this->hidden = $hidden;

        return $this;
    }

    /**
     * Get hidden
     *
     * @return integer
     */
    public function getHidden()
    {
        return $this->hidden;
    }

    /**
     * Set guid
     *
     * @param string $guid
     * @return InfoDictionaryidentifier
     */
    public function setGuid($guid)
    {
        $this->guid = $guid;

        return $this;
    }

    /**
     * Get guid
     *
     * @return string
     */
    public function getGuid()
    {
        return $this->guid;
    }

    /**
     * Set currenttime
     *
     * @param \DateTime $currenttime
     * @return InfoDictionaryidentifier
     */
    public function setCurrenttime($currenttime)
    {
        $this->currenttime = $currenttime;

        return $this;
    }

    /**
     * Get currenttime
     *
     * @return \DateTime
     */
    public function getCurrenttime()
    {
        return $this->currenttime;
    }

    /**
     * Set moduser
     *
     * @param string $moduser
     * @return InfoDictionaryidentifier
     */
    public function setModuser($moduser)
    {
        $this->moduser = $moduser;

        return $this;
    }

    /**
     * Get moduser
     *
     * @return string
     */
    public function getModuser()
    {
        return $this->moduser;
    }
}
