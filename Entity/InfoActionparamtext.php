<?php

namespace TeamSoft\CrmRepositoryBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * InfoParamfortask
 *
 * @ORM\Table(name="info_actionparamtext")
 * @ORM\Entity
 */
class InfoActionparamtext implements ServiceFieldInterface
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var InfoAction
     *
     * @ORM\ManyToOne(targetEntity="InfoAction")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="action_id", referencedColumnName="id")
     * })
     */
    private $action;

    /**
     * @var InfoParamforaction
     *
     * @ORM\ManyToOne(targetEntity="InfoParamforaction")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="param_id", referencedColumnName="id")
     * })
     */
    private $param;

    /**
     * @var string|null
     *
     * @ORM\Column(name="text", type="string", length=255, nullable=true)
     */
    private $text;

    /**
     * @var string|null
     *
     * @ORM\Column(name="guid", type="guid", nullable=true)
     */
    private $guid;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="currenttime", type="datetime", nullable=true)
     */
    private $currenttime;

    /**
     * @var string|null
     *
     * @ORM\Column(name="moduser", type="string", length=16, nullable=true)
     */
    private $moduser;

    /**
     * @var InfoUser
     *
     * @ORM\ManyToOne(targetEntity="InfoUser")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="responsible_id", referencedColumnName="id")
     * })
     */
    private $responsible;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="dt", type="datetime", nullable=true)
     */
    private $dt;

    /**
     * @var InfoCustomdictionaryvalue
     *
     * @ORM\ManyToOne(targetEntity="InfoCustomdictionaryvalue")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="status_id", referencedColumnName="id")
     * })
     */
    private $status;

    /**
     * @var string|null
     *
     * @ORM\Column(name="comment", type="string", length=255, nullable=true)
     */
    private $comment;

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set text.
     *
     * @param string|null $text
     *
     * @return InfoActionparamtext
     */
    public function setText($text = null)
    {
        $this->text = $text;

        return $this;
    }

    /**
     * Get text.
     *
     * @return string|null
     */
    public function getText()
    {
        return $this->text;
    }

    /**
     * Set guid.
     *
     * @param string|null $guid
     *
     * @return InfoActionparamtext
     */
    public function setGuid($guid = null)
    {
        $this->guid = $guid;

        return $this;
    }

    /**
     * Get guid.
     *
     * @return string|null
     */
    public function getGuid()
    {
        return $this->guid;
    }

    /**
     * Set currenttime.
     *
     * @param \DateTime $currenttime
     *
     * @return InfoActionparamtext
     */
    public function setCurrenttime(\DateTime $currenttime)
    {
        $this->currenttime = $currenttime;

        return $this;
    }

    /**
     * Get currenttime.
     *
     * @return \DateTime|null
     */
    public function getCurrenttime()
    {
        return $this->currenttime;
    }

    /**
     * Set moduser.
     *
     * @param string|null $moduser
     *
     * @return InfoActionparamtext
     */
    public function setModuser($moduser = null)
    {
        $this->moduser = $moduser;

        return $this;
    }

    /**
     * Get moduser.
     *
     * @return string|null
     */
    public function getModuser()
    {
        return $this->moduser;
    }

    /**
     * Set dt.
     *
     * @param \DateTime|null $dt
     *
     * @return InfoActionparamtext
     */
    public function setDt($dt = null)
    {
        $this->dt = $dt;

        return $this;
    }

    /**
     * Get dt.
     *
     * @return \DateTime|null
     */
    public function getDt()
    {
        return $this->dt;
    }

    /**
     * Set comment.
     *
     * @param string|null $comment
     *
     * @return InfoActionparamtext
     */
    public function setComment($comment = null)
    {
        $this->comment = $comment;

        return $this;
    }

    /**
     * Get comment.
     *
     * @return string|null
     */
    public function getComment()
    {
        return $this->comment;
    }

    /**
     * Set action.
     *
     * @param \TeamSoft\CrmRepositoryBundle\Entity\InfoAction|null $action
     *
     * @return InfoActionparamtext
     */
    public function setAction(\TeamSoft\CrmRepositoryBundle\Entity\InfoAction $action = null)
    {
        $this->action = $action;

        return $this;
    }

    /**
     * Get action.
     *
     * @return \TeamSoft\CrmRepositoryBundle\Entity\InfoAction|null
     */
    public function getAction()
    {
        return $this->action;
    }

    /**
     * Set param.
     *
     * @param \TeamSoft\CrmRepositoryBundle\Entity\InfoParamforaction|null $param
     *
     * @return InfoActionparamtext
     */
    public function setParam(\TeamSoft\CrmRepositoryBundle\Entity\InfoParamforaction $param = null)
    {
        $this->param = $param;

        return $this;
    }

    /**
     * Get param.
     *
     * @return \TeamSoft\CrmRepositoryBundle\Entity\InfoParamforaction|null
     */
    public function getParam()
    {
        return $this->param;
    }

    /**
     * Set responsible.
     *
     * @param \TeamSoft\CrmRepositoryBundle\Entity\InfoUser|null $responsible
     *
     * @return InfoActionparamtext
     */
    public function setResponsible(\TeamSoft\CrmRepositoryBundle\Entity\InfoUser $responsible = null)
    {
        $this->responsible = $responsible;

        return $this;
    }

    /**
     * Get responsible.
     *
     * @return \TeamSoft\CrmRepositoryBundle\Entity\InfoUser|null
     */
    public function getResponsible()
    {
        return $this->responsible;
    }

    /**
     * Set status.
     *
     * @param \TeamSoft\CrmRepositoryBundle\Entity\InfoCustomdictionaryvalue|null $status
     *
     * @return InfoActionparamtext
     */
    public function setStatus(\TeamSoft\CrmRepositoryBundle\Entity\InfoCustomdictionaryvalue $status = null)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status.
     *
     * @return \TeamSoft\CrmRepositoryBundle\Entity\InfoCustomdictionaryvalue|null
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @return int|null
     */
    public function getParamId()
    {
        return $this->param ? $this->param->getId() : null;
    }

    /**
     * @return int|null
     */
    public function getStatusId()
    {
        return $this->status ? $this->status->getId() : null;
    }

    /**
     * @return int|null
     */
    public function getResponsibleId()
    {
        return $this->responsible ? $this->responsible->getId() : null;
    }
}
