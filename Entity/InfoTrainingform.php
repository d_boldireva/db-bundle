<?php

namespace TeamSoft\CrmRepositoryBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * InfoTrainingform
 *
 * @ORM\Table(name="info_trainingform")
 * @ORM\Entity
 */
class InfoTrainingform implements ServiceFieldInterface
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var int|null
     *
     * @ORM\Column(name="mark", type="integer", nullable=true)
     */
    private $mark;

    /**
     * @var string|null
     *
     * @ORM\Column(name="comment", type="text", length=-1, nullable=true)
     */
    private $comment;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="currenttime", type="datetime", nullable=true)
     */
    private $currenttime;

    /**
     * @var string|null
     *
     * @ORM\Column(name="moduser", type="string", length=16, nullable=true)
     */
    private $moduser;

    /**
     * @var string|null
     *
     * @ORM\Column(name="guid", type="guid", nullable=true)
     */
    private $guid;

    /**
     * @var \InfoTasktype
     *
     * @ORM\ManyToOne(targetEntity="InfoTasktype")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="tasktype_id", referencedColumnName="id")
     * })
     */
    private $tasktype;

    /**
     * @var \InfoParamfortask
     *
     * @ORM\ManyToOne(targetEntity="InfoParamfortask")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="paramfortask_id", referencedColumnName="id")
     * })
     */
    private $paramfortask;

    /**
     * @var \InfoTask
     *
     * @ORM\ManyToOne(targetEntity="InfoTask")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="task_id", referencedColumnName="id")
     * })
     */
    private $task;

    /**
     * @var \InfoUser
     *
     * @ORM\ManyToOne(targetEntity="InfoUser")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     * })
     */
    private $user;



    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set mark.
     *
     * @param int|null $mark
     *
     * @return InfoTrainingform
     */
    public function setMark($mark = null)
    {
        $this->mark = $mark;

        return $this;
    }

    /**
     * Get mark.
     *
     * @return int|null
     */
    public function getMark()
    {
        return $this->mark;
    }

    /**
     * Set comment.
     *
     * @param string|null $comment
     *
     * @return InfoTrainingform
     */
    public function setComment($comment = null)
    {
        $this->comment = $comment;

        return $this;
    }

    /**
     * Get comment.
     *
     * @return string|null
     */
    public function getComment()
    {
        return $this->comment;
    }

    /**
     * Set currenttime.
     *
     * @param \DateTime|null $currenttime
     *
     * @return InfoTrainingform
     */
    public function setCurrenttime(\DateTime $currenttime = null)
    {
        $this->currenttime = $currenttime;

        return $this;
    }

    /**
     * Get currenttime.
     *
     * @return \DateTime|null
     */
    public function getCurrenttime()
    {
        return $this->currenttime;
    }

    /**
     * Set moduser.
     *
     * @param string|null $moduser
     *
     * @return InfoTrainingform
     */
    public function setModuser($moduser = null)
    {
        $this->moduser = $moduser;

        return $this;
    }

    /**
     * Get moduser.
     *
     * @return string|null
     */
    public function getModuser()
    {
        return $this->moduser;
    }

    /**
     * Set guid.
     *
     * @param string|null $guid
     *
     * @return InfoTrainingform
     */
    public function setGuid($guid = null)
    {
        $this->guid = $guid;

        return $this;
    }

    /**
     * Get guid.
     *
     * @return string|null
     */
    public function getGuid()
    {
        return $this->guid;
    }

    /**
     * Set tasktype.
     *
     * @param \TeamSoft\CrmRepositoryBundle\Entity\InfoTasktype|null $tasktype
     *
     * @return InfoTrainingform
     */
    public function setTasktype(\TeamSoft\CrmRepositoryBundle\Entity\InfoTasktype $tasktype = null)
    {
        $this->tasktype = $tasktype;

        return $this;
    }

    /**
     * Get tasktype.
     *
     * @return \TeamSoft\CrmRepositoryBundle\Entity\InfoTasktype|null
     */
    public function getTasktype()
    {
        return $this->tasktype;
    }

    /**
     * Set paramfortask.
     *
     * @param \TeamSoft\CrmRepositoryBundle\Entity\InfoParamfortask|null $paramfortask
     *
     * @return InfoTrainingform
     */
    public function setParamfortask(\TeamSoft\CrmRepositoryBundle\Entity\InfoParamfortask $paramfortask = null)
    {
        $this->paramfortask = $paramfortask;

        return $this;
    }

    /**
     * Get paramfortask.
     *
     * @return \TeamSoft\CrmRepositoryBundle\Entity\InfoParamfortask|null
     */
    public function getParamfortask()
    {
        return $this->paramfortask;
    }

    /**
     * Set task.
     *
     * @param \TeamSoft\CrmRepositoryBundle\Entity\InfoTask|null $task
     *
     * @return InfoTrainingform
     */
    public function setTask(\TeamSoft\CrmRepositoryBundle\Entity\InfoTask $task = null)
    {
        $this->task = $task;

        return $this;
    }

    /**
     * Get task.
     *
     * @return \TeamSoft\CrmRepositoryBundle\Entity\InfoTask|null
     */
    public function getTask()
    {
        return $this->task;
    }

    /**
     * Set user.
     *
     * @param \TeamSoft\CrmRepositoryBundle\Entity\InfoUser|null $user
     *
     * @return InfoTrainingform
     */
    public function setUser(\TeamSoft\CrmRepositoryBundle\Entity\InfoUser $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user.
     *
     * @return \TeamSoft\CrmRepositoryBundle\Entity\InfoUser|null
     */
    public function getUser()
    {
        return $this->user;
    }

    public function getTaskTypeId()
    {
        return $this->getTasktype() ? $this->getTasktype()->getId() : null;
    }

    public function getUserId()
    {
        return $this->getUser() ? $this->getUser()->getId() : null;
    }

    public function getParamForTaskId()
    {
        return $this->getParamfortask() ? $this->getParamfortask()->getId() : null;
    }

    public function getTaskId()
    {
        return $this->getTask() ? $this->getTask()->getId() : null;
    }
}
