<?php

namespace TeamSoft\CrmRepositoryBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * InfoDcrtype
 *
 * @ORM\Table(name="info_dcrtype")
 * @ORM\Entity()
 */
class InfoDcrtype
{
    const APPLY_IN_SAME_DAY = 'same_day';
    const APPLY_IN_NEXT_CYCLE = 'next_cycle';
    const APPLY_IN_NEXT_CYCLE_ONCE_PER_QUARTER = 'next_cycle_once_per_quarter';

    const NEW_SHOP = "new_shop"; //  Добавление новой аптеки
    const UPDATE_SHOP_EXCEPT_CATEGORY = "update_shop_except_category_and_archive"; //  Любое изменение в карточке аптеки (кроме категории)
    const UPDATE_SHOP_CATEGORY = "update_shop_category"; //  Изменение категории аптеки
    const ADD_SHOP_TO_ARCHIVE = "add_shop_to_archive"; //  Добавление аптеки в архив
    const NEW_AGENCY = "new_agency"; //  Добавление нового учреждения, не аптеки
    const UPDATE_AGENCY = "update_agency"; // Любое изменение в карточке учреждения, не аптеки
    const NEW_SHOP_CONTACT = "new_shop_contact"; //  Добавление сотрудника аптеки
    const NEW_AGENCY_CONTACT = "new_agency_contact"; //  Добавление нового врача
    const UPDATE_CONTACT_EXCEPT_CATEGORY = "update_contact_except_category_and_archive"; //  Любое изменение в карточке врача (кроме категории)
    const UPDATE_CONTACT_CATEGORY = "update_contact_category"; //  Изменение категории врача
    const ADD_CONTACT_TO_ARCHIVE = "add_contact_to_archive"; //  Добавление врача в архив

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255, nullable=true)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="code", type="string", length=255, nullable=true)
     */
    private $code;

    /**
     * @var string
     *
     * @ORM\Column(name="guid", type="guid", nullable=true)
     */
    private $guid;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="currenttime", type="datetime", nullable=true)
     */
    private $currenttime;

    /**
     * @var string
     *
     * @ORM\Column(name="moduser", type="string", length=16, nullable=true)
     */
    private $moduser;

    /**
     * @var InfoCustomdictionaryvalue
     *
     * @ORM\ManyToOne(targetEntity="InfoCustomdictionaryvalue")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="applydate_id", referencedColumnName="id")
     * })
     */
    private $applyDate;

    /**
     * @ORM\ManyToMany(targetEntity="InfoRole")
     * @ORM\JoinTable(name="info_dcrtyperoleworkflow",
     *   joinColumns={@ORM\JoinColumn(name="dcrtype_id", referencedColumnName="id")},
     *   inverseJoinColumns={@ORM\JoinColumn(name="role_id", referencedColumnName="id")}
     * )
     * @ORM\OrderBy({"rang" = "DESC"})
     */
    private $roles;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->roles = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return InfoDcrtype
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set code
     *
     * @param string $code
     * @return InfoDcrtype
     */
    public function setCode($code)
    {
        $this->code = $code;

        return $this;
    }

    /**
     * Get code
     *
     * @return string
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * Set guid
     *
     * @param string $guid
     * @return InfoDcrtype
     */
    public function setGuid($guid)
    {
        $this->guid = $guid;

        return $this;
    }

    /**
     * Get guid
     *
     * @return string
     */
    public function getGuid()
    {
        return $this->guid;
    }

    /**
     * Set currenttime
     *
     * @param \DateTime $currenttime
     * @return InfoDcrtype
     */
    public function setCurrenttime($currenttime)
    {
        $this->currenttime = $currenttime;

        return $this;
    }

    /**
     * Get currenttime
     *
     * @return \DateTime
     */
    public function getCurrenttime()
    {
        return $this->currenttime;
    }

    /**
     * Set moduser
     *
     * @param string $moduser
     * @return InfoDcrtype
     */
    public function setModuser($moduser)
    {
        $this->moduser = $moduser;

        return $this;
    }

    /**
     * Get moduser
     *
     * @return string
     */
    public function getModuser()
    {
        return $this->moduser;
    }

    /**
     * Set applyDate
     *
     * @param InfoCustomdictionaryvalue $applyDate
     * @return InfoDcrtype
     */
    public function setApplyDate(InfoCustomdictionaryvalue $applyDate = null)
    {
        $this->applyDate = $applyDate;

        return $this;
    }

    /**
     * Get applyDate
     *
     * @return InfoCustomdictionaryvalue
     */
    public function getApplyDate()
    {
        return $this->applyDate;
    }

    /**
     * Add role
     *
     * @param InfoRole $role
     *
     * @return InfoDcrtype
     */
    public function addRole(InfoRole $role)
    {
        $this->roles->add($role);

        return $this;
    }

    /**
     * Remove role
     *
     * @param InfoRole $role
     */
    public function removeRole(InfoRole $role)
    {
        $this->roles->removeElement($role);
    }

    /**
     * Get roles
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getRoles()
    {
        return $this->roles;
    }
}
