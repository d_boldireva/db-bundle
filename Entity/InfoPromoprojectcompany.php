<?php

namespace TeamSoft\CrmRepositoryBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * InfoPromoprojectcompany
 *
 * @ORM\Table(name="info_promoprojectcompany")
 * @ORM\Entity(repositoryClass="TeamSoft\CrmRepositoryBundle\Repository\InfoPromoprojectcompanyRepository")
 */
class InfoPromoprojectcompany implements ServiceFieldInterface
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="currenttime", type="datetime", nullable=true)
     */
    private $currenttime;

    /**
     * @var string|null
     *
     * @ORM\Column(name="moduser", type="string", length=16, nullable=true)
     */
    private $moduser;

    /**
     * @var string|null
     *
     * @ORM\Column(name="guid", type="guid", nullable=true)
     */
    private $guid;

    /**
     * @var InfoPromoproject
     *
     * @ORM\ManyToOne(targetEntity="InfoPromoproject")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="promoproject_id", referencedColumnName="id")
     * })
     */
    private $promoproject;

    /**
     * @var InfoCompany
     *
     * @ORM\ManyToOne(targetEntity="InfoCompany")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="company_id", referencedColumnName="id")
     * })
     */
    private $company;

    /**
     * @var InfoContact
     *
     * @ORM\ManyToOne(targetEntity="InfoContact")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="contact_id", referencedColumnName="id")
     * })
     */
    private $contact;

    /**
     * @var InfoDirection
     *
     * @ORM\ManyToOne(targetEntity="InfoDirection")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="direction_id", referencedColumnName="id")
     * })
     */
    private $direction;


    /**
     * @var string|null
     */
    private $name;


    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set currenttime.
     *
     * @param \DateTime|null $currenttime
     *
     * @return InfoPromoprojectcompany
     */
    public function setCurrenttime(\DateTime $currenttime = null)
    {
        $this->currenttime = $currenttime;

        return $this;
    }

    /**
     * Get currenttime.
     *
     * @return \DateTime|null
     */
    public function getCurrenttime()
    {
        return $this->currenttime;
    }

    /**
     * Set moduser.
     *
     * @param string|null $moduser
     *
     * @return InfoPromoprojectcompany
     */
    public function setModuser($moduser = null)
    {
        $this->moduser = $moduser;

        return $this;
    }

    /**
     * Get moduser.
     *
     * @return string|null
     */
    public function getModuser()
    {
        return $this->moduser;
    }

    /**
     * Set guid.
     *
     * @param string|null $guid
     *
     * @return InfoPromoprojectcompany
     */
    public function setGuid($guid = null)
    {
        $this->guid = $guid;

        return $this;
    }

    /**
     * Get guid.
     *
     * @return string|null
     */
    public function getGuid()
    {
        return $this->guid;
    }

    /**
     * Set promoproject.
     *
     * @param \TeamSoft\CrmRepositoryBundle\Entity\InfoPromoproject|null $promoproject
     *
     * @return InfoPromoprojectcompany
     */
    public function setPromoproject(\TeamSoft\CrmRepositoryBundle\Entity\InfoPromoproject $promoproject = null)
    {
        $this->promoproject = $promoproject;

        return $this;
    }

    /**
     * Get promoproject.
     *
     * @return \TeamSoft\CrmRepositoryBundle\Entity\InfoPromoproject|null
     */
    public function getPromoproject()
    {
        return $this->promoproject;
    }

    /**
     * Set company.
     *
     * @param \TeamSoft\CrmRepositoryBundle\Entity\InfoCompany|null $company
     *
     * @return InfoPromoprojectcompany
     */
    public function setCompany(\TeamSoft\CrmRepositoryBundle\Entity\InfoCompany $company = null)
    {
        $this->company = $company;

        return $this;
    }

    /**
     * Get company.
     *
     * @return \TeamSoft\CrmRepositoryBundle\Entity\InfoCompany|null
     */
    public function getCompany()
    {
        return $this->company;
    }

    /**
     * Set contact.
     *
     * @param \TeamSoft\CrmRepositoryBundle\Entity\InfoContact|null $contact
     *
     * @return InfoPromoprojectcompany
     */
    public function setContact(\TeamSoft\CrmRepositoryBundle\Entity\InfoContact $contact = null)
    {
        $this->contact = $contact;

        return $this;
    }

    /**
     * Get contact.
     *
     * @return \TeamSoft\CrmRepositoryBundle\Entity\InfoContact|null
     */
    public function getContact()
    {
        return $this->contact;
    }

    /**
     * Set direction.
     *
     * @param \TeamSoft\CrmRepositoryBundle\Entity\InfoDirection|null $direction
     *
     * @return InfoPromoprojectcompany
     */
    public function setDirection(\TeamSoft\CrmRepositoryBundle\Entity\InfoDirection $direction = null)
    {
        $this->direction = $direction;

        return $this;
    }

    /**
     * Get direction.
     *
     * @return \TeamSoft\CrmRepositoryBundle\Entity\InfoDirection|null
     */
    public function getDirection()
    {
        return $this->direction;
    }

    public function getCompanyId () {
        return $this->getCompany() ? $this->getCompany()->getId() : null;
    }

    public function getContactId () {
        return $this->getContact() ? $this->getContact()->getId() : null;
    }


    /**
     * Get id.
     *
     * @return int
     */
    public function getName()
    {
        return $this->contact->getFirstname() + ' ' + $this->contact->getMiddlename() + ' ' + $this->contact->getLastname();
    }
}
