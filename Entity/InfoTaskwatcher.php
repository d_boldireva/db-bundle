<?php

namespace TeamSoft\CrmRepositoryBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * InfoTaskwatcher
 *
 * @ORM\Table(name="info_taskwatcher")
 * @ORM\Entity
 */
class InfoTaskwatcher implements ServiceFieldInterface
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="currenttime", type="datetime", nullable=true)
     */
    private $currenttime;

    /**
     * @var string|null
     *
     * @ORM\Column(name="moduser", type="string", length=16, nullable=true)
     */
    private $moduser;

    /**
     * @var string|null
     *
     * @ORM\Column(name="guid", type="guid", nullable=true)
     */
    private $guid;

    /**
     * @var int|null
     *
     * @ORM\Column(name="entitytype", type="integer", nullable=true)
     */
    private $entitytype;

    /**
     * @var \InfoTask
     *
     * @ORM\ManyToOne(targetEntity="InfoTask")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="task_id", referencedColumnName="id")
     * })
     */
    private $task;

    /**
     * @var \InfoUser
     *
     * @ORM\ManyToOne(targetEntity="InfoUser")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     * })
     */
    private $user;



    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set currenttime.
     *
     * @param \DateTime|null $currenttime
     *
     * @return InfoTaskwatcher
     */
    public function setCurrenttime(\DateTime $currenttime = null)
    {
        $this->currenttime = $currenttime;

        return $this;
    }

    /**
     * Get currenttime.
     *
     * @return \DateTime|null
     */
    public function getCurrenttime()
    {
        return $this->currenttime;
    }

    /**
     * Set moduser.
     *
     * @param string|null $moduser
     *
     * @return InfoTaskwatcher
     */
    public function setModuser($moduser = null)
    {
        $this->moduser = $moduser;

        return $this;
    }

    /**
     * Get moduser.
     *
     * @return string|null
     */
    public function getModuser()
    {
        return $this->moduser;
    }

    /**
     * Set guid.
     *
     * @param string|null $guid
     *
     * @return InfoTaskwatcher
     */
    public function setGuid($guid = null)
    {
        $this->guid = $guid;

        return $this;
    }

    /**
     * Get guid.
     *
     * @return string|null
     */
    public function getGuid()
    {
        return $this->guid;
    }

    /**
     * Set entitytype.
     *
     * @param int|null $entitytype
     *
     * @return InfoTaskwatcher
     */
    public function setEntitytype($entitytype = null)
    {
        $this->entitytype = $entitytype;

        return $this;
    }

    /**
     * Get entitytype.
     *
     * @return int|null
     */
    public function getEntitytype()
    {
        return $this->entitytype;
    }

    /**
     * Get entitytype int value.
     *
     * @return int
     */
    public function getEntityTypeInt()
    {
        return intval($this->entitytype);
    }

    /**
     * Set task.
     *
     * @param \TeamSoft\CrmRepositoryBundle\Entity\InfoTask|null $task
     *
     * @return InfoTaskwatcher
     */
    public function setTask(\TeamSoft\CrmRepositoryBundle\Entity\InfoTask $task = null)
    {
        $this->task = $task;

        return $this;
    }

    /**
     * Get task.
     *
     * @return \TeamSoft\CrmRepositoryBundle\Entity\InfoTask|null
     */
    public function getTask()
    {
        return $this->task;
    }

    /**
     * Set user.
     *
     * @param \TeamSoft\CrmRepositoryBundle\Entity\InfoUser|null $user
     *
     * @return InfoTaskwatcher
     */
    public function setUser(\TeamSoft\CrmRepositoryBundle\Entity\InfoUser $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user.
     *
     * @return \TeamSoft\CrmRepositoryBundle\Entity\InfoUser|null
     */
    public function getUser()
    {
        return $this->user;
    }

    public function getTaskId() {
        return $this->getTask() ? $this->getTask()->getId() : null;
    }

    public function getUserId() {
        return $this->getUser() ? $this->getUser()->getId() : null;
    }
}
