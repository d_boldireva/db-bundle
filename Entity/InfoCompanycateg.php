<?php

namespace TeamSoft\CrmRepositoryBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * InfoCompanycateg
 *
 * @ORM\Table(name="info_companycateg")
 * @ORM\Entity
 */
class InfoCompanycateg implements ServiceFieldInterface
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="guid", type="guid", nullable=true)
     */
    private $guid;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="currenttime", type="datetime", nullable=true)
     */
    private $currenttime;

    /**
     * @var string
     *
     * @ORM\Column(name="moduser", type="string", length=16, nullable=true)
     */
    private $moduser;

    /**
     * @var InfoCompany
     *
     * @ORM\ManyToOne(targetEntity="InfoCompany")
     * @ORM\JoinColumn(name="company_id", referencedColumnName="id", onDelete="CASCADE")
     */
    private $company;

    /**
     * @var InfoCompanycategory
     *
     * @ORM\ManyToOne(targetEntity="InfoCompanycategory")
     * @ORM\JoinColumn(name="category_id", referencedColumnName="id", onDelete="CASCADE")
     */
    private $companycategory;

    /**
     * @var InfoDirection
     *
     * @ORM\ManyToOne(targetEntity="InfoDirection")
     * @ORM\JoinColumn(name="direction_id", referencedColumnName="id", onDelete="SET NULL")
     */
    private $direction;

    /**
     * Set guid
     *
     * @param string $guid
     * @return InfoCompanycateg
     */
    public function setGuid($guid)
    {
        $this->guid = $guid;

        return $this;
    }

    /**
     * Get guid
     *
     * @return string
     */
    public function getGuid()
    {
        return $this->guid;
    }

    /**
     * Set currenttime
     *
     * @param \DateTime $currenttime
     * @return InfoCompanycateg
     */
    public function setCurrenttime(\DateTime $currenttime)
    {
        $this->currenttime = $currenttime;

        return $this;
    }

    /**
     * Get currenttime
     *
     * @return \DateTime
     */
    public function getCurrenttime()
    {
        return $this->currenttime;
    }

    /**
     * Set moduser
     *
     * @param string $moduser
     * @return InfoCompanycateg
     */
    public function setModuser($moduser)
    {
        $this->moduser = $moduser;

        return $this;
    }

    /**
     * Get moduser
     *
     * @return string
     */
    public function getModuser()
    {
        return $this->moduser;
    }

    /**
     * @param InfoCompany $company
     * @return $this
     */
    public function setCompany(InfoCompany $company){
        $this->company = $company;
        return $this;
    }

    /**
     * @return InfoCompany
     */
    public function getCompany(){
        return $this->company;
    }

    /**
     * @param InfoCompanycategory $companycategory
     * @return $this
     */
    public function setCompanycategory(?InfoCompanycategory $companycategory){
        $this->companycategory = $companycategory;
        return $this;
    }

    /**
     * @return InfoCompanycategory
     */
    public function getCompanycategory(){
        return $this->companycategory;
    }

    /**
     * @param InfoDirection $direction
     * @return $this
     */
    public function setDirection(?InfoDirection $direction){
        $this->direction = $direction;
        return $this;
    }

    /**
     * @return InfoDirection
     */
    public function getDirection(){
        return $this->direction;
    }

    /**
     * @return int|null
     */
    public function getCategoryId(): ?int
    {
        return $this->getCompanycategory() ? $this->getCompanycategory()->getId() : null;
    }

    /**
     * @return string|null
     */
    public function getCategoryName(): ?string
    {
        return $this->getCompanycategory() ? $this->getCompanycategory()->getName() : null;
    }

    /**
     * @return int|null
     */
    public function getDirectionId(): ?int
    {
        return $this->getDirection() ? $this->getDirection()->getId() : null;
    }
}
