<?php

namespace TeamSoft\CrmRepositoryBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * InfoPreparationDtDose
 *
 * @ORM\Table(name="info_preparationdtdose")
 * @ORM\Entity
 */
class InfoPreparationDtDose implements ServiceFieldInterface
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;
    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="datefrom", type="datetime", nullable=true)
     */
    private $dateFrom;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="datetill", type="datetime", nullable=true)
     */
    private $dateTill;

    /**
     * @var string|null
     *
     * @ORM\Column(name="dose", type="string", length=255, nullable=true)
     */
    private $dose;

    /**
     * @var InfoPreparation|null
     *
     * @ORM\OneToOne(targetEntity="InfoPreparation")
     * @ORM\JoinColumn(name="preparation_id", referencedColumnName="id")
     */
    private $preparation;

    /**
     * @var string|null
     *
     * @ORM\Column(name="guid", type="guid", nullable=true)
     */
    private $guid;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="currenttime", type="datetime", nullable=true)
     */
    private $currenttime;

    /**
     * @var string|null
     *
     * @ORM\Column(name="moduser", type="string", length=16, nullable=true)
     */
    private $moduser;

    /**
     * @var InfoTaskPreparationDtDose|null
     * @ORM\OneToOne(targetEntity="InfoTaskPreparationDtDose", mappedBy="preparationDtDose")
     */
    private $taskPreparationDtDose;

    /**
     * @return int
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return \DateTime|null
     */
    public function getDateFrom(): ?\DateTime
    {
        return $this->dateFrom;
    }

    /**
     * @param \DateTime|null $dateFrom
     * @return self
     */
    public function setDateFrom(?\DateTime $dateFrom): self
    {
        $this->dateFrom = $dateFrom;
        return $this;
    }

    /**
     * @return \DateTime|null
     */
    public function getDateTill(): ?\DateTime
    {
        return $this->dateTill;
    }

    /**
     * @param \DateTime|null $dateTill
     * @return self
     */
    public function setDateTill(?\DateTime $dateTill): self
    {
        $this->dateTill = $dateTill;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getDose(): ?string
    {
        return $this->dose;
    }

    /**
     * @param string|null $dose
     * @return self
     */
    public function setDose(?string $dose): self
    {
        $this->dose = $dose;
        return $this;
    }

    /**
     * @return InfoPreparation|null
     */
    public function getPreparation(): ?InfoPreparation
    {
        return $this->preparation;
    }

    /**
     * @param InfoPreparation|null $preparation
     * @return self
     */
    public function setPreparation(?InfoPreparation $preparation): self
    {
        $this->preparation = $preparation;
        return $this;
    }

    /**
     * @return InfoTaskPreparationDtDose|null
     */
    public function getTaskPreparationDtDose(): ?InfoTaskPreparationDtDose
    {
        return $this->taskPreparationDtDose;
    }

    /**
     * @param InfoTaskPreparationDtDose|null $taskPreparationDtDose
     * @return self
     */
    public function setTaskPreparationDtDose(?InfoTaskPreparationDtDose $taskPreparationDtDose): self
    {
        $this->taskPreparationDtDose = $taskPreparationDtDose;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getGuid(): ?string
    {
        return $this->guid;
    }

    /**
     * @param string|null $guid
     * @return self
     */
    public function setGuid($guid): self
    {
        $this->guid = $guid;
        return $this;
    }

    /**
     * @return \DateTime|null
     */
    public function getCurrenttime(): ?\DateTime
    {
        return $this->currenttime;
    }

    /**
     * @param \DateTime|null $currenttime
     * @return self
     */
    public function setCurrenttime(?\DateTime $currenttime): self
    {
        $this->currenttime = $currenttime;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getModuser(): ?string
    {
        return $this->moduser;
    }

    /**
     * @param string|null $moduser
     * @return self
     */
    public function setModuser($moduser): self
    {
        $this->moduser = $moduser;
        return $this;
    }
}
