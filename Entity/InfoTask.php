<?php

namespace TeamSoft\CrmRepositoryBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\Common\Collections\Criteria;
use Doctrine\ORM\Mapping as ORM;
use TeamSoft\CrmRepositoryBundle\Entity\Expenses\InfoExpense;
use TeamSoft\CrmRepositoryBundle\Entity\RTC\InfoRtcRoomReserve;

/**
 * InfoTask
 *
 * @ORM\Table(name="info_task")
 * @ORM\Entity(repositoryClass="TeamSoft\CrmRepositoryBundle\Repository\InfoTask")
 */
class InfoTask implements ServiceFieldInterface, DateTimeWithGmtOffsetInterface
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="Name", type="string", length=255, nullable=true)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="contact_code", type="string", length=255, nullable=true)
     */
    private $contactCode;

    /**
     * @var string
     *
     * @ORM\Column(name="Description", type="varchar_or_n_varchar_image_or_string", length=16, nullable=true)
     */
    private $description;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="modified", type="datetime", nullable=true)
     */
    private $modified;

    /**
     * @var string
     *
     * @ORM\Column(name="taskresult", type="string", length=255, nullable=true)
     */
    private $taskresult;

    /**
     * @var string
     *
     * @ORM\Column(name="taskpurpose", type="string", length=255, nullable=true)
     */
    private $taskpurpose;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="datefrom", type="datetime", nullable=true)
     */
    private $datefrom;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="datetill", type="datetime", nullable=true)
     */
    private $datetill;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dateplan", type="datetime", nullable=true)
     */
    private $dateplan;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date1", type="datetime", nullable=true)
     */
    private $date1;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date2", type="datetime", nullable=true)
     */
    private $date2;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="createdate", type="datetime", nullable=true)
     */
    private $createdate;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="taskstart", type="datetime", nullable=true)
     */
    private $taskstart;

    /**
     * @var integer
     *
     * @ORM\Column(name="istrans", type="integer", nullable=true)
     */
    private $istrans;

    /**
     * @var string
     *
     * @ORM\Column(name="guid", type="guid", nullable=true)
     */
    private $guid;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="currenttime", type="datetime", nullable=true)
     */
    private $currenttime;

    /**
     * @var string
     *
     * @ORM\Column(name="moduser", type="string", length=16, nullable=true)
     */
    private $moduser;

    /**
     * @var integer
     *
     * @ORM\Column(name="num", type="integer", nullable=true)
     */
    private $num;

    /**
     * @var integer
     *
     * @ORM\Column(name="nopromo", type="integer", nullable=true)
     */
    private $nopromo;

    /**
     * @var float
     *
     * @ORM\Column(name="gps_lat", type="float", precision=53, scale=0, nullable=true)
     */
    private $gpsLat;

    /**
     * @var float
     *
     * @ORM\Column(name="gps_lng", type="float", precision=53, scale=0, nullable=true)
     */
    private $gpsLng;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="gps_date", type="datetime", nullable=true)
     */
    private $gpsDate;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="factclosed", type="datetime", nullable=true)
     */
    private $factclosed;

    /**
     * @var string
     *
     * @ORM\Column(name="gps_accuracy", type="string", length=255, nullable=true)
     */
    private $gpsAccuracy;

    /**
     * @var string
     *
     * @ORM\Column(name="gps_error", type="string", length=255, nullable=true)
     */
    private $gpsError;

    /**
     * @var \InfoDictionary
     *
     * @ORM\ManyToOne(targetEntity="InfoDictionary")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="taskpurpose_id", referencedColumnName="id")
     * })
     */
    private $taskpurpose2;

    /**
     * @var \InfoDictionary
     *
     * @ORM\ManyToOne(targetEntity="InfoDictionary")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="department_id", referencedColumnName="id")
     * })
     */
    private $department;

    /**
     * @var \InfoDictionary
     *
     * @ORM\ManyToOne(targetEntity="InfoDictionary")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="specialization_id", referencedColumnName="id")
     * })
     */
    private $specialization;

    /**
     * @var \InfoDictionary
     *
     * @ORM\ManyToOne(targetEntity="InfoDictionary")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="tasktypepurpose_id", referencedColumnName="id")
     * })
     */
    private $tasktypepurpose;

    /**
     * @var \InfoCompany
     *
     * @ORM\ManyToOne(targetEntity="InfoCompany")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="company_id", referencedColumnName="id")
     * })
     */
    private $company;

    /**
     * @var \InfoPlan
     *
     * @ORM\ManyToOne(targetEntity="InfoPlan")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="plan_id", referencedColumnName="id")
     * })
     */
    private $plan;

    /**
     * @var \InfoContact
     *
     * @ORM\ManyToOne(targetEntity="InfoContact")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="contact_id", referencedColumnName="id")
     * })
     */
    private $contact;

    /**
     * @var \InfoUser
     *
     * @ORM\ManyToOne(targetEntity="InfoUser")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="owner_id", referencedColumnName="id")
     * })
     */
    private $owner;

    /**
     * @var \InfoUser
     *
     * @ORM\ManyToOne(targetEntity="InfoUser")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="modifier_id", referencedColumnName="id")
     * })
     */
    private $modifier;

    /**
     * @var \InfoUser
     *
     * @ORM\ManyToOne(targetEntity="InfoUser")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="responsible_id", referencedColumnName="id")
     * })
     */
    private $responsible;

    /**
     * @var \InfoUser
     *
     * @ORM\ManyToOne(targetEntity="InfoUser")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="responsible2_id", referencedColumnName="id")
     * })
     */
    private $responsible2;

    /**
     * @var \InfoProcessitem
     *
     * @ORM\ManyToOne(targetEntity="InfoProcessitem")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="processitem_id", referencedColumnName="id")
     * })
     */
    private $processitem;

    /**
     * @var \InfoTaskstate
     *
     * @ORM\ManyToOne(targetEntity="InfoTaskstate")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="taskstate_id", referencedColumnName="id")
     * })
     */
    private $taskstate;

    /**
     * @var \InfoTasktype
     *
     * @ORM\ManyToOne(targetEntity="InfoTasktype")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="tasktype_id", referencedColumnName="id")
     * })
     */
    private $tasktype;

    /**
     * @var \InfoTaskresult
     *
     * @ORM\ManyToOne(targetEntity="InfoTaskresult")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="taskresult_id", referencedColumnName="id")
     * })
     */
    private $taskresult2;

    /**
     * @var \InfoProcess
     *
     * @ORM\ManyToOne(targetEntity="InfoProcess")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="process_id", referencedColumnName="id")
     * })
     */
    private $process;

    /**
     * @var InfoTaskinfo[] | ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="InfoTaskinfo", mappedBy="subj", cascade={"persist"})
     */
    private $taskInfoCollection;

    /**
     * @var ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="InfoCompanypreparation2", mappedBy="task", cascade={"persist"})
     */
    private $companyPreparation2Collection;

    /**
     * @var InfoTaskpreparation[]|ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="InfoTaskpreparation", mappedBy="task", cascade={"persist"}, orphanRemoval=true)
     */
    private $taskPreparationCollection;

    /**
     * @var ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="InfoTaskparam", mappedBy="task", cascade={"persist"}, orphanRemoval=true)
     */
    private $taskParamCollection;

    /**
     * @var ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="InfoTaskmaterial", mappedBy="task", cascade={"persist"}, orphanRemoval=true)
     */
    private $taskMaterialCollection;

    /**
     * @var ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="InfoContactpotential", mappedBy="task", cascade={"persist"})
     */
    private $contactPotentialCollection;

    /**
     * @var ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="InfoTaskfilegroup", mappedBy="subj", cascade={"persist"})
     */
    private $taskFileGroupCollection;

    /**
     * @var ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="InfoTaskSurveyByInn", mappedBy="task", cascade={"persist"}, orphanRemoval=true)
     */
    private $taskSurveyByInnCollection;


    /**
     * @var ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="InfoSurveyResult", mappedBy="task", cascade={"persist"}, orphanRemoval=true)
     */
    private $surveyResultCollection;


    /**
     * @var ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="InfoTasksubj", mappedBy="task", cascade={"persist"}, orphanRemoval=true)
     */
    private $taskSubjectCollection;

    /**
     * @var InfoTaskPreparationDtDose[]|ArrayCollection
     * @ORM\OneToMany(targetEntity="InfoTaskPreparationDtDose", mappedBy="task")
     */
    private $taskPreparationDtDoses;

//    /**
//     * @ORM\ManyToMany(targetEntity="InfoEvent", mappedBy="tasks")
//     */
//    private $events;

    /**
     * @var ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="InfoTaskwatcher", mappedBy="task", cascade={"persist"}, orphanRemoval=true)
     */
    private $taskWatchers;

    /**
     * @ORM\ManyToMany(targetEntity="InfoUser")
     * @ORM\JoinTable(name="info_taskwatcher",
     *     joinColumns={@ORM\JoinColumn(name="task_id", referencedColumnName="id")},
     *     inverseJoinColumns={@ORM\JoinColumn(name="user_id", referencedColumnName="id")}
     * )
     */
    private $watcherCollection;

    /**
     * @var ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="InfoContactintask", mappedBy="task", cascade={"persist"}, orphanRemoval=true)
     */
    private $contactInTaskCollection;

    /**
     * @var \InfoTraining
     *
     * @ORM\ManyToOne(targetEntity="InfoTraining")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="training_id", referencedColumnName="id")
     * })
     */
    private $training;

    /**
     * @var ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="InfoTrainingform", mappedBy="task", cascade={"persist"}, orphanRemoval=true)
     */
    private $trainingForms;

    /**
     * @var InfoTraining
     *
     * @ORM\ManyToOne(targetEntity="InfoTraining")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="trainingpresentation_id", referencedColumnName="id")
     * })
     */
    private $trainingPresentation;

    /**
     * @var integer
     *
     * @ORM\Column(name="gmt_offset", type="integer", nullable=true)
     */
    private $gmtOffset;

    private $taskNum;

    /**
     * @var InfoRtcRoomReserve
     *
     * @ORM\OneToOne(targetEntity="\TeamSoft\CrmRepositoryBundle\Entity\RTC\InfoRtcRoomReserve", mappedBy="task", cascade={"persist", "remove"})
     */
    private $rtcRoomReserve;

    /**
     * @var ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="InfoMcmdistributionaction", mappedBy="task", cascade={"persist"})
     */
    private $mcmDistributionActions;

    /**
     * @var ArrayCollection<int,InfoExpense>
     *
     * @ORM\OneToMany(targetEntity="\TeamSoft\CrmRepositoryBundle\Entity\Expenses\InfoExpense", mappedBy="task")
     */
    private $expenseCollection;

    /**
     * @var ArrayCollection<int,InfoPromoproject>
     *
     * @ORM\ManyToMany(targetEntity="InfoPromoproject", inversedBy="taskCollection")
     * @ORM\JoinTable(
     *     name="info_taskpromoproject",
     *     joinColumns={@ORM\JoinColumn(name="task_id", referencedColumnName="id")},
     *     inverseJoinColumns={@ORM\JoinColumn(name="promoproject_id", referencedColumnName="id")}
     * )
     */
    private $promoProjectCollection;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->taskInfoCollection = new ArrayCollection();
        $this->companyPreparation2Collection = new ArrayCollection();
        $this->taskPreparationCollection = new ArrayCollection();
        $this->contactPotentialCollection = new ArrayCollection();
        $this->taskParamCollection = new ArrayCollection();
        $this->taskMaterialCollection = new ArrayCollection();
        $this->taskFileGroupCollection = new ArrayCollection();
        $this->taskSubjectCollection = new ArrayCollection();
//        $this->events = new ArrayCollection();
        $this->taskWatchers = new ArrayCollection();
        $this->watcherCollection = new ArrayCollection();
        $this->contactInTaskCollection = new ArrayCollection();
        $this->trainingForms = new ArrayCollection();
        $this->taskSurveyByInnCollection = new ArrayCollection();
        $this->surveyResultCollection = new ArrayCollection();
        $this->mcmDistributionActions = new ArrayCollection();
        $this->taskPreparationDtDoses = new ArrayCollection();
        $this->expenseCollection = new ArrayCollection();
        $this->promoProjectCollection = new ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return InfoTask
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set contactCode
     *
     * @param string $contactCode
     * @return InfoTask
     */
    public function setContactCode($contactCode)
    {
        $this->contactCode = $contactCode;

        return $this;
    }

    /**
     * Get contactCode
     *
     * @return string
     */
    public function getContactCode()
    {
        return $this->contactCode;
    }

    /**
     * Set description
     *
     * @param string $description
     * @return InfoTask
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return iconv('cp1251', 'utf-8', $this->description);
    }

    /**
     * Set modified
     *
     * @param \DateTime $modified
     * @return InfoTask
     */
    public function setModified(\DateTime $modified)
    {
        $this->modified = $modified;

        return $this;
    }

    /**
     * Get modified
     *
     * @return \DateTime
     */
    public function getModified()
    {
        return $this->modified;
    }

    /**
     * Set taskresult
     *
     * @param string $taskresult
     * @return InfoTask
     */
    public function setTaskresult($taskresult)
    {
        $this->taskresult = $taskresult;

        return $this;
    }

    /**
     * Get taskresult
     *
     * @return string
     */
    public function getTaskresult()
    {
        return $this->taskresult;
    }

    /**
     * Set taskpurpose
     *
     * @param string $taskpurpose
     * @return InfoTask
     */
    public function setTaskpurpose($taskpurpose)
    {
        $this->taskpurpose = $taskpurpose;

        return $this;
    }

    /**
     * Get taskpurpose
     *
     * @return string
     */
    public function getTaskpurpose()
    {
        return $this->taskpurpose;
    }

    /**
     * Set datefrom
     *
     * @param \DateTime $datefrom
     * @return InfoTask
     */
    public function setDatefrom($datefrom)
    {
        $this->datefrom = $datefrom;

        return $this;
    }

    /**
     * Get datefrom
     *
     * @return \DateTime
     */
    public function getDatefrom()
    {
        return $this->datefrom;
    }

    /**
     * Set datetill
     *
     * @param \DateTime $datetill
     * @return InfoTask
     */
    public function setDatetill($datetill)
    {
        $this->datetill = $datetill;

        return $this;
    }

    /**
     * Get datetill
     *
     * @return \DateTime
     */
    public function getDatetill()
    {
        return $this->datetill;
    }

    /**
     * Set dateplan
     *
     * @param \DateTime $dateplan
     * @return InfoTask
     */
    public function setDateplan($dateplan)
    {
        $this->dateplan = $dateplan;

        return $this;
    }

    /**
     * Get dateplan
     *
     * @return \DateTime
     */
    public function getDateplan()
    {
        return $this->dateplan;
    }

    /**
     * Set createdate
     *
     * @param \DateTime $createdate
     * @return InfoTask
     */
    public function setCreatedate(\DateTime $createdate)
    {
        $this->createdate = $createdate;

        return $this;
    }

    /**
     * Get createdate
     *
     * @return \DateTime
     */
    public function getCreatedate()
    {
        return $this->createdate;
    }

    /**
     * Set istrans
     *
     * @param integer $istrans
     * @return InfoTask
     */
    public function setIstrans($istrans)
    {
        $this->istrans = $istrans;

        return $this;
    }

    /**
     * Get istrans
     *
     * @return integer
     */
    public function getIstrans()
    {
        return $this->istrans;
    }

    /**
     * Set guid
     *
     * @param guid $guid
     * @return InfoTask
     */
    public function setGuid($guid)
    {
        $this->guid = $guid;

        return $this;
    }

    /**
     * Get guid
     *
     * @return guid
     */
    public function getGuid()
    {
        return $this->guid;
    }

    /**
     * Set currenttime
     *
     * @param \DateTime $currenttime
     * @return InfoTask
     */
    public function setCurrenttime(\DateTime $currenttime)
    {
        $this->currenttime = $currenttime;

        return $this;
    }

    /**
     * Get currenttime
     *
     * @return \DateTime
     */
    public function getCurrenttime()
    {
        return $this->currenttime;
    }

    /**
     * Set moduser
     *
     * @param string $moduser
     * @return InfoTask
     */
    public function setModuser($moduser)
    {
        $this->moduser = $moduser;

        return $this;
    }

    /**
     * Get moduser
     *
     * @return string
     */
    public function getModuser()
    {
        return $this->moduser;
    }

    /**
     * Set num
     *
     * @param integer $num
     * @return InfoTask
     */
    public function setNum($num)
    {
        $this->num = $num;

        return $this;
    }

    /**
     * Get num
     *
     * @return integer
     */
    public function getNum()
    {
        return $this->num;
    }

    /**
     * Set nopromo
     *
     * @param integer $nopromo
     * @return InfoTask
     */
    public function setNopromo($nopromo)
    {
        $this->nopromo = $nopromo;

        return $this;
    }

    /**
     * Get nopromo
     *
     * @return integer
     */
    public function getNopromo()
    {
        return $this->nopromo;
    }

    /**
     * Set gpsLat
     *
     * @param float $gpsLat
     * @return InfoTask
     */
    public function setGpsLat($gpsLat)
    {
        $this->gpsLat = $gpsLat;

        return $this;
    }

    /**
     * Get gpsLat
     *
     * @return float
     */
    public function getGpsLat()
    {
        return $this->gpsLat;
    }

    /**
     * Set gpsLng
     *
     * @param float $gpsLng
     * @return InfoTask
     */
    public function setGpsLng($gpsLng)
    {
        $this->gpsLng = $gpsLng;

        return $this;
    }

    /**
     * Get gpsLng
     *
     * @return float
     */
    public function getGpsLng()
    {
        return $this->gpsLng;
    }

    /**
     * Set gpsDate
     *
     * @param \DateTime $gpsDate
     * @return InfoTask
     */
    public function setGpsDate($gpsDate)
    {
        $this->gpsDate = $gpsDate;

        return $this;
    }

    /**
     * Get gpsDate
     *
     * @return \DateTime
     */
    public function getGpsDate()
    {
        return $this->gpsDate;
    }

    /**
     * Set factclosed
     *
     * @param \DateTime $factclosed
     * @return InfoTask
     */
    public function setFactclosed($factclosed)
    {
        $this->factclosed = $factclosed;

        return $this;
    }

    /**
     * Get factclosed
     *
     * @return \DateTime
     */
    public function getFactclosed()
    {
        return $this->factclosed;
    }

    /**
     * Set gpsAccuracy
     *
     * @param string $gpsAccuracy
     * @return InfoTask
     */
    public function setGpsAccuracy($gpsAccuracy)
    {
        $this->gpsAccuracy = $gpsAccuracy;

        return $this;
    }

    /**
     * Get gpsAccuracy
     *
     * @return string
     */
    public function getGpsAccuracy()
    {
        return $this->gpsAccuracy;
    }

    /**
     * Set gpsError
     *
     * @param string $gpsError
     * @return InfoTask
     */
    public function setGpsError($gpsError)
    {
        $this->gpsError = $gpsError;

        return $this;
    }

    /**
     * Get gpsError
     *
     * @return string
     */
    public function getGpsError()
    {
        return $this->gpsError;
    }

    /**
     * Set taskpurpose2
     *
     * @param \TeamSoft\CrmRepositoryBundle\Entity\InfoDictionary $taskpurpose2
     * @return InfoTask
     */
    public function setTaskpurpose2(\TeamSoft\CrmRepositoryBundle\Entity\InfoDictionary $taskpurpose2 = null)
    {
        $this->taskpurpose2 = $taskpurpose2;

        return $this;
    }

    /**
     * Get taskpurpose2
     *
     * @return \TeamSoft\CrmRepositoryBundle\Entity\InfoDictionary
     */
    public function getTaskpurpose2()
    {
        return $this->taskpurpose2;
    }

    /**
     * Set department
     *
     * @param \TeamSoft\CrmRepositoryBundle\Entity\InfoDictionary $department
     * @return InfoTask
     */
    public function setDepartment(\TeamSoft\CrmRepositoryBundle\Entity\InfoDictionary $department = null)
    {
        $this->department = $department;

        return $this;
    }

    /**
     * Get department
     *
     * @return \TeamSoft\CrmRepositoryBundle\Entity\InfoDictionary
     */
    public function getDepartment()
    {
        return $this->department;
    }

    /**
     * Set specialization
     *
     * @param \TeamSoft\CrmRepositoryBundle\Entity\InfoDictionary $specialization
     * @return InfoTask
     */
    public function setSpecialization(\TeamSoft\CrmRepositoryBundle\Entity\InfoDictionary $specialization = null)
    {
        $this->specialization = $specialization;

        return $this;
    }

    /**
     * Get specialization
     *
     * @return \TeamSoft\CrmRepositoryBundle\Entity\InfoDictionary
     */
    public function getSpecialization()
    {
        return $this->specialization;
    }

    /**
     * Set tasktypepurpose
     *
     * @param \TeamSoft\CrmRepositoryBundle\Entity\InfoDictionary $tasktypepurpose
     * @return InfoTask
     */
    public function setTasktypepurpose(\TeamSoft\CrmRepositoryBundle\Entity\InfoDictionary $tasktypepurpose = null)
    {
        $this->tasktypepurpose = $tasktypepurpose;

        return $this;
    }

    /**
     * Get tasktypepurpose
     *
     * @return \TeamSoft\CrmRepositoryBundle\Entity\InfoDictionary
     */
    public function getTasktypepurpose()
    {
        return $this->tasktypepurpose;
    }

    /**
     * Set company
     *
     * @param \TeamSoft\CrmRepositoryBundle\Entity\InfoCompany $company
     * @return InfoTask
     */
    public function setCompany(\TeamSoft\CrmRepositoryBundle\Entity\InfoCompany $company = null)
    {
        $this->company = $company;

        return $this;
    }

    /**
     * Get company
     *
     * @return \TeamSoft\CrmRepositoryBundle\Entity\InfoCompany
     */
    public function getCompany()
    {
        return $this->company;
    }

    /**
     * Set plan
     *
     * @param \TeamSoft\CrmRepositoryBundle\Entity\InfoPlan $plan
     * @return InfoTask
     */
    public function setPlan(\TeamSoft\CrmRepositoryBundle\Entity\InfoPlan $plan = null)
    {
        $this->plan = $plan;

        return $this;
    }

    /**
     * Get plan
     *
     * @return \TeamSoft\CrmRepositoryBundle\Entity\InfoPlan
     */
    public function getPlan()
    {
        return $this->plan;
    }

    /**
     * Set contact
     *
     * @param \TeamSoft\CrmRepositoryBundle\Entity\InfoContact $contact
     * @return InfoTask
     */
    public function setContact(\TeamSoft\CrmRepositoryBundle\Entity\InfoContact $contact = null)
    {
        $this->contact = $contact;

        return $this;
    }

    /**
     * Get contact
     *
     * @return \TeamSoft\CrmRepositoryBundle\Entity\InfoContact
     */
    public function getContact()
    {
        return $this->contact;
    }

    /**
     * Set owner
     *
     * @param \TeamSoft\CrmRepositoryBundle\Entity\InfoUser $owner
     * @return InfoTask
     */
    public function setOwner(\TeamSoft\CrmRepositoryBundle\Entity\InfoUser $owner = null)
    {
        $this->owner = $owner;

        return $this;
    }

    /**
     * Get owner
     *
     * @return \TeamSoft\CrmRepositoryBundle\Entity\InfoUser
     */
    public function getOwner()
    {
        return $this->owner;
    }

    /**
     * Set modifier
     *
     * @param \TeamSoft\CrmRepositoryBundle\Entity\InfoUser $modifier
     * @return InfoTask
     */
    public function setModifier(\TeamSoft\CrmRepositoryBundle\Entity\InfoUser $modifier = null)
    {
        $this->modifier = $modifier;

        return $this;
    }

    /**
     * Get modifier
     *
     * @return \TeamSoft\CrmRepositoryBundle\Entity\InfoUser
     */
    public function getModifier()
    {
        return $this->modifier;
    }

    /**
     * Set responsible
     *
     * @param \TeamSoft\CrmRepositoryBundle\Entity\InfoUser $responsible
     * @return InfoTask
     */
    public function setResponsible(\TeamSoft\CrmRepositoryBundle\Entity\InfoUser $responsible = null)
    {
        $this->responsible = $responsible;

        return $this;
    }

    /**
     * Get responsible
     *
     * @return \TeamSoft\CrmRepositoryBundle\Entity\InfoUser
     */
    public function getResponsible()
    {
        return $this->responsible;
    }

    /**
     * Set responsible2
     *
     * @param \TeamSoft\CrmRepositoryBundle\Entity\InfoUser $responsible2
     * @return InfoTask
     */
    public function setResponsible2(\TeamSoft\CrmRepositoryBundle\Entity\InfoUser $responsible2 = null)
    {
        $this->responsible2 = $responsible2;

        return $this;
    }

    /**
     * Get responsible2
     *
     * @return \TeamSoft\CrmRepositoryBundle\Entity\InfoUser
     */
    public function getResponsible2()
    {
        return $this->responsible2;
    }

    /**
     * Set processitem
     *
     * @param \TeamSoft\CrmRepositoryBundle\Entity\InfoProcessitem $processitem
     * @return InfoTask
     */
    public function setProcessitem(\TeamSoft\CrmRepositoryBundle\Entity\InfoProcessitem $processitem = null)
    {
        $this->processitem = $processitem;

        return $this;
    }

    /**
     * Get processitem
     *
     * @return \TeamSoft\CrmRepositoryBundle\Entity\InfoProcessitem
     */
    public function getProcessitem()
    {
        return $this->processitem;
    }

    /**
     * Set taskstate
     *
     * @param \TeamSoft\CrmRepositoryBundle\Entity\InfoTaskstate $taskstate
     * @return InfoTask
     */
    public function setTaskstate(\TeamSoft\CrmRepositoryBundle\Entity\InfoTaskstate $taskstate = null)
    {
        $this->taskstate = $taskstate;

        return $this;
    }

    /**
     * Get taskstate
     *
     * @return \TeamSoft\CrmRepositoryBundle\Entity\InfoTaskstate
     */
    public function getTaskstate()
    {
        return $this->taskstate;
    }

    /**
     * Set tasktype
     *
     * @param \TeamSoft\CrmRepositoryBundle\Entity\InfoTasktype $tasktype
     * @return InfoTask
     */
    public function setTasktype(\TeamSoft\CrmRepositoryBundle\Entity\InfoTasktype $tasktype = null)
    {
        $this->tasktype = $tasktype;

        return $this;
    }

    /**
     * Get tasktype
     *
     * @return \TeamSoft\CrmRepositoryBundle\Entity\InfoTasktype
     */
    public function getTasktype()
    {
        return $this->tasktype;
    }

    /**
     * Set taskresult2
     *
     * @param \TeamSoft\CrmRepositoryBundle\Entity\InfoTaskresult $taskresult2
     * @return InfoTask
     */
    public function setTaskresult2(\TeamSoft\CrmRepositoryBundle\Entity\InfoTaskresult $taskresult2 = null)
    {
        $this->taskresult2 = $taskresult2;

        return $this;
    }

    /**
     * Get taskresult2
     *
     * @return \TeamSoft\CrmRepositoryBundle\Entity\InfoTaskresult
     */
    public function getTaskresult2()
    {
        return $this->taskresult2;
    }

    /**
     * Set process
     *
     * @param \TeamSoft\CrmRepositoryBundle\Entity\InfoProcess $process
     * @return InfoTask
     */
    public function setProcess(\TeamSoft\CrmRepositoryBundle\Entity\InfoProcess $process = null)
    {
        $this->process = $process;

        return $this;
    }

    /**
     * Get process
     *
     * @return \TeamSoft\CrmRepositoryBundle\Entity\InfoProcess
     */
    public function getProcess()
    {
        return $this->process;
    }

    public function getTasktypeId()
    {
        return $this->getTasktype() ? $this->getTasktype()->getId() : null;
    }

    public function getTaskstateId()
    {
        return $this->getTaskstate() ? $this->getTaskstate()->getId() : null;
    }

    public function getCompanyId()
    {
        return $this->getCompany() ? $this->getCompany()->getId() : null;
    }

    public function getContactId()
    {
        return $this->getContact() ? $this->getContact()->getId() : null;
    }

    public function getResponsibleId()
    {
        return $this->getResponsible() ? $this->getResponsible()->getId() : null;
    }

    public function getResponsible2Id()
    {
        return $this->getResponsible2() ? $this->getResponsible2()->getId() : null;
    }

    public function getSpecializationId()
    {
        return $this->getSpecialization() ? $this->getSpecialization()->getId() : null;
    }

    public function getPlanId()
    {
        return $this->getPlan() ? $this->getPlan()->getId() : null;
    }

    public function getTrainingId()
    {
        return $this->getTraining() ? $this->getTraining()->getId() : null;
    }

    public function getTrainingPresentationId()
    {
        return $this->getTrainingPresentation() ? $this->getTrainingPresentation()->getId() : null;
    }

    /**
     * Add taskInfoCollection
     *
     * @param \TeamSoft\CrmRepositoryBundle\Entity\InfoTaskinfo $taskInfo
     *
     * @return InfoTask
     */
    public function addTaskInfoCollection(\TeamSoft\CrmRepositoryBundle\Entity\InfoTaskinfo $taskInfo)
    {
        $taskInfo->setSubj($this);
        $this->taskInfoCollection->add($taskInfo);

        return $this;
    }

    /**
     * Remove taskInfoCollection
     *
     * @param \TeamSoft\CrmRepositoryBundle\Entity\InfoTaskinfo $taskInfo
     */
    public function removeTaskInfoCollection(\TeamSoft\CrmRepositoryBundle\Entity\InfoTaskinfo $taskInfo)
    {
        $taskInfo->setSubj(null);
        $this->taskInfoCollection->removeElement($taskInfo);
    }

    /**
     * Get taskInfoCollection
     *
     * @return InfoTaskinfo[]| \Doctrine\Common\Collections\Collection
     */
    public function getTaskInfoCollection(): Collection
    {
        return $this->taskInfoCollection;
    }

    /**
     * Add companyPreparation2Collection
     *
     * @param \TeamSoft\CrmRepositoryBundle\Entity\InfoCompanypreparation2 $companyPreparation2
     *
     * @return InfoTask
     */
    public function addCompanyPreparation2Collection(\TeamSoft\CrmRepositoryBundle\Entity\InfoCompanypreparation2 $companyPreparation2)
    {
        $companyPreparation2->setTask($this);
        $this->companyPreparation2Collection->add($companyPreparation2);

        return $this;
    }

    /**
     * Remove $companyPreparation2
     *
     * @param \TeamSoft\CrmRepositoryBundle\Entity\InfoCompanypreparation2 $companyPreparation2
     */
    public function removeCompanyPreparation2Collection(\TeamSoft\CrmRepositoryBundle\Entity\InfoCompanypreparation2 $companyPreparation2)
    {
        $companyPreparation2->setTask(null);
        $this->companyPreparation2Collection->removeElement($companyPreparation2);
    }

    /**
     * Get companyPreparation2Collection
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getCompanyPreparation2Collection()
    {
        return $this->companyPreparation2Collection;
    }

    /**
     * Add taskPreparationCollection
     *
     * @param \TeamSoft\CrmRepositoryBundle\Entity\InfoTaskpreparation $taskPreparation
     *
     * @return InfoTask
     */
    public function addTaskPreparationCollection(\TeamSoft\CrmRepositoryBundle\Entity\InfoTaskpreparation $taskPreparation)
    {
        $taskPreparation->setTask($this);
        $this->taskPreparationCollection->add($taskPreparation);

        return $this;
    }

    /**
     * Remove taskPreparationCollection
     *
     * @param \TeamSoft\CrmRepositoryBundle\Entity\InfoTaskpreparation $taskPreparation
     */
    public function removeTaskPreparationCollection(\TeamSoft\CrmRepositoryBundle\Entity\InfoTaskpreparation $taskPreparation)
    {
        $taskPreparation->setTask(null);
        $this->taskPreparationCollection->removeElement($taskPreparation);
    }

    /**
     * Get taskPreparationCollection
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getTaskPreparationCollection()
    {
        return $this->taskPreparationCollection;
    }

    /**
     * Add taskMaterialCollection
     *
     * @param \TeamSoft\CrmRepositoryBundle\Entity\InfoTaskmaterial $taskMaterial
     *
     * @return InfoTask
     */
    public function addTaskMaterialCollection(\TeamSoft\CrmRepositoryBundle\Entity\InfoTaskmaterial $taskMaterial)
    {
        $taskMaterial->setTask($this);
        $this->taskMaterialCollection->add($taskMaterial);

        return $this;
    }

    /**
     * Remove taskMaterialCollection
     *
     * @param \TeamSoft\CrmRepositoryBundle\Entity\InfoTaskmaterial $taskMaterial
     */
    public function removeTaskMaterialCollection(\TeamSoft\CrmRepositoryBundle\Entity\InfoTaskmaterial $taskMaterial)
    {
        $taskMaterial->setTask(null);
        $this->taskMaterialCollection->removeElement($taskMaterial);
    }

    /**
     * Get taskMaterialCollection
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getTaskMaterialCollection()
    {
        return $this->taskMaterialCollection;
    }

    /**
     * Add taskParamCollection
     *
     * @param InfoTaskparam $taskParam
     *
     * @return InfoTask
     */
    public function addTaskParamCollection(InfoTaskparam $taskParam)
    {
        $taskParam->setTask($this);
        $this->taskParamCollection->add($taskParam);

        return $this;
    }

    /**
     * Remove taskParamCollection
     *
     * @param InfoTaskparam $taskParam
     */
    public function removeTaskParamCollection(InfoTaskparam $taskParam)
    {
        $taskParam->setTask(null);
        $this->taskParamCollection->removeElement($taskParam);
    }

    /**
     * Get taskParamCollection
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getTaskParamCollection()
    {
        return $this->taskParamCollection;
    }

    /**
     * Add contactPotentialCollection
     *
     * @param \TeamSoft\CrmRepositoryBundle\Entity\InfoContactpotential $contactPotential
     *
     * @return InfoTask
     */
    public function addContactPotentialCollection(\TeamSoft\CrmRepositoryBundle\Entity\InfoContactpotential $contactPotential)
    {
        $contactPotential->setTask($this);
        $this->contactPotentialCollection->add($contactPotential);

        return $this;
    }

    /**
     * Remove contactPotentialCollection
     *
     * @param \TeamSoft\CrmRepositoryBundle\Entity\InfoContactpotential $contactPotential
     */
    public function removeContactPotentialCollection(\TeamSoft\CrmRepositoryBundle\Entity\InfoContactpotential $contactPotential)
    {
        $contactPotential->setTask(null);
        $this->contactPotentialCollection->removeElement($contactPotential);
    }

    /**
     * Get contactPotentialCollection
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getContactPotentialCollection()
    {
        return $this->contactPotentialCollection;
    }

    /**
     * Add taskSubjectCollection
     *
     * @param \TeamSoft\CrmRepositoryBundle\Entity\InfoTasksubj $taskSubject
     *
     * @return InfoTask
     */
    public function addTaskSubjectCollection(\TeamSoft\CrmRepositoryBundle\Entity\InfoTasksubj $taskSubject)
    {
        $taskSubject->setTask($this);
        $this->taskSubjectCollection->add($taskSubject);

        return $this;
    }

    /**
     * Remove taskSubjectCollection
     *
     * @param \TeamSoft\CrmRepositoryBundle\Entity\InfoTasksubj $taskSubject
     */
    public function removeTaskSubjectCollection(\TeamSoft\CrmRepositoryBundle\Entity\InfoTasksubj $taskSubject)
    {
        $taskSubject->setTask(null);
        $this->taskSubjectCollection->removeElement($taskSubject);
    }

    /**
     * Get taskSubjectCollection
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getTaskSubjectCollection()
    {
        return $this->taskSubjectCollection;
    }

    /**
     * Add taskFileGroupCollection
     *
     * @param \TeamSoft\CrmRepositoryBundle\Entity\InfoTaskfilegroup $taskFileGroup
     *
     * @return InfoTask
     */
    public function addTaskFileGroupCollection(\TeamSoft\CrmRepositoryBundle\Entity\InfoTaskfilegroup $taskFileGroup)
    {
        $taskFileGroup->setSubj($this);
        $this->taskFileGroupCollection->add($taskFileGroup);

        return $this;
    }

    /**
     * Remove taskFileGroupCollection
     *
     * @param \TeamSoft\CrmRepositoryBundle\Entity\InfoTaskfilegroup $taskFileGroup
     */
    public function removeTaskFileGroupCollection(\TeamSoft\CrmRepositoryBundle\Entity\InfoTaskfilegroup $taskFileGroup)
    {
        $taskFileGroup->setSubj(null);
        $this->taskFileGroupCollection->removeElement($taskFileGroup);
    }

    /**
     * Get taskFileGroupCollection
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getTaskFileGroupCollection()
    {
        return $this->taskFileGroupCollection;
    }

    /**
     * @return bool
     */
    public function isDone()
    {
        return $this->isFinish() && $this->isClosed();
    }

    /**
     * @return int
     */
    public function isClosed()
    {
        return $this->getTaskstate() && $this->getTaskstate()->getIsclosed();
    }

    /**
     * @return bool
     */
    public function isFinish()
    {
        return $this->getTaskstate() && $this->getTaskstate()->getIsfinish();
    }


    public function getAttachments()
    {
        return $this->getTaskFileGroupCollection()->filter(function (\TeamSoft\CrmRepositoryBundle\Entity\InfoTaskfilegroup $taskFileGroup) {
            return $taskFileGroup->getName() === 'attachments';
        });
    }

    /**
     * Add event
     *
     * @param \TeamSoft\CrmRepositoryBundle\Entity\InfoEvent $event
     *
     * @return InfoTask
     */
//    public function addEvent(\TeamSoft\CrmRepositoryBundle\Entity\InfoEvent $event)
//    {
//        $this->events[] = $event;
//
//        return $this;
//    }

    /**
     * Remove event
     *
     * @param \TeamSoft\CrmRepositoryBundle\Entity\InfoEvent $event
     */
//    public function removeEvent(\TeamSoft\CrmRepositoryBundle\Entity\InfoEvent $event)
//    {
//        $this->events->removeElement($event);
//    }

    /**
     * Get events
     *
     * @return \Doctrine\Common\Collections\Collection
     */
//    public function getEvents()
//    {
//        return $this->events;
//    }

    /**
     * Add watcher
     *
     * @param \TeamSoft\CrmRepositoryBundle\Entity\InfoUser $taskWatcher
     *
     * @return InfoTask
     */
    public function addWatcherCollection(\TeamSoft\CrmRepositoryBundle\Entity\InfoUser $taskWatcher)
    {
        $this->watcherCollection->add($taskWatcher);

        return $this;
    }

    /**
     * Remove watcher
     *
     * @param \TeamSoft\CrmRepositoryBundle\Entity\InfoUser $taskWatcher
     */
    public function removeWatcherCollection(\TeamSoft\CrmRepositoryBundle\Entity\InfoUser $taskWatcher)
    {
        $this->watcherCollection->removeElement($taskWatcher);
    }

    /**
     * Get watchers
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getWatcherCollection()
    {
        return $this->watcherCollection;
    }

    /**
     * Add participant
     *
     * @param \TeamSoft\CrmRepositoryBundle\Entity\InfoContactintask $contactInTask
     *
     * @return InfoTask
     */
    public function addContactInTaskCollection(\TeamSoft\CrmRepositoryBundle\Entity\InfoContactintask $contactInTask)
    {
        $contactInTask->setTask($this);
        $this->contactInTaskCollection->add($contactInTask);

        return $this;
    }

    /**
     * Remove participant
     *
     * @param \TeamSoft\CrmRepositoryBundle\Entity\InfoContactintask $contactInTask
     */
    public function removeContactInTaskCollection(\TeamSoft\CrmRepositoryBundle\Entity\InfoContactintask $contactInTask)
    {
        $contactInTask->setTask(null);
        $this->contactInTaskCollection->removeElement($contactInTask);
    }

    /**
     * Get participants
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getContactInTaskCollection()
    {
        return $this->contactInTaskCollection;
    }

    /**
     * @return mixed
     */
    public function getTaskNum()
    {
        return $this->taskNum;
    }

    /**
     * @param mixed $taskNum
     */
    public function setTaskNum($taskNum): void
    {
        $this->taskNum = $taskNum;
    }


    /**
     * Add taskWatcher.
     *
     * @param \TeamSoft\CrmRepositoryBundle\Entity\InfoTaskwatcher $taskWatcher
     *
     * @return InfoTask
     */
    public function addTaskWatcher(\TeamSoft\CrmRepositoryBundle\Entity\InfoTaskwatcher $taskWatcher)
    {
        $taskWatcher->setTask($this);
        $this->taskWatchers->add($taskWatcher);

        return $this;
    }

    /**
     * Remove taskWatcher.
     *
     * @param \TeamSoft\CrmRepositoryBundle\Entity\InfoTaskwatcher $taskWatcher
     *
     * @return boolean TRUE if this collection contained the specified element, FALSE otherwise.
     */
    public function removeTaskWatcher(\TeamSoft\CrmRepositoryBundle\Entity\InfoTaskwatcher $taskWatcher)
    {
        $taskWatcher->setTask(null);
        return $this->taskWatchers->removeElement($taskWatcher);
    }

    /**
     * Get taskWatchers.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getTaskWatchers()
    {
        return $this->taskWatchers;
    }

    /**
     * Set date1.
     *
     * @param \DateTime|null $date1
     *
     * @return InfoTask
     */
    public function setDate1($date1 = null)
    {
        $this->date1 = $date1;

        return $this;
    }

    /**
     * Get date1.
     *
     * @return \DateTime|null
     */
    public function getDate1()
    {
        return $this->date1;
    }

    /**
     * Set date2.
     *
     * @param \DateTime|null $date2
     *
     * @return InfoTask
     */
    public function setDate2($date2 = null)
    {
        $this->date2 = $date2;

        return $this;
    }

    /**
     * Get date2.
     *
     * @return \DateTime|null
     */
    public function getDate2()
    {
        return $this->date2;
    }

    /**
     * Set training.
     *
     * @param \TeamSoft\CrmRepositoryBundle\Entity\InfoTraining|null $training
     *
     * @return InfoTask
     */
    public function setTraining(\TeamSoft\CrmRepositoryBundle\Entity\InfoTraining $training = null)
    {
        $this->training = $training;

        return $this;
    }

    /**
     * Get training.
     *
     * @return \TeamSoft\CrmRepositoryBundle\Entity\InfoTraining|null
     */
    public function getTraining()
    {
        return $this->training;
    }

    /**
     * Set trainingPresentation.
     *
     * @param \TeamSoft\CrmRepositoryBundle\Entity\InfoTraining|null $trainingPresentation
     *
     * @return InfoTask
     */
    public function setTrainingPresentation(\TeamSoft\CrmRepositoryBundle\Entity\InfoTraining $trainingPresentation = null)
    {
        $this->trainingPresentation = $trainingPresentation;

        return $this;
    }

    /**
     * Get trainingPresentation.
     *
     * @return InfoTraining
     */
    public function getTrainingPresentation()
    {
        return $this->trainingPresentation;
    }

    public function getCoaches()
    {
        $criteria = Criteria::create()->where(Criteria::expr()->eq("entitytype", 0));
        return $this->taskWatchers->matching($criteria)->map(function (InfoTaskwatcher $taskWatcher) {
            return $taskWatcher->getUser();
        });
    }

    public function getMembers()
    {
        $criteria = Criteria::create()->where(Criteria::expr()->eq("entitytype", 1));
        return $this->taskWatchers->matching($criteria)->map(function (InfoTaskwatcher $taskWatcher) {
            return $taskWatcher->getUser();
        });
    }

    /**
     * Add trainingForm.
     *
     * @param InfoTrainingform $trainingForm
     *
     * @return InfoTask
     */
    public function addTrainingForm(InfoTrainingform $trainingForm)
    {
        $trainingForm->setTask($this);
        $this->trainingForms->add($trainingForm);

        return $this;
    }

    /**
     * Remove trainingForm.
     *
     * @param InfoTrainingform $trainingForm
     *
     * @return boolean TRUE if this collection contained the specified element, FALSE otherwise.
     */
    public function removeTrainingForm(InfoTrainingform $trainingForm)
    {
        $trainingForm->setTask(null);
        return $this->trainingForms->removeElement($trainingForm);
    }

    /**
     * Get trainingForms.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getTrainingForms()
    {
        return $this->trainingForms;
    }

    /**
     * Add taskSurveyByInnCollection.
     *
     * @param \TeamSoft\CrmRepositoryBundle\Entity\InfoTaskSurveyByInn $taskSurveyByInnCollection
     *
     * @return InfoTask
     */
    public function addTaskSurveyByInnCollection(\TeamSoft\CrmRepositoryBundle\Entity\InfoTaskSurveyByInn $taskSurveyByInnCollection)
    {
        $taskSurveyByInnCollection->setTask($this);
        $this->taskSurveyByInnCollection[] = $taskSurveyByInnCollection;

        return $this;
    }

    /**
     * Remove taskSurveyByInnCollection.
     *
     * @param \TeamSoft\CrmRepositoryBundle\Entity\InfoTaskSurveyByInn $taskSurveyByInnCollection
     *
     * @return boolean TRUE if this collection contained the specified element, FALSE otherwise.
     */
    public function removeTaskSurveyByInnCollection(\TeamSoft\CrmRepositoryBundle\Entity\InfoTaskSurveyByInn $taskSurveyByInnCollection)
    {
        return $this->taskSurveyByInnCollection->removeElement($taskSurveyByInnCollection);
    }

    /**
     * Get taskSurveyByInnCollection.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getTaskSurveyByInnCollection()
    {
        return $this->taskSurveyByInnCollection;
    }

    /**
     * Add surveyResultCollection.
     *
     * @param \TeamSoft\CrmRepositoryBundle\Entity\InfoSurveyResult $surveyResultCollection
     *
     * @return InfoTask
     */
    public function addSurveyResultCollection(\TeamSoft\CrmRepositoryBundle\Entity\InfoSurveyResult $surveyResultCollection)
    {
        $surveyResultCollection->setTask($this);
        $this->surveyResultCollection[] = $surveyResultCollection;

        return $this;
    }

    /**
     * Remove surveyResultCollection.
     *
     * @param \TeamSoft\CrmRepositoryBundle\Entity\InfoSurveyResult $surveyResultCollection
     *
     * @return boolean TRUE if this collection contained the specified element, FALSE otherwise.
     */
    public function removeSurveyResultCollection(\TeamSoft\CrmRepositoryBundle\Entity\InfoSurveyResult $surveyResultCollection)
    {
        return $this->surveyResultCollection->removeElement($surveyResultCollection);
    }

    /**
     * Get surveyResultCollection.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getSurveyResultCollection()
    {
        return $this->surveyResultCollection;
    }

    /**
     * Set gmtOffset.
     *
     * @param int|null $gmtOffset
     *
     * @return InfoTask
     */
    public function setGmtOffset(int $gmtOffset = null)
    {
        $this->gmtOffset = $gmtOffset;

        return $this;
    }

    /**
     * Get gmtOffset.
     *
     * @return int|null
     */
    public function getGmtOffset()
    {
        return $this->gmtOffset;
    }

    public function getDateTimeWithGmtOffsetPropertyNameList()
    {
        return ['datefrom', 'datetill', 'gpsDate', 'factclosed'];
    }

    /**
     * @param InfoTaskPreparationDtDose $taskPreparationDtDose
     *
     * @return InfoTask
     */
    public function addTaskPreparationDtDose(InfoTaskPreparationDtDose $taskPreparationDtDose)
    {
        $taskPreparationDtDose->setTask($this);
        if ($this->taskPreparationDtDoses->contains($taskPreparationDtDose)) {
            $this->taskPreparationDtDoses->add($taskPreparationDtDose);
        }

        return $this;
    }

    /**
     * @param InfoTaskPreparationDtDose $taskPreparationDtDose
     */
    public function removeTaskPreparationDtDose(InfoTaskPreparationDtDose $taskPreparationDtDose): void
    {
        $this->taskPreparationDtDoses->removeElement($taskPreparationDtDose);
    }

    /**
     * @return InfoTaskPreparationDtDose|Collection
     */
    public function getTaskPreparationDtDoses(): Collection
    {
        return $this->taskPreparationDtDoses;
    }

    /**
     * @return bool
     */
    public function hasRtcRoomReserve()
    {
        return !!$this->rtcRoomReserve;
    }

    /**
     * @return int|null
     */
    public function getRtcRoomReserveId()
    {
        return $this->rtcRoomReserve ? $this->rtcRoomReserve->getId() : null;
    }

    /**
     * @return InfoRtcRoomReserve
     */
    public function getRtcRoomReserve()
    {
        return $this->rtcRoomReserve;
    }

    /**
     * @param InfoRtcRoomReserve|null $rtcRoomReserve
     * @return $this
     */
    public function setRtcRoomReserve($rtcRoomReserve)
    {
        if ($rtcRoomReserve) {
            $rtcRoomReserve->setTask($this);
        }
        $this->rtcRoomReserve = $rtcRoomReserve;
        return $this;
    }

    /**
     * @return ArrayCollection
     */
    public function getMcmDistributionActions()
    {
        return $this->mcmDistributionActions;
    }

    /**
     * @param InfoMcmdistributionaction $mcmDistributionAction
     * @return $this
     */
    public function addMcmDistributionAction(InfoMcmdistributionaction $mcmDistributionAction)
    {
        if (!$this->mcmDistributionActions->contains($mcmDistributionAction)) {
            $this->mcmDistributionActions->add($mcmDistributionAction);
        }

        $mcmDistributionAction->setTask($this);

        return $this;
    }

    /**
     * @param InfoMcmdistributionaction $mcmDistributionAction
     * @return $this
     */
    public function removeMcmDistributionAction(InfoMcmdistributionaction $mcmDistributionAction)
    {
        if ($this->mcmDistributionActions->contains($mcmDistributionAction)) {
            $this->mcmDistributionActions->removeElement($mcmDistributionAction);
            $mcmDistributionAction->setTask(null);
        }

        return $this;
    }

    /**
     * @return ArrayCollection
     */
    public function getExpenseCollection(){
        return $this->expenseCollection;
    }

    /**
     * @param InfoExpense $expense
     * @return $this
     */
    public function addExpenseCollection(InfoExpense $expense){
        $expense->setTask($this);

        if(!$this->expenseCollection->contains($expense)){
            $this->expenseCollection->add($expense);
        }

        return $this;
    }

    /**
     * @param InfoExpense $expense
     * @return $this
     */
    public function removeExpenseCollection(InfoExpense $expense){
        if($this->expenseCollection->contains($expense)){
            $this->expenseCollection->removeElement($expense);
        }

        $expense->setTask(null);

        return $this;
    }

    /**
     * @return ArrayCollection
     */
    public function getPromoProjectCollection(){
        return $this->promoProjectCollection;
    }

    public function getTaskstart(): ?\DateTime
    {
        return $this->taskstart;
    }


    public function setTaskstart(?\DateTime $taskstart): self
    {
        $this->taskstart = $taskstart;

        return $this;
    }
}
