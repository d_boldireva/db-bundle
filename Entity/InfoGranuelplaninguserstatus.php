<?php

namespace TeamSoft\CrmRepositoryBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * InfoGranuelplaninguserstatus
 *
 * @ORM\Table(name="info_granuelplaninguserstatus")
 * @ORM\Entity(repositoryClass="TeamSoft\CrmRepositoryBundle\Repository\InfoGranuelplaninguserstatus")
 */
class InfoGranuelplaninguserstatus implements ServiceFieldInterface
{
    /**
     * @var int|null
     *
     * @ORM\Column(name="user_status", type="integer", nullable=true)
     */
    private $userStatus;

    /**
     * @var int
     *
     * @ORM\Column(name="ID", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var \TeamSoft\CrmRepositoryBundle\Entity\InfoUser
     *
     * @ORM\ManyToOne(targetEntity="\TeamSoft\CrmRepositoryBundle\Entity\InfoUser")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     * })
     */
    private $user;

    /**
     * @var InfoSaleperiod|null
     *
     * @ORM\ManyToOne(targetEntity="\TeamSoft\CrmRepositoryBundle\Entity\InfoSaleperiod")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="saleperiod_id", referencedColumnName="id")
     * })
     */
    private $salePeriod;

    /**
     * @var string
     *
     * @ORM\Column(name="guid", type="guid", nullable=true)
     */
    private $guid;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="currenttime", type="datetime", nullable=true)
     */
    private $currenttime;

    /**
     * @var string
     *
     * @ORM\Column(name="moduser", type="string", length=16, nullable=true)
     */
    private $moduser;

    /**
     * Set userStatus.
     *
     * @param int|null $userStatus
     *
     * @return InfoGranuelplaninguserstatus
     */
    public function setUserStatus($userStatus = null)
    {
        $this->userStatus = $userStatus;

        return $this;
    }

    /**
     * Get userStatus.
     *
     * @return int|null
     */
    public function getUserStatus()
    {
        return $this->userStatus;
    }

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set user.
     *
     * @param \TeamSoft\CrmRepositoryBundle\Entity\InfoUser |null $user
     *
     * @return InfoGranuelplaninguserstatus
     */
    public function setUser(\TeamSoft\CrmRepositoryBundle\Entity\InfoUser $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user.
     *
     * @return \TeamSoft\CrmRepositoryBundle\Entity\InfoUser|null
     */
    public function getUser()
    {
        return $this->user;
    }

    public function getSalePeriod(): ?InfoSaleperiod
    {
        return $this->salePeriod;
    }

    public function setSalePeriod(?InfoSaleperiod $salePeriod): self
    {
        $this->salePeriod = $salePeriod;
        return $this;
    }

    public function getGuid(): ?string
    {
        return $this->guid;
    }

    public function setGuid($guid = null): self
    {
        $this->guid = $guid;
        return $this;
    }

    public function getCurrenttime(): ?\DateTime
    {
        return $this->currenttime;
    }

    public function setCurrenttime(?\DateTime $currenttime): self
    {
        $this->currenttime = $currenttime;
        return $this;
    }

    public function getModuser(): ?string
    {
        return $this->moduser;
    }

    public function setModuser($moduser = null): self
    {
        $this->moduser = $moduser;
        return $this;
    }
}
