<?php

namespace TeamSoft\CrmRepositoryBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * InfoPlandetail
 *
 * @ORM\Table(name="info_plandetail")
 * @ORM\Entity(repositoryClass="TeamSoft\CrmRepositoryBundle\Repository\InfoPlandetail")
 */
class InfoPlandetail implements ServiceFieldInterface
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var int
     *
     * @ORM\Column(name="prcnt", type="integer", nullable=true)
     */
    private $prcnt;

    /**
     * @var string
     */
    private $percent;

    /**
     * @var int
     *
     * @ORM\Column(name="TaskCount", type="integer", nullable=true)
     */
    private $taskcount;

    /**
     * @var int
     *
     * @ORM\Column(name="cntone", type="integer", nullable=true)
     */
    private $cntone;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="varchar_or_n_varchar_image_or_string", nullable=true)
     */
    private $description;

    /**
     * @var guid
     *
     * @ORM\Column(name="guid", type="guid", nullable=true)
     */
    private $guid;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="currenttime", type="datetime", nullable=true)
     */
    private $currenttime;

    /**
     * @var string
     *
     * @ORM\Column(name="moduser", type="string", length=16, nullable=true)
     */
    private $moduser;

    /**
     * @var int
     *
     * @ORM\Column(name="pokr", type="integer", nullable=true)
     */
    private $pokr;

    /**
     * @var InfoPlanmodel
     *
     * @ORM\ManyToOne(targetEntity="InfoPlanmodel")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="planmodel_id", referencedColumnName="id")
     * })
     */
    private $planmodel;

    /**
     * @var InfoCompanycategory
     *
     * @ORM\ManyToOne(targetEntity="InfoCompanycategory")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="categorycompany_id", referencedColumnName="id")
     * })
     */
    private $categorycompany;

    /**
     * @var InfoContactcateg
     *
     * @ORM\ManyToOne(targetEntity="InfoContactcateg")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="category_id", referencedColumnName="id")
     * })
     */
    private $category;

    /**
     * @var InfoPlan
     *
     * @ORM\ManyToOne(targetEntity="InfoPlan", inversedBy="plandetails")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="plan_id", referencedColumnName="id")
     * })
     */
    private $plan;

    /**
     * @var InfoTasktype
     *
     * @ORM\ManyToOne(targetEntity="InfoTasktype")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="tasktype_id", referencedColumnName="id")
     * })
     */
    private $tasktype;

    /**
     * @var InfoTarget
     *
     * @ORM\ManyToOne(targetEntity="InfoTarget")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="target_id", referencedColumnName="id")
     * })
     */
    private $target;

    /**
     * @var InfoPlanpreparation[]|ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="InfoPlanpreparation", mappedBy="plandetail")
     */
    private $planPreparations;

    /**
     * @var InfoPlanpresentation[]|ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="InfoPlanpresentation", mappedBy="plandetail")
     */
    private $planPresentations;

    /**
     * @var InfoPlanpromo[]|ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="InfoPlanpromo", mappedBy="plandetail")
     */
    private $planPromos;

    public function __construct()
    {
        $this->planPreparations = new ArrayCollection();
        $this->planPresentations = new ArrayCollection();
        $this->planPromos = new ArrayCollection();
    }


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set prcnt
     *
     * @param int $prcnt
     *
     * @return InfoPlandetail
     */
    public function setPrcnt($prcnt)
    {
        $this->prcnt = $prcnt;

        return $this;
    }

    /**
     * Get prcnt
     *
     * @return int
     */
    public function getPrcnt()
    {
        return $this->prcnt;
    }

    /**
     * Set percent
     *
     * @param string $percent
     *
     * @return InfoPlandetail
     */
    public function setPercent($percent)
    {
        $this->percent = $percent;

        return $this;
    }

    /**
     * Get percent
     *
     * @return string
     */
    public function getPercent()
    {
        return $this->percent;
    }

    /**
     * Set taskcount
     *
     * @param int $taskcount
     *
     * @return InfoPlandetail
     */
    public function setTaskcount($taskcount)
    {
        $this->taskcount = $taskcount;

        return $this;
    }

    /**
     * Get taskcount
     *
     * @return int
     */
    public function getTaskcount()
    {
        return $this->taskcount;
    }

    /**
     * Set cntone
     *
     * @param int $cntone
     *
     * @return InfoPlandetail
     */
    public function setCntone($cntone)
    {
        $this->cntone = $cntone;

        return $this;
    }

    /**
     * Get cntone
     *
     * @return int
     */
    public function getCntone()
    {
        return $this->cntone;
    }

    /**
     * Set description
     *
     * @param string $description
     *
     * @return InfoPlandetail
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set guid
     *
     * @param string $guid
     *
     * @return InfoPlandetail
     */
    public function setGuid($guid)
    {
        $this->guid = $guid;

        return $this;
    }

    /**
     * Get guid
     *
     * @return string
     */
    public function getGuid()
    {
        return $this->guid;
    }

    /**
     * Set currenttime
     *
     * @param \DateTime $currenttime
     *
     * @return InfoPlandetail
     */
    public function setCurrenttime(\DateTime $currenttime = null)
    {
        $this->currenttime = $currenttime;

        return $this;
    }

    /**
     * Get currenttime
     *
     * @return \DateTime
     */
    public function getCurrenttime()
    {
        return $this->currenttime;
    }

    /**
     * Set moduser
     *
     * @param string $moduser
     *
     * @return InfoPlandetail
     */
    public function setModuser($moduser)
    {
        $this->moduser = $moduser;

        return $this;
    }

    /**
     * Get moduser
     *
     * @return string
     */
    public function getModuser()
    {
        return $this->moduser;
    }

    /**
     * Set pokr
     *
     * @param int $pokr
     *
     * @return InfoPlandetail
     */
    public function setPokr($pokr)
    {
        $this->pokr = $pokr;

        return $this;
    }

    /**
     * Get pokr
     *
     * @return int
     */
    public function getPokr()
    {
        return $this->pokr;
    }

    /**
     * Set planmodel
     *
     * @param \TeamSoft\CrmRepositoryBundle\Entity\InfoPlanmodel $planmodel
     *
     * @return InfoPlandetail
     */
    public function setPlanmodel(\TeamSoft\CrmRepositoryBundle\Entity\InfoPlanmodel $planmodel = null)
    {
        $this->planmodel = $planmodel;

        return $this;
    }

    /**
     * Get planmodel
     *
     * @return \TeamSoft\CrmRepositoryBundle\Entity\InfoPlanmodel
     */
    public function getPlanmodel()
    {
        return $this->planmodel;
    }

    /**
     * Set categorycompany
     *
     * @param \TeamSoft\CrmRepositoryBundle\Entity\InfoCompanycategory $categorycompany
     *
     * @return InfoPlandetail
     */
    public function setCategorycompany(\TeamSoft\CrmRepositoryBundle\Entity\InfoCompanycategory $categorycompany = null)
    {
        $this->categorycompany = $categorycompany;

        return $this;
    }

    /**
     * Get categorycompany
     *
     * @return \TeamSoft\CrmRepositoryBundle\Entity\InfoCompanycategory
     */
    public function getCategorycompany()
    {
        return $this->categorycompany;
    }

    /**
     * Set category
     *
     * @param \TeamSoft\CrmRepositoryBundle\Entity\InfoContactcateg $category
     *
     * @return InfoPlandetail
     */
    public function setCategory(\TeamSoft\CrmRepositoryBundle\Entity\InfoContactcateg $category = null)
    {
        $this->category = $category;

        return $this;
    }

    /**
     * Get category
     *
     * @return \TeamSoft\CrmRepositoryBundle\Entity\InfoContactcateg
     */
    public function getCategory()
    {
        return $this->category;
    }

    /**
     * Set plan
     *
     * @param \TeamSoft\CrmRepositoryBundle\Entity\InfoPlan $plan
     *
     * @return InfoPlandetail
     */
    public function setPlan(\TeamSoft\CrmRepositoryBundle\Entity\InfoPlan $plan = null)
    {
        $this->plan = $plan;

        return $this;
    }

    /**
     * Get plan
     *
     * @return \TeamSoft\CrmRepositoryBundle\Entity\InfoPlan
     */
    public function getPlan()
    {
        return $this->plan;
    }

    /**
     * Set tasktype
     *
     * @param \TeamSoft\CrmRepositoryBundle\Entity\InfoTasktype $tasktype
     *
     * @return InfoPlandetail
     */
    public function setTasktype(\TeamSoft\CrmRepositoryBundle\Entity\InfoTasktype $tasktype = null)
    {
        $this->tasktype = $tasktype;

        return $this;
    }

    /**
     * Get tasktype
     *
     * @return \TeamSoft\CrmRepositoryBundle\Entity\InfoTasktype
     */
    public function getTasktype()
    {
        return $this->tasktype;
    }

    /**
     * Set target
     *
     * @param \TeamSoft\CrmRepositoryBundle\Entity\InfoTarget $target
     *
     * @return InfoPlandetail
     */
    public function setTarget(\TeamSoft\CrmRepositoryBundle\Entity\InfoTarget $target = null)
    {
        $this->target = $target;

        return $this;
    }

    /**
     * Get target
     *
     * @return \TeamSoft\CrmRepositoryBundle\Entity\InfoTarget
     */
    public function getTarget()
    {
        return $this->target;
    }

    /**
     * @return int|null|string
     */
    public function getTaskPromo()
    {
        $taskPromo = null;
        if (is_numeric($this->taskcount) && $this->tasktype && is_numeric($this->tasktype->getPromocount())) {
            $taskPromo = $this->taskcount * $this->tasktype->getPromocount();
        }
        return $taskPromo;
    }

    /**
     * @return int|null
     */
    public function getTaskTypeId()
    {
        return $this->getTasktype() ? $this->getTasktype()->getId() : null;
    }

    /**
     * @return int|null
     */
    public function getTargetId()
    {
        return $this->getTarget() ? $this->getTarget()->getId() : null;
    }

    /**
     * @return int|null
     */
    public function getPlanModelId()
    {
        return $this->getPlanmodel() ? $this->getPlanmodel()->getId() : null;
    }

    /**
     * @return int|null
     */
    public function getCompanyCategoryId()
    {
        return $this->getCategorycompany() ? $this->getCategorycompany()->getId() : null;
    }

    /**
     * @return int|null
     */
    public function getContactCategoryId()
    {
        return $this->getCategory() ? $this->getCategory()->getId() : null;
    }

    /**
     * @return int|null
     */
    public function getPlanId()
    {
        return $this->getPlan() ? $this->getPlan()->getId() : null;
    }

    /**
     * @return ArrayCollection|InfoPlanpreparation[]
     */
    public function getPlanPreparations(): Collection
    {
        return $this->planPreparations;
    }

    /**
     * @param InfoPlanpreparation $infoPlanpreparation
     * @return self
     */
    public function addPlanPreparation(InfoPlanpreparation $infoPlanpreparation): self
    {
        if (!$this->planPreparations->contains($infoPlanpreparation)) {
            $infoPlanpreparation->setPlandetail($this);
            $this->planPreparations->add($infoPlanpreparation);
        }

        return $this;
    }

    /**
     * Remove
     *
     * @param InfoPlanpreparation $infoPlanpreparation
     * @return self
     */
    public function removePlanPreparation(InfoPlanpreparation $infoPlanpreparation): self
    {
        $this->planPreparations->removeElement($infoPlanpreparation);

        return $this;
    }

    /**
     * @return ArrayCollection|InfoPlanpresentation[]
     */
    public function getPlanPresentations(): Collection
    {
        return $this->planPresentations;
    }

    /**
     * @param InfoPlanpresentation $infoPlanpresentation
     * @return self
     */
    public function addPlanPresentation(InfoPlanpresentation $infoPlanpresentation): self
    {
        if (!$this->planPresentations->contains($infoPlanpresentation)) {
            $infoPlanpresentation->setPlandetail($this);
            $this->planPresentations->add($infoPlanpresentation);
        }

        return $this;
    }

    /**
     * Remove
     *
     * @param InfoPlanpresentation $infoPlanpresentation
     * @return self
     */
    public function removePlanPresentation(InfoPlanpresentation $infoPlanpresentation): self
    {
        $this->planPresentations->removeElement($infoPlanpresentation);

        return $this;
    }

    /**
     * @return ArrayCollection|InfoPlanpromo[]
     */
    public function getPlanPromos(): Collection
    {
        return $this->planPromos;
    }

    /**
     * @param InfoPlanpromo $infoPlanpromo
     * @return self
     */
    public function addPlanPromo(InfoPlanpromo $infoPlanpromo): self
    {
        if (!$this->planPromos->contains($infoPlanpromo)) {
            $infoPlanpromo->setPlandetail($this);
            $this->planPromos->add($infoPlanpromo);
        }

        return $this;
    }

    /**
     * Remove
     *
     * @param InfoPlanpromo $infoPlanpromo
     * @return self
     */
    public function removePlanPromo(InfoPlanpromo $infoPlanpromo): self
    {
        $this->planPromos->removeElement($infoPlanpromo);

        return $this;
    }
}