<?php

namespace TeamSoft\CrmRepositoryBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * InfoSystemdictionary
 *
 * @ORM\Table(name="info_systemdictionary")
 * @ORM\Entity
 */
class InfoSystemdictionary
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var int
     *
     * @ORM\Column(name="num", type="integer", nullable=true)
     */
    private $num;

    /**
     * @var string
     *
     * @ORM\Column(name="tablename", type="string", length=100, nullable=true)
     */
    private $tablename;

    /**
     * @var guid
     *
     * @ORM\Column(name="guid", type="guid", nullable=true)
     */
    private $guid;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="currenttime", type="datetime", nullable=true)
     */
    private $currenttime;

    /**
     * @var string
     *
     * @ORM\Column(name="moduser", type="string", length=16, nullable=true)
     */
    private $moduser;

    /**
     * @var int
     *
     * @ORM\Column(name="identifier", type="integer", nullable=true)
     */
    private $identifier;

    /**
     * @var PoDictionary
     *
     * @ORM\OneToOne(targetEntity="PoDictionary", mappedBy="systemDictionary")
     */
    private $poDictionary;

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set num
     *
     * @param int $num
     *
     * @return InfoSystemdictionary
     */
    public function setNum($num)
    {
        $this->num = $num;
    
        return $this;
    }

    /**
     * Get num
     *
     * @return int
     */
    public function getNum()
    {
        return $this->num;
    }

    /**
     * Set tablename
     *
     * @param string $tablename
     *
     * @return InfoSystemdictionary
     */
    public function setTablename($tablename)
    {
        $this->tablename = $tablename;
    
        return $this;
    }

    /**
     * Get tablename
     *
     * @return string
     */
    public function getTablename()
    {
        return $this->tablename;
    }

    /**
     * Set guid
     *
     * @param guid $guid
     *
     * @return InfoSystemdictionary
     */
    public function setGuid($guid)
    {
        $this->guid = $guid;
    
        return $this;
    }

    /**
     * Get guid
     *
     * @return guid
     */
    public function getGuid()
    {
        return $this->guid;
    }

    /**
     * Set currenttime
     *
     * @param \DateTime $currenttime
     *
     * @return InfoSystemdictionary
     */
    public function setCurrenttime($currenttime)
    {
        $this->currenttime = $currenttime;
    
        return $this;
    }

    /**
     * Get currenttime
     *
     * @return \DateTime
     */
    public function getCurrenttime()
    {
        return $this->currenttime;
    }

    /**
     * Set moduser
     *
     * @param string $moduser
     *
     * @return InfoSystemdictionary
     */
    public function setModuser($moduser)
    {
        $this->moduser = $moduser;
    
        return $this;
    }

    /**
     * Get moduser
     *
     * @return string
     */
    public function getModuser()
    {
        return $this->moduser;
    }

    /**
     * Set identifier
     *
     * @param int $identifier
     *
     * @return InfoSystemdictionary
     */
    public function setIdentifier($identifier)
    {
        $this->identifier = $identifier;
    
        return $this;
    }

    /**
     * Get identifier
     *
     * @return int
     */
    public function getIdentifier()
    {
        return $this->identifier;
    }

    /**
     * Set poDictionary
     *
     * @param PoDictionary $poDictionary
     * @return InfoSystemdictionary
     */
    public function setPoDictionary(PoDictionary $poDictionary = null)
    {
        $this->poDictionary = $poDictionary;

        return $this;
    }

    /**
     * Get poDictionary
     *
     * @return PoDictionary
     */
    public function getPoDictionary()
    {
        return $this->poDictionary;
    }

    /**
     * Get tablename as name
     * Added to impart general style of dictionaries
     *
     * @return string
     */
    public function getName(){
        return $this->poDictionary ? $this->poDictionary->getName() : $this->tablename;
    }
}
