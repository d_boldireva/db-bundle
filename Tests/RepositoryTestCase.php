<?php

namespace TeamSoft\CrmRepositoryBundle\Tests;

use Doctrine\Persistence\ManagerRegistry;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class RepositoryTestCase extends WebTestCase
{

    /**
     * {@inheritDoc}
     */
    protected function setUp()
    {
        self::bootKernel();
    }

    /**
     * {@inheritDoc}
     */
    protected function tearDown()
    {
        parent::tearDown();
    }

    protected function getDoctrine(): ManagerRegistry
    {
        /**
         * @var \Psr\Container\ContainerInterface $container
         */
        $container = static::$kernel->getContainer();
        if (!$container->has('doctrine')) {
            throw new \LogicException('The DoctrineBundle is not registered in your application. Try running "composer require symfony/orm-pack".');
        }
        return $container->get('doctrine');
    }

    protected function getRepository(string $className)
    {
       return $this->getDoctrine()->getRepository($className);
    }
}
