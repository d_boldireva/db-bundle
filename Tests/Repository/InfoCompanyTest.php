<?php

namespace TeamSoft\CrmRepositoryBundle\Tests\Repository;

use TeamSoft\CrmRepositoryBundle\Tests\RepositoryTestCase;
use TeamSoft\CrmRepositoryBundle\Repository\InfoCompany;

class InfoTeamLocationTest extends RepositoryTestCase
{

    public function testFindByFilter()
    {
        /**
         * @var InfoCompany $repository
         */
        $repository = $this->getRepository(InfoCompany::class);
        $result = $repository->findByFilter([
            'isArchive' => true,
            'isMain' => true,
        ]);
        $this->assertNotEmpty($result);
    }

    public function testFindPharmacyNetworksByName () {
        /**
         * @var InfoCompany $repository
         */
        $repository = $this->getRepository(InfoCompany::class);
        $result = $repository->findPharmacyNetworksByName('');
        $this->assertNotEmpty($result);
    }
}
