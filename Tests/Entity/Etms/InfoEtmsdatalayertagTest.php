<?php

namespace TeamSoft\CrmRepositoryBundle\Tests\Entity;

use Doctrine\DBAL\Types\Type;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use TeamSoft\CrmRepositoryBundle\Entity\Etms\InfoEtmsdatalayertag;

class InfoEtmsdatalayertagTest extends KernelTestCase
{

    /**
     * @var \Doctrine\ORM\EntityManager
     */
    private $em;

    /**
     * {@inheritDoc}
     */
    public function setUp()
    {
        self::bootKernel();
        $this->em = static::$kernel->getContainer()
            ->get('doctrine')
            ->getManager()
        ;
    }

    public function testCreateEntity()
    {

        $entity = new InfoEtmsdatalayertag();
        $uuidGenerator = new \Doctrine\ORM\Id\UuidGenerator();
        $entity->setCode(uniqid());
        $entity->setGuid($uuidGenerator->generate($this->em, null));
        $entity->setModuser($this->getModeUser($entity));
        $entity->setCurrenttime($this->getCurrentTime($entity));
        $this->em->persist($entity);
        $this->em->flush();
        $this->assertNotNull($entity->getId());
    }

    /**
     * {@inheritDoc}
     */
    protected function tearDown()
    {
        parent::tearDown();
        $this->em->close();
    }

    private function getModeUser (InfoEtmsdatalayertag $entity) {
        $conn = $this->em->getConnection();
        $sql = 'SELECT ' . $entity->getModuser();
        return $conn->executeQuery($sql)->fetchOne();
    }

    private function getCurrentTime (InfoEtmsdatalayertag $entity) {
        $currentTime = $entity->getCurrenttime();
        if (is_string($currentTime)) {
            $conn = $this->em->getConnection();
            $sql = 'SELECT ' . $entity->getCurrenttime();
            $value = $conn->executeQuery($sql)->fetchOne();
            $type = Type::getType(Type::DATETIME_MUTABLE);
            $currentTime = $type->convertToPHPValue($value, $conn->getDatabasePlatform());
        }
        return $currentTime;
    }
}
