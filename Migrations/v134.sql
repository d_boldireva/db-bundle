------------------------------------------------------------------------------------------------------------------------
-- https://teamsoft.atlassian.net/browse/REDDYSRSA-207
------------------------------------------------------------------------------------------------------------------------
exec insertLanguageInterface  'en', 'Unsubscribe', N'Unsubscribe', 'crm'
exec insertLanguageInterface  'ru', 'Unsubscribe', N'Отписка', 'crm'
exec insertLanguageInterface  'uk', 'Unsubscribe', N'Відписка', 'crm'
GO

IF OBJECT_ID(N'[info_mcmunsubscribeanswer]', 'U') IS NULL
BEGIN
CREATE TABLE [info_mcmunsubscribeanswer]
(
    [id]                      [int] IDENTITY (1,1) NOT NULL,
    [contact_id]              [int]                NULL,
    [surveyquestion_id]       [int]                NULL,
    [surveyquestionanswer_id] [int]                NULL,
    [answer]                  [varchar](max)      NULL,
    [currenttime]             datetime             NULL
    CONSTRAINT [DF_info_mcmunsubscribeanswercurrenttime] DEFAULT (getdate()),
    [time]                    [timestamp]          NULL,
    [moduser]                 [varchar](16)        NULL
    CONSTRAINT [DF_info_mcmunsubscribeanswermoduser] DEFAULT ([dbo].[Get_CurrentCode]()),
    [guid]                    uniqueidentifier     NULL
    CONSTRAINT [DF_info_mcmunsubscribeanswerguid] DEFAULT (newid()),
    CONSTRAINT [PK_info_mcmunsubscribeanswer] PRIMARY KEY (id) ON [PRIMARY],
    CONSTRAINT [FK_info_mcmunsubscribeanswer_contact_id] FOREIGN KEY ([contact_id]) REFERENCES info_contact ([id]),
    CONSTRAINT [FK_info_mcmunsubscribeanswer_surveyquestion_id] FOREIGN KEY ([surveyquestion_id]) REFERENCES info_surveyquestion ([id]),
    CONSTRAINT [FK_info_mcmunsubscribeanswer_surveyquestionanswer_id] FOREIGN KEY ([surveyquestionanswer_id]) REFERENCES info_surveyquestionanswer ([id])
    )
END
GO

if not exists (select * from dbo.sysobjects where id = object_id('del_info_mcmunsubscribeanswer') and OBJECTPROPERTY(id, 'IsTrigger') = 1)
    exec('create trigger del_info_mcmunsubscribeanswer on info_mcmunsubscribeanswer for delete as if object_id(''empty.dbo.f_rep_del'') is null insert into rep_del(tablename, del_guid) select ''info_mcmunsubscribeanswer'', guid from deleted')
GO

if not exists (select * from dbo.sysobjects where id = object_id('moduser_info_mcmunsubscribeanswer') and OBJECTPROPERTY(id, 'IsTrigger') = 1)
    exec('create trigger moduser_info_mcmunsubscribeanswer on info_mcmunsubscribeanswer for insert, update as if not update(moduser) update info_mcmunsubscribeanswer set moduser = dbo.Get_CurrentCode(), currenttime = getdate() where id in (select id from inserted)')
------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------

------------------------------------------------------------------------------------------------------------------------
---https://teamsoft.atlassian.net/browse/MCM-441
------------------------------------------------------------------------------------------------------------------------
IF OBJECT_ID(N'[info_mcmtelegramuser]') IS NULL
BEGIN
CREATE TABLE [dbo].[info_mcmtelegramuser]
(
    [id]                    [int] IDENTITY (1,1) NOT NULL,
    [contact_id]            [int]                NULL,
    [phone]                 [varchar](64)        NULL,
    [first_name]            [varchar](256)       NULL,
    [last_name]             [varchar](256)       NULL,
    [user_name]             [varchar](256)       NULL,
    [user_identifier]       [varchar](64)        NULL,
    [chat_identifier]       [varchar](64)        NULL,
    [authorization_date]    [datetime]           NULL,
    [currenttime]           [datetime]           NULL,
    [time]                  [timestamp]          NULL,
    [moduser]               [varchar](16)        NULL,
    [guid]                  [uniqueidentifier]   NULL,
    CONSTRAINT [PK_info_mcmtelegramuser] PRIMARY KEY CLUSTERED ([id] ASC) WITH (
                                                                              PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON,
                                                                              ALLOW_PAGE_LOCKS = ON
                                                                              ) ON [PRIMARY]
    ) ON [PRIMARY]

ALTER TABLE [dbo].[info_mcmtelegramuser] ADD CONSTRAINT [DF_info_mcmtelegramuser_currenttime]
    DEFAULT (getdate()) FOR [currenttime]
ALTER TABLE [dbo].[info_mcmtelegramuser] ADD CONSTRAINT [DF_info_mcmtelegramuser_moduser]
    DEFAULT ([dbo].[Get_CurrentCode]()) FOR [moduser]
ALTER TABLE [dbo].[info_mcmtelegramuser] ADD CONSTRAINT [DF_info_mcmtelegramuser_guid]
    DEFAULT (newid()) FOR [guid]
ALTER TABLE [dbo].[info_mcmtelegramuser] WITH CHECK
    ADD CONSTRAINT [DF_info_mcmtelegramuser_contact_id] FOREIGN KEY ([contact_id])
    REFERENCES [dbo].[info_contact] ([id]) ON DELETE CASCADE
END
GO

IF NOT EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS
                WHERE table_name = 'info_mcmcontent' AND column_name = 'is_greeting')
alter table info_mcmcontent add is_greeting int null;
GO

IF NOT EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS
                WHERE table_name = 'info_mcmcontent' AND column_name = 'is_request_phone')
alter table info_mcmcontent add is_request_phone int null;
GO

IF NOT EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS
              WHERE table_name = 'info_contactowner' AND column_name = 'is_main_owner')
alter table info_contactowner add is_main_owner int null;
GO

IF NOT EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS
              WHERE table_name = 'info_contactphone' AND column_name = 'istelegrammcm')
alter table info_contactphone add istelegrammcm int null;
GO

IF NOT EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS
              WHERE table_name = 'info_mcmtarget' AND column_name = 'telegram')
alter table info_mcmtarget add telegram int null;
GO

-- add info_mcmcontentvariable record
IF NOT EXISTS(SELECT 1
              FROM info_mcmcontentvariable
              WHERE code = 'TELEGRAM_CODE')
BEGIN
INSERT INTO info_mcmcontentvariable (name, code)
VALUES ('Telegram Code', 'TELEGRAM_CODE')
END
GO



exec insertLanguageInterface  'en', 'Send phone number', N'Send phone number', 'mcm'
exec insertLanguageInterface  'ru', 'Send phone number', N'Отправить номер телефона', 'mcm'
exec insertLanguageInterface  'uk', 'Send phone number', N'Надіслати номер телефону', 'mcm'
GO

exec insertLanguageInterface 'en', 'Greeting', N'Greeting', 'mcm'
exec insertLanguageInterface 'ru', 'Greeting', N'Приветствие', 'mcm'
exec insertLanguageInterface 'uk', 'Greeting', N'Привітання', 'mcm'
GO

exec insertLanguageInterface 'en', 'Telegram Template', N'Telegram Template', 'mcm'
exec insertLanguageInterface 'ru', 'Telegram Template', N'Telegram Шаблон', 'mcm'
exec insertLanguageInterface 'uk', 'Telegram Template', N'Telegram Шаблон', 'mcm'
GO

exec insertLanguageInterface 'en', 'Request a phone number', N'Request a phone number', 'mcm'
exec insertLanguageInterface 'ru', 'Request a phone number', N'Запросить номер телефона', 'mcm'
exec insertLanguageInterface 'uk', 'Request a phone number', N'Запросити номер телефону', 'mcm'
GO

exec insertLanguageInterface  'en', 'recipient (s)', N'recipient (s)', 'mcm'
exec insertLanguageInterface  'ru', 'recipient (s)', N'получателя(-лей)', 'mcm'
exec insertLanguageInterface  'uk', 'recipient (s)', N'одержувач(-чів)', 'mcm'
GO

exec insertLanguageInterface  'en', 'Please, select a sending date', N'Please, select a sending date', 'validators'
exec insertLanguageInterface  'ru', 'Please, select a sending date', N'Пожалуйста, выберите дату отправки', 'validators'
exec insertLanguageInterface  'uk', 'Please, select a sending date', N'Будь ласка, виберіть дату відправки', 'validators'
GO
------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------

----------------------------------------------------------------------------------------------------
-- https://teamsoft.atlassian.net/browse/GEODATA-838
----------------------------------------------------------------------------------------------------
IF EXISTS (select 1 from dbo.sysobjects where id = object_id('moduser_info_polygon') and OBJECTPROPERTY(id, 'IsTrigger') = 1)
BEGIN
DROP TRIGGER moduser_info_polygon
END
GO

IF EXISTS (select 1 from dbo.sysobjects where id = object_id('update_polygon_buffer_in_info_polygon_trigger') and OBJECTPROPERTY(id, 'IsTrigger') = 1)
BEGIN
DROP TRIGGER update_polygon_buffer_in_info_polygon_trigger
END
GO

if not exists(SELECT * FROM information_schema.columns WHERE table_name='info_polygon' and column_name = 'polygon_buffer')
begin
ALTER TABLE info_polygon add polygon_buffer geometry;
end;
GO

/*
IF NOT EXISTS (SELECT 1 FROM info_polygon WHERE polygon_buffer is not null)
BEGIN
UPDATE info_polygon SET polygon_buffer = polygon.BufferWithTolerance(0.005, 0.1, 1);
END
GO
*/

CREATE TRIGGER update_polygon_buffer_in_info_polygon_trigger ON info_polygon FOR INSERT, UPDATE AS IF NOT UPDATE(polygon_buffer)
UPDATE info_polygon SET polygon_buffer = polygon.BufferWithTolerance(0.005, 0.1, 1), moduser = dbo.Get_CurrentCode() WHERE id IN (select id from inserted)
    GO

CREATE trigger [dbo].[moduser_info_polygon] on [dbo].[info_polygon] for insert, update as if not update(moduser)
update info_polygon set moduser = dbo.Get_CurrentCode(), currenttime = getdate() where id in (select id from inserted)
    GO

if not exists(select 1 from sys.indexes where name = 'ix_info_polygon_polygon_buffer' and type_desc = 'SPATIAL')
BEGIN
        CREATE SPATIAL INDEX [ix_info_polygon_polygon_buffer] ON [dbo].[info_polygon]
            (
                [polygon_buffer]
            ) USING GEOMETRY_GRID
            WITH (
            BOUNDING_BOX =(15, 40, 180, 75), GRIDS =(LEVEL_1 = MEDIUM, LEVEL_2 = MEDIUM, LEVEL_3 = MEDIUM, LEVEL_4 = MEDIUM),
            CELLS_PER_OBJECT = 16, PAD_INDEX = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
END
GO
----------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------

------------------------------------------------------------------------------------------------------------------------
-- https://teamsoft.atlassian.net/browse/GEODATA-834
------------------------------------------------------------------------------------------------------------------------
IF NOT EXISTS(SELECT 1 FROM info_options WHERE name = 'geomarketingUseMultyVersion')
BEGIN
INSERT INTO info_options (name, value) VALUES ('geomarketingUseMultyVersion', '0')
END;
GO
------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------
-- https://teamsoft.atlassian.net/browse/DRREDDYSKZ-196
------------------------------------------------------------------------------------------------------------------------
IF NOT EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE table_name = 'info_contactemail'AND column_name = 'priority')
ALTER TABLE info_contactemail ADD priority int NULL
GO
------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------
-- https://teamsoft.atlassian.net/browse/DRREDDYSRO-29
----------------------------------------------------------------------------------------------------
IF NOT EXISTS(SELECT 1 FROM information_schema.columns WHERE table_name='info_saleperiod' and column_name = 'is_for_granual')
BEGIN
ALTER TABLE info_saleperiod ADD is_for_granual int null;
END;


IF NOT EXISTS (SELECT 1 FROM information_schema.columns WHERE table_name = 'info_companypotential' and column_name = 'prep_id')
BEGIN
ALTER TABLE info_companypotential ADD prep_id int;
ALTER TABLE info_companypotential ADD CONSTRAINT FK_info_companypotential_prep_id FOREIGN KEY(prep_id) REFERENCES info_preparation (id);
END;
GO
----------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------
-- https://teamsoft.atlassian.net/browse/GEODATA-834
------------------------------------------------------------------------------------------------------------------------
IF NOT EXISTS(SELECT 1 FROM info_options WHERE name = 'geomarketingUseMultiVersion')
BEGIN
INSERT INTO info_options (name, value) VALUES ('geomarketingUseMultiVersion', '0')
END;
GO
------------------------------------------------------------------------------------------------------------------------

------------------------------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------
-- https://teamsoft.atlassian.net/browse/DRREDDYSRO-39
----------------------------------------------------------------------------------------------------
IF OBJECT_ID(N'info_tasksaleproject', 'U') is NULL
begin
create table info_tasksaleproject(
    [id] int identity(1,1)  not null
    , [task_id] int  null
            , [saleproject_id] int  null
            , [distributor_id] int  null
            , [currenttime] datetime  null CONSTRAINT [DF_info_tasksaleprojectcurrenttime] DEFAULT (getdate())
            , [time] timestamp  null
            , [moduser] varchar (16) null CONSTRAINT [DF_info_tasksaleprojectmoduser] DEFAULT ([dbo].[Get_CurrentCode]())
            , [guid] uniqueidentifier  null CONSTRAINT [DF_info_tasksaleprojectguid] DEFAULT (newid())
            , CONSTRAINT [PK_info_tasksaleproject] PRIMARY KEY (id) ON [PRIMARY]
    , CONSTRAINT [FK_info_tasksaleproject_task_id] FOREIGN KEY ([task_id]) REFERENCES info_task ([id])
    , CONSTRAINT [FK_info_tasksaleproject_saleproject_id] FOREIGN KEY ([saleproject_id]) REFERENCES info_saleproject ([id])
    , CONSTRAINT [FK_info_tasksaleproject_distributor_id] FOREIGN KEY ([distributor_id]) REFERENCES info_distributor ([id])
    )
end
GO

if not exists (select * from dbo.sysobjects where id = object_id('del_info_tasksaleproject') and OBJECTPROPERTY(id, 'IsTrigger') = 1)
    exec('create trigger del_info_tasksaleproject on info_tasksaleproject for delete as if object_id(''empty.dbo.f_rep_del'') is null
insert into rep_del(tablename, del_guid) select ''info_tasksaleproject'', guid from deleted')
GO

if not exists (select * from dbo.sysobjects where id = object_id('moduser_info_tasksaleproject') and OBJECTPROPERTY(id, 'IsTrigger') = 1)
    exec('create trigger moduser_info_tasksaleproject on info_tasksaleproject for insert, update as if not update(moduser)
update info_tasksaleproject set moduser = dbo.Get_CurrentCode(), currenttime = getdate() where id in (select id from inserted)')
GO
IF OBJECT_ID(N'info_tasksaleprojectprep', 'U') is NULL
begin
create table info_tasksaleprojectprep(
    [id] int identity(1,1)  not null
    , [tasksaleproject_id] int  null
            , [prep_id] int  null
            , [quantity] varchar (255) null
            , [discount] money  null
            , [adddiscount] money  null
            , [adddiscountvalue] money  null
            , [currenttime] datetime  null CONSTRAINT [DF_info_tasksaleprojectprepcurrenttime] DEFAULT (getdate())
            , [time] timestamp  null
            , [moduser] varchar (16) null CONSTRAINT [DF_info_tasksaleprojectprepmoduser] DEFAULT ([dbo].[Get_CurrentCode]())
            , [guid] uniqueidentifier  null CONSTRAINT [DF_info_tasksaleprojectprepguid] DEFAULT (newid())
            , CONSTRAINT [PK_info_tasksaleprojectprep] PRIMARY KEY (id) ON [PRIMARY]
    , CONSTRAINT [FK_info_tasksaleprojectprep_tasksaleproject_id] FOREIGN KEY ([tasksaleproject_id]) REFERENCES info_tasksaleproject ([id])
    , CONSTRAINT [FK_info_tasksaleprojectprep_prep_id] FOREIGN KEY ([prep_id]) REFERENCES info_preparation ([id])
    )
end
GO

if not exists (select * from dbo.sysobjects where id = object_id('del_info_tasksaleprojectprep') and OBJECTPROPERTY(id, 'IsTrigger') = 1)
    exec('create trigger del_info_tasksaleprojectprep on info_tasksaleprojectprep for delete as if object_id(''empty.dbo.f_rep_del'') is null
insert into rep_del(tablename, del_guid) select ''info_tasksaleprojectprep'', guid from deleted')
GO

if not exists (select * from dbo.sysobjects where id = object_id('moduser_info_tasksaleprojectprep') and OBJECTPROPERTY(id, 'IsTrigger') = 1)
    exec('create trigger moduser_info_tasksaleprojectprep on info_tasksaleprojectprep for insert, update as if not update(moduser)
update info_tasksaleprojectprep set moduser = dbo.Get_CurrentCode(), currenttime = getdate() where id in (select id from inserted)')
GO

IF NOT EXISTS (SELECT 1 FROM information_schema.columns WHERE table_name = 'info_tasksaleproject' AND column_name = 'user_id')
BEGIN
ALTER TABLE info_tasksaleproject ADD user_id int null
ALTER TABLE info_tasksaleproject ADD CONSTRAINT FK_info_tasksaleproject_user_id FOREIGN KEY(user_id) REFERENCES info_user (id)
END

IF NOT EXISTS (SELECT 1 FROM information_schema.columns WHERE table_name = 'info_tasksaleproject' AND column_name = 'company_id')
BEGIN
ALTER TABLE info_tasksaleproject ADD company_id int null
ALTER TABLE info_tasksaleproject ADD CONSTRAINT FK_info_tasksaleproject_company_id FOREIGN KEY(company_id) REFERENCES info_company (id)
END

IF NOT EXISTS (SELECT 1 FROM information_schema.columns WHERE table_name = 'info_tasksaleproject' AND column_name = 'dt')
BEGIN
ALTER TABLE info_tasksaleproject ADD dt datetime null
END

IF NOT EXISTS (SELECT 1 FROM information_schema.columns WHERE table_name = 'info_tasksaleproject' AND column_name = 'approvementstatus')
BEGIN
ALTER TABLE info_tasksaleproject ADD approvementstatus int null
ALTER TABLE info_tasksaleproject ADD CONSTRAINT FK_info_tasksaleproject_approvementstatus FOREIGN KEY(approvementstatus) REFERENCES info_CustomDictionaryValue (id)
END
GO

declare @SERVICEID int
set @SERVICEID = (SELECT id FROM info_service WHERE identifier = 'sale_project')

declare @SERVICE_PRIVILEGE_MAIN_ID int
set @SERVICE_PRIVILEGE_MAIN_ID = (SELECT id FROM info_serviceprivilege WHERE parent_id IS NULL AND service_id = @SERVICEID)

IF NOT EXISTS (SELECT 1 FROM info_serviceprivilege WHERE parent_id = @SERVICE_PRIVILEGE_MAIN_ID AND code = 'ROLE_ACCESS_TO_SALE_PROJECT_DISCOUNT_MATRIX_APPROVAL')
    INSERT INTO info_serviceprivilege(name, code, service_id, parent_id)
    VALUES('Access to discount matrix approval', 'ROLE_ACCESS_TO_SALE_PROJECT_DISCOUNT_MATRIX_APPROVAL', @SERVICEID, @SERVICE_PRIVILEGE_MAIN_ID)

IF NOT EXISTS (SELECT 1 FROM info_serviceprivilege WHERE parent_id = @SERVICE_PRIVILEGE_MAIN_ID AND code = 'ROLE_ACCESS_TO_SALE_PROJECT_DISCOUNT_PRICE_LIST')
    INSERT INTO info_serviceprivilege(name, code, service_id, parent_id)
    VALUES('Access to price list', 'ROLE_ACCESS_TO_SALE_PROJECT_DISCOUNT_PRICE_LIST', @SERVICEID, @SERVICE_PRIVILEGE_MAIN_ID)

IF NOT EXISTS (SELECT 1 FROM info_serviceprivilege WHERE parent_id = @SERVICE_PRIVILEGE_MAIN_ID AND code = 'ROLE_ACCESS_TO_SALE_PROJECT_DISCOUNT_CAMPAIGN')
    INSERT INTO info_serviceprivilege(name, code, service_id, parent_id)
    VALUES('Access to discount campaign', 'ROLE_ACCESS_TO_SALE_PROJECT_DISCOUNT_CAMPAIGN', @SERVICEID, @SERVICE_PRIVILEGE_MAIN_ID)
GO

exec insertLanguageInterface 'en', 'Discount matrix approval', 'Discount matrix approval', 'sale_project';
exec insertLanguageInterface 'ru', 'Discount matrix approval', 'Утверждение матрицы скидок', 'sale_project';
exec insertLanguageInterface 'uk', 'Discount matrix approval', 'Затвердження матриці знижок', 'sale_project';
GO
exec insertLanguageInterface 'en', 'Campaign', 'Campaign', 'sale_project';
exec insertLanguageInterface 'ru', 'Campaign', 'Кампания', 'sale_project';
exec insertLanguageInterface 'uk', 'Campaign', 'Кампанія', 'sale_project';
GO
exec insertLanguageInterface 'en', 'Additional discount value', 'Additional discount value', 'sale_project';
exec insertLanguageInterface 'ru', 'Additional discount value', 'Дополнительная стоимость скидки', 'sale_project';
exec insertLanguageInterface 'uk', 'Additional discount value', 'Додаткова вартість знижки', 'sale_project';
GO
exec insertLanguageInterface 'en', 'Pending', 'Pending', 'sale_project';
exec insertLanguageInterface 'ru', 'Pending', 'В ожидании', 'sale_project';
exec insertLanguageInterface 'uk', 'Pending', 'В очікуванні', 'sale_project';
GO
exec insertLanguageInterface 'en', 'Approved', 'Approved', 'sale_project';
exec insertLanguageInterface 'ru', 'Approved', 'Утверждено', 'sale_project';
exec insertLanguageInterface 'uk', 'Approved', 'Затверджено', 'sale_project';
GO
exec insertLanguageInterface 'en', 'Rejected', 'Rejected', 'sale_project';
exec insertLanguageInterface 'ru', 'Rejected', 'Отклонено', 'sale_project';
exec insertLanguageInterface 'uk', 'Rejected', 'Відхилено', 'sale_project';
GO
exec insertLanguageInterface 'en', 'Reopened', 'Reopened', 'sale_project';
exec insertLanguageInterface 'ru', 'Reopened', 'Переоткрыто', 'sale_project';
exec insertLanguageInterface 'uk', 'Reopened', 'Перевідкрито', 'sale_project';
GO
------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------
-- Fix translation of IS AN REPRESENTATIVE
------------------------------------------------------------------------------------------------------------------------
EXEC deleteLanguageInterface 'IS AN REPRESENTATIVE','messages';

EXEC insertLanguageInterface 'en','IS A REPRESENTATIVE','IS A REPRESENTATIVE','messages';
EXEC insertLanguageInterface 'ru','IS A REPRESENTATIVE','ЯВЛЯЕТСЯ ПРЕДСТАВИТЕЛЕМ','messages';
EXEC insertLanguageInterface 'uk','IS A REPRESENTATIVE','Є ПРЕДСТАВНИКОМ','messages';

UPDATE info_dictionaryidentifier SET value = 'IS A REPRESENTATIVE' WHERE value = 'IS AN REPRESENTATIVE';
GO
------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------
-- PHARMAWEB-589
------------------------------------------------------------------------------------------------------------------------
EXEC deleteLanguageInterface 'Created copy of role', 'messages';

EXEC insertLanguageInterface 'en', 'Created copy of role', 'Created copy of role', 'messages';
EXEC insertLanguageInterface 'ru', 'Created copy of role', 'Создана копия роли', 'messages';
EXEC insertLanguageInterface 'uk', 'Created copy of role', 'Створена копія ролі', 'messages';
GO
------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------
