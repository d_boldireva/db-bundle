------------------------------------------------------------------------------------------------------------------------
-- https://teamsoft.atlassian.net/browse/UKEKSPO-27
------------------------------------------------------------------------------------------------------------------------
IF NOT EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE table_name = 'info_mcmfile' AND column_name = 'width')
ALTER TABLE info_mcmfile ADD width int NULL
GO

IF NOT EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE table_name = 'info_mcmfile' AND column_name = 'height')
ALTER TABLE info_mcmfile ADD height int NULL
GO

IF NOT EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE table_name = 'info_mcmcontent' AND column_name = 'certificate_id')

BEGIN
ALTER TABLE info_mcmcontent ADD certificate_id int NULL

ALTER TABLE info_mcmcontent
    ADD CONSTRAINT fk_info_mcmcontent_certificate_id FOREIGN KEY (certificate_id)
        REFERENCES info_mcmfile (id);
END
GO

IF OBJECT_ID(N'info_mcmcertificatevariable', 'U') is NULL
BEGIN
create table info_mcmcertificatevariable
(
    id             int identity constraint pk_info_mcmcertificatevariable primary key,
    color          varchar(255),
    size           int,
    position_left  float,
    position_top   float,
    variable_id    integer,
    certificate_id integer,
    currenttime    datetime
        constraint DF_info_mcmcertificatevariablecurrenttime default getdate(),
    moduser        varchar(16)
        constraint DF_info_mcmcertificatevariablemoduser default [dbo].[Get_CurrentCode](),
    guid           uniqueidentifier
        constraint DF_info_mcmcertificatevariableguid default newid()
)

ALTER TABLE info_mcmcertificatevariable
    ADD CONSTRAINT fk_info_mcmcertificatevariable_certificate_id FOREIGN KEY (certificate_id)
        REFERENCES info_mcmfile (id) ON UPDATE CASCADE ON DELETE CASCADE;

ALTER TABLE info_mcmcertificatevariable
    ADD CONSTRAINT fk_info_mcmcertificatevariable_variable_id FOREIGN KEY (variable_id)
        REFERENCES info_mcmcontentvariable (id) ON UPDATE CASCADE ON DELETE CASCADE;
END
GO

EXEC insertLanguageInterface 'en', 'Add certificate', N'Add certificate', 'messages';
EXEC insertLanguageInterface 'ru', 'Add certificate', N'Добавить сертификат', 'messages';
EXEC insertLanguageInterface 'uk', 'Add certificate', N'Додати сертифікат', 'messages';
GO
------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------
--- https://teamsoft.atlassian.net/browse/EGISRU-306
------------------------------------------------------------------------------------------------------------------------
IF NOT EXISTS (SELECT 1 FROM information_schema.columns
                WHERE table_name = 'info_contactphone' AND column_name = 'for1stsms')
BEGIN
ALTER TABLE info_contactphone ADD for1stsms INT NULL
END
GO
------------------------------------------------------------------------------------------------------------------------
--- https://teamsoft.atlassian.net/browse/DBBI-18
------------------------------------------------------------------------------------------------------------------------
IF OBJECT_ID(N'ps_projectfield_dictionary', 'U') is NULL
BEGIN
create table ps_projectfield_dictionary
(
    id            int identity
        constraint PK_ps_projectfield_dictionary
        primary key,
    name                varchar(255) null,
    code                varchar(255) null,
    type                int null,
    is_required         int null,
    customdictionary_id int null
        constraint FK_PK_ps_projectfield_dictionary_customdictionary_id
        references info_CustomDictionary,
    systemdictionary_id int null
        constraint FK_PK_ps_projectfield_dictionary_systemdictionary_id
        references info_systemdictionary,
    time                timestamp null,
    guid                uniqueidentifier
        constraint DF_ps_projectfield_dictionaryguid default newid(),
    currenttime   datetime
        constraint DF_ps_projectfield_dictionarycurrenttime default getdate(),
    moduser       varchar(16)
        constraint DF_ps_projectfield_dictionarymoduser default [dbo].[Get_CurrentCode]()
)
END
GO

IF OBJECT_ID(N'ps_projectfield', 'U') is NULL
BEGIN
-- auto-generated definition
create table ps_projectfield
(
    id            int identity
        constraint PK_ps_projectfield
        primary key,
    project_id   int
        constraint FK_ps_project_project_id
            references ps_project,
    field_id   int
        constraint FK_ps_projectfield_dictionary_field_id
            references ps_projectfield_dictionary,
    time          timestamp null,
    guid          uniqueidentifier
        constraint DF_ps_projectfieldguid default newid(),
    currenttime   datetime
        constraint DF_ps_projectfieldcurrenttime default getdate(),
    moduser       varchar(16)
        constraint DF_ps_projectfieldmoduser default [dbo].[Get_CurrentCode]()
)
END
GO

IF OBJECT_ID(N'[ps_deletedfiles]', 'U') IS NULL
begin
CREATE TABLE [dbo].[ps_deletedfiles]
(
    [id]          [int] IDENTITY (1,1) NOT NULL,
    [name]        [varchar](255)       NULL,
    [original]    [varchar](255)       NULL,
    [table_name]  [varchar](255)       NULL,
    [pattern_id]  [int]                NULL,
    [user_id]     [int]                NULL,
    [isimported]  [int]                NULL,
    [currenttime] [datetime]           NULL,
    [time]        [timestamp]          NULL,
    [moduser]     [varchar](16)        NULL,
    [guid]        [uniqueidentifier]   NULL,
    CONSTRAINT [PK_ps_deletedfiles] PRIMARY KEY CLUSTERED
(
[id] ASC
) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
    ) ON [PRIMARY]

ALTER TABLE [dbo].[ps_deletedfiles]
    ADD CONSTRAINT [DF_ps_deletedfilescurrenttime] DEFAULT (getdate()) FOR [currenttime]
ALTER TABLE [dbo].[ps_deletedfiles]
    ADD CONSTRAINT [DF_ps_deletedfilesmoduser] DEFAULT ([dbo].[Get_CurrentCode]()) FOR [moduser]
ALTER TABLE [dbo].[ps_deletedfiles]
    ADD CONSTRAINT [DF_ps_deletedfilesguid] DEFAULT (newid()) FOR [guid]
ALTER TABLE [dbo].[ps_deletedfiles] WITH CHECK ADD CONSTRAINT [FK_ps_uploadfiles_pattern_id] FOREIGN KEY ([pattern_id]) REFERENCES [dbo].[ps_pattern] ([id])
ALTER TABLE [dbo].[ps_deletedfiles] WITH CHECK ADD CONSTRAINT [FK_ps_uploadfiles_user_id] FOREIGN KEY ([user_id]) REFERENCES [dbo].[info_user] ([id])
end
Go

IF NOT EXISTS(SELECT 1
              FROM INFORMATION_SCHEMA.COLUMNS
              WHERE table_name = 'ps_pattern_field'
                AND column_name = 'project_field_id')
ALTER TABLE ps_pattern_field
    ADD project_field_id int NULL
        constraint FK_ps_pattern_project_field_id
            references ps_projectfield_dictionary
    GO

exec insertLanguageInterface 'en', 'Please add following required fields to the project', N'Please add following required fields to project'
exec insertLanguageInterface 'ru', 'Please add following required fields to the project', N'Добавьте обязательные поля к проекту'
exec insertLanguageInterface 'uk', 'Please add following required fields to the project', N'Додайте обов`язкові поля до проекту'
exec insertLanguageInterface 'en', 'Patterns not found', N'Patterns not found'
exec insertLanguageInterface 'ru', 'Patterns not found', N'Шаблоны не найдены'
exec insertLanguageInterface 'uk', 'Patterns not found', N'Шаблони не знайдено'
GO
----------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------
-- https://teamsoft.atlassian.net/browse/EGISRU-296
----------------------------------------------------------------------------------------------------
exec insertLanguageInterface 'en', 'Number of tasks per day', N'Number of tasks per day', 'crm'
exec insertLanguageInterface 'ru', 'Number of tasks per day', N'Число задач в день', 'crm'
exec insertLanguageInterface 'uk', 'Number of tasks per day', N'Число завдань в день', 'crm'
GO
----------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------
-- add translations
----------------------------------------------------------------------------------------------------
update info_languageinterface set translation = 'Города' where [key] = 'cities' and domain='geomarketing'  and language_id in (select id from info_language where slug='ru')
update info_languageinterface set translation = 'Міста' where [key] = 'cities' and domain='geomarketing' and language_id in (select id from info_language where slug='uk')
update info_languageinterface set translation = 'Товарооборот' where [key] ='turnover' and domain='geomarketing'  and language_id in (select id from info_language where slug='ru')
update info_languageinterface set translation = 'Товарообіг ' where [key] ='turnover' and domain='geomarketing' and language_id in (select id from info_language where slug='uk')
GO
----------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------
