------------------------------------------------------------------------------------------------------------------------
-- https://teamsoft.atlassian.net/browse/DRREDDYS-670
------------------------------------------------------------------------------------------------------------------------
exec insertLanguageInterface  'en', 'Show inactive', N'Show inactive', 'mcm'
exec insertLanguageInterface  'ru', 'Show inactive', N'Показать неактивные', 'mcm'
exec insertLanguageInterface  'uk', 'Show inactive', N'Показати неактивні', 'mcm'
GO

exec insertLanguageInterface  'en', 'Distributions are loading', N'Distributions are loading', 'mcm'
exec insertLanguageInterface  'ru', 'Distributions are loading', N'Рассылки загружаются', 'mcm'
exec insertLanguageInterface  'uk', 'Distributions are loading', N'Розсилки завантажуються', 'mcm'
GO

exec insertLanguageInterface  'en', 'Contents are loading', N'Contents are loading', 'mcm'
exec insertLanguageInterface  'ru', 'Contents are loading', N'Контент загружается', 'mcm'
exec insertLanguageInterface  'uk', 'Contents are loading', N'Контент завантажується', 'mcm'
GO

exec insertLanguageInterface  'en', 'Conferences are loading', N'Conferences are loading', 'mcm'
exec insertLanguageInterface  'ru', 'Conferences are loading', N'Конференции загружаются', 'mcm'
exec insertLanguageInterface  'uk', 'Conferences are loading', N'Конференції завантажуються', 'mcm'
GO
------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------

------------------------------------------------------------------------------------------------------------------------
-- PHARMAWEB-589
------------------------------------------------------------------------------------------------------------------------
EXEC deleteLanguageInterface 'Created copy of role', 'messages';

EXEC insertLanguageInterface 'en', 'Created copy of role', 'Created copy of role', 'messages';
EXEC insertLanguageInterface 'ru', 'Created copy of role', 'Создана копия роли', 'messages';
EXEC insertLanguageInterface 'uk', 'Created copy of role', 'Створена копія ролі', 'messages';
GO
------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------

-------------------------------------------------------------------------------------------------------------------------------------------------------------------
------- https://teamsoft.atlassian.net/browse/NESTLERU-45
-------------------------------------------------------------------------------------------------------------------------------------------------------------------
IF NOT EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE table_name = 'info_mcmjson' AND column_name = 'mcmdistributionaction_id')
begin
ALTER TABLE info_mcmjson ADD [mcmdistributionaction_id] INT NULL
ALTER TABLE info_mcmjson
    ADD CONSTRAINT FK_info_mcmjson_mcmdistributionaction_id
        FOREIGN KEY ([mcmdistributionaction_id])
            REFERENCES info_mcmdistributionaction([id]);
end
GO
----------------------------------------------------------------------------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------------------------------------------------------------------------

-------------------------------------------------------------------------------------------------------------------------------------------------------------------
------- https://teamsoft.atlassian.net/browse/HEELRU-36
-------------------------------------------------------------------------------------------------------------------------------------------------------------------
exec insertLanguageInterface 'en', 'Did not open mailing letter', 'Did not open mailing letter', 'mcm';
exec insertLanguageInterface 'ru', 'Did not open mailing letter', 'Не открыл письмо рассылки', 'mcm';
exec insertLanguageInterface 'uk', 'Did not open mailing letter', 'Не відкрив листа розсилки', 'mcm';
go
-------------------------------------------------------------------------------------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------------------------------------------------------------------------------

-------------------------------------------------------------------------------------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------------------------------------------------------------------------------
------- https://teamsoft.atlassian.net/browse/HEELRU-35
-------------------------------------------------------------------------------------------------------------------------------------------------------------------
exec insertLanguageInterface 'en', 'Only those who received, but did not open a message for the period', 'Only those who received, but did not open a message for the period', 'mcm'
exec insertLanguageInterface 'ru', 'Only those who received, but did not open a message for the period', 'Только те, кто получил, но не открывал сообщение за период', 'mcm'
exec insertLanguageInterface 'uk', 'Only those who received, but did not open a message for the period', 'Тільки ті, хто отримав, але не відкрив повідомлення за період', 'mcm'

GO
-------------------------------------------------------------------------------------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------
------- https://teamsoft.atlassian.net/browse/NESTLERU-64
------------------------------------------------------------------------------------------------------------------------
IF NOT EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE table_name = 'info_mcmfrom' AND column_name = 'alphaname')
begin
ALTER TABLE info_mcmfrom ADD [alphaname] varchar(255) NULL
end
GO
------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------------------------------------------------------------------------
------- https://teamsoft.atlassian.net/browse/NESTLERU-45
----------------------------------------------------------------------------------------------------------------------------------------------------------------------
IF NOT EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE table_name = 'info_mcmjson' AND column_name = 'mcmdistributionaction_id')
begin
ALTER TABLE info_mcmjson ADD [mcmdistributionaction_id] INT NULL
ALTER TABLE info_mcmjson
    ADD CONSTRAINT FK_info_mcmjson_mcmdistributionaction_id
        FOREIGN KEY ([mcmdistributionaction_id])
            REFERENCES info_mcmdistributionaction([id]);
end
GO
----------------------------------------------------------------------------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------
-- https://teamsoft.atlassian.net/browse/EGISRU-190
----------------------------------------------------------------------------------------------------
exec insertLanguageInterface 'en', 'Role', N'Role', 'crm';
exec insertLanguageInterface 'ru', 'Role', N'Роль', 'crm';
exec insertLanguageInterface 'uk', 'Role', N'Роль', 'crm';

exec insertLanguageInterface 'en', 'Direction', N'Direction', 'crm';
exec insertLanguageInterface 'ru', 'Direction', N'Направление', 'crm';
exec insertLanguageInterface 'uk', 'Direction', N'Напрям', 'crm';
GO
----------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------
-- https://teamsoft.atlassian.net/browse/ER-3
----------------------------------------------------------------------------------------------------
exec insertLanguageInterface 'en', 'Select the type of electronic signature', 'Select the type of electronic signature', 'crm';
exec insertLanguageInterface 'ru', 'Select the type of electronic signature', 'Выберите тип электронной подписи', 'crm';
exec insertLanguageInterface 'uk', 'Select the type of electronic signature', 'Виберіть тип електронного підпису', 'crm';

exec insertLanguageInterface 'en', 'Gallery', 'Gallery', 'crm';
exec insertLanguageInterface 'ru', 'Gallery', 'Галерея', 'crm';
exec insertLanguageInterface 'uk', 'Gallery', 'Галерея', 'crm';
GO
----------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------

-----------------------------------------------------------------------------------------------------------------------
-- https://teamsoft.atlassian.net/browse/SINTEZ-80
------------------------------------------------------------------------------------------------------------------------
IF NOT EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'info_mcmdistributiondirection' AND COLUMN_NAME = 'target_id')
BEGIN
ALTER TABLE info_mcmdistributiondirection ADD target_id INT NULL;
ALTER TABLE info_mcmdistributiondirection WITH CHECK ADD CONSTRAINT fk_info_mcmdistributiondirection_target_id
    FOREIGN KEY (target_id) REFERENCES info_target(id)
END
GO
------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------
--- https://teamsoft.atlassian.net/browse/NAN-43
------------------------------------------------------------------------------------------------------------------------
IF EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'po_gridconfig' AND COLUMN_NAME = 'order')
    EXEC sp_RENAME 'po_gridconfig.[order]', 'sort_order', 'COLUMN';
GO

IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'po_gridconfig' AND COLUMN_NAME = 'sortable')
ALTER TABLE po_gridconfig ADD sortable int NULL;
GO

IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'po_gridconfig' AND COLUMN_NAME = 'serializer_groups')
ALTER TABLE po_gridconfig ADD serializer_groups varchar(MAX) NULL;
GO

IF OBJECT_ID(N'po_gridconfigbyrole', 'U') is NULL
BEGIN
create table po_gridconfigbyrole
(
    [id] [int] IDENTITY (1,1) NOT NULL,
    [gridconfig_id] [int] NULL,
    [role_id] [int] NULL,
    [sort_order] [int] NULL,
    [is_shown] [int] NULL,
    [sortable] [int] NULL,
    [currenttime] [datetime] NULL,
    [time] [timestamp] NULL,
    [moduser] [varchar](16) NULL,
    [guid] [uniqueidentifier] NULL,
    CONSTRAINT [PK_po_gridconfigbyrole] PRIMARY KEY CLUSTERED ([id] ASC)
        WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
        ON [PRIMARY]
    ) ON [PRIMARY]

ALTER TABLE [dbo].[po_gridconfigbyrole]
    ADD CONSTRAINT [DF_po_gridconfigbyrolecurrenttime] DEFAULT (getdate()) FOR [currenttime]
ALTER TABLE [dbo].[po_gridconfigbyrole]
    ADD CONSTRAINT [DF_po_gridconfigbyrolemoduser] DEFAULT ([dbo].[Get_CurrentCode]()) FOR [moduser]
ALTER TABLE [dbo].[po_gridconfigbyrole]
    ADD CONSTRAINT [DF_po_gridconfigbyroleguid] DEFAULT (newid()) FOR [guid]

ALTER TABLE [dbo].[po_gridconfigbyrole]
    WITH CHECK ADD CONSTRAINT [FK_po_gridconfigbyrole_role_id] FOREIGN KEY (role_id)
    REFERENCES info_role ([id]);

ALTER TABLE [dbo].[po_gridconfigbyrole]
    WITH CHECK ADD CONSTRAINT [FK_po_gridconfigbyrole_gridconfig_id] FOREIGN KEY (gridconfig_id)
    REFERENCES po_gridconfig ([id]);
END
GO

exec createCurrenttimeAndModuserTrigger N'po_gridconfigbyrole'
GO

IF OBJECT_ID(N'po_gridconfigbyuser', 'U') is NULL
BEGIN
create table po_gridconfigbyuser
(
    [id] [int] IDENTITY (1,1) NOT NULL,
    [gridconfig_id] [int] NULL,
    [user_id] [int] NULL,
    [sort_order] [int] NULL,
    [is_shown] [int] NULL,
    [sortable] [int] NULL,
    [currenttime] [datetime] NULL,
    [time] [timestamp] NULL,
    [moduser] [varchar](16) NULL,
    [guid] [uniqueidentifier] NULL,
    CONSTRAINT [PK_po_gridconfigbyuser] PRIMARY KEY CLUSTERED ([id] ASC)
        WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
        ON [PRIMARY]
    ) ON [PRIMARY]

ALTER TABLE [dbo].[po_gridconfigbyuser]
    ADD CONSTRAINT [DF_po_gridconfigbyusercurrenttime] DEFAULT (getdate()) FOR [currenttime]
ALTER TABLE [dbo].[po_gridconfigbyuser]
    ADD CONSTRAINT [DF_po_gridconfigbyusermoduser] DEFAULT ([dbo].[Get_CurrentCode]()) FOR [moduser]
ALTER TABLE [dbo].[po_gridconfigbyuser]
    ADD CONSTRAINT [DF_po_gridconfigbyuserguid] DEFAULT (newid()) FOR [guid]

ALTER TABLE [dbo].[po_gridconfigbyuser]
    WITH CHECK ADD CONSTRAINT [FK_po_gridconfigbyuser_user_id] FOREIGN KEY (user_id)
    REFERENCES info_user ([id]);

ALTER TABLE [dbo].[po_gridconfigbyuser]
    WITH CHECK ADD CONSTRAINT [FK_po_gridconfigbyuser_gridconfig_id] FOREIGN KEY (gridconfig_id)
    REFERENCES po_gridconfig ([id]);
END
GO

exec createCurrenttimeAndModuserTrigger N'po_gridconfigbyuser'
GO

IF EXISTS(SELECT 1 FROM sysobjects WHERE id = object_id(N'insertPoGridconfig') AND xtype IN (N'P'))
BEGIN
DROP PROCEDURE insertPoGridconfig
END
GO

CREATE PROCEDURE insertPoGridconfig(@page INT, @field_code VARCHAR(255),
                                    @field_name VARCHAR(255), @is_shown INT = 1,
                                    @subpage INT = null, @order INT = null)
    AS
BEGIN
    DECLARE @entityID INT;
    IF (@subpage IS NULL)
BEGIN
            SET @entityID = (SELECT TOP 1 id FROM po_gridconfig WHERE page = @page AND field_code = @field_code AND subpage IS NULL);
END
ELSE
BEGIN
            SET @entityID = (SELECT TOP 1 id FROM po_gridconfig WHERE page = @page AND field_code = @field_code AND subpage = @subpage);
END

    --
    IF (@entityID IS NULL)
BEGIN
INSERT INTO po_gridconfig(page, subpage, field_code, sort_order, is_shown, field_name)
VALUES (@page, @subpage, @field_code, @order, @is_shown, @field_name);
END
ELSE
BEGIN
UPDATE po_gridconfig
SET page=@page, subpage=@subpage, field_code=@field_code, sort_order=@order, is_shown=@is_shown, field_name=@field_name
WHERE id = @entityID;
END
END
GO

DECLARE @_dcr int;
select @_dcr = (case when COUNT(info_role.id) > 0 THEN 1 ELSE 0 end)  from info_serviceprivilege
                                                                               inner join info_service on info_service.id = info_serviceprivilege.service_id and info_service.identifier = 'dcr'
                                                                               inner join info_roleprivilege on info_roleprivilege.privilege_id = info_serviceprivilege.id
                                                                               inner join info_role on info_role.id = info_roleprivilege.role_id and info_role.code not like '%admin%';

exec insertPoGridconfig 1, 'name', N'Company name'
exec insertPoGridconfig 1, 'type', N'Type'
exec insertPoGridconfig 1, 'category', N'Category'
exec insertPoGridconfig 1, 'address', N'Address'
exec insertPoGridconfig 1, 'last_task_date', N'Last task date', 0
exec insertPoGridconfig 1, 'cr_status_verification', N'Status verification', @_dcr
exec insertPoGridconfig 1, 'get_verification', N'Status change date', @_dcr
exec insertPoGridconfig 1, 'modifier_user_name', N'Request initiator', @_dcr
exec insertPoGridconfig 1, 'is_archive', N'Status', @_dcr
exec insertPoGridconfig 1, 'code', N'Code', 0
exec insertPoGridconfig 1, 'postcode', N'Postcode', 0
exec insertPoGridconfig 1, 'phone1', N'Phone', 0
exec insertPoGridconfig 1, 'phone2', N'Phone (add.)', 0
exec insertPoGridconfig 1, 'eaddr', N'Email', 0
exec insertPoGridconfig 1, 'waddr', N'Site', 0
exec insertPoGridconfig 1, 'owner', N'Owner', 0

exec insertPoGridconfig 2, 'lastname', N'Last name'
exec insertPoGridconfig 2, 'firstname', N'First name'
exec insertPoGridconfig 2, 'middlename', N'Middle name'
exec insertPoGridconfig 2, 'specialization', N'Specialization'
exec insertPoGridconfig 2, 'companyname', N'Company'
exec insertPoGridconfig 2, 'category', N'Category'
exec insertPoGridconfig 2, 'address', N'Address'
exec insertPoGridconfig 2, 'phone1', N'Phone', @_dcr
exec insertPoGridconfig 2, 'eaddr', N'Email', @_dcr
exec insertPoGridconfig 2, 'cr_status_verification', N'Status verification', @_dcr
exec insertPoGridconfig 2, 'get_verification', N'Status change date', @_dcr
exec insertPoGridconfig 2, 'modifier_user_name', N'Request initiator', @_dcr
exec insertPoGridconfig 2, 'is_archive', N'Status', 0
exec insertPoGridconfig 2, 'phone2', N'Phone (add.)', 0
exec insertPoGridconfig 2, 'contacttype', N'Type contact', 0
exec insertPoGridconfig 2, 'contactspec', N'Specialization (add.)', 0
exec insertPoGridconfig 2, 'position', N'Position', 0
exec insertPoGridconfig 2, 'postcode', N'Postcode', 0
exec insertPoGridconfig 2, 'owner', N'Responsible', 0
exec insertPoGridconfig 2, 'contactowner', N'Responsible (add.)', 0
exec insertPoGridconfig 2, 'legal_name', N'Legal name', 0
exec insertPoGridconfig 2, 'reg_address', N'Registration address', 0
exec insertPoGridconfig 2, 'insurance_certificate', N'Insurance certificate', 0
exec insertPoGridconfig 2, 'identification_number', N'Taxpayer identification number', 0
exec insertPoGridconfig 2, 'bank', N'Bank', 0
exec insertPoGridconfig 2, 'bic', N'Bank identification code', 0
exec insertPoGridconfig 2, 'correspondent_account', N'Correspondent account', 0
exec insertPoGridconfig 2, 'snils', N'SNILS', 0
exec insertPoGridconfig 2, 'kpp', N'KPP', 0
exec insertPoGridconfig 2, 'legal_status', N'Legal status', 0
exec insertPoGridconfig 2, 'interaction', N'Interaction', 0
exec insertPoGridconfig 2, 'passport_seriesnumber', N'Passport series', 0
exec insertPoGridconfig 2, 'passport_date', N'Passport when issued', 0
exec insertPoGridconfig 2, 'passport_issued', N'Passport issued by', 0
exec insertPoGridconfig 2, 'city', N'City', 0
exec insertPoGridconfig 2, 'region', N'Region', 0
GO

ALTER TABLE info_CustomDictionary ALTER COLUMN code VARCHAR (100);
GO

EXEC insertLanguageInterface 'en', 'Last name', 'Last name', 'po_grid_config';
EXEC insertLanguageInterface 'ru', 'Last name', N'Фамилия', 'po_grid_config';
EXEC insertLanguageInterface 'uk', 'Last name', N'Прізвище', 'po_grid_config'

EXEC insertLanguageInterface 'en', 'Company', 'Company', 'po_grid_config';
EXEC insertLanguageInterface 'ru', 'Company', N'Компания', 'po_grid_config';
EXEC insertLanguageInterface 'uk', 'Company', N'Компанія', 'po_grid_config';

EXEC insertLanguageInterface 'en', 'Specialization', 'Company', 'po_grid_config';
EXEC insertLanguageInterface 'ru', 'Specialization', N'Специализация', 'po_grid_config';
EXEC insertLanguageInterface 'uk', 'Specialization', N'Спеціалізація', 'po_grid_config';

EXEC insertLanguageInterface 'en', 'First name', 'First name', 'po_grid_config';
EXEC insertLanguageInterface 'ru', 'First name', N'Имя', 'po_grid_config';
EXEC insertLanguageInterface 'uk', 'First name', N'Ім''я', 'po_grid_config';

EXEC insertLanguageInterface 'en', 'Middle name', 'Middle name', 'po_grid_config';
EXEC insertLanguageInterface 'ru', 'Middle name', N'Отчество', 'po_grid_config';
EXEC insertLanguageInterface 'uk', 'Middle name', N'По батькові', 'po_grid_config';

EXEC insertLanguageInterface 'en', 'Address', 'Address', 'po_grid_config';
EXEC insertLanguageInterface 'ru', 'Address', N'Адрес', 'po_grid_config';
EXEC insertLanguageInterface 'uk', 'Address', N'Адреса', 'po_grid_config';

EXEC insertLanguageInterface 'en', 'Phone', 'Phone', 'po_grid_config';
EXEC insertLanguageInterface 'ru', 'Phone', N'Телефон', 'po_grid_config';
EXEC insertLanguageInterface 'uk', 'Phone', N'Телефон', 'po_grid_config';

EXEC insertLanguageInterface 'en', 'Phone (add.)', 'Phone (add.)', 'po_grid_config';
EXEC insertLanguageInterface 'ru', 'Phone (add.)', N'Телефон (доп.)', 'po_grid_config';
EXEC insertLanguageInterface 'uk', 'Phone (add.)', N'Телефон (дод.)', 'po_grid_config';

EXEC insertLanguageInterface 'en', 'Email', 'Email', 'po_grid_config';
EXEC insertLanguageInterface 'ru', 'Email', N'Email', 'po_grid_config';
EXEC insertLanguageInterface 'uk', 'Email', N'Email', 'po_grid_config';

EXEC insertLanguageInterface 'en', 'Request initiator', 'Request initiator', 'po_grid_config';
EXEC insertLanguageInterface 'ru', 'Request initiator', N'Инициатор запроса', 'po_grid_config';
EXEC insertLanguageInterface 'uk', 'Request initiator', N'Ініціатор запиту', 'po_grid_config';

EXEC insertLanguageInterface 'en', 'Category', 'Category', 'po_grid_config';
EXEC insertLanguageInterface 'ru', 'Category', N'Категория', 'po_grid_config';
EXEC insertLanguageInterface 'uk', 'Category', N'Категорія', 'po_grid_config';

EXEC insertLanguageInterface 'en', 'Type contact', 'Type contact', 'po_grid_config';
EXEC insertLanguageInterface 'ru', 'Type contact', N'Тип контакта', 'po_grid_config';
EXEC insertLanguageInterface 'uk', 'Type contact', N'Тип контакту', 'po_grid_config';

EXEC insertLanguageInterface 'en', 'Specialization (add.)', 'Specialization (add.)', 'po_grid_config';
EXEC insertLanguageInterface 'ru', 'Specialization (add.)', N'Специализация (доп.)', 'po_grid_config';
EXEC insertLanguageInterface 'uk', 'Specialization (add.)', N'Спеціалізація (дод.)', 'po_grid_config';

EXEC insertLanguageInterface 'en', 'Position', 'Position', 'po_grid_config';
EXEC insertLanguageInterface 'ru', 'Position', N'Должность', 'po_grid_config';
EXEC insertLanguageInterface 'uk', 'Position', N'Посада', 'po_grid_config';

EXEC insertLanguageInterface 'en', 'Postcode', 'Postcode', 'po_grid_config';
EXEC insertLanguageInterface 'ru', 'Postcode', N'Почтовый индекс', 'po_grid_config';
EXEC insertLanguageInterface 'uk', 'Postcode', N'Поштовий індекс', 'po_grid_config';

EXEC insertLanguageInterface 'en', 'Responsible', 'Responsible', 'po_grid_config';
EXEC insertLanguageInterface 'ru', 'Responsible', N'Ответственный', 'po_grid_config';
EXEC insertLanguageInterface 'uk', 'Responsible', N'Відповідальний', 'po_grid_config';

EXEC insertLanguageInterface 'en', 'Responsible (add.)', 'Responsible (add.)', 'po_grid_config';
EXEC insertLanguageInterface 'ru', 'Responsible (add.)', N'Ответственный (доп.)', 'po_grid_config';
EXEC insertLanguageInterface 'uk', 'Responsible (add.)', N'Відповідальний (дод.)', 'po_grid_config';

EXEC insertLanguageInterface 'en', 'Bank identification code', 'Bank identification code', 'po_grid_config';
EXEC insertLanguageInterface 'ru', 'Bank identification code', N'Идентификационный код банка', 'po_grid_config';
EXEC insertLanguageInterface 'uk', 'Bank identification code', N'Ідентифікаційний код банку', 'po_grid_config';

EXEC insertLanguageInterface 'en', 'Passport series', 'Passport series', 'po_grid_config';
EXEC insertLanguageInterface 'ru', 'Passport series', N'Серия паспорта', 'po_grid_config';
EXEC insertLanguageInterface 'uk', 'Passport series', N'Серія паспорту', 'po_grid_config';

EXEC insertLanguageInterface 'en', 'Passport when issued', 'Passport when issued', 'po_grid_config';
EXEC insertLanguageInterface 'ru', 'Passport when issued', N'Паспорт при выдаче', 'po_grid_config';
EXEC insertLanguageInterface 'uk', 'Passport when issued', N'Паспорт при видачі', 'po_grid_config';

EXEC insertLanguageInterface 'en', 'Passport issued by', 'Passport issued by', 'po_grid_config';
EXEC insertLanguageInterface 'ru', 'Passport issued by', N'Кем выдан паспорт', 'po_grid_config';
EXEC insertLanguageInterface 'uk', 'Passport issued by', N'Паспорт виданий', 'po_grid_config';

EXEC insertLanguageInterface 'en', 'City', 'City', 'po_grid_config';
EXEC insertLanguageInterface 'ru', 'City', N'Город', 'po_grid_config';
EXEC insertLanguageInterface 'uk', 'City', N'Місто', 'po_grid_config';

EXEC insertLanguageInterface 'en', 'Region', 'Region', 'po_grid_config';
EXEC insertLanguageInterface 'ru', 'Region', N'Регион', 'po_grid_config';
EXEC insertLanguageInterface 'uk', 'Region', N'Регіон', 'po_grid_config';

EXEC insertLanguageInterface 'en', 'Last task date', 'Last task date', 'po_grid_config';
EXEC insertLanguageInterface 'ru', 'Last task date', N'Дата последнего задания', 'po_grid_config';
EXEC insertLanguageInterface 'uk', 'Last task date', N'Дата останнього завдання', 'po_grid_config';

EXEC insertLanguageInterface 'en', 'Code', 'Code', 'po_grid_config';
EXEC insertLanguageInterface 'ru', 'Code', N'Код', 'po_grid_config';
EXEC insertLanguageInterface 'uk', 'Code', N'Код', 'po_grid_config';

EXEC insertLanguageInterface 'en', 'Postcode', 'Postcode', 'po_grid_config';
EXEC insertLanguageInterface 'ru', 'Postcode', N'Почтовый индекс', 'po_grid_config';
EXEC insertLanguageInterface 'uk', 'Postcode', N'Поштовий індекс', 'po_grid_config';

EXEC insertLanguageInterface 'en', 'Site', 'Site', 'po_grid_config';
EXEC insertLanguageInterface 'ru', 'Site', N'Сайт', 'po_grid_config';
EXEC insertLanguageInterface 'uk', 'Site', N'Сайт', 'po_grid_config';

EXEC insertLanguageInterface 'en', 'Owner', 'Owner', 'po_grid_config';
EXEC insertLanguageInterface 'ru', 'Owner', N'Владелец', 'po_grid_config';
EXEC insertLanguageInterface 'uk', 'Owner', N'Власник', 'po_grid_config';

EXEC insertLanguageInterface 'en', 'Grid settings', 'Grid settings', 'crm';
EXEC insertLanguageInterface 'ru', 'Grid settings', N'Настройки сетки', 'crm';
EXEC insertLanguageInterface 'uk', 'Grid settings', N'Налаштування сітки', 'crm';


EXEC insertLanguageInterface 'en', 'Field name', 'Field name', 'crm';
EXEC insertLanguageInterface 'ru', 'Field name', N'Имя поля', 'crm';
EXEC insertLanguageInterface 'uk', 'Field name', N'Назва поля', 'crm';

exec insertLanguageInterface 'en', 'Table configuration settings', 'Table configuration settings', 'crm';
exec insertLanguageInterface 'ru', 'Table configuration settings', N'Настройки конфигурации таблицы', 'crm';
exec insertLanguageInterface 'uk', 'Table configuration settings', N'Налаштування конфігурації таблиці', 'crm';
GO

EXEC insertLanguageInterface 'en', 'Status', 'Status', 'messages';
EXEC insertLanguageInterface 'ru', 'Status', N'Статус', 'messages';
EXEC insertLanguageInterface 'uk', 'Status', N'Статус', 'messages';

EXEC insertLanguageInterface 'en', 'Type', 'Type', 'messages';
EXEC insertLanguageInterface 'ru', 'Type', N'Тип', 'messages';
EXEC insertLanguageInterface 'uk', 'Type', N'Тип', 'messages';

EXEC insertLanguageInterface 'en', 'Status change date', 'Status change date', 'messages';
EXEC insertLanguageInterface 'ru', 'Status change date', N'Дата изменения статуса', 'messages';
EXEC insertLanguageInterface 'uk', 'Status change date', N'Дата зміни статусу', 'messages';

EXEC insertLanguageInterface 'en', 'Columns (web)', 'Columns (web)', 'messages';
EXEC insertLanguageInterface 'ru', 'Columns (web)', N'Столбцы (web)', 'messages';
EXEC insertLanguageInterface 'uk', 'Columns (web)', N'Стовпці (web)', 'messages';

EXEC insertLanguageInterface 'en', 'Columns (Web) type', 'For page', 'messages';
EXEC insertLanguageInterface 'ru', 'Columns (Web) type', N'Для страницы', 'messages';
EXEC insertLanguageInterface 'uk', 'Columns (Web) type', N'Для сторінки', 'messages';

EXEC insertLanguageInterface 'en', 'Copy settings', 'Copy settings', 'messages';
EXEC insertLanguageInterface 'ru', 'Copy settings', N'Копировать настройки', 'messages';
EXEC insertLanguageInterface 'uk', 'Copy settings', N'Копіювання налаштування', 'messages';

EXEC insertLanguageInterface 'en', 'GRID_COPY_SETTINGS', 'Copy grid settings from role "{{role}}" for "{{page}}" page', 'messages';
EXEC insertLanguageInterface 'ru', 'GRID_COPY_SETTINGS', N'Скопировать настройки grid из роли "{{role}}" для страницы "{{page}}"', 'messages';
EXEC insertLanguageInterface 'uk', 'GRID_COPY_SETTINGS', N'Скопіюйте налаштування grid з ролі "{{role}}" для сторінки "{{page}}"', 'messages';

EXEC insertLanguageInterface 'en', 'Select role', 'Select role', 'messages';
EXEC insertLanguageInterface 'ru', 'Select role', N'Выберите роль', 'messages';
EXEC insertLanguageInterface 'uk', 'Select role', N'Виберіть роль', 'messages';

EXEC insertLanguageInterface 'en', 'Field name', 'Field name', 'messages';
EXEC insertLanguageInterface 'ru', 'Field name', N'Имя поля', 'messages';
EXEC insertLanguageInterface 'uk', 'Field name', N'Назва поля', 'messages';

EXEC insertLanguageInterface 'en', 'Field code', 'Field code', 'messages';
EXEC insertLanguageInterface 'ru', 'Field code', N'Код поля', 'messages';
EXEC insertLanguageInterface 'uk', 'Field code', N'Код поля', 'messages';

EXEC insertLanguageInterface 'en', 'Active', 'Active', 'messages';
EXEC insertLanguageInterface 'ru', 'Active', N'Активный', 'messages';
EXEC insertLanguageInterface 'uk', 'Active', N'Активний', 'messages';

EXEC insertLanguageInterface 'en', 'Sortable', 'Sortable', 'messages';
EXEC insertLanguageInterface 'ru', 'Sortable', N'Сортируемый', 'messages';
EXEC insertLanguageInterface 'uk', 'Sortable', N'Сортується', 'messages';

EXEC insertLanguageInterface 'en', 'Off', 'Off', 'messages';
EXEC insertLanguageInterface 'ru', 'Off', N'Выключен', 'messages';
EXEC insertLanguageInterface 'uk', 'Off', N'Вимкнено', 'messages';

EXEC insertLanguageInterface 'en', 'On', 'On', 'messages';
EXEC insertLanguageInterface 'ru', 'On', N'Включен', 'messages';
EXEC insertLanguageInterface 'uk', 'On', N'Увімкнено', 'messages';

EXEC insertLanguageInterface 'en', 'Fail update model. Click for try update model again', 'Fail update model. Click for try update model again', 'messages';
EXEC insertLanguageInterface 'ru', 'Fail update model. Click for try update model again', N'Неудачная модель обновления. Нажмите, чтобы повторить попытку обновления модели', 'messages';
EXEC insertLanguageInterface 'uk', 'Fail update model. Click for try update model again', N'Не вдалося оновити модель. Натисніть, щоб спробувати оновити модель ще раз', 'messages';
GO

EXEC insertLanguageInterface 'en', 'Unexpected error, please try again later. If the error occurred again contact to support.', 'Unexpected error, please try again later. If the error occurred again contact to support.', 'crm';
EXEC insertLanguageInterface 'ru', 'Unexpected error, please try again later. If the error occurred again contact to support.', N'Непредвиденная ошибка. Повторите попытку позже. Если ошибка возникла снова, обратитесь в службу поддержки.', 'crm';
EXEC insertLanguageInterface 'uk', 'Unexpected error, please try again later. If the error occurred again contact to support.', N'Несподівана помилка. Повторіть спробу пізніше. Якщо помилка повторилася, зверніться до служби підтримки.', 'crm';
GO
------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------
--fix info_user
------------------------------------------------------------------------------------------------------------------------
IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'info_user' AND COLUMN_NAME = 'isKAM')
ALTER TABLE info_user ADD isKAM integer null
GO
------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------

------------------------------------------------------------------------------------------------------------------------
---https://teamsoft.atlassian.net/browse/REDDYSRSA-219
------------------------------------------------------------------------------------------------------------------------
IF NOT EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE table_name = 'info_promoproject' AND column_name = 'isarchive')
alter table info_promoproject add isarchive int null;
GO
------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------

-------------------------------------------------------------------------------------------------------------------------------------------------------------------
------- https://teamsoft.atlassian.net/browse/EGISRU-190
-------------------------------------------------------------------------------------------------------------------------------------------------------------------
exec insertLanguageInterface 'en', 'At least one field in each block must be filled', 'At least one field in each block must be filled', 'crm'
exec insertLanguageInterface 'ru', 'At least one field in each block must be filled', 'Как минимум одно поле в каждом блоке должно быть заполнено', 'crm'
exec insertLanguageInterface 'uk', 'At least one field in each block must be filled', 'Принаймні одне поле в кожному блоці має бути заповнене', 'crm'

exec insertLanguageInterface 'en', 'Saved', N'Saved', 'crm'
exec insertLanguageInterface 'ru', 'Saved', N'Сохранено', 'crm'
exec insertLanguageInterface 'uk', 'Saved', N'Збережено', 'crm'
GO
-------------------------------------------------------------------------------------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------
---https://teamsoft.atlassian.net/browse/LOREALRU-70
------------------------------------------------------------------------------------------------------------------------
IF NOT EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE table_name = 'info_country' AND column_name = 'crm_phonemask')
alter table info_country add crm_phonemask varchar(255) null;
GO
------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------------------------
-- https://teamsoft.atlassian.net/browse/SINTEZ-87
---------------------------------------------------------------------------------------------------------------------
IF NOT EXISTS (SELECT 1 FROM information_schema.columns WHERE table_name = 'info_mcmjson' and column_name = 'contact_id')
BEGIN
ALTER TABLE info_mcmjson ADD contact_id int;
ALTER TABLE info_mcmjson ADD CONSTRAINT FK_info_mcmjson_contact_id FOREIGN KEY(contact_id) REFERENCES info_contact (id);
END;
GO
---------------------------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------------------------
