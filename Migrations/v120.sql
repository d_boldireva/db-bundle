----------------------------------------------------------------------------------------------------------
-- Symfony to 5
----------------------------------------------------------------------------------------------------------
IF OBJECT_ID(N'[po_refresh_token]', 'U') IS NOT NULL
    BEGIN
        DROP TABLE [dbo].[po_refresh_token]
    END
GO

IF OBJECT_ID(N'[po_auth_code]', 'U') IS NOT NULL
    BEGIN
        DROP TABLE [dbo].[po_auth_code]
    END
GO

IF OBJECT_ID(N'[po_access_token]', 'U') IS NOT NULL
    BEGIN
        DROP TABLE [dbo].[po_access_token]
    END
GO

IF OBJECT_ID(N'[po_client]', 'U') IS NOT NULL
    BEGIN
        DROP TABLE [dbo].[po_client]
    END
GO
----------------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------
---https://teamsoft.atlassian.net/browse/DRREDDYSKZ-100
------------------------------------------------------------------------------------------------------------------------
exec insertLanguageInterface  'en', 'Not sent', N'Not sent', 'messages'
exec insertLanguageInterface  'ru', 'Not sent', N'Не отправлено', 'messages'
exec insertLanguageInterface  'uk', 'Not sent', N'Не відправлено', 'messages'
GO

exec insertLanguageInterface  'uk', 'RESENDING_SMS_DELIVERED', N'Перевідправка смс - доставлено', 'messages'
exec insertLanguageInterface  'uk', 'RESENDING_SMS_SENT', N'Перевідправка смс - надіслано', 'messages'
exec insertLanguageInterface  'uk', 'RESENDING_SMS_VISITED', N'Перевідправка смс - відвідано', 'messages'
GO
------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------
---https://teamsoft.atlassian.net/browse/MCM-353
------------------------------------------------------------------------------------------------------------------------
IF NOT EXISTS (SELECT 1 FROM information_schema.columns WHERE table_name = 'info_mcmdistributionaction' AND column_name = 'price')
    BEGIN
        ALTER TABLE info_mcmdistributionaction ADD price float NULL
    END
GO
------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------

-----------------------------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------------------------

------------------------------------------------------------------------------------------------------------------------
---https://teamsoft.atlassian.net/browse/MCM-336
------------------------------------------------------------------------------------------------------------------------
IF NOT EXISTS (SELECT 1 FROM information_schema.columns
               WHERE table_name = 'info_mcmdistribution' AND column_name = 'isarchive')
    BEGIN
        ALTER TABLE info_mcmdistribution ADD isarchive INT NULL
    END
GO

exec insertLanguageInterface 'en', 'Distribution has been successfully archived', N'Distribution has been successfully archived', 'messages'
exec insertLanguageInterface 'ru', 'Distribution has been successfully archived', N'Рассылка была успешно заархивирована', 'messages'
exec insertLanguageInterface 'uk', 'Distribution has been successfully archived', N'Розсилка була успішно заархівована', 'messages'
GO
exec insertLanguageInterface 'en', 'Content has been successfully unarchived', N'Content has been successfully unarchived', 'messages'
exec insertLanguageInterface 'ru', 'Content has been successfully unarchived', N'Контент был успешно разархивирован', 'messages'
exec insertLanguageInterface 'uk', 'Content has been successfully unarchived', N'Контент був успішно розархівований', 'messages'
GO
exec insertLanguageInterface 'en', 'Distribution has been successfully unarchived', N'Distribution has been successfully unarchived', 'messages'
exec insertLanguageInterface 'ru', 'Distribution has been successfully unarchived', N'Рассылка была успешно разархивирована', 'messages'
exec insertLanguageInterface 'uk', 'Distribution has been successfully unarchived', N'Розсилка була успішно розархівована', 'messages'
GO
exec insertLanguageInterface 'en', 'Distribution was not archived', N'Distribution was not archived', 'messages'
exec insertLanguageInterface 'ru', 'Distribution was not archived', N'Рассылка не была заархивирована', 'messages'
exec insertLanguageInterface 'uk', 'Distribution was not archived', N'Розсилка не була заархівована', 'messages'
GO
exec insertLanguageInterface 'en', 'Distribution was not unarchived', N'Distribution was not unarchived', 'messages'
exec insertLanguageInterface 'ru', 'Distribution was not unarchived', N'Рассылка не была разархивирована', 'messages'
exec insertLanguageInterface 'uk', 'Distribution was not unarchived', N'Розсилка не була розархівована', 'messages'
GO
exec insertLanguageInterface 'en', 'Content was not unarchived', N'Content was not unarchived', 'messages'
exec insertLanguageInterface 'ru', 'Content was not unarchived', N'Контент не был разархивирован', 'messages'
exec insertLanguageInterface 'uk', 'Content was not unarchived', N'Контент не був розархівований', 'messages'
GO
------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------

------------------------------------------------------------------------------------------------------------------------
---https://teamsoft.atlassian.net/browse/LOREALRU-14
------------------------------------------------------------------------------------------------------------------------
declare @ROLE_SUPER_ADMIN_ID int;
set @ROLE_SUPER_ADMIN_ID = (SELECT id FROM info_role WHERE code = 'ROLE_SUPER_ADMIN');

declare @ROLE_ADMIN_ID int;
set @ROLE_ADMIN_ID = (SELECT id FROM info_role WHERE code = 'ROLE_ADMIN');

declare @ROLE_USER_ID int;
set @ROLE_USER_ID = (SELECT id FROM info_role WHERE code = 'ROLE_USER');

INSERT INTO info_tabcontroldictionary (parent_type, type, parent_type_name, type_name)
SELECT 10, 403, 'Клиенты', 'Запросы'
WHERE NOT EXISTS(SELECT 1 FROM info_tabcontroldictionary WHERE parent_type = 10 AND type = 403);

INSERT INTO info_tabcontroldictionary (parent_type, type, parent_type_name, type_name)
SELECT 11, 403, 'Учреждения', 'Запросы'
WHERE NOT EXISTS(SELECT 1 FROM info_tabcontroldictionary WHERE parent_type = 11 AND type = 403);


IF NOT EXISTS(SELECT 1 FROM info_tabcontrol WHERE parrenttype = 11 AND type = 27 and role_id = @ROLE_SUPER_ADMIN_ID)
    INSERT INTO info_tabcontrol(parrenttype, type, name, isshow, role_id)
    VALUES (11, 27, 'Ответственные', 1, @ROLE_SUPER_ADMIN_ID);

IF NOT EXISTS(SELECT 1 FROM info_tabcontrol WHERE parrenttype = 11 AND type = 27 and role_id = @ROLE_ADMIN_ID)
    INSERT INTO info_tabcontrol(parrenttype, type, name, isshow, role_id)
    VALUES (11, 27, 'Ответственные', 1, @ROLE_ADMIN_ID);

IF NOT EXISTS(SELECT 1 FROM info_tabcontrol WHERE parrenttype = 11 AND type = 25 and role_id = @ROLE_USER_ID)
    INSERT INTO info_tabcontrol(parrenttype, type, name, isshow, role_id)
    VALUES (11, 27, 'Ответственные', 1, @ROLE_USER_ID);

IF NOT EXISTS(SELECT 1 FROM info_tabcontrol WHERE parrenttype = 11 AND type = 25 and role_id = @ROLE_SUPER_ADMIN_ID)
    INSERT INTO info_tabcontrol(parrenttype, type, name, isshow, role_id)
    VALUES (11, 25, 'История взаимоотношений', 1, @ROLE_SUPER_ADMIN_ID);

IF NOT EXISTS(SELECT 1 FROM info_tabcontrol WHERE parrenttype = 11 AND type = 25 and role_id = @ROLE_ADMIN_ID)
    INSERT INTO info_tabcontrol(parrenttype, type, name, isshow, role_id)
    VALUES (11, 25, 'История взаимоотношений', 1, @ROLE_ADMIN_ID);

IF NOT EXISTS(SELECT 1 FROM info_tabcontrol WHERE parrenttype = 11 AND type = 25 and role_id = @ROLE_USER_ID)
    INSERT INTO info_tabcontrol(parrenttype, type, name, isshow, role_id)
    VALUES (11, 25, 'История взаимоотношений', 1, @ROLE_USER_ID);

IF NOT EXISTS(SELECT 1 FROM info_tabcontrol WHERE parrenttype = 10 AND type = 27 and role_id = @ROLE_SUPER_ADMIN_ID)
    INSERT INTO info_tabcontrol(parrenttype, type, name, isshow, role_id)
    VALUES (10, 27, 'Ответственные', 1, @ROLE_SUPER_ADMIN_ID);

IF NOT EXISTS(SELECT 1 FROM info_tabcontrol WHERE parrenttype = 10 AND type = 27 and role_id = @ROLE_ADMIN_ID)
    INSERT INTO info_tabcontrol(parrenttype, type, name, isshow, role_id)
    VALUES (10, 27, 'Ответственные', 1, @ROLE_ADMIN_ID);

IF NOT EXISTS(SELECT 1 FROM info_tabcontrol WHERE parrenttype = 10 AND type = 27 and role_id = @ROLE_USER_ID)
    INSERT INTO info_tabcontrol(parrenttype, type, name, isshow, role_id)
    VALUES (10, 27, 'Ответственные', 1, @ROLE_USER_ID);

IF NOT EXISTS(SELECT 1 FROM info_tabcontrol WHERE parrenttype = 10 AND type = 25 and role_id = @ROLE_SUPER_ADMIN_ID)
    INSERT INTO info_tabcontrol(parrenttype, type, name, isshow, role_id)
    VALUES (10, 25, 'История взаимоотношений', 1, @ROLE_SUPER_ADMIN_ID);

IF NOT EXISTS(SELECT 1 FROM info_tabcontrol WHERE parrenttype = 10 AND type = 25 and role_id = @ROLE_ADMIN_ID)
    INSERT INTO info_tabcontrol(parrenttype, type, name, isshow, role_id)
    VALUES (10, 25, 'История взаимоотношений', 1, @ROLE_ADMIN_ID);

IF NOT EXISTS(SELECT 1 FROM info_tabcontrol WHERE parrenttype = 10 AND type = 25 and role_id = @ROLE_USER_ID)
    INSERT INTO info_tabcontrol(parrenttype, type, name, isshow, role_id)
    VALUES (10, 25, 'История взаимоотношений', 1, @ROLE_USER_ID);

IF NOT EXISTS(SELECT 1 FROM info_tabcontrol WHERE parrenttype = 11 AND type = 114 and role_id = @ROLE_SUPER_ADMIN_ID)
    INSERT INTO info_tabcontrol(parrenttype, type, name, isshow, role_id)
    VALUES (11, 114, 'Сотрудники', 1, @ROLE_SUPER_ADMIN_ID);

IF NOT EXISTS(SELECT 1 FROM info_tabcontrol WHERE parrenttype = 11 AND type = 114 and role_id = @ROLE_ADMIN_ID)
    INSERT INTO info_tabcontrol(parrenttype, type, name, isshow, role_id)
    VALUES (11, 114, 'Сотрудники', 1, @ROLE_ADMIN_ID);

IF NOT EXISTS(SELECT 1 FROM info_tabcontrol WHERE parrenttype = 11 AND type = 114 and role_id = @ROLE_USER_ID)
    INSERT INTO info_tabcontrol(parrenttype, type, name, isshow, role_id)
    VALUES (11, 114, 'Сотрудники', 1, @ROLE_USER_ID);

GO

------------------------------------------------------------------------------------------------------------------------
--https://teamsoft.atlassian.net/browse/PHARMAWEB-354
------------------------------------------------------------------------------------------------------------------------
exec insertLanguageInterface 'en', 'Yes', 'Yes', 'crm'
exec insertLanguageInterface 'ru', 'Yes', N'Да', 'crm'
exec insertLanguageInterface 'uk', 'Yes', N'Так', 'crm'
GO

exec insertLanguageInterface 'en', 'FIELD_TO_LONG_WITH_MAX_LENGTH', 'Field is too long. Max length is [maxLength] characters.', 'crm'
exec insertLanguageInterface 'ru', 'FIELD_TO_LONG_WITH_MAX_LENGTH', N'Поле слишком длинное. Максимальная длина - [maxLength] символов.', 'crm'
exec insertLanguageInterface 'uk', 'FIELD_TO_LONG_WITH_MAX_LENGTH', N'Поле занадто довге. Максимальна довжина - [maxLength] символів.', 'crm'
GO

exec insertLanguageInterface 'en', 'Clear', 'Clear', 'crm'
exec insertLanguageInterface 'ru', 'Clear', N'Очистить', 'crm'
exec insertLanguageInterface 'uk', 'Clear', N'Очистити', 'crm'
GO

exec insertLanguageInterface 'en', 'Please, select a brand first!', 'Please, select a brand first!', 'crm'
exec insertLanguageInterface 'ru', 'Please, select a brand first!', N'Пожалуйста, сначала выберите бренд!', 'crm'
exec insertLanguageInterface 'uk', 'Please, select a brand first!', N'Будь ласка, спочатку оберіть бренд!', 'crm'
GO

exec insertLanguageInterface 'en', 'One or more brands must be set as promotion.', 'One or more brands must be set as promotion.', 'crm'
exec insertLanguageInterface 'ru', 'One or more brands must be set as promotion.', N'Один или несколько брендов должны быть указаны как промоция.', 'crm'
exec insertLanguageInterface 'uk', 'One or more brands must be set as promotion.', N'Одна або кілька брендів повинні бути встановлені як промоція.', 'crm'
GO


------------------------------------------------------------------------------------------------------------------------
---https://teamsoft.atlassian.net/browse/DRREDDYSMM-45
------------------------------------------------------------------------------------------------------------------------
IF NOT EXISTS (SELECT 1 FROM information_schema.columns
               WHERE table_name = 'info_contactphone' AND column_name = 'country_phone_mask')
    BEGIN
        ALTER TABLE info_contactphone ADD country_phone_mask INT NULL
    END
GO
------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------


------------------------------------------------------------------------------------------------------------------------
---https://teamsoft.atlassian.net/browse/MCM-299
------------------------------------------------------------------------------------------------------------------------
exec insertLanguageInterface 'en', 'Drag and drop files here or click to upload', N'Drag and drop files here or click to upload', 'mcm'
exec insertLanguageInterface 'uk', 'Drag and drop files here or click to upload', N'Перетягніть сюди файли або натисніть, щоб завантажити', 'mcm'
exec insertLanguageInterface 'ru', 'Drag and drop files here or click to upload', N'Перетащите нужные файлы сюда или нажмите, чтобы загрузить', 'mcm'
GO

exec insertLanguageInterface 'en', 'File(s) uploaded successfully', N'File(s) uploaded successfully', 'mcm'
exec insertLanguageInterface 'uk', 'File(s) uploaded successfully', N'Файл(и) успішно завантажені', 'mcm'
exec insertLanguageInterface 'ru', 'File(s) uploaded successfully', N'Файл(ы) успешно загружены', 'mcm'
GO

exec insertLanguageInterface 'en', 'Something went wrong', N'Something went wrong', 'mcm'
exec insertLanguageInterface 'uk', 'Something went wrong', N'Щось пішло не так', 'mcm'
exec insertLanguageInterface 'ru', 'Something went wrong', N'Что-то пошло не так', 'mcm'
GO

exec insertLanguageInterface 'en', 'In progress', N'In progress', 'mcm'
exec insertLanguageInterface 'uk', 'In progress', N'Обробка', 'mcm'
exec insertLanguageInterface 'ru', 'In progress', N'Обработка', 'mcm'
GO

exec insertLanguageInterface 'en', 'The maximum file size should not exceed 12 MB', N'The maximum file size should not exceed 12 MB', 'mcm'
exec insertLanguageInterface 'uk', 'The maximum file size should not exceed 12 MB', N'Максимальний розмір файлу не має перевищувати 12 MB', 'mcm'
exec insertLanguageInterface 'ru', 'The maximum file size should not exceed 12 MB', N'Максимальный размер файла не должен превышать 12 MB', 'mcm'
GO

exec insertLanguageInterface 'en', 'Personalization', N'Personalization', 'mcm'
exec insertLanguageInterface 'uk', 'Personalization', N'Персоралізація', 'mcm'
exec insertLanguageInterface 'ru', 'Personalization', N'Персонализация', 'mcm'
GO

exec insertLanguageInterface 'en', 'No images yet', N'No images yet', 'mcm'
exec insertLanguageInterface 'uk', 'No images yet', N'Поки нема зображень', 'mcm'
exec insertLanguageInterface 'ru', 'No images yet', N'Пока нет изображений', 'mcm'
GO

exec insertLanguageInterface 'en', 'Paste all your code here below and click import', N'Paste all your code here below and click import', 'mcm'
exec insertLanguageInterface 'uk', 'Paste all your code here below and click import', N'Скопіюйте сюди ваш код і натисніть імпорт', 'mcm'
exec insertLanguageInterface 'ru', 'Paste all your code here below and click import', N'Скопируйте ваш код сюда и нажмите импорт', 'mcm'
GO

exec insertLanguageInterface 'en', 'Copy the code and use it wherever you want', N'Copy the code and use it wherever you want', 'mcm'
exec insertLanguageInterface 'uk', 'Copy the code and use it wherever you want', N'Скопіюйте вихідний код сторінки', 'mcm'
exec insertLanguageInterface 'ru', 'Copy the code and use it wherever you want', N'Скопируйте исходный код страницы', 'mcm'
GO

exec insertLanguageInterface 'en', 'Button', N'Button', 'mcm'
exec insertLanguageInterface 'uk', 'Button', N'Кнопка', 'mcm'
exec insertLanguageInterface 'ru', 'Button', N'Кнопка', 'mcm'
GO

exec insertLanguageInterface 'en', 'Save', N'Save', 'mcm'
exec insertLanguageInterface 'uk', 'Save', N'Зберегти', 'mcm'
exec insertLanguageInterface 'ru', 'Save', N'Сохранить', 'mcm'
GO

exec insertLanguageInterface 'en', 'Edit code', N'Edit code', 'mcm'
exec insertLanguageInterface 'uk', 'Edit code', N'Редагувати код', 'mcm'
exec insertLanguageInterface 'ru', 'Edit code', N'Редактировать код', 'mcm'
GO

exec insertLanguageInterface 'en', 'Send test email', N'Send test email', 'mcm'
exec insertLanguageInterface 'uk', 'Send test email', N'Надіслати тестовий лист', 'mcm'
exec insertLanguageInterface 'ru', 'Send test email', N'Отправить тестовое письмо', 'mcm'
GO

exec insertLanguageInterface 'en', 'Sender', N'Sender', 'mcm'
exec insertLanguageInterface 'uk', 'Sender', N'Відправник', 'mcm'
exec insertLanguageInterface 'ru', 'Sender', N'Отправитель', 'mcm'
GO

exec insertLanguageInterface 'en', 'Receiver', N'Receiver', 'mcm'
exec insertLanguageInterface 'uk', 'Receiver', N'Отримувач', 'mcm'
exec insertLanguageInterface 'ru', 'Receiver', N'Получатель', 'mcm'
GO

exec insertLanguageInterface 'en', 'Send', N'Send', 'mcm'
exec insertLanguageInterface 'uk', 'Send', N'Надіслати', 'mcm'
exec insertLanguageInterface 'ru', 'Send', N'Отправить', 'mcm'
GO

exec insertLanguageInterface 'en', 'Delete content', N'Delete content', 'mcm'
exec insertLanguageInterface 'uk', 'Delete content', N'Очистити вміст', 'mcm'
exec insertLanguageInterface 'ru', 'Delete content', N'Очистить содержимое', 'mcm'
GO

exec insertLanguageInterface 'en', 'Attach file', N'Attach file', 'mcm'
exec insertLanguageInterface 'uk', 'Attach file', N'Прикріпити файл', 'mcm'
exec insertLanguageInterface 'ru', 'Attach file', N'Прикрепить файл', 'mcm'
GO

exec insertLanguageInterface 'en', 'Text message', N'Text message', 'mcm'
exec insertLanguageInterface 'uk', 'Text message', N'Текстове повідомлення', 'mcm'
exec insertLanguageInterface 'ru', 'Text message', N'Текстовое сообщение', 'mcm'
GO

exec insertLanguageInterface 'en', 'Close', N'Close', 'mcm'
exec insertLanguageInterface 'uk', 'Close', N'Закрити', 'mcm'
exec insertLanguageInterface 'ru', 'Close', N'Закрыть', 'mcm'
GO

exec insertLanguageInterface 'en', 'Old version', N'Old version', 'mcm'
exec insertLanguageInterface 'uk', 'Old version', N'Старий редактор', 'mcm'
exec insertLanguageInterface 'ru', 'Old version', N'Старый редактор', 'mcm'
GO

exec insertLanguageInterface 'en', 'Email title', N'Email title', 'mcm'
exec insertLanguageInterface 'uk', 'Email title', N'Назва email-розсилки', 'mcm'
exec insertLanguageInterface 'ru', 'Email title', N'Название email-рассылки', 'mcm'
GO

exec insertLanguageInterface 'en', 'Email subject', N'Email subject', 'mcm'
exec insertLanguageInterface 'uk', 'Email subject', N'Тема листа', 'mcm'
exec insertLanguageInterface 'ru', 'Email subject', N'Тема письма', 'mcm'
GO

exec insertLanguageInterface 'en', 'Archived', N'Archived', 'mcm'
exec insertLanguageInterface 'uk', 'Archived', N'Архівний', 'mcm'
exec insertLanguageInterface 'ru', 'Archived', N'Архивный', 'mcm'
GO

exec insertLanguageInterface 'en', 'sync', N'sync', 'mcm'
exec insertLanguageInterface 'uk', 'sync', N'завантаження', 'mcm'
exec insertLanguageInterface 'ru', 'sync', N'загрузка', 'mcm'
GO

exec insertLanguageInterface 'en', 'Select element', N'Select element', 'mcm'
exec insertLanguageInterface 'uk', 'Select element', N'Обрати елемент', 'mcm'
exec insertLanguageInterface 'ru', 'Select element', N'Выбрать элемент', 'mcm'
GO

exec insertLanguageInterface 'en', 'Element properties', N'Element properties', 'mcm'
exec insertLanguageInterface 'uk', 'Element properties', N'Властивості елементу', 'mcm'
exec insertLanguageInterface 'ru', 'Element properties', N'Свойства элемента', 'mcm'
GO

exec insertLanguageInterface 'en', 'Elements tree', N'Elements tree', 'mcm'
exec insertLanguageInterface 'uk', 'Elements tree', N'Дерево елементів', 'mcm'
exec insertLanguageInterface 'ru', 'Elements tree', N'Дерево элементов', 'mcm'
GO

exec insertLanguageInterface 'en', 'New element', N'New element', 'mcm'
exec insertLanguageInterface 'uk', 'New element', N'Новий едемент', 'mcm'
exec insertLanguageInterface 'ru', 'New element', N'Новый элемент', 'mcm'
GO

exec insertLanguageInterface 'en', 'Add image', N'Add image', 'grapesjs'
exec insertLanguageInterface 'uk', 'Add image', N'Додати зображення', 'grapesjs'
exec insertLanguageInterface 'ru', 'Add image', N'Добавить изображение', 'grapesjs'
GO

exec insertLanguageInterface 'en', 'Select image', N'Select image', 'grapesjs'
exec insertLanguageInterface 'uk', 'Select image', N'Обрати зображення', 'grapesjs'
exec insertLanguageInterface 'ru', 'Select image', N'Выбрать изображение', 'grapesjs'
GO

exec insertLanguageInterface 'en', 'Box', N'Box', 'grapesjs'
exec insertLanguageInterface 'uk', 'Box', N'Контейнер', 'grapesjs'
exec insertLanguageInterface 'ru', 'Box', N'Контейнер', 'grapesjs'
GO

exec insertLanguageInterface 'en', 'Body', N'Body', 'grapesjs'
exec insertLanguageInterface 'uk', 'Body', N'Тіло', 'grapesjs'
exec insertLanguageInterface 'ru', 'Body', N'Тело', 'grapesjs'
GO

exec insertLanguageInterface 'en', 'Text', N'Text', 'grapesjs'
exec insertLanguageInterface 'uk', 'Text', N'Текст', 'grapesjs'
exec insertLanguageInterface 'ru', 'Text', N'Текст', 'grapesjs'
GO

exec insertLanguageInterface 'en', 'Comment', N'Comment', 'grapesjs'
exec insertLanguageInterface 'uk', 'Comment', N'Коментар', 'grapesjs'
exec insertLanguageInterface 'ru', 'Comment', N'Коментарий', 'grapesjs'
GO

exec insertLanguageInterface 'en', 'Image', N'Image', 'grapesjs'
exec insertLanguageInterface 'uk', 'Image', N'Зображення', 'grapesjs'
exec insertLanguageInterface 'ru', 'Image', N'Изображение', 'grapesjs'
GO

exec insertLanguageInterface 'en', 'Video', N'Video', 'grapesjs'
exec insertLanguageInterface 'uk', 'Video', N'Відео', 'grapesjs'
exec insertLanguageInterface 'ru', 'Video', N'Видео', 'grapesjs'
GO

exec insertLanguageInterface 'en', 'Label', N'Label', 'grapesjs'
exec insertLanguageInterface 'uk', 'Label', N'Назва', 'grapesjs'
exec insertLanguageInterface 'ru', 'Label', N'Название', 'grapesjs'
GO

exec insertLanguageInterface 'en', 'Link', N'Link', 'grapesjs'
exec insertLanguageInterface 'uk', 'Link', N'Посилання', 'grapesjs'
exec insertLanguageInterface 'ru', 'Link', N'Ссылка', 'grapesjs'
GO

exec insertLanguageInterface 'en', 'Table foot', N'Table foot', 'grapesjs'
exec insertLanguageInterface 'uk', 'Table foot', N'Футер таблиці', 'grapesjs'
exec insertLanguageInterface 'ru', 'Table foot', N'Футер таблицы', 'grapesjs'
GO

exec insertLanguageInterface 'en', 'Map', N'Map', 'grapesjs'
exec insertLanguageInterface 'uk', 'Map', N'Мапа', 'grapesjs'
exec insertLanguageInterface 'ru', 'Map', N'Карта', 'grapesjs'
GO

exec insertLanguageInterface 'en', 'Table body', N'Table body', 'grapesjs'
exec insertLanguageInterface 'uk', 'Table body', N'Тіло таблиці', 'grapesjs'
exec insertLanguageInterface 'ru', 'Table body', N'Тело таблицы', 'grapesjs'
GO

exec insertLanguageInterface 'en', 'Table head', N'Table head', 'grapesjs'
exec insertLanguageInterface 'uk', 'Table head', N'Хедер таблиці', 'grapesjs'
exec insertLanguageInterface 'ru', 'Table head', N'Хедер таблицы', 'grapesjs'
GO

exec insertLanguageInterface 'en', 'Table', N'Table', 'grapesjs'
exec insertLanguageInterface 'uk', 'Table', N'Таблиця', 'grapesjs'
exec insertLanguageInterface 'ru', 'Table', N'Таблица', 'grapesjs'
GO

exec insertLanguageInterface 'en', 'Table row', N'Table row', 'grapesjs'
exec insertLanguageInterface 'uk', 'Table row', N'Ряд таблиці', 'grapesjs'
exec insertLanguageInterface 'ru', 'Table row', N'Ряд таблицы', 'grapesjs'
GO

exec insertLanguageInterface 'en', 'Table cell', N'Table cell', 'grapesjs'
exec insertLanguageInterface 'uk', 'Table cell', N'Клітина таблиці', 'grapesjs'
exec insertLanguageInterface 'ru', 'Table cell', N'Клетка таблицы', 'grapesjs'
GO

exec insertLanguageInterface 'en', 'Device', N'Device', 'grapesjs'
exec insertLanguageInterface 'uk', 'Device', N'Пристрій', 'grapesjs'
exec insertLanguageInterface 'ru', 'Device', N'Устройство', 'grapesjs'
GO

exec insertLanguageInterface 'en', 'Desktop', N'Desktop', 'grapesjs'
exec insertLanguageInterface 'uk', 'Desktop', N'Комп''ютер', 'grapesjs'
exec insertLanguageInterface 'ru', 'Desktop', N'Компъютер', 'grapesjs'
GO

exec insertLanguageInterface 'en', 'Tablet', N'Tablet', 'grapesjs'
exec insertLanguageInterface 'uk', 'Tablet', N'Планшет', 'grapesjs'
exec insertLanguageInterface 'ru', 'Tablet', N'Планшет', 'grapesjs'
GO

exec insertLanguageInterface 'en', 'Mobile landscape', N'Mobile landscape', 'grapesjs'
exec insertLanguageInterface 'uk', 'Mobile landscape', N'Телефон', 'grapesjs'
exec insertLanguageInterface 'ru', 'Mobile landscape', N'Телефон', 'grapesjs'
GO

exec insertLanguageInterface 'en', 'Mobile portrait', N'Mobile portrait', 'grapesjs'
exec insertLanguageInterface 'uk', 'Mobile portrait', N'Телефон (портрет)', 'grapesjs'
exec insertLanguageInterface 'ru', 'Mobile portrait', N'Телефон (портрет)', 'grapesjs'
GO

exec insertLanguageInterface 'en', 'Preview', N'Preview', 'grapesjs'
exec insertLanguageInterface 'uk', 'Preview', N'Попередній перегляд', 'grapesjs'
exec insertLanguageInterface 'ru', 'Preview', N'Предпросмотр', 'grapesjs'
GO

exec insertLanguageInterface 'en', 'Fullscreen', N'Fullscreen', 'grapesjs'
exec insertLanguageInterface 'uk', 'Fullscreen', N'Повний екран', 'grapesjs'
exec insertLanguageInterface 'ru', 'Fullscreen', N'Полный экран', 'grapesjs'
GO

exec insertLanguageInterface 'en', 'View components', N'View components', 'grapesjs'
exec insertLanguageInterface 'uk', 'View components', N'Переглянути компоненти', 'grapesjs'
exec insertLanguageInterface 'ru', 'View components', N'Просмотреть компоненты', 'grapesjs'
GO

exec insertLanguageInterface 'en', 'View code', N'View code', 'grapesjs'
exec insertLanguageInterface 'uk', 'View code', N'Переглянути код', 'grapesjs'
exec insertLanguageInterface 'ru', 'View code', N'Просмотреть код', 'grapesjs'
GO

exec insertLanguageInterface 'en', 'Open style manager', N'Open style manager', 'grapesjs'
exec insertLanguageInterface 'uk', 'Open style manager', N'Відкрити редактор стилів', 'grapesjs'
exec insertLanguageInterface 'ru', 'Open style manager', N'Открыть редактор стилей', 'grapesjs'
GO

exec insertLanguageInterface 'en', 'Settings', N'Settings', 'grapesjs'
exec insertLanguageInterface 'uk', 'Settings', N'Налаштування', 'grapesjs'
exec insertLanguageInterface 'ru', 'Settings', N'Настройки', 'grapesjs'
GO

exec insertLanguageInterface 'en', 'Open layer manager', N'Open layer manager', 'grapesjs'
exec insertLanguageInterface 'uk', 'Open layer manager', N'Відкрити редактор шарів', 'grapesjs'
exec insertLanguageInterface 'ru', 'Open layer manager', N'Открыть редактор слоев', 'grapesjs'
GO

exec insertLanguageInterface 'en', 'Open blocks', N'Open blocks', 'grapesjs'
exec insertLanguageInterface 'uk', 'Open blocks', N'Відкрити редактор шарів', 'grapesjs'
exec insertLanguageInterface 'ru', 'Open blocks', N'Открыть редактор слоев', 'grapesjs'
GO

exec insertLanguageInterface 'en', 'Classes', N'Classes', 'grapesjs'
exec insertLanguageInterface 'uk', 'Classes', N'Класи', 'grapesjs'
exec insertLanguageInterface 'ru', 'Classes', N'Классы', 'grapesjs'
GO

exec insertLanguageInterface 'en', 'Selected', N'Selected', 'grapesjs'
exec insertLanguageInterface 'uk', 'Selected', N'Вибраний', 'grapesjs'
exec insertLanguageInterface 'ru', 'Selected', N'Выбранный', 'grapesjs'
GO

exec insertLanguageInterface 'en', 'State', N'State', 'grapesjs'
exec insertLanguageInterface 'uk', 'State', N'Стан', 'grapesjs'
exec insertLanguageInterface 'ru', 'State', N'Состояние', 'grapesjs'
GO

exec insertLanguageInterface 'en', 'Hover', N'Hover', 'grapesjs'
exec insertLanguageInterface 'uk', 'Hover', N'Наведення', 'grapesjs'
exec insertLanguageInterface 'ru', 'Hover', N'Наведение', 'grapesjs'
GO

exec insertLanguageInterface 'en', 'Click', N'Click', 'grapesjs'
exec insertLanguageInterface 'uk', 'Click', N'Натискання', 'grapesjs'
exec insertLanguageInterface 'ru', 'Click', N'Нажатие', 'grapesjs'
GO

exec insertLanguageInterface 'en', 'Even/odd', N'Even/odd', 'grapesjs'
exec insertLanguageInterface 'uk', 'Even/odd', N'Парний/непарний', 'grapesjs'
exec insertLanguageInterface 'ru', 'Even/odd', N'Четный/нечетный', 'grapesjs'
GO

exec insertLanguageInterface 'en', 'Select an element before using style manager', N'Select an element before using style manager', 'grapesjs'
exec insertLanguageInterface 'uk', 'Select an element before using style manager', N'Оберіть елемент щоб редагувати стиль', 'grapesjs'
exec insertLanguageInterface 'ru', 'Select an element before using style manager', N'Выберите элемент чтобы редатировать стиль', 'grapesjs'
GO

exec insertLanguageInterface 'en', 'Layer', N'Layer', 'grapesjs'
exec insertLanguageInterface 'uk', 'Layer', N'Шар', 'grapesjs'
exec insertLanguageInterface 'ru', 'Layer', N'Слой', 'grapesjs'
GO

exec insertLanguageInterface 'en', 'Images', N'Images', 'grapesjs'
exec insertLanguageInterface 'uk', 'Images', N'Зображення', 'grapesjs'
exec insertLanguageInterface 'ru', 'Images', N'Изображение', 'grapesjs'
GO

exec insertLanguageInterface 'en', 'General', N'General', 'grapesjs'
exec insertLanguageInterface 'uk', 'General', N'Загальні', 'grapesjs'
exec insertLanguageInterface 'ru', 'General', N'Общее', 'grapesjs'
GO

exec insertLanguageInterface 'en', 'Layout', N'Layout', 'grapesjs'
exec insertLanguageInterface 'uk', 'Layout', N'Розміщення', 'grapesjs'
exec insertLanguageInterface 'ru', 'Layout', N'Размещение', 'grapesjs'
GO

exec insertLanguageInterface 'en', 'Typography', N'Typography', 'grapesjs'
exec insertLanguageInterface 'uk', 'Typography', N'Типографія', 'grapesjs'
exec insertLanguageInterface 'ru', 'Typography', N'Типография', 'grapesjs'
GO

exec insertLanguageInterface 'en', 'Decorations', N'Decorations', 'grapesjs'
exec insertLanguageInterface 'uk', 'Decorations', N'Декорації', 'grapesjs'
exec insertLanguageInterface 'ru', 'Decorations', N'Декорации', 'grapesjs'
GO

exec insertLanguageInterface 'en', 'Extra', N'Extra', 'grapesjs'
exec insertLanguageInterface 'uk', 'Extra', N'Додатково', 'grapesjs'
exec insertLanguageInterface 'ru', 'Extra', N'Дополнительно', 'grapesjs'
GO

exec insertLanguageInterface 'en', 'Flex', N'Flex', 'grapesjs'
exec insertLanguageInterface 'uk', 'Flex', N'Flex', 'grapesjs'
exec insertLanguageInterface 'ru', 'Flex', N'Flex', 'grapesjs'
GO

exec insertLanguageInterface 'en', 'Dimension', N'Dimension', 'grapesjs'
exec insertLanguageInterface 'uk', 'Dimension', N'Виміри', 'grapesjs'
exec insertLanguageInterface 'ru', 'Dimension', N'Измерения', 'grapesjs'
GO

exec insertLanguageInterface 'en', 'Select an element before using trait Manager', N'Select an element before using trait manager', 'grapesjs'
exec insertLanguageInterface 'uk', 'Select an element before using trait Manager', N'Спочатку виберіть елемент', 'grapesjs'
exec insertLanguageInterface 'ru', 'Select an element before using trait Manager', N'Сначала выберите элемент', 'grapesjs'
GO

exec insertLanguageInterface 'en', 'Component settings', N'Component settings', 'grapesjs'
exec insertLanguageInterface 'uk', 'Component settings', N'Налаштування компоненту', 'grapesjs'
exec insertLanguageInterface 'ru', 'Component settings', N'Настройка компонента', 'grapesjs'
GO

exec insertLanguageInterface 'en', 'This windows', N'This window', 'grapesjs'
exec insertLanguageInterface 'uk', 'This window', N'Це вікно', 'grapesjs'
exec insertLanguageInterface 'ru', 'This window', N'Это окно', 'grapesjs'
GO

exec insertLanguageInterface 'en', 'New window', N'New window', 'grapesjs'
exec insertLanguageInterface 'uk', 'New window', N'Нове вікно', 'grapesjs'
exec insertLanguageInterface 'ru', 'New window', N'Новое окно', 'grapesjs'
GO

exec insertLanguageInterface 'en', 'Link block', N'Link block', 'grapesjs'
exec insertLanguageInterface 'uk', 'Link block', N'Блок з посиланням', 'grapesjs'
exec insertLanguageInterface 'ru', 'Link block', N'Блок со ссылкой', 'grapesjs'
GO

exec insertLanguageInterface 'en', 'List items', N'List items', 'grapesjs'
exec insertLanguageInterface 'uk', 'List items', N'Елементи списку', 'grapesjs'
exec insertLanguageInterface 'ru', 'List items', N'Элементы списка', 'grapesjs'
GO

exec insertLanguageInterface 'en', 'Import', N'Import', 'grapesjs'
exec insertLanguageInterface 'uk', 'Import', N'Імпорт', 'grapesjs'
exec insertLanguageInterface 'ru', 'Import', N'Импорт', 'grapesjs'
GO

exec insertLanguageInterface 'en', 'Export template', N'Export template', 'grapesjs'
exec insertLanguageInterface 'uk', 'Export template', N'Експортувати шаблон', 'grapesjs'
exec insertLanguageInterface 'ru', 'Export template', N'Экспортировать шаблон', 'grapesjs'
GO

exec insertLanguageInterface 'en', 'Import template', N'Import template', 'grapesjs'
exec insertLanguageInterface 'uk', 'Import template', N'Імпортувати шаблон', 'grapesjs'
exec insertLanguageInterface 'ru', 'Import template', N'Импортировать шаблон', 'grapesjs'
GO

exec insertLanguageInterface 'en', 'Open blocks', N'Open blocks', 'grapesjs'
exec insertLanguageInterface 'uk', 'Open blocks', N'Розгорнути блоки', 'grapesjs'
exec insertLanguageInterface 'ru', 'Open blocks', N'Развернуть блоки', 'grapesjs'
GO

exec insertLanguageInterface 'en', 'Open layers', N'Open layers', 'grapesjs'
exec insertLanguageInterface 'uk', 'Open layers', N'Розгорнути шари', 'grapesjs'
exec insertLanguageInterface 'ru', 'Open layers', N'Развернуть слои', 'grapesjs'
GO

exec insertLanguageInterface 'en', 'Quote', N'Quote', 'grapesjs'
exec insertLanguageInterface 'uk', 'Quote', N'Цитата', 'grapesjs'
exec insertLanguageInterface 'ru', 'Quote', N'Цытата', 'grapesjs'
GO

exec insertLanguageInterface 'en', '1/3 section', N'1/3 section', 'grapesjs'
exec insertLanguageInterface 'uk', '1/3 section', N'1/3 cекція', 'grapesjs'
exec insertLanguageInterface 'ru', '1/3 section', N'1/3 cекция', 'grapesjs'
GO

exec insertLanguageInterface 'en', '3/7 section', N'3/7 section', 'grapesjs'
exec insertLanguageInterface 'uk', '3/7 section', N'3/7 cекція', 'grapesjs'
exec insertLanguageInterface 'ru', '3/7 section', N'3/7 cекция', 'grapesjs'
GO

exec insertLanguageInterface 'en', '1/2 section', N'1/2 section', 'grapesjs'
exec insertLanguageInterface 'uk', '1/2 section', N'1/2 cекція', 'grapesjs'
exec insertLanguageInterface 'ru', '1/2 section', N'1/2 cекция', 'grapesjs'
GO

exec insertLanguageInterface 'en', '1 section', N'1 section', 'grapesjs'
exec insertLanguageInterface 'uk', '1 section', N'1 cекція', 'grapesjs'
exec insertLanguageInterface 'ru', '1 section', N'1 cекция', 'grapesjs'
GO

exec insertLanguageInterface 'en', 'View components', N'View components', 'grapesjs'
exec insertLanguageInterface 'uk', 'View components', N'Переглянути компоненти', 'grapesjs'
exec insertLanguageInterface 'ru', 'View components', N'Просмотреть компоненты', 'grapesjs'
GO

exec insertLanguageInterface 'en', 'Text section', N'Text section', 'grapesjs'
exec insertLanguageInterface 'uk', 'Text section', N'Текстова секція', 'grapesjs'
exec insertLanguageInterface 'ru', 'Text section', N'Текстовая секция', 'grapesjs'
GO

exec insertLanguageInterface 'en', 'Toggle images', N'Toggle images', 'grapesjs'
exec insertLanguageInterface 'uk', 'Toggle images', N'Показати/сховати зображення', 'grapesjs'
exec insertLanguageInterface 'ru', 'Toggle images', N'Показать/скрыть изображения', 'grapesjs'
GO

exec insertLanguageInterface 'en', 'Divider', N'Divider', 'grapesjs'
exec insertLanguageInterface 'uk', 'Divider', N'Розділювач', 'grapesjs'
exec insertLanguageInterface 'ru', 'Divider', N'Разделитель', 'grapesjs'
GO

exec insertLanguageInterface 'en', 'View code', N'View code', 'grapesjs'
exec insertLanguageInterface 'uk', 'View code', N'Переглянути код', 'grapesjs'
exec insertLanguageInterface 'ru', 'View code', N'Просмотреть код', 'grapesjs'
GO

exec insertLanguageInterface 'en', 'Full screen', N'Full screen', 'grapesjs'
exec insertLanguageInterface 'uk', 'Full screen', N'Повний екран', 'grapesjs'
exec insertLanguageInterface 'ru', 'Full screen', N'Полный экран', 'grapesjs'
GO

exec insertLanguageInterface 'en', 'Grid items', N'Grid Items', 'grapesjs'
exec insertLanguageInterface 'uk', 'Grid items', N'Елементи сітки', 'grapesjs'
exec insertLanguageInterface 'ru', 'Grid Items', N'Элементы сетки', 'grapesjs'
GO
------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------
-- add missing privileges to admin role
----------------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------------
IF NOT EXISTS (SELECT 1 FROM info_roleprivilege WHERE role_id = (SELECT id FROM info_role WHERE code='ROLE_ADMIN' AND privilege_id IN (SELECT id FROM info_Serviceprivilege WHERE code LIKE 'ADMIN_ACCESS_TO%')))
    INSERT INTO info_roleprivilege(privilege_id, role_id)
    SELECT id, (SELECT id FROM info_role WHERE code='ROLE_ADMIN') FROM info_serviceprivilege WHERE CODE LIKE 'ADMIN_ACCESS_TO%'
------------------------------------------------------------------------------------------------------------------------
-- https://teamsoft.atlassian.net/browse/PHARMASALE-168
------------------------------------------------------------------------------------------------------------------------
UPDATE ps_pattern_field SET [column]=replace(replace(replace(replace([column], CHAR(10), ' '), '  ',' '), '  ', ' '), '  ', ' ');
------------------------------------------------------------------------------------------------------------------------
-- https://teamsoft.atlassian.net/browse/PHARMAWEB-399
------------------------------------------------------------------------------------------------------------------------
DECLARE @SERVICEID int
set @SERVICEID = (SELECT id FROM info_service WHERE identifier = 'expenses')
IF NOT EXISTS(SELECT 1 FROM info_serviceprivilege WHERE code='ROLE_ACCESS_TO_EXPENSES' AND service_id=@SERVICEID AND parent_id IS NULL)
    INSERT INTO info_serviceprivilege(name, service_id, code) VALUES('Access to expenses', @SERVICEID, 'ROLE_ACCESS_TO_EXPENSES')

IF NOT EXISTS(SELECT 1 FROM info_serviceprivilege WHERE code='ROLE_ACCESS_TO_EXPENSES_LINE1' AND service_id=@SERVICEID AND parent_id IS NULL)
   INSERT INTO info_serviceprivilege(name, service_id, code) VALUES('Access to expenses line1', @SERVICEID, 'ROLE_ACCESS_TO_EXPENSES_LINE1')

IF NOT EXISTS(SELECT 1 FROM info_serviceprivilege WHERE code='ROLE_ACCESS_TO_EXPENSES_LINE2' AND service_id=@SERVICEID AND parent_id IS NULL)
    INSERT INTO info_serviceprivilege(name, service_id, code) VALUES('Access to expenses line2', @SERVICEID, 'ROLE_ACCESS_TO_EXPENSES_LINE2')
GO
------------------------------------------------------------------------------------------------------------------------
---https://teamsoft.atlassian.net/browse/PHARMAWEB-391
------------------------------------------------------------------------------------------------------------------------
ALTER PROCEDURE [dbo].[insertLanguageInterface](@slug VARCHAR(2), @key VARCHAR(255), @translation VARCHAR(255), @domain VARCHAR(255) = 'messages')
AS
BEGIN
    DECLARE @language_id int;
    SELECT TOP 1 @language_id = id FROM info_language WHERE slug = @slug

    IF @language_id is null return

    IF NOT EXISTS(SELECT 1
                     FROM info_languageinterface
                     WHERE [key] = @key AND language_id = @language_id AND domain = @domain)
    INSERT INTO info_languageinterface ([key], translation, language_id, domain)
    VALUES (@key, @translation, @language_id, @domain)
    ELSE
    UPDATE info_languageinterface set translation = @translation
    WHERE [key] = @key AND language_id = @language_id AND domain = @domain

END
GO

exec insertLanguageInterface 'ru', 'Visits', N'Визиты', 'geomarketing';
exec insertLanguageInterface 'ru', 'Turnover', N'Товарооборот', 'messages';
exec insertLanguageInterface 'ru', 'Clustering regions', N'Кластеризация областей', 'geomarketing';
exec insertLanguageInterface 'ru', 'Tables', N'Таблицы', 'messages';
exec insertLanguageInterface 'ru', 'Specializations', N'Специализации', 'messages';
exec insertLanguageInterface 'ru', 'Select brand', N'Выбрать бренд', 'messages';
exec insertLanguageInterface 'ru', 'One of the following fields must contain value ("Preparation", "Preparation code")', N'Одно из полей должно содержать значение ("Препарат", "Код препарата")', 'messages';
exec insertLanguageInterface 'ru', 'No plan for selected user', N'План по выбранному пользователю отсутствует', 'messages';
exec insertLanguageInterface 'ru', 'Middle Name', N'Отчество', 'messages';
exec insertLanguageInterface 'ru', 'ismain', N'Главное учреждение', 'crm';
exec insertLanguageInterface 'ru', 'Granually planing', N'Грануальное планирование', 'messages';
exec insertLanguageInterface 'ru', 'Preparation', N'Препараты', 'geomarketing';
exec insertLanguageInterface 'ru', 'emptyResponsibles', N'Нет ответственных', 'messages';
exec insertLanguageInterface 'ru', 'Edit contact', N'Редактировать контакт', 'crm';
exec insertLanguageInterface 'ru', 'Edit account', N'Редактировать учреждение', 'crm';
exec insertLanguageInterface 'ru', 'Coefficient', N'Коэффициент', 'messages';
exec insertLanguageInterface 'ru', 'Are you sure, you want to close window without saving changes?', N'Вы уверены, что хотите выйти без сохранения данных?', 'messages';
exec insertLanguageInterface 'ru', 'Administrator', N'Администратор (полный доступ)', 'messages';
exec insertLanguageInterface 'ru', 'Contact category', N'Категория клиента', 'geomarketing';
exec insertLanguageInterface 'ru', 'Rep cluster', N'Расчёт РЕП', 'geomarketing';
exec insertLanguageInterface 'ru', 'Click to continue drawing polygon', N'Нажмите на карту чтобы продолжить рисовать полигон', 'geomarketing';
exec insertLanguageInterface 'ru', 'Confirm archivation', N'Подтверждение архивации', 'messages';
exec insertLanguageInterface 'ru', 'Confirm unarchiving', N'Подтверждение разархивации', 'messages';
exec insertLanguageInterface 'ru', 'Privileges updated', N'Привилегии обновлены', 'messages';
exec insertLanguageInterface 'ru', 'ismain', N'Главное учреждение', 'change_requests';
exec insertLanguageInterface 'ru', 'Search', N'Поиск', 'geomarketing';
exec insertLanguageInterface 'ru', 'No pins within selected polygons', N'Нет пинов внутри выбранных полигонов', 'geomarketing';
exec insertLanguageInterface 'ru', 'Less then two polygons in selection', N'Меньше двух полигонов в выбранной территории', 'geomarketing';
exec insertLanguageInterface 'ru', 'filters', N'Фильтры', 'ag-grid';
exec insertLanguageInterface 'ru', 'Comment', N'Комментарий', 'grapesjs';
exec insertLanguageInterface 'ru', 'Desktop', N'Компьютер', 'grapesjs';
exec insertLanguageInterface 'ru', 'Select an element before using style manager', N'Выберите элемент чтобы редактировать стиль', 'grapesjs';
exec insertLanguageInterface 'ru', 'Quote', N'Цитата', 'grapesjs';
exec insertLanguageInterface 'en', 'Add company logo', N'Add English company logo', 'messages';
exec insertLanguageInterface 'en', 'additionalowner', N'Additional responsible', 'crm';
exec insertLanguageInterface 'en', 'hierarchy', N'Hierarchy', 'messages';
exec insertLanguageInterface 'en', 'owner', N'Responsible', 'crm';
exec insertLanguageInterface 'en', 'Use your browsers Back button to navigate to the page you have prevously come from message', N'Use your browsers Back button to navigate to the page you have previously come from message', 'crm';
exec insertLanguageInterface 'en', 'More about polygon', N'More about polygon', 'geomarketing';
exec insertLanguageInterface 'en', 'Add subornidate', N'Add subordinate', 'messages';
exec insertLanguageInterface 'en', 'Adress', N'Address', 'messages';
exec insertLanguageInterface 'en', 'Coordinate obtained successfully', N'Coordinate obtained successfully', 'messages';
exec insertLanguageInterface 'en', 'You''ll be able to get the click-through statistics for the link', N'You''ll be able to get the click-through statistics for the link', 'messages';
exec insertLanguageInterface 'en', 'Links will not be converted. You won''t be able to track who followed the link', N'Links will not be converted. You won''t be able to track who followed the link', 'messages';
exec insertLanguageInterface 'en', 'Distribution statistics not available', N'Distribution statistics not available', 'messages';
exec insertLanguageInterface 'en', 'Business region', N'Business region', 'messages';
exec insertLanguageInterface 'en', 'Coordinate obtained successfully', N'Coordinate obtained successfully', 'messages';
exec insertLanguageInterface 'uk', 'Choose region', N'Оберіть область', 'geomarketing';
exec insertLanguageInterface 'uk', 'Users and polygons', N'Користувачі і полігони', 'geomarketing';
exec insertLanguageInterface 'uk', 'Zoom to all companies', N'Масштабувати за всіма організаціями', 'geomarketing';
exec insertLanguageInterface 'uk', 'Task not found', N'Візит не знайдено', 'crm';
exec insertLanguageInterface 'uk', 'NULL', N'Введіть свою електронну пошту і ми Вам відправимо лист', 'messages';
exec insertLanguageInterface 'uk', 'Feedback', N'Зворотній зв''язок', 'messages';
exec insertLanguageInterface 'uk', 'NULL', N'Поле має пов''язаний запис і не може бути видалено', 'messages';
exec insertLanguageInterface 'uk', 'No companies', N'Організації відсутні', 'geomarketing';
exec insertLanguageInterface 'uk', 'NULL', N'Новий клієнт', 'crm';
exec insertLanguageInterface 'uk', 'NULL', N'Значення має бути числовим', 'messages';
exec insertLanguageInterface 'uk', 'NULL', N'Значення не з списку', 'messages';
exec insertLanguageInterface 'uk', 'NULL', N'Звіт не знайдено', 'crm';
exec insertLanguageInterface 'uk', 'Rows where this column is empty will be ignored', N'Записи з порожнім значенням в цьому стовпчику будуть ігноруватися', 'messages';
exec insertLanguageInterface 'uk', 'NULL', N'Усі дані були скопійовані та збережені', 'messages';
exec insertLanguageInterface 'uk', 'Pivot mode', N'Режим групування', 'messages';
exec insertLanguageInterface 'uk', 'NULL', N'Будь ласка оберіть як мінімум один стовпчик', 'validators';
exec insertLanguageInterface 'uk', 'Select month', N'Оберіть місяць', 'messages';
exec insertLanguageInterface 'uk', 'NULL', N'Будь ласка оберіть як мінімум один стовпчик', 'validators';
exec insertLanguageInterface 'uk', 'New', N'Новий', 'targeting';
exec insertLanguageInterface 'uk', 'Not changed', N'Не змінено', 'targeting';
exec insertLanguageInterface 'uk', 'Personalization', N'Персоналізація', 'mcm';
exec insertLanguageInterface 'uk', 'Contact category', N'Категорія спеціалістів', 'geomarketing';
exec insertLanguageInterface 'uk', 'STATUS_VERIFICATION_4', N'Відхилено', 'crm';
exec insertLanguageInterface 'uk', 'Old value', N'Старе значення', 'crm';
exec insertLanguageInterface 'uk', 'lastname', N'Прізвище', 'crm';
exec insertLanguageInterface 'uk', 'Choose polygon for comparison', N'Виберіть полігон для порівняння (максимум 5)', 'geomarketing';
GO

------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------