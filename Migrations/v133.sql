------------------------------------------------------------------------------------------------------------------------
-- https://teamsoft.atlassian.net/browse/VEKTORRU-120
------------------------------------------------------------------------------------------------------------------------
exec insertLanguageInterface 'en', 'Sorry, you cannot view this monthly report.', N'Sorry, you cannot view this monthly report.', 'car-report'
exec insertLanguageInterface 'ru', 'Sorry, you cannot view this monthly report.', N'Простите, вы не можете просмотреть этот ежемесячный отчет.', 'car-report'
exec insertLanguageInterface 'uk', 'Sorry, you cannot view this monthly report.', N'Вибачте, ви не можете переглянути цей щомісячний звіт.', 'car-report'
GO
------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------

----------------------------------------------------------------------------------------------------
-- https://teamsoft.atlassian.net/browse/MCM-411
----------------------------------------------------------------------------------------------------
IF OBJECT_ID(N'[info_mcmtemplate]', 'U') IS NULL
BEGIN
CREATE TABLE info_mcmtemplate
(
    id            [int] IDENTITY (1,1) NOT NULL,
    name          [varchar](255),
    content       [varchar](max),
    user_id       [int],
    [currenttime] [datetime]           NULL,
    [time]        [timestamp]          NULL,
    [moduser]     [varchar](16)        NULL,
    [guid]        [uniqueidentifier]   NULL,
    CONSTRAINT [PK_info_mcmtemplate] PRIMARY KEY CLUSTERED(
        [id] ASC
        ) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
    ) ON [PRIMARY]

ALTER TABLE [dbo].[info_mcmtemplate]
    ADD CONSTRAINT [DF_info_mcmtemplatecurrenttime] DEFAULT (getdate()) FOR [currenttime]
ALTER TABLE [dbo].[info_mcmtemplate]
    ADD CONSTRAINT [DF_info_mcmtemplatemoduser] DEFAULT ([dbo].[Get_CurrentCode]()) FOR [moduser]
ALTER TABLE [dbo].[info_mcmtemplate]
    ADD CONSTRAINT [DF_info_mcmtemplateguid] DEFAULT (newid()) FOR [guid]
ALTER TABLE [dbo].[info_mcmtemplate]
    WITH CHECK
    ADD CONSTRAINT [FK_info_mcmtemplate_user_id] FOREIGN KEY ([user_id]) REFERENCES [dbo].[info_user] ([id])
END
GO


IF NOT EXISTS(
    SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS
    WHERE table_name = 'info_mcmcontent' AND column_name = 'json'
)
ALTER TABLE [dbo].[info_mcmcontent] ADD json varchar(max) NULL
    GO


EXEC insertLanguageInterface 'en', 'Container', 'Container', 'mcm';
GO
EXEC insertLanguageInterface 'ru', 'Container', 'Контейнер', 'mcm';
GO
EXEC insertLanguageInterface 'uk', 'Container', 'Контейнер', 'mcm';
GO

EXEC insertLanguageInterface 'en', 'Blocks', 'Blocks', 'mcm'
GO
EXEC insertLanguageInterface 'ru', 'Blocks', 'Блоки', 'mcm'
GO
EXEC insertLanguageInterface 'uk', 'Blocks', 'Блоки', 'mcm'
GO

EXEC insertLanguageInterface 'en', 'Image', 'Image', 'mcm'
GO
EXEC insertLanguageInterface 'ru', 'Image', 'Изображение', 'mcm'
GO
EXEC insertLanguageInterface 'uk', 'Image', 'Зображення', 'mcm'
GO

EXEC insertLanguageInterface 'en', 'Separator', 'Separator', 'mcm'
GO
EXEC insertLanguageInterface 'ru', 'Separator', 'Разделитель', 'mcm'
GO
EXEC insertLanguageInterface 'uk', 'Separator', 'Роздільник', 'mcm'
GO

EXEC insertLanguageInterface 'en', 'Social networks', 'Social networks', 'mcm'
GO
EXEC insertLanguageInterface 'ru', 'Social networks', 'Социальные сети', 'mcm'
GO
EXEC insertLanguageInterface 'uk', 'Social networks', 'Соціальні мережі', 'mcm'
GO

EXEC insertLanguageInterface 'en', 'Video', 'Video', 'mcm'
GO
EXEC insertLanguageInterface 'ru', 'Video', 'Видео', 'mcm'
GO
EXEC insertLanguageInterface 'uk', 'Video', 'Відео', 'mcm'
GO

EXEC insertLanguageInterface 'en', 'Text', 'Text', 'mcm'
GO
EXEC insertLanguageInterface 'ru', 'Text', 'Текст', 'mcm'
GO
EXEC insertLanguageInterface 'uk', 'Text', 'Текст', 'mcm'
GO

EXEC insertLanguageInterface 'en', 'Edit template', 'Edit template', 'mcm'
GO
EXEC insertLanguageInterface 'ru', 'Edit template', 'Редактировать', 'mcm'
GO
EXEC insertLanguageInterface 'uk', 'Edit template', 'Редагувати', 'mcm'
GO

EXEC insertLanguageInterface 'en', 'Copy template', 'Copy template', 'mcm'
GO
EXEC insertLanguageInterface 'ru', 'Copy template', 'Копировать', 'mcm'
GO
EXEC insertLanguageInterface 'uk', 'Copy template', 'Копіювати', 'mcm'
GO

EXEC insertLanguageInterface 'en', 'Rename template', 'Rename template', 'mcm'
GO
EXEC insertLanguageInterface 'ru', 'Rename template', 'Переименовать', 'mcm'
GO
EXEC insertLanguageInterface 'uk', 'Rename template', 'Перейменувати', 'mcm'
GO

EXEC insertLanguageInterface 'en', 'Delete template', 'Delete template', 'mcm'
GO
EXEC insertLanguageInterface 'ru', 'Delete template', 'Удалить', 'mcm'
GO
EXEC insertLanguageInterface 'uk', 'Delete template', 'Видалити', 'mcm'
GO

EXEC insertLanguageInterface 'en', 'Template constructor', 'Template constructor', 'mcm'
GO
EXEC insertLanguageInterface 'ru', 'Template constructor', 'Конструктор шаблонов', 'mcm'
GO
EXEC insertLanguageInterface 'uk', 'Template constructor', 'Констуктор шаблонів', 'mcm'
GO

EXEC insertLanguageInterface 'en', 'Structure', 'Structure', 'mcm'
GO
EXEC insertLanguageInterface 'ru', 'Structure', 'Структура', 'mcm'
GO
EXEC insertLanguageInterface 'uk', 'Structure', 'Структура', 'mcm'
GO

EXEC insertLanguageInterface 'en', 'My templates', 'Structure', 'mcm'
GO
EXEC insertLanguageInterface 'ru', 'My templates', 'Мои шаблоны', 'mcm'
GO
EXEC insertLanguageInterface 'uk', 'My templates', 'Мої шаблони', 'mcm'
GO

EXEC insertLanguageInterface 'en', 'Unable to recognize video url', 'Unable to recognize video url', 'mcm'
GO
EXEC insertLanguageInterface 'ru', 'Unable to recognize video url', 'Не удается распознать ссылку', 'mcm'
GO
EXEC insertLanguageInterface 'uk', 'Unable to recognize video url', 'Не вдається розпізнати посилання', 'mcm'
GO

EXEC insertLanguageInterface 'en', 'Please add the video URL', 'Please add the video URL', 'mcm'
GO
EXEC insertLanguageInterface 'ru', 'Please add the video URL', 'Укажите ссылку на видео', 'mcm'
GO
EXEC insertLanguageInterface 'uk', 'Please add the video URL', 'Вкажіть посилання на відео', 'mcm'
GO

EXEC insertLanguageInterface 'en', 'Video URL', 'Video URL', 'mcm'
GO
EXEC insertLanguageInterface 'ru', 'Video URL', 'Ссылка на видео', 'mcm'
GO
EXEC insertLanguageInterface 'uk', 'Video URL', 'Посилання на відео', 'mcm'
GO

EXEC insertLanguageInterface 'en', 'Style and size', 'Style and size', 'mcm'
GO
EXEC insertLanguageInterface 'ru', 'Style and size', 'Стиль и размер', 'mcm'
GO
EXEC insertLanguageInterface 'uk', 'Style and size', 'Стиль та розмір', 'mcm'
GO

EXEC insertLanguageInterface 'en', 'Styles', 'Styles', 'mcm'
GO
EXEC insertLanguageInterface 'ru', 'Styles', 'Стиль', 'mcm'
GO
EXEC insertLanguageInterface 'uk', 'Styles', 'Стиль', 'mcm'
GO

EXEC insertLanguageInterface 'en', 'Interval', 'Interval', 'mcm'
GO
EXEC insertLanguageInterface 'ru', 'Interval', 'Интервал', 'mcm'
GO
EXEC insertLanguageInterface 'uk', 'Interval', 'Інтервал', 'mcm'
GO

EXEC insertLanguageInterface 'en', 'Button', 'Button', 'mcm'
GO
EXEC insertLanguageInterface 'ru', 'Button', 'Кнопка', 'mcm'
GO
EXEC insertLanguageInterface 'uk', 'Button', 'Кнопка', 'mcm'
GO

EXEC insertLanguageInterface 'en', 'Sender', 'Sender', 'mcm'
GO
EXEC insertLanguageInterface 'ru', 'Sender', 'Отправитель', 'mcm'
GO
EXEC insertLanguageInterface 'uk', 'Sender', 'Відправник', 'mcm'
GO

EXEC insertLanguageInterface 'en', 'Receiver', 'Receiver', 'mcm'
GO
EXEC insertLanguageInterface 'ru', 'Receiver', 'Получатель', 'mcm'
GO
EXEC insertLanguageInterface 'uk', 'Receiver', 'Отримувач', 'mcm'
GO

EXEC insertLanguageInterface 'en', 'Template name', 'Template name', 'mcm'
GO
EXEC insertLanguageInterface 'ru', 'Template name', 'Название шаблона', 'mcm'
GO
EXEC insertLanguageInterface 'uk', 'Template name', 'Назва шаблону', 'mcm'
GO

EXEC insertLanguageInterface 'en', 'Add', 'Add', 'mcm'
GO
EXEC insertLanguageInterface 'ru', 'Add', 'Добавьте', 'mcm'
GO
EXEC insertLanguageInterface 'uk', 'Add', 'Додайте', 'mcm'
GO

EXEC insertLanguageInterface 'en', 'or', 'or', 'mcm'
GO
EXEC insertLanguageInterface 'ru', 'or', 'или', 'mcm'
GO
EXEC insertLanguageInterface 'uk', 'or', 'або', 'mcm'
GO

EXEC insertLanguageInterface 'en', 'Are you sure you want to delete this template?',
     'Are you sure you want to delete this template?', 'mcm'
GO
EXEC insertLanguageInterface 'ru', 'Are you sure you want to delete this template?',
     'Вы уверены, что хотите удалить шаблон?', 'mcm'
GO
EXEC insertLanguageInterface 'uk', 'Are you sure you want to delete this template?',
     'Ви впевнені, що хочете видалити шаблон?', 'mcm'
GO

EXEC insertLanguageInterface 'en', 'Are you sure you want to delete this item?',
     'Are you sure you want to delete this item?', 'mcm'
GO
EXEC insertLanguageInterface 'ru', 'Are you sure you want to delete this item?',
     'Вы уверены, что хотите удалить элемент?', 'mcm'
GO
EXEC insertLanguageInterface 'uk', 'Are you sure you want to delete this item?',
     'Ви впевнені, що хочете видалити елемент?', 'mcm'
GO

EXEC insertLanguageInterface 'en', 'URL to generate a preview image. The image will link to the provided URL.',
     'URL to generate a preview image. The image will link to the provided URL.', 'mcm'
GO
EXEC insertLanguageInterface 'ru', 'URL to generate a preview image. The image will link to the provided URL.',
     'Ссылка, чтобы сгенерировать предпросмотр. Изображение будет ссылаться на указанную ссылку.', 'mcm'
GO
EXEC insertLanguageInterface 'uk', 'URL to generate a preview image. The image will link to the provided URL.',
     'Посилання, щоб згенерувати попередній перегляд. Зображення буде посилатися на вказане посилання', 'mcm'
GO

EXEC insertLanguageInterface 'en', 'Border', 'Border', 'mcm'
GO
EXEC insertLanguageInterface 'ru', 'Border', 'Граница', 'mcm'
GO
EXEC insertLanguageInterface 'uk', 'Border', 'Межа', 'mcm'
GO

EXEC insertLanguageInterface 'en', 'Hyperlink color', 'Hyperlink color', 'mcm'
GO
EXEC insertLanguageInterface 'ru', 'Hyperlink color', 'Цвет ссылки', 'mcm'
GO
EXEC insertLanguageInterface 'uk', 'Hyperlink color', 'Колір посилання', 'mcm'
GO

EXEC insertLanguageInterface 'en', 'Font color', 'Font color', 'mcm'
GO
EXEC insertLanguageInterface 'ru', 'Font color', 'Цвет текста', 'mcm'
GO
EXEC insertLanguageInterface 'uk', 'Font color', 'Колір тексту', 'mcm'
GO

EXEC insertLanguageInterface 'en', 'Foreground color', 'Foreground color', 'mcm'
GO
EXEC insertLanguageInterface 'ru', 'Foreground color', 'Цвет', 'mcm'
GO
EXEC insertLanguageInterface 'uk', 'Foreground color', 'Колір', 'mcm'
GO

EXEC insertLanguageInterface 'en', 'Background color', 'Background color', 'mcm'
GO
EXEC insertLanguageInterface 'ru', 'Background color', 'Цвет фона', 'mcm'
GO
EXEC insertLanguageInterface 'uk', 'Background color', 'Колір фону', 'mcm'
GO

EXEC insertLanguageInterface 'en', 'Top', 'Top', 'mcm'
GO
EXEC insertLanguageInterface 'ru', 'Top', 'Сверху', 'mcm'
GO
EXEC insertLanguageInterface 'uk', 'Top', 'Зверху', 'mcm'
GO

EXEC insertLanguageInterface 'en', 'Left', 'Left', 'mcm'
GO
EXEC insertLanguageInterface 'ru', 'Left', 'Слева', 'mcm'
GO
EXEC insertLanguageInterface 'uk', 'Left', 'Зліва', 'mcm'
GO

EXEC insertLanguageInterface 'en', 'Right', 'Right', 'mcm'
GO
EXEC insertLanguageInterface 'ru', 'Right', 'Справа', 'mcm'
GO
EXEC insertLanguageInterface 'uk', 'Right', 'Справа', 'mcm'
GO

EXEC insertLanguageInterface 'en', 'Bottom', 'Bottom', 'mcm'
GO
EXEC insertLanguageInterface 'ru', 'Bottom', 'Снизу', 'mcm'
GO
EXEC insertLanguageInterface 'uk', 'Bottom', 'Знизу', 'mcm'
GO

EXEC insertLanguageInterface 'en', 'All sides', 'All sides', 'mcm'
GO
EXEC insertLanguageInterface 'ru', 'All sides', 'Все стороны', 'mcm'
GO
EXEC insertLanguageInterface 'uk', 'All sides', 'Всі сторони', 'mcm'
GO

EXEC insertLanguageInterface 'en', 'Padding', 'Padding', 'mcm'
GO
EXEC insertLanguageInterface 'ru', 'Padding', 'Отступ', 'mcm'
GO
EXEC insertLanguageInterface 'uk', 'Padding', 'Відступ', 'mcm'
GO

EXEC insertLanguageInterface 'en', 'Border radius', 'Border radius', 'mcm'
GO
EXEC insertLanguageInterface 'ru', 'Border radius', 'Закругление краёв', 'mcm'
GO
EXEC insertLanguageInterface 'uk', 'Border radius', 'Заокруглення країв', 'mcm'
GO

EXEC insertLanguageInterface 'en', 'Alignment', 'Alignment', 'mcm'
GO
EXEC insertLanguageInterface 'ru', 'Alignment', 'Выравнивание', 'mcm'
GO
EXEC insertLanguageInterface 'uk', 'Alignment', 'Вирівнювання', 'mcm'
GO

EXEC insertLanguageInterface 'en', 'Font style', 'Font style', 'mcm'
GO
EXEC insertLanguageInterface 'ru', 'Font style', 'Стиль текста', 'mcm'
GO
EXEC insertLanguageInterface 'uk', 'Font style', 'Стиль тексту', 'mcm'
GO

EXEC insertLanguageInterface 'en', 'Play icon', 'Play icon', 'mcm'
GO
EXEC insertLanguageInterface 'ru', 'Play icon', 'Кнопка Play', 'mcm'
GO
EXEC insertLanguageInterface 'uk', 'Play icon', 'Кнопка Play', 'mcm'
GO

EXEC insertLanguageInterface 'en', 'Line height', 'Line height', 'mcm'
GO
EXEC insertLanguageInterface 'ru', 'Line height', 'Высота строки', 'mcm'
GO
EXEC insertLanguageInterface 'uk', 'Line height', 'Висота строки', 'mcm'
GO

EXEC insertLanguageInterface 'en', 'Height', 'Height', 'mcm'
GO
EXEC insertLanguageInterface 'ru', 'Height', 'Высота', 'mcm'
GO
EXEC insertLanguageInterface 'uk', 'Height', 'Висота', 'mcm'
GO

EXEC insertLanguageInterface 'en', 'Font', 'Font', 'mcm'
GO
EXEC insertLanguageInterface 'ru', 'Font', 'Шрифт', 'mcm'
GO
EXEC insertLanguageInterface 'uk', 'Font', 'Шрифт', 'mcm'
GO

EXEC insertLanguageInterface 'en', 'Text', 'Text', 'mcm'
GO
EXEC insertLanguageInterface 'ru', 'Text', 'Текст', 'mcm'
GO
EXEC insertLanguageInterface 'uk', 'Text', 'Текст', 'mcm'
GO

EXEC insertLanguageInterface 'en', 'Hyperlink', 'Hyperlink', 'mcm'
GO
EXEC insertLanguageInterface 'ru', 'Hyperlink', 'Гиперссылка', 'mcm'
GO
EXEC insertLanguageInterface 'uk', 'Hyperlink', 'Посилання', 'mcm'
GO

EXEC insertLanguageInterface 'en', 'Width', 'Width', 'mcm'
GO
EXEC insertLanguageInterface 'ru', 'Width', 'Ширина', 'mcm'
GO
EXEC insertLanguageInterface 'uk', 'Width', 'Ширина', 'mcm'
GO

EXEC insertLanguageInterface 'en', 'Background image', 'Background image', 'mcm'
GO
EXEC insertLanguageInterface 'ru', 'Background image', 'Фоновое изображение', 'mcm'
GO
EXEC insertLanguageInterface 'uk', 'Background image', 'Фонове зображення', 'mcm'
GO

EXEC insertLanguageInterface 'en', 'Style', 'Style', 'mcm'
GO
EXEC insertLanguageInterface 'ru', 'Style', 'Стиль', 'mcm'
GO
EXEC insertLanguageInterface 'uk', 'Style', 'Стиль', 'mcm'
GO

EXEC insertLanguageInterface 'en', 'Auto width', 'Auto width', 'mcm'
GO
EXEC insertLanguageInterface 'ru', 'Auto width', 'Автоширина', 'mcm'
GO
EXEC insertLanguageInterface 'uk', 'Auto width', 'Автоширина', 'mcm'
GO

EXEC insertLanguageInterface 'en', 'Opacity', 'Opacity', 'mcm'
GO
EXEC insertLanguageInterface 'ru', 'Opacity', 'Прозрачность', 'mcm'
GO
EXEC insertLanguageInterface 'uk', 'Opacity', 'Прозорість', 'mcm'
GO

EXEC insertLanguageInterface 'en', 'Are you sure you want to close window without saving data?',
     'Are you sure you want to close window without saving data?', 'mcm'
GO
EXEC insertLanguageInterface 'ru', 'Are you sure you want to close window without saving data?',
     'Вы уверены, что хотите закрыть окно без сохранения данных?', 'mcm'
GO
EXEC insertLanguageInterface 'uk', 'Are you sure you want to close window without saving data?',
     'Ви впевнені, що хочете закрити вікно без збереження даних', 'mcm'
GO

EXEC insertLanguageInterface 'en', 'Template has been saved', 'Template has been saved', 'mcm'
GO
EXEC insertLanguageInterface 'ru', 'Template has been saved', 'Шаблон сохранён', 'mcm'
GO
EXEC insertLanguageInterface 'uk', 'Template has been saved', 'Шаблон збережено', 'mcm'
GO

EXEC insertLanguageInterface 'en', 'Content has been saved', 'Content has been saved', 'mcm'
GO
EXEC insertLanguageInterface 'ru', 'Content has been saved', 'Контент сохранён', 'mcm'
GO
EXEC insertLanguageInterface 'uk', 'Content has been saved', 'Контент збережено', 'mcm'
GO

EXEC insertLanguageInterface 'en', 'Save content', 'Save content', 'mcm'
GO
EXEC insertLanguageInterface 'ru', 'Save content', 'Сохранить контент', 'mcm'
GO
EXEC insertLanguageInterface 'uk', 'Save content', 'Зберегти контент', 'mcm'
GO

EXEC insertLanguageInterface 'en', 'Send test message', 'Send test message', 'mcm'
GO
EXEC insertLanguageInterface 'ru', 'Send test message', 'Отправить тестовое письмо', 'mcm'
GO
EXEC insertLanguageInterface 'uk', 'Send test message', 'Відправити тестовий лист', 'mcm'
GO

EXEC insertLanguageInterface 'en', 'Attach file', 'Attach file', 'mcm'
GO
EXEC insertLanguageInterface 'ru', 'Attach file', 'Прикрепить файл', 'mcm'
GO
EXEC insertLanguageInterface 'uk', 'Attach file', 'Прикріпити файл', 'mcm'
GO

EXEC insertLanguageInterface 'en', 'Fullscreen mode', 'Fullscreen mode', 'mcm'
GO
EXEC insertLanguageInterface 'ru', 'Fullscreen mode', 'Полноэкранный режим', 'mcm'
GO
EXEC insertLanguageInterface 'uk', 'Fullscreen mode', 'Повноекранний режим', 'mcm'
GO

EXEC insertLanguageInterface 'en', 'Exit fullscreen mode', 'Exit fullscreen mode', 'mcm'
GO
EXEC insertLanguageInterface 'ru', 'Exit fullscreen mode', 'Выйти из полноэкранного режима', 'mcm'
GO
EXEC insertLanguageInterface 'uk', 'Exit fullscreen mode', 'Вийти з повноекранного режиму', 'mcm'
GO

EXEC insertLanguageInterface 'en', 'Preview laptop', 'Preview laptop', 'mcm'
GO
EXEC insertLanguageInterface 'ru', 'Preview laptop', 'Предпросмотр', 'mcm'
GO
EXEC insertLanguageInterface 'uk', 'Preview laptop', 'Попередній перегляд', 'mcm'
GO

EXEC insertLanguageInterface 'en', 'Preview mobile', 'Preview mobile', 'mcm'
GO
EXEC insertLanguageInterface 'ru', 'Preview mobile', 'Предпросмотр на смартфоне', 'mcm'
GO
EXEC insertLanguageInterface 'uk', 'Preview mobile', 'Попередній перегляд на смартфоні', 'mcm'
GO

EXEC insertLanguageInterface 'en', 'Save to my templates', 'Save to my templates', 'mcm'
GO
EXEC insertLanguageInterface 'ru', 'Save to my templates', 'Сохранить в мои шаблоны', 'mcm'
GO
EXEC insertLanguageInterface 'uk', 'Save to my templates', 'Зберегти в мої шаблони', 'mcm'
GO

EXEC insertLanguageInterface 'en', 'Drag element from right side', 'Drag element from right side', 'mcm'
GO
EXEC insertLanguageInterface 'ru', 'Drag element from right side', 'Перетяните элемент с правой части', 'mcm'
GO
EXEC insertLanguageInterface 'uk', 'Drag element from right side', 'Перетягніть елемент з правої частини', 'mcm'
GO

EXEC insertLanguageInterface 'en', 'drop here', 'drop here', 'mcm'
GO
EXEC insertLanguageInterface 'ru', 'drop here', 'переместить сюда', 'mcm'
GO
EXEC insertLanguageInterface 'uk', 'drop here', 'перемістити сюди', 'mcm'
GO

EXEC insertLanguageInterface 'en', 'move here', 'move here', 'mcm'
GO
EXEC insertLanguageInterface 'ru', 'move here', 'переместить сюда', 'mcm'
GO
EXEC insertLanguageInterface 'uk', 'move here', 'перемістити сюди', 'mcm'
GO

EXEC insertLanguageInterface 'en', 'No content here', 'No content here', 'mcm'
GO
EXEC insertLanguageInterface 'ru', 'No content here', 'Нет содержимого', 'mcm'
GO
EXEC insertLanguageInterface 'uk', 'No content here', 'Немає вмісту', 'mcm'
GO

EXEC insertLanguageInterface 'en', 'Type your text here', 'Type your text here', 'mcm'
GO
EXEC insertLanguageInterface 'ru', 'Type your text here', 'Вводите текст здесь', 'mcm'
GO
EXEC insertLanguageInterface 'uk', 'Type your text here', 'Уведіть свій текст тут', 'mcm'
GO

EXEC insertLanguageInterface 'en', 'Message has been successfully sent', 'Message has been successfully sent', 'mcm'
GO
EXEC insertLanguageInterface 'ru', 'Message has been successfully sent', 'Сообщение успешно отправлено', 'mcm'
GO
EXEC insertLanguageInterface 'uk', 'Message has been successfully sent', 'Повідомлення успішно надіслано', 'mcm'
GO


EXEC insertLanguageInterface 'en', 'Start moving blocks from right side', 'Start moving blocks from right side', 'mcm'
GO
EXEC insertLanguageInterface 'ru', 'Start moving blocks from right side', 'Начните перетаскивание блоков с правой панели', 'mcm'
GO
EXEC insertLanguageInterface 'uk', 'Start moving blocks from right side', 'Почніть перетягування блоків з правої панелі', 'mcm'
GO

EXEC insertLanguageInterface 'en', 'Do you really want to leave the constructor?', 'Do you really want to leave the constructor?', 'mcm'
GO
EXEC insertLanguageInterface 'ru', 'Do you really want to leave the constructor?', 'Вы действительно хотите выйти из конструктора?', 'mcm'
GO
EXEC insertLanguageInterface 'uk', 'Do you really want to leave the constructor?', 'Ви дійсно хочете залишити конструктор?', 'mcm'
GO
------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------
-- https://teamsoft.atlassian.net/browse/MCM-455
----------------------------------------------------------------------------------------------------

DECLARE @SERVICEID INT, @SERVICE_PRIVILEGE_MAIN_ID INT;

set @SERVICEID = (SELECT id FROM info_service WHERE identifier = 'mcm');
set @SERVICE_PRIVILEGE_MAIN_ID = (SELECT id FROM info_serviceprivilege WHERE parent_id IS NULL AND service_id = @SERVICEID);

IF NOT EXISTS (SELECT 1 FROM info_serviceprivilege WHERE parent_id = @SERVICE_PRIVILEGE_MAIN_ID AND code = 'MCM_HIDE_EMAIL')
begin
INSERT INTO info_serviceprivilege(name, code, service_id, parent_id)
VALUES('Hide MCM email content/distribution', 'MCM_HIDE_EMAIL', @SERVICEID, @SERVICE_PRIVILEGE_MAIN_ID);
END;

IF NOT EXISTS (SELECT 1 FROM info_serviceprivilege WHERE parent_id = @SERVICE_PRIVILEGE_MAIN_ID  AND code = 'MCM_HIDE_SMS')
BEGIN
INSERT INTO info_serviceprivilege(name, code, service_id, parent_id)
VALUES('Hide MCM sms content/distribution', 'MCM_HIDE_SMS', @SERVICEID, @SERVICE_PRIVILEGE_MAIN_ID);
END;

IF NOT EXISTS (SELECT 1 FROM info_serviceprivilege WHERE parent_id = @SERVICE_PRIVILEGE_MAIN_ID  AND code = 'MCM_HIDE_VIBER')
BEGIN
INSERT INTO info_serviceprivilege(name, code, service_id, parent_id)
VALUES('Hide MCM viber content/distribution', 'MCM_HIDE_VIBER', @SERVICEID, @SERVICE_PRIVILEGE_MAIN_ID);
END;

IF NOT EXISTS (SELECT 1 FROM info_serviceprivilege WHERE parent_id = @SERVICE_PRIVILEGE_MAIN_ID  AND code = 'MCM_HIDE_WHATSAPP')
BEGIN
INSERT INTO info_serviceprivilege(name, code, service_id, parent_id)
VALUES('Hide MCM whatsapp content/distribution', 'MCM_HIDE_WHATSAPP', @SERVICEID, @SERVICE_PRIVILEGE_MAIN_ID);
END;

IF NOT EXISTS (SELECT 1 FROM info_serviceprivilege WHERE parent_id = @SERVICE_PRIVILEGE_MAIN_ID  AND code = 'MCM_HIDE_TELEGRAM')
BEGIN
INSERT INTO info_serviceprivilege(name, code, service_id, parent_id)
VALUES('Hide MCM telegram content/distribution', 'MCM_HIDE_TELEGRAM', @SERVICEID, @SERVICE_PRIVILEGE_MAIN_ID);
END;
GO

exec insertLanguageInterface 'en', 'Hide MCM email content/distribution', 'Hide MCM email content/distribution', 'messages';
exec insertLanguageInterface 'ru', 'Hide MCM email content/distribution', 'Скрыть контент/рассылку электронной почты MCM', 'messages';
exec insertLanguageInterface 'uk', 'Hide MCM email content/distribution', 'Сховати контент/розсилку електронної пошти MCM', 'messages';

exec insertLanguageInterface 'en', 'Hide MCM sms content/distribution', 'Hide MCM sms content/distribution', 'messages';
exec insertLanguageInterface 'ru', 'Hide MCM sms content/distribution', 'Скрыть контент/рассылку смс MCM', 'messages';
exec insertLanguageInterface 'uk', 'Hide MCM sms content/distribution', 'Сховати контент/розсилку смс MCM', 'messages';

exec insertLanguageInterface 'en', 'Hide MCM viber content/distribution', 'Hide MCM viber content/distribution', 'messages';
exec insertLanguageInterface 'ru', 'Hide MCM viber content/distribution', 'Скрыть контент/рассылку viber MCM', 'messages';
exec insertLanguageInterface 'uk', 'Hide MCM viber content/distribution', 'Сховати контент/розсилку viber MCM', 'messages';

exec insertLanguageInterface 'en', 'Hide MCM whatsapp content/distribution', 'Hide MCM whatsapp content/distribution', 'messages';
exec insertLanguageInterface 'ru', 'Hide MCM whatsapp content/distribution', 'Скрыть контент/рассылку whatsapp MCM', 'messages';
exec insertLanguageInterface 'uk', 'Hide MCM whatsapp content/distribution', 'Сховати контент/розсилку whatsapp MCM', 'messages';

exec insertLanguageInterface 'en', 'Hide MCM telegram content/distribution', 'Hide MCM telegram content/distribution', 'messages';
exec insertLanguageInterface 'ru', 'Hide MCM telegram content/distribution', 'Скрыть контент/рассылку telegram MCM', 'messages';
exec insertLanguageInterface 'uk', 'Hide MCM telegram content/distribution', 'Сховати контент/розсилку telegram MCM', 'messages';
GO
----------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------
-- https://teamsoft.atlassian.net/browse/DRREDDYSRO-34
----------------------------------------------------------------------------------------------------
IF NOT EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.columns WHERE table_name = 'info_saleprojectprep' and column_name = 'discount_threshold_4')
BEGIN
ALTER TABLE info_saleprojectprep ADD discount_threshold_4 int NULL ;
END;
GO

IF NOT EXISTS(SELECT 1 FROM information_schema.columns WHERE table_name = 'info_saleprojectprep' and column_name = 'discount_4')
BEGIN
ALTER TABLE info_saleprojectprep ADD discount_4 money NULL ;
END;
GO
----------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------
--- https://teamsoft.atlassian.net/browse/EGISRU-456
----------------------------------------------------------------------------------------------------
exec insertLanguageInterface 'en', 'Not working', N'Not working', 'messages'
exec insertLanguageInterface 'ru', 'Not working', N'Не работает', 'messages'
exec insertLanguageInterface 'uk', 'Not working', N'Не працює', 'messages'
GO
----------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------

------------------------------------------------------------------------------------------------------------------------
-- https://teamsoft.atlassian.net/browse/GEODATA-822
------------------------------------------------------------------------------------------------------------------------
IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'info_user' AND COLUMN_NAME = 'hide_me_in_geo_force')
BEGIN
ALTER TABLE info_user ADD hide_me_in_geo_force INTEGER NULL
END
GO
------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------
-- https://teamsoft.atlassian.net/browse/OL-65
------------------------------------------------------------------------------------------------------------------------

if not exists(SELECT 1 FROM information_schema.columns WHERE table_name='info_mcmfilter' and column_name = 'subtitle')
begin
alter table info_mcmfilter add subtitle VARCHAR(255);
end;

if not exists(SELECT 1 FROM information_schema.columns WHERE table_name='info_mcmfilter' and column_name = 'additional_dict')
begin
alter table info_mcmfilter add additional_dict VARCHAR(255);
end;

if not exists(SELECT 1 FROM information_schema.columns WHERE table_name='info_mcmfilter' and column_name = 'depend_field')
begin
alter table info_mcmfilter add depend_field VARCHAR(255);
end;
GO
------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------

----------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------
-- https://teamsoft.atlassian.net/browse/CAMPINA-114
----------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------
if not exists (SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'info_contactsign' AND Column_Name = 'agreement_id')
BEGIN
ALTER TABLE info_contactsign ADD agreement_id int NULL
ALTER TABLE info_contactsign ADD CONSTRAINT FK_info_contactsign_info_dictionaryremote FOREIGN KEY (agreement_id) REFERENCES info_dictionaryremote(id)
END

exec insertLanguageInterface 'en', 'No data', 'No data', 'crm';
exec insertLanguageInterface 'ru', 'No data', 'Нет данных', 'crm';
exec insertLanguageInterface 'uk', 'No data', 'Немає даних', 'crm';

exec insertLanguageInterface 'en', 'Electronic agreement for the lecturer', 'Electronic agreement for the lecturer', 'crm';
exec insertLanguageInterface 'ru', 'Electronic agreement for the lecturer', 'Электронное соглашение для лектора', 'crm';
exec insertLanguageInterface 'uk', 'Electronic agreement for the lecturer', 'Електронна угода для лектора', 'crm';

exec insertLanguageInterface 'en', 'Specified', 'Specified', 'crm';
exec insertLanguageInterface 'ru', 'Specified', 'Указано', 'crm';
exec insertLanguageInterface 'uk', 'Specified', 'Вказано', 'crm';

exec deleteLanguageInterface 'Electronic signature', 'crm';

exec insertLanguageInterface 'en', 'Electronic signature', 'Electronic signature', 'crm';
exec insertLanguageInterface 'ru', 'Electronic signature', 'Электронная подпись', 'crm';
exec insertLanguageInterface 'uk', 'Electronic signature', 'Електронний підпис', 'crm';
GO
----------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------
-- https://teamsoft.atlassian.net/browse/NESTLERU-21
----------------------------------------------------------------------------------------------------
IF NOT EXISTS (SELECT 1 FROM info_serviceprivilege WHERE [code] = 'DCR_USE_MCM')
begin
    DECLARE @serviceid int, @previlageid int;

    SET @serviceid = (SELECT id FROM info_service WHERE identifier = 'dcr')
    SET @previlageid = (SELECT id FROM info_serviceprivilege WHERE parent_id IS NULL AND service_id = @serviceid)

    INSERT INTO info_serviceprivilege([name], code, service_id, parent_id)
    VALUES('Access MCM in DCR', 'DCR_USE_MCM', @serviceid, @previlageid)
end
GO

IF NOT EXISTS (SELECT 1 FROM info_serviceprivilege WHERE [code] = 'CRM_USE_MCM')
begin
    DECLARE @serviceid int, @previlageid int;

    SET @serviceid = (SELECT id FROM info_service WHERE identifier = 'crm')
    SET @previlageid = (SELECT id FROM info_serviceprivilege WHERE parent_id IS NULL AND service_id = @serviceid)

    INSERT INTO info_serviceprivilege([name], code, service_id, parent_id)
    VALUES('Access MCM in CRM', 'CRM_USE_MCM', @serviceid, @previlageid)
end
GO
----------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------
-- fix
----------------------------------------------------------------------------------------------------
IF NOT EXISTS(SELECT 1 FROM info_options WHERE name='taskCoordinatesDateDisplayPriority')
    BEGIN
        insert into info_options(name, value) values ('taskCoordinatesDateDisplayPriority', 1);
    END
update info_options set value = 1 where name = 'taskCoordinatesDateDisplayPriority';
GO
----------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------

---------------------------------------------------------------------------------------------------------
-- https://teamsoft.atlassian.net/browse/DRREDDYS-610
---------------------------------------------------------------------------------------------------------
IF OBJECT_ID(N'[po_apitoken]', 'U') IS NULL
BEGIN
CREATE TABLE  [dbo].[po_apitoken]
(
    [id]                [int] IDENTITY (1,1) NOT NULL,
    [token]             [varchar](255)       NULL,
    [expires_at]        [datetime]           NULL,
    [user_id]           [int]                NULL,
    [currenttime]       [datetime]           NULL,
    [time]              [timestamp]          NULL,
    [moduser]           [varchar]            NULL,
    [guid]              [uniqueidentifier]   NULL,
    CONSTRAINT [PK_po_apitoken] PRIMARY KEY CLUSTERED
(
[id] ASC
) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
    ) ON [PRIMARY]
ALTER TABLE [dbo].[po_apitoken]
    ADD CONSTRAINT fk_po_apitoken_user_id FOREIGN KEY (user_id) REFERENCES info_user (id);
ALTER TABLE [dbo].[po_apitoken]
    ADD CONSTRAINT [DF_po_apitokencurrenttime] DEFAULT (getdate()) FOR [currenttime]
ALTER TABLE [dbo].[po_apitoken]
    ADD CONSTRAINT [DF_po_apitokenmoduser] DEFAULT ([dbo].[Get_CurrentCode]()) FOR [moduser]
ALTER TABLE [dbo].[po_apitoken]
    ADD CONSTRAINT [DF_po_apitokenguid] DEFAULT (newid()) FOR [guid]
END
GO

declare
@SERVICE_PRIVILEGE_API_SCOPE_ALL int,
	@SERVICE_PRIVILEGE_API_SCOPE_USER_CURRENT int,
    @SERVICE_PRIVILEGE_API_SCOPE_OPTIONS int,
    @SERVICE_PRIVILEGE_API_SCOPE_RTC int,
    @ROLE_RTC_USER_ID int;

IF NOT EXISTS (SELECT 1 FROM info_serviceprivilege WHERE code = 'ROLE_API_SCOPE_ALL')
	INSERT INTO info_serviceprivilege(name, code, service_id, parent_id) VALUES ('API scope all', 'ROLE_API_SCOPE_ALL', null, null);

set @SERVICE_PRIVILEGE_API_SCOPE_ALL = (SELECT id FROM info_serviceprivilege WHERE code = 'ROLE_API_SCOPE_ALL');

IF NOT EXISTS (SELECT 1 FROM info_serviceprivilege WHERE code = 'ROLE_API_SCOPE_USER_CURRENT')
	INSERT INTO info_serviceprivilege(name, code, service_id, parent_id) VALUES ('API scope user current', 'ROLE_API_SCOPE_USER_CURRENT', null, null);

set @SERVICE_PRIVILEGE_API_SCOPE_USER_CURRENT = (SELECT id FROM info_serviceprivilege WHERE code = 'ROLE_API_SCOPE_USER_CURRENT');

IF NOT EXISTS (SELECT 1 FROM info_serviceprivilege WHERE code = 'ROLE_API_SCOPE_OPTIONS')
	INSERT INTO info_serviceprivilege(name, code, service_id, parent_id) VALUES ('API scope options', 'ROLE_API_SCOPE_OPTIONS', null, null);

set @SERVICE_PRIVILEGE_API_SCOPE_OPTIONS = (SELECT id FROM info_serviceprivilege WHERE code = 'ROLE_API_SCOPE_OPTIONS');

IF NOT EXISTS (SELECT 1 FROM info_serviceprivilege WHERE code = 'ROLE_API_SCOPE_RTC')
	INSERT INTO info_serviceprivilege(name, code, service_id, parent_id) VALUES ('API scope RTC', 'ROLE_API_SCOPE_RTC', null, null);

set @SERVICE_PRIVILEGE_API_SCOPE_RTC = (SELECT id FROM info_serviceprivilege WHERE code = 'ROLE_API_SCOPE_RTC');

insert into info_roleprivilege (privilege_id, role_id)
select @SERVICE_PRIVILEGE_API_SCOPE_ALL as privilege_id, id as role_id from info_role ir where code != 'ROLE_RTC_USER';

set @ROLE_RTC_USER_ID = (SELECT id FROM info_role WHERE code='ROLE_RTC_USER');
insert into info_roleprivilege (privilege_id, role_id)
values
(@SERVICE_PRIVILEGE_API_SCOPE_USER_CURRENT, @ROLE_RTC_USER_ID),
(@SERVICE_PRIVILEGE_API_SCOPE_OPTIONS, @ROLE_RTC_USER_ID),
(@SERVICE_PRIVILEGE_API_SCOPE_RTC, @ROLE_RTC_USER_ID);

GO
------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------
-- add translation
------------------------------------------------------------------------------------------------------------------------
exec insertLanguageInterface 'en', 'Country', N'Country', 'messages';
exec insertLanguageInterface 'ru', 'Country', N'Страна', 'messages';
exec insertLanguageInterface 'uk', 'Country', N'Країна', 'messages';
GO
------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------
-- https://teamsoft.atlassian.net/browse/DRREDDYSRO-45
----------------------------------------------------------------------------------------------------
IF NOT EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE table_name = 'info_expense' AND column_name = 'expensetype_id')
BEGIN
ALTER TABLE info_expense ADD expensetype_id int NULL;
ALTER TABLE info_expense ADD CONSTRAINT FK_info_expense_expensetype_id FOREIGN KEY (expensetype_id)
    REFERENCES info_CustomDictionaryValue (id) ON DELETE NO ACTION ON UPDATE NO ACTION;
END
GO

IF NOT EXISTS (SELECT 1 FROM information_schema.columns WHERE table_name='info_expensedetail' and column_name = 'receipt_number')
    BEGIN
        ALTER TABLE info_expensedetail ADD receipt_number varchar(255) NULL;
    END
GO

IF NOT EXISTS (SELECT 1 FROM information_schema.columns WHERE table_name='info_expensedetail' and column_name = 'receipt_dt')
    BEGIN
        ALTER TABLE info_expensedetail ADD receipt_dt datetime null;
    END
GO

/*
exec deleteLanguageInterface 'Returned', 'expense';
exec insertLanguageInterface 'en', 'Returned', 'Reopened', 'expense';
exec insertLanguageInterface 'ru', 'Returned', 'Переоткрыто', 'expense';
exec insertLanguageInterface 'uk', 'Returned', 'Перевідкрито', 'expense';
GO
*/
----------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------
-- add translations
----------------------------------------------------------------------------------------------------
exec insertLanguageInterface 'ru', 'Find', 'Найти', 'messages';
exec insertLanguageInterface 'uk', 'user', 'Користувач...', 'messages';
exec insertLanguageInterface 'uk', 'exit', 'Вийти', 'messages';
GO
----------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------
-- https://teamsoft.atlassian.net/browse/UKEKSPO-55
----------------------------------------------------------------------------------------------------
exec insertLanguageInterface 'en', 'Conference details', 'Conference details', 'mcm';
exec insertLanguageInterface 'ru', 'Conference details', 'Детали конференции', 'mcm';
exec insertLanguageInterface 'uk', 'Conference details', 'Подробиці конференції', 'mcm';
GO
----------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------
