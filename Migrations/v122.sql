update po_entityfields set name = 'Brands' where field = 'brends_id' and page = 8;
delete from po_entityfields where page = 8 and field = 'info_promomaterial';
insert into po_entityfields (page, field, name, [group]) values (8, 'info_promomaterial', 'Promo materials', 'reports');
update po_entityfields set [group] = NULL where field = 'department_id' and page = 4;
update po_entityfields set [group] = NULL where field = 'info_contactintask' and page = 4;
update po_entityfields set [group] = 'reports' where field = 'brendcomment' and page = 4;
update po_entityfields set [group] = 'reports' where field = 'countpart' and page = 4;
update po_entityfields set [group] = 'reports' where field = 'nexttaskpurpose' and page = 4;
update po_entityfields set [group] = 'reports' where field = 'info_task.contact_code' and page = 4;
update po_entityfields set [group] = 'reports' where field = 'info_taskcomment.call' and page = 4;
update po_entityfields set [group] = 'reports' where field = 'info_taskrcpa' and page = 4;
update po_entityfields set field = 'info_contactpotential.pervostolniki' where field = 'pervostolniki' and page = 4;
update po_entityfields set field = 'info_companypromo' where field = 'promocontrol' and page = 4;
update po_entityfields set field = 'info_goal.company_goals' where field = 'company_goals' and page = 4;
update po_entityfields set field = 'info_kamplan2.planfactkam' where field = 'planfactkam' and page = 4;
update po_entityfields set field = 'info_promoproject.action' where field = 'action' and page = 4;
update po_entityfields set field = 'info_tasktenderplan.tenderplan' where field = 'tenderplan' and page = 4;
update po_entityfields set field = 'info_promoproject.marketingProject' where field = 'marketingproject' and page = 4;
delete from po_entityfields where field = 'info_survey' and page = 4;
delete from po_entityfields where page = 4 and field = 'info_contactintask.lecturer';
insert into po_entityfields (page, field, name, [group]) values (4, 'info_contactintask.lecturer', 'Lecturers', NULL);
delete from po_entityfields where page = 4 and field = 'field_training_evaluation_form';
insert into po_entityfields (page, field, name, [group]) values (4, 'field_training_evaluation_form', 'Evaluation form', 'reports');
delete from po_entityfields where page = 4 and field = 'info_tasktenderfact.tenderfact';
insert into po_entityfields (page, field, name, [group]) values (4, 'info_tasktenderfact.tenderfact', 'KAM Agreement', 'reports');
delete from po_entityfields where page = 4 and field = 'taskresult';
insert into po_entityfields (page, field, name, [group]) values (4, 'taskresult', 'Task result', 'reports');
delete from po_entityfields where page = 4 and field = 'previous_task_result';
insert into po_entityfields (page, field, name, [group]) values (4, 'previous_task_result', 'Previous task result', 'reports');

------------------------------------------------------------------------------------------------------------------------
--- https://teamsoft.atlassian.net/browse/MCM-379
------------------------------------------------------------------------------------------------------------------------
exec insertLanguageInterface 'en', 'Available templates', 'Available templates', 'crm'
exec insertLanguageInterface 'ru', 'Available templates', N'Доступные шаблоны', 'crm'
exec insertLanguageInterface 'uk', 'Available templates', N'Доступні шаблони', 'crm'
GO

exec insertLanguageInterface 'en', 'Send', 'Send', 'crm'
exec insertLanguageInterface 'ru', 'Send', N'Отправить', 'crm'
exec insertLanguageInterface 'uk', 'Send', N'Надіслати', 'crm'
GO

exec insertLanguageInterface 'en', 'Communication with the client', 'Communication with the client', 'crm'
exec insertLanguageInterface 'ru', 'Communication with the client', N'Коммуникации с клиентом', 'crm'
exec insertLanguageInterface 'uk', 'Communication with the client', N'Комунікації з клієнтом', 'crm'
GO

exec insertLanguageInterface 'en', 'Sender name', 'Sender name', 'crm'
exec insertLanguageInterface 'ru', 'Sender name', N'Имя отправителя', 'crm'
exec insertLanguageInterface 'uk', 'Sender name', N'Ім''я відправника', 'crm'
GO

exec insertLanguageInterface 'en', 'Subject', 'Subject', 'crm'
exec insertLanguageInterface 'ru', 'Subject', N'Тема', 'crm'
exec insertLanguageInterface 'uk', 'Subject', N'Тема', 'crm'
GO

exec insertLanguageInterface 'en', 'Attached files', 'Attached files', 'crm'
exec insertLanguageInterface 'ru', 'Attached files', N'Вложенные файлы', 'crm'
exec insertLanguageInterface 'uk', 'Attached files', N'Вкладені файли', 'crm'
GO

exec insertLanguageInterface 'en', 'The message will be sent.', 'The message will be sent.', 'crm'
exec insertLanguageInterface 'ru', 'The message will be sent.', N'Сообщение будет отправлено.', 'crm'
exec insertLanguageInterface 'uk', 'The message will be sent.', N'Повідомлення буде відправлено.', 'crm'
GO

--IF NOT EXISTS(SELECT 1 FROM info_options WHERE [name]='mcmMassMailings')
--    INSERT INTO info_options(name, value)
--    VALUES ('mcmMassMailings', 0);

exec insertLanguageInterface 'en', 'Mass mailings', 'Mass mailings', 'crm'
exec insertLanguageInterface 'ru', 'Mass mailings', N'Массовые рассылки', 'crm'
exec insertLanguageInterface 'uk', 'Mass mailings', N'Масові розсилки', 'crm'
GO

IF NOT EXISTS(SELECT 1 FROM info_options WHERE [name]='contactVisitsHistoryDepth')
    INSERT INTO info_options(name, value)
    VALUES ('contactVisitsHistoryDepth', 6);


IF NOT EXISTS(SELECT 1 FROM info_options WHERE [name]='companyVisitsHistoryDepth')
    INSERT INTO info_options(name, value)
    VALUES ('companyVisitsHistoryDepth', 6);

exec insertLanguageInterface 'en', 'Selected distribution is not for mailing by medical representatives', 'Selected distribution is not for mailing by medical representatives', 'validators'
exec insertLanguageInterface 'ru', 'Selected distribution is not for mailing by medical representatives', N'Избранная рассылка не предназначена для рассылки медицинскими представителями', 'validators'
exec insertLanguageInterface 'uk', 'Selected distribution is not for mailing by medical representatives', N'Обрана розсилка не призначена для розсилки медичними представниками', 'validators'
GO

exec insertLanguageInterface 'en', 'Selected distribution has other period', 'Selected distribution has other period', 'validators'
exec insertLanguageInterface 'ru', 'Selected distribution has other period', N'Избранная рассылка имеет другой период', 'validators'
exec insertLanguageInterface 'uk', 'Selected distribution has other period', N'Обрана розсилка має інший період', 'validators'
GO

exec insertLanguageInterface 'en', 'Are you sure, you want to clear the list?', 'Are you sure, you want to clear the list?', 'crm'
exec insertLanguageInterface 'ru', 'Are you sure, you want to clear the list?', N'Вы уверены, что хотите очистить список?', 'crm'
exec insertLanguageInterface 'uk', 'Are you sure, you want to clear the list?', N'Ви впевнені, що хочете очистити список?', 'crm'
GO

exec insertLanguageInterface 'en', 'Next contacts have no channel for this distribution:', 'Next contacts have no channel for this distribution:', 'crm'
exec insertLanguageInterface 'ru', 'Next contacts have no channel for this distribution:', N'Следующие контакты не имеют канала для этой рассылки:', 'crm'
exec insertLanguageInterface 'uk', 'Next contacts have no channel for this distribution:', N'Наступні контакти не мають каналу для цієї розсилки:', 'crm'
GO


------------------------------------------------------------------------------------------------------------------------
---https://teamsoft.atlassian.net/browse/ACINO-204
------------------------------------------------------------------------------------------------------------------------
IF NOT EXISTS (SELECT 1 FROM information_schema.columns
                WHERE table_name = 'info_contactemail' AND column_name = 'from_site')
    BEGIN
        ALTER TABLE info_contactemail ADD from_site INT NULL
    END
GO
------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------


------------------------------------------------------------------------------------------------------------------------
---https://teamsoft.atlassian.net/browse/DRREDDYSBY-47
------------------------------------------------------------------------------------------------------------------------
exec insertLanguageInterface 'en', 'Types of', N'Types of', 'messages'
exec insertLanguageInterface 'ru', 'Types of', N'Типы', 'messages'
exec insertLanguageInterface 'uk', 'Types of', N'Типи', 'messages'
GO
exec insertLanguageInterface 'en', 'distributions', N'distributions', 'messages'
exec insertLanguageInterface 'ru', 'distributions', N'рассылок', 'messages'
exec insertLanguageInterface 'uk', 'distributions', N'розсилок', 'messages'
GO
------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------

------------------------------------------------------------------------------------------------------------------------
---fix MCM crush. ADD CONSTRAINT
------------------------------------------------------------------------------------------------------------------------
update info_mcmdistribution set mcmfrom_id = null where mcmfrom_id not in (select id from info_mcmfrom)
GO

IF EXISTS(select 1 from information_schema.constraint_column_usage where
        table_name='info_mcmdistribution' AND constraint_name='FK_info_mcmdistribution_mcmfrom_id')
    BEGIN
        ALTER TABLE info_mcmdistribution DROP CONSTRAINT FK_info_mcmdistribution_mcmfrom_id
    END
GO

ALTER TABLE info_mcmdistribution
      ADD CONSTRAINT FK_info_mcmdistribution_mcmfrom_id FOREIGN KEY (mcmfrom_id)
      REFERENCES info_mcmfrom (id)
------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------
--- https://teamsoft.atlassian.net/browse/DRREDDYS-491
--- https://teamsoft.atlassian.net/browse/DRREDDYS-443
------------------------------------------------------------------------------------------------------------------------
IF NOT EXISTS (SELECT 1 FROM information_schema.columns
               WHERE table_name = 'info_granualplan' AND column_name = 'direction_id')
    BEGIN
        ALTER TABLE info_granualplan ADD direction_id INT NULL
    END
GO

IF OBJECT_ID('dbo.[fk_info_granualplan_direction_id]', 'F') IS NULL
    BEGIN
        ALTER TABLE info_granualplan
            ADD CONSTRAINT fk_info_granualplan_direction_id FOREIGN KEY (direction_id)
                REFERENCES info_direction (id)
                ON DELETE NO ACTION
                ON UPDATE NO ACTION
    END
GO

exec insertLanguageInterface 'en', 'Write key words for start searching pharmacies', N'Write key words for start searching pharmacies', 'messages'
exec insertLanguageInterface 'ru', 'Write key words for start searching pharmacies', N'Напишите ключевые слова для поиска аптек', 'messages'
exec insertLanguageInterface 'uk', 'Write key words for start searching pharmacies', N'Напишіть ключові слова для початку пошуку аптек', 'messages'
GO

exec insertLanguageInterface 'en', 'Write key words for start searching preparations', N'Write key words for start searching preparations', 'messages'
exec insertLanguageInterface 'ru', 'Write key words for start searching preparations', N'Напишите ключевые слова для начала поиска препаратов', 'messages'
exec insertLanguageInterface 'uk', 'Write key words for start searching preparations', N'Запишіть ключові слова для початку пошуку препаратів', 'messages'
GO

exec insertLanguageInterface 'en', 'No matching pharmacy found', N'No matching pharmacy found', 'messages'
exec insertLanguageInterface 'ru', 'No matching pharmacy found', N'Подходящей аптеки не найдено', 'messages'
exec insertLanguageInterface 'uk', 'No matching pharmacy found', N'Відповідної аптеки не знайдено', 'messages'
GO

exec insertLanguageInterface 'en', 'Are you sure, you want to save current granular plan?', N'Are you sure, you want to save current granular plan?', 'messages'
exec insertLanguageInterface 'ru', 'Are you sure, you want to save current granular plan?', N'Вы уверены, что хотите сохранить текущий детальный план?', 'messages'
exec insertLanguageInterface 'uk', 'Are you sure, you want to save current granular plan?', N'Ви впевнені, що хочете зберегти поточний детальний план?', 'messages'
GO

exec insertLanguageInterface 'en', 'Save changes and load xls', N'Save changes and load xls', 'messages'
exec insertLanguageInterface 'ru', 'Save changes and load xls', N'Сохранить изменения и загрузить xls', 'messages'
exec insertLanguageInterface 'uk', 'Save changes and load xls', N'Зберегти зміни та завантажити xls', 'messages'
GO

exec insertLanguageInterface 'en', 'Load xls', N'Load xls', 'messages'
exec insertLanguageInterface 'ru', 'Load xls', N'Загрузить xls', 'messages'
exec insertLanguageInterface 'uk', 'Load xls', N'Завантажити xls', 'messages'
GO

exec insertLanguageInterface 'en', 'Filters affect csv data', N'Filters affect csv data', 'messages'
exec insertLanguageInterface 'ru', 'Filters affect csv data', N'Фильтры влияют на данные CSV', 'messages'
exec insertLanguageInterface 'uk', 'Filters affect csv data', N'Фільтри впливають на дані CSV', 'messages'
GO

exec insertLanguageInterface 'en', 'Confirm', N'Confirm', 'messages'
exec insertLanguageInterface 'ru', 'Confirm', N'Подтвердить', 'messages'
exec insertLanguageInterface 'uk', 'Confirm', N'Підтвердити', 'messages'
GO

exec insertLanguageInterface 'en', 'uah', N'uah', 'messages'
exec insertLanguageInterface 'ru', 'uah', N'грн', 'messages'
exec insertLanguageInterface 'uk', 'uah', N'грн', 'messages'
GO

exec insertLanguageInterface 'en', 'Sale period for granular plan not found. Please, create it.', N'Sale period for granular plan not found. Please, create it.', 'messages'
exec insertLanguageInterface 'ru', 'Sale period for granular plan not found. Please, create it.', N'Период продажи для детального плана не найден. Пожалуйста, создайте его.', 'messages'
exec insertLanguageInterface 'uk', 'Sale period for granular plan not found. Please, create it.', N'Період продажу для детального плану не знайдено. Будь ласка, створіть його.', 'messages'
GO
------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------

------------------------------------------------------------------------------------------------------------------------
---https://teamsoft.atlassian.net/browse/EGISRU-252
------------------------------------------------------------------------------------------------------------------------
exec insertLanguageInterface 'en', 'NSR Rate', N'HCP Rate', 'targeting'
exec insertLanguageInterface 'ru', 'NSR Rate', N'НСР Рейтинг', 'targeting'
exec insertLanguageInterface 'uk', 'NSR Rate', N'НСР Рейтинг', 'targeting'
GO
------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------
delete  from po_entityfields where field like '%nextaskpurpose%';
update po_entityfields set name = 'Date from of task' where field ='datefrom' and page = 4;
update po_entityfields set name = 'Date till of task' where field ='datetill' and page = 4;
update po_entityfields set [group] = 'reports' where field like '%training%' and page = 4;
update po_entityfields set name='SNILS' where name ='СНИЛС';
update po_entityfields set name='First Name' where name ='Firstname';
update po_entityfields set name='Last Name' where name ='Lastname';
update po_entityfields set name='Middle Name' where name ='Midlename';
update po_entityfields set name='KPP' where name ='КПП';
update info_languageinterface set [key] = 'Coordinates' where [key] = 'coordinates';
update info_languageinterface set [key] = 'Last Name' where [key] = 'Last name' and domain='messages';
update info_languageinterface set [key] = 'Middle Name' where [key] = 'Middle name' and domain='messages';
delete from info_languageinterface where [key]='Companies' and domain='messages' and translation in ('Аптеки', 'Установи', 'Pharmacies');
delete from info_languageinterface where [key]='Actions' and domain='messages' and translation in ('Дії', 'Действия');
delete from info_languageinterface where [key]='DV goals';
delete from info_languageinterface where [key]='Medical representative';


exec insertLanguageInterface 'uk', 'Date from of task', N'Дата початку візиту', 'messages';
exec insertLanguageInterface 'ru', 'Date from of task', N'Дата начала визита', 'messages';
exec insertLanguageInterface 'en', 'Date from of task', N'Date from of task', 'messages';
exec insertLanguageInterface 'uk', 'Date till of task', N'Дата закінчення візиту', 'messages';
exec insertLanguageInterface 'ru', 'Date till of task', N'Дата окончания визита', 'messages';
exec insertLanguageInterface 'en', 'Date till of task', N'Date till of task', 'messages';
exec insertLanguageInterface 'en', 'KPP', N'KPP', 'messages';
exec insertLanguageInterface 'ru', 'KPP', N'КПП', 'messages';
exec insertLanguageInterface 'uk', 'KPP', N'КПП', 'messages';
exec insertLanguageInterface 'en', 'SNILS', N'SNILS', 'messages';
exec insertLanguageInterface 'ru', 'SNILS', N'СНИЛС', 'messages';
exec insertLanguageInterface 'uk', 'SNILS', N'СНІЛС', 'messages';
exec insertLanguageInterface 'en', 'Correspondent account', N'Correspondent account', 'messages';
exec insertLanguageInterface 'ru', 'Correspondent account', N'Корреспондентский счет', 'messages';
exec insertLanguageInterface 'uk', 'Correspondent account', N'Кореспондентський рахунок', 'messages';
exec insertLanguageInterface 'en', 'Bank identification code (BIC)', N'Bank identification code (BIC)', 'messages';
exec insertLanguageInterface 'ru', 'Bank identification code (BIC)', N'Идентификационный код банка (BIC)', 'messages';
exec insertLanguageInterface 'uk', 'Bank identification code (BIC)', N'Ідентифікаційний код банку (BIC)', 'messages';
exec insertLanguageInterface 'en', 'Bank', N'Bank', 'messages';
exec insertLanguageInterface 'ru', 'Bank', N'Банк', 'messages';
exec insertLanguageInterface 'uk', 'Bank', N'Банк', 'messages';
exec insertLanguageInterface 'en', 'Taxpayer identification number', N'Taxpayer identification number', 'messages';
exec insertLanguageInterface 'ru', 'Taxpayer identification number', N'Идентификационный номер налогоплательщика', 'messages';
exec insertLanguageInterface 'uk', 'Taxpayer identification number', N'Ідентифікаційний номер платника податків', 'messages';
exec insertLanguageInterface 'en', 'Insurance certificate', N'Insurance certificate', 'messages';
exec insertLanguageInterface 'ru', 'Insurance certificate', N'Страховой сертификат', 'messages';
exec insertLanguageInterface 'uk', 'Insurance certificate', N'Страховий сертифікат', 'messages';
exec insertLanguageInterface 'en', 'Registration address', N'Registration address', 'messages';
exec insertLanguageInterface 'ru', 'Registration address', N'Адрес регистрации', 'messages';
exec insertLanguageInterface 'uk', 'Registration address', N'Адреса реєстрації', 'messages';
exec insertLanguageInterface 'en', 'Legal name', N'Legal name', 'messages';
exec insertLanguageInterface 'ru', 'Legal name', N'Юридическое название', 'messages';
exec insertLanguageInterface 'uk', 'Legal name', N'Юридична назва', 'messages';
exec insertLanguageInterface 'en', 'Interaction', N'Interaction', 'messages';
exec insertLanguageInterface 'ru', 'Interaction', N'Взаимодействие', 'messages';
exec insertLanguageInterface 'uk', 'Interaction', N'Взаємодія', 'messages';
exec insertLanguageInterface 'en', 'Legal status', N'Legal status', 'messages';
exec insertLanguageInterface 'ru', 'Legal status', N'Правовой статус', 'messages';
exec insertLanguageInterface 'uk', 'Legal status', N'Правовий статус', 'messages';
exec insertLanguageInterface 'en', 'Passport', N'Passport', 'messages';
exec insertLanguageInterface 'ru', 'Passport', N'Паспорт', 'messages';
exec insertLanguageInterface 'uk', 'Passport', N'Паспорт', 'messages';
exec insertLanguageInterface 'en', 'Series number', N'Series number', 'messages';
exec insertLanguageInterface 'ru', 'Series number', N'Номер серии', 'messages';
exec insertLanguageInterface 'uk', 'Series number', N'Номер серії', 'messages';
exec insertLanguageInterface 'en', 'When issued', N'When issued', 'messages';
exec insertLanguageInterface 'ru', 'When issued', N'Когда выдан', 'messages';
exec insertLanguageInterface 'uk', 'When issued', N'Коли виданий', 'messages';
exec insertLanguageInterface 'en', 'Issued by', N'Issued by', 'messages';
exec insertLanguageInterface 'ru', 'Issued by', N'Кем выдан', 'messages';
exec insertLanguageInterface 'uk', 'Issued by', N'Ким видано', 'messages';
exec insertLanguageInterface 'en', 'Coordinates', N'Coordinates', 'messages';
exec insertLanguageInterface 'ru', 'Coordinates', N'Координаты', 'messages';
exec insertLanguageInterface 'uk', 'Coordinates', N'Координати', 'messages';
exec insertLanguageInterface 'en', 'Series', N'Series', 'messages';
exec insertLanguageInterface 'ru', 'Series', N'Серия', 'messages';
exec insertLanguageInterface 'uk', 'Series', N'Серiя', 'messages';
exec insertLanguageInterface 'en', 'First Name', N'First Name', 'messages';
exec insertLanguageInterface 'ru', 'First Name', N'Имя', 'messages';
exec insertLanguageInterface 'uk', 'First Name', N'Ім''я', 'messages';
exec insertLanguageInterface 'en', 'Last Name', N'Last Name', 'messages';
exec insertLanguageInterface 'ru', 'Last Name', N'Фамилия', 'messages';
exec insertLanguageInterface 'uk', 'Last Name', N'Прізвище', 'messages';
exec insertLanguageInterface 'en', 'Middle Name', N'Middle Name', 'messages';
exec insertLanguageInterface 'ru', 'Middle Name', N'Отчество', 'messages';
exec insertLanguageInterface 'uk', 'Middle Name', N'По батькові', 'messages';
exec insertLanguageInterface 'en', 'Companies', N'Companies', 'messages';
exec insertLanguageInterface 'ru', 'Companies', N'Учреждения', 'messages';
exec insertLanguageInterface 'uk', 'Companies', N'Установи', 'messages';
exec insertLanguageInterface 'en', 'Actions', N'Actions', 'messages';
exec insertLanguageInterface 'ru', 'Actions', N'Задачи', 'messages';
exec insertLanguageInterface 'uk', 'Actions', N'Задачи', 'messages';
exec insertLanguageInterface 'en', 'DV goals', N'DV goals', 'messages';
exec insertLanguageInterface 'ru', 'DV goals', N'Цели ДВ', 'messages';
exec insertLanguageInterface 'uk', 'DV goals', N'Цілі ПВ', 'messages';
exec insertLanguageInterface 'en', 'Medical representative', N'Medical representative', 'messages';
exec insertLanguageInterface 'ru', 'Medical representative', N'Медицинский представитель', 'messages';
exec insertLanguageInterface 'uk', 'Medical representative', N'Медичний представник', 'messages';
GO
------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------
