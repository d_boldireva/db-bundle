---------------------------------------------------------------------------------------------------------
--https://teamsoft.atlassian.net/browse/UMJ-9
----------------------------------------------------------------------------------------------------------
IF NOT EXISTS(SELECT 1 FROM info_options WHERE name = 'linkFromMcmWebinarContact')
    INSERT INTO info_options (name, value) VALUES ('linkFromMcmWebinarContact', 0)
GO

IF OBJECT_ID(N'[mcm_webinar]', 'U') IS NULL
    BEGIN
        CREATE TABLE [dbo].[mcm_webinar]
        (
            [id]                [int]                IDENTITY(1,1) NOT NULL,
            [name]              [varchar](255)       NULL,
            [webinardate]       [datetime]           NULL,
            [webinarurl]        [varchar](255)       NULL,
            [webinarurlforconnect] [varchar](255)    NULL,
            [createdate]        [datetime]           default getdate(),
            [isarchive]         [int]                NULL,
            [currenttime]       [datetime]           NULL,
            [moduser]           [varchar](16)        NULL,
            [morionid]          [int]                NULL,
            CONSTRAINT [PK_mcm_webinar] PRIMARY KEY CLUSTERED ([id] ASC)
             WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
             ON [PRIMARY]
        )

        ALTER TABLE [dbo].[mcm_webinar]
            ADD CONSTRAINT [DF_mcm_webinarcurrenttime] DEFAULT (getdate()) FOR [currenttime]
        ALTER TABLE [dbo].[mcm_webinar]
            ADD CONSTRAINT [DF_mcm_webinarmoduser] DEFAULT [dbo].[Get_CurrentCode]() FOR [moduser]
    END
GO

if not exists(select *
            from dbo.sysobjects
            where id = object_id('moduser_mcm_webinar')
                and OBJECTPROPERTY(id, 'IsTrigger') = 1)
    exec ('create trigger moduser_mcm_webinar on mcm_webinar for insert, update as if not update(moduser)
        update mcm_webinar set moduser = dbo.Get_CurrentCode(), currenttime = getdate()
        where id in (select id from inserted)')
GO

IF OBJECT_ID(N'[mcm_webinarcontact]', 'U') IS NULL
    BEGIN
        CREATE TABLE [dbo].[mcm_webinarcontact]
        (
            [id]             [int]          IDENTITY(1,1) NOT NULL,
            [mcm_webinar_id] [int]          NULL,
             [morionid]      [int]          NULL,
             [currenttime]   [datetime]     NULL,
             [moduser]       [varchar](16)  NULL,
             [type]          [varchar](255) NULL,
             [link]          [varchar](255) NULL,
             [status_new]    [int]          NULL,
             CONSTRAINT [PK_mcm_webinarcontact] PRIMARY KEY CLUSTERED ([id] ASC)
             WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
             ON [PRIMARY]
        )

        ALTER TABLE [dbo].[mcm_webinarcontact]  WITH CHECK ADD CONSTRAINT [FK_mcm_webinarcontact_mcm_webinar_id]
            FOREIGN KEY([mcm_webinar_id]) REFERENCES [dbo].[mcm_webinar] ([id]) ON DELETE CASCADE ON UPDATE CASCADE
        ALTER TABLE [dbo].[mcm_webinarcontact]
            ADD CONSTRAINT [DF_mcm_webinarcontactcurrenttime] DEFAULT (getdate()) FOR [currenttime]
        ALTER TABLE [dbo].[mcm_webinarcontact]
            ADD CONSTRAINT [DF_mcm_webinarcontactmoduser] DEFAULT [dbo].[Get_CurrentCode]() FOR [moduser]
    END
GO

if not exists(select *
            from dbo.sysobjects
            where id = object_id('moduser_mcm_webinarcontact')
                and OBJECTPROPERTY(id, 'IsTrigger') = 1)
    exec ('create trigger moduser_mcm_webinarcontact on mcm_webinarcontact for insert, update as if not update(moduser)
        update mcm_webinarcontact set moduser = dbo.Get_CurrentCode(), currenttime = getdate()
        where id in (select id from inserted)')
GO

-----------------------------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------------------------

------------------------------------------------------------------------------------------------------------------------
-- https://teamsoft.atlassian.net/browse/PHARMAWEB-324
------------------------------------------------------------------------------------------------------------------------
EXEC insertLanguageInterface 'en', 'Supported Versions', N'Supported Versions', 'messages';
EXEC insertLanguageInterface 'ru', 'Supported Versions', N'Поддерживаемые версии', 'messages';
EXEC insertLanguageInterface 'uk', 'Supported Versions', N'Підтримувані версії', 'messages';
GO

EXEC insertLanguageInterface 'en', 'Not supported', N'Not supported', 'messages';
EXEC insertLanguageInterface 'ru', 'Not supported', N'Не поддерживается', 'messages';
EXEC insertLanguageInterface 'uk', 'Not supported', N'Не підтримується', 'messages';
GO

exec insertLanguageInterface 'en', 'from_option_version', 'From {{version}} version'
exec insertLanguageInterface 'ru', 'from_option_version', N'С {{version}} версии'
exec insertLanguageInterface 'uk', 'from_option_version', N'Від {{version}} версії'
GO
------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------

------------------------------------------------------------------------------------------------------------------------
-- fix translation
-----------------------------------------------------------------------------------------------------------
UPDATE info_languageinterface
SET translation = N'Отчеты'
WHERE translation = N'Отчетов' AND domain = 'messages' AND [key] = 'reports';
-----------------------------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------------------------

-----------------------------------------------------------------------------------------------------------
-- https://teamsoft.atlassian.net/browse/PHARMASALE-163
-----------------------------------------------------------------------------------------------------------
IF EXISTS(SELECT 1 FROM info_sales_file WHERE [table_name] LIKE '%.%')
    BEGIN
        DECLARE @table_name NVARCHAR(255)
        DECLARE @new_table_name NVARCHAR(255)
        DECLARE @table_name_escape NVARCHAR(255)
        DECLARE @sale_id INT
        DECLARE @pos INT
        DECLARE @count INT

        SELECT TOP 1 @table_name = table_name, @sale_id = id FROM info_sales_file WHERE [table_name] LIKE '%.%'

        WHILE (1=1)
            BEGIN
                SELECT @pos = CHARINDEX('.', @table_name)
                SELECT @table_name_escape = '[' + @table_name + ']'
                SELECT @new_table_name = SUBSTRING(@table_name, 1, @pos - 1)

                IF OBJECT_ID(@table_name_escape, 'U') IS NOT NULL
                    EXEC sp_rename @table_name_escape, @new_table_name, 'OBJECT'

                UPDATE info_sales_file SET table_name=@new_table_name WHERE id = @sale_id

                SELECT TOP 1 @table_name = table_name, @sale_id = id FROM info_sales_file WHERE [table_name] LIKE '%.%'
                SELECT @count = COUNT(id) FROM info_sales_file WHERE [table_name] LIKE '%.%'

                IF (@count = 0)
                    BREAK
            END
    END
GO

IF EXISTS(select 1 from information_schema.constraint_column_usage where
        table_name='ps_distributor_field' AND constraint_name='u_ps_distributor_field_name_type')
    BEGIN
        ALTER TABLE ps_distributor_field DROP CONSTRAINT u_ps_distributor_field_name_type
    END
GO

IF (OBJECT_ID('FK_ps_distributor_field_ps_pattern_types', 'F') IS NULL)
    BEGIN
        UPDATE  p
        SET [p].distributor_field_id = null
        FROM ps_pattern_field AS p
                 INNER JOIN ps_distributor_field AS d ON (d.id=p.distributor_field_id)
                 LEFT JOIN ps_pattern_types AS t ON(t.id=d.type_id)
        WHERE t.id IS NULL


        DELETE FROM ps_distributor_field WHERE type_id IS NULL

        ALTER TABLE ps_distributor_field
            ALTER COLUMN type_id int NOT NULL;

        ALTER TABLE ps_distributor_field
            ADD CONSTRAINT FK_ps_distributor_field_ps_pattern_types FOREIGN KEY (type_id)
                REFERENCES ps_pattern_types (id)
    END
GO

IF NOT EXISTS (SELECT 1 FROM sys.objects WHERE parent_object_id = OBJECT_ID(N'dbo.ps_distributor_field') AND name='u_ps_distributor_field_name_type')
    BEGIN
        ALTER TABLE ps_distributor_field ADD CONSTRAINT u_ps_distributor_field_name_type UNIQUE(field_name, type_id)
    END
GO

exec insertLanguageInterface 'en', 'Please, indicate the following fields: %fields%', 'Please, indicate the following fields: %fields%', 'messages'
exec insertLanguageInterface 'ru', 'Please, indicate the following fields: %fields%', N'Пожалуйста, укажите следующие поля: %fields%', 'messages'
exec insertLanguageInterface 'uk', 'Please, indicate the following fields: %fields%', N'Будь ласка, вкажіть наступні поля: %fields%', 'messages'
GO

exec insertLanguageInterface 'en', 'To synonyms', 'To synonyms', 'messages'
exec insertLanguageInterface 'ru', 'To synonyms', N'К синонимам', 'messages'
exec insertLanguageInterface 'uk', 'To synonyms', N'До синонімів', 'messages'
GO

DELETE FROM syn_org WHERE id IN (SELECT sy.id FROM syn_org AS sy LEFT JOIN ps_synorg_uploadfile psu on psu.company_hash = sy.company_hash WHERE psu.id IS NULL)
                      AND company_id IS NULL
                      AND org_id_morion IS NULL
GO
------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------

------------------------------------------------------------------------------------------------------------------------
--add column info_mcmdistributionaction
------------------------------------------------------------------------------------------------------------------------
IF NOT EXISTS(SELECT 1
              FROM INFORMATION_SCHEMA.COLUMNS
              WHERE table_name = 'info_mcmdistributionaction'
                AND column_name = 'user_id')
ALTER TABLE info_mcmdistributionaction
    ADD user_id int NULL
GO
------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------

---------------------------------------------------------------------------------------------------------
-- https://teamsoft.atlassian.net/browse/PHARMASALE-164
---------------------------------------------------------------------------------------------------------
IF NOT EXISTS(SELECT 1 FROM information_schema.columns WHERE table_name = 'ps_pattern_field' AND column_name = 'position')
    BEGIN
        ALTER TABLE ps_pattern_field ADD [position] INT NOT NULL DEFAULT 1
    end
GO

IF NOT EXISTS (SELECT 1 FROM sys.objects WHERE parent_object_id = OBJECT_ID(N'dbo.ps_distributor_field') AND name='u_ps_distributor_field_name_type')
    BEGIN
        ALTER TABLE ps_distributor_field ADD CONSTRAINT u_ps_distributor_field_name_type UNIQUE(field_name, type_id)
    END
GO

exec insertLanguageInterface'en', 'This value should not be blank.', 'This value should not be blank.', 'messages'
exec insertLanguageInterface 'ru', 'This value should not be blank.', N'Это значение не должно быть пустым.', 'messages'
exec insertLanguageInterface 'uk', 'This value should not be blank.', N'Це значення не повинно бути порожнім.', 'messages'
GO

exec insertLanguageInterface 'en', 'You field is too short.', 'You field is too short.', 'messages'
exec insertLanguageInterface 'ru', 'You field is too short.', N'Ваше поле слишком короткое.', 'messages'
exec insertLanguageInterface 'uk', 'You field is too short.', N'У вас поле занадто коротке.', 'messages'
GO

exec insertLanguageInterface 'en', 'You field is too long.', 'You field is too long.', 'messages'
exec insertLanguageInterface 'ru', 'You field is too long.', N'Ваше поле слишком длинное.', 'messages'
exec insertLanguageInterface 'uk', 'You field is too long.', N'У вас поле занадто довге.', 'messages'
GO

exec insertLanguageInterface 'en', 'All columns must have name', 'All columns must have name', 'messages'
exec insertLanguageInterface 'ru', 'All columns must have name', N'Все столбцы должны иметь имя', 'messages'
exec insertLanguageInterface 'uk', 'All columns must have name', N'Усі стовпці повинні мати ім’я', 'messages'
GO

exec insertLanguageInterface 'en', 'This file too big. Max allowed size: {maxSize}. File size {size}', 'This file too big. Max allowed size: {maxSize}. File size {size}', 'messages'
exec insertLanguageInterface 'ru', 'This file too big. Max allowed size: {maxSize}. File size {size}', N'Этот файл слишком большой. Максимально допустимый размер: {maxSize}. Размер файла: {size}', 'messages'
exec insertLanguageInterface 'uk', 'This file too big. Max allowed size: {maxSize}. File size {size}', N'Цей файл завеликий. Максимально допустимий розмір: {maxSize}. Розмір файла: {size}', 'messages'
GO

exec insertLanguageInterface 'en', 'Please, remove duplicated columns: %fields%', 'Please, remove duplicated columns: %fields%', 'messages'
exec insertLanguageInterface 'ru', 'Please, remove duplicated columns: %fields%', N'Пожалуйста, удалите задублированные колонки: %fields%', 'messages'
exec insertLanguageInterface 'uk', 'Please, remove duplicated columns: %fields%', N'Будь ласка, видаліть задубльовані колонки: %fields%', 'messages'
GO

exec insertLanguageInterface 'en',
     'Please, use columns for selected type only (%selected_type%). Column "%field_name%" belong to "%field_type%" type.',
     'Please, use columns for selected type only (%selected_type%). Column "%field_name%" belong to "%field_type%" type.',
     'messages'
exec insertLanguageInterface 'ru',
     'Please, use columns for selected type only (%selected_type%). Column "%field_name%" belong to "%field_type%" type.',
     N'Пожалуйста, используйте столбцы только для выбранного типа (%selected_type%). Колонка "%field_name%" относится к типу "%field_type%".',
     'messages'
exec insertLanguageInterface 'uk',
    'Please, use columns for selected type only (%selected_type%). Column "%field_name%" belong to "%field_type%" type.',
    N'Будь ласка, використовуйте колонки лише для вибраного типу (%selected_type%). Колонка "%field_name%" належить до типу "%field_type%".',
    'messages'
GO
---------------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------------

---------------------------------------------------------------------------------------------------------
--https://teamsoft.atlassian.net/browse/UMJ-11
----------------------------------------------------------------------------------------------------------
IF NOT EXISTS(SELECT 1
              FROM info_mcmtriggertype
              WHERE name = 'gettingWebinarPersonalLinks')
    INSERT INTO info_mcmtriggertype (name) VALUES ('gettingWebinarPersonalLinks')
GO

IF NOT EXISTS(SELECT 1
              FROM info_mcmcontentvariable
              WHERE code = 'WEBINARNAME')
    INSERT INTO info_mcmcontentvariable (name, code) VALUES ('Название вебинара', 'WEBINARNAME')
GO
IF NOT EXISTS(SELECT 1
              FROM info_mcmcontentvariable
              WHERE code = 'WEBINARLINK_PERS')
    INSERT INTO info_mcmcontentvariable (name, code) VALUES ('Персональная ссылка на вебинар', 'WEBINARLINK_PERS')
GO

IF NOT EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'mcm_webinarcontact' AND Column_Name = 'status_send')
    BEGIN
        ALTER TABLE mcm_webinarcontact
            ADD status_send int NULL
    END
GO

exec insertLanguageInterface  'en', 'Getting personal webinar link', N'Getting personal webinar link', 'messages'
exec insertLanguageInterface  'ru', 'Getting personal webinar link', N'Получение персональной ссылки на вебинар', 'messages'
exec insertLanguageInterface  'uk', 'Getting personal webinar link', N'Отримання персональних посилань на вебінар', 'messages'
GO
----------------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------------

------------------------------------------------------------------------------------------------------------------------
--https://teamsoft.atlassian.net/browse/PHARMAWEB-337
------------------------------------------------------------------------------------------------------------------------
exec insertLanguageInterface 'en', 'Target audiences', N'Target audiences', 'messages'
exec insertLanguageInterface 'ru', 'Target audiences', N'Целевые аудитории', 'messages'
exec insertLanguageInterface 'uk', 'Target audiences', N'Цільові аудиторії', 'messages'
GO
exec insertLanguageInterface 'en', 'Number of contact', N'Number of contact', 'messages'
exec insertLanguageInterface 'ru', 'Number of contact', N'Количество контактов', 'messages'
exec insertLanguageInterface 'uk', 'Number of contact', N'Кількість контактів', 'messages'
GO
exec insertLanguageInterface 'en', 'Total customers in the database CRM', N'Total customers in the database CRM', 'messages'
exec insertLanguageInterface 'ru', 'Total customers in the database CRM', N'Всего клиентов в базе CRM', 'messages'
exec insertLanguageInterface 'uk', 'Total customers in the database CRM', N'Всього клієнтів в базі CRM', 'messages'
GO
exec insertLanguageInterface 'en', 'No contact', N'No contact', 'messages'
exec insertLanguageInterface 'ru', 'No contact', N'Без контактов', 'messages'
exec insertLanguageInterface 'uk', 'No contact', N'Без контактів', 'messages'
GO
exec insertLanguageInterface 'en', 'With contacts', N'With contacts', 'messages'
exec insertLanguageInterface 'ru', 'With contacts', N'С контактами', 'messages'
exec insertLanguageInterface 'uk', 'With contacts', N'З контактами', 'messages'
GO
exec insertLanguageInterface 'en', 'Types', N'Types', 'messages'
exec insertLanguageInterface 'ru', 'Types', N'Типы', 'messages'
exec insertLanguageInterface 'uk', 'Types', N'Типи', 'messages'
GO
exec insertLanguageInterface 'en', 'recipient (s)', N'recipient (s)', 'messages'
exec insertLanguageInterface 'ru', 'recipient (s)', N'получателя(-лей)', 'messages'
exec insertLanguageInterface 'uk', 'recipient (s)', N'одержувач(-чів)', 'messages'
GO
exec insertLanguageInterface 'en', 'Please, select at least one filter', N'Please, select at least one filter', 'messages'
exec insertLanguageInterface 'uk', 'Please, select at least one filter', N'Виберіть принаймні один фільтр', 'messages'
GO
exec insertLanguageInterface 'en', 'of content', N'of of content', 'messages'
exec insertLanguageInterface 'ru', 'of content', N'of контента', 'messages'
exec insertLanguageInterface 'uk', 'of content', N'of контенту', 'messages'
GO
------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------

------------------------------------------------------------------------------------------------------------------------
---https://teamsoft.atlassian.net/browse/REDDYSRSA-174
------------------------------------------------------------------------------------------------------------------------
exec insertLanguageInterface  'ru', 'This value should not be blank.', N'Это значение не должно быть пустым', 'messages'
exec insertLanguageInterface  'uk', 'This value should not be blank.', N'Значення не повинно бути порожнiм', 'messages'
exec insertLanguageInterface  'en', 'This value should not be blank.', N'This value should not be blank.', 'messages'
GO
exec insertLanguageInterface  'ru', 'Links will be converted to the following format:', N'Ссылки будут преобразованы в формат', 'messages'
exec insertLanguageInterface  'en', 'Links will be converted to the following format:', N'Links will be converted to the following format:', 'messages'
exec insertLanguageInterface  'uk', 'Links will be converted to the following format:', N'Посилання будуть трансформованi в формат', 'messages'
GO
exec insertLanguageInterface  'ru', 'You''ll be able to get the click-through statictics for the link.', N', при этом вы сможете получить статистику переходов по ссылке.', 'messages'
exec insertLanguageInterface  'en', 'You''ll be able to get the click-through statictics for the link.', N'You''ll be able to get the click-through statictics for the link.', 'messages'
exec insertLanguageInterface  'uk', 'You''ll be able to get the click-through statictics for the link.', N', при цьому ви зможете отримати статистику вiдвiдувань за посиланням', 'messages'
GO
exec insertLanguageInterface  'ru', 'Links will not be converted.You won''t be able to track who followed the link', N'Ссылки не будут преобразованы, но при этом НЕЛЬЗЯ будет отследить кто переходил по ссылке.', 'messages'
exec insertLanguageInterface  'en', 'Links will not be converted.You won''t be able to track who followed the link', N'Links will not be converted.You won''t be able to track who followed the link', 'messages'
exec insertLanguageInterface  'uk', 'Links will not be converted.You won''t be able to track who followed the link', N'Посилання не будуть трансформованi. Ви не зможете вiдслiдкувати, хто преходив за посиланням', 'messages'
GO
exec insertLanguageInterface  'ru', 'Open new editor', N'Открыть новую версию редактора', 'messages'
exec insertLanguageInterface  'en', 'Open new editor', N'Open new editor', 'messages'
exec insertLanguageInterface  'uk', 'Open new editor', N'Нова версiя редактору', 'messages'
GO
exec insertLanguageInterface  'en', 'Click or drag the image here', N'Click or drag the image here', 'messages'
exec insertLanguageInterface  'ru', 'Click or drag the image here', N'Кликните или перетяните изображение сюда', 'messages'
exec insertLanguageInterface  'uk', 'Click or drag the image here', N'Натиснiть або перетянiть зображення', 'messages'
GO
exec insertLanguageInterface  'en', 'Upload', N'Upload', 'messages'
exec insertLanguageInterface  'ru', 'Upload', N'Загрузить', 'messages'
exec insertLanguageInterface  'uk', 'Upload', N'Завантажити', 'messages'
GO
exec insertLanguageInterface  'en', 'Close', N'Close', 'messages'
exec insertLanguageInterface  'ru', 'Close', N'Закрыть', 'messages'
exec insertLanguageInterface  'uk', 'Close', N'Зачинити', 'messages'
GO
exec insertLanguageInterface  'en', 'Save', N'Save', 'messages'
exec insertLanguageInterface  'ru', 'Save', N'Сохранить', 'messages'
exec insertLanguageInterface  'uk', 'Save', N'Зберегти', 'messages'
GO
exec insertLanguageInterface  'en', 'Old version', N'Old version', 'messages'
exec insertLanguageInterface  'ru', 'Old version', N'Вернуться к старой версии', 'messages'
exec insertLanguageInterface  'uk', 'Old version', N'Повернутися до старої версії', 'messages'
GO
exec insertLanguageInterface  'en', 'Text message', N'Text message', 'messages'
exec insertLanguageInterface  'ru', 'Text message', N'Тестовое сообщение', 'messages'
exec insertLanguageInterface  'uk', 'Text message', N'Текстове повiдомлення', 'messages'
GO
exec insertLanguageInterface  'en', 'Attach file', N'Attach file', 'messages'
exec insertLanguageInterface  'ru', 'Attach file', N'Прикрепить файл', 'messages'
exec insertLanguageInterface  'uk', 'Attach file', N'Прикріпити файл', 'messages'
GO
exec insertLanguageInterface  'en', 'Delete content', N'Delete content', 'messages'
exec insertLanguageInterface  'ru', 'Delete content', N'Удалить контент', 'messages'
exec insertLanguageInterface  'uk', 'Delete content', N'Delete content', 'messages'
GO
exec insertLanguageInterface  'en', 'Select element', N'Select element', 'messages'
exec insertLanguageInterface  'ru', 'Select element', N'Выбрать элемент', 'messages'
exec insertLanguageInterface  'uk', 'Select element', N'Вибрати елемент', 'messages'
GO
exec insertLanguageInterface  'en', 'Element properties', N'Element properties', 'messages'
exec insertLanguageInterface  'ru', 'Element properties', N'Свойства элемента', 'messages'
exec insertLanguageInterface  'uk', 'Element properties', N'Властивості елемента', 'messages'
GO
exec insertLanguageInterface  'en', 'Elements tree', N'Elements tree', 'messages'
exec insertLanguageInterface  'ru', 'Elements tree', N'Дерево элементов', 'messages'
exec insertLanguageInterface  'uk', 'Elements tree', N'Дерево елементів', 'messages'
GO
exec insertLanguageInterface  'en', 'New element', N'New element', 'messages'
exec insertLanguageInterface  'ru', 'New element', N'Новый элемент', 'messages'
exec insertLanguageInterface  'uk', 'New element', N'Новий елемент', 'messages'
GO
exec insertLanguageInterface  'en', 'Distribution statistics not avaialble', N'Distribution statistics not avaialble', 'messages'
exec insertLanguageInterface  'ru', 'Distribution statistics not avaialble', N'Статистика рассылки не доступна', 'messages'
exec insertLanguageInterface  'uk', 'Distribution statistics not avaialble', N'Статистика розсилки недоступна', 'messages'
GO
------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------

------------------------------------------------------------------------------------------------------------------------
---https://teamsoft.atlassian.net/browse/PHLINEUZ-3
------------------------------------------------------------------------------------------------------------------------
exec insertLanguageInterface 'en', 'Country', 'Country'
exec insertLanguageInterface 'ru', 'Country', N'Страна'
exec insertLanguageInterface 'uk', 'Country', N'Країна'
GO
------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------

------------------------------------------------------------------------------------------------------------------------
---https://teamsoft.atlassian.net/browse/MCM-343
------------------------------------------------------------------------------------------------------------------------
exec insertLanguageInterface 'en', 'You received this email because it is your email address',
                                   N'You received this email because it is your email address',
                                   'messages'
exec insertLanguageInterface 'ru', 'You received this email because it is your email address',
                                   N'Вы получили это письмо, так как Ваш электронный адрес',
                                   'messages'
exec insertLanguageInterface 'uk', 'You received this email because it is your email address',
                                   N'Ви отримали цей лист, оскільки Ваша електронна адреса',
                                   'messages'
GO
exec insertLanguageInterface 'en', 'specified as the mailing address of the mailing list',
                                   N'specified as the mailing address of the mailing list',
                                   'messages'
exec insertLanguageInterface 'ru', 'specified as the mailing address of the mailing list',
                                   N'значится подписчиком рассылки',
                                   'messages'
exec insertLanguageInterface 'uk', 'specified as the mailing address of the mailing list',
                                   N'зазначена як адреса підписника розсилки',
                                   'messages'
GO
exec insertLanguageInterface 'en', 'To unsubscribe, click here', N'To unsubscribe, click here', 'messages'
exec insertLanguageInterface 'ru', 'To unsubscribe, click here', N'Отписаться от рассылки', 'messages'
exec insertLanguageInterface 'uk', 'To unsubscribe, click here', N'Щоб відписатися, натисніть тут', 'messages'
GO
------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------

------------------------------------------------------------------------------------------------------------------------
---https://teamsoft.atlassian.net/browse/SANOFI-8
------------------------------------------------------------------------------------------------------------------------
IF NOT EXISTS(SELECT 1 FROM info_serviceprivilege WHERE [code] = 'ADMIN_ACCESS_TO_USER_HIERARCHY')
    INSERT INTO info_serviceprivilege (name, code, parent_id, service_id)
    VALUES (
               N'Access to user hierarchy',
               N'ADMIN_ACCESS_TO_USER_HIERARCHY',
               (SELECT TOP 1 id FROM info_serviceprivilege WHERE code = 'ROLE_ACCESS_TO_ADMINISTRATION'),
               (SELECT TOP 1 id FROM info_service WHERE identifier = 'administration')
           );
GO

IF NOT EXISTS(SELECT 1 FROM info_serviceprivilege WHERE [code] = 'ADMIN_ACCESS_TO_DICTIONARIES')
    INSERT INTO info_serviceprivilege (name, code, parent_id, service_id)
    VALUES (
               N'Access to dictionaries',
               N'ADMIN_ACCESS_TO_DICTIONARIES',
               (SELECT TOP 1 id FROM info_serviceprivilege WHERE code = 'ROLE_ACCESS_TO_ADMINISTRATION'),
               (SELECT TOP 1 id FROM info_service WHERE identifier = 'administration')
           );
GO

IF NOT EXISTS(SELECT 1 FROM info_serviceprivilege WHERE [code] = 'ADMIN_ACCESS_TO_ROLES')
    INSERT INTO info_serviceprivilege (name, code, parent_id, service_id)
    VALUES (
               N'Access to roles',
               N'ADMIN_ACCESS_TO_ROLES',
               (SELECT TOP 1 id FROM info_serviceprivilege WHERE code = 'ROLE_ACCESS_TO_ADMINISTRATION'),
               (SELECT TOP 1 id FROM info_service WHERE identifier = 'administration')
           );
GO

IF NOT EXISTS(SELECT 1 FROM info_serviceprivilege WHERE [code] = 'ADMIN_ACCESS_TO_SETTINGS')
    INSERT INTO info_serviceprivilege (name, code, parent_id, service_id)
    VALUES (
               N'Access to settings',
               N'ADMIN_ACCESS_TO_SETTINGS',
               (SELECT TOP 1 id FROM info_serviceprivilege WHERE code = 'ROLE_ACCESS_TO_ADMINISTRATION'),
               (SELECT TOP 1 id FROM info_service WHERE identifier = 'administration')
           );
GO

INSERT INTO info_roleprivilege(privilege_id, role_id)
select info_serviceprivilege.id, info_role.id
from info_role
cross join info_serviceprivilege
where info_role.id in (select role_id from info_roleprivilege r, info_serviceprivilege s where r.privilege_id = s.id and code = 'ROLE_ACCESS_TO_ADMINISTRATION')
and not exists (select 1 from info_roleprivilege r, info_serviceprivilege s where info_role.id = r.role_id and r.privilege_id = s.id and code LIKE 'ADMIN_ACCESS_TO%')
and info_serviceprivilege.code LIKE 'ADMIN_ACCESS_TO%'
GO

exec insertLanguageInterface 'ru', 'Access to dictionaries', N'Доступ к справочникам'
go
exec insertLanguageInterface 'uk', 'Access to dictionaries', N'Доступ до довідників'
go
exec insertLanguageInterface 'ru', 'Access to roles', N'Доступ к ролям'
go
exec insertLanguageInterface 'uk', 'Access to roles', N'Доступ до ролей'
go
exec insertLanguageInterface 'ru', 'Access to settings', N'Доступ к настройкам'
go
exec insertLanguageInterface 'uk', 'Access to settings', N'Доступ до налаштувань'
go
exec insertLanguageInterface 'ru', 'Access to user hierarchy', N'Доступ к иерархии пользователей'
go
exec insertLanguageInterface 'uk', 'Access to user hierarchy', N'Доступ до ієрархії користувачів'
go
------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------

------------------------------------------------------------------------------------------------------------------------
-- fix translation
-----------------------------------------------------------------------------------------------------------
exec insertLanguageInterface 'ru', 'Tab Control type', N'Типы вкладок', 'messages';
exec insertLanguageInterface 'uk', 'Tab Control type', N'Типи вкладок', 'messages';
exec insertLanguageInterface 'en', 'Tab Control type', N'Tab Control types', 'messages';

exec insertLanguageInterface 'ru', 'Add tab', N'Добавить вкладку', 'messages';
exec insertLanguageInterface 'uk', 'Add tab', N'Додати вкладку', 'messages';
exec insertLanguageInterface 'en', 'Add tab', N'Add tab', 'messages';

exec insertLanguageInterface 'ru', 'Name En', N'Название на Англ.', 'messages';
exec insertLanguageInterface 'uk', 'Name En', N'Назва на Англ.', 'messages';
exec insertLanguageInterface 'en', 'Name En', N'English name', 'messages';

exec insertLanguageInterface 'ru', 'Visible', N'Отображение', 'messages';
exec insertLanguageInterface 'uk', 'Visible', N'Відображення', 'messages';
exec insertLanguageInterface 'en', 'Visible', N'Visible', 'messages';

exec insertLanguageInterface 'ru', 'New tab', N'Новая вкладка', 'messages';
exec insertLanguageInterface 'uk', 'New tab', N'Нова вкладка', 'messages';
exec insertLanguageInterface 'en', 'New tab', N'New tab', 'messages';
GO
------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------

------------------------------------------------------------------------------------------------------------------------
---https://teamsoft.atlassian.net/browse/BIONORICA-12
------------------------------------------------------------------------------------------------------------------------
IF NOT EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE table_name = 'info_action' AND column_name = 'gps_accuracy')
    ALTER TABLE info_action ADD gps_accuracy int NULL
GO
------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------

-----------------------------------------------------------------------------------------------------------
-- fix translation TinyMCE button
-----------------------------------------------------------------------------------------------------------
exec insertLanguageInterface 'en', 'Personalization', N'Personalization', 'messages'
exec insertLanguageInterface 'ru', 'Personalization', N'Персонализация', 'messages'
exec insertLanguageInterface 'uk', 'Personalization', N'Персоналізація', 'messages'
GO
-----------------------------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------------------------

-----------------------------------------------------------------------------------------------------------
-- https://teamsoft.atlassian.net/browse/ALKALOIDRU-25
---------------------------------------------------------------------------------------------------------
IF NOT EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'info_mcmdistribution' AND Column_Name = 'gmt_offset')
BEGIN
ALTER TABLE info_mcmdistribution
    ADD gmt_offset INT NULL
END
GO
---------------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------------

------------------------------------------------------------------------------------------------------------------------
---https://teamsoft.atlassian.net/browse/MCM-350
------------------------------------------------------------------------------------------------------------------------
exec insertLanguageInterface  'en', 'Check the number with conversion links for transitions statistics',
                                     N'Check the number with conversion links for transitions statistics',
                                     'messages'
exec insertLanguageInterface  'ru', 'Check the number with conversion links for transitions statistics',
                                     N'Проверить кол-во с преобразованием ссылки для статистики переходов',
                                     'messages'
exec insertLanguageInterface  'uk', 'Check the number with conversion links for transitions statistics',
                                     N'Перевірити кількість з перетворенням посилання для статистики переходів',
                                     'messages'
GO

IF NOT EXISTS (SELECT 1 FROM information_schema.columns WHERE table_name = 'info_mcmdistribution' AND column_name = 'countsms')
    BEGIN
        ALTER TABLE info_mcmdistribution ADD countsms INT NULL
    END
GO
------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------

-----------------------------------------------------------------------------------------------------------
-- https://teamsoft.atlassian.net/browse/PHARMASALE-162
---------------------------------------------------------------------------------------------------------
exec insertLanguageInterface 'en', 'Unknown table for uploaded file {{file_name}}. This is a technical error, please, call support to solve this problem.',
    N'Unknown table for uploaded file {{file_name}}. This is a technical error, please, call support to solve this problem.', 'messages'
exec insertLanguageInterface 'ru', 'Unknown table for uploaded file {{file_name}}. This is a technical error, please, call support to solve this problem.',
    N'Неизвестная таблица для загруженного файла {{file_name}}. Это техническая ошибка, пожалуйста, обратитесь в службу поддержки, чтобы решить эту проблему', 'messages'
exec insertLanguageInterface 'uk', 'Unknown table for uploaded file {{file_name}}. This is a technical error, please, call support to solve this problem.',
    N'Невідома таблиця для завантаженого файлу {{file_name}}. Це технічна помилка. Будь ласка, зверніться до служби підтримки, щоб вирішити цю проблему.', 'messages'
GO

exec insertLanguageInterface 'en', 'Can not read uploaded file. Please, call support and inform about this problem.',
    N'Can not read uploaded file. Please, call support and inform about this problem.', 'messages'
exec insertLanguageInterface 'ru', 'Can not read uploaded file. Please, call support and inform about this problem.',
    N'Не удалось прочитать загруженный файл. Пожалуйста, обратитесь в службу поддержки и сообщите об этой проблеме.', 'messages'
exec insertLanguageInterface 'uk', 'Can not read uploaded file. Please, call support and inform about this problem.',
    N'Не вдається прочитати завантажений файл. Будь ласка, зверніться до служби підтримки та повідомте про цю проблему.', 'messages'
GO

exec insertLanguageInterface 'en', 'Name of the columns are not defined. Please check the uploading file.',
    N'Name of the columns are not defined. Please check the uploading file.', 'messages'
exec insertLanguageInterface 'ru', 'Name of the columns are not defined. Please check the uploading file.',
    N'Название столбцов не определено. Пожалуйста, проверьте файл загрузки.', 'messages'
exec insertLanguageInterface 'uk', 'Name of the columns are not defined. Please check the uploading file.',
    N'Назва стовпців не визначена. Перевірте файл для завантаження.', 'messages'
GO

------------------------------------------------------------------------------------------------------------------------
-- https://teamsoft.atlassian.net/browse/PHARMAWEB-269
------------------------------------------------------------------------------------------------------------------------
EXEC insertLanguageInterface 'ru', 'Surveys', N'Анкеты', 'crm';
EXEC insertLanguageInterface 'uk', 'Surveys', N'Анкети', 'crm';
EXEC insertLanguageInterface 'ru', 'Answers from previous task', N'Ответы из предыдущего визита', 'crm';
EXEC insertLanguageInterface 'uk', 'Answers from previous task', N'Відповіді з попереднього візиту', 'crm';
EXEC insertLanguageInterface 'ru', 'Multiple usage', N'Многократное использование', 'crm';
EXEC insertLanguageInterface 'uk', 'Multiple usage', N'Багаторазове використання', 'crm';
EXEC insertLanguageInterface 'ru', 'Show comment', N'Отображать комментарий', 'crm';
EXEC insertLanguageInterface 'uk', 'Show comment', N'Показувати коментар', 'crm';
EXEC insertLanguageInterface 'ru', 'Create question', N'Создать вопрос', 'crm';
EXEC insertLanguageInterface 'uk', 'Create question', N'Створити питання', 'crm';
EXEC insertLanguageInterface 'ru', 'Edit question', N'Редактировать вопрос', 'crm';
EXEC insertLanguageInterface 'uk', 'Edit question', N'Редагувати питання', 'crm';
EXEC insertLanguageInterface 'ru', 'Create filter', N'Создать фильтр', 'crm';
EXEC insertLanguageInterface 'uk', 'Create filter', N'Створити фільтр', 'crm';
EXEC insertLanguageInterface 'ru', 'Copy filter', N'Копировать фильтр', 'crm';
EXEC insertLanguageInterface 'uk', 'Copy filter', N'Копіювати фільтр', 'crm';
EXEC insertLanguageInterface 'ru', 'Edit filter', N'Редактировать фильтр', 'crm';
EXEC insertLanguageInterface 'uk', 'Edit filter', N'Редагувати фільтр', 'crm';
EXEC insertLanguageInterface 'ru', 'Create survey', N'Создать анкету', 'crm';
EXEC insertLanguageInterface 'uk', 'Create survey', N'Створити анкету', 'crm';
EXEC insertLanguageInterface 'ru', 'Edit survey', N'Редактировать анкету', 'crm';
EXEC insertLanguageInterface 'uk', 'Edit survey', N'Редагувати анкету', 'crm';
EXEC insertLanguageInterface 'ru', 'Copy survey', N'Копировать анкету', 'crm';
EXEC insertLanguageInterface 'uk', 'Copy survey', N'Копіювати анкету', 'crm';
EXEC insertLanguageInterface 'ru', 'Required', N'Обязательно', 'crm';
EXEC insertLanguageInterface 'uk', 'Required', N'Обов''язково', 'crm';
EXEC insertLanguageInterface 'ru', 'Questions', N'Вопросы', 'crm';
EXEC insertLanguageInterface 'uk', 'Questions', N'Питання', 'crm';
EXEC insertLanguageInterface 'ru', 'Answer type', N'Тип ответа', 'crm';
EXEC insertLanguageInterface 'uk', 'Answer type', N'Тип відповіді', 'crm';
EXEC insertLanguageInterface 'ru', 'Answers', N'Ответы', 'crm';
EXEC insertLanguageInterface 'uk', 'Answers', N'Відповіді', 'crm';
EXEC insertLanguageInterface 'ru', 'Answer', N'Ответ', 'crm';
EXEC insertLanguageInterface 'uk', 'Answer', N'Відповідь', 'crm';
EXEC insertLanguageInterface 'ru', 'Not required', N'Необязательно', 'crm';
EXEC insertLanguageInterface 'uk', 'Not required', N'Необов''язково', 'crm';
EXEC insertLanguageInterface 'ru', 'Are you sure you want to delete this survey?', N'Вы уверены, что хотите удалить выбранную анкету?', 'crm';
EXEC insertLanguageInterface 'uk', 'Are you sure you want to delete this survey?', N'Ви впевнені, що хочете видалити вибрану анкету?', 'crm';
EXEC insertLanguageInterface 'ru', 'Are you sure you want to delete this question?', N'Вы уверены, что хотите удалить выбранный вопрос?', 'crm';
EXEC insertLanguageInterface 'uk', 'Are you sure you want to delete this question?', N'Ви впевнені, що хочете видалити вибране питання?', 'crm';
EXEC insertLanguageInterface 'ru', 'Are you sure you want to delete this filter?', N'Вы уверены, что хотите удалить выбранный фильтр?', 'crm';
EXEC insertLanguageInterface 'uk', 'Are you sure you want to delete this filter?', N'Ви впевнені, що хочете видалити вибране фільтр?', 'crm';
EXEC insertLanguageInterface 'ru', 'Text', N'Текст', 'crm';
EXEC insertLanguageInterface 'uk', 'Text', N'Текст', 'crm';
EXEC insertLanguageInterface 'ru', 'Number', N'Число', 'crm';
EXEC insertLanguageInterface 'uk', 'Number', N'Число', 'crm';
EXEC insertLanguageInterface 'ru', 'List', N'Список', 'crm';
EXEC insertLanguageInterface 'uk', 'List', N'Список', 'crm';
EXEC insertLanguageInterface 'ru', 'Multiple list', N'Список (мультивыбор)', 'crm';
EXEC insertLanguageInterface 'uk', 'Multiple list', N'Список (мультивибір)', 'crm';
EXEC insertLanguageInterface 'ru', 'Add answer', N'Добавить ответ', 'crm';
EXEC insertLanguageInterface 'uk', 'Add answer', N'Додати відповідь', 'crm';
EXEC insertLanguageInterface 'ru', 'Input answer', N'Введите ответ', 'crm';
EXEC insertLanguageInterface 'uk', 'Input answer', N'Введіть відповідь', 'crm';
EXEC insertLanguageInterface 'ru', 'Questions not found', N'Вопросы не найдены', 'crm';
EXEC insertLanguageInterface 'uk', 'Questions not found', N'Питання не знайдено', 'crm';
EXEC insertLanguageInterface 'ru', 'Loading questions', N'Загрузка вопросов', 'crm';
EXEC insertLanguageInterface 'uk', 'Loading questions', N'Завантаження питань', 'crm';
EXEC insertLanguageInterface 'ru', 'Filters not found', N'Фильтры не найдены', 'crm';
EXEC insertLanguageInterface 'uk', 'Filters not found', N'Фильтри не знайдено', 'crm';
EXEC insertLanguageInterface 'ru', 'Loading filters', N'Загрузка фильтров', 'crm';
EXEC insertLanguageInterface 'uk', 'Loading filters', N'Завантаження фільтрів', 'crm';
EXEC insertLanguageInterface 'ru', 'empty', N'пусто', 'crm';
EXEC insertLanguageInterface 'uk', 'empty', N'пусто', 'crm';
EXEC insertLanguageInterface 'ru', 'not empty', N'не пусто', 'crm';
EXEC insertLanguageInterface 'uk', 'not empty', N'не пусто', 'crm';
EXEC insertLanguageInterface 'ru', 'equal', N'равно', 'crm';
EXEC insertLanguageInterface 'uk', 'equal', N'дорівнює', 'crm';
EXEC insertLanguageInterface 'ru', 'or', N'или', 'crm';
EXEC insertLanguageInterface 'uk', 'or', N'або', 'crm';
EXEC insertLanguageInterface 'ru', 'and', N'и', 'crm';
EXEC insertLanguageInterface 'uk', 'and', N'і', 'crm';
EXEC insertLanguageInterface 'ru', 'not equal', N'не равно', 'crm';
EXEC insertLanguageInterface 'uk', 'not equal', N'не дорівнює', 'crm';
EXEC insertLanguageInterface 'ru', 'Contact', N'Клиент', 'crm';
EXEC insertLanguageInterface 'uk', 'Contact', N'Клієнт', 'crm';
EXEC insertLanguageInterface 'ru', 'Conditions', N'Условия отображения', 'crm';
EXEC insertLanguageInterface 'uk', 'Conditions', N'Умови відображення', 'crm';
EXEC insertLanguageInterface 'ru', 'Add condition', N'Добавить условие', 'crm';
EXEC insertLanguageInterface 'uk', 'Add condition', N'Додати умову', 'crm';
EXEC insertLanguageInterface 'ru', 'Choose dictionary', N'Выберите справочник', 'crm';
EXEC insertLanguageInterface 'uk', 'Choose dictionary', N'Виберіть довідник', 'crm';
EXEC insertLanguageInterface 'ru', 'Operator', N'Оператор', 'crm';
EXEC insertLanguageInterface 'uk', 'Operator', N'Оператор', 'crm';
EXEC insertLanguageInterface 'ru', 'Dictionary', N'Справочник', 'crm';
EXEC insertLanguageInterface 'uk', 'Dictionary', N'Довідник', 'crm';
EXEC insertLanguageInterface 'ru', 'Type', N'Тип', 'crm';
EXEC insertLanguageInterface 'uk', 'Type', N'Тип', 'crm';
EXEC insertLanguageInterface 'ru', 'Value', N'Значение', 'crm';
EXEC insertLanguageInterface 'uk', 'Value', N'Значення', 'crm';
EXEC insertLanguageInterface 'ru', 'Access denied', N'Доступ запрещен', 'crm';
EXEC insertLanguageInterface 'uk', 'Access denied', N'Доступ заборонено', 'crm';

IF NOT EXISTS (SELECT 1 FROM information_schema.columns WHERE table_name = 'info_survey' AND column_name = 'page')
    BEGIN
        ALTER TABLE info_survey ADD page INT NULL
    END
GO
exec insertLanguageInterface 'ru', 'Show archived', N'Показать архивные', 'crm'
exec insertLanguageInterface 'uk', 'Show archived', N'Показати архівні', 'crm'
exec insertLanguageInterface 'ru', 'Survey moved to archive', N'Анкета перемещена в архив', 'crm'
exec insertLanguageInterface 'uk', 'Survey moved to archive', N'Анкету переміщено в архів', 'crm'
exec insertLanguageInterface 'ru', 'Survey will be archived because of existing answers. Continue?', N'Анкета будет перемещена в архив, так как существуют ответы. Продолжить?', 'crm'
exec insertLanguageInterface 'uk', 'Survey will be archived because of existing answers. Continue?', N'Анкету буде переміщено в архів через існуючі відповіді. Продовжити?', 'crm'


IF NOT EXISTS(SELECT 1
              FROM INFORMATION_SCHEMA.COLUMNS
              WHERE table_name = 'info_surveyfilter'
                AND column_name = 'json_filter')
ALTER TABLE info_surveyfilter ADD json_filter varchar(max) NULL
GO

IF NOT EXISTS(SELECT 1
              FROM INFORMATION_SCHEMA.COLUMNS
              WHERE table_name = 'info_surveyquestion'
                AND column_name = 'is_required')
ALTER TABLE info_surveyquestion ADD is_required int NULL
GO

IF OBJECT_ID(N'[info_filterdictionary]', 'U') IS NULL
    BEGIN
        CREATE TABLE [dbo].[info_filterdictionary]
        (
            [id]             [int]          IDENTITY(1,1) NOT NULL,
            [name]           [varchar](255) NULL,
            [field]          [varchar](255) NULL,
            [sql]            [varchar](255) NULL,
            [empty_sql]      [varchar](255) NULL,
            [sqlite]         [varchar](255) NULL,
            [empty_sqlite]   [varchar](255) NULL,
            [page]           [varchar](255) NULL,
            [dictionary_id]  [int]          NULL,
            [currenttime]    [datetime]           NULL,
            [time]           [timestamp]          NULL,
            [moduser]        [varchar](16)        NULL,
            [guid]           [uniqueidentifier]   NULL,
            CONSTRAINT [PK_info_filterdictionary] PRIMARY KEY CLUSTERED
                (
                 [id] ASC
                    ) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
        ) ON [PRIMARY]

        ALTER TABLE [dbo].[info_filterdictionary]
            ADD CONSTRAINT [DF_info_filterdictionarycurrenttime] DEFAULT (getdate()) FOR [currenttime]
        ALTER TABLE [dbo].[info_filterdictionary]
            ADD CONSTRAINT [DF_info_filterdictionarymoduser] DEFAULT ([dbo].[Get_CurrentCode]()) FOR [moduser]
        ALTER TABLE [dbo].[info_filterdictionary]
            ADD CONSTRAINT [DF_info_filterdictionaryguid] DEFAULT (newid()) FOR [guid]

        alter table info_filterdictionary
            add constraint info_filterdictionary_po_dictionary_id_fk
                foreign key (dictionary_id) references po_dictionary
                    on update set null on delete set null

        INSERT INTO info_filterdictionary (name, field, sql, empty_sql, page, dictionary_id) VALUES (N'User position', null, N':responsible_id in (select u.id from info_user u inner join info_dictionary d on u.position_id = d.id where d.guid #SIGN# #VALUE#)', N':responsible_id in (select id from info_user where position_id #SIGN#)', N'survey', (SELECT TOP 1 id FROM po_dictionary WHERE tablename = 'info_dictionary' AND identifier = 35));
        INSERT INTO info_filterdictionary (name, field, sql, empty_sql, page, dictionary_id) VALUES (N'Contact position', null, N':contact_id in (select c.id from info_contact c inner join info_dictionary p on c.position_id = p.id where p.guid #SIGN# #VALUE#)', N':contact_id in (select id from info_contact where position_id #SIGN#)', N'survey', null);
        INSERT INTO info_filterdictionary (name, field, sql, empty_sql, page, dictionary_id) VALUES (N'Responsible', null, N':responsible_id in (select id from info_user where guid #SIGN# #VALUE#)', N':responsible_id #SIGN#', N'survey', null);
        INSERT INTO info_filterdictionary (name, field, sql, empty_sql, page, dictionary_id) VALUES (N'City', null, N':company_id in (select c.id from info_company c inner join info_city ct on c.city_id = ct.id where ct.guid #SIGN# #VALUE#)', N':company_id in (select id from info_company where city_id #SIGN#)', N'survey', (SELECT TOP 1 id FROM po_dictionary WHERE tablename = 'info_city'));
        INSERT INTO info_filterdictionary (name, field, sql, empty_sql, page, dictionary_id) VALUES (N'Region', null, N':company_id in (select c.id from info_company c inner join info_region r on c.region_id = r.id where r.guid #SIGN# #VALUE#)', N':company_id in (select id from info_company where region_id #SIGN#)', N'survey', (SELECT TOP 1 id FROM po_dictionary WHERE tablename = 'info_region'));
        INSERT INTO info_filterdictionary (name, field, sql, empty_sql, page, dictionary_id) VALUES (N'Country', null, N':company_id in (select c.id from info_company c inner join info_country ct on c.country_id = ct.id where ct.guid #SIGN# #VALUE#)', N':company_id in (select id from info_company where country_id #SIGN#)', N'survey', (SELECT TOP 1 id FROM po_dictionary WHERE tablename = 'info_country'));
        INSERT INTO info_filterdictionary (name, field, sql, empty_sql, page, dictionary_id) VALUES (N'Specialization', null, N':specialization_id in (select id from info_dictionary where guid #SIGN# #VALUE#)', N':specialization_id #SIGN#', N'survey', null);
        INSERT INTO info_filterdictionary (name, field, sql, empty_sql, page, dictionary_id) VALUES (N'Task type', null, N':tasktype_id in (select id from info_tasktype where guid #SIGN# #VALUE#)', N':tasktype_id #SIGN#', N'survey', (SELECT TOP 1 id FROM po_dictionary WHERE tablename = 'info_tasktype'));
        INSERT INTO info_filterdictionary (name, field, sql, empty_sql, page, dictionary_id) VALUES (N'Contact type', null, N':contact_id in (select c.id from info_contact c inner join info_contacttype ct on c.contacttype_id = ct.id where ct.guid #SIGN# #VALUE#)', N':contact_id in (select id from info_contact where contacttype_id #SIGN#)', N'survey', (SELECT TOP 1 id FROM po_dictionary WHERE tablename = 'info_contacttype'));
        INSERT INTO info_filterdictionary (name, field, sql, empty_sql, page, dictionary_id) VALUES (N'Company type', null, N':company_id in (select c.id from info_company c inner join info_companytype ct on c.companytype_id = ct.id where ct.guid #SIGN# #VALUE#)', N':company_id in (select id from info_company where companytype_id #SIGN#)', N'survey', (SELECT TOP 1 id FROM po_dictionary WHERE tablename = 'info_companytype'));
    END
GO

IF NOT EXISTS(SELECT 1 FROM info_serviceprivilege WHERE [code] = 'ROLE_ACCESS_TO_SURVEY_MANAGER')
    INSERT INTO info_serviceprivilege ([name], code, service_id, parent_id)
    VALUES (
               'Access to surveys manager',
               'ROLE_ACCESS_TO_SURVEY_MANAGER',
               (SELECT id FROM info_service WHERE identifier = 'crm'),
               (SELECT MIN(id) FROM info_serviceprivilege WHERE code = 'ROLE_ACCESS_TO_CRM')
           )

    EXEC insertLanguageInterface 'ru', 'Access denied', N'Доступ к менеджеру анкет';
    EXEC insertLanguageInterface 'uk', 'Access denied', N'Доступ до менеджера анкет';
GO
------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------

------------------------------------------------------------------------------------------------------------------------
---https://teamsoft.atlassian.net/browse/MCM-353
------------------------------------------------------------------------------------------------------------------------
IF NOT EXISTS (SELECT 1 FROM information_schema.columns WHERE table_name = 'info_mcmdistributionaction' AND column_name = 'price')
    BEGIN
        ALTER TABLE info_mcmdistributionaction ADD price float NULL
    END
GO
------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------

------------------------------------------------------------------------------------------------------------------------
---https://teamsoft.atlassian.net/browse/MCM-336
------------------------------------------------------------------------------------------------------------------------
IF NOT EXISTS (SELECT 1 FROM information_schema.columns
                WHERE table_name = 'info_mcmdistribution' AND column_name = 'isarchive')
    BEGIN
        ALTER TABLE info_mcmdistribution ADD isarchive INT NULL
    END
GO

exec insertLanguageInterface 'en', 'Distribution has been successfully archived', N'Distribution has been successfully archived', 'messages'
exec insertLanguageInterface 'ru', 'Distribution has been successfully archived', N'Рассылка была успешно заархивирована', 'messages'
exec insertLanguageInterface 'uk', 'Distribution has been successfully archived', N'Розсилка була успішно заархівована', 'messages'
GO
exec insertLanguageInterface 'en', 'Content has been successfully unarchived', N'Content has been successfully unarchived', 'messages'
exec insertLanguageInterface 'ru', 'Content has been successfully unarchived', N'Контент был успешно разархивирован', 'messages'
exec insertLanguageInterface 'uk', 'Content has been successfully unarchived', N'Контент був успішно розархівований', 'messages'
GO
exec insertLanguageInterface 'en', 'Distribution has been successfully unarchived', N'Distribution has been successfully unarchived', 'messages'
exec insertLanguageInterface 'ru', 'Distribution has been successfully unarchived', N'Рассылка была успешно разархивирована', 'messages'
exec insertLanguageInterface 'uk', 'Distribution has been successfully unarchived', N'Розсилка була успішно розархівована', 'messages'
GO
exec insertLanguageInterface 'en', 'Distribution was not archived', N'Distribution was not archived', 'messages'
exec insertLanguageInterface 'ru', 'Distribution was not archived', N'Рассылка не была заархивирована', 'messages'
exec insertLanguageInterface 'uk', 'Distribution was not archived', N'Розсилка не була заархівована', 'messages'
GO
exec insertLanguageInterface 'en', 'Distribution was not unarchived', N'Distribution was not unarchived', 'messages'
exec insertLanguageInterface 'ru', 'Distribution was not unarchived', N'Рассылка не была разархивирована', 'messages'
exec insertLanguageInterface 'uk', 'Distribution was not unarchived', N'Розсилка не була розархівована', 'messages'
GO
exec insertLanguageInterface 'en', 'Content was not unarchived', N'Content was not unarchived', 'messages'
exec insertLanguageInterface 'ru', 'Content was not unarchived', N'Контент не был разархивирован', 'messages'
exec insertLanguageInterface 'uk', 'Content was not unarchived', N'Контент не був розархівований', 'messages'
GO
------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------

------------------------------------------------------------------------------------------------------------------------
---https://teamsoft.atlassian.net/browse/DRREDDYSMM-45
------------------------------------------------------------------------------------------------------------------------
IF NOT EXISTS (SELECT 1 FROM information_schema.columns
                WHERE table_name = 'info_contactphone' AND column_name = 'country_phone_mask')
    BEGIN
        ALTER TABLE info_contactphone ADD country_phone_mask INT NULL
    END
GO
------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------

------------------------------------------------------------------------------------------------------------------------
---https://teamsoft.atlassian.net/browse/DRREDDYSKZ-100
------------------------------------------------------------------------------------------------------------------------
exec insertLanguageInterface  'en', 'Not sent', N'Not sent', 'messages'
exec insertLanguageInterface  'ru', 'Not sent', N'Не отправлено', 'messages'
exec insertLanguageInterface  'uk', 'Not sent', N'Не відправлено', 'messages'
GO

exec insertLanguageInterface  'uk', 'RESENDING_SMS_DELIVERED', N'Перевідправка смс - доставлено', 'messages'
exec insertLanguageInterface  'uk', 'RESENDING_SMS_SENT', N'Перевідправка смс - надіслано', 'messages'
exec insertLanguageInterface  'uk', 'RESENDING_SMS_VISITED', N'Перевідправка смс - відвідано', 'messages'
GO
------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------

---------------------------------------------------------------------------------------------------------
-- fix translate
---------------------------------------------------------------------------------------------------------
delete from info_languageinterface where domain = 'geomarketing' and [key] in ('Zoom to all companies', 'Search', 'regions', 'users')
GO

exec insertLanguageInterface 'en', 'Zoom to all companies', 'Zoom to all companies', 'geomarketing'
exec insertLanguageInterface 'ru', 'Zoom to all companies', N'Масштабировать по всем организациям', 'geomarketing'
exec insertLanguageInterface 'uk', 'Zoom to all companies', N'Маштабувати за всіма організаціями', 'geomarketing'
GO

exec insertLanguageInterface 'en', 'Search', 'Search', 'geomarketing'
exec insertLanguageInterface 'ru', 'Search', N'Поиск', 'geomarketing'
exec insertLanguageInterface 'uk', 'Search', N'Пошук', 'geomarketing'
GO

exec insertLanguageInterface 'en', 'Users', 'Users', 'geomarketing'
exec insertLanguageInterface 'ru', 'Users', N'Пользователи', 'geomarketing'
exec insertLanguageInterface 'uk', 'Users', N'Користувачі', 'geomarketing'
GO

exec insertLanguageInterface 'en', 'Regions', 'Regions', 'geomarketing'
exec insertLanguageInterface 'ru', 'Regions', N'Области', 'geomarketing'
exec insertLanguageInterface 'uk', 'Regions', N'Області', 'geomarketing'
GO

exec insertLanguageInterface 'en', 'outside of direct hierarchy', 'outside of direct hierarchy', 'geomarketing'
exec insertLanguageInterface 'ru', 'outside of direct hierarchy', N'пользователем выше по иерархии', 'geomarketing'
exec insertLanguageInterface 'uk', 'outside of direct hierarchy', N'користувачем више за ієрархією', 'geomarketing'
GO
-------------------------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------------------
