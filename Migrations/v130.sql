------------------------------------------------------------------------------------------------------------------------
---https://teamsoft.atlassian.net/browse/UKEKSPO-56
------------------------------------------------------------------------------------------------------------------------
exec insertLanguageInterface 'en', 'Stream Url RU', N'Stream Url RU:', 'messages'
exec insertLanguageInterface 'ru', 'Stream Url RU', N'Ссылка на стрим (русский)', 'messages'
exec insertLanguageInterface 'uk', 'Stream Url RU', N'Посилання на стрім (російський)', 'messages'

exec insertLanguageInterface 'en', 'Stream Url En', N'Stream Url En:', 'messages'
exec insertLanguageInterface 'ru', 'Stream Url En', N'Ссылка на стрим (английский)', 'messages'
exec insertLanguageInterface 'uk', 'Stream Url En', N'Посилання на стрім (англійський)', 'messages'

exec insertLanguageInterface 'en', 'Stream Url UA', N'Stream Url UA:', 'messages'
exec insertLanguageInterface 'ru', 'Stream Url UA', N'Ссылка на стрим (украинский)', 'messages'
exec insertLanguageInterface 'uk', 'Stream Url UA', N'Посилання на стрім (український)', 'messages'

exec insertLanguageInterface 'en', 'Add stream', N'Add stream', 'messages'
exec insertLanguageInterface 'ru', 'Add stream', N'Добавить ссылкy на стрим', 'messages'
exec insertLanguageInterface 'uk', 'Add stream', N'Додати посилання на стрім', 'messages'

IF NOT EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE table_name = 'info_mcmconference' AND column_name = 'url_en')
alter table info_mcmconference add url_en varchar(255) null;
GO

IF NOT EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE table_name = 'info_mcmconference' AND column_name = 'url_ua')
alter table info_mcmconference add url_ua varchar(255) null;
GO
------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------

------------------------------------------------------------------------------------------------------------------------
------------------https://teamsoft.atlassian.net/browse/DRREDDYSRO-9
------------------------------------------------------------------------------------------------------------------------
-- add info_service record
IF NOT EXISTS(SELECT 1
              FROM info_service
              WHERE identifier = 'promo_project')
BEGIN
INSERT INTO info_service (identifier, name, description, enable)
VALUES ('promo_project', 'Promo Projects', 'Manage promo projects', 1)
END
GO

-- add info_serviceprivilege record
IF NOT EXISTS(SELECT 1
              FROM info_serviceprivilege
              WHERE code = 'ROLE_ACCESS_TO_PROMO_PROJECT')
BEGIN
INSERT INTO info_serviceprivilege (name, description, code, service_id)
VALUES ('Access to promo projects', 'Access to promo project service', 'ROLE_ACCESS_TO_PROMO_PROJECT',
        (SELECT TOP 1 id FROM info_service WHERE identifier = 'promo_project'))
END
GO

IF NOT EXISTS(SELECT 1 FROM information_schema.columns WHERE table_name = 'po_webservice' AND column_name = 'roles')
BEGIN
    ALTER TABLE po_webservice ADD [roles] VARCHAR(max) NULL
END
GO

-- add po_webservice record
IF NOT EXISTS(SELECT 1
              FROM po_webservice
              WHERE service_id = (SELECT TOP 1 id FROM info_service WHERE info_service.identifier = 'promo_project'))
BEGIN
INSERT INTO po_webservice (identifier, enable, allow_if, route, url, name, roles, service_id)
SELECT TOP 1 s.identifier                              AS [identifier],
                     1                                         AS [enable],
                     'is_granted("' + i.code + '") == true'    AS [allow_if],
                     'team_soft_' + s.identifier + '_homepage' AS [route],
                     '/{_locale}/promo-project'                AS [url],
                     s.name                                    AS [name],
                     '{"' + i.code + '"}:[]'                   AS [roles],
                     s.id                                      AS [service_id]
FROM info_service s
    INNER JOIN info_serviceprivilege i on s.id = i.service_id
WHERE s.identifier = 'promo_project'
END
GO

IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'info_promoprojectbrend' AND COLUMN_NAME = 'prepline_id')
BEGIN
ALTER TABLE info_promoprojectbrend ADD prepline_id INT NULL
END
GO

IF OBJECT_ID('fk_info_promoprojectbrend_prepline_id') IS NULL
BEGIN
ALTER TABLE info_promoprojectbrend WITH CHECK ADD CONSTRAINT fk_info_promoprojectbrend_prepline_id
    FOREIGN KEY (prepline_id) REFERENCES info_prepline (id) ON DELETE SET NULL;
ALTER TABLE info_promoprojectbrend CHECK CONSTRAINT fk_info_promoprojectbrend_prepline_id
END
GO

IF OBJECT_ID(N'info_promoprojectdirection') IS NULL BEGIN
CREATE TABLE [dbo].[info_promoprojectdirection]
(
    [id]                [int] IDENTITY (1,1) NOT NULL,
    [direction_id]      [int]                NULL,
    [promoproject_id]   [int]                NULL,
    [currenttime]       [datetime]           NULL,
    [time]              [timestamp]          NULL,
    [moduser]           [varchar](16)        NULL,
    [guid]              [uniqueidentifier]   NULL,
    CONSTRAINT [PK_info_promoprojectdirection] PRIMARY KEY CLUSTERED
(
[id] ASC
) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
    ) ON [PRIMARY]

ALTER TABLE [dbo].[info_promoprojectdirection] ADD CONSTRAINT [DF_info_promoprojectdirection_currnttime] DEFAULT (getdate()) FOR [currenttime]
ALTER TABLE [dbo].[info_promoprojectdirection] ADD CONSTRAINT [DF_info_promoprojectdirection_moduser] DEFAULT ([dbo].[Get_CurrentCode]()) FOR [moduser]
ALTER TABLE [dbo].[info_promoprojectdirection] ADD CONSTRAINT [DF_info_promoprojectdirection_guid] DEFAULT (newid()) FOR [guid]
ALTER TABLE [dbo].[info_promoprojectdirection] WITH CHECK
    ADD CONSTRAINT [FK_info_promoprojectdirection_direction_id] FOREIGN KEY ([direction_id]) REFERENCES [dbo].[info_direction] ([id]) ON DELETE CASCADE
ALTER TABLE [dbo].[info_promoprojectdirection] WITH CHECK
    ADD CONSTRAINT [FK_info_promoprojectdirection_promoproject_id] FOREIGN KEY ([promoproject_id]) REFERENCES [dbo].[info_promoproject] ([id]) ON DELETE CASCADE
end
GO
------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------

------------------------------------------------------------------------------------------------------------------------
-- https://teamsoft.atlassian.net/browse/DRREDDYSRO-7
------------------------------------------------------------------------------------------------------------------------
IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'info_expense' AND COLUMN_NAME = 'line_pm_status')
BEGIN
ALTER TABLE info_expense ADD line_pm_status INT NULL
END
GO

IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'info_expense' AND COLUMN_NAME = 'line_pm_comment')
BEGIN
ALTER TABLE info_expense ADD line_pm_comment VARCHAR(255) NULL
END
GO

IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'info_expense' AND COLUMN_NAME = 'moduser_line_pm')
BEGIN
ALTER TABLE info_expense ADD moduser_line_pm VARCHAR(16) NULL
END
GO

IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'info_expense' AND COLUMN_NAME = 'modified_line_pm')
BEGIN
ALTER TABLE info_expense ADD modified_line_pm DATETIME NULL
END
GO

IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'info_expense' AND COLUMN_NAME = 'task_id')
BEGIN
ALTER TABLE info_expense ADD task_id INT NULL
END
GO

IF OBJECT_ID('FK_info_expense_task_id') IS NULL
BEGIN
ALTER TABLE info_expense WITH CHECK ADD CONSTRAINT FK_info_expense_task_id
    FOREIGN KEY (task_id) REFERENCES info_task (id) ON DELETE SET NULL;
ALTER TABLE info_expense CHECK CONSTRAINT FK_info_expense_task_id
END
GO

IF NOT EXISTS (SELECT 1 FROM info_serviceprivilege WHERE code = 'ROLE_ACCESS_TO_EXPENSES_LINE_PM') BEGIN
    INSERT INTO info_serviceprivilege (name, description, code, service_id, parent_id)
    values ('Access to expenses line PM',   --name
            null,                           --description
            'ROLE_ACCESS_TO_EXPENSES_LINE_PM',  --code
            (select top 1 id from info_service where identifier = 'expenses'), --service_id
            (select top 1 id from info_serviceprivilege where code = 'ROLE_ACCESS_TO_EXPENSES') --parent_id
        )
END
GO

--User
exec insertLanguageInterface 'en', 'User', 'User', 'expense';
exec insertLanguageInterface 'ru', 'User', 'Пользователь', 'expense';
exec insertLanguageInterface 'uk', 'User', 'Користувач', 'expense';

--Expense type
exec insertLanguageInterface 'en', 'Expense type', 'Expense type', 'expense';
exec insertLanguageInterface 'ru', 'Expense type', 'Тип расходов', 'expense';
exec insertLanguageInterface 'uk', 'Expense type', 'Тип витрат', 'expense';

--Search
exec insertLanguageInterface 'en', 'Search', 'Search', 'expense';
exec insertLanguageInterface 'ru', 'Search', 'Поиск', 'expense';
exec insertLanguageInterface 'uk', 'Search', 'Пошук', 'expense';

--Pending
exec insertLanguageInterface 'en', 'Pending', 'Pending', 'expense';
exec insertLanguageInterface 'ru', 'Pending', 'Ожидает подтверждения', 'expense';
exec insertLanguageInterface 'uk', 'Pending', 'Очікує підтвердження', 'expense';

--Approved
exec insertLanguageInterface 'en', 'Approved', 'Approved', 'expense';
exec insertLanguageInterface 'ru', 'Approved', 'Подтверждено', 'expense';
exec insertLanguageInterface 'uk', 'Approved', 'Підтверджено', 'expense';

--Rejected
exec insertLanguageInterface 'en', 'Rejected', 'Rejected', 'expense';
exec insertLanguageInterface 'ru', 'Rejected', 'Отклонено', 'expense';
exec insertLanguageInterface 'uk', 'Rejected', 'Відхилено', 'expense';

--Returned
exec insertLanguageInterface 'en', 'Returned', 'Returned', 'expense';
exec insertLanguageInterface 'ru', 'Returned', 'Возвращено на доработку', 'expense';
exec insertLanguageInterface 'uk', 'Returned', 'Повернено на доробку', 'expense';

--Total
exec insertLanguageInterface 'en', 'Total', 'Total', 'expense';
exec insertLanguageInterface 'ru', 'Total', 'Всего', 'expense';
exec insertLanguageInterface 'uk', 'Total', 'Всього', 'expense';

--Managers comment
exec insertLanguageInterface 'en', 'Managers comment', 'Managers comment', 'expense';
exec insertLanguageInterface 'ru', 'Managers comment', 'Коментарий менеджера', 'expense';
exec insertLanguageInterface 'uk', 'Managers comment', 'Коментар менеджера', 'expense';

--Brands
exec insertLanguageInterface 'en', 'Brands', 'Brands', 'expense';
exec insertLanguageInterface 'ru', 'Brands', 'Продукты', 'expense';
exec insertLanguageInterface 'uk', 'Brands', 'Продукти', 'expense';

--No expenses found
exec insertLanguageInterface 'en', 'No expenses found', 'No expenses found', 'expense';
exec insertLanguageInterface 'ru', 'No expenses found', 'Отчеты не найдены', 'expense';
exec insertLanguageInterface 'uk', 'No expenses found', 'Звіти не знайдені', 'expense';

--Save
exec insertLanguageInterface 'en', 'Save', 'Save', 'expense';
exec insertLanguageInterface 'ru', 'Save', 'Сохранить', 'expense';
exec insertLanguageInterface 'uk', 'Save', 'Зберегти', 'expense';

--No brands
exec insertLanguageInterface 'en', 'No brands', 'No brands', 'expense';
exec insertLanguageInterface 'ru', 'No brands', 'Продукты не указаны', 'expense';
exec insertLanguageInterface 'uk', 'No brands', 'Продукти не вказані', 'expense';
go

exec insertLanguageInterface 'en', 'Name', 'Name', 'promo-project';
exec insertLanguageInterface 'ru', 'Name', 'Название', 'promo-project';
exec insertLanguageInterface 'uk', 'Name', 'Назва', 'promo-project';

exec insertLanguageInterface 'en', 'Date from', 'Date from', 'promo-project';
exec insertLanguageInterface 'ru', 'Date from', 'Начало', 'promo-project';
exec insertLanguageInterface 'uk', 'Date from', 'Початок', 'promo-project';

exec insertLanguageInterface 'en', 'Date till', 'Date till', 'promo-project';
exec insertLanguageInterface 'ru', 'Date till', 'Завершение', 'promo-project';
exec insertLanguageInterface 'uk', 'Date till', 'Завершення', 'promo-project';

exec insertLanguageInterface 'en', 'Brand', 'Brand', 'promo-project';
exec insertLanguageInterface 'ru', 'Brand', 'Продукт', 'promo-project';
exec insertLanguageInterface 'uk', 'Brand', 'Продукт', 'promo-project';

exec insertLanguageInterface 'en', 'Search', 'Search', 'promo-project';
exec insertLanguageInterface 'ru', 'Search', 'Поиск', 'promo-project';
exec insertLanguageInterface 'uk', 'Search', 'Пошук', 'promo-project';

exec insertLanguageInterface 'en', 'Select brands', 'Select brands', 'promo-project';
exec insertLanguageInterface 'ru', 'Select brands', 'Выбрать продукты', 'promo-project';
exec insertLanguageInterface 'uk', 'Select brands', 'Вибрати продукти', 'promo-project';

exec insertLanguageInterface 'en', 'Create new promo project', 'Create new promo project', 'promo-project';
exec insertLanguageInterface 'ru', 'Create new promo project', 'Создать новую кампанию', 'promo-project';
exec insertLanguageInterface 'uk', 'Create new promo project', 'Створити нову кампанію', 'promo-project';

exec insertLanguageInterface 'en', 'Edit promo project', 'Edit promo project', 'promo-project';
exec insertLanguageInterface 'ru', 'Edit promo project', 'Редактировать кампанию', 'promo-project';
exec insertLanguageInterface 'uk', 'Edit promo project', 'Редагувати кампанію', 'promo-project';

exec insertLanguageInterface 'en', 'Create promo project', 'Create promo project', 'promo-project';
exec insertLanguageInterface 'ru', 'Create promo project', 'Создать кампанию', 'promo-project';
exec insertLanguageInterface 'uk', 'Create promo project', 'Створити кампанію', 'promo-project';

exec insertLanguageInterface 'en', 'Campaign name', 'Campaing name', 'promo-project';
exec insertLanguageInterface 'ru', 'Campaign name', 'Название кампании', 'promo-project';
exec insertLanguageInterface 'uk', 'Campaign name', 'Назва кампанії', 'promo-project';

exec insertLanguageInterface 'en', 'Directions', 'Directions', 'promo-project';
exec insertLanguageInterface 'ru', 'Directions', 'Направления', 'promo-project';
exec insertLanguageInterface 'uk', 'Directions', 'Напрями', 'promo-project';

exec insertLanguageInterface 'en', 'Product', 'Product', 'promo-project';
exec insertLanguageInterface 'ru', 'Product', 'Продукт', 'promo-project';
exec insertLanguageInterface 'uk', 'Product', 'Продукт', 'promo-project';

exec insertLanguageInterface 'en', 'Percent of time', 'Percent of time', 'promo-project';
exec insertLanguageInterface 'ru', 'Percent of time', 'Процент времени', 'promo-project';
exec insertLanguageInterface 'uk', 'Percent of time', 'Відсоток часу', 'promo-project';

exec insertLanguageInterface 'en', 'No brands', 'No brands', 'promo-project';
exec insertLanguageInterface 'ru', 'No brands', 'Нет продуктов', 'promo-project';
exec insertLanguageInterface 'uk', 'No brands', 'Нема продуктів', 'promo-project';

exec insertLanguageInterface 'en', 'Add brand', 'Add brand', 'promo-project';
exec insertLanguageInterface 'ru', 'Add brand', 'Добавить продукт', 'promo-project';
exec insertLanguageInterface 'uk', 'Add brand', 'Додати продукт', 'promo-project';

exec insertLanguageInterface 'en', 'datefrom must be less then datetill', 'datefrom must be less then datetill', 'promo-project';
exec insertLanguageInterface 'ru', 'datefrom must be less then datetill', 'Дата начала должна быть меньше даты завершения', 'promo-project';
exec insertLanguageInterface 'uk', 'datefrom must be less then datetill', 'Дата початку має бути менше дати завершення', 'promo-project';

exec insertLanguageInterface 'en', 'Loading', 'Loading', 'promo-project';
exec insertLanguageInterface 'ru', 'Loading', 'Загрузкаа', 'promo-project';
exec insertLanguageInterface 'uk', 'Loading', 'Завантаження', 'promo-project';

exec insertLanguageInterface 'en', 'Promo Project', 'Promo Project', 'promo-project';
exec insertLanguageInterface 'ru', 'Promo Project', 'Маркетинговые кампании', 'promo-project';
exec insertLanguageInterface 'uk', 'Promo Project', 'Маркетингові кампанії', 'promo-project';

exec insertLanguageInterface 'en', 'Nothing found', 'Nothing found', 'promo-project';
exec insertLanguageInterface 'ru', 'Nothing found', 'Кампании не найдены', 'promo-project';
exec insertLanguageInterface 'uk', 'Nothing found', 'Кампанії не знайдені', 'promo-project';

exec insertLanguageInterface 'en', 'edit', 'edit', 'promo-project';
exec insertLanguageInterface 'ru', 'edit', 'редактировать', 'promo-project';
exec insertLanguageInterface 'uk', 'edit', 'редагувати', 'promo-project';

exec insertLanguageInterface 'en', 'Save changes', 'Save changes', 'promo-project';
exec insertLanguageInterface 'ru', 'Save changes', 'Сохранить изменения', 'promo-project';
exec insertLanguageInterface 'uk', 'Save changes', 'Зберегти зміни', 'promo-project';

exec insertLanguageInterface 'en', 'Close', 'Close', 'promo-project';
exec insertLanguageInterface 'ru', 'Close', 'Закрыть', 'promo-project';
exec insertLanguageInterface 'uk', 'Close', 'Закрити', 'promo-project';

exec insertLanguageInterface 'en', 'Promo Projects', 'Promo Projects', 'messages';
exec insertLanguageInterface 'ru', 'Promo Projects', 'Маркетинговые кампании', 'messages';
exec insertLanguageInterface 'uk', 'Promo Projects', 'Маркетингові кампанії', 'messages';

exec insertLanguageInterface 'en', 'Manage promo projects', 'Manage promo projects', 'messages';
exec insertLanguageInterface 'ru', 'Manage promo projects', 'Управление маркетнговыми кампаниями', 'messages';
exec insertLanguageInterface 'uk', 'Manage promo projects', 'Керування маркетинговими кампаніями', 'messages';

exec insertLanguageInterface 'en', 'This value should not be null.', 'This value should not be null.', 'promo-project';
exec insertLanguageInterface 'ru', 'This value should not be null.', 'Поле не может быть null.', 'promo-project';
exec insertLanguageInterface 'uk', 'This value should not be null.', 'Поле не може бути null.', 'promo-project';

exec insertLanguageInterface 'en', 'This value should not be blank.', 'This value should not be blank.', 'promo-project';
exec insertLanguageInterface 'ru', 'This value should not be blank.', 'Поле не может быть пустым.', 'promo-project';
exec insertLanguageInterface 'uk', 'This value should not be blank.', 'Поле не може бути пустим.', 'promo-project';

exec insertLanguageInterface 'en', 'Select product', 'Select product', 'promo-project';
exec insertLanguageInterface 'ru', 'Select product', 'Укажите продукт', 'promo-project';
exec insertLanguageInterface 'uk', 'Select product', 'Вкажіть продукт', 'promo-project';

exec insertLanguageInterface 'en', 'Select percent of time', 'Select percent of time', 'promo-project';
exec insertLanguageInterface 'ru', 'Select percent of time', 'Укажите процент времени', 'promo-project';
exec insertLanguageInterface 'uk', 'Select percent of time', 'Вкажіть відсоток часу', 'promo-project';
go
------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------

----------------------------------------------------------------------------------------------------
-- https://teamsoft.atlassian.net/browse/EGISRU-276
----------------------------------------------------------------------------------------------------

IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'info_mcmtarget' AND COLUMN_NAME = 'fromfile')
BEGIN
    ALTER TABLE info_mcmtarget ADD fromfile INT NULL
END
GO

IF NOT EXISTS(SELECT 1 FROM info_options WHERE name = 'importTargetAudience')
    INSERT INTO info_options (name, value)
    VALUES ('importTargetAudience', 0)
GO

exec insertLanguageInterface 'en', 'Contacts in file ', N'Contacts in file', 'messages'
exec insertLanguageInterface 'ru', 'Contacts in file ', N'Контактов в файле', 'messages'
exec insertLanguageInterface 'uk', 'Contacts in file ', N'Контактів у файлі', 'messages'


exec insertLanguageInterface 'en', 'The file is empty', N'The file is empty', 'messages'
exec insertLanguageInterface 'ru', 'The file is empty', N'Файл пустой', 'messages'
exec insertLanguageInterface 'uk', 'The file is empty', N'Файл пустий', 'messages'

exec insertLanguageInterface 'en', 'The file is not supported', N'The file is not supported', 'messages'
exec insertLanguageInterface 'ru', 'The file is not supported', N'Данный формат файла не поддерживается', 'messages'
exec insertLanguageInterface 'uk', 'The file is not supported', N'Даний формат файлу не підтримується', 'messages'

----------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------

----------------------------------------------------------------------------------------------------
-- https://teamsoft.atlassian.net/browse/EGISRU-358
----------------------------------------------------------------------------------------------------
IF NOT EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'info_mcmunsubscribed' AND COLUMN_NAME = 'distribution_id')
BEGIN
ALTER TABLE info_mcmunsubscribed ADD distribution_id INT NULL;
ALTER TABLE info_mcmunsubscribed WITH CHECK ADD CONSTRAINT fk_info_mcmunsubscribed_distribution_id
    FOREIGN KEY (distribution_id) REFERENCES info_mcmdistribution (id) ON DELETE CASCADE ON UPDATE CASCADE;

exec insertLanguageInterface 'en', 'Unsubscribed', 'Unsubscribed', 'messages';
exec insertLanguageInterface 'ru', 'Unsubscribed', 'Отписавшиеся', 'messages';
exec insertLanguageInterface 'uk', 'Unsubscribed', 'Відписані', 'messages';

exec insertLanguageInterface 'en', 'Available for distribution', 'Available for distribution', 'messages';
exec insertLanguageInterface 'ru', 'Available for distribution', 'Доступные для рассылки', 'messages';
exec insertLanguageInterface 'uk', 'Available for distribution', 'Доступні для розсилки', 'messages';

exec insertLanguageInterface 'en', 'Available with contacts', 'Available with contacts', 'messages';
exec insertLanguageInterface 'ru', 'Available with contacts', 'Доступно с контактами', 'messages';
exec insertLanguageInterface 'uk', 'Available with contacts', 'Доступно з контактами', 'messages';
END
GO
----------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------

------------------------------------------------------------------------------------------------------------------------
-- https://teamsoft.atlassian.net/browse/PHARMAWEB-431
------------------------------------------------------------------------------------------------------------------------
IF NOT EXISTS(SELECT 1 FROM info_serviceprivilege WHERE [code] = 'CRM_HIDE_MENU_PROFILE')
BEGIN
INSERT INTO info_serviceprivilege ([name], code, service_id, parent_id)
VALUES (
           'Hide profile from menu CRM',
           'CRM_HIDE_MENU_PROFILE',
           (SELECT id FROM info_service WHERE identifier = 'crm'),
           (SELECT MIN(id) FROM info_serviceprivilege WHERE code = 'ROLE_ACCESS_TO_CRM')
       )

DECLARE @privilegeID int
        SET @privilegeID = (SELECT id FROM info_serviceprivilege WHERE code = 'CRM_HIDE_MENU_PROFILE')

        INSERT INTO info_roleprivilege (privilege_id, role_id)
SELECT @privilegeID AS privilege_id, obr.role_id AS role_id
FROM info_optionsbyrole as obr
         INNER JOIN info_options AS o ON (obr.option_id = o.id)
WHERE o.name='hideMenuProfile' AND isnull(o.value,0)=1
END
GO

IF NOT EXISTS(SELECT 1 FROM info_serviceprivilege WHERE [code] = 'CRM_HIDE_MENU_CONTACT')
BEGIN
INSERT INTO info_serviceprivilege ([name], code, service_id, parent_id)
VALUES (
           'Hide contact from menu CRM',
           'CRM_HIDE_MENU_CONTACT',
           (SELECT id FROM info_service WHERE identifier = 'crm'),
           (SELECT MIN(id) FROM info_serviceprivilege WHERE code = 'ROLE_ACCESS_TO_CRM')
       )

DECLARE @privilegeID int
        SET @privilegeID = (SELECT id FROM info_serviceprivilege WHERE code = 'CRM_HIDE_MENU_CONTACT')

        INSERT INTO info_roleprivilege (privilege_id, role_id)
SELECT @privilegeID AS privilege_id, obr.role_id AS role_id
FROM info_optionsbyrole as obr
         INNER JOIN info_options AS o ON (obr.option_id = o.id)
WHERE o.name='hideMenuContact' AND isnull(o.value,0)=1
END
GO

IF NOT EXISTS(SELECT 1 FROM info_serviceprivilege WHERE [code] = 'CRM_HIDE_MENU_COMPANY')
BEGIN
INSERT INTO info_serviceprivilege ([name], code, service_id, parent_id)
VALUES (
           'Hide company from menu CRM',
           'CRM_HIDE_MENU_COMPANY',
           (SELECT id FROM info_service WHERE identifier = 'crm'),
           (SELECT MIN(id) FROM info_serviceprivilege WHERE code = 'ROLE_ACCESS_TO_CRM')
       )

DECLARE @privilegeID int
        SET @privilegeID = (SELECT id FROM info_serviceprivilege WHERE code = 'CRM_HIDE_MENU_COMPANY')

        INSERT INTO info_roleprivilege (privilege_id, role_id)
SELECT @privilegeID AS privilege_id, obr.role_id AS role_id
FROM info_optionsbyrole as obr
         INNER JOIN info_options AS o ON (obr.option_id = o.id)
WHERE o.name='hideMenuCompany' AND isnull(o.value,0)=1
END
GO

IF NOT EXISTS(SELECT 1 FROM info_serviceprivilege WHERE [code] = 'CRM_HIDE_MENU_REPORT')
BEGIN
INSERT INTO info_serviceprivilege ([name], code, service_id, parent_id)
VALUES (
           'Hide report from menu CRM',
           'CRM_HIDE_MENU_REPORT',
           (SELECT id FROM info_service WHERE identifier = 'crm'),
           (SELECT MIN(id) FROM info_serviceprivilege WHERE code = 'ROLE_ACCESS_TO_CRM')
       )

DECLARE @privilegeID int
        SET @privilegeID = (SELECT id FROM info_serviceprivilege WHERE code = 'CRM_HIDE_MENU_REPORT')

        INSERT INTO info_roleprivilege (privilege_id, role_id)
SELECT @privilegeID AS privilege_id, obr.role_id AS role_id
FROM info_optionsbyrole as obr
         INNER JOIN info_options AS o ON (obr.option_id = o.id)
WHERE o.name='hideMenuReport' AND isnull(o.value,0)=1
END
GO

IF NOT EXISTS(SELECT 1 FROM info_serviceprivilege WHERE [code] = 'CRM_HIDE_MENU_PLAN')
BEGIN
INSERT INTO info_serviceprivilege ([name], code, service_id, parent_id)
VALUES (
           'Hide plan from menu CRM',
           'CRM_HIDE_MENU_PLAN',
           (SELECT id FROM info_service WHERE identifier = 'crm'),
           (SELECT MIN(id) FROM info_serviceprivilege WHERE code = 'ROLE_ACCESS_TO_CRM')
       )
END
GO

IF NOT EXISTS(SELECT 1 FROM info_serviceprivilege WHERE [code] = 'CRM_HIDE_MENU_SURVEY_MANAGER')
BEGIN
INSERT INTO info_serviceprivilege ([name], code, service_id, parent_id)
VALUES (
           'Hide survey from menu CRM',
           'CRM_HIDE_MENU_SURVEY_MANAGER',
           (SELECT id FROM info_service WHERE identifier = 'crm'),
           (SELECT MIN(id) FROM info_serviceprivilege WHERE code = 'ROLE_ACCESS_TO_CRM')
       )

DECLARE @privilegeID int
        SET @privilegeID = (SELECT id FROM info_serviceprivilege WHERE code = 'CRM_HIDE_MENU_SURVEY_MANAGER')

        INSERT INTO info_roleprivilege (privilege_id, role_id)
SELECT @privilegeID AS privilege_id, role.id AS role_id
FROM info_role as role
WHERE role.id NOT IN (
    SELECT role_id
    FROM info_roleprivilege
             INNER JOIN info_serviceprivilege i on info_roleprivilege.privilege_id = i.id
    WHERE i.code='ROLE_ACCESS_TO_SURVEY_MANAGER'
)
END
GO

IF NOT EXISTS(SELECT 1 FROM info_serviceprivilege WHERE [code] = 'CRM_HIDE_MENU_VERIFICATION')
BEGIN
INSERT INTO info_serviceprivilege ([name], code, service_id, parent_id)
VALUES (
           'Hide verification from menu CRM',
           'CRM_HIDE_MENU_VERIFICATION',
           (SELECT id FROM info_service WHERE identifier = 'crm'),
           (SELECT MIN(id) FROM info_serviceprivilege WHERE code = 'ROLE_ACCESS_TO_CRM')
       )

DECLARE @privilegeID int
        SET @privilegeID = (SELECT id FROM info_serviceprivilege WHERE code = 'CRM_HIDE_MENU_VERIFICATION')

        INSERT INTO info_roleprivilege (privilege_id, role_id)
SELECT @privilegeID AS privilege_id, role.id AS role_id
FROM info_role as role
WHERE role.id NOT IN (
    SELECT role_id
    FROM info_roleprivilege
             INNER JOIN info_serviceprivilege i on info_roleprivilege.privilege_id = i.id
    WHERE i.code='ROLE_ACCESS_TO_CRM_VERIFICATION'
)
END
GO

exec insertLanguageInterface 'en', 'Hide profile from menu CRM', N'Hide profile from menu CRM', 'messages'
exec insertLanguageInterface 'ru', 'Hide profile from menu CRM', N'Скрыть профиль из меню CRM', 'messages'
exec insertLanguageInterface 'uk', 'Hide profile from menu CRM', N'Приховати профіль з меню CRM', 'messages'
GO

exec insertLanguageInterface 'en', 'Hide contact from menu CRM', N'Hide contact from menu CRM', 'messages'
exec insertLanguageInterface 'ru', 'Hide contact from menu CRM', N'Скрыть клиенты из меню CRM', 'messages'
exec insertLanguageInterface 'uk', 'Hide contact from menu CRM', N'Приховати клієнти з меню CRM', 'messages'
GO

exec insertLanguageInterface 'en', 'Hide company from menu CRM', N'Hide company from menu CRM', 'messages'
exec insertLanguageInterface 'ru', 'Hide company from menu CRM', N'Скрыть учреждения из меню CRM', 'messages'
exec insertLanguageInterface 'uk', 'Hide company from menu CRM', N'Приховати установи з меню CRM', 'messages'
GO

exec insertLanguageInterface 'en', 'Hide report from menu CRM', N'Hide report from menu CRM', 'messages'
exec insertLanguageInterface 'ru', 'Hide report from menu CRM', N'Скрыть отчеты из меню CRM', 'messages'
exec insertLanguageInterface 'uk', 'Hide report from menu CRM', N'Приховати звіти з меню CRM', 'messages'
GO

exec insertLanguageInterface 'en', 'Hide plan from menu CRM', N'Hide plan from menu CRM', 'messages'
exec insertLanguageInterface 'ru', 'Hide plan from menu CRM', N'Скрыть план из меню CRM', 'messages'
exec insertLanguageInterface 'uk', 'Hide plan from menu CRM', N'Приховати план з меню CRM', 'messages'
GO

exec insertLanguageInterface 'en', 'Hide CRM from Homepage', N'Hide CRM from Homepage', 'messages'
exec insertLanguageInterface 'ru', 'Hide CRM from Homepage', N'Скрыть CRM с главной страницы', 'messages'
exec insertLanguageInterface 'uk', 'Hide CRM from Homepage', N'Приховати CRM на головній сторінці', 'messages'
GO

exec insertLanguageInterface 'en', 'Hide survey from menu CRM', N'Hide survey from menu CRM', 'messages'
exec insertLanguageInterface 'ru', 'Hide survey from menu CRM', N'Скрыть опрос из меню CRM', 'messages'
exec insertLanguageInterface 'uk', 'Hide survey from menu CRM', N'Приховати опитування з меню CRM', 'messages'
GO

exec insertLanguageInterface 'en', 'Hide verification from menu CRM', N'Hide verification from menu CRM', 'messages'
exec insertLanguageInterface 'ru', 'Hide verification from menu CRM', N'Скрыть проверка из меню CRM', 'messages'
exec insertLanguageInterface 'uk', 'Hide verification from menu CRM', N'Приховати перевірку з меню CRM', 'messages'
GO
------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------

------------------------------------------------------------------------------------------------------------------------
-- https://teamsoft.atlassian.net/browse/NESTLERU-15
------------------------------------------------------------------------------------------------------------------------
IF NOT EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'info_reporttabfield' AND COLUMN_NAME = 'filter')
BEGIN
ALTER TABLE info_reporttabfield ADD filter VARCHAR(50) NULL;
END
GO
------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------
-- translations
------------------------------------------------------------------------------------------------------------------------
exec insertLanguageInterface 'en','Status verification','Status verification';
exec insertLanguageInterface 'en','STATUS_VERIFICATION','Status verification';
exec insertLanguageInterface 'en','STATUS_VERIFICATION_1','Pending verification';
exec insertLanguageInterface 'en','STATUS_VERIFICATION_2','Sent for verification';
exec insertLanguageInterface 'en','STATUS_VERIFICATION_3','Verified';
exec insertLanguageInterface 'en','STATUS_VERIFICATION_4','Rejected';
exec insertLanguageInterface 'en','STATUS_VERIFICATION_5','Re-verification required';
exec insertLanguageInterface 'en','STATUS_VERIFICATION_6','Can''t be verified';
exec insertLanguageInterface 'ru','Status verification','Статус верификации';
exec insertLanguageInterface 'ru','STATUS_VERIFICATION','Статус верификации';
exec insertLanguageInterface 'ru','STATUS_VERIFICATION_1','Ожидает верификацию';
exec insertLanguageInterface 'ru','STATUS_VERIFICATION_2','Отправлено на верификацию';
exec insertLanguageInterface 'ru','STATUS_VERIFICATION_3','Верифицировано';
exec insertLanguageInterface 'ru','STATUS_VERIFICATION_4','Отклонено';
exec insertLanguageInterface 'ru','STATUS_VERIFICATION_5','Требует повторной верификации';
exec insertLanguageInterface 'ru','STATUS_VERIFICATION_6','Не подлежит верификации';
exec insertLanguageInterface 'uk','Status verification','Статус верифікації';
exec insertLanguageInterface 'uk','STATUS_VERIFICATION','Статус верифікації';
exec insertLanguageInterface 'uk','STATUS_VERIFICATION_1','Очікує верифікацію';
exec insertLanguageInterface 'uk','STATUS_VERIFICATION_2','Відправлено на верифікацію';
exec insertLanguageInterface 'uk','STATUS_VERIFICATION_3','Верифіковано';
exec insertLanguageInterface 'uk','STATUS_VERIFICATION_4','Відхилено';
exec insertLanguageInterface 'uk','STATUS_VERIFICATION_5','Потребує повторної верифікації';
exec insertLanguageInterface 'uk','STATUS_VERIFICATION_6','Не підлягає верифікації';
GO
------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------
---https://teamsoft.atlassian.net/browse/NESTLERU-18
------------------------------------------------------------------------------------------------------------------------
IF OBJECT_ID(N'info_mcmjson') IS NULL BEGIN
CREATE TABLE [dbo].[info_mcmjson]
(
    [id]                [int] IDENTITY (1,1) NOT NULL,
    [morionid]          [int]                NULL,
    [createdate]        [datetime]           NULL,
    [name]              [varchar](255)       NULL,
    [metric]            [varchar](MAX)       NULL,
    [currenttime]       [datetime]           NULL,
    [time]              [timestamp]          NULL,
    [moduser]           [varchar](16)        NULL,
    [guid]              [uniqueidentifier]   NULL,
    CONSTRAINT [PK_info_mcmjson] PRIMARY KEY CLUSTERED
(
[id] ASC
) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
    ) ON [PRIMARY]

ALTER TABLE [dbo].[info_mcmjson] ADD CONSTRAINT [DF_info_mcmjson_currenttime] DEFAULT (getdate()) FOR [currenttime]
ALTER TABLE [dbo].[info_mcmjson] ADD CONSTRAINT [DF_info_mcmjson_moduser] DEFAULT ([dbo].[Get_CurrentCode]()) FOR [moduser]
ALTER TABLE [dbo].[info_mcmjson] ADD CONSTRAINT [DF_info_mcmjson_guid] DEFAULT (newid()) FOR [guid]

end
GO
------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------

------------------------------------------------------------------------------------------------------------------------
-- https://teamsoft.atlassian.net/browse/EGISRU-339
------------------------------------------------------------------------------------------------------------------------

IF NOT EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'info_mcmdistributiondirection'
AND COLUMN_NAME = 'user_id')
BEGIN
    ALTER TABLE info_mcmdistributiondirection ADD user_id INT NULL;
    ALTER TABLE info_mcmdistributiondirection WITH CHECK ADD CONSTRAINT fk_info_mcmdistributiondirection_user_id
        FOREIGN KEY (user_id) REFERENCES info_user(id)
END
GO

IF NOT EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'info_mcmdistributiondirection'
AND COLUMN_NAME = 'region_id')
BEGIN
    ALTER TABLE info_mcmdistributiondirection ADD region_id INT NULL;
    ALTER TABLE info_mcmdistributiondirection WITH CHECK ADD CONSTRAINT fk_info_mcmdistributiondirection_region_id
        FOREIGN KEY (region_id) REFERENCES info_region(id)
END
GO

IF NOT EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'info_mcmdistributiondirection'
AND COLUMN_NAME = 'submission_id')
BEGIN
    ALTER TABLE info_mcmdistributiondirection ADD submission_id INT NULL;
    ALTER TABLE info_mcmdistributiondirection WITH CHECK ADD CONSTRAINT fk_info_mcmdistributiondirection_submission_id
        FOREIGN KEY (submission_id) REFERENCES info_user(id)
END
GO
------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------
