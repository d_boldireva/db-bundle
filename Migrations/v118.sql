---------------------------------------------------------------------------------------------------------
-- https://teamsoft.atlassian.net/browse/MCM-310
---------------------------------------------------------------------------------------------------------
IF OBJECT_ID(N'[info_attendees]', 'U') IS NULL
BEGIN
CREATE TABLE  [dbo].[info_attendees]
(
    [id]                [int] IDENTITY (1,1) NOT NULL,
    [user_id]           [varchar](255)       NULL,
    [full_name]         [varchar](255)       NULL,
    [role]              [varchar](255)       NULL,
    [is_presenter]      [int]                NULL,
    [is_listening_only] [int]                NULL,
    [has_joined_voice]  [int]                NULL,
    [has_video]         [int]                NULL,
    [client_type]       [varchar](255)       NULL,
    [info_meetings]     [varchar](255)       NULL,
    [currenttime]       [datetime]           NULL,
    [time]              [timestamp]          NULL,
    [moduser]           [varchar]            NULL,
    [guid]              [uniqueidentifier]   NULL,
    CONSTRAINT [PK_info_attendees] PRIMARY KEY CLUSTERED
(
[id] ASC
) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
    ) ON [PRIMARY]
ALTER TABLE [dbo].[info_attendees]
    ADD CONSTRAINT [DF_info_attendeescurrenttime] DEFAULT (getdate()) FOR [currenttime]
ALTER TABLE [dbo].[info_attendees]
    ADD CONSTRAINT [DF_info_attendeesmoduser] DEFAULT ([dbo].[Get_CurrentCode]()) FOR [moduser]
ALTER TABLE [dbo].[info_attendees]
    ADD CONSTRAINT [DF_info_attendeesguid] DEFAULT (newid()) FOR [guid]
END
GO

IF OBJECT_ID(N'[info_meetings]', 'U') IS NULL
BEGIN
CREATE TABLE  [dbo].[info_meetings](
    [id]                        [int] IDENTITY (1,1) NOT NULL,
    [meeting_name]              [varchar](255)       NULL,
    [meeting_id]                [varchar](255)       NOT NULL,
    [create_time]               [varchar](255)       NULL,
    [create_date]               [varchar](255)       NULL,
    [duration]                  [int]                NULL,
    [has_user_joined]           [int]                NULL,
    [recording]                 [int]                NULL,
    [has_been_forcibly_ended]   [int]                NULL,
    [start_time]                [varchar](255)       NULL,
    [end_time]                  [varchar](255)       NULL,
    [participant_count]         [int]                NULL,
    [listener_count]            [int]                NULL,
    [voice_participant_count]   [int]                NULL,
    [video_count]               [int]                NULL,
    [max_users]                 [int]                NULL,
    [moderator_count]           [int]                NULL,
    [room_id]                   [varchar](255)       NULL,
    [currenttime]               [datetime]           NULL,
    [time]                      [timestamp]          NULL,
    [moduser]                   [varchar]            NULL,
    [guid]                      [uniqueidentifier]   NULL,
    CONSTRAINT [PK_info_meetings] PRIMARY KEY CLUSTERED
(
[id] ASC
) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
    ) ON [PRIMARY]
ALTER TABLE [dbo].[info_meetings]
    ADD CONSTRAINT [DF_info_meetingscurrenttime] DEFAULT (getdate()) FOR [currenttime]
ALTER TABLE [dbo].[info_meetings]
    ADD CONSTRAINT [DF_info_meetingsmoduser] DEFAULT ([dbo].[Get_CurrentCode]()) FOR [moduser]
ALTER TABLE [dbo].[info_meetings]
    ADD CONSTRAINT [DF_info_meetingsguid] DEFAULT (newid()) FOR [guid]
IF NOT EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'info_attendees' AND Column_Name = 'info_meetings')
BEGIN
create unique index info_meetings_meeting_id_uindex
    on info_meetings (meeting_id);
alter table info_attendees
    add constraint info_attendees_info_meetings_meeting_id_fk
        foreign key (info_meetings) references info_meetings (meeting_id);
end
END
GO

IF NOT EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'info_user' AND Column_Name = 'secret')
BEGIN
ALTER TABLE info_user
    ADD secret VARCHAR(255) NULL
END
GO

IF NOT EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'info_user' AND Column_Name = 'meeting_url')
BEGIN
ALTER TABLE info_user
    ADD meeting_url VARCHAR(255) NULL
END
GO

---------------------------------------------------------------------------------------------------------
-- https://teamsoft.atlassian.net/browse/PHARMAWEB-319
---------------------------------------------------------------------------------------------------------
IF NOT EXISTS (SELECT 1 FROM information_schema.columns WHERE table_name = 'info_region' AND column_name = 'gmt')
    ALTER TABLE info_region ADD gmt varchar(50);
---------------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------------

---------------------------------------------------------------------------------------------------------
-- https://teamsoft.atlassian.net/browse/PHARMAWEB-318
---------------------------------------------------------------------------------------------------------
IF NOT EXISTS(SELECT 1 FROM info_serviceprivilege WHERE [code] = 'CRM_HIDE_ON_WELCOME_PAGE')
    INSERT INTO info_serviceprivilege ([name], code, service_id, parent_id)
        VALUES (
            'Hide CRM from Homepage',
            'CRM_HIDE_ON_WELCOME_PAGE',
            (SELECT id FROM info_service WHERE identifier = 'crm'),
            (SELECT MIN(id) FROM info_serviceprivilege WHERE code = 'ROLE_ACCESS_TO_CRM')
         )
GO
---------------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------
--https://teamsoft.atlassian.net/browse/ROSH-2
------------------------------------------------------------------------------------------------------------------------
IF NOT EXISTS(SELECT 1
              FROM information_schema.columns
              WHERE table_name = 'info_contactrequest'
                AND column_name = 'dictvalue_id_old')
    BEGIN
        ALTER TABLE info_contactrequest
            ADD [dictvalue_id_old] INT NULL
        ALTER TABLE info_contactrequest
            ADD CONSTRAINT [FK_contactrequest_dictvalue_id_old] FOREIGN KEY ([dictvalue_id_old])
            REFERENCES info_CustomDictionaryValue ([id])
    end
GO

IF NOT EXISTS(SELECT 1
              FROM information_schema.columns
              WHERE table_name = 'info_contactrequest'
                AND column_name = 'dictvalue_id_new')
    BEGIN
        ALTER TABLE info_contactrequest
            ADD [dictvalue_id_new] INT NULL
        ALTER TABLE info_contactrequest
            ADD CONSTRAINT [FK_contactrequest_dictvalue_id_new] FOREIGN KEY ([dictvalue_id_new])
            REFERENCES info_CustomDictionaryValue ([id])
    end
GO

exec insertLanguageInterface 'en', 'checkrfm', N'RFM','crm';
exec insertLanguageInterface 'ru', 'checkrfm', N'RFM','crm';
exec insertLanguageInterface 'uk', 'checkrfm', N'RFM','crm';
GO

------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------
--GEODATA-693 start
if object_id(N'info_etmsactionlogbatch', 'U') is null
begin
create table info_etmsactionlogbatch
(
    id          int              not null identity (1,1),
    version_id  int              not null,
    currenttime datetime         null default (CURRENT_TIMESTAMP),
    moduser     varchar(16)      null default ([dbo].[Get_CurrentCode]()),
    guid        uniqueidentifier null default (newid()),
    primary key (id)
)
end
go

if object_id(N'fk_info_etmsactionlogbatch_version_id') is null
begin
alter table info_etmsactionlogbatch with check add constraint fk_info_etmsactionlogbatch_version_id foreign key (version_id)
    references info_etmsmapsversion(id) on
delete
cascade
alter table info_etmsactionlogbatch check constraint fk_info_etmsactionlogbatch_version_id
end
go

if not exists(select * from sys.indexes where name = 'ix_info_etmsactionlogbatch_version_id')
begin
create
nonclustered index ix_info_etmsactionlogbatch_version_id on info_etmsactionlogbatch (version_id) include (id)
end
go

if object_id(N'info_etmsactionlogbatchowner','U') is null
begin
create table info_etmsactionlogbatchowner
(
    id          int              not null identity (1,1),
    batch_id    int              not null unique,
    owner_id    int              not null,
    currenttime datetime         null default (CURRENT_TIMESTAMP),
    moduser     varchar(16)      null default ([dbo].[Get_CurrentCode]()),
    guid        uniqueidentifier null default (newid()),
    primary key (id)
)
end
go

if object_id(N'fk_info_etmsactionlogbatchowner_batch_id') is null
begin
alter table info_etmsactionlogbatchowner with check add constraint fk_info_etmsactionlogbatchowner_batch_id foreign key (batch_id)
    references info_etmsactionlogbatch(id) on
delete
cascade
alter table info_etmsactionlogbatchowner check constraint fk_info_etmsactionlogbatchowner_batch_id
end
go

if object_id(N'fk_info_etmsactionlogbatchowner_owner_id') is null
begin
alter table info_etmsactionlogbatchowner with check add constraint fk_info_etmsactionlogbatchowner_owner_id foreign key (owner_id)
    references info_user(id) on
delete
cascade
alter table info_etmsactionlogbatchowner check constraint fk_info_etmsactionlogbatchowner_owner_id
end
go

if not exists(select * from sys.indexes where name = 'uq_info_etmsactionlogbatchowner_batch_id_owner_id')
begin
create unique nonclustered index uq_info_etmsactionlogbatchowner_batch_id_owner_id on info_etmsactionlogbatchowner (batch_id, owner_id) include (id)
end
go

if object_id(N'info_etmsactionlog', 'U') is null
begin
create table info_etmsactionlog
(
    id          int              not null identity (1,1),
    batch_id    int              not null,
    polygon_id  int              not null,
    currenttime datetime         null default (CURRENT_TIMESTAMP),
    moduser     varchar(16)      null default ([dbo].[Get_CurrentCode]()),
    guid        uniqueidentifier null default (newid()),
    primary key (id)
)
end
go

if object_id(N'fk_info_etmsactionlog_batch_id') is null
begin
alter table info_etmsactionlog with check add constraint fk_info_etmsactionlog_batch_id foreign key (batch_id)
    references info_etmsactionlogbatch(id) on
delete
cascade
alter table info_etmsactionlog check constraint fk_info_etmsactionlog_batch_id
end
go

if object_id(N'fk_info_etmsactionlog_polygon_id') is null
begin
alter table info_etmsactionlog with check add constraint fk_info_etmsactionlog_polygon_id foreign key (polygon_id)
    references info_polygon(id) on
delete
cascade
alter table info_etmsactionlog check constraint fk_info_etmsactionlog_polygon_id
end
go

if not exists(select * from sys.indexes where name = 'uq_info_etmsactionlog_batch_id_polygon_id')
begin
create unique nonclustered index uq_info_etmsactionlog_batch_id_polygon_id on info_etmsactionlog (batch_id, polygon_id) include (id)
end
go

if object_id(N'info_etmsactionlogprev', 'U') is null
begin
create table info_etmsactionlogprev
(
    id          int              not null identity (1,1),
    action_id   int              not null unique,
    user_id     int              null,
    currenttime datetime         null default (CURRENT_TIMESTAMP),
    moduser     varchar(16)      null default ([dbo].[Get_CurrentCode]()),
    guid        uniqueidentifier null default (newid()),
    primary key (id)
)
end
go

if object_id(N'fk_info_etmsactionlogprev_action_id') is null
begin
alter table info_etmsactionlogprev with check add constraint fk_info_etmsactionlogprev_action_id foreign key (action_id)
    references info_etmsactionlog(id) on
delete
cascade
alter table info_etmsactionlogprev check constraint fk_info_etmsactionlogprev_action_id
end
go

if object_id(N'fk_info_etmsactionlogprev_user_id') is null
begin
alter table info_etmsactionlogprev with check add constraint fk_info_etmsactionlogprev_user_id foreign key (user_id)
    references info_user(id) on
delete
cascade
alter table info_etmsactionlogprev check constraint fk_info_etmsactionlogprev_user_id
end
go

if not exists(select * from sys.indexes where name = 'uq_info_etmsactionlogprev_action_id_user_id')
begin
create unique nonclustered index uq_info_etmsactionlogprev_action_id_user_id on info_etmsactionlogprev (action_id, user_id) include (id)
end
go

if object_id(N'info_etmsactionlognew','U') is null
begin
create table info_etmsactionlognew
(
    id          int              not null identity (1,1),
    action_id   int              not null unique,
    user_id     int              null,
    currenttime datetime         null default (CURRENT_TIMESTAMP),
    moduser     varchar(16)      null default ([dbo].[Get_CurrentCode]()),
    guid        uniqueidentifier null default (newid()),
    primary key (id)
)
end
go

if object_id(N'fk_info_etmsactionlognew_action_id') is null
begin
alter table info_etmsactionlognew with check add constraint fk_info_etmsactionlognew_action_id foreign key (action_id)
    references info_etmsactionlog( id ) on
delete
cascade
alter table info_etmsactionlognew check constraint fk_info_etmsactionlognew_action_id
end
go

if object_id(N'fk_info_etmsactionlognew_user_id') is null
begin
alter table info_etmsactionlognew with check add constraint fk_info_etmsactionlognew_user_id foreign key (user_id)
    references info_user(id) on
delete
cascade
alter table info_etmsactionlognew check constraint fk_info_etmsactionlognew_user_id
end
go

if not exists(select * from sys.indexes where name = 'uq_info_etmsactionlognew_action_id_user_id')
begin
create unique nonclustered index uq_info_etmsactionlognew_action_id_user_id on info_etmsactionlognew (action_id, user_id) include (id)
end
go

if object_id(N'vw_etmsactionlog') is not null
begin
drop view vw_etmsactionlog
end

go

CREATE VIEW vw_etmsactionlog as
select al.id         as id,
       ab.id         as batch_id,
       abo.owner_id  as owner_id,
       alp.user_id   as prev_user_id,
       aln.user_id   as new_user_id,
       al.polygon_id as polygon_id
from info_etmsactionlogbatch ab
         left join info_etmsactionlogbatchowner abo on abo.batch_id = ab.id
         inner join info_etmsactionlog al on al.batch_id = ab.id
         left join info_etmsactionlogprev alp on alp.action_id = al.id
         left join info_etmsactionlognew aln on aln.action_id = al.id
go
--GEODATA-693 end

--GEODATA-635
if object_id('info_etmsrepcluster') is null
begin
create table info_etmsrepcluster
(
    id             int              not null identity(1,1),
    name           varchar(255)     null,
    created        datetime         null,
    company_visits int              null,
    contact_visits int              null,
    owner_id       int              null,
    currenttime    datetime         null default (CURRENT_TIMESTAMP),
    moduser        varchar(16)      null default (dbo.Get_CurrentCode()),
    guid           uniqueidentifier null default (newid()),
    primary key (id)
)
end
go

if object_id('fk_info_etmsrepcluster_owner_id') is null
begin
alter table info_etmsrepcluster with check add constraint fk_info_etmsrepcluster_owner_id foreign key (owner_id) references info_user (id) on delete cascade;
alter table info_etmsrepcluster check constraint fk_info_etmsrepcluster_owner_id
end
go

if not exists(select 1 from sys.indexes where name = 'ix_info_etmsrepcluster_owner_id')
begin
create
nonclustered index ix_info_etmsrepcluster_owner_id on info_etmsrepcluster (owner_id)
end
go

if object_id('info_etmsrepcluster_companytype') is null
begin
create table info_etmsrepcluster_companytype
(
    id             int              not null identity(1,1),
    repcluster_id  int              not null,
    companytype_id int              not null,
    value          int              not null,
    currenttime    datetime         null default (CURRENT_TIMESTAMP),
    moduser        varchar(16)      null default (dbo.Get_CurrentCode()),
    guid           uniqueidentifier null default (newid()),
    primary key (id)
)
end
go

if object_id('fk_info_etmsrepcluster_companytype_companytype_id') is null
begin
alter table info_etmsrepcluster_companytype with check add constraint fk_info_etmsrepcluster_companytype_companytype_id foreign key (companytype_id) references info_companytype (id) on delete cascade;
alter table info_etmsrepcluster_companytype check constraint fk_info_etmsrepcluster_companytype_companytype_id
end
go

if object_id('fk_info_etmsrepcluster_companytype_repcluster_id') is null
begin
alter table info_etmsrepcluster_companytype with check add constraint fk_info_etmsrepcluster_companytype_repcluster_id foreign key (repcluster_id) references info_etmsrepcluster (id) on delete cascade;
alter table info_etmsrepcluster_companytype check constraint fk_info_etmsrepcluster_companytype_repcluster_id
end
go

if not exists(select 1 from sys.indexes where name = 'ix_info_etmsrepcluster_companytype_repcluster_id')
begin
create
nonclustered index ix_info_etmsrepcluster_companytype_repcluster_id on info_etmsrepcluster_companytype (repcluster_id)
end
go

if not exists(select 1 from sys.indexes where name = 'ix_info_etmsrepcluster_companytype_companytype_id')
begin
create
nonclustered index ix_info_etmsrepcluster_companytype_companytype_id on info_etmsrepcluster_companytype (companytype_id)
end
go

if object_id('info_etmsrepcluster_contactspec') is null
begin
create table info_etmsrepcluster_contactspec
(
    id            int              not null identity(1,1),
    repcluster_id int              not null,
    spec_id       int              not null,
    value         int              not null,
    currenttime   datetime         null default (CURRENT_TIMESTAMP),
    moduser       varchar(16)      null default (dbo.Get_CurrentCode()),
    guid          uniqueidentifier null default (newid()),
    primary key (id)
)
end
go

if object_id('fk_info_etmsrepcluster_contactspec_spec_id') is null
begin
alter table info_etmsrepcluster_contactspec with check add constraint fk_info_etmsrepcluster_contactspec_spec_id foreign key (spec_id) references info_dictionary (id) on delete cascade;
alter table info_etmsrepcluster_contactspec check constraint fk_info_etmsrepcluster_contactspec_spec_id
end
go

if object_id('fk_info_etmsrepcluster_contactspec_repcluster_id') is null
begin
alter table info_etmsrepcluster_contactspec with check add constraint fk_info_etmsrepcluster_contactspec_repcluster_id foreign key (repcluster_id) references info_etmsrepcluster (id) on delete cascade;
alter table info_etmsrepcluster_contactspec check constraint fk_info_etmsrepcluster_contactspec_repcluster_id
end
go

if not exists(select 1 from sys.indexes where name = 'ix_info_etmsrepcluster_repcluster_id')
begin
create
nonclustered index ix_info_etmsrepcluster_repcluster_id on info_etmsrepcluster_contactspec (repcluster_id)
end
go

if not exists(select 1 from sys.indexes where name = 'ix_info_etmsrepcluster_spec_id')
begin
create
nonclustered index ix_info_etmsrepcluster_spec_id on info_etmsrepcluster_contactspec (spec_id)
end
go

if object_id('info_etmsrepcluster_cluster') is null
begin
create table info_etmsrepcluster_cluster
(
    id            int              not null identity(1,1),
    name          varchar(255)     null,
    repcluster_id int              not null,
    region_id     int              null,
    color         varchar(255)     null,
    currenttime   datetime         null default (CURRENT_TIMESTAMP),
    moduser       varchar(16)      null default (dbo.Get_CurrentCode()),
    guid          uniqueidentifier null default (newid()),
    primary key (id)
)
end
go

if object_id('fk_info_etmsrepcluster_cluster_repcluster_id') is null
begin
alter table info_etmsrepcluster_cluster with check add constraint fk_info_etmsrepcluster_cluster_repcluster_id foreign key (repcluster_id) references info_etmsrepcluster (id) on delete cascade;
alter table info_etmsrepcluster_cluster check constraint fk_info_etmsrepcluster_cluster_repcluster_id
end
go

if object_id('fk_info_etmsrepcluster_cluster_region_id') is null
begin
alter table info_etmsrepcluster_cluster with check add constraint fk_info_etmsrepcluster_cluster_region_id foreign key (region_id) references info_region (id) on delete set null;
alter table info_etmsrepcluster_cluster check constraint fk_info_etmsrepcluster_cluster_region_id
end
go

if not exists(select 1 from sys.indexes where name = 'ix_info_etmsrepcluster_cluster_repcluster_id')
begin
create
nonclustered index ix_info_etmsrepcluster_cluster_repcluster_id on info_etmsrepcluster_cluster (repcluster_id)
end
go

if not exists(select 1 from sys.indexes where name = 'ix_info_etmsrepcluster_cluster_region_id')
begin
create
nonclustered index ix_info_etmsrepcluster_cluster_region_id on info_etmsrepcluster_cluster (region_id)
end
go

if object_id('info_etmsrepcluster_company') is null
begin
create table info_etmsrepcluster_company
(
    id          int              not null identity (1,1),
    company_id  int              not null,
    cluster_id  int              not null,
    currenttime datetime         null default (CURRENT_TIMESTAMP),
    moduser     varchar(16)      null default (dbo.Get_CurrentCode()),
    guid        uniqueidentifier null default (newid()),
    primary key (id)
)
end
go

if object_id('fk_info_etmsrepcluster_company_company_id') is null
begin
alter table info_etmsrepcluster_company with check add constraint fk_info_etmsrepcluster_company_company_id foreign key (company_id) references info_company (id) on delete cascade
alter table info_etmsrepcluster_company check constraint fk_info_etmsrepcluster_company_company_id
end
go

if object_id('fk_info_etmsrepcluster_company_cluster_id') is null
begin
alter table info_etmsrepcluster_company with check add constraint fk_info_etmsrepcluster_company_cluster_id foreign key (cluster_id) references info_etmsrepcluster_cluster (id) on delete cascade
alter table info_etmsrepcluster_company check constraint fk_info_etmsrepcluster_company_cluster_id
end
go

if not exists(select 1 from sys.indexes where name = 'ix_info_etmsrepcluster_company_company_id')
begin
create
nonclustered index ix_info_etmsrepcluster_company_company_id on info_etmsrepcluster_company (company_id)
end
go

if not exists(select 1 from sys.indexes where name = 'ix_info_etmsrepcluster_company_cluster_id')
begin
create
nonclustered index ix_info_etmsrepcluster_company_cluster_id on info_etmsrepcluster_company (cluster_id)
end
go

if object_id('info_etmsrepcluster_contact') is null
begin
create table info_etmsrepcluster_contact
(
    id          int              not null identity (1,1),
    contact_id  int              not null,
    cluster_id  int              not null,
    currenttime datetime         null default (CURRENT_TIMESTAMP),
    moduser     varchar(16)      null default (dbo.Get_CurrentCode()),
    guid        uniqueidentifier null default (newid()),
    primary key (id)
)
end
go

if object_id('fk_info_etmsrepcluster_company_company_id') is null
begin
alter table info_etmsrepcluster_contact with check add constraint fk_info_etmsrepcluster_contact_company_id foreign key (contact_id) references info_contact (id) on delete cascade
alter table info_etmsrepcluster_contact check constraint fk_info_etmsrepcluster_contact_company_id
end
go

if object_id('fk_info_etmsrepcluster_company_cluster_id') is null
begin
alter table info_etmsrepcluster_contact with check add constraint fk_info_etmsrepcluster_company_cluster_id foreign key (cluster_id) references info_etmsrepcluster_cluster (id) on delete cascade
alter table info_etmsrepcluster_contact check constraint fk_info_etmsrepcluster_contact_cluster_id
end
go

if not exists(select 1 from sys.indexes where name = 'ix_info_etmsrepcluster_contact_company_id')
begin
create
nonclustered index ix_info_etmsrepcluster_contact_company_id on info_etmsrepcluster_contact (contact_id)
end
go

if not exists(select 1 from sys.indexes where name = 'ix_info_etmsrepcluster_contact_cluster_id')
begin
create
nonclustered index ix_info_etmsrepcluster_contact_cluster_id on info_etmsrepcluster_contact (cluster_id)
end
go

merge info_languageinterface li
using (
  select data.[key], data.domain, l.id as language_id, data.translation from info_language l
                                                                               inner join (
    select 'Approved by [user]' as [key], 'geomarketing' as [domain], 'en' as slug, 'Approved by [user]' as translation
    union all
    select 'Approved by [user]' as [key], 'geomarketing' as [domain], 'ru' as slug, 'Утверждено пользователем [user]' as translation
    union all
    select 'Approved by [user]' as [key], 'geomarketing' as [domain], 'uk' as slug, 'Затверджено користувачем [user]' as translation
  ) data on data.slug = l.slug
) data on li.[key] = data.[key] and li.domain = data.domain and li.language_id = data.language_id
when not matched by target
  then
insert
([key], domain, language_id, translation)
values (data.[key], data.domain, data.language_id, data.translation);

IF NOT EXISTS( SELECT 1 FROM sys.indexes WHERE name = 'ix_info_polygon_polygonlayer_id')
BEGIN
CREATE
NONCLUSTERED INDEX ix_info_polygon_polygonlayer_id ON info_polygon ( polygonlayer_id )
end
go

IF NOT EXISTS( SELECT 1 FROM sys.indexes WHERE name = 'ix_info_polygonuser_version_id')
BEGIN
CREATE
NONCLUSTERED INDEX ix_info_polygonuser_version_id ON info_polygonuser ( version_id )
end
go

IF NOT EXISTS( SELECT 1 FROM sys.indexes WHERE name = 'ix_info_company_morionid')
BEGIN
CREATE
NONCLUSTERED INDEX ix_info_company_morionid ON info_company ( morionid )
end
go

IF OBJECT_ID(N'info_turnover', 'U') is NULL
    BEGIN
        -- auto-generated definition
        create table info_turnover
        (
            id           int identity
                constraint PK_info_turnover
                    primary key,
            company_id   int,
            dt           datetime,
            dt2          datetime,
            turnover     int,
            morionid     int,
            DATA_YEAR    int,
            DATA_QUARTER int,
            currenttime  datetime
                constraint DF_info_turnover_currenttime default getdate(),
            DATA_MONTH   int,
            CATEGORY     varchar(50),
            RX_OTC       varchar(50)
        )

        create index IX_info_turnover
            on info_turnover (company_id)

        create index ix_info_turnover_dt
            on info_turnover (dt) include (company_id, turnover)

    END
GO

if not exists (select 1 from sys.indexes where name = 'ix_info_turnover_dt')
begin
create
nonclustered index ix_info_turnover_dt on info_turnover (dt) include (company_id, turnover)
end

go

UPDATE info_languageinterface
SET translation = 'GeoForce'
WHERE domain = 'geomarketing'
  and [key] = 'geomarketing';

DELETE
FROM info_options
WHERE name = 'geomarketingTopBrickPalette'

go

UPDATE info_options
SET value = NULL
WHERE name = 'geomarketingEnableExperimentalFeatures'

go
    IF NOT EXISTS (
SELECT 1
FROM info_options
WHERE name = 'geomarketingProvinceStrokeWeight'
)
BEGIN
INSERT INTO info_options (name, value)
VALUES ('geomarketingProvinceStrokeWeight', 0.9)
END
go

IF NOT EXISTS(SELECT 1 FROM info_options WHERE name = 'geomarketingGridColumnGroup')
BEGIN
INSERT INTO info_options (name, value)
VALUES ('geomarketingGridColumnGroup', NULL)
end
go

IF EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE COLUMN_NAME = 'color' AND TABLE_NAME = 'po_comparisonfield')
BEGIN
ALTER TABLE po_comparisonfield DROP COLUMN color
end
go

IF EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE COLUMN_NAME = 'responsible' AND TABLE_NAME = 'po_comparisonfield')
BEGIN
ALTER TABLE po_comparisonfield DROP COLUMN responsible
end
go

IF EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE COLUMN_NAME = 'creator' AND TABLE_NAME = 'po_comparisonfield')
BEGIN
ALTER TABLE po_comparisonfield DROP COLUMN creator
end

go

update info_languageinterface
set translation = 'GeoForce'
where [key] = 'geomarketing'

go
    exec insertLanguageInterface 'en'
    , 'Set user pin'
    , N'Set user pin'
    , 'geomarketing';
exec insertLanguageInterface 'ru', 'Set user pin', N'Указать пин пользователя','geomarketing';
exec insertLanguageInterface 'uk', 'Set user pin', N'Вказати пін користувача','geomarketing';

exec insertLanguageInterface 'en', 'Toggle polygons', N'Toggle polygons','geomarketing';
exec insertLanguageInterface 'ru', 'Toggle polygons', N'Отобразить/скрыть полигоны','geomarketing';
exec insertLanguageInterface 'uk', 'Toggle polygons', N'Відобразити/приховати полігони','geomarketing';

exec insertLanguageInterface 'en', 'Toggle user polygons', N'Toggle user polygons','geomarketing';
exec insertLanguageInterface 'ru', 'Toggle user polygons', N'Отобразить/скрыть полигоны пользователя','geomarketing';
exec insertLanguageInterface 'uk', 'Toggle user polygons', N'Відобразити/приховати полігони користувача','geomarketing';

exec insertLanguageInterface 'en', 'Zoom to user polygons', N'Zoom to user polygons','geomarketing';
exec insertLanguageInterface 'ru', 'Zoom to user polygons', N'Приблизить к полигонам пользователя','geomarketing';
exec insertLanguageInterface 'uk', 'Zoom to user polygons', N'Наблизити до полігонів користувача','geomarketing';

exec insertLanguageInterface 'en', 'Open user details', N'Open user details','geomarketing';
exec insertLanguageInterface 'ru', 'Open user details', N'Показать информацию о пользователе','geomarketing';
exec insertLanguageInterface 'uk', 'Open user details', N'Відобразити інформацію про користувача','geomarketing';

exec insertLanguageInterface 'en', 'Loading custom layers for current direction', N'Loading custom layers for current direction','geomarketing';
exec insertLanguageInterface 'ru', 'Loading custom layers for current direction', N'Загрузка пользовательских слоев для текущего направления','geomarketing';
exec insertLanguageInterface 'uk', 'Loading custom layers for current direction', N'Завантаження користувацьких шарів для поточного напряму','geomarketing';

update info_service
set name = 'geomarketing'
where identifier = 'geomarketing'

go

update po_webservice
set name = 'geomarketing'
where identifier = 'geomarketing'

go
exec insertLanguageInterface 'uk'
    , 'geomarketing'
    , N'GeoForce'
    , 'messages';
exec insertLanguageInterface 'ru', 'geomarketing', N'GeoForce','messages';
exec insertLanguageInterface 'en', 'geomarketing', N'GeoForce','messages';
GO

IF NOT EXISTS( SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'info_etmsdatalayervalue' AND COLUMN_NAME = 'gpspoint') BEGIN
alter table info_etmsdatalayervalue add gpspoint geometry
    END
    GO

update info_etmsdatalayervalue set gpspoint = geometry::STGeomFromText('Point('+CAST([gps_lo] as VARCHAR(20))+' '+CAST([gps_la] as VARCHAR(20))+')',0)
GO

SET ANSI_NULLS ON;
SET ANSI_PADDING ON;
SET QUOTED_IDENTIFIER ON;
GO
IF NOT EXISTS(select 1 from sys.indexes where name = 'ix_info_etmsdatalayervalue_gpspoint') BEGIN
CREATE SPATIAL INDEX [ix_info_etmsdatalayervalue_gpspoint] ON [dbo].[info_etmsdatalayervalue]
	(
		[gpspoint]
	)USING  GEOMETRY_GRID
	WITH (
	BOUNDING_BOX =(15, 40, 180, 75), GRIDS =(LEVEL_1 = MEDIUM,LEVEL_2 = MEDIUM,LEVEL_3 = MEDIUM,LEVEL_4 = MEDIUM),
	CELLS_PER_OBJECT = 16, PAD_INDEX  = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
END
GO
SET QUOTED_IDENTIFIER OFF;
SET ANSI_NULLS OFF;
SET ANSI_PADDING OFF;
GO

DELETE FROM info_languageinterface WHERE [domain] = 'geomarketing' AND [key] IN ('[user] already pinned in this polygon', '[user] has no pin. Click on map to set', '{0} items selected', 'Add', 'Add companies to polygon', 'Add group', 'Add polygon', 'Additional responsible', 'Address', 'alerts.companiesAreMissing', 'alerts.emptyFilters', 'alerts.noCompanies', 'alerts.noPolygonsInSelection', 'alerts.please_complete_polygon', 'alerts.polygonsAreMissing', 'alerts.selfIntersection', 'alerts.threeVertices', 'All', 'Amount', 'Applied filters', 'Apply', 'Apply changes', 'Approve', 'approved', 'Approved by [user]', 'Approved list', 'Archive', 'Area', 'Auto update', 'Automatic generation polygon', 'Back', 'Balance', 'Balancing', 'Balancing error code', 'Balancing result', 'BI PharmXplorer', 'Brush', 'Can not edit', 'Cancel', 'Categories', 'Category', 'Check all', 'Choose region', 'Choose region part', 'cities', 'Close', 'Clustering regions', 'Color', 'Column', 'Column groups', 'Compactness', 'Companies', 'Company categories', 'Company types', 'Company visits', 'companyCategories', 'companyCategory', 'companyCnt', 'companyTypes', 'Compare', 'comparison', 'Comparison limit', 'Comparison per', 'Competitive group', 'Config grid column', 'Config group grid column', 'confirm', 'Contact category', 'contactCategory', 'contactCnt', 'Contacts', 'Contacts concentration', 'contactSpecialization', 'contactTargetGroups', 'contactTypes', 'Count', 'Create polygon', 'Create version', 'creator', 'CRM Targeting', 'Current', 'Current map version', 'Date', 'Delete', 'Deleting', 'Deselect all', 'Direction', 'Edit', 'Edit group', 'Enter polygon name', 'Erase', 'Erase all', 'Erase by filter', 'Erase by users and regions', 'Erasing', 'Error', 'Error starting balance, code', 'Fields saved', 'Filtering by username', 'Filters', 'Find company', 'For period', 'from', 'geomarketing', 'haveBrands', 'Highlight unassigned polygons', 'Info', 'km', 'Last available quarter', 'Last quarter', 'Layers', 'Less then two polygons in selection', 'Loading', 'Loading companies', 'Loading custom layers for current direction', 'Loading sales', 'm', 'Main', 'Manage map version', 'Map version not selected', 'Maps version name', 'Markers circle', 'Markers circle turnover', 'message', 'Min organizations in polygon', 'mln', 'Month', 'More', 'More about polygon', 'mssku', 'msskuByBrands', 'N/A', 'Name', 'new_version', 'No', 'No companies', 'No contacts', 'No custom layers', 'No data', 'No items selected', 'No pins within selected polygons', 'No responsible', 'No results', 'No sales', 'No turnover', 'No turnover per year', 'no version', 'No visits', 'noBrands', 'not approved', 'Not enough points to create layer', 'Not found', 'Not working', 'Nothing selected', 'notVisited', 'Open user details', 'Out of the map', 'Outside of polygon', 'Paint', 'Painting options', 'pcs', 'Perimeter', 'Pharmacy networks', 'Pins for users: [users] places in the same polygon', 'Please select a map version', 'Point division applied', 'Pointer', 'Points in polygon', 'Polygon', 'Polygon Name', 'Polygon name was successfully saved', 'polygons', 'polygons2', 'Population', 'Potential', 'Preparation', 'Preparation brand', 'Press enter to save', 'Promotions', 'Quarter', 'questions.confirmErase', 'questions.copy_paint_from_current_layer', 'questions.deletePolygon', 'questions.saveEdits', 'questions.undo', 'Redo', 'Redone', 'Refresh', 'Region', 'regions', 'Regions filled', 'Reject', 'Reset', 'Reset changes', 'Responsible', 'Responsible unassigned', 'Responsible was successfully saved', 'Ruler', 'Sales', 'Sales per year', 'Sales period', 'Sales plan', 'Save', 'Saved', 'Saving', 'Search', 'Select all', 'Selected', 'Send', 'Sheet', 'Sheet approved by user', 'Sheet will be approved for all subordinates', 'Show', 'Show all polygons', 'Specialization', 'Specializations', 'Split by points', 'Status', 'Stop balancing', 'Subordinates', 'Sum', 'Summary table', 'Task state', 'Task type', 'tendersku', 'Territories will be reset. Continue?', 'The subordination of the pharmacy network', 'This is your active version. Are you sure you want to make it archive?', 'to', 'Toggle user polygons', 'Toggle polygons', 'tomln', 'Tool panel', 'tooltip.geomarketingCompanyInfoTurnoverByMonthDisabled', 'tooltip.geomarketingCompanyInfoTurnoverByMonthEnabled', 'Top bricks', 'Top pharmacies', 'Total', 'Total coefficient', 'Turnover', 'turnoverRange', 'Type', 'Types', 'Unable to get status', 'Unable to start balancing', 'uncheck all', 'Undo', 'Undone', 'unknown', 'Untitled', 'User', 'user unassigned', 'users', 'Users and polygons', 'Value', 'Version', 'Version config', 'version.api', 'version.archive', 'version.name', 'version.targeting', 'Visits', 'visitsRange', 'Year', 'Yes', 'Zoom to all companies', 'Zoom to user polygons', 'Click to start drawing line', 'Company', 'Set user pin', 'Error loading', 'Loading more', 'No results', 'Searching', 'Blocked by [user]', 'Пользовательский', 'Область', 'Районы', 'Город', 'Почтовые индексы', 'Кварталы', 'Options', 'Project to layer', 'Cluster', 'Contact visits', 'Max visits per day', 'Work days', 'Stop', 'Loading companies & contacts', 'Total visits', 'Contact', 'Rep cluster', 'Top rep cluster count', 'Radius', 'Hold RMB and to draw a circle', 'Click on map to place a marker', 'Click on map to place a marker', 'Click to continue drawing polygon', 'Click first point to complete polygon', 'Click to start drawing polygon', 'Self-intersection is not allowed', 'Click to continue', 'Click last point to complete', 'Click to draw a line', 'Hold RMB to draw shape', 'Release RMB to stop', 'Cancel shape', 'Draw circle', 'Draw marker circle', 'Draw maker', 'Draw polygon', 'Draw polyline', 'Draw rectangle', 'Complete', 'Complete shape', 'Remove last vertex', 'Remove last drawn vertex', 'Press ''Cancel'' to discard changes', 'Drag vertexes and markers to edit shape', 'Click point to delete', 'Cancel editing, discard all changes', 'Clear all', 'Clear all layers', 'Save changes', 'Edit layers', 'No layers to edit', 'Remove layers', 'No layers to remove');

--add translation for key [user] already pinned in this polygon
exec insertLanguageInterface 'en', '[user] already pinned in this polygon', '[user] already pinned in this polygon', 'geomarketing';
exec insertLanguageInterface 'ru', '[user] already pinned in this polygon', '[user] прикреплен к этому полигону', 'geomarketing';
exec insertLanguageInterface 'uk', '[user] already pinned in this polygon', '[user] вже прикріплений до цього полігону', 'geomarketing';

--add translation for key [user] has no pin. Click on map to set
exec insertLanguageInterface 'en', '[user] has no pin. Click on map to set', '[user] has no pin. Click on map to set', 'geomarketing';
exec insertLanguageInterface 'ru', '[user] has no pin. Click on map to set', 'У пользователя [user] не установлена точка старта. Нажмите на карту чтобы установить пин', 'geomarketing';
exec insertLanguageInterface 'uk', '[user] has no pin. Click on map to set', 'У користувача [user] не встановлена точка старту. Натисніть на карту щоб встановити пін', 'geomarketing';

--add translation for key {0} items selected
exec insertLanguageInterface 'en', '{0} items selected', '{0} items selected', 'geomarketing';
exec insertLanguageInterface 'ru', '{0} items selected', '{0} выбрано', 'geomarketing';
exec insertLanguageInterface 'uk', '{0} items selected', '{0} обрано', 'geomarketing';

--add translation for key Add
exec insertLanguageInterface 'en', 'Add', 'Add', 'geomarketing';
exec insertLanguageInterface 'ru', 'Add', 'Добавить', 'geomarketing';
exec insertLanguageInterface 'uk', 'Add', 'Додати', 'geomarketing';

--add translation for key Add companies to polygon
exec insertLanguageInterface 'en', 'Add companies to polygon', 'Add companies to polygon', 'geomarketing';
exec insertLanguageInterface 'ru', 'Add companies to polygon', 'Добавить организации в полигон', 'geomarketing';
exec insertLanguageInterface 'uk', 'Add companies to polygon', 'Додати організації до полігону', 'geomarketing';

--add translation for key Add group
exec insertLanguageInterface 'en', 'Add group', 'Add group', 'geomarketing';
exec insertLanguageInterface 'ru', 'Add group', 'Добавить группу', 'geomarketing';
exec insertLanguageInterface 'uk', 'Add group', 'Додати групу', 'geomarketing';

--add translation for key Add polygon
exec insertLanguageInterface 'en', 'Add polygon', 'Create polygon', 'geomarketing';
exec insertLanguageInterface 'ru', 'Add polygon', 'Создать полигон', 'geomarketing';
exec insertLanguageInterface 'uk', 'Add polygon', 'Створити полігон', 'geomarketing';

--add translation for key Additional responsible
exec insertLanguageInterface 'en', 'Additional responsible', 'Additional responsible', 'geomarketing';
exec insertLanguageInterface 'ru', 'Additional responsible', 'Доп.ответственные', 'geomarketing';
exec insertLanguageInterface 'uk', 'Additional responsible', 'Додаткові відповідальні', 'geomarketing';

--add translation for key Address
exec insertLanguageInterface 'en', 'Address', 'Address', 'geomarketing';
exec insertLanguageInterface 'ru', 'Address', 'Адрес', 'geomarketing';
exec insertLanguageInterface 'uk', 'Address', 'Адреса', 'geomarketing';

--add translation for key alerts.companiesAreMissing
exec insertLanguageInterface 'en', 'alerts.companiesAreMissing', 'Companies are missing', 'geomarketing';
exec insertLanguageInterface 'ru', 'alerts.companiesAreMissing', 'Организации не найдены', 'geomarketing';
exec insertLanguageInterface 'uk', 'alerts.companiesAreMissing', 'Організацій не знайдено', 'geomarketing';

--add translation for key alerts.emptyFilters
exec insertLanguageInterface 'en', 'alerts.emptyFilters', 'Filters are empty. Please select something', 'geomarketing';
exec insertLanguageInterface 'ru', 'alerts.emptyFilters', 'Фильтр пуст. Выберите хоть один параметр', 'geomarketing';
exec insertLanguageInterface 'uk', 'alerts.emptyFilters', 'Зазначте параметри фильтрації для фільтру', 'geomarketing';

--add translation for key alerts.noCompanies
exec insertLanguageInterface 'en', 'alerts.noCompanies', 'There are no companies for [[userName]]', 'geomarketing';
exec insertLanguageInterface 'ru', 'alerts.noCompanies', 'Организации для [[userName]] отсутствуют', 'geomarketing';
exec insertLanguageInterface 'uk', 'alerts.noCompanies', 'Організації для [[userName]] відсутні', 'geomarketing';

--add translation for key alerts.noPolygonsInSelection
exec insertLanguageInterface 'en', 'alerts.noPolygonsInSelection', 'No polygons in selection', 'geomarketing';
exec insertLanguageInterface 'ru', 'alerts.noPolygonsInSelection', 'Отсутствуют полигоны в выборке', 'geomarketing';
exec insertLanguageInterface 'uk', 'alerts.noPolygonsInSelection', 'Відсутні полігони у вибірці', 'geomarketing';

--add translation for key alerts.please_complete_polygon
exec insertLanguageInterface 'en', 'alerts.please_complete_polygon', 'Please complete polygon', 'geomarketing';
exec insertLanguageInterface 'ru', 'alerts.please_complete_polygon', 'Пожалуйста завершите полигон', 'geomarketing';
exec insertLanguageInterface 'uk', 'alerts.please_complete_polygon', 'Будь ласка завершіть полігон', 'geomarketing';

--add translation for key alerts.polygonsAreMissing
exec insertLanguageInterface 'en', 'alerts.polygonsAreMissing', 'Polygons are missing', 'geomarketing';
exec insertLanguageInterface 'ru', 'alerts.polygonsAreMissing', 'Полигоны отсутствуют', 'geomarketing';
exec insertLanguageInterface 'uk', 'alerts.polygonsAreMissing', 'Полігони відсутні', 'geomarketing';

--add translation for key alerts.selfIntersection
exec insertLanguageInterface 'en', 'alerts.selfIntersection', 'Invalid polygon!<br>Polygons with self intersection are not allowed', 'geomarketing';
exec insertLanguageInterface 'ru', 'alerts.selfIntersection', 'Неправильный полигон!<br>Полигоны с самопересечением недопустимы.', 'geomarketing';
exec insertLanguageInterface 'uk', 'alerts.selfIntersection', 'Неправильний полігон!<br>Полігони з самоперетином не дозволені.', 'geomarketing';

--add translation for key alerts.threeVertices
exec insertLanguageInterface 'en', 'alerts.threeVertices', 'Polygon MUST have three vertices at least', 'geomarketing';
exec insertLanguageInterface 'ru', 'alerts.threeVertices', 'Полигоны должны иметь не менее трех вершин', 'geomarketing';
exec insertLanguageInterface 'uk', 'alerts.threeVertices', 'Полігони повинні мати не менше трьох вершин', 'geomarketing';

--add translation for key All
exec insertLanguageInterface 'en', 'All', 'All', 'geomarketing';
exec insertLanguageInterface 'ru', 'All', 'Все', 'geomarketing';
exec insertLanguageInterface 'uk', 'All', 'Усі', 'geomarketing';

--add translation for key Amount
exec insertLanguageInterface 'en', 'Amount', 'Amount', 'geomarketing';
exec insertLanguageInterface 'ru', 'Amount', 'Количество', 'geomarketing';
exec insertLanguageInterface 'uk', 'Amount', 'Кількість', 'geomarketing';

--add translation for key Applied filters
exec insertLanguageInterface 'en', 'Applied filters', 'Applied filters', 'geomarketing';
exec insertLanguageInterface 'ru', 'Applied filters', 'Применённые фильтры', 'geomarketing';
exec insertLanguageInterface 'uk', 'Applied filters', 'Застосовані фільтри', 'geomarketing';

--add translation for key Apply
exec insertLanguageInterface 'en', 'Apply', 'Apply', 'geomarketing';
exec insertLanguageInterface 'ru', 'Apply', 'Применить', 'geomarketing';
exec insertLanguageInterface 'uk', 'Apply', 'Застосувати', 'geomarketing';

--add translation for key Apply changes
exec insertLanguageInterface 'en', 'Apply changes', 'Apply changes', 'geomarketing';
exec insertLanguageInterface 'ru', 'Apply changes', 'Применить изменения', 'geomarketing';
exec insertLanguageInterface 'uk', 'Apply changes', 'Застосувати зміни', 'geomarketing';

--add translation for key Approve
exec insertLanguageInterface 'en', 'Approve', 'Approve', 'geomarketing';
exec insertLanguageInterface 'ru', 'Approve', 'Утвердить', 'geomarketing';
exec insertLanguageInterface 'uk', 'Approve', 'Затвердити', 'geomarketing';

--add translation for key approved
exec insertLanguageInterface 'en', 'approved', 'approved', 'geomarketing';
exec insertLanguageInterface 'ru', 'approved', 'утвержден', 'geomarketing';
exec insertLanguageInterface 'uk', 'approved', 'затверджений', 'geomarketing';

--add translation for key Approved by [user]
exec insertLanguageInterface 'en', 'Approved by [user]', 'Approved by user [user]', 'geomarketing';
exec insertLanguageInterface 'ru', 'Approved by [user]', 'Утверждено пользователем [user]', 'geomarketing';
exec insertLanguageInterface 'uk', 'Approved by [user]', 'Затверджено користувачем [user]', 'geomarketing';

--add translation for key Approved list
exec insertLanguageInterface 'en', 'Approved list', 'Approved list', 'geomarketing';
exec insertLanguageInterface 'ru', 'Approved list', 'Утвердили', 'geomarketing';
exec insertLanguageInterface 'uk', 'Approved list', 'Затвердили', 'geomarketing';

--add translation for key Archive
exec insertLanguageInterface 'en', 'Archive', 'Archive', 'geomarketing';
exec insertLanguageInterface 'ru', 'Archive', 'Архив', 'geomarketing';
exec insertLanguageInterface 'uk', 'Archive', 'Архів', 'geomarketing';

--add translation for key Area
exec insertLanguageInterface 'en', 'Area', 'Area', 'geomarketing';
exec insertLanguageInterface 'ru', 'Area', 'Пл.', 'geomarketing';
exec insertLanguageInterface 'uk', 'Area', 'Пл.', 'geomarketing';

--add translation for key Auto update
exec insertLanguageInterface 'en', 'Auto update', 'Auto refresh', 'geomarketing';
exec insertLanguageInterface 'ru', 'Auto update', 'Автообновление', 'geomarketing';
exec insertLanguageInterface 'uk', 'Auto update', 'Авто-оновлення', 'geomarketing';

--add translation for key Automatic generation polygon
exec insertLanguageInterface 'en', 'Automatic generation polygon', 'Automatic polygon generation', 'geomarketing';
exec insertLanguageInterface 'ru', 'Automatic generation polygon', 'Автоматическое создание полигона', 'geomarketing';
exec insertLanguageInterface 'uk', 'Automatic generation polygon', 'Автоматична генерація полігону', 'geomarketing';

--add translation for key Back
exec insertLanguageInterface 'en', 'Back', 'Back', 'geomarketing';
exec insertLanguageInterface 'ru', 'Back', 'Назад', 'geomarketing';
exec insertLanguageInterface 'uk', 'Back', 'Назад', 'geomarketing';

--add translation for key Balance
exec insertLanguageInterface 'en', 'Balance', 'Balance', 'geomarketing';
exec insertLanguageInterface 'ru', 'Balance', 'Балансировать', 'geomarketing';
exec insertLanguageInterface 'uk', 'Balance', 'Збалансувати', 'geomarketing';

--add translation for key Balancing
exec insertLanguageInterface 'en', 'Balancing', 'Balancing', 'geomarketing';
exec insertLanguageInterface 'ru', 'Balancing', 'Балансировка', 'geomarketing';
exec insertLanguageInterface 'uk', 'Balancing', 'Балансування', 'geomarketing';

--add translation for key Balancing error code
exec insertLanguageInterface 'en', 'Balancing error code', 'Balancing error code', 'geomarketing';
exec insertLanguageInterface 'ru', 'Balancing error code', 'Ошибка балансировки, код', 'geomarketing';
exec insertLanguageInterface 'uk', 'Balancing error code', 'Помилка балансування, код', 'geomarketing';

--add translation for key Balancing result
exec insertLanguageInterface 'en', 'Balancing result', 'Balancing result', 'geomarketing';
exec insertLanguageInterface 'ru', 'Balancing result', 'Результаты балансировки', 'geomarketing';
exec insertLanguageInterface 'uk', 'Balancing result', 'Результати балансування', 'geomarketing';

--add translation for key BI PharmXplorer
exec insertLanguageInterface 'en', 'BI PharmXplorer', 'BI PharmXplorer', 'geomarketing';
exec insertLanguageInterface 'ru', 'BI PharmXplorer', 'BI PharmXplorer', 'geomarketing';
exec insertLanguageInterface 'uk', 'BI PharmXplorer', 'BI PharmXplorer', 'geomarketing';

--add translation for key Brush
exec insertLanguageInterface 'en', 'Brush', 'Brush', 'geomarketing';
exec insertLanguageInterface 'ru', 'Brush', 'Кисть', 'geomarketing';
exec insertLanguageInterface 'uk', 'Brush', 'Фарбувати', 'geomarketing';

--add translation for key Can not edit
exec insertLanguageInterface 'en', 'Can not edit', 'Can not edit', 'geomarketing';
exec insertLanguageInterface 'ru', 'Can not edit', 'Редактирование недоступно', 'geomarketing';
exec insertLanguageInterface 'uk', 'Can not edit', 'Редагування недоступне', 'geomarketing';

--add translation for key Cancel
exec insertLanguageInterface 'en', 'Cancel', 'Cancel', 'geomarketing';
exec insertLanguageInterface 'ru', 'Cancel', 'Отменить', 'geomarketing';
exec insertLanguageInterface 'uk', 'Cancel', 'Скасувати', 'geomarketing';

--add translation for key Categories
exec insertLanguageInterface 'en', 'Categories', 'Categories', 'geomarketing';
exec insertLanguageInterface 'ru', 'Categories', 'Категории', 'geomarketing';
exec insertLanguageInterface 'uk', 'Categories', 'Категорії', 'geomarketing';

--add translation for key Category
exec insertLanguageInterface 'en', 'Category', 'Category', 'geomarketing';
exec insertLanguageInterface 'ru', 'Category', 'Категория', 'geomarketing';
exec insertLanguageInterface 'uk', 'Category', 'Категорія', 'geomarketing';

--add translation for key Check all
exec insertLanguageInterface 'en', 'Check all', 'Check all', 'geomarketing';
exec insertLanguageInterface 'ru', 'Check all', 'Отметить все', 'geomarketing';
exec insertLanguageInterface 'uk', 'Check all', 'Обрати усі', 'geomarketing';

--add translation for key Choose region
exec insertLanguageInterface 'en', 'Choose region', 'Choose region', 'geomarketing';
exec insertLanguageInterface 'ru', 'Choose region', 'Выберите область', 'geomarketing';
exec insertLanguageInterface 'uk', 'Choose region', 'Оберіть обасть', 'geomarketing';

--add translation for key Choose region part
exec insertLanguageInterface 'en', 'Choose region part', 'Choose region part', 'geomarketing';
exec insertLanguageInterface 'ru', 'Choose region part', 'Выберите регион', 'geomarketing';
exec insertLanguageInterface 'uk', 'Choose region part', 'Оберіть регіон', 'geomarketing';

--add translation for key cities
exec insertLanguageInterface 'en', 'cities', 'Cities', 'geomarketing';
exec insertLanguageInterface 'ru', 'cities', 'Города', 'geomarketing';
exec insertLanguageInterface 'uk', 'cities', 'Міста', 'geomarketing';

--add translation for key Close
exec insertLanguageInterface 'en', 'Close', 'Close', 'geomarketing';
exec insertLanguageInterface 'ru', 'Close', 'Закрыть', 'geomarketing';
exec insertLanguageInterface 'uk', 'Close', 'Закрити', 'geomarketing';

--add translation for key Clustering regions
exec insertLanguageInterface 'en', 'Clustering regions', 'Clustering regions', 'geomarketing';
exec insertLanguageInterface 'ru', 'Clustering regions', 'Кластеризация облестей', 'geomarketing';
exec insertLanguageInterface 'uk', 'Clustering regions', 'Кластеризація областей', 'geomarketing';

--add translation for key Color
exec insertLanguageInterface 'en', 'Color', 'Color', 'geomarketing';
exec insertLanguageInterface 'ru', 'Color', 'Цвет', 'geomarketing';
exec insertLanguageInterface 'uk', 'Color', 'Колір', 'geomarketing';

--add translation for key Column
exec insertLanguageInterface 'en', 'Column', 'Columns', 'geomarketing';
exec insertLanguageInterface 'ru', 'Column', 'Колонки', 'geomarketing';
exec insertLanguageInterface 'uk', 'Column', 'Колонки', 'geomarketing';

--add translation for key Column groups
exec insertLanguageInterface 'en', 'Column groups', 'Column groups', 'geomarketing';
exec insertLanguageInterface 'ru', 'Column groups', 'Группы колонок', 'geomarketing';
exec insertLanguageInterface 'uk', 'Column groups', 'Групи колонок', 'geomarketing';

--add translation for key Compactness
exec insertLanguageInterface 'en', 'Compactness', 'Compactness', 'geomarketing';
exec insertLanguageInterface 'ru', 'Compactness', 'Компактность территории', 'geomarketing';
exec insertLanguageInterface 'uk', 'Compactness', 'Компактність території', 'geomarketing';

--add translation for key Companies
exec insertLanguageInterface 'en', 'Companies', 'Companies', 'geomarketing';
exec insertLanguageInterface 'ru', 'Companies', 'Организации', 'geomarketing';
exec insertLanguageInterface 'uk', 'Companies', 'Організації', 'geomarketing';

--add translation for key Company categories
exec insertLanguageInterface 'en', 'Company categories', 'Company categories', 'geomarketing';
exec insertLanguageInterface 'ru', 'Company categories', 'Категории организаций', 'geomarketing';
exec insertLanguageInterface 'uk', 'Company categories', 'Категорії організацій', 'geomarketing';

--add translation for key Company types
exec insertLanguageInterface 'en', 'Company types', 'Company types', 'geomarketing';
exec insertLanguageInterface 'ru', 'Company types', 'Типы организаций', 'geomarketing';
exec insertLanguageInterface 'uk', 'Company types', 'Типи організацій', 'geomarketing';

--add translation for key Company visits
exec insertLanguageInterface 'en', 'Company visits', 'Company visits', 'geomarketing';
exec insertLanguageInterface 'ru', 'Company visits', 'Визиты в организации', 'geomarketing';
exec insertLanguageInterface 'uk', 'Company visits', 'Візити до компаній', 'geomarketing';

--add translation for key companyCategories
exec insertLanguageInterface 'en', 'companyCategories', 'Companies by category', 'geomarketing';
exec insertLanguageInterface 'ru', 'companyCategories', 'Организации по категориям', 'geomarketing';
exec insertLanguageInterface 'uk', 'companyCategories', 'Організації за категоріями', 'geomarketing';

--add translation for key companyCategory
exec insertLanguageInterface 'en', 'companyCategory', 'Company categories', 'geomarketing';
exec insertLanguageInterface 'ru', 'companyCategory', 'Категории организаций', 'geomarketing';
exec insertLanguageInterface 'uk', 'companyCategory', 'Категорії організацій', 'geomarketing';

--add translation for key companyCnt
exec insertLanguageInterface 'en', 'companyCnt', 'Companies', 'geomarketing';
exec insertLanguageInterface 'ru', 'companyCnt', 'Организаций', 'geomarketing';
exec insertLanguageInterface 'uk', 'companyCnt', 'Організацій', 'geomarketing';

--add translation for key companyTypes
exec insertLanguageInterface 'en', 'companyTypes', 'Companies by type', 'geomarketing';
exec insertLanguageInterface 'ru', 'companyTypes', 'Организации по типам', 'geomarketing';
exec insertLanguageInterface 'uk', 'companyTypes', 'Організації по типам', 'geomarketing';

--add translation for key Compare
exec insertLanguageInterface 'en', 'Compare', 'Compare', 'geomarketing';
exec insertLanguageInterface 'ru', 'Compare', 'Сравнить', 'geomarketing';
exec insertLanguageInterface 'uk', 'Compare', 'Порівняти', 'geomarketing';

--add translation for key comparison
exec insertLanguageInterface 'en', 'comparison', 'Comparison', 'geomarketing';
exec insertLanguageInterface 'ru', 'comparison', 'Сравнение', 'geomarketing';
exec insertLanguageInterface 'uk', 'comparison', 'Порівняння', 'geomarketing';

--add translation for key Comparison limit
exec insertLanguageInterface 'en', 'Comparison limit', 'Select from two to five elements', 'geomarketing';
exec insertLanguageInterface 'ru', 'Comparison limit', 'Выберите от двух до пяти элементов', 'geomarketing';
exec insertLanguageInterface 'uk', 'Comparison limit', 'Оберіть від двох до п''яти елементів', 'geomarketing';

--add translation for key Comparison per
exec insertLanguageInterface 'en', 'Comparison per', 'Comparison by:', 'geomarketing';
exec insertLanguageInterface 'ru', 'Comparison per', 'Сравнение по:', 'geomarketing';
exec insertLanguageInterface 'uk', 'Comparison per', 'Порівняння за:', 'geomarketing';

--add translation for key Competitive group
exec insertLanguageInterface 'en', 'Competitive group', 'Competitive groups', 'geomarketing';
exec insertLanguageInterface 'ru', 'Competitive group', 'Конкурентные группы', 'geomarketing';
exec insertLanguageInterface 'uk', 'Competitive group', 'Конкурентні групи', 'geomarketing';

--add translation for key Config grid column
exec insertLanguageInterface 'en', 'Config grid column', 'Config grid comlumns', 'geomarketing';
exec insertLanguageInterface 'ru', 'Config grid column', 'Настроить колонки', 'geomarketing';
exec insertLanguageInterface 'uk', 'Config grid column', 'Налаштувати колонки', 'geomarketing';

--add translation for key Config group grid column
exec insertLanguageInterface 'en', 'Config group grid column', 'Config grid column groups', 'geomarketing';
exec insertLanguageInterface 'ru', 'Config group grid column', 'Настроить группы колонок', 'geomarketing';
exec insertLanguageInterface 'uk', 'Config group grid column', 'Налаштувати групи колонок', 'geomarketing';

--add translation for key confirm
exec insertLanguageInterface 'en', 'confirm', 'Confirm', 'geomarketing';
exec insertLanguageInterface 'ru', 'confirm', 'Подтвердите', 'geomarketing';
exec insertLanguageInterface 'uk', 'confirm', 'Підтвердіть', 'geomarketing';

--add translation for key Contact category
exec insertLanguageInterface 'en', 'Contact category', 'Contact category', 'geomarketing';
exec insertLanguageInterface 'ru', 'Contact category', 'Категорія клієнта', 'geomarketing';
exec insertLanguageInterface 'uk', 'Contact category', 'Категорія спеціаліств', 'geomarketing';

--add translation for key contactCategory
exec insertLanguageInterface 'en', 'contactCategory', 'Contacts per category', 'geomarketing';
exec insertLanguageInterface 'ru', 'contactCategory', 'Специалистов по категориям', 'geomarketing';
exec insertLanguageInterface 'uk', 'contactCategory', 'Спеціалістів за категоріями', 'geomarketing';

--add translation for key contactCnt
exec insertLanguageInterface 'en', 'contactCnt', 'Contacts', 'geomarketing';
exec insertLanguageInterface 'ru', 'contactCnt', 'Специалистов', 'geomarketing';
exec insertLanguageInterface 'uk', 'contactCnt', 'Спеціалістів', 'geomarketing';

--add translation for key Contacts
exec insertLanguageInterface 'en', 'Contacts', 'Employees', 'geomarketing';
exec insertLanguageInterface 'ru', 'Contacts', 'Сотрудники', 'geomarketing';
exec insertLanguageInterface 'uk', 'Contacts', 'Співробітники', 'geomarketing';

--add translation for key Contacts concentration
exec insertLanguageInterface 'en', 'Contacts concentration', 'Contacts concentration', 'geomarketing';
exec insertLanguageInterface 'ru', 'Contacts concentration', 'Концентрация специалистов', 'geomarketing';
exec insertLanguageInterface 'uk', 'Contacts concentration', 'Концентрація спеціалістів', 'geomarketing';

--add translation for key contactSpecialization
exec insertLanguageInterface 'en', 'contactSpecialization', 'Contacts per specialization', 'geomarketing';
exec insertLanguageInterface 'ru', 'contactSpecialization', 'Специалистов по специализациям', 'geomarketing';
exec insertLanguageInterface 'uk', 'contactSpecialization', 'Спеціалістів за спеціазаціями', 'geomarketing';

--add translation for key contactTargetGroups
exec insertLanguageInterface 'en', 'contactTargetGroups', 'Contacts per target', 'geomarketing';
exec insertLanguageInterface 'ru', 'contactTargetGroups', 'Специалистов по таргет-группам', 'geomarketing';
exec insertLanguageInterface 'uk', 'contactTargetGroups', 'Спеціалістів за таргет-групами', 'geomarketing';

--add translation for key contactTypes
exec insertLanguageInterface 'en', 'contactTypes', 'Contacts', 'geomarketing';
exec insertLanguageInterface 'ru', 'contactTypes', 'Специалистов', 'geomarketing';
exec insertLanguageInterface 'uk', 'contactTypes', 'Спеціалістів', 'geomarketing';

--add translation for key Count
exec insertLanguageInterface 'en', 'Count', 'Count', 'geomarketing';
exec insertLanguageInterface 'ru', 'Count', 'К-во', 'geomarketing';
exec insertLanguageInterface 'uk', 'Count', 'К-сть', 'geomarketing';

--add translation for key Create polygon
exec insertLanguageInterface 'en', 'Create polygon', 'Create polygon', 'geomarketing';
exec insertLanguageInterface 'ru', 'Create polygon', 'Создать полигон', 'geomarketing';
exec insertLanguageInterface 'uk', 'Create polygon', 'Створити полігон', 'geomarketing';

--add translation for key Create version
exec insertLanguageInterface 'en', 'Create version', 'Create version', 'geomarketing';
exec insertLanguageInterface 'ru', 'Create version', 'Создать версию', 'geomarketing';
exec insertLanguageInterface 'uk', 'Create version', 'Створити версію', 'geomarketing';

--add translation for key creator
exec insertLanguageInterface 'en', 'creator', 'Creator', 'geomarketing';
exec insertLanguageInterface 'ru', 'creator', 'Создатель', 'geomarketing';
exec insertLanguageInterface 'uk', 'creator', 'Автор', 'geomarketing';

--add translation for key CRM Targeting
exec insertLanguageInterface 'en', 'CRM Targeting', 'CRM Targeting', 'geomarketing';
exec insertLanguageInterface 'ru', 'CRM Targeting', 'CRM Таргетирование', 'geomarketing';
exec insertLanguageInterface 'uk', 'CRM Targeting', 'CRM Таргетування', 'geomarketing';

--add translation for key Current
exec insertLanguageInterface 'en', 'Current', 'Current', 'geomarketing';
exec insertLanguageInterface 'ru', 'Current', 'Текущая', 'geomarketing';
exec insertLanguageInterface 'uk', 'Current', 'Поточна', 'geomarketing';

--add translation for key Current map version
exec insertLanguageInterface 'en', 'Current map version', 'Current map version', 'geomarketing';
exec insertLanguageInterface 'ru', 'Current map version', 'Текущая версия', 'geomarketing';
exec insertLanguageInterface 'uk', 'Current map version', 'Поточна версія', 'geomarketing';

--add translation for key Date
exec insertLanguageInterface 'en', 'Date', 'Date', 'geomarketing';
exec insertLanguageInterface 'ru', 'Date', 'Дата', 'geomarketing';
exec insertLanguageInterface 'uk', 'Date', 'Дата', 'geomarketing';

--add translation for key Delete
exec insertLanguageInterface 'en', 'Delete', 'Delete', 'geomarketing';
exec insertLanguageInterface 'ru', 'Delete', 'Удалить', 'geomarketing';
exec insertLanguageInterface 'uk', 'Delete', 'Видалити', 'geomarketing';

--add translation for key Deleting
exec insertLanguageInterface 'en', 'Deleting', 'Deleting', 'geomarketing';
exec insertLanguageInterface 'ru', 'Deleting', 'Удаление...', 'geomarketing';
exec insertLanguageInterface 'uk', 'Deleting', 'Видалення...', 'geomarketing';

--add translation for key Deselect all
exec insertLanguageInterface 'en', 'Deselect all', 'Deselect all', 'geomarketing';
exec insertLanguageInterface 'ru', 'Deselect all', 'Отменить все', 'geomarketing';
exec insertLanguageInterface 'uk', 'Deselect all', 'Відмінити усі', 'geomarketing';

--add translation for key Direction
exec insertLanguageInterface 'en', 'Direction', 'Direction', 'geomarketing';
exec insertLanguageInterface 'ru', 'Direction', 'Продуктовое направление', 'geomarketing';
exec insertLanguageInterface 'uk', 'Direction', 'Продуктова лінійка', 'geomarketing';

--add translation for key Edit
exec insertLanguageInterface 'en', 'Edit', 'Edit', 'geomarketing';
exec insertLanguageInterface 'ru', 'Edit', 'Редактировать', 'geomarketing';
exec insertLanguageInterface 'uk', 'Edit', 'Редагувати', 'geomarketing';

--add translation for key Edit group
exec insertLanguageInterface 'en', 'Edit group', 'Edit group', 'geomarketing';
exec insertLanguageInterface 'ru', 'Edit group', 'Редактировать группу', 'geomarketing';
exec insertLanguageInterface 'uk', 'Edit group', 'Редагувати групу', 'geomarketing';

--add translation for key Enter polygon name
exec insertLanguageInterface 'en', 'Enter polygon name', 'Enter polygon name', 'geomarketing';
exec insertLanguageInterface 'ru', 'Enter polygon name', 'Укажите название полигона', 'geomarketing';
exec insertLanguageInterface 'uk', 'Enter polygon name', 'Вкажіть назву полігона', 'geomarketing';

--add translation for key Erase
exec insertLanguageInterface 'en', 'Erase', 'Erase', 'geomarketing';
exec insertLanguageInterface 'ru', 'Erase', 'Стереть', 'geomarketing';
exec insertLanguageInterface 'uk', 'Erase', 'Стерти', 'geomarketing';

--add translation for key Erase all
exec insertLanguageInterface 'en', 'Erase all', 'Erase all', 'geomarketing';
exec insertLanguageInterface 'ru', 'Erase all', 'Стереть все', 'geomarketing';
exec insertLanguageInterface 'uk', 'Erase all', 'Стерти усе', 'geomarketing';

--add translation for key Erase by filter
exec insertLanguageInterface 'en', 'Erase by filter', 'Erase by filter', 'geomarketing';
exec insertLanguageInterface 'ru', 'Erase by filter', 'Стереть по фильтру', 'geomarketing';
exec insertLanguageInterface 'uk', 'Erase by filter', 'Стерти за фільтром', 'geomarketing';

--add translation for key Erase by users and regions
exec insertLanguageInterface 'en', 'Erase by users and regions', 'Erase by users and regions', 'geomarketing';
exec insertLanguageInterface 'ru', 'Erase by users and regions', 'Стереть по пользователям и областям', 'geomarketing';
exec insertLanguageInterface 'uk', 'Erase by users and regions', 'Стерти за користувачами та областями', 'geomarketing';

--add translation for key Erasing
exec insertLanguageInterface 'en', 'Erasing', 'Erasing', 'geomarketing';
exec insertLanguageInterface 'ru', 'Erasing', 'Стирание', 'geomarketing';
exec insertLanguageInterface 'uk', 'Erasing', 'Стирання', 'geomarketing';

--add translation for key Error
exec insertLanguageInterface 'en', 'Error', 'Error!', 'geomarketing';
exec insertLanguageInterface 'ru', 'Error', 'Ошибка!', 'geomarketing';
exec insertLanguageInterface 'uk', 'Error', 'Помилка!', 'geomarketing';

--add translation for key Error starting balance, code
exec insertLanguageInterface 'en', 'Error starting balance, code', 'Balancing start error. Code', 'geomarketing';
exec insertLanguageInterface 'ru', 'Error starting balance, code', 'Ошибка при старте балансировки. Код ', 'geomarketing';
exec insertLanguageInterface 'uk', 'Error starting balance, code', 'Помилка при старті балансування. Код', 'geomarketing';

--add translation for key Fields saved
exec insertLanguageInterface 'en', 'Fields saved', 'Fields saved', 'geomarketing';
exec insertLanguageInterface 'ru', 'Fields saved', 'Поля сохранены', 'geomarketing';
exec insertLanguageInterface 'uk', 'Fields saved', 'Поля збережені', 'geomarketing';

--add translation for key Filtering by username
exec insertLanguageInterface 'en', 'Filtering by username', 'Filter by username', 'geomarketing';
exec insertLanguageInterface 'ru', 'Filtering by username', 'Фильтр по имени пользователя', 'geomarketing';
exec insertLanguageInterface 'uk', 'Filtering by username', 'Фільтр по імені користувача', 'geomarketing';

--add translation for key Filters
exec insertLanguageInterface 'en', 'Filters', 'Company filters', 'geomarketing';
exec insertLanguageInterface 'ru', 'Filters', 'Фильтр организаций', 'geomarketing';
exec insertLanguageInterface 'uk', 'Filters', 'Фільтр організацій', 'geomarketing';

--add translation for key Find company
exec insertLanguageInterface 'en', 'Find company', 'Find company', 'geomarketing';
exec insertLanguageInterface 'ru', 'Find company', 'Найти организацию', 'geomarketing';
exec insertLanguageInterface 'uk', 'Find company', 'Знайти організацію', 'geomarketing';

--add translation for key For period
exec insertLanguageInterface 'en', 'For period', 'For period', 'geomarketing';
exec insertLanguageInterface 'ru', 'For period', 'За период', 'geomarketing';
exec insertLanguageInterface 'uk', 'For period', 'За період', 'geomarketing';

--add translation for key from
exec insertLanguageInterface 'en', 'from', 'from', 'geomarketing';
exec insertLanguageInterface 'ru', 'from', 'с', 'geomarketing';
exec insertLanguageInterface 'uk', 'from', 'з', 'geomarketing';

--add translation for key geomarketing
exec insertLanguageInterface 'en', 'geomarketing', 'GeoForce', 'geomarketing';
exec insertLanguageInterface 'ru', 'geomarketing', 'GeoForce', 'geomarketing';
exec insertLanguageInterface 'uk', 'geomarketing', 'GeoForce', 'geomarketing';

--add translation for key haveBrands
exec insertLanguageInterface 'en', 'haveBrands', 'Sale IN', 'geomarketing';
exec insertLanguageInterface 'ru', 'haveBrands', 'Продажи брендов', 'geomarketing';
exec insertLanguageInterface 'uk', 'haveBrands', 'Продажі брендів', 'geomarketing';

--add translation for key Highlight unassigned polygons
exec insertLanguageInterface 'en', 'Highlight unassigned polygons', 'Highlight unassigned polygons', 'geomarketing';
exec insertLanguageInterface 'ru', 'Highlight unassigned polygons', 'Подсветить неназначенные полигоны', 'geomarketing';
exec insertLanguageInterface 'uk', 'Highlight unassigned polygons', 'Підсвітити непризначені полігони', 'geomarketing';

--add translation for key Info
exec insertLanguageInterface 'en', 'Info', 'Info', 'geomarketing';
exec insertLanguageInterface 'ru', 'Info', 'Информация', 'geomarketing';
exec insertLanguageInterface 'uk', 'Info', 'Інформація', 'geomarketing';

--add translation for key km
exec insertLanguageInterface 'en', 'km', 'km', 'geomarketing';
exec insertLanguageInterface 'ru', 'km', 'км', 'geomarketing';
exec insertLanguageInterface 'uk', 'km', 'км', 'geomarketing';

--add translation for key Last available quarter
exec insertLanguageInterface 'en', 'Last available quarter', 'Last available quarter', 'geomarketing';
exec insertLanguageInterface 'ru', 'Last available quarter', 'Последний доступный квартал', 'geomarketing';
exec insertLanguageInterface 'uk', 'Last available quarter', 'Останній доступний квартал', 'geomarketing';

--add translation for key Last quarter
exec insertLanguageInterface 'en', 'Last quarter', 'Last quarter', 'geomarketing';
exec insertLanguageInterface 'ru', 'Last quarter', 'Последний квартал', 'geomarketing';
exec insertLanguageInterface 'uk', 'Last quarter', 'Останній квартал', 'geomarketing';

--add translation for key Layers
exec insertLanguageInterface 'en', 'Layers', 'Layers', 'geomarketing';
exec insertLanguageInterface 'ru', 'Layers', 'Слои', 'geomarketing';
exec insertLanguageInterface 'uk', 'Layers', 'Шари', 'geomarketing';

--add translation for key Less then two polygons in selection
exec insertLanguageInterface 'en', 'Less then two polygons in selection', 'Less then two polygons in selection', 'geomarketing';
exec insertLanguageInterface 'ru', 'Less then two polygons in selection', 'Меньше двух полигонов в выбранной територии', 'geomarketing';
exec insertLanguageInterface 'uk', 'Less then two polygons in selection', 'Менше двох полігонів на обраній туриторії', 'geomarketing';

--add translation for key Loading
exec insertLanguageInterface 'en', 'Loading', 'Loading...', 'geomarketing';
exec insertLanguageInterface 'ru', 'Loading', 'Загрузка...', 'geomarketing';
exec insertLanguageInterface 'uk', 'Loading', 'Завантаження...', 'geomarketing';

--add translation for key Loading companies
exec insertLanguageInterface 'en', 'Loading companies', 'Loading companies...', 'geomarketing';
exec insertLanguageInterface 'ru', 'Loading companies', 'Загрузка организаций...', 'geomarketing';
exec insertLanguageInterface 'uk', 'Loading companies', 'Завантаження організацій...', 'geomarketing';

--add translation for key Loading custom layers for current direction
exec insertLanguageInterface 'en', 'Loading custom layers for current direction', 'Loading custom layers', 'geomarketing';
exec insertLanguageInterface 'ru', 'Loading custom layers for current direction', 'Загрузка слоев', 'geomarketing';
exec insertLanguageInterface 'uk', 'Loading custom layers for current direction', 'Завантаження шарів', 'geomarketing';

--add translation for key Loading sales
exec insertLanguageInterface 'en', 'Loading sales', 'Loading sales', 'geomarketing';
exec insertLanguageInterface 'ru', 'Loading sales', 'Загрузка продаж...', 'geomarketing';
exec insertLanguageInterface 'uk', 'Loading sales', 'Завантаження продажів...', 'geomarketing';

--add translation for key m
exec insertLanguageInterface 'en', 'm', 'm', 'geomarketing';
exec insertLanguageInterface 'ru', 'm', 'м', 'geomarketing';
exec insertLanguageInterface 'uk', 'm', 'м', 'geomarketing';

--add translation for key Main
exec insertLanguageInterface 'en', 'Main', 'Main', 'geomarketing';
exec insertLanguageInterface 'ru', 'Main', 'Основное', 'geomarketing';
exec insertLanguageInterface 'uk', 'Main', 'Головне', 'geomarketing';

--add translation for key Manage map version
exec insertLanguageInterface 'en', 'Manage map version', 'Manage map versions', 'geomarketing';
exec insertLanguageInterface 'ru', 'Manage map version', 'Управление версиями карты', 'geomarketing';
exec insertLanguageInterface 'uk', 'Manage map version', 'Керування версіями мапи', 'geomarketing';

--add translation for key Map version not selected
exec insertLanguageInterface 'en', 'Map version not selected', 'Map version not selected', 'geomarketing';
exec insertLanguageInterface 'ru', 'Map version not selected', 'Версия не указана', 'geomarketing';
exec insertLanguageInterface 'uk', 'Map version not selected', 'Версія не вказана', 'geomarketing';

--add translation for key Maps version name
exec insertLanguageInterface 'en', 'Maps version name', 'Name', 'geomarketing';
exec insertLanguageInterface 'ru', 'Maps version name', 'Название', 'geomarketing';
exec insertLanguageInterface 'uk', 'Maps version name', 'Назва', 'geomarketing';

--add translation for key Markers circle
exec insertLanguageInterface 'en', 'Markers circle', 'Contact amount', 'geomarketing';
exec insertLanguageInterface 'ru', 'Markers circle', 'Количество специалистов', 'geomarketing';
exec insertLanguageInterface 'uk', 'Markers circle', 'Кількість спеціалістів', 'geomarketing';

--add translation for key Markers circle turnover
exec insertLanguageInterface 'en', 'Markers circle turnover', 'Turnover', 'geomarketing';
exec insertLanguageInterface 'ru', 'Markers circle turnover', 'Товарооборот', 'geomarketing';
exec insertLanguageInterface 'uk', 'Markers circle turnover', 'Товарообіг', 'geomarketing';

--add translation for key message
exec insertLanguageInterface 'en', 'message', 'Message', 'geomarketing';
exec insertLanguageInterface 'ru', 'message', 'Сообщение', 'geomarketing';
exec insertLanguageInterface 'uk', 'message', 'Сповіщення', 'geomarketing';

--add translation for key Min organizations in polygon
exec insertLanguageInterface 'en', 'Min organizations in polygon', 'Min organizations in polygon', 'geomarketing';
exec insertLanguageInterface 'ru', 'Min organizations in polygon', 'Минимум организаций в полигоне', 'geomarketing';
exec insertLanguageInterface 'uk', 'Min organizations in polygon', 'Мінімум організацій у полігоні', 'geomarketing';

--add translation for key mln
exec insertLanguageInterface 'en', 'mln', 'mln', 'geomarketing';
exec insertLanguageInterface 'ru', 'mln', 'млн', 'geomarketing';
exec insertLanguageInterface 'uk', 'mln', 'млн', 'geomarketing';

--add translation for key Month
exec insertLanguageInterface 'en', 'Month', 'Month', 'geomarketing';
exec insertLanguageInterface 'ru', 'Month', 'Месяц', 'geomarketing';
exec insertLanguageInterface 'uk', 'Month', 'Місяць', 'geomarketing';

--add translation for key More
exec insertLanguageInterface 'en', 'More', 'More', 'geomarketing';
exec insertLanguageInterface 'ru', 'More', 'Ещё', 'geomarketing';
exec insertLanguageInterface 'uk', 'More', 'Більше', 'geomarketing';

--add translation for key More about polygon
exec insertLanguageInterface 'en', 'More about polygon', 'More abount polygon', 'geomarketing';
exec insertLanguageInterface 'ru', 'More about polygon', 'Подробнее о полигоне', 'geomarketing';
exec insertLanguageInterface 'uk', 'More about polygon', 'Інформація про полігон', 'geomarketing';

--add translation for key mssku
exec insertLanguageInterface 'en', 'mssku', 'MS and SKU', 'geomarketing';
exec insertLanguageInterface 'ru', 'mssku', 'MS и SKU', 'geomarketing';
exec insertLanguageInterface 'uk', 'mssku', 'MS та SKU', 'geomarketing';

--add translation for key msskuByBrands
exec insertLanguageInterface 'en', 'msskuByBrands', 'MS and brand', 'geomarketing';
exec insertLanguageInterface 'ru', 'msskuByBrands', 'MS и Бренд', 'geomarketing';
exec insertLanguageInterface 'uk', 'msskuByBrands', 'MS та Бренд', 'geomarketing';

--add translation for key N/A
exec insertLanguageInterface 'en', 'N/A', 'N/A', 'geomarketing';
exec insertLanguageInterface 'ru', 'N/A', 'Н/Д', 'geomarketing';
exec insertLanguageInterface 'uk', 'N/A', 'Н/З', 'geomarketing';

--add translation for key Name
exec insertLanguageInterface 'en', 'Name', 'Name', 'geomarketing';
exec insertLanguageInterface 'ru', 'Name', 'Название', 'geomarketing';
exec insertLanguageInterface 'uk', 'Name', 'Назва', 'geomarketing';

--add translation for key new_version
exec insertLanguageInterface 'en', 'new_version', 'New version', 'geomarketing';
exec insertLanguageInterface 'ru', 'new_version', 'Новая версия', 'geomarketing';
exec insertLanguageInterface 'uk', 'new_version', 'Нова версія', 'geomarketing';

--add translation for key No
exec insertLanguageInterface 'en', 'No', 'No', 'geomarketing';
exec insertLanguageInterface 'ru', 'No', 'Нет', 'geomarketing';
exec insertLanguageInterface 'uk', 'No', 'Ні', 'geomarketing';

--add translation for key No companies
exec insertLanguageInterface 'en', 'No companies', 'No companies', 'geomarketing';
exec insertLanguageInterface 'ru', 'No companies', 'Нет организаций', 'geomarketing';
exec insertLanguageInterface 'uk', 'No companies', 'Организіії відсутні', 'geomarketing';

--add translation for key No contacts
exec insertLanguageInterface 'en', 'No contacts', 'No contacts', 'geomarketing';
exec insertLanguageInterface 'ru', 'No contacts', 'Нет сотрудников', 'geomarketing';
exec insertLanguageInterface 'uk', 'No contacts', 'Співробітники відсутні', 'geomarketing';

--add translation for key No custom layers
exec insertLanguageInterface 'en', 'No custom layers', 'No custom layers', 'geomarketing';
exec insertLanguageInterface 'ru', 'No custom layers', 'Нет дополнительных слоев', 'geomarketing';
exec insertLanguageInterface 'uk', 'No custom layers', 'Немає додаткових шарів', 'geomarketing';

--add translation for key No data
exec insertLanguageInterface 'en', 'No data', 'No data', 'geomarketing';
exec insertLanguageInterface 'ru', 'No data', 'Нет данных', 'geomarketing';
exec insertLanguageInterface 'uk', 'No data', 'Немає даних', 'geomarketing';

--add translation for key No items selected
exec insertLanguageInterface 'en', 'No items selected', 'No items selected', 'geomarketing';
exec insertLanguageInterface 'ru', 'No items selected', 'Ничего не выбрано', 'geomarketing';
exec insertLanguageInterface 'uk', 'No items selected', 'Нічого не вибрано', 'geomarketing';

--add translation for key No pins within selected polygons
exec insertLanguageInterface 'en', 'No pins within selected polygons', 'No pins in selected polygons', 'geomarketing';
exec insertLanguageInterface 'ru', 'No pins within selected polygons', 'Нет пинов внутри выбраных полигонов', 'geomarketing';
exec insertLanguageInterface 'uk', 'No pins within selected polygons', 'Немає пінів у вибрпних полігонах', 'geomarketing';

--add translation for key No responsible
exec insertLanguageInterface 'en', 'No responsible', 'No responsible', 'geomarketing';
exec insertLanguageInterface 'ru', 'No responsible', 'Нет ответственного', 'geomarketing';
exec insertLanguageInterface 'uk', 'No responsible', 'Відсутній відповідальний', 'geomarketing';

--add translation for key No results
exec insertLanguageInterface 'en', 'No results', 'No results', 'geomarketing';
exec insertLanguageInterface 'ru', 'No results', 'Нет результатов', 'geomarketing';
exec insertLanguageInterface 'uk', 'No results', 'Немає результатів', 'geomarketing';

--add translation for key No sales
exec insertLanguageInterface 'en', 'No sales', 'No sales', 'geomarketing';
exec insertLanguageInterface 'ru', 'No sales', 'Нет продаж', 'geomarketing';
exec insertLanguageInterface 'uk', 'No sales', 'Немає продажів', 'geomarketing';

--add translation for key No turnover
exec insertLanguageInterface 'en', 'No turnover', 'No turnover', 'geomarketing';
exec insertLanguageInterface 'ru', 'No turnover', 'Товарооборот отсутствует', 'geomarketing';
exec insertLanguageInterface 'uk', 'No turnover', 'Немає товарообігу', 'geomarketing';

--add translation for key No turnover per year
exec insertLanguageInterface 'en', 'No turnover per year', 'No turnover per [[year]] year', 'geomarketing';
exec insertLanguageInterface 'ru', 'No turnover per year', 'Нет товарооборота за [[year]] год', 'geomarketing';
exec insertLanguageInterface 'uk', 'No turnover per year', 'Нема товарообігу за [[year]] рік', 'geomarketing';

--add translation for key no version
exec insertLanguageInterface 'en', 'no version', 'No version', 'geomarketing';
exec insertLanguageInterface 'ru', 'no version', 'Нет версии', 'geomarketing';
exec insertLanguageInterface 'uk', 'no version', 'Немає версії', 'geomarketing';

--add translation for key No visits
exec insertLanguageInterface 'en', 'No visits', 'No visits', 'geomarketing';
exec insertLanguageInterface 'ru', 'No visits', 'Нет визитов', 'geomarketing';
exec insertLanguageInterface 'uk', 'No visits', 'Немає візитів', 'geomarketing';

--add translation for key noBrands
exec insertLanguageInterface 'en', 'noBrands', 'No brands', 'geomarketing';
exec insertLanguageInterface 'ru', 'noBrands', 'Без продаж', 'geomarketing';
exec insertLanguageInterface 'uk', 'noBrands', 'Немає продажів брендів', 'geomarketing';

--add translation for key not approved
exec insertLanguageInterface 'en', 'not approved', 'Not approved', 'geomarketing';
exec insertLanguageInterface 'ru', 'not approved', 'Не утвержден', 'geomarketing';
exec insertLanguageInterface 'uk', 'not approved', 'Не затверджений', 'geomarketing';

--add translation for key Not enough points to create layer
exec insertLanguageInterface 'en', 'Not enough points to create layer', 'Not enough points to create layer. Edit layer params', 'geomarketing';
exec insertLanguageInterface 'ru', 'Not enough points to create layer', 'Недостаточно организаций/специалистов для создания слоя. Выберите другие параметры', 'geomarketing';
exec insertLanguageInterface 'uk', 'Not enough points to create layer', 'Недостатньо організацій/спеціалістів для створення шару. Оберіть інші параметри', 'geomarketing';

--add translation for key Not found
exec insertLanguageInterface 'en', 'Not found', 'Not found', 'geomarketing';
exec insertLanguageInterface 'ru', 'Not found', 'Не найдено', 'geomarketing';
exec insertLanguageInterface 'uk', 'Not found', 'Не знайдено', 'geomarketing';

--add translation for key Not working
exec insertLanguageInterface 'en', 'Not working', 'Not working', 'geomarketing';
exec insertLanguageInterface 'ru', 'Not working', 'Не работает', 'geomarketing';
exec insertLanguageInterface 'uk', 'Not working', 'Не працює', 'geomarketing';

--add translation for key Nothing selected
exec insertLanguageInterface 'en', 'Nothing selected', 'Nothing selected', 'geomarketing';
exec insertLanguageInterface 'ru', 'Nothing selected', 'Ничего не выбрано', 'geomarketing';
exec insertLanguageInterface 'uk', 'Nothing selected', 'Нічого не вибрано', 'geomarketing';

--add translation for key notVisited
exec insertLanguageInterface 'en', 'notVisited', 'No visits', 'geomarketing';
exec insertLanguageInterface 'ru', 'notVisited', 'Без визитов', 'geomarketing';
exec insertLanguageInterface 'uk', 'notVisited', 'Без візитів', 'geomarketing';

--add translation for key Open user details
exec insertLanguageInterface 'en', 'Open user details', 'Open user details', 'geomarketing';
exec insertLanguageInterface 'ru', 'Open user details', 'Информацию о пользователе', 'geomarketing';
exec insertLanguageInterface 'uk', 'Open user details', 'Інформація про користувача', 'geomarketing';

--add translation for key Out of the map
exec insertLanguageInterface 'en', 'Out of the map', 'Out of map', 'geomarketing';
exec insertLanguageInterface 'ru', 'Out of the map', 'Вне границ карты', 'geomarketing';
exec insertLanguageInterface 'uk', 'Out of the map', 'За межами мапи', 'geomarketing';

--add translation for key Outside of polygon
exec insertLanguageInterface 'en', 'Outside of polygon', 'Outside of polygon', 'geomarketing';
exec insertLanguageInterface 'ru', 'Outside of polygon', 'За границами полигона', 'geomarketing';
exec insertLanguageInterface 'uk', 'Outside of polygon', 'За межами полігону', 'geomarketing';

--add translation for key Paint
exec insertLanguageInterface 'en', 'Paint', 'Brush', 'geomarketing';
exec insertLanguageInterface 'ru', 'Paint', 'Рисовать', 'geomarketing';
exec insertLanguageInterface 'uk', 'Paint', 'Фарбувати', 'geomarketing';

--add translation for key Painting options
exec insertLanguageInterface 'en', 'Painting options', 'Brush options', 'geomarketing';
exec insertLanguageInterface 'ru', 'Painting options', 'Параметры рисования', 'geomarketing';
exec insertLanguageInterface 'uk', 'Painting options', 'Параметри фарбування', 'geomarketing';

--add translation for key pcs
exec insertLanguageInterface 'en', 'pcs', 'pcs', 'geomarketing';
exec insertLanguageInterface 'ru', 'pcs', 'уп', 'geomarketing';
exec insertLanguageInterface 'uk', 'pcs', 'уп', 'geomarketing';

--add translation for key Perimeter
exec insertLanguageInterface 'en', 'Perimeter', 'P', 'geomarketing';
exec insertLanguageInterface 'ru', 'Perimeter', 'П', 'geomarketing';
exec insertLanguageInterface 'uk', 'Perimeter', 'П', 'geomarketing';

--add translation for key Pharmacy networks
exec insertLanguageInterface 'en', 'Pharmacy networks', 'Pharmacy networks', 'geomarketing';
exec insertLanguageInterface 'ru', 'Pharmacy networks', 'Аптечные сети', 'geomarketing';
exec insertLanguageInterface 'uk', 'Pharmacy networks', 'Аптечні мережі', 'geomarketing';

--add translation for key Pins for users: [users] places in the same polygon
exec insertLanguageInterface 'en', 'Pins for users: [users] places in the same polygon', 'Pins for users: [users] places in the same polygon', 'geomarketing';
exec insertLanguageInterface 'ru', 'Pins for users: [users] places in the same polygon', 'Пины для пользователей: [users] размещены в одном полигоне', 'geomarketing';
exec insertLanguageInterface 'uk', 'Pins for users: [users] places in the same polygon', 'Піни для користувачів: [users] розміщені в одному полігоні', 'geomarketing';

--add translation for key Please select a map version
exec insertLanguageInterface 'en', 'Please select a map version', 'Select a map version', 'geomarketing';
exec insertLanguageInterface 'ru', 'Please select a map version', 'Укажите версию окраски', 'geomarketing';
exec insertLanguageInterface 'uk', 'Please select a map version', 'Вкажіть версію забарвлення', 'geomarketing';

--add translation for key Point division applied
exec insertLanguageInterface 'en', 'Point division applied', 'Point division applied', 'geomarketing';
exec insertLanguageInterface 'ru', 'Point division applied', 'Применено деление по точкам', 'geomarketing';
exec insertLanguageInterface 'uk', 'Point division applied', 'Застосовано розподіл по точках', 'geomarketing';

--add translation for key Pointer
exec insertLanguageInterface 'en', 'Pointer', 'Pointer', 'geomarketing';
exec insertLanguageInterface 'ru', 'Pointer', 'Указатель', 'geomarketing';
exec insertLanguageInterface 'uk', 'Pointer', 'Покажчик', 'geomarketing';

--add translation for key Points in polygon
exec insertLanguageInterface 'en', 'Points in polygon', 'Points in polygon', 'geomarketing';
exec insertLanguageInterface 'ru', 'Points in polygon', 'К-во точек в полигоне', 'geomarketing';
exec insertLanguageInterface 'uk', 'Points in polygon', 'К-сть точок в полiгонi', 'geomarketing';

--add translation for key Polygon
exec insertLanguageInterface 'en', 'Polygon', 'Polygon', 'geomarketing';
exec insertLanguageInterface 'ru', 'Polygon', 'Полигон', 'geomarketing';
exec insertLanguageInterface 'uk', 'Polygon', 'Полігон', 'geomarketing';

--add translation for key Polygon Name
exec insertLanguageInterface 'en', 'Polygon Name', 'Polygon Name', 'geomarketing';
exec insertLanguageInterface 'ru', 'Polygon Name', 'Название полигона', 'geomarketing';
exec insertLanguageInterface 'uk', 'Polygon Name', 'Назва полігону', 'geomarketing';

--add translation for key Polygon name was successfully saved
exec insertLanguageInterface 'en', 'Polygon name was successfully saved', 'Polygon name was saved', 'geomarketing';
exec insertLanguageInterface 'ru', 'Polygon name was successfully saved', 'Название полигона сохранено', 'geomarketing';
exec insertLanguageInterface 'uk', 'Polygon name was successfully saved', 'Назва полігона збережена', 'geomarketing';

--add translation for key polygons
exec insertLanguageInterface 'en', 'polygons', 'Polygons', 'geomarketing';
exec insertLanguageInterface 'ru', 'polygons', 'Полигоны', 'geomarketing';
exec insertLanguageInterface 'uk', 'polygons', 'Полігони', 'geomarketing';

--add translation for key polygons2
exec insertLanguageInterface 'en', 'polygons2', 'polygons', 'geomarketing';
exec insertLanguageInterface 'ru', 'polygons2', 'полигоны', 'geomarketing';
exec insertLanguageInterface 'uk', 'polygons2', 'полігони', 'geomarketing';

--add translation for key Population
exec insertLanguageInterface 'en', 'Population', 'Population', 'geomarketing';
exec insertLanguageInterface 'ru', 'Population', 'Населения', 'geomarketing';
exec insertLanguageInterface 'uk', 'Population', 'Населення', 'geomarketing';

--add translation for key Potential
exec insertLanguageInterface 'en', 'Potential', 'Preparation', 'geomarketing';
exec insertLanguageInterface 'ru', 'Potential', 'Потенциал', 'geomarketing';
exec insertLanguageInterface 'uk', 'Potential', 'Потенціал', 'geomarketing';

--add translation for key Preparation
exec insertLanguageInterface 'en', 'Preparation', 'Preparation', 'geomarketing';
exec insertLanguageInterface 'ru', 'Preparation', 'Препарати', 'geomarketing';
exec insertLanguageInterface 'uk', 'Preparation', 'Препарати', 'geomarketing';

--add translation for key Preparation brand
exec insertLanguageInterface 'en', 'Preparation brand', 'Preparation brand', 'geomarketing';
exec insertLanguageInterface 'ru', 'Preparation brand', 'Бренд препарата', 'geomarketing';
exec insertLanguageInterface 'uk', 'Preparation brand', 'Бренд препарату', 'geomarketing';

--add translation for key Press enter to save
exec insertLanguageInterface 'en', 'Press enter to save', 'Press enter to save', 'geomarketing';
exec insertLanguageInterface 'ru', 'Press enter to save', 'Нажмите Enter чтобы сохранить', 'geomarketing';
exec insertLanguageInterface 'uk', 'Press enter to save', 'Натисніть Enter щоб зберегти', 'geomarketing';

--add translation for key Promotions
exec insertLanguageInterface 'en', 'Promotions', 'Promotions', 'geomarketing';
exec insertLanguageInterface 'ru', 'Promotions', 'Промоции', 'geomarketing';
exec insertLanguageInterface 'uk', 'Promotions', 'Промоції', 'geomarketing';

--add translation for key Quarter
exec insertLanguageInterface 'en', 'Quarter', 'Quarter', 'geomarketing';
exec insertLanguageInterface 'ru', 'Quarter', 'Квартал', 'geomarketing';
exec insertLanguageInterface 'uk', 'Quarter', 'Квартал', 'geomarketing';

--add translation for key questions.confirmErase
exec insertLanguageInterface 'en', 'questions.confirmErase', 'Exactly erase all painted areas?', 'geomarketing';
exec insertLanguageInterface 'ru', 'questions.confirmErase', 'Точно стереть все окрашенные территории?', 'geomarketing';
exec insertLanguageInterface 'uk', 'questions.confirmErase', 'Точно стерти всі зафарбовані території?', 'geomarketing';

--add translation for key questions.copy_paint_from_current_layer
exec insertLanguageInterface 'en', 'questions.copy_paint_from_current_layer', 'Copy color from current version?', 'geomarketing';
exec insertLanguageInterface 'ru', 'questions.copy_paint_from_current_layer', 'Скопировать окраску из текущей версии?', 'geomarketing';
exec insertLanguageInterface 'uk', 'questions.copy_paint_from_current_layer', 'Скопіювати забарвлення з поточної версії?', 'geomarketing';

--add translation for key questions.deletePolygon
exec insertLanguageInterface 'en', 'questions.deletePolygon', 'Delete polygon?', 'geomarketing';
exec insertLanguageInterface 'ru', 'questions.deletePolygon', 'Удалить полигон?', 'geomarketing';
exec insertLanguageInterface 'uk', 'questions.deletePolygon', 'Видалити полігон?', 'geomarketing';

--add translation for key questions.saveEdits
exec insertLanguageInterface 'en', 'questions.saveEdits', 'You changed the landfill. <br> Take into account the old changes in the list of institutions?', 'geomarketing';
exec insertLanguageInterface 'ru', 'questions.saveEdits', 'Вы изменили полигон.<br>Учитывать старые изменения в списке учреждений?', 'geomarketing';
exec insertLanguageInterface 'uk', 'questions.saveEdits', 'Ви змінили полігон. <br> Враховувати старі зміни в списку організацій?', 'geomarketing';

--add translation for key questions.undo
exec insertLanguageInterface 'en', 'questions.undo', 'Cancel changes?', 'geomarketing';
exec insertLanguageInterface 'ru', 'questions.undo', 'Отменить изменения?', 'geomarketing';
exec insertLanguageInterface 'uk', 'questions.undo', 'Відмінити зміни?', 'geomarketing';

--add translation for key Redo
exec insertLanguageInterface 'en', 'Redo', 'Redo', 'geomarketing';
exec insertLanguageInterface 'ru', 'Redo', 'Redo', 'geomarketing';
exec insertLanguageInterface 'uk', 'Redo', 'Redo', 'geomarketing';

--add translation for key Redone
exec insertLanguageInterface 'en', 'Redone', 'Redone', 'geomarketing';
exec insertLanguageInterface 'ru', 'Redone', 'Применен', 'geomarketing';
exec insertLanguageInterface 'uk', 'Redone', 'Застосовано', 'geomarketing';

--add translation for key Refresh
exec insertLanguageInterface 'en', 'Refresh', 'Refresh', 'geomarketing';
exec insertLanguageInterface 'ru', 'Refresh', 'Обновить', 'geomarketing';
exec insertLanguageInterface 'uk', 'Refresh', 'Оновити', 'geomarketing';

--add translation for key Region
exec insertLanguageInterface 'en', 'Region', 'Region', 'geomarketing';
exec insertLanguageInterface 'ru', 'Region', 'Область', 'geomarketing';
exec insertLanguageInterface 'uk', 'Region', 'Область', 'geomarketing';

--add translation for key regions
exec insertLanguageInterface 'en', 'regions', 'Regions', 'geomarketing';
exec insertLanguageInterface 'ru', 'regions', 'Области', 'geomarketing';
exec insertLanguageInterface 'uk', 'regions', 'Області', 'geomarketing';

--add translation for key Regions filled
exec insertLanguageInterface 'en', 'Regions filled', 'Regions filled', 'geomarketing';
exec insertLanguageInterface 'ru', 'Regions filled', 'Областей наполнено', 'geomarketing';
exec insertLanguageInterface 'uk', 'Regions filled', 'Областей наповнено', 'geomarketing';

--add translation for key Reject
exec insertLanguageInterface 'en', 'Reject', 'Reject', 'geomarketing';
exec insertLanguageInterface 'ru', 'Reject', 'Отклонить', 'geomarketing';
exec insertLanguageInterface 'uk', 'Reject', 'Відхилити', 'geomarketing';

--add translation for key Reset
exec insertLanguageInterface 'en', 'Reset', 'Reset', 'geomarketing';
exec insertLanguageInterface 'ru', 'Reset', 'Сбросить', 'geomarketing';
exec insertLanguageInterface 'uk', 'Reset', 'Скинути', 'geomarketing';

--add translation for key Reset changes
exec insertLanguageInterface 'en', 'Reset changes', 'Reset changes', 'geomarketing';
exec insertLanguageInterface 'ru', 'Reset changes', 'Сбросить изменения', 'geomarketing';
exec insertLanguageInterface 'uk', 'Reset changes', 'Скинути зміни', 'geomarketing';

--add translation for key Responsible
exec insertLanguageInterface 'en', 'Responsible', 'Responsible', 'geomarketing';
exec insertLanguageInterface 'ru', 'Responsible', 'Ответственный', 'geomarketing';
exec insertLanguageInterface 'uk', 'Responsible', 'Відповідальний ', 'geomarketing';

--add translation for key Responsible unassigned
exec insertLanguageInterface 'en', 'Responsible unassigned', 'Responsible unassigned', 'geomarketing';
exec insertLanguageInterface 'ru', 'Responsible unassigned', 'Ответственный не назначен', 'geomarketing';
exec insertLanguageInterface 'uk', 'Responsible unassigned', 'Відповідального не призначено', 'geomarketing';

--add translation for key Responsible was successfully saved
exec insertLanguageInterface 'en', 'Responsible was successfully saved', 'Responsible was successfully saved', 'geomarketing';
exec insertLanguageInterface 'ru', 'Responsible was successfully saved', 'Ответственный был успешно сохранен', 'geomarketing';
exec insertLanguageInterface 'uk', 'Responsible was successfully saved', 'Відповідальний був успішно збережений', 'geomarketing';

--add translation for key Ruler
exec insertLanguageInterface 'en', 'Ruler', 'Ruler', 'geomarketing';
exec insertLanguageInterface 'ru', 'Ruler', 'Линейка', 'geomarketing';
exec insertLanguageInterface 'uk', 'Ruler', 'Лінійка', 'geomarketing';

--add translation for key Sales
exec insertLanguageInterface 'en', 'Sales', 'SALE IN', 'geomarketing';
exec insertLanguageInterface 'ru', 'Sales', 'Продажи', 'geomarketing';
exec insertLanguageInterface 'uk', 'Sales', 'Продажі', 'geomarketing';

--add translation for key Sales per year
exec insertLanguageInterface 'en', 'Sales per year', 'Sales per year', 'geomarketing';
exec insertLanguageInterface 'ru', 'Sales per year', 'Товарооборот за год', 'geomarketing';
exec insertLanguageInterface 'uk', 'Sales per year', 'Товарообіг за рік', 'geomarketing';

--add translation for key Sales period
exec insertLanguageInterface 'en', 'Sales period', 'Sales period', 'geomarketing';
exec insertLanguageInterface 'ru', 'Sales period', 'Период продаж', 'geomarketing';
exec insertLanguageInterface 'uk', 'Sales period', 'Період продажів', 'geomarketing';

--add translation for key Sales plan
exec insertLanguageInterface 'en', 'Sales plan', 'Sales plan', 'geomarketing';
exec insertLanguageInterface 'ru', 'Sales plan', 'План продаж', 'geomarketing';
exec insertLanguageInterface 'uk', 'Sales plan', 'План продажів', 'geomarketing';

--add translation for key Save
exec insertLanguageInterface 'en', 'Save', 'Saved', 'geomarketing';
exec insertLanguageInterface 'ru', 'Save', 'Сохранить', 'geomarketing';
exec insertLanguageInterface 'uk', 'Save', 'Зберегти', 'geomarketing';

--add translation for key Saved
exec insertLanguageInterface 'en', 'Saved', 'Saved', 'geomarketing';
exec insertLanguageInterface 'ru', 'Saved', 'Сохранено!', 'geomarketing';
exec insertLanguageInterface 'uk', 'Saved', 'Збереженно!', 'geomarketing';

--add translation for key Saving
exec insertLanguageInterface 'en', 'Saving', 'Saving', 'geomarketing';
exec insertLanguageInterface 'ru', 'Saving', 'Сохранение...', 'geomarketing';
exec insertLanguageInterface 'uk', 'Saving', 'Збереження...', 'geomarketing';

--add translation for key Search
exec insertLanguageInterface 'en', 'Search', 'Search', 'geomarketing';
exec insertLanguageInterface 'ru', 'Search', 'Пошук', 'geomarketing';
exec insertLanguageInterface 'uk', 'Search', 'Пошук', 'geomarketing';

--add translation for key Select all
exec insertLanguageInterface 'en', 'Select all', 'Select all', 'geomarketing';
exec insertLanguageInterface 'ru', 'Select all', 'Выбрать все', 'geomarketing';
exec insertLanguageInterface 'uk', 'Select all', 'Обрати все', 'geomarketing';

--add translation for key Selected
exec insertLanguageInterface 'en', 'Selected', 'Selected', 'geomarketing';
exec insertLanguageInterface 'ru', 'Selected', 'Выбрано', 'geomarketing';
exec insertLanguageInterface 'uk', 'Selected', 'Вибрано', 'geomarketing';

--add translation for key Send
exec insertLanguageInterface 'en', 'Send', 'Send', 'geomarketing';
exec insertLanguageInterface 'ru', 'Send', 'Отправить', 'geomarketing';
exec insertLanguageInterface 'uk', 'Send', 'Надіслати', 'geomarketing';

--add translation for key Sheet
exec insertLanguageInterface 'en', 'Sheet', 'Sheet', 'geomarketing';
exec insertLanguageInterface 'ru', 'Sheet', 'Слой', 'geomarketing';
exec insertLanguageInterface 'uk', 'Sheet', 'Шар', 'geomarketing';

--add translation for key Sheet approved by user
exec insertLanguageInterface 'en', 'Sheet approved by user', 'Sheet approved by user', 'geomarketing';
exec insertLanguageInterface 'ru', 'Sheet approved by user', 'Слой утвержден пользователем', 'geomarketing';
exec insertLanguageInterface 'uk', 'Sheet approved by user', 'Шар затверджений користувачем', 'geomarketing';

--add translation for key Sheet will be approved for all subordinates
exec insertLanguageInterface 'en', 'Sheet will be approved for all subordinates', 'Approve for all subordinates', 'geomarketing';
exec insertLanguageInterface 'ru', 'Sheet will be approved for all subordinates', 'Утвердить для всех подчиненных', 'geomarketing';
exec insertLanguageInterface 'uk', 'Sheet will be approved for all subordinates', 'Затвердити для усіх підлеглих', 'geomarketing';

--add translation for key Show
exec insertLanguageInterface 'en', 'Show', 'Show all polygons', 'geomarketing';
exec insertLanguageInterface 'ru', 'Show', 'Показать', 'geomarketing';
exec insertLanguageInterface 'uk', 'Show', 'Показати', 'geomarketing';

--add translation for key Show all polygons
exec insertLanguageInterface 'en', 'Show all polygons', 'Show all polygons', 'geomarketing';
exec insertLanguageInterface 'ru', 'Show all polygons', 'Показать все полигоны', 'geomarketing';
exec insertLanguageInterface 'uk', 'Show all polygons', 'Показати усі полігони', 'geomarketing';

--add translation for key Specialization
exec insertLanguageInterface 'en', 'Specialization', 'Specialization', 'geomarketing';
exec insertLanguageInterface 'ru', 'Specialization', 'Специализация', 'geomarketing';
exec insertLanguageInterface 'uk', 'Specialization', 'Спеціалізація', 'geomarketing';

--add translation for key Specializations
exec insertLanguageInterface 'en', 'Specializations', 'Specializations', 'geomarketing';
exec insertLanguageInterface 'ru', 'Specializations', 'Специализации', 'geomarketing';
exec insertLanguageInterface 'uk', 'Specializations', 'Спеціалізації', 'geomarketing';

--add translation for key Split by points
exec insertLanguageInterface 'en', 'Split by points', 'Split by points', 'geomarketing';
exec insertLanguageInterface 'ru', 'Split by points', 'Делить по точкам', 'geomarketing';
exec insertLanguageInterface 'uk', 'Split by points', 'Ділити по точкам', 'geomarketing';

--add translation for key Status
exec insertLanguageInterface 'en', 'Status', 'Status', 'geomarketing';
exec insertLanguageInterface 'ru', 'Status', 'Статус', 'geomarketing';
exec insertLanguageInterface 'uk', 'Status', 'Статус', 'geomarketing';

--add translation for key Stop balancing
exec insertLanguageInterface 'en', 'Stop balancing', 'Stop balancing', 'geomarketing';
exec insertLanguageInterface 'ru', 'Stop balancing', 'Остановить балансировку', 'geomarketing';
exec insertLanguageInterface 'uk', 'Stop balancing', 'Зупинити балансування', 'geomarketing';

--add translation for key Subordinates
exec insertLanguageInterface 'en', 'Subordinates', 'Subordinates', 'geomarketing';
exec insertLanguageInterface 'ru', 'Subordinates', 'Подчинённые', 'geomarketing';
exec insertLanguageInterface 'uk', 'Subordinates', 'Підлеглі', 'geomarketing';

--add translation for key Sum
exec insertLanguageInterface 'en', 'Sum', 'Sum', 'geomarketing';
exec insertLanguageInterface 'ru', 'Sum', 'Сумма', 'geomarketing';
exec insertLanguageInterface 'uk', 'Sum', 'Сума', 'geomarketing';

--add translation for key Summary table
exec insertLanguageInterface 'en', 'Summary table', 'Summary table', 'geomarketing';
exec insertLanguageInterface 'ru', 'Summary table', 'Сводная таблица', 'geomarketing';
exec insertLanguageInterface 'uk', 'Summary table', 'Зведена таблиця', 'geomarketing';

--add translation for key Task state
exec insertLanguageInterface 'en', 'Task state', 'Task state', 'geomarketing';
exec insertLanguageInterface 'ru', 'Task state', 'Состояние визита', 'geomarketing';
exec insertLanguageInterface 'uk', 'Task state', 'Стан візиту', 'geomarketing';

--add translation for key Task type
exec insertLanguageInterface 'en', 'Task type', 'Task type', 'geomarketing';
exec insertLanguageInterface 'ru', 'Task type', 'Тип визита', 'geomarketing';
exec insertLanguageInterface 'uk', 'Task type', 'Тип візиту', 'geomarketing';

--add translation for key tendersku
exec insertLanguageInterface 'en', 'tendersku', 'Tender purchases', 'geomarketing';
exec insertLanguageInterface 'ru', 'tendersku', 'Тендерные закупки', 'geomarketing';
exec insertLanguageInterface 'uk', 'tendersku', 'Тендерні закупівлі', 'geomarketing';

--add translation for key Territories will be reset. Continue?
exec insertLanguageInterface 'en', 'Territories will be reset. Continue?', 'Territories will be reset. Continue?', 'geomarketing';
exec insertLanguageInterface 'ru', 'Territories will be reset. Continue?', 'Территории сбросятся. Продолжить?', 'geomarketing';
exec insertLanguageInterface 'uk', 'Territories will be reset. Continue?', 'Території скинуться. Продовжити?', 'geomarketing';

--add translation for key The subordination of the pharmacy network
exec insertLanguageInterface 'en', 'The subordination of the pharmacy network', 'Headquarter', 'geomarketing';
exec insertLanguageInterface 'ru', 'The subordination of the pharmacy network', 'Главная организация', 'geomarketing';
exec insertLanguageInterface 'uk', 'The subordination of the pharmacy network', 'Головна організація', 'geomarketing';

--add translation for key This is your active version. Are you sure you want to make it archive?
exec insertLanguageInterface 'en', 'This is your active version. Are you sure you want to make it archive?', 'This is your active version. Are you sure you want to make it archive?', 'geomarketing';
exec insertLanguageInterface 'ru', 'This is your active version. Are you sure you want to make it archive?', 'Это ваша текущая версия. Вы уверены: что хотите сделать её архивной?', 'geomarketing';
exec insertLanguageInterface 'uk', 'This is your active version. Are you sure you want to make it archive?', 'Це ваша поточна версія. Ви впевнені: що хочете зробити її архівної?', 'geomarketing';

--add translation for key to
exec insertLanguageInterface 'en', 'to', 'to', 'geomarketing';
exec insertLanguageInterface 'ru', 'to', 'по', 'geomarketing';
exec insertLanguageInterface 'uk', 'to', 'по', 'geomarketing';

--add translation for key Toggle user polygons
exec insertLanguageInterface 'en', 'Toggle user polygons', 'Show user polygons', 'geomarketing';
exec insertLanguageInterface 'ru', 'Toggle user polygons', 'Отобразить/скрыть полигоны пользователя', 'geomarketing';
exec insertLanguageInterface 'uk', 'Toggle user polygons', 'Показати / приховати полігони користувача', 'geomarketing';

--add translation for key Toggle polygons
exec insertLanguageInterface 'en', 'Toggle polygons', 'Show polygons', 'geomarketing';
exec insertLanguageInterface 'ru', 'Toggle polygons', 'Отобразить/скрыть полигоны', 'geomarketing';
exec insertLanguageInterface 'uk', 'Toggle polygons', 'Показати / приховати полігони', 'geomarketing';

--add translation for key tomln
exec insertLanguageInterface 'en', 'tomln', 'TO, mln', 'geomarketing';
exec insertLanguageInterface 'ru', 'tomln', 'ТО, млн', 'geomarketing';
exec insertLanguageInterface 'uk', 'tomln', 'ТО, млн', 'geomarketing';

--add translation for key Tool panel
exec insertLanguageInterface 'en', 'Tool panel', 'Tool panel', 'geomarketing';
exec insertLanguageInterface 'ru', 'Tool panel', 'Настройки колонок', 'geomarketing';
exec insertLanguageInterface 'uk', 'Tool panel', 'Налаштування колонок', 'geomarketing';

--add translation for key tooltip.geomarketingCompanyInfoTurnoverByMonthDisabled
exec insertLanguageInterface 'en', 'tooltip.geomarketingCompanyInfoTurnoverByMonthDisabled', 'Turnover per quarter', 'geomarketing';
exec insertLanguageInterface 'ru', 'tooltip.geomarketingCompanyInfoTurnoverByMonthDisabled', 'Поквартальный товарооборот', 'geomarketing';
exec insertLanguageInterface 'uk', 'tooltip.geomarketingCompanyInfoTurnoverByMonthDisabled', 'Поквартальний товарообіг', 'geomarketing';

--add translation for key tooltip.geomarketingCompanyInfoTurnoverByMonthEnabled
exec insertLanguageInterface 'en', 'tooltip.geomarketingCompanyInfoTurnoverByMonthEnabled', 'Turnover per month', 'geomarketing';
exec insertLanguageInterface 'ru', 'tooltip.geomarketingCompanyInfoTurnoverByMonthEnabled', 'Товарооборот за месяц', 'geomarketing';
exec insertLanguageInterface 'uk', 'tooltip.geomarketingCompanyInfoTurnoverByMonthEnabled', 'Товарообіг за місяць', 'geomarketing';

--add translation for key Top bricks
exec insertLanguageInterface 'en', 'Top bricks', 'Top bricks', 'geomarketing';
exec insertLanguageInterface 'ru', 'Top bricks', 'Топ брик', 'geomarketing';
exec insertLanguageInterface 'uk', 'Top bricks', 'Топ брик', 'geomarketing';

--add translation for key Top pharmacies
exec insertLanguageInterface 'en', 'Top pharmacies', 'Top pharmacies', 'geomarketing';
exec insertLanguageInterface 'ru', 'Top pharmacies', 'Потенциальные аптеки', 'geomarketing';
exec insertLanguageInterface 'uk', 'Top pharmacies', 'Потенційні аптеки', 'geomarketing';

--add translation for key Total
exec insertLanguageInterface 'en', 'Total', 'Total coefficient', 'geomarketing';
exec insertLanguageInterface 'ru', 'Total', 'Итог', 'geomarketing';
exec insertLanguageInterface 'uk', 'Total', 'Підсумок', 'geomarketing';

--add translation for key Total coefficient
exec insertLanguageInterface 'en', 'Total coefficient', 'Total coefficient', 'geomarketing';
exec insertLanguageInterface 'ru', 'Total coefficient', 'Суммарный коэффициент', 'geomarketing';
exec insertLanguageInterface 'uk', 'Total coefficient', 'Сумарний коефіцієнт', 'geomarketing';

--add translation for key Turnover
exec insertLanguageInterface 'en', 'Turnover', 'Turnover', 'geomarketing';
exec insertLanguageInterface 'ru', 'Turnover', 'Товарооборот', 'geomarketing';
exec insertLanguageInterface 'uk', 'Turnover', 'Товарообіг ', 'geomarketing';

--add translation for key turnoverRange
exec insertLanguageInterface 'en', 'turnoverRange', 'Turnover', 'geomarketing';
exec insertLanguageInterface 'ru', 'turnoverRange', 'Товарооборот', 'geomarketing';
exec insertLanguageInterface 'uk', 'turnoverRange', 'Товарообіг ', 'geomarketing';

--add translation for key Type
exec insertLanguageInterface 'en', 'Type', 'Type', 'geomarketing';
exec insertLanguageInterface 'ru', 'Type', 'Тип', 'geomarketing';
exec insertLanguageInterface 'uk', 'Type', 'Тип', 'geomarketing';

--add translation for key Types
exec insertLanguageInterface 'en', 'Types', 'Company types', 'geomarketing';
exec insertLanguageInterface 'ru', 'Types', 'Типы организаций', 'geomarketing';
exec insertLanguageInterface 'uk', 'Types', 'Типи організацій', 'geomarketing';

--add translation for key Unable to get status
exec insertLanguageInterface 'en', 'Unable to get status', 'Unable to get status', 'geomarketing';
exec insertLanguageInterface 'ru', 'Unable to get status', 'Невозможно получить статус балансировки', 'geomarketing';
exec insertLanguageInterface 'uk', 'Unable to get status', 'Неможливо отримати статус балансування', 'geomarketing';

--add translation for key Unable to start balancing
exec insertLanguageInterface 'en', 'Unable to start balancing', 'Unable to start balancing', 'geomarketing';
exec insertLanguageInterface 'ru', 'Unable to start balancing', 'Невозможно начать балансировку', 'geomarketing';
exec insertLanguageInterface 'uk', 'Unable to start balancing', 'Неможливо почати балансування', 'geomarketing';

--add translation for key uncheck all
exec insertLanguageInterface 'en', 'uncheck all', 'Uncheck all', 'geomarketing';
exec insertLanguageInterface 'ru', 'uncheck all', 'Снять все', 'geomarketing';
exec insertLanguageInterface 'uk', 'uncheck all', 'Зняти все ', 'geomarketing';

--add translation for key Undo
exec insertLanguageInterface 'en', 'Undo', 'Undo', 'geomarketing';
exec insertLanguageInterface 'ru', 'Undo', 'Undo', 'geomarketing';
exec insertLanguageInterface 'uk', 'Undo', 'Undo', 'geomarketing';

--add translation for key Undone
exec insertLanguageInterface 'en', 'Undone', 'Undone', 'geomarketing';
exec insertLanguageInterface 'ru', 'Undone', 'Отменено', 'geomarketing';
exec insertLanguageInterface 'uk', 'Undone', 'Скасовано', 'geomarketing';

--add translation for key unknown
exec insertLanguageInterface 'en', 'unknown', 'Unknown', 'geomarketing';
exec insertLanguageInterface 'ru', 'unknown', 'Неизвестно', 'geomarketing';
exec insertLanguageInterface 'uk', 'unknown', 'Невідомо', 'geomarketing';

--add translation for key Untitled
exec insertLanguageInterface 'en', 'Untitled', 'Untitled', 'geomarketing';
exec insertLanguageInterface 'ru', 'Untitled', 'Без названия', 'geomarketing';
exec insertLanguageInterface 'uk', 'Untitled', 'Без назви', 'geomarketing';

--add translation for key User
exec insertLanguageInterface 'en', 'User', 'User', 'geomarketing';
exec insertLanguageInterface 'ru', 'User', 'Пользователь', 'geomarketing';
exec insertLanguageInterface 'uk', 'User', 'Користувач', 'geomarketing';

--add translation for key user unassigned
exec insertLanguageInterface 'en', 'user unassigned', 'User unassigned', 'geomarketing';
exec insertLanguageInterface 'ru', 'user unassigned', 'Пользователь не установлен', 'geomarketing';
exec insertLanguageInterface 'uk', 'user unassigned', 'Користувач не встановлено', 'geomarketing';

--add translation for key users
exec insertLanguageInterface 'en', 'users', 'Users', 'geomarketing';
exec insertLanguageInterface 'ru', 'users', 'Пользователи', 'geomarketing';
exec insertLanguageInterface 'uk', 'users', 'Користувачи', 'geomarketing';

--add translation for key Users and polygons
exec insertLanguageInterface 'en', 'Users and polygons', 'Users and polygons', 'geomarketing';
exec insertLanguageInterface 'ru', 'Users and polygons', 'Пользователи и полигоны', 'geomarketing';
exec insertLanguageInterface 'uk', 'Users and polygons', 'Користувачи і полігони', 'geomarketing';

--add translation for key Value
exec insertLanguageInterface 'en', 'Value', 'Value', 'geomarketing';
exec insertLanguageInterface 'ru', 'Value', 'Объём', 'geomarketing';
exec insertLanguageInterface 'uk', 'Value', 'Об''єм', 'geomarketing';

--add translation for key Version
exec insertLanguageInterface 'en', 'Version', 'Version', 'geomarketing';
exec insertLanguageInterface 'ru', 'Version', 'Версия', 'geomarketing';
exec insertLanguageInterface 'uk', 'Version', 'Версія', 'geomarketing';

--add translation for key Version config
exec insertLanguageInterface 'en', 'Version config', 'Version config', 'geomarketing';
exec insertLanguageInterface 'ru', 'Version config', 'Управление версиями', 'geomarketing';
exec insertLanguageInterface 'uk', 'Version config', 'Управління версіями', 'geomarketing';

--add translation for key version.api
exec insertLanguageInterface 'en', 'version.api', 'API', 'geomarketing';
exec insertLanguageInterface 'ru', 'version.api', 'API', 'geomarketing';
exec insertLanguageInterface 'uk', 'version.api', 'API', 'geomarketing';

--add translation for key version.archive
exec insertLanguageInterface 'en', 'version.archive', 'Archive', 'geomarketing';
exec insertLanguageInterface 'ru', 'version.archive', 'Архив', 'geomarketing';
exec insertLanguageInterface 'uk', 'version.archive', 'Архів', 'geomarketing';

--add translation for key version.name
exec insertLanguageInterface 'en', 'version.name', 'Name', 'geomarketing';
exec insertLanguageInterface 'ru', 'version.name', 'Название', 'geomarketing';
exec insertLanguageInterface 'uk', 'version.name', 'Назва', 'geomarketing';

--add translation for key version.targeting
exec insertLanguageInterface 'en', 'version.targeting', 'Targeting', 'geomarketing';
exec insertLanguageInterface 'ru', 'version.targeting', 'Таргетирование', 'geomarketing';
exec insertLanguageInterface 'uk', 'version.targeting', 'Таргетування', 'geomarketing';

--add translation for key Visits
exec insertLanguageInterface 'en', 'Visits', 'Visits', 'geomarketing';
exec insertLanguageInterface 'ru', 'Visits', 'Визити', 'geomarketing';
exec insertLanguageInterface 'uk', 'Visits', 'Візити', 'geomarketing';

--add translation for key visitsRange
exec insertLanguageInterface 'en', 'visitsRange', 'Range', 'geomarketing';
exec insertLanguageInterface 'ru', 'visitsRange', 'Период визитов', 'geomarketing';
exec insertLanguageInterface 'uk', 'visitsRange', 'Період візитів', 'geomarketing';

--add translation for key Year
exec insertLanguageInterface 'en', 'Year', 'Year', 'geomarketing';
exec insertLanguageInterface 'ru', 'Year', 'Год', 'geomarketing';
exec insertLanguageInterface 'uk', 'Year', 'Рік', 'geomarketing';

--add translation for key Yes
exec insertLanguageInterface 'en', 'Yes', 'Yes', 'geomarketing';
exec insertLanguageInterface 'ru', 'Yes', 'Да', 'geomarketing';
exec insertLanguageInterface 'uk', 'Yes', 'Так', 'geomarketing';

--add translation for key Zoom to all companies
exec insertLanguageInterface 'en', 'Zoom to all companies', 'Zoom to all companies', 'geomarketing';
exec insertLanguageInterface 'ru', 'Zoom to all companies', 'Масштабировать по всем учреждениям', 'geomarketing';
exec insertLanguageInterface 'uk', 'Zoom to all companies', 'Маштабувати за всіма організаціями', 'geomarketing';

--add translation for key Zoom to user polygons
exec insertLanguageInterface 'en', 'Zoom to user polygons', 'Zoom to user polygons', 'geomarketing';
exec insertLanguageInterface 'ru', 'Zoom to user polygons', 'Масштабировать к полигонам', 'geomarketing';
exec insertLanguageInterface 'uk', 'Zoom to user polygons', 'Масштабувати до полігонів', 'geomarketing';

--add translation for key Click to start drawing line
exec insertLanguageInterface 'en', 'Click to start drawing line', 'Click to start drawing line', 'geomarketing';
exec insertLanguageInterface 'ru', 'Click to start drawing line', 'Нажмите на карту чтобы начать рисовать', 'geomarketing';
exec insertLanguageInterface 'uk', 'Click to start drawing line', 'Натисніть на мапу щоб почати малювати', 'geomarketing';

--add translation for key Company
exec insertLanguageInterface 'en', 'Company', 'Company', 'geomarketing';
exec insertLanguageInterface 'ru', 'Company', 'Организация', 'geomarketing';
exec insertLanguageInterface 'uk', 'Company', 'Організація', 'geomarketing';

--add translation for key Set user pin
exec insertLanguageInterface 'en', 'Set user pin', 'Set user pin', 'geomarketing';
exec insertLanguageInterface 'ru', 'Set user pin', 'Указать точку старта', 'geomarketing';
exec insertLanguageInterface 'uk', 'Set user pin', 'Встановити точку старту', 'geomarketing';

--add translation for key Error loading
exec insertLanguageInterface 'en', 'Error loading', 'Error loading', 'geomarketing';
exec insertLanguageInterface 'ru', 'Error loading', 'Ошибка при загрузке', 'geomarketing';
exec insertLanguageInterface 'uk', 'Error loading', 'Помилка при завантаженні', 'geomarketing';

--add translation for key Loading more
exec insertLanguageInterface 'en', 'Loading more', 'Loading more', 'geomarketing';
exec insertLanguageInterface 'ru', 'Loading more', 'Загрузка', 'geomarketing';
exec insertLanguageInterface 'uk', 'Loading more', 'Завантаження', 'geomarketing';

--add translation for key No results
exec insertLanguageInterface 'en', 'No results', 'No results', 'geomarketing';
exec insertLanguageInterface 'ru', 'No results', 'Нет результатов', 'geomarketing';
exec insertLanguageInterface 'uk', 'No results', 'Нема результатів', 'geomarketing';

--add translation for key Searching
exec insertLanguageInterface 'en', 'Searching', 'Searching', 'geomarketing';
exec insertLanguageInterface 'ru', 'Searching', 'Поиск', 'geomarketing';
exec insertLanguageInterface 'uk', 'Searching', 'Пошук', 'geomarketing';

--add translation for key Blocked by [user]
exec insertLanguageInterface 'en', 'Blocked by [user]', 'Blocked by [user]', 'geomarketing';
exec insertLanguageInterface 'ru', 'Blocked by [user]', 'Заблокировано [user]', 'geomarketing';
exec insertLanguageInterface 'uk', 'Blocked by [user]', 'Заблоковано [user]', 'geomarketing';

--add translation for key Пользовательский
exec insertLanguageInterface 'en', 'Пользовательский', 'Custom layer', 'geomarketing';
exec insertLanguageInterface 'ru', 'Пользовательский', 'Пользовательский', 'geomarketing';
exec insertLanguageInterface 'uk', 'Пользовательский', 'Користувацький', 'geomarketing';

--add translation for key Область
exec insertLanguageInterface 'en', 'Область', 'Regions', 'geomarketing';
exec insertLanguageInterface 'ru', 'Область', 'Область', 'geomarketing';
exec insertLanguageInterface 'uk', 'Область', 'Область', 'geomarketing';

--add translation for key Районы
exec insertLanguageInterface 'en', 'Районы', 'Districts', 'geomarketing';
exec insertLanguageInterface 'ru', 'Районы', 'Районы', 'geomarketing';
exec insertLanguageInterface 'uk', 'Районы', 'Райони', 'geomarketing';

--add translation for key Город
exec insertLanguageInterface 'en', 'Город', 'City', 'geomarketing';
exec insertLanguageInterface 'ru', 'Город', 'Город', 'geomarketing';
exec insertLanguageInterface 'uk', 'Город', 'Місто', 'geomarketing';

--add translation for key Почтовые индексы
exec insertLanguageInterface 'en', 'Почтовые индексы', 'Zip layer', 'geomarketing';
exec insertLanguageInterface 'ru', 'Почтовые индексы', 'Почтовые индексы', 'geomarketing';
exec insertLanguageInterface 'uk', 'Почтовые индексы', 'Поштові індекси', 'geomarketing';

--add translation for key Кварталы
exec insertLanguageInterface 'en', 'Кварталы', 'Suburbs', 'geomarketing';
exec insertLanguageInterface 'ru', 'Кварталы', 'Кварталы', 'geomarketing';
exec insertLanguageInterface 'uk', 'Кварталы', 'Квартали', 'geomarketing';

--add translation for key Options
exec insertLanguageInterface 'en', 'Options', 'Options', 'geomarketing';
exec insertLanguageInterface 'ru', 'Options', 'Настройки', 'geomarketing';
exec insertLanguageInterface 'uk', 'Options', 'Налаштування', 'geomarketing';

--add translation for key Project to layer
exec insertLanguageInterface 'en', 'Project to layer', 'Project to layer', 'geomarketing';
exec insertLanguageInterface 'ru', 'Project to layer', 'Спроецировать на слой', 'geomarketing';
exec insertLanguageInterface 'uk', 'Project to layer', 'Спроеціювати на шар', 'geomarketing';

--add translation for key Cluster
exec insertLanguageInterface 'en', 'Cluster', 'Cluster', 'geomarketing';
exec insertLanguageInterface 'ru', 'Cluster', 'Кластеризировать', 'geomarketing';
exec insertLanguageInterface 'uk', 'Cluster', 'Кластеризувати', 'geomarketing';

--add translation for key Contact visits
exec insertLanguageInterface 'en', 'Contact visits', 'Contact visits', 'geomarketing';
exec insertLanguageInterface 'ru', 'Contact visits', 'Визиты к специалистам', 'geomarketing';
exec insertLanguageInterface 'uk', 'Contact visits', 'Візити до спеціалістів', 'geomarketing';

--add translation for key Max visits per day
exec insertLanguageInterface 'en', 'Max visits per day', 'Visits per day', 'geomarketing';
exec insertLanguageInterface 'ru', 'Max visits per day', 'Визитов в день', 'geomarketing';
exec insertLanguageInterface 'uk', 'Max visits per day', 'Візитів за день', 'geomarketing';

--add translation for key Work days
exec insertLanguageInterface 'en', 'Work days', 'Work days', 'geomarketing';
exec insertLanguageInterface 'ru', 'Work days', 'Рабочих дней', 'geomarketing';
exec insertLanguageInterface 'uk', 'Work days', 'Робочих днів', 'geomarketing';

--add translation for key Stop
exec insertLanguageInterface 'en', 'Stop', 'Stop', 'geomarketing';
exec insertLanguageInterface 'ru', 'Stop', 'Остановить', 'geomarketing';
exec insertLanguageInterface 'uk', 'Stop', 'Зупинити', 'geomarketing';

--add translation for key Loading companies & contacts
exec insertLanguageInterface 'en', 'Loading companies & contacts', 'Loading companies & contacts', 'geomarketing';
exec insertLanguageInterface 'ru', 'Loading companies & contacts', 'Загрузка организаций и специалистов', 'geomarketing';
exec insertLanguageInterface 'uk', 'Loading companies & contacts', 'Завантаження організацій та спеціалістів', 'geomarketing';

--add translation for key Total visits
exec insertLanguageInterface 'en', 'Total visits', 'Total visits', 'geomarketing';
exec insertLanguageInterface 'ru', 'Total visits', 'Всего визитов', 'geomarketing';
exec insertLanguageInterface 'uk', 'Total visits', 'Усього візитів', 'geomarketing';

--add translation for key Contact
exec insertLanguageInterface 'en', 'Contact', 'Contacts', 'geomarketing';
exec insertLanguageInterface 'ru', 'Contact', 'Специалисты', 'geomarketing';
exec insertLanguageInterface 'uk', 'Contact', 'Спеціалісти', 'geomarketing';

--add translation for key Rep cluster
exec insertLanguageInterface 'en', 'Rep cluster', 'Rep cluster', 'geomarketing';
exec insertLanguageInterface 'ru', 'Rep cluster', 'Рассчет РЕП', 'geomarketing';
exec insertLanguageInterface 'uk', 'Rep cluster', 'Розрахунок РЕП', 'geomarketing';

--add translation for key Top rep cluster count
exec insertLanguageInterface 'en', 'Top rep cluster count', 'Top reps', 'geomarketing';
exec insertLanguageInterface 'ru', 'Top rep cluster count', 'Топ РЕП', 'geomarketing';
exec insertLanguageInterface 'uk', 'Top rep cluster count', 'Топ РЕП', 'geomarketing';

--add translation for key Radius
exec insertLanguageInterface 'en', 'Radius', 'Radius', 'geomarketing';
exec insertLanguageInterface 'ru', 'Radius', 'Радиус', 'geomarketing';
exec insertLanguageInterface 'uk', 'Radius', 'Радіус', 'geomarketing';

--add translation for key Hold RMB and to draw a circle
exec insertLanguageInterface 'en', 'Hold RMB and to draw a circle', 'Hold RMB and to draw a circle', 'geomarketing';
exec insertLanguageInterface 'ru', 'Hold RMB and to draw a circle', 'Зажмите ПКМ и потяните чтобы нарисовать круг', 'geomarketing';
exec insertLanguageInterface 'uk', 'Hold RMB and to draw a circle', 'Утримуйте ПКМ і потягніть щоб намалювати коло', 'geomarketing';

--add translation for key Click on map to place a marker
exec insertLanguageInterface 'en', 'Click on map to place a marker', 'Click on map to place a marker', 'geomarketing';
exec insertLanguageInterface 'ru', 'Click on map to place a marker', 'Нажмите на карту чтобы установить маркер', 'geomarketing';
exec insertLanguageInterface 'uk', 'Click on map to place a marker', 'Натисніть на мапу щоб встановити маркер', 'geomarketing';

--add translation for key Click on map to place a marker
exec insertLanguageInterface 'en', 'Click on map to place a marker', 'Click on map to place a marker', 'geomarketing';
exec insertLanguageInterface 'ru', 'Click on map to place a marker', 'Нажмите на карту чтобы установить маркер', 'geomarketing';
exec insertLanguageInterface 'uk', 'Click on map to place a marker', 'Натисніть на мапу щоб встановити маркер', 'geomarketing';

--add translation for key Click to continue drawing polygon
exec insertLanguageInterface 'en', 'Click to continue drawing polygon', 'Click to continue drawing polygon', 'geomarketing';
exec insertLanguageInterface 'ru', 'Click to continue drawing polygon', 'Нажмите на карту чтобы продожить рисовать полигон', 'geomarketing';
exec insertLanguageInterface 'uk', 'Click to continue drawing polygon', 'Натисніть на мапу щоб продовжити малювати полігон', 'geomarketing';

--add translation for key Click first point to complete polygon
exec insertLanguageInterface 'en', 'Click first point to complete polygon', 'Click first point to complete polygon', 'geomarketing';
exec insertLanguageInterface 'ru', 'Click first point to complete polygon', 'Нажмите на начальную точку чтобы завершить полигон', 'geomarketing';
exec insertLanguageInterface 'uk', 'Click first point to complete polygon', 'Натисніть на точку старту щоб завершити полігон', 'geomarketing';

--add translation for key Click to start drawing polygon
exec insertLanguageInterface 'en', 'Click to start drawing polygon', 'Click to start drawing polygon', 'geomarketing';
exec insertLanguageInterface 'ru', 'Click to start drawing polygon', 'Нажмите на карту чтобы начать рисовать полигон', 'geomarketing';
exec insertLanguageInterface 'uk', 'Click to start drawing polygon', 'Натисніть на мапу щоб почати малювати полігон', 'geomarketing';

--add translation for key Self-intersection is not allowed
exec insertLanguageInterface 'en', 'Self-intersection is not allowed', 'Self-intersection is not allowed', 'geomarketing';
exec insertLanguageInterface 'ru', 'Self-intersection is not allowed', 'Грани фигуры не должны пересекаться', 'geomarketing';
exec insertLanguageInterface 'uk', 'Self-intersection is not allowed', 'Грані фігури не мають перетинатись', 'geomarketing';

--add translation for key Click to continue
exec insertLanguageInterface 'en', 'Click to continue', 'Click to continue', 'geomarketing';
exec insertLanguageInterface 'ru', 'Click to continue', 'Нажмите на карту чтобы продожить рисовать линию', 'geomarketing';
exec insertLanguageInterface 'uk', 'Click to continue', 'Натисніть на мапу щоб продовжити лінію', 'geomarketing';

--add translation for key Click last point to complete
exec insertLanguageInterface 'en', 'Click last point to complete', 'Click last point to complete', 'geomarketing';
exec insertLanguageInterface 'ru', 'Click last point to complete', 'Нажмите на последнюю точку чтобы завершить линию', 'geomarketing';
exec insertLanguageInterface 'uk', 'Click last point to complete', 'Натисніть на останню точку щоб завершити лінію', 'geomarketing';

--add translation for key Click to draw a line
exec insertLanguageInterface 'en', 'Click to draw a line', 'Click to draw a line', 'geomarketing';
exec insertLanguageInterface 'ru', 'Click to draw a line', 'Нажмите на карту чтобы начать рисовать линию', 'geomarketing';
exec insertLanguageInterface 'uk', 'Click to draw a line', 'Натисніть на мапу щоб почати малювати лінію', 'geomarketing';

--add translation for key Hold RMB to draw shape
exec insertLanguageInterface 'en', 'Hold RMB to draw shape', 'Hold RMB to draw shape', 'geomarketing';
exec insertLanguageInterface 'ru', 'Hold RMB to draw shape', 'Зажмите ПКМ и потяните чтобы начать рисовать', 'geomarketing';
exec insertLanguageInterface 'uk', 'Hold RMB to draw shape', 'Утримуйте ПКМ і потягніть щоб почати малювати', 'geomarketing';

--add translation for key Release RMB to stop
exec insertLanguageInterface 'en', 'Release RMB to stop', 'Release RMB to stop', 'geomarketing';
exec insertLanguageInterface 'ru', 'Release RMB to stop', 'Отпустите ПКМ чтобы завершить', 'geomarketing';
exec insertLanguageInterface 'uk', 'Release RMB to stop', 'Відпустіть ПКМ щоб завершити', 'geomarketing';

--add translation for key Cancel shape
exec insertLanguageInterface 'en', 'Cancel shape', 'Cancel shape', 'geomarketing';
exec insertLanguageInterface 'ru', 'Cancel shape', 'Отменить фигуру', 'geomarketing';
exec insertLanguageInterface 'uk', 'Cancel shape', 'Відмінити фігуру', 'geomarketing';

--add translation for key Draw circle
exec insertLanguageInterface 'en', 'Draw circle', 'Draw circle', 'geomarketing';
exec insertLanguageInterface 'ru', 'Draw circle', 'Нарисовать круг', 'geomarketing';
exec insertLanguageInterface 'uk', 'Draw circle', 'Намалювати коло', 'geomarketing';

--add translation for key Draw marker circle
exec insertLanguageInterface 'en', 'Draw marker circle', 'Draw marker circle', 'geomarketing';
exec insertLanguageInterface 'ru', 'Draw marker circle', 'Установить круг-маркер', 'geomarketing';
exec insertLanguageInterface 'uk', 'Draw marker circle', 'Встановити маркер-коло', 'geomarketing';

--add translation for key Draw maker
exec insertLanguageInterface 'en', 'Draw maker', 'Draw maker', 'geomarketing';
exec insertLanguageInterface 'ru', 'Draw maker', 'Установить маркер', 'geomarketing';
exec insertLanguageInterface 'uk', 'Draw maker', 'Встановити маркер', 'geomarketing';

--add translation for key Draw polygon
exec insertLanguageInterface 'en', 'Draw polygon', 'Draw polygon', 'geomarketing';
exec insertLanguageInterface 'ru', 'Draw polygon', 'Нарисовать полигон', 'geomarketing';
exec insertLanguageInterface 'uk', 'Draw polygon', 'Намалювати полігон', 'geomarketing';

--add translation for key Draw polyline
exec insertLanguageInterface 'en', 'Draw polyline', 'Draw polyline', 'geomarketing';
exec insertLanguageInterface 'ru', 'Draw polyline', 'Нарисовать линию', 'geomarketing';
exec insertLanguageInterface 'uk', 'Draw polyline', 'Намалювати лінію', 'geomarketing';

--add translation for key Draw rectangle
exec insertLanguageInterface 'en', 'Draw rectangle', 'Draw rectangle', 'geomarketing';
exec insertLanguageInterface 'ru', 'Draw rectangle', 'Нарисовать прямоугольник', 'geomarketing';
exec insertLanguageInterface 'uk', 'Draw rectangle', 'Намалювати прямокутник', 'geomarketing';

--add translation for key Complete
exec insertLanguageInterface 'en', 'Complete', 'Complete', 'geomarketing';
exec insertLanguageInterface 'ru', 'Complete', 'Завершить', 'geomarketing';
exec insertLanguageInterface 'uk', 'Complete', 'Завершити', 'geomarketing';

--add translation for key Complete shape
exec insertLanguageInterface 'en', 'Complete shape', 'Complete shape', 'geomarketing';
exec insertLanguageInterface 'ru', 'Complete shape', 'Завершить фигуру', 'geomarketing';
exec insertLanguageInterface 'uk', 'Complete shape', 'Завершити фігуру', 'geomarketing';

--add translation for key Remove last vertex
exec insertLanguageInterface 'en', 'Remove last vertex', 'Remove last vertex', 'geomarketing';
exec insertLanguageInterface 'ru', 'Remove last vertex', 'Удалить последнюю вершину', 'geomarketing';
exec insertLanguageInterface 'uk', 'Remove last vertex', 'Видалити останню вершину', 'geomarketing';

--add translation for key Remove last drawn vertex
exec insertLanguageInterface 'en', 'Remove last drawn vertex', 'Remove last drawn vertex', 'geomarketing';
exec insertLanguageInterface 'ru', 'Remove last drawn vertex', 'Удалить последнюю добавленную вершину', 'geomarketing';
exec insertLanguageInterface 'uk', 'Remove last drawn vertex', 'Видалити останню додану вершину', 'geomarketing';

--add translation for key Press 'Cancel' to discard changes
exec insertLanguageInterface 'en', 'Press ''Cancel'' to discard changes', 'Press ''Cancel'' to discard changes', 'geomarketing';
exec insertLanguageInterface 'ru', 'Press ''Cancel'' to discard changes', 'Нажмите ''Отменить'' чтобы отменить изменения', 'geomarketing';
exec insertLanguageInterface 'uk', 'Press ''Cancel'' to discard changes', 'Натисніть', 'geomarketing';

--add translation for key Drag vertexes and markers to edit shape
exec insertLanguageInterface 'en', 'Drag vertexes and markers to edit shape', 'Drag vertexes and markers to edit shape', 'geomarketing';
exec insertLanguageInterface 'ru', 'Drag vertexes and markers to edit shape', 'Перетаскивайте точки и маркера чтобы изменить фигуру', 'geomarketing';
exec insertLanguageInterface 'uk', 'Drag vertexes and markers to edit shape', 'Перетягуйте точки та маркери щоб змінити фігуру', 'geomarketing';

--add translation for key Click point to delete
exec insertLanguageInterface 'en', 'Click point to delete', 'Click point to delete', 'geomarketing';
exec insertLanguageInterface 'ru', 'Click point to delete', 'Нажмите на точку чтобы удалить', 'geomarketing';
exec insertLanguageInterface 'uk', 'Click point to delete', 'Натисніть на точку щоб видалити', 'geomarketing';

--add translation for key Cancel editing, discard all changes
exec insertLanguageInterface 'en', 'Cancel editing, discard all changes', 'Cancel editing, discard all changes', 'geomarketing';
exec insertLanguageInterface 'ru', 'Cancel editing, discard all changes', 'Отменить редактирование, сбросить все изменения', 'geomarketing';
exec insertLanguageInterface 'uk', 'Cancel editing, discard all changes', 'Відмінити редагування, скинути усі зміни', 'geomarketing';

--add translation for key Clear all
exec insertLanguageInterface 'en', 'Clear all', 'Clear all', 'geomarketing';
exec insertLanguageInterface 'ru', 'Clear all', 'Очистить всё', 'geomarketing';
exec insertLanguageInterface 'uk', 'Clear all', 'Очистити усе', 'geomarketing';

--add translation for key Clear all layers
exec insertLanguageInterface 'en', 'Clear all layers', 'Clear all layers', 'geomarketing';
exec insertLanguageInterface 'ru', 'Clear all layers', 'Очистить все слои', 'geomarketing';
exec insertLanguageInterface 'uk', 'Clear all layers', 'Очистити усі шари', 'geomarketing';

--add translation for key Save changes
exec insertLanguageInterface 'en', 'Save changes', 'Save changes', 'geomarketing';
exec insertLanguageInterface 'ru', 'Save changes', 'Сохранить изменения', 'geomarketing';
exec insertLanguageInterface 'uk', 'Save changes', 'Зберегти зміни', 'geomarketing';

--add translation for key Edit layers
exec insertLanguageInterface 'en', 'Edit layers', 'Edit layers', 'geomarketing';
exec insertLanguageInterface 'ru', 'Edit layers', 'Редактировать слои', 'geomarketing';
exec insertLanguageInterface 'uk', 'Edit layers', 'Редагувати шари', 'geomarketing';

--add translation for key No layers to edit
exec insertLanguageInterface 'en', 'No layers to edit', 'No layers to edit', 'geomarketing';
exec insertLanguageInterface 'ru', 'No layers to edit', 'Нет слоев для редактирования', 'geomarketing';
exec insertLanguageInterface 'uk', 'No layers to edit', 'Нема шарів для редагування', 'geomarketing';

--add translation for key Remove layers
exec insertLanguageInterface 'en', 'Remove layers', 'Remove layers', 'geomarketing';
exec insertLanguageInterface 'ru', 'Remove layers', 'Удалить слои', 'geomarketing';
exec insertLanguageInterface 'uk', 'Remove layers', 'Видалити шари', 'geomarketing';

--add translation for key No layers to remove
exec insertLanguageInterface 'en', 'No layers to remove', 'No layers to remove', 'geomarketing';
exec insertLanguageInterface 'ru', 'No layers to remove', 'Нет слоев для удаления', 'geomarketing';
exec insertLanguageInterface 'uk', 'No layers to remove', 'Нема шарів для видалення', 'geomarketing';

--add translation for key Target groups
exec insertLanguageInterface 'en', 'Target groups', 'Target groups', 'geomarketing';
exec insertLanguageInterface 'ru', 'Target groups', 'Таргет-группы', 'geomarketing';
exec insertLanguageInterface 'uk', 'Target groups', 'Таргет-групи', 'geomarketing';
GO

IF NOT EXISTS(select 1 from info_languageinterface where domain = 'geomarketing' and [key] like 'turnover' COLLATE Latin1_General_CS_AS) BEGIN
    INSERT INTO info_languageinterface (domain, language_id, [key], translation)
    SELECT 'geomarketing', id, 'turnover', 'Turnover' FROM info_language WHERE slug = 'en'
    UNION ALL
    SELECT 'geomarketing', id, 'turnover', 'Товарооборот' FROM info_language WHERE slug = 'ru'
    UNION ALL
    SELECT 'geomarketing', id, 'turnover', 'Товарообiг' FROM info_language WHERE slug = 'uk'
END
GO

DELETE FROM info_languageinterface WHERE [domain] = 'messages' AND [key] IN ('This user has assigned polygons. Reassign polygons to another user or clear them?', 'Reassign user polygons', 'Reassign polygons', 'User to assign polygons', 'Clear polygons', 'Cancel');

--add translation for key This user has assigned polygons. Reassign polygons to another user or clear them?
exec insertLanguageInterface 'en', 'This user has assigned polygons. Reassign polygons to another user or clear them?', 'This user has assigned polygons. Reassign polygons to another user or clear them?', 'messages';
exec insertLanguageInterface 'ru', 'This user has assigned polygons. Reassign polygons to another user or clear them?', 'У этого пользователя есть назначенные территории. Назначить их другому пользователю или очистить?', 'messages';
exec insertLanguageInterface 'uk', 'This user has assigned polygons. Reassign polygons to another user or clear them?', 'У цього користувача є призначені території. Призначити їх іншому користувачу або очистити?', 'messages';

--add translation for key Reassign user polygons
exec insertLanguageInterface 'en', 'Reassign user polygons', 'Reassign user polygons', 'messages';
exec insertLanguageInterface 'ru', 'Reassign user polygons', 'Переназначить територии пользователя', 'messages';
exec insertLanguageInterface 'uk', 'Reassign user polygons', 'Перепризначити території користувача', 'messages';

--add translation for key Reassign polygons
exec insertLanguageInterface 'en', 'Reassign polygons', 'Reassign polygons', 'messages';
exec insertLanguageInterface 'ru', 'Reassign polygons', 'Переназначить територии', 'messages';
exec insertLanguageInterface 'uk', 'Reassign polygons', 'Перепризначити території ', 'messages';

--add translation for key User to assign polygons
exec insertLanguageInterface 'en', 'User to assign polygons', 'User to assign polygons', 'messages';
exec insertLanguageInterface 'ru', 'User to assign polygons', 'Пользователь которому будут назначены територии', 'messages';
exec insertLanguageInterface 'uk', 'User to assign polygons', 'Користувач якому будуть призначені території', 'messages';

--add translation for key Clear polygons
exec insertLanguageInterface 'en', 'Clear polygons', 'Clear polygons', 'messages';
exec insertLanguageInterface 'ru', 'Clear polygons', 'Очистить територии', 'messages';
exec insertLanguageInterface 'uk', 'Clear polygons', 'Очистити території', 'messages';

--add translation for key Cancel
exec insertLanguageInterface 'en', 'Cancel', 'Cancel', 'messages';
exec insertLanguageInterface 'ru', 'Cancel', 'Отменить', 'messages';
exec insertLanguageInterface 'uk', 'Cancel', 'Скасувати', 'messages';

GO

-- https://teamsoft.atlassian.net/browse/MCM-333
---------------------------------------------------------------------------------------------------------
IF NOT EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'info_user' AND Column_Name = 'webinarlink')
BEGIN
ALTER TABLE info_user
    ADD webinarlink VARCHAR(255) NULL
END
GO

IF NOT EXISTS (SELECT 1 FROM info_mcmcontentvariable WHERE code = 'WEBINARLINK')
    BEGIN
        INSERT INTO info_mcmcontentvariable(name, code) VALUES('Ссылка на вебинар', 'WEBINARLINK')
    END
GO
---------------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------------

---------------------------------------------------------------------------------------------------------
-- https://teamsoft.atlassian.net/browse/PHARMAWEB-309
---------------------------------------------------------------------------------------------------------
IF NOT EXISTS (SELECT 1 FROM po_dictionary WHERE [name] = 'Setting visibility of dictionaries')
    BEGIN
        INSERT INTO po_dictionary ([name], tablename, group_id)
        VALUES ('Setting visibility of dictionaries', 'po_dictionarybyrole', (SELECT id FROM po_dictionarygroup WHERE [name] IN ('General', 'Общие')) )
        DECLARE @language_en as int
        DECLARE @language_ru as int
        DECLARE @language_uk as int

        SELECT @language_en = id FROM info_language WHERE slug = 'en'
        SELECT @language_ru = id FROM info_language WHERE slug = 'ru'
        SELECT @language_uk = id FROM info_language WHERE slug = 'uk'

        INSERT INTO info_dictionaryidentifier (tablename, columnname, [value], identifier, hidden, order_num, required, language_id) VALUES
        ('po_dictionarybyrole', 'role_id', 'Role', 0, 0, 0, 1, @language_en),
        ('po_dictionarybyrole', 'role_id', N'Роль', 0, 0, 0, 1, @language_ru),
        ('po_dictionarybyrole', 'role_id', N'Роль', 0, 0, 0, 1, @language_uk),
        ('po_dictionarybyrole', 'dictionary_id', 'Dictionary', 0, 0, 0, 1, @language_en),
        ('po_dictionarybyrole', 'dictionary_id', N'Справочник', 0, 0, 0, 1, @language_ru),
        ('po_dictionarybyrole', 'dictionary_id', N'Довідник', 0, 0, 0, 1, @language_uk)


	    INSERT INTO rep_constraint (foreign_table, foreign_field, references_table, references_field) VALUES
	    ('po_dictionarybyrole', 'role_id', 'info_role', 'id'),
	    ('po_dictionarybyrole', 'dictionary_id', 'po_dictionary', 'id')
    END
GO

IF OBJECT_ID(N'[po_dictionarybyrole]', 'U') IS NULL
    BEGIN
        CREATE TABLE [dbo].[po_dictionarybyrole]
        (
            [id]             [int] IDENTITY (1,1) NOT NULL,
            [dictionary_id]  int                  NOT NULL,
            [role_id]        int                  NOT NULL,
            [currenttime]    [datetime]           NULL,
            [time]           [timestamp]          NULL,
            [moduser]        [varchar](16)        NULL,
            [guid]           [uniqueidentifier]   NULL,
            CONSTRAINT [PK_po_dictionarybyrole] PRIMARY KEY CLUSTERED
                (
                 [id] ASC
                    ) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
        ) ON [PRIMARY]

        ALTER TABLE [dbo].[po_dictionarybyrole]
            WITH CHECK ADD CONSTRAINT [FK_po_dictionarybyrole_dictionary_id] FOREIGN KEY ([dictionary_id])
                REFERENCES [dbo].[po_dictionary] ([id])

        ALTER TABLE [dbo].[po_dictionarybyrole]
            WITH CHECK ADD CONSTRAINT [FK_po_dictionarybyrole_role_id] FOREIGN KEY ([role_id])
                REFERENCES [dbo].[info_role] ([id])

        CREATE UNIQUE INDEX u_po_dictionarybyrole ON po_dictionarybyrole (dictionary_id, role_id)
    END
GO

IF OBJECT_ID(N'[po_dictionarybyrole]', 'U') IS NOT NULL AND EXISTS (SELECT 1 FROM info_role WHERE code = 'ROLE_ADMIN') AND NOT EXISTS (SELECT 1 FROM po_dictionarybyrole)
    BEGIN
        DECLARE @role_admin_id as int
        SELECT @role_admin_id = id FROM info_role WHERE code = 'ROLE_ADMIN'

        INSERT INTO po_dictionarybyrole (dictionary_id, role_id)
        SELECT d.id AS dictionary_id, @role_admin_id AS role_id
        FROM po_dictionary AS d
                 LEFT JOIN po_dictionarybyrole AS r ON (r.dictionary_id = d.id AND r.role_id = @role_admin_id)
        WHERE r.id IS NULL
    END
GO

EXEC insertLanguageInterface 'uk', 'Setting visibility of dictionaries', N'Налаштування видимості довідників', 'messages'
GO
EXEC insertLanguageInterface 'ru', 'Setting visibility of dictionaries', N'Настройки видимости справочников', 'messages'
GO
EXEC insertLanguageInterface 'en', 'Setting visibility of dictionaries', N'Setting visibility of dictionaries', 'messages'
GO

EXEC insertLanguageInterface 'uk', 'The combination of these fields already exists', N'Комбінація таких полів вже існує', 'messages'
GO
EXEC insertLanguageInterface 'ru', 'The combination of these fields already exists', N'Комбинация таких полей уже существует', 'messages'
GO
EXEC insertLanguageInterface 'en', 'The combination of these fields already exists', N'The combination of these fields already exists', 'messages'
GO

exec insertLanguageInterface 'en', 'There are no available editing dictionaries', 'There are no available editing dictionaries'
exec insertLanguageInterface 'ru', 'There are no available editing dictionaries', N'Отсутствуют доступные справочники для редактирования'
exec insertLanguageInterface 'uk', 'There are no available editing dictionaries', N'Відсутні доступні довідники для редагування'
GO

----------------------------------------------------------------------------------------------------------------------------------------------------------------------
---translation key fix
----------------------------------------------------------------------------------------------------------------------------------------------------------------------
exec insertLanguageInterface 'uk', 'Companies type', N'Типи установ', 'messages'
exec insertLanguageInterface 'ru', 'Companies type', N'Типы учреждений', 'messages'
exec insertLanguageInterface 'en', 'Companies type', N'Company types', 'messages'

exec insertLanguageInterface 'uk', 'Contacts type', N'Типи клієнтів', 'messages'
exec insertLanguageInterface 'ru', 'Contacts type', N'Типы клиентов', 'messages'
exec insertLanguageInterface 'en', 'Contacts type', N'Contact types', 'messages'

exec insertLanguageInterface 'uk', 'Tasks type', N'Типи візитів', 'messages'
exec insertLanguageInterface 'ru', 'Tasks type', N'Типы визитов', 'messages'
exec insertLanguageInterface 'en', 'Tasks type', N'Task types', 'messages'

exec insertLanguageInterface 'uk', 'Actions type', N'Типи завдань', 'messages'
exec insertLanguageInterface 'ru', 'Actions type', N'Типы задач', 'messages'
exec insertLanguageInterface 'en', 'Actions type', N'Action types', 'messages'
--
-- https://teamsoft.atlassian.net/browse/VEKTORRU-97
--
DELETE info_languageinterface WHERE [key]='Plan ofice' AND domain='messages'
GO

exec insertLanguageInterface 'en', 'Plan office', N'Plan office', 'messages'
exec insertLanguageInterface 'ru', 'Plan office', N'План офиса', 'messages'
exec insertLanguageInterface 'uk', 'Plan office', N'План офісу', 'messages'
GO

------------------------------------------------------------------------------------------------------------------------
--https://teamsoft.atlassian.net/browse/PHARMAWEB-337
------------------------------------------------------------------------------------------------------------------------
exec insertLanguageInterface 'en', 'Target audiences', N'Target audiences', 'messages'
exec insertLanguageInterface 'ru', 'Target audiences', N'Целевые аудитории', 'messages'
exec insertLanguageInterface 'uk', 'Target audiences', N'Цільові аудиторії', 'messages'
GO
exec insertLanguageInterface 'en', 'Number of contact', N'Number of contact', 'messages'
exec insertLanguageInterface 'ru', 'Number of contact', N'Количество контактов', 'messages'
exec insertLanguageInterface 'uk', 'Number of contact', N'Кількість контактів', 'messages'
GO
exec insertLanguageInterface 'en', 'Total customers in the database CRM', N'Total customers in the database CRM', 'messages'
exec insertLanguageInterface 'ru', 'Total customers in the database CRM', N'Всего клиентов в базе CRM', 'messages'
exec insertLanguageInterface 'uk', 'Total customers in the database CRM', N'Всього клієнтів в базі CRM', 'messages'
GO
exec insertLanguageInterface 'en', 'No contact', N'No contact', 'messages'
exec insertLanguageInterface 'ru', 'No contact', N'Без контактов', 'messages'
exec insertLanguageInterface 'uk', 'No contact', N'Без контактів', 'messages'
GO
exec insertLanguageInterface 'en', 'With contacts', N'With contacts', 'messages'
exec insertLanguageInterface 'ru', 'With contacts', N'С контактами', 'messages'
exec insertLanguageInterface 'uk', 'With contacts', N'З контактами', 'messages'
GO
exec insertLanguageInterface 'en', 'Types', N'Types', 'messages'
exec insertLanguageInterface 'ru', 'Types', N'Типы', 'messages'
exec insertLanguageInterface 'uk', 'Types', N'Типи', 'messages'
GO
exec insertLanguageInterface 'en', 'recipient (s)', N'recipient (s)', 'messages'
exec insertLanguageInterface 'ru', 'recipient (s)', N'получателя(-лей)', 'messages'
exec insertLanguageInterface 'uk', 'recipient (s)', N'одержувач(-чів)', 'messages'
GO
exec insertLanguageInterface 'en', 'Please, select at least one filter', N'Please, select at least one filter', 'messages'
exec insertLanguageInterface 'uk', 'Please, select at least one filter', N'Виберіть принаймні один фільтр', 'messages'
GO
exec insertLanguageInterface 'en', 'of content', N'of of content', 'messages'
exec insertLanguageInterface 'ru', 'of content', N'of контента', 'messages'
exec insertLanguageInterface 'uk', 'of content', N'of контенту', 'messages'
GO
------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------------
-- https://teamsoft.atlassian.net/browse/PHARMAWEB-294
---------------------------------------------------------------------------------------------------------
-- insertLanguageInterface (@slug VARCHAR(2), @key VARCHAR(255), @translation VARCHAR(255), @domain VARCHAR(255) = 'messages')
exec insertLanguageInterface 'en', 'Additional phone', 'Additional phone'
GO
exec insertLanguageInterface 'ru', 'Additional phone', N'Дополнительный телефон'
GO
exec insertLanguageInterface 'uk', 'Additional phone', N'Додатковий телефон'
GO

exec insertLanguageInterface 'en', 'Primary phone', 'Primary phone'
GO
exec insertLanguageInterface 'ru', 'Primary phone', N'Основной телефон'
GO
exec insertLanguageInterface 'uk', 'Primary phone', N'Основний телефон'
GO


UPDATE  i
SET [i].value = 'Additional phone'
FROM info_dictionaryidentifier AS i
    INNER JOIN info_language AS l ON(l.id =  i.language_id)
    WHERE [i].tablename = 'info_user' AND [i].columnname = 'phonehome' AND [l].slug = 'en'
GO

UPDATE  i
SET [i].value = N'Дополнительный телефон'
FROM info_dictionaryidentifier AS i
         INNER JOIN info_language AS l ON(l.id =  i.language_id)
WHERE [i].tablename = 'info_user' AND [i].columnname = 'phonehome' AND [l].slug = 'ru'
GO

UPDATE  i
SET [i].value = N'Додатковий телефон'
FROM info_dictionaryidentifier AS i
         INNER JOIN info_language AS l ON(l.id =  i.language_id)
WHERE [i].tablename = 'info_user' AND [i].columnname = 'phonehome' AND [l].slug = 'uk'
GO


UPDATE  i
SET [i].value = 'Primary phone'
    FROM info_dictionaryidentifier AS i
    INNER JOIN info_language AS l ON(l.id =  i.language_id)
    WHERE [i].tablename = 'info_user' AND [i].columnname = 'phonemob' AND [l].slug = 'en'
GO

UPDATE  i
SET [i].value = N'Основной телефон'
FROM info_dictionaryidentifier AS i
         INNER JOIN info_language AS l ON(l.id =  i.language_id)
WHERE [i].tablename = 'info_user' AND [i].columnname = 'phonemob' AND [l].slug = 'ru'
GO

UPDATE  i
SET [i].value = N'Основний телефон'
FROM info_dictionaryidentifier AS i
         INNER JOIN info_language AS l ON(l.id =  i.language_id)
WHERE [i].tablename = 'info_user' AND [i].columnname = 'phonemob' AND [l].slug = 'uk'
GO
---------------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------------
-- https://teamsoft.atlassian.net/browse/PHARMAWEB-330
---------------------------------------------------------------------------------------------------------
IF NOT EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'info_reporttabfield' AND Column_Name = 'action')
BEGIN
 ALTER TABLE info_reporttabfield ADD action VARCHAR(255) NULL
END
GO

IF NOT EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'info_reporttabfield' AND Column_Name = 'action_text')
BEGIN
 ALTER TABLE info_reporttabfield ADD action_text VARCHAR(255) NULL
END
GO

IF NOT EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'info_reporttabfield' AND Column_Name = 'action_reference')
BEGIN
 ALTER TABLE info_reporttabfield ADD action_reference VARCHAR(255) NULL
END
GO

exec insertLanguageInterface 'ru', 'Reference column', N'Ссылка на колонку', 'crm'
go
exec insertLanguageInterface 'uk', 'Reference column', N'Посилання на колонку', 'crm'
go
exec insertLanguageInterface 'ru', 'Column type', N'Тип колонки', 'crm'
go
exec insertLanguageInterface 'uk', 'Column type', N'Тип колонки', 'crm'
go
exec insertLanguageInterface 'ru', 'Choose type', N'Выберите тип', 'crm'
go
exec insertLanguageInterface 'uk', 'Choose type', N'Виберіть тип', 'crm'
go
exec insertLanguageInterface 'ru', 'Action button', N'Действие', 'crm'
go
exec insertLanguageInterface 'uk', 'Action button', N'Дія', 'crm'
go
exec insertLanguageInterface 'ru', 'Formula', N'Формула', 'crm'
go
exec insertLanguageInterface 'uk', 'Formula', N'Формула', 'crm'
go
exec insertLanguageInterface 'ru', 'Choose action', N'Выберите действие', 'crm'
go
exec insertLanguageInterface 'uk', 'Choose action', N'Виберіть дію', 'crm'
go
exec insertLanguageInterface 'ru', 'Add task by company', N'Добавить визит по компании', 'crm'
go
exec insertLanguageInterface 'uk', 'Add task by company', N'Додати візит по компанії', 'crm'
go
exec insertLanguageInterface 'ru', 'Add task by contact', N'Добавить задачу по клиенту', 'crm'
go
exec insertLanguageInterface 'uk', 'Add task by contact', N'Додати візит по клієнту', 'crm'
go
exec insertLanguageInterface 'ru', 'Show company', N'Показать компанию', 'crm'
go
exec insertLanguageInterface 'uk', 'Show company', N'Показати компанію', 'crm'
go
exec insertLanguageInterface 'ru', 'Show contact', N'Показать клиента', 'crm'
go
exec insertLanguageInterface 'uk', 'Show contact', N'Показати клієнта', 'crm'
go
exec insertLanguageInterface 'ru', 'Action text', N'Текст', 'crm'
go
exec insertLanguageInterface 'uk', 'Action text', N'Текст', 'crm'
go

---------------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------------------------------------------------------------------------
-- v-17
-- https://teamsoft.atlassian.net/browse/PHARMAWEB-317
----------------------------------------------------------------------------------------------------------------------------------------------------------------------
IF NOT EXISTS(SELECT 1
              FROM INFORMATION_SCHEMA.COLUMNS
              WHERE table_name = 'info_report'
                AND column_name = 'limit')
    begin
        ALTER TABLE info_report
            ADD [limit] int NULL default 100000
    end
GO
IF NOT EXISTS(SELECT 1
              FROM INFORMATION_SCHEMA.COLUMNS
              WHERE table_name = 'info_reporttabfilter'
                AND column_name = 'slice_id')
    begin
        ALTER TABLE info_reporttabfilter ADD [slice_id] int

        ALTER TABLE [dbo].[info_reporttabfilter]
            WITH CHECK ADD CONSTRAINT [FK_info_reporttabfilter_slice_id] FOREIGN KEY ([slice_id])
                REFERENCES [dbo].[info_slice] ([id])
    end
GO

exec insertLanguageInterface 'ru', 'Limit number of visible records', N'Ограничение количества видимых записей', 'crm'
GO
exec insertLanguageInterface 'ru', 'Limit number of visible records', N'Обмеження кількості видимих записів', 'crm'
GO

exec insertLanguageInterface 'ru', 'Export to CSV', N'Экспорт в CSV', 'crm'
GO

exec insertLanguageInterface 'ru', 'Export to CSV', N'Експортувати в CSV', 'crm'
GO
-----------------------------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------------------------

---------------FIX info_polygon index----------------------------------------------------------------------
-----------------------------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------------------------

if exists(select * from sys.indexes where name = 'ix_info_polygon' and type_desc = 'SPATIAL')
        BEGIN
           DROP INDEX ix_info_polygon ON [dbo].[info_polygon];
        END
GO
SET ANSI_NULLS ON;
SET ANSI_PADDING ON;
SET QUOTED_IDENTIFIER ON;
GO
if not exists(select *
              from sys.indexes
              where name = 'ix_info_polygon_polygon'
                and type_desc = 'SPATIAL')
begin
CREATE SPATIAL INDEX [ix_info_polygon_polygon] ON [dbo].[info_polygon]
            (
             [polygon]
                ) USING GEOMETRY_GRID
            WITH (
            BOUNDING_BOX =(15, 40, 180, 75), GRIDS =(LEVEL_1 = MEDIUM, LEVEL_2 = MEDIUM, LEVEL_3 = MEDIUM, LEVEL_4 = MEDIUM),
            CELLS_PER_OBJECT = 16, PAD_INDEX = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
end
GO
SET ANSI_NULLS OFF;
SET ANSI_PADDING OFF;
SET QUOTED_IDENTIFIER OFF;
GO
-----------------------------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------------------------

-----------------------------------------------------------------------------------------------------------
--https://teamsoft.atlassian.net/browse/PHARMAWEB-284
-----------------------------------------------------------------------------------------------------------

INSERT INTO info_tabcontroldictionary (parent_type, type, parent_type_name, type_name) SELECT 5, 401, 'Склад', 'Промо-материалы'
WHERE NOT EXISTS (SELECT 1 FROM info_tabcontroldictionary WHERE parent_type = 5 AND type = 401);
INSERT INTO info_tabcontroldictionary (parent_type, type, parent_type_name, type_name) SELECT 5, 402, 'Склад', 'Перемещения'
WHERE NOT EXISTS (SELECT 1 FROM info_tabcontroldictionary WHERE parent_type = 5 AND type = 402);
INSERT INTO info_tabcontroldictionary (parent_type, type, parent_type_name, type_name) SELECT 5, 403, 'Склад', 'Запросы'
WHERE NOT EXISTS (SELECT 1 FROM info_tabcontroldictionary WHERE parent_type = 5 AND type = 403);
----------------------------
INSERT INTO info_tabcontroldictionary (parent_type, type, parent_type_name, type_name) SELECT 10, 25, 'Клиенты', 'История взаимоотношений'
WHERE NOT EXISTS (SELECT 1 FROM info_tabcontroldictionary WHERE parent_type = 10 AND type = 25);
INSERT INTO info_tabcontroldictionary (parent_type, type, parent_type_name, type_name) SELECT 10, 27, 'Клиенты', 'Ответственные'
WHERE NOT EXISTS (SELECT 1 FROM info_tabcontroldictionary WHERE parent_type = 10 AND type = 27);
INSERT INTO info_tabcontroldictionary (parent_type, type, parent_type_name, type_name) SELECT 10, 110, 'Клиенты', 'Ответственные (верификация)'
WHERE NOT EXISTS (SELECT 1 FROM info_tabcontroldictionary WHERE parent_type = 10 AND type = 110);
INSERT INTO info_tabcontroldictionary (parent_type, type, parent_type_name, type_name) SELECT 10, 116, 'Клиенты', 'Продажи'
WHERE NOT EXISTS (SELECT 1 FROM info_tabcontroldictionary WHERE parent_type = 10 AND type = 116);
INSERT INTO info_tabcontroldictionary (parent_type, type, parent_type_name, type_name) SELECT 10, 117, 'Клиенты', 'Продажи за последний месяц'
WHERE NOT EXISTS (SELECT 1 FROM info_tabcontroldictionary WHERE parent_type = 10 AND type = 117);
INSERT INTO info_tabcontroldictionary (parent_type, type, parent_type_name, type_name) SELECT 10, 118, 'Клиенты', 'Мероприятия'
WHERE NOT EXISTS (SELECT 1 FROM info_tabcontroldictionary WHERE parent_type = 10 AND type = 118);
INSERT INTO info_tabcontroldictionary (parent_type, type, parent_type_name, type_name) SELECT 10, 119, 'Клиенты', 'История промо-материалов'
WHERE NOT EXISTS (SELECT 1 FROM info_tabcontroldictionary WHERE parent_type = 10 AND type = 119);
INSERT INTO info_tabcontroldictionary (parent_type, type, parent_type_name, type_name) SELECT 10, 123, 'Клиенты', 'Кампании'
WHERE NOT EXISTS (SELECT 1 FROM info_tabcontroldictionary WHERE parent_type = 10 AND type = 123);
INSERT INTO info_tabcontroldictionary (parent_type, type, parent_type_name, type_name) SELECT 10, 127, 'Клиенты', 'Параметры брендов'
WHERE NOT EXISTS (SELECT 1 FROM info_tabcontroldictionary WHERE parent_type = 10 AND type = 127);
INSERT INTO info_tabcontroldictionary (parent_type, type, parent_type_name, type_name) SELECT 10, 130, 'Клиенты', 'Маркетинговые проекты'
WHERE NOT EXISTS (SELECT 1 FROM info_tabcontroldictionary WHERE parent_type = 10 AND type = 130);
----------------------------
INSERT INTO info_tabcontroldictionary (parent_type, type, parent_type_name, type_name) SELECT 11, 25, 'Учреждения', 'История взаимоотношений'
WHERE NOT EXISTS (SELECT 1 FROM info_tabcontroldictionary WHERE parent_type = 11 AND type = 25);
INSERT INTO info_tabcontroldictionary (parent_type, type, parent_type_name, type_name) SELECT 11, 27, 'Учреждения', 'Ответственные'
WHERE NOT EXISTS (SELECT 1 FROM info_tabcontroldictionary WHERE parent_type = 11 AND type = 27);
INSERT INTO info_tabcontroldictionary (parent_type, type, parent_type_name, type_name) SELECT 11, 110, 'Учреждения', 'Ответственные (верификация)'
WHERE NOT EXISTS (SELECT 1 FROM info_tabcontroldictionary WHERE parent_type = 11 AND type = 110);
INSERT INTO info_tabcontroldictionary (parent_type, type, parent_type_name, type_name) SELECT 11, 111, 'Учреждения', 'Календарь продаж'
WHERE NOT EXISTS (SELECT 1 FROM info_tabcontroldictionary WHERE parent_type = 11 AND type = 111);
INSERT INTO info_tabcontroldictionary (parent_type, type, parent_type_name, type_name) SELECT 11, 112, 'Учреждения', 'Акции'
WHERE NOT EXISTS (SELECT 1 FROM info_tabcontroldictionary WHERE parent_type = 11 AND type = 112);
INSERT INTO info_tabcontroldictionary (parent_type, type, parent_type_name, type_name) SELECT 11, 113, 'Учреждения', 'Промоции'
WHERE NOT EXISTS (SELECT 1 FROM info_tabcontroldictionary WHERE parent_type = 11 AND type = 113);
INSERT INTO info_tabcontroldictionary (parent_type, type, parent_type_name, type_name) SELECT 11, 114, 'Учреждения', 'Сотрудники'
WHERE NOT EXISTS (SELECT 1 FROM info_tabcontroldictionary WHERE parent_type = 11 AND type = 114);
INSERT INTO info_tabcontroldictionary (parent_type, type, parent_type_name, type_name) SELECT 11, 120, 'Учреждения', 'Отделения'
WHERE NOT EXISTS (SELECT 1 FROM info_tabcontroldictionary WHERE parent_type = 11 AND type = 120);
INSERT INTO info_tabcontroldictionary (parent_type, type, parent_type_name, type_name) SELECT 11, 122, 'Учреждения', 'Дистрибьюторы'
WHERE NOT EXISTS (SELECT 1 FROM info_tabcontroldictionary WHERE parent_type = 11 AND type = 122);
INSERT INTO info_tabcontroldictionary (parent_type, type, parent_type_name, type_name) SELECT 11, 123, 'Учреждения', 'Кампании'
WHERE NOT EXISTS (SELECT 1 FROM info_tabcontroldictionary WHERE parent_type = 11 AND type = 123);
INSERT INTO info_tabcontroldictionary (parent_type, type, parent_type_name, type_name) SELECT 11, 130, 'Учреждения', 'Маркетинговые проекты'
WHERE NOT EXISTS (SELECT 1 FROM info_tabcontroldictionary WHERE parent_type = 11 AND type = 130);
----------------------------
INSERT INTO info_tabcontroldictionary (parent_type, type, parent_type_name, type_name) SELECT 30, 124, 'Фармаконадзор', 'Заключение'
WHERE NOT EXISTS (SELECT 1 FROM info_tabcontroldictionary WHERE parent_type = 30 AND type = 124);
----------------------------
INSERT INTO info_tabcontroldictionary (parent_type, type, parent_type_name, type_name) SELECT 62, 125, 'Рекламации', 'Заключение'
WHERE NOT EXISTS (SELECT 1 FROM info_tabcontroldictionary WHERE parent_type = 62 AND type = 125);
GO
-----------------------------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------------------------

-- fix translation
-----------------------------------------------------------------------------------------------------------
UPDATE info_languageinterface
SET translation = 'Отчеты'
WHERE translation = 'Отчетов' AND domain = 'messages' AND [key] = 'reports';
GO
-----------------------------------------------------------------------------------------------------------

------------------------------------------------------------------------------------------------------------------------
--add column info_mcmdistributionaction
------------------------------------------------------------------------------------------------------------------------

IF OBJECT_ID(N'[info_mcmdistributionaction]', 'U') IS NULL
    BEGIN
        -- auto-generated definition
        create table info_mcmdistributionaction
        (
            id              int identity
                constraint PK_info_mcmdistributionaction
                    primary key,
            distribution_id int
                constraint FK_info_mcmdistributionaction_info_mcmdistribution
                    references info_mcmdistribution,
            contact_id      int
                constraint FK_info_mcmdistributionaction_info_contact
                    references info_contact,
            status          varchar(255),
            request_id      varchar(255),
            failed          int,
            delivered       int,
            messenger_id    int,
            createdate      datetime,
            currenttime     datetime,
            time            timestamp null,
            moduser         varchar(16),
            guid            uniqueidentifier,
            reason          varchar(255),
            recipient       varchar(255),
            trigger_id      int
                constraint FK_info_mcmdistributionaction_info_mcmtrigger
                    references info_mcmtrigger,
            senddate        datetime,
            task_id         int
                constraint FK_info_mcmdistributionaction_info_task
                    references info_task,
            user_id         int
        )
    end
go

IF NOT EXISTS(SELECT 1
              FROM INFORMATION_SCHEMA.COLUMNS
              WHERE table_name = 'info_mcmdistributionaction'
                AND column_name = 'user_id')
ALTER TABLE info_mcmdistributionaction
    ADD user_id int NULL
GO
------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------