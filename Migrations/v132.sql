------------------------------------------------------------------------------------------------------------------------
-- fix
------------------------------------------------------------------------------------------------------------------------
delete from po_webservice where name = 'DCR'
update po_webservice set url = '/dcr/{_locale}' where identifier = 'dcr'
GO
------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------
-- https://teamsoft.atlassian.net/browse/REDDYSRSA-200
------------------------------------------------------------------------------------------------------------------------
IF NOT EXISTS(
    SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS
    WHERE table_name = 'info_mcmdistribution'AND column_name = 'tag'
)
ALTER TABLE info_mcmdistribution ADD tag varchar(255) NULL
GO

exec insertLanguageInterface  'en', 'Tag', N'Tag', 'mcm'
exec insertLanguageInterface  'ru', 'Tag', N'Тег', 'mcm'
exec insertLanguageInterface  'uk', 'Tag', N'Тег', 'mcm'
GO
------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------
-- https://teamsoft.atlassian.net/browse/REDDYSRSA-201
----------------------------------------------------------------------------------------------------
IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'info_promoproject' AND Column_Name = 'isformcm')
begin
ALTER TABLE info_promoproject ADD isformcm int
end
GO

EXEC insertLanguageInterface 'en', 'For MCM', 'For MCM', 'messages';
EXEC insertLanguageInterface 'ru', 'For MCM', 'Для MCM', 'messages';
EXEC insertLanguageInterface 'uk', 'For MCM', 'Для MCM', 'messages';
GO
----------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------

------------------------------------------------------------------------------------------------------------------------
-- https://teamsoft.atlassian.net/browse/EGISRU-391
------------------------------------------------------------------------------------------------------------------------
IF NOT EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE table_name = 'info_mcmdistributionaction' AND column_name = 'final_status_date')
ALTER TABLE info_mcmdistributionaction ADD final_status_date datetime NULL
GO
------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------
-- https://teamsoft.atlassian.net/browse/EGISRU-229
----------------------------------------------------------------------------------------------------
IF NOT EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'info_mcmtarget'AND Column_Name = 'email_actual')
BEGIN
ALTER TABLE info_mcmtarget
    ADD email_actual INT NULL
END
GO
IF NOT EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'info_mcmtarget'AND Column_Name = 'viber_actual')
BEGIN
ALTER TABLE info_mcmtarget
    ADD viber_actual INT NULL
END
GO
IF NOT EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'info_mcmtarget'AND Column_Name = 'sms_actual')
BEGIN
ALTER TABLE info_mcmtarget
    ADD sms_actual INT NULL
END
GO
IF NOT EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE table_name='info_mcmtarget' and column_name = 'is_custom_modify')
begin
ALTER TABLE info_mcmtarget add is_custom_modify INT NULL;
END
GO

EXEC insertLanguageInterface 'en', 'Selected contacts', 'Selected contacts', 'mcm';
EXEC insertLanguageInterface 'ru', 'Selected contacts', 'Выбранные контакты', 'mcm';
EXEC insertLanguageInterface 'uk', 'Selected contacts', 'Обрані контакти', 'mcm';

EXEC insertLanguageInterface 'en', 'Show recipient list', 'Show recipient list', 'mcm';
EXEC insertLanguageInterface 'ru', 'Show recipient list', 'Просмотреть список получателей', 'mcm';
EXEC insertLanguageInterface 'uk', 'Show recipient list', 'Переглянути список одержувачів', 'mcm';
GO
----------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------
---https://teamsoft.atlassian.net/browse/MCM-419
------------------------------------------------------------------------------------------------------------------------
IF OBJECT_ID(N'[info_mcmcontentbutton]', 'U') IS NULL
BEGIN
CREATE TABLE [dbo].[info_mcmcontentbutton]
(
    [id]          [int] IDENTITY (1,1) NOT NULL,
    [content_id]  [int]                NULL,
    [caption]     [varchar](255)       NULL,
    [url]         [varchar](255)       NULL,
    [phone]       [varchar](255)       NULL,
    [type]        [varchar](255)       NULL,
    [currenttime] datetime             NULL
    CONSTRAINT [DF_info_mcmcontentbuttoncurrenttime] DEFAULT (getdate()),
    [time]        [timestamp]          NULL,
    [moduser]     varchar(16)          NULL
    CONSTRAINT [DF_info_mcmcontentbuttonmoduser] DEFAULT ([dbo].[Get_CurrentCode]()),
    [guid]        uniqueidentifier     NULL
    CONSTRAINT [DF_info_mcmcontentbuttonguid] DEFAULT (newid()),
    CONSTRAINT [PK_info_mcmcontentbutton] PRIMARY KEY (id) ON [PRIMARY],
    CONSTRAINT [FK_info_mcmcontentbutton_content_id] FOREIGN KEY ([content_id]) REFERENCES info_mcmcontent ([id])
    )
END
GO

if not exists (select * from dbo.sysobjects where id = object_id('del_info_mcmcontentbutton') and OBJECTPROPERTY(id, 'IsTrigger') = 1)
    exec('create trigger del_info_mcmcontentbutton on info_mcmcontentbutton for delete as if object_id(''empty.dbo.f_rep_del'') is null insert into rep_del(tablename, del_guid) select ''info_mcmcontentbutton'', guid from deleted')
GO

if not exists (select * from dbo.sysobjects where id = object_id('moduser_info_mcmcontentbutton') and OBJECTPROPERTY(id, 'IsTrigger') = 1)
    exec('create trigger moduser_info_mcmcontentbutton on info_mcmcontentbutton for insert, update as if not update(moduser) update info_mcmcontentbutton set moduser = dbo.Get_CurrentCode(), currenttime = getdate() where id in (select id from inserted)')
GO

IF NOT EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE table_name = 'info_contactphone' AND column_name = 'iswhatsappmcm')
ALTER TABLE info_contactphone ADD iswhatsappmcm integer NULL
GO

IF NOT EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE table_name = 'info_mcmcontent' AND column_name = 'header_type')
ALTER TABLE info_mcmcontent ADD header_type varchar(255) NULL
GO

IF NOT EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE table_name = 'info_mcmcontent' AND column_name = 'header_body')
ALTER TABLE info_mcmcontent ADD header_body varchar(255) NULL
GO

IF NOT EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE table_name = 'info_mcmcontent' AND column_name = 'is_buttons_text')
ALTER TABLE info_mcmcontent ADD is_buttons_text int NULL
GO

IF NOT EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE table_name = 'info_mcmcontent' AND column_name = 'is_buttons_action')
ALTER TABLE info_mcmcontent ADD is_buttons_action int NULL
GO

IF NOT EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE table_name = 'info_mcmcontent' AND column_name = 'parent_id')
BEGIN
    ALTER TABLE info_mcmcontent ADD parent_id int NULL
    ALTER TABLE [dbo].[info_mcmcontent]
        WITH CHECK ADD CONSTRAINT [FK_info_mcmcontent_parent_id] FOREIGN KEY ([parent_id])
        REFERENCES [dbo].[info_mcmcontent] ([id])
END
GO

IF NOT EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE table_name = 'info_mcmdistributionaction' AND column_name = 'content_id')
BEGIN
    ALTER TABLE info_mcmdistributionaction ADD content_id int NULL
    ALTER TABLE [dbo].[info_mcmdistributionaction]
        WITH CHECK ADD CONSTRAINT [FK_info_mcmdistributionaction_content_id] FOREIGN KEY ([content_id])
        REFERENCES [dbo].[info_mcmcontent] ([id])
END
GO

IF NOT EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE table_name = 'info_mcmdistributionaction' AND column_name = 'del_status_at')
    ALTER TABLE info_mcmdistributionaction ADD del_status_at datetime NULL
GO

IF NOT EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE table_name = 'info_device' AND column_name = 'guid')
    BEGIN
        ALTER TABLE info_device ADD guid uniqueidentifier NULL
        ALTER TABLE [dbo].[info_device]
            ADD CONSTRAINT [DF_info_deviceguid] DEFAULT (newid()) FOR [guid]
    END
GO

IF OBJECT_ID(N'[info_mcmcontactdistributionmsg]', 'U') IS NULL
BEGIN
CREATE TABLE [dbo].[info_mcmcontactdistributionmsg]
(
    [id]                     [int] IDENTITY (1,1) NOT NULL,
    [contact_id]             [int]                NULL,
    [distribution_id]        [int]                NULL,
    [content_id]             [int]                NULL,
    [user_id]                [int]                NULL,
    [distributionaction_id]  [int]                NULL,
    [currenttime]            [datetime]           NULL,
    [time]                   [timestamp]          NULL,
    [guid] uniqueidentifier  null CONSTRAINT [DF_info_mcmcontactdistributionmsgguid] DEFAULT (newid()),
    [moduser] varchar (16) null CONSTRAINT [DF_info_mcmcontactdistributionmsgmoduser] DEFAULT ([dbo].[Get_CurrentCode]())

    CONSTRAINT [PK_info_mcmcontactdistributionmsg] PRIMARY KEY CLUSTERED
(
[id] ASC
) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON,
      ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
    ) ON [PRIMARY]

ALTER TABLE [dbo].[info_mcmcontactdistributionmsg]
    WITH CHECK ADD CONSTRAINT [FK_info_mcmcontactdistributionmsg_info_mcmcontent] FOREIGN KEY ([content_id])
    REFERENCES [dbo].[info_mcmcontent] ([id])

ALTER TABLE [dbo].[info_mcmcontactdistributionmsg]
    WITH CHECK ADD CONSTRAINT [FK_info_mcmcontactdistributionmsg_info_mcmdistribution] FOREIGN KEY ([distribution_id])
    REFERENCES [dbo].[info_mcmdistribution] ([id])

ALTER TABLE [dbo].[info_mcmcontactdistributionmsg]
    WITH CHECK ADD CONSTRAINT [FK_info_mcmcontactdistributionmsg_info_contact] FOREIGN KEY ([contact_id])
    REFERENCES [dbo].[info_contact] ([id])

ALTER TABLE [dbo].[info_mcmcontactdistributionmsg]
    WITH CHECK ADD CONSTRAINT [FK_info_mcmcontactdistributionmsg_info_user] FOREIGN KEY ([user_id])
    REFERENCES [dbo].[info_user] ([id])

ALTER TABLE [dbo].[info_mcmcontactdistributionmsg]
    WITH CHECK ADD CONSTRAINT [FK_info_mcmcontactdistributionmsg_info_mcmdistributionaction] FOREIGN KEY ([distributionaction_id])
    REFERENCES [dbo].[info_mcmdistributionaction] ([id])
END
GO

IF OBJECT_ID(N'info_mcmcontactanswer', 'U') is NULL
begin
create table info_mcmcontactanswer(
    [id] int identity(1,1) not null
    , [distributionmsg_id] int  null
            , [msg] varchar (255) null
            , [url] varchar (255) null
            , [dt] datetime  null
            , [currenttime] datetime  null CONSTRAINT [DF_info_mcmcontactanswercurrenttime] DEFAULT (getdate())
            , [time] timestamp  null
            , [moduser] varchar (16) null CONSTRAINT [DF_info_mcmcontactanswermoduser] DEFAULT ([dbo].[Get_CurrentCode]())
            , [guid] uniqueidentifier  null CONSTRAINT [DF_info_mcmcontactanswerguid] DEFAULT (newid())
            , CONSTRAINT [PK_info_mcmcontactanswer] PRIMARY KEY (id) ON [PRIMARY]
    , CONSTRAINT [FK_info_mcmcontactanswer_distributionmsg_id] FOREIGN KEY ([distributionmsg_id])
    REFERENCES info_mcmcontactdistributionmsg ([id])
    )
end
GO

if not exists (select * from dbo.sysobjects where id = object_id('del_info_mcmcontactanswer') and OBJECTPROPERTY(id, 'IsTrigger') = 1)
    exec('create trigger del_info_mcmcontactanswer on info_mcmcontactanswer for delete as if object_id(''empty.dbo.f_rep_del'') is null insert into rep_del(tablename, del_guid) select ''info_mcmcontactanswer'', guid from deleted')
GO

if not exists (select * from dbo.sysobjects where id = object_id('moduser_info_mcmcontactanswer') and OBJECTPROPERTY(id, 'IsTrigger') = 1)
    exec('create trigger moduser_info_mcmcontactanswer on info_mcmcontactanswer for insert, update as if not update(moduser) update info_mcmcontactanswer set moduser = dbo.Get_CurrentCode(), currenttime = getdate() where id in (select id from inserted)')
GO

exec insertLanguageInterface  'en', 'Whatsapp Template', N'Whatsapp Template', 'mcm'
exec insertLanguageInterface  'ru', 'Whatsapp Template', N'Whatsapp Шаблон', 'mcm'
exec insertLanguageInterface  'uk', 'Whatsapp Template', N'Whatsapp Шаблон', 'mcm'
GO

exec insertLanguageInterface  'en', 'Signature', N'Signature', 'mcm'
exec insertLanguageInterface  'ru', 'Signature', N'Подпись', 'mcm'
exec insertLanguageInterface  'uk', 'Signature', N'Підпис', 'mcm'
GO

exec insertLanguageInterface  'en', 'Main template', N'Main template', 'mcm'
exec insertLanguageInterface  'ru', 'Main template', N'Основной шаблон', 'mcm'
exec insertLanguageInterface  'uk', 'Main template', N'Головний шаблон', 'mcm'
GO

exec insertLanguageInterface  'en', 'Answers', N'Answers', 'mcm'
exec insertLanguageInterface  'ru', 'Answers', N'Ответы', 'mcm'
exec insertLanguageInterface  'uk', 'Answers', N'Відповіді', 'mcm'
GO

exec insertLanguageInterface  'en', 'Heading', N'Heading', 'mcm'
exec insertLanguageInterface  'ru', 'Heading', N'Заглавие', 'mcm'
exec insertLanguageInterface  'uk', 'Heading', N'Заголовок', 'mcm'
GO

exec insertLanguageInterface  'en', 'Browse', N'Browse', 'mcm'
exec insertLanguageInterface  'ru', 'Browse', N'Обзор', 'mcm'
exec insertLanguageInterface  'uk', 'Browse', N'Огляд', 'mcm'
GO

exec insertLanguageInterface  'en', 'File uploading', N'File uploading', 'mcm'
exec insertLanguageInterface  'ru', 'File uploading', N'Файл загружается', 'mcm'
exec insertLanguageInterface  'uk', 'File uploading', N'Файл завантажується', 'mcm'
GO

exec insertLanguageInterface  'en', 'Delete', N'Delete', 'mcm'
exec insertLanguageInterface  'ru', 'Delete', N'Удалить', 'mcm'
exec insertLanguageInterface  'uk', 'Delete', N'Видалити', 'mcm'
GO

exec insertLanguageInterface  'en', 'Add buttons', N'Add buttons', 'mcm'
exec insertLanguageInterface  'ru', 'Add buttons', N'Добавить кнопки', 'mcm'
exec insertLanguageInterface  'uk', 'Add buttons', N'Додати кнопки', 'mcm'
GO

exec insertLanguageInterface  'en', 'Button URL', N'Button URL', 'mcm'
exec insertLanguageInterface  'ru', 'Button URL', N'Ссылка в кнопке', 'mcm'
exec insertLanguageInterface  'uk', 'Button URL', N'Посилання в кнопці', 'mcm'
GO

exec insertLanguageInterface  'en', 'Text', N'Text', 'mcm'
exec insertLanguageInterface  'ru', 'Text', N'Текст', 'mcm'
exec insertLanguageInterface  'uk', 'Text', N'Текст', 'mcm'
GO

exec insertLanguageInterface  'en', 'Document', N'Document', 'mcm'
exec insertLanguageInterface  'ru', 'Document', N'Документ', 'mcm'
exec insertLanguageInterface  'uk', 'Document', N'Документ', 'mcm'
GO

exec insertLanguageInterface  'en', 'Image file is too large', N'Image file is too large', 'mcm'
exec insertLanguageInterface  'ru', 'Image file is too large', N'Файл изображения слишком большой', 'mcm'
exec insertLanguageInterface  'uk', 'Image file is too large', N'Файл зображення завеликий', 'mcm'
GO

exec insertLanguageInterface  'en', 'max', N'max', 'mcm'
exec insertLanguageInterface  'ru', 'max', N'максимум', 'mcm'
exec insertLanguageInterface  'uk', 'max', N'максимум', 'mcm'
GO

exec insertLanguageInterface  'en', 'Attach video file', N'Attach video file', 'mcm'
exec insertLanguageInterface  'ru', 'Attach video file', N'Прикрепить видео', 'mcm'
exec insertLanguageInterface  'uk', 'Attach video file', N'Прикріпити відео', 'mcm'
GO

exec insertLanguageInterface  'en', 'With text', N'With text', 'mcm'
exec insertLanguageInterface  'ru', 'With text', N'С текстом', 'mcm'
exec insertLanguageInterface  'uk', 'With text', N'З текстом', 'mcm'
GO

exec insertLanguageInterface  'en', 'With action', N'With action', 'mcm'
exec insertLanguageInterface  'ru', 'With action', N'С действием', 'mcm'
exec insertLanguageInterface  'uk', 'With action', N'З дією', 'mcm'
GO

exec insertLanguageInterface  'en', 'Text button', N'Text button', 'mcm'
exec insertLanguageInterface  'ru', 'Text button', N'Текстовая кнопка', 'mcm'
exec insertLanguageInterface  'uk', 'Text button', N'Текстова кнопка', 'mcm'
GO

exec insertLanguageInterface  'en', 'Add button', N'Add button', 'mcm'
exec insertLanguageInterface  'ru', 'Add button', N'Добавить кнопку', 'mcm'
exec insertLanguageInterface  'uk', 'Add button', N'Додати кнопку', 'mcm'
GO

exec insertLanguageInterface  'en', 'Phone number', N'Phone number', 'mcm'
exec insertLanguageInterface  'ru', 'Phone number', N'Номер телефона', 'mcm'
exec insertLanguageInterface  'uk', 'Phone number', N'Номер телефону', 'mcm'
GO

exec insertLanguageInterface  'en', 'Add call button', N'Add call button', 'mcm'
exec insertLanguageInterface  'ru', 'Add call button', N'Добавить кнопку вызова', 'mcm'
exec insertLanguageInterface  'uk', 'Add call button', N'Додати кнопку виклику', 'mcm'
GO

exec insertLanguageInterface  'en', 'Add link button', N'Add link button', 'mcm'
exec insertLanguageInterface  'ru', 'Add link button', N'Добавить кнопку ссылки', 'mcm'
exec insertLanguageInterface  'uk', 'Add link button', N'Додати кнопку посилання', 'mcm'
GO

exec insertLanguageInterface  'en', 'Message', N'Message', 'mcm'
exec insertLanguageInterface  'ru', 'Message', N'Сообщение', 'mcm'
exec insertLanguageInterface  'uk', 'Message', N'Повідомлення', 'mcm'
GO

exec insertLanguageInterface  'en', 'Add answer', N'Add answer', 'mcm'
exec insertLanguageInterface  'ru', 'Add answer', N'Добавить ответ', 'mcm'
exec insertLanguageInterface  'uk', 'Add answer', N'Додати відповідь', 'mcm'
GO

exec insertLanguageInterface  'en', 'Answer type', N'Answer type', 'mcm'
exec insertLanguageInterface  'ru', 'Answer type', N'Тип ответа', 'mcm'
exec insertLanguageInterface  'uk', 'Answer type', N'Тип відповіді', 'mcm'
GO

exec insertLanguageInterface  'en', 'Document caption', N'Document caption', 'mcm'
exec insertLanguageInterface  'ru', 'Document caption', N'Подпись документа', 'mcm'
exec insertLanguageInterface  'uk', 'Document caption', N'Підпис документа', 'mcm'
GO

exec insertLanguageInterface  'en', 'Video caption', N'Video caption', 'mcm'
exec insertLanguageInterface  'ru', 'Video caption', N'Подпись к видео', 'mcm'
exec insertLanguageInterface  'uk', 'Video caption', N'Підпис до видео', 'mcm'
GO

exec insertLanguageInterface  'en', 'Image caption', N'Image caption', 'mcm'
exec insertLanguageInterface  'ru', 'Image caption', N'Подпись изображения', 'mcm'
exec insertLanguageInterface  'uk', 'Image caption', N'Підпис зображення', 'mcm'
GO

exec insertLanguageInterface  'en', 'Add another answer', N'Add another answer', 'mcm'
exec insertLanguageInterface  'ru', 'Add another answer', N'Добавить еще один ответ', 'mcm'
exec insertLanguageInterface  'uk', 'Add another answer', N'Додати ще одну відповідь', 'mcm'
GO

exec insertLanguageInterface  'en', 'Remove', N'Remove', 'mcm'
exec insertLanguageInterface  'ru', 'Remove', N'Удалить', 'mcm'
exec insertLanguageInterface  'uk', 'Remove', N'Видалити', 'mcm'
GO


exec insertLanguageInterface  'en', 'Sort by name', N'Sort by name', 'mcm'
exec insertLanguageInterface  'ru', 'Sort by name', N'Сортировать по названию', 'mcm'
exec insertLanguageInterface  'uk', 'Sort by name', N'Сортувати за назвою', 'mcm'
GO

exec insertLanguageInterface  'en', 'Sort by oldest', N'Sort by oldest', 'mcm'
exec insertLanguageInterface  'ru', 'Sort by oldest', N'Сортировать по cамым старым', 'mcm'
exec insertLanguageInterface  'uk', 'Sort by oldest', N'Сортувати за найстарішими', 'mcm'
GO

exec insertLanguageInterface  'en', 'Sort by newest', N'Sort by newest', 'mcm'
exec insertLanguageInterface  'ru', 'Sort by newest', N'Сортировать по самым новым', 'mcm'
exec insertLanguageInterface  'uk', 'Sort by newest', N'Сортувати за найновішими', 'mcm'
GO

exec insertLanguageInterface  'en', 'Not sent', N'Not sent', 'mcm'
exec insertLanguageInterface  'ru', 'Not sent', N'Не отправлено', 'mcm'
exec insertLanguageInterface  'uk', 'Not sent', N'Не відправлено', 'mcm'
GO

exec insertLanguageInterface  'en', 'Undelivered', N'Undelivered', 'mcm'
exec insertLanguageInterface  'ru', 'Undelivered', N'Не доставлено', 'mcm'
exec insertLanguageInterface  'uk', 'Undelivered', N'Не доставлено', 'mcm'
GO

exec insertLanguageInterface  'en', 'Distribution statistics not avaialble', N'Distribution statistics not avaialble', 'mcm'
exec insertLanguageInterface  'ru', 'Distribution statistics not avaialble', N'Статистика рассылки недоступна', 'mcm'
exec insertLanguageInterface  'uk', 'Distribution statistics not avaialble', N'Статистика розсилки недоступна', 'mcm'
GO

exec insertLanguageInterface  'en', 'Target audiences', N'Target audiences', 'mcm'
exec insertLanguageInterface  'ru', 'Target audiences', N'Целевые аудитории', 'mcm'
exec insertLanguageInterface  'uk', 'Target audiences', N'Цільові аудиторії', 'mcm'
GO

exec insertLanguageInterface  'en', 'With contacts', N'With contacts', 'mcm'
exec insertLanguageInterface  'ru', 'With contacts', N'С контактами', 'mcm'
exec insertLanguageInterface  'uk', 'With contacts', N'З контактами', 'mcm'
GO

exec insertLanguageInterface  'en', 'Total customers in the database CRM', N'Total customers in the database CRM', 'mcm'
exec insertLanguageInterface  'ru', 'Total customers in the database CRM', N'Всего клиентов в базе CRM', 'mcm'
exec insertLanguageInterface  'uk', 'Total customers in the database CRM', N'Всього клієнтів в базі CRM', 'mcm'
GO

exec insertLanguageInterface  'en', 'No contact', N'No contact', 'mcm'
exec insertLanguageInterface  'ru', 'No contact', N'Без контактов', 'mcm'
exec insertLanguageInterface  'uk', 'No contact', N'Без контактів', 'mcm'
GO

exec insertLanguageInterface  'en', 'Count', N'Count', 'mcm'
exec insertLanguageInterface  'ru', 'Count', N'Количество', 'mcm'
exec insertLanguageInterface  'uk', 'Count', N'Кількість', 'mcm'
GO

exec insertLanguageInterface  'en', 'Phone', N'Phone', 'mcm'
exec insertLanguageInterface  'ru', 'Phone', N'Телефон', 'mcm'
exec insertLanguageInterface  'uk', 'Phone', N'Телефон', 'mcm'
GO

exec insertLanguageInterface  'en', 'Email', N'Email', 'mcm'
exec insertLanguageInterface  'ru', 'Email', N'Email', 'mcm'
exec insertLanguageInterface  'uk', 'Email', N'Email', 'mcm'
GO

exec insertLanguageInterface  'en', 'Viber', N'Viber', 'mcm'
exec insertLanguageInterface  'ru', 'Viber', N'Viber', 'mcm'
exec insertLanguageInterface  'uk', 'Viber', N'Viber', 'mcm'
GO

exec insertLanguageInterface 'en', 'Types of', N'Types of', 'mcm'
exec insertLanguageInterface 'ru', 'Types of', N'Типы', 'mcm'
exec insertLanguageInterface 'uk', 'Types of', N'Типи', 'mcm'
GO

exec insertLanguageInterface 'en', 'distributions', N'distributions', 'mcm'
exec insertLanguageInterface 'ru', 'distributions', N'рассылок', 'mcm'
exec insertLanguageInterface 'uk', 'distributions', N'розсилок', 'mcm'
GO

exec insertLanguageInterface 'en', 'File', N'File', 'mcm'
exec insertLanguageInterface 'ru', 'File', N'Файл', 'mcm'
exec insertLanguageInterface 'uk', 'File', N'Файл', 'mcm'
GO

exec insertLanguageInterface  'ru', 'Links will be converted to the following format:', N'Ссылки будут преобразованы в формат', 'mcm'
exec insertLanguageInterface  'en', 'Links will be converted to the following format:', N'Links will be converted to the following format:', 'mcm'
exec insertLanguageInterface  'uk', 'Links will be converted to the following format:', N'Посилання будуть трансформованi в формат', 'mcm'
GO

exec insertLanguageInterface  'ru', 'You''ll be able to get the click-through statictics for the link.', N', при этом вы сможете получить статистику переходов по ссылке.', 'mcm'
exec insertLanguageInterface  'en', 'You''ll be able to get the click-through statictics for the link.', N'You''ll be able to get the click-through statictics for the link.', 'mcm'
exec insertLanguageInterface  'uk', 'You''ll be able to get the click-through statictics for the link.', N', при цьому ви зможете отримати статистику вiдвiдувань за посиланням', 'mcm'
GO

exec insertLanguageInterface  'ru', 'Links will not be converted.You won''t be able to track who followed the link', N'Ссылки не будут преобразованы. Вы не сможете отследить, кто переходил по ссылке', 'mcm'
exec insertLanguageInterface  'en', 'Links will not be converted.You won''t be able to track who followed the link', N'Links will not be converted. You won''t be able to track who followed the link', 'mcm'
exec insertLanguageInterface  'uk', 'Links will not be converted.You won''t be able to track who followed the link', N'Посилання не будуть трансформованi. Ви не зможете вiдслiдкувати, хто преходив за посиланням', 'mcm'
GO

exec insertLanguageInterface  'en', 'Viber Distribution script', N'Viber Distribution script', 'mcm'
exec insertLanguageInterface  'ru', 'Viber Distribution script', N'Сценарий Viber рассылки', 'mcm'
exec insertLanguageInterface  'uk', 'Viber Distribution script', N'Сценарій Viber розсилки', 'mcm'
GO

exec insertLanguageInterface  'en', 'Whatsapp Distribution script', N'Whatsapp Distribution script', 'mcm'
exec insertLanguageInterface  'ru', 'Whatsapp Distribution script', N'Сценарий Whatsapp рассылки', 'mcm'
exec insertLanguageInterface  'uk', 'Whatsapp Distribution script', N'Сценарій Whatsapp розсилки', 'mcm'
GO

exec insertLanguageInterface  'en', 'SMS Distribution script', N'SMS Distribution script', 'mcm'
exec insertLanguageInterface  'ru', 'SMS Distribution script', N'Сценарий СМС рассылки', 'mcm'
exec insertLanguageInterface  'uk', 'SMS Distribution script', N'Сценарій SMS розсилки', 'mcm'
GO

exec insertLanguageInterface  'en', 'Email Distribution script', N'Email Distribution script', 'mcm'
exec insertLanguageInterface  'ru', 'Email Distribution script', N'Сценарий Email рассылки', 'mcm'
exec insertLanguageInterface  'uk', 'Email Distribution script', N'Сценарій Email розсилки', 'mcm'
GO

exec insertLanguageInterface  'en', 'The maximum file size should not exceed', N'The maximum file size should not exceed', 'mcm'
exec insertLanguageInterface  'ru', 'The maximum file size should not exceed', N'Максимальный размер файла не должен превышать', 'mcm'
exec insertLanguageInterface  'uk', 'The maximum file size should not exceed', N'Максимальний розмір файлу не має перевищувати', 'mcm'
GO

exec insertLanguageInterface  'en', 'MB', N'MB', 'mcm'
exec insertLanguageInterface  'ru', 'MB', N'МБ', 'mcm'
exec insertLanguageInterface  'uk', 'MB', N'МБ', 'mcm'
GO


exec insertLanguageInterface  'en', 'Template', N'Template', 'mcm'
exec insertLanguageInterface  'ru', 'Template', N'Шаблон', 'mcm'
exec insertLanguageInterface  'uk', 'Template', N'Шаблон', 'mcm'
GO

exec insertLanguageInterface  'en', 'Validity', N'Validity', 'mcm'
exec insertLanguageInterface  'ru', 'Validity', N'Срок действия', 'mcm'
exec insertLanguageInterface  'uk', 'Validity', N'Термін дії', 'mcm'
GO
------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------

------------------------------------------------------------------------------------------------------------------------
-- https://teamsoft.atlassian.net/browse/ACINORU-108
------------------------------------------------------------------------------------------------------------------------
-- IF NOT EXISTS(SELECT 1 FROM info_addinfotype WHERE code = 'сonsent_email')
-- begin
--     insert into info_addinfotype (name, code, datatype, fielddatatype) values ('Почта отправлена', 'сonsent_email', 1, 1);
-- end;
-- GO
--
-- IF NOT EXISTS(SELECT 1 FROM info_addinfotype WHERE code = 'consent_sms')
-- begin
--     insert into info_addinfotype (name, code, datatype, fielddatatype) values ('СМС отправлена', 'consent_sms', 1, 1);
-- end;
-- GO
--
-- IF NOT EXISTS(SELECT 1 FROM info_addinfotype WHERE code = 'consent_phone')
-- begin
--     insert into info_addinfotype (name, code, datatype, fielddatatype) values ('Телефон отправлен', 'consent_phone', 1, 1);
-- end;
-- GO
--
-- IF NOT EXISTS(SELECT 1 FROM info_addinfotype WHERE code = 'consent_remote_detailing')
-- begin
--     insert into info_addinfotype (name, code, datatype, fielddatatype) values ('Место проведения отправлено', 'consent_remote_detailing', 1, 1);
-- end;
-- GO
--
-- IF NOT EXISTS(SELECT 1 FROM info_addinfotype WHERE code = 'consent_data_storage')
-- begin
--     insert into info_addinfotype (name, code, datatype, fielddatatype) values ('Данные хранилища отправлены', 'consent_data_storage', 1, 1);
-- end;
-- GO
------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------
