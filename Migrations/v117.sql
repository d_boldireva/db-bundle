---------------------------------------------------------------------------------------------------------
-- https://teamsoft.atlassian.net/browse/YURIAPHARM-429
---------------------------------------------------------------------------------------------------------

UPDATE info_mcmfilter set filter = 'info_contact.id NOT IN (SELECT contact_id FROM info_task WHERE datefrom BETWEEN #MIN# AND #MAX# AND contact_id IS NOT NULL UNION SELECT info_contactintask.contact_id FROM info_contactintask LEFT JOIN info_task ON info_task.id = info_contactintask.Task_id WHERE info_task.datefrom BETWEEN #MIN# AND #MAX#)'
WHERE name = 'Without tasks'
GO
---------------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------------

-- https://teamsoft.atlassian.net/browse/MCM-317
---------------------------------------------------------------------------------------------------------
IF NOT EXISTS (SELECT 1 FROM information_schema.columns WHERE table_name = 'info_contactphone' AND column_name = 'isvibermcm')
    BEGIN
        ALTER TABLE info_contactphone ADD isvibermcm INT NULL
    END
GO

IF NOT EXISTS (SELECT 1 FROM information_schema.columns WHERE table_name = 'info_contactphone' AND column_name = 'issmsmcm')
    BEGIN
        ALTER TABLE info_contactphone ADD issmsmcm INT NULL
    END
GO
---------------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------------

---------------------------------------------------------------------------------------------------------
-- https://teamsoft.atlassian.net/browse/PHARMAWEB-285
---------------------------------------------------------------------------------------------------------
IF NOT EXISTS (SELECT 1 FROM information_schema.columns WHERE table_name = 'info_country' AND column_name = 'pharma_url')
    ALTER TABLE info_country ADD pharma_url varchar(255);
---------------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------------


--------------------------------------------------
--PHARMAWEB-284
IF OBJECT_ID(N'[info_tabcontroldictionary]', 'U') IS NULL
    BEGIN
        CREATE TABLE [dbo].[info_tabcontroldictionary]
        (
            [id]                [int] IDENTITY (1,1) NOT NULL,
            [parent_type]       [int]                NULL,
            [parent_type_name]  [varchar](255)       NULL,
            [type]              [int]                NULL,
            [type_name]         [varchar](255)       NULL,
            [currenttime]       [datetime]           NULL,
            [time]              [timestamp]          NULL,
            [moduser]           [varchar](16)        NULL,
            [guid]              [uniqueidentifier]   NULL,
            CONSTRAINT [PK_info_tabcontroldictionary] PRIMARY KEY CLUSTERED
                (
                 [id] ASC
                    ) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
        ) ON [PRIMARY]

        ALTER TABLE [dbo].[info_tabcontroldictionary]
            ADD CONSTRAINT [DF_info_tabcontroldictionarycurrenttime] DEFAULT (getdate()) FOR [currenttime]
        ALTER TABLE [dbo].[info_tabcontroldictionary]
            ADD CONSTRAINT [DF_info_tabcontroldictionarymoduser] DEFAULT ([dbo].[Get_CurrentCode]()) FOR [moduser]
        ALTER TABLE [dbo].[info_tabcontroldictionary]
            ADD CONSTRAINT [DF_info_tabcontroldictionaryguid] DEFAULT (newid()) FOR [guid]
    END
GO
exec insertLanguageInterface 'ru', 'Tab Control', N'Вкладки'
go
exec insertLanguageInterface 'uk', 'Tab Control', N'Вкладки'
go
exec insertLanguageInterface 'ru', 'No tabs found', N'Нет вкладок'
go
exec insertLanguageInterface 'uk', 'No tabs found', N'Немає вкладок'
go
exec insertLanguageInterface 'ru', 'Are you sure, you want to delete selected tab?', N'Вы уверены, что хотите удалить выбранную вкладку?'
go
exec insertLanguageInterface 'uk', 'Are you sure, you want to delete selected tab?', N'Ви впевнені, що хочете видалити вибрану вкладку?'
go
exec insertLanguageInterface 'ru', 'Delete tab', N'Удалить вкладку'
go
exec insertLanguageInterface 'uk', 'Delete tab', N'Видалити вкладку'
go

-- https://teamsoft.atlassian.net/browse/MCM-295

IF NOT EXISTS(SELECT 1
              FROM information_schema.columns
              WHERE table_name = 'info_contactphone'
                AND column_name = 'morionid')
    BEGIN
        ALTER TABLE info_contactphone
            ADD [morionid] INT NULL;
    END
GO

IF NOT EXISTS(SELECT 1
              FROM info_customdictionary
              WHERE name = 'Пол')
    BEGIN
        insert into info_customdictionary (name) values ('Пол');
            insert into info_customdictionaryvalue (customdictionary_id, dictionaryvalue) values
            ((select id from info_customdictionary where [name] = 'Пол'), 'Мужской'),
            ((select id from info_customdictionary where [name] = 'Пол'), 'Женский');
    END
GO

IF NOT EXISTS(SELECT * FROM po_dictionarygroup WHERE [name] = 'MCM')
    BEGIN
        insert into po_dictionarygroup (name) values ('MCM');
        insert into po_dictionary (identifier, name, tablename, group_id) values
        (NULL, 'Поля персонализации', 'info_mcmcontentvariable', (select id from po_dictionarygroup where name = 'MCM')),
        (NULL, 'Фильтры ЦА', 'info_mcmfilter', (select id from po_dictionarygroup where name = 'MCM')),
        (NULL, 'Отправители', 'info_mcmfrom', (select id from po_dictionarygroup where name = 'MCM')),
        (NULL, 'Поля персонализации клиента', 'info_mcmcontactvariables', (select id from po_dictionarygroup where name = 'MCM'));
--         (NULL, 'Телефоны клиентов', 'info_contactphone', (select id from po_dictionarygroup where name = 'MCM')),
--         (NULL, 'Email адреса клиентов', 'info_contactemail', (select id from po_dictionarygroup where name = 'MCM'));
        insert into info_dictionaryidentifier (tablename, columnname, value, identifier, hidden, order_num, required, language_id) values
        ('info_mcmfrom', 'name', 'Name', 0, 0, 0, 1, (select id from info_language where slug = 'en')),
        ('info_mcmfrom', 'name', 'Имя', 0, 0, 0, 1, (select id from info_language where slug = 'ru')),
        ('info_mcmfrom', 'name', 'Ім''я', 0, 0, 0, 1, (select id from info_language where slug = 'uk')),
        ('info_mcmfrom', 'email', 'Email', 0, 0, 0, 0, (select id from info_language where slug = 'en')),
        ('info_mcmfrom', 'email', 'Email', 0, 0, 0, 0, (select id from info_language where slug = 'ru')),
        ('info_mcmfrom', 'email', 'Email', 0, 0, 0, 0, (select id from info_language where slug = 'uk')),
        ('info_mcmfrom', 'replyto', 'Reply to', 0, 0, 0, 0, (select id from info_language where slug = 'en')),
        ('info_mcmfrom', 'replyto', 'Адрес ответа', 0, 0, 0, 0, (select id from info_language where slug = 'ru')),
        ('info_mcmfrom', 'replyto', 'Адреса відповіді', 0, 0, 0, 0, (select id from info_language where slug = 'uk')),
        ('info_mcmfrom', 'type', 'Type', 0, 0, 0, 1, (select id from info_language where slug = 'en')),
        ('info_mcmfrom', 'type', 'Тип', 0, 0, 0, 1, (select id from info_language where slug = 'ru')),
        ('info_mcmfrom', 'type', 'Тип', 0, 0, 0, 1, (select id from info_language where slug = 'uk')),
        ('info_mcmfrom', 'isdefault', 'Is default', 0, 0, 0, 0, (select id from info_language where slug = 'en')),
        ('info_mcmfrom', 'isdefault', 'По умолчанию', 0, 0, 0, 0, (select id from info_language where slug = 'ru')),
        ('info_mcmfrom', 'isdefault', 'За замовчуванням', 0, 0, 0, 0, (select id from info_language where slug = 'uk')),
        ('info_mcmcontentvariable', 'name', 'Name', 0, 0, 0, 1, (select id from info_language where slug = 'en')),
        ('info_mcmcontentvariable', 'name', 'Имя', 0, 0, 0, 1, (select id from info_language where slug = 'ru')),
        ('info_mcmcontentvariable', 'name', 'Назва', 0, 0, 0, 1, (select id from info_language where slug = 'uk')),
        ('info_mcmcontentvariable', 'code', 'Code', 0, 0, 0, 1, (select id from info_language where slug = 'en')),
        ('info_mcmcontentvariable', 'code', 'Код', 0, 0, 0, 1, (select id from info_language where slug = 'ru')),
        ('info_mcmcontentvariable', 'code', 'Код', 0, 0, 0, 1, (select id from info_language where slug = 'uk')),
        ('info_mcmcontentvariable', 'defaultValue', 'Default value', 0, 0, 0, 0, (select id from info_language where slug = 'en')),
        ('info_mcmcontentvariable', 'defaultValue', 'Значение по умолчанию', 0, 0, 0, 0, (select id from info_language where slug = 'ru')),
        ('info_mcmcontentvariable', 'defaultValue', 'Значення за замовчуванням', 0, 0, 0, 0, (select id from info_language where slug = 'uk')),
        ('info_mcmcontentvariable', 'isdefault', 'Is default', 0, 0, 0, 0, (select id from info_language where slug = 'en')),
        ('info_mcmcontentvariable', 'isdefault', 'По умолчанию', 0, 0, 0, 0, (select id from info_language where slug = 'ru')),
        ('info_mcmcontentvariable', 'isdefault', 'За замовчуванням', 0, 0, 0, 0, (select id from info_language where slug = 'uk')),
        ('info_mcmcontactvariables', 'contact_id', 'Contact', 0, 0, 0, 1, (select id from info_language where slug = 'en')),
        ('info_mcmcontactvariables', 'contact_id', 'Клиент', 0, 0, 0, 1, (select id from info_language where slug = 'ru')),
        ('info_mcmcontactvariables', 'contact_id', 'Клієнт', 0, 0, 0, 1, (select id from info_language where slug = 'uk')),
        ('info_mcmcontactvariables', 'stringValue', 'Value', 0, 0, 0, 1, (select id from info_language where slug = 'en')),
        ('info_mcmcontactvariables', 'stringValue', 'Значение', 0, 0, 0, 1, (select id from info_language where slug = 'ru')),
        ('info_mcmcontactvariables', 'stringValue', 'Значення', 0, 0, 0, 1, (select id from info_language where slug = 'uk')),
        ('info_mcmfilter', 'name', 'Name', 0, 0, 0, 0, (select id from info_language where slug = 'en')),
        ('info_mcmfilter', 'name', 'Имя', 0, 0, 0, 0, (select id from info_language where slug = 'ru')),
        ('info_mcmfilter', 'name', 'Назва', 0, 0, 0, 0, (select id from info_language where slug = 'uk')),
        ('info_mcmfilter', 'filter', 'Filter', 0, 0, 0, 0, (select id from info_language where slug = 'en')),
        ('info_mcmfilter', 'filter', 'Фильтр', 0, 0, 0, 0, (select id from info_language where slug = 'ru')),
        ('info_mcmfilter', 'filter', 'Фільтр', 0, 0, 0, 0, (select id from info_language where slug = 'uk')),
        ('info_mcmfilter', 'customWhere', 'Custom where', 0, 0, 0, 0, (select id from info_language where slug = 'en')),
        ('info_mcmfilter', 'customWhere', 'Пользовательский where', 0, 0, 0, 0, (select id from info_language where slug = 'ru')),
        ('info_mcmfilter', 'customWhere', 'Користувацький where', 0, 0, 0, 0, (select id from info_language where slug = 'uk')),
        ('info_mcmfilter', 'systemdictionary_id', 'System dictionary', 0, 0, 0, 0, (select id from info_language where slug = 'en')),
        ('info_mcmfilter', 'systemdictionary_id', 'Системный справочник', 0, 0, 0, 0, (select id from info_language where slug = 'ru')),
        ('info_mcmfilter', 'systemdictionary_id', 'Системний довідник', 0, 0, 0, 0, (select id from info_language where slug = 'uk')),
        ('info_mcmfilter', 'customdictionary_id', 'Custom dictionary', 0, 0, 0, 0, (select id from info_language where slug = 'en')),
        ('info_mcmfilter', 'customdictionary_id', 'Пользовательский справочник', 0, 0, 0, 0, (select id from info_language where slug = 'ru')),
        ('info_mcmfilter', 'customdictionary_id', 'Користувацький довідник', 0, 0, 0, 0, (select id from info_language where slug = 'uk')),
        ('info_mcmfilter', 'name_ru', 'Name (RU)', 0, 0, 0, 0, (select id from info_language where slug = 'en')),
        ('info_mcmfilter', 'name_ru', 'Имя на русском', 0, 0, 0, 0, (select id from info_language where slug = 'ru')),
        ('info_mcmfilter', 'name_ru', 'Назва російською', 0, 0, 0, 0, (select id from info_language where slug = 'uk')),
        ('info_mcmfilter', 'type', 'Type', 0, 0, 0, 0, (select id from info_language where slug = 'en')),
        ('info_mcmfilter', 'type', 'Тип', 0, 0, 0, 0, (select id from info_language where slug = 'ru')),
        ('info_mcmfilter', 'type', 'Тип', 0, 0, 0, 0, (select id from info_language where slug = 'uk'));
        --('info_contactphone', 'contact_id', 'Contact', 0, 0, 0, 1, (select id from info_language where slug = 'en')),
        --('info_contactphone', 'contact_id', 'Клиент', 0, 0, 0, 1, (select id from info_language where slug = 'ru')),
        --('info_contactphone', 'contact_id', 'Клієнт', 0, 0, 0, 1, (select id from info_language where slug = 'uk')),
        --('info_contactphone', 'phone', 'Phone', 0, 0, 0, 1, (select id from info_language where slug = 'en')),
        --('info_contactphone', 'phone', 'Телефон', 0, 0, 0, 1, (select id from info_language where slug = 'ru')),
        --('info_contactphone', 'phone', 'Телефон', 0, 0, 0, 1, (select id from info_language where slug = 'uk')),
        --('info_contactphone', 'mdm_phone_id', 'MDM Phone ID', 0, 0, 0, 0, (select id from info_language where slug = 'en')),
        --('info_contactphone', 'mdm_phone_id', 'MDM Phone ID', 0, 0, 0, 0, (select id from info_language where slug = 'ru')),
        --('info_contactphone', 'mdm_phone_id', 'MDM Phone ID', 0, 0, 0, 0, (select id from info_language where slug = 'uk')),
        --('info_contactphone', 'isviber', 'Is viber', 0, 0, 0, 0, (select id from info_language where slug = 'en')),
        --('info_contactphone', 'isviber', 'Viber', 0, 0, 0, 0, (select id from info_language where slug = 'ru')),
        --('info_contactphone', 'isviber', 'Viber', 0, 0, 0, 0, (select id from info_language where slug = 'uk')),
        --('info_contactphone', 'user_id', 'User', 0, 0, 0, 0, (select id from info_language where slug = 'en')),
        --('info_contactphone', 'user_id', 'Пользователь', 0, 0, 0, 0, (select id from info_language where slug = 'ru')),
        --('info_contactphone', 'user_id', 'Користувач', 0, 0, 0, 0, (select id from info_language where slug = 'uk')),
        --('info_contactphone', 'phonestate', 'State', 0, 0, 0, 0, (select id from info_language where slug = 'en')),
        --('info_contactphone', 'phonestate', 'Статус', 0, 0, 0, 0, (select id from info_language where slug = 'ru')),
        --('info_contactphone', 'phonestate', 'Статус', 0, 0, 0, 0, (select id from info_language where slug = 'uk')),
        --('info_contactemail', 'contact_id', 'Contact', 0, 0, 0, 1, (select id from info_language where slug = 'en')),
        --('info_contactemail', 'contact_id', 'Клиент', 0, 0, 0, 1, (select id from info_language where slug = 'ru')),
        --('info_contactemail', 'contact_id', 'Клієнт', 0, 0, 0, 1, (select id from info_language where slug = 'uk')),
        --('info_contactemail', 'email', 'Email', 0, 0, 0, 1, (select id from info_language where slug = 'en')),
        --('info_contactemail', 'email', 'Email', 0, 0, 0, 1, (select id from info_language where slug = 'ru')),
        --('info_contactemail', 'email', 'Email', 0, 0, 0, 1, (select id from info_language where slug = 'uk')),
        --('info_contactemail', 'morionid', 'Morion ID', 0, 0, 0, 0, (select id from info_language where slug = 'en')),
        --('info_contactemail', 'morionid', 'Morion ID', 0, 0, 0, 0, (select id from info_language where slug = 'ru')),
        --('info_contactemail', 'morionid', 'Morion ID', 0, 0, 0, 0, (select id from info_language where slug = 'uk')),
        --('info_contactemail', 'response', 'Response', 0, 0, 0, 0, (select id from info_language where slug = 'en')),
        --('info_contactemail', 'response', 'Ответ', 0, 0, 0, 0, (select id from info_language where slug = 'ru')),
        --('info_contactemail', 'response', 'Відповідь', 0, 0, 0, 0, (select id from info_language where slug = 'uk')),
        --('info_contactemail', 'status', 'State', 0, 0, 0, 0, (select id from info_language where slug = 'en')),
        --('info_contactemail', 'status', 'Статус', 0, 0, 0, 0, (select id from info_language where slug = 'ru')),
        --('info_contactemail', 'status', 'Статус', 0, 0, 0, 0, (select id from info_language where slug = 'uk'));
    END
GO

IF EXISTS(SELECT 1 FROM information_schema.columns WHERE table_name = 'info_mcmcontentvariable') AND NOT EXISTS(SELECT 1 FROM info_mcmcontentvariable)
    BEGIN
        insert into info_mcmcontentvariable (name, code, defaultValue, isdefault) values
        ('Обращение',	'GREETING',	'Уважаемый(ая)',	NULL),
        ('Фамилия',	'LASTNAME',	'',	1),
        ('Имя',	'FIRSTNAME',	'',	1),
        ('Отчество',	'MIDDLENAME',	'',	1),
        ('Дата рождения',	'BIRTHDATE',	'',	1),
        ('Телефон',	'PHONE2',	'',	1),
        ('Электронная почта',	'EADDR',	'',	1),
        ('Звернення_(укр)',	'GREETING_UKR',	'Шановний(на)',	NULL),
        ('Прізвище',	'LASTNAME_UKR',	'',	NULL),
        ('Ім"я',	'FIRSTNAME_UKR',	'',	NULL),
        ('По батькові',	'MIDDLENAME_UKR',	'',	NULL),
        ('test link',	'TESTLINK',	'http://www.morion.ua/',	NULL),
        ('test sms',	'TEST',	'test mcm',	NULL);
    END
GO
--for servier_ru
IF NOT EXISTS(SELECT 1 FROM information_schema.columns WHERE table_name = 'info_systemdictionary' AND column_name = 'identifier')
    BEGIN
        ALTER TABLE info_systemdictionary ADD identifier int
    END
GO

IF EXISTS(SELECT 1 FROM information_schema.columns WHERE table_name = 'info_mcmfilter') AND NOT EXISTS(SELECT 1 FROM info_mcmfilter)
    BEGIN
    insert into info_mcmfilter (name, filter, customWhere, systemdictionary_id, customdictionary_id, name_ru, type) values
        ('Target-group',	'info_target.id IN (#VALUE#)',	'info_target.id IN (SELECT info_target.id FROM info_target JOIN info_targetspec ON info_targetspec.target_id = info_target.id JOIN info_contact ON info_contact.specialization_id = info_targetspec.spec_id)',	(select id from info_systemdictionary where tablename = 'info_target'),	NULL,	'Таргет-группа',	'string'),
        ('Company Type',	'info_company.companytype_id IN (#VALUE#)',	'info_companytype.id IN (SELECT info_companytype.id FROM info_contact INNER JOIN info_company ON info_company.id = info_contact.company_id INNER JOIN info_companytype ON info_companytype.id = info_company.companytype_id)',	(select id from info_systemdictionary where tablename = 'info_companyType'),	NULL,	'Тип компании',	'string'),
        ('Region',	'info_contact.region_id IN (#VALUE#)',	'info_regionpart.id IN (SELECT info_regionpart.id FROM info_region INNER JOIN info_regioninregionpart ON info_regioninregionpart.region_id = info_region.id INNER JOIN info_regionpart ON info_regionpart.id = info_regioninregionpart.regionpart_id INNER JOIN info_contact ON info_contact.Region_id = info_region.id)',	(select id from info_systemdictionary where tablename = 'info_regionpart'),	NULL,	'Регион',	'string'),
        ('District',	'info_contact.region_id IN (#VALUE#)',	'info_region.id IN (SELECT info_region.id FROM info_region INNER JOIN info_contact ON info_contact.Region_id = info_region.id)',	(select id from info_systemdictionary where tablename = 'info_Region'),	NULL,	'Область',	'string'),
        ('Specialization',	'info_contact.specialization_id IN (#VALUE#)',	'info_dictionary.id IN (SELECT info_dictionary.id FROM info_dictionary JOIN info_contact ON info_contact.Specialization_id = info_dictionary.id)',	(select id from info_systemdictionary where identifier = 28),	NULL,	'Специализация',	'string'),
        ('Category',	'info_contact.category_id IN (#VALUE#)',	'info_contactcateg.id IN (SELECT info_contactcateg.id FROM info_contactcateg INNER JOIN info_contact ON info_contact.category_id = info_contactcateg.id)',	(select id from info_systemdictionary where tablename = 'info_contactcateg'),	NULL,	'Категория',	'string'),
        ('Gender',	'info_contact.sex IN (#VALUE#)',	NULL,	NULL,	(select id from info_customdictionary where name = 'Пол'),	'Пол',	'string'),
        ('Position',	'info_contact.specialization_id IN (#VALUE#)',	'info_dictionary.id IN (  SELECT info_dictionary.id   FROM info_dictionary   JOIN info_contact ON info_contact.Specialization_id = info_dictionary.id  where info_dictionary.Identifier = 4)',	(select id from info_systemdictionary where identifier = 28),	NULL,	'Должность',	'string'),
        ('Contacttype',	'info_contact.contacttype_id IN (#VALUE#)',	'info_contacttype.id IN (SELECT info_contacttype.id FROM info_contacttype INNER JOIN info_contact ON info_contact.ContactType_id = info_contacttype.id)',	(select id from info_systemdictionary where tablename = 'info_contacttype'),	NULL,	'Тип клиента',	'string'),
        ('Without tasks',	'info_contact.id NOT IN (SELECT contact_id FROM info_task WHERE datefrom BETWEEN #MIN# AND #MAX# UNION SELECT info_contactintask.contact_id FROM info_contactintask LEFT JOIN info_task ON info_task.id = info_contactintask.Task_id WHERE info_task.datefrom BETWEEN #MIN# AND #MAX#)',	'select case DAY(GETDATE()) when 1 then DATEADD(MONTH, -1, DATEADD(DAY,(DATEPART(DAY,GETDATE())-1)*(-1),GETDATE())) else DATEADD(DAY,(DATEPART(DAY,GETDATE())-1)*(-1),GETDATE()) end as min, GETDATE() as max',	NULL,	NULL,	'Не посещённые в период',	'dateRange'),
        ('Without actions',	'info_contact.id NOT IN (SELECT contact_id FROM info_action WHERE datefrom BETWEEN #MIN# AND #MAX# UNION SELECT info_contactinaction.contact_id FROM info_contactinaction LEFT JOIN info_action ON info_action.id = info_contactinaction.Action_id WHERE info_action.datefrom BETWEEN #MIN# AND #MAX#)',	'select case DAY(GETDATE()) when 1 then DATEADD(MONTH, -1, DATEADD(DAY,(DATEPART(DAY,GETDATE())-1)*(-1),GETDATE())) else DATEADD(DAY,(DATEPART(DAY,GETDATE())-1)*(-1),GETDATE()) end as min, GETDATE() as max',	NULL,	NULL,	'Без мероприятий в период',	'dateRange');
    END
GO

---------------------------------------------------------------------------------------------------------
-- https://teamsoft.atlassian.net/browse/MCM-329
---------------------------------------------------------------------------------------------------------
IF NOT EXISTS (SELECT 1 FROM information_schema.columns WHERE table_name = 'info_country' AND column_name = 'phonemask')
    BEGIN
        ALTER TABLE info_country ADD phonemask varchar(255)
    END
GO
---------------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------------
-- https://teamsoft.atlassian.net/browse/ACINO-183
---------------------------------------------------------------------------------------------------------
IF NOT EXISTS(SELECT 1 FROM po_entityfields WHERE page = 1 AND field = 'gps_la')
    BEGIN
        INSERT po_entityfields (page, field, name)
        VALUES (1, 'gps_la', 'Coordinates')
    END
GO
IF NOT EXISTS(SELECT 1 FROM po_entityfields WHERE page = 1 AND field = 'okrug_id')
    BEGIN
        INSERT po_entityfields (page, field, name)
        VALUES (1, 'okrug_id', 'District')
    END
GO
IF NOT EXISTS(SELECT 1 FROM po_entityfields WHERE page = 1 AND field = 'info_contactconnection.company_slave_id')
    BEGIN
        INSERT po_entityfields (page, field, name)
        VALUES (1, 'info_contactconnection.company_slave_id', 'Associated Doctors')
    END
GO
IF NOT EXISTS(SELECT 1 FROM po_entityfields WHERE page = 1 AND field = 'agensy_main_agency')
    BEGIN
        INSERT po_entityfields (page, field, name)
        VALUES (1, 'agensy_main_agency', 'Main institution')
    END
GO
IF NOT EXISTS(SELECT 1 FROM po_entityfields WHERE page = 1 AND field = 'agensy_parent_agency')
    BEGIN
        INSERT po_entityfields (page, field, name)
        VALUES (1, 'agensy_parent_agency', 'Parent institution')
    END
GO
IF NOT EXISTS(SELECT 1 FROM po_entityfields WHERE page = 1 AND field = 'ismain')
    BEGIN
        INSERT po_entityfields (page, field, name)
        VALUES (1, 'ismain', 'Is main')
    END
GO
IF NOT EXISTS(SELECT 1 FROM po_entityfields WHERE page = 1 AND field = 'main_id')
    BEGIN
        INSERT po_entityfields (page, field, name)
        VALUES (1, 'main_id', 'Main company')
    END
GO
IF NOT EXISTS(SELECT 1 FROM po_entityfields WHERE page = 1 AND field = 'description')
    BEGIN
        INSERT po_entityfields (page, field, name)
        VALUES (1, 'description', 'Description')
    END
GO
IF NOT EXISTS(SELECT 1 FROM po_entityfields WHERE page = 1 AND field = 'suburb')
    BEGIN
        INSERT po_entityfields (page, field, name)
        VALUES (1, 'suburb', 'Suburb');
    END
GO
IF NOT EXISTS(SELECT 1 FROM po_entityfields WHERE page = 1 AND field = 'isarchive')
    BEGIN
        INSERT po_entityfields (page, field, name)
        VALUES (1, 'isarchive', 'Archivation reason');
    END
GO

IF NOT EXISTS(SELECT 1 FROM po_entityfields WHERE page = 1 AND field = 'tax_number')
    BEGIN
        INSERT po_entityfields (page, field, name)
        VALUES (1, 'tax_number', 'TAX number');
    END
GO


IF NOT EXISTS(SELECT 1 FROM po_entityfields WHERE page = 1 AND field = 'morionid')
    BEGIN
        INSERT po_entityfields (page, field, name)
        VALUES (1, 'morionid', 'Account MDM ID');
    END
GO
IF NOT EXISTS(SELECT 1 FROM po_entityfields WHERE page = 2 AND field = 'description')
    BEGIN
        INSERT po_entityfields (page, field, name)
        VALUES (2, 'description', 'Description');
    END
GO
IF NOT EXISTS(SELECT 1 FROM po_entityfields WHERE page = 2 AND field = 'isarchive')
    BEGIN
        INSERT po_entityfields (page, field, name)
        VALUES (2, 'isarchive', 'Archivation reason');
    END
GO
IF NOT EXISTS(SELECT 1 FROM po_entityfields WHERE page = 2 AND field = 'case_number')
    BEGIN
        INSERT po_entityfields (page, field, name)
        VALUES (2, 'case_number', 'Case number');
    END
GO
IF NOT EXISTS(SELECT 1 FROM po_entityfields WHERE page = 2 AND field = 'morionid')
    BEGIN
        INSERT po_entityfields (page, field, name)
        VALUES (2, 'morionid', 'Account MDM ID');
    END
GO
IF NOT EXISTS(SELECT 1 FROM po_entityfields WHERE page = 2 AND field = 'info_contact.categoryprep')
    BEGIN
        INSERT po_entityfields (page, field, name)
        VALUES (2, 'info_contact.categoryprep', 'Auto Category');
    END
GO
IF NOT EXISTS(SELECT 1 FROM po_entityfields WHERE page = 2 AND field = 'sip_call')
    BEGIN
        INSERT po_entityfields (page, field, name)
        VALUES (2, 'sip_call', 'Hide sip telephony call button');
    END
GO
IF NOT EXISTS(SELECT 1 FROM po_entityfields WHERE page = 2 AND field = 'okrug_id')
    BEGIN
        INSERT po_entityfields (page, field, name)
        VALUES (2, 'okrug_id', 'District');
    END
GO
IF NOT EXISTS(SELECT 1 FROM po_entityfields WHERE page = 2 AND field = 'signature')
    BEGIN
        INSERT po_entityfields (page, field, name)
        VALUES (2, 'signature', 'Signature');
    END
GO

IF NOT EXISTS(SELECT 1 FROM po_entityfields WHERE page = 2 AND field = 'info_contactconnection.admin_slave_id')
    BEGIN
        INSERT po_entityfields (page, field, name)
        VALUES (2, 'info_contactconnection.admin_slave_id', 'Administrative connections');
    END
GO
IF NOT EXISTS(SELECT 1 FROM po_entityfields WHERE page = 2 AND field = 'info_contactconnection.social_slave_id')
    BEGIN
        INSERT po_entityfields (page, field, name)
        VALUES (2, 'info_contactconnection.social_slave_id', 'Social connections');
    END
GO
IF NOT EXISTS(SELECT 1 FROM po_entityfields WHERE page = 2 AND field = 'info_contactconnection.company_slave_id')
    BEGIN
        INSERT po_entityfields (page, field, name)
        VALUES (2, 'info_contactconnection.company_slave_id', 'Prescriber Pharmacy');
    END
GO
IF NOT EXISTS(SELECT 1 FROM po_entityfields WHERE page = 4 AND field = 'training')
    BEGIN
        INSERT po_entityfields (page, field, name)
        VALUES (4, 'training', 'Блок тренингов');
    END
GO
IF NOT EXISTS(SELECT 1 FROM po_entityfields WHERE page = 4 AND field = 'field_training_members')
    BEGIN
        INSERT po_entityfields (page, field, name)
        VALUES (4, 'field_training_members', 'Training block');
    END
GO
IF NOT EXISTS(SELECT 1 FROM po_entityfields WHERE page = 4 AND field = 'field_training_member')
    BEGIN
        INSERT po_entityfields (page, field, name)
        VALUES (4, 'field_training_member', 'Training participant');
    END
GO
IF NOT EXISTS(SELECT 1 FROM po_entityfields WHERE page = 4 AND field = 'field_training_evaluation_form_pts')
    BEGIN
        INSERT po_entityfields (page, field, name)
        VALUES (4, 'field_training_evaluation_form_pts', 'TCP assessment form');
    END
GO
IF NOT EXISTS(SELECT 1 FROM po_entityfields WHERE page = 4 AND field = 'countpart')
    BEGIN
        INSERT po_entityfields (page, field, name)
        VALUES (4, 'countpart', 'Number of participants');
    END
GO
IF NOT EXISTS(SELECT 1 FROM po_entityfields WHERE page = 4 AND field = 'info_promoproject.projects')
    BEGIN
        INSERT po_entityfields (page, field, name)
        VALUES (4, 'info_promoproject.projects', 'Projects');
    END
GO
IF NOT EXISTS(SELECT 1 FROM po_entityfields WHERE page = 4 AND field = 'info_promoproject.action')
    BEGIN
        INSERT po_entityfields (page, field, name)
        VALUES (4, 'info_promoproject.action', 'Special Deal');
    END
GO
IF NOT EXISTS(SELECT 1 FROM po_entityfields WHERE page = 4 AND field = 'info_taskcomment.call')
    BEGIN
        INSERT po_entityfields (page, field, name)
        VALUES (4, 'info_taskcomment.call', 'General comment');
    END
GO
IF NOT EXISTS(SELECT 1 FROM po_entityfields WHERE page = 4 AND field = 'taskresult')
    BEGIN
        INSERT po_entityfields (page, field, name)
        VALUES (4, 'taskresult', 'Task result');
    END
GO
IF NOT EXISTS(SELECT 1 FROM po_entityfields WHERE page = 4 AND field = 'previous_task_result')
    BEGIN
        INSERT po_entityfields (page, field, name)
        VALUES (4, 'previous_task_result', 'Previous task result');
    END
GO
IF NOT EXISTS(SELECT 1 FROM po_entityfields WHERE page = 4 AND field = 'tasksubjects')
    BEGIN
        INSERT po_entityfields (page, field, name)
        VALUES (4, 'tasksubjects', 'Task subjects');
    END
GO
IF NOT EXISTS(SELECT 1 FROM po_entityfields WHERE page = 4 AND field = 'task_surveys_by_inn')
    BEGIN
        INSERT po_entityfields (page, field, name)
        VALUES (4, 'task_surveys_by_inn', 'INN surveys');
    END
GO
IF NOT EXISTS(SELECT 1 FROM po_entityfields WHERE page = 4 AND field = 'info_taskrcpa')
    BEGIN
        INSERT po_entityfields (page, field, name)
        VALUES (4, 'info_taskrcpa', 'RCPA');
    END
GO
IF NOT EXISTS(SELECT 1 FROM po_entityfields WHERE page = 4 AND field = 'info_task.contact_code')
    BEGIN
        INSERT po_entityfields (page, field, name)
        VALUES (4, 'info_task.contact_code', 'QR Promo');
    END
GO
IF NOT EXISTS(SELECT 1 FROM po_entityfields WHERE page = 4 AND field = 'info_mcmdistributionaction.webinar')
    BEGIN
        INSERT po_entityfields (page, field, name)
        VALUES (4, 'info_mcmdistributionaction.webinar', 'Invitation Type');
    END
GO
IF NOT EXISTS(SELECT 1 FROM po_entityfields WHERE page = 4 AND field = 'sip_call')
    BEGIN
        INSERT po_entityfields (page, field, name)
        VALUES (4, 'sip_call', 'Hide sip telephony call button');
    END
GO
IF NOT EXISTS(SELECT 1 FROM po_entityfields WHERE page = 8 AND field = 'info_user.medresp')
    BEGIN
        INSERT po_entityfields (page, field, name)
        VALUES (8, 'info_user.medresp', 'Medical representative');
    END
GO
IF NOT EXISTS(SELECT 1 FROM po_entityfields WHERE page = 8 AND field = 'info_actionparamtext.devmap')
    BEGIN
        INSERT po_entityfields (page, field, name)
        VALUES (8, 'info_actionparamtext.devmap', 'Development map');
    END
GO
IF EXISTS(SELECT 1 FROM po_entityfields WHERE page = 4 AND field = 'pervostolniki')
    BEGIN
        UPDATE po_entityfields
        SET field = 'info_contactpotential.pervostolniki'
        WHERE page = 4 AND field = 'pervostolniki'
    END
GO
IF EXISTS(SELECT 1 FROM po_entityfields WHERE page = 4 AND field = 'promocontrol')
    BEGIN
        UPDATE po_entityfields
        SET field = 'info_companypromo'
        WHERE page = 4 AND field = 'promocontrol'
    END
GO
IF EXISTS(SELECT 1 FROM po_entityfields WHERE page = 4 AND field = 'projects')
    BEGIN
        UPDATE po_entityfields
        SET field = 'info_promoproject.projects'
        WHERE page = 4 AND field = 'projects'
    END
GO
IF EXISTS(SELECT 1 FROM po_entityfields WHERE page = 4 AND field = 'action')
    BEGIN
        UPDATE po_entityfields
        SET field = 'info_promoproject.action'
        WHERE page = 4 AND field = 'action'
    END
GO

IF EXISTS(SELECT 1 FROM po_entityfields WHERE page = 4 AND field = 'tenderplan')
    BEGIN
        UPDATE po_entityfields
        SET field = 'info_tasktenderplan.tenderplan'
        WHERE page = 4 AND field = 'tenderplan'
    END
GO
IF EXISTS(SELECT 1 FROM po_entityfields WHERE page = 4 AND field = 'tenderfact')
    BEGIN
        UPDATE po_entityfields
        SET field = 'info_tasktenderfact.tenderfact'
        WHERE page = 4  AND field = 'tenderfact'
    END
GO
IF EXISTS(SELECT 1 FROM po_entityfields WHERE page = 4 AND field = 'planfactkam')
    BEGIN
        UPDATE po_entityfields
        SET field = 'info_kamplan2.planfactkam'
        WHERE page = 4 AND field = 'planfactkam'
    END
GO
IF EXISTS(SELECT 1 FROM po_entityfields WHERE page = 4 AND field = 'company_goals')
    BEGIN
        UPDATE po_entityfields
        SET field = 'info_goal.company_goals'
        WHERE page = 4 AND field = 'company_goals'
    END
GO
IF EXISTS(SELECT 1 FROM po_entityfields WHERE page = 4 AND field = 'marketingproject')
    BEGIN
        UPDATE po_entityfields
        SET field = 'info_promoproject.marketingProject'
        WHERE page = 4 AND field = 'marketingproject'
    END
    GO

IF EXISTS(SELECT 1 FROM po_entityfields WHERE page = 4 AND field = 'visitGoals')
    BEGIN
        UPDATE po_entityfields
        SET field = 'info_paramfortask.visitGoals'
        WHERE page = 4 AND field = 'visitGoals'
    END
GO
IF EXISTS(SELECT 1 FROM po_entityfields WHERE page = 8 AND field = 'brand_id')
    BEGIN
        UPDATE po_entityfields
        SET field = 'brends_id'
        WHERE page = 8 AND field = 'brand_id'
    END
GO
IF EXISTS(SELECT 1 FROM po_entityfields WHERE page = 8 AND field = 'brends_id')
    BEGIN
        UPDATE po_entityfields
        SET field = 'info_promomaterial'
        WHERE page = 8 AND field = 'brends_id'
    END
GO
--------------------------------------------------------------------------------------------------
-- Add translations
--------------------------------------------------------------------------------------------------
exec insertLanguageInterface 'en', 'Status change date', N'Status change date', 'crm'
exec insertLanguageInterface 'ru', 'Status change date', N'Дата изменения статуса', 'crm'
exec insertLanguageInterface 'uk', 'Status change date', N'Дата зміни статусу', 'crm'
GO
---------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------

---------------------------------------------------------------------------------------------------
-- https://teamsoft.atlassian.net/browse/PHARMAWEB-335
-- Добавить миграции для поп. полей
---------------------------------------------------------------------------------------------------
IF NOT EXISTS (SELECT 1 FROM info_addinfotasktype WHERE role_id is not null)
BEGIN
	insert into info_addinfotasktype (addinfotype_id, subjtype_id, role_id)
	select info_addinfotypebyrole.addinfotype_id, info_addinfotasktype.subjtype_id, info_addinfotypebyrole.role_id
	from info_addinfotasktype
	inner join info_addinfotypebyrole on info_addinfotasktype.addinfotype_id = info_addinfotypebyrole.addinfotype_id
		delete from info_addinfotasktype where role_id is null
END
IF NOT EXISTS (SELECT 1 FROM info_addinfocompanytype WHERE role_id is not null)
BEGIN
	insert into info_addinfocompanytype (addinfotype_id, subjtype_id, role_id)
	select info_addinfotypebyrole.addinfotype_id, info_addinfocompanytype.subjtype_id, info_addinfotypebyrole.role_id
	from info_addinfocompanytype
	inner join info_addinfotypebyrole on info_addinfocompanytype.addinfotype_id = info_addinfotypebyrole.addinfotype_id
	delete from info_addinfocompanytype where role_id is null
END
IF NOT EXISTS (SELECT 1 FROM info_addinfocontacttype WHERE role_id is not null)
BEGIN
	insert into info_addinfocontacttype (addinfotype_id, subjtype_id, role_id)
	select info_addinfotypebyrole.addinfotype_id, info_addinfocontacttype.subjtype_id, info_addinfotypebyrole.role_id
	from info_addinfocontacttype
	inner join info_addinfotypebyrole on info_addinfocontacttype.addinfotype_id = info_addinfotypebyrole.addinfotype_id
	delete from info_addinfocontacttype where role_id is null
END
IF NOT EXISTS (SELECT 1 FROM info_addinfoactiontype WHERE role_id is not null)
BEGIN
	insert into info_addinfoactiontype (addinfotype_id, subjtype_id, role_id)
	select info_addinfotypebyrole.addinfotype_id, info_addinfoactiontype.subjtype_id, info_addinfotypebyrole.role_id
	from info_addinfoactiontype
	inner join info_addinfotypebyrole on info_addinfoactiontype.addinfotype_id = info_addinfotypebyrole.addinfotype_id
	delete from info_addinfoactiontype where role_id is null
END
GO
---------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------

-- https://teamsoft.atlassian.net/browse/MCM-333
---------------------------------------------------------------------------------------------------------
IF NOT EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'info_user' AND Column_Name = 'webinarlink')
    BEGIN
        ALTER TABLE info_user
            ADD webinarlink VARCHAR(255) NULL
    END
GO

IF NOT EXISTS (SELECT 1 FROM info_mcmcontentvariable WHERE code = 'WEBINARLINK')
    BEGIN
        INSERT INTO info_mcmcontentvariable(name, code) VALUES('Ссылка на вебинар', 'WEBINARLINK')
    END
GO
---------------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------------