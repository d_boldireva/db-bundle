----------------------------------------------------------------------------------------------------
-- https://teamsoft.atlassian.net/browse/GRINDEXCRM-27
----------------------------------------------------------------------------------------------------
IF NOT EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE table_name = 'info_country' AND column_name = 'colorhex')
    ALTER TABLE [dbo].[info_country] ADD colorhex varchar(50) NULL
GO
----------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------
-- https://teamsoft.atlassian.net/browse/DRREDDYSMM-92
----------------------------------------------------------------------------------------------------
IF NOT EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE table_name = 'info_companycategory' AND column_name = 'targeting_cnt')
    ALTER TABLE [dbo].[info_companycategory] ADD targeting_cnt int NULL
GO

IF NOT EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE table_name = 'info_plancompanytask' AND column_name = 'category_id')
    BEGIN
        ALTER TABLE [dbo].[info_plancompanytask] ADD category_id int NULL
        ALTER TABLE [dbo].[info_plancompanytask] WITH CHECK
        ADD CONSTRAINT [fk_info_plancompanytask_category_id] FOREIGN KEY ([category_id]) REFERENCES [dbo].[info_companycategory]([id])
    END
GO

IF NOT EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE table_name = 'info_contactcateg' AND column_name = 'targeting_cnt')
    ALTER TABLE [dbo].[info_contactcateg] ADD targeting_cnt int NULL
GO

IF NOT EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE table_name = 'info_plancontacttask' AND column_name = 'category_id')
    BEGIN
        ALTER TABLE [dbo].[info_plancontacttask] ADD category_id int NULL
        ALTER TABLE [dbo].[info_plancontacttask] WITH CHECK
        ADD CONSTRAINT [fk_info_plancontacttask_category_id] FOREIGN KEY ([category_id]) REFERENCES [dbo].[info_contactcateg]([id])
    END
GO
    ----------------------------------------------------------------------------------------------------

----------------------------------------------------------------------------------------------------
-- https://teamsoft.atlassian.net/browse/SINTEZ-52
----------------------------------------------------------------------------------------------------
IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'po_photoreportfilter' AND COLUMN_NAME = 'additional_filter') BEGIN
    ALTER TABLE po_photoreportfilter ADD additional_filter varchar(max) NULL
END
GO
----------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------

----------------------------------------------------------------------------------------------------
-- https://teamsoft.atlassian.net/browse/FARMAK-327
----------------------------------------------------------------------------------------------------
exec insertLanguageInterface 'en', 'Subdirections', N'Subdirections', 'messages'
exec insertLanguageInterface 'ru', 'Subdirections', N'Субпродуктовые направления', 'messages'
exec insertLanguageInterface 'uk', 'Subdirections', N'Субпродуктові напрямки', 'messages'
GO
----------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------
---https://teamsoft.atlassian.net/browse/VEKTORRU-112
------------------------------------------------------------------------------------------------------------------------
IF OBJECT_ID(N'info_service', 'U') IS NOT NULL AND NOT EXISTS(
    SELECT 1
    FROM info_service
    WHERE identifier = 'car_report'
)
BEGIN
    insert info_service (identifier, name, enable)
    values ('car_report', 'Travel sheets', 1)
END
GO

IF OBJECT_ID(N'po_webservice', 'U') IS NOT NULL AND NOT EXISTS(
    SELECT 1
    FROM po_webservice
    WHERE identifier = 'car_report'
) AND EXISTS(
    SELECT 1 FROM info_service WHERE identifier = 'car_report'
)
BEGIN
    declare @SERVICE_ID int
    set @SERVICE_ID = (SELECT id FROM info_service WHERE identifier = 'car_report')

    insert po_webservice (identifier, enable, route, allow_if, url, name, service_id)
    values ('car_report', 1, 'team_soft_car_report_homepage', 'true', '/{_locale}/car-report', 'Travel sheets', @SERVICE_ID)
END
GO

IF OBJECT_ID(N'info_serviceprivilege', 'U') IS NOT NULL AND NOT EXISTS(SELECT 1
    FROM info_serviceprivilege
    WHERE code = 'ROLE_ACCESS_TO_CAR_REPORT'
) AND EXISTS(
    SELECT 1
    FROM info_service
    WHERE identifier = 'car_report'
)
BEGIN
    declare @SERVICE_ID int
    declare @SERVICE_PRIVILEGE_ID int
    declare @ROLE_ID int
    set @SERVICE_ID = (SELECT id FROM info_service WHERE identifier = 'car_report')
    set @ROLE_ID = (SELECT id FROM info_role WHERE code = 'ROLE_SUPER_ADMIN')

    insert info_serviceprivilege (name, code, service_id)
    values ('Access to Travel sheets', 'ROLE_ACCESS_TO_CAR_REPORT', @SERVICE_ID)

    set @SERVICE_PRIVILEGE_ID = (SELECT id FROM info_serviceprivilege WHERE code = 'ROLE_ACCESS_TO_CAR_REPORT')
    insert info_roleprivilege (privilege_id, role_id) values (@SERVICE_PRIVILEGE_ID, @ROLE_ID)
END
GO

exec insertLanguageInterface 'en', 'Access to Travel sheets', N'Access to Travel sheets', 'messages'
exec insertLanguageInterface 'ru', 'Access to Travel sheets', N'Доступ к путевым листам', 'messages'
exec insertLanguageInterface 'uk', 'Access to Travel sheets', N'Доступ до подорожнім листам', 'messages'
GO

IF OBJECT_ID(N'info_car', 'U') IS NULL
BEGIN
    -- auto-generated definition
create table info_car
(
    id               int identity
        constraint PK_info_car
        primary key,
    currenttime      datetime
        constraint DF_info_carcurrenttime default getdate(),
    servertime      datetime
        constraint DF_info_carservertime default getdate(),
    time             timestamp null,
    moduser          varchar(16)
        constraint DF_info_carmoduser default [dbo].[Get_CurrentCode](),
    guid             uniqueidentifier
        constraint DF_info_carguid default newid(),
    num              varchar(16),
    car_name         varchar(255),
    fuel_norma       money,
    createdate       datetime
        constraint DF_info_carcreatedate default getdate(),
    leasing_datetill datetime
)

create index ix_info_car_guid on info_car (guid)
END
GO

IF OBJECT_ID(N'info_carreport', 'U') IS NULL
BEGIN
    -- auto-generated definition
create table info_carreport
(
    id              int identity
        constraint PK_info_carreport
        primary key,
    currenttime     datetime
        constraint DF_info_carreportcurrenttime default getdate(),
    servertime      datetime
        constraint DF_info_carreportservertime default getdate(),
    time            timestamp null,
    moduser         varchar(16)
        constraint DF_info_carreportmoduser default [dbo].[Get_CurrentCode](),
    guid            uniqueidentifier
        constraint DF_info_carreportguid default newid(),
    user_id         int
        constraint FK_info_carreport_user_id
            references info_user,
    odometrvalue    int,
    fuel_rest       int,
    car_id          int
        constraint FK_info_carreport_car_id
            references info_car,
    real_date       datetime,
    date            datetime,
    dt_start        datetime,
    dt_finish       datetime,
    odometer_start  int,
    odometer_finish int,
    refueled        money,
    refueledbycard  money,
    fuel_start      money,
    fuel_finish     money,
    isclosed        int
)

create index ix_info_carreport_guid
        on info_carreport (guid)

    create index ix_info_carreport_user_id
        on info_carreport (user_id)

    create index ix_info_carreport_car_id
        on info_carreport (car_id)
END
GO

IF NOT EXISTS(
    SELECT *
    FROM sys.indexes
    WHERE name = 'ix_info_carreport_date'
    AND object_id = OBJECT_ID('info_carreport', 'U')
) AND OBJECT_ID(N'info_carreport', 'U') IS NOT NULL
BEGIN
    create index ix_info_carreport_date on info_carreport (date)
END
GO

IF OBJECT_ID(N'info_carreportsheet', 'U') IS NULL
BEGIN
    -- auto-generated definition
create table info_carreportsheet
(
    id              int identity
        constraint PK_info_carreportsheet
        primary key,
    currenttime     datetime
        constraint DF_info_carreportsheettime default getdate(),
    moduser         varchar(16)
        constraint DF_info_carreportsheetmoduser default [dbo].[Get_CurrentCode](),
    guid            uniqueidentifier
        constraint DF_info_carreportsheetguid default newid(),
    createdate      datetime
        constraint DF_info_carreportsheetcreatedate default getdate(),
    odometer_start  int,
    odometer_finish int,
    fuel_start      money,
    fuel_finish     money,
    refueled        money,
    month           date,
    user_id         int
        constraint FK_info_carreportsheet_user_id
            references info_user,
)

create index ix_info_carreportsheet_guid
        on info_carreportsheet (guid)


    create index ix_info_carreportsheet_month
        on info_carreportsheet (month)

ALTER TABLE info_carreportsheet
    ADD CONSTRAINT uq_info_carreportsheet_month_user_id UNIQUE (month, user_id)
END
GO

IF OBJECT_ID(N'info_carreportsheethistory', 'U') IS NULL
BEGIN
    -- auto-generated definition
create table info_carreportsheethistory
(
    id              int identity
        constraint PK_info_carreportsheethistory
        primary key,
    currenttime     datetime
        constraint DF_info_carreportsheethistorycurrenttime default getdate(),
    moduser         varchar(16)
        constraint DF_info_carreportsheethistorymoduser default [dbo].[Get_CurrentCode](),
    guid            uniqueidentifier
        constraint DF_info_carreportsheethistoryguid default newid(),
    createdate      datetime
        constraint DF_info_carreportsheethistorycreatedate default getdate(),
    odometer_start  int,
    odometer_finish int,
    fuel_start      money,
    fuel_finish     money,
    refueled        money,
    month           date,
    user_id         int,
    sheet_id        int
)

create index ix_info_carreportsheethistory_guid
        on info_carreportsheethistory (guid)

    create index ix_info_carreportsheethistory_month
        on info_carreportsheethistory (month)

    create index ix_info_carreportsheethistory_user_id
        on info_carreportsheethistory (user_id)

    create index ix_info_carreportsheethistory_sheet_id
        on info_carreportsheethistory (sheet_id)
END
GO

IF OBJECT_ID(N'info_carreportsheeteditable', 'U') IS NULL
BEGIN
    -- auto-generated definition
create table info_carreportsheeteditable
(
    id           int identity
        constraint PK_info_carreportsheeteditable
        primary key,
    currenttime  datetime
        constraint DF_info_carreportsheeteditabletime default getdate(),
    moduser      varchar(16)
        constraint DF_info_carreportsheeteditablemoduser default [dbo].[Get_CurrentCode](),
    guid         uniqueidentifier
        constraint DF_info_carreportsheeteditableguid default newid(),
    createdate   datetime
        constraint DF_info_carreportsheeteditablecreatedate default getdate(),
    month        date,
    user_id      int
        constraint FK_info_carreportsheeteditable_user_id
            references info_user,
    period_start date,
    period_end   date
)

create index ix_info_carreportsheeteditable_guid
        on info_carreportsheeteditable (guid)

    create index ix_info_carreportsheeteditable_month
        on info_carreportsheeteditable (month)

ALTER TABLE info_carreportsheeteditable
    ADD CONSTRAINT uq_info_carreportsheeteditable_month_user_id UNIQUE (month, user_id)
END
GO

IF OBJECT_ID(N'info_carmodel', 'U') is NULL
BEGIN
        -- auto-generated definition
create table info_carmodel
(
    id                   int identity
        constraint PK_info_carmodel
        primary key,
    model                varchar(255),
    fuelconsumption      money,
    carmaker_id          int
        constraint FK_info_carmodel_carmaker_id
            references info_dictionary,
    guid                 uniqueidentifier
        constraint DF_info_carmodelguid default newid(),
    time                 timestamp null,
    currenttime          datetime
        constraint DF_info_carmodelcurrenttime default getdate(),
    moduser              varchar(16)
        constraint DF_info_carmodelmoduser default [dbo].[Get_CurrentCode](),
    fuelconsumptiontrack money,
    fuel_norma_in_summer money,
    fuel_norma_in_winter money
)

    create index ix_info_carmodel_guid on info_carmodel (guid)
    create index ix_info_carmodel_carmaker_id on info_carmodel (carmaker_id)
END
GO

IF NOT EXISTS(
    SELECT 1
    FROM INFORMATION_SCHEMA.COLUMNS
    WHERE table_name = 'info_user'
    AND column_name = 'carmodel_id'
)
ALTER TABLE info_user
    ADD carmodel_id int
        constraint FK_info_user_carmodel_id references info_carmodel
    GO

IF NOT EXISTS(
    SELECT 1
    FROM INFORMATION_SCHEMA.COLUMNS
    WHERE table_name = 'info_user'
    AND column_name = 'carnum_id'
)
ALTER TABLE info_user
    ADD carnum_id int
        constraint FK_info_user_carnum_info_car references info_car
    GO

IF NOT EXISTS(
    SELECT *
    FROM sys.indexes
    WHERE name = 'ix_info_user_carmodel_id'
    AND object_id = OBJECT_ID('info_user', 'U')
)
BEGIN
    create index ix_info_user_carmodel_id on info_user (carmodel_id)
END
GO

IF NOT EXISTS(
    SELECT *
    FROM sys.indexes
    WHERE name = 'ix_info_user_carnum_id'
    AND object_id = OBJECT_ID('info_user', 'U')
)
BEGIN
    create index ix_info_user_carnum_id on info_user (carnum_id)
END
GO

IF NOT EXISTS(
    SELECT 1
    FROM info_serviceprivilege
    WHERE [code] = 'ROLE_ACCESS_TO_CAR_REPORT_ADMIN'
)
BEGIN
    DECLARE @privilegeAccessID int
    SET @privilegeAccessID = (SELECT MIN(id) FROM info_serviceprivilege WHERE code = 'ROLE_ACCESS_TO_CAR_REPORT')

    INSERT INTO info_serviceprivilege ([name], code, service_id, parent_id)
    VALUES ('Can to manage CAR_REPORT',
            'ROLE_ACCESS_TO_CAR_REPORT_ADMIN',
            (SELECT id FROM info_service WHERE identifier = 'car_report'),
            @privilegeAccessID)

    DECLARE @privilegeID int
    SET @privilegeID = (SELECT id FROM info_serviceprivilege WHERE code = 'ROLE_ACCESS_TO_CAR_REPORT_ADMIN')

    INSERT INTO info_roleprivilege (privilege_id, role_id)
SELECT @privilegeID AS privilege_id, rol.id AS role_id
FROM info_role as rol
WHERE rol.code IN ('ROLE_SUPER_ADMIN', 'ROLE_ADMIN')

    INSERT INTO info_roleprivilege (privilege_id, role_id)
SELECT @privilegeAccessID AS privilege_id, rol.id AS role_id
FROM info_role as rol
WHERE rol.code IN ('ROLE_SUPER_ADMIN', 'ROLE_ADMIN')
END
GO

IF NOT EXISTS(
    SELECT 1
    FROM info_serviceprivilege
    WHERE [code] = 'ROLE_ACCESS_TO_CAR_REPORT_USER'
)
BEGIN
INSERT INTO info_serviceprivilege ([name], code, service_id, parent_id)
VALUES ('Can write own travel sheets (car-report)',
        'ROLE_ACCESS_TO_CAR_REPORT_USER',
        (SELECT id FROM info_service WHERE identifier = 'car_report'),
        (SELECT MIN(id) FROM info_serviceprivilege WHERE code = 'ROLE_ACCESS_TO_CAR_REPORT'))

DECLARE @privilegeID int
    SET @privilegeID = (SELECT id FROM info_serviceprivilege WHERE code = 'ROLE_ACCESS_TO_CAR_REPORT_USER')

    INSERT INTO info_roleprivilege (privilege_id, role_id)
SELECT DISTINCT @privilegeID AS privilege_id, rol.role_id AS role_id
FROM info_user as rol
WHERE rol.carnum_id is not null
  AND ISNULL(rol.isnowork, 0) = 0
END
GO

IF NOT EXISTS(
    SELECT 1
    FROM INFORMATION_SCHEMA.COLUMNS
    WHERE table_name = 'info_carreport'
    AND column_name = 'car_report_sheet_id'
)
ALTER TABLE info_carreport
    ADD car_report_sheet_id int
        constraint fk_info_carreport_car_report_sheet_idd references info_carreportsheet
    GO

IF NOT EXISTS(
    SELECT 1
    FROM INFORMATION_SCHEMA.COLUMNS
    WHERE table_name = 'info_carreport'
    AND column_name = 'skip_report'
)
ALTER TABLE info_carreport ADD skip_report int
    GO

IF OBJECT_ID(N'info_carreportsheetfile', 'U') IS NULL
BEGIN
    -- auto-generated definition
create table info_carreportsheetfile
(
    id                  int identity
        constraint PK_info_carreportsheetfile
        primary key,
    content             image,
    car_report_sheet_id int
        constraint FK_info_car_report_sheet_id
            references info_carreportsheet,
    guid                uniqueidentifier
        constraint DF_info_carreportsheetfileguid default newid(),
    currenttime         datetime
        constraint DF_info_carreportsheetfilecurrenttime default getdate(),
    moduser             varchar(16)
        constraint DF_info_carreportsheetfilemoduser default [dbo].[Get_CurrentCode](),
)
END
GO

IF NOT EXISTS(
    SELECT 1
    FROM INFORMATION_SCHEMA.COLUMNS
    WHERE table_name = 'info_carmodel'
    AND column_name = 'fuel_norma_in_summer'
)
ALTER TABLE info_carmodel ADD fuel_norma_in_summer money
    GO

IF NOT EXISTS(
    SELECT 1
    FROM INFORMATION_SCHEMA.COLUMNS
    WHERE table_name = 'info_carmodel'
    AND column_name = 'fuel_norma_in_winter'
)
ALTER TABLE info_carmodel ADD fuel_norma_in_winter money
    GO


IF NOT EXISTS(
    SELECT *
    FROM info_carmodel
    WHERE model = N'Мерседес Бенц GLS 350 D'
)
    INSERT INTO info_carmodel (model, fuel_norma_in_summer, fuel_norma_in_winter)
    VALUES (N'Мерседес Бенц GLS 350 D', 12.780, 13.680);
GO

IF NOT EXISTS(
    SELECT *
    FROM info_carmodel
    WHERE model = N'Рено Сандеро АКПП 1,6 102 л.с.'
)
    INSERT INTO info_carmodel (model, fuel_norma_in_summer, fuel_norma_in_winter)
    VALUES (N'Рено Сандеро АКПП 1,6 102 л.с.', 12.804, 13.774);
GO

IF NOT EXISTS(
    SELECT *
    FROM info_carmodel
    WHERE model = N'Рено Сандеро МКПП 1,4 75 л.с.'
)
    INSERT INTO info_carmodel (model, fuel_norma_in_summer, fuel_norma_in_winter)
    VALUES (N'Рено Сандеро МКПП 1,4 75 л.с.', 9.372, 9.585);
GO

IF NOT EXISTS(
    SELECT *
    FROM info_carmodel
    WHERE model = N'Рено Сандеро МКПП 1,6 102 л.с.'
)
    INSERT INTO info_carmodel (model, fuel_norma_in_summer, fuel_norma_in_winter)
    VALUES (N'Рено Сандеро МКПП 1,6 102 л.с.', 10.824, 11.644);
GO

IF NOT EXISTS(
    SELECT *
    FROM info_carmodel
    WHERE model = N'Рено Сандеро МКПП 1,6 84 л.с.'
)
    INSERT INTO info_carmodel (model, fuel_norma_in_summer, fuel_norma_in_winter)
    VALUES (N'Рено Сандеро МКПП 1,6 84 л.с.', 10.428, 11.218);
GO

IF NOT EXISTS(
    SELECT *
    FROM info_carmodel
    WHERE model = N'Рено Флюэнс АКПП 1,6 114 л.с.'
)
    INSERT INTO info_carmodel (model, fuel_norma_in_summer, fuel_norma_in_winter)
    VALUES (N'Рено Флюэнс АКПП 1,6 114 л.с.', 10.012, 12.922);
GO

IF NOT EXISTS(
    SELECT *
    FROM info_carmodel
    WHERE model = N'Шкода Октавия АКПП 1,6 110 л.с.'
)
    INSERT INTO info_carmodel (model, fuel_norma_in_summer, fuel_norma_in_winter)
    VALUES (N'Шкода Октавия АКПП 1,6 110 л.с.', 11.220, 11.470);
GO

IF NOT EXISTS(
    SELECT 1
    FROM INFORMATION_SCHEMA.COLUMNS
    WHERE table_name = 'info_car'
    AND column_name = 'model_id'
)
BEGIN
    ALTER TABLE info_car ADD model_id int
    ALTER TABLE info_car ADD CONSTRAINT fr_info_car_model_id FOREIGN KEY (model_id) REFERENCES info_carmodel (id)
END
GO

IF NOT EXISTS(SELECT 1 FROM info_car WHERE model_id IS NOT NULL)
    BEGIN
        UPDATE car SET model_id = carmodel.id
        FROM info_car AS car
                 JOIN info_carmodel AS carmodel ON car.car_name = carmodel.model
        WHERE carmodel.id IS NOT NULL
    END
GO

exec insertLanguageInterface 'en', 'Can write own travel sheets (car-report)', N'Can fill in own waybills (auto report)', 'messages'
exec insertLanguageInterface 'ru', 'Can write own travel sheets (car-report)', N'Может заполнять собственные путевые листы (автоотчет)', 'messages'
exec insertLanguageInterface 'uk', 'Can write own travel sheets (car-report)', N'Може заповнювати власні подорожні листи (автозвіт)', 'messages'
GO

exec insertLanguageInterface 'en', 'Can to manage CAR_REPORT', N'Admin panel access (auto report)', 'messages'
exec insertLanguageInterface 'ru', 'Can to manage CAR_REPORT', N'Доступ к админ-панели (автоотчет)', 'messages'
exec insertLanguageInterface 'uk', 'Can to manage CAR_REPORT', N'Доступ до адмін-панелі (автозвіт)', 'messages'
GO

exec insertLanguageInterface 'en', 'Travel sheets', N'Travel sheets', 'messages'
exec insertLanguageInterface 'ru', 'Travel sheets', N'Путевые листы', 'messages'
exec insertLanguageInterface 'uk', 'Travel sheets', N'Проїзні листи', 'messages'
GO

exec insertLanguageInterface 'en', 'Choose a user', N'Choose a user', 'car-report'
exec insertLanguageInterface 'ru', 'Choose a user', N'Выберите пользователя', 'car-report'
exec insertLanguageInterface 'uk', 'Choose a user', N'Виберіть користувача', 'car-report'
GO

exec insertLanguageInterface 'en', 'Choose a month', N'Choose a month', 'car-report'
exec insertLanguageInterface 'ru', 'Choose a month', N'Выберите месяц', 'car-report'
exec insertLanguageInterface 'uk', 'Choose a month', N'Виберіть місяць', 'car-report'
GO

exec insertLanguageInterface 'en', 'Search', N'Search', 'car-report'
exec insertLanguageInterface 'ru', 'Search', N'Поиск', 'car-report'
exec insertLanguageInterface 'uk', 'Search', N'Пошук', 'car-report'
GO

exec insertLanguageInterface 'en', 'Reports not found', N'Reports not found', 'car-report'
exec insertLanguageInterface 'ru', 'Reports not found', N'Отчеты не найдены', 'car-report'
exec insertLanguageInterface 'uk', 'Reports not found', N'Звітів не знайдено', 'car-report'
GO

exec insertLanguageInterface 'en', 'Departure, time', N'Departure, time', 'car-report'
exec insertLanguageInterface 'ru', 'Departure, time', N'Выезд, время', 'car-report'
exec insertLanguageInterface 'uk', 'Departure, time', N'Виїзд, час', 'car-report'
GO

exec insertLanguageInterface 'en', 'Return, time', N'Return, time', 'car-report'
exec insertLanguageInterface 'ru', 'Return, time', N'Возвращение, время', 'car-report'
exec insertLanguageInterface 'uk', 'Return, time', N'Повернення, час', 'car-report'
GO

exec insertLanguageInterface 'en', 'Owner', N'Owner', 'car-report'
exec insertLanguageInterface 'ru', 'Owner', N'Владелец', 'car-report'
exec insertLanguageInterface 'uk', 'Owner', N'Власник', 'car-report'
GO

exec insertLanguageInterface 'en', 'Download report (.pdf)', N'Download report (.pdf)', 'car-report'
exec insertLanguageInterface 'ru', 'Download report (.pdf)', N'Скачать отчет (.pdf)', 'car-report'
exec insertLanguageInterface 'uk', 'Download report (.pdf)', N'Завантажити звіт (.pdf)', 'car-report'
GO

exec insertLanguageInterface 'en', 'Compare indicators', N'Compare indicators', 'car-report'
exec insertLanguageInterface 'ru', 'Compare indicators', N'Сравнить индикаторы', 'car-report'
exec insertLanguageInterface 'uk', 'Compare indicators', N'Порівняти показники', 'car-report'
GO

exec insertLanguageInterface 'en', 'Load more sheets', N'Load more sheets', 'car-report'
exec insertLanguageInterface 'ru', 'Load more sheets', N'Загрузить больше листов', 'car-report'
exec insertLanguageInterface 'uk', 'Load more sheets', N'Завантажте більше аркушів', 'car-report'
GO

exec insertLanguageInterface 'en', 'Date', N'Date', 'car-report'
exec insertLanguageInterface 'ru', 'Date', N'Дата', 'car-report'
exec insertLanguageInterface 'uk', 'Date', N'Дата', 'car-report'
GO

exec insertLanguageInterface 'en', 'Without a report', N'Without a report', 'car-report'
exec insertLanguageInterface 'ru', 'Without a report', N'Без отчета', 'car-report'
exec insertLanguageInterface 'uk', 'Without a report', N'Без звіту', 'car-report'
GO

exec insertLanguageInterface 'en', 'Please, fill "Return, km"', N'Please, fill "Return, km"', 'car-report'
exec insertLanguageInterface 'ru', 'Please, fill "Return, km"', N'Заполните, пожалуйста, «Возвращение, км»', 'car-report'
exec insertLanguageInterface 'uk', 'Please, fill "Return, km"', N'Будь ласка, заповніть "Повернення, км"', 'car-report'
GO

exec insertLanguageInterface 'en', 'Field "Return, km" must be more then "Departure, km"', N'Field "Return, km" must be more then "Departure, km"', 'car-report'
exec insertLanguageInterface 'ru', 'Field "Return, km" must be more then "Departure, km"', N'Поле «Возвращение, км» должно быть больше, чем «Выезд, км».', 'car-report'
exec insertLanguageInterface 'uk', 'Field "Return, km" must be more then "Departure, km"', N'Поле "Повернення, км" має бути більше, ніж "Виїзд, км"', 'car-report'
GO

exec insertLanguageInterface 'en', 'Save', N'Save', 'car-report'
exec insertLanguageInterface 'ru', 'Save', N'Сохранить', 'car-report'
exec insertLanguageInterface 'uk', 'Save', N'Зберегти', 'car-report'
GO

exec insertLanguageInterface 'en', 'All months', N'All months', 'car-report'
exec insertLanguageInterface 'ru', 'All months', N'Все месяцы', 'car-report'
exec insertLanguageInterface 'uk', 'All months', N'Усі місяці', 'car-report'
GO

exec insertLanguageInterface 'en', 'All users', N'All users', 'car-report'
exec insertLanguageInterface 'ru', 'All users', N'Все пользователи', 'car-report'
exec insertLanguageInterface 'uk', 'All users', N'Всі користувачі', 'car-report'
GO

exec insertLanguageInterface 'en', 'This report not found', N'This report not found', 'car-report'
exec insertLanguageInterface 'ru', 'This report not found', N'Этот отчет не найден', 'car-report'
exec insertLanguageInterface 'uk', 'This report not found', N'Цей звіт не знайдено', 'car-report'
GO

exec insertLanguageInterface 'en', 'Load more expenses', N'Load more expenses', 'car-report'
exec insertLanguageInterface 'ru', 'Load more expenses', N'Загрузите больше расходов', 'car-report'
exec insertLanguageInterface 'uk', 'Load more expenses', N'Завантажте більше витрат', 'car-report'
GO

exec insertLanguageInterface 'en', 'Start of period', N'Start of period', 'car-report'
exec insertLanguageInterface 'ru', 'Start of period', N'Начало периода', 'car-report'
exec insertLanguageInterface 'uk', 'Start of period', N'Початок періоду', 'car-report'
GO

exec insertLanguageInterface 'en', 'This value should not be blank.', N'This value should not be blank.', 'car-report'
exec insertLanguageInterface 'ru', 'This value should not be blank.', N'Это значение не должно быть пустым.', 'car-report'
exec insertLanguageInterface 'uk', 'This value should not be blank.', N'Це значення не повинно бути порожнім.', 'car-report'
GO

exec insertLanguageInterface 'en', 'The date of period must be more then end of period', N'The date of period must be more then end of period', 'car-report'
exec insertLanguageInterface 'ru', 'The date of period must be more then end of period', N'Дата периода должна быть больше, чем конец периода', 'car-report'
exec insertLanguageInterface 'uk', 'The date of period must be more then end of period', N'Дата періоду повинна бути більше, ніж кінець періоду', 'car-report'
GO

exec insertLanguageInterface 'en', 'End of period', N'End of period', 'car-report'
exec insertLanguageInterface 'ru', 'End of period', N'Конец периода', 'car-report'
exec insertLanguageInterface 'uk', 'End of period', N'Кінець періоду', 'car-report'
GO

exec insertLanguageInterface 'en', 'The date of period must be less then start of period', N'The date of period must be less then start of period', 'car-report'
exec insertLanguageInterface 'ru', 'The date of period must be less then start of period', N'Дата периода должна быть меньше начала периода.', 'car-report'
exec insertLanguageInterface 'uk', 'The date of period must be less then start of period', N'Дата періоду повинна бути меншою за початок періоду', 'car-report'
GO

exec insertLanguageInterface 'en', 'Permission created', N'Permission created', 'car-report'
exec insertLanguageInterface 'ru', 'Permission created', N'Разрешение создано', 'car-report'
exec insertLanguageInterface 'uk', 'Permission created', N'Дозвіл створено', 'car-report'
GO

exec insertLanguageInterface 'en', 'Permission modified', N'Permission modified', 'car-report'
exec insertLanguageInterface 'ru', 'Permission modified', N'Разрешение изменено', 'car-report'
exec insertLanguageInterface 'uk', 'Permission modified', N'Дозвіл змінено', 'car-report'
GO

exec insertLanguageInterface 'en', 'Unexpected error. Contact to support.', N'Unexpected error. Contact to support.', 'car-report'
exec insertLanguageInterface 'ru', 'Unexpected error. Contact to support.', N'Непредвиденная ошибка. Обратитесь в службу поддержки.', 'car-report'
exec insertLanguageInterface 'uk', 'Unexpected error. Contact to support.', N'Неочікувана помилка. Зверніться до служби підтримки.', 'car-report'
GO

exec insertLanguageInterface 'en', 'Distance, km', N'Distance, km', 'car-report'
exec insertLanguageInterface 'ru', 'Distance, km', N'Расстояние, км', 'car-report'
exec insertLanguageInterface 'uk', 'Distance, km', N'Відстань, км', 'car-report'
GO

exec insertLanguageInterface 'en', 'Manage', N'Manage', 'car-report'
exec insertLanguageInterface 'ru', 'Manage', N'Управлять', 'car-report'
exec insertLanguageInterface 'uk', 'Manage', N'Управління', 'car-report'
GO

exec insertLanguageInterface 'en', 'Please, fill "Departure, km"', N'Please, fill "Departure, km"', 'car-report'
exec insertLanguageInterface 'ru', 'Please, fill "Departure, km"', N'Пожалуйста, заполните "Выезд, км"', 'car-report'
exec insertLanguageInterface 'uk', 'Please, fill "Departure, km"', N'Будь ласка, заповніть "Виїзд, км"', 'car-report'
GO

exec insertLanguageInterface 'en', 'Departure, km', N'Departure, km', 'car-report'
exec insertLanguageInterface 'ru', 'Departure, km', N'Выезд, км', 'car-report'
exec insertLanguageInterface 'uk', 'Departure, km', N'Виїзд, км', 'car-report'
GO

exec insertLanguageInterface 'en', 'Return, km', N'Return, km', 'car-report'
exec insertLanguageInterface 'ru', 'Return, km', N'Возвращение, км', 'car-report'
exec insertLanguageInterface 'uk', 'Return, km', N'Повернення, км', 'car-report'
GO

exec insertLanguageInterface 'en', 'Field "Departure, km" must be less then "Return, km"', N'Field "Departure, km" must be less then "Return, km"', 'car-report'
exec insertLanguageInterface 'ru', 'Field "Departure, km" must be less then "Return, km"', N'Поле «Выезд, км» должно быть меньше «Возвращение, км».', 'car-report'
exec insertLanguageInterface 'uk', 'Field "Departure, km" must be less then "Return, km"', N'Поле "Виїзд, км" має бути менше, ніж "Повернення, км"', 'car-report'
GO

exec insertLanguageInterface 'en', 'Field "Departure, km" must be more then 0', N'Field "Departure, km" must be more then 0', 'car-report'
exec insertLanguageInterface 'ru', 'Field "Departure, km" must be more then 0', N'Поле «Выезд, км» должно быть больше 0.', 'car-report'
exec insertLanguageInterface 'uk', 'Field "Departure, km" must be more then 0', N'Поле "Виїзд, км" має бути більше 0', 'car-report'
GO

exec insertLanguageInterface 'en', 'Please, fill "Return, km"', N'Please, fill "Return, km"', 'car-report'
exec insertLanguageInterface 'ru', 'Please, fill "Return, km"', N'Заполните, пожалуйста, «Возвращение, км»', 'car-report'
exec insertLanguageInterface 'uk', 'Please, fill "Return, km"', N'Будь ласка, заповніть "Повернення, км"', 'car-report'
GO

exec insertLanguageInterface 'en', 'Field "Return, km" must be more then 0', N'Field "Return, km" must be more then 0', 'car-report'
exec insertLanguageInterface 'ru', 'Field "Return, km" must be more then 0', N'Поле «Возвращение, км» должно быть больше 0', 'car-report'
exec insertLanguageInterface 'uk', 'Field "Return, km" must be more then 0', N'Поле "Повернення, км" має бути більше 0', 'car-report'
GO

exec insertLanguageInterface 'en', 'Balance upon departure, l.', N'Balance upon departure, l.', 'car-report'
exec insertLanguageInterface 'ru', 'Balance upon departure, l.', N'Остаток при выезде, л.', 'car-report'
exec insertLanguageInterface 'uk', 'Balance upon departure, l.', N'Залишок на виїзді, л.', 'car-report'
GO

exec insertLanguageInterface 'en', 'Please, fill "Balance upon departure, l."', N'Please, fill "Balance upon departure, l."', 'car-report'
exec insertLanguageInterface 'ru', 'Please, fill "Balance upon departure, l."', N'Пожалуйста, заполните «Остаток при выезде, л.».', 'car-report'
exec insertLanguageInterface 'uk', 'Please, fill "Balance upon departure, l."', N'Будь ласка, заповніть "Залишок при від''їзді, л."', 'car-report'
GO

exec insertLanguageInterface 'en', 'Field "Balance upon departure, l." must be more then 0', N'Field "Balance upon departure, l." must be more then 0', 'car-report'
exec insertLanguageInterface 'ru', 'Field "Balance upon departure, l." must be more then 0', N'Поле «Остаток при выезде, л.» должно быть больше 0', 'car-report'
exec insertLanguageInterface 'uk', 'Field "Balance upon departure, l." must be more then 0', N'Поле "Залишок при виїзді, л." має бути більше 0', 'car-report'
GO

exec insertLanguageInterface 'en', 'Balance upon return l.', N'Balance upon return l.', 'car-report'
exec insertLanguageInterface 'ru', 'Balance upon return l.', N'Остаток по возврату л.', 'car-report'
exec insertLanguageInterface 'uk', 'Balance upon return l.', N'Залишок при поверненні л.', 'car-report'
GO

exec insertLanguageInterface 'en', 'Please, fill "Balance upon return l."', N'Please, fill "Balance upon return l."', 'car-report'
exec insertLanguageInterface 'ru', 'Please, fill "Balance upon return l."', N'Заполните, пожалуйста, «Возвратный остаток, л.»', 'car-report'
exec insertLanguageInterface 'uk', 'Please, fill "Balance upon return l."', N'Будь ласка, заповніть "Зворотний баланс, л."', 'car-report'
GO

exec insertLanguageInterface 'en', 'Field "Balance upon return l." must be more then 0', N'Field "Balance upon return l." must be more then 0', 'car-report'
exec insertLanguageInterface 'ru', 'Field "Balance upon return l." must be more then 0', N'Поле «Остаток при возврате л. должно быть больше 0', 'car-report'
exec insertLanguageInterface 'uk', 'Field "Balance upon return l." must be more then 0', N'Поле "Залишок при поверненні л." має бути більше 0', 'car-report'
GO

exec insertLanguageInterface 'en', 'Issued, l.', N'Issued, l.', 'car-report'
exec insertLanguageInterface 'ru', 'Issued, l.', N'Выдано, л.', 'car-report'
exec insertLanguageInterface 'uk', 'Issued, l.', N'Видано, л.', 'car-report'
GO

exec insertLanguageInterface 'en', 'Please, fill "Issued, l."', N'Please, fill "Issued, l."', 'car-report'
exec insertLanguageInterface 'ru', 'Please, fill "Issued, l."', N'Пожалуйста, заполните "Выдано, л."', 'car-report'
exec insertLanguageInterface 'uk', 'Please, fill "Issued, l."', N'Будь ласка, заповніть "Видано, л."', 'car-report'
GO

exec insertLanguageInterface 'en', 'View uploaded photo', N'View uploaded photo', 'car-report'
exec insertLanguageInterface 'ru', 'View uploaded photo', N'Посмотреть загруженное фото', 'car-report'
exec insertLanguageInterface 'uk', 'View uploaded photo', N'Переглянути завантажену фотографію', 'car-report'
GO

exec insertLanguageInterface 'en', 'Please select a file', N'Please select a file', 'car-report'
exec insertLanguageInterface 'ru', 'Please select a file', N'Пожалуйста, выберите файл', 'car-report'
exec insertLanguageInterface 'uk', 'Please select a file', N'Виберіть файл', 'car-report'
GO

exec insertLanguageInterface 'en', 'Photo', N'Photo', 'car-report'
exec insertLanguageInterface 'ru', 'Photo', N'Фото', 'car-report'
exec insertLanguageInterface 'uk', 'Photo', N'Фото', 'car-report'
GO

exec insertLanguageInterface 'en', 'Report on the date already exist. See report ID: {{report_id}}', N'Report on the date already exist. See report ID: {{report_id}}', 'validators'
exec insertLanguageInterface 'ru', 'Report on the date already exist. See report ID: {{report_id}}', N'Отчет на дату уже существует. См. Идентификатор отчета: {{report_id}}', 'validators'
exec insertLanguageInterface 'uk', 'Report on the date already exist. See report ID: {{report_id}}', N'Звіт про дату вже існує. Див. Ідентифікатор звіту: {{report_id}}', 'validators'
GO

exec insertLanguageInterface 'en', '"Return, km" cannot be less than {{odometer_start}}', N'"Return, km" cannot be less than {{odometer_start}}', 'validators'
exec insertLanguageInterface 'ru', '"Return, km" cannot be less than {{odometer_start}}', N'«Возврат, км» не может быть меньше {{odometer_start}}', 'validators'
exec insertLanguageInterface 'uk', '"Return, km" cannot be less than {{odometer_start}}', N'"Повернення, км" не може бути меншим за {{odometer_start}}', 'validators'
GO

exec insertLanguageInterface 'en', 'The last day must correspond to the monthly report', N'The last day must correspond to the monthly report', 'validators'
exec insertLanguageInterface 'ru', 'The last day must correspond to the monthly report', N'Последний день должен соответствовать ежемесячному отчету.', 'validators'
exec insertLanguageInterface 'uk', 'The last day must correspond to the monthly report', N'Останній день повинен відповідати щомісячному звіту', 'validators'
GO

exec insertLanguageInterface 'en', 'This permission already exist. Please see {{editable_id}}', N'This permission already exist. Please {{editable_id}}', 'validators'
exec insertLanguageInterface 'ru', 'This permission already exist. Please see {{editable_id}}', N'Это разрешение уже существует. См. {{editable_id}}', 'validators'
exec insertLanguageInterface 'uk', 'This permission already exist. Please see {{editable_id}}', N'Цей дозвіл вже існує. Перегляньте {{editable_id}}', 'validators'
GO

exec insertLanguageInterface 'en', 'Sorry, you can`t edit your monthly report.', N'Sorry, you can`t edit your monthly report.', 'car-report'
exec insertLanguageInterface 'ru', 'Sorry, you can`t edit your monthly report.', N'Извините, но вы не можете редактировать месячный отчет.', 'car-report'
exec insertLanguageInterface 'uk', 'Sorry, you can`t edit your monthly report.', N'Вибачте, але ви не можете редагувати місячний звіт.', 'car-report'
GO

exec insertLanguageInterface 'en', 'Sorry but you can`t edit this report.', N'Sorry but you can`t edit this report.', 'car-report'
exec insertLanguageInterface 'ru', 'Sorry but you can`t edit this report.', N'К сожалению, вы не можете редактировать этот отчет.', 'car-report'
exec insertLanguageInterface 'uk', 'Sorry but you can`t edit this report.', N'На жаль, ви не можете редагувати цей звіт.', 'car-report'
GO

exec insertLanguageInterface 'en', 'Travel sheets', N'Travel sheets', 'car-report'
exec insertLanguageInterface 'ru', 'Travel sheets', N'Путевые листы', 'car-report'
exec insertLanguageInterface 'uk', 'Travel sheets', N'Проїзні листи', 'car-report'
GO

exec insertLanguageInterface 'en', 'Next report', N'Next report', 'car-report'
exec insertLanguageInterface 'ru', 'Next report', N'Следующий отчет', 'car-report'
exec insertLanguageInterface 'uk', 'Next report', N'Наступний звіт', 'car-report'
GO

exec insertLanguageInterface 'en', 'Edit', N'Edit', 'car-report'
exec insertLanguageInterface 'ru', 'Edit', N'Редактировать', 'car-report'
exec insertLanguageInterface 'uk', 'Edit', N'Редагувати', 'car-report'
GO

exec insertLanguageInterface 'en', 'View', N'View', 'car-report'
exec insertLanguageInterface 'ru', 'View', N'Посмотреть', 'car-report'
exec insertLanguageInterface 'uk', 'View', N'Перегляд', 'car-report'
GO

exec insertLanguageInterface 'en', 'Saved', N'Saved', 'car-report'
exec insertLanguageInterface 'ru', 'Saved', N'Сохранено', 'car-report'
exec insertLanguageInterface 'uk', 'Saved', N'Збережено', 'car-report'
GO

exec insertLanguageInterface 'en', 'Monthly report', N'Monthly report', 'car-report'
exec insertLanguageInterface 'ru', 'Monthly report', N'Месячный отчет', 'car-report'
exec insertLanguageInterface 'uk', 'Monthly report', N'Місячний звіт', 'car-report'
GO

exec insertLanguageInterface 'en', 'Daily reports', N'Daily reports', 'car-report'
exec insertLanguageInterface 'ru', 'Daily reports', N'Ежедневные отчеты', 'car-report'
exec insertLanguageInterface 'uk', 'Daily reports', N'Щоденні звіти', 'car-report'
GO

exec insertLanguageInterface 'en', 'Please, fill out the report on the {{day}}', N'Please, fill out the report on the {{day}}', 'validators'
exec insertLanguageInterface 'ru', 'Please, fill out the report on the {{day}}', N'Пожалуйста, заполните отчет за {{day}}', 'validators'
exec insertLanguageInterface 'uk', 'Please, fill out the report on the {{day}}', N'Будь ласка, заповніть звіт за {{day}}', 'validators'
GO

exec insertLanguageInterface 'en', '"Return, km" cannot be more monthly report ({{max_report}} km.)', N'"Return, km" cannot be more monthly report ({{max_report}} km.)', 'validators'
exec insertLanguageInterface 'ru', '"Return, km" cannot be more monthly report ({{max_report}} km.)', N'«Возврат, км» не может быть более ежемесячным отчетом ({{max_report}} км.)', 'validators'
exec insertLanguageInterface 'uk', '"Return, km" cannot be more monthly report ({{max_report}} km.)', N'"Повернення, км" не може бути більш щомісячним звітом ({{max_report}} км.)', 'validators'
GO

--- paginator
exec insertLanguageInterface 'en', 'Items per page:', N'Items per page:', 'car-report'
exec insertLanguageInterface 'ru', 'Items per page:', N'Элементов на странице:', 'car-report'
exec insertLanguageInterface 'uk', 'Items per page:', N'Елементів на сторінці:', 'car-report'
GO

--- paginator
exec insertLanguageInterface 'en', 'of', N'of', 'car-report'
exec insertLanguageInterface 'ru', 'of', N'из', 'car-report'
exec insertLanguageInterface 'uk', 'of', N'з', 'car-report'
GO

--- paginator
exec insertLanguageInterface 'en', 'Next page', N'Next page', 'car-report'
exec insertLanguageInterface 'ru', 'Next page', N'Следующая страница', 'car-report'
exec insertLanguageInterface 'uk', 'Next page', N'Наступна сторінка', 'car-report'
GO

--- paginator
exec insertLanguageInterface 'en', 'Previous page', N'Previous page', 'car-report'
exec insertLanguageInterface 'ru', 'Previous page', N'Предыдущая страница', 'car-report'
exec insertLanguageInterface 'uk', 'Previous page', N'Попередня сторінка', 'car-report'
GO

--- paginator
exec insertLanguageInterface 'en', 'Last page', N'Last page', 'car-report'
exec insertLanguageInterface 'ru', 'Last page', N'Последняя страница', 'car-report'
exec insertLanguageInterface 'uk', 'Last page', N'Остання сторінка', 'car-report'
GO

--- paginator
exec insertLanguageInterface 'en', 'First page', N'First page', 'car-report'
exec insertLanguageInterface 'ru', 'First page', N'Первая страница', 'car-report'
exec insertLanguageInterface 'uk', 'First page', N'Перша сторінка', 'car-report'
GO

--- calendar
EXEC insertLanguageInterface 'en', 'January', 'January', 'calendar';
EXEC insertLanguageInterface 'ru', 'January', N'Январь', 'calendar';
EXEC insertLanguageInterface 'uk', 'January', N'Січень', 'calendar';

EXEC insertLanguageInterface 'en', 'February', 'February', 'calendar';
EXEC insertLanguageInterface 'ru', 'February', N'Февраль', 'calendar';
EXEC insertLanguageInterface 'uk', 'February', N'Лютий', 'calendar';

EXEC insertLanguageInterface 'en', 'March', 'March', 'calendar';
EXEC insertLanguageInterface 'ru', 'March', N'Март', 'calendar';
EXEC insertLanguageInterface 'uk', 'March', N'Березень', 'calendar';

EXEC insertLanguageInterface 'en', 'April', 'April', 'calendar';
EXEC insertLanguageInterface 'ru', 'April', N'Апрель', 'calendar';
EXEC insertLanguageInterface 'uk', 'April', N'Квітень', 'calendar';

EXEC insertLanguageInterface 'en', 'May', 'May', 'calendar';
EXEC insertLanguageInterface 'ru', 'May', N'Май', 'calendar';
EXEC insertLanguageInterface 'uk', 'May', N'Травень', 'calendar';

EXEC insertLanguageInterface 'en', 'June', 'June', 'calendar';
EXEC insertLanguageInterface 'ru', 'June', N'Июнь', 'calendar';
EXEC insertLanguageInterface 'uk', 'June', N'Червень', 'calendar';

EXEC insertLanguageInterface 'en', 'July', 'July', 'calendar';
EXEC insertLanguageInterface 'ru', 'July', N'Июль', 'calendar';
EXEC insertLanguageInterface 'uk', 'July', N'Липень', 'calendar';

EXEC insertLanguageInterface 'en', 'August', 'August', 'calendar';
EXEC insertLanguageInterface 'ru', 'August', N'Август', 'calendar';
EXEC insertLanguageInterface 'uk', 'August', N'Серпень', 'calendar';

EXEC insertLanguageInterface 'en', 'September', 'September', 'calendar';
EXEC insertLanguageInterface 'ru', 'September', N'Сентябрь', 'calendar';
EXEC insertLanguageInterface 'uk', 'September', N'Вересень', 'calendar';

EXEC insertLanguageInterface 'en', 'October', 'October', 'calendar';
EXEC insertLanguageInterface 'ru', 'October', N'Октябрь', 'calendar';
EXEC insertLanguageInterface 'uk', 'October', N'Жовтень', 'calendar';

EXEC insertLanguageInterface 'en', 'November', 'November', 'calendar';
EXEC insertLanguageInterface 'ru', 'November', N'Ноябрь', 'calendar';
EXEC insertLanguageInterface 'uk', 'November', N'Листопад', 'calendar';

EXEC insertLanguageInterface 'en', 'December', 'December', 'calendar';
EXEC insertLanguageInterface 'ru', 'December', N'Декабрь', 'calendar';
EXEC insertLanguageInterface 'uk', 'December', N'Грудень', 'calendar';
GO
------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------
-- https://teamsoft.atlassian.net/browse/PHARMAWEB-393
----------------------------------------------------------------------------------------------------
EXEC insertLanguageInterface 'en', 'This brand has already been added', 'This brand has already been added', 'validators';
EXEC insertLanguageInterface 'ru', 'This brand has already been added', 'Этот бренд уже добавлен', 'validators';
EXEC insertLanguageInterface 'uk', 'This brand has already been added', 'Цей бренд вже додано', 'validators';

EXEC insertLanguageInterface 'en', 'This promomaterial has already been added', 'This promomaterial has already been added', 'validators';
EXEC insertLanguageInterface 'ru', 'This promomaterial has already been added', 'Этот промоматериал уже добавлен', 'validators';
EXEC insertLanguageInterface 'uk', 'This promomaterial has already been added', 'Цей проматеріал вже додано', 'validators';
----------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------

----------------------------------------------------------------------------------------------------
--- time
----------------------------------------------------------------------------------------------------
IF NOT EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE table_name = 'info_contactblog' AND column_name = 'time')
ALTER TABLE info_contactblog ADD time timestamp null
GO

IF NOT EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE table_name = 'info_plancompanypreparation2set2' AND column_name = 'time')
ALTER TABLE info_plancompanypreparation2set2 ADD time timestamp null
GO

IF NOT EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE table_name = 'info_contactcomment' AND column_name = 'time')
ALTER TABLE info_contactcomment ADD time timestamp null
GO

IF NOT EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE table_name = 'info_taskpreparationdtdose' AND column_name = 'time')
ALTER TABLE info_taskpreparationdtdose ADD time timestamp null
GO

IF NOT EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE table_name = 'info_taskpreparationdtdosefile' AND column_name = 'time')
ALTER TABLE info_taskpreparationdtdosefile ADD time timestamp null
GO

IF NOT EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE table_name = 'info_companysign' AND column_name = 'time')
ALTER TABLE info_companysign ADD time timestamp null
GO

IF NOT EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE table_name = 'info_companysignfile' AND column_name = 'time')
ALTER TABLE info_companysignfile ADD time timestamp null
GO

IF NOT EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE table_name = 'info_car' AND column_name = 'time')
ALTER TABLE info_car ADD time timestamp null
GO

IF NOT EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE table_name = 'info_carreportsheet' AND column_name = 'time')
ALTER TABLE info_carreportsheet ADD time timestamp null
GO

IF NOT EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE table_name = 'info_carreportsheethistory' AND column_name = 'time')
ALTER TABLE info_carreportsheethistory ADD time timestamp null
GO

IF NOT EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE table_name = 'info_carreportsheeteditable' AND column_name = 'time')
ALTER TABLE info_carreportsheeteditable ADD time timestamp null
GO

IF NOT EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE table_name = 'info_carreportsheetfile' AND column_name = 'time')
ALTER TABLE info_carreportsheetfile ADD time timestamp null
GO
----------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------

------------------------------------------------------------------------------------------------------------------------
---add column in info_user -> passportnumber
------------------------------------------------------------------------------------------------------------------------
IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'info_user' AND COLUMN_NAME = 'passportnumber')
    BEGIN
        ALTER TABLE info_user ADD passportnumber VARCHAR(255) NULL
    END
GO
------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------
-- https://teamsoft.atlassian.net/browse/DRREDDYS-590
----------------------------------------------------------------------------------------------------
EXEC insertLanguageInterface 'en', 'April', 'April', 'granual';
EXEC insertLanguageInterface 'ru', 'April', 'Апрель', 'granual';
EXEC insertLanguageInterface 'uk', 'April', 'Квітень', 'granual';

EXEC insertLanguageInterface 'en', 'August', 'August', 'granual';
EXEC insertLanguageInterface 'ru', 'August', 'Август', 'granual';
EXEC insertLanguageInterface 'uk', 'August', 'Серпень', 'granual';

EXEC insertLanguageInterface 'en', 'December', 'December', 'granual';
EXEC insertLanguageInterface 'ru', 'December', 'Декабрь', 'granual';
EXEC insertLanguageInterface 'uk', 'December', 'Грудень', 'granual';

EXEC insertLanguageInterface 'en', 'February', 'February', 'granual';
EXEC insertLanguageInterface 'ru', 'February', 'Февраль', 'granual';
EXEC insertLanguageInterface 'uk', 'February', 'Лютий', 'granual';

EXEC insertLanguageInterface 'en', 'January', 'January', 'granual';
EXEC insertLanguageInterface 'ru', 'January', 'Январь', 'granual';
EXEC insertLanguageInterface 'uk', 'January', 'Січень', 'granual';

EXEC insertLanguageInterface 'en', 'July', 'July', 'granual';
EXEC insertLanguageInterface 'ru', 'July', 'Июль', 'granual';
EXEC insertLanguageInterface 'uk', 'July', 'Липень', 'granual';

EXEC insertLanguageInterface 'en', 'June', 'June', 'granual';
EXEC insertLanguageInterface 'ru', 'June', 'Июнь', 'granual';
EXEC insertLanguageInterface 'uk', 'June', 'Червень', 'granual';

EXEC insertLanguageInterface 'en', 'March', 'March', 'granual';
EXEC insertLanguageInterface 'ru', 'March', 'Март', 'granual';
EXEC insertLanguageInterface 'uk', 'March', 'Березень', 'granual';

EXEC insertLanguageInterface 'en', 'May', 'May', 'granual';
EXEC insertLanguageInterface 'ru', 'May', 'Май', 'granual';
EXEC insertLanguageInterface 'uk', 'May', 'Травень', 'granual';

EXEC insertLanguageInterface 'en', 'November', 'November', 'granual';
EXEC insertLanguageInterface 'ru', 'November', 'Ноябрь', 'granual';
EXEC insertLanguageInterface 'uk', 'November', 'Листопад', 'granual';

EXEC insertLanguageInterface 'en', 'October', 'October', 'granual';
EXEC insertLanguageInterface 'ru', 'October', 'Октябрь', 'granual';
EXEC insertLanguageInterface 'uk', 'October', 'Жовтень', 'granual';

EXEC insertLanguageInterface 'en', 'September', 'September', 'granual';
EXEC insertLanguageInterface 'ru', 'September', 'Сентябрь', 'granual';
EXEC insertLanguageInterface 'uk', 'September', 'Вересень', 'granual';
GO
----------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------
