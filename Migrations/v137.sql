---------------------------------------------------------------------------------------------------------------------------------------------------
--https://teamsoft.atlassian.net/browse/REDDYSRSA-249
-----------------------------------------------------------------------------------------------------------------------------------------------
IF NOT EXISTS(SELECT 1 FROM information_schema.columns WHERE table_name = 'info_clm' and column_name = 'efile_id')
begin
    ALTER TABLE info_clm ADD efile_id INT NULL;
    ALTER TABLE info_clm WITH CHECK ADD CONSTRAINT FK_info_clm_efile_id FOREIGN KEY (efile_id) REFERENCES info_customdictionaryvalue (id);
END;
GO

IF NOT EXISTS(SELECT 1 FROM information_schema.columns WHERE table_name = 'info_clm' and column_name = 'pmac_number')
begin
    ALTER TABLE info_clm ADD pmac_number VARCHAR(255) null;
END;
GO

exec insertLanguageInterface 'en', 'PMAC Number', 'PMAC Number', 'clm';
exec insertLanguageInterface 'ru', 'PMAC Number', 'Номер PMAC', 'clm';
exec insertLanguageInterface 'uk', 'PMAC Number', 'Номер PMAC', 'clm';
GO

exec insertLanguageInterface 'en', 'eFile corresponds to approved material', 'eFile corresponds to approved material', 'clm';
exec insertLanguageInterface 'ru', 'eFile corresponds to approved material', 'Электронный файл соответствует утвержденному материалу', 'clm';
exec insertLanguageInterface 'uk', 'eFile corresponds to approved material', 'Електронний файл відповідає затвердженому матеріалу', 'clm';
GO
---------------------------------------------------------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------------------------------------
-- https://teamsoft.atlassian.net/browse/REDDYSRSA-247
------------------------------------------------------------------------------------------------------------------------
IF NOT EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE table_name = 'info_mcmcontent' AND column_name = 'efile_id')
BEGIN
    ALTER TABLE info_mcmcontent ADD efile_id INT NULL
    ALTER TABLE [dbo].[info_mcmcontent] WITH CHECK ADD CONSTRAINT [FK_info_mcmcontent_efile_id]
        FOREIGN KEY ([efile_id]) REFERENCES [dbo].[info_customdictionaryvalue] ([id])
END
GO

IF NOT EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE table_name = 'info_mcmcontent' AND column_name = 'pmac_number')
BEGIN
    ALTER TABLE info_mcmcontent ADD pmac_number VARCHAR(255) NULL
END
GO

exec insertLanguageInterface  'en', 'Digital Compliance', N'Digital Compliance', 'mcm';
exec insertLanguageInterface  'ru', 'Digital Compliance', N'Цифровое соответствие', 'mcm';
exec insertLanguageInterface  'uk', 'Digital Compliance', N'Цифрова відповідність', 'mcm';
GO

exec insertLanguageInterface  'en', 'Additional information', N'Additional information', 'mcm';
exec insertLanguageInterface  'ru', 'Additional information', N'Дополнительная информация', 'mcm';
exec insertLanguageInterface  'uk', 'Additional information', N'Додаткова інформація', 'mcm';
GO

exec insertLanguageInterface  'en', 'eFile corresponds to approved material', N'eFile corresponds to approved material', 'mcm';
exec insertLanguageInterface  'ru', 'eFile corresponds to approved material', N'eFile соответствует утвержденному материалу', 'mcm';
exec insertLanguageInterface  'uk', 'eFile corresponds to approved material', N'eFile відповідає затвердженому матеріалу', 'mcm';
GO

exec insertLanguageInterface  'en', 'PMAC Number', N'PMAC Number', 'mcm';
exec insertLanguageInterface  'ru', 'PMAC Number', N'Номер PMAC', 'mcm';
exec insertLanguageInterface  'uk', 'PMAC Number', N'Номер PMAC', 'mcm';
GO

------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------
-- https://teamsoft.atlassian.net/browse/MCM-479
----------------------------------------------------------------------------------------------------
exec insertLanguageInterface 'en', 'Telegram Distribution script', N'Telegram Distribution script', 'mcm';
exec insertLanguageInterface 'ru', 'Telegram Distribution script', N'Сценарий Telegram рассылки', 'mcm';
exec insertLanguageInterface 'uk', 'Telegram Distribution script', N'Сценарій Telegram розсилки', 'mcm';
GO
----------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------------------------------------------------------------------------
------- https://teamsoft.atlassian.net/browse/DRREDDYSKZ-209
----------------------------------------------------------------------------------------------------------------------------------------------------------------------
IF EXISTS(SELECT * FROM sysobjects WHERE id = OBJECT_ID(N'targeting_should_confirm_approve_pharmacy') AND xtype IN (N'FN', N'IF', N'TF'))
    DROP FUNCTION targeting_should_confirm_approve_pharmacy
GO

CREATE FUNCTION targeting_should_confirm_approve_pharmacy(@user_id INT, @dt DATETIME)
    RETURNS TABLE AS RETURN (SELECT 1 item_delta, 1 visit_delta, 1 item_percent, 1 visit_percent FROM targeting_company_summary(@user_id, @dt, 1))
GO

IF EXISTS(SELECT * FROM sysobjects WHERE id = OBJECT_ID(N'targeting_should_confirm_approve_doctors') AND xtype IN (N'FN', N'IF', N'TF'))
    DROP FUNCTION targeting_should_confirm_approve_doctors
GO

CREATE FUNCTION targeting_should_confirm_approve_doctors(@user_id INT, @dt DATETIME)
    RETURNS TABLE AS RETURN (SELECT 1 item_delta, 1 visit_delta, 1 item_percent, 1 visit_percent FROM targeting_contact_summary(@user_id, @dt, 1))
GO

-- #5 segment column
EXEC insertPoGridconfig 100, 'segmentText', 'Segment', 0, 1
EXEC insertPoGridconfig 100, 'segmentText', 'Segment', 0, 2
GO

-- #6 morionid column
EXEC insertPoGridconfig 100, 'morionId', 'Morion ID', 0, 1
EXEC insertPoGridconfig 100, 'morionId', 'Morion ID', 0, 2
GO
----------------------------------------------------------------------------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------
---https://teamsoft.atlassian.net/browse/MCM-474
------------------------------------------------------------------------------------------------------------------------
IF NOT EXISTS (SELECT 1 FROM information_schema.columns WHERE table_name = 'info_mcmfrom' and column_name = 'country_id')
BEGIN
    ALTER TABLE info_mcmfrom ADD country_id INT NULL;
    ALTER TABLE info_mcmfrom ADD CONSTRAINT FK_info_mcmfrom_country_id FOREIGN KEY(country_id) REFERENCES info_country (id);
END;
GO
IF NOT EXISTS (SELECT 1 FROM information_schema.columns WHERE table_name = 'info_mcmtrigger' and column_name = 'owner_id')
BEGIN
    ALTER TABLE info_mcmtrigger ADD owner_id INT NULL;
    ALTER TABLE info_mcmtrigger ADD CONSTRAINT FK_info_mcmtrigger_owner_id FOREIGN KEY(owner_id) REFERENCES info_user (id);
END;
GO
------------------------------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------------------------------------------------------------------------
------- https://teamsoft.atlassian.net/browse/EGISRU-487
----------------------------------------------------------------------------------------------------------------------------------------------------------------------
IF NOT EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE table_name = 'info_plancontactrule' AND column_name = 'user_id')
begin
    ALTER TABLE info_plancontactrule ADD [user_id] INT NULL
    ALTER TABLE info_plancontactrule ADD CONSTRAINT FK_info_plancontactrule_user_id FOREIGN KEY ([user_id]) REFERENCES info_user([id]);
end
GO
IF NOT EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE table_name = 'info_plancompanyrule' AND column_name = 'user_id')
begin
    ALTER TABLE info_plancompanyrule ADD [user_id] INT NULL
    ALTER TABLE info_plancompanyrule ADD CONSTRAINT FK_info_plancompanyrule_user_id FOREIGN KEY ([user_id]) REFERENCES info_user([id]);
end
GO
----------------------------------------------------------------------------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------------------------
-- https://teamsoft.atlassian.net/browse/PHARMAWEB-453
---------------------------------------------------------------------------------------------------------------------
IF EXISTS(SELECT 1 FROM information_schema.columns WHERE table_name = 'po_dictionary' AND NOT EXISTS(SELECT 1 FROM po_dictionary WHERE tablename = 'info_role'))
BEGIN
    INSERT po_dictionary (identifier, name, tablename, group_id)
    VALUES (NULL, 'Roles', 'info_role', (SELECT id FROM po_dictionarygroup WHERE name = N'Общие'))

    DECLARE @en_id int, @ru_id int, @uk_id int;
    SELECT TOP 1 @en_id = id from info_language where slug = 'en';
    SELECT TOP 1 @ru_id = id from info_language where slug = 'ru';
    SELECT TOP 1 @uk_id = id from info_language where slug = 'uk';

    INSERT info_dictionaryidentifier (tablename, columnname, value, identifier, hidden, order_num, required, language_id)
        VALUES ('info_role', 'name', N'Name', 0, 0, 1, 1, @en_id),
               ('info_role', 'name', N'Название', 0, 0, 1, 1, @ru_id),
               ('info_role', 'name', N'Назва', 0, 0, 1, 1, @uk_id),
               ('info_role', 'code', N'Code', 0, 0, 2, 1, @en_id),
               ('info_role', 'code', N'Код', 0, 0, 2, 1, @ru_id),
               ('info_role', 'code', N'Код', 0, 0, 2, 1, @uk_id),
               ('info_role', 'is_archived', N'Is archived', 0, 0, 1, 0, @en_id),
               ('info_role', 'is_archived', N'Архивная', 0, 0, 1, 0, @ru_id),
               ('info_role', 'is_archived', N'Архівна', 0, 0, 1, 0, @uk_id)
END
GO

IF NOT EXISTS(SELECT 1 FROM information_schema.columns WHERE table_name = 'info_role' AND column_name = 'is_archived')
BEGIN
    ALTER TABLE info_role ADD is_archived INT NULL
END

DECLARE @dictionary_role_id INT = (SELECT TOP 1 id FROM po_dictionary WHERE tablename = 'info_role')
DECLARE @role_super_admin_id INT = (SELECT TOP 1 id FROM info_role WHERE code = 'ROLE_SUPER_ADMIN')

IF NOT EXISTS(SELECT 1 FROM po_dictionarybyrole WHERE dictionary_id = @dictionary_role_id AND role_id = @role_super_admin_id)
BEGIN
    INSERT INTO po_dictionarybyrole (dictionary_id, role_id) VALUES (@dictionary_role_id, @role_super_admin_id)
END
GO
---------------------------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------
------- https://teamsoft.atlassian.net/browse/DRREDDYSMM-146
------------------------------------------------------------------------------------------------------------------------
IF NOT EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE table_name = 'info_mcmdistribution' AND column_name = 'landing_content_id')
BEGIN
    ALTER TABLE info_mcmdistribution ADD [landing_content_id] INT NULL
    ALTER TABLE info_mcmdistribution ADD CONSTRAINT FK_info_mcmdistribution_landing_content_id
        FOREIGN KEY ([landing_content_id]) REFERENCES info_mcmcontent([id]);
END
GO

exec insertLanguageInterface 'en', 'Landing page', N'Landing page', 'mcm';
exec insertLanguageInterface 'ru', 'Landing page', N'Целевая страница', 'mcm';
exec insertLanguageInterface 'uk', 'Landing page', N'Цільова сторінка', 'mcm';
GO

IF NOT EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE table_name = 'info_mcmcontent' AND column_name = 'is_landing')
    ALTER TABLE info_mcmcontent ADD is_landing INT NULL
GO
------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------
-- https://teamsoft.atlassian.net/browse/PHARMAWEB-656
------------------------------------------------------------------------------------------------------------------------
EXEC insertLanguageInterface 'en', 'Total photos found', 'Total photos found', 'messages';
EXEC insertLanguageInterface 'ru', 'Total photos found', N'Всего фотографий найдено', 'messages';
EXEC insertLanguageInterface 'uk', 'Total photos found', N'Всього знайдено фотографій', 'messages';
EXEC insertLanguageInterface 'en', 'Photos can be download', 'Photos can be download', 'messages';
EXEC insertLanguageInterface 'ru', 'Photos can be download', N'Фотографий можно скачать', 'messages';
EXEC insertLanguageInterface 'uk', 'Photos can be download', N'Фотографій можна завантажити', 'messages';
------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------
-- https://teamsoft.atlassian.net/browse/PHARMAWEB-489
------------------------------------------------------------------------------------------------------------------------
EXEC insertLanguageInterface 'en', 'Download will start in a few seconds...', 'Download will start in a few seconds...', 'messages';
EXEC insertLanguageInterface 'ru', 'Download will start in a few seconds...', N'Загрузка начнется через несколько секунд...', 'messages';
EXEC insertLanguageInterface 'uk', 'Download will start in a few seconds...', N'Завантаження почнеться за кілька секунд...', 'messages';
------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------
------- https://teamsoft.atlassian.net/browse/DRREDDYSRO-94
------------------------------------------------------------------------------------------------------------------------
IF EXISTS (SELECT 1 FROM ps_pattern_types where name = N'Продажи дистрибьюторов')
BEGIN
    DECLARE @typeID INT = (SELECT TOP 1 id FROM ps_pattern_types WHERE name = N'Продажи дистрибьюторов');

    IF NOT EXISTS(SELECT * FROM ps_distributor_field WHERE field_name = 'discount_1')
    BEGIN
        INSERT INTO ps_distributor_field (name, field_name, is_required, type_id) VALUES (N'Скидка 1', 'discount_1', 0, @typeID);
    END;

    IF NOT EXISTS(SELECT * FROM ps_distributor_field WHERE field_name = 'discount_2')
    BEGIN
        INSERT INTO ps_distributor_field (name, field_name, is_required, type_id) VALUES (N'Скидка 2', 'discount_2', 0, @typeID);
    END;
END;
GO
------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------------------------
-- https://teamsoft.atlassian.net/browse/DRREDDYSRO-86
---------------------------------------------------------------------------------------------------------------------
if NOT EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE table_name='info_granuelplaninguserstatus' AND column_name = 'saleperiod_id')
BEGIN
ALTER TABLE info_granuelplaninguserstatus ADD saleperiod_id INTEGER NULL;
ALTER TABLE info_granuelplaninguserstatus ADD constraint fk_info_granuelplaninguserstatus_saleperiod_id
    FOREIGN KEY (saleperiod_id) REFERENCES info_saleperiod (id);
END;
GO

if NOT EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE table_name='info_granuelplaninguserstatus' AND column_name = 'currenttime')
ALTER TABLE info_granuelplaninguserstatus ADD currenttime datetime NULL DEFAULT (CURRENT_TIMESTAMP);
GO

if NOT EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE table_name='info_granuelplaninguserstatus' AND column_name = 'moduser')
ALTER TABLE info_granuelplaninguserstatus ADD moduser VARCHAR(16) NULL DEFAULT ([dbo].[Get_CurrentCode]());
GO

if NOT EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE table_name='info_granuelplaninguserstatus' AND column_name = 'guid')
ALTER TABLE info_granuelplaninguserstatus ADD guid uniqueidentifier NULL DEFAULT (newid());
GO

exec insertLanguageInterface 'en', 'Period not found', 'Period not found', 'granual';
exec insertLanguageInterface 'ru', 'Period not found', 'Период не найден', 'granual';
exec insertLanguageInterface 'uk', 'Period not found', 'Період не знайдено', 'granual';
GO
---------------------------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------------------------
------- https://teamsoft.atlassian.net/browse/REDDYSRSA-243
------------------------------------------------------------------------------------------------------------------------
exec insertLanguageInterface 'en', 'Claim ID', 'Claim ID', 'expense';
exec insertLanguageInterface 'ru', 'Claim ID', 'ID запроса', 'expense';
exec insertLanguageInterface 'uk', 'Claim ID', 'ID запиту', 'expense';
GO
------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------------------------
-- https://teamsoft.atlassian.net/browse/EGISRU-376
---------------------------------------------------------------------------------------------------------------------
if NOT EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE table_name='info_taskfile' AND column_name = 'photostate_id')
BEGIN
ALTER TABLE info_taskfile ADD photostate_id INTEGER NULL;
ALTER TABLE info_taskfile ADD constraint fk_info_taskfile_photostate_id
    FOREIGN KEY (photostate_id) REFERENCES info_customdictionaryvalue (id);
END;
GO
---------------------------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------
----------- https://teamsoft.atlassian.net/browse/PHARMAWEB-665
------------------------------------------------------------------------------------------------------------------------
exec insertLanguageInterface 'en', 'Approved successfully!', 'Approved successfully!', 'targeting';
exec insertLanguageInterface 'ru', 'Approved successfully!', 'Утверждено успешно!', 'targeting';
exec insertLanguageInterface 'uk', 'Approved successfully!', 'Успішно схвалено!', 'targeting';

exec insertLanguageInterface 'en', 'Approving failed. Status text error', 'Approving failed. Status text error', 'targeting';
exec insertLanguageInterface 'ru', 'Approving failed. Status text error', 'Не удалось подтвердить. Ошибка текста статуса', 'targeting';
exec insertLanguageInterface 'uk', 'Approving failed. Status text error', 'Не вдалося затвердити. Помилка тексту статусу', 'targeting';

exec insertLanguageInterface 'en', 'Saved successfully!', 'Saved successfully!', 'targeting';
exec insertLanguageInterface 'ru', 'Saved successfully!', 'Успешно сохранено!', 'targeting';
exec insertLanguageInterface 'uk', 'Saved successfully!', 'Збережено успішно!', 'targeting';

exec insertLanguageInterface 'en', 'Saving failed. Status text error', 'Saving failed. Status text error', 'targeting';
exec insertLanguageInterface 'ru', 'Saving failed. Status text error', 'Не удалось сохранить. Ошибка текста статуса', 'targeting';
exec insertLanguageInterface 'uk', 'Saving failed. Status text error', 'Не вдалося зберегти. Помилка тексту статусу', 'targeting';
GO
------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------
-- https://teamsoft.atlassian.net/browse/PHARMAWEB-666
----------------------------------------------------------------------------------------------------
exec insertLanguageInterface 'en', 'Request initiator', 'Request initiator', 'messages';
exec insertLanguageInterface 'ru', 'Request initiator', 'Инициатор запроса', 'messages';
exec insertLanguageInterface 'uk', 'Request initiator', 'Ініціатор запиту', 'messages';

exec insertLanguageInterface 'en', 'Phone (add.)', 'Phone (add.)', 'messages';
exec insertLanguageInterface 'ru', 'Phone (add.)', 'Телефон (доп.)', 'messages';
exec insertLanguageInterface 'uk', 'Phone (add.)', 'Телефон (дод.)', 'messages';

exec insertLanguageInterface 'en', 'Type contact', 'Type contact', 'messages';
exec insertLanguageInterface 'ru', 'Type contact', 'Тип контакта', 'messages';
exec insertLanguageInterface 'uk', 'Type contact', 'Тип контакту', 'messages';

exec insertLanguageInterface 'en', 'Specialization (add.)', 'Specialization (add.)', 'messages';
exec insertLanguageInterface 'ru', 'Specialization (add.)', 'Специализация (доп.)', 'messages';
exec insertLanguageInterface 'uk', 'Specialization (add.)', 'Специализация (дод.)', 'messages';

exec insertLanguageInterface 'en', 'Responsible (add.)', 'Responsible (add.)', 'messages';
exec insertLanguageInterface 'ru', 'Responsible (add.)', 'Ответственный (доп.)', 'messages';
exec insertLanguageInterface 'uk', 'Responsible (add.)', 'Відповідальний (дод.)', 'messages';

exec insertLanguageInterface 'en', 'Bank identification code', 'Bank identification code', 'messages';
exec insertLanguageInterface 'ru', 'Bank identification code', 'Идентификационный код банка', 'messages';
exec insertLanguageInterface 'uk', 'Bank identification code', 'Ідентифікаційний код банку', 'messages';

exec insertLanguageInterface 'en', 'Passport series', 'Passport series', 'messages';
exec insertLanguageInterface 'ru', 'Passport series', 'Серия паспорта', 'messages';
exec insertLanguageInterface 'uk', 'Passport series', 'Серія паспорта', 'messages';

exec insertLanguageInterface 'en', 'Passport issued by', 'Passport issued by', 'messages';
exec insertLanguageInterface 'ru', 'Passport issued by', 'Кем выдан паспорт', 'messages';
exec insertLanguageInterface 'uk', 'Passport issued by', 'Ким виданий паспорт', 'messages';

exec insertLanguageInterface 'en', 'Passport when issued', 'Passport when issued', 'messages';
exec insertLanguageInterface 'ru', 'Passport when issued', 'Когда выдан паспорт', 'messages';
exec insertLanguageInterface 'uk', 'Passport when issued', 'Коли видано паспорт', 'messages';

exec insertLanguageInterface 'en', 'First name', 'First name', 'messages';
exec insertLanguageInterface 'ru', 'First name', N'Имя', 'messages';
exec insertLanguageInterface 'uk', 'First name', N'Ім''я', 'messages';

exec insertLanguageInterface 'en', 'Postcode', 'Postcode', 'messages';
exec insertLanguageInterface 'ru', 'Postcode', N'Почтовый индекс', 'messages';
exec insertLanguageInterface 'uk', 'Postcode', N'Поштовий індекс', 'messages';

exec insertLanguageInterface 'en', 'Middle name', 'Middle name', 'messages';
exec insertLanguageInterface 'ru', 'Middle name', N'Отчество', 'messages';
exec insertLanguageInterface 'uk', 'Middle name', N'По батькові', 'messages';

exec insertLanguageInterface 'en', 'Company name', 'Company name', 'messages';
exec insertLanguageInterface 'ru', 'Company name', 'Название компании', 'messages';
exec insertLanguageInterface 'uk', 'Company name', 'Назва компанії', 'messages';

exec insertLanguageInterface 'en', 'Site', 'Site', 'messages';
exec insertLanguageInterface 'ru', 'Site', 'Сайт', 'messages';
exec insertLanguageInterface 'uk', 'Site', 'Сайт', 'messages';

exec insertLanguageInterface 'en', 'Owner', 'Owner', 'messages';
exec insertLanguageInterface 'ru', 'Owner', 'Владелец', 'messages';
exec insertLanguageInterface 'uk', 'Owner', 'Власник', 'messages';

exec insertLanguageInterface 'en', 'Last name', 'Last name', 'messages';
exec insertLanguageInterface 'ru', 'Last name', 'Фамилия', 'messages';
exec insertLanguageInterface 'uk', 'Last name', 'Прізвище', 'messages';
GO
----------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------
-- https://teamsoft.atlassian.net/browse/EGISRU-376
------------------------------------------------------------------------------------------------------------------------
IF NOT EXISTS(SELECT 1 FROM info_options WHERE [name]='photo_report.hide_photostate_selector')
    INSERT INTO info_options(name, value)
    VALUES ('photo_report.hide_photostate_selector', 1);
GO
------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------