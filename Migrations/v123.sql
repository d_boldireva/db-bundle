------------------------------------------------------------------------------------------------------------------------
---https://teamsoft.atlassian.net/browse/EGISRU-219
------------------------------------------------------------------------------------------------------------------------
exec insertLanguageInterface 'en', 'DV Survey', N'DV Survey', 'crm'
exec insertLanguageInterface 'ru', 'DV Survey', N'Анкета ДВ', 'crm'
exec insertLanguageInterface 'uk', 'DV Survey', N'Анкета ДВ', 'crm'
exec insertLanguageInterface 'en', 'DV Survey on action', N'DV Survey on action', 'crm'
exec insertLanguageInterface 'ru', 'DV Survey on action', N'Анкета ДВ на задаче', 'crm'
exec insertLanguageInterface 'uk', 'DV Survey on action', N'Анкета ДВ на задачі', 'crm'
exec insertLanguageInterface 'en', 'Parameter', N'Parameter', 'crm'
exec insertLanguageInterface 'ru', 'Parameter', N'Параметр', 'crm'
exec insertLanguageInterface 'uk', 'Parameter', N'Параметр', 'crm'
exec insertLanguageInterface 'en', 'Block', N'Block', 'crm'
exec insertLanguageInterface 'ru', 'Block', N'Блок', 'crm'
exec insertLanguageInterface 'uk', 'Block', N'Блок', 'crm'
exec insertLanguageInterface 'en', 'Group', N'Group', 'crm'
exec insertLanguageInterface 'ru', 'Group', N'Группа', 'crm'
exec insertLanguageInterface 'uk', 'Group', N'Група', 'crm'
exec insertLanguageInterface 'en', 'Hidden', N'Hidden', 'crm'
exec insertLanguageInterface 'ru', 'Hidden', N'Скрытый', 'crm'
exec insertLanguageInterface 'uk', 'Hidden', N'Прихований', 'crm'
exec insertLanguageInterface 'en', 'Show hidden', N'Show hidden', 'crm'
exec insertLanguageInterface 'ru', 'Show hidden', N'Показать скрытые', 'crm'
exec insertLanguageInterface 'uk', 'Show hidden', N'Показати приховані', 'crm'
exec insertLanguageInterface 'en', 'Required comment', N'Required comment', 'crm'
exec insertLanguageInterface 'ru', 'Required comment', N'Обязательный комментарий', 'crm'
exec insertLanguageInterface 'uk', 'Required comment', N'Обов''язковий коментар', 'crm'
exec insertLanguageInterface 'en', 'Goal', N'Goal', 'crm'
exec insertLanguageInterface 'ru', 'Goal', N'Цель', 'crm'
exec insertLanguageInterface 'uk', 'Goal', N'Ціль', 'crm'
exec insertLanguageInterface 'en', 'Enable sorting', N'Enable sorting', 'crm'
exec insertLanguageInterface 'ru', 'Enable sorting', N'Включить сортировку', 'crm'
exec insertLanguageInterface 'uk', 'Enable sorting', N'Ввімкнути сортування', 'crm'
exec insertLanguageInterface 'en', 'Alert', N'Alert', 'crm'
exec insertLanguageInterface 'ru', 'Alert', N'Предупреждение', 'crm'
exec insertLanguageInterface 'uk', 'Alert', N'Сповіщення', 'crm'
exec insertLanguageInterface 'en', 'Item cannot be deleted', N'Item cannot be deleted', 'crm'
exec insertLanguageInterface 'ru', 'Item cannot be deleted', N'Запись не может быть удалена', 'crm'
exec insertLanguageInterface 'uk', 'Item cannot be deleted', N'Запис не можна видалити', 'crm'
exec insertLanguageInterface 'en', 'Are you sure you want to delete this item?', N'Are you sure you want to delete this item?', 'crm'
exec insertLanguageInterface 'ru', 'Are you sure you want to delete this item?', N'Вы уверены, что хотите удалить эту запись?', 'crm'
exec insertLanguageInterface 'uk', 'Are you sure you want to delete this item?', N'Ви впевнені, що хочете видалити цей запис?', 'crm'
exec insertLanguageInterface 'en', 'Doctor', N'Doctor', 'crm'
exec insertLanguageInterface 'ru', 'Doctor', N'Врач', 'crm'
exec insertLanguageInterface 'uk', 'Doctor', N'Лікар', 'crm'
exec insertLanguageInterface 'en', 'Show previous', N'Show previous', 'crm'
exec insertLanguageInterface 'ru', 'Show previous', N'Показать предыдущий', 'crm'
exec insertLanguageInterface 'uk', 'Show previous', N'Показати попередній', 'crm'
exec insertLanguageInterface 'en', 'Positions', 'Positions', 'crm';
exec insertLanguageInterface 'ru', 'Positions', 'Должности', 'crm';
exec insertLanguageInterface 'uk', 'Positions', 'Посади', 'crm';
exec insertLanguageInterface 'uk', 'Task types', 'Типи візитів', 'crm';
exec insertLanguageInterface 'ru', 'Task types', 'Типы визитов', 'crm';
exec insertLanguageInterface 'en', 'Task types', 'Task types', 'crm';
GO
------------------------------------------------------------------------------------------------------------------------
--- https://teamsoft.atlassian.net/browse/DRREDDYS-527
------------------------------------------------------------------------------------------------------------------------
IF NOT EXISTS (SELECT 1 FROM information_schema.columns WHERE table_name = 'info_salefact' AND column_name = 'sale_type')
    BEGIN
        ALTER TABLE info_salefact ADD sale_type VARCHAR(50)
    END
GO

IF NOT EXISTS(SELECT 1 FROM sys.indexes
              WHERE object_id = OBJECT_ID('info_salefact', 'U')
                AND name='ix_info_salefact_sale_type')
    BEGIN
        create index ix_info_salefact_sale_type on info_salefact (sale_type)
    END
GO
------------------------------------------------------------------------------------------------------------------------
--- https://teamsoft.atlassian.net/browse/FARMAK-315
------------------------------------------------------------------------------------------------------------------------
exec insertLanguageInterface 'en', 'Medical representative', N'Medical representative'
exec insertLanguageInterface 'ru', 'Medical representative', N'Медицинский представитель'
exec insertLanguageInterface 'uk', 'Medical representative', N'Медичний представник'
exec insertLanguageInterface 'en', 'Regional representative', N'Regional representative'
exec insertLanguageInterface 'ru', 'Regional representative', N'Региональный представитель'
exec insertLanguageInterface 'uk', 'Regional representative', N'Регіональний представник'
GO
------------------------------------------------------------------------------------------------------------------------
---https://teamsoft.atlassian.net/browse/EGISRU-219
------------------------------------------------------------------------------------------------------------------------
IF NOT EXISTS (SELECT 1 FROM information_schema.columns
               WHERE table_name = 'info_paramfortask' AND column_name = 'isshowprev')
    BEGIN
        ALTER TABLE info_paramfortask ADD isshowprev INT NULL
    END
GO


exec insertLanguageInterface 'en', 'Show previous', N'Show previous', 'crm'
exec insertLanguageInterface 'ru', 'Show previous', N'Показать предыдущий', 'crm'
exec insertLanguageInterface 'uk', 'Show previous', N'Показати попередній', 'crm'
exec insertLanguageInterface 'en', 'Previous task value', N'Previous task value', 'crm'
exec insertLanguageInterface 'ru', 'Previous task value', N'Значение предыдущего визита', 'crm'
exec insertLanguageInterface 'uk', 'Previous task value', N'Значення попереднього візиту', 'crm'
GO
------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------
--- https://teamsoft.atlassian.net/browse/MCM-395
------------------------------------------------------------------------------------------------------------------------
exec insertLanguageInterface 'en', 'Listen to audio', N'Listen to audio'
exec insertLanguageInterface 'ru', 'Listen to audio', N'Прослушать аудио'
exec insertLanguageInterface 'uk', 'Listen to audio', N'Прослухати аудіо'
GO

------------------------------------------------------------------------------------------------------------------------
---https://teamsoft.atlassian.net/browse/DRREDDYS-414
------------------------------------------------------------------------------------------------------------------------
exec insertLanguageInterface 'en', 'Alien', N'Alien', 'targeting'
exec insertLanguageInterface 'ru', 'Alien', N'Чужой', 'targeting'
exec insertLanguageInterface 'uk', 'Alien', N'Чужий', 'targeting'
GO
------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------
---https://teamsoft.atlassian.net/browse/PHARMAWEB-448
------------------------------------------------------------------------------------------------------------------------
IF NOT EXISTS (SELECT 1 FROM information_schema.columns
               WHERE table_name = 'info_reporttabfield' AND column_name = 'style_color')
    BEGIN
        ALTER TABLE info_reporttabfield ADD style_color varchar(255) null
    END
GO

IF NOT EXISTS (SELECT 1 FROM information_schema.columns
               WHERE table_name = 'info_reporttabfield' AND column_name = 'style_background')
    BEGIN
        ALTER TABLE info_reporttabfield ADD style_background varchar(255) null
    END
GO

IF NOT EXISTS (SELECT 1 FROM information_schema.columns
               WHERE table_name = 'info_reporttabfield' AND column_name = 'style_condition')
    BEGIN
        ALTER TABLE info_reporttabfield ADD style_condition varchar(255) null
    END
GO

IF NOT EXISTS (SELECT 1 FROM information_schema.columns
               WHERE table_name = 'info_reporttabfield' AND column_name = 'style_value1')
    BEGIN
        ALTER TABLE info_reporttabfield ADD style_value1 varchar(255) null
    END
GO

IF NOT EXISTS (SELECT 1 FROM information_schema.columns
               WHERE table_name = 'info_reporttabfield' AND column_name = 'style_value2')
    BEGIN
        ALTER TABLE info_reporttabfield ADD style_value2 varchar(255) null
    END
GO

IF NOT EXISTS (SELECT 1 FROM information_schema.columns
               WHERE table_name = 'info_reporttabfield' AND column_name = 'is_pivot')
    BEGIN
        ALTER TABLE info_reporttabfield ADD [is_pivot] int null
    END
GO


-------
-- https://teamsoft.atlassian.net/browse/MCM-348
-------
IF OBJECT_ID(N'[info_mcmconference]', 'U') IS NULL
    BEGIN
        CREATE TABLE [dbo].[info_mcmconference]
        (
            [id]              [int] IDENTITY (1,1) NOT NULL,
            [name]            [varchar](255)       NULL,
            [code]            [varchar](255)       NULL,
            [datefrom]        [datetime]           NULL,
            [gmt_offset]      [int]                NULL,
            [url]             [varchar](max)       NULL,
            [pdf]             [varchar](max)       NULL,
            [room]            [varchar](max)       NULL,
            [group_id]        [int]                NULL,
            [owner_id]        [int]                NULL,
            [currenttime]     [datetime]           NULL,
            [time]            [timestamp]          NULL,
            [moduser]         [varchar](16)        NULL,
            [guid]            [uniqueidentifier]   NULL,
            CONSTRAINT [PK_info_mcmconference] PRIMARY KEY CLUSTERED
                (
                 [id] ASC
                    ) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
        ) ON [PRIMARY]

        ALTER TABLE [dbo].[info_mcmconference]
            WITH CHECK ADD CONSTRAINT [FK_info_mcmconference_group_id] FOREIGN KEY ([group_id])
            REFERENCES [dbo].[info_CustomDictionaryValue] ([id])
        ALTER TABLE [dbo].[info_mcmconference]
            WITH CHECK ADD CONSTRAINT [FK_info_mcmconference_owner_id] FOREIGN KEY ([owner_id])
            REFERENCES [dbo].[info_user] ([id])

        ALTER TABLE [dbo].[info_mcmconference]
            ADD CONSTRAINT [DF_info_mcmconferencecurrenttime] DEFAULT (getdate()) FOR [currenttime]
        ALTER TABLE [dbo].[info_mcmconference]
            ADD CONSTRAINT [DF_info_mcmconferencemoduser] DEFAULT ([dbo].[Get_CurrentCode]()) FOR [moduser]
        ALTER TABLE [dbo].[info_mcmconference]
            ADD CONSTRAINT [DF_info_mcmconferenceguid] DEFAULT (newid()) FOR [guid]
    END
go

IF OBJECT_ID(N'[info_mcmconferenceformat]', 'U') IS NULL
    BEGIN
        CREATE TABLE [dbo].[info_mcmconferenceformat]
        (
            [id]             [int] IDENTITY (1,1) NOT NULL,
            [conference_id]  int                  NOT NULL,
            [format_id]      int                  NOT NULL,
            [currenttime]     [datetime]           NULL,
            [time]            [timestamp]          NULL,
            [moduser]         [varchar](16)        NULL,
            [guid]            [uniqueidentifier]   NULL,
            CONSTRAINT [PK_info_mcmconferenceformat] PRIMARY KEY CLUSTERED
                (
                 [id] ASC
                    ) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
        ) ON [PRIMARY]

        ALTER TABLE [dbo].[info_mcmconferenceformat]
            WITH CHECK ADD CONSTRAINT [FK_info_mcmconferenceformat_info_mcmconference] FOREIGN KEY ([conference_id])
                REFERENCES [dbo].[info_mcmconference] ([id])

        ALTER TABLE [dbo].[info_mcmconferenceformat]
            WITH CHECK ADD CONSTRAINT [FK_info_mcmconferenceformat_info_customdictionaryvalue] FOREIGN KEY ([format_id])
                REFERENCES [dbo].[info_customdictionaryvalue] ([id])

        ALTER TABLE [dbo].[info_mcmconferenceformat]
            ADD CONSTRAINT [DF_info_mcmconferenceformatcurrenttime] DEFAULT (getdate()) FOR [currenttime]
        ALTER TABLE [dbo].[info_mcmconferenceformat]
            ADD CONSTRAINT [DF_info_mcmconferenceformatmoduser] DEFAULT ([dbo].[Get_CurrentCode]()) FOR [moduser]
        ALTER TABLE [dbo].[info_mcmconferenceformat]
            ADD CONSTRAINT [DF_info_mcmconferenceformatguid] DEFAULT (newid()) FOR [guid]
    END
GO

IF OBJECT_ID(N'[info_mcmconferenceresponsible]', 'U') IS NULL
    BEGIN
        CREATE TABLE [dbo].[info_mcmconferenceresponsible]
        (
            [id]             [int] IDENTITY (1,1) NOT NULL,
            [conference_id]  int                  NOT NULL,
            [user_id]        int                  NOT NULL,
            [currenttime]     [datetime]           NULL,
            [time]            [timestamp]          NULL,
            [moduser]         [varchar](16)        NULL,
            [guid]            [uniqueidentifier]   NULL,
            CONSTRAINT [PK_info_mcmconferenceresponsible] PRIMARY KEY CLUSTERED
                (
                 [id] ASC
                    ) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
        ) ON [PRIMARY]

        ALTER TABLE [dbo].[info_mcmconferenceresponsible]
            WITH CHECK ADD CONSTRAINT [FK_info_mcmconferenceresponsible_info_mcmconference] FOREIGN KEY ([conference_id])
                REFERENCES [dbo].[info_mcmconference] ([id])

        ALTER TABLE [dbo].[info_mcmconferenceresponsible]
            WITH CHECK ADD CONSTRAINT [FK_info_mcmconferenceresponsible_info_user] FOREIGN KEY ([user_id])
                REFERENCES [dbo].[info_user] ([id])

        ALTER TABLE [dbo].[info_mcmconferenceresponsible]
            ADD CONSTRAINT [DF_info_mcmconferenceresponsiblecurrenttime] DEFAULT (getdate()) FOR [currenttime]
        ALTER TABLE [dbo].[info_mcmconferenceresponsible]
            ADD CONSTRAINT [DF_info_mcmconferenceresponsiblemoduser] DEFAULT ([dbo].[Get_CurrentCode]()) FOR [moduser]
        ALTER TABLE [dbo].[info_mcmconferenceresponsible]
            ADD CONSTRAINT [DF_info_mcmconferenceresponsibleguid] DEFAULT (newid()) FOR [guid]
    END
GO

IF OBJECT_ID(N'[info_mcmconferencedetail]', 'U') IS NULL
    BEGIN
        CREATE TABLE [dbo].[info_mcmconferencedetail]
        (
            [id]             [int] IDENTITY (1,1) NOT NULL,
            [conference_id]  int                  NULL,
            [country_id]     int                  NOT NULL,
            [promocode]      [varchar](255)       NULL,
            [url]            [varchar](max)       NULL,
            [currenttime]    [datetime]           NULL,
            [time]           [timestamp]          NULL,
            [moduser]        [varchar](16)        NULL,
            [guid]           [uniqueidentifier]   NULL,
            CONSTRAINT [PK_info_mcmconferencedetail] PRIMARY KEY CLUSTERED
                (
                 [id] ASC
                    ) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
        ) ON [PRIMARY]

        ALTER TABLE [dbo].[info_mcmconferencedetail]
            WITH CHECK ADD CONSTRAINT [FK_info_mcmconferencedetail_info_mcmconference] FOREIGN KEY ([conference_id])
                REFERENCES [dbo].[info_mcmconference] ([id])

        ALTER TABLE [dbo].[info_mcmconferencedetail]
            WITH CHECK ADD CONSTRAINT [FK_info_mcmconferencedetail_info_customdictionaryvalue] FOREIGN KEY ([country_id])
                REFERENCES [dbo].[info_customdictionaryvalue] ([id])

        ALTER TABLE [dbo].[info_mcmconferencedetail]
            ADD CONSTRAINT [DF_info_mcmconferencedetailcurrenttime] DEFAULT (getdate()) FOR [currenttime]
        ALTER TABLE [dbo].[info_mcmconferencedetail]
            ADD CONSTRAINT [DF_info_mcmconferencedetailmoduser] DEFAULT ([dbo].[Get_CurrentCode]()) FOR [moduser]
        ALTER TABLE [dbo].[info_mcmconferencedetail]
            ADD CONSTRAINT [DF_info_mcmconferencedetailguid] DEFAULT (newid()) FOR [guid]
    END
GO

IF OBJECT_ID(N'[info_mcmconferencedetailformat]', 'U') IS NULL
    BEGIN
        CREATE TABLE [dbo].[info_mcmconferencedetailformat]
        (
            [id]             [int] IDENTITY (1,1) NOT NULL,
            [detail_id]      int                  NOT NULL,
            [format_id]      int                  NOT NULL,
            [price]          int                  NULL,
            [currency]       [varchar](255)       NULL,
            [currenttime]    [datetime]           NULL,
            [time]           [timestamp]          NULL,
            [moduser]        [varchar](16)        NULL,
            [guid]           [uniqueidentifier]   NULL,
            CONSTRAINT [PK_info_mcmconferencedetailformat] PRIMARY KEY CLUSTERED
                (
                 [id] ASC
                    ) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
        ) ON [PRIMARY]

        ALTER TABLE [dbo].[info_mcmconferencedetailformat]
            WITH CHECK ADD CONSTRAINT [FK_info_mcmconferencedetailformat_info_mcmconferencedetail] FOREIGN KEY ([detail_id])
                REFERENCES [dbo].[info_mcmconferencedetail] ([id]) ON DELETE CASCADE

        ALTER TABLE [dbo].[info_mcmconferencedetailformat]
            WITH CHECK ADD CONSTRAINT [FK_info_mcmconferencedetailformat_info_customdictionaryvalue] FOREIGN KEY ([format_id])
                REFERENCES [dbo].[info_customdictionaryvalue] ([id])

        ALTER TABLE [dbo].[info_mcmconferencedetailformat]
            ADD CONSTRAINT [DF_info_mcmconferencedetailformatcurrenttime] DEFAULT (getdate()) FOR [currenttime]
        ALTER TABLE [dbo].[info_mcmconferencedetailformat]
            ADD CONSTRAINT [DF_info_mcmconferencedetailformatmoduser] DEFAULT ([dbo].[Get_CurrentCode]()) FOR [moduser]
        ALTER TABLE [dbo].[info_mcmconferencedetailformat]
            ADD CONSTRAINT [DF_info_mcmconferencedetailformatguid] DEFAULT (newid()) FOR [guid]
    END
GO

IF OBJECT_ID(N'[info_mcmconferencevideo]', 'U') IS NULL
    BEGIN
        CREATE TABLE [dbo].[info_mcmconferencevideo]
        (
            [id]             [int] IDENTITY (1,1) NOT NULL,
            [conference_id]  int                  NULL,
            [url]            [varchar](max)       NULL,
            [currenttime]    [datetime]           NULL,
            [time]           [timestamp]          NULL,
            [moduser]        [varchar](16)        NULL,
            [guid]           [uniqueidentifier]   NULL,
            CONSTRAINT [PK_info_mcmconferencevideo] PRIMARY KEY CLUSTERED
                (
                 [id] ASC
                    ) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
        ) ON [PRIMARY]

        ALTER TABLE [dbo].[info_mcmconferencevideo]
            WITH CHECK ADD CONSTRAINT [FK_info_mcmconferencevideo_info_mcmconference] FOREIGN KEY ([conference_id])
                REFERENCES [dbo].[info_mcmconference] ([id])

        ALTER TABLE [dbo].[info_mcmconferencevideo]
            ADD CONSTRAINT [DF_info_mcmconferencevideocurrenttime] DEFAULT (getdate()) FOR [currenttime]
        ALTER TABLE [dbo].[info_mcmconferencevideo]
            ADD CONSTRAINT [DF_info_mcmconferencevideomoduser] DEFAULT ([dbo].[Get_CurrentCode]()) FOR [moduser]
        ALTER TABLE [dbo].[info_mcmconferencevideo]
            ADD CONSTRAINT [DF_info_mcmconferencevideoguid] DEFAULT (newid()) FOR [guid]
    END
GO

IF OBJECT_ID(N'[info_mcmconferencecontact]', 'U') IS NULL
    BEGIN
        CREATE TABLE [dbo].[info_mcmconferencecontact]
        (
            [id]             [int] IDENTITY (1,1) NOT NULL,
            [conference_id]  [int]                NOT NULL,
            [name]           [varchar](255)       NULL,
            [position]       [varchar](255)       NULL,
            [language_id]    [int]                NULL,
            [company]        [varchar](255)       NULL,
            [address]        [varchar](255)       NULL,
            [egrpou]         [varchar](255)       NULL,
            [phone]          [varchar](255)       NULL,
            [email]          [varchar](255)       NULL,
            [country]        [varchar](255)       NULL,
            [promocode]      [varchar](255)       NULL,
            [price]          [int]                NULL,
            [hidden]         [int]                NULL,
            [currency]       [varchar](255)       NULL,
            [currenttime]    [datetime]           NULL,
            [time]           [timestamp]          NULL,
            [moduser]        [varchar](16)        NULL,
            [guid]           [uniqueidentifier]   NULL,
            CONSTRAINT [PK_info_mcmconferencecontact] PRIMARY KEY CLUSTERED
                (
                 [id] ASC
                    ) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
        ) ON [PRIMARY]

        ALTER TABLE [dbo].[info_mcmconferencecontact]
            WITH CHECK ADD CONSTRAINT [FK_info_mcmconferencecontact_info_mcmconference] FOREIGN KEY ([conference_id])
                REFERENCES [dbo].[info_mcmconference] ([id])
        ALTER TABLE [dbo].[info_mcmconferencecontact]
            WITH CHECK ADD CONSTRAINT [FK_info_mcmconferencecontact_info_language] FOREIGN KEY ([language_id])
                REFERENCES [dbo].[info_language] ([id])

        ALTER TABLE [dbo].[info_mcmconferencecontact]
            ADD CONSTRAINT [DF_info_mcmconferencecontactcurrenttime] DEFAULT (getdate()) FOR [currenttime]
        ALTER TABLE [dbo].[info_mcmconferencecontact]
            ADD CONSTRAINT [DF_info_mcmconferencecontactmoduser] DEFAULT ([dbo].[Get_CurrentCode]()) FOR [moduser]
        ALTER TABLE [dbo].[info_mcmconferencecontact]
            ADD CONSTRAINT [DF_info_mcmconferencecontactguid] DEFAULT (newid()) FOR [guid]
    END
GO

IF OBJECT_ID(N'[info_mcmconferencemember]', 'U') IS NULL
    BEGIN
        CREATE TABLE [dbo].[info_mcmconferencemember]
        (
            [id]                    [int] IDENTITY (1,1) NOT NULL,
            [conferencecontact_id]  [int]                NOT NULL,
            [contact_id]            [int]                NOT NULL,
            [language_id]           [int]                NULL,
            [format_id]             [int]                NULL,
            [price]                 [int]                NULL,
            [currency]              [varchar](255)       NULL,
            [role]                  [varchar](255)       NULL,
            [last_date]             [datetime]           NULL,
            [last_subject]          [varchar](255)       NULL,
            [token_received]        [int]                NULL,
            [access_conference]     [int]                NULL,
            [prev_access_conference] [int]               NULL,
            [access_video]          [int]                NULL,
            [access_pdf]            [int]                NULL,
            [certificate]           [int]                NULL,
            [code_conf]             [varchar](255)       NULL,
            [user_name]             [varchar](255)       NULL,
            [user_pass]             [varchar](255)       NULL,
            [user_link]             [varchar](255)       NULL,
            [currenttime]           [datetime]           NULL,
            [time]                  [timestamp]          NULL,
            [moduser]               [varchar](16)        NULL,
            [guid]                  [uniqueidentifier]   NULL,
            CONSTRAINT [PK_info_mcmconferencemember] PRIMARY KEY CLUSTERED
                (
                 [id] ASC
                    ) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
        ) ON [PRIMARY]

        ALTER TABLE [dbo].[info_mcmconferencemember]
            WITH CHECK ADD CONSTRAINT [FK_info_mcmconferencemember_info_mcmconferencecontact] FOREIGN KEY ([conferencecontact_id])
                REFERENCES [dbo].[info_mcmconferencecontact] ([id])
        ALTER TABLE [dbo].[info_mcmconferencemember]
            WITH CHECK ADD CONSTRAINT [FK_info_mcmconferencemember_info_contact] FOREIGN KEY ([contact_id])
                REFERENCES [dbo].[info_contact] ([id])
        ALTER TABLE [dbo].[info_mcmconferencemember]
            WITH CHECK ADD CONSTRAINT [FK_info_mcmconferencemember_info_customdictionaryvalue] FOREIGN KEY ([format_id])
                REFERENCES [dbo].[info_customdictionaryvalue] ([id])
        ALTER TABLE [dbo].[info_mcmconferencemember]
            WITH CHECK ADD CONSTRAINT [FK_info_mcmconferencemember_info_language] FOREIGN KEY ([language_id])
                REFERENCES [dbo].[info_language] ([id])

        ALTER TABLE [dbo].[info_mcmconferencemember]
            ADD CONSTRAINT [DF_info_mcmconferencemembercurrenttime] DEFAULT (getdate()) FOR [currenttime]
        ALTER TABLE [dbo].[info_mcmconferencemember]
            ADD CONSTRAINT [DF_info_mcmconferencemembermoduser] DEFAULT ([dbo].[Get_CurrentCode]()) FOR [moduser]
        ALTER TABLE [dbo].[info_mcmconferencemember]
            ADD CONSTRAINT [DF_info_mcmconferencememberguid] DEFAULT (newid()) FOR [guid]
    END
GO

IF NOT EXISTS (SELECT 1 FROM information_schema.columns WHERE table_name = 'info_mcmdistribution' AND column_name = 'conference_id')
    begin
        alter table [dbo].[info_mcmdistribution] add conference_id [int] null;

        ALTER TABLE [dbo].[info_mcmdistribution]
            WITH CHECK ADD CONSTRAINT [FK_info_mcmdistribution_info_mcmconference] FOREIGN KEY ([conference_id])
                REFERENCES [dbo].[info_mcmconference] ([id])
    end
GO

if not exists(SELECT 1
              FROM INFORMATION_SCHEMA.COLUMNS
              WHERE TABLE_NAME = 'info_mcmdistributionaction'
                AND Column_Name = 'conferencecontact_id')
    begin
        alter table [dbo].[info_mcmdistributionaction] add conferencecontact_id [int] null;

        ALTER TABLE [dbo].[info_mcmdistributionaction]
            WITH CHECK ADD CONSTRAINT [FK_info_mcmdistributionaction_info_mcmconferencecontact] FOREIGN KEY ([conferencecontact_id])
                REFERENCES [dbo].[info_mcmconferencecontact] ([id])
    end
GO

if not exists(select *
              from info_CustomDictionary
              where code = 'mcm_conference_format')
    begin
        insert info_CustomDictionary (code) values ('mcm_conference_format')
        insert info_CustomDictionaryValue (CustomDictionary_Id, DictionaryValue)
        values ((SELECT TOP 1 id FROM info_CustomDictionary Where code = 'mcm_conference_format'), 'Оффлайн'),
               ((SELECT TOP 1 id FROM info_CustomDictionary Where code = 'mcm_conference_format'), 'Онлайн (трансляция, презентации)'),
               ((SELECT TOP 1 id FROM info_CustomDictionary Where code = 'mcm_conference_format'), 'Онлайн[ОД2] (презентации+видео)'),
               ((SELECT TOP 1 id FROM info_CustomDictionary Where code = 'mcm_conference_format'), 'Онлайн образование')
    end
GO

if not exists(select *
              from info_CustomDictionary
              where code = 'mcm_conference_country')
    begin
        insert info_CustomDictionary (code) values ('mcm_conference_country')
        insert info_CustomDictionaryValue (CustomDictionary_Id, DictionaryValue)
        values ((SELECT TOP 1 id FROM info_CustomDictionary Where code = 'mcm_conference_country'), 'Беларусь'),
               ((SELECT TOP 1 id FROM info_CustomDictionary Where code = 'mcm_conference_country'), 'Казахстан'),
               ((SELECT TOP 1 id FROM info_CustomDictionary Where code = 'mcm_conference_country'), 'Россия'),
               ((SELECT TOP 1 id FROM info_CustomDictionary Where code = 'mcm_conference_country'), 'Узбекистан'),
               ((SELECT TOP 1 id FROM info_CustomDictionary Where code = 'mcm_conference_country'), 'Украина'),
               ((SELECT TOP 1 id FROM info_CustomDictionary Where code = 'mcm_conference_country'), 'Другая страна')
    end
GO

if not exists(select *
              from info_CustomDictionary
              where code = 'mcm_conference_role')
    begin
        insert info_CustomDictionary (code) values ('mcm_conference_role')
        insert info_CustomDictionaryValue (CustomDictionary_Id, DictionaryValue, code)
        values ((SELECT TOP 1 id FROM info_CustomDictionary Where code = 'mcm_conference_role'), 'Участник', 'participant'),
               ((SELECT TOP 1 id FROM info_CustomDictionary Where code = 'mcm_conference_role'), 'VIP-участник', null),
               ((SELECT TOP 1 id FROM info_CustomDictionary Where code = 'mcm_conference_role'), 'Спикер', null),
               ((SELECT TOP 1 id FROM info_CustomDictionary Where code = 'mcm_conference_role'), 'Модератор чата', null),
               ((SELECT TOP 1 id FROM info_CustomDictionary Where code = 'mcm_conference_role'), 'Спонсор', null),
               ((SELECT TOP 1 id FROM info_CustomDictionary Where code = 'mcm_conference_role'), 'Сотрудник', null)
    end
GO

if not exists(select *
              from info_CustomDictionary
              where code = 'mcm_conference_group')
    begin
        insert info_CustomDictionary (code) values ('mcm_conference_group')
        insert info_CustomDictionaryValue (CustomDictionary_Id, DictionaryValue)
        values ((SELECT TOP 1 id FROM info_CustomDictionary Where code = 'mcm_conference_group'), 'УкрКoмЭкспо'),
               ((SELECT TOP 1 id FROM info_CustomDictionary Where code = 'mcm_conference_group'), 'Морион'),
               ((SELECT TOP 1 id FROM info_CustomDictionary Where code = 'mcm_conference_group'), 'ProximaResearch')
    end
GO

IF OBJECT_ID(N'[info_mcmconferencerole]', 'U') IS NULL
    BEGIN
        CREATE TABLE [dbo].[info_mcmconferencerole]
        (
            [id]             [int] IDENTITY (1,1) NOT NULL,
            [conference_id]  int                  NOT NULL,
            [role_id]      int                  NOT NULL,
            CONSTRAINT [PK_info_mcmconferencerole] PRIMARY KEY CLUSTERED
                (
                 [id] ASC
                    ) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
        ) ON [PRIMARY]

        ALTER TABLE [dbo].[info_mcmconferencerole]
            WITH CHECK ADD CONSTRAINT [FK_info_mcmconferencerole_info_mcmconference] FOREIGN KEY ([conference_id])
                REFERENCES [dbo].[info_mcmconference] ([id])

        ALTER TABLE [dbo].[info_mcmconferencerole]
            WITH CHECK ADD CONSTRAINT [FK_info_mcmconferencerole_info_customdictionaryvalue] FOREIGN KEY ([role_id])
                REFERENCES [dbo].[info_customdictionaryvalue] ([id])
    END
GO

IF OBJECT_ID(N'[info_mcmconferencepackage]', 'U') IS NULL
    BEGIN
        CREATE TABLE [dbo].[info_mcmconferencepackage]
        (
            [id]             [int] IDENTITY (1,1) NOT NULL,
            [conference_id]  [int]                NOT NULL,
            [name]           [varchar](255)       NULL,
            [description]    [varchar](max)       NULL,
            [price]          [int]                NULL,
            [currency]       [varchar](255)       NULL,
            [promocode]      [varchar](255)       NULL,
            [currenttime]    [datetime]           NULL,
            [time]           [timestamp]          NULL,
            [moduser]        [varchar](16)        NULL,
            [guid]           [uniqueidentifier]   NULL,
            CONSTRAINT [PK_info_mcmconferencepackage] PRIMARY KEY CLUSTERED
                (
                 [id] ASC
                    ) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
        ) ON [PRIMARY]

        ALTER TABLE [dbo].[info_mcmconferencepackage]
            WITH CHECK ADD CONSTRAINT [FK_info_mcmconferencepackage_info_mcmconference] FOREIGN KEY ([conference_id])
                REFERENCES [dbo].[info_mcmconference] ([id])

        ALTER TABLE [dbo].[info_mcmconferencepackage]
            ADD CONSTRAINT [DF_info_mcmconferencepackagecurrenttime] DEFAULT (getdate()) FOR [currenttime]
        ALTER TABLE [dbo].[info_mcmconferencepackage]
            ADD CONSTRAINT [DF_info_mcmconferencepackagemoduser] DEFAULT ([dbo].[Get_CurrentCode]()) FOR [moduser]
        ALTER TABLE [dbo].[info_mcmconferencepackage]
            ADD CONSTRAINT [DF_info_mcmconferencepackageguid] DEFAULT (newid()) FOR [guid]
    END
GO

declare @SERVICEID int
set @SERVICEID = (SELECT id FROM info_service WHERE identifier = 'mcm')
IF NOT EXISTS (SELECT 1 FROM info_serviceprivilege WHERE parent_id IS NULL AND service_id = @SERVICEID)
    INSERT INTO info_serviceprivilege(name, service_id, code) VALUES('Access to MCM', @SERVICEID, 'ROLE_ACCESS_TO_MCM')

declare @SERVICE_PRIVILEGE_MAIN_ID int
set @SERVICE_PRIVILEGE_MAIN_ID = (SELECT id FROM info_serviceprivilege WHERE parent_id IS NULL AND service_id = @SERVICEID)

IF NOT EXISTS (SELECT 1 FROM info_serviceprivilege WHERE parent_id = @SERVICE_PRIVILEGE_MAIN_ID AND code = 'ROLE_ACCESS_TO_MCM_CONTENT')
    INSERT INTO info_serviceprivilege(name, code, service_id, parent_id) VALUES('Access to MCM Content', 'ROLE_ACCESS_TO_MCM_CONTENT', @SERVICEID, @SERVICE_PRIVILEGE_MAIN_ID)
IF NOT EXISTS (SELECT 1 FROM info_serviceprivilege WHERE parent_id = @SERVICE_PRIVILEGE_MAIN_ID AND code = 'ROLE_ACCESS_TO_MCM_TARGET')
    INSERT INTO info_serviceprivilege(name, code, service_id, parent_id) VALUES('Access to MCM Target', 'ROLE_ACCESS_TO_MCM_TARGET', @SERVICEID, @SERVICE_PRIVILEGE_MAIN_ID)
IF NOT EXISTS (SELECT 1 FROM info_serviceprivilege WHERE parent_id = @SERVICE_PRIVILEGE_MAIN_ID AND code = 'ROLE_ACCESS_TO_MCM_DISTRIBUTION')
    INSERT INTO info_serviceprivilege(name, code, service_id, parent_id) VALUES('Access to MCM Distribution', 'ROLE_ACCESS_TO_MCM_DISTRIBUTION', @SERVICEID, @SERVICE_PRIVILEGE_MAIN_ID)
IF NOT EXISTS (SELECT 1 FROM info_serviceprivilege WHERE parent_id = @SERVICE_PRIVILEGE_MAIN_ID AND code = 'ROLE_ACCESS_TO_MCM_REPORT')
    INSERT INTO info_serviceprivilege(name, code, service_id, parent_id) VALUES('Access to MCM Report', 'ROLE_ACCESS_TO_MCM_REPORT', @SERVICEID, @SERVICE_PRIVILEGE_MAIN_ID)
IF NOT EXISTS (SELECT 1 FROM info_serviceprivilege WHERE parent_id = @SERVICE_PRIVILEGE_MAIN_ID AND code = 'ROLE_ACCESS_TO_MCM_CONFERENCE')
    INSERT INTO info_serviceprivilege(name, code, service_id, parent_id) VALUES('Access to MCM Conference', 'ROLE_ACCESS_TO_MCM_CONFERENCE', @SERVICEID, @SERVICE_PRIVILEGE_MAIN_ID)

IF NOT EXISTS (SELECT 1 FROM information_schema.columns WHERE table_name = 'info_mcmdistribution' AND column_name = 'owner_id')
    BEGIN
        ALTER TABLE info_mcmdistribution ADD owner_id INT NULL;
        ALTER TABLE [dbo].[info_mcmdistribution]
            WITH CHECK ADD CONSTRAINT [FK_info_mcmdistribution_owner_id] FOREIGN KEY ([owner_id])
                REFERENCES [dbo].[info_user] ([id])
    END
GO

exec insertLanguageInterface  'en', 'Conferences', N'Conferences', 'messages'
exec insertLanguageInterface  'ru', 'Conferences', N'Конференции', 'messages'
exec insertLanguageInterface  'uk', 'Conferences', N'Конференції', 'messages'
GO

exec insertLanguageInterface  'en', 'Conference', N'Conference', 'messages'
exec insertLanguageInterface  'ru', 'Conference', N'Конференция', 'messages'
exec insertLanguageInterface  'uk', 'Conference', N'Конференція', 'messages'
GO

exec insertLanguageInterface  'en', 'Participation format', N'Participation format', 'messages'
exec insertLanguageInterface  'ru', 'Participation format', N'Формат участия', 'messages'
exec insertLanguageInterface  'uk', 'Participation format', N'Формат участі', 'messages'
GO

exec insertLanguageInterface  'en', 'Conference code', N'Conference code', 'messages'
exec insertLanguageInterface  'ru', 'Conference code', N'Код конференции', 'messages'
exec insertLanguageInterface  'uk', 'Conference code', N'Код конференції', 'messages'
GO

exec insertLanguageInterface  'en', 'Start date', N'Start date', 'messages'
exec insertLanguageInterface  'ru', 'Start date', N'Дата начала', 'messages'
exec insertLanguageInterface  'uk', 'Start date', N'Дата початку', 'messages'
GO

exec insertLanguageInterface  'en', 'Hours', N'Hours', 'messages'
exec insertLanguageInterface  'ru', 'Hours', N'Часы', 'messages'
exec insertLanguageInterface  'uk', 'Hours', N'Години', 'messages'
GO

exec insertLanguageInterface  'en', 'Minutes', N'Minutes', 'messages'
exec insertLanguageInterface  'ru', 'Minutes', N'Минуты', 'messages'
exec insertLanguageInterface  'uk', 'Minutes', N'Хвилини', 'messages'
GO

exec insertLanguageInterface  'en', 'Sponsorship packages', N'Sponsorship packages', 'messages'
exec insertLanguageInterface  'ru', 'Sponsorship packages', N'Спонсорские пакеты', 'messages'
exec insertLanguageInterface  'uk', 'Sponsorship packages', N'Спонсорські пакети', 'messages'
GO

exec insertLanguageInterface  'en', 'Company group', N'Company group', 'messages'
exec insertLanguageInterface  'ru', 'Company group', N'Группа компании Морион', 'messages'
exec insertLanguageInterface  'uk', 'Company group', N'Група компанії Моріон', 'messages'
GO

exec insertLanguageInterface  'en', 'Participation role', N'Participation role', 'messages'
exec insertLanguageInterface  'ru', 'Participation role', N'Роли участника', 'messages'
exec insertLanguageInterface  'uk', 'Participation role', N'Ролі учасника', 'messages'
GO

exec insertLanguageInterface  'en', 'Currency', N'Currency', 'messages'
exec insertLanguageInterface  'ru', 'Currency', N'Валюта', 'messages'
exec insertLanguageInterface  'uk', 'Currency', N'Валюта', 'messages'
GO

exec insertLanguageInterface  'en', 'Promocode', N'Promocode', 'messages'
exec insertLanguageInterface  'ru', 'Promocode', N'Промокод', 'messages'
exec insertLanguageInterface  'uk', 'Promocode', N'Промокод', 'messages'
GO

exec insertLanguageInterface  'en', 'Add package', N'Add package', 'messages'
exec insertLanguageInterface  'ru', 'Add package', N'Добавить пакет', 'messages'
exec insertLanguageInterface  'uk', 'Add package', N'Додати пакет', 'messages'
GO

exec insertLanguageInterface  'en', 'Conference details', N'Conference details', 'messages'
exec insertLanguageInterface  'ru', 'Conference details', N'Детали конференции', 'messages'
exec insertLanguageInterface  'uk', 'Conference details', N'Деталі конференції', 'messages'
GO

exec insertLanguageInterface  'en', 'Link', N'Link', 'messages'
exec insertLanguageInterface  'ru', 'Link', N'Ссылка', 'messages'
exec insertLanguageInterface  'uk', 'Link', N'Посилання', 'messages'
GO

exec insertLanguageInterface  'en', 'Add country', N'Add country', 'messages'
exec insertLanguageInterface  'ru', 'Add country', N'Добавить страну', 'messages'
exec insertLanguageInterface  'uk', 'Add country', N'Додати країну', 'messages'
GO

exec insertLanguageInterface  'en', 'Stream Url', N'Stream Url', 'messages'
exec insertLanguageInterface  'ru', 'Stream Url', N'Ссылка на стрим', 'messages'
exec insertLanguageInterface  'uk', 'Stream Url', N'Посилання на стрім', 'messages'
GO

exec insertLanguageInterface  'en', 'vMIX/BBB Room', N'vMIX/BBB Room', 'messages'
exec insertLanguageInterface  'ru', 'vMIX/BBB Room', N'Ссылка на vMIX\ ВВВ-комнату', 'messages'
exec insertLanguageInterface  'uk', 'vMIX/BBB Room', N'Посилання на vMIX\ ВВВ-кімнату', 'messages'
GO

exec insertLanguageInterface  'en', 'Pdf Url', N'Pdf Url', 'messages'
exec insertLanguageInterface  'ru', 'Pdf Url', N'Ссылка на pdf', 'messages'
exec insertLanguageInterface  'uk', 'Pdf Url', N'Посилання на pdf', 'messages'
GO

exec insertLanguageInterface  'en', 'Video Url', N'Video Url', 'messages'
exec insertLanguageInterface  'ru', 'Video Url', N'Ссылка на видео', 'messages'
exec insertLanguageInterface  'uk', 'Video Url', N'Посилання на відео', 'messages'
GO

exec insertLanguageInterface  'en', 'Add video', N'Add video', 'messages'
exec insertLanguageInterface  'ru', 'Add video', N'Добавить видео', 'messages'
exec insertLanguageInterface  'uk', 'Add video', N'Додати відео', 'messages'
GO

exec insertLanguageInterface  'en', 'Content by default', N'Content by default', 'messages'
exec insertLanguageInterface  'ru', 'Content by default', N'Контент по умолчанию', 'messages'
exec insertLanguageInterface  'uk', 'Content by default', N'Контент за замовчуванням', 'messages'
GO

IF NOT EXISTS (SELECT 1 FROM information_schema.columns WHERE table_name = 'info_contact' AND column_name = 'language_id')
    BEGIN
        ALTER TABLE info_contact ADD language_id INT NULL;
        ALTER TABLE [dbo].[info_contact]
            WITH CHECK ADD CONSTRAINT [FK_info_contact_language_id] FOREIGN KEY ([language_id])
                REFERENCES [dbo].[info_language] ([id])
    END
GO

IF NOT EXISTS (SELECT 1 FROM information_schema.columns WHERE table_name = 'info_mcmcontentvariable' AND column_name = 'language_id')
    BEGIN
        ALTER TABLE info_mcmcontentvariable ADD language_id INT NULL;
        ALTER TABLE [dbo].[info_mcmcontentvariable]
            WITH CHECK ADD CONSTRAINT [FK_info_mcmcontentvariable_language_id] FOREIGN KEY ([language_id])
                REFERENCES [dbo].[info_language] ([id])
    END
GO

IF OBJECT_ID(N'[info_mcmcontentlanguage]', 'U') IS NULL
    BEGIN
        CREATE TABLE [dbo].[info_mcmcontentlanguage]
        (
            [id]             [int] IDENTITY (1,1) NOT NULL,
            [language_id]    [int]                NOT NULL,
            [content_id]     [int]                NOT NULL,
            [default]        [int]                NULL,
            [caption]        [varchar](255)       NULL,
            [subject]        [varchar](255)       NULL,
            [body]           [varchar](max)       NULL,
            [alternative_body] [varchar](max)     NULL,
            [currenttime]    [datetime]           NULL,
            [time]           [timestamp]          NULL,
            [moduser]        [varchar](16)        NULL,
            [guid]           [uniqueidentifier]   NULL,
            CONSTRAINT [PK_info_mcmcontentlanguage] PRIMARY KEY CLUSTERED
                (
                 [id] ASC
                    ) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
        ) ON [PRIMARY]

        ALTER TABLE [dbo].[info_mcmcontentlanguage]
            WITH CHECK ADD CONSTRAINT [FK_info_mcmcontent_content_id] FOREIGN KEY ([content_id])
                REFERENCES [dbo].[info_mcmcontent] ([id])

        ALTER TABLE [dbo].[info_mcmcontentlanguage]
            WITH CHECK ADD CONSTRAINT [FK_info_mcmcontentlanguage_info_language] FOREIGN KEY ([language_id])
                REFERENCES [dbo].[info_language] ([id])

        ALTER TABLE [dbo].[info_mcmcontentlanguage]
            ADD CONSTRAINT [DF_info_mcmcontentlanguagecurrenttime] DEFAULT (getdate()) FOR [currenttime]
        ALTER TABLE [dbo].[info_mcmcontentlanguage]
            ADD CONSTRAINT [DF_info_mcmcontentlanguagemoduser] DEFAULT ([dbo].[Get_CurrentCode]()) FOR [moduser]
        ALTER TABLE [dbo].[info_mcmcontentlanguage]
            ADD CONSTRAINT [DF_info_mcmcontentlanguageguid] DEFAULT (newid()) FOR [guid]
    END
GO

exec insertLanguageInterface  'en', 'Delete country', N'Delete country', 'messages'
exec insertLanguageInterface  'ru', 'Delete country', N'Удалить страну', 'messages'
exec insertLanguageInterface  'uk', 'Delete country', N'Видалити країну', 'messages'
GO

exec insertLanguageInterface  'en', 'Members', N'Members', 'messages'
exec insertLanguageInterface  'ru', 'Members', N'Участники', 'messages'
exec insertLanguageInterface  'uk', 'Members', N'Учасники', 'messages'
GO

exec insertLanguageInterface  'en', 'Member', N'Member', 'messages'
exec insertLanguageInterface  'ru', 'Member', N'Участник', 'messages'
exec insertLanguageInterface  'uk', 'Member', N'Учасник', 'messages'
GO

exec insertLanguageInterface  'en', 'History', N'History', 'messages'
exec insertLanguageInterface  'ru', 'History', N'История', 'messages'
exec insertLanguageInterface  'uk', 'History', N'Історія', 'messages'
GO

exec insertLanguageInterface  'en', 'Card', N'Card', 'messages'
exec insertLanguageInterface  'ru', 'Card', N'Карточка', 'messages'
exec insertLanguageInterface  'uk', 'Card', N'Картка', 'messages'
GO

exec insertLanguageInterface  'en', 'Contact', N'Contact', 'messages'
exec insertLanguageInterface  'ru', 'Contact', N'Контакт', 'messages'
exec insertLanguageInterface  'uk', 'Contact', N'Контакт', 'messages'
GO

exec insertLanguageInterface  'en', 'Last date', N'Last date', 'messages'
exec insertLanguageInterface  'ru', 'Last date', N'Дата последней рассылки', 'messages'
exec insertLanguageInterface  'uk', 'Last date', N'Дата останньої розсилки', 'messages'
GO

exec insertLanguageInterface  'en', 'Last subject', N'Last subject', 'messages'
exec insertLanguageInterface  'ru', 'Last subject', N'Название последней рассылки', 'messages'
exec insertLanguageInterface  'uk', 'Last subject', N'Назва останньої розсилки', 'messages'
GO

exec insertLanguageInterface  'en', 'Access to conference', N'Access to conference', 'messages'
exec insertLanguageInterface  'ru', 'Access to conference', N'Доступ к конференции', 'messages'
exec insertLanguageInterface  'uk', 'Access to conference', N'Доступ до конференції', 'messages'
GO

exec insertLanguageInterface  'en', 'Access to PDF', N'Access to PDF', 'messages'
exec insertLanguageInterface  'ru', 'Access to PDF', N'Доступ к pdf', 'messages'
exec insertLanguageInterface  'uk', 'Access to PDF', N'Доступ до pdf', 'messages'
GO

exec insertLanguageInterface  'en', 'Access to video', N'Access to video', 'messages'
exec insertLanguageInterface  'ru', 'Access to video', N'Доступ к видео', 'messages'
exec insertLanguageInterface  'uk', 'Access to video', N'Доступ до відео', 'messages'
GO

exec insertLanguageInterface  'en', 'Certificate', N'Certificate', 'messages'
exec insertLanguageInterface  'ru', 'Certificate', N'Сертификат', 'messages'
exec insertLanguageInterface  'uk', 'Certificate', N'Сертифікат', 'messages'
GO

exec insertLanguageInterface  'en', 'EGRPOU', N'EGRPOU', 'messages'
exec insertLanguageInterface  'ru', 'EGRPOU', N'ЕГРПОУ', 'messages'
exec insertLanguageInterface  'uk', 'EGRPOU', N'ЄДРПОУ', 'messages'
GO

IF NOT EXISTS(SELECT *
              FROM info_mcmcontentvariable
              WHERE code like 'CONFERENCE.LINK')
    BEGIN
        INSERT INTO info_mcmcontentvariable (name, code, forUrl)
        VALUES ('Персональная ссылка на конференцию', 'CONFERENCE.LINK', 1)
    END
GO

exec insertLanguageInterface  'en', 'Contact person', N'Contact person', 'messages'
exec insertLanguageInterface  'ru', 'Contact person', N'Контактное лицо', 'messages'
exec insertLanguageInterface  'uk', 'Contact person', N'Контактна особа', 'messages'
GO

exec insertLanguageInterface  'en', 'Contact persons', N'Contact persons', 'messages'
exec insertLanguageInterface  'ru', 'Contact persons', N'Контактные лица', 'messages'
exec insertLanguageInterface  'uk', 'Contact persons', N'Контактні особи', 'messages'
GO

IF NOT EXISTS (SELECT 1 FROM information_schema.columns WHERE table_name = 'info_mcmdistributionaction' AND column_name = 'gmt_offset')
    BEGIN
        ALTER TABLE info_mcmdistributionaction ADD gmt_offset int NULL
    END
GO

-- IF NOT EXISTS(SELECT 1
--               FROM info_mcmtriggertype
--               WHERE name = 'gettingConferenceAccess')
--     INSERT INTO info_mcmtriggertype (name) VALUES ('gettingConferenceAccess')
-- GO
--
-- IF NOT EXISTS(SELECT 1
--               FROM info_mcmtriggertype
--               WHERE name = 'removingConferenceAccess')
--     INSERT INTO info_mcmtriggertype (name) VALUES ('removingConferenceAccess')
-- GO
--
-- IF NOT EXISTS(SELECT 1
--               FROM info_mcmtriggertype
--               WHERE name = 'gettingConferenceVideoAccess')
--     INSERT INTO info_mcmtriggertype (name) VALUES ('gettingConferenceVideoAccess')
-- GO
--
-- IF NOT EXISTS(SELECT 1
--               FROM info_mcmtriggertype
--               WHERE name = 'gettingConferencePdfAccess')
--     INSERT INTO info_mcmtriggertype (name) VALUES ('gettingConferencePdfAccess')
-- GO
--
-- IF NOT EXISTS(SELECT 1
--               FROM info_mcmtriggertype
--               WHERE name = 'gettingConferenceCertificate')
--     INSERT INTO info_mcmtriggertype (name) VALUES ('gettingConferenceCertificate')
-- GO
--
-- IF NOT EXISTS(SELECT 1
--               FROM info_mcmtriggertype
--               WHERE name = 'gettingConferenceToken')
--     INSERT INTO info_mcmtriggertype (name) VALUES ('gettingConferenceToken')
-- GO

exec insertLanguageInterface  'en', 'Getting conference access', N'Getting conference access', 'messages'
exec insertLanguageInterface  'ru', 'Getting conference access', N'Получение доступа к конференции', 'messages'
exec insertLanguageInterface  'uk', 'Getting conference access', N'Отримання доступу до конференції', 'messages'
GO

exec insertLanguageInterface  'en', 'Removing conference access', N'Removing conference access', 'messages'
exec insertLanguageInterface  'ru', 'Removing conference access', N'Снятие доступа к конференции', 'messages'
exec insertLanguageInterface  'uk', 'Removing conference access', N'Зняття доступу до конференції', 'messages'
GO

exec insertLanguageInterface  'en', 'Getting conference video access', N'Getting conference video access', 'messages'
exec insertLanguageInterface  'ru', 'Getting conference video access', N'Получение доступа к видео конференции', 'messages'
exec insertLanguageInterface  'uk', 'Getting conference video access', N'Отримання доступу до відео конференції', 'messages'
GO

exec insertLanguageInterface  'en', 'Getting conference pdf access', N'Getting conference pdf access', 'messages'
exec insertLanguageInterface  'ru', 'Getting conference pdf access', N'Получение доступа к pdf конференции', 'messages'
exec insertLanguageInterface  'uk', 'Getting conference pdf access', N'Отримання доступу до pdf конференції', 'messages'
GO

exec insertLanguageInterface  'en', 'Getting conference certificate', N'Getting conference certificate', 'messages'
exec insertLanguageInterface  'ru', 'Getting conference certificate', N'Получение сертификата конференции', 'messages'
exec insertLanguageInterface  'uk', 'Getting conference certificate', N'Отримання сертифікату конференції', 'messages'
GO

exec insertLanguageInterface  'en', 'Getting conference token', N'Getting conference token', 'messages'
exec insertLanguageInterface  'ru', 'Getting conference token', N'Получение токена конференции', 'messages'
exec insertLanguageInterface  'uk', 'Getting conference token', N'Отримання токену конференції', 'messages'
GO

IF NOT EXISTS (SELECT 1 FROM information_schema.columns WHERE table_name = 'info_mcmtrigger' AND column_name = 'conference_id')
    BEGIN
        ALTER TABLE info_mcmtrigger ADD conference_id int NULL
    END
GO

IF NOT EXISTS (SELECT 1 FROM information_schema.columns WHERE table_name = 'info_mcmdistribution' AND column_name = 'reminder')
    BEGIN
        ALTER TABLE info_mcmdistribution ADD reminder datetime NULL
    END
GO

exec insertLanguageInterface  'en', 'Reminder', N'Reminder', 'messages'
exec insertLanguageInterface  'ru', 'Reminder', N'Напоминание', 'messages'
exec insertLanguageInterface  'uk', 'Reminder', N'Нагадування', 'messages'
GO

exec insertLanguageInterface  'en', 'Specify default language and subject to save the content', N'Specify default language and subject to save the content', 'messages'
exec insertLanguageInterface  'ru', 'Specify default language and subject to save the content', N'Укажите язык по умолчанию и тему для сохранения содержимого', 'messages'
exec insertLanguageInterface  'uk', 'Specify default language and subject to save the content', N'Вкажіть мову та тему за замовчуванням, щоб зберегти вміст', 'messages'
GO

update info_mcmdistribution
set owner_id = (select user_id from info_mcmcontent where id = info_mcmdistribution.mcmcontent_id)
where owner_id is null
GO

exec insertLanguageInterface  'en', 'Delete video', N'Delete video', 'messages'
exec insertLanguageInterface  'ru', 'Delete video', N'Удалить видео', 'messages'
exec insertLanguageInterface  'uk', 'Delete video', N'Видалити відео', 'messages'
GO

IF NOT EXISTS (SELECT 1 FROM information_schema.columns WHERE table_name = 'info_contact' AND column_name = 'lastname_ukr')
    BEGIN
        ALTER TABLE info_contact ADD lastname_ukr [varchar](255) NULL
    END
GO

IF NOT EXISTS (SELECT 1 FROM information_schema.columns WHERE table_name = 'info_contact' AND column_name = 'firstname_ukr')
    BEGIN
        ALTER TABLE info_contact ADD firstname_ukr [varchar](255) NULL
    END
GO

IF NOT EXISTS (SELECT 1 FROM information_schema.columns WHERE table_name = 'info_contact' AND column_name = 'middlename_ukr')
    BEGIN
        ALTER TABLE info_contact ADD middlename_ukr [varchar](255) NULL
    END
GO

IF NOT EXISTS (SELECT 1 FROM information_schema.columns WHERE table_name = 'info_contact' AND column_name = 'lastname_eng')
    BEGIN
        ALTER TABLE info_contact ADD lastname_eng [varchar](255) NULL
    END
GO

IF NOT EXISTS (SELECT 1 FROM information_schema.columns WHERE table_name = 'info_contact' AND column_name = 'firstname_eng')
    BEGIN
        ALTER TABLE info_contact ADD firstname_eng [varchar](255) NULL
    END
GO

IF NOT EXISTS (SELECT 1 FROM information_schema.columns WHERE table_name = 'info_contact' AND column_name = 'middlename_eng')
    BEGIN
        ALTER TABLE info_contact ADD middlename_eng [varchar](255) NULL
    END
GO

------------------------------------------------------------------------------------------------------------------------
--https://teamsoft.atlassian.net/browse/MCM-388
------------------------------------------------------------------------------------------------------------------------
IF OBJECT_ID(N'[info_mcmconferencecontent]', 'U') IS NULL
    BEGIN
        CREATE TABLE [dbo].[info_mcmconferencecontent]
        (
            [id]             [int] IDENTITY (1,1) NOT NULL,
            [conference_id]  [int]                  NOT NULL,
            [content_id]     [int]                  NOT NULL,
            [currenttime]    [datetime]           NULL,
            [time]           [timestamp]          NULL,
            [moduser]        [varchar](16)        NULL,
            [guid]           [uniqueidentifier]   NULL,
            CONSTRAINT [PK_info_mcmconferencecontent] PRIMARY KEY CLUSTERED
                (
                 [id] ASC
                    ) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
        ) ON [PRIMARY]

        ALTER TABLE [dbo].[info_mcmconferencecontent]
            WITH CHECK ADD CONSTRAINT [FK_info_mcmconferencecontent_info_mcmconference] FOREIGN KEY ([conference_id])
                REFERENCES [dbo].[info_mcmconference] ([id])

        ALTER TABLE [dbo].[info_mcmconferencecontent]
            WITH CHECK ADD CONSTRAINT [FK_info_mcmconferencecontent_info_mcmcontent] FOREIGN KEY ([content_id])
                REFERENCES [dbo].[info_mcmcontent] ([id])
    END
GO

exec insertLanguageInterface 'en', 'The end of the period must be later than the start of the period', N'The end of the period must be later than the start of the period', 'validators'
exec insertLanguageInterface 'ru', 'The end of the period must be later than the start of the period', N'Окончание периода должно быть позже начала периода.', 'validators'
exec insertLanguageInterface 'uk', 'The end of the period must be later than the start of the period', N'Кінець періоду повинен бути пізніше початку періоду', 'validators'
GO
------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------

------------------------------------------------------------------------------------------------------------------------
--- https://teamsoft.atlassian.net/browse/PHARMAWEB-385
------------------------------------------------------------------------------------------------------------------------
IF NOT EXISTS (SELECT 1 FROM information_schema.columns WHERE table_name = 'info_planpresentation' AND column_name = 'position')
    BEGIN
        ALTER TABLE info_planpresentation ADD position INT NULL
    END
GO

exec insertLanguageInterface 'en', 'Sequence number', N'Sequence number', 'crm'
exec insertLanguageInterface 'ru', 'Sequence number', N'Порядковый номер', 'crm'
exec insertLanguageInterface 'uk', 'Sequence number', N'Порядковий номер', 'crm'
GO
------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------

------------------------------------------------------------------------------------------------------------------------
---https://teamsoft.atlassian.net/browse/EGISRU-219
------------------------------------------------------------------------------------------------------------------------
exec insertLanguageInterface 'en', 'Number of working days', N'Number of working days', 'crm'
exec insertLanguageInterface 'ru', 'Number of working days', N'Количество рабочих дней', 'crm'
exec insertLanguageInterface 'uk', 'Number of working days', N'Кількість робочих днів', 'crm'
GO
------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------

------------------------------------------------------------------------------------------------------------------------
--- https://teamsoft.atlassian.net/browse/GEODATA-776
------------------------------------------------------------------------------------------------------------------------
update info_languageinterface
set translation = N'Pharmacy Chain'
where [key] = 'Pharmacy networks'
  and domain = 'geomarketing'
  and language_id = (select id FROM info_language WHERE slug = 'en')
GO

DELETE
FROM info_languageinterface
WHERE [domain] = 'geomarketing'
  AND [key] = 'version.targeting'
GO

exec insertLanguageInterface 'en', 'version.targeting', 'CRM', 'geomarketing';
exec insertLanguageInterface 'ru', 'version.targeting', 'CRM', 'geomarketing';
exec insertLanguageInterface 'uk', 'version.targeting', 'CRM', 'geomarketing';
GO

update info_languageinterface
set [key] = 'regions'
where [key] = 'Regions'
  and domain = 'geomarketing'
GO
------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------

------------------------------------------------------------------------------------------------------------------------
---https://teamsoft.atlassian.net/browse/PHARMAWEB-389
------------------------------------------------------------------------------------------------------------------------
exec insertLanguageInterface 'en', 'Rewrite details', N'Rewrite details', 'crm'
exec insertLanguageInterface 'ru', 'Rewrite details', N'Перезаписать детали', 'crm'
exec insertLanguageInterface 'uk', 'Rewrite details', N'Перепишіть деталі', 'crm'
GO
exec insertLanguageInterface 'en', 'Paste', N'Paste', 'crm'
exec insertLanguageInterface 'ru', 'Paste', N'Вставить', 'crm'
exec insertLanguageInterface 'uk', 'Paste', N'Вставити', 'crm'
GO
exec insertLanguageInterface 'en', 'Copy', N'Copy', 'crm'
exec insertLanguageInterface 'ru', 'Copy', N'Копировать', 'crm'
exec insertLanguageInterface 'uk', 'Copy', N'Копіювати', 'crm'
GO
exec insertLanguageInterface 'en', 'Copied', N'Copied', 'crm'
exec insertLanguageInterface 'ru', 'Copied', N'Скопировано', 'crm'
exec insertLanguageInterface 'uk', 'Copied', N'Скопійовано', 'crm'
GO
exec insertLanguageInterface 'en', 'Nothing to rewrite', N'Nothing to rewrite', 'crm'
exec insertLanguageInterface 'ru', 'Nothing to rewrite', N'Нечего перезаписывать', 'crm'
exec insertLanguageInterface 'uk', 'Nothing to rewrite', N'Нічого перезаписувати', 'crm'
GO
exec insertLanguageInterface 'en', 'Rewrite plan details', N'Rewrite plan details', 'crm'
exec insertLanguageInterface 'ru', 'Rewrite plan details', N'Перезаписать детали плана', 'crm'
exec insertLanguageInterface 'uk', 'Rewrite plan details', N'Перезаписати деталі плану', 'crm'
GO
exec insertLanguageInterface 'en', 'Rewrite', N'Rewrite', 'crm'
exec insertLanguageInterface 'ru', 'Rewrite', N'Перезаписать', 'crm'
exec insertLanguageInterface 'uk', 'Rewrite', N'Перезаписати', 'crm'
GO
exec insertLanguageInterface 'en', 'Attention!!! Selected details of plan will be rewritten.', N'Attention!!! Selected details of plan will be rewritten.', 'crm'
exec insertLanguageInterface 'ru', 'Attention!!! Selected details of plan will be rewritten.', N'Внимание!!! Выбранные детали плана будут перезаписаны.', 'crm'
exec insertLanguageInterface 'uk', 'Attention!!! Selected details of plan will be rewritten.', N'Увага!!! Вибрані деталі плану будуть перезаписані.', 'crm'
GO
exec insertLanguageInterface 'en', 'data from', N'data from', 'crm'
exec insertLanguageInterface 'ru', 'data from', N'данными из', 'crm'
exec insertLanguageInterface 'uk', 'data from', N'даними від', 'crm'
GO
exec insertLanguageInterface 'en', 'Rewritten', N'Rewritten', 'crm'
exec insertLanguageInterface 'ru', 'Rewritten', N'Перезаписанный', 'crm'
exec insertLanguageInterface 'uk', 'Rewritten', N'Перезаписаний', 'crm'
GO
exec insertLanguageInterface 'en', 'You cannot rewrite the plan details, because they belong to another plan.', N'You cannot rewrite the plan details, because they belong to another plan.', 'validators'
exec insertLanguageInterface 'ru', 'You cannot rewrite the plan details, because they belong to another plan.', N'Вы не можете перезаписать детали плана, потому что они принадлежат другому плану.', 'validators'
exec insertLanguageInterface 'uk', 'You cannot rewrite the plan details, because they belong to another plan.', N'Ви не можете перезаписати деталі плану, оскільки вони належать до іншого плану.', 'validators'
GO
------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------

------------------------------------------------------------------------------------------------------------------------
--- https://teamsoft.atlassian.net/browse/PHARMAWEB-382
------------------------------------------------------------------------------------------------------------------------
IF OBJECT_ID(N'info_userpositiontasktype', 'U') is NULL
    BEGIN
        -- auto-generated definition
        create table info_userpositiontasktype
        (
            id            int identity
                constraint PK_info_userpositiontasktype
                    primary key,
            position_id   int
                constraint FK_info_userpositiontasktype_position_id
                    references info_dictionary,
            tasktype_id   int
                constraint FK_info_userpositiontasktype_tasktype_id
                    references info_tasktype,
            time          timestamp null,
            guid          uniqueidentifier
                constraint DF_info_userpositiontasktypeguid default newid(),
            currenttime   datetime
                constraint DF_info_userpositiontasktypecurrenttime default getdate(),
            moduser       varchar(16)
                constraint DF_info_userpositiontasktypemoduser default [dbo].[Get_CurrentCode](),
            actiontype_id int
                constraint FK_info_userpositiontasktype_actiontype_id
                    references info_actiontype
        )
    END
GO

IF NOT EXISTS(SELECT 1 FROM sys.indexes WHERE object_id = OBJECT_ID('info_userpositiontasktype', 'U') AND name = 'ix_info_userpositiontasktype_guid')
    BEGIN
        create index ix_info_userpositiontasktype_guid on info_userpositiontasktype (guid)
    END
GO

IF NOT EXISTS(SELECT 1 FROM sys.indexes WHERE object_id = OBJECT_ID('info_userpositiontasktype', 'U') AND name = 'ix_info_userpositiontasktype_position_id')
    BEGIN
        create index ix_info_userpositiontasktype_position_id on info_userpositiontasktype (position_id)
    END
GO

IF NOT EXISTS(SELECT 1 FROM sys.indexes WHERE object_id = OBJECT_ID('info_userpositiontasktype', 'U') AND name = 'ix_info_userpositiontasktype_tasktype_id')
    BEGIN
        create index ix_info_userpositiontasktype_tasktype_id on info_userpositiontasktype (tasktype_id)
    END
GO

IF NOT EXISTS(SELECT 1 FROM sys.indexes WHERE object_id = OBJECT_ID('info_userpositiontasktype', 'U') AND name = 'ix_info_userpositiontasktype_actiontype_id')
    BEGIN
        create index ix_info_userpositiontasktype_actiontype_id on info_userpositiontasktype (actiontype_id)
    END
GO

IF OBJECT_ID(N'fk_info_target_direction_id', 'F') IS NULL
    BEGIN
        ALTER TABLE info_target
            ADD CONSTRAINT fk_info_target_direction_id FOREIGN KEY (direction_id)
                REFERENCES info_direction (id)
    END
GO

IF OBJECT_ID(N'fk_info_companycategory_info_direction', 'F') IS NULL
    BEGIN
        ALTER TABLE info_companycategory
            ADD CONSTRAINT fk_info_companycategory_info_direction FOREIGN KEY (direction_id)
                REFERENCES info_direction (id)
    END
GO

IF NOT EXISTS(SELECT 1 FROM sys.indexes WHERE object_id = OBJECT_ID('info_companycategory', 'U') AND name = 'ix_info_companycategory_direction_id')
    BEGIN
        create index ix_info_companycategory_direction_id on info_companycategory (direction_id)
    END
GO

exec insertLanguageInterface 'en', 'All product', N'All product', 'crm'
exec insertLanguageInterface 'ru', 'All product', N'Все продуктовые', 'crm'
exec insertLanguageInterface 'uk', 'All product', N'Всі продуктові', 'crm'
GO

exec insertLanguageInterface 'en', 'client is optional', N'client is optional', 'crm'
exec insertLanguageInterface 'ru', 'client is optional', N'клиент необязательный', 'crm'
exec insertLanguageInterface 'uk', 'client is optional', N'клієнт необов’язковий', 'crm'
GO
------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------
--- https://teamsoft.atlassian.net/browse/PHARMAWEB-414
------------------------------------------------------------------------------------------------------------------------
IF NOT EXISTS (SELECT 1 FROM information_schema.columns WHERE table_name = 'info_dictionary' AND column_name = 'language_id')
    BEGIN
        ALTER TABLE [dbo].[info_dictionary] ADD [language_id] int null
        ALTER TABLE [dbo].[info_dictionary]
            WITH NOCHECK ADD CONSTRAINT [FK_info_dictionary_info_language] FOREIGN KEY ([language_id])
                REFERENCES [dbo].[info_language] ([id])

    END
GO
IF NOT EXISTS (SELECT 1 FROM information_schema.columns WHERE table_name = 'info_taskstate' AND column_name = 'language_id')
    BEGIN
        ALTER TABLE [dbo].[info_taskstate] ADD [language_id] int null
        ALTER TABLE [dbo].[info_taskstate]
            WITH NOCHECK ADD CONSTRAINT [FK_info_taskstate_info_language] FOREIGN KEY ([language_id])
                REFERENCES [dbo].[info_language] ([id])

    END
GO
IF NOT EXISTS (SELECT 1 FROM information_schema.columns WHERE table_name = 'info_actionstate' AND column_name = 'language_id')
    BEGIN
        ALTER TABLE [dbo].[info_actionstate] ADD [language_id] int null
        ALTER TABLE [dbo].[info_actionstate]
            WITH NOCHECK ADD CONSTRAINT [FK_info_actionstate_info_language] FOREIGN KEY ([language_id])
                REFERENCES [dbo].[info_language] ([id])

    END
GO
IF NOT EXISTS (SELECT 1 FROM information_schema.columns WHERE table_name = 'info_companytype' AND column_name = 'language_id')
    BEGIN
        ALTER TABLE [dbo].[info_companytype] ADD [language_id] int null
        ALTER TABLE [dbo].[info_companytype]
            WITH NOCHECK ADD CONSTRAINT [FK_info_companytype_info_language] FOREIGN KEY ([language_id])
                REFERENCES [dbo].[info_language] ([id])

    END
GO
IF NOT EXISTS (SELECT 1 FROM information_schema.columns WHERE table_name = 'info_contacttype' AND column_name = 'language_id')
    BEGIN
        ALTER TABLE [dbo].[info_contacttype] ADD [language_id] int null
        ALTER TABLE [dbo].[info_contacttype]
            WITH NOCHECK ADD CONSTRAINT [FK_info_contacttype_info_language] FOREIGN KEY ([language_id])
                REFERENCES [dbo].[info_language] ([id])

    END
GO
IF NOT EXISTS (SELECT 1 FROM information_schema.columns WHERE table_name = 'info_tasktype' AND column_name = 'language_id')
    BEGIN
        ALTER TABLE [dbo].[info_tasktype] ADD [language_id] int null
        ALTER TABLE [dbo].[info_tasktype]
            WITH NOCHECK ADD CONSTRAINT [FK_info_tasktype_info_language] FOREIGN KEY ([language_id])
                REFERENCES [dbo].[info_language] ([id])

    END
GO
IF NOT EXISTS (SELECT 1 FROM information_schema.columns WHERE table_name = 'info_actiontype' AND column_name = 'language_id')
    BEGIN
        ALTER TABLE [dbo].[info_actiontype] ADD [language_id] int null
        ALTER TABLE [dbo].[info_actiontype]
            WITH NOCHECK ADD CONSTRAINT [FK_info_actiontype_info_language] FOREIGN KEY ([language_id])
                REFERENCES [dbo].[info_language] ([id])

    END
GO
IF NOT EXISTS (SELECT 1 FROM information_schema.columns WHERE table_name = 'info_tabcontrol' AND column_name = 'language_id')
    BEGIN
        ALTER TABLE [dbo].[info_tabcontrol] ADD [language_id] int null
        ALTER TABLE [dbo].[info_tabcontrol]
            WITH NOCHECK ADD CONSTRAINT [FK_info_tabcontrol_info_language] FOREIGN KEY ([language_id])
                REFERENCES [dbo].[info_language] ([id])

    END
GO
IF NOT EXISTS (SELECT 1 FROM information_schema.columns WHERE table_name = 'info_country' AND column_name = 'language_id')
    BEGIN
        ALTER TABLE [dbo].[info_country] ADD [language_id] int
        ALTER TABLE [dbo].[info_country]
            WITH NOCHECK ADD CONSTRAINT [FK_info_country_info_language] FOREIGN KEY ([language_id])
                REFERENCES [dbo].[info_language] ([id])

    END
GO
------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------

------------------------------------------------------------------------------------------------------------------------
-- https://teamsoft.atlassian.net/browse/PHARMAWEB-381
------------------------------------------------------------------------------------------------------------------------
exec insertLanguageInterface 'en', 'Select presentation', N'Select presentation', 'crm'
exec insertLanguageInterface 'ru', 'Select presentation', N'Выберите презентацию', 'crm'
exec insertLanguageInterface 'uk', 'Select presentation', N'Виберіть презентацію', 'crm'
GO

exec insertLanguageInterface 'en', 'File extension', N'File extension', 'crm'
exec insertLanguageInterface 'ru', 'File extension', N'Расширение файла', 'crm'
exec insertLanguageInterface 'uk', 'File extension', N'Розширення файлу', 'crm'
GO
------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------

------------------------------------------------------------------------------------------------------------------------
--- https://teamsoft.atlassian.net/browse/DBBI-13
------------------------------------------------------------------------------------------------------------------------

IF OBJECT_ID(N'ps_project', 'U') is NULL
    begin
        create table ps_project
        (
            [id]            int identity (1,1) not null,
            [name]          varchar(255)       null,
            [currenttime]   datetime           null
                CONSTRAINT [DF_ps_projectcurrenttime] DEFAULT (getdate()),
            [time]          timestamp          null,
            [moduser]       varchar(16)        null
                CONSTRAINT [DF_ps_projectmoduser] DEFAULT ([dbo].[Get_CurrentCode]()),
            [guid]          uniqueidentifier   null
                CONSTRAINT [DF_ps_projectguid] DEFAULT (newid()),
            CONSTRAINT [PK_ps_project] PRIMARY KEY (id) ON [PRIMARY]
        )
    end
GO
IF NOT EXISTS (SELECT 1 FROM po_dictionarygroup WHERE name = 'PharmaSales')
    INSERT INTO po_dictionarygroup (name) VALUES ('PharmaSales')
GO
IF EXISTS(SELECT 1
          FROM information_schema.columns
          WHERE table_name = 'po_dictionary'
            AND NOT EXISTS(SELECT 1 FROM po_dictionary WHERE tablename = 'ps_project'))
    BEGIN
        insert po_dictionary (identifier, name, tablename, group_id)
        values (NULL, 'Проекты', 'ps_project', (select id from po_dictionarygroup where name = 'PharmaSales'))

        insert info_dictionaryidentifier (tablename, columnname, value, identifier, hidden, order_num, required, language_id)
        values ('ps_project', 'name', 'name', '0', '0', '2', '0', '1'),
           ('ps_project', 'name', 'Имя', '0', '0', '2', '0', '2'),
           ('ps_project', 'name', 'Назва', '0', '0', '2', '0', '3')
    END
GO
IF EXISTS(SELECT 1
          FROM information_schema.columns
          WHERE table_name = 'po_dictionary'
            AND NOT EXISTS(SELECT 1 FROM po_dictionary WHERE tablename = 'info_distributor'))
    BEGIN
        insert po_dictionary (identifier, name, tablename, group_id)
        values (NULL, 'Дистрибьюторы', 'info_distributor', (select id from po_dictionarygroup where name = 'PharmaSales'))

        insert info_dictionaryidentifier (tablename, columnname, value, identifier, hidden, order_num, required, language_id)
        values ('info_distributor', 'name', 'Name', '0', '0', '2', '0', '1'),
           ('info_distributor', 'name', 'Имя', '0', '0', '2', '0', '2'),
           ('info_distributor', 'name', 'Назва', '0', '0', '2', '0', '3'),
           ('info_distributor', 'region_id', 'Region', '0', '0', '2', '0', '1'),
           ('info_distributor', 'region_id', 'Область', '0', '0', '2', '0', '2'),
           ('info_distributor', 'region_id', 'Область', '0', '0', '2', '0', '3'),
           ('info_distributor', 'city_id', 'City', '0', '0', '2', '0', '1'),
           ('info_distributor', 'city_id', 'Город', '0', '0', '2', '0', '2'),
           ('info_distributor', 'city_id', 'Місто', '0', '0', '2', '0', '3'),
           ('info_distributor', 'address', 'Address', '0', '0', '2', '0', '1'),
           ('info_distributor', 'address', 'Адрес', '0', '0', '2', '0', '2'),
           ('info_distributor', 'address', 'Адреса', '0', '0', '2', '0', '3'),
           ('info_distributor', 'phone', 'Phone', '0', '0', '2', '0', '1'),
           ('info_distributor', 'phone', 'Телефон', '0', '0', '2', '0', '2'),
           ('info_distributor', 'phone', 'Телефон', '0', '0', '2', '0', '3')
    END
GO
IF NOT EXISTS(SELECT 1
              FROM INFORMATION_SCHEMA.COLUMNS
              WHERE table_name = 'ps_pattern'
                AND column_name = 'project_id')
ALTER TABLE ps_pattern
    ADD project_id int NULL
    constraint FK_ps_pattern_project_id
                references ps_project
GO
------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------

------------------------------------------------------------------------------------------------------------------------
--- https://teamsoft.atlassian.net/browse/CAMPINA-104
------------------------------------------------------------------------------------------------------------------------
exec insertLanguageInterface 'en', 'Sorry, but image of agreement not found', N'Sorry, but image of agreement not found', 'crm'
exec insertLanguageInterface 'ru', 'Sorry, but image of agreement not found', N'Извините, но изображение согласия не найдено', 'crm'
exec insertLanguageInterface 'uk', 'Sorry, but image of agreement not found', N'Вибачте, але зображення згоди не знайдено', 'crm'
GO
------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------


------------------------------------------------------------------------------------------------------------------------
--fix merge master
------------------------------------------------------------------------------------------------------------------------
exec insertLanguageInterface 'en', 'Not selected', N'Not selected', 'mcm'
exec insertLanguageInterface 'ru', 'Not selected', N'Не выбрано', 'mcm'
exec insertLanguageInterface 'uk', 'Not selected', N'Не вибрано', 'mcm'
GO
------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------
-- https://teamsoft.atlassian.net/browse/ACINO-207
------------------------------------------------------------------------------------------------------------------------
IF OBJECT_ID(N'info_contactcomment', 'U') is NULL
    BEGIN
        create table info_contactcomment
        (
            id            int identity
                constraint PK_info_contactcomment
                    primary key,
            contact_id   int
                constraint FK_info_contactcomment_contact_id
                    references info_contact,
            comment_large  varchar(max),
            guid          uniqueidentifier
                constraint DF_info_contactcommentguid default newid(),
            currenttime   datetime
                constraint DF_info_contactcommentcurrenttime default getdate(),
            moduser       varchar(16)
                constraint DF_info_contactcommentmoduser default [dbo].[Get_CurrentCode](),
        )
    END
GO

IF EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'createCurrenttimeAndModuserTrigger') AND xtype IN (N'P'))
    BEGIN
        DROP PROCEDURE createCurrenttimeAndModuserTrigger
    END
GO

CREATE PROCEDURE createCurrenttimeAndModuserTrigger(@table_name VARCHAR(255))
AS
BEGIN
    DECLARE @trigger_name AS VARCHAR(MAX) = 'moduser_' + @table_name;
    IF NOT EXISTS (select * from dbo.sysobjects where id = object_id(@trigger_name) and OBJECTPROPERTY(id, 'IsTrigger') = 1) BEGIN
        exec('create trigger ' + @trigger_name + ' on ' + @table_name + ' for insert, update as if not update(moduser)
update ' + @table_name + ' set moduser = dbo.Get_CurrentCode(), currenttime = getdate() where id in (select id from inserted)')
    END
END
GO

createCurrenttimeAndModuserTrigger N'info_contactcomment'

IF OBJECT_ID(N'info_fieldname', 'U') is NULL
    BEGIN
        -- auto-generated definition
        create table info_fieldname
        (
            id          int identity
                constraint PK_info_fieldname
                    primary key,
            page        int,
            name        varchar(255),
            title       varchar(255),
            currenttime datetime
                constraint DF_info_fieldnamecurrenttime default getdate(),
            moduser     varchar(16)
                constraint DF_info_fieldnamemoduser default [dbo].[Get_CurrentCode](),
            guid        uniqueidentifier
                constraint DF_info_fieldnameguid default newid()
        )
    END
GO

createCurrenttimeAndModuserTrigger N'info_fieldname'
GO

EXEC insertPoEntityFields 2, 'comment_large', N'Comment'
GO
------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------

------------------------------------------------------------------------------------------------------------------------
--https://teamsoft.atlassian.net/browse/DRREDDYSKZ-115
------------------------------------------------------------------------------------------------------------------------
IF OBJECT_ID(N'[info_brand]', 'U') IS NULL
    BEGIN
        CREATE TABLE [dbo].[info_brand]
        (
            [id]             [int] IDENTITY (1,1) NOT NULL,
            [name]           [varchar](255)       NOT NULL,
            [isarchive]      [int]                NULL,
            [currenttime]    [datetime]           NULL,
            [time]           [timestamp]          NULL,
            [moduser]        [varchar](16)        NULL,
            [guid]           [uniqueidentifier]   NULL,
            CONSTRAINT [PK_info_brand] PRIMARY KEY CLUSTERED ([id] ASC)
                WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF,
                ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
        ) ON [PRIMARY]
    END
GO

if not exists(SELECT 1
              FROM INFORMATION_SCHEMA.COLUMNS
              WHERE TABLE_NAME = 'info_mcmdistribution'
                AND Column_Name = 'brand_id')
    begin
        alter table [dbo].[info_mcmdistribution] add brand_id [int] null;

        ALTER TABLE [dbo].[info_mcmdistribution]
            WITH CHECK ADD CONSTRAINT [FK_info_mcmdistribution_info_brand] FOREIGN KEY ([brand_id])
                REFERENCES [dbo].[info_brand] ([id])
    end
GO
------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------

------------------------------------------------------------------------------------------------------------------------
--https://teamsoft.atlassian.net/browse/PHARMAANDROID-701
------------------------------------------------------------------------------------------------------------------------
IF EXISTS (SELECT 1 FROM information_schema.columns
               WHERE table_name = 'info_reporttabfield' AND column_name = 'pivot')
    BEGIN
        EXEC sp_rename 'info_reporttabfield.pivot', 'is_pivot', 'COLUMN';
    END
GO
------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------

------------------------------------------------------------------------------------------------------------------------
--fix_v23
------------------------------------------------------------------------------------------------------------------------
exec insertLanguageInterface 'en', 'Title', N'Title', 'mcm'
exec insertLanguageInterface 'ru', 'Title', N'Название', 'mcm'
exec insertLanguageInterface 'uk', 'Title', N'Назва', 'mcm'
GO
exec insertLanguageInterface 'en', 'Subject', N'Subject', 'mcm'
exec insertLanguageInterface 'ru', 'Subject', N'Тема', 'mcm'
exec insertLanguageInterface 'uk', 'Subject', N'Тема', 'mcm'
GO
exec insertLanguageInterface 'en', 'Content by default', N'Content by default', 'mcm'
exec insertLanguageInterface 'ru', 'Content by default', N'Контент по умолчанию', 'mcm'
exec insertLanguageInterface 'uk', 'Content by default', N'Контент за замовчуванням', 'mcm'
GO
exec insertLanguageInterface 'en', 'Default content subject cannot be empty', N'Default content subject cannot be empty', 'mcm'
exec insertLanguageInterface 'ru', 'Default content subject cannot be empty', N'Тема контента по умолчанию не может быть незаполненной', 'mcm'
exec insertLanguageInterface 'uk', 'Default content subject cannot be empty', N'Тема контенту за замовчуванням не може бути незаповненою', 'mcm'
GO
exec insertLanguageInterface 'en', 'Archived', N'Archived', 'mcm'
exec insertLanguageInterface 'ru', 'Archived', N'Архивный', 'mcm'
exec insertLanguageInterface 'uk', 'Archived', N'Архівний', 'mcm'
GO
exec insertLanguageInterface 'en', 'Save', N'Save', 'mcm'
exec insertLanguageInterface 'ru', 'Save', N'Сохранить', 'mcm'
exec insertLanguageInterface 'uk', 'Save', N'Зберегти', 'mcm'
GO
exec insertLanguageInterface 'en', 'Email Template', N'Email Template', 'mcm'
exec insertLanguageInterface 'ru', 'Email Template', N'Email Шаблон', 'mcm'
exec insertLanguageInterface 'uk', 'Email Template', N'Email Шаблон', 'mcm'
GO
exec insertLanguageInterface 'en', 'Number of contacts', N'Number of contacts', 'mcm'
exec insertLanguageInterface 'ru', 'Number of contacts', N'Количество контактов', 'mcm'
exec insertLanguageInterface 'uk', 'Number of contacts', N'Кількість контактів', 'mcm'
GO
exec insertLanguageInterface 'en', 'Image', N'Image', 'mcm'
exec insertLanguageInterface 'ru', 'Image', N'Изображение', 'mcm'
exec insertLanguageInterface 'uk', 'Image', N'Зображення', 'mcm'
GO
exec insertLanguageInterface 'en', 'Check the number with conversion links for conversion statistics', N'Check the number with conversion links for conversion statistics', 'mcm'
exec insertLanguageInterface 'ru', 'Check the number with conversion links for conversion statistics', N'Проверить кол-во с преобразованием ссылки для статистики переходов', 'mcm'
exec insertLanguageInterface 'uk', 'Check the number with conversion links for conversion statistics', N'Перевірити кількість з перетворенням посилання для статистики переходів', 'mcm'
GO
exec insertLanguageInterface  'en', 'Open new editor', N'Open new editor', 'mcm'
exec insertLanguageInterface  'ru', 'Open new editor', N'Открыть новый редактор', 'mcm'
exec insertLanguageInterface  'uk', 'Open new editor', N'Відкрити новий редактор', 'mcm'
GO
exec insertLanguageInterface  'en', 'Or', N'Or', 'mcm'
exec insertLanguageInterface  'ru', 'Or', N'Или', 'mcm'
exec insertLanguageInterface  'uk', 'Or', N'Або', 'mcm'
GO
exec insertLanguageInterface  'en', 'Target', N'Target', 'mcm'
exec insertLanguageInterface  'ru', 'Target', N'Таргет-группа', 'mcm'
exec insertLanguageInterface  'uk', 'Target', N'Таргет-група', 'mcm'
GO
exec insertLanguageInterface 'en', 'Group deletion confirmation', 'Group deletion confirmation'
exec insertLanguageInterface 'ru', 'Group deletion confirmation', N'Подтверждение удаления папки'
exec insertLanguageInterface 'uk', 'Group deletion confirmation', N'Пiдтвердження видалення групи'
GO
exec insertLanguageInterface 'en', 'Folder can''t be deleted. It must be empty', 'Folder can''t be deleted. It must be empty'
exec insertLanguageInterface 'ru', 'Folder can''t be deleted. It must be empty', N'Папка не может быть удалена. Должна быть пустой'
exec insertLanguageInterface 'uk', 'Folder can''t be deleted. It must be empty', N'Група не може бути видалена. Має бути порожньою'
GO
exec insertLanguageInterface  'en', 'Trigger', N'Trigger', 'mcm'
exec insertLanguageInterface  'ru', 'Trigger', N'Триггер', 'mcm'
exec insertLanguageInterface  'uk', 'Trigger', N'Тригер', 'mcm'
GO
exec insertLanguageInterface  'en', 'Message has been successfully sent', N'Message has been successfully sent', 'mcm'
exec insertLanguageInterface  'ru', 'Message has been successfully sent', N'Письмо/сообщение отправлено успешно', 'mcm'
exec insertLanguageInterface  'uk', 'Message has been successfully sent', N'Лист/повідомлення надіслано успішно', 'mcm'
GO
exec insertLanguageInterface  'en', 'Content has been successfully archived', N'Content has been successfully archived', 'mcm'
exec insertLanguageInterface  'ru', 'Content has been successfully archived', N'Контент заархивирован успешно', 'mcm'
exec insertLanguageInterface  'uk', 'Content has been successfully archived', N'Контент заархівований успішно', 'mcm'
GO
exec insertLanguageInterface  'en', 'Message was not sent', N'Message was not sent', 'mcm'
exec insertLanguageInterface  'ru', 'Message was not sent', N'Письмо/сообщение не отправлено', 'mcm'
exec insertLanguageInterface  'uk', 'Message was not sent', N'Лист/повідомлення не відправлено', 'mcm'
GO
exec insertLanguageInterface  'en', 'Content has been successfully unarchived', N'Content has been successfully unarchived', 'mcm'
exec insertLanguageInterface  'ru', 'Content has been successfully unarchived', N'Контент успешно разархивирован', 'mcm'
exec insertLanguageInterface  'uk', 'Content has been successfully unarchived', N'Контент успішно розархівований', 'mcm'
GO
exec insertLanguageInterface  'en', 'Content was not archived', N'Content was not archived', 'mcm'
exec insertLanguageInterface  'ru', 'Content was not archived', N'Контент не был заархивирован', 'mcm'
exec insertLanguageInterface  'uk', 'Content was not archived', N'Контент не був заархівований', 'mcm'
GO
exec insertLanguageInterface  'en', 'Content was not unarchived', N'Content was not unarchived', 'mcm'
exec insertLanguageInterface  'ru', 'Content was not unarchived', N'Контент не был разархивирован', 'mcm'
exec insertLanguageInterface  'uk', 'Content was not unarchived', N'Контент не був розархівований', 'mcm'
GO
------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------

------------------------------------------------------------------------------------------------------------------------
--- https://teamsoft.atlassian.net/browse/PHARMAWEB-465
------------------------------------------------------------------------------------------------------------------------
IF NOT EXISTS(SELECT 1
              FROM INFORMATION_SCHEMA.COLUMNS
              WHERE table_name = 'info_report'
                AND column_name = 'is_mcm')
ALTER TABLE info_report ADD is_mcm int NULL
GO
------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------
IF NOT EXISTS (SELECT 1 FROM info_roleprivilege WHERE role_id = (SELECT id FROM info_role WHERE code='ROLE_SUPER_ADMIN' AND privilege_id = (SELECT TOP 1 id FROM info_Serviceprivilege WHERE code = 'ROLE_ACCESS_TO_REPORT_MANAGER')))
    INSERT INTO info_roleprivilege(privilege_id, role_id)
    SELECT id, (SELECT id FROM info_role WHERE code='ROLE_SUPER_ADMIN') FROM info_serviceprivilege WHERE code = 'ROLE_ACCESS_TO_REPORT_MANAGER'
GO

IF NOT EXISTS (SELECT 1 FROM info_roleprivilege WHERE role_id = (SELECT id FROM info_role WHERE code='ROLE_SUPER_ADMIN' AND privilege_id = (SELECT TOP 1 id FROM info_Serviceprivilege WHERE code = 'ROLE_ACCESS_TO_MCM_CONTENT')))
    INSERT INTO info_roleprivilege(privilege_id, role_id)
    SELECT id, (SELECT id FROM info_role WHERE code='ROLE_SUPER_ADMIN') FROM info_serviceprivilege WHERE code = 'ROLE_ACCESS_TO_MCM_CONTENT'
GO
IF NOT EXISTS (SELECT 1 FROM info_roleprivilege WHERE role_id = (SELECT id FROM info_role WHERE code='ROLE_SUPER_ADMIN' AND privilege_id = (SELECT TOP 1 id FROM info_Serviceprivilege WHERE code = 'ROLE_ACCESS_TO_MCM_TARGET')))
    INSERT INTO info_roleprivilege(privilege_id, role_id)
    SELECT id, (SELECT id FROM info_role WHERE code='ROLE_SUPER_ADMIN') FROM info_serviceprivilege WHERE code = 'ROLE_ACCESS_TO_MCM_TARGET'
GO
IF NOT EXISTS (SELECT 1 FROM info_roleprivilege WHERE role_id = (SELECT id FROM info_role WHERE code='ROLE_SUPER_ADMIN' AND privilege_id = (SELECT TOP 1 id FROM info_Serviceprivilege WHERE code = 'ROLE_ACCESS_TO_MCM_DISTRIBUTION')))
    INSERT INTO info_roleprivilege(privilege_id, role_id)
    SELECT id, (SELECT id FROM info_role WHERE code='ROLE_SUPER_ADMIN') FROM info_serviceprivilege WHERE code = 'ROLE_ACCESS_TO_MCM_DISTRIBUTION'
GO
IF NOT EXISTS (SELECT 1 FROM info_roleprivilege WHERE role_id = (SELECT id FROM info_role WHERE code='ROLE_SUPER_ADMIN' AND privilege_id = (SELECT TOP 1 id FROM info_Serviceprivilege WHERE code = 'ROLE_ACCESS_TO_MCM_REPORT')))
    INSERT INTO info_roleprivilege(privilege_id, role_id)
    SELECT id, (SELECT id FROM info_role WHERE code='ROLE_SUPER_ADMIN') FROM info_serviceprivilege WHERE code = 'ROLE_ACCESS_TO_MCM_REPORT'
GO
IF NOT EXISTS (SELECT 1 FROM info_roleprivilege WHERE role_id = (SELECT id FROM info_role WHERE code='ROLE_SUPER_ADMIN' AND privilege_id = (SELECT TOP 1 id FROM info_Serviceprivilege WHERE code = 'ROLE_ACCESS_TO_MCM_CONFERENCE')))
    INSERT INTO info_roleprivilege(privilege_id, role_id)
    SELECT id, (SELECT id FROM info_role WHERE code='ROLE_SUPER_ADMIN') FROM info_serviceprivilege WHERE code = 'ROLE_ACCESS_TO_MCM_CONFERENCE'
GO

------------------------------------------------------------------------------------------------------------------------
--https://teamsoft.atlassian.net/browse/DRREDDYS-538
------------------------------------------------------------------------------------------------------------------------
if not exists(SELECT 1
              FROM INFORMATION_SCHEMA.COLUMNS
              WHERE TABLE_NAME = 'info_mcmdistribution'
                AND Column_Name = 'trigger_parent_id')
    begin
        alter table [dbo].[info_mcmdistribution] add trigger_parent_id [int] null;

        ALTER TABLE [dbo].[info_mcmdistribution]
            WITH CHECK ADD CONSTRAINT [FK_info_mcmdistribution_info_mcmdistribution_t] FOREIGN KEY ([trigger_parent_id])
                REFERENCES [dbo].[info_mcmdistribution] ([id])
    end
GO

if not exists(SELECT 1
              FROM INFORMATION_SCHEMA.COLUMNS
              WHERE TABLE_NAME = 'info_mcmdistribution'
                AND Column_Name = 'trigger_id')
    begin
        alter table [dbo].[info_mcmdistribution] add trigger_id [int] null;

        ALTER TABLE [dbo].[info_mcmdistribution]
            WITH CHECK ADD CONSTRAINT [FK_info_mcmdistribution_info_mcmtrigger] FOREIGN KEY ([trigger_id])
                REFERENCES [dbo].[info_mcmtrigger] ([id])
    end
GO
------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------

------------------------------------------------------------------------------------------------------------------------
--add translation for v23
------------------------------------------------------------------------------------------------------------------------
delete  from po_entityfields where field like '%nextaskpurpose%';
update po_entityfields set name = 'Date from of task' where field ='datefrom' and page = 4;
update po_entityfields set name = 'Date till of task' where field ='datetill' and page = 4;
update po_entityfields set [group] = 'reports' where field like '%training%' and page = 4;
update po_entityfields set name='SNILS' where name ='СНИЛС';
update po_entityfields set name='First Name' where name ='Firstname';
update po_entityfields set name='Last Name' where name ='Lastname';
update po_entityfields set name='Middle Name' where name ='Midlename';
update po_entityfields set name='KPP' where name ='КПП';
update info_languageinterface set [key] = 'Coordinates' where [key] = 'coordinates';
update info_languageinterface set [key] = 'Last Name' where [key] = 'Last name' and domain='messages';
update info_languageinterface set [key] = 'Middle Name' where [key] = 'Middle name' and domain='messages';
delete from info_languageinterface where [key]='Companies' and domain='messages' and translation in ('Аптеки', 'Установи', 'Pharmacies');
delete from info_languageinterface where [key]='Actions' and domain='messages' and translation in ('Дії', 'Действия');
delete from info_languageinterface where [key]='DV goals';
delete from info_languageinterface where [key]='Medical representative';


exec insertLanguageInterface 'uk', 'Date from of task', N'Дата початку візиту', 'messages';
exec insertLanguageInterface 'ru', 'Date from of task', N'Дата начала визита', 'messages';
exec insertLanguageInterface 'en', 'Date from of task', N'Date from of task', 'messages';
exec insertLanguageInterface 'uk', 'Date till of task', N'Дата закінчення візиту', 'messages';
exec insertLanguageInterface 'ru', 'Date till of task', N'Дата окончания визита', 'messages';
exec insertLanguageInterface 'en', 'Date till of task', N'Date till of task', 'messages';
exec insertLanguageInterface 'en', 'KPP', N'KPP', 'messages';
exec insertLanguageInterface 'ru', 'KPP', N'КПП', 'messages';
exec insertLanguageInterface 'uk', 'KPP', N'КПП', 'messages';
exec insertLanguageInterface 'en', 'SNILS', N'SNILS', 'messages';
exec insertLanguageInterface 'ru', 'SNILS', N'СНИЛС', 'messages';
exec insertLanguageInterface 'uk', 'SNILS', N'СНІЛС', 'messages';
exec insertLanguageInterface 'en', 'Correspondent account', N'Correspondent account', 'messages';
exec insertLanguageInterface 'ru', 'Correspondent account', N'Корреспондентский счет', 'messages';
exec insertLanguageInterface 'uk', 'Correspondent account', N'Кореспондентський рахунок', 'messages';
exec insertLanguageInterface 'en', 'Bank identification code (BIC)', N'Bank identification code (BIC)', 'messages';
exec insertLanguageInterface 'ru', 'Bank identification code (BIC)', N'Идентификационный код банка (BIC)', 'messages';
exec insertLanguageInterface 'uk', 'Bank identification code (BIC)', N'Ідентифікаційний код банку (BIC)', 'messages';
exec insertLanguageInterface 'en', 'Bank', N'Bank', 'messages';
exec insertLanguageInterface 'ru', 'Bank', N'Банк', 'messages';
exec insertLanguageInterface 'uk', 'Bank', N'Банк', 'messages';
exec insertLanguageInterface 'en', 'Taxpayer identification number', N'Taxpayer identification number', 'messages';
exec insertLanguageInterface 'ru', 'Taxpayer identification number', N'Идентификационный номер налогоплательщика', 'messages';
exec insertLanguageInterface 'uk', 'Taxpayer identification number', N'Ідентифікаційний номер платника податків', 'messages';
exec insertLanguageInterface 'en', 'Insurance certificate', N'Insurance certificate', 'messages';
exec insertLanguageInterface 'ru', 'Insurance certificate', N'Страховой сертификат', 'messages';
exec insertLanguageInterface 'uk', 'Insurance certificate', N'Страховий сертифікат', 'messages';
exec insertLanguageInterface 'en', 'Registration address', N'Registration address', 'messages';
exec insertLanguageInterface 'ru', 'Registration address', N'Адрес регистрации', 'messages';
exec insertLanguageInterface 'uk', 'Registration address', N'Адреса реєстрації', 'messages';
exec insertLanguageInterface 'en', 'Legal name', N'Legal name', 'messages';
exec insertLanguageInterface 'ru', 'Legal name', N'Юридическое название', 'messages';
exec insertLanguageInterface 'uk', 'Legal name', N'Юридична назва', 'messages';
exec insertLanguageInterface 'en', 'Interaction', N'Interaction', 'messages';
exec insertLanguageInterface 'ru', 'Interaction', N'Взаимодействие', 'messages';
exec insertLanguageInterface 'uk', 'Interaction', N'Взаємодія', 'messages';
exec insertLanguageInterface 'en', 'Legal status', N'Legal status', 'messages';
exec insertLanguageInterface 'ru', 'Legal status', N'Правовой статус', 'messages';
exec insertLanguageInterface 'uk', 'Legal status', N'Правовий статус', 'messages';
exec insertLanguageInterface 'en', 'Passport', N'Passport', 'messages';
exec insertLanguageInterface 'ru', 'Passport', N'Паспорт', 'messages';
exec insertLanguageInterface 'uk', 'Passport', N'Паспорт', 'messages';
exec insertLanguageInterface 'en', 'Series number', N'Series number', 'messages';
exec insertLanguageInterface 'ru', 'Series number', N'Номер серии', 'messages';
exec insertLanguageInterface 'uk', 'Series number', N'Номер серії', 'messages';
exec insertLanguageInterface 'en', 'When issued', N'When issued', 'messages';
exec insertLanguageInterface 'ru', 'When issued', N'Когда выдан', 'messages';
exec insertLanguageInterface 'uk', 'When issued', N'Коли виданий', 'messages';
exec insertLanguageInterface 'en', 'Issued by', N'Issued by', 'messages';
exec insertLanguageInterface 'ru', 'Issued by', N'Кем выдан', 'messages';
exec insertLanguageInterface 'uk', 'Issued by', N'Ким видано', 'messages';
exec insertLanguageInterface 'en', 'Coordinates', N'Coordinates', 'messages';
exec insertLanguageInterface 'ru', 'Coordinates', N'Координаты', 'messages';
exec insertLanguageInterface 'uk', 'Coordinates', N'Координати', 'messages';
exec insertLanguageInterface 'en', 'Series', N'Series', 'messages';
exec insertLanguageInterface 'ru', 'Series', N'Серия', 'messages';
exec insertLanguageInterface 'uk', 'Series', N'Серiя', 'messages';
exec insertLanguageInterface 'en', 'First Name', N'First Name', 'messages';
exec insertLanguageInterface 'ru', 'First Name', N'Имя', 'messages';
exec insertLanguageInterface 'uk', 'First Name', N'Ім''я', 'messages';
exec insertLanguageInterface 'en', 'Last Name', N'Last Name', 'messages';
exec insertLanguageInterface 'ru', 'Last Name', N'Фамилия', 'messages';
exec insertLanguageInterface 'uk', 'Last Name', N'Прізвище', 'messages';
exec insertLanguageInterface 'en', 'Middle Name', N'Middle Name', 'messages';
exec insertLanguageInterface 'ru', 'Middle Name', N'Отчество', 'messages';
exec insertLanguageInterface 'uk', 'Middle Name', N'По батькові', 'messages';
exec insertLanguageInterface 'en', 'Companies', N'Companies', 'messages';
exec insertLanguageInterface 'ru', 'Companies', N'Учреждения', 'messages';
exec insertLanguageInterface 'uk', 'Companies', N'Установи', 'messages';
exec insertLanguageInterface 'en', 'Actions', N'Actions', 'messages';
exec insertLanguageInterface 'ru', 'Actions', N'Задачи', 'messages';
exec insertLanguageInterface 'uk', 'Actions', N'Задачи', 'messages';
exec insertLanguageInterface 'en', 'DV goals', N'DV goals', 'messages';
exec insertLanguageInterface 'ru', 'DV goals', N'Цели ДВ', 'messages';
exec insertLanguageInterface 'uk', 'DV goals', N'Цілі ПВ', 'messages';
exec insertLanguageInterface 'en', 'Medical representative', N'Medical representative', 'messages';
exec insertLanguageInterface 'ru', 'Medical representative', N'Медицинский представитель', 'messages';
exec insertLanguageInterface 'uk', 'Medical representative', N'Медичний представник', 'messages';

delete from info_languageinterface where [key]='Tax number';
delete from info_languageinterface where [key]='Suburb' and domain='messages';
delete from po_entityfields where field in ('agensy_main_agency', 'agensy_parent_agency');
update po_entityfields set name='Account MDM ID' where field='morionid';
update po_entityfields set name='Share' where field='info_promoproject.action';
update po_entityfields set name='Topics of task' where name='Task subjects';
update po_entityfields set name='KAM Agreement' where name='KAM agreement';
update po_entityfields set name='Training participants' where name='Training block';

exec insertLanguageInterface 'en', 'Is main', N'Main company (flag)', 'messages';
exec insertLanguageInterface 'ru', 'Is main', N'Главное учреждение (флаг)', 'messages';
exec insertLanguageInterface 'uk', 'Is main', N'Головна установа (флаг)', 'messages';
exec insertLanguageInterface 'en', 'Main company', N'Main company', 'messages';
exec insertLanguageInterface 'ru', 'Main company', N'Главное учреждение', 'messages';
exec insertLanguageInterface 'uk', 'Main company', N'Головна установа', 'messages';
exec insertLanguageInterface 'en', 'TAX number', N'TAX number', 'messages';
exec insertLanguageInterface 'ru', 'TAX number', N'Налоговый номер', 'messages';
exec insertLanguageInterface 'uk', 'TAX number', N'Податковий номер', 'messages';
exec insertLanguageInterface 'en', 'Suburb', N'Suburb', 'messages';
exec insertLanguageInterface 'ru', 'Suburb', N'Район', 'messages';
exec insertLanguageInterface 'uk', 'Suburb', N'Район', 'messages';
exec insertLanguageInterface 'en', 'Account MDM ID', N'Account MDM ID', 'messages';
exec insertLanguageInterface 'ru', 'Account MDM ID', N'Аккаунт MDM ID', 'messages';
exec insertLanguageInterface 'uk', 'Account MDM ID', N'Аккаунт MDM ID', 'messages';
exec insertLanguageInterface 'en', 'Associated Doctors', N'Associated Doctors', 'messages';
exec insertLanguageInterface 'ru', 'Associated Doctors', N'Связанные врачи', 'messages';
exec insertLanguageInterface 'uk', 'Associated Doctors', N'Пов''язані лікарі', 'messages';
exec insertLanguageInterface 'en', 'Auto Category', N'Auto Category', 'messages';
exec insertLanguageInterface 'ru', 'Auto Category', N'Автоматическая категория', 'messages';
exec insertLanguageInterface 'uk', 'Auto Category', N'Автоматична категорiя', 'messages';
exec insertLanguageInterface 'en', 'Hide sip telephony call button', N'Hide sip telephony call button', 'messages';
exec insertLanguageInterface 'ru', 'Hide sip telephony call button', N'Скрытие кнопки вызова sip телефонии', 'messages';
exec insertLanguageInterface 'uk', 'Hide sip telephony call button', N'Приховування кнопки виклику sip телефонії', 'messages';
exec insertLanguageInterface 'en', 'Social connections', N'Social connections', 'messages';
exec insertLanguageInterface 'ru', 'Social connections', N'Социальные связи', 'messages';
exec insertLanguageInterface 'uk', 'Social connections', N'Соціальні звя''зки', 'messages';
exec insertLanguageInterface 'en', 'Administrative connections', N'Administrative connections', 'messages';
exec insertLanguageInterface 'ru', 'Administrative connections', N'Административные связи', 'messages';
exec insertLanguageInterface 'uk', 'Administrative connections', N'Адміністративні зв''язки', 'messages';
exec insertLanguageInterface 'en', 'Prescriber Pharmacy', N'Prescriber Pharmacy', 'messages';
exec insertLanguageInterface 'ru', 'Prescriber Pharmacy', N'Прескрайбер-аптека', 'messages';
exec insertLanguageInterface 'uk', 'Prescriber Pharmacy', N'Прескрайбер-аптека', 'messages';
exec insertLanguageInterface 'en', 'General comment', N'General comment', 'messages';
exec insertLanguageInterface 'ru', 'General comment', N'Общий комментарий', 'messages';
exec insertLanguageInterface 'uk', 'General comment', N'Загальний коментар', 'messages';
exec insertLanguageInterface 'en', 'INN surveys', N'INN surveys', 'messages';
exec insertLanguageInterface 'ru', 'INN surveys', N'МНН Анкеты', 'messages';
exec insertLanguageInterface 'uk', 'INN surveys', N'МНН Анкети', 'messages';
exec insertLanguageInterface 'en', 'Task subjects', N'Task subjects', 'messages';
exec insertLanguageInterface 'ru', 'Task subjects', N'Темы визита', 'messages';
exec insertLanguageInterface 'uk', 'Task subjects', N'Темы визита', 'messages';
exec insertLanguageInterface 'en', 'QR Promo', N'QR Promo', 'messages';
exec insertLanguageInterface 'ru', 'QR Promo', N'QR Промокод', 'messages';
exec insertLanguageInterface 'uk', 'QR Promo', N'QR Промокод', 'messages';
exec insertLanguageInterface 'en', 'Invitation Type', N'Invitation Type', 'messages';
exec insertLanguageInterface 'ru', 'Invitation Type', N'Тип приглашения', 'messages';
exec insertLanguageInterface 'uk', 'Invitation Type', N'Тип запрошення', 'messages';
exec insertLanguageInterface 'en', 'TCP assessment form', N'TCP assessment form', 'messages';
exec insertLanguageInterface 'ru', 'TCP assessment form', N'Форма оценки ПТС', 'messages';
exec insertLanguageInterface 'uk', 'TCP assessment form', N'Форма оцiки ПТС', 'messages';

delete from info_languageinterface where [key]='Medical Representative';
update info_languageinterface set translation = 'Цель на следующую сессию ДВ' where translation = 'Цель на следующую сессию DV';
update info_languageinterface set translation = 'Мета на наступну сесію ПВ' where translation = 'Мета на наступну сесію DV';
update info_languageinterface set translation = 'Мета на поточну сесію ПВ' where translation = 'Мета на поточну сесію ДВ';
update info_languageinterface set [key] = 'Action type' where [key] = 'Action Type' and domain='messages';
delete from po_entityfields where name='Task action' and page=4;
delete from po_entityfields where field='tenderfact';
delete from po_entityfields where field='tenderplan';
delete from po_entityfields where field='department';
update po_entityfields set [group] = 'reports' where field='info_tasktenderplan.tenderplan';
update po_entityfields set [group] = 'reports' where field='info_tasktenderfact.tenderfact';
update po_entityfields set [group] = 'reports' where field='info_kamplan2.planfactkam';
update po_entityfields set name = 'Department' where name ='Branch';
update info_languageinterface set translation ='Kлієнти' where translation ='клієнти';
update info_languageinterface set translation ='Привілеї' where translation ='привілеї';
update po_entityfields set field = 'info_contact.legal_name' where field ='legal_name';
update po_entityfields set name = 'Legal name' where field ='info_contact.legal_name';

exec insertLanguageInterface 'en', 'Medical representative', N'Medical representative', 'messages';
exec insertLanguageInterface 'ru', 'Medical representative', N'Медицинский представитель', 'messages';
exec insertLanguageInterface 'uk', 'Medical representative', N'Медичний представник', 'messages';
exec insertLanguageInterface 'en', 'Action type', N'Action type', 'messages';
exec insertLanguageInterface 'ru', 'Action type', N'Тип задачи', 'messages';
exec insertLanguageInterface 'uk', 'Action type', N'Тип задачи', 'messages';
exec insertLanguageInterface 'en', 'Department', N'Department', 'messages';
exec insertLanguageInterface 'ru', 'Department', N'Отделение', 'messages';
exec insertLanguageInterface 'uk', 'Department', N'Відділення', 'messages';
exec insertLanguageInterface 'en', 'Client', N'Client', 'messages';
exec insertLanguageInterface 'ru', 'Client', N'Клиент', 'messages';
exec insertLanguageInterface 'uk', 'Client', N'Клієнт', 'messages';
exec insertLanguageInterface 'en', 'Legal name', N'Legal Name', 'messages';
exec insertLanguageInterface 'ru', 'Legal name', N'Юридическое название', 'messages';
exec insertLanguageInterface 'uk', 'Legal name', N'Юридична назва', 'messages';
exec insertLanguageInterface 'en', 'Passport fields', N'Passport fields', 'messages';
exec insertLanguageInterface 'ru', 'Passport fields', N'Паспортные данные', 'messages';
exec insertLanguageInterface 'uk', 'Passport fields', N'Паспортнi данi', 'messages';

update info_languageinterface set translation='Форма согласия' where [key]='Consent form' and language_id=2;
update info_languageinterface set translation='Форма згоди' where [key]='Consent form' and language_id=3;
delete from po_entityfields where field='info_promoproject.marketingProject' and [group] is null;
delete from po_entityfields where field='info_companypromo' and [group] is null;
delete from po_entityfields where field='isarchive';
delete from po_entityfields where field='info_tasktenderplan.tenderplan' and [group] is null;
delete from po_entityfields where field='info_kamplan2.planfactkam' and [group] is null;
delete from po_entityfields where field='info_contactpotential.pervostolniki' and [group] is null;
delete from po_entityfields where field='info_goal.company_goals' and [group] is null;
delete from po_entityfields where field='info_tasktenderplan.tenderplan' and [group] is null;
update po_entityfields set name ='Promo materials' where field='info_taskmaterial';
update po_entityfields set [group] ='reports' where field='info_taskmaterial';
update po_entityfields set name='Is main' where field='ismain';
update po_entityfields set name='Main company' where field='main_id';
delete from info_languageinterface where [key]='Main' and domain='messages';
delete from info_languageinterface where [key]='Main company(ID)' and domain='messages';
delete from info_languageinterface where [key]='Tax number';
delete from info_languageinterface where [key]='Suburb' and domain='messages';
delete from po_entityfields where field in ('agensy_main_agency', 'agensy_parent_agency');
update po_entityfields set name='Account MDM ID' where field='morionid';
update po_entityfields set name='Share' where field='info_promoproject.action';
update po_entityfields set name='Topics of task' where name='Task subjects';
update po_entityfields set name='KAM Agreement' where name='KAM agreement';
update po_entityfields set name='Training participants' where name='Training block';
update po_entityfields set name='Hide sip-call' where name='Hide sip telephony call button';
update po_entityfields set name='QR Promocode' where name='QR Promo';
update po_entityfields set name='Related doctors' where field='info_contactconnection.company_slave_id' and page=1;
update po_entityfields set name='Company relation' where field='info_contactconnection.company_slave_id' and page=2;
update po_entityfields set name='Administrative relations' where field='info_contactconnection.admin_slave_id';
update po_entityfields set name='Social relations' where field='info_contactconnection.social_slave_id';
update po_entityfields set name='District' where field='suburb' and page=1;
update po_entityfields set name='MHH Questionnaires' where field='task_surveys_by_inn';

exec insertLanguageInterface 'en', 'Is main', N'Main company (flag)', 'messages';
exec insertLanguageInterface 'ru', 'Is main', N'Главное учреждение (флаг)', 'messages';
exec insertLanguageInterface 'uk', 'Is main', N'Головна установа (флаг)', 'messages';
exec insertLanguageInterface 'en', 'Main company', N'Main company', 'messages';
exec insertLanguageInterface 'ru', 'Main company', N'Главное учреждение', 'messages';
exec insertLanguageInterface 'uk', 'Main company', N'Головна установа', 'messages';
exec insertLanguageInterface 'en', 'TAX number', N'TAX number', 'messages';
exec insertLanguageInterface 'ru', 'TAX number', N'Налоговый номер', 'messages';
exec insertLanguageInterface 'uk', 'TAX number', N'Податковий номер', 'messages';
exec insertLanguageInterface 'en', 'Account MDM ID', N'Account MDM ID', 'messages';
exec insertLanguageInterface 'ru', 'Account MDM ID', N'Аккаунт MDM ID', 'messages';
exec insertLanguageInterface 'uk', 'Account MDM ID', N'Аккаунт MDM ID', 'messages';
exec insertLanguageInterface 'en', 'Auto Category', N'Auto Category', 'messages';
exec insertLanguageInterface 'ru', 'Auto Category', N'Автоматическая категория', 'messages';
exec insertLanguageInterface 'uk', 'Auto Category', N'Автоматична категорiя', 'messages';
exec insertLanguageInterface 'en', 'Prescriber Pharmacy', N'Prescriber Pharmacy', 'messages';
exec insertLanguageInterface 'ru', 'Prescriber Pharmacy', N'Прескрайбер-аптека', 'messages';
exec insertLanguageInterface 'uk', 'Prescriber Pharmacy', N'Прескрайбер-аптека', 'messages';
exec insertLanguageInterface 'en', 'General comment', N'General comment', 'messages';
exec insertLanguageInterface 'ru', 'General comment', N'Общий комментарий', 'messages';
exec insertLanguageInterface 'uk', 'General comment', N'Загальний коментар', 'messages';
exec insertLanguageInterface 'en', 'Task subjects', N'Task subjects', 'messages';
exec insertLanguageInterface 'ru', 'Task subjects', N'Темы визита', 'messages';
exec insertLanguageInterface 'uk', 'Task subjects', N'Темы визита', 'messages';
exec insertLanguageInterface 'en', 'Invitation Type', N'Invitation Type', 'messages';
exec insertLanguageInterface 'ru', 'Invitation Type', N'Тип приглашения', 'messages';
exec insertLanguageInterface 'uk', 'Invitation Type', N'Тип запрошення', 'messages';
exec insertLanguageInterface 'en', 'TCP assessment form', N'TCP assessment form', 'messages';
exec insertLanguageInterface 'ru', 'TCP assessment form', N'Форма оценки ПТС', 'messages';
exec insertLanguageInterface 'uk', 'TCP assessment form', N'Форма оцiки ПТС', 'messages';
exec insertLanguageInterface 'en', 'Lecturers', N'Lecturers', 'messages';
exec insertLanguageInterface 'ru', 'Lecturers', N'Лекторы', 'messages';
exec insertLanguageInterface 'uk', 'Lecturers', N'Лектори', 'messages';

exec insertLanguageInterface 'en', 'Are you sure, you want to delete selected tab?', N'Are you sure, you want to delete selected tab?', 'messages'
exec insertLanguageInterface 'ru', 'Are you sure, you want to delete selected tab?', N'Вы уверены, что хотите удалить выбранную вкладку?', 'messages'
exec insertLanguageInterface 'uk', 'Are you sure, you want to delete selected tab?', N'Ви впевнені, що хочете видалити вибрану вкладку?', 'messages'
exec insertLanguageInterface 'en', 'Delete tab', N'Delete tab', 'messages'
exec insertLanguageInterface 'ru', 'Delete tab', N'Удалить вкладку', 'messages'
exec insertLanguageInterface 'uk', 'Delete tab', N'Видалити вкладку', 'messages'

DELETE from info_languageinterface where [key] = 'File Name' and domain = 'messages';
exec insertLanguageInterface 'en', 'File name', N'File name', 'messages';
exec insertLanguageInterface 'ru', 'File name', N'Название файла', 'messages';
exec insertLanguageInterface 'uk', 'File name', N'Назва файлу', 'messages';
GO
------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------

------------------------------------------------------------------------------------------------------------------------
--https://teamsoft.atlassian.net/browse/UKEKSPO-24
------------------------------------------------------------------------------------------------------------------------
exec insertLanguageInterface  'en', 'Token not received', N'Token not received', 'mcm'
exec insertLanguageInterface  'ru', 'Token not received', N'Токен не получен', 'mcm'
exec insertLanguageInterface  'uk', 'Token not received', N'Токен не отримано', 'mcm'
GO
exec insertLanguageInterface  'en', 'Token', N'Token', 'mcm'
exec insertLanguageInterface  'ru', 'Token', N'Токен', 'mcm'
exec insertLanguageInterface  'uk', 'Token', N'Токен', 'mcm'
GO
exec insertLanguageInterface  'en', 'Copy', N'Copy', 'mcm'
exec insertLanguageInterface  'ru', 'Copy', N'Копировать', 'mcm'
exec insertLanguageInterface  'uk', 'Copy', N'Копіювати', 'mcm'
GO
------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------

------------------------------------------------------------------------------------------------------------------------
--https://teamsoft.atlassian.net/browse/MCM-389
------------------------------------------------------------------------------------------------------------------------
exec insertLanguageInterface  'en', 'Conference', N'Conference', 'mcm'
exec insertLanguageInterface  'ru', 'Conference', N'Конференция', 'mcm'
exec insertLanguageInterface  'uk', 'Conference', N'Конференція', 'mcm'
GO
exec insertLanguageInterface  'en', 'Participation format', N'Participation format', 'mcm'
exec insertLanguageInterface  'ru', 'Participation format', N'Формат участия', 'mcm'
exec insertLanguageInterface  'uk', 'Participation format', N'Формат участі', 'mcm'
GO
exec insertLanguageInterface  'en', 'Date', N'Date', 'mcm'
exec insertLanguageInterface  'ru', 'Date', N'Дата', 'mcm'
exec insertLanguageInterface  'uk', 'Date', N'Дата', 'mcm'
GO
exec insertLanguageInterface  'en', 'Company', N'Company', 'mcm'
exec insertLanguageInterface  'ru', 'Company', N'Компания', 'mcm'
exec insertLanguageInterface  'uk', 'Company', N'Установа', 'mcm'
GO
exec insertLanguageInterface  'en', 'Address', N'Address', 'mcm'
exec insertLanguageInterface  'ru', 'Address', N'Адрес', 'mcm'
exec insertLanguageInterface  'uk', 'Address', N'Адреса', 'mcm'
GO
exec insertLanguageInterface  'en', 'Full name of contact person', N'Full name of contact person', 'mcm'
exec insertLanguageInterface  'ru', 'Full name of contact person', N'ФИО контактного лица', 'mcm'
exec insertLanguageInterface  'uk', 'Full name of contact person', N'ПІБ контактної особи', 'mcm'
GO
exec insertLanguageInterface  'en', 'Phone number of contact person', N'Phone number of contact person', 'mcm'
exec insertLanguageInterface  'ru', 'Phone number of contact person', N'Телефон контактного лица', 'mcm'
exec insertLanguageInterface  'uk', 'Phone number of contact person', N'Телефон контактної особи', 'mcm'
GO
exec insertLanguageInterface  'en', 'Code EGRPOU', N'Code EGRPOU', 'mcm'
exec insertLanguageInterface  'ru', 'Code EGRPOU', N'Код ЕГРПОУ', 'mcm'
exec insertLanguageInterface  'uk', 'Code EGRPOU', N'Код ЄДРПОУ', 'mcm'
GO
exec insertLanguageInterface  'en', 'Contact person email', N'Contact person email', 'mcm'
exec insertLanguageInterface  'ru', 'Contact person email', N'Email контактного лица', 'mcm'
exec insertLanguageInterface  'uk', 'Contact person email', N'Email контактної особи', 'mcm'
GO
exec insertLanguageInterface  'en', 'Member surname', N'Member surname', 'mcm'
exec insertLanguageInterface  'ru', 'Member surname', N'Фамилия участника', 'mcm'
exec insertLanguageInterface  'uk', 'Member surname', N'Прізвище учасника', 'mcm'
GO
exec insertLanguageInterface  'en', 'Member name', N'Member name', 'mcm'
exec insertLanguageInterface  'ru', 'Member name', N'Имя участника', 'mcm'
exec insertLanguageInterface  'uk', 'Member name', N'Iм''я учасника', 'mcm'
GO
exec insertLanguageInterface  'en', 'Member patronymic', N'Member patronymic', 'mcm'
exec insertLanguageInterface  'ru', 'Member patronymic', N'Отчество участника', 'mcm'
exec insertLanguageInterface  'uk', 'Member patronymic', N'По батькові учасника', 'mcm'
GO
exec insertLanguageInterface  'en', 'Member email', N'Member email', 'mcm'
exec insertLanguageInterface  'ru', 'Member email', N'Email участника', 'mcm'
exec insertLanguageInterface  'uk', 'Member email', N'Email учасника', 'mcm'
GO
exec insertLanguageInterface  'en', 'Member phone', N'Member phone', 'mcm'
exec insertLanguageInterface  'ru', 'Member phone', N'Телефон участника', 'mcm'
exec insertLanguageInterface  'uk', 'Member phone', N'Телефон учасника', 'mcm'
GO
exec insertLanguageInterface  'en', 'Member position', N'Member position', 'mcm'
exec insertLanguageInterface  'ru', 'Member position', N'Должность участника', 'mcm'
exec insertLanguageInterface  'uk', 'Member position', N'Посада учасника', 'mcm'
GO
exec insertLanguageInterface  'en', 'Promo code', N'Promo code', 'mcm'
exec insertLanguageInterface  'ru', 'Promo code', N'Промокод', 'mcm'
exec insertLanguageInterface  'uk', 'Promo code', N'Промокод', 'mcm'
GO
exec insertLanguageInterface  'en', 'Amount', N'Amount', 'mcm'
exec insertLanguageInterface  'ru', 'Amount', N'Сумма', 'mcm'
exec insertLanguageInterface  'uk', 'Amount', N'Сума', 'mcm'
GO
------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------

------------------------------------------------------------------------------------------------------------------------
--translation for GeoForce
------------------------------------------------------------------------------------------------------------------------
update info_languageinterface set translation=N'Города' where [key] ='cities' and domain='geomarketing'  and language_id in (select id from info_language where slug='ru');
update info_languageinterface set translation=N'Міста' where [key] ='cities' and domain='geomarketing' and language_id in (select id from info_language where slug='uk');

update info_languageinterface set translation=N'Товарооборот' where [key] ='cities' and domain='geomarketing'  and language_id in (select id from info_language where slug='ru');
update info_languageinterface set translation=N'Товарообіг' where [key] ='turnover' and domain='geomarketing' and language_id in (select id from info_language where slug='uk');
GO
------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------

------------------------------------------------------------------------------------------------------------------------
--https://teamsoft.atlassian.net/browse/EGISRU-293
------------------------------------------------------------------------------------------------------------------------
exec insertLanguageInterface  'en', 'Preview', N'Preview', 'mcm'
exec insertLanguageInterface  'ru', 'Preview', N'Предпросмотр', 'mcm'
exec insertLanguageInterface  'uk', 'Preview', N'Попередній перегляд', 'mcm'
GO
------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------
