------------------------------------------------------------------------------------------------------------------------
-- https://teamsoft.atlassian.net/browse/PHARMAWEB-454
------------------------------------------------------------------------------------------------------------------------
EXEC insertLanguageInterface 'en','Copy settings from other role','Copy settings from other role','messages';
EXEC insertLanguageInterface 'ru','Copy settings from other role',N'Копировать настройки из роли','messages';
EXEC insertLanguageInterface 'uk','Copy settings from other role',N'Копіювати налаштування з ролі','messages';

EXEC insertLanguageInterface 'en','Select a role to copy the settings from','Select a role to copy the settings from','messages';
EXEC insertLanguageInterface 'ru','Select a role to copy the settings from',N'Выберите роль, из которой нужно скопировать настройки','messages';
EXEC insertLanguageInterface 'uk','Select a role to copy the settings from',N'Виберіть роль, з якої потрібно скопіювати налаштування','messages';

EXEC insertLanguageInterface 'en','Settings has been applied to the role','Settings has been applied to the role','messages';
EXEC insertLanguageInterface 'ru','Settings has been applied to the role',N'Настройки были применены к роли','messages';
EXEC insertLanguageInterface 'uk','Settings has been applied to the role',N'Налаштування застосовано до ролі','messages';
GO
------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------
-- https://teamsoft.atlassian.net/browse/MCM-504
------------------------------------------------------------------------------------------------------------------------
IF NOT EXISTS(SELECT 1 FROM info_options WHERE name = 'RTCCallEndedText')
BEGIN
INSERT INTO info_options (name, value) VALUES ('RTCCallEndedText', 'RTC_CALL_ENDED_TEXT')
END;
GO

exec insertLanguageInterface 'en', 'RTC_CALL_ENDED_TEXT', N'Video call appointment expired on %TASK_DATETILL%', 'crm'
exec insertLanguageInterface 'ru', 'RTC_CALL_ENDED_TEXT', N'Срок видеозвонка вышел %TASK_DATETILL%', 'crm'
exec insertLanguageInterface 'uk', 'RTC_CALL_ENDED_TEXT', N'Термін дії відеовиклику вийшов %TASK_DATETILL%', 'crm'
GO
------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------
-- https://teamsoft.atlassian.net/browse/FARMAK-378
----------------------------------------------------------------------------------------------------
IF NOT EXISTS (SELECT * FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'info_user' AND COLUMN_NAME = 'flags_due_date')
BEGIN
ALTER TABLE info_user ADD flags_due_date DATE
END;
GO
----------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------
-- https://teamsoft.atlassian.net/browse/DRREDDYSRO-98
----------------------------------------------------------------------------------------------------
exec insertLanguageInterface 'en', 'access to GDPR collection', 'access to GDPR collection', 'messages';
exec insertLanguageInterface 'ru', 'access to GDPR collection', 'доступ к коллекции GDPR', 'messages';
exec insertLanguageInterface 'uk', 'access to GDPR collection', 'доступ до колекції GDPR', 'messages';
GO
----------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------
-- https://teamsoft.atlassian.net/browse/EGISRU-538
------------------------------------------------------------------------------------------------------------------------
exec insertLanguageInterface 'en', 'the lower limit of the number of pharmacies', 'the lower limit of the number of pharmacies', 'targeting';
exec insertLanguageInterface 'ru', 'the lower limit of the number of pharmacies', 'нижняя граница количества аптек', 'targeting';
exec insertLanguageInterface 'uk', 'the lower limit of the number of pharmacies', 'нижня межа кількості аптек', 'targeting';
GO

exec insertLanguageInterface 'en', 'the upper limit of the number of pharmacies', 'the upper limit of the number of pharmacies', 'targeting';
exec insertLanguageInterface 'ru', 'the upper limit of the number of pharmacies', 'верхняя граница количества аптек', 'targeting';
exec insertLanguageInterface 'uk', 'the upper limit of the number of pharmacies', 'верхня межа кількості аптек', 'targeting';
GO

exec insertLanguageInterface 'en', 'the lower limit of the number of visits', 'the lower limit of the number of visits', 'targeting';
exec insertLanguageInterface 'ru', 'the lower limit of the number of visits', 'нижний предел количества посещений', 'targeting';
exec insertLanguageInterface 'uk', 'the lower limit of the number of visits', 'нижня межа кількості відвідувань', 'targeting';
GO

exec insertLanguageInterface 'en', 'the upper limit of the number of visits', 'the upper limit of the number of visits', 'targeting';
exec insertLanguageInterface 'ru', 'the upper limit of the number of visits', 'верхний предел количества посещений', 'targeting';
exec insertLanguageInterface 'uk', 'the upper limit of the number of visits', 'верхня межа кількості відвідувань', 'targeting';
GO
------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------
--https://teamsoft.atlassian.net/browse/EGISRU-528
------------------------------------------------------------------------------------------------------------------------
IF OBJECT_ID(N'info_targetingactivitylog', 'U') is NULL
begin
create table [dbo].[info_targetingactivitylog]
(
    [id] int identity (1,1) not null,
    [user_id] int not null,
    [panel_user_id] int not null,
    [panel_type] varchar(255) not null,
    [approvementstatus] int not null,
    [period] varchar(255) not null,
    [action_date] datetime not null
    CONSTRAINT [DF_info_targetingactivitylogaction_date] DEFAULT (getdate()),
    [currenttime]       datetime           null
    CONSTRAINT [DF_info_targetingactivitylogcurrenttime] DEFAULT (getdate()),
    [time]              timestamp          null,
    [moduser]           varchar(16)        null
    CONSTRAINT [DF_info_targetingactivitylogmoduser] DEFAULT ([dbo].[Get_CurrentCode]()),
    [guid]              uniqueidentifier   null
    CONSTRAINT [DF_info_targetingactivitylogguid] DEFAULT (newid()),
    CONSTRAINT [PK_info_targetingactivitylog] PRIMARY KEY (id) ON [PRIMARY],
    CONSTRAINT [FK_info_targetingactivitylog_user_id] FOREIGN KEY ([user_id]) REFERENCES info_user ([id]),
    CONSTRAINT [FK_info_targetingactivitylog_panel_user_id] FOREIGN KEY ([panel_user_id]) REFERENCES info_user ([id])
    )
end
GO
------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------
------- https://teamsoft.atlassian.net/browse/PHARMAWEB-679
------------------------------------------------------------------------------------------------------------------------
IF EXISTS (SELECT 1 FROM ps_pattern_types where name = N'Продажи дистрибьюторов')
BEGIN
    DECLARE @typeID INT = (SELECT TOP 1 id FROM ps_pattern_types WHERE name = N'Продажи дистрибьюторов');

    IF NOT EXISTS(SELECT * FROM ps_distributor_field WHERE field_name = 'sale_type')
    BEGIN
        INSERT INTO ps_distributor_field (name, field_name, is_required, type_id) VALUES (N'Тип продаж', 'sale_type', 0, @typeID);
    END;
END;
GO
------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------
------- https://teamsoft.atlassian.net/browse/PHARMAWEB-678
------------------------------------------------------------------------------------------------------------------------
BEGIN
    DECLARE @group_id int;
    SELECT TOP 1 @group_id = id from po_dictionarygroup where name = 'PharmaSales';

    IF NOT EXISTS (SELECT * FROM po_dictionary WHERE group_id = @group_id AND tablename = 'info_saletype' AND name = 'Sale types')
    BEGIN
        INSERT INTO po_dictionary (group_id, tablename, name) VALUES (@group_id, 'info_saletype', 'Sale types');
    END
END

IF OBJECT_ID(N'info_saletype', 'U') is NULL
    BEGIN
        CREATE TABLE info_saletype(
              [id] [int] IDENTITY (1,1) NOT NULL PRIMARY KEY
            , [name] [text]
            , [code] [text]
            , [is_for_granual] [int]
            , [is_discount] [int]
            , [currenttime] [datetime]
            , [time] [timestamp]
            , [moduser] [varchar](16)
            , [guid] [uniqueidentifier]
        );

        ALTER TABLE [dbo].[info_saletype]
            ADD CONSTRAINT [DF_info_saletypecurrenttime] DEFAULT (getdate()) FOR [currenttime]

        ALTER TABLE [dbo].[info_saletype]
            ADD CONSTRAINT [DF_info_saletypemoduser] DEFAULT ([dbo].[Get_CurrentCode]()) FOR [moduser]

        ALTER TABLE [dbo].[info_saletype]
            ADD CONSTRAINT [DF_info_saletypeguid] DEFAULT (newid()) FOR [guid]

        ALTER TABLE [dbo].[info_saletype]
            WITH CHECK ADD CONSTRAINT [FK_info_saletype_id] UNIQUE (id);
    END
GO

IF NOT EXISTS (SELECT * FROM sys.columns WHERE  object_id = OBJECT_ID(N'[dbo].[info_salefact]') AND name = 'sale_type_id')
BEGIN
    ALTER TABLE [dbo].[info_salefact] ADD [sale_type_id] INT NULL;
END
GO

IF OBJECT_ID('info_salefact', 'U') IS NOT NULL
BEGIN
    ALTER TABLE [dbo].[info_salefact] WITH CHECK ADD CONSTRAINT [FK_info_salefact_sale_type_id]
        FOREIGN KEY (sale_type_id) REFERENCES info_saletype ([id]) ON DELETE CASCADE;
END
GO

IF NOT EXISTS (SELECT * FROM sys.columns WHERE object_id = OBJECT_ID(N'[dbo].[info_saleperiod]') AND name = 'sale_type_id')
BEGIN
    ALTER TABLE [dbo].[info_saleperiod] ADD [sale_type_id] INT NULL;
END
GO

IF OBJECT_ID('info_saleperiod', 'U') IS NOT NULL
BEGIN
    ALTER TABLE [dbo].[info_saleperiod]
        WITH CHECK ADD CONSTRAINT [FK_info_saleperiod_sale_type_id] FOREIGN KEY (sale_type_id) REFERENCES info_saletype ([id]) ON DELETE CASCADE;
END
GO

IF NOT EXISTS (SELECT * FROM info_dictionaryidentifier WHERE tablename = 'info_saletype')
BEGIN
    DECLARE @en_id int, @ru_id int, @uk_id int;
    SELECT TOP 1 @en_id = id from info_language where slug = 'en';
    SELECT TOP 1 @ru_id = id from info_language where slug = 'ru';
    SELECT TOP 1 @uk_id = id from info_language where slug = 'uk';

    INSERT INTO info_dictionaryidentifier (tablename, columnname, value, order_num, language_id) values ('info_saletype', 'name', 'name', 0, @en_id);
    INSERT INTO info_dictionaryidentifier (tablename, columnname, value, order_num, language_id) values ('info_saletype', 'name', N'название', 0, @ru_id);
    INSERT INTO info_dictionaryidentifier (tablename, columnname, value, order_num, language_id) values ('info_saletype', 'name', N'назва', 0, @uk_id);
    INSERT INTO info_dictionaryidentifier (tablename, columnname, value, order_num, language_id) values ('info_saletype', 'code', 'code', 1, @en_id);
    INSERT INTO info_dictionaryidentifier (tablename, columnname, value, order_num, language_id) values ('info_saletype', 'code', N'код', 1, @ru_id);
    INSERT INTO info_dictionaryidentifier (tablename, columnname, value, order_num, language_id) values ('info_saletype', 'code', N'код', 1, @uk_id);
    INSERT INTO info_dictionaryidentifier (tablename, columnname, value, order_num, language_id) values ('info_saletype', 'is_for_granual', 'use in granular planning', 2, @en_id);
    INSERT INTO info_dictionaryidentifier (tablename, columnname, value, order_num, language_id) values ('info_saletype', 'is_for_granual', N'использовать в грануальном планировании', 2, @ru_id);
    INSERT INTO info_dictionaryidentifier (tablename, columnname, value, order_num, language_id) values ('info_saletype', 'is_for_granual', N'використовувати у грануальному плануванні', 2, @uk_id);
    INSERT INTO info_dictionaryidentifier (tablename, columnname, value, order_num, language_id) values ('info_saletype', 'is_discount', 'use in discounts', 3, @en_id);
    INSERT INTO info_dictionaryidentifier (tablename, columnname, value, order_num, language_id) values ('info_saletype', 'is_discount', N'использовать в скидках', 3, @ru_id);
    INSERT INTO info_dictionaryidentifier (tablename, columnname, value, order_num, language_id) values ('info_saletype', 'is_discount', N'використовувати у знижках', 3, @uk_id);
END;
GO

exec insertLanguageInterface 'en', 'Sales types', N'Sales types', 'messages';
exec insertLanguageInterface 'ru', 'Sales types', N'Типы продаж', 'messages';
exec insertLanguageInterface 'uk', 'Sales types', N'Типи продажів', 'messages';
GO
------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------
----- https://teamsoft.atlassian.net/browse/GEODATA-865
------------------------------------------------------------------------------------------------------------------------
exec deleteLanguageInterface 'Go to info', 'geomarketing';

exec insertLanguageInterface 'en', 'Go to info', N'Go to info', 'geomarketing';
exec insertLanguageInterface 'ru', 'Go to info', N'Информация', 'geomarketing';
exec insertLanguageInterface 'uk', 'Go to info', N'Інформація', 'geomarketing';

exec insertLanguageInterface 'en', 'Do you want to delete this polygon?', N'Do you want to delete this polygon?', 'geomarketing';
exec insertLanguageInterface 'ru', 'Do you want to delete this polygon?', N'Вы хотите удалить этот полигон?', 'geomarketing';
exec insertLanguageInterface 'uk', 'Do you want to delete this polygon?', N'Ви бажаєте видалити цей полігон?', 'geomarketing';
GO
------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------
------- https://teamsoft.atlassian.net/browse/EGISRU-539
------------------------------------------------------------------------------------------------------------------------
exec insertLanguageInterface 'en', 'Dismiss successfully!', 'Dismiss successfully!', 'targeting';
exec insertLanguageInterface 'ru', 'Dismiss successfully!', 'Успешно отклонено!', 'targeting';
exec insertLanguageInterface 'uk', 'Dismiss successfully!', 'Успішно відхилено!', 'targeting';

exec insertLanguageInterface 'en', 'Dismissing failed. Status text error:', 'Dismissing failed. Status text error:', 'targeting';
exec insertLanguageInterface 'ru', 'Dismissing failed. Status text error:', 'Отклонить не удалось. Текстовая ошибка статуса:', 'targeting';
exec insertLanguageInterface 'uk', 'Dismissing failed. Status text error:', 'Відхилити не вдалося. Текстова помилка статусу:', 'targeting';
GO
------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------
------- https://teamsoft.atlassian.net/browse/OL-141
------------------------------------------------------------------------------------------------------------------------
IF NOT EXISTS (SELECT 1 FROM information_schema.columns WHERE table_name = 'info_mcmfrom' AND column_name = 'provider_login')
BEGIN
    ALTER TABLE info_mcmfrom ADD provider_login varchar(255) NULL
END
GO

IF NOT EXISTS (SELECT 1 FROM information_schema.columns WHERE table_name = 'info_mcmfrom' AND column_name = 'provider_password')
BEGIN
    ALTER TABLE info_mcmfrom ADD provider_password varchar(255) NULL
END
GO
------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------
