------------------------------------------------------------------------------------------------------------------------
------- https://teamsoft.atlassian.net/browse/EGISRU-540
------------------------------------------------------------------------------------------------------------------------
IF NOT EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE table_name = 'info_contactphone' AND column_name = 'ismcmcall')
ALTER TABLE info_contactphone ADD ismcmcall integer NULL
GO
------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------
-- https://teamsoft.atlassian.net/browse/DRREDDYSKZ-242
----------------------------------------------------------------------------------------------------
exec insertLanguageInterface 'en', 'Confirm action', N'Confirm action', 'targeting';
exec insertLanguageInterface 'ru', 'Confirm action', N'Подтвердить действие', 'targeting';
exec insertLanguageInterface 'uk', 'Confirm action', N'Підтвердити дію', 'targeting';
GO

exec insertLanguageInterface 'en', 'The number of doctor/visit changes is exceeded!', N'The number of doctor/visit changes is exceeded!', 'targeting';
exec insertLanguageInterface 'ru', 'The number of doctor/visit changes is exceeded!', N'Количество смен врача/посещений превышено!', 'targeting';
exec insertLanguageInterface 'uk', 'The number of doctor/visit changes is exceeded!', N'Кількість змін лікаря/відвідувань перевищена!', 'targeting';
GO

exec insertLanguageInterface 'en', 'Are you sure you want to approve the target list?', N'Are you sure you want to approve the target list?', 'targeting';
exec insertLanguageInterface 'ru', 'Are you sure you want to approve the target list?', N'Вы уверены, что хотите утвердить целевой список?', 'targeting';
exec insertLanguageInterface 'uk', 'Are you sure you want to approve the target list?', N'Ви впевнені, що хочете затвердити цільовий перелік?', 'targeting';
GO

exec insertLanguageInterface 'en', 'Cancel', N'Cancel', 'targeting';
exec insertLanguageInterface 'ru', 'Cancel', N'Отмена', 'targeting';
exec insertLanguageInterface 'uk', 'Cancel', N'Скасувати', 'targeting';
GO
----------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------------------------
-- https://teamsoft.atlassian.net/browse/GEODATA-864
-- https://teamsoft.atlassian.net/browse/GEODATA-870
---------------------------------------------------------------------------------------------------------------------
IF NOT EXISTS(SELECT 1 FROM info_options WHERE name = 'geomarketingPieChartTelWithEmail')
BEGIN
INSERT INTO info_options (name, value) VALUES ('geomarketingPieChartTelWithEmail', '0')
END;
GO

IF NOT EXISTS(SELECT 1 FROM info_options WHERE name = 'geomarketingPieChartViberWithWhatsApp')
BEGIN
INSERT INTO info_options (name, value) VALUES ('geomarketingPieChartViberWithWhatsApp', '0')
END;
GO

exec insertLanguageInterface 'en', 'MCM сhart (Viber vs WhatsApp)', 'MCM сhart (Viber vs WhatsApp)', 'geomarketing';
exec insertLanguageInterface 'ru', 'MCM сhart (Viber vs WhatsApp)', 'Диаграмма MCM (Viber vs WhatsApp)', 'geomarketing';
exec insertLanguageInterface 'uk', 'MCM сhart (Viber vs WhatsApp)', 'Діаграма MCM (Viber vs WhatsApp)', 'geomarketing';
GO

exec insertLanguageInterface 'en', 'MCM сhart (Phone vs Email)', 'MCM сhart (Phone vs Email)', 'geomarketing';
exec insertLanguageInterface 'ru', 'MCM сhart (Phone vs Email)', 'Диаграмма MCM (Phone vs Email)', 'geomarketing';
exec insertLanguageInterface 'uk', 'MCM сhart (Phone vs Email)', 'Діаграма MCM (Phone vs Email)', 'geomarketing';
GO

exec insertLanguageInterface 'en', 'MCM сhart', 'MCM сhart', 'geomarketing';
exec insertLanguageInterface 'ru', 'MCM сhart', 'Диаграмма MCM', 'geomarketing';
exec insertLanguageInterface 'uk', 'MCM сhart', 'Діаграма MCM', 'geomarketing';
GO

exec insertLanguageInterface 'en', 'MCM сhart legend', 'MCM сhart legend', 'geomarketing';
exec insertLanguageInterface 'ru', 'MCM сhart legend', 'Легенда диаграммы MCM', 'geomarketing';
exec insertLanguageInterface 'uk', 'MCM сhart legend', 'Легенда діаграми MCM', 'geomarketing';
GO

exec insertLanguageInterface 'en', 'Without MCM', 'Without MCM', 'geomarketing';
exec insertLanguageInterface 'ru', 'Without MCM', 'Без МСМ', 'geomarketing';
exec insertLanguageInterface 'uk', 'Without MCM', 'Без МСМ', 'geomarketing';
GO

exec insertLanguageInterface 'en', 'Phones', 'Phones', 'geomarketing';
exec insertLanguageInterface 'ru', 'Phones', 'Телефоны', 'geomarketing';
exec insertLanguageInterface 'uk', 'Phones', 'Телефони', 'geomarketing';
GO
---------------------------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------
-- https://teamsoft.atlassian.net/browse/SINTEZ-124
----------------------------------------------------------------------------------------------------
INSERT INTO info_options (name, value) SELECT 'collapseClmFolders', 1
    WHERE NOT EXISTS (SELECT id FROM info_options WHERE name = 'collapseClmFolders');
----------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------
-- https://teamsoft.atlassian.net/browse/MD-40
----------------------------------------------------------------------------------------------------
IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'info_AddInfoType' AND COLUMN_NAME = 'additionaltype')
BEGIN
    ALTER TABLE info_AddInfoType ADD additionaltype integer
END
GO
----------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------
-- https://teamsoft.atlassian.net/browse/PHARMAWEB-674
------------------------------------------------------------------------------------------------------------------------
EXEC insertLanguageInterface 'en', 'Storage', 'Storage', 'crm';
EXEC insertLanguageInterface 'ru', 'Storage', N'Склад', 'crm';
EXEC insertLanguageInterface 'uk', 'Storage', N'Склад', 'crm';
GO

EXEC insertLanguageInterface 'en', 'Search', 'Search', 'crm';
EXEC insertLanguageInterface 'ru', 'Search', N'Поиск', 'crm';
EXEC insertLanguageInterface 'uk', 'Search', N'Пошук', 'crm';
GO

EXEC insertLanguageInterface 'en', 'Create new storage', 'Create new storage', 'crm';
EXEC insertLanguageInterface 'ru', 'Create new storage', N'Добавить новый склад', 'crm';
EXEC insertLanguageInterface 'uk', 'Create new storage', N'Додати новий склад', 'crm';
GO

EXEC insertLanguageInterface 'en', 'Description', 'Description', 'crm';
EXEC insertLanguageInterface 'ru', 'Description', N'Описание', 'crm';
EXEC insertLanguageInterface 'uk', 'Description', N'Опис', 'crm';
GO

EXEC insertLanguageInterface 'en', 'Storage has been created.', 'Storage has been created.', 'crm';
EXEC insertLanguageInterface 'ru', 'Storage has been created.', N'Склад успешно добавлен.', 'crm';
EXEC insertLanguageInterface 'uk', 'Storage has been created.', N'Склад успішно доданий.', 'crm';
GO

EXEC insertLanguageInterface 'en', 'There are no users without storage.', 'There are no users without storage.', 'crm';
EXEC insertLanguageInterface 'ru', 'There are no users without storage.', N'Нет пользователей без склада.', 'crm';
EXEC insertLanguageInterface 'uk', 'There are no users without storage.', N'Немає користувачів без складу.', 'crm';
GO

EXEC insertLanguageInterface 'en', 'Please select user from dropdown list', 'Please select user from dropdown list', 'crm';
EXEC insertLanguageInterface 'ru', 'Please select user from dropdown list', N'Пожалуйста, выберите пользователя из выпадающего списка', 'crm';
EXEC insertLanguageInterface 'uk', 'Please select user from dropdown list', N'Будь ласка, виберіть користувача із випадаючого списку', 'crm';
GO

EXEC insertLanguageInterface 'en', 'Primary storage', 'Primary storage', 'crm';
EXEC insertLanguageInterface 'ru', 'Primary storage', N'Основной склад', 'crm';
EXEC insertLanguageInterface 'uk', 'Primary storage', N'Основний склад', 'crm';
GO

EXEC insertLanguageInterface 'en', 'Is archived', 'Is archived', 'crm';
EXEC insertLanguageInterface 'ru', 'Is archived', N'В архиве', 'crm';
EXEC insertLanguageInterface 'uk', 'Is archived', N'В архіві', 'crm';
GO

EXEC insertLanguageInterface 'en', 'Transfers', 'Transfers', 'crm';
EXEC insertLanguageInterface 'ru', 'Transfers', N'Перемещения', 'crm';
EXEC insertLanguageInterface 'uk', 'Transfers', N'Переміщення', 'crm';
GO

EXEC insertLanguageInterface 'en', 'Qty.', 'Qty.', 'crm';
EXEC insertLanguageInterface 'ru', 'Qty.', N'Кол-во', 'crm';
EXEC insertLanguageInterface 'uk', 'Qty.', N'К-ть', 'crm';
GO

EXEC insertLanguageInterface 'en', 'Date received', 'Date received', 'crm';
EXEC insertLanguageInterface 'ru', 'Date received', N'Дата получения', 'crm';
EXEC insertLanguageInterface 'uk', 'Date received', N'Дата отримання', 'crm';
GO

EXEC insertLanguageInterface 'en', 'Qty. received', 'Qty. received', 'crm';
EXEC insertLanguageInterface 'ru', 'Qty. received', N'К-во получено', 'crm';
EXEC insertLanguageInterface 'uk', 'Qty. received', N'К-ть отримано', 'crm';
GO

EXEC insertLanguageInterface 'en', 'Receive', 'Receive', 'crm';
EXEC insertLanguageInterface 'ru', 'Receive', N'Получение', 'crm';
EXEC insertLanguageInterface 'uk', 'Receive', N'Отримання', 'crm';
GO

EXEC insertLanguageInterface 'en', 'Request date', 'Request date', 'crm';
EXEC insertLanguageInterface 'ru', 'Request date', N'Дата запроса', 'crm';
EXEC insertLanguageInterface 'uk', 'Request date', N'Дата запиту', 'crm';
GO

EXEC insertLanguageInterface 'en', 'Receive date', 'Receive date', 'crm';
EXEC insertLanguageInterface 'ru', 'Receive date', N'Дата получения', 'crm';
EXEC insertLanguageInterface 'uk', 'Receive date', N'Дата отримання', 'crm';
GO

EXEC insertLanguageInterface 'en', 'Resolved', 'Resolved', 'crm';
EXEC insertLanguageInterface 'ru', 'Resolved', N'Обработан', 'crm';
EXEC insertLanguageInterface 'uk', 'Resolved', N'Оброблений', 'crm';
GO

EXEC insertLanguageInterface 'en', 'Please select promo material from dropdown list', 'Please select promo material from dropdown list', 'crm';
EXEC insertLanguageInterface 'ru', 'Please select promo material from dropdown list', N'Пожалуйста, выберите промо-материал из выпадающего списка', 'crm';
EXEC insertLanguageInterface 'uk', 'Please select promo material from dropdown list', N'Будь ласка, виберіть промо-матеріал із випадаючого списку', 'crm';
GO

EXEC insertLanguageInterface 'en', 'Edit promo material', 'Edit promo material', 'crm';
EXEC insertLanguageInterface 'ru', 'Edit promo material', N'Редактировать промо-материал', 'crm';
EXEC insertLanguageInterface 'uk', 'Edit promo material', N'Редагувати промо-матеріал', 'crm';
GO

EXEC insertLanguageInterface 'en', 'Promo material has been added to the storage', 'Promo material has been added to the storage', 'crm';
EXEC insertLanguageInterface 'ru', 'Promo material has been added to the storage', N'Промо-материал добавлен на склад', 'crm';
EXEC insertLanguageInterface 'uk', 'Promo material has been added to the storage', N'Промо-матеріал додано на склад', 'crm';
GO

EXEC insertLanguageInterface 'en', 'Promo material has been updated successfully', 'Promo material has been updated successfully', 'crm';
EXEC insertLanguageInterface 'ru', 'Promo material has been updated successfully', N'Промо-материал успешно обновлен', 'crm';
EXEC insertLanguageInterface 'uk', 'Promo material has been updated successfully', N'Промо-матеріал успішно оновлено', 'crm';
GO

EXEC insertLanguageInterface 'en', 'Add movement', 'Add movement', 'crm';
EXEC insertLanguageInterface 'ru', 'Add movement', N'Добавить перемещение', 'crm';
EXEC insertLanguageInterface 'uk', 'Add movement', N'Додати переміщення', 'crm';
GO

EXEC insertLanguageInterface 'en', 'User direction', 'User direction', 'crm';
EXEC insertLanguageInterface 'ru', 'User direction', N'Продуктовое направление', 'crm';
EXEC insertLanguageInterface 'uk', 'User direction', N'Продуктовий напрямок', 'crm';
GO

EXEC insertLanguageInterface 'en', 'Any', 'Any', 'crm';
EXEC insertLanguageInterface 'ru', 'Any', N'Любое', 'crm';
EXEC insertLanguageInterface 'uk', 'Any', N'Будь-яке', 'crm';
GO

EXEC insertLanguageInterface 'en', 'User subdirection', 'User subdirection', 'crm';
EXEC insertLanguageInterface 'ru', 'User subdirection', N'Суб-продуктовое направление', 'crm';
EXEC insertLanguageInterface 'uk', 'User subdirection', N'Суб-продуктовий напрямок', 'crm';
GO

EXEC insertLanguageInterface 'en', 'Quantity to move', 'Quantity to move', 'crm';
EXEC insertLanguageInterface 'ru', 'Quantity to move', N'Кол-во перенести', 'crm';
EXEC insertLanguageInterface 'uk', 'Quantity to move', N'К-ть перенести', 'crm';
GO

EXEC insertLanguageInterface 'en', 'Promo materials has been moved', 'Promo materials has been moved', 'crm';
EXEC insertLanguageInterface 'ru', 'Promo materials has been moved', N'Промо-материалы перемещены', 'crm';
EXEC insertLanguageInterface 'uk', 'Promo materials has been moved', N'Промо-матеріали переміщені', 'crm';
GO

EXEC insertLanguageInterface 'en', 'Sum', 'Sum', 'crm';
EXEC insertLanguageInterface 'ru', 'Sum',N'Сумма','crm';
EXEC insertLanguageInterface 'uk', 'Sum', N'Сума', 'crm';
GO

-- Add is_archived column
IF NOT EXISTS (SELECT 1 FROM information_schema.columns WHERE table_name = 'info_sklad' AND column_name = 'is_archived')
BEGIN
ALTER TABLE info_sklad ADD is_archived INTEGER NULL
END
GO
------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------
-- https://teamsoft.atlassian.net/browse/TAKEDARU-7
----------------------------------------------------------------------------------------------------
IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'info_contactsign' AND COLUMN_NAME = 'signtype')
BEGIN
    ALTER TABLE info_contactsign ADD signtype integer
END
GO
----------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------
-- https://teamsoft.atlassian.net/browse/SALUTARIS-6
------------------------------------------------------------------------------------------------------------------------
IF OBJECT_ID(N'info_entitytypeworkflow', 'U') is NULL
begin create table info_entitytypeworkflow(
    [id] int identity(1,1) not null
    , [currenttime] datetime  null CONSTRAINT [DF_info_entitytypeworkflowcurrenttime] DEFAULT (getdate())
, [time] timestamp  null
, [moduser] varchar (16) null CONSTRAINT [DF_info_entitytypeworkflowmoduser] DEFAULT ([dbo].[Get_CurrentCode]())
, [guid] uniqueidentifier  null CONSTRAINT [DF_info_entitytypeworkflowguid] DEFAULT (newid())
, [action_type_id] int
, [chain_type] varchar(256)
, [chain_num] int
, [chain_role_id] int
, [role_id] int
, CONSTRAINT [PK_info_entitytypeworkflow] PRIMARY KEY (id) ON [PRIMARY]
    , CONSTRAINT [FK_info_entitytypeworkflow_action_type_id] FOREIGN KEY ([action_type_id]) REFERENCES info_actiontype ([id]) ON UPDATE CASCADE ON DELETE CASCADE
    , CONSTRAINT [FK_info_entitytypeworkflow_chain_role_id] FOREIGN KEY ([chain_role_id]) REFERENCES info_role ([id]) ON UPDATE CASCADE ON DELETE CASCADE
    , CONSTRAINT [FK_info_entitytypeworkflow_role_id] FOREIGN KEY ([role_id]) REFERENCES info_role ([id])
    )
end
GO

if not exists (select * from dbo.sysobjects where id = object_id('del_info_entitytypeworkflow') and OBJECTPROPERTY(id, 'IsTrigger') = 1)
exec('create trigger del_info_entitytypeworkflow on info_entitytypeworkflow for delete as if object_id(''empty.dbo.f_rep_del'') is null
insert into rep_del(tablename, del_guid) select ''info_entitytypeworkflow'', guid from deleted')
GO

if not exists (select * from dbo.sysobjects where id = object_id('moduser_info_entitytypeworkflow') and OBJECTPROPERTY(id, 'IsTrigger') = 1)
exec('create trigger moduser_info_entitytypeworkflow on info_entitytypeworkflow for insert, update as if not update(moduser)
update info_entitytypeworkflow set moduser = dbo.Get_CurrentCode(), currenttime = getdate() where id in (select id from inserted)')
GO

IF OBJECT_ID(N'info_entitytypeworkflowlog', 'U') is NULL
begin create table info_entitytypeworkflowlog(
    [id] int identity(1,1) not null
    , [currenttime] datetime  null CONSTRAINT [DF_info_entitytypeworkflowlogcurrenttime] DEFAULT (getdate())
, [time] timestamp  null
, [moduser] varchar (16) null CONSTRAINT [DF_info_entitytypeworkflowlogmoduser] DEFAULT ([dbo].[Get_CurrentCode]())
, [guid] uniqueidentifier  null CONSTRAINT [DF_info_entitytypeworkflowlogguid] DEFAULT (newid())
, [workflow_id] int
, [action_id] int
, [user_id] int
, [modifier_id] int
, [date] datetime
, [status] int
, CONSTRAINT [PK_info_entitytypeworkflowlog] PRIMARY KEY (id) ON [PRIMARY]
    , CONSTRAINT [FK_info_entitytypeworkflowlog_workflow_id] FOREIGN KEY ([workflow_id]) REFERENCES info_entitytypeworkflow ([id])
    , CONSTRAINT [FK_info_entitytypeworkflowlog_action_id] FOREIGN KEY ([action_id]) REFERENCES info_action ([id]) ON UPDATE CASCADE ON DELETE CASCADE
    , CONSTRAINT [FK_info_entitytypeworkflowlog_user_id] FOREIGN KEY ([user_id]) REFERENCES info_user ([id]) ON UPDATE CASCADE ON DELETE CASCADE
    , CONSTRAINT [FK_info_entitytypeworkflowlog_modifier_id] FOREIGN KEY ([modifier_id]) REFERENCES info_user ([id])
    )
end
GO

if not exists (select * from dbo.sysobjects where id = object_id('del_info_entitytypeworkflowlog') and OBJECTPROPERTY(id, 'IsTrigger') = 1)
exec('create trigger del_info_entitytypeworkflowlog on info_entitytypeworkflowlog for delete as if object_id(''empty.dbo.f_rep_del'') is null
insert into rep_del(tablename, del_guid) select ''info_entitytypeworkflowlog'', guid from deleted')
GO

if not exists (select * from dbo.sysobjects where id = object_id('moduser_info_entitytypeworkflowlog') and OBJECTPROPERTY(id, 'IsTrigger') = 1)
exec('create trigger moduser_info_entitytypeworkflowlog on info_entitytypeworkflowlog for insert, update as if not update(moduser)
update info_entitytypeworkflowlog set moduser = dbo.Get_CurrentCode(), currenttime = getdate() where id in (select id from inserted)')
GO

IF NOT EXISTS (SELECT 1 FROM info_serviceprivilege WHERE code = 'ROLE_ACCESS_TO_EXPENSES_APPROVEMENT') BEGIN
    INSERT INTO info_serviceprivilege (name, description, code, service_id, parent_id)
    values ('Access to expenses approvement',   --name
            null,                           --description
            'ROLE_ACCESS_TO_EXPENSES_APPROVEMENT',  --code
            (select top 1 id from info_service where identifier = 'expenses'), --service_id
            (select top 1 id from info_serviceprivilege where code = 'ROLE_ACCESS_TO_EXPENSES') --parent_id
        )
END
GO

exec insertLanguageInterface 'en', 'Actions approvement', N'Actions approvement', 'expense'
exec insertLanguageInterface 'ru', 'Actions approvement', N'Подтверждение задач', 'expense'
exec insertLanguageInterface 'uk', 'Actions approvement', N'Підтвердження задач', 'expense'
GO

exec insertLanguageInterface 'en', 'Responsible', N'Responsible', 'expense'
exec insertLanguageInterface 'ru', 'Responsible', N'Ответственный', 'expense'
exec insertLanguageInterface 'uk', 'Responsible', N'Відповідальний', 'expense'
GO

exec insertLanguageInterface 'en', 'Action type', N'Action type', 'expense'
exec insertLanguageInterface 'ru', 'Action type', N'Тип задачи', 'expense'
exec insertLanguageInterface 'uk', 'Action type', N'Тип задачи', 'expense'
GO

exec insertLanguageInterface 'en', 'Submission', N'Submission', 'expense'
exec insertLanguageInterface 'ru', 'Submission', N'Подчинение', 'expense'
exec insertLanguageInterface 'uk', 'Submission', N'Підпорядкування', 'expense'
GO

exec insertLanguageInterface 'en', 'Date from', N'Date from', 'expense'
exec insertLanguageInterface 'ru', 'Date from', N'Начало', 'expense'
exec insertLanguageInterface 'uk', 'Date from', N'Початок', 'expense'
GO

exec insertLanguageInterface 'en', 'Date till', N'Date till', 'expense'
exec insertLanguageInterface 'ru', 'Date till', N'Завершение', 'expense'
exec insertLanguageInterface 'uk', 'Date till', N'Завершення', 'expense'
GO

exec insertLanguageInterface 'en', 'Actions', N'Actions', 'expense'
exec insertLanguageInterface 'ru', 'Actions', N'Действия', 'expense'
exec insertLanguageInterface 'uk', 'Actions', N'Дії', 'expense'
GO

exec insertLanguageInterface 'en', 'Action total', N'Action total'
exec insertLanguageInterface 'ru', 'Action total', N'Сумма Итог'
exec insertLanguageInterface 'uk', 'Action total', N'Загальна сума'
GO

exec insertLanguageInterface 'en', 'Approve', N'Approve', 'expense'
exec insertLanguageInterface 'ru', 'Approve', N'Подтвердить', 'expense'
exec insertLanguageInterface 'uk', 'Approve', N'Підтвердити', 'expense'
GO

exec insertLanguageInterface 'en', 'Reject', N'Reject', 'expense'
exec insertLanguageInterface 'ru', 'Reject', N'Отклонить', 'expense'
exec insertLanguageInterface 'uk', 'Reject', N'Відхилити', 'expense'
GO

exec insertLanguageInterface 'en', 'Open', N'Open', 'expense'
exec insertLanguageInterface 'ru', 'Open', N'Открыть', 'expense'
exec insertLanguageInterface 'uk', 'Open', N'Відкрити', 'expense'
GO

exec insertPoEntityFields 8, 'total', N'Action total';
GO
------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------
-- fix translations
------------------------------------------------------------------------------------------------------------------------
exec insertLanguageInterface 'uk', 'Enabled', N'Увімкнено', 'messages'
exec insertLanguageInterface 'ru', 'Enabled', N'Включено', 'messages'
exec insertLanguageInterface 'en', 'Enabled', N'Enabled', 'messages'
GO

exec insertLanguageInterface 'uk', 'Disabled', N'Вимкнено', 'messages'
exec insertLanguageInterface 'ru', 'Disabled', N'Выключено', 'messages'
exec insertLanguageInterface 'en', 'Disabled', N'Disabled', 'messages'
GO

--add translation for key Less then two polygons in selection
EXEC deleteLanguageInterface 'Less then two polygons in selection', 'geomarketing';
exec insertLanguageInterface 'en', 'Less then two polygons in selection', 'Less then two polygons in selection', 'geomarketing';
exec insertLanguageInterface 'ru', 'Less then two polygons in selection', 'Меньше двух полигонов в выбранной територии', 'geomarketing';
exec insertLanguageInterface 'uk', 'Less then two polygons in selection', 'Менше двох полігонів на обраній туриторії', 'geomarketing';
GO

--add translation for key Reassign user polygons
EXEC deleteLanguageInterface 'Reassign user polygons', 'messages';
exec insertLanguageInterface 'en', 'Reassign user polygons', 'Reassign user polygons', 'messages';
exec insertLanguageInterface 'ru', 'Reassign user polygons', 'Переназначить територии пользователя', 'messages';
exec insertLanguageInterface 'uk', 'Reassign user polygons', 'Перепризначити території користувача', 'messages';
GO

--add translation for key Reassign polygons
EXEC deleteLanguageInterface 'Reassign polygons', 'messages';
exec insertLanguageInterface 'en', 'Reassign polygons', 'Reassign polygons', 'messages';
exec insertLanguageInterface 'ru', 'Reassign polygons', 'Переназначить територии', 'messages';
exec insertLanguageInterface 'uk', 'Reassign polygons', 'Перепризначити території ', 'messages';
GO

--add translation for key User to assign polygons
EXEC deleteLanguageInterface 'User to assign polygons', 'messages';
exec insertLanguageInterface 'en', 'User to assign polygons', 'User to assign polygons', 'messages';
exec insertLanguageInterface 'ru', 'User to assign polygons', 'Пользователь которому будут назначены територии', 'messages';
exec insertLanguageInterface 'uk', 'User to assign polygons', 'Користувач якому будуть призначені території', 'messages';
GO

--add translation for key Clear polygons
EXEC deleteLanguageInterface 'Clear polygons', 'messages';
exec insertLanguageInterface 'en', 'Clear polygons', 'Clear polygons', 'messages';
exec insertLanguageInterface 'ru', 'Clear polygons', 'Очистить територии', 'messages';
exec insertLanguageInterface 'uk', 'Clear polygons', 'Очистити території', 'messages';
GO

exec insertLanguageInterface 'en', 'Task without GPS coordinate', 'Task without GPS coordinate', 'messages';
exec insertLanguageInterface 'ru', 'Task without GPS coordinate', 'Визит без GPS координат', 'messages';
exec insertLanguageInterface 'uk', 'Task without GPS coordinate', 'Візит без GPS координат', 'messages';
GO

exec insertLanguageInterface 'en', 'Planned task without coordinate', 'Planned task without coordinate', 'messages';
exec insertLanguageInterface 'ru', 'Planned task without coordinate', 'Плановый визит без координат', 'messages';
exec insertLanguageInterface 'uk', 'Planned task without coordinate', 'Плановий візит без координат', 'messages';
GO

exec insertLanguageInterface 'uk', 'associate', 'Співробітник', 'messages';
exec insertLanguageInterface 'uk', 'Fact route(clustered)', 'Факт. маршрут(кластеризований)', 'messages';
exec insertLanguageInterface 'uk', 'Fact route(task)', 'Факт. маршрут(візит)', 'messages';
exec insertLanguageInterface 'uk', 'Fact route(tracker)', 'Факт. маршрут(трекер)', 'messages';
exec insertLanguageInterface 'uk', 'legend', 'Легенда карти', 'messages';
GO

EXEC deleteLanguageInterface 'Foreign territory', 'messages';
EXEC insertLanguageInterface 'en', 'Foreign territory', N'Foreign territory', 'messages';
EXEC insertLanguageInterface 'ru', 'Foreign territory', N'Чужая территория', 'messages';
EXEC insertLanguageInterface 'uk', 'Foreign territory', N'Чужа територія', 'messages';
GO

EXEC deleteLanguageInterface 'Reminder for next visit', 'messages';
EXEC insertLanguageInterface 'en', 'Reminder for next visit', N'Reminder for next visit', 'messages';
EXEC insertLanguageInterface 'ru', 'Reminder for next visit', N'Напоминание на след. визит', 'messages';
EXEC insertLanguageInterface 'uk', 'Reminder for next visit', N'Нагадування на слід. візит', 'messages';
GO

EXEC deleteLanguageInterface 'Campaign_rd_or', 'messages';
EXEC insertLanguageInterface 'en', 'Campaign_rd_or', N'Campaign_rd_or', 'messages';
EXEC insertLanguageInterface 'ru', 'Campaign_rd_or', N'Кампания_rd_or', 'messages';
EXEC insertLanguageInterface 'uk', 'Campaign_rd_or', N'Кампанія_rd_or', 'messages';
GO
------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------
