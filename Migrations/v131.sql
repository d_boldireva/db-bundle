------------------------------------------------------------------------------------------------------------------------
---https://teamsoft.atlassian.net/browse/EGISRU-387
------------------------------------------------------------------------------------------------------------------------
ALTER PROCEDURE [dbo].[insertLanguageInterface](@slug VARCHAR(2), @key VARCHAR(255), @translation VARCHAR(255), @domain VARCHAR(255) = 'messages')
    AS
    BEGIN
        DECLARE @language_id int;
        SELECT TOP 1 @language_id = id FROM info_language WHERE slug = @slug

		DELETE FROM info_languageinterface WHERE [key] collate SQL_Latin1_General_CP1_CS_AS = @key AND language_id = @language_id AND domain = @domain
        INSERT INTO info_languageinterface ([key], translation, language_id, domain)VALUES(@key, @translation, @language_id, @domain)

    END
GO

exec insertLanguageInterface  'en', 'Body', N'Body', 'mcm'
exec insertLanguageInterface  'ru', 'Body', N'Контент', 'mcm'
exec insertLanguageInterface  'uk', 'Body', N'Контент', 'mcm'
GO

exec insertLanguageInterface  'en', 'Not specified', N'Not specified', 'mcm'
exec insertLanguageInterface  'ru', 'Not specified', N'Не указано', 'mcm'
exec insertLanguageInterface  'uk', 'Not specified', N'Не вказано', 'mcm'
GO

exec insertLanguageInterface  'en', 'Next', N'Next', 'mcm'
exec insertLanguageInterface  'ru', 'Next', N'Далее', 'mcm'
exec insertLanguageInterface  'uk', 'Next', N'Далі', 'mcm'
GO

exec insertLanguageInterface  'en', 'Previous', N'Previous', 'mcm'
exec insertLanguageInterface  'ru', 'Previous', N'Назад', 'mcm'
exec insertLanguageInterface  'uk', 'Previous', N'Назад', 'mcm'
GO

exec insertLanguageInterface  'en', 'Distribution statistics', N'Distribution statistics', 'mcm'
exec insertLanguageInterface  'ru', 'Distribution statistics', N'Статистика по рассылке', 'mcm'
exec insertLanguageInterface  'uk', 'Distribution statistics', N'Статистика по розсилці', 'mcm'
GO

exec insertLanguageInterface  'en', 'SMS Body', N'SMS Body', 'mcm'
exec insertLanguageInterface  'ru', 'SMS Body', N'SMS контент', 'mcm'
exec insertLanguageInterface  'uk', 'SMS Body', N'SMS контент', 'mcm'
GO

exec insertLanguageInterface  'en', 'Sender Name', N'Sender Name', 'mcm'
exec insertLanguageInterface  'ru', 'Sender Name', N'Имя отправителя', 'mcm'
exec insertLanguageInterface  'uk', 'Sender Name', N'Ім''я відправника', 'mcm'
GO

exec insertLanguageInterface  'en', 'Resend with SMS', N'Resend with SMS', 'mcm'
exec insertLanguageInterface  'ru', 'Resend with SMS', N'Переотправить в SMS', 'mcm'
exec insertLanguageInterface  'uk', 'Resend with SMS', N'Переслати повторно в SMS', 'mcm'
GO

exec insertLanguageInterface  'en', 'Files', N'Files', 'mcm'
exec insertLanguageInterface  'ru', 'Files', N'Файлы', 'mcm'
exec insertLanguageInterface  'uk', 'Files', N'Файли', 'mcm'
GO

exec insertLanguageInterface  'en', 'Viber Template', N'Viber Template', 'mcm'
exec insertLanguageInterface  'ru', 'Viber Template', N'Viber Шаблон', 'mcm'
exec insertLanguageInterface  'uk', 'Viber Template', N'Viber Шаблон', 'mcm'
GO

exec insertLanguageInterface  'en', 'SMS Template', N'SMS Template', 'mcm'
exec insertLanguageInterface  'ru', 'SMS Template', N'SMS Шаблон', 'mcm'
exec insertLanguageInterface  'uk', 'SMS Template', N'SMS Шаблон', 'mcm'
GO

exec insertLanguageInterface  'en', 'Footer Language', N'Footer Language', 'mcm'
exec insertLanguageInterface  'ru', 'Footer Language', N'Язык текста отписки', 'mcm'
exec insertLanguageInterface  'uk', 'Footer Language', N'Мова тексту відписки', 'mcm'
GO

exec insertLanguageInterface  'en', 'Action Type', N'Action Type', 'mcm'
exec insertLanguageInterface  'ru', 'Action Type', N'Тип задачи', 'mcm'
exec insertLanguageInterface  'uk', 'Action Type', N'Тип завдання', 'mcm'
GO

exec insertLanguageInterface  'en', 'SMS', N'SMS', 'mcm'
exec insertLanguageInterface  'ru', 'SMS', N'SMS', 'mcm'
exec insertLanguageInterface  'uk', 'SMS', N'SMS', 'mcm'
GO

exec insertLanguageInterface  'en', 'Target Audience', N'Target Audience', 'mcm'
exec insertLanguageInterface  'ru', 'Target Audience', N'Целевая аудитория', 'mcm'
exec insertLanguageInterface  'uk', 'Target Audience', N'Цільова аудиторія', 'mcm'
GO

exec insertLanguageInterface  'en', 'Distribution date and time', N'Distribution date and time', 'mcm'
exec insertLanguageInterface  'ru', 'Distribution date and time', N'Плановая дата и время рассылки', 'mcm'
exec insertLanguageInterface  'uk', 'Distribution date and time', N'Запланована дата та час розсилки', 'mcm'
GO

exec insertLanguageInterface  'en', 'Send now', N'Send now', 'mcm'
exec insertLanguageInterface  'ru', 'Send now', N'Отправить сейчас', 'mcm'
exec insertLanguageInterface  'uk', 'Send now', N'Надіслати зараз', 'mcm'
GO

exec insertLanguageInterface  'en', 'Webinar date', N'Webinar date', 'mcm'
exec insertLanguageInterface  'ru', 'Webinar date', N'Период вебинара', 'mcm'
exec insertLanguageInterface  'uk', 'Webinar date', N'Період вебінару', 'mcm'
GO

exec insertLanguageInterface  'en', 'To', N'To', 'mcm'
exec insertLanguageInterface  'ru', 'To', N'До', 'mcm'
exec insertLanguageInterface  'uk', 'To', N'До', 'mcm'
GO

exec insertLanguageInterface  'en', 'From', N'From', 'mcm'
exec insertLanguageInterface  'ru', 'From', N'От', 'mcm'
exec insertLanguageInterface  'uk', 'From', N'Від', 'mcm'
GO

exec insertLanguageInterface  'en', 'Webinar name', N'Webinar name', 'mcm'
exec insertLanguageInterface  'ru', 'Webinar name', N'Название вебинара', 'mcm'
exec insertLanguageInterface  'uk', 'Webinar name', N'Назва вебінару', 'mcm'
GO

exec insertLanguageInterface  'en', 'Distribution is creating, please wait', N'Distribution is creating, please wait', 'mcm'
exec insertLanguageInterface  'ru', 'Distribution is creating, please wait', N'Рассылка создается, пожалуйста, подождите', 'mcm'
exec insertLanguageInterface  'uk', 'Distribution is creating, please wait', N'Розсилка створюється, будь ласка, зачекайте', 'mcm'
GO

exec insertLanguageInterface  'en', 'Webinar', N'Webinar', 'mcm'
exec insertLanguageInterface  'ru', 'Webinar', N'Вебинар', 'mcm'
exec insertLanguageInterface  'uk', 'Webinar', N'Вебінар', 'mcm'
GO

exec insertLanguageInterface  'en', 'Trigger date', N'Trigger date', 'mcm'
exec insertLanguageInterface  'ru', 'Trigger date', N'Период действия триггера', 'mcm'
exec insertLanguageInterface  'uk', 'Trigger date', N'Період дії тригера', 'mcm'
GO

exec insertLanguageInterface  'en', 'Date correction', N'Date correction', 'mcm'
exec insertLanguageInterface  'ru', 'Date correction', N'Корректировка даты', 'mcm'
exec insertLanguageInterface  'uk', 'Date correction', N'Виправлення дати', 'mcm'
GO

exec insertLanguageInterface  'en', '1 week after', N'1 week after', 'mcm'
exec insertLanguageInterface  'ru', '1 week after', N'Через неделю после', 'mcm'
exec insertLanguageInterface  'uk', '1 week after', N'Через тиждень після', 'mcm'
GO

exec insertLanguageInterface  'en', '3 days after', N'3 days after', 'mcm'
exec insertLanguageInterface  'ru', '3 days after', N'Через три дня после', 'mcm'
exec insertLanguageInterface  'uk', '3 days after', N'Через три дні після', 'mcm'
GO

exec insertLanguageInterface  'en', '1 day after', N'1 day after', 'mcm'
exec insertLanguageInterface  'ru', '1 day after', N'Через день после', 'mcm'
exec insertLanguageInterface  'uk', '1 day after', N'Через день після', 'mcm'
GO

exec insertLanguageInterface  'en', 'Not needed', N'Not needed', 'mcm'
exec insertLanguageInterface  'ru', 'Not needed', N'Не требуется', 'mcm'
exec insertLanguageInterface  'uk', 'Not needed', N'Не потрібно', 'mcm'
GO

exec insertLanguageInterface  'en', '1 day before', N'1 day before', 'mcm'
exec insertLanguageInterface  'ru', '1 day before', N'За день до', 'mcm'
exec insertLanguageInterface  'uk', '1 day before', N'За день до', 'mcm'
GO

exec insertLanguageInterface  'en', '3 days before', N'3 days before', 'mcm'
exec insertLanguageInterface  'ru', '3 days before', N'За три дня до', 'mcm'
exec insertLanguageInterface  'uk', '3 days before', N'За три дні до', 'mcm'
GO

exec insertLanguageInterface  'en', '1 week before', N'1 week before', 'mcm'
exec insertLanguageInterface  'ru', '1 week before', N'За неделю до', 'mcm'
exec insertLanguageInterface  'uk', '1 week before', N'За тиждень до', 'mcm'
GO

exec insertLanguageInterface  'en', 'Distribution', N'Distribution', 'mcm'
exec insertLanguageInterface  'ru', 'Distribution', N'Рассылки', 'mcm'
exec insertLanguageInterface  'uk', 'Distribution', N'Розсилки', 'mcm'
GO

exec insertLanguageInterface  'en', 'Trigger type', N'Trigger type', 'mcm'
exec insertLanguageInterface  'ru', 'Trigger type', N'Тип триггера', 'mcm'
exec insertLanguageInterface  'uk', 'Trigger type', N'Тип тригера', 'mcm'
GO

exec insertLanguageInterface  'en', 'Is active', N'Is active', 'mcm'
exec insertLanguageInterface  'ru', 'Is active', N'Активен', 'mcm'
exec insertLanguageInterface  'uk', 'Is active', N'Активний', 'mcm'
GO

exec insertLanguageInterface  'en', 'Trigger name', N'Trigger name', 'mcm'
exec insertLanguageInterface  'ru', 'Trigger name', N'Название триггера', 'mcm'
exec insertLanguageInterface  'uk', 'Trigger name', N'Ім''я тригера', 'mcm'
GO

exec insertLanguageInterface  'en', 'Approximate number of messages', N'Approximate number of messages', 'mcm'
exec insertLanguageInterface  'ru', 'Approximate number of messages', N'Примерное количество sms сообщений', 'mcm'
exec insertLanguageInterface  'uk', 'Approximate number of messages', N'Приблизна кількість sms повідомлень', 'mcm'
GO

exec insertLanguageInterface  'en', 'Certificate', N'Certificate', 'mcm'
exec insertLanguageInterface  'ru', 'Certificate', N'Сертификат', 'mcm'
exec insertLanguageInterface  'uk', 'Certificate', N'Сертифікат', 'mcm'
GO
------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------

------------------------------------------------------------------------------------------------------------------------
--- https://teamsoft.atlassian.net/browse/EGISRU-370
------------------------------------------------------------------------------------------------------------------------
exec insertLanguageInterface 'en', 'The file is already being formed', N'The file is already being formed', 'crm'
exec insertLanguageInterface 'ru', 'The file is already being formed', N'Файл уже формируется', 'crm'
exec insertLanguageInterface 'uk', 'The file is already being formed', N'Файл уже формується', 'crm'
GO

exec insertLanguageInterface 'en', 'Export to CSV', N'Export to CSV', 'crm'
exec insertLanguageInterface 'ru', 'Export to CSV', N'Экспорт в CSV', 'crm'
exec insertLanguageInterface 'ru', 'Export to CSV', N'Експортувати в CSV', 'crm'
GO

exec insertLanguageInterface 'en', 'Export to XLS', N'Export to XLS', 'crm'
exec insertLanguageInterface 'ru', 'Export to XLS', N'Экспорт в XLS', 'crm'
exec insertLanguageInterface 'ru', 'Export to XLS', N'Експортувати в XLS', 'crm'
GO
------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------
-- https://teamsoft.atlassian.net/browse/DRREDDYS-614
------------------------------------------------------------------------------------------------------------------------
IF NOT EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE table_name = 'info_mcmcontent' AND column_name = 'contenttype_id')
BEGIN
ALTER TABLE info_mcmcontent ADD contenttype_id int NULL
ALTER TABLE [dbo].[info_mcmcontent]
    WITH CHECK ADD CONSTRAINT [FK_info_mcmcontent_contenttype_id] FOREIGN KEY ([contenttype_id]) REFERENCES [dbo].[info_customdictionaryvalue] ([id])
END
GO

IF NOT EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE table_name = 'info_mcmcontent' AND column_name = 'contenttopic_id')
BEGIN
ALTER TABLE info_mcmcontent ADD contenttopic_id int NULL
ALTER TABLE [dbo].[info_mcmcontent]
    WITH CHECK ADD CONSTRAINT [FK_info_mcmcontent_contenttopic_id] FOREIGN KEY ([contenttopic_id]) REFERENCES [dbo].[info_customdictionaryvalue] ([id])
END
GO

IF NOT EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE table_name = 'info_mcmcontent' AND column_name = 'funnelstage_id')
BEGIN
ALTER TABLE info_mcmcontent ADD funnelstage_id int NULL
ALTER TABLE [dbo].[info_mcmcontent]
    WITH CHECK ADD CONSTRAINT [FK_info_mcmcontent_funnelstage_id] FOREIGN KEY ([funnelstage_id]) REFERENCES [dbo].[info_customdictionaryvalue] ([id])
END
GO

IF NOT EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE table_name = 'info_mcmtrigger' AND column_name = 'funnelstage_id')
BEGIN
ALTER TABLE info_mcmtrigger ADD funnelstage_id int NULL
ALTER TABLE [dbo].[info_mcmtrigger]
    WITH CHECK ADD CONSTRAINT [FK_info_mcmtrigger_funnelstage_id] FOREIGN KEY ([funnelstage_id]) REFERENCES [dbo].[info_customdictionaryvalue] ([id])
END
GO

IF NOT EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE table_name = 'info_mcmcontent' AND column_name = 'subcontenttopic_id')
BEGIN
ALTER TABLE info_mcmcontent ADD subcontenttopic_id int NULL
ALTER TABLE [dbo].[info_mcmcontent]
    WITH CHECK ADD CONSTRAINT [FK_info_mcmcontent_subcontenttopic_id] FOREIGN KEY ([subcontenttopic_id]) REFERENCES [dbo].[info_customdictionaryvalue] ([id])
END
GO

EXEC insertLanguageInterface 'en', 'Content type', 'Content type', 'mcm';
EXEC insertLanguageInterface 'ru', 'Content type', N'Тип контента', 'mcm';
EXEC insertLanguageInterface 'uk', 'Content type', N'Тип контенту', 'mcm';

EXEC insertLanguageInterface 'en', 'Content topic', 'Content topic', 'mcm';
EXEC insertLanguageInterface 'ru', 'Content topic', N'Тема контента', 'mcm';
EXEC insertLanguageInterface 'uk', 'Content topic', N'Тема контенту', 'mcm';

EXEC insertLanguageInterface 'en', 'Funnel stage', 'Funnel stage', 'mcm';
EXEC insertLanguageInterface 'ru', 'Funnel stage', N'Этап воронки', 'mcm';
EXEC insertLanguageInterface 'uk', 'Funnel stage', N'Етап воронки', 'mcm';

EXEC insertLanguageInterface 'en', 'Content subtopic', 'Content subtopic', 'mcm';
EXEC insertLanguageInterface 'ru', 'Content subtopic', N'Подтема контента', 'mcm';
EXEC insertLanguageInterface 'uk', 'Content subtopic', N'Підтема контенту', 'mcm';
GO
------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------
-- https://teamsoft.atlassian.net/browse/FARMAK-352
------------------------------------------------------------------------------------------------------------------------
EXEC insertLanguageInterface 'en', 'Select action', 'Select action', 'crm';
EXEC insertLanguageInterface 'ru', 'Select action', 'Выберите действие', 'crm';
EXEC insertLanguageInterface 'uk', 'Select action', 'Оберіть дію', 'crm';

EXEC insertLanguageInterface 'en', 'Replacement of presentation', 'Replacement of presentation', 'crm';
EXEC insertLanguageInterface 'ru', 'Replacement of presentation', 'Замена презентации', 'crm';
EXEC insertLanguageInterface 'uk', 'Replacement of presentation', 'Заміна презентації', 'crm';

EXEC insertLanguageInterface 'en', 'Target group', 'Target group', 'crm';
EXEC insertLanguageInterface 'ru', 'Target group', 'Целевая группа', 'crm';
EXEC insertLanguageInterface 'uk', 'Target group', 'Цільова група', 'crm';

EXEC insertLanguageInterface 'en', 'Replace from', 'Replace from', 'crm';
EXEC insertLanguageInterface 'ru', 'Replace from', 'Заменить с', 'crm';
EXEC insertLanguageInterface 'uk', 'Replace from', 'Замінити з', 'crm';

EXEC insertLanguageInterface 'en', 'Replace to', 'Replace to', 'crm';
EXEC insertLanguageInterface 'ru', 'Replace to', 'Заменить на', 'crm';
EXEC insertLanguageInterface 'uk', 'Replace to', 'Замінити на', 'crm';

EXEC insertLanguageInterface 'en', 'Users', 'Users', 'crm';
EXEC insertLanguageInterface 'ru', 'Users', 'Пользователи', 'crm';
EXEC insertLanguageInterface 'uk', 'Users', 'Користувачі', 'crm';

EXEC insertLanguageInterface 'en', 'Institution category', 'Institution category', 'crm';
EXEC insertLanguageInterface 'ru', 'Institution category', 'Категория учреждения', 'crm';
EXEC insertLanguageInterface 'uk', 'Institution category', 'Категорія установи', 'crm';

EXEC insertLanguageInterface 'en', 'Presentation number in order', 'Presentation number in order', 'crm';
EXEC insertLanguageInterface 'ru', 'Presentation number in order', 'Номер презентации по порядку', 'crm';
EXEC insertLanguageInterface 'uk', 'Presentation number in order', 'Номер презентації по порядку', 'crm';

EXEC insertLanguageInterface 'en', 'This value should not be equal to "Replace from"', 'This value should not be equal to «Replace from»', 'validators';
EXEC insertLanguageInterface 'ru', 'This value should not be equal to "Replace from"', 'Это значение не должно быть равно «Заменить с».', 'validators';
EXEC insertLanguageInterface 'uk', 'This value should not be equal to "Replace from"', 'Це значення не повинно дорівнювати «Замінити з»', 'validators';
GO
------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------
---https://teamsoft.atlassian.net/browse/MCM-433
------------------------------------------------------------------------------------------------------------------------
IF NOT EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE table_name = 'info_mcmdistribution' AND column_name = 'accept_filter')
    BEGIN
        ALTER TABLE info_mcmdistribution ADD accept_filter INT NULL
    END
GO

exec insertLanguageInterface 'en', 'Only those who have not received distributions for the period', 'Only those who have not received distributions for the period', 'mcm'
exec insertLanguageInterface 'ru', 'Only those who have not received distributions for the period', N'Только не получавшие рассылки за период', 'mcm'
exec insertLanguageInterface 'uk', 'Only those who have not received distributions for the period', N'Тільки ті, що не отримали розсилки за період', 'mcm'

exec insertLanguageInterface 'en', 'Only those who have received distribution for the period', 'Only those who have received distribution for the period', 'mcm'
exec insertLanguageInterface 'ru', 'Only those who have received distribution for the period', N'Только получавшие рассылки за период', 'mcm'
exec insertLanguageInterface 'uk', 'Only those who have received distribution for the period', N'Тільки ті, що  отримали розсилки за період', 'mcm'

exec insertLanguageInterface 'en', 'Only those who have read the letter / message for the period', 'Only those who have read the letter / message for the period', 'mcm'
exec insertLanguageInterface 'ru', 'Only those who have read the letter / message for the period', N'Только прочитавшие письмо/сообщение за период', 'mcm'
exec insertLanguageInterface 'uk', 'Only those who have read the letter / message for the period', N'Тільки ті, що  прочитали лист/повідомлення за період', 'mcm'

exec insertLanguageInterface 'en', 'Only those who have followed the link in the distribution for the period', 'Only those who have followed the link in the distribution for the period', 'mcm'
exec insertLanguageInterface 'ru', 'Only those who have followed the link in the distribution for the period', N'Только перешедшие по ссылке в рассылке за период', 'mcm'
exec insertLanguageInterface 'uk', 'Only those who have followed the link in the distribution for the period', N'Тільки ті, що  перейшли по посиланню в розсилці за період', 'mcm'
GO
------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------
-- https://teamsoft.atlassian.net/browse/FARMAK-351
----------------------------------------------------------------------------------------------------
EXEC insertLanguageInterface 'en', 'Edit group', 'Edit group', 'messages';
EXEC insertLanguageInterface 'ru', 'Edit group', 'Редактировать группу', 'messages';
EXEC insertLanguageInterface 'uk', 'Edit group', 'Редагувати групу', 'messages';

EXEC insertLanguageInterface 'en', 'Create group', 'Create group', 'messages';
EXEC insertLanguageInterface 'ru', 'Create group', 'Создать группу', 'messages';
EXEC insertLanguageInterface 'uk', 'Create group', 'Створити групу', 'messages';

EXEC insertLanguageInterface 'en', 'Actions', 'Actions', 'messages';
EXEC insertLanguageInterface 'ru', 'Actions', 'Действия', 'messages';
EXEC insertLanguageInterface 'uk', 'Actions', 'Дії', 'messages';

EXEC insertLanguageInterface 'en', 'Errors', 'Errors', 'messages';
EXEC insertLanguageInterface 'ru', 'Errors', 'Ошибки', 'messages';
EXEC insertLanguageInterface 'uk', 'Errors', 'Помилки', 'messages';

EXEC insertLanguageInterface 'en', 'CLM group', 'CLM group', 'messages';
EXEC insertLanguageInterface 'ru', 'CLM group', 'CLM группа', 'messages';
EXEC insertLanguageInterface 'uk', 'CLM group', 'CLM група', 'messages';

EXEC insertLanguageInterface 'en', 'Parent group', 'Parent group', 'messages';
EXEC insertLanguageInterface 'ru', 'Parent group', 'Родительская группа', 'messages';
EXEC insertLanguageInterface 'uk', 'Parent group', 'Батьківська група', 'messages';

EXEC insertLanguageInterface 'en', 'Rollback', 'Rollback', 'messages';
EXEC insertLanguageInterface 'ru', 'Rollback', 'Откатить', 'messages';
EXEC insertLanguageInterface 'uk', 'Rollback', 'Відкотити', 'messages';

EXEC insertLanguageInterface 'en', 'Presentation name', 'Presentation name', 'messages';
EXEC insertLanguageInterface 'ru', 'Presentation name', 'Название презентации', 'messages';
EXEC insertLanguageInterface 'uk', 'Presentation name', 'Назва презентації', 'messages';

EXEC insertLanguageInterface 'en', 'File name', 'File name', 'messages';
EXEC insertLanguageInterface 'ru', 'File name', 'Имя файла', 'messages';
EXEC insertLanguageInterface 'uk', 'File name', 'Назва файлу', 'messages';

EXEC insertLanguageInterface 'en', 'Create subgroup', 'Create subgroup', 'messages';
EXEC insertLanguageInterface 'ru', 'Create subgroup', 'Создать группу', 'messages';
EXEC insertLanguageInterface 'uk', 'Create subgroup', 'Створити групу', 'messages';

EXEC insertLanguageInterface 'en', 'Archive group', 'Archive group', 'messages';
EXEC insertLanguageInterface 'ru', 'Archive group', 'Архивировать группу', 'messages';
EXEC insertLanguageInterface 'uk', 'Archive group', 'Архівувати групу', 'messages';

EXEC insertLanguageInterface 'en', 'Delete group', 'Delete group', 'messages';
EXEC insertLanguageInterface 'ru', 'Delete group', 'Удалить группу', 'messages';
EXEC insertLanguageInterface 'uk', 'Delete group', 'Видалити групу', 'messages';

EXEC insertLanguageInterface 'en', 'Unarchive group', 'Unarchive group', 'messages';
EXEC insertLanguageInterface 'ru', 'Unarchive group', 'Разархивировать группу', 'messages';
EXEC insertLanguageInterface 'uk', 'Unarchive group', 'Розархівувати групу', 'messages';

EXEC insertLanguageInterface 'en', 'Plan task period is already in use', 'Plan task period is already in use', 'crm';
EXEC insertLanguageInterface 'ru', 'Plan task period is already in use', 'Период уже используется', 'crm';
EXEC insertLanguageInterface 'uk', 'Plan task period is already in use', 'Період уже використовується', 'crm';

EXEC insertLanguageInterface 'en', 'Uploaded files', 'Uploaded files', 'messages';
EXEC insertLanguageInterface 'ru', 'Uploaded files', 'Загруженные файлы', 'messages';
EXEC insertLanguageInterface 'uk', 'Uploaded files', 'Завантажені файли', 'messages';

EXEC insertLanguageInterface 'en', 'All files processed', 'All files processed', 'messages';
EXEC insertLanguageInterface 'ru', 'All files processed', 'Все файлы обработаны', 'messages';
EXEC insertLanguageInterface 'uk', 'All files processed', 'Усі файли опрацьовані', 'messages';

GO
----------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------

----------------------------------------------------------------------------------------------------
-- https://teamsoft.atlassian.net/browse/FERONRU-96
----------------------------------------------------------------------------------------------------
IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'info_clm' AND COLUMN_NAME = 'fsize')
BEGIN
ALTER TABLE info_clm ADD fsize INT NULL
END
GO

EXEC insertLanguageInterface 'en', 'File size', 'File size', 'messages';
EXEC insertLanguageInterface 'ru', 'File size', 'Размер файла', 'messages';
EXEC insertLanguageInterface 'uk', 'File size', 'Розмір файлу', 'messages';

GO
----------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------
-- https://teamsoft.atlassian.net/browse/DRREDDYSMM-113
------------------------------------------------------------------------------------------------------------------------
IF OBJECT_ID(N'[info_mcmfilterrelation]', 'U') IS NULL
BEGIN
CREATE TABLE [dbo].[info_mcmfilterrelation]
(
    [id]                  [int] IDENTITY (1,1) NOT NULL,
    [subj_id]             [int]                NULL,
    [child_id]            [int]                NULL,
    [custom_where]        [varchar](max)       NULL,
    [currenttime]         [datetime]           NULL,
    [time]                [timestamp]          NULL,
    [moduser]             [varchar](16)        NULL,
    [guid]                [uniqueidentifier]   NULL,
    CONSTRAINT [PK_info_mcmfilterrelation] PRIMARY KEY CLUSTERED(
[id] ASC
) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON,
      ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
    ) ON [PRIMARY];

ALTER TABLE [dbo].[info_mcmfilterrelation]
    WITH CHECK ADD CONSTRAINT [FK_info_mcmfilterrelation_info_mcmfilter_sudj] FOREIGN KEY ([subj_id])
    REFERENCES [dbo].[info_mcmfilter] ([id]);
ALTER TABLE [dbo].[info_mcmfilterrelation]
    WITH CHECK ADD CONSTRAINT [FK_info_mcmfilterrelation_info_mcmfilter_child] FOREIGN KEY ([child_id])
    REFERENCES [dbo].[info_mcmfilter] ([id]);
ALTER TABLE [dbo].[info_mcmfilterrelation]
    ADD CONSTRAINT [DF_info_mcmfilterrelationcurrenttime] DEFAULT (getdate()) FOR [currenttime]
ALTER TABLE [dbo].[info_mcmfilterrelation]
    ADD CONSTRAINT [DF_info_mcmfilterrelationmoduser] DEFAULT ([dbo].[Get_CurrentCode]()) FOR [moduser]
ALTER TABLE [dbo].[info_mcmfilterrelation]
    ADD CONSTRAINT [DF_info_mcmfilterrelationguid] DEFAULT (newid()) FOR [guid]
END
GO
------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------
-- https://teamsoft.atlassian.net/browse/VEKTORRU-121
------------------------------------------------------------------------------------------------------------------------
IF NOT EXISTS(SELECT 1 FROM info_options WHERE name='taskCoordinatesDateDisplayPriority')
BEGIN
insert into info_options(name, value)
values ('taskCoordinatesDateDisplayPriority', 0);
END
GO
------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------

------------------------------------------------------------------------------------------------------------------------
---https://teamsoft.atlassian.net/browse/DRREDDYSRO-28
------------------------------------------------------------------------------------------------------------------------
IF NOT EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE table_name = 'info_country' AND column_name = 'currency_id')
BEGIN
ALTER TABLE info_country ADD currency_id int NULL
ALTER TABLE [dbo].[info_country]
    WITH CHECK ADD CONSTRAINT [FK_info_country_currency_id] FOREIGN KEY ([currency_id]) REFERENCES [dbo].[info_currency] ([id])
END
GO

EXEC insertLanguageInterface 'en', 'ron', 'ron', 'messages';
EXEC insertLanguageInterface 'ru', 'ron', 'рон', 'messages';
EXEC insertLanguageInterface 'uk', 'ron', 'рон', 'messages';
GO
------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------

----------------------------------------------------------------------------------------------------
-- https://teamsoft.atlassian.net/browse/FARMAK-290
----------------------------------------------------------------------------------------------------
UPDATE [dbo].[info_filterdictionary] set page = 'survey_task' where page = 'survey';
----------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------
-- https://teamsoft.atlassian.net/browse/PHARMAWEB-556
------------------------------------------------------------------------------------------------------------------------
IF NOT EXISTS(SELECT 1 FROM info_service WHERE identifier = 'dcr')
BEGIN
    INSERT INTO info_service (identifier, name, description, enable)
    VALUES ('dcr', 'DCR suite', 'Data change request', 1)
END
GO

declare @SERVICE_ID int
set @SERVICE_ID = (SELECT TOP 1 id FROM info_service WHERE identifier = 'dcr')
IF NOT EXISTS(SELECT 1 FROM po_webservice WHERE service_id = @SERVICE_ID)
BEGIN
INSERT INTO po_webservice (identifier, enable, allow_if, route, url, name, roles, service_id)
VALUES ('dcr', 1, 'has_role("ROLE_ACCESS_TO_DCR") == true', null, null, 'DCR suite', '{"ROLE_ACCESS_TO_DCR"}:[]', @SERVICE_ID)
END
GO

declare @SERVICE_ID int, @ROLE_ID int, @SERVICE_PRIVILEGE_ID int;

set @SERVICE_ID = (SELECT id FROM info_service WHERE identifier = 'dcr')
set @ROLE_ID = (SELECT TOP 1 id FROM info_role WHERE code = 'ROLE_SUPER_ADMIN')

IF NOT EXISTS(SELECT 1 FROM info_serviceprivilege WHERE code = 'ROLE_ACCESS_TO_DCR')
    AND EXISTS(SELECT 1 FROM info_service WHERE id = @SERVICE_ID)
BEGIN
    INSERT INTO info_serviceprivilege (name, description, code, service_id)
    VALUES ('Access to DCR', null, 'ROLE_ACCESS_TO_DCR', @SERVICE_ID)

    INSERT INTO info_roleprivilege (privilege_id, role_id) values (@SERVICE_PRIVILEGE_ID, @ROLE_ID);
END
GO
------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------
-- https://teamsoft.atlassian.net/browse/EGISRU-269
------------------------------------------------------------------------------------------------------------------------
exec insertLanguageInterface 'en', 'Direction', 'Direction', 'messages';
exec insertLanguageInterface 'ru', 'Direction', N'Направление', 'messages';
exec insertLanguageInterface 'uk', 'Direction', N'Напрямок', 'messages';
GO
------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------
-- https://teamsoft.atlassian.net/browse/FERONRU-96
------------------------------------------------------------------------------------------------------------------------
IF EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE table_name = 'info_user' AND column_name = 'isnowork' AND column_default IS NULL)
ALTER TABLE info_user ADD DEFAULT 0 FOR isnowork;
GO
------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------

------------------------------------------------------------------------------------------------------------------------
--DRREDDYSRO-13
------------------------------------------------------------------------------------------------------------------------
--add column [info_saleperiod].[is_discount] [int null]
IF NOT EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'info_saleperiod' AND COLUMN_NAME = 'is_discount')
BEGIN
ALTER TABLE info_saleperiod ADD is_discount int null
END
GO

--add index [info_saleperiod].[is_discount] with name [ix_info_saleperiod_is_discount]
IF NOT EXISTS(SELECT 1 FROM sys.indexes WHERE name = 'ix_info_saleperiod_is_discount')
BEGIN
        CREATE NONCLUSTERED INDEX ix_info_saleperiod_is_discount ON info_saleperiod (is_discount)
END
GO

--add column [info_pricesaleperiod].[saleperiod_id] [int null]
IF NOT EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'info_pricesaleperiod' AND COLUMN_NAME = 'saleperiod_id')
BEGIN
ALTER TABLE info_pricesaleperiod ADD saleperiod_id int null
END
GO

--add foreign key on column [info_pricesaleperiod].[saleperiod_id]
--references [info_saleperiod].[id]
--with cascade [set null]
IF NOT EXISTS(SELECT 1 FROM sys.foreign_keys WHERE name = 'fk_info_pricesaleperiod_saleperiod_id')
BEGIN
ALTER TABLE info_pricesaleperiod WITH CHECK ADD CONSTRAINT fk_info_pricesaleperiod_saleperiod_id FOREIGN KEY (saleperiod_id) REFERENCES info_saleperiod(id) ON DELETE set null
END
GO

--add column [info_pricesaleperiod].[cip] [float null]
IF NOT EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'info_pricesaleperiod' AND COLUMN_NAME = 'cip')
BEGIN
ALTER TABLE info_pricesaleperiod ADD cip float null
END
GO

--add column [info_pricesaleperiod].[pp] [float null]
IF NOT EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'info_pricesaleperiod' AND COLUMN_NAME = 'pp')
BEGIN
ALTER TABLE info_pricesaleperiod ADD pp float null
END
GO

--add column [info_pricesaleperiod].[copay] [float null]
IF NOT EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'info_pricesaleperiod' AND COLUMN_NAME = 'copay')
BEGIN
ALTER TABLE info_pricesaleperiod ADD copay float null
END
GO

--create table [info_saleproject]
IF OBJECT_ID(N'info_saleproject') IS NULL
BEGIN
CREATE TABLE [dbo].[info_saleproject]
(
    [id]                [int] IDENTITY (1,1) NOT NULL,

    [name]              [varchar](255)       NULL,
    [datefrom]          [datetime]           NULL,
    [datetill]          [datetime]           NULL,
    [description]       [varchar](1024)      NULL,
    [state]             [int]                NULL,

    [currenttime]       [datetime]           NULL,
    [time]              [timestamp]          NULL,
    [moduser]           [varchar](16)        NULL,
    [guid]              [uniqueidentifier]   NULL,
    CONSTRAINT [PK_info_saleproject] PRIMARY KEY CLUSTERED
(
[id] ASC
) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
    ) ON [PRIMARY]

ALTER TABLE [dbo].[info_saleproject] ADD CONSTRAINT [DF_info_saleproject_currnttime] DEFAULT (getdate()) FOR [currenttime]
ALTER TABLE [dbo].[info_saleproject] ADD CONSTRAINT [DF_info_saleproject_moduser] DEFAULT ([dbo].[Get_CurrentCode]()) FOR [moduser]
ALTER TABLE [dbo].[info_saleproject] ADD CONSTRAINT [DF_info_saleproject_guid] DEFAULT (newid()) FOR [guid]
END
GO


--create table [info_saleprojectprep]
IF OBJECT_ID(N'info_saleprojectprep') IS NULL
BEGIN
CREATE TABLE [dbo].[info_saleprojectprep]
(
    [id]                    [int] IDENTITY (1,1) NOT NULL,

    [saleproject_id]        [int]                NULL,
    [prep_id]               [int]                NULL,
    [discount_threshold_1]  [int]                NULL,
    [discount_1]            [money]              NULL,
    [discount_threshold_2]  [int]                NULL,
    [discount_2]            [money]              NULL,
    [discount_threshold_3]  [int]                NULL,
    [discount_3]            [money]              NULL,

    [currenttime]           [datetime]           NULL,
    [time]                  [timestamp]          NULL,
    [moduser]               [varchar](16)        NULL,
    [guid]                  [uniqueidentifier]   NULL,
    CONSTRAINT [PK_info_saleprojectprep] PRIMARY KEY CLUSTERED
(
[id] ASC
) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
    ) ON [PRIMARY]

ALTER TABLE [dbo].[info_saleprojectprep] ADD CONSTRAINT [DF_info_saleprojectprep_currnttime] DEFAULT (getdate()) FOR [currenttime]
ALTER TABLE [dbo].[info_saleprojectprep] ADD CONSTRAINT [DF_info_saleprojectprep_moduser] DEFAULT ([dbo].[Get_CurrentCode]()) FOR [moduser]
ALTER TABLE [dbo].[info_saleprojectprep] ADD CONSTRAINT [DF_info_saleprojectprep_guid] DEFAULT (newid()) FOR [guid]
END
GO

IF NOT EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'info_saleprojectprep' AND COLUMN_NAME = 'prepdiscount_id')
BEGIN
ALTER TABLE info_saleprojectprep ADD prepdiscount_id INT NULL
END
GO

IF OBJECT_ID('fk_info_saleprojectprep_prepdiscount_id') IS NULL
BEGIN
ALTER TABLE info_saleprojectprep WITH CHECK ADD CONSTRAINT fk_info_saleprojectprep_prepdiscount_id
    FOREIGN KEY (prepdiscount_id) REFERENCES info_preparation (id);
ALTER TABLE info_saleprojectprep CHECK CONSTRAINT fk_info_saleprojectprep_prepdiscount_id
END
GO

IF NOT EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'info_saleprojectprep' AND COLUMN_NAME = 'prepdiscount_threshold')
BEGIN
ALTER TABLE info_saleprojectprep ADD prepdiscount_threshold int null
END
GO

--add foreign key on column [info_saleprojectprep].[saleproject_id]
--references [info_saleproject].[id]
--with cascade [CASCADE]
IF NOT EXISTS(SELECT 1 FROM sys.foreign_keys WHERE name = 'fk_info_saleprojectprep_saleproject_id')
BEGIN
ALTER TABLE info_saleprojectprep WITH CHECK ADD CONSTRAINT fk_info_saleprojectprep_saleproject_id FOREIGN KEY (saleproject_id) REFERENCES info_saleproject(id) ON DELETE CASCADE
END
GO

--add foreign key on column [info_saleprojectprep].[prep_id]
--references [info_preparation].[id]
--with cascade [CASCADE]
IF NOT EXISTS(SELECT 1 FROM sys.foreign_keys WHERE name = 'fk_info_saleprojectprep_prep_id')
BEGIN
ALTER TABLE info_saleprojectprep WITH CHECK ADD CONSTRAINT fk_info_saleprojectprep_prep_id FOREIGN KEY (prep_id) REFERENCES info_preparation(id) ON DELETE CASCADE
END
GO

--add service [sale_project]
--named: 'Sale Projects'
--
--with role ROLE_ACCESS_TO_SALE_PROJECTS
--named: Access to Sale Project service
IF NOT EXISTS (SELECT 1 FROM info_service WHERE identifier = 'sale_project')
BEGIN
INSERT INTO info_service (identifier, name, description, enable)
VALUES ('sale_project', 'Sale Projects', 'Manage sale projects', 1)
END
GO

IF NOT EXISTS (SELECT 1 FROM info_serviceprivilege WHERE code = 'ROLE_ACCESS_TO_SALE_PROJECT')
BEGIN
INSERT INTO info_serviceprivilege (name, code, service_id)
VALUES ('Access to Sale Project service', 'ROLE_ACCESS_TO_SALE_PROJECT', (select top 1 id from info_service where identifier = 'sale_project'))
END
GO

DECLARE @serviceprivilege_id INT = (select top 1 id from info_serviceprivilege where code = 'ROLE_ACCESS_TO_SALE_PROJECT');
DECLARE @role_id INT = (select top 1 id from info_role where code = 'ROLE_SUPER_ADMIN');

IF NOT EXISTS (SELECT 1 FROM info_roleprivilege WHERE role_id = @role_id AND privilege_id = @serviceprivilege_id)
BEGIN
INSERT INTO info_roleprivilege (privilege_id, role_id) VALUES (@serviceprivilege_id,@role_id)
END
GO

IF NOT EXISTS (SELECT 1 FROM po_webservice WHERE identifier = 'sale_project')
BEGIN
INSERT INTO po_webservice (identifier, enable, allow_if, route, url, name, roles, service_id)
VALUES ('sale_project', 1, 'is_granted("ROLE_ACCESS_TO_SALE_PROJECT") == true', 'team_soft_sale_project_homepage',
        '/{_locale}/sale-project','Sale Projects', '{"ROLE_ACCESS_TO_SALE_PROJECT"}"[]',
        (SELECT TOP 1 id FROM info_service WHERE identifier = 'sale_project'))
END
GO

exec insertLanguageInterface 'en', 'Sale Projects', 'Discount campaigns', 'messages';
exec insertLanguageInterface 'ru', 'Sale Projects', 'Скидочные Кампании', 'messages';
exec insertLanguageInterface 'uk', 'Sale Projects', 'Знижкові Кампанії', 'messages';

exec insertLanguageInterface 'en', 'Manage sale projects', 'Manage sale projects', 'messages';
exec insertLanguageInterface 'ru', 'Manage sale projects', 'Управление скидочными кампаниями', 'messages';
exec insertLanguageInterface 'uk', 'Manage sale projects', 'Керування знижковими кампаніями', 'messages';

exec insertLanguageInterface 'en', 'Access to Sale Project service', 'Access to Sale Project service', 'messages';
exec insertLanguageInterface 'ru', 'Access to Sale Project service', 'Доступ к сервису скидочных кампаний', 'messages';
exec insertLanguageInterface 'uk', 'Access to Sale Project service', 'Доступ до сервісу знижкових кампаній', 'messages';
GO

--translate domain [sale_project]
DECLARE @domain varchar(255) = 'sale_project';

--translate [Delete]
exec insertLanguageInterface 'en', 'Delete', 'Delete', @domain;
exec insertLanguageInterface 'ru', 'Delete', 'Удалить', @domain;
exec insertLanguageInterface 'uk', 'Delete', 'Видалити', @domain;

--translate [From]
exec insertLanguageInterface 'en', 'From', 'From', @domain;
exec insertLanguageInterface 'ru', 'From', 'От', @domain;
exec insertLanguageInterface 'uk', 'From', 'Від', @domain;

--translate [Till]
exec insertLanguageInterface 'en', 'Till', 'Till', @domain;
exec insertLanguageInterface 'ru', 'Till', 'До', @domain;
exec insertLanguageInterface 'uk', 'Till', 'До', @domain;

--translate [Create sale period]
exec insertLanguageInterface 'en', 'Create sale period', 'Create discount campaign', @domain;
exec insertLanguageInterface 'ru', 'Create sale period', 'Создать скидочную кампанию', @domain;
exec insertLanguageInterface 'uk', 'Create sale period', 'Створити скидочну компанію', @domain;

--translate [Edit sale period]
exec insertLanguageInterface 'en', 'Edit sale period', 'Edit discount campaign', @domain;
exec insertLanguageInterface 'ru', 'Edit sale period', 'Редактировать скидочную кампанию', @domain;
exec insertLanguageInterface 'uk', 'Edit sale period', 'Редагувати скидочну компанію', @domain;

--translate [Copy sale period]
exec insertLanguageInterface 'en', 'Copy sale period', 'Copy discount campaign', @domain;
exec insertLanguageInterface 'ru', 'Copy sale period', 'Копировать скидочную кампанию', @domain;
exec insertLanguageInterface 'uk', 'Copy sale period', 'Копіювати скидочну компанію', @domain;

--translate [Create]
exec insertLanguageInterface 'en', 'Create', 'Create', @domain;
exec insertLanguageInterface 'ru', 'Create', 'Создать', @domain;
exec insertLanguageInterface 'uk', 'Create', 'Створити', @domain;

--translate [Edit]
exec insertLanguageInterface 'en', 'Edit', 'Edit', @domain;
exec insertLanguageInterface 'ru', 'Edit', 'Редактировать', @domain;
exec insertLanguageInterface 'uk', 'Edit', 'Редагувати', @domain;

--translate [Save]
exec insertLanguageInterface 'en', 'Save', 'Save', @domain;
exec insertLanguageInterface 'ru', 'Save', 'Сохранить', @domain;
exec insertLanguageInterface 'uk', 'Save', 'Зберегти', @domain;

--translate [Description]
exec insertLanguageInterface 'en', 'Description', 'Description', @domain;
exec insertLanguageInterface 'ru', 'Description', 'Описание', @domain;
exec insertLanguageInterface 'uk', 'Description', 'Опис', @domain;

--translate [Preparations]
exec insertLanguageInterface 'en', 'Preparations', 'Preparations', @domain;
exec insertLanguageInterface 'ru', 'Preparations', 'Препарати', @domain;
exec insertLanguageInterface 'uk', 'Preparations', 'Препарати', @domain;

--translate [Preparation]
exec insertLanguageInterface 'en', 'Preparation', 'Preparation', @domain;
exec insertLanguageInterface 'ru', 'Preparation', 'Препарат', @domain;
exec insertLanguageInterface 'uk', 'Preparation', 'Препарат', @domain;

--translate [CIP]
exec insertLanguageInterface 'en', 'CIP', 'CIP', @domain;
exec insertLanguageInterface 'ru', 'CIP', 'CIP', @domain;
exec insertLanguageInterface 'uk', 'CIP', 'CIP', @domain;

--translate [PP]
exec insertLanguageInterface 'en', 'PP', 'PP', @domain;
exec insertLanguageInterface 'ru', 'PP', 'PP', @domain;
exec insertLanguageInterface 'uk', 'PP', 'PP', @domain;

--translate [Copay]
exec insertLanguageInterface 'en', 'Copay', 'Copay', @domain;
exec insertLanguageInterface 'ru', 'Copay', 'Доплата', @domain;
exec insertLanguageInterface 'uk', 'Copay', 'Доплата', @domain;

--translate [Price]
exec insertLanguageInterface 'en', 'Price', 'Price', @domain;
exec insertLanguageInterface 'ru', 'Price', 'Цена', @domain;
exec insertLanguageInterface 'uk', 'Price', 'Цена', @domain;

--translate [Carriage and insurance paid to]
exec insertLanguageInterface 'en', 'Carriage and insurance paid to', 'Carriage and insurance paid to', @domain;
exec insertLanguageInterface 'ru', 'Carriage and insurance paid to', 'Цена импорта товара для компании', @domain;
exec insertLanguageInterface 'uk', 'Carriage and insurance paid to', 'Ціна імпорту товару для компанії', @domain;

--translate [Price paid by pharmacy]
exec insertLanguageInterface 'en', 'Price paid by pharmacy', 'Price paid by pharmacy', @domain;
exec insertLanguageInterface 'ru', 'Price paid by pharmacy', 'Цена продажи в аптеку', @domain;
exec insertLanguageInterface 'uk', 'Price paid by pharmacy', 'Ціна продажу аптеці', @domain;

--translate [Pharmacy price]
exec insertLanguageInterface 'en', 'Pharmacy price', 'Pharmacy price', @domain;
exec insertLanguageInterface 'ru', 'Pharmacy price', 'Цена в аптеке', @domain;
exec insertLanguageInterface 'uk', 'Pharmacy price', 'Ціна в аптеці', @domain;

--translate [Name]
exec insertLanguageInterface 'en', 'Name', 'Name', @domain;
exec insertLanguageInterface 'ru', 'Name', 'Название', @domain;
exec insertLanguageInterface 'uk', 'Name', 'Назва', @domain;

--translate [Discount]
exec insertLanguageInterface 'en', 'Discount', 'Discount', @domain;
exec insertLanguageInterface 'ru', 'Discount', 'Скидка', @domain;
exec insertLanguageInterface 'uk', 'Discount', 'Знижка', @domain;

--translate [Product discount]
exec insertLanguageInterface 'en', 'Product discount', 'Product discount', @domain;
exec insertLanguageInterface 'ru', 'Product discount', 'Продукт-скидка', @domain;
exec insertLanguageInterface 'uk', 'Product discount', 'Продукт-знижка', @domain;

--translate [Threshold]
exec insertLanguageInterface 'en', 'Quantity', 'Quantity', @domain;
exec insertLanguageInterface 'ru', 'Quantity', 'Количество', @domain;
exec insertLanguageInterface 'uk', 'Quantity', 'Кількість', @domain;

--translate [Quantity of goods when discount is applied]
exec insertLanguageInterface 'en', 'Quantity of goods when discount is applied', 'Quantity of goods when discount is applied', @domain;
exec insertLanguageInterface 'ru', 'Quantity of goods when discount is applied', 'Количество товара для которого применяется скидка', @domain;
exec insertLanguageInterface 'uk', 'Quantity of goods when discount is applied', 'Кількість товару для якого застосовується знижка', @domain;

--translate [Amount]
exec insertLanguageInterface 'en', 'Amount', 'Amount', @domain;
exec insertLanguageInterface 'ru', 'Amount', 'Размер', @domain;
exec insertLanguageInterface 'uk', 'Amount', 'Розмір', @domain;

--translate [Discount amount]
exec insertLanguageInterface 'en', 'Discount amount', 'Discount amount', @domain;
exec insertLanguageInterface 'ru', 'Discount amount', 'Размер скидки', @domain;
exec insertLanguageInterface 'uk', 'Discount amount', 'Розмір знижки', @domain;

--translate [Select preparation]
exec insertLanguageInterface 'en', 'Select preparation', 'Select preparation', @domain;
exec insertLanguageInterface 'ru', 'Select preparation', 'Выберите препарат', @domain;
exec insertLanguageInterface 'uk', 'Select preparation', 'Виберіть препарат', @domain;

--translate [Nothing found]
exec insertLanguageInterface 'en', 'Nothing found', 'Nothing found', @domain;
exec insertLanguageInterface 'ru', 'Nothing found', 'Ничего не найдено', @domain;
exec insertLanguageInterface 'uk', 'Nothing found', 'Нічого не знайдено', @domain;

--translate [Add preparation]
exec insertLanguageInterface 'en', 'Add preparation', 'Add preparation', @domain;
exec insertLanguageInterface 'ru', 'Add preparation', 'Добавить препарат', @domain;
exec insertLanguageInterface 'uk', 'Add preparation', 'Додати препарат', @domain;

--translate [Search]
exec insertLanguageInterface 'en', 'Search', 'Search', @domain;
exec insertLanguageInterface 'ru', 'Search', 'Поиск', @domain;
exec insertLanguageInterface 'uk', 'Search', 'Пошук', @domain;

--translate [No preparations added]
exec insertLanguageInterface 'en', 'No preparations added', 'No preparations added', @domain;
exec insertLanguageInterface 'ru', 'No preparations added', 'Препараты не добавлены', @domain;
exec insertLanguageInterface 'uk', 'No preparations added', 'Препарати не додані', @domain;

--translate [Status]
exec insertLanguageInterface 'en', 'Status', 'Status', @domain;
exec insertLanguageInterface 'ru', 'Status', 'Состояние', @domain;
exec insertLanguageInterface 'uk', 'Status', 'Стан', @domain;

--translate [Status]
exec insertLanguageInterface 'en', 'Pricing', 'List of prices', @domain;
exec insertLanguageInterface 'ru', 'Pricing', 'Ценообразование', @domain;
exec insertLanguageInterface 'uk', 'Pricing', 'Ціноутворення', @domain;

exec insertLanguageInterface 'en', 'Sales', 'Discount campaigns', @domain;
exec insertLanguageInterface 'ru', 'Sales', 'Продажи', @domain;
exec insertLanguageInterface 'uk', 'Sales', 'Продажі', @domain;

exec insertLanguageInterface 'en', 'edit', 'edit', @domain;
exec insertLanguageInterface 'ru', 'edit', 'редактировать', @domain;
exec insertLanguageInterface 'uk', 'edit', 'редагувати', @domain;

exec insertLanguageInterface 'en', 'Add preparation', 'Add preparation', @domain;
exec insertLanguageInterface 'ru', 'Add preparation', 'Добавить препарат', @domain;
exec insertLanguageInterface 'uk', 'Add preparation', 'Додати препарат', @domain;

exec insertLanguageInterface 'en', 'Promo Projects', 'Marketing campaigns', 'messages';
exec insertLanguageInterface 'en', 'Create promo project', 'Create marketing campaign', 'promo-project';
exec insertLanguageInterface 'en', 'Edit promo project', 'Edit marketing campaign', 'promo-project';
GO
--end translate block

exec insertLanguageInterface 'en', 'There is another period from {{ dateFrom }} to {{ dateTill }}', N'There is another period from {{ dateFrom }} to {{ dateTill }}', 'validators'
exec insertLanguageInterface 'ru', 'There is another period from {{ dateFrom }} to {{ dateTill }}', N'Период с {{ dateTill }} по {{ dateTill }} уже существует', 'validators'
exec insertLanguageInterface 'uk', 'There is another period from {{ dateFrom }} to {{ dateTill }}', N'Період з {{ dateTill }} по {{ dateTill }} уже існує', 'validators'
GO
exec insertLanguageInterface 'en', 'There is another plan task period from {{ dt1 }} to {{ dt2 }}', N'There is another plan task period from {{ dt1 }} to {{ dt2 }}', 'validators'
exec insertLanguageInterface 'ru', 'There is another plan task period from {{ dt1 }} to {{ dt2 }}', N'Период с {{ dt1 }} по {{ dt2 }} уже существует', 'validators'
exec insertLanguageInterface 'uk', 'There is another plan task period from {{ dt1 }} to {{ dt2 }}', N'Період з {{ dt1 }} до {{ dt2 }} уже існує', 'validators'
GO
exec insertLanguageInterface 'en', 'There is another plan dt from {{ dt1 }} to {{ dt2 }}', N'There is another plan dt from {{ dt1 }} to {{ dt2 }}', 'validators'
exec insertLanguageInterface 'ru', 'There is another plan dt from {{ dt1 }} to {{ dt2 }}', N'Период с {{ dt1 }} по {{ dt2 }} уже существует', 'validators'
exec insertLanguageInterface 'uk', 'There is another plan dt from {{ dt1 }} to {{ dt2 }}', N'Період з {{ dt1 }} до {{ dt2 }} уже існує', 'validators'
GO

exec insertLanguageInterface 'en', 'datefrom must be less then datetill', 'datefrom must be less then datetill', 'validators';
exec insertLanguageInterface 'ru', 'datefrom must be less then datetill', 'Дата начала должна быть меньше даты завершения', 'validators';
exec insertLanguageInterface 'uk', 'datefrom must be less then datetill', 'Дата початку має бути менше дати завершення', 'validators';
GO
------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------
-- https://teamsoft.atlassian.net/browse/FARMAK-367
----------------------------------------------------------------------------------------------------
EXEC insertLanguageInterface 'en', 'Product subdirection', 'Product subdirection', 'crm';
EXEC insertLanguageInterface 'ru', 'Product subdirection', 'Cубпродуктовое направление', 'crm';
EXEC insertLanguageInterface 'uk', 'Product subdirection', 'Cубпродуктовий напрямок', 'crm';
GO
----------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------

------------------------------------------------------------------------------------------------------------------------
--- https://teamsoft.atlassian.net/browse/GEODATA-776
------------------------------------------------------------------------------------------------------------------------
update info_languageinterface
set translation = N'Pharmacy Chain'
where [key] = 'Pharmacy networks'
  and domain = 'geomarketing'
  and language_id = (select id FROM info_language WHERE slug = 'en')
    GO

DELETE
FROM info_languageinterface
WHERE [domain] = 'geomarketing'
  AND [key] = 'version.targeting'
    GO

    exec insertLanguageInterface 'en', 'version.targeting', 'CRM', 'geomarketing';
exec insertLanguageInterface 'ru', 'version.targeting', 'CRM', 'geomarketing';
exec insertLanguageInterface 'uk', 'version.targeting', 'CRM', 'geomarketing';
GO

update info_languageinterface
set [key] = 'regions'
where [key] = 'Regions'
  and domain = 'geomarketing'
    GO
------------------------------------------------------------------------------------------------------------------------
---https://teamsoft.atlassian.net/browse/GEODATA-759
------------------------------------------------------------------------------------------------------------------------
    IF NOT EXISTS(SELECT 1
    FROM INFORMATION_SCHEMA.COLUMNS
    WHERE table_name = 'info_etmsbalancingparam'
  AND column_name = 'contactcategory_id')
ALTER TABLE info_etmsbalancingparam
    ADD contactcategory_id int NULL
GO

IF NOT EXISTS (SELECT 1 FROM info_options WHERE name = 'geomarketingContactCategUseDirection')
    INSERT INTO info_options (name, value) VALUES ('geomarketingContactCategUseDirection', 0);
GO

update info_languageinterface set [key] = 'Cities' where [key] = 'cities' and domain = 'geomarketing'
update info_languageinterface set [key] = 'Turnover' where [key] = 'turnover' and domain = 'geomarketing'
update info_languageinterface set [key] = 'Regions' where [key] = 'regions' and domain = 'geomarketing'

    exec insertLanguageInterface 'en', 'Contact categories', N'Contact categories', 'geomarketing'
    exec insertLanguageInterface 'ru', 'Contact categories', N'Категории контактов', 'geomarketing'
    exec insertLanguageInterface 'uk', 'Contact categories', N'Категорії контактів ', 'geomarketing'
    GO
------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------

------------------------------------------------------------------------------------------------------------------------
---https://teamsoft.atlassian.net/browse/GEODATA-755
------------------------------------------------------------------------------------------------------------------------
    IF NOT EXISTS(SELECT 1
    FROM info_options
    WHERE name = 'geomarketingUndoEnable')
INSERT INTO info_options (name, value) VALUES ('geomarketingUndoEnable', '1')
    GO
------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------


------------------------------------------------------------------------------------------------------------------------
---https://teamsoft.atlassian.net/browse/GEODATA-724
------------------------------------------------------------------------------------------------------------------------
    IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'info_etmsbalancing' AND COLUMN_NAME = 'polygonlayer_id')
BEGIN
ALTER TABLE info_etmsbalancing ADD polygonlayer_id INT NULL
END
GO

IF OBJECT_ID('FK_info_etmsbalancing_polygonlayer_id') IS NULL
BEGIN
ALTER TABLE info_etmsbalancing WITH CHECK ADD CONSTRAINT FK_info_etmsbalancing_polygonlayer_id
    FOREIGN KEY (polygonlayer_id) REFERENCES info_polygonlayer (id);

ALTER TABLE info_etmsbalancing CHECK CONSTRAINT FK_info_etmsbalancing_polygonlayer_id;
END
GO

UPDATE eb
SET eb.polygonlayer_id = (select top 1 polygonlayer_id from info_polygon p where p.id = pub.polygon_id)
    FROM info_etmsbalancing eb
         left join info_polygonuserbalancing pub on pub.balancing_id = eb.id
WHERE eb.polygonlayer_id IS NULL
    GO
------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------

------------------------------------------------------------------------------------------------------------------------
---https://teamsoft.atlassian.net/browse/GEODATA-783
------------------------------------------------------------------------------------------------------------------------
    IF EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'deleteLanguageInterface') AND xtype IN (N'P'))
DROP PROCEDURE deleteLanguageInterface
    GO

CREATE PROCEDURE deleteLanguageInterface(@key VARCHAR(255), @domain VARCHAR(255) = 'messages')
    AS
BEGIN
    DELETE info_languageinterface WHERE [key] collate SQL_Latin1_General_CP1_CS_AS = @key AND domain = @domain
END
GO

exec deleteLanguageInterface 'Polygon deleted', 'geomarketing'
exec deleteLanguageInterface 'Polygon updated', 'geomarketing'
exec deleteLanguageInterface 'turnover', 'geomarketing'
exec deleteLanguageInterface 'Go to info', 'geomarketing'
exec deleteLanguageInterface 'Edit polygon', 'geomarketing'
exec deleteLanguageInterface 'Polygon updated', 'geomarketing'
exec deleteLanguageInterface 'User updated', 'geomarketing'
exec deleteLanguageInterface 'version.targeting', 'geomarketing'
exec deleteLanguageInterface 'Nothing to erase', 'geomarketing'
exec deleteLanguageInterface 'Show', 'geomarketing'

exec insertLanguageInterface 'en', 'Show', N'Show', 'geomarketing';
exec insertLanguageInterface 'ru', 'Show', N'Показать', 'geomarketing';
exec insertLanguageInterface 'uk', 'Show', N'Показати', 'geomarketing';

exec insertLanguageInterface 'en', 'Polygon deleted', N'Polygon deleted', 'geomarketing'
exec insertLanguageInterface 'ru', 'Polygon deleted', N'Полигон удален', 'geomarketing'
exec insertLanguageInterface 'uk', 'Polygon deleted', N'Полігон видалений', 'geomarketing'

exec insertLanguageInterface 'en', 'Polygon updated', N'Polygon updated', 'geomarketing'
exec insertLanguageInterface 'ru', 'Polygon updated', N'Полигон изменен', 'geomarketing'
exec insertLanguageInterface 'uk', 'Polygon updated', N'Полігон змінений', 'geomarketing'

exec insertLanguageInterface 'en', 'turnover', N'Turnover', 'geomarketing'
exec insertLanguageInterface 'ru', 'turnover', N'Товарооборот', 'geomarketing'
exec insertLanguageInterface 'uk', 'turnover', N'Товарообіг', 'geomarketing'

exec insertLanguageInterface 'en', 'Go to info', N'Go to info', 'geomarketing'
exec insertLanguageInterface 'ru', 'Go to info', N'Инфрмация', 'geomarketing'
exec insertLanguageInterface 'uk', 'Go to info', N'Інформація', 'geomarketing'

exec insertLanguageInterface 'en', 'Edit polygon', N'Edit polygon', 'geomarketing'
exec insertLanguageInterface 'ru', 'Edit polygon', N'Редактировать полигон', 'geomarketing'
exec insertLanguageInterface 'uk', 'Edit polygon', N'Редагувати полігон', 'geomarketing'

exec insertLanguageInterface 'en', 'User updated', N'User updated', 'geomarketing'
exec insertLanguageInterface 'ru', 'User updated', N'Пользователь изменен', 'geomarketing'
exec insertLanguageInterface 'uk', 'User updated', N'Користувача змінено', 'geomarketing'

exec insertLanguageInterface 'en', 'version.targeting', N'CRM', 'geomarketing'
exec insertLanguageInterface 'ru', 'version.targeting', N'CRM', 'geomarketing'
exec insertLanguageInterface 'uk', 'version.targeting', N'CRM', 'geomarketing'

exec insertLanguageInterface 'en', 'The minimum value cannot exceed the maximum', N'The minimum value cannot exceed the maximum', 'geomarketing'
exec insertLanguageInterface 'ru', 'The minimum value cannot exceed the maximum', N'Минимальное значение не может превышать максимальное', 'geomarketing'
exec insertLanguageInterface 'uk', 'The minimum value cannot exceed the maximum', N'Мінімальне значення не може перевищувати максимальне', 'geomarketing'

exec insertLanguageInterface 'en', 'The maximum value cannot be less than the minimum', N'The maximum value cannot be less than the minimum', 'geomarketing'
exec insertLanguageInterface 'ru', 'The maximum value cannot be less than the minimum', N'Максимальное значение не может быть меньше минимального.', 'geomarketing'
exec insertLanguageInterface 'uk', 'The maximum value cannot be less than the minimum', N'Максимальне значення не може бути менше мінімального', 'geomarketing'

exec insertLanguageInterface 'en', 'Less then two pins within selected polygons', N'Less then two pins within selected polygons', 'geomarketing'
exec insertLanguageInterface 'ru', 'Less then two pins within selected polygons', N'Менее двух пинов в выбранных полигонах', 'geomarketing'
exec insertLanguageInterface 'uk', 'Less then two pins within selected polygons', N'Менше двох пінів в обраних полігонах', 'geomarketing'

exec insertLanguageInterface 'en', 'Nothing to erase', N'Nothing to erase', 'geomarketing'
exec insertLanguageInterface 'ru', 'Nothing to erase', N'Нечего стирать', 'geomarketing'
exec insertLanguageInterface 'uk', 'Nothing to erase', N'Нічого стирати', 'geomarketing'

exec insertLanguageInterface 'en', 'Polygon created', N'Polygon created', 'geomarketing'
exec insertLanguageInterface 'ru', 'Polygon created', N'Полигон создано', 'geomarketing'
exec insertLanguageInterface 'uk', 'Polygon created', N'Полігон створено', 'geomarketing'

GO
------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------

------------------------------------------------------------------------------------------------------------------------
---https://teamsoft.atlassian.net/browse/PHARMAWEB-431 --fix-translation
------------------------------------------------------------------------------------------------------------------------
exec deleteLanguageInterface 'Hide survey from menu CRM', 'messages'
exec deleteLanguageInterface 'Hide verification from menu CRM', 'messages'
GO

exec insertLanguageInterface 'en', 'Hide survey from menu CRM', N'Hide survey from menu CRM', 'messages'
exec insertLanguageInterface 'ru', 'Hide survey from menu CRM', N'Скрыть анкеты из меню CRM', 'messages'
exec insertLanguageInterface 'uk', 'Hide survey from menu CRM', N'Приховати анкети з меню CRM', 'messages'
GO

exec insertLanguageInterface 'en', 'Hide verification from menu CRM', N'Hide verification from menu CRM', 'messages'
exec insertLanguageInterface 'ru', 'Hide verification from menu CRM', N'Скрыть верификацию из меню CRM', 'messages'
exec insertLanguageInterface 'uk', 'Hide verification from menu CRM', N'Приховати верифікацію з меню CRM', 'messages'
GO
------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------

----------------------------------------------------------------------------------------------------
-- https://teamsoft.atlassian.net/browse/FARMAK-352
----------------------------------------------------------------------------------------------------
EXEC insertLanguageInterface 'en', 'User positions', 'User positions', 'crm';
EXEC insertLanguageInterface 'ru', 'User positions', 'Должности пользователей', 'crm';
EXEC insertLanguageInterface 'uk', 'User positions', 'Посади користувачів', 'crm';
GO
----------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------

----------------------------------------------------------------------------------------------------
-- https://teamsoft.atlassian.net/browse/FARMAK-290
----------------------------------------------------------------------------------------------------
IF NOT EXISTS(SELECT 1 FROM info_filterdictionary WHERE name = 'Action type' and page='survey_action')
    INSERT INTO info_filterdictionary (name, sql, empty_sql, sqlite, empty_sqlite, page, dictionary_id)
    VALUES (
        'Action type',
        ':actiontype_id in (select id from info_actiontype where guid #SIGN# #VALUE#)',
        ':actiontype_id #SIGN#',
        '#actiontype_id# in (select _id from info_actiontype where lower(guid) #SIGN# lower(#VALUE#))',
        '#actiontype_id# #SIGN#',
        'survey_action',
        (select top 1 id from po_dictionary where tablename='info_actiontype')
    );
GO
IF NOT EXISTS(SELECT 1 FROM info_filterdictionary WHERE name = 'Responsible' and page='survey_action')
    INSERT INTO info_filterdictionary (name, sql, empty_sql, sqlite, empty_sqlite, page, dictionary_id)
    VALUES (
        'Responsible',
        ':responsible_id in (select id from info_user where guid #SIGN# #VALUE#)',
        ':responsible_id #SIGN#',
        '#responsible_id# in (select _id from info_user where lower(guid) #SIGN# lower(#VALUE#))',
        '#responsible_id# #SIGN#',
        'survey_action',
        (select top 1 id from po_dictionary where tablename='info_user')
    );
GO
----------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------

----------------------------------------------------------------------------------------------------
-- https://teamsoft.atlassian.net/browse/PHARMAWEB-562
----------------------------------------------------------------------------------------------------
DELETE FROM info_options WHERE name='geomarketingCacheAlive';

EXEC deleteLanguageInterface 'Less then two polygons in selection', 'geomarketing'

--add translation for key Less then two polygons in selection
EXEC insertLanguageInterface 'en', 'Less then two polygons in selection', 'Less then two polygons in selection', 'geomarketing'
EXEC insertLanguageInterface 'ru', 'Less then two polygons in selection', N'Меньше двух полигонов в выбранной территории', 'geomarketing'
EXEC insertLanguageInterface 'uk', 'Less then two polygons in selection', N'Менше двох полігонів на обраній території', 'geomarketing'


EXEC deleteLanguageInterface 'No pins within selected polygons', 'geomarketing'

--add translation for key No pins within selected polygons
EXEC insertLanguageInterface 'en', 'No pins within selected polygons', 'No pins in selected polygons', 'geomarketing'
EXEC insertLanguageInterface 'ru', 'No pins within selected polygons', N'Нет пинов внутри выбраных полигонов', 'geomarketing'
EXEC insertLanguageInterface 'uk', 'No pins within selected polygons', N'Немає пінів у вибраних полігонах', 'geomarketing'
GO
----------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------
