if not exists(SELECT 1
              FROM INFORMATION_SCHEMA.COLUMNS
              WHERE TABLE_NAME = 'po_entityfields'
                AND column_Name = 'group')
    begin
        alter table po_entityfields
            add [group] varchar(255) null
    end
go

if exists(SELECT 1
          FROM INFORMATION_SCHEMA.COLUMNS
          WHERE TABLE_NAME = 'po_entityfields'
            AND column_Name = 'group')
    begin
        if exists(select 1 from po_entityfields where page = 8 and field = 'photo')
            begin
                update po_entityfields
                set [group] = 'reports',
                    name    = 'Photo computations'
                where page = 8
                  and field = 'photo'
            end
        else
            begin
                insert po_entityfields (page, [group], field, name)
                values (8, 'reports', 'photo', 'Photo computations')
            end
    end
go

if exists(SELECT 1
          FROM INFORMATION_SCHEMA.COLUMNS
          WHERE TABLE_NAME = 'po_entityfields'
            AND column_Name = 'group')
    begin
        if exists(select 1 from po_entityfields where page = 8 and field = 'brand_id')
            begin
                update po_entityfields set [group] = 'reports', name = 'Brands' where page = 8 and field = 'brand_id'
            end
        else
            begin
                insert po_entityfields (page, [group], field, name)
                values (8, 'reports', 'brand_id', 'Brands')
            end
    end
go

if exists(SELECT 1
          FROM INFORMATION_SCHEMA.COLUMNS
          WHERE TABLE_NAME = 'po_entityfields'
            AND column_Name = 'group')
    begin
        if exists(select 1 from po_entityfields where page = 8 and field = 'brends_id')
            begin
                update po_entityfields
                set [group] = 'reports',
                    name    = 'Promo materials'
                where page = 8
                  and field = 'brends_id'
            end
        else
            begin
                insert po_entityfields (page, [group], field, name)
                values (8, 'reports', 'brends_id', 'Promo materials')
            end
    end
go

if exists(SELECT 1
          FROM INFORMATION_SCHEMA.COLUMNS
          WHERE TABLE_NAME = 'po_entityfields'
            AND column_Name = 'group')
    begin
        if exists(select 1 from po_entityfields where page = 8 and field = 'info_user.medresp')
            begin
                update po_entityfields
                set [group] = 'reports',
                    name    = 'Medical representative'
                where page = 8
                  and field = 'info_user.medresp'
            end
        else
            begin
                insert po_entityfields (page, [group], field, name)
                values (8, 'reports', 'info_user.medresp', 'Medical representative')
            end
    end
go

if exists(SELECT 1
          FROM INFORMATION_SCHEMA.COLUMNS
          WHERE TABLE_NAME = 'po_entityfields'
            AND column_Name = 'group')
    begin
        if exists(select 1 from po_entityfields where page = 8 and field = 'current_target_dv')
            begin
                update po_entityfields
                set [group] = 'reports',
                    name    = 'Current DV session target'
                where page = 8
                  and field = 'current_target_dv'
            end
        else
            begin
                insert po_entityfields (page, [group], field, name)
                values (8, 'reports', 'current_target_dv', 'Current DV session target')
            end
    end
go

if exists(SELECT 1
          FROM INFORMATION_SCHEMA.COLUMNS
          WHERE TABLE_NAME = 'po_entityfields'
            AND column_Name = 'group')
    begin
        if exists(select 1 from po_entityfields where page = 8 and field = 'next_target_dv')
            begin
                update po_entityfields
                set [group] = 'reports',
                    name    = 'Next DV session target'
                where page = 8
                  and field = 'next_target_dv'
            end
        else
            begin
                insert po_entityfields (page, [group], field, name)
                values (8, 'reports', 'next_target_dv', 'Next DV session target')
            end
    end
go

if exists(SELECT 1
          FROM INFORMATION_SCHEMA.COLUMNS
          WHERE TABLE_NAME = 'po_entityfields'
            AND column_Name = 'group')
    begin
        if exists(select 1 from po_entityfields where page = 8 and field = 'datetime_dv')
            begin
                update po_entityfields
                set [group] = 'reports',
                    name    = 'Interim control date'
                where page = 8
                  and field = 'datetime_dv'
            end
        else
            begin
                insert po_entityfields (page, [group], field, name)
                values (8, 'reports', 'datetime_dv', 'Interim control date')
            end
    end
go

if exists(SELECT 1
          FROM INFORMATION_SCHEMA.COLUMNS
          WHERE TABLE_NAME = 'po_entityfields'
            AND column_Name = 'group')
    begin
        if exists(select 1 from po_entityfields where page = 8 and field = 'anketa_dv')
            begin
                update po_entityfields
                set [group] = 'reports',
                    name    = 'DV Questionary'
                where page = 8
                  and field = 'anketa_dv'
            end
        else
            begin
                insert po_entityfields (page, [group], field, name)
                values (8, 'reports', 'anketa_dv', 'DV Questionary')
            end
    end
go

if exists(SELECT 1
          FROM INFORMATION_SCHEMA.COLUMNS
          WHERE TABLE_NAME = 'po_entityfields'
            AND column_Name = 'group')
    begin
        if exists(select 1 from po_entityfields where page = 4 and field = 'info_taskpreparation')
            begin
                update po_entityfields
                set [group] = 'reports',
                    name    = 'Brands'
                where page = 4
                  and field = 'info_taskpreparation'
            end
        else
            begin
                insert po_entityfields (page, [group], field, name)
                values (4, 'reports', 'info_taskpreparation', 'Brands')
            end
    end
go

if exists(SELECT 1
          FROM INFORMATION_SCHEMA.COLUMNS
          WHERE TABLE_NAME = 'po_entityfields'
            AND column_Name = 'group')
    begin
        if exists(select 1 from po_entityfields where page = 4 and field = 'info_tasksubject')
            begin
                update po_entityfields
                set [group] = 'reports',
                    name    = 'Task subject'
                where page = 4
                  and field = 'info_tasksubject'
            end
        else
            begin
                insert po_entityfields (page, [group], field, name)
                values (4, 'reports', 'info_tasksubject', 'Task subject')
            end
    end
go

if exists(SELECT 1
          FROM INFORMATION_SCHEMA.COLUMNS
          WHERE TABLE_NAME = 'po_entityfields'
            AND column_Name = 'group')
    begin
        if exists(select 1 from po_entityfields where page = 4 and field = 'info_tasksubj')
            begin
                update po_entityfields
                set [group] = 'reports',
                    name    = 'Task subject by brand'
                where page = 4
                  and field = 'info_tasksubj'
            end
        else
            begin
                insert po_entityfields (page, [group], field, name)
                values (4, 'reports', 'info_tasksubj', 'Task subject by brand')
            end
    end
go

if exists(SELECT 1
          FROM INFORMATION_SCHEMA.COLUMNS
          WHERE TABLE_NAME = 'po_entityfields'
            AND column_Name = 'group')
    begin
        if exists(select 1 from po_entityfields where page = 4 and field = 'info_taskmaterial')
            begin
                update po_entityfields
                set [group] = 'reports',
                    name    = 'Promo materials'
                where page = 4
                  and field = 'info_taskmaterial'
            end
        else
            begin
                insert po_entityfields (page, [group], field, name)
                values (4, 'reports', 'info_taskmaterial', 'Promo materials')
            end
    end
go

if exists(SELECT 1
          FROM INFORMATION_SCHEMA.COLUMNS
          WHERE TABLE_NAME = 'po_entityfields'
            AND column_Name = 'group')
    begin
        if exists(select 1 from po_entityfields where page = 4 and field = 'info_companypreparation2')
            begin
                update po_entityfields
                set [group] = 'reports',
                    name    = 'Control'
                where page = 4
                  and field = 'info_companypreparation2'
            end
        else
            begin
                insert po_entityfields (page, [group], field, name)
                values (4, 'reports', 'info_companypreparation2', 'Control')
            end
    end
go

if exists(SELECT 1
          FROM INFORMATION_SCHEMA.COLUMNS
          WHERE TABLE_NAME = 'po_entityfields'
            AND column_Name = 'group')
    begin
        if exists(select 1 from po_entityfields where page = 4 and field = 'info_contactpotential')
            begin
                update po_entityfields
                set [group] = 'reports',
                    name    = 'Agreement'
                where page = 4
                  and field = 'info_contactpotential'
            end
        else
            begin
                insert po_entityfields (page, [group], field, name)
                values (4, 'reports', 'info_contactpotential', 'Agreement')
            end
    end
go

if exists(SELECT 1
          FROM INFORMATION_SCHEMA.COLUMNS
          WHERE TABLE_NAME = 'po_entityfields'
            AND column_Name = 'group')
    begin
        if exists(select 1 from po_entityfields where page = 4 and field = 'info_taskpresentation')
            begin
                update po_entityfields
                set [group] = 'reports',
                    name    = 'CLM'
                where page = 4
                  and field = 'info_taskpresentation'
            end
        else
            begin
                insert po_entityfields (page, [group], field, name)
                values (4, 'reports', 'info_taskpresentation', 'CLM')
            end
    end
go

if exists(SELECT 1
          FROM INFORMATION_SCHEMA.COLUMNS
          WHERE TABLE_NAME = 'po_entityfields'
            AND column_Name = 'group')
    begin
        if exists(select 1 from po_entityfields where page = 4 and field = 'info_taskparam')
            begin
                update po_entityfields
                set [group] = 'reports',
                    name    = 'Double visit questionary'
                where page = 4
                  and field = 'info_taskparam'
            end
        else
            begin
                insert po_entityfields (page, [group], field, name)
                values (4, 'reports', 'info_taskparam', 'Double visit questionary')
            end
    end
go

if exists(SELECT 1
          FROM INFORMATION_SCHEMA.COLUMNS
          WHERE TABLE_NAME = 'po_entityfields'
            AND column_Name = 'group')
    begin
        if exists(select 1 from po_entityfields where page = 4 and field = 'info_survey')
            begin
                update po_entityfields
                set [group] = 'reports',
                    name    = 'Surveying of doctor'
                where page = 4
                  and field = 'info_survey'
            end
        else
            begin
                insert po_entityfields (page, [group], field, name)
                values (4, 'reports', 'info_survey', 'Surveying of doctor')
            end
    end
go

if exists(SELECT 1
          FROM INFORMATION_SCHEMA.COLUMNS
          WHERE TABLE_NAME = 'po_entityfields'
            AND column_Name = 'group')
    begin
        if exists(select 1 from po_entityfields where page = 4 and field = 'previousnexttaskpurpose')
            begin
                update po_entityfields
                set [group] = 'reports',
                    name    = 'Task purpose'
                where page = 4
                  and field = 'previousnexttaskpurpose'
            end
        else
            begin
                insert po_entityfields (page, [group], field, name)
                values (4, 'reports', 'previousnexttaskpurpose', 'Task purpose')
            end
    end
go

if exists(SELECT 1
          FROM INFORMATION_SCHEMA.COLUMNS
          WHERE TABLE_NAME = 'po_entityfields'
            AND column_Name = 'group')
    begin
        if exists(select 1 from po_entityfields where page = 4 and field = 'nextaskpurpose')
            begin
                update po_entityfields
                set [group] = 'reports',
                    name    = 'Next task purpose'
                where page = 4
                  and field = 'nextaskpurpose'
            end
        else
            begin
                insert po_entityfields (page, [group], field, name)
                values (4, 'reports', 'nextaskpurpose', 'Next task purpose')
            end
    end
go

if exists(SELECT 1
          FROM INFORMATION_SCHEMA.COLUMNS
          WHERE TABLE_NAME = 'po_entityfields'
            AND column_Name = 'group')
    begin
        if exists(select 1 from po_entityfields where page = 4 and field = 'taskreport')
            begin
                update po_entityfields
                set [group] = 'reports',
                    name    = 'Task report'
                where page = 4
                  and field = 'taskreport'
            end
        else
            begin
                insert po_entityfields (page, [group], field, name)
                values (4, 'reports', 'taskreport', 'Task report')
            end
    end
go

if exists(SELECT 1
          FROM INFORMATION_SCHEMA.COLUMNS
          WHERE TABLE_NAME = 'po_entityfields'
            AND column_Name = 'group')
    begin
        if exists(select 1 from po_entityfields where page = 4 and field = 'previoustaskreport')
            begin
                update po_entityfields
                set [group] = 'reports',
                    name    = 'Previous task report'
                where page = 4
                  and field = 'previoustaskreport'
            end
        else
            begin
                insert po_entityfields (page, [group], field, name)
                values (4, 'reports', 'previoustaskreport', 'Previous task report')
            end
    end
go

if exists(SELECT 1
          FROM INFORMATION_SCHEMA.COLUMNS
          WHERE TABLE_NAME = 'po_entityfields'
            AND column_Name = 'group')
    begin
        if exists(select 1 from po_entityfields where page = 4 and field = 'department_id')
            begin
                update po_entityfields
                set [group] = 'reports',
                    name    = 'Department'
                where page = 4
                  and field = 'department_id'
            end
        else
            begin
                insert po_entityfields (page, [group], field, name)
                values (4, 'reports', 'department_id', 'Department')
            end
    end
go

if exists(SELECT 1
          FROM INFORMATION_SCHEMA.COLUMNS
          WHERE TABLE_NAME = 'po_entityfields'
            AND column_Name = 'group')
    begin
        if exists(select 1 from po_entityfields where page = 4 and field = 'info_markerpharmacysale')
            begin
                update po_entityfields
                set [group] = 'reports',
                    name    = 'Sales in marker pharmacies'
                where page = 4
                  and field = 'info_markerpharmacysale'
            end
        else
            begin
                insert po_entityfields (page, [group], field, name)
                values (4, 'reports', 'info_markerpharmacysale', 'Sales in marker pharmacies')
            end
    end
go

if exists(SELECT 1
          FROM INFORMATION_SCHEMA.COLUMNS
          WHERE TABLE_NAME = 'po_entityfields'
            AND column_Name = 'group')
    begin
        if exists(select 1 from po_entityfields where page = 4 and field = 'info_taskcompanydistributorpreparation')
            begin
                update po_entityfields
                set [group] = 'reports',
                    name    = 'Monitoring of prices'
                where page = 4
                  and field = 'info_taskcompanydistributorpreparation'
            end
        else
            begin
                insert po_entityfields (page, [group], field, name)
                values (4, 'reports', 'info_taskcompanydistributorpreparation', 'Monitoring of prices')
            end
    end
go

if exists(SELECT 1
          FROM INFORMATION_SCHEMA.COLUMNS
          WHERE TABLE_NAME = 'po_entityfields'
            AND column_Name = 'group')
    begin
        if exists(select 1 from po_entityfields where page = 4 and field = 'info_companyarrangementconfirmation')
            begin
                update po_entityfields
                set [group] = 'reports',
                    name    = 'Agreement control to AC'
                where page = 4
                  and field = 'info_companyarrangementconfirmation'
            end
        else
            begin
                insert po_entityfields (page, [group], field, name)
                values (4, 'reports', 'info_companyarrangementconfirmation', 'Agreement control to AC')
            end
    end
go

if exists(SELECT 1
          FROM INFORMATION_SCHEMA.COLUMNS
          WHERE TABLE_NAME = 'po_entityfields'
            AND column_Name = 'group')
    begin
        if exists(select 1 from po_entityfields where page = 4 and field = 'info_taskfile')
            begin
                update po_entityfields
                set [group] = 'reports',
                    name    = 'Photo computations'
                where page = 4
                  and field = 'info_taskfile'
            end
        else
            begin
                insert po_entityfields (page, [group], field, name)
                values (4, 'reports', 'info_taskfile', 'Photo computations')
            end
    end
go

if exists(SELECT 1
          FROM INFORMATION_SCHEMA.COLUMNS
          WHERE TABLE_NAME = 'po_entityfields'
            AND column_Name = 'group')
    begin
        if exists(select 1 from po_entityfields where page = 4 and field = 'info_clinicalcase')
            begin
                update po_entityfields
                set [group] = 'reports',
                    name    = 'Clinical situations'
                where page = 4
                  and field = 'info_clinicalcase'
            end
        else
            begin
                insert po_entityfields (page, [group], field, name)
                values (4, 'reports', 'info_clinicalcase', 'Clinical situations')
            end
    end
go

if exists(SELECT 1
          FROM INFORMATION_SCHEMA.COLUMNS
          WHERE TABLE_NAME = 'po_entityfields'
            AND column_Name = 'group')
    begin
        if exists(select 1 from po_entityfields where page = 4 and field = 'field_training')
            begin
                update po_entityfields
                set [group] = 'reports',
                    name    = 'Training'
                where page = 4
                  and field = 'field_training'
            end
        else
            begin
                insert po_entityfields (page, [group], field, name)
                values (4, 'reports', 'field_training', 'Training')
            end
    end
go

if exists(SELECT 1
          FROM INFORMATION_SCHEMA.COLUMNS
          WHERE TABLE_NAME = 'po_entityfields'
            AND column_Name = 'group')
    begin
        if exists(select 1 from po_entityfields where page = 4 and field = 'field_training_coaches')
            begin
                update po_entityfields
                set [group] = 'reports',
                    name    = 'Training coaches'
                where page = 4
                  and field = 'field_training_coaches'
            end
        else
            begin
                insert po_entityfields (page, [group], field, name)
                values (4, 'reports', 'field_training_coaches', 'Training coaches')
            end
    end
go

if exists(SELECT 1
          FROM INFORMATION_SCHEMA.COLUMNS
          WHERE TABLE_NAME = 'po_entityfields'
            AND column_Name = 'group')
    begin
        if exists(select 1 from po_entityfields where page = 4 and field = 'field_training_evaluation_form')
            begin
                update po_entityfields
                set [group] = 'reports',
                    name    = 'Evaluation form'
                where page = 4
                  and field = 'field_training_evaluation_form'
            end
        else
            begin
                insert po_entityfields (page, [group], field, name)
                values (4, 'reports', 'field_training_evaluation_form', 'Evaluation form')
            end
    end
go

if exists(SELECT 1
          FROM INFORMATION_SCHEMA.COLUMNS
          WHERE TABLE_NAME = 'po_entityfields'
            AND column_Name = 'group')
    begin
        if exists(select 1 from po_entityfields where page = 4 and field = 'field_training_date_start')
            begin
                update po_entityfields
                set [group] = 'reports',
                    name    = 'Test start date and time'
                where page = 4
                  and field = 'field_training_date_start'
            end
        else
            begin
                insert po_entityfields (page, [group], field, name)
                values (4, 'reports', 'field_training_date_start', 'Test start date and time')
            end
    end
go

if exists(SELECT 1
          FROM INFORMATION_SCHEMA.COLUMNS
          WHERE TABLE_NAME = 'po_entityfields'
            AND column_Name = 'group')
    begin
        if exists(select 1 from po_entityfields where page = 4 and field = 'field_training_date_finish')
            begin
                update po_entityfields
                set [group] = 'reports',
                    name    = 'Date and time of the end of the test'
                where page = 4
                  and field = 'field_training_date_finish'
            end
        else
            begin
                insert po_entityfields (page, [group], field, name)
                values (4, 'reports', 'field_training_date_finish', 'Date and time of the end of the test')
            end
    end
go

if exists(SELECT 1
          FROM INFORMATION_SCHEMA.COLUMNS
          WHERE TABLE_NAME = 'po_entityfields'
            AND column_Name = 'group')
    begin
        if exists(select 1 from po_entityfields where page = 4 and field = 'field_presentation')
            begin
                update po_entityfields
                set [group] = 'reports',
                    name    = 'Training Presentation'
                where page = 4
                  and field = 'field_presentation'
            end
        else
            begin
                insert po_entityfields (page, [group], field, name)
                values (4, 'reports', 'field_presentation', 'Training Presentation')
            end
    end
go

if exists(SELECT 1
          FROM INFORMATION_SCHEMA.COLUMNS
          WHERE TABLE_NAME = 'po_entityfields'
            AND column_Name = 'group')
    begin
        if exists(select 1 from po_entityfields where page = 4 and field = 'pervostolniki')
            begin
                update po_entityfields
                set [group] = 'reports',
                    name    = 'Firstmen'
                where page = 4
                  and field = 'pervostolniki'
            end
        else
            begin
                insert po_entityfields (page, [group], field, name)
                values (4, 'reports', 'pervostolniki', 'Firstmen')
            end
    end
go

if exists(SELECT 1
          FROM INFORMATION_SCHEMA.COLUMNS
          WHERE TABLE_NAME = 'po_entityfields'
            AND column_Name = 'group')
    begin
        if exists(select 1 from po_entityfields where page = 4 and field = 'promocontrol')
            begin
                update po_entityfields
                set [group] = 'reports',
                    name    = 'Planogram'
                where page = 4
                  and field = 'promocontrol'
            end
        else
            begin
                insert po_entityfields (page, [group], field, name)
                values (4, 'reports', 'promocontrol', 'Planogram')
            end
    end
go

if exists(SELECT 1
          FROM INFORMATION_SCHEMA.COLUMNS
          WHERE TABLE_NAME = 'po_entityfields'
            AND column_Name = 'group')
    begin
        if exists(select 1 from po_entityfields where page = 4 and field = 'projects')
            begin
                update po_entityfields
                set [group] = 'reports',
                    name    = 'Projects'
                where page = 4
                  and field = 'projects'
            end
        else
            begin
                insert po_entityfields (page, [group], field, name)
                values (4, 'reports', 'projects', 'Projects')
            end
    end
go

if exists(SELECT 1
          FROM INFORMATION_SCHEMA.COLUMNS
          WHERE TABLE_NAME = 'po_entityfields'
            AND column_Name = 'group')
    begin
        if exists(select 1 from po_entityfields where page = 4 and field = 'action')
            begin
                update po_entityfields
                set [group] = 'reports',
                    name    = 'Task action'
                where page = 4
                  and field = 'action'
            end
        else
            begin
                insert po_entityfields (page, [group], field, name)
                values (4, 'reports', 'action', 'Task action')
            end
    end
go

if exists(SELECT 1
          FROM INFORMATION_SCHEMA.COLUMNS
          WHERE TABLE_NAME = 'po_entityfields'
            AND column_Name = 'group')
    begin
        if exists(select 1 from po_entityfields where page = 4 and field = 'tenderplan')
            begin
                update po_entityfields
                set [group] = 'reports',
                    name    = 'Tender list'
                where page = 4
                  and field = 'tenderplan'
            end
        else
            begin
                insert po_entityfields (page, [group], field, name)
                values (4, 'reports', 'tenderplan', 'Tender list')
            end
    end
go

if exists(SELECT 1
          FROM INFORMATION_SCHEMA.COLUMNS
          WHERE TABLE_NAME = 'po_entityfields'
            AND column_Name = 'group')
    begin
        if exists(select 1 from po_entityfields where page = 4 and field = 'tenderfact')
            begin
                update po_entityfields
                set [group] = 'reports',
                    name    = 'KAM agreement'
                where page = 4
                  and field = 'tenderfact'
            end
        else
            begin
                insert po_entityfields (page, [group], field, name)
                values (4, 'reports', 'tenderfact', 'KAM agreement')
            end
    end
go

if exists(SELECT 1
          FROM INFORMATION_SCHEMA.COLUMNS
          WHERE TABLE_NAME = 'po_entityfields'
            AND column_Name = 'group')
    begin
        if exists(select 1 from po_entityfields where page = 4 and field = 'planfactkam')
            begin
                update po_entityfields
                set [group] = 'reports',
                    name    = 'KAM sales plan-fact'
                where page = 4
                  and field = 'planfactkam'
            end
        else
            begin
                insert po_entityfields (page, [group], field, name)
                values (4, 'reports', 'planfactkam', 'KAM sales plan-fact')
            end
    end
go

if exists(SELECT 1
          FROM INFORMATION_SCHEMA.COLUMNS
          WHERE TABLE_NAME = 'po_entityfields'
            AND column_Name = 'group')
    begin
        if exists(select 1 from po_entityfields where page = 4 and field = 'company_goals')
            begin
                update po_entityfields
                set [group] = 'reports',
                    name    = 'Company goals'
                where page = 4
                  and field = 'company_goals'
            end
        else
            begin
                insert po_entityfields (page, [group], field, name)
                values (4, 'reports', 'company_goals', 'Company goals')
            end
    end
go

if exists(SELECT 1
          FROM INFORMATION_SCHEMA.COLUMNS
          WHERE TABLE_NAME = 'po_entityfields'
            AND column_Name = 'group')
    begin
        if exists(select 1 from po_entityfields where page = 4 and field = 'marketingproject')
            begin
                update po_entityfields
                set [group] = 'reports',
                    name    = 'Marketing projects'
                where page = 4
                  and field = 'marketingproject'
            end
        else
            begin
                insert po_entityfields (page, [group], field, name)
                values (4, 'reports', 'marketingproject', 'Marketing projects')
            end
    end
go

if exists(SELECT 1
          FROM INFORMATION_SCHEMA.COLUMNS
          WHERE TABLE_NAME = 'po_entityfields'
            AND column_Name = 'group')
    begin
        if exists(select 1 from po_entityfields where page = 4 and field = 'visitGoals')
            begin
                update po_entityfields
                set [group] = 'reports',
                    name    = 'DV goals'
                where page = 4
                  and field = 'visitGoals'
            end
        else
            begin
                insert po_entityfields (page, [group], field, name)
                values (4, 'reports', 'visitGoals', 'DV goals')
            end
    end
go

if exists(SELECT 1
          FROM INFORMATION_SCHEMA.COLUMNS
          WHERE TABLE_NAME = 'po_entityfields'
            AND column_Name = 'group')
    begin
        if exists(select 1 from po_entityfields where page = 4 and field = 'debt_overdue')
            begin
                update po_entityfields
                set [group] = 'reports',
                    name    = 'Debt overdue'
                where page = 4
                  and field = 'debt_overdue'
            end
        else
            begin
                insert po_entityfields (page, [group], field, name)
                values (4, 'reports', 'debt_overdue', 'Debt overdue')
            end
    end
go

if exists(SELECT 1
          FROM INFORMATION_SCHEMA.COLUMNS
          WHERE TABLE_NAME = 'po_entityfields'
            AND column_Name = 'group')
    begin
        if exists(select 1 from po_entityfields where page = 4 and field = 'debt_request_status')
            begin
                update po_entityfields
                set [group] = 'reports',
                    name    = 'Application status'
                where page = 4
                  and field = 'debt_request_status'
            end
        else
            begin
                insert po_entityfields (page, [group], field, name)
                values (4, 'reports', 'debt_request_status', 'Application status')
            end
    end
go

if exists(SELECT 1
          FROM INFORMATION_SCHEMA.COLUMNS
          WHERE TABLE_NAME = 'po_entityfields'
            AND column_Name = 'group')
    begin
        if exists(select 1 from po_entityfields where page = 4 and field = 'debt_current')
            begin
                update po_entityfields
                set [group] = 'reports',
                    name    = 'Current overdue'
                where page = 4
                  and field = 'debt_current'
            end
        else
            begin
                insert po_entityfields (page, [group], field, name)
                values (4, 'reports', 'debt_current', 'Current overdue')
            end
    end
go

if exists(SELECT 1
          FROM INFORMATION_SCHEMA.COLUMNS
          WHERE TABLE_NAME = 'po_entityfields'
            AND column_Name = 'group')
    begin
        if exists(select 1 from po_entityfields where page = 4 and field = 'debt_place_order_in')
            begin
                update po_entityfields
                set [group] = 'reports',
                    name    = 'Place an order in'
                where page = 4
                  and field = 'debt_place_order_in'
            end
        else
            begin
                insert po_entityfields (page, [group], field, name)
                values (4, 'reports', 'debt_place_order_in', 'Place an order in')
            end
    end
go

if exists(SELECT 1
          FROM INFORMATION_SCHEMA.COLUMNS
          WHERE TABLE_NAME = 'po_entityfields'
            AND column_Name = 'group')
    begin
        if exists(select 1 from po_entityfields where page = 4 and field = 'debt_discount')
            begin
                update po_entityfields
                set [group] = 'reports',
                    name    = 'Percent of discount by default'
                where page = 4
                  and field = 'debt_discount'
            end
        else
            begin
                insert po_entityfields (page, [group], field, name)
                values (4, 'reports', 'debt_discount', 'Percent of discount by default')
            end
    end
go

if exists(SELECT 1
          FROM INFORMATION_SCHEMA.COLUMNS
          WHERE TABLE_NAME = 'po_entityfields'
            AND column_Name = 'group')
    begin
        if exists(select 1 from po_entityfields where page = 4 and field = 'info_contactintask')
            begin
                update po_entityfields
                set [group] = 'reports',
                    name    = 'Participants'
                where page = 4
                  and field = 'info_contactintask'
            end
        else
            begin
                insert po_entityfields (page, [group], field, name)
                values (4, 'reports', 'info_contactintask', 'Participants')
            end
    end
go

IF OBJECT_ID(N'[info_optionsbyrole]', 'U') IS NULL
    BEGIN
        CREATE TABLE [dbo].[info_optionsbyrole]
        (
            [id]          [int] IDENTITY (1,1) NOT NULL,
            [option_id]   int,
            [role_id]     int,
            [value]       varchar(255),
            [currenttime] [datetime]           NULL,
            [time]        [timestamp]          NULL,
            [moduser]     [varchar](16)        NULL,
            [guid]        [uniqueidentifier]   NULL,
            CONSTRAINT [PK_info_optionsbyrole] PRIMARY KEY CLUSTERED
                (
                 [id] ASC
                    ) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
        ) ON [PRIMARY]

        ALTER TABLE [dbo].[info_optionsbyrole]
            ADD CONSTRAINT [DF_info_optionsbyrolecurrenttime] DEFAULT (getdate()) FOR [currenttime]
        ALTER TABLE [dbo].[info_optionsbyrole]
            ADD CONSTRAINT [DF_info_optionsbyrolemoduser] DEFAULT ([dbo].[Get_CurrentCode]()) FOR [moduser]
        ALTER TABLE [dbo].[info_optionsbyrole]
            ADD CONSTRAINT [DF_info_optionsbyroleguid] DEFAULT (newid()) FOR [guid]

        ALTER TABLE [dbo].[info_optionsbyrole]
            WITH CHECK ADD CONSTRAINT [FK_info_optionsbyrole_option_id] FOREIGN KEY ([option_id])
                REFERENCES [dbo].[info_options] ([id])

        ALTER TABLE [dbo].[info_optionsbyrole]
            WITH CHECK ADD CONSTRAINT [FK_info_optionsbyrole_role_id] FOREIGN KEY ([role_id])
                REFERENCES [dbo].[info_role] ([id])
    END
GO

if exists(SELECT 1
          FROM INFORMATION_SCHEMA.COLUMNS
          WHERE TABLE_NAME = 'po_dictionary'
            AND Column_Name = 'systemdictionary_id')
    begin
        update po_dictionary
        set systemdictionary_id = (select top 1 id
                                   from info_systemdictionary
                                   where tablename = po_dictionary.tablename
                                     and isnull(identifier, 0) = isnull(po_dictionary.identifier, 0))
    end
GO

-- BOXVESION-147
-- Add your translations simplier
-- exec insertLanguageInterface 'ru', 'Privileges', N'Привилегии'
-- -- https://docs.microsoft.com/en-us/sql/relational-databases/system-compatibility-views/sys-sysobjects-transact-sql?view=sql-server-ver15
IF EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'insertLanguageInterface') AND xtype IN (N'P'))
    DROP PROCEDURE insertLanguageInterface
GO

CREATE PROCEDURE insertLanguageInterface(@slug VARCHAR(2), @key VARCHAR(255), @translation VARCHAR(255))
AS
BEGIN
    DECLARE @language_id int;
    SELECT TOP 1 @language_id = id FROM info_language WHERE slug = @slug

    INSERT INTO info_languageinterface ([key], translation, language_id, domain)
    SELECT TOP 1 @key, @translation, @language_id, 'messages'
    WHERE NOT EXISTS(SELECT 1 FROM info_languageinterface WHERE [key] = @key AND language_id = @language_id)
END

go

exec insertLanguageInterface 'ru', 'Privileges', N'Привилегии'
go
exec insertLanguageInterface 'uk', 'Privileges', N'Привілеї'
go

exec insertLanguageInterface 'ru', 'Access to admin toolkit', N'Доступ к панели администратора'
go
exec insertLanguageInterface 'uk', 'Access to admin toolkit', N'Доступ до панелі адміністратора'
go

go
exec insertLanguageInterface 'ru', 'Access to CRM', N'Доступ к CRM'
go
exec insertLanguageInterface 'uk', 'Access to CRM', N'Доступ до CRM'
go

exec insertLanguageInterface 'ru', 'Access to reports', N'Доступ к отчетам'
go
exec insertLanguageInterface 'uk', 'Access to reports', N'Доступ до звітів'
go

exec insertLanguageInterface 'ru', 'Access to reports designer', N'Доступ к дизайнеру отчетов'
go
exec insertLanguageInterface 'uk', 'Access to reports designer', N'Доступ до дизайнера звітів'
go

exec insertLanguageInterface 'ru', 'Access to CRM Verification', N'Доступ к CRM верификации'
go
exec insertLanguageInterface 'uk', 'Access to CRM Verification', N'Доступ до CRM верифікації'
go

exec insertLanguageInterface 'ru', 'Adding a company', N'Создание компании'
go
exec insertLanguageInterface 'uk', 'Adding a company', N'Створення компанії'
go

exec insertLanguageInterface 'ru', 'Adding a contact', N'Создание контакта'
go
exec insertLanguageInterface 'uk', 'Adding a contact', N'Створення контакту'
go

exec insertLanguageInterface 'ru', 'Adding a task', N'Создание визита'
go
exec insertLanguageInterface 'uk', 'Adding a task', N'Створення візиту'
go

exec insertLanguageInterface 'ru', 'Adding an action', N'Создание задачи'
go
exec insertLanguageInterface 'uk', 'Adding an action', N'Створення задачі'
go

exec insertLanguageInterface 'ru', 'Editing a company', N'Редактирование компании'
go
exec insertLanguageInterface 'uk', 'Editing a company', N'Редагування компанії'
go

exec insertLanguageInterface 'ru', 'Editing a contact', N'Редактирование контакта'
go
exec insertLanguageInterface 'uk', 'Editing a contact', N'Редагування контакту'
go

exec insertLanguageInterface 'ru', 'Editing a task', N'Редактирование визита'
go
exec insertLanguageInterface 'uk', 'Editing a task', N'Редагування візиту'
go

exec insertLanguageInterface 'ru', 'Editing an action', N'Редактирование задачи'
go
exec insertLanguageInterface 'uk', 'Editing an action', N'Редагування задачі'
go

exec insertLanguageInterface 'ru', 'Editing an owned company', N'Редактирование принадлежащей компании'
go
exec insertLanguageInterface 'uk', 'Editing an owned company', N'Редагування власної компанії'
go

exec insertLanguageInterface 'ru', 'Editing an owned contact', N'Редактирование принадлежащего контакта'
go
exec insertLanguageInterface 'uk', 'Editing an owned contact', N'Редагування власного контакту'
go

exec insertLanguageInterface 'ru', 'Editing an owned task', N'Редактирование принадлежащего визита'
go
exec insertLanguageInterface 'uk', 'Editing an owned task', N'Редагування власного візиту'
go

exec insertLanguageInterface 'ru', 'Editing an owned action', N'Редактирование принадлежащей задачи'
go
exec insertLanguageInterface 'uk', 'Editing an owned action', N'Редагування власної задачі'
go
------------------------------------------------------------------------------------------------------------------
--https://teamsoft.atlassian.net/browse/EGISRU-5
IF NOT EXISTS (SELECT 1 FROM information_schema.columns WHERE table_name = 'info_user' AND column_name = 'ad_identifier')
    BEGIN
        ALTER TABLE info_user ADD ad_identifier varchar(255)  null
    END
GO

IF NOT EXISTS (SELECT 1 FROM information_schema.columns WHERE table_name = 'info_user' AND column_name = 'ad_city')
    BEGIN
        ALTER TABLE info_user ADD ad_city varchar(255)  null
    END
GO

IF OBJECT_ID(N'[info_activedirectorydictionary]','U') IS NULL
  begin
    CREATE TABLE [dbo].[info_activedirectorydictionary](
      [id] [int] IDENTITY(1,1) NOT NULL,
      [direction_value] [varchar](255) NULL,
      [department_value] [varchar](255) NULL,
      [direction_id] [int] NULL,
      [currenttime] [datetime] NULL,
      [time] [timestamp] NULL,
      [moduser] [varchar](16) NULL,
      [guid] [uniqueidentifier] NULL,
      CONSTRAINT [PK_info_activedirectorydictionary] PRIMARY KEY CLUSTERED
        (
          [id] ASC
        )WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
    ) ON [PRIMARY]

    ALTER TABLE [dbo].[info_activedirectorydictionary] ADD  CONSTRAINT [DF_info_activedirectorydictionary_currenttime]  DEFAULT (getdate()) FOR [currenttime]
    ALTER TABLE [dbo].[info_activedirectorydictionary] ADD  CONSTRAINT [DF_info_activedirectorydictionary_moduser]  DEFAULT ([dbo].[Get_CurrentCode]()) FOR [moduser]
    ALTER TABLE [dbo].[info_activedirectorydictionary] ADD  CONSTRAINT [DF_info_activedirectorydictionary_guid]  DEFAULT (newid()) FOR [guid]
    ALTER TABLE [dbo].[info_activedirectorydictionary]  WITH CHECK ADD CONSTRAINT [FK_info_activedirectorydictionary_direction_id]
    FOREIGN KEY([direction_id]) REFERENCES [dbo].[info_direction] ([id])
  end
GO

exec insertLanguageInterface 'en', 'Active Directory user', 'Active Directory user';
exec insertLanguageInterface 'ru', 'Active Directory user', 'Пользователь Active Directory';
exec insertLanguageInterface 'uk', 'Active Directory user', 'Користувач Active Directory';

exec insertLanguageInterface 'uk', 'Active Directory account', 'Active Directory account';
exec insertLanguageInterface 'ru', 'Active Directory account', 'Учетная запись Active Directory';
exec insertLanguageInterface 'uk', 'Active Directory account', 'Обліковий запис Active Directory';

exec insertLanguageInterface 'en', 'Active Directory city', 'Active Directory city';
exec insertLanguageInterface 'ru', 'Active Directory city', 'Город Active Directory';
exec insertLanguageInterface 'uk', 'Active Directory city', 'Місто Active Directory';

exec insertLanguageInterface 'en', 'This Active Directory user has already been added.', 'This Active Directory user has already been added.';
exec insertLanguageInterface 'ru', 'This Active Directory user has already been added.', 'Этот пользователь AD уже добавлен.';
exec insertLanguageInterface 'uk', 'This Active Directory user has already been added.', 'Даний користувач AD уже доданий.';

GO
-------------------------------------------------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------------------------------------------------------------------------

----------------------------------------------------------------------------------------------------------------------------------------------------------------------
--115
--https://teamsoft.atlassian.net/browse/PHARMAWEB-254
----------------------------------------------------------------------------------------------------------------------------------------------------------------------

IF NOT EXISTS(SELECT 1 FROM information_schema.columns WHERE table_name = 'info_role' AND column_name = 'parent_id')
    BEGIN
        ALTER TABLE info_role ADD parent_id int
        ALTER TABLE [dbo].[info_role] WITH CHECK ADD CONSTRAINT [FK_info_role_parent_id] FOREIGN KEY ([parent_id]) REFERENCES [dbo].[info_role] ([id])
    END
GO

update info_role set parent_id = (select top 1 id from info_role where code = 'ROLE_ADMIN');
update info_role set parent_id = (select top 1 id from info_role where code = 'ROLE_SUPER_ADMIN') where code = 'ROLE_ADMIN';
update info_role set parent_id = null where code = 'ROLE_SUPER_ADMIN';

GO
-------------------------------------------------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------------------------------------------------------------------------

-- EGISRU-43

IF OBJECT_ID(N'po_gridconfig', 'U') is NULL
    BEGIN
        CREATE TABLE [dbo].[po_gridconfig]
        (
            [id] [int] IDENTITY(1,1) NOT NULL,
            page [int] NULL,
            subpage [int] NULL,
            field_code [varchar](255) NULL,
            [order] [int] NULL,
            is_shown [int] NULL,
            [currenttime] [datetime] NULL,
            [time] [timestamp] NULL,
            [moduser] [varchar](16) NULL,
            [guid] [uniqueidentifier] NULL,
            CONSTRAINT [PK_po_gridconfig] PRIMARY KEY CLUSTERED ([id] ASC)
            WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
            ON [PRIMARY]
        ) ON [PRIMARY]

        ALTER TABLE [dbo].[po_gridconfig] ADD  CONSTRAINT [DF_po_gridconfigcurrenttime] DEFAULT (getdate()) FOR [currenttime]
        ALTER TABLE [dbo].[po_gridconfig] ADD  CONSTRAINT [DF_po_gridconfigmoduser] DEFAULT ([dbo].[Get_CurrentCode]()) FOR [moduser]
        ALTER TABLE [dbo].[po_gridconfig] ADD  CONSTRAINT [DF_po_gridconfigguid] DEFAULT (newid()) FOR [guid]

        insert into po_gridconfig (page, subpage, field_code, [order], is_shown)
        values
            (100, 1, 'statusRasch', 1, 1),
            (100, 1, 'statusUtv', 2, 1),
            (100, 1, 'isChanged', 3, 1),
            (100, 1, 'status', 4, 1),
            (100, 1, 'name', 5, 1),
            (100, 1, 'cityName', 6, 1),
            (100, 1, 'address', 7, 1),
            (100, 1, 'categoryName', 8, 1),
            (100, 1, 'approvedCategory', 9, 1),
            (100, 1, 'segment', 10, 1),
            (100, 1, 'datesList', 11, 1),
            (100, 1, 'others', 12, 0),
            (100, 1, 'tovarooborot', 13, 1),
            (100, 1, 'salePerTovarooborot', 14, 1),
            (100, 1, 'segmentEng', 15, 1),
            (100, 1, 'plancnt', 16, 1),
            (100, 1, 'tasks', 17, 1),
            (100, 1, 'sales', 18, 0),
            (100, 1, 'net', 19, 0),
            (100, 1, 'arrangement', 20, 0),
            (100, 1, 'critery1', 21, 0),
            (100, 1, 'critery2', 22, 0),
            (100, 1, 'critery3', 23, 0),
            (100, 1, 'removalReason', 24, 0),
            (100, 1, 'comment', 25, 0),
            (100, 1, 'rejectionReason', 26, 0),
            (100, 2, 'statusRasch', 1, 1),
            (100, 2, 'statusUtv', 2, 1),
            (100, 2, 'isChanged', 3, 1),
            (100, 2, 'status', 4, 1),
            (100, 2, 'name', 5, 1),
            (100, 2, 'targetName', 6, 1),
            (100, 2, 'companyName', 7, 1),
            (100, 2, 'categoryName', 8, 1),
            (100, 2, 'approvedCategory', 9, 1),
            (100, 2, 'datesList', 10, 1),
            (100, 2, 'segment', 11, 1),
            (100, 2, 'others', 12, 0),
            (100, 2, 'patientcount', 13, 1),
            (100, 2, 'potential', 14, 1),
            (100, 2, 'factPotential', 15, 1),
            (100, 2, 'segmentEng', 16, 1),
            (100, 2, 'plancnt', 17, 1),
            (100, 2, 'tasks', 18, 1),
            (100, 2, 'removalCategory', 19, 0),
            (100, 2, 'comment', 20, 0),
            (100, 2, 'rejectionReason', 21, 0),
            (100, 2, 'nsrRate', 22, 0),
            (100, 2, 'potentialRate', 23, 0);
    END
GO

IF OBJECT_ID(N'[info_plancontactrule]','U') IS NULL
    BEGIN
        CREATE TABLE [dbo].[info_plancontactrule](
             [id] [int] IDENTITY(1,1) NOT NULL,
             target_id [int] NULL,
             category_id [int] NULL,
             contact_cnt [int] NULL,
             visit_cnt [int] NULL,
             datefrom [datetime] NULL,
             datetill [datetime] NULL,
             brand_id [int] NULL,
             [currenttime] [datetime] NULL,
             [time] [timestamp] NULL,
             [moduser] [varchar](16) NULL,
             [guid] [uniqueidentifier] NULL,
             CONSTRAINT [PK_info_plancontactrule] PRIMARY KEY CLUSTERED ([id] ASC)
             WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
             ON [PRIMARY]
        )

        ALTER TABLE [dbo].[info_plancontactrule]
        WITH CHECK ADD CONSTRAINT [FK_info_plancontactrule_info_target]
        FOREIGN KEY([target_id]) REFERENCES [dbo].[info_target] ([id])

        ALTER TABLE [dbo].[info_plancontactrule]
        WITH CHECK ADD CONSTRAINT [FK_info_plancontactrule_info_contactcateg]
        FOREIGN KEY([category_id]) REFERENCES [dbo].[info_contactcateg] ([id])

        ALTER TABLE [dbo].[info_plancontactrule] ADD  CONSTRAINT [DF_info_plancontactrulecurrenttime] DEFAULT (getdate()) FOR [currenttime]
        ALTER TABLE [dbo].[info_plancontactrule] ADD  CONSTRAINT [DF_info_plancontactrulemoduser] DEFAULT ([dbo].[Get_CurrentCode]()) FOR [moduser]
        ALTER TABLE [dbo].[info_plancontactrule] ADD  CONSTRAINT [DF_info_plancontactruleguid] DEFAULT (newid()) FOR [guid]
    END
GO

IF OBJECT_ID(N'[info_targetingcompanycritery]','U') IS NULL
    BEGIN
        CREATE TABLE [dbo].[info_targetingcompanycritery](
             [id] [int] IDENTITY(1,1) NOT NULL,
             company_id [int] NULL,
             direction_id [int] NULL,
             critery1 [varchar](255),
             critery2 [varchar](255),
             critery3 [varchar](255),
             [currenttime] [datetime] NULL,
             [time] [timestamp] NULL,
             [moduser] [varchar](16) NULL,
             [guid] [uniqueidentifier] NULL,
             CONSTRAINT [PK_info_targetingcompanycritery] PRIMARY KEY CLUSTERED ([id] ASC)
             WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
             ON [PRIMARY]
        )

        ALTER TABLE [dbo].[info_targetingcompanycritery]
        WITH CHECK ADD CONSTRAINT [FK_info_targetingcompanycritery_info_company]
        FOREIGN KEY([company_id]) REFERENCES [dbo].[info_company] ([id])

        ALTER TABLE [dbo].[info_targetingcompanycritery]
        WITH CHECK ADD CONSTRAINT [FK_info_targetingcompanycritery_info_direction]
        FOREIGN KEY([direction_id]) REFERENCES [dbo].[info_direction] ([id])

        ALTER TABLE [dbo].[info_targetingcompanycritery] ADD  CONSTRAINT [DF_info_targetingcompanycriterycurrenttime] DEFAULT (getdate()) FOR [currenttime]
        ALTER TABLE [dbo].[info_targetingcompanycritery] ADD  CONSTRAINT [DF_info_targetingcompanycriterymoduser] DEFAULT ([dbo].[Get_CurrentCode]()) FOR [moduser]
        ALTER TABLE [dbo].[info_targetingcompanycritery] ADD  CONSTRAINT [DF_info_targetingcompanycriteryguid] DEFAULT (newid()) FOR [guid]
    END
GO

IF OBJECT_ID(N'[info_targetingcontactrate]','U') IS NULL
    BEGIN
        CREATE TABLE [dbo].[info_targetingcontactrate](
             [id] [int] IDENTITY(1,1) NOT NULL,
             contact_id [int] NULL,
             direction_id [int] NULL,
             nsr_rate [varchar](255),
             potential_rate [varchar](255),
             [currenttime] [datetime] NULL,
             [time] [timestamp] NULL,
             [moduser] [varchar](16) NULL,
             [guid] [uniqueidentifier] NULL,
             CONSTRAINT [PK_info_targetingcontactrate] PRIMARY KEY CLUSTERED ([id] ASC)
             WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
             ON [PRIMARY]
        )

        ALTER TABLE [dbo].[info_targetingcontactrate]
        WITH CHECK ADD CONSTRAINT [FK_info_targetingcontactrate_info_contact]
        FOREIGN KEY([contact_id]) REFERENCES [dbo].[info_contact] ([id])

        ALTER TABLE [dbo].[info_targetingcontactrate]
        WITH CHECK ADD CONSTRAINT [FK_info_targetingcontactrate_info_direction]
        FOREIGN KEY([direction_id]) REFERENCES [dbo].[info_direction] ([id])

        ALTER TABLE [dbo].[info_targetingcontactrate] ADD  CONSTRAINT [DF_info_targetingcontactratecurrenttime] DEFAULT (getdate()) FOR [currenttime]
        ALTER TABLE [dbo].[info_targetingcontactrate] ADD  CONSTRAINT [DF_info_targetingcontactratemoduser] DEFAULT ([dbo].[Get_CurrentCode]()) FOR [moduser]
        ALTER TABLE [dbo].[info_targetingcontactrate] ADD  CONSTRAINT [DF_info_targetingcontactrateguid] DEFAULT (newid()) FOR [guid]
    END
GO

DECLARE @en_id int, @ru_id int, @uk_id int;
SELECT TOP 1 @en_id = id from info_language where slug = 'en';
SELECT TOP 1 @ru_id = id from info_language where slug = 'ru';
SELECT TOP 1 @uk_id = id from info_language where slug = 'uk';
--
IF NOT EXISTS (SELECT 1
FROM info_languageinterface
WHERE [key] = 'Responsible'
AND language_id = @en_id
AND domain = 'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain)
VALUES ('Responsible', 'Responsible', @en_id, 'messages');
--
IF NOT EXISTS(SELECT 1
              FROM info_languageinterface
              WHERE [key] = 'Critery'
                AND language_id = @ru_id
                AND domain = 'targeting')
    INSERT INTO info_languageinterface ([key], translation, language_id, domain)
    VALUES ('Critery', 'Критерий', @ru_id, 'targeting');
IF NOT EXISTS(SELECT 1
              FROM info_languageinterface
              WHERE [key] = 'Critery'
                AND language_id = @uk_id
                AND domain = 'targeting')
    INSERT INTO info_languageinterface ([key], translation, language_id, domain)
    VALUES ('Critery', 'Критерій', @uk_id, 'targeting');
IF NOT EXISTS(SELECT 1
              FROM info_languageinterface
              WHERE [key] = 'Arrangement'
                AND language_id = @ru_id
                AND domain = 'targeting')
    INSERT INTO info_languageinterface ([key], translation, language_id, domain)
    VALUES ('Arrangement', 'Контракт', @ru_id, 'targeting');
IF NOT EXISTS(SELECT 1
              FROM info_languageinterface
              WHERE [key] = 'Arrangement'
                AND language_id = @uk_id
                AND domain = 'targeting')
    INSERT INTO info_languageinterface ([key], translation, language_id, domain)
    VALUES ('Arrangement', 'Контракт', @uk_id, 'targeting');
IF NOT EXISTS(SELECT 1
              FROM info_languageinterface
              WHERE [key] = 'Network'
                AND language_id = @ru_id
                AND domain = 'targeting')
    INSERT INTO info_languageinterface ([key], translation, language_id, domain)
    VALUES ('Network', 'Сеть', @ru_id, 'targeting');
IF NOT EXISTS(SELECT 1
              FROM info_languageinterface
              WHERE [key] = 'Network'
                AND language_id = @uk_id
                AND domain = 'targeting')
    INSERT INTO info_languageinterface ([key], translation, language_id, domain)
    VALUES ('Network', 'Мережа', @uk_id, 'targeting');
IF NOT EXISTS(SELECT 1
              FROM info_languageinterface
              WHERE [key] = 'Sales'
                AND language_id = @ru_id
                AND domain = 'targeting')
    INSERT INTO info_languageinterface ([key], translation, language_id, domain)
    VALUES ('Sales', 'Продажи', @ru_id, 'targeting');
IF NOT EXISTS(SELECT 1
              FROM info_languageinterface
              WHERE [key] = 'Sales'
                AND language_id = @uk_id
                AND domain = 'targeting')
    INSERT INTO info_languageinterface ([key], translation, language_id, domain)
    VALUES ('Sales', 'Продажі', @uk_id, 'targeting');
IF NOT EXISTS(SELECT 1
              FROM info_languageinterface
              WHERE [key] = 'Yes'
                AND language_id = @ru_id
                AND domain = 'targeting')
    INSERT INTO info_languageinterface ([key], translation, language_id, domain)
    VALUES ('Yes', 'Да', @ru_id, 'targeting');
IF NOT EXISTS(SELECT 1
              FROM info_languageinterface
              WHERE [key] = 'Yes'
                AND language_id = @uk_id
                AND domain = 'targeting')
    INSERT INTO info_languageinterface ([key], translation, language_id, domain)
    VALUES ('Yes', 'Так', @uk_id, 'targeting');
IF NOT EXISTS(SELECT 1
              FROM info_languageinterface
              WHERE [key] = 'Yes'
                AND language_id = @en_id
                AND domain = 'targeting')
    INSERT INTO info_languageinterface ([key], translation, language_id, domain)
    VALUES ('Yes', 'Yes', @en_id, 'targeting');
IF NOT EXISTS(SELECT 1
              FROM info_languageinterface
              WHERE [key] = 'No'
                AND language_id = @ru_id
                AND domain = 'targeting')
    INSERT INTO info_languageinterface ([key], translation, language_id, domain)
    VALUES ('No', 'Нет', @ru_id, 'targeting');
IF NOT EXISTS(SELECT 1
              FROM info_languageinterface
              WHERE [key] = 'No'
                AND language_id = @uk_id
                AND domain = 'targeting')
    INSERT INTO info_languageinterface ([key], translation, language_id, domain)
    VALUES ('No', 'Ні', @uk_id, 'targeting');
IF NOT EXISTS(SELECT 1
              FROM info_languageinterface
              WHERE [key] = 'No'
                AND language_id = @en_id
                AND domain = 'targeting')
    INSERT INTO info_languageinterface ([key], translation, language_id, domain)
    VALUES ('No', 'No', @en_id, 'targeting');
IF NOT EXISTS(SELECT 1
              FROM info_languageinterface
              WHERE [key] = 'rejectionReason'
                AND language_id = @ru_id
                AND domain = 'targeting')
    INSERT INTO info_languageinterface ([key], translation, language_id, domain)
    VALUES ('rejectionReason', 'Причину отклонения', @ru_id, 'targeting');
IF NOT EXISTS(SELECT 1
              FROM info_languageinterface
              WHERE [key] = 'rejectionReason'
                AND language_id = @uk_id
                AND domain = 'targeting')
    INSERT INTO info_languageinterface ([key], translation, language_id, domain)
    VALUES ('rejectionReason', 'Причину відхилення', @uk_id, 'targeting');
IF NOT EXISTS(SELECT 1
              FROM info_languageinterface
              WHERE [key] = 'rejectionReason'
                AND language_id = @en_id
                AND domain = 'targeting')
    INSERT INTO info_languageinterface ([key], translation, language_id, domain)
    VALUES ('rejectionReason', 'Rejection Reason', @en_id, 'targeting');
IF NOT EXISTS(SELECT 1
              FROM info_languageinterface
              WHERE [key] = 'removalReason'
                AND language_id = @ru_id
                AND domain = 'targeting')
    INSERT INTO info_languageinterface ([key], translation, language_id, domain)
    VALUES ('removalReason', 'Причину удаления', @ru_id, 'targeting');
IF NOT EXISTS(SELECT 1
              FROM info_languageinterface
              WHERE [key] = 'removalReason'
                AND language_id = @uk_id
                AND domain = 'targeting')
    INSERT INTO info_languageinterface ([key], translation, language_id, domain)
    VALUES ('removalReason', 'Причину видалення', @uk_id, 'targeting');
IF NOT EXISTS(SELECT 1
              FROM info_languageinterface
              WHERE [key] = 'removalReason'
                AND language_id = @en_id
                AND domain = 'targeting')
    INSERT INTO info_languageinterface ([key], translation, language_id, domain)
    VALUES ('removalReason', 'Removal Reason', @en_id, 'targeting');

IF NOT EXISTS(SELECT 1
              FROM info_languageinterface
              WHERE [key] = 'Removal reason'
                AND language_id = @ru_id
                AND domain = 'targeting')
    INSERT INTO info_languageinterface ([key], translation, language_id, domain)
    VALUES ('Removal reason', 'Причина удаления', @ru_id, 'targeting');
IF NOT EXISTS(SELECT 1
              FROM info_languageinterface
              WHERE [key] = 'Removal reason'
                AND language_id = @uk_id
                AND domain = 'targeting')
    INSERT INTO info_languageinterface ([key], translation, language_id, domain)
    VALUES ('Removal reason', 'Причина видалення', @uk_id, 'targeting');
IF NOT EXISTS(SELECT 1
              FROM info_languageinterface
              WHERE [key] = 'Comment'
                AND language_id = @ru_id
                AND domain = 'targeting')
    INSERT INTO info_languageinterface ([key], translation, language_id, domain)
    VALUES ('Comment', 'Комментарий', @ru_id, 'targeting');
IF NOT EXISTS(SELECT 1
              FROM info_languageinterface
              WHERE [key] = 'Comment'
                AND language_id = @uk_id
                AND domain = 'targeting')
    INSERT INTO info_languageinterface ([key], translation, language_id, domain)
    VALUES ('Comment', 'Коментар', @uk_id, 'targeting');
IF NOT EXISTS(SELECT 1
              FROM info_languageinterface
              WHERE [key] = 'Rejection Reason'
                AND language_id = @ru_id
                AND domain = 'targeting')
    INSERT INTO info_languageinterface ([key], translation, language_id, domain)
    VALUES ('Rejection Reason', 'Причина отклонения', @ru_id, 'targeting');
IF NOT EXISTS(SELECT 1
              FROM info_languageinterface
              WHERE [key] = 'Rejection Reason'
                AND language_id = @uk_id
                AND domain = 'targeting')
    INSERT INTO info_languageinterface ([key], translation, language_id, domain)
    VALUES ('Rejection Reason', 'Причина відхилення', @uk_id, 'targeting');
IF NOT EXISTS(SELECT 1
              FROM info_languageinterface
              WHERE [key] = 'NSR Rate'
                AND language_id = @ru_id
                AND domain = 'targeting')
    INSERT INTO info_languageinterface ([key], translation, language_id, domain)
    VALUES ('NSR Rate', 'НСР рейтинг', @ru_id, 'targeting');
IF NOT EXISTS(SELECT 1
              FROM info_languageinterface
              WHERE [key] = 'NSR Rate'
                AND language_id = @uk_id
                AND domain = 'targeting')
    INSERT INTO info_languageinterface ([key], translation, language_id, domain)
    VALUES ('NSR Rate', 'НСР рейтинг', @uk_id, 'targeting');
IF NOT EXISTS(SELECT 1
              FROM info_languageinterface
              WHERE [key] = 'Unrealized potential rating'
                AND language_id = @ru_id
                AND domain = 'targeting')
    INSERT INTO info_languageinterface ([key], translation, language_id, domain)
    VALUES ('Unrealized potential rating', 'Рейтинг по нереализованному потенциалу', @ru_id, 'targeting');
IF NOT EXISTS(SELECT 1
              FROM info_languageinterface
              WHERE [key] = 'Unrealized potential rating'
                AND language_id = @uk_id
                AND domain = 'targeting')
    INSERT INTO info_languageinterface ([key], translation, language_id, domain)
    VALUES ('Unrealized potential rating', 'Рейтинг по нереалізованому потенціалу', @uk_id, 'targeting');

GO

IF NOT EXISTS(SELECT 1
      FROM INFORMATION_SCHEMA.COLUMNS
      WHERE table_name = 'info_plancompanytask'
        AND column_name = 'removal_reason')
ALTER TABLE info_plancompanytask
    ADD removal_reason varchar(255) NULL
GO

IF NOT EXISTS(SELECT 1
      FROM INFORMATION_SCHEMA.COLUMNS
      WHERE table_name = 'info_plancompanytask'
        AND column_name = 'comment')
ALTER TABLE info_plancompanytask
    ADD comment varchar(255) NULL
GO

IF NOT EXISTS(SELECT 1
      FROM INFORMATION_SCHEMA.COLUMNS
      WHERE table_name = 'info_plancompanytask'
        AND column_name = 'rejection_reason')
ALTER TABLE info_plancompanytask
    ADD rejection_reason varchar(255) NULL
GO

IF NOT EXISTS(SELECT 1
      FROM INFORMATION_SCHEMA.COLUMNS
      WHERE table_name = 'info_plancontacttask'
        AND column_name = 'removal_reason')
ALTER TABLE info_plancontacttask
    ADD removal_reason varchar(255) NULL
GO

IF NOT EXISTS(SELECT 1
      FROM INFORMATION_SCHEMA.COLUMNS
      WHERE table_name = 'info_plancontacttask'
        AND column_name = 'comment')
ALTER TABLE info_plancontacttask
    ADD comment varchar(255) NULL
GO

IF NOT EXISTS(SELECT 1
      FROM INFORMATION_SCHEMA.COLUMNS
      WHERE table_name = 'info_plancontacttask'
        AND column_name = 'rejection_reason')
ALTER TABLE info_plancontacttask
    ADD rejection_reason varchar(255) NULL
GO

IF NOT EXISTS(SELECT 1
              FROM INFORMATION_SCHEMA.COLUMNS
              WHERE table_name = 'info_AddInfoType'
                AND column_name = 'direction_id')
ALTER TABLE info_AddInfoType
    ADD direction_id int NULL
GO

IF OBJECT_ID(N'[info_companyarrangement]','U') IS NULL
    BEGIN
        -- auto-generated definition
        create table info_companyarrangement
        (
            id          int identity
                constraint PK_info_companyarrangement
                    primary key,
            currenttime datetime
                constraint DF_info_companyarrangementcurrenttime default getdate(),
            time        timestamp null,
            moduser     varchar(16)
                constraint DF_info_companyarrangementmoduser default [dbo].[Get_CurrentCode](),
            guid        uniqueidentifier
                constraint DF_info_companyarrangementguid default newid(),
            company_id  int
                constraint FK_info_companyarrangement_company_id
                    references info_company
                    on update cascade,
            datefrom    datetime,
            datetill    datetime,
            priority_id int
                constraint FK_info_companyarrangement_priority_id
                    references info_dictionary
                    on update cascade
        )


        create index ix_info_companyarrangement_guid
            on info_companyarrangement (guid)


        create index ix_info_companyarrangement_company_id
            on info_companyarrangement (company_id)


        create index ix_info_companyarrangement_priority_id
            on info_companyarrangement (priority_id)

    END
GO

IF OBJECT_ID(N'[info_userbrick]', 'U') IS NULL
    begin
        CREATE TABLE [dbo].[info_userbrick]
        (
            [id]          [int] IDENTITY (1,1) NOT NULL,
            [user_id]     [int]                NULL,
            [company_id]  [int]                NULL,
            [contact_id]  [int]                NULL,
            [currenttime] [datetime]           NULL,
            [time]        [timestamp]          NULL,
            [moduser]     [varchar](16)        NULL,
            [guid]        [uniqueidentifier]   NULL,
            CONSTRAINT [PK_info_userbrick] PRIMARY KEY CLUSTERED
                (
                 [id] ASC
                    ) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
        ) ON [PRIMARY]


        SET ANSI_PADDING OFF

        ALTER TABLE [dbo].[info_userbrick]
            ADD CONSTRAINT [DF_info_userbrickcurrenttime] DEFAULT (getdate()) FOR [currenttime]

        ALTER TABLE [dbo].[info_userbrick]
            ADD CONSTRAINT [DF_info_userbrickmoduser] DEFAULT ([dbo].[Get_CurrentCode]()) FOR [moduser]

        ALTER TABLE [dbo].[info_userbrick]
            ADD CONSTRAINT [DF_info_userbrickguid] DEFAULT (newid()) FOR [guid]

        ALTER TABLE [dbo].[info_userbrick]
            WITH CHECK ADD CONSTRAINT [FK_info_userbrick_info_company] FOREIGN KEY ([company_id])
                REFERENCES [dbo].[info_company] ([id])

        ALTER TABLE [dbo].[info_userbrick]
            CHECK CONSTRAINT [FK_info_userbrick_info_company]

        ALTER TABLE [dbo].[info_userbrick]
            WITH CHECK ADD CONSTRAINT [FK_info_userbrick_info_contact] FOREIGN KEY ([contact_id])
                REFERENCES [dbo].[info_contact] ([id])

        ALTER TABLE [dbo].[info_userbrick]
            CHECK CONSTRAINT [FK_info_userbrick_info_contact]

        ALTER TABLE [dbo].[info_userbrick]
            WITH CHECK ADD CONSTRAINT [FK_info_userbrick_info_user] FOREIGN KEY ([user_id])
                REFERENCES [dbo].[info_user] ([id])

        ALTER TABLE [dbo].[info_userbrick]
            CHECK CONSTRAINT [FK_info_userbrick_info_user]
    end
GO

if not exists(SELECT 1
              FROM INFORMATION_SCHEMA.COLUMNS
              WHERE TABLE_NAME = 'info_plancompanytask'
                AND Column_Name = 'status')
    begin
        alter table info_plancompanytask
            add status int NULL
    end
GO

IF EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'targeting_company_initial') AND xtype IN (N'FN', N'IF', N'TF'))
    DROP FUNCTION targeting_company_initial
GO

CREATE FUNCTION targeting_company_initial
(
  @user_id INT,
  @firstDate DATETIME,
  @monthCount INT
)
RETURNS TABLE
AS
RETURN
(
  SELECT TOP 2147483647 company_id
    , MAX(visit_cnt) visit_cnt
    , CASE
        WHEN MAX(visit_cnt) = 0 THEN 2
        ELSE MAX(status)
      END [status]
  FROM
  (
    SELECT c.company_id, pcr.visit_cnt, 0 [status]
    FROM
      (
        SELECT
          c.id company_id, cat.id category_id, ci2.IntValue
          , ROW_NUMBER() OVER ( PARTITION BY cat.id ORDER BY ci2.IntValue desc ) cnt_per_category
        FROM info_company c
          JOIN info_companyinfo ci1 ON c.id = ci1.Subj_id AND ci1.InfoType_id IN (
          select id from info_AddInfoType where Code like 'pharm_categ_%'
          and direction_id in (select direction_id from info_userdirection where user_id = @user_id
          ))
          JOIN info_companycategory cat ON ci1.sysdictvalue_id = cat.id
          JOIN info_userbrick ub ON c.id = ub.company_id
          LEFT JOIN info_companyinfo ci2 ON c.id = ci2.Subj_id AND ci2.InfoType_id IN (SELECT id FROM info_AddInfoType WHERE Code = 'tovarooborot')
        WHERE ub.user_id = @user_id
      ) c
      JOIN info_plancompanyrule pcr ON c.Category_id = pcr.category_id
    WHERE cnt_per_category <= pcr.company_cnt AND pcr.datefrom = @firstDate

    UNION

    SELECT pct.company_id, 0 visit_cnt, 1 [status]
    FROM info_plancompanytask pct
      JOIN info_company c ON pct.company_id = c.id AND isnull(IsArchive, 0) = 0
      JOIN info_companytype ct ON c.CompanyType_id = ct.id AND isnull(isshop, 0) =1
      JOIN info_userbrick ub ON c.id = ub.company_id AND pct.user_id = ub.user_id
    WHERE pct.user_id = @user_id AND pct.dt >= CONVERT(varchar(7), DATEADD(month, -1 * @monthCount, GETDATE()), 20) + '-01' AND pct.dt < GETDATE() AND isnull([status], 0) <> 2

    UNION

    SELECT t.company_id, 0 visit_cnt, 1 [status]
    FROM info_task t
      JOIN info_company c ON t.company_id = c.id AND isnull(IsArchive, 0) = 0
      JOIN info_companytype ct ON c.CompanyType_id = ct.id AND isnull(isshop, 0) =1
      JOIN info_userbrick ub ON c.id = ub.company_id AND responsible_id = ub.user_id
    WHERE responsible_id = @user_id AND t.datefrom >= DATEADD(month, -1 * @monthCount, GETDATE()) AND t.datefrom < GETDATE()
  ) result
  GROUP BY company_id
)
GO

if not exists(SELECT 1
              FROM INFORMATION_SCHEMA.COLUMNS
              WHERE TABLE_NAME = 'info_plancontacttask'
                AND Column_Name = 'status')
    begin
        alter table info_plancontacttask
            add status int NULL
    end
GO

IF EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'targeting_contact_initial') AND xtype IN (N'FN', N'IF', N'TF'))
    DROP FUNCTION targeting_contact_initial
GO

CREATE FUNCTION targeting_contact_initial
(
  @user_id INT,
  @firstDate DATETIME,
  @monthCount INT
)
RETURNS TABLE
AS
RETURN
(

  SELECT TOP 2147483647 contact_id
    , MAX(visit_cnt) visit_cnt
    , CASE
        WHEN MAX(visit_cnt) = 0 THEN 2
        ELSE MAX(status)
      END [status]
  FROM
  (
    SELECT c.contact_id, pcr.visit_cnt, 0 [status]
    FROM
      (
        SELECT c.id contact_id, cc.id category_id, t.id target_id
          , ROW_NUMBER() OVER ( PARTITION BY cc.id, t.id ORDER BY ci.IntValue desc ) cnt_per_category
        FROM info_contact c
          JOIN info_contactinfo ci3 ON c.id = ci3.Subj_id
            AND ci3.InfoType_id IN (
            select id from info_AddInfoType where Code like 'pharm_categ_cont_%'
              and direction_id in (select direction_id from info_userdirection where user_id = @user_id)
          )
         JOIN info_contactcateg cc ON ci3.sysdictvalue_id = cc.id AND cc.contacttype_id in (SELECT id FROM info_contacttype where Name = 'Прескрайбер')
          JOIN info_targetspec ts ON ts.spec_id = c.specialization_id
          JOIN info_target t ON t.id=ts.target_id
          JOIN info_userbrick ub ON c.id = ub.contact_id
          LEFT JOIN info_contactinfo ci ON c.id = ci.Subj_id AND ci.InfoType_id IN (SELECT id FROM info_AddInfoType WHERE Code = 'patientcount')
        WHERE ub.user_id = @user_id AND isnull(c.IsArchive, 0) = 0
        GROUP BY c.id, cc.id, t.id, ci.IntValue
      ) c
      JOIN info_plancontactrule pcr ON c.category_id = pcr.category_id AND c.target_id = pcr.target_id
    WHERE cnt_per_category <= pcr.contact_cnt AND pcr.datefrom = @firstDate

    UNION

    SELECT c.id contact_id, 0 visit_cnt, 1 [status]
    FROM info_plancontacttask pct
      JOIN info_contact c ON pct.contact_id = c.id AND isnull(IsArchive, 0) = 0
      JOIN info_userbrick ub ON c.id = ub.contact_id AND pct.user_id = ub.user_id
    WHERE pct.user_id = @user_id AND pct.dt >= CONVERT(varchar(7), DATEADD(month, -1 * @monthCount, GETDATE()), 20) + '-01' AND pct.dt < GETDATE() AND isnull(pct.[status], 0) <> 2

    UNION

    SELECT t.contact_id, 0 visit_cnt, 1 [status]
    FROM info_task t
      JOIN info_contact c ON t.contact_id = c.id AND isnull(IsArchive, 0) = 0
      JOIN info_userbrick ub ON c.id = ub.contact_id AND responsible_id = ub.user_id
    WHERE responsible_id = @user_id AND t.datefrom >= DATEADD(month, -1 * @monthCount, GETDATE()) AND t.datefrom < GETDATE()
    GROUP BY t.contact_id
  ) result
  GROUP BY contact_id
)
GO

IF EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'targeting_company') AND xtype IN (N'FN', N'IF', N'TF'))
    DROP FUNCTION targeting_company
GO

CREATE FUNCTION targeting_company
(
  @user_id INT,
  @firstDate datetime,
  @monthCount INT
)
RETURNS TABLE
AS
RETURN
(
  SELECT TOP 2147483647 co.id
    , co.name
    , co.category_id
    , ISNULL(city.name, '-') as cityName
    , ISNULL(st.Name, '')+ ' ' + ISNULL(co.Street, '') + (CASE WHEN co.Building IS NULL OR co.Building = ''
                                                          THEN ''
                                                          ELSE ', '+co.Building
                                                          END) as [address]
    , ISNULL(cat.name, '-') AS categoryName
    , salefact.fact AS tovarooborot
    , ISNULL(LOWER(cdv1.DictionaryValue), '-') AS segment
    , CASE
      WHEN LOWER(cdv1.DictionaryValue) = 'чемпион' THEN 'loyal'
      WHEN LOWER(cdv1.DictionaryValue) = 'ключ' THEN 'grow'
      WHEN LOWER(cdv1.DictionaryValue) = 'лояльный' THEN 'maintain'
      WHEN LOWER(cdv1.DictionaryValue) = 'не посещаем' THEN 'expose'
      ELSE '-'
      END AS segmentEng
    , MIN(isnull(pcr.visit_cnt, 0)) AS plancnt
    , MAX(case when MONTH(pct.dt) = MONTH(@firstDate) then pct.tasks else 0 end) AS tasks1
    , MAX(case when MONTH(pct.dt) = MONTH(@firstDate)+1 then pct.tasks else 0 end) AS tasks2
    , MAX(case when MONTH(pct.dt) = MONTH(@firstDate)+2 then pct.tasks else 0 end) AS tasks3
    , MAX(case when MONTH(pct.dt) = MONTH(@firstDate)+3 then pct.tasks else 0 end) AS tasks4
    , ct.Name AS companyType
    , pct.approvementStatus AS approvalStatus
    , ISNULL(pct.status, '-') [status]
    , round(COALESCE(CAST(salefact.sale AS FLOAT) / NULLIF(salefact.fact, 0) * 100, 0), 2) AS salePerTovarooborot
    , CASE WHEN (
        SELECT TOP 1 id FROM info_plancompanytask
          WHERE user_id = @user_id
            AND dt BETWEEN DATEADD(month, -1 * @monthCount, @firstDate) AND DATEADD(ss, -1, @firstDate)
            AND company_id = co.id
            AND approvementStatus = 2
            AND tasks > 0
      ) IS NULL THEN 0
      ELSE 1
      END AS isPrevious
    , others.name as others
    , tcc.critery1
    , tcc.critery2
    , tcc.critery3
    , net.name as net
    , CASE WHEN count(car.id) = 0 THEN 'No' ELSE 'Yes' END as arrangement
    , isnull(sf.sales, 0) as sales
    , pct.removal_reason as "removalReason"
    , pct.comment
    , pct.rejection_reason as "rejectionReason"
  FROM info_plancompanytask pct
    LEFT JOIN info_company co ON co.id = pct.company_id
    JOIN info_companytype ct ON co.CompanyType_id = ct.id
    LEFT JOIN info_city city ON city.id = co.city_id
    LEFT JOIN info_dictionary st ON co.streettype_id = st.id
    LEFT JOIN info_companyinfo ci1 ON co.id = ci1.Subj_id AND ci1.InfoType_id IN (SELECT id FROM info_AddInfoType WHERE Code = 'segm_apt')
    LEFT JOIN info_CustomDictionaryValue cdv1 ON cdv1.id = ci1.DictValue_id

    LEFT JOIN info_companyinfo ci2 ON co.id = ci2.Subj_id
        AND ci2.InfoType_id IN (
        select id from info_AddInfoType where Code like 'pharm_categ_%'
        and direction_id in (select direction_id from info_userdirection where user_id = @user_id)
		)
    LEFT JOIN info_companycategory cat ON ci2.sysdictvalue_id = cat.id
    LEFT JOIN info_plancompanyrule pcr ON cat.id=pcr.category_id AND pct.dt BETWEEN pcr.datefrom AND pcr.datetill
    LEFT JOIN
    (
      SELECT company_id, SUM(amount) sale, MAX(intvalue) fact FROM
       (
         SELECT sf.company_id, ci.IntValue, sf.Amount, DENSE_RANK() over(partition by sf.company_id order by sp.datefrom desc) as dt -- последние три месяца где есть продажи учреждения
         FROM info_salefact as sf
           JOIN info_saleperiod as sp on sp.id = sf.SalePeriod_id
           JOIN info_CompanyInfo as ci on ci.Subj_id = sf.company_id and ci.InfoType_id in (SELECT id FROM info_AddInfoType WHERE Code = 'tovarooborot')
       ) tmp
      WHERE tmp.dt < 4
      GROUP BY tmp.company_id
    ) AS salefact ON salefact.company_id = co.id
       LEFT JOIN (
               SELECT company_id, name =
               STUFF((SELECT ', ' + name
                          FROM (select pct.company_id, u.name
       from info_plancompanytask pct
       left join info_user u on u.id = pct.user_id
       where pct.user_id in (select distinct user_id from info_userdirection where direction_id in (
       select direction_id from info_userdirection where user_id = @user_id)) and not pct.user_id = @user_id
       and dt >= @firstDate AND dt < DATEADD(month, @monthCount, @firstDate) and not COALESCE(tasks, 0) = 0) as b
                          WHERE b.company_id = a.company_id
                         FOR XML PATH('')), 1, 2, '')
       FROM (select pct.company_id, u.name
       from info_plancompanytask pct
       left join info_user u on u.id = pct.user_id
       where pct.user_id in (select distinct user_id from info_userdirection where direction_id in (
       select direction_id from info_userdirection where user_id = @user_id)) and not pct.user_id = @user_id
       and dt >= @firstDate AND dt < DATEADD(month, @monthCount, @firstDate) and not COALESCE(tasks, 0) = 0) as a
       GROUP BY company_id
       ) AS others ON others.company_id = co.id
       LEFT JOIN info_targetingcompanycritery tcc ON tcc.company_id = co.id
    AND tcc.direction_id IN
        (SELECT direction_id
         FROM info_userdirection
         WHERE user_id = @user_id)
         LEFT JOIN info_company net ON co.main_id = net.id
         LEFT JOIN info_companyarrangement car ON car.company_id = co.id AND @firstDate BETWEEN car.datefrom AND car.datetill
         LEFT JOIN (select company_id, sum(count) as sales from info_salefact
                    where saleperiod_id IN (select id from info_saleperiod where @firstDate between datefrom and datetill)
                      and preparation_id IN (select preparation_id from info_preparationdirection where direction_id in (SELECT direction_id FROM info_userdirection WHERE user_id = @user_id))
                    group by company_id) as sf on sf.company_id = co.id
  WHERE pct.user_id = @user_id
        AND pct.company_id IS NOT NULL
        AND pct.dt >= @firstDate AND pct.dt < DATEADD(month, @monthCount, @firstDate)
  GROUP BY co.id, pct.id, co.name, city.name, st.Name, co.Street, co.Building, co.category_id, cat.name, ct.Name, pct.approvementStatus, salefact.sale, salefact.fact, cdv1.DictionaryValue, pct.status, others.name,
         tcc.critery1, tcc.critery2, tcc.critery3, net.name, sf.sales, pct.removal_reason, pct.comment, pct.rejection_reason
  ORDER BY salefact.fact DESC
)
GO

IF EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'targeting_contact') AND xtype IN (N'FN', N'IF', N'TF'))
    DROP FUNCTION targeting_contact
GO

CREATE FUNCTION targeting_contact
(
  @user_id INT,
  @firstDate DATETIME,
  @monthCount INT
)
RETURNS TABLE
AS
RETURN
(

  SELECT TOP 2147483647 c.id
    , (c.lastname + ' '+ c.firstname + ' '+ c.middlename) as name
    , ISNULL(t.name, '-') AS targetName
    , ISNULL(city.Name, '-') + ', ' + isnull(co.name, '-') AS companyName
    , ISNULL(c.categoryprep, '-') AS categoryName
    , ci1.IntValue AS patientcount
    , ISNULL(LOWER(cdv.DictionaryValue), '-') AS segment
    , CASE
      WHEN LOWER(cdv.DictionaryValue) = 'чемпион' THEN 'loyal'
      WHEN LOWER(cdv.DictionaryValue) = 'ключ' THEN 'grow'
      WHEN LOWER(cdv.DictionaryValue) = 'лояльный' THEN 'maintain'
      WHEN LOWER(cdv.DictionaryValue) = 'не посещаем' THEN 'expose'
      ELSE '-'
      END AS segmentEng
    , MIN(isnull(pcr.visit_cnt, 0)) AS plancnt
    , MAX(case when MONTH(dt) = MONTH(@firstDate) then pct.tasks else 0 end) AS tasks1
    , MAX(case when MONTH(dt) = MONTH(@firstDate)+1 then pct.tasks else 0 end) AS tasks2
    , MAX(case when MONTH(dt) = MONTH(@firstDate)+2 then pct.tasks else 0 end) AS tasks3
    , pct.approvementStatus AS approvalStatus
    , ISNULL(pct.status, '-') [status]
    , potential.cnt AS potential
    , round(COALESCE(convert(float,potential.factCnt)/NULLIF(potential.cnt, 0) * 100, 0), 2) factPotential
    , CASE WHEN (
        SELECT TOP 1 id FROM info_plancontacttask
          WHERE user_id = @user_id
            AND dt BETWEEN DATEADD(month, -1 * @monthCount, @firstDate) AND DATEADD(ss, -1, @firstDate)
            AND contact_id = c.id
            AND approvementStatus = 2
            AND tasks > 0
      ) IS NULL THEN 0
      ELSE 1
      END AS isPrevious
      , others.name as others
      , tcr.nsr_rate as "nsrRate"
      , tcr.potential_rate as "potentialRate"
      , pct.removal_reason as "removalReason"
      , pct.comment
      , pct.rejection_reason as "rejectionReason"
  FROM info_plancontacttask pct
    inner JOIN info_contact c ON c.id = pct.contact_id
    inner join info_userdirection ud on ud.user_id=@user_id
    LEFT JOIN info_targetspec ts on ts.spec_id = c.specialization_id
    LEFT JOIN info_target t on t.id=ts.target_id
    LEFT JOIN info_company co on co.id = c.company_id
    LEFT JOIN info_contactinfo ci3 ON c.id = ci3.Subj_id
    AND ci3.InfoType_id IN (
        select id from info_AddInfoType where Code like 'pharm_categ_cont_%'
        and direction_id in (select direction_id from info_userdirection where user_id = @user_id)
    )
    LEFT JOIN info_contactcateg cc ON cc.name = c.categoryPrep and cc.contacttype_Id in (SELECT id FROM info_contacttype where Name = 'Прескрайбер')
    LEFT JOIN info_contactinfo ci1 ON c.id = ci1.Subj_id AND ci1.InfoType_id IN (SELECT id FROM info_AddInfoType WHERE Code = 'patientcount')
    LEFT JOIN info_contactinfo ci2 ON c.id = ci2.Subj_id
    AND ci2.InfoType_id IN (
        select id from info_AddInfoType where Code like 'segm_doctor_%'
        and direction_id in (select direction_id from info_userdirection where user_id = @user_id)
    )
    LEFT JOIN info_CustomDictionaryValue cdv ON cdv.id = ci2.DictValue_id
    LEFT JOIN info_city city ON c.City_id = city.id
    LEFT JOIN info_plancontactrule pcr ON t.id = pcr.target_id AND pcr.category_id = cc.id AND dt BETWEEN pcr.datefrom AND pcr.datetill
    LEFT JOIN info_plancontactrule pcr1 ON t.id = pcr1.target_id  AND dt BETWEEN pcr1.datefrom AND pcr1.datetill
    LEFT JOIN
    (
      SELECT cp.Contact_id, cp.brend_id, SUM(cp.Potential) cnt, SUM(cp.FactCount) factCnt FROM info_contactpotential cp
        JOIN
        (
         SELECT contact_id, MAX(datefrom) dt
         FROM info_task as t JOIN info_taskstate ts ON t.taskstate_id = ts.id
         WHERE t.datefrom >= DATEADD(MONTH, -1 * @monthCount, GETDATE()) AND ts.isclosed = 1
         GROUP BY t.contact_id
        ) AS max_date ON cp.Date = dateadd( dd, 0, datediff(dd, 0, max_date.dt)) AND cp.Contact_id = max_date.contact_id
      GROUP BY cp.Contact_id, cp.brend_id
    ) AS potential ON c.id = potential.Contact_id AND pcr1.brand_id = potential.brend_id
    LEFT JOIN (
       SELECT contact_id, name =
         STUFF((SELECT ', ' + name
          FROM (select pct.contact_id, u.name
       from info_plancontacttask pct
       left join info_user u on u.id = pct.user_id
       where pct.user_id in (select distinct user_id from info_userdirection where direction_id in (
       select direction_id from info_userdirection where user_id = @user_id)) and not pct.user_id = @user_id
       and dt >= @firstDate AND dt < DATEADD(month, @monthCount, @firstDate) and not COALESCE(tasks, 0) = 0) as b
          WHERE b.contact_id = a.contact_id
         FOR XML PATH('')), 1, 2, '')
       FROM (select pct.contact_id, u.name
       from info_plancontacttask pct
       left join info_user u on u.id = pct.user_id
       where pct.user_id in (select distinct user_id from info_userdirection where direction_id in (
       select direction_id from info_userdirection where user_id = @user_id)) and not pct.user_id = @user_id
       and dt >= @firstDate AND dt < DATEADD(month, @monthCount, @firstDate) and not COALESCE(tasks, 0) = 0) as a
       GROUP BY contact_id
   ) AS others ON others.contact_id = c.id
       LEFT JOIN info_targetingcontactrate tcr on tcr.contact_id = c.id
        AND tcr.direction_id IN
            (SELECT direction_id
             FROM info_userdirection
             WHERE user_id = @user_id)
  WHERE pct.user_id = @user_id
        AND pct.contact_id is not NULL
        AND pct.dt >= @firstDate AND pct.dt < DATEADD(month, @monthCount, @firstDate)
  GROUP BY c.id, pct.id, c.lastname, c.firstname, c.middlename, t.name,  co.name, c.categoryprep, pct.approvementStatus, ci1.IntValue, cdv.DictionaryValue, pct.status, potential.cnt, potential.factCnt, isnull(city.Name, '-') + ', ' + isnull(co.name, '-'), others.name, tcr.nsr_rate, tcr.potential_rate, tcr.nsr_rate, tcr.potential_rate, pct.removal_reason, pct.comment, pct.rejection_reason
  ORDER BY ci1.IntValue DESC
)
GO


IF EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'targeting_company_summary') AND xtype IN (N'FN', N'IF', N'TF'))
    DROP FUNCTION targeting_company_summary
GO

CREATE FUNCTION targeting_company_summary
(
  @user_id INT,
  @firstDate DATETIME,
  @monthCount INT
)
RETURNS TABLE
AS
RETURN
(
  SELECT category_name [group_name]
    , SUM(item_cnt) [item_cnt]
    , SUM(visit_cnt) [visit_cnt]
    , SUM(itemfact_cnt) [itemfact_cnt]
    , SUM(visitfact_cnt) [visitfact_cnt]
  FROM
  (
    SELECT
      CASE
        WHEN pcr.visit_cnt = 1 THEN 'B'
        WHEN pcr.visit_cnt = 2 THEN 'A'
        WHEN pcr.visit_cnt > 2 THEN 'A+'
        ELSE '-'
      END category_name
      , sum(pcr.company_cnt) AS item_cnt
      , sum(pcr.visit_cnt * pcr.company_cnt * @monthCount) AS visit_cnt
      , 0 AS itemfact_cnt
      , 0 AS visitfact_cnt
    FROM info_plancompanyrule AS pcr
    inner join info_companycategory cc on pcr.category_id=cc.id
    inner join info_userdirection ud on cc.direction_id=ud.direction_id
    WHERE pcr.datefrom >= @firstDate AND pcr.datefrom < DATEADD(month, @monthCount, @firstDate) and ud.user_id=@user_id
    GROUP BY pcr.visit_cnt, cc.name

    UNION ALL

    SELECT ISNULL(cat.name, '-'), 0, 0, count(DISTINCT c.id), SUM(pt.tasks)
    FROM info_plancompanytask AS pt
      INNER JOIN info_company AS c ON c.id = pt.company_id
      LEFT JOIN info_plancompanyrule pr ON pr.id = (
        SELECT TOP 1 cr.id FROM info_plancompanyrule cr
        inner join info_companycategory ccat on cr.category_id=ccat.id
        inner join info_direction dd on ccat.direction_id=dd.id
        inner join info_userdirection uud on dd.id=uud.direction_id and uud.user_id=@user_id
        WHERE visit_cnt = pt.tasks AND pt.dt BETWEEN datefrom AND datetill
      )
      LEFT JOIN info_companycategory cat ON pr.category_id = cat.id
    WHERE pt.dt >= @firstDate AND pt.dt < DATEADD(month, @monthCount, @firstDate) AND isnull(pt.tasks, 0) > 0 AND pt.user_id = @user_id
    GROUP BY cat.name
  ) AS t
  GROUP BY category_name
)
GO

IF EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'targeting_contact_summary') AND xtype IN (N'FN', N'IF', N'TF'))
    DROP FUNCTION targeting_contact_summary
GO

CREATE FUNCTION targeting_contact_summary
(
  @user_id INT,
  @firstDate DATETIME,
  @monthCount INT
)
RETURNS TABLE
AS
RETURN
(
  SELECT
    id,
    tar [group_name],
    category,
    SUM(contact_cnt) [item_cnt],
    SUM(visit_cnt) [visit_cnt],
    SUM(contactfact_cnt) [itemfact_cnt],
    SUM(visitfact_cnt) [visitfact_cnt]
      FROM
        (
          SELECT t.id
            , t.name AS tar
            , CASE
                WHEN pcr.visit_cnt = 1 THEN 'B'
                WHEN pcr.visit_cnt = 2 THEN 'A'
                WHEN pcr.visit_cnt > 2 THEN 'A+'
                ELSE '-'
              END category
            , SUM(pcr.contact_cnt) AS contact_cnt
            , SUM(pcr.visit_cnt * pcr.contact_cnt * @monthCount) AS visit_cnt
            , 0 AS contactfact_cnt
            , 0 AS visitfact_cnt
          FROM info_plancontactrule AS pcr
            INNER JOIN info_target AS t ON t.id = pcr.target_id
            inner join info_contactcateg cc on cc.id=pcr.category_id
           inner join info_userdirection ud on ud.direction_id=cc.direction_id and ud.user_id=@user_id
          WHERE pcr.datefrom >= @firstDate AND pcr.datefrom < DATEADD(month, @monthCount, @firstDate)
          GROUP BY t.name, t.id, pcr.visit_cnt, cc.name, pcr.contact_cnt

          UNION ALL

          SELECT t.id
            , t.name
            , cat.name
            , 0
            , 0
            , count(DISTINCT c.id)
            , SUM(pt.tasks)
          FROM info_plancontacttask AS pt
            JOIN info_contact AS c ON c.id = pt.contact_id
            JOIN info_targetspec AS ts ON ts.spec_id = c.Specialization_id
            JOIN info_target AS t ON t.id = ts.target_id
            LEFT JOIN info_plancontactrule AS pcr ON  pcr.id = (
              SELECT top 1 pcrr.id FROM info_plancontactrule pcrr
              inner join info_contactcateg ccc on pcrr.category_id=ccc.id
                inner join info_userdirection ud on ud.direction_id=ccc.direction_id and ud.user_id=@user_id
                WHERE pt.dt BETWEEN pcrr.datefrom AND pcrr.datetill AND ((pcrr.visit_cnt = pt.tasks) OR (pcrr.visit_cnt = 3 AND pt.tasks > 2))
                AND pcrr.target_id = t.id
            )
            LEFT JOIN info_contactcateg cat ON pcr.Category_id = cat.id
          WHERE pt.dt >= @firstDate AND pt.dt < DATEADD(month, @monthCount, @firstDate) AND isnull(pt.tasks, 0) > 0 AND pt.user_id = @user_id
          GROUP BY t.id, t.name, cat.name
        ) AS t
  GROUP BY id, tar, category
)
GO
----------------------------------------------------------------------------------------------------------
--https://teamsoft.atlassian.net/browse/MCM-293
IF NOT EXISTS(SELECT 1
              FROM info_options
              WHERE name = 'MCMReturnPath')
    INSERT INTO info_options (name, value) VALUES ('MCMReturnPath', 'donotreply@mcm.proximaresearch.com')
GO
-----------------------------------------------------------------------------------------------------------

----------------------------------------------------------------------------------------------------------
--fix - adding phonecode into info_country
IF NOT EXISTS (SELECT 1 FROM information_schema.columns WHERE table_name = 'info_country' AND column_name = 'phonecode')
    BEGIN
        ALTER TABLE info_country ADD phonecode varchar(255)  null
    END
GO
-----------------------------------------------------------------------------------------------------------
