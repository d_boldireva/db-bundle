------------------------------------------------------------------------------------------------------------------------
------- https://teamsoft.atlassian.net/browse/PHARMAWEB-672
------------------------------------------------------------------------------------------------------------------------
exec insertLanguageInterface 'en', 'distribution_created', 'Created', 'mcm';
exec insertLanguageInterface 'ru', 'distribution_created', 'Создана', 'mcm';
exec insertLanguageInterface 'uk', 'distribution_created', 'Створена', 'mcm';
GO
exec insertLanguageInterface 'en', 'content_created', 'Created', 'mcm';
exec insertLanguageInterface 'ru', 'content_created', 'Создан', 'mcm';
exec insertLanguageInterface 'uk', 'content_created', 'Створений', 'mcm';
GO
------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------
-- https://teamsoft.atlassian.net/browse/PHARMAWEB-456
------------------------------------------------------------------------------------------------------------------------
IF NOT EXISTS (SELECT 1 FROM information_schema.columns WHERE table_name = 'info_dictionaryidentifier' AND column_name = 'values_list')
BEGIN
ALTER TABLE info_dictionaryidentifier ADD values_list varchar(255) NULL
END
GO

DECLARE @en_id int, @ru_id int, @uk_id int;
SELECT TOP 1 @en_id = id FROM info_language WHERE slug = 'en';
SELECT TOP 1 @ru_id = id FROM info_language WHERE slug = 'ru';
SELECT TOP 1 @uk_id = id FROM info_language WHERE slug = 'uk';

IF NOT EXISTS (SELECT 1 FROM info_dictionaryidentifier WHERE tablename = 'info_tasktype' AND columnname = 'showname')
BEGIN
INSERT INTO info_dictionaryidentifier (tablename, columnname, value, values_list, order_num, hidden, required, language_id)
VALUES ('info_tasktype', 'showname', 'Display in the schedule', '[{"name":"—","value":0},{"name":"Client","value":1},{"name":"Company","value":2},{"name":"Continuous numbering by doctor","value":3}]', 7, 0, 0, @en_id),
       ('info_tasktype', 'showname', 'Отображать в расписании', '[{"name":"—","value":0},{"name":"Клиент","value":1},{"name":"Учреждение","value":2},{"name":"Сквозная нумерация по врачу","value":3}]', 7, 0, 0, @ru_id),
       ('info_tasktype', 'showname', 'Відображати у розкладі', '[{"name":"—","value":0},{"name":"Клієнт","value":1},{"name":"Установа","value":2},{"name":"Наскрізна нумерація за лікарем","value":3}]', 7, 0, 0, @uk_id)
END
GO
------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------
-- https://teamsoft.atlassian.net/browse/FARMAK-289
------------------------------------------------------------------------------------------------------------------------
IF NOT EXISTS (SELECT * FROM info_tabcontroldictionary WHERE parent_type = 13 AND type = 4)
    INSERT INTO info_tabcontroldictionary (parent_type, type, parent_type_name, type_name)
    VALUES (13, 4, 'Визиты', 'Отчет')
GO
------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------

----------------------------------------------------------------------------------------------------
-- https://teamsoft.atlassian.net/browse/ARTERIUM-255
----------------------------------------------------------------------------------------------------
EXEC insertLanguageInterface 'en','Disable edit','Disable edit','messages';
EXEC insertLanguageInterface 'ru','Disable edit',N'Отключить редактирование','messages';
EXEC insertLanguageInterface 'uk','Disable edit',N'Вимкнути редагування','messages';

EXEC insertLanguageInterface 'en','Show on visit','Show on visit','messages';
EXEC insertLanguageInterface 'ru','Show on visit',N'Отображать на визите','messages';
EXEC insertLanguageInterface 'uk','Show on visit',N'Відображати на візиті','messages';
GO
----------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------------------------
-- https://teamsoft.atlassian.net/browse/CRM-99
---------------------------------------------------------------------------------------------------------------------
EXEC insertLanguageInterface 'en', 'Select target roles', 'Select target roles', 'messages';
EXEC insertLanguageInterface 'ru', 'Select target roles', N'Выберите целевые роли', 'messages';
EXEC insertLanguageInterface 'uk', 'Select target roles', N'Оберіть цільові ролі', 'messages';

EXEC insertLanguageInterface 'en', 'Please, choose at least one target role', 'Please, choose at least one target role', 'messages';
EXEC insertLanguageInterface 'ru', 'Please, choose at least one target role', N'Пожалуйста, выберите хотя бы одну роль', 'messages';
EXEC insertLanguageInterface 'uk', 'Please, choose at least one target role', N'Будь ласка, оберіть принаймні одну роль', 'messages';
GO
---------------------------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------
-- https://teamsoft.atlassian.net/browse/PHARMAWEB-258
----------------------------------------------------------------------------------------------------
exec insertLanguageInterface 'en', 'access to editing companies', 'access to editing companies', 'messages';
exec insertLanguageInterface 'ru', 'access to editing companies', 'доступ к редактированию компаний', 'messages';
exec insertLanguageInterface 'uk', 'access to editing companies', 'доступ до редагування компаній', 'messages';

exec insertLanguageInterface 'en', 'access to editing clients', 'access to editing clients', 'messages';
exec insertLanguageInterface 'ru', 'access to editing clients', 'доступ к редактированию клиентов', 'messages';
exec insertLanguageInterface 'uk', 'access to editing clients', 'доступ до редагування клієнтів', 'messages';

exec insertLanguageInterface 'en', 'disable ability to make calls to clients', 'disable ability to make calls to clients', 'messages';
exec insertLanguageInterface 'ru', 'disable ability to make calls to clients', 'отключить возможность осуществлять звонки клиентам', 'messages';
exec insertLanguageInterface 'uk', 'disable ability to make calls to clients', 'вимкнути можливість здійснювати дзвінки клієнтам', 'messages';

exec insertLanguageInterface 'en', 'disable ability to send messages to clients', 'disable ability to send messages to clients', 'messages';
exec insertLanguageInterface 'ru', 'disable ability to send messages to clients', 'отключить возможность отправки сообщений клиентам', 'messages';
exec insertLanguageInterface 'uk', 'disable ability to send messages to clients', 'вимкнути можливість надсилання повідомлень клієнтам', 'messages';

exec insertLanguageInterface 'en', 'allow editing potential in agreement', 'allow editing potential in agreement', 'messages';
exec insertLanguageInterface 'ru', 'allow editing potential in agreement', 'разрешить редактирование потенциала в договорённости', 'messages';
exec insertLanguageInterface 'uk', 'allow editing potential in agreement', 'дозволити редагування потенціалу в домовленості', 'messages';

exec insertLanguageInterface 'en', 'disable ability to send email messages to clients', 'disable ability to send email messages to clients', 'messages';
exec insertLanguageInterface 'ru', 'disable ability to send email messages to clients', 'отключить возможность отправки email сообщений клиентам', 'messages';
exec insertLanguageInterface 'uk', 'disable ability to send emailagreement messages to clients', 'вимкнути можливість надсилання email повідомлень клієнтам', 'messages';

exec insertLanguageInterface 'en', 'disable ability to send sms messages to clients', 'disable ability to send sms messages to clients', 'messages';
exec insertLanguageInterface 'ru', 'disable ability to send sms messages to clients', 'отключить возможность отправки sms сообщений клиентам', 'messages';
exec insertLanguageInterface 'uk', 'disable ability to send sms messages to clients', 'вимкнути можливість надсилання sms повідомлень клієнтам', 'messages';

exec insertLanguageInterface 'en', 'disable ability to send viber messages to clients', 'disable ability to send viber messages to clients', 'messages';
exec insertLanguageInterface 'ru', 'disable ability to send viber messages to clients', 'отключить возможность отправки viber сообщений клиентам', 'messages';
exec insertLanguageInterface 'uk', 'disable ability to send viber messages to clients', 'вимкнути можливість надсилання viber повідомлень клієнтам', 'messages';
GO
----------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------
-- https://teamsoft.atlassian.net/browse/DRREDDYSMM-152
------------------------------------------------------------------------------------------------------------------------
IF NOT EXISTS (SELECT 1 FROM information_schema.columns WHERE table_name = 'info_dcrtyperoleworkflow' AND column_name = 'chain_type')
BEGIN
ALTER TABLE info_dcrtyperoleworkflow ADD chain_type varchar(255) NULL
END
GO

IF NOT EXISTS (SELECT 1 FROM information_schema.columns WHERE table_name = 'info_dcrtyperoleworkflow' AND column_name = 'chain_num')
BEGIN
ALTER TABLE info_dcrtyperoleworkflow ADD chain_num INT NULL
END
GO

IF NOT EXISTS (SELECT 1 FROM information_schema.columns WHERE table_name = 'info_companyrequest' AND column_name = 'chain_num')
BEGIN
ALTER TABLE info_companyrequest ADD chain_num INT NULL
END
GO

IF NOT EXISTS(SELECT 1 FROM information_schema.columns WHERE table_name = 'info_companyrequest' and column_name = 'user_id')
BEGIN
ALTER TABLE info_companyrequest ADD user_id INT NULL;
ALTER TABLE info_companyrequest WITH CHECK ADD CONSTRAINT FK_info_companyrequest_user_id FOREIGN KEY (user_id) REFERENCES info_user (id);
END
GO

IF NOT EXISTS (SELECT 1 FROM information_schema.columns WHERE table_name = 'info_contactrequest' AND column_name = 'chain_num')
BEGIN
ALTER TABLE info_contactrequest ADD chain_num INT NULL
END
GO

IF NOT EXISTS(SELECT 1 FROM information_schema.columns WHERE table_name = 'info_contactrequest' and column_name = 'user_id')
BEGIN
ALTER TABLE info_contactrequest ADD user_id INT NULL;
ALTER TABLE info_contactrequest WITH CHECK ADD CONSTRAINT FK_info_contactrequest_user_id FOREIGN KEY (user_id) REFERENCES info_user (id);
END
GO
------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------
-- https://teamsoft.atlassian.net/browse/FARMAK-354
------------------------------------------------------------------------------------------------------------------------
if not exists(SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'info_filterdictionary' AND column_Name = 'subtitle')
begin
alter table info_filterdictionary add subtitle varchar(255) null
END;
GO

if not exists(SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'info_filterdictionary' AND column_Name = 'additional_dict')
begin
alter table info_filterdictionary add additional_dict varchar(255) null
END;
GO

if not exists(SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'info_filterdictionary' AND column_Name = 'depend_field')
begin
alter table info_filterdictionary add depend_field varchar(255) null
END;
GO
------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------
-- https://teamsoft.atlassian.net/browse/PHARMAWEB-488
------------------------------------------------------------------------------------------------------------------------
IF NOT EXISTS(SELECT 1 FROM information_schema.columns WHERE table_name = 'po_dictionary' and column_name = 'customdictionary_id')
begin
ALTER TABLE po_dictionary ADD customdictionary_id INT NULL;
ALTER TABLE po_dictionary WITH CHECK ADD CONSTRAINT FK_po_dictionary_customdictionary_id FOREIGN KEY (customdictionary_id) REFERENCES info_customdictionaryvalue (id);
END;
GO
------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------
-- https://teamsoft.atlassian.net/browse/CRM-100
------------------------------------------------------------------------------------------------------------------------
IF OBJECT_ID('info_entitymapparamjoin', 'U') IS NOT NULL
BEGIN
ALTER TABLE info_entitymapparamjoin DROP CONSTRAINT FK_info_entitymapparamjoin_entitymap_id;
ALTER TABLE info_entitymapparamjoin DROP CONSTRAINT FK_info_entitymapparamjoin_entitymapparam_id;
ALTER TABLE info_entitymapparamjoin ADD CONSTRAINT FK_info_entitymapparamjoin_entitymap_id FOREIGN KEY (entitymap_id) REFERENCES info_entitymap (id) ON DELETE CASCADE;
ALTER TABLE info_entitymapparamjoin ADD CONSTRAINT FK_info_entitymapparamjoin_entitymapparam_id FOREIGN KEY (entitymapparam_id) REFERENCES info_entitymapparam (id) ON DELETE CASCADE;
END
GO
------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------
--https://teamsoft.atlassian.net/browse/UKEKSPO-75
------------------------------------------------------------------------------------------------------------------------
IF NOT EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE table_name = 'info_mcmcontentlanguage' AND column_name = 'json')
ALTER TABLE [dbo].[info_mcmcontentlanguage] ADD json varchar(max) NULL
    GO

    exec insertLanguageInterface 'en', 'Content language', N'Content language', 'mcm';
exec insertLanguageInterface 'ru', 'Content language', N'Язык контента', 'mcm';
exec insertLanguageInterface 'uk', 'Content language', N'Мова контенту', 'mcm';
GO
------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------
------- https://teamsoft.atlassian.net/browse/REDDYSRSA-246
------------------------------------------------------------------------------------------------------------------------
exec insertLanguageInterface 'en', 'Link to image', 'Link to image', 'mcm';
exec insertLanguageInterface 'ru', 'Link to image', 'Ссылка на изображение', 'mcm';
exec insertLanguageInterface 'uk', 'Link to image', 'Посилання на зображення', 'mcm';
GO

exec insertLanguageInterface 'en', 'Download', 'Download', 'mcm';
exec insertLanguageInterface 'ru', 'Download', 'Загрузить', 'mcm';
exec insertLanguageInterface 'uk', 'Download', 'Завантажити', 'mcm';
GO
------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------------------------
-- https://teamsoft.atlassian.net/browse/ARTERIUM-167
---------------------------------------------------------------------------------------------------------------------
exec insertLanguageInterface 'en','Clear','Clear','ag-grid';
exec insertLanguageInterface 'ru','Clear','Очистить','ag-grid';
exec insertLanguageInterface 'uk','Clear','Очистити','ag-grid';

exec insertLanguageInterface 'en','Select a file','Select a file','messages';
exec insertLanguageInterface 'ru','Select a file','Выбрать файл','messages';
exec insertLanguageInterface 'uk','Select a file','Вибрати файл','messages';
GO
---------------------------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------------------------

------------------------------------------------------------------------------------------------------------------------
-- https://teamsoft.atlassian.net/browse/PHARMAWEB-632
------------------------------------------------------------------------------------------------------------------------
exec insertLanguageInterface 'en', 'Remove report', 'Remove report', 'crm';
exec insertLanguageInterface 'ru', 'Remove report', 'Удалить отчет', 'crm';
exec insertLanguageInterface 'uk', 'Remove report', 'Видалити звіт', 'crm';

exec insertLanguageInterface 'en', 'Are you sure you want to delete the report?', 'Are you sure you want to delete the report?', 'crm';
exec insertLanguageInterface 'ru', 'Are you sure you want to delete the report?', 'Вы уверены, что хотите удалить отчет?', 'crm';
exec insertLanguageInterface 'uk', 'Are you sure you want to delete the report?', 'Ви впевнені, що хочете видалити звіт?', 'crm';
GO
------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------
 -- fix translations
------------------------------------------------------------------------------------------------------------------------
exec insertLanguageInterface 'en', 'Copy filter', N'Copy filter', 'crm';
exec insertLanguageInterface 'ru', 'Copy filter', N'Копировать фильтр', 'crm';
exec insertLanguageInterface 'uk', 'Copy filter', N'Копіювати фільтр', 'crm';
GO

delete from info_languageinterface where [key]='District' and [domain]='messages';
exec insertLanguageInterface 'en', 'District', N'District', 'messages';
exec insertLanguageInterface 'ru', 'District', N'Округ', 'messages';
exec insertLanguageInterface 'uk', 'District', N'Округ', 'messages';
GO

delete from info_languageinterface where [key]='TAX number' and [domain]='messages';
exec insertLanguageInterface 'en', 'Tax number', N'Tax number', 'messages';
exec insertLanguageInterface 'ru', 'Tax number', N'Налоговый номер', 'messages';
exec insertLanguageInterface 'uk', 'Tax number', N'Податковий номер', 'messages';
GO
------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------------------------------------------------------------------------------
------- https://teamsoft.atlassian.net/browse/PHARMAWEB-591
-------------------------------------------------------------------------------------------------------------------------------------------------------------------
IF NOT EXISTS (
    SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS
    WHERE TABLE_NAME = 'info_user' AND COLUMN_NAME = 'po_password_modified'
)
BEGIN
ALTER TABLE info_user ADD po_password_modified DATETIME;
END
-------------------------------------------------------------------------------------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------------------------------------------------------------------------------

