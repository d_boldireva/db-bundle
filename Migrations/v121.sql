------------------------------------------------------------------------------------------------------------------------
---https://teamsoft.atlassian.net/browse/IZVARINO-140
------------------------------------------------------------------------------------------------------------------------
IF OBJECT_ID(N'info_contactblog', 'U') IS NULL
    BEGIN
        create table info_contactblog(
                                         [id] int identity(1,1)  not null
            , [contact_id] int  NOT NULL
            , [name] varchar (50) null
            -- https://stackoverflow.com/questions/2726649/the-data-types-text-and-nvarchar-are-incompatible-in-the-equal-to-operator
            , [link] VARCHAR(MAX) not null
            , [subscribers] int null
            , currenttime datetime         null default (CURRENT_TIMESTAMP)
            , moduser     varchar(16)      null default ([dbo].[Get_CurrentCode]())
            , guid        uniqueidentifier null default (newid())
            , CONSTRAINT [PK_info_contactblog] PRIMARY KEY (id) ON [PRIMARY]
        )

        ALTER TABLE info_contactblog
            ADD CONSTRAINT FK_info_contactblog_info_contact FOREIGN KEY (contact_id)
                REFERENCES info_contact (id)
    END

IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'info_contact' AND COLUMN_NAME = 'legal_status')
    BEGIN
        ALTER TABLE info_contact ADD legal_status int null

        ALTER TABLE info_contact
            ADD CONSTRAINT fk_info_contact_legal_status_info_customdictionaryvalue FOREIGN KEY (legal_status)
                REFERENCES info_CustomDictionaryValue (id)
    END

IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'info_contact' AND COLUMN_NAME = 'interaction')
    BEGIN
        ALTER TABLE info_contact ADD interaction int null

        ALTER TABLE info_contact
            ADD CONSTRAINT fk_info_contact_interaction_info_customdictionaryvalue FOREIGN KEY (interaction)
                REFERENCES info_CustomDictionaryValue (id)
    END

IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'info_contact' AND COLUMN_NAME = 'legal_name')
    BEGIN
        ALTER TABLE info_contact ADD legal_name varchar (255) null
    END

IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'info_contact' AND COLUMN_NAME = 'passport_seriesnumber')
    BEGIN
        ALTER TABLE info_contact ADD passport_seriesnumber varchar (255) null
    END

IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'info_contact' AND COLUMN_NAME = 'passport_date')
    BEGIN
        ALTER TABLE info_contact ADD passport_date date null
    END

IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'info_contact' AND COLUMN_NAME = 'passport_issued')
    BEGIN
        ALTER TABLE info_contact ADD passport_issued varchar (255) null
    END

IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'info_contact' AND COLUMN_NAME = 'regaddress')
    BEGIN
        ALTER TABLE info_contact ADD regaddress varchar (255) null
    END

IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'info_contact' AND COLUMN_NAME = 'insurance_certificate')
    BEGIN
        ALTER TABLE info_contact ADD insurance_certificate varchar (255) null
    END

IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'info_contact' AND COLUMN_NAME = 'identification_number')
    BEGIN
        ALTER TABLE info_contact ADD identification_number varchar (255) null
    END

IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'info_contact' AND COLUMN_NAME = 'checking_account')
    BEGIN
        ALTER TABLE info_contact ADD checking_account varchar (255) null
    END

IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'info_contact' AND COLUMN_NAME = 'bank')
    BEGIN
        ALTER TABLE info_contact ADD bank varchar (255) null
    END

IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'info_contact' AND COLUMN_NAME = 'bic')
    BEGIN
        ALTER TABLE info_contact ADD bic varchar (255) null
    END

IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'info_contact' AND COLUMN_NAME = 'correspondent_account')
    BEGIN
        ALTER TABLE info_contact ADD correspondent_account varchar (255) null
    END

IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'info_contact' AND COLUMN_NAME = 'snils')
    BEGIN
        ALTER TABLE info_contact ADD snils varchar (255) null
    END

IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'info_contact' AND COLUMN_NAME = 'kpp')
    BEGIN
        ALTER TABLE info_contact ADD kpp varchar (255) null
    END

IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'info_contacttype' AND COLUMN_NAME = 'isblogger')
    BEGIN
        ALTER TABLE info_contacttype ADD isblogger integer null
    END

IF NOT EXISTS (SELECT 1 FROM info_dictionaryidentifier WHERE [tablename] = 'info_contacttype' AND [columnname] = 'isblogger')
    BEGIN
        DECLARE @language_en as int
        DECLARE @language_ru as int
        DECLARE @language_uk as int

        SELECT @language_en = id FROM info_language WHERE slug = 'en'
        SELECT @language_ru = id FROM info_language WHERE slug = 'ru'
        SELECT @language_uk = id FROM info_language WHERE slug = 'uk'

        INSERT INTO info_dictionaryidentifier (tablename, columnname, [value], identifier, hidden, order_num, required, language_id) VALUES
        ('info_contacttype', 'isblogger', 'Blogger', 0, 0, 1, 0, @language_en),
        ('info_contacttype', 'isblogger', N'Блогер', 0, 0, 1, 0, @language_ru),
        ('info_contacttype', 'isblogger', N'Блогер', 0, 0, 1, 0, @language_uk)
    END
GO

IF EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'insertPoEntityFields') AND xtype IN (N'P'))
    BEGIN
        DROP PROCEDURE insertPoEntityFields
    END
GO

CREATE PROCEDURE insertPoEntityFields(@page INT, @field VARCHAR(255), @name VARCHAR(255), @group VARCHAR(255) = null)
AS
BEGIN
    IF NOT EXISTS(SELECT 1 FROM po_entityfields WHERE page=@page AND field= @field AND name=@name)
        BEGIN
            INSERT INTO po_entityfields(page, field, name, [group]) VALUES (@page, @field, @name, @group);
        END
END
GO

EXEC insertPoEntityFields 2, 'legal_status', N'Legal status'
EXEC insertPoEntityFields 2, 'interaction', N'Interaction'
EXEC insertPoEntityFields 2, 'legal_name', N'Legal Name'
EXEC insertPoEntityFields 2, 'passport_seriesnumber', N'Series', 'passport'
EXEC insertPoEntityFields 2, 'passport_date', N'When issued', 'passport'
EXEC insertPoEntityFields 2, 'passport_issued', N'Issued by', 'passport'
EXEC insertPoEntityFields 2, 'regaddress', N'Registration address'
EXEC insertPoEntityFields 2, 'insurance_certificate', N'Insurance certificate'
EXEC insertPoEntityFields 2, 'identification_number', N'Taxpayer identification number'
EXEC insertPoEntityFields 2, 'bank', N'Bank'
EXEC insertPoEntityFields 2, 'bic', N'Bank identification code (BIC)'
EXEC insertPoEntityFields 2, 'correspondent_account', N'Correspondent account'
-- only for RF
EXEC insertPoEntityFields 2, 'snils', N'СНИЛС'
EXEC insertPoEntityFields 2, 'kpp', N'КПП'
GO

exec insertLanguageInterface 'en', 'Blogs', 'Blogs', 'crm'
exec insertLanguageInterface 'ru', 'Blogs', N'Блоги', 'crm'
exec insertLanguageInterface 'uk', 'Blogs', N'Блоги', 'crm'
exec insertLanguageInterface 'en', 'Add blog', 'Add blog', 'crm'
exec insertLanguageInterface 'ru', 'Add blog', N'Добавить блог', 'crm'
exec insertLanguageInterface 'uk', 'Add blog', N'Додати блог', 'crm'
exec insertLanguageInterface 'en', 'Edit blog', 'Edit blog', 'crm'
exec insertLanguageInterface 'ru', 'Edit blog', N'Редактировать блог', 'crm'
exec insertLanguageInterface 'uk', 'Edit blog', N'Редагувати блог', 'crm'
exec insertLanguageInterface 'en', 'Link is duplicated', 'Link is duplicated', 'crm'
exec insertLanguageInterface 'ru', 'Link is duplicated', N'Ссылка дублируется', 'crm'
exec insertLanguageInterface 'uk', 'Link is duplicated', N'Посилання повторюється', 'crm'
exec insertLanguageInterface 'en', 'Subscribers', 'Subscribers', 'crm'
exec insertLanguageInterface 'ru', 'Subscribers', N'Подписчики', 'crm'
exec insertLanguageInterface 'uk', 'Subscribers', N'Підписчики', 'crm'
exec insertLanguageInterface 'en', 'Open in new tab', 'Open in new tab', 'crm'
exec insertLanguageInterface 'ru', 'Open in new tab', N'Открыть в новой вкладке', 'crm'
exec insertLanguageInterface 'uk', 'Open in new tab', N'Відкрити в новій вкладці', 'crm'
exec insertLanguageInterface 'en', 'Link', 'Link', 'crm'
exec insertLanguageInterface 'ru', 'Link', N'Ссылка', 'crm'
exec insertLanguageInterface 'uk', 'Link', N'Посилання', 'crm'
exec insertLanguageInterface 'en', 'KPP', 'KPP', 'crm'
exec insertLanguageInterface 'ru', 'KPP', N'КПП', 'crm'
exec insertLanguageInterface 'uk', 'KPP', N'КПП', 'crm'
exec insertLanguageInterface 'en', 'SNILS', 'SNILS', 'crm'
exec insertLanguageInterface 'ru', 'SNILS', N'СНИЛС', 'crm'
exec insertLanguageInterface 'uk', 'SNILS', N'СНІЛС', 'crm'
exec insertLanguageInterface 'en', 'Correspondent account', 'Correspondent account', 'crm'
exec insertLanguageInterface 'ru', 'Correspondent account', N'Корреспондентский счет', 'crm'
exec insertLanguageInterface 'uk', 'Correspondent account', N'Кореспондентський рахунок', 'crm'
exec insertLanguageInterface 'en', 'Bank identification code (BIC)', 'Bank identification code (BIC)', 'crm'
exec insertLanguageInterface 'ru', 'Bank identification code (BIC)', N'Идентификационный код банка (BIC)', 'crm'
exec insertLanguageInterface 'uk', 'Bank identification code (BIC)', N'Ідентифікаційний код банку (BIC)', 'crm'
exec insertLanguageInterface 'en', 'Bank', 'Bank', 'crm'
exec insertLanguageInterface 'ru', 'Bank', N'Банк', 'crm'
exec insertLanguageInterface 'uk', 'Bank', N'Банк', 'crm'
exec insertLanguageInterface 'en', 'Taxpayer identification number', 'Taxpayer identification number', 'crm'
exec insertLanguageInterface 'ru', 'Taxpayer identification number', N'Идентификационный номер налогоплательщика', 'crm'
exec insertLanguageInterface 'uk', 'Taxpayer identification number', N'Ідентифікаційний номер платника податків', 'crm'
exec insertLanguageInterface 'en', 'Insurance certificate', 'Insurance certificate', 'crm'
exec insertLanguageInterface 'ru', 'Insurance certificate', N'Страховой сертификат', 'crm'
exec insertLanguageInterface 'uk', 'Insurance certificate', N'Страховий сертифікат', 'crm'
exec insertLanguageInterface 'en', 'Registration address', 'Registration address', 'crm'
exec insertLanguageInterface 'ru', 'Registration address', N'Адрес регистрации', 'crm'
exec insertLanguageInterface 'uk', 'Registration address', N'Адреса реєстрації', 'crm'
exec insertLanguageInterface 'en', 'Legal name', 'Legal name', 'crm'
exec insertLanguageInterface 'ru', 'Legal name', N'Юридическое название', 'crm'
exec insertLanguageInterface 'uk', 'Legal name', N'Юридична назва', 'crm'
exec insertLanguageInterface 'en', 'Interaction', 'Interaction', 'crm'
exec insertLanguageInterface 'ru', 'Interaction', N'Взаимодействие', 'crm'
exec insertLanguageInterface 'uk', 'Interaction', N'Взаємодія', 'crm'
exec insertLanguageInterface 'en', 'Legal status', 'Legal status', 'crm'
exec insertLanguageInterface 'ru', 'Legal status', N'Правовой статус', 'crm'
exec insertLanguageInterface 'uk', 'Legal status', N'Правовий статус', 'crm'
exec insertLanguageInterface 'en', 'Passport', 'Passport', 'crm'
exec insertLanguageInterface 'ru', 'Passport', N'Паспорт', 'crm'
exec insertLanguageInterface 'uk', 'Passport', N'Паспорт', 'crm'
exec insertLanguageInterface 'en', 'Series number', 'Series number', 'crm'
exec insertLanguageInterface 'ru', 'Series number', N'Номер серии', 'crm'
exec insertLanguageInterface 'uk', 'Series number', N'Номер серії', 'crm'
exec insertLanguageInterface 'en', 'When issued', 'When issued', 'crm'
exec insertLanguageInterface 'ru', 'When issued', N'Когда выдан', 'crm'
exec insertLanguageInterface 'uk', 'When issued', N'Коли виданий', 'crm'
exec insertLanguageInterface 'en', 'Issued by', 'Issued by', 'crm'
exec insertLanguageInterface 'ru', 'Issued by', N'Кем выдан', 'crm'
exec insertLanguageInterface 'uk', 'Issued by', N'Ким видано', 'crm'
GO


------------------------------------------------------------------------------------------------------------------------
-- add missing privileges to admin role
----------------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------------
IF NOT EXISTS (SELECT 1 FROM info_roleprivilege WHERE role_id = (SELECT id FROM info_role WHERE code='ROLE_ADMIN' AND privilege_id IN (SELECT id FROM info_Serviceprivilege WHERE code LIKE 'ADMIN_ACCESS_TO%')))
INSERT INTO info_roleprivilege(privilege_id, role_id)
SELECT id, (SELECT id FROM info_role WHERE code='ROLE_ADMIN') FROM info_serviceprivilege WHERE CODE LIKE 'ADMIN_ACCESS_TO%'

------------------------------------------------------------------------------------------------------------------------
---https://teamsoft.atlassian.net/browse/MCM-354
------------------------------------------------------------------------------------------------------------------------
exec insertLanguageInterface  'en', 'Sort by newest', N'Sort by newest', 'messages'
exec insertLanguageInterface  'ru', 'Sort by newest', N'Сортировать по самым новым', 'messages'
exec insertLanguageInterface  'uk', 'Sort by newest', N'Сортувати за найновішими', 'messages'
GO

exec insertLanguageInterface  'en', 'Sort by oldest', N'Sort by oldest', 'messages'
exec insertLanguageInterface  'ru', 'Sort by oldest', N'Сортировать по cамым старым', 'messages'
exec insertLanguageInterface  'uk', 'Sort by oldest', N'Сортувати за найстарішими', 'messages'
GO

exec insertLanguageInterface  'en', 'Sort by name', N'Sort by name', 'messages'
exec insertLanguageInterface  'ru', 'Sort by name', N'Сортировать по названию', 'messages'
exec insertLanguageInterface  'uk', 'Sort by name', N'Сортувати за назвою', 'messages'
GO
------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------

--- https://teamsoft.atlassian.net/browse/EGISRU-214
------------------------------------------------------------------------------------------------------------------------

--delete from po_gridconfig where id not in (select id from (select max(id) id, subpage, page, field_code from po_gridconfig GROUP BY subpage, page, field_code) t)

IF NOT EXISTS(SELECT * FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'po_gridconfig' AND COLUMN_NAME = 'field_name')
    BEGIN
        ALTER TABLE po_gridconfig ADD field_name VARCHAR(255) NULL
    END
GO

delete from po_gridconfig  where field_code = 'tasks';
insert into po_gridconfig
(page, subpage, field_code, field_name, is_shown)
values
(100, 1, 'tasks0', 'Visits for quarter', 1),
(100, 2, 'tasks0', 'Visits for quarter', 1),
(100, 1, 'tasks1', 'Visits for %PERIOD%', 1),
(100, 2, 'tasks1', 'Visits for %PERIOD%', 1),
(100, 1, 'tasks2', 'Visits for %PERIOD%', 1),
(100, 2, 'tasks2', 'Visits for %PERIOD%', 1),
(100, 1, 'tasks3', 'Visits for %PERIOD%', 1),
(100, 2, 'tasks3', 'Visits for %PERIOD%', 1)

----- Common -------
update po_gridconfig set field_name = 'Status Calc.' where page = 100 AND field_code = 'statusRasch';
update po_gridconfig set field_name = 'Status Appr.' where page = 100 AND field_code = 'statusUtv';
update po_gridconfig set field_name = 'Change' where page = 100 AND field_code = 'isChanged';
update po_gridconfig set field_name = 'Status' where page = 100 AND field_code = 'status';
update po_gridconfig set field_name = 'Cat. Calc.' where page = 100 AND field_code = 'categoryName';
update po_gridconfig set field_name = 'Cat. Appr.' where page = 100 AND field_code = 'approvedCategory';
update po_gridconfig set field_name = 'Segment' where page = 100 AND field_code = 'segmentEng';
update po_gridconfig set field_name = 'Calls per month by plan' where page = 100 AND field_code = 'plancnt';
update po_gridconfig set field_name = 'In another user''s database' where page = 100 AND field_code = 'others';
update po_gridconfig set field_name = 'Removal reason' where page = 100 AND field_code = 'removalReason';
update po_gridconfig set field_name = 'Comment' where page = 100 AND field_code = 'comment';
update po_gridconfig set field_name = 'Rejection reason' where page = 100 AND field_code = 'rejectionReason';
--------------------------
----- Page = 1 -------
update po_gridconfig set field_name = 'Institution name' where page = 100 AND subpage = 1 AND field_code = 'name';
update po_gridconfig set field_name = 'Network' where page = 100 AND subpage = 1 AND field_code = 'net';
update po_gridconfig set field_name = 'City' where page = 100 AND subpage = 1 AND field_code = 'cityName';
update po_gridconfig set field_name = 'Address' where page = 100 AND subpage = 1 AND field_code = 'address';
update po_gridconfig set field_name = 'Turnover' where page = 100 AND subpage = 1 AND field_code = 'tovarooborot';
update po_gridconfig set field_name = 'Sales/turnover' where page = 100 AND subpage = 1 AND field_code = 'salePerTovarooborot';
update po_gridconfig set field_name = 'Sales' where page = 100 AND subpage = 1 AND field_code = 'sales';
update po_gridconfig set field_name = 'Critery 1' where page = 100 AND subpage = 1 AND field_code = 'critery1';
update po_gridconfig set field_name = 'Critery 2' where page = 100 AND subpage = 1 AND field_code = 'critery2';
update po_gridconfig set field_name = 'Critery 3' where page = 100 AND subpage = 1 AND field_code = 'critery3';
update po_gridconfig set field_name = 'Arrangement' where page = 100 AND subpage = 1 AND field_code = 'arrangement';
----------------------------
----- Page = 2 -------
update po_gridconfig set field_name = 'Full name' where page = 100 AND subpage = 2 AND field_code = 'name';
update po_gridconfig set field_name = 'Target-group' where page = 100 AND subpage = 2 AND field_code = 'targetName';
update po_gridconfig set field_name = 'City, place of work' where page = 100 AND subpage = 2 AND field_code = 'companyName';
update po_gridconfig set field_name = 'Patient flow' where page = 100 AND subpage = 2 AND field_code = 'patientcount';
update po_gridconfig set field_name = 'Potency.' where page = 100 AND subpage = 2 AND field_code = 'potential';
update po_gridconfig set field_name = 'Fact/potency.' where page = 100 AND subpage = 2 AND field_code = 'factPotential';
update po_gridconfig set field_name = 'NSR rate' where page = 100 AND subpage = 2 AND field_code = 'nsrRate';
update po_gridconfig set field_name = 'Unrealized potential rating' where page = 100 AND subpage = 2 AND field_code = 'potentialRate';

exec insertLanguageInterface 'en', 'Sales/turnover', 'Sales/turnover', 'targeting';
exec insertLanguageInterface 'ru', 'Sales/turnover', 'Продажи/товарооборот', 'targeting';
exec insertLanguageInterface 'uk', 'Sales/turnover', 'Продажі/товарообіг', 'targeting';
GO

exec insertLanguageInterface 'en', 'Calls by plan', 'Calls by plan', 'targeting';
exec insertLanguageInterface 'ru', 'Calls by plan', 'Частота по стратегии', 'targeting';
exec insertLanguageInterface 'uk', 'Calls by plan', 'Частота по стратегії', 'targeting';
GO

exec insertLanguageInterface 'en', 'Critery 1', 'Critery 1', 'targeting';
exec insertLanguageInterface 'ru', 'Critery 1', 'Критерий 1', 'targeting';
exec insertLanguageInterface 'uk', 'Critery 1', 'Критерій 1', 'targeting';
GO

exec insertLanguageInterface 'en', 'Critery 2', 'Critery 2', 'targeting';
exec insertLanguageInterface 'ru', 'Critery 2', 'Критерий 2', 'targeting';
exec insertLanguageInterface 'uk', 'Critery 2', 'Критерій 2', 'targeting';
GO

exec insertLanguageInterface 'en', 'Critery 3', 'Critery 3', 'targeting';
exec insertLanguageInterface 'ru', 'Critery 3', 'Критерий 3', 'targeting';
exec insertLanguageInterface 'uk', 'Critery 3', 'Критерій 3', 'targeting';
GO

exec insertLanguageInterface 'en', 'Visits for quarter', 'Visits for quarter', 'targeting';
exec insertLanguageInterface 'ru', 'Visits for quarter', 'Частота за квартал', 'targeting';
exec insertLanguageInterface 'uk', 'Visits for quarter', 'Частота за квартал', 'targeting';
GO

exec insertLanguageInterface 'en', 'Visits for %PERIOD%', 'Visits for %PERIOD%', 'targeting';
exec insertLanguageInterface 'ru', 'Visits for %PERIOD%', 'Частота за %PERIOD%', 'targeting';
exec insertLanguageInterface 'uk', 'Visits for %PERIOD%', 'Частота за %PERIOD%', 'targeting';
GO

exec insertLanguageInterface 'en', 'City, place of work', 'City, place of work', 'targeting';
exec insertLanguageInterface 'ru', 'City, place of work', 'Город, место работы', 'targeting';
exec insertLanguageInterface 'uk', 'City, place of work', 'Місто, місце роботи', 'targeting';
GO

exec insertLanguageInterface 'en', 'NSR rate', 'NSR rate', 'targeting';
exec insertLanguageInterface 'ru', 'NSR rate', 'HCP рейтинг', 'targeting';
exec insertLanguageInterface 'uk', 'NSR rate', 'HCP рейтинг', 'targeting';
GO

exec insertLanguageInterface 'en', 'Potential rate', 'Potential rate', 'targeting';
exec insertLanguageInterface 'ru', 'Potential rate', 'Нереализованный рейтинг', 'targeting';
exec insertLanguageInterface 'uk', 'Potential rate', 'Нереалізований рейтинг', 'targeting';
GO

exec insertLanguageInterface 'en', 'Validation', 'Validation', 'targeting';
exec insertLanguageInterface 'ru', 'Validation', 'Валидация', 'targeting';
exec insertLanguageInterface 'uk', 'Validation', 'Валідація', 'targeting';
GO

exec insertLanguageInterface 'en', 'did not pass', 'did not pass', 'targeting';
exec insertLanguageInterface 'ru', 'did not pass', 'не прошли', 'targeting';
exec insertLanguageInterface 'uk', 'did not pass', 'не пройшли', 'targeting';
GO

exec insertLanguageInterface 'en', 'the lower limit of the number of doctors', 'the lower limit of the number of doctors', 'targeting';
exec insertLanguageInterface 'ru', 'the lower limit of the number of doctors', 'по нижней границе количества врачей', 'targeting';
exec insertLanguageInterface 'uk', 'the lower limit of the number of doctors', 'по нижній межі килькості лікарів', 'targeting';
GO

exec insertLanguageInterface 'en', 'the upper limit of the number of doctors', 'the upper limit of the number of doctors', 'targeting';
exec insertLanguageInterface 'ru', 'the upper limit of the number of doctors', 'по верхней границе количества врачей', 'targeting';
exec insertLanguageInterface 'uk', 'the upper limit of the number of doctors', 'по верхній межі килькості лікарів', 'targeting';
GO

exec insertLanguageInterface 'en', 'the lower limit of the number of visits', 'the lower limit of the number of doctors', 'targeting';
exec insertLanguageInterface 'ru', 'the lower limit of the number of visits', 'по нижней границе количества визитов', 'targeting';
exec insertLanguageInterface 'uk', 'the lower limit of the number of visits', 'по нижній межі килькості візитів', 'targeting';
GO

exec insertLanguageInterface 'en', 'the upper limit of the number of visits', 'the upper limit of the number of doctors', 'targeting';
exec insertLanguageInterface 'ru', 'the upper limit of the number of visits', 'по верхней границе количества визитов', 'targeting';
exec insertLanguageInterface 'uk', 'the upper limit of the number of visits', 'по верхній межі килькості візитів', 'targeting';
GO

update po_gridconfig set field_name = 'Calls by plan' where page = 100 AND field_code = 'plancnt';
GO

IF NOT EXISTS(SELECT * FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'info_plancontacttask' AND COLUMN_NAME = 'target_id')
    BEGIN
        ALTER TABLE info_plancontacttask ADD target_id integer;
		ALTER TABLE info_plancontacttask ADD CONSTRAINT FK_info_plancontacttask_target_id FOREIGN KEY(target_id) REFERENCES info_target (id);
    END
GO
IF NOT EXISTS(SELECT * FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'info_plancontacttask' AND COLUMN_NAME = 'visits_bottom_delta')
    BEGIN
        ALTER TABLE info_plancontacttask ADD visits_bottom_delta integer;
    END
GO
IF NOT EXISTS(SELECT * FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'info_plancontacttask' AND COLUMN_NAME = 'visits_upper_delta')
    BEGIN
        ALTER TABLE info_plancontacttask ADD visits_upper_delta integer;
    END
GO
IF NOT EXISTS(SELECT * FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'info_plancontacttask' AND COLUMN_NAME = 'contact_cnt_bottom_delta')
    BEGIN
        ALTER TABLE info_plancontacttask ADD contact_cnt_bottom_delta integer;
    END
GO
IF NOT EXISTS(SELECT * FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'info_plancontacttask' AND COLUMN_NAME = 'contact_cnt_upper_delta')
    BEGIN
        ALTER TABLE info_plancontacttask ADD contact_cnt_upper_delta integer;
    END
GO
IF NOT EXISTS(SELECT * FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'info_plancompanytask' AND COLUMN_NAME = 'target_id')
    BEGIN
        ALTER TABLE info_plancompanytask ADD target_id integer;
    END
GO

IF EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'targeting_check_rules_delta') AND xtype IN (N'FN', N'IF', N'TF'))
    DROP FUNCTION targeting_check_rules_delta
GO

CREATE FUNCTION targeting_check_rules_delta
(
  @user_id INT,
  @dt DATETIME
)
RETURNS TABLE
AS
RETURN
(
  select 1 as dummy from info_user where 1 = 0
)
GO
------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------
---https://teamsoft.atlassian.net/browse/ACINO-193
------------------------------------------------------------------------------------------------------------------------
IF NOT EXISTS (SELECT 1 FROM information_schema.columns
                WHERE table_name = 'info_mcmdistribution' AND column_name = 'for_site')
    BEGIN
        ALTER TABLE info_mcmdistribution ADD for_site INT NULL
    END
GO

IF NOT EXISTS (SELECT 1 FROM information_schema.columns
                WHERE table_name = 'info_contactphone' AND column_name = 'from_site')
    BEGIN
        ALTER TABLE info_contactphone ADD from_site INT NULL
    END
GO

exec insertLanguageInterface 'en', 'For site', N'For site', 'mcm'
exec insertLanguageInterface 'uk', 'For site', N'Для сайту', 'mcm'
exec insertLanguageInterface 'ru', 'For site', N'Для сайта', 'mcm'
GO
------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------

--- not necessary, for zero_release db
------------------------------------------------------------------------------------------------------------------------
IF NOT EXISTS(SELECT 1 FROM information_schema.columns WHERE table_name = 'info_tabcontrol' AND column_name = 'report_id')
    BEGIN
        ALTER TABLE info_tabcontrol ADD [report_id] INT NULL
        ALTER TABLE info_tabcontrol
            ADD CONSTRAINT FK_info_tabcontrol_report_id FOREIGN KEY (report_id)
                REFERENCES info_report (id)
    end
GO

-----------------------------------------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------------------------------------
INSERT INTO info_tabcontroldictionary (parent_type, type, parent_type_name, type_name)
SELECT 10, 4, 'Клиенты', 'Отчет' where not exists(select 1 from info_tabcontroldictionary where parent_type = 10 and type = 4);
INSERT INTO info_tabcontroldictionary (parent_type, type, parent_type_name, type_name)
SELECT 11, 4, 'Учреждения', 'Отчет' where not exists(select 1 from info_tabcontroldictionary where parent_type = 11 and type = 4);
-----------------------------------------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------------------------------------

------------------------------------------------------------------------------------------------------------------------
---https://teamsoft.atlassian.net/browse/DRREDDYS-443
------------------------------------------------------------------------------------------------------------------------
UPDATE info_salegranualplan SET cnt = CEILING(cnt) WHERE cnt <> CEILING(cnt);
-----------------------------------------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------------------------------------

------------------------------------------------------------------------------------------------------------------------
--- https://teamsoft.atlassian.net/browse/EGISRU-205
------------------------------------------------------------------------------------------------------------------------
IF NOT EXISTS(SELECT 1 FROM info_options WHERE [name]='choiceSurveyBeforeEdit')
    INSERT INTO info_options(name, value)
    VALUES ('choiceSurveyBeforeEdit', 0);


exec insertLanguageInterface 'en', 'SURVEY_TITLE', 'Questionnaire: {{name}}', 'crm'
exec insertLanguageInterface 'ru', 'SURVEY_TITLE', N'Анкета: {{name}}', 'crm'
exec insertLanguageInterface 'uk', 'SURVEY_TITLE', N'Анкета: {{name}}', 'crm'
GO
------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------

------------------------------------------------------------------------------------------------------------------------
---https://teamsoft.atlassian.net/browse/MCM-375
------------------------------------------------------------------------------------------------------------------------
IF NOT EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS
            WHERE TABLE_NAME = 'info_mcmtrigger' AND Column_Name = 'gmt_offset')
    BEGIN
        ALTER TABLE info_mcmtrigger
        ADD gmt_offset INT NULL
    END
GO
------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------
---https://teamsoft.atlassian.net/browse/REDDYSRSA-184
------------------------------------------------------------------------------------------------------------------------
IF OBJECT_ID(N'info_mcmdistributiondirection', 'U') is NULL
    begin
        create table info_mcmdistributiondirection(
        [id] int identity(1,1)  not null
        , [distribution_id] int  null
        , [direction_id] int  null
        , [currenttime] datetime  null CONSTRAINT [DF_info_mcmdistributiondirectioncurrenttime] DEFAULT (getdate())
        , [time] timestamp  null
        , [moduser] varchar (16) null CONSTRAINT [DF_info_mcmdistributiondirectionmoduser] DEFAULT ([dbo].[Get_CurrentCode]())
        , [guid] uniqueidentifier  null CONSTRAINT [DF_info_mcmdistributiondirectionguid] DEFAULT (newid())
        , CONSTRAINT [PK_info_mcmdistributiondirection] PRIMARY KEY (id) ON [PRIMARY]
        , CONSTRAINT [FK_info_mcmdistributiondirection_distribution_id] FOREIGN KEY ([distribution_id])
            REFERENCES info_mcmdistribution ([id]) ON UPDATE CASCADE ON DELETE CASCADE
        , CONSTRAINT [FK_info_mcmdistributiondirection_direction_id] FOREIGN KEY ([direction_id])
            REFERENCES info_direction ([id])
        )
    end
GO

if not exists (select * from dbo.sysobjects where id = object_id('del_info_mcmdistributiondirection') and OBJECTPROPERTY(id, 'IsTrigger') = 1)
exec('create trigger del_info_mcmdistributiondirection on info_mcmdistributiondirection for delete as if object_id(''empty.dbo.f_rep_del'') is null
insert into rep_del(tablename, del_guid) select ''info_mcmdistributiondirection'', guid from deleted')
GO

if not exists (select * from dbo.sysobjects where id = object_id('moduser_info_mcmdistributiondirection') and OBJECTPROPERTY(id, 'IsTrigger') = 1)
exec('create trigger moduser_info_mcmdistributiondirection on info_mcmdistributiondirection for insert, update as if not update(moduser)
update info_mcmdistributiondirection set moduser = dbo.Get_CurrentCode(), currenttime = getdate() where id in (select id from inserted)')
GO

exec insertLanguageInterface 'en', 'Product directions', N'Product directions', 'mcm'
exec insertLanguageInterface 'ru', 'Product directions', N'Продуктовые направления', 'mcm'
exec insertLanguageInterface 'uk', 'Product directions', N'Продуктовi напрямки', 'mcm'
GO
------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------
exec insertLanguageInterface 'en', 'Value must be larger or equal to {{ min }}', N'Value must be larger or equal to {{ min }}'
exec insertLanguageInterface 'ru', 'Value must be larger or equal to {{ min }}', N'Значение должно быть больше или равно {{ min }}'
exec insertLanguageInterface 'uk', 'Value must be larger or equal to {{ min }}', N'Значення має бути більшим або рівним {{ min }}'
GO

exec insertLanguageInterface 'en', 'Value must be less or equal to {{ max }}', N'Value must be less or equal to {{ max }}', 'validators'
exec insertLanguageInterface 'ru', 'Value must be less or equal to {{ max }}', N'Значение должно быть меньше или равно {{ max }}', 'validators'
exec insertLanguageInterface 'uk', 'Value must be less or equal to {{ max }}', N'Значення має бути меншим або рівним {{ max }}', 'validators'
GO

exec insertLanguageInterface 'en', 'The value must be in the range from {{ min }} to {{ max }}', N'The value must be in the range from {{ min }} to {{ max }}', 'validators'
exec insertLanguageInterface 'ru', 'The value must be in the range from {{ min }} to {{ max }}', N'Значение должно быть в диапазоне от {{ min }} до {{ max }}', 'validators'
exec insertLanguageInterface 'uk', 'The value must be in the range from {{ min }} to {{ max }}', N'Значення має знаходитися в діапазоні від {{ min }} до {{ max }}', 'validators'
GO

------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------
exec insertLanguageInterface 'en', 'Log In via SSO', N'Log In via SSO', 'messages'
exec insertLanguageInterface 'ru', 'Log In via SSO', N'Войти через SSO', 'messages'
exec insertLanguageInterface 'uk', 'Log In via SSO', N'Увійти через SSO}', 'messages'
GO



