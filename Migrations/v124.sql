------------------------------------------------------------------------------------------------------------------------
-- fix
------------------------------------------------------------------------------------------------------------------------
IF OBJECT_ID(N'[info_user_preparationbrand]', 'U') IS NULL
    BEGIN
        CREATE TABLE [dbo].[info_user_preparationbrand]
        (
            [id]              [int] IDENTITY (1,1) NOT NULL,
            [user_id]         [int]                NULL,
            [brand_id]        [int]                NULL,
            [param_id]        [int]                NULL,
            [currenttime]     [datetime]           NULL,
            [time]            [timestamp]          NULL,
            [moduser]         [varchar](16)        NULL,
            [guid]            [uniqueidentifier]   NULL,
            CONSTRAINT [PK_info_user_preparationbrand] PRIMARY KEY CLUSTERED
                (
                 [id] ASC
                    ) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
        ) ON [PRIMARY]

        ALTER TABLE [dbo].[info_user_preparationbrand]
            WITH CHECK ADD CONSTRAINT [FK_info_user_preparationbrand_user_id] FOREIGN KEY ([user_id])
            REFERENCES [dbo].[info_user] ([id])
        ALTER TABLE [dbo].[info_user_preparationbrand]
            WITH CHECK ADD CONSTRAINT [FK_info_user_preparationbrand_brand_id] FOREIGN KEY ([brand_id])
            REFERENCES [dbo].[info_preparationbrend] ([id])
        ALTER TABLE [dbo].[info_user_preparationbrand]
            WITH CHECK ADD CONSTRAINT [FK_info_user_preparationbrand_param_id] FOREIGN KEY ([param_id])
            REFERENCES [dbo].[info_paramfortask] ([id])

        ALTER TABLE [dbo].[info_user_preparationbrand]
            ADD CONSTRAINT [DF_info_user_preparationbrandcurrenttime] DEFAULT (getdate()) FOR [currenttime]
        ALTER TABLE [dbo].[info_user_preparationbrand]
            ADD CONSTRAINT [DF_info_user_preparationbrandmoduser] DEFAULT ([dbo].[Get_CurrentCode]()) FOR [moduser]
        ALTER TABLE [dbo].[info_user_preparationbrand]
            ADD CONSTRAINT [DF_info_user_preparationbrandguid] DEFAULT (newid()) FOR [guid]
    END
GO
------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------
--- https://teamsoft.atlassian.net/browse/CAMPINA-95
------------------------------------------------------------------------------------------------------------------------
IF OBJECT_ID(N'info_companysign', 'U') is NULL
    BEGIN
        create table info_companysign
        (
            id          int identity
                constraint pk_info_companysign
                    primary key,
            company_id  int,
            createdate  date,
            modified    date,

            currenttime datetime
                constraint DF_info_companysigncurrenttime default getdate(),
            moduser     varchar(16)
                constraint DF_info_companysignmoduser default [dbo].[Get_CurrentCode](),
            guid        uniqueidentifier
                constraint DF_info_companysignguid default newid()
        )

        ALTER TABLE info_companysign
            ADD CONSTRAINT fk_info_companysign_company_id FOREIGN KEY (company_id) REFERENCES info_company (id) ON UPDATE CASCADE ON DELETE CASCADE;
    END
GO

exec createCurrenttimeAndModuserTrigger N'info_companysign'
GO

IF OBJECT_ID(N'info_companysignfile', 'U') is NULL
    BEGIN
        create table info_companysignfile
        (
            id             int identity
                constraint pk_info_companysignfile
                    primary key,
            agreement      image,
            filetype       varchar(16),
            companysign_id integer,

            currenttime    datetime
                constraint DF_info_companysignfilecurrenttime default getdate(),
            moduser        varchar(16)
                constraint DF_info_companysignfilemoduser default [dbo].[Get_CurrentCode](),
            guid           uniqueidentifier
                constraint DF_info_companysignfileguid default newid()
        )

        ALTER TABLE info_companysignfile
            ADD CONSTRAINT fk_info_companysignfile_companysign_id FOREIGN KEY (companysign_id) REFERENCES info_companysign (id) ON UPDATE CASCADE ON DELETE CASCADE;
    END
GO

exec createCurrenttimeAndModuserTrigger N'info_companysignfile'
GO

exec insertPoEntityFields 1, 'company_sign', N'Signature';
GO
------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------------
--- https://teamsoft.atlassian.net/browse/PHARMAWEB-413
---------------------------------------------------------------------------------------------------------
IF NOT EXISTS(SELECT * FROM info_CustomDictionary WHERE code = 'pharmacy_network')
    BEGIN
        INSERT info_CustomDictionary (name, code) VALUES ('Pharmacy network', 'pharmacy_network');
    END
GO

IF NOT EXISTS(SELECT * FROM info_CustomDictionary WHERE code = 'phototag')
    BEGIN
        INSERT info_CustomDictionary (name, code) VALUES ('Photo tag', 'phototag');
    END
GO

IF NOT EXISTS(SELECT * FROM info_systemdictionary where tablename = 'info_companycategory')
    BEGIN
        INSERT INTO info_systemdictionary (tablename, num) VALUES ('info_companycategory', (select max(num)+1 from info_systemdictionary));
    END
GO

IF NOT EXISTS(SELECT * FROM info_systemdictionary where tablename = 'info_taskfilegroup')
    BEGIN
        INSERT INTO info_systemdictionary (tablename, num) VALUES ('info_taskfilegroup', (select max(num)+1 from info_systemdictionary));
    END
GO

IF NOT EXISTS(SELECT * FROM info_systemdictionary where tablename = 'info_photodictionary')
    BEGIN
        INSERT INTO info_systemdictionary (tablename, num) VALUES ('info_photodictionary', (select max(num)+1 from info_systemdictionary));
    END
GO

IF OBJECT_ID(N'[po_photoreportfilter]', 'U') IS NULL
    BEGIN
        CREATE TABLE [dbo].[po_photoreportfilter]
        (
            [id]                  [int] IDENTITY (1,1) NOT NULL,
            [code]                [varchar](255)       NULL,
            [name]                [varchar](255)       NULL,
            [filter]              [varchar](max)       NULL,
            [default_filter]      [varchar](max)       NULL,
            [position]            [int]                NULL,
            [field_data_type]     [int]                NULL,
            [enable]              [int]                NULL,
            [is_required]         [int]                NULL,
            [custom_where]        [varchar](max)       NULL,
            [systemdictionary_id] [int]                NULL,
            [customdictionary_id] [int]                NULL,
            [currenttime]         [datetime]           NULL,
            [time]                [timestamp]          NULL,
            [moduser]             [varchar](16)        NULL,
            [guid]                [uniqueidentifier]   NULL,
            CONSTRAINT [PK_po_photoreportfilter] PRIMARY KEY CLUSTERED
                (
                 [id] ASC
                    ) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
        ) ON [PRIMARY];
        ALTER TABLE [dbo].[po_photoreportfilter]
            WITH CHECK ADD CONSTRAINT [FK_po_photoreportfilter_systemdictionary_id] FOREIGN KEY ([systemdictionary_id])
                REFERENCES [dbo].[info_systemdictionary] ([id]);
        ALTER TABLE [dbo].[po_photoreportfilter]
            WITH CHECK ADD CONSTRAINT [FK_po_photoreportfilter_customdictionary_id] FOREIGN KEY ([customdictionary_id])
                REFERENCES [dbo].[info_customdictionary] ([id]);
        ALTER TABLE [dbo].[po_photoreportfilter]
            ADD CONSTRAINT [DF_po_photoreportfiltercurrenttime] DEFAULT (getdate()) FOR [currenttime]
        ALTER TABLE [dbo].[po_photoreportfilter]
            ADD CONSTRAINT [DF_po_photoreportfiltermoduser] DEFAULT ([dbo].[Get_CurrentCode]()) FOR [moduser]
        ALTER TABLE [dbo].[po_photoreportfilter]
            ADD CONSTRAINT [DF_po_photoreportfilterguid] DEFAULT (newid()) FOR [guid]

        INSERT INTO po_photoreportfilter (
    code,
    name,
    filter,
    default_filter,
    position,
    field_data_type,
    enable,
    is_required,
    custom_where,
    systemdictionary_id,
    customdictionary_id)
VALUES
    ('regionalmanager', 'Regional manager',
     ' (t.owner_id in (SELECT child_id FROM info_contactsubmission WHERE subj_id in (#VALUE#)) or t.owner_id in (#VALUE#)) ',
     null, 1, 7, 0, 0,
     '  (coalesce(info_user.isnowork, 0) != 1 AND (coalesce((SELECT value FROM info_options WHERE name = ''photoReportManagerFiltersWithoutPosition''), 0) = 1
         AND coalesce(info_user.isReg, 0) = 1) OR (coalesce((SELECT value FROM info_options WHERE name = ''photoReportManagerFiltersWithoutPosition''), 0) != 1
         AND coalesce(info_user.position_id, 0) IN (SELECT id FROM info_dictionary WHERE Identifier = 35
         AND coalesce(flags & 2, 0) = 2))) ',
     (select top 1 id from info_systemdictionary where tablename = 'info_user'), null),
    ('flcm', 'FLCM',
     ' (t.owner_id in (SELECT child_id FROM info_contactsubmission WHERE subj_id in (#VALUE#)) or t.owner_id in (#VALUE#)) ',
     null, 2, 7, 0, 0,
     ' position_id in (select top 1 id from info_dictionary where name = ''flcm'') ',
     (select top 1 id from info_systemdictionary where tablename = 'info_user'), null),
    ('regionpart', 'Bussines region',
     ' (t.company_id in (select id from info_company where info_company.Region_Id in (select region_id from info_regioninregionpart where regionpart_id in (#VALUE#))) or t.contact_id in (select id from info_contact where info_contact.Region_id in (select region_id from info_regioninregionpart where regionpart_id in (#VALUE#)))) ',
     null, 4, 7, 0, 0,
     ' id in (select regionpart_id from info_regioninregionpart where region_id in (
        select region_id from info_regioninuser where user_id in (SELECT u.id FROM info_user u WHERE (u.id = #USER_ID#
                OR u.id IN (SELECT child_id FROM info_contactSubmission WHERE subj_id = #USER_ID#)
                OR EXISTS (SELECT 1 FROM info_user WHERE id = #USER_ID#
                AND CONVERT(VARCHAR(8000), CAST([profile] AS BINARY)) IN (''lpa0HlFGfiF7L0YU+lE='', ''lpWlGlRcEg=='') ))
            AND ISNULL(u.isNoWork, 0) = 0
            AND ISNULL(u.is_gps_disabled, 0) = 0))) ',
            (select top 1 id from info_systemdictionary where tablename = 'info_regionpart'), null),
        ('region', 'Region',
                   ' (t.company_id in (select id from info_company where info_company.Region_Id in (#VALUE#)) or t.contact_id in (select id from info_contact where info_contact.Region_id in (#VALUE#))) ',
        null, 5, 7, 0, 0,
                   ' id in (select region_id from info_regioninregionpart where region_id in (
            select region_id from info_regioninuser where user_id in (SELECT u.id FROM info_user u WHERE (u.id = #USER_ID#
                    OR u.id IN (SELECT child_id FROM info_contactSubmission WHERE subj_id = #USER_ID#)
                    OR EXISTS (SELECT 1 FROM info_user WHERE id = #USER_ID#
                    AND CONVERT(VARCHAR(8000), CAST([profile] AS BINARY)) IN (''lpa0HlFGfiF7L0YU+lE='', ''lpWlGlRcEg=='') ))
                AND ISNULL(u.isNoWork, 0) = 0
                AND ISNULL(u.is_gps_disabled, 0) = 0)
            )) ',
            (select top 1 id from info_systemdictionary where tablename = 'info_Region'), null),
        ('phototype', 'Photo type',
                   ' tfg.photodictionary_id IN (select top 1 id from info_photodictionary where name in (select name from info_photodictionary where id in (#VALUE#))) ',
        null, 17, 7, 0, 0,
                   '  id in (SELECT id FROM info_photodictionary WHERE page = 4)  ',
                (select top 1 id from info_systemdictionary where tablename = 'info_taskfilegroup'), null),
        ('spec', 'Specialization',
                   ' t.Contact_id in (SELECT id FROM info_contact WHERE specialization_id IN (#VALUE#)) ',
        null, 16, 7, 0, 0,
                   ' id in (SELECT specialization_id
            FROM info_task
            where id in (select subj_id from info_taskfilegroup where id in (
             select subj_id from vw_task_file_image))
            and owner_id in (SELECT u.id FROM info_user u WHERE (u.id = #USER_ID#
                    OR u.id IN (SELECT child_id FROM info_contactSubmission WHERE subj_id = #USER_ID#)
                    OR EXISTS (SELECT 1 FROM info_user WHERE id = #USER_ID#
                    AND CONVERT(VARCHAR(8000), CAST([profile] AS BINARY)) IN (''lpa0HlFGfiF7L0YU+lE='', ''lpWlGlRcEg=='') ))
                AND ISNULL(u.isNoWork, 0) = 0
                AND ISNULL(u.is_gps_disabled, 0) = 0)
            and not contact_id is null) ',
            (select top 1 id from info_systemdictionary where tablename = 'info_dictionary'), null),
        ('tasktype', 'Task type',
                   ' t.tasktype_id IN (#VALUE#) ',
        null, 14, 7, 0, 0,
                   ' id in (select tasktype_id from info_task where id in (
                select subj_id from info_taskfilegroup
                ) and owner_id in (
                SELECT u.id FROM info_user u WHERE (u.id = #USER_ID#
                    OR u.id IN (SELECT child_id FROM info_contactSubmission WHERE subj_id = #USER_ID#)
                    OR EXISTS (SELECT 1 FROM info_user WHERE id = #USER_ID#
                    AND CONVERT(VARCHAR(8000), CAST([profile] AS BINARY)) IN (''lpa0HlFGfiF7L0YU+lE='', ''lpWlGlRcEg=='') ))
                AND ISNULL(u.isNoWork, 0) = 0
                AND ISNULL(u.is_gps_disabled, 0) = 0
                )) ',
            (select top 1 id from info_systemdictionary where tablename = 'info_tasktype'), null),
        ('brand', 'Brand',
                   ' tfi.photobrand IN (#VALUE#) ',
        null, 18, 7, 0, 0,
                   ' id in (select photobrand from vw_task_file_image where subj_id in (
            select id from info_taskfilegroup where subj_id in (
                select id from info_task where owner_id in (
                SELECT u.id FROM info_user u WHERE (u.id = #USER_ID#
                OR u.id IN (SELECT child_id FROM info_contactSubmission WHERE subj_id = #USER_ID#)
                OR EXISTS (SELECT 1 FROM info_user WHERE id = #USER_ID#
                AND CONVERT(VARCHAR(8000), CAST([profile] AS BINARY)) IN (''lpa0HlFGfiF7L0YU+lE='', ''lpWlGlRcEg=='') ))
                AND ISNULL(u.isNoWork, 0) = 0
                AND ISNULL(u.is_gps_disabled, 0) = 0
            )))) ',
            (select top 1 id from info_systemdictionary where tablename = 'info_preparationbrend'), null),
        ('city', 'City',
                   ' (t.company_id in (select id from info_company where info_company.City_id in (#VALUE#)) or t.contact_id in (select id from info_contact where info_contact.City_id in (#VALUE#))) ',
        null, 6, 7, 0, 0,
                   ' Region_id in (select region_id from info_regioninregionpart)
            and Region_id in (select region_id from info_regioninuser where user_id in (
                SELECT u.id FROM info_user u WHERE (u.id = #USER_ID#
                OR u.id IN (SELECT child_id FROM info_contactSubmission WHERE subj_id = #USER_ID#)
                OR EXISTS (SELECT 1 FROM info_user WHERE id = #USER_ID#
                AND CONVERT(VARCHAR(8000), CAST([profile] AS BINARY)) IN (''lpa0HlFGfiF7L0YU+lE='', ''lpWlGlRcEg=='') ))
                AND ISNULL(u.isNoWork, 0) = 0
                AND ISNULL(u.is_gps_disabled, 0) = 0
            )) ',
            (select top 1 id from info_systemdictionary where tablename = 'info_city'), null),
        ('user', 'User',
                   ' t.owner_id in (#VALUE#) ',
                   ' (t.owner_id in (select child_id from info_contactsubmission where subj_id = #USER_ID#) or t.owner_id in (#USER_ID#)) ',
        10, 7, 0, 0,
                   ' id in (select owner_id from info_task where id in (
            select subj_id from info_taskfilegroup where id in (
            select subj_id from vw_task_file_image))) ',
            (select top 1 id from info_systemdictionary where tablename = 'info_user'), null),
        ('pharm_representative', 'Pharmacy representative',
                   ' t.owner_id in (#VALUE#) ',
        null, 3, 7, 0, 0, '',
        (select top 1 id from info_systemdictionary where tablename = 'info_user'), null),
        ('period', 'Period',
                   ' t.datetill BETWEEN ''#VALUEFROM# 00:00:00'' and ''#VALUETO# 23:59:59'' ',
        null, 11, 8, 0, 0, '',
        null, null),
        ('net', 'Pharmacy network',
                   ' t.Company_id in (select top 1 id from info_company where main_id in (#VALUE#)) ',
        null, 15, 7, 0, 0,
                   ' id in (select main_id from info_company where id in (
            select company_id from info_task where id in (
            select subj_id from info_taskfilegroup where id in (
            select subj_id from vw_task_file_image
            )) and owner_id in (SELECT u.id FROM info_user u WHERE (u.id = #USER_ID#
            OR u.id IN (SELECT child_id FROM info_contactSubmission WHERE subj_id = #USER_ID#)
            OR EXISTS (SELECT 1 FROM info_user WHERE id = #USER_ID#
            AND CONVERT(VARCHAR(8000), CAST([profile] AS BINARY)) IN (''lpa0HlFGfiF7L0YU+lE='', ''lpWlGlRcEg=='') ))
            AND ISNULL(u.isNoWork, 0) = 0
            AND ISNULL(u.is_gps_disabled, 0) = 0))) ',
            (select top 1 id from info_systemdictionary where tablename = 'info_company'), null),
        ('datefrom', 'From',
                   ' t.datefrom >= ''#VALUE# 00:00:00'' ',
        null, 12, 4, 0, 0, '', null, null),
        ('datetill', 'To',
                   ' t.datefrom <= ''#VALUE# 23:59:59'' ',
        null, 13, 4, 0, 0, '', null, null),
        ('regionalmanager', 'Regional manager',
                   ' (t.owner_id in (SELECT child_id FROM info_contactsubmission WHERE subj_id in (SELECT child_id FROM info_contactsubmission WHERE subj_id in (#VALUE#))) or t.owner_id in (#VALUE#)) ',
        null, 1, 7, 0, 0,
        '  coalesce(info_user.isnowork, 0) != 1 AND (coalesce((SELECT value FROM info_options WHERE name = ''photoReportManagerFiltersWithoutPosition''), 0) = 1
             AND coalesce(info_user.isReg, 0) = 1) OR (coalesce((SELECT value FROM info_options WHERE name = ''photoReportManagerFiltersWithoutPosition''), 0) != 1
             AND coalesce(info_user.position_id, 0) IN (SELECT id FROM info_dictionary WHERE Identifier = 35
             AND coalesce(flags & 2, 0) = 2)) ',
        (select top 1 id from info_systemdictionary where tablename = 'info_user'), null),
        ('shop', 'Company',
                   ' t.company_id IN (#VALUE#) ',
        null, 8, 7, 0, 0, '', (select top 1 id from info_systemdictionary where tablename = 'info_company'), null),
        ('companycategory', 'Company category',
                   ' t.company_id IN (select top 1 id from info_company where companytype_id in (select top 1 id from info_companytype where isshop = 1)) AND
         t.company_id IN (select top 1 id from info_company where category_id in (select top 1 id from info_companycategory where id in (#VALUE#))) ',
        null, 7, 7, 0, 0, '', (select top 1 id from info_systemdictionary where tablename = 'info_companycategory'), null),
        ('tag', 'Tag',
                   ' t.id IN (select subj_id from info_taskfilegroup where id in (select taskfilegroup_id from info_taskfilegrouptag where tag_id in (#VALUE#))) ',
        null, 9, 7, 0, 0, '', null, (select top 1 id from info_CustomDictionary where code = 'phototag')),
        ('phototype', 'Photo type',
                   ' tfg.name IN (SELECT distinct name FROM info_taskfilegroup WHERE id in (#VALUE#) UNION ALL SELECT distinct dictionaryvalue FROM info_customdictionaryvalue WHERE id in (#VALUE#)) ',
        null, 17, 7, 0, 0,
                   ' id in (select id from (SELECT max(id) as id, name FROM info_taskfilegroup where id in (select subj_id from vw_task_file_image) group by name) as i) ',
                (select top 1 id from info_systemdictionary where tablename = 'info_taskfilegroup'), null),
        ('customnet', 'Pharmacy network',
                   ' t.company_id in (select Subj_id from info_companyinfo where InfoType_id in (select top 1 id from info_AddInfoType
         where CustomDictionary_id IN (select top 1 id from info_CustomDictionary where code = ''pharmacy_network'')) AND DictValue_id in (#VALUE#)) ',
        null, 15, 7, 0, 0, '', null, (select top 1 id from info_CustomDictionary where code = 'pharmacy_network'));
    END
GO

-----------------------------------------------------------------------------------------------------------
IF OBJECT_ID(N'[po_photoreportfilterrelation]', 'U') IS NULL
    BEGIN
        CREATE TABLE [dbo].[po_photoreportfilterrelation]
        (
            [id]                  [int] IDENTITY (1,1) NOT NULL,
            [subj_id]             [int]                NULL,
            [child_id]            [int]                NULL,
            [custom_where]        [varchar](max)       NULL,
            [currenttime]         [datetime]           NULL,
            [time]                [timestamp]          NULL,
            [moduser]             [varchar](16)        NULL,
            [guid]                [uniqueidentifier]   NULL,
            CONSTRAINT [PK_po_photoreportfilterrelation] PRIMARY KEY CLUSTERED
                (
                 [id] ASC
                    ) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
        ) ON [PRIMARY];
        ALTER TABLE [dbo].[po_photoreportfilterrelation]
            WITH CHECK ADD CONSTRAINT [FK_po_photoreportfilterrelation_po_photoreportfilter_sudj] FOREIGN KEY ([subj_id])
                REFERENCES [dbo].[po_photoreportfilter] ([id]);
        ALTER TABLE [dbo].[po_photoreportfilterrelation]
            WITH CHECK ADD CONSTRAINT [FK_po_photoreportfilterrelation_po_photoreportfilter_child] FOREIGN KEY ([child_id])
                REFERENCES [dbo].[po_photoreportfilter] ([id]);
        ALTER TABLE [dbo].[po_photoreportfilterrelation]
            ADD CONSTRAINT [DF_po_photoreportfilterrelationcurrenttime] DEFAULT (getdate()) FOR [currenttime]
        ALTER TABLE [dbo].[po_photoreportfilterrelation]
            ADD CONSTRAINT [DF_po_photoreportfilterrelationmoduser] DEFAULT ([dbo].[Get_CurrentCode]()) FOR [moduser]
        ALTER TABLE [dbo].[po_photoreportfilterrelation]
            ADD CONSTRAINT [DF_po_photoreportfilterrelationguid] DEFAULT (newid()) FOR [guid]

        INSERT po_photoreportfilterrelation (subj_id, child_id, custom_where)
            select
                (select top 1 id from po_photoreportfilter where code IN ('regionalmanager')) as subj_id,
                (select top 1 id from po_photoreportfilter where code IN ('flcm')) as child_id,
                'id in (SELECT child_id FROM info_contactsubmission WHERE subj_id in (#VALUES#))' as custom_where
            union
            select
                (select top 1 id from po_photoreportfilter where code IN ('regionalmanager')) as subj_id,
                (select top 1 id from po_photoreportfilter where code IN ('pharm_representative')) as child_id,
                'id in (SELECT child_id FROM info_contactsubmission WHERE subj_id in
                (SELECT child_id FROM info_contactsubmission WHERE subj_id in (#VALUES#)))' as custom_where
            union
            select
                (select top 1 id from po_photoreportfilter where code IN ('flcm')) as subj_id,
                (select top 1 id from po_photoreportfilter where code IN ('pharm_representative')) as child_id,
                'id in (SELECT child_id FROM info_contactsubmission WHERE subj_id in (#VALUES#))' as custom_where;
    END
GO

IF OBJECT_ID('vw_task_file_image') IS NOT NULL
    BEGIN
        DROP VIEW vw_task_file_image
    END
GO

CREATE VIEW vw_task_file_image
AS
SELECT id,
       subj_id,
       name,
       [content],
       fsize,
       fmodified,
       time,
       guid,
       currenttime,
       moduser,
       whatsinside,
       photostate,
       comment,
       photobrand,
       photoservicetype,
       photoprep
FROM dbo.info_taskfile
WHERE RIGHT(name, CHARINDEX('.', REVERSE(name)) - 1) IN
      ('tif', 'tiff', 'bmp', 'jpg', 'jpeg', 'gif', 'png', 'eps', 'raw', 'cr2', 'nef', 'orf', 'sr2', 'webp', 'svg', 'ai',
       'eps')
GO

exec insertLanguageInterface 'en', 'Pharmacy representative', 'Pharmacy representative'
exec insertLanguageInterface 'ru', 'Pharmacy representative', N'Представитель'
exec insertLanguageInterface 'uk', 'Pharmacy representative', N'Представник'
GO

exec insertLanguageInterface 'en', 'Tag', 'Tag'
exec insertLanguageInterface 'ru', 'Tag', N'Тег'
exec insertLanguageInterface 'uk', 'Tag', N'Тег'
GO

exec insertLanguageInterface 'en', 'Pharmacy network', 'Pharmacy network'
exec insertLanguageInterface 'ru', 'Pharmacy network', N'Аптечная сеть'
exec insertLanguageInterface 'uk', 'Pharmacy network', N'Аптечна мережа'
GO

exec insertLanguageInterface 'en', 'Show file Id', 'Show file Id'
exec insertLanguageInterface 'ru', 'Show file Id', N'Показать Id файла'
exec insertLanguageInterface 'uk', 'Show file Id', N'Показати Id файлу'
GO

exec insertLanguageInterface 'en', 'Expand all', 'Expand all'
exec insertLanguageInterface 'ru', 'Expand all', N'Развернуть все'
exec insertLanguageInterface 'uk', 'Expand all', N'Розгорнути все'
GO

IF EXISTS(SELECT 1
          FROM information_schema.columns
          WHERE table_name = 'po_dictionarygroup'
            AND NOT EXISTS(SELECT 1 FROM po_dictionarygroup WHERE [name] = 'Photo report'))
    BEGIN
        INSERT po_dictionarygroup ([name])
        VALUES ('Photo report')
    END
GO

IF EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'insertDictionaryIdentifier') AND xtype IN (N'P'))
    DROP PROCEDURE insertDictionaryIdentifier
GO

CREATE PROCEDURE insertDictionaryIdentifier(@tablename VARCHAR(255), @columnname VARCHAR(255), @value VARCHAR(255), @required INTEGER, @slug VARCHAR(2))
AS
BEGIN
    DECLARE @language_id int;
    SELECT TOP 1 @language_id = id FROM info_language WHERE slug = @slug

    IF @language_id is null return

    IF NOT EXISTS(SELECT 1
                     FROM info_dictionaryidentifier
                     WHERE tablename = @tablename AND columnname = @columnname AND language_id = @language_id)
    INSERT INTO info_dictionaryidentifier (tablename, columnname, [value], required, language_id)
    VALUES (@tablename, @columnname, @value, @required, @language_id)
    ELSE
    UPDATE info_dictionaryidentifier set [value] = @value, required = @required
    WHERE tablename = @tablename AND columnname = @columnname AND language_id = @language_id
END

go

IF EXISTS(SELECT 1
          FROM information_schema.columns
          WHERE table_name = 'po_dictionary'
            AND NOT EXISTS(SELECT 1 FROM po_dictionary WHERE tablename = 'po_photoreportfilter'))
    BEGIN
        INSERT po_dictionary (identifier, [name], tablename, group_id)
        VALUES (NULL, 'Filters', 'po_photoreportfilter', (SELECT id FROM po_dictionarygroup WHERE [name] = 'Photo report')),
            (NULL, 'Relations', 'po_photoreportfilterrelation', (SELECT id FROM po_dictionarygroup WHERE [name] = 'Photo report'));

        exec insertDictionaryIdentifier 'po_photoreportfilter', 'code', 'Code', 1, 'en'
        exec insertDictionaryIdentifier 'po_photoreportfilter', 'code', 'Код', 1, 'ru'
        exec insertDictionaryIdentifier 'po_photoreportfilter', 'code', 'Код', 1, 'uk'

        exec insertDictionaryIdentifier 'po_photoreportfilter', 'name', 'Name', 1, 'en'
        exec insertDictionaryIdentifier 'po_photoreportfilter', 'name', 'Название', 1, 'ru'
        exec insertDictionaryIdentifier 'po_photoreportfilter', 'name', 'Назва', 1, 'uk'

        exec insertDictionaryIdentifier 'po_photoreportfilter', 'filter', 'Filter', 1, 'en'
        exec insertDictionaryIdentifier 'po_photoreportfilter', 'filter', 'Фильтр', 1, 'ru'
        exec insertDictionaryIdentifier 'po_photoreportfilter', 'filter', 'Фільтр', 1, 'uk'

        exec insertDictionaryIdentifier 'po_photoreportfilter', 'default_filter', 'Default filter', 0, 'en'
        exec insertDictionaryIdentifier 'po_photoreportfilter', 'default_filter', 'По умолчанию', 0, 'ru'
        exec insertDictionaryIdentifier 'po_photoreportfilter', 'default_filter', 'За замовчкванням', 0, 'uk'

        exec insertDictionaryIdentifier 'po_photoreportfilter', 'position', 'Position', 0, 'en'
        exec insertDictionaryIdentifier 'po_photoreportfilter', 'position', 'Порядок', 0, 'ru'
        exec insertDictionaryIdentifier 'po_photoreportfilter', 'position', 'Порядок', 0, 'uk'

        exec insertDictionaryIdentifier 'po_photoreportfilter', 'field_data_type', 'Data type', 1, 'en'
        exec insertDictionaryIdentifier 'po_photoreportfilter', 'field_data_type', 'Тип данных', 1, 'ru'
        exec insertDictionaryIdentifier 'po_photoreportfilter', 'field_data_type', 'Тип даних', 1, 'uk'

        exec insertDictionaryIdentifier 'po_photoreportfilter', 'enable', 'Enabled', 0, 'en'
        exec insertDictionaryIdentifier 'po_photoreportfilter', 'enable', 'Включен', 0, 'ru'
        exec insertDictionaryIdentifier 'po_photoreportfilter', 'enable', 'Увімкнений', 0, 'uk'

        exec insertDictionaryIdentifier 'po_photoreportfilter', 'is_required', 'Required', 0, 'en'
        exec insertDictionaryIdentifier 'po_photoreportfilter', 'is_required', 'Является обязательным', 0, 'ru'
        exec insertDictionaryIdentifier 'po_photoreportfilter', 'is_required', 'Є обов''язковим', 0, 'uk'

        exec insertDictionaryIdentifier 'po_photoreportfilter', 'custom_where', 'Custom where', 0, 'en'
        exec insertDictionaryIdentifier 'po_photoreportfilter', 'custom_where', 'WHERE', 0, 'ru'
        exec insertDictionaryIdentifier 'po_photoreportfilter', 'custom_where', 'WHERE', 0, 'uk'

        exec insertDictionaryIdentifier 'po_photoreportfilter', 'systemdictionary_id', 'System dictionary', 0, 'en'
        exec insertDictionaryIdentifier 'po_photoreportfilter', 'systemdictionary_id', 'Системный справочник', 0, 'ru'
        exec insertDictionaryIdentifier 'po_photoreportfilter', 'systemdictionary_id', 'Системний довідник', 0, 'uk'

        exec insertDictionaryIdentifier 'po_photoreportfilter', 'customdictionary_id', 'Custom dictionary', 0, 'en'
        exec insertDictionaryIdentifier 'po_photoreportfilter', 'customdictionary_id', 'Пользовательский справочник', 0, 'ru'
        exec insertDictionaryIdentifier 'po_photoreportfilter', 'customdictionary_id', 'Користувацький довідник', 0, 'uk'

        exec insertDictionaryIdentifier 'po_photoreportfilterrelation', 'subj_id', 'Parent', 1, 'en'
        exec insertDictionaryIdentifier 'po_photoreportfilterrelation', 'subj_id', 'Основной', 1, 'ru'
        exec insertDictionaryIdentifier 'po_photoreportfilterrelation', 'subj_id', 'Основний', 1, 'uk'

        exec insertDictionaryIdentifier 'po_photoreportfilterrelation', 'child_id', 'Child', 1, 'en'
        exec insertDictionaryIdentifier 'po_photoreportfilterrelation', 'child_id', 'Связанный', 1, 'ru'
        exec insertDictionaryIdentifier 'po_photoreportfilterrelation', 'child_id', 'Зв''язаний', 1, 'uk'

        exec insertDictionaryIdentifier 'po_photoreportfilterrelation', 'custom_where', 'WHERE', 1, 'en'
        exec insertDictionaryIdentifier 'po_photoreportfilterrelation', 'custom_where', 'WHERE', 1, 'ru'
        exec insertDictionaryIdentifier 'po_photoreportfilterrelation', 'custom_where', 'WHERE', 1, 'uk'

        INSERT rep_constraint (foreign_table, foreign_field, references_table, references_field) VALUES
            ('po_photoreportfilter', 'customdictionary_id', 'info_customdictionary', 'id'),
            ('po_photoreportfilterrelation', 'subj_id', 'po_photoreportfilter', 'id'),
            ('po_photoreportfilterrelation', 'child_id', 'po_photoreportfilter', 'id');
            --('po_photoreportfilter', 'systemdictionary_id', 'info_systemdictionary', 'id');
    END
GO

exec insertLanguageInterface 'en', 'Please, fill all required fields', 'Please, fill all required fields'
exec insertLanguageInterface 'ru', 'Please, fill all required fields', N'Пожалуйста, заполните все обязательные поля'
exec insertLanguageInterface 'uk', 'Please, fill all required fields', N'Заповніть усі обов''язкові поля'
GO

exec insertLanguageInterface 'en', 'Filters', 'Filters'
exec insertLanguageInterface 'ru', 'Filters', N'Фильтры'
exec insertLanguageInterface 'uk', 'Filters', N'Фільтри'
GO

exec insertLanguageInterface 'en', 'Relations', 'Relations'
exec insertLanguageInterface 'ru', 'Relations', N'Связи'
exec insertLanguageInterface 'uk', 'Relations', N'Зв''язки'
GO

exec insertLanguageInterface 'en', 'To', 'To'
exec insertLanguageInterface 'ru', 'To', N'До'
exec insertLanguageInterface 'uk', 'To', N'До'
GO
---------------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------------

------------------------------------------------------------------------------------------------------------------------
--fix info_user
------------------------------------------------------------------------------------------------------------------------
IF NOT EXISTS(SELECT 1
    FROM INFORMATION_SCHEMA.COLUMNS WHERE table_name = 'info_user' AND column_name = 'isKAM')
    ALTER TABLE info_user ADD isKAM int NULL
GO
------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------

------------------------------------------------------------------------------------------------------------------------
--fix conference
------------------------------------------------------------------------------------------------------------------------
exec insertLanguageInterface  'en', 'Price', N'Price', 'mcm'
exec insertLanguageInterface  'ru', 'Price', N'Цена', 'mcm'
exec insertLanguageInterface  'uk', 'Price', N'Ціна', 'mcm'
GO
exec insertLanguageInterface  'en', 'Contact persons', N'Contact persons', 'mcm'
exec insertLanguageInterface  'ru', 'Contact persons', N'Контактные лица', 'mcm'
exec insertLanguageInterface  'uk', 'Contact persons', N'Контактні особи', 'mcm'
GO
exec insertLanguageInterface  'en', 'Please, choose contact or member', N'Please, choose contact or member', 'mcm'
exec insertLanguageInterface  'ru', 'Please, choose contact or member', N'Пожалуйста, выберите контакт или участника', 'mcm'
exec insertLanguageInterface  'uk', 'Please, choose contact or member', N'Будь ласка, виберіть контакт або учасника', 'mcm'
GO
exec insertLanguageInterface  'en', 'Please, choose conference', N'Please, choose conference', 'mcm'
exec insertLanguageInterface  'ru', 'Please, choose conference', N'Пожалуйста, выберите конференцию', 'mcm'
exec insertLanguageInterface  'uk', 'Please, choose conference', N'Будь ласка, виберіть конференцію', 'mcm'
GO
exec insertLanguageInterface  'en', 'Get token', N'Get token', 'mcm'
exec insertLanguageInterface  'ru', 'Get token', N'Получить токен', 'mcm'
exec insertLanguageInterface  'uk', 'Get token', N'Отримати токен', 'mcm'
GO
exec insertLanguageInterface  'en', 'Name', N'Name', 'mcm'
exec insertLanguageInterface  'ru', 'Name', N'Название', 'mcm'
exec insertLanguageInterface  'uk', 'Name', N'Назва', 'mcm'
GO
exec insertLanguageInterface  'en', 'Responsible', N'Responsible', 'mcm'
exec insertLanguageInterface  'ru', 'Responsible', N'Ответственный', 'mcm'
exec insertLanguageInterface  'uk', 'Responsible', N'Відповідальний', 'mcm'
GO
exec insertLanguageInterface  'en', 'Countries', N'Countries', 'mcm'
exec insertLanguageInterface  'ru', 'Countries', N'Страны', 'mcm'
exec insertLanguageInterface  'uk', 'Countries', N'Країни', 'mcm'
GO
exec insertLanguageInterface  'en', 'Full name', N'Full name', 'mcm'
exec insertLanguageInterface  'ru', 'Full name', N'ФИО', 'mcm'
exec insertLanguageInterface  'uk', 'Full name', N'Повне ім''я', 'mcm'
GO
exec insertLanguageInterface  'en', 'Position', N'Position', 'mcm'
exec insertLanguageInterface  'ru', 'Position', N'Должность', 'mcm'
exec insertLanguageInterface  'uk', 'Position', N'Посада', 'mcm'
GO
exec insertLanguageInterface  'en', 'Country', N'Country', 'mcm'
exec insertLanguageInterface  'ru', 'Country', N'Страна', 'mcm'
exec insertLanguageInterface  'uk', 'Country', N'Країна', 'mcm'
GO
exec insertLanguageInterface  'en', 'Conference code', N'Conference code', 'mcm'
exec insertLanguageInterface  'ru', 'Conference code', N'Код конференции', 'mcm'
exec insertLanguageInterface  'uk', 'Conference code', N'Код конференції', 'mcm'
GO
exec insertLanguageInterface  'en', 'Participation role', N'Participation role', 'mcm'
exec insertLanguageInterface  'ru', 'Participation role', N'Роли участника', 'mcm'
exec insertLanguageInterface  'uk', 'Participation role', N'Ролі учасника', 'mcm'
GO
exec insertLanguageInterface  'en', 'Sponsorship packages', N'Sponsorship packages', 'mcm'
exec insertLanguageInterface  'ru', 'Sponsorship packages', N'Спонсорские пакеты', 'mcm'
exec insertLanguageInterface  'uk', 'Sponsorship packages', N'Спонсорські пакети', 'mcm'
GO
exec insertLanguageInterface  'en', 'Firstname', N'Firstname', 'mcm'
exec insertLanguageInterface  'ru', 'Firstname', N'Имя', 'mcm'
exec insertLanguageInterface  'uk', 'Firstname', N'Ім''я', 'mcm'
GO
exec insertLanguageInterface  'en', 'Lastname', N'Lastname', 'mcm'
exec insertLanguageInterface  'ru', 'Lastname', N'Фамилия', 'mcm'
exec insertLanguageInterface  'uk', 'Lastname', N'Прізвище', 'mcm'
GO
exec insertLanguageInterface  'en', 'Middlename', N'Middlename', 'mcm'
exec insertLanguageInterface  'ru', 'Middlename', N'Отчество', 'mcm'
exec insertLanguageInterface  'uk', 'Middlename', N'По батькові', 'mcm'
GO
exec insertLanguageInterface  'en', 'Last date', N'Last date', 'mcm'
exec insertLanguageInterface  'ru', 'Last date', N'Дата последней рассылки', 'mcm'
exec insertLanguageInterface  'uk', 'Last date', N'Дата останньої розсилки', 'mcm'
GO
exec insertLanguageInterface  'en', 'Last subject', N'Last subject', 'mcm'
exec insertLanguageInterface  'ru', 'Last subject', N'Название последней рассылки', 'mcm'
exec insertLanguageInterface  'uk', 'Last subject', N'Назва останньої розсилки', 'mcm'
GO
exec insertLanguageInterface  'en', 'Video', N'Video', 'mcm'
exec insertLanguageInterface  'ru', 'Video', N'Видео', 'mcm'
exec insertLanguageInterface  'uk', 'Video', N'Відео', 'mcm'
GO
exec insertLanguageInterface  'en', 'Token received', N'Token received', 'mcm'
exec insertLanguageInterface  'ru', 'Token received', N'Токен получен', 'mcm'
exec insertLanguageInterface  'uk', 'Token received', N'Токен отримано', 'mcm'
GO
exec insertLanguageInterface  'en', 'Add members', N'Add members', 'mcm'
exec insertLanguageInterface  'ru', 'Add members', N'Добавить участников', 'mcm'
exec insertLanguageInterface  'uk', 'Add members', N'Додати учасників', 'mcm'
GO
exec insertLanguageInterface  'en', 'Access to video', N'Access to video', 'mcm'
exec insertLanguageInterface  'ru', 'Access to video', N'Доступ к видео', 'mcm'
exec insertLanguageInterface  'uk', 'Access to video', N'Доступ до відео', 'mcm'
GO
exec insertLanguageInterface  'en', 'Access to conference', N'Access to conference', 'mcm'
exec insertLanguageInterface  'ru', 'Access to conference', N'Доступ к конференции', 'mcm'
exec insertLanguageInterface  'uk', 'Access to conference', N'Доступ до конференції', 'mcm'
GO
exec insertLanguageInterface  'en', 'Contact person', N'Contact person', 'mcm'
exec insertLanguageInterface  'ru', 'Contact person', N'Контактное лицо', 'mcm'
exec insertLanguageInterface  'uk', 'Contact person', N'Контактна особа', 'mcm'
GO
exec insertLanguageInterface  'en', 'State', N'State', 'mcm'
exec insertLanguageInterface  'ru', 'State', N'Состояние', 'mcm'
exec insertLanguageInterface  'uk', 'State', N'Стан', 'mcm'
GO

exec insertLanguageInterface 'en', 'Available for mcm', 'Available for mcm', 'crm'
exec insertLanguageInterface 'ru', 'Available for mcm', N'Доступен в МСМ', 'crm'
exec insertLanguageInterface 'uk', 'Available for mcm', N'Доступно в МСМ', 'crm'
GO
------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------
