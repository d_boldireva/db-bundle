------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------
IF EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'insertLanguageInterface') AND xtype IN (N'P'))
    DROP PROCEDURE insertLanguageInterface
GO

CREATE PROCEDURE insertLanguageInterface(@slug VARCHAR(2), @key VARCHAR(255), @translation VARCHAR(255), @domain VARCHAR(255) = 'messages')
    AS
    BEGIN
        DECLARE @language_id int;
        SELECT TOP 1 @language_id = id FROM info_language WHERE slug = @slug

        INSERT INTO info_languageinterface ([key], translation, language_id, domain)
        SELECT TOP 1 @key, @translation, @language_id, @domain
        WHERE NOT EXISTS(SELECT 1
                         FROM info_languageinterface
                         WHERE [key] collate SQL_Latin1_General_CP1_CS_AS = @key AND language_id = @language_id AND domain = @domain)
    END
GO
------------------------------------------------------------------------------------------------------------------------
---https://teamsoft.atlassian.net/browse/PHARMAWEB-426
------------------------------------------------------------------------------------------------------------------------
IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'info_contactphone' AND COLUMN_NAME = 'isvisibleincrm')
    BEGIN
        ALTER TABLE info_contactphone ADD isvisibleincrm integer null
    END

IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'info_contactphone' AND COLUMN_NAME = 'phonetype_id')
    BEGIN
        ALTER TABLE info_contactphone ADD phonetype_id integer null

        ALTER TABLE info_contactphone
            ADD CONSTRAINT fk_info_contactphone_phonetype_id FOREIGN KEY (phonetype_id)
                REFERENCES info_phonetype (id)
    END


IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'info_contactemail' AND COLUMN_NAME = 'isvisibleincrm')
    BEGIN
        ALTER TABLE info_contactemail ADD isvisibleincrm integer null
    END

exec insertPoEntityFields 2, 'contact_phones', N'List of phones', 'contacts'
exec insertPoEntityFields 2, 'contact_emails', N'List of emails', 'contacts'
GO

exec insertLanguageInterface 'en', 'The email is repeated several times.', N'The email is repeated several times.', 'validators'
exec insertLanguageInterface 'ru', 'The email is repeated several times.', N'Почта повторяется несколько раз', 'validators'
exec insertLanguageInterface 'uk', 'The email is repeated several times.', N'Пошта повторюється кілька разів', 'validators'
GO

exec insertLanguageInterface 'en', 'The phone is repeated several times.', N'The phone is repeated several times.', 'validators'
exec insertLanguageInterface 'ru', 'The phone is repeated several times.', N'Телефон повторяется несколько раз', 'validators'
exec insertLanguageInterface 'uk', 'The phone is repeated several times.', N'Телефон повторюється кілька разів', 'validators'
GO

exec insertLanguageInterface 'en', N'Phone number doesn''t match with phone pattern. {{pattern}}', N'Phone number doesn''t  match with phone pattern. {{pattern}}', 'validators'
exec insertLanguageInterface 'ru', N'Phone number doesn''t match with phone pattern. {{pattern}}', N'Номер телефона не совпадает с шаблоном телефона. {{pattern}}', 'validators'
exec insertLanguageInterface 'uk', N'Phone number doesn''t match with phone pattern. {{pattern}}', N'Номер телефону не відповідає шаблону телефону. {{pattern}}', 'validators'
GO

exec insertLanguageInterface 'en', 'Phones', N'Phones', 'crm'
exec insertLanguageInterface 'ru', 'Phones', N'Телефоны', 'crm'
exec insertLanguageInterface 'uk', 'Phones', N'Телефони', 'crm'
GO

exec insertLanguageInterface 'en', 'Phone type', N'Phone type', 'crm'
exec insertLanguageInterface 'ru', 'Phone type', N'Тип телефона', 'crm'
exec insertLanguageInterface 'uk', 'Phone type', N'Тип телефону', 'crm'
GO

exec insertLanguageInterface 'en', 'Phone number', N'Phone number', 'crm'
exec insertLanguageInterface 'ru', 'Phone number', N'Номер телефона', 'crm'
exec insertLanguageInterface 'uk', 'Phone number', N'Номер телефону', 'crm'
GO

exec insertLanguageInterface 'en', N'Phone number doesn''t match with phone pattern.', N'Phone number doesn''t match with phone pattern.', 'crm'
exec insertLanguageInterface 'ru', N'Phone number doesn''t match with phone pattern.', N'Номер телефона не совпадает с шаблоном телефона.', 'crm'
exec insertLanguageInterface 'uk', N'Phone number doesn''t match with phone pattern.', N'Номер телефону не відповідає шаблону телефону.', 'crm'
GO

exec insertLanguageInterface 'en', 'The phone is repeated several times.', N'The phone is repeated several times.', 'crm'
exec insertLanguageInterface 'ru', 'The phone is repeated several times.', N'Телефон повторяется несколько раз', 'crm'
exec insertLanguageInterface 'uk', 'The phone is repeated several times.', N'Телефон повторюється кілька разів', 'crm'
GO

exec insertLanguageInterface 'en', 'This value should not be blank.', N'This value should not be blank.', 'crm'
exec insertLanguageInterface 'ru', 'This value should not be blank.', N'Телефон повторяется несколько раз', 'crm'
exec insertLanguageInterface 'uk', 'This value should not be blank.', N'Телефон повторюється кілька разів', 'crm'
GO

exec insertLanguageInterface 'en', 'List of phones', N'List of phones', 'messages'
exec insertLanguageInterface 'ru', 'List of phones', N'Список телефонов', 'messages'
exec insertLanguageInterface 'uk', 'List of phones', N'Список телефонів', 'messages'
GO

exec insertLanguageInterface 'en', 'List of emails', N'List of emails', 'messages'
exec insertLanguageInterface 'ru', 'List of emails', N'Список emails', 'messages'
exec insertLanguageInterface 'uk', 'List of emails', N'Список emails', 'messages'
GO

exec insertLanguageInterface 'en', 'Contacts of clients', N'Contacts of clients', 'messages'
exec insertLanguageInterface 'ru', 'Contacts of clients', N'Контакты клиентов', 'messages'
exec insertLanguageInterface 'uk', 'Contacts of clients', N'Контакти клієнтів', 'messages'
GO

DELETE FROM info_hidefield WHERE page = 2 AND name = 'contact_phones';
INSERT INTO info_hidefield(page, name, role_id) SELECT 2, 'contact_phones', id FROM info_role;

DELETE FROM info_hidefield WHERE page = 2 AND name = 'contact_emails';
INSERT INTO info_hidefield(page, name, role_id) SELECT 2, 'contact_emails', id FROM info_role;
------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------

------------------------------------------------------------------------------------------------------------------------
--- https://teamsoft.atlassian.net/browse/CAMPINA-95
------------------------------------------------------------------------------------------------------------------------
IF OBJECT_ID(N'info_companysign', 'U') is NULL
    BEGIN
        create table info_companysign
        (
            id          int identity
                constraint pk_info_companysign
                    primary key,
            company_id  int,
            createdate  date,
            modified    date,

            currenttime datetime
                constraint DF_info_companysigncurrenttime default getdate(),
            moduser     varchar(16)
                constraint DF_info_companysignmoduser default [dbo].[Get_CurrentCode](),
            guid        uniqueidentifier
                constraint DF_info_companysignguid default newid()
        )

        ALTER TABLE info_companysign
            ADD CONSTRAINT fk_info_companysign_company_id FOREIGN KEY (company_id) REFERENCES info_company (id) ON UPDATE CASCADE ON DELETE CASCADE;
    END
GO

exec createCurrenttimeAndModuserTrigger N'info_companysign'
GO

IF OBJECT_ID(N'info_companysignfile', 'U') is NULL
    BEGIN
        create table info_companysignfile
        (
            id             int identity
                constraint pk_info_companysignfile
                    primary key,
            agreement      image,
            filetype       varchar(16),
            companysign_id integer,

            currenttime    datetime
                constraint DF_info_companysignfilecurrenttime default getdate(),
            moduser        varchar(16)
                constraint DF_info_companysignfilemoduser default [dbo].[Get_CurrentCode](),
            guid           uniqueidentifier
                constraint DF_info_companysignfileguid default newid()
        )

        ALTER TABLE info_companysignfile
            ADD CONSTRAINT fk_info_companysignfile_companysign_id FOREIGN KEY (companysign_id) REFERENCES info_companysign (id) ON UPDATE CASCADE ON DELETE CASCADE;
    END
GO

exec createCurrenttimeAndModuserTrigger N'info_companysignfile'
GO

exec insertPoEntityFields 1, 'company_sign', N'Signature';
GO
------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------

------------------------------------------------------------------------------------------------------------------------
--- https://teamsoft.atlassian.net/browse/CAMPINA-92
------------------------------------------------------------------------------------------------------------------------
IF OBJECT_ID(N'info_preparationdtdose', 'U') is NULL
    begin
        create table info_preparationdtdose(
                                               [id] int identity(1,1)  not null
            , [currenttime] datetime  null CONSTRAINT [DF_info_preparationdtdosecurrenttime] DEFAULT (getdate())
            , [time] timestamp  null
            , [moduser] varchar (16) null CONSTRAINT [DF_info_preparationdtdosemoduser] DEFAULT ([dbo].[Get_CurrentCode]())
            , [guid] uniqueidentifier  null CONSTRAINT [DF_info_preparationdtdoseguid] DEFAULT (newid())
            , [datefrom] datetime  null
            , [datetill] datetime  null
            , [preparation_id] int  null
            , [dose] varchar (255) null
            , CONSTRAINT [PK_info_preparationdtdose] PRIMARY KEY (id) ON [PRIMARY]
            , CONSTRAINT [FK_info_preparationdtdose_preparation_id] FOREIGN KEY ([preparation_id]) REFERENCES info_preparation ([id]) ON UPDATE CASCADE ON DELETE CASCADE
        )
    end
GO

exec createCurrenttimeAndModuserTrigger N'info_preparationdtdose'
GO

IF OBJECT_ID(N'info_taskpreparationdtdose', 'U') is NULL
    begin
        create table info_taskpreparationdtdose(
             [id] int identity(1,1)  not null
            , [currenttime] datetime  null DEFAULT (getdate())
            , [moduser] varchar (16) null DEFAULT ([dbo].[Get_CurrentCode]())
            , [guid] uniqueidentifier null DEFAULT (newid())
            , [task_id] integer  null
            , [preparationdtdose_id] integer  null
            , [cnt] int  null
            , [createdate] datetime  null
            , CONSTRAINT [PK_info_taskpreparationdtdose] PRIMARY KEY (id) ON [PRIMARY]
            , CONSTRAINT [FK_info_taskpreparationdtdose_preparationdtdose_id] FOREIGN KEY ([preparationdtdose_id]) REFERENCES info_preparationdtdose ([id]) ON UPDATE CASCADE ON DELETE CASCADE
            , CONSTRAINT [FK_info_taskpreparationdtdose_task_id] FOREIGN KEY ([task_id]) REFERENCES info_task ([id]) ON UPDATE CASCADE ON DELETE CASCADE
        )
    end

exec createCurrenttimeAndModuserTrigger N'info_taskpreparationdtdose'
GO

IF OBJECT_ID(N'info_taskpreparationdtdosefile', 'U') is NULL
    begin
        create table info_taskpreparationdtdosefile(
              [id] int identity(1,1)  not null
            , [currenttime] datetime  null DEFAULT (getdate())
            , [moduser] varchar (16) null DEFAULT ([dbo].[Get_CurrentCode]())
            , [guid] uniqueidentifier null DEFAULT (newid())
            , [taskpreparationdtdose_id] integer null
            , [request] image  null
            , [filetype] varchar (16) null
            , CONSTRAINT [PK_info_taskpreparationdtdosefile] PRIMARY KEY (id) ON [PRIMARY]
            , CONSTRAINT [FK_info_taskpreparationdtdosefile_taskpreparationdtdose_id] FOREIGN KEY ([taskpreparationdtdose_id]) REFERENCES info_taskpreparationdtdose ([id]) ON UPDATE CASCADE ON DELETE CASCADE
        )
    end
GO

exec createCurrenttimeAndModuserTrigger N'info_taskpreparationdtdosefile'
GO

EXEC insertPoEntityFields 4, 'task_preparation_dt_doses', N'Doctor''s professional assessment'
GO

-- admin panel
exec insertLanguageInterface 'en', N'Doctor''s professional assessment',  N'Doctor''s professional assessment', 'messages';
exec insertLanguageInterface 'ru',  N'Doctor''s professional assessment', N'Профессиональная оценка врача', 'messages';
exec insertLanguageInterface 'uk',  N'Doctor''s professional assessment', N'Професійна оцінка лікаря', 'messages';
GO

exec insertLanguageInterface 'en', N'Doctor''s professional assessment',  N'Doctor''s professional assessment', 'crm';
exec insertLanguageInterface 'ru',  N'Doctor''s professional assessment', N'Профессиональная оценка врача', 'crm';
exec insertLanguageInterface 'uk',  N'Doctor''s professional assessment', N'Професійна оцінка лікаря', 'crm';
GO

exec insertLanguageInterface 'en', N'Indicated',  N'Indicated', 'crm';
exec insertLanguageInterface 'ru',  N'Indicated', N'Указано', 'crm';
exec insertLanguageInterface 'uk',  N'Indicated', N'Вказано', 'crm';
GO

exec insertLanguageInterface 'en', N'Not indicated',  N'Not indicated', 'crm';
exec insertLanguageInterface 'ru',  N'Not indicated', N'Не указано', 'crm';
exec insertLanguageInterface 'uk',  N'Not indicated', N'Не вказано', 'crm';
GO

exec insertLanguageInterface 'en', N'Sorry, but image of assessment not found',  N'Sorry, but image of assessment not found', 'crm';
exec insertLanguageInterface 'ru',  N'Sorry, but image of assessment not found', N'Извините, но изображение оценки не найдено', 'crm';
exec insertLanguageInterface 'uk',  N'Sorry, but image of assessment not found', N'Вибачте, але зображення оцінки не знайдено', 'crm';
GO
------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------

------------------------------------------------------------------------------------------------------------------------
--https://teamsoft.atlassian.net/browse/MCM-404
------------------------------------------------------------------------------------------------------------------------
IF NOT EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS
                WHERE table_name = 'info_mcmdistribution' AND column_name = 'invite')
ALTER TABLE info_mcmdistribution ADD invite int NULL
GO

IF NOT EXISTS(SELECT 1
              FROM info_mcmcontentvariable
              WHERE code = 'USER_NAME')
    INSERT INTO info_mcmcontentvariable (name, code) VALUES ('ФИО МП', 'USER_NAME')
GO
IF NOT EXISTS(SELECT 1
              FROM info_mcmcontentvariable
              WHERE code = 'LINKFORDOCTOR')
    INSERT INTO info_mcmcontentvariable (name, code) VALUES ('Ссылка для врача', 'LINKFORDOCTOR')
GO
IF NOT EXISTS(SELECT 1
              FROM info_mcmcontentvariable
              WHERE code = 'VISITDATE')
    INSERT INTO info_mcmcontentvariable (name, code) VALUES ('Дата визита', 'VISITDATE')
GO

exec insertLanguageInterface  'en', 'Invitation', N'Invitation', 'mcm'
exec insertLanguageInterface  'ru', 'Invitation', N'Приглашение', 'mcm'
exec insertLanguageInterface  'uk', 'Invitation', N'Запрошення', 'mcm'
GO
------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------

------------------------------------------------------------------------------------------------------------------------
---https://teamsoft.atlassian.net/browse/REDDYSRSA-191
------------------------------------------------------------------------------------------------------------------------
IF NOT EXISTS(SELECT 1 FROM info_serviceprivilege WHERE [code] = 'ROLE_ACCESS_TO_CREATE_EXPENSES')
    BEGIN
        INSERT INTO info_serviceprivilege ([name], code, service_id, parent_id)
        VALUES (
                   'Allow create expenses',
                   'ROLE_ACCESS_TO_CREATE_EXPENSES',
                   (SELECT id FROM info_service WHERE identifier = 'expenses'),
                   (SELECT MIN(id) FROM info_serviceprivilege WHERE code = 'ROLE_ACCESS_TO_EXPENSES')
               )
    END
GO

IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'info_expense' AND COLUMN_NAME = 'iscountrytrip')
    BEGIN
        ALTER TABLE info_expense ADD iscountrytrip integer null
    END

IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'info_expense' AND COLUMN_NAME = 'line2status')
    BEGIN
        ALTER TABLE info_expense ADD line2status integer null
    END

IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'info_expense' AND COLUMN_NAME = 'line2comment')
    BEGIN
        ALTER TABLE info_expense ADD line2comment VARCHAR(255) null
    END

IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'info_expense' AND COLUMN_NAME = 'moduser_line1')
    BEGIN
        ALTER TABLE info_expense ADD moduser_line1 VARCHAR(16) null
    END

IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'info_expense' AND COLUMN_NAME = 'moduser_line2')
    BEGIN
        ALTER TABLE info_expense ADD moduser_line2 VARCHAR(16) null
    END

IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'info_expense' AND COLUMN_NAME = 'modified_line1')
    BEGIN
        ALTER TABLE info_expense ADD modified_line1 datetime null
    END

IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'info_expense' AND COLUMN_NAME = 'modified_line2')
    BEGIN
        ALTER TABLE info_expense ADD modified_line2 datetime null
    END

exec insertLanguageInterface 'en', 'Allow create expenses', N'Allow create expenses', 'messages'
exec insertLanguageInterface 'ru', 'Allow create expenses', N'Разрешить создавать расходы', 'messages'
exec insertLanguageInterface 'uk', 'Allow create expenses', N'Дозволити створювати витрати', 'messages'
GO

exec insertLanguageInterface 'en', 'Load more expenses', N'Load more expenses', 'expense'
exec insertLanguageInterface 'ru', 'Load more expenses', N'Загрузить больше расходов', 'expense'
exec insertLanguageInterface 'uk', 'Load more expenses', N'Завантажити більше витрат', 'expense'
GO

exec insertLanguageInterface 'en', 'View expense', N'View expense', 'expense'
exec insertLanguageInterface 'ru', 'View expense', N'Просмотр расхода', 'expense'
exec insertLanguageInterface 'uk', 'View expense', N'Перегляд витрати', 'expense'
GO

exec insertLanguageInterface 'en', 'Edit expense', N'Edit expense', 'expense'
exec insertLanguageInterface 'ru', 'Edit expense', N'Редактировать расход', 'expense'
exec insertLanguageInterface 'uk', 'Edit expense', N'Редагувати витрату', 'expense'
GO

exec insertLanguageInterface 'en', 'Create new expense', N'Create new expense', 'expense'
exec insertLanguageInterface 'ru', 'Create new expense', N'Создать новый расход', 'expense'
exec insertLanguageInterface 'uk', 'Create new expense', N'Створити нову витрату', 'expense'
GO

exec insertLanguageInterface 'en', 'A country trip', N'A country trip', 'expense'
exec insertLanguageInterface 'ru', 'A country trip', N'Загородная поездка', 'expense'
exec insertLanguageInterface 'uk', 'A country trip', N'Заміська подорож', 'expense'
GO

exec insertLanguageInterface 'en', 'Choose a date', N'Choose a date', 'expense'
exec insertLanguageInterface 'ru', 'Choose a date', N'Выберите дату', 'expense'
exec insertLanguageInterface 'uk', 'Choose a date', N'Виберіть дату', 'expense'
GO

exec insertLanguageInterface 'en', 'Please, choose a date of expense', N'Please, choose a date of expense', 'expense'
exec insertLanguageInterface 'ru', 'Please, choose a date of expense', N'Пожалуйста, выберите дату расхода', 'expense'
exec insertLanguageInterface 'uk', 'Please, choose a date of expense', N'Будь ласка. Виберіть дату витрати', 'expense'
GO

exec insertLanguageInterface 'en', 'Please, add a expense type', N'Please, add a expense type', 'expense'
exec insertLanguageInterface 'ru', 'Please, add a expense type', N'Пожалуйста, добавьте тип расходов', 'expense'
exec insertLanguageInterface 'uk', 'Please, add a expense type', N'Будь ласка, додайте тип витрат', 'expense'
GO

exec insertLanguageInterface 'en', 'Expense type', N'Expense type', 'expense'
exec insertLanguageInterface 'ru', 'Expense type', N'Тип расходов', 'expense'
exec insertLanguageInterface 'uk', 'Expense type', N'Тип витрат', 'expense'
GO

exec insertLanguageInterface 'en', 'Please, choose type of expense', N'Please, choose type of expense', 'expense'
exec insertLanguageInterface 'ru', 'Please, choose type of expense', N'Пожалуйста, выберите тип расхода', 'expense'
exec insertLanguageInterface 'uk', 'Please, choose type of expense', N'Будь ласка, виберіть тип витрат', 'expense'
GO

exec insertLanguageInterface 'en', 'View uploaded photo', N'View uploaded photo', 'expense'
exec insertLanguageInterface 'ru', 'View uploaded photo', N'Посмотреть загруженное фото', 'expense'
exec insertLanguageInterface 'uk', 'View uploaded photo', N'Переглянути завантажену фотографію', 'expense'
GO

exec insertLanguageInterface 'en', 'Please select a file', N'Please select a file', 'expense'
exec insertLanguageInterface 'ru', 'Please select a file', N'Пожалуйста, выберите файл', 'expense'
exec insertLanguageInterface 'uk', 'Please select a file', N'Будь ласка, виберіть файл', 'expense'
GO

exec insertLanguageInterface 'en', 'This value should not be blank.', N'This value should not be blank.', 'expense'
exec insertLanguageInterface 'ru', 'This value should not be blank.', N'Это значение не должно быть пустым.', 'expense'
exec insertLanguageInterface 'uk', 'This value should not be blank.', N'Це значення не повинно бути порожнім.', 'expense'
GO

exec insertLanguageInterface 'en', 'Max 255 characters', N'Max 255 characters', 'expense'
exec insertLanguageInterface 'ru', 'Max 255 characters', N'Максимум 255 символов', 'expense'
exec insertLanguageInterface 'uk', 'Max 255 characters', N'Макс. 255 символів', 'expense'
GO

exec insertLanguageInterface 'en', 'Amount', N'Amount', 'expense'
exec insertLanguageInterface 'ru', 'Amount', N'Сумма', 'expense'
exec insertLanguageInterface 'uk', 'Amount', N'Сума', 'expense'
GO

exec insertLanguageInterface 'en', 'Photo', N'Photo', 'expense'
exec insertLanguageInterface 'ru', 'Photo', N'Фото', 'expense'
exec insertLanguageInterface 'uk', 'Photo', N'Фото', 'expense'
GO

exec insertLanguageInterface 'en', 'Edit', N'Edit', 'expense'
exec insertLanguageInterface 'ru', 'Edit', N'Редактировать', 'expense'
exec insertLanguageInterface 'uk', 'Edit', N'Редагувати', 'expense'
GO

exec insertLanguageInterface 'en', 'Create', N'Create', 'expense'
exec insertLanguageInterface 'ru', 'Create', N'Создать', 'expense'
exec insertLanguageInterface 'uk', 'Create', N'Створити', 'expense'
GO

exec insertLanguageInterface 'en', 'Expense not found.', N'Expense not found.', 'expense'
exec insertLanguageInterface 'ru', 'Expense not found.', N'Расход не найден.', 'expense'
exec insertLanguageInterface 'uk', 'Expense not found.', N'Витрату не знайдено.', 'expense'
GO

exec insertLanguageInterface 'en', 'You are not allowed to edit this expense.', N'You are not allowed to edit this expense.', 'expense'
exec insertLanguageInterface 'ru', 'You are not allowed to edit this expense.', N'Вам не разрешено редактировать этот расход.', 'expense'
exec insertLanguageInterface 'uk', 'You are not allowed to edit this expense.', N'Вам не дозволено редагувати цю витрату.', 'expense'
GO

exec insertLanguageInterface 'en', 'Unsupported error. Please, contact to support.', N'Unsupported error. Please, contact to support.', 'expense'
exec insertLanguageInterface 'ru', 'Unsupported error. Please, contact to support.', N'Неподдерживаемая ошибка. Обращайтесь в службу поддержки.', 'expense'
exec insertLanguageInterface 'uk', 'Unsupported error. Please, contact to support.', N'Непідтримувана помилка. Будь ласка, зв''яжіться зі службою підтримки.', 'expense'
GO

exec insertLanguageInterface 'en', 'Need confirm', N'Need confirm', 'expense'
exec insertLanguageInterface 'ru', 'Need confirm', N'Нужно подтвердить', 'expense'
exec insertLanguageInterface 'uk', 'Need confirm', N'Потрібно підтвердити', 'expense'
GO

exec insertLanguageInterface 'en', 'Are you sure you want to delete a previously saved type of expense?', N'Are you sure you want to delete a previously saved type of expense?', 'expense'
exec insertLanguageInterface 'ru', 'Are you sure you want to delete a previously saved type of expense?', N'Вы действительно хотите удалить ранее сохраненный тип расхода?', 'expense'
exec insertLanguageInterface 'uk', 'Are you sure you want to delete a previously saved type of expense?', N'Ви впевнені, що хочете видалити раніше збережений тип витрати?', 'expense'
GO

exec insertLanguageInterface 'en', 'Please, fill in all required fields', N'Please, fill in all required fields', 'expense'
exec insertLanguageInterface 'ru', 'Please, fill in all required fields', N'Пожалуйста, заполните все обязательные поля', 'expense'
exec insertLanguageInterface 'uk', 'Please, fill in all required fields', N'Будь ласка, заповніть всі обов''язкові поля', 'expense'
GO

exec insertLanguageInterface 'en', 'Saved', N'Saved', 'expense'
exec insertLanguageInterface 'ru', 'Saved', N'Сохранено', 'expense'
exec insertLanguageInterface 'uk', 'Saved', N'Збережено', 'expense'
GO

exec insertLanguageInterface 'en', 'Sorry but you can`t edit this expense.', N'Sorry but you can`t edit this expense.', 'expense'
exec insertLanguageInterface 'ru', 'Sorry but you can`t edit this expense.', N'Извините, но вы не можете редактировать этот расход.', 'expense'
exec insertLanguageInterface 'uk', 'Sorry but you can`t edit this expense.', N'Вибачте, але ви не можете редагувати цю витрату.', 'expense'
GO

exec insertLanguageInterface 'en', 'Sorry, but this expense had been already deleted.', N'Sorry but this expense was already deleted.', 'expense'
exec insertLanguageInterface 'ru', 'Sorry, but this expense had been already deleted.', N'Извините, но этот расход уже удален.', 'expense'
exec insertLanguageInterface 'uk', 'Sorry, but this expense had been already deleted.', N'Вибачте, але цю витрату вже видалено.', 'expense'
GO

exec insertLanguageInterface 'en', N'Unable to read file', N'Unable to read file', 'expense'
exec insertLanguageInterface 'ru', N'Unable to read file', N'Невозможно прочитать файл', 'expense'
exec insertLanguageInterface 'uk', N'Unable to read file', N'Не вдається прочитати файл', 'expense'
GO

exec insertLanguageInterface 'en', N'Expenses not found', N'Expenses not found', 'expense'
exec insertLanguageInterface 'ru', N'Expenses not found', N'Расходы не найдены', 'expense'
exec insertLanguageInterface 'uk', N'Expenses not found', N'Витрат не знайдено', 'expense'
GO

exec insertLanguageInterface 'en', N'Close', N'Close', 'expense'
exec insertLanguageInterface 'ru', N'Close', N'Закрыть', 'expense'
exec insertLanguageInterface 'uk', N'Close', N'Закрити', 'expense'
GO

exec insertLanguageInterface 'en', N'Sorry, but image not found', N'Sorry, but image not found', 'expense'
exec insertLanguageInterface 'ru', N'Sorry, but image not found', N'Извините, но изображение не найдено', 'expense'
exec insertLanguageInterface 'uk', N'Sorry, but image not found', N'Вибачте, але зображення не знайдено', 'expense'
GO

exec insertLanguageInterface 'en', N'Is a country trip?', N'Is a country trip?', 'expense'
exec insertLanguageInterface 'ru', N'Is a country trip?', N'Это загородная поездка?', 'expense'
exec insertLanguageInterface 'uk', N'Is a country trip?', N'Це заміська поїздка?', 'expense'
GO

exec insertLanguageInterface 'en', N'Yes', N'Yes', 'expense'
exec insertLanguageInterface 'ru', N'Yes', N'Да', 'expense'
exec insertLanguageInterface 'uk', N'Yes', N'Так', 'expense'
GO

exec insertLanguageInterface 'en', N'No', N'No', 'expense'
exec insertLanguageInterface 'ru', N'No', N'Нет', 'expense'
exec insertLanguageInterface 'uk', N'No', N'Ні', 'expense'
GO

exec insertLanguageInterface 'en', N'The date of expense', N'The date of expense', 'expense'
exec insertLanguageInterface 'ru', N'The date of expense', N'Дата расхода', 'expense'
exec insertLanguageInterface 'uk', N'The date of expense', N'Дата витрати', 'expense'
GO

exec insertLanguageInterface 'en', N'Status', N'Status', 'expense'
exec insertLanguageInterface 'ru', N'Status', N'Статус', 'expense'
exec insertLanguageInterface 'uk', N'Status', N'Статус', 'expense'
GO

exec insertLanguageInterface 'en', N'Claim id', N'Status', 'expense'
exec insertLanguageInterface 'ru', N'Claim id', N'Идентификатор заявки', 'expense'
exec insertLanguageInterface 'uk', N'Claim id', N'Ідентифікатор заявки', 'expense'
GO

exec insertLanguageInterface 'en', N'Vendor name', N'Vendor name', 'expense'
exec insertLanguageInterface 'ru', N'Vendor name', N'Имя поставщика', 'expense'
exec insertLanguageInterface 'uk', N'Vendor name', N'Назва постачальника', 'expense'
GO

exec insertLanguageInterface 'en', N'Comment', N'Comment', 'expense'
exec insertLanguageInterface 'ru', N'Comment', N'Комментарий', 'expense'
exec insertLanguageInterface 'uk', N'Comment', N'Коментар', 'expense'
GO

exec insertLanguageInterface 'en', N'You are not allowed to view this expense.', N'You are not allowed to view this expense.', 'expense'
exec insertLanguageInterface 'ru', N'You are not allowed to view this expense.', N'Вам не разрешено просматривать этот расход.', 'expense'
exec insertLanguageInterface 'uk', N'You are not allowed to view this expense.', N'Вам не дозволено цю витрату.', 'expense'
GO

exec insertLanguageInterface 'en', N'Are you sure you want to delete this expense?', N'Are you sure you want to delete this expense?', 'expense'
exec insertLanguageInterface 'ru', N'Are you sure you want to delete this expense?', N'Вы уверены, что хотите удалить этот расход?', 'expense'
exec insertLanguageInterface 'uk', N'Are you sure you want to delete this expense?', N'Ви впевнені, що хочете видалити цю витрату?', 'expense'
GO

exec insertLanguageInterface 'en', N'Expense deleted', N'Expense deleted', 'expense'
exec insertLanguageInterface 'ru', N'Expense deleted', N'Расход удален', 'expense'
exec insertLanguageInterface 'uk', N'Expense deleted', N'Витрату видалено', 'expense'
GO

exec insertLanguageInterface 'en', N'Sorry but expense was already deleted.', N'Sorry but expense was already deleted.', 'expense'
exec insertLanguageInterface 'ru', N'Sorry but expense was already deleted.', N'Извините, но расход уже удален.', 'expense'
exec insertLanguageInterface 'uk', N'Sorry but expense was already deleted.', N'Вибачте, але витрата вже видалена.', 'expense'
GO

exec insertLanguageInterface 'en', 'Total amount', N'Total amount', 'expense'
exec insertLanguageInterface 'ru', 'Total amount', N'Итого', 'expense'
exec insertLanguageInterface 'uk', 'Total amount', N'Загальна сума', 'expense'
GO

exec insertLanguageInterface 'en', 'Types of expenses', N'Types of expenses', 'expense'
exec insertLanguageInterface 'ru', 'Types of expenses', N'Виды расходов', 'expense'
exec insertLanguageInterface 'uk', 'Types of expenses', N'Види витрат', 'expense'
GO

exec insertLanguageInterface 'en', 'Expenses', N'Expenses', 'expense'
exec insertLanguageInterface 'ru', 'Expenses', N'Расходы', 'expense'
exec insertLanguageInterface 'uk', 'Expenses', N'Витрати', 'expense'
GO

exec insertLanguageInterface 'en', 'Expenses claims', N'Expenses claims', 'expense'
exec insertLanguageInterface 'ru', 'Expenses claims', N'Заявления о расходах', 'expense'
exec insertLanguageInterface 'uk', 'Expenses claims', N'Заяви про витрати', 'expense'
GO
------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------

------------------------------------------------------------------------------------------------------------------------
---https://teamsoft.atlassian.net/browse/UKEKSPO-31
------------------------------------------------------------------------------------------------------------------------
exec insertLanguageInterface  'en', 'Are you sure you want to delete this members?', N'Are you sure you want to delete this members?', 'mcm'
exec insertLanguageInterface  'ru', 'Are you sure you want to delete this members?', N'Вы уверены, что хотите удалить этих участников?', 'mcm'
exec insertLanguageInterface  'uk', 'Are you sure you want to delete this members?', N'Ви впевнені, що хочете видалити цих учасників?', 'mcm'
GO
exec insertLanguageInterface  'en', 'Confirm', N'Confirm', 'mcm'
exec insertLanguageInterface  'ru', 'Confirm', N'Подтвердить', 'mcm'
exec insertLanguageInterface  'uk', 'Confirm', N'Підтвердити', 'mcm'
GO
exec insertLanguageInterface  'en', 'Cancel', N'Cancel', 'mcm'
exec insertLanguageInterface  'ru', 'Cancel', N'Отмена', 'mcm'
exec insertLanguageInterface  'uk', 'Cancel', N'Відмінити', 'mcm'
GO
------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------


------------------------------------------------------------------------------------------------------------------------
--- https://teamsoft.atlassian.net/browse/HERBS-94
------------------------------------------------------------------------------------------------------------------------

exec insertLanguageInterface 'en', 'File can''t be greater than 2GB', N'File can''t be greater than 2GB'
exec insertLanguageInterface 'ru', 'File can''t be greater than 2GB', N'Файл не может быть более 2GB'
exec insertLanguageInterface 'uk', 'File can''t be greater than 2GB', N'Файл не може бути більшим за 2GB'
GO

------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------


------------------------------------------------------------------------------------------------------------------------
--- add translation
------------------------------------------------------------------------------------------------------------------------
exec insertLanguageInterface 'ru', 'Access to DCR', N'Доступ к DCR', 'messages';
exec insertLanguageInterface 'uk', 'Access to DCR', N'Доступ до DCR', 'messages';
exec insertLanguageInterface 'en', 'Access to DCR', N'Access to DCR', 'messages';

exec insertLanguageInterface 'ru', 'Hide CRM from Homepage', N'Скрыть CRM с домашней страницы', 'messages';
exec insertLanguageInterface 'uk', 'Hide CRM from Homepage', N'Сховати CRM з домашньої сторінки', 'messages';
exec insertLanguageInterface 'en', 'Hide CRM from Homepage', N'Hide CRM from Homepage', 'messages';

exec insertLanguageInterface 'ru', 'Access to MCM Content', N'Доступ к MCM контент', 'messages';
exec insertLanguageInterface 'uk', 'Access to MCM Content', N'Доступ до MCM контент', 'messages';
exec insertLanguageInterface 'en', 'Access to MCM Content', N'Access to MCM Content', 'messages';

exec insertLanguageInterface 'ru', 'Access to MCM Target', N'Доступ к MCM целевая аудитория', 'messages';
exec insertLanguageInterface 'uk', 'Access to MCM Target', N'Доступ до MCM цільова аудиторія', 'messages';
exec insertLanguageInterface 'en', 'Access to MCM Target', N'Access to MCM Target', 'messages';

exec insertLanguageInterface 'ru', 'Access to MCM Distribution', N'Доступ к MCM рассылки', 'messages';
exec insertLanguageInterface 'uk', 'Access to MCM Distribution', N'Доступ до MCM розсилки', 'messages';
exec insertLanguageInterface 'en', 'Access to MCM Distribution', N'Access to MCM Distribution', 'messages';

exec insertLanguageInterface 'ru', 'Access to MCM Report', N'Доступ к MCM отчеты', 'messages';
exec insertLanguageInterface 'uk', 'Access to MCM Report', N'Доступ до MCM звіти', 'messages';
exec insertLanguageInterface 'en', 'Access to MCM Report', N'Access to MCM Report', 'messages';

exec insertLanguageInterface 'ru', 'Access to MCM Conference', N'Доступ к MCM конференции', 'messages';
exec insertLanguageInterface 'uk', 'Access to MCM Conference', N'Доступ до MCM конференції', 'messages';
exec insertLanguageInterface 'en', 'Access to MCM Conference', N'Access to MCM Conference', 'messages';

exec insertLanguageInterface 'ru', 'Access to expenses line1', N'Доступ к расходам линия 1', 'messages';
exec insertLanguageInterface 'uk', 'Access to expenses line1', N'Доступ до витрат лінія 1', 'messages';
exec insertLanguageInterface 'en', 'Access to expenses line1', N'Access to expenses line1', 'messages';

exec insertLanguageInterface 'ru', 'Access to expenses line2', N'Доступ к расходам линия 2', 'messages';
exec insertLanguageInterface 'uk', 'Access to expenses line2', N'Доступ до витрат лінія 2', 'messages';
exec insertLanguageInterface 'en', 'Access to expenses line2', N'Access to expenses line2', 'messages';


exec insertLanguageInterface 'ru', 'Access to surveys manager', N'Доступ к редактированию анкет', 'messages';
exec insertLanguageInterface 'uk', 'Access to surveys manager', N'Доступ до редагування анкет', 'messages';
exec insertLanguageInterface 'en', 'Access to surveys manager', N'Access to surveys manager', 'messages';
GO
------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------
