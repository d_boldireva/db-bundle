----------------------------------------------------------------------------------------------------
-- https://teamsoft.atlassian.net/browse/PHARMAWEB-623
----------------------------------------------------------------------------------------------------
exec insertLanguageInterface 'en', 'Polygon created', N'Polygon created', 'geomarketing';
exec insertLanguageInterface 'ru', 'Polygon created', N'Полигон создан', 'geomarketing';
exec insertLanguageInterface 'uk', 'Polygon created', N'Полігон створений', 'geomarketing';
GO
----------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------------------------------------------------------------------------
------- https://teamsoft.atlassian.net/browse/GEODATA-853
----------------------------------------------------------------------------------------------------------------------------------------------------------------------
exec insertLanguageInterface 'en', 'Hide user in users structure in GeoForce', N'Hide user in users structure in GeoForce', 'messages';
exec insertLanguageInterface 'ru', 'Hide user in users structure in GeoForce', N'Скрыть пользователя из рег. структуры в GeoForce', 'messages';
exec insertLanguageInterface 'uk', 'Hide user in users structure in GeoForce', N'Приховати користувача iз рег. структури у GeoForce', 'messages';
GO
----------------------------------------------------------------------------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------
-- https://teamsoft.atlassian.net/browse/PHARMAWEB-578
----------------------------------------------------------------------------------------------------
/*IF (NOT EXISTS(SELECT *
               FROM sys.indexes
               WHERE name = 'code_unique_constraint'))
BEGIN
ALTER TABLE info_role ADD CONSTRAINT code_unique_constraint UNIQUE(code)
END*/

exec insertLanguageInterface 'en', 'The value of the field "{{field_name}}" already exists', N'The value of the field "{{field_name}}" already exists', 'validators';
exec insertLanguageInterface 'ru', 'The value of the field "{{field_name}}" already exists', N'Значение поля "{{field_name}}" уже существует', 'validators';
exec insertLanguageInterface 'uk', 'The value of the field "{{field_name}}" already exists', N'Значення поля "{{field_name}}" вже існує', 'validators';
GO
----------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------
-- https://teamsoft.atlassian.net/browse/DRREDDYS-709
----------------------------------------------------------------------------------------------------
exec insertLanguageInterface 'en', 'Question potential', N'Question potential', 'crm';
exec insertLanguageInterface 'ru', 'Question potential', N'Вопрос потенциал', 'crm';
exec insertLanguageInterface 'uk', 'Question potential', N'Питання потенціал', 'crm';

exec insertLanguageInterface 'en', 'Question plan', N'Question plan', 'crm';
exec insertLanguageInterface 'ru', 'Question plan', N'Вопрос план', 'crm';
exec insertLanguageInterface 'uk', 'Question plan', N'Питання план', 'crm';

exec insertLanguageInterface 'en', 'Question fact', N'Question fact', 'crm';
exec insertLanguageInterface 'ru', 'Question fact', N'Вопрос факт', 'crm';
exec insertLanguageInterface 'uk', 'Question fact', N'Питання факт', 'crm';
GO
----------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------
------- https://teamsoft.atlassian.net/browse/MCM-469
------------------------------------------------------------------------------------------------------------------------
exec insertLanguageInterface
    'en',
    'The file type is prohibited. Pending format {{ file_type }}',
    'The file type is prohibited. Pending format {{ file_type }}',
    'validators';
exec insertLanguageInterface
    'ru',
    'The file type is prohibited. Pending format {{ file_type }}',
    'Тип файла запрещен. Ожидает формат {{ file_type }}',
    'validators';
exec insertLanguageInterface
    'uk',
    'The file type is prohibited. Pending format {{ file_type }}',
    'Тип файлу заборонений. Очікуваний формат {{ file_type }}',
    'validators';


exec insertLanguageInterface
    'en',
    'The video type is prohibited. Pending format {{ file_type }}',
    'The video type is prohibited. Pending format {{ file_type }}',
    'validators';
exec insertLanguageInterface
    'ru',
    'The video type is prohibited. Pending format {{ file_type }}',
    'Тип видео запрещен. Ожидающий формат {{ file_type }}',
    'validators';
exec insertLanguageInterface
    'uk',
    'The video type is prohibited. Pending format {{ file_type }}',
    'Тип відео заборонено. Очікуваний формат {{ file_type }}',
    'validators';
GO
------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------
------- https://teamsoft.atlassian.net/browse/NESTLERU-63
------------------------------------------------------------------------------------------------------------------------
IF NOT EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE table_name = 'po_gridconfigbyrole' AND column_name = 'field_width')
begin
    ALTER TABLE po_gridconfigbyrole ADD [field_width] INT NULL
end
GO
IF NOT EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE table_name = 'po_gridconfig' AND column_name = 'field_width')
begin
    ALTER TABLE po_gridconfig ADD [field_width] INT NULL
end
GO
IF NOT EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE table_name = 'po_gridconfigbyuser' AND column_name = 'field_width')
begin
    ALTER TABLE po_gridconfigbyuser ADD [field_width] INT NULL
end
GO

exec insertLanguageInterface 'en', 'Field width', 'Field width', 'messages';
exec insertLanguageInterface 'ru', 'Field width', 'Ширина колонки', 'messages';
exec insertLanguageInterface 'uk', 'Field width', 'Ширина колонки', 'messages';

exec insertLanguageInterface 'en', 'Field width', 'Field width', 'crm';
exec insertLanguageInterface 'ru', 'Field width', 'Ширина колонки', 'crm';
exec insertLanguageInterface 'uk', 'Field width', 'Ширина колонки', 'crm';
GO
------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------
-- https://teamsoft.atlassian.net/browse/UKEKSPO-45
------------------------------------------------------------------------------------------------------------------------
exec insertLanguageInterface  'en', 'Bulk accesses', N'Bulk accesses', 'messages'
exec insertLanguageInterface  'ru', 'Bulk accesses', N'Массовые доступы', 'messages'
exec insertLanguageInterface  'uk', 'Bulk accesses', N'Масові доступи', 'messages'
GO

exec insertLanguageInterface  'en', 'Access to PDF', N'Access to PDF', 'messages'
exec insertLanguageInterface  'ru', 'Access to PDF', N'Доступ к PDF', 'messages'
exec insertLanguageInterface  'uk', 'Access to PDF', N'Доступ до PDF', 'messages'
GO

exec insertLanguageInterface  'en', 'Access to video', N'Access to video', 'messages'
exec insertLanguageInterface  'ru', 'Access to video', N'Доступ к видео', 'messages'
exec insertLanguageInterface  'uk', 'Access to video', N'Доступ до відео', 'messages'
GO

exec insertLanguageInterface  'en', 'Access to conference', N'Access to conference', 'messages'
exec insertLanguageInterface  'ru', 'Access to conference', N'Доступ к конференции', 'messages'
exec insertLanguageInterface  'uk', 'Access to conference', N'Доступ до конференції', 'messages'
GO

exec insertLanguageInterface  'en', 'Full access', N'Full access', 'messages'
exec insertLanguageInterface  'ru', 'Full access', N'Полный доступ', 'messages'
exec insertLanguageInterface  'uk', 'Full access', N'Повний доступ', 'messages'
GO

exec insertLanguageInterface  'en', 'Data not saved. Do you want to save data?', N'Data not saved. Do you want to save data?', 'messages'
exec insertLanguageInterface  'ru', 'Data not saved. Do you want to save data?', N'Данные не сохранены. Хотите сохранить данные?', 'messages'
exec insertLanguageInterface  'uk', 'Data not saved. Do you want to save data?', N'Дані не збережено. Бажаєте зберегти дані?', 'messages'
GO

exec insertLanguageInterface  'en', 'Token not received. An error occurred while processing a contact', N'Token not received. An error occurred while processing a contact', 'messages'
exec insertLanguageInterface  'ru', 'Token not received. An error occurred while processing a contact', N'Токен не получен. Возникла ошибка при обработке контакта', 'messages'
exec insertLanguageInterface  'uk', 'Token not received. An error occurred while processing a contact', N'Токен не отримано. Виникла помилка при обробці контакту', 'messages'
GO
----------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------
-- https://teamsoft.atlassian.net/browse/FARMAK-355
----------------------------------------------------------------------------------------------------
exec insertLanguageInterface 'en', 'Individual settings are valid until', N'Individual settings are valid until', 'messages';
exec insertLanguageInterface 'ru', 'Individual settings are valid until', N'Индивидуальные настройки действуют до', 'messages';
exec insertLanguageInterface 'uk', 'Individual settings are valid until', N'Індивідуальні налаштування діють до', 'messages';

exec insertLanguageInterface 'en', 'Choose date', N'Choose date', 'messages';
exec insertLanguageInterface 'ru', 'Choose date', N'Выберите дату', 'messages';
exec insertLanguageInterface 'uk', 'Choose date', N'Виберіть дату', 'messages';
GO
----------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------------------------
-- https://teamsoft.atlassian.net/browse/ER-5
---------------------------------------------------------------------------------------------------------------------
if not exists(SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'info_contactsign' AND Column_Name = 'archive_date')
begin
    alter table info_contactsign add archive_date DATETIME NULL
end
if not exists(SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'info_contactsign' AND Column_Name = 'isarchive')
begin
    alter table info_contactsign add isarchive INT NULL
end
GO

exec insertLanguageInterface 'en', 'Withdraw consent', 'Withdraw consent', 'crm';
exec insertLanguageInterface 'ru', 'Withdraw consent', 'Отозвать согласие', 'crm';
exec insertLanguageInterface 'uk', 'Withdraw consent', 'Відкликати згоду', 'crm';

exec insertLanguageInterface 'en', 'Date of obtaining consent', 'Date of obtaining consent', 'crm';
exec insertLanguageInterface 'ru', 'Date of obtaining consent', 'Дата получения согласия', 'crm';
exec insertLanguageInterface 'uk', 'Date of obtaining consent', 'Дата отримання згоди', 'crm';

exec insertLanguageInterface 'en', 'Date of obtaining consent', 'Date of obtaining consent', 'messages';
exec insertLanguageInterface 'ru', 'Date of obtaining consent', 'Дата получения согласия', 'messages';
exec insertLanguageInterface 'uk', 'Date of obtaining consent', 'Дата отримання згоди', 'messages';

exec insertLanguageInterface 'en', 'Withdraw consent', 'Withdraw consent', 'messages';
exec insertLanguageInterface 'ru', 'Withdraw consent', 'Отозвать согласие', 'messages';
exec insertLanguageInterface 'uk', 'Withdraw consent', 'Відкликати згоду', 'messages';

exec insertLanguageInterface 'en', 'Are you sure you want to revoke your consent?', 'Are you sure you want to revoke your consent?', 'crm';
exec insertLanguageInterface 'ru', 'Are you sure you want to revoke your consent?', 'Вы уверены, что хотите отозвать свое согласие?', 'crm';
exec insertLanguageInterface 'uk', 'Are you sure you want to revoke your consent?', 'Ви впевнені, що хочете відкликати свою згоду?', 'crm';

exec insertPoGridconfig 2, 'contact_sign_createdate', 'Date of obtaining consent', 0;
exec insertPoEntityFields 2, 'contact_sign_createdate', 'Date of obtaining consent';
exec insertPoEntityFields 2, 'contact_sign_withdraw', 'Withdraw consent';
GO
------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------------------------------------------------------------------------
------- https://teamsoft.atlassian.net/browse/NAN-56
----------------------------------------------------------------------------------------------------------------------------------------------------------------------
IF EXISTS (SELECT 1 FROM information_schema.columns WHERE table_name='po_gridconfigbyrole' and column_name = 'sortable')
BEGIN
    UPDATE po_gridconfigbyrole SET sortable = is_shown;
    exec sp_rename 'po_gridconfigbyrole.sortable',  'is_shown_dcr', 'COLUMN';
END;
GO
IF EXISTS (SELECT 1 FROM information_schema.columns WHERE table_name='po_gridconfig' and column_name = 'sortable')
BEGIN
    UPDATE po_gridconfig SET sortable = is_shown;
    exec sp_rename 'po_gridconfig.sortable',  'is_shown_dcr', 'COLUMN';
END;
GO
IF EXISTS (SELECT 1 FROM information_schema.columns WHERE table_name='po_gridconfigbyuser' and column_name = 'sortable')
BEGIN
    UPDATE po_gridconfigbyuser SET sortable = is_shown;
    exec sp_rename 'po_gridconfigbyuser.sortable',  'is_shown_dcr', 'COLUMN';
END;
GO
----------------------------------------------------------------------------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------------------------------------------------------
--https://teamsoft.atlassian.net/browse/ALKALOIDRU-41
---------------------------------------------------------------------------------------------------------------------------------------------------
exec insertLanguageInterface 'uk', 'Birthdate', N'Дата народження', 'crm'
exec insertLanguageInterface 'ru', 'Birthdate', N'Дата рождения', 'crm'
exec insertLanguageInterface 'en', 'Birthdate', N'Birthdate', 'crm'
GO
----------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------
------- https://teamsoft.atlassian.net/browse/FERONRU-99
------------------------------------------------------------------------------------------------------------------------
exec insertLanguageInterface 'en', 'Hide archived', 'Hide archived', 'messages';
exec insertLanguageInterface 'ru', 'Hide archived', 'Скрыть архивные', 'messages';
exec insertLanguageInterface 'uk', 'Hide archived', 'Приховати архівні', 'messages';

exec insertLanguageInterface 'en', 'Show archived', 'Show archived', 'messages';
exec insertLanguageInterface 'ru', 'Show archived', 'Показать архивные', 'messages';
exec insertLanguageInterface 'uk', 'Show archived', 'Показати архівні', 'messages';
GO
------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------
------- https://teamsoft.atlassian.net/browse/PHARMAWEB-649
------------------------------------------------------------------------------------------------------------------------
IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'info_mcmconferencemember' AND COLUMN_NAME = 'member_role_id')
BEGIN
    ALTER TABLE info_mcmconferencemember ADD member_role_id int null;

    ALTER TABLE info_mcmconferencemember
        ADD CONSTRAINT fk_info_mcmconferencemember_member_role_id FOREIGN KEY (member_role_id)
            REFERENCES info_CustomDictionaryValue (id);
END;
GO
------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------
--fix
------------------------------------------------------------------------------------------------------------------------
exec insertLanguageInterface 'en', 'Please, select at least one filter', N'Please, select at least one filter', 'validators';
exec insertLanguageInterface 'en', 'Please, select at least one filter', N'Пожалуйста, выберите как минимум один фильтр', 'validators';
exec insertLanguageInterface 'uk', 'Please, select at least one filter', N'Будь ласка, виберіть принаймні один фільтр', 'validators';
GO
------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------
-- https://teamsoft.atlassian.net/browse/UKEKSPO-77
----------------------------------------------------------------------------------------------------
exec insertLanguageInterface 'en', 'Less than', 'Less than', 'ag-grid';
exec insertLanguageInterface 'ru', 'Less than', 'Меньше', 'ag-grid';
exec insertLanguageInterface 'uk', 'Less than', 'Менше', 'ag-grid';

exec insertLanguageInterface 'en', 'Greater than', 'Greater than', 'ag-grid';
exec insertLanguageInterface 'ru', 'Greater than', 'Больше', 'ag-grid';
exec insertLanguageInterface 'uk', 'Greater than', 'Більше', 'ag-grid';

exec insertLanguageInterface 'en', 'In range', 'In range', 'ag-grid';
exec insertLanguageInterface 'ru', 'In range', 'Диапазон', 'ag-grid';
exec insertLanguageInterface 'uk', 'In range', 'Діапазон', 'ag-grid';

exec insertLanguageInterface 'en', 'andCondition', 'AND', 'ag-grid';
exec insertLanguageInterface 'ru', 'andCondition', 'И', 'ag-grid';
exec insertLanguageInterface 'uk', 'andCondition', 'І', 'ag-grid';

exec insertLanguageInterface 'en', 'orCondition', 'OR', 'ag-grid';
exec insertLanguageInterface 'ru', 'orCondition', 'ИЛИ', 'ag-grid';
exec insertLanguageInterface 'uk', 'orCondition', 'АБО', 'ag-grid';

exec insertLanguageInterface 'en', 'Update date', 'Update date', 'mcm';
exec insertLanguageInterface 'ru', 'Update date', 'Дата обновления', 'mcm';
exec insertLanguageInterface 'uk', 'Update date', 'Дата оновлення', 'mcm';
GO
----------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------
