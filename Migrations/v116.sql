-------------------------------------------------------------------------------------------------------
----- https://teamsoft.atlassian.net/browse/BOXVERSION-96
-------------------------------------------------------------------------------------------------------
IF EXISTS (SELECT * FROM sysobjects WHERE id = object_id(N'insertLanguageInterface') AND xtype IN (N'P'))
    DROP PROCEDURE insertLanguageInterface
GO

CREATE PROCEDURE insertLanguageInterface(@slug VARCHAR(2), @key VARCHAR(255), @translation VARCHAR(255), @domain VARCHAR(255) = 'messages')
    AS
    BEGIN
        DECLARE @language_id int;
        SELECT TOP 1 @language_id = id FROM info_language WHERE slug = @slug

        INSERT INTO info_languageinterface ([key], translation, language_id, domain)
        SELECT TOP 1 @key, @translation, @language_id, @domain
        WHERE NOT EXISTS(SELECT 1
                         FROM info_languageinterface
                         WHERE [key] = @key AND language_id = @language_id AND domain = @domain)
    END
GO

exec insertLanguageInterface 'ru', 'Create report', N'Создать отчет', 'crm'
GO
exec insertLanguageInterface 'ru', 'Edit report', N'Редактировать отчет', 'crm'
GO
exec insertLanguageInterface 'uk', 'Create report', N'Створити звіт', 'crm'
GO
exec insertLanguageInterface 'uk', 'Edit report', N'Редагувати звіт', 'crm'
GO
exec insertLanguageInterface 'ru', 'Title', N'Название', 'crm'
GO
exec insertLanguageInterface 'uk', 'Title', N'Назва', 'crm'
GO
exec insertLanguageInterface 'ru', 'Description', N'Описание', 'crm'
GO
exec insertLanguageInterface 'uk', 'Description', N'Опис', 'crm'
GO
exec insertLanguageInterface 'ru', 'Platforms', N'Платформы', 'crm'
GO
exec insertLanguageInterface 'uk', 'Platforms', N'Платформи', 'crm'
GO
exec insertLanguageInterface 'ru', 'Save', N'Сохранить', 'crm'
GO
exec insertLanguageInterface 'uk', 'Save', N'Зберегти', 'crm'
GO
exec insertLanguageInterface 'ru', 'Add', N'Добавить', 'crm'
GO
exec insertLanguageInterface 'uk', 'Add', N'Додати', 'crm'
GO
delete
from info_languageinterface
where [key] = 'CANCEL'
  and domain = 'crm'
GO
exec insertLanguageInterface 'uk', 'Cancel', N'Відмінити', 'crm'
GO
exec insertLanguageInterface 'ru', 'Cancel', N'Отмена', 'crm'
GO
exec insertLanguageInterface 'uk', 'Remove', N'Видалити', 'crm'
GO
exec insertLanguageInterface 'ru', 'Remove', N'Удалить', 'crm'
GO
exec insertLanguageInterface 'ru', 'Preview', N'Предпросмотр', 'crm'
GO
exec insertLanguageInterface 'uk', 'Preview', N'Перегляд', 'crm'
GO
exec insertLanguageInterface 'ru', 'Reports not found', N'Отчеты не найдены', 'crm'
GO
exec insertLanguageInterface 'uk', 'Reports not found', N'Звіти не знайдені', 'crm'
GO
exec insertLanguageInterface 'ru', 'Main view', N'Главная таблица', 'crm'
GO
exec insertLanguageInterface 'uk', 'Main view', N'Головна таблиця', 'crm'
GO
exec insertLanguageInterface 'ru', 'Additional views', N'Дополнительные таблицы', 'crm'
GO
exec insertLanguageInterface 'uk', 'Additional views', N'Додаткові таблиці', 'crm'
GO
exec insertLanguageInterface 'ru', 'Available for mobile', N'Доступно в приложении', 'crm'
GO
exec insertLanguageInterface 'uk', 'Available for mobile', N'Доступно в додатку', 'crm'
GO
exec insertLanguageInterface 'ru', 'Available for web', N'Доступно на сайте', 'crm'
GO
exec insertLanguageInterface 'uk', 'Available for web', N'Доступно на сайті', 'crm'
GO
exec insertLanguageInterface 'ru', 'Edit tab', N'Редактировать вкладку', 'crm'
GO
exec insertLanguageInterface 'uk', 'Edit tab', N'Редагувати вкладку', 'crm'
GO
exec insertLanguageInterface 'ru', 'View filters', N'Фильтры', 'crm'
GO
exec insertLanguageInterface 'uk', 'View filters', N'Фільтри', 'crm'
GO
exec insertLanguageInterface 'ru', 'Access to report', N'Доступ к отчету', 'crm'
GO
exec insertLanguageInterface 'uk', 'Access to report', N'Доступ до звіту', 'crm'
GO
exec insertLanguageInterface 'ru', 'Are you sure, you want to delete selected tab?',
     N'Вы уверены, что хотите удалить выбранную вкладку?', 'crm'
GO
exec insertLanguageInterface 'uk', 'Are you sure, you want to delete selected tab?',
     N'Ви впевнені, що хочете видалити вибрану вкладку?', 'crm'
GO
exec insertLanguageInterface 'ru', 'Settings', N'Настройки', 'crm'
exec insertLanguageInterface 'uk', 'Settings', N'Налаштування', 'crm'

exec insertLanguageInterface 'ru', 'Edit', N'Редактировать', 'crm'
exec insertLanguageInterface 'uk', 'Edit', N'Редагувати', 'crm'

IF OBJECT_ID(N'[info_slicegroup]', 'U') IS NULL
    BEGIN
        CREATE TABLE [dbo].[info_slicegroup]
        (
            [id]          [int] IDENTITY (1,1) NOT NULL,
            [name]        [varchar](255)       NULL,
            [position]    [int]                NULL,
            [currenttime] [datetime]           NULL,
            [time]        [timestamp]          NULL,
            [moduser]     [varchar](16)        NULL,
            [guid]        [uniqueidentifier]   NULL,
            CONSTRAINT [PK_info_slicegroup] PRIMARY KEY CLUSTERED
                (
                 [id] ASC
                    ) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
        ) ON [PRIMARY]

        ALTER TABLE [dbo].[info_slicegroup]
            ADD CONSTRAINT [DF_info_slicegroupcurrenttime] DEFAULT (getdate()) FOR [currenttime]
        ALTER TABLE [dbo].[info_slicegroup]
            ADD CONSTRAINT [DF_info_slicegroupmoduser] DEFAULT ([dbo].[Get_CurrentCode]()) FOR [moduser]
        ALTER TABLE [dbo].[info_slicegroup]
            ADD CONSTRAINT [DF_info_slicegroupguid] DEFAULT (newid()) FOR [guid]
    END
GO

IF OBJECT_ID(N'[info_slice]', 'U') IS NULL
    BEGIN
        CREATE TABLE [dbo].[info_slice]
        (
            [id]          [int] IDENTITY (1,1) NOT NULL,
            [client_sql]  [varchar](max)       NULL,
            [server_sql]  [varchar](max)       NULL,
            [name]        [varchar](255)       NULL,
            [code]        [varchar](255)       NULL,
            [group_id]    [int]                NULL,
            [currenttime] [datetime]           NULL,
            [time]        [timestamp]          NULL,
            [moduser]     [varchar](16)        NULL,
            [guid]        [uniqueidentifier]   NULL,
            CONSTRAINT [PK_info_slice] PRIMARY KEY CLUSTERED
                (
                 [id] ASC
                    ) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
        ) ON [PRIMARY]

        create unique index info_slice_code_uindex
            on info_slice (code)

        ALTER TABLE [dbo].[info_slice]
            WITH CHECK ADD CONSTRAINT [FK_info_slice_group_id] FOREIGN KEY ([group_id])
                REFERENCES [dbo].[info_slicegroup] ([id])

        ALTER TABLE [dbo].[info_slice]
            ADD CONSTRAINT [DF_info_slicecurrenttime] DEFAULT (getdate()) FOR [currenttime]
        ALTER TABLE [dbo].[info_slice]
            ADD CONSTRAINT [DF_info_slicemoduser] DEFAULT ([dbo].[Get_CurrentCode]()) FOR [moduser]
        ALTER TABLE [dbo].[info_slice]
            ADD CONSTRAINT [DF_info_sliceguid] DEFAULT (newid()) FOR [guid]
    END
GO

IF OBJECT_ID(N'[info_slicefield]', 'U') IS NULL
    BEGIN
        CREATE TABLE [dbo].[info_slicefield]
        (
            [id]          [int] IDENTITY (1,1) NOT NULL,
            [name]        [varchar](255)       NULL,
            [field]       [varchar](255)       NULL,
            [slice_id]    [int]                NULL,
            [currenttime] [datetime]           NULL,
            [time]        [timestamp]          NULL,
            [moduser]     [varchar](16)        NULL,
            [guid]        [uniqueidentifier]   NULL,
            CONSTRAINT [PK_info_slicefield] PRIMARY KEY CLUSTERED
                (
                 [id] ASC
                    ) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
        ) ON [PRIMARY]

        ALTER TABLE [dbo].[info_slicefield]
            WITH CHECK ADD CONSTRAINT [FK_info_slicefield_slice_id] FOREIGN KEY ([slice_id])
                REFERENCES [dbo].[info_slice] ([id])

        ALTER TABLE [dbo].[info_slicefield]
            ADD CONSTRAINT [DF_info_slicefieldcurrenttime] DEFAULT (getdate()) FOR [currenttime]
        ALTER TABLE [dbo].[info_slicefield]
            ADD CONSTRAINT [DF_info_slicefieldmoduser] DEFAULT ([dbo].[Get_CurrentCode]()) FOR [moduser]
        ALTER TABLE [dbo].[info_slicefield]
            ADD CONSTRAINT [DF_info_slicefieldguid] DEFAULT (newid()) FOR [guid]
    END
GO

IF OBJECT_ID(N'[info_reporttab]', 'U') IS NULL
    BEGIN
        CREATE TABLE [dbo].[info_reporttab]
        (
            [id]          [int] IDENTITY (1,1) NOT NULL,
            [name]        [varchar](255)       NULL,
            [position]    [int]                NULL,
            [report_id]   [int]                NULL,
            [slice_id]    [int]                NULL,
            [currenttime] [datetime]           NULL,
            [time]        [timestamp]          NULL,
            [moduser]     [varchar](16)        NULL,
            [guid]        [uniqueidentifier]   NULL,
            CONSTRAINT [PK_info_reporttab] PRIMARY KEY CLUSTERED
                (
                 [id] ASC
                    ) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
        ) ON [PRIMARY]

        ALTER TABLE [dbo].[info_reporttab]
            WITH CHECK ADD CONSTRAINT [FK_info_reporttab_report_id] FOREIGN KEY ([report_id])
                REFERENCES [dbo].[info_report] ([id])

        ALTER TABLE [dbo].[info_reporttab]
            WITH CHECK ADD CONSTRAINT [FK_info_reporttab_slice_id] FOREIGN KEY ([slice_id])
                REFERENCES [dbo].[info_slice] ([id])

        ALTER TABLE [dbo].[info_reporttab]
            ADD CONSTRAINT [DF_info_reporttabcurrenttime] DEFAULT (getdate()) FOR [currenttime]
        ALTER TABLE [dbo].[info_reporttab]
            ADD CONSTRAINT [DF_info_reporttabmoduser] DEFAULT ([dbo].[Get_CurrentCode]()) FOR [moduser]
        ALTER TABLE [dbo].[info_reporttab]
            ADD CONSTRAINT [DF_info_reporttabguid] DEFAULT (newid()) FOR [guid]
    END
GO

IF OBJECT_ID(N'[info_slicefilterdictionary]', 'U') IS NULL
    BEGIN
        CREATE TABLE [dbo].[info_slicefilterdictionary]
        (
            [id]                  [int] IDENTITY (1,1) NOT NULL,
            [name]                [varchar](255)       NULL,
            [field_data_type]     [int]                NULL,
            [is_required]         [int]                NULL,
            [client_custom_where] [varchar](max)       NULL,
            [server_custom_where] [varchar](max)       NULL,
            [systemdictionary_id] [int]                NULL,
            [customdictionary_id] [int]                NULL,
            [currenttime]         [datetime]           NULL,
            [time]                [timestamp]          NULL,
            [moduser]             [varchar](16)        NULL,
            [guid]                [uniqueidentifier]   NULL,
            CONSTRAINT [PK_info_slicefilterdictionary] PRIMARY KEY CLUSTERED
                (
                 [id] ASC
                    ) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
        ) ON [PRIMARY]

        ALTER TABLE [dbo].[info_slicefilterdictionary]
            WITH CHECK ADD CONSTRAINT [FK_info_slicefilterdictionary_customdictionary_id] FOREIGN KEY ([customdictionary_id])
                REFERENCES [dbo].[info_customdictionary] ([id])

        ALTER TABLE [dbo].[info_slicefilterdictionary]
            WITH CHECK ADD CONSTRAINT [FK_info_slicefilterdictionary_systemdictionary_id] FOREIGN KEY ([systemdictionary_id])
                REFERENCES [dbo].[info_systemdictionary] ([id])

        ALTER TABLE [dbo].[info_slicefilterdictionary]
            ADD CONSTRAINT [DF_info_slicefilterdictionarycurrenttime] DEFAULT (getdate()) FOR [currenttime]
        ALTER TABLE [dbo].[info_slicefilterdictionary]
            ADD CONSTRAINT [DF_info_slicefilterdictionarymoduser] DEFAULT ([dbo].[Get_CurrentCode]()) FOR [moduser]
        ALTER TABLE [dbo].[info_slicefilterdictionary]
            ADD CONSTRAINT [DF_info_slicefilterdictionaryguid] DEFAULT (newid()) FOR [guid]
    END
GO

IF OBJECT_ID(N'[info_slicefilter]', 'U') IS NULL
    BEGIN
        CREATE TABLE [dbo].[info_slicefilter]
        (
            [id]                    [int] IDENTITY (1,1) NOT NULL,
            [client_filter]         [varchar](max)       NULL,
            [server_filter]         [varchar](max)       NULL,
            [client_default_filter] [varchar](max)       NULL,
            [server_default_filter] [varchar](max)       NULL,
            [slice_id]              [int]                NULL,
            [filter_id]             [int]                NULL,
            [currenttime]           [datetime]           NULL,
            [time]                  [timestamp]          NULL,
            [moduser]               [varchar](16)        NULL,
            [guid]                  [uniqueidentifier]   NULL,
            CONSTRAINT [PK_info_slicefilter] PRIMARY KEY CLUSTERED
                (
                 [id] ASC
                    ) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
        ) ON [PRIMARY]

        ALTER TABLE [dbo].[info_slicefilter]
            WITH CHECK ADD CONSTRAINT [FK_info_slicefilter_slice_id] FOREIGN KEY ([slice_id])
                REFERENCES [dbo].[info_slice] ([id])

        ALTER TABLE [dbo].[info_slicefilter]
            WITH CHECK ADD CONSTRAINT [FK_info_slicefilter_filter_id] FOREIGN KEY ([filter_id])
                REFERENCES [dbo].[info_slicefilterdictionary] ([id])

        ALTER TABLE [dbo].[info_slicefilter]
            ADD CONSTRAINT [DF_info_slicefiltercurrenttime] DEFAULT (getdate()) FOR [currenttime]
        ALTER TABLE [dbo].[info_slicefilter]
            ADD CONSTRAINT [DF_info_slicefiltermoduser] DEFAULT ([dbo].[Get_CurrentCode]()) FOR [moduser]
        ALTER TABLE [dbo].[info_slicefilter]
            ADD CONSTRAINT [DF_info_slicefilterguid] DEFAULT (newid()) FOR [guid]
    END
GO

IF OBJECT_ID(N'[info_slicerelation]', 'U') IS NULL
    BEGIN
        CREATE TABLE [dbo].[info_slicerelation]
        (
            [id]           [int] IDENTITY (1,1) NOT NULL,
            [parent_id]    [int]                NOT NULL,
            [child_id]     [int]                NOT NULL,
            [parent_field] [varchar](255)       NOT NULL,
            [child_field]  [varchar](255)       NOT NULL,
            [currenttime]  [datetime]           NULL,
            [time]         [timestamp]          NULL,
            [moduser]      [varchar](16)        NULL,
            [guid]         [uniqueidentifier]   NULL,
            CONSTRAINT [PK_info_slicerelation] PRIMARY KEY CLUSTERED
                (
                 [id] ASC
                    ) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
        ) ON [PRIMARY]

        ALTER TABLE [dbo].[info_slicerelation]
            WITH CHECK ADD CONSTRAINT [FK_info_slicerelation_parent_id] FOREIGN KEY ([parent_id])
                REFERENCES [dbo].[info_slice] ([id])

        ALTER TABLE [dbo].[info_slicerelation]
            WITH CHECK ADD CONSTRAINT [FK_info_slicerelation_child_id] FOREIGN KEY ([child_id])
                REFERENCES [dbo].[info_slice] ([id])

        ALTER TABLE [dbo].[info_slicerelation]
            ADD CONSTRAINT [DF_info_slicerelationcurrenttime] DEFAULT (getdate()) FOR [currenttime]
        ALTER TABLE [dbo].[info_slicerelation]
            ADD CONSTRAINT [DF_info_slicerelationmoduser] DEFAULT ([dbo].[Get_CurrentCode]()) FOR [moduser]
        ALTER TABLE [dbo].[info_slicerelation]
            ADD CONSTRAINT [DF_info_slicerelationguid] DEFAULT (newid()) FOR [guid]
    END
GO

IF OBJECT_ID(N'[info_reporttabslice]', 'U') IS NULL
    BEGIN
        CREATE TABLE [dbo].[info_reporttabslice]
        (
            [id]           [int] IDENTITY (1,1) NOT NULL,
            [reporttab_id] [int]                NOT NULL,
            [slice_id]     [int]                NOT NULL,
            [currenttime]  [datetime]           NULL,
            [time]         [timestamp]          NULL,
            [moduser]      [varchar](16)        NULL,
            [guid]         [uniqueidentifier]   NULL,
            CONSTRAINT [PK_info_reporttabslice] PRIMARY KEY CLUSTERED
                (
                 [id] ASC
                    ) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
        ) ON [PRIMARY]

        ALTER TABLE [dbo].[info_reporttabslice]
            WITH CHECK ADD CONSTRAINT [FK_info_reporttabslice_reporttab_id] FOREIGN KEY ([reporttab_id])
                REFERENCES [dbo].[info_reporttab] ([id])

        ALTER TABLE [dbo].[info_reporttabslice]
            WITH CHECK ADD CONSTRAINT [FK_info_reporttabslice_slice_id] FOREIGN KEY ([slice_id])
                REFERENCES [dbo].[info_slice] ([id])

        ALTER TABLE [dbo].[info_reporttabslice]
            ADD CONSTRAINT [DF_info_reporttabslicecurrenttime] DEFAULT (getdate()) FOR [currenttime]
        ALTER TABLE [dbo].[info_reporttabslice]
            ADD CONSTRAINT [DF_info_reporttabslicemoduser] DEFAULT ([dbo].[Get_CurrentCode]()) FOR [moduser]
        ALTER TABLE [dbo].[info_reporttabslice]
            ADD CONSTRAINT [DF_info_reporttabsliceguid] DEFAULT (newid()) FOR [guid]
    END
GO

IF OBJECT_ID(N'[info_reporttabfield]', 'U') IS NULL
    BEGIN
        CREATE TABLE [dbo].[info_reporttabfield]
        (
            [id]           [int] IDENTITY (1,1) NOT NULL,
            [reporttab_id] [int]                NOT NULL,
            [field_id]     [int]                NULL,
            [group_by]     [int]                NULL,
            [position]     [int]                NULL,
            [width]        [int]                NULL,
            [visible]      [int]                NULL,
            [name]         [varchar](255)       NULL,
            [formula]      [varchar](255)       NULL,
            [aggregation]  [varchar](255)       NULL,
            [sorting]      [varchar](255)       NULL,
            [currenttime]  [datetime]           NULL,
            [time]         [timestamp]          NULL,
            [moduser]      [varchar](16)        NULL,
            [guid]         [uniqueidentifier]   NULL,
            CONSTRAINT [PK_info_reporttabfield] PRIMARY KEY CLUSTERED
                (
                 [id] ASC
                    ) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
        ) ON [PRIMARY]

        ALTER TABLE [dbo].[info_reporttabfield]
            WITH CHECK ADD CONSTRAINT [FK_info_reporttabfield_reporttab_id] FOREIGN KEY ([reporttab_id])
                REFERENCES [dbo].[info_reporttab] ([id])

        ALTER TABLE [dbo].[info_reporttabfield]
            WITH CHECK ADD CONSTRAINT [FK_info_reporttabfield_field_id] FOREIGN KEY ([field_id])
                REFERENCES [dbo].[info_slicefield] ([id])

        ALTER TABLE [dbo].[info_reporttabfield]
            ADD CONSTRAINT [DF_info_reporttabfieldcurrenttime] DEFAULT (getdate()) FOR [currenttime]
        ALTER TABLE [dbo].[info_reporttabfield]
            ADD CONSTRAINT [DF_info_reporttabfieldmoduser] DEFAULT ([dbo].[Get_CurrentCode]()) FOR [moduser]
        ALTER TABLE [dbo].[info_reporttabfield]
            ADD CONSTRAINT [DF_info_reporttabfieldguid] DEFAULT (newid()) FOR [guid]
    END
GO

IF OBJECT_ID(N'[info_reporttabfilter]', 'U') IS NULL
    BEGIN
        CREATE TABLE [dbo].[info_reporttabfilter]
        (
            [id]                [int] IDENTITY (1,1) NOT NULL,
            [reporttab_id]      [int]                NOT NULL,
            [slice_id]          [int]                NULL,
            [field_id]          [int]                NULL,
            [filter_id]         [int]                NULL,
            [client_filter_sql] [varchar](max)       NULL,
            [server_filter_sql] [varchar](max)       NULL,
            [currenttime]       [datetime]           NULL,
            [time]              [timestamp]          NULL,
            [moduser]           [varchar](16)        NULL,
            [guid]              [uniqueidentifier]   NULL,
            CONSTRAINT [PK_info_reporttabfilter] PRIMARY KEY CLUSTERED
                (
                 [id] ASC
                    ) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
        ) ON [PRIMARY]

        ALTER TABLE [dbo].[info_reporttabfilter]
            WITH CHECK ADD CONSTRAINT [FK_info_reporttabfilter_reporttab_id] FOREIGN KEY ([reporttab_id])
                REFERENCES [dbo].[info_reporttab] ([id])

        ALTER TABLE [dbo].[info_reporttabfilter]
            WITH CHECK ADD CONSTRAINT [FK_info_reporttabfilter_filter_id] FOREIGN KEY ([filter_id])
                REFERENCES [dbo].[info_slicefilter] ([id])

        ALTER TABLE [dbo].[info_reporttabfilter]
            WITH CHECK ADD CONSTRAINT [FK_info_reporttabfilter_slice_id] FOREIGN KEY (slice_id)
                REFERENCES [dbo].[info_slice] ([id])

        ALTER TABLE [dbo].[info_reporttabfilter]
            ADD CONSTRAINT [DF_info_reporttabfiltercurrenttime] DEFAULT (getdate()) FOR [currenttime]
        ALTER TABLE [dbo].[info_reporttabfilter]
            ADD CONSTRAINT [DF_info_reporttabfiltermoduser] DEFAULT ([dbo].[Get_CurrentCode]()) FOR [moduser]
        ALTER TABLE [dbo].[info_reporttabfilter]
            ADD CONSTRAINT [DF_info_reporttabfilterguid] DEFAULT (newid()) FOR [guid]
    END
GO
IF NOT EXISTS(SELECT 1
              FROM information_schema.columns
              WHERE table_name = 'info_report'
                AND column_name = 'slice_id')
    BEGIN
        ALTER TABLE info_report
            ADD slice_id INT null

        ALTER TABLE [dbo].[info_report]
            WITH CHECK ADD CONSTRAINT [FK_info_report_slice_id] FOREIGN KEY ([slice_id])
                REFERENCES [dbo].[info_slice] ([id])
    END
GO

IF NOT EXISTS(SELECT 1
              FROM information_schema.columns
              WHERE table_name = 'info_report'
                AND column_name = 'is_web')
    BEGIN
        ALTER TABLE info_report
            ADD is_web INT null
    END
GO

IF NOT EXISTS(SELECT 1
              FROM information_schema.columns
              WHERE table_name = 'info_report'
                AND column_name = 'is_mobile')
    BEGIN
        ALTER TABLE info_report
            ADD is_mobile INT null
    END
GO

IF NOT EXISTS(SELECT 1
              FROM information_schema.columns
              WHERE table_name = 'info_reportaccess'
                AND column_name = 'role_id')
    BEGIN
        ALTER TABLE info_reportaccess
            ADD role_id INT null

        ALTER TABLE [dbo].[info_reportaccess]
            WITH CHECK ADD CONSTRAINT [FK_info_reportaccess_role_id] FOREIGN KEY ([role_id])
                REFERENCES [dbo].[info_role] ([id])
    END
GO

exec insertLanguageInterface 'ru', 'Formulas', N'Формулы', 'ag-grid'
exec insertLanguageInterface 'uk', 'Formulas', N'Формули', 'ag-grid'
exec insertLanguageInterface 'ru', 'filterOoo', N'Фильтровать...', 'ag-grid'
exec insertLanguageInterface 'ru', 'equals', N'Равно', 'ag-grid'
exec insertLanguageInterface 'ru', 'notEqual', N'Не равно', 'ag-grid'
exec insertLanguageInterface 'ru', 'lessThan', N'Меньше', 'ag-grid'
exec insertLanguageInterface 'ru', 'greaterThan', N'Больше', 'ag-grid'
exec insertLanguageInterface 'ru', 'lessThanOrEqual', N'Меньше или равно', 'ag-grid'
exec insertLanguageInterface 'ru', 'greaterThanOrEqual', N'Больше или равно', 'ag-grid'
exec insertLanguageInterface 'ru', 'inRange', N'Диапазон', 'ag-grid'
exec insertLanguageInterface 'ru', 'contains', N'Содержит', 'ag-grid'
exec insertLanguageInterface 'ru', 'notContains', N'Не содержит', 'ag-grid'
exec insertLanguageInterface 'ru', 'startsWith', N'Начинается с', 'ag-grid'
exec insertLanguageInterface 'ru', 'endsWith', N'Заканчивается на', 'ag-grid'
exec insertLanguageInterface 'ru', 'andCondition', N'И', 'ag-grid'
exec insertLanguageInterface 'ru', 'orCondition', N'ИЛИ', 'ag-grid'
exec insertLanguageInterface 'ru', 'noRowsToShow', N'Нет данных', 'ag-grid'
exec insertLanguageInterface 'uk', 'filterOoo', N'Фільтрувати...', 'ag-grid'
exec insertLanguageInterface 'uk', 'equals', N'Дорівнює', 'ag-grid'
exec insertLanguageInterface 'uk', 'notEqual', N'Не дорівнює', 'ag-grid'
exec insertLanguageInterface 'uk', 'lessThan', N'Менше', 'ag-grid'
exec insertLanguageInterface 'uk', 'greaterThan', N'Більше', 'ag-grid'
exec insertLanguageInterface 'uk', 'lessThanOrEqual', N'Менше або дорівнює', 'ag-grid'
exec insertLanguageInterface 'uk', 'greaterThanOrEqual', N'Більше або дорівнює', 'ag-grid'
exec insertLanguageInterface 'uk', 'inRange', N'Діапазон', 'ag-grid'
exec insertLanguageInterface 'uk', 'contains', N'Містить', 'ag-grid'
exec insertLanguageInterface 'uk', 'notContains', N'Не містить', 'ag-grid'
exec insertLanguageInterface 'uk', 'startsWith', N'Починається с', 'ag-grid'
exec insertLanguageInterface 'uk', 'endsWith', N'Закінчується на', 'ag-grid'
exec insertLanguageInterface 'uk', 'andCondition', N'І', 'ag-grid'
exec insertLanguageInterface 'uk', 'orCondition', N'АБО', 'ag-grid'
exec insertLanguageInterface 'uk', 'noRowsToShow', N'Немає даних', 'ag-grid'

-- validators
exec insertLanguageInterface 'ru', 'Please, add, at least, one tab to report', N'Добавьте, минимум, одну вкладку',
     'validators'
exec insertLanguageInterface 'uk', 'Please, add, at least, one tab to report', N'Додайте хоча б одну вкладку',
     'validators'
exec insertLanguageInterface 'ru', 'View as', N'Посмотреть как', 'crm'
exec insertLanguageInterface 'uk', 'View as', N'Переглянути як', 'crm'
exec insertLanguageInterface 'ru', 'Tab', N'Вкладка', 'crm'
exec insertLanguageInterface 'uk', 'Tab', N'Вкладка', 'crm'
exec insertLanguageInterface 'ru', 'Start date', N'Начало', 'crm'
exec insertLanguageInterface 'uk', 'Start date', N'Початок', 'crm'
exec insertLanguageInterface 'ru', 'End date', N'Конец', 'crm'
exec insertLanguageInterface 'uk', 'End date', N'Кінець', 'crm'
exec insertLanguageInterface 'ru', 'Select filters', N'Выберите фильтры', 'crm'
exec insertLanguageInterface 'uk', 'Select filters', N'Оберіть фильтри', 'crm'
exec insertLanguageInterface 'ru', 'This filter cannot be empty', N'Фильтр не может быть пустым ', 'crm'
exec insertLanguageInterface 'uk', 'This filter cannot be empty', N'Фільтр не може бути пустим', 'crm'
exec insertLanguageInterface 'ru', 'Back to report list', N'Вернуться к списку отчетов', 'crm'
exec insertLanguageInterface 'uk', 'Back to report list', N'Повернутись до списку звітів', 'crm'

INSERT INTO info_serviceprivilege (service_id, code, name, description, parent_id)
VALUES ((SELECT TOP 1 id FROM info_service WHERE identifier = 'crm'),
        'ROLE_ACCESS_TO_REPORT_MANAGER',
        'Access to reports designer', null,
        (SELECT TOP 1 id FROM info_serviceprivilege WHERE code = 'ROLE_ACCESS_TO_CRM'));
GO
--------------------------------------------------------------------------------------------------------
--------------------------------------------------------------------------------------------------------

--------------------------------------------------------------------------------------------------------
-- https://teamsoft.atlassian.net/browse/MCM-306
--------------------------------------------------------------------------------------------------------
IF NOT EXISTS (SELECT 1 FROM information_schema.columns WHERE table_name = 'info_mcmdistribution' AND column_name = 'iswebinar')
    BEGIN
        ALTER TABLE info_mcmdistribution ADD iswebinar INT NULL
    END
GO
IF NOT EXISTS (SELECT 1 FROM information_schema.columns WHERE table_name = 'info_mcmdistribution' AND column_name = 'isactive')
    BEGIN
        ALTER TABLE info_mcmdistribution ADD isactive INT NULL
    END
GO

exec insertLanguageInterface 'ru', 'Webinars', N'Вебинары'
go
exec insertLanguageInterface 'uk', 'Webinars', N'Вебінари'
go
exec insertLanguageInterface 'ru', 'Webinars not found', N'Вебинары не найдены'
go
exec insertLanguageInterface 'uk', 'Webinars not found', N'Вебінари не знайдено'
go
exec insertLanguageInterface 'ru', 'Webinar', N'Вебинар'
go
exec insertLanguageInterface 'uk', 'Webinar', N'Вебінар'
go
exec insertLanguageInterface 'ru', 'Webinar name', N'Название вебинара'
go
exec insertLanguageInterface 'uk', 'Webinar name', N'Назва вебінару'
go
exec insertLanguageInterface 'ru', 'Webinar date', N'Период вебинара'
go
exec insertLanguageInterface 'uk', 'Webinar date', N'Період вебінару'
go
exec insertLanguageInterface 'en', 'webinarSigningError', N'This service is unavailable. Please, contact ProximaReserch manager for more details.'
go
exec insertLanguageInterface 'ru', 'webinarSigningError', N'Данный функционал вам пока не доступен. Но не расстраивайтесь, вы можете обратиться к менеджеру ProximaReserch для получении детальной информации о подключении инструмента «Вебинары»'
go
exec insertLanguageInterface 'uk', 'webinarSigningError', N'Функцінал недоступний. Зверніться до менеджера ProximaReserch для отримання інформації щодо підключення інструменту «Вебінари»'
go

-------------------------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------------------

--------------------------------------------------------------------------------------------------------
-- https://teamsoft.atlassian.net/browse/PHARMAWEB-279
--------------------------------------------------------------------------------------------------------
IF NOT EXISTS (SELECT 1 FROM information_schema.columns WHERE table_name = 'info_addinfoactiontype' AND column_name = 'role_id')
    BEGIN
        ALTER TABLE info_addinfoactiontype ADD role_id INT NULL
        ALTER TABLE [dbo].[info_addinfoactiontype]
            WITH CHECK ADD CONSTRAINT [FK_info_addinfoactiontype_role_id] FOREIGN KEY ([role_id])
                REFERENCES [dbo].[info_role] ([id])
    END
GO
IF NOT EXISTS (SELECT 1 FROM information_schema.columns WHERE table_name = 'info_addinfotasktype' AND column_name = 'role_id')
    BEGIN
        ALTER TABLE info_addinfotasktype ADD role_id INT NULL
        ALTER TABLE [dbo].[info_addinfotasktype]
            WITH CHECK ADD CONSTRAINT [FK_info_addinfotasktype_role_id] FOREIGN KEY ([role_id])
                REFERENCES [dbo].[info_role] ([id])
    END
GO
IF NOT EXISTS (SELECT 1 FROM information_schema.columns WHERE table_name = 'info_addinfocompanytype' AND column_name = 'role_id')
    BEGIN
        ALTER TABLE info_addinfocompanytype ADD role_id INT NULL
        ALTER TABLE [dbo].[info_addinfocompanytype]
            WITH CHECK ADD CONSTRAINT [FK_info_addinfocompanytype_role_id] FOREIGN KEY ([role_id])
                REFERENCES [dbo].[info_role] ([id])
    END
GO
IF NOT EXISTS (SELECT 1 FROM information_schema.columns WHERE table_name = 'info_addinfocontacttype' AND column_name = 'role_id')
    BEGIN
        ALTER TABLE info_addinfocontacttype ADD role_id INT NULL
        ALTER TABLE [dbo].[info_addinfocontacttype]
            WITH CHECK ADD CONSTRAINT [FK_info_addinfocontacttype_role_id] FOREIGN KEY ([role_id])
                REFERENCES [dbo].[info_role] ([id])
    END
GO

IF NOT EXISTS(SELECT 1
              FROM information_schema.columns
              WHERE table_name = 'info_addinfotypebyrole'
                AND column_name = 'currenttime')
    BEGIN
        ALTER TABLE info_addinfotypebyrole
            ADD [currenttime] [datetime] NULL
        ALTER TABLE [dbo].[info_addinfotypebyrole]
            ADD CONSTRAINT [DF_info_addinfotypebyrolecurrenttime] DEFAULT (getdate()) FOR [currenttime]
    END
GO

IF NOT EXISTS(SELECT 1
              FROM information_schema.columns
              WHERE table_name = 'info_addinfotypebyrole'
                AND column_name = 'moduser')
    BEGIN
        ALTER TABLE info_addinfotypebyrole
            ADD [moduser] [varchar](16) NULL
        ALTER TABLE [dbo].[info_addinfotypebyrole]
            ADD CONSTRAINT [DF_info_addinfotypebyrolemoduser] DEFAULT ([dbo].[Get_CurrentCode]()) FOR [moduser]
    END
GO

IF NOT EXISTS(SELECT 1
              FROM information_schema.columns
              WHERE table_name = 'info_addinfotypebyrole'
                AND column_name = 'guid')
    BEGIN
        ALTER TABLE info_addinfotypebyrole
            ADD [guid] [uniqueidentifier] NULL
        ALTER TABLE [dbo].[info_addinfotypebyrole]
            ADD CONSTRAINT [DF_info_addinfotypebyroleguid] DEFAULT (newid()) FOR [guid]
    END
GO

IF NOT EXISTS(SELECT 1
              FROM information_schema.columns
              WHERE table_name = 'info_addinfotypebyrole'
                AND column_name = 'time')
    BEGIN
        ALTER TABLE info_addinfotypebyrole
            ADD [time] [timestamp] NULL
    END
GO
IF NOT EXISTS(SELECT 1
              FROM information_schema.columns
              WHERE table_name = 'info_tasktypebyrole'
                AND column_name = 'currenttime')
    BEGIN
        ALTER TABLE info_tasktypebyrole
            ADD [currenttime] [datetime] NULL
        ALTER TABLE [dbo].[info_tasktypebyrole]
            ADD CONSTRAINT [DF_info_tasktypebyrolecurrenttime] DEFAULT (getdate()) FOR [currenttime]
    END
GO

IF NOT EXISTS(SELECT 1
              FROM information_schema.columns
              WHERE table_name = 'info_tasktypebyrole'
                AND column_name = 'moduser')
    BEGIN
        ALTER TABLE info_tasktypebyrole
            ADD [moduser] [varchar](16) NULL
        ALTER TABLE [dbo].[info_tasktypebyrole]
            ADD CONSTRAINT [DF_info_tasktypebyrolemoduser] DEFAULT ([dbo].[Get_CurrentCode]()) FOR [moduser]
    END
GO

IF NOT EXISTS(SELECT 1
              FROM information_schema.columns
              WHERE table_name = 'info_tasktypebyrole'
                AND column_name = 'guid')
    BEGIN
        ALTER TABLE info_tasktypebyrole
            ADD [guid] [uniqueidentifier] NULL
        ALTER TABLE [dbo].[info_tasktypebyrole]
            ADD CONSTRAINT [DF_info_tasktypebyroleguid] DEFAULT (newid()) FOR [guid]
    END
GO

IF NOT EXISTS(SELECT 1
              FROM information_schema.columns
              WHERE table_name = 'info_tasktypebyrole'
                AND column_name = 'time')
    BEGIN
        ALTER TABLE info_tasktypebyrole
            ADD [time] [timestamp] NULL
    END
GO
IF NOT EXISTS(SELECT 1
              FROM information_schema.columns
              WHERE table_name = 'info_actiontypebyrole'
                AND column_name = 'currenttime')
    BEGIN
        ALTER TABLE info_actiontypebyrole
            ADD [currenttime] [datetime] NULL
        ALTER TABLE [dbo].[info_actiontypebyrole]
            ADD CONSTRAINT [DF_info_actiontypebyrolecurrenttime] DEFAULT (getdate()) FOR [currenttime]
    END
GO

IF NOT EXISTS(SELECT 1
              FROM information_schema.columns
              WHERE table_name = 'info_actiontypebyrole'
                AND column_name = 'moduser')
    BEGIN
        ALTER TABLE info_actiontypebyrole
            ADD [moduser] [varchar](16) NULL
        ALTER TABLE [dbo].[info_actiontypebyrole]
            ADD CONSTRAINT [DF_info_actiontypebyrolemoduser] DEFAULT ([dbo].[Get_CurrentCode]()) FOR [moduser]
    END
GO

IF NOT EXISTS(SELECT 1
              FROM information_schema.columns
              WHERE table_name = 'info_actiontypebyrole'
                AND column_name = 'guid')
    BEGIN
        ALTER TABLE info_actiontypebyrole
            ADD [guid] [uniqueidentifier] NULL
        ALTER TABLE [dbo].[info_actiontypebyrole]
            ADD CONSTRAINT [DF_info_actiontypebyroleguid] DEFAULT (newid()) FOR [guid]
    END
GO

IF NOT EXISTS(SELECT 1
              FROM information_schema.columns
              WHERE table_name = 'info_actiontypebyrole'
                AND column_name = 'time')
    BEGIN
        ALTER TABLE info_actiontypebyrole
            ADD [time] [timestamp] NULL
    END
GO

IF NOT EXISTS(SELECT 1
              FROM information_schema.columns
              WHERE table_name = 'info_contacttypebyrole'
                AND column_name = 'currenttime')
    BEGIN
        ALTER TABLE info_contacttypebyrole
            ADD [currenttime] [datetime] NULL
        ALTER TABLE [dbo].[info_contacttypebyrole]
            ADD CONSTRAINT [DF_info_contacttypebyrolecurrenttime] DEFAULT (getdate()) FOR [currenttime]
    END
GO

IF NOT EXISTS(SELECT 1
              FROM information_schema.columns
              WHERE table_name = 'info_contacttypebyrole'
                AND column_name = 'moduser')
    BEGIN
        ALTER TABLE info_contacttypebyrole
            ADD [moduser] [varchar](16) NULL
        ALTER TABLE [dbo].[info_contacttypebyrole]
            ADD CONSTRAINT [DF_info_contacttypebyrolemoduser] DEFAULT ([dbo].[Get_CurrentCode]()) FOR [moduser]
    END
GO

IF NOT EXISTS(SELECT 1
              FROM information_schema.columns
              WHERE table_name = 'info_contacttypebyrole'
                AND column_name = 'guid')
    BEGIN
        ALTER TABLE info_contacttypebyrole
            ADD [guid] [uniqueidentifier] NULL
        ALTER TABLE [dbo].[info_contacttypebyrole]
            ADD CONSTRAINT [DF_info_contacttypebyroleguid] DEFAULT (newid()) FOR [guid]
    END
GO

IF NOT EXISTS(SELECT 1
              FROM information_schema.columns
              WHERE table_name = 'info_contacttypebyrole'
                AND column_name = 'time')
    BEGIN
        ALTER TABLE info_contacttypebyrole
            ADD [time] [timestamp] NULL
    END
GO

IF NOT EXISTS(SELECT 1
              FROM information_schema.columns
              WHERE table_name = 'info_companytypebyrole'
                AND column_name = 'currenttime')
    BEGIN
        ALTER TABLE info_companytypebyrole
            ADD [currenttime] [datetime] NULL
        ALTER TABLE [dbo].[info_companytypebyrole]
            ADD CONSTRAINT [DF_info_companytypebyrolecurrenttime] DEFAULT (getdate()) FOR [currenttime]
    END
GO

IF NOT EXISTS(SELECT 1
              FROM information_schema.columns
              WHERE table_name = 'info_companytypebyrole'
                AND column_name = 'moduser')
    BEGIN
        ALTER TABLE info_companytypebyrole
            ADD [moduser] [varchar](16) NULL
        ALTER TABLE [dbo].[info_companytypebyrole]
            ADD CONSTRAINT [DF_info_companytypebyrolemoduser] DEFAULT ([dbo].[Get_CurrentCode]()) FOR [moduser]
    END
GO

IF NOT EXISTS(SELECT 1
              FROM information_schema.columns
              WHERE table_name = 'info_companytypebyrole'
                AND column_name = 'guid')
    BEGIN
        ALTER TABLE info_companytypebyrole
            ADD [guid] [uniqueidentifier] NULL
        ALTER TABLE [dbo].[info_companytypebyrole]
            ADD CONSTRAINT [DF_info_companytypebyroleguid] DEFAULT (newid()) FOR [guid]
    END
GO

IF NOT EXISTS(SELECT 1
              FROM information_schema.columns
              WHERE table_name = 'info_companytypebyrole'
                AND column_name = 'time')
    BEGIN
        ALTER TABLE info_companytypebyrole
            ADD [time] [timestamp] NULL
    END
GO


exec insertLanguageInterface 'ru', 'visible by default', N'отображается по умолчанию'
exec insertLanguageInterface 'uk', 'visible by default', N'відображається за замовчуванням'

-------------------------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------------------------

---------------------------------------------------------------------------------------------------------------------------------------------------
-- v-16
--Переводы
---------------------------------------------------------------------------------------------------------------------------------------------------

--Админка
exec insertLanguageInterface 'uk', 'Services', N'Сервіси', 'messages'
exec insertLanguageInterface 'ru', 'Services', N'Сервисы', 'messages'
exec insertLanguageInterface 'en', 'Services', N'Services', 'messages'

exec insertLanguageInterface 'uk', 'Settings', N'Параметри', 'messages'
exec insertLanguageInterface 'ru', 'Settings', N'Настройки', 'messages'
exec insertLanguageInterface 'en', 'Settings', N'Settings', 'messages'

exec insertLanguageInterface 'uk', 'Group by', N'Групування', 'messages'
exec insertLanguageInterface 'ru', 'Group by', N'Группировка', 'messages'
exec insertLanguageInterface 'en', 'Group by', N'Group by', 'messages'

exec insertLanguageInterface 'uk', 'direction', N'Прод. напрямок', 'messages'
exec insertLanguageInterface 'ru', 'direction', N'Прод. направление', 'messages'
exec insertLanguageInterface 'en', 'direction', N'Prod. direction', 'messages'

exec insertLanguageInterface 'uk', 'territory', N'Територія', 'messages'
exec insertLanguageInterface 'ru', 'territory', N'Территория', 'messages'
exec insertLanguageInterface 'en', 'territory', N'Territory', 'messages'

exec insertLanguageInterface 'uk', 'User not selected', N'Користувач не обраний', 'messages'
exec insertLanguageInterface 'ru', 'User not selected', N'Пользователь не выбран', 'messages'
exec insertLanguageInterface 'en', 'User not selected', N'User not selected', 'messages'

exec insertLanguageInterface 'uk', 'Hide inactive', N'Приховати неактивних', 'messages'
exec insertLanguageInterface 'ru', 'Hide inactive', N'Скрыть неактивных', 'messages'
exec insertLanguageInterface 'en', 'Hide inactive', N'Hide inactive', 'messages'

exec insertLanguageInterface 'uk', 'Expand All', N'Розгорнути все', 'messages'
exec insertLanguageInterface 'ru', 'Expand All', N'Развернуть все', 'messages'
exec insertLanguageInterface 'en', 'Expand All', N'Expand All', 'messages'

exec insertLanguageInterface 'uk', 'General information', N'Основна інформація', 'messages'
exec insertLanguageInterface 'ru', 'General information', N'Основная информация', 'messages'
exec insertLanguageInterface 'en', 'General information', N'General information', 'messages'

exec insertLanguageInterface 'uk', 'Role', N'Роль', 'messages'
exec insertLanguageInterface 'ru', 'Role', N'Роль', 'messages'
exec insertLanguageInterface 'en', 'Role', N'Role', 'messages'

exec insertLanguageInterface 'uk', 'Product directions', N'Продуктові напрямки', 'messages'
exec insertLanguageInterface 'ru', 'Product directions', N'Продуктовые направления', 'messages'
exec insertLanguageInterface 'en', 'Product directions', N'Product directions', 'messages'

exec insertLanguageInterface 'uk', 'Primary IMEI', N'Первинний IMEI', 'messages'
exec insertLanguageInterface 'ru', 'Primary IMEI', N'Основной IMEI', 'messages'
exec insertLanguageInterface 'en', 'Primary IMEI', N'Primary IMEI', 'messages'

exec insertLanguageInterface 'uk', 'Mobile', N'Мобільний телефон', 'messages'
exec insertLanguageInterface 'ru', 'Mobile', N'Мобильный телефон', 'messages'
exec insertLanguageInterface 'en', 'Mobile', N'Mobile phone', 'messages'

exec insertLanguageInterface 'uk', 'Responsibility Coefficient', N'Коефіцієнт відповідальності', 'messages'
exec insertLanguageInterface 'ru', 'Responsibility Coefficient', N'Коэффициент ответственности', 'messages'
exec insertLanguageInterface 'en', 'Responsibility Coefficient', N'Responsibility Coefficient', 'messages'

exec insertLanguageInterface 'uk', 'Language', N'Мова', 'messages'
exec insertLanguageInterface 'ru', 'Language', N'Язык', 'messages'
exec insertLanguageInterface 'en', 'Language', N'Language', 'messages'

exec insertLanguageInterface 'uk', 'External ID', N'Зовнішній ідентифікатор користувача', 'messages'
exec insertLanguageInterface 'ru', 'External ID', N'Внешний идентификатор пользователя', 'messages'
exec insertLanguageInterface 'en', 'External ID', N'External ID', 'messages'

exec insertLanguageInterface 'uk', 'Territory of work', N'Територія роботи', 'messages'
exec insertLanguageInterface 'ru', 'Territory of work', N'Территория работы', 'messages'
exec insertLanguageInterface 'en', 'Territory of work', N'Work area', 'messages'

exec insertLanguageInterface 'uk', 'Copy settings', N'Копіювати параметри', 'messages'
exec insertLanguageInterface 'ru', 'Copy settings', N'Копировать настройки', 'messages'
exec insertLanguageInterface 'en', 'Copy settings', N'Copy settings', 'messages'

exec insertLanguageInterface 'uk', 'Directions', N'Напрямки', 'messages'
exec insertLanguageInterface 'ru', 'Directions', N'Направления', 'messages'
exec insertLanguageInterface 'en', 'Directions', N'Directions', 'messages'

exec insertLanguageInterface 'uk', 'Show', N'Показати', 'messages'
exec insertLanguageInterface 'ru', 'Show', N'Показать', 'messages'
exec insertLanguageInterface 'en', 'Show', N'Show', 'messages'

exec insertLanguageInterface 'uk', 'Not Working', N'Не працює', 'messages'
exec insertLanguageInterface 'ru', 'Not Working', N'Не работает', 'messages'
exec insertLanguageInterface 'en', 'Not Working', N'Not Working', 'messages'

exec insertLanguageInterface 'uk', 'Changes will be lost. Do you want to save changes?', N'Зміни будуть загублені. Зберегти зміни?', 'messages'
exec insertLanguageInterface 'ru', 'Changes will be lost. Do you want to save changes?', N'Изменения будут утеряны. Сохранить изменения?', 'messages'
exec insertLanguageInterface 'en', 'Changes will be lost. Do you want to save changes?', N'Changes will be lost. Do you want to save changes?', 'messages'

exec insertLanguageInterface 'uk', 'Drop', N'Скасувати', 'messages'
exec insertLanguageInterface 'ru', 'Drop', N'Сбросить', 'messages'
exec insertLanguageInterface 'en', 'Drop', N'Drop', 'messages'

exec insertLanguageInterface 'uk', 'Send data on email', N'Надіслати облікові дані на пошту', 'messages'
exec insertLanguageInterface 'ru', 'Send data on email', N'Отправить учетные данные на почту', 'messages'
exec insertLanguageInterface 'en', 'Send data on email', N'Send user data on email', 'messages'

exec insertLanguageInterface 'uk', 'Selected user', N'Обраного користувача', 'messages'
exec insertLanguageInterface 'ru', 'Selected user', N'Выбранного пользователя', 'messages'
exec insertLanguageInterface 'en', 'Selected user', N'Selected user', 'messages'

exec insertLanguageInterface 'uk', 'GMT', N'Часовий пояс', 'messages'
exec insertLanguageInterface 'ru', 'GMT', N'Часовой пояс', 'messages'
exec insertLanguageInterface 'en', 'GMT', N'GMT', 'messages'

exec insertLanguageInterface 'uk', 'Previous', N'Назад', 'messages'
exec insertLanguageInterface 'ru', 'Previous', N'Назад', 'messages'
exec insertLanguageInterface 'en', 'Previous', N'Previous', 'messages'

exec insertLanguageInterface 'uk', 'At the top is your profile. Where you can view or edit basic information about you.', N'Вгорі знаходиться ваш профіль. Де ви можете подивитися або редагувати базову інформацію про себе.', 'messages'
exec insertLanguageInterface 'ru', 'At the top is your profile. Where you can view or edit basic information about you.', N'Вверху находится ваш профиль. Где вы можете посмотреть или редактировать базовую информацию о себе.', 'messages'
exec insertLanguageInterface 'en', 'At the top is your profile. Where you can view or edit basic information about you.', N'At the top is your profile. Where you can view or edit basic information about you.', 'messages'

exec insertLanguageInterface 'uk', 'The main thing to remember is where to look.', N'Головне пам''ятати, де шукати.', 'messages'
exec insertLanguageInterface 'ru', 'The main thing to remember is where to look.', N'Главное помнить, где искать.', 'messages'
exec insertLanguageInterface 'en', 'The main thing to remember is where to look.', N'The main thing to remember is where to look', 'messages'

exec insertLanguageInterface 'uk', 'If you have forgotten something, tips are always at your fingertips, in the upper right corner of the screen!', N'Якщо ви що-небудь забули, підказки завжди у вас під рукою, у верхньому правому куті екрану!', 'messages'
exec insertLanguageInterface 'ru', 'If you have forgotten something, tips are always at your fingertips, in the upper right corner of the screen!', N'Если вы что-нибудь забыли, подсказки всегда у вас под рукой, в верхнем правом углу экрана!', 'messages'
exec insertLanguageInterface 'en', 'If you have forgotten something, tips are always at your fingertips, in the upper right corner of the screen!', N'If you have forgotten something, tips are always at your fingertips, in the upper right corner of the screen!', 'messages'

exec insertLanguageInterface 'uk', 'My profile', N'Мій профіль', 'messages'
exec insertLanguageInterface 'ru', 'My profile', N'Мой профиль', 'messages'
exec insertLanguageInterface 'en', 'My profile', N'My profile', 'messages'

exec insertLanguageInterface 'uk', 'Log out', N'Вихід', 'messages'
exec insertLanguageInterface 'ru', 'Log out', N'Выйти', 'messages'
exec insertLanguageInterface 'en', 'Log out', N'Log out', 'messages'

exec insertLanguageInterface 'uk', 'Birthdate', N'Дата народження', 'messages'
exec insertLanguageInterface 'ru', 'Birthdate', N'Дата рождения', 'messages'
exec insertLanguageInterface 'en', 'Birthdate', N'Birthdate', 'messages'

exec insertLanguageInterface 'uk', 'Add more', N'Додати ще', 'messages'
exec insertLanguageInterface 'ru', 'Add more', N'Добавить еще', 'messages'
exec insertLanguageInterface 'en', 'Add more', N'Add more', 'messages'

exec insertLanguageInterface 'uk', 'Общие', N'Загальні', 'messages'
exec insertLanguageInterface 'ru', 'Общие', N'Общие', 'messages'
exec insertLanguageInterface 'en', 'Общие', N'General', 'messages'

exec insertLanguageInterface 'uk', 'Визиты', N'Візити', 'messages'
exec insertLanguageInterface 'ru', 'Визиты', N'Визиты', 'messages'
exec insertLanguageInterface 'en', 'Визиты', N'Tasks', 'messages'

exec insertLanguageInterface 'uk', 'Задачи', N'Завдання', 'messages'
exec insertLanguageInterface 'ru', 'Задачи', N'Задачи', 'messages'
exec insertLanguageInterface 'en', 'Задачи', N'Actions', 'messages'

exec insertLanguageInterface 'uk', 'Клиенты', N'Клієнти', 'messages'
exec insertLanguageInterface 'ru', 'Клиенты', N'Клиенты', 'messages'
exec insertLanguageInterface 'en', 'Клиенты', N'Clients', 'messages'

exec insertLanguageInterface 'uk', 'Локализация', N'Локалізація', 'messages'
exec insertLanguageInterface 'ru', 'Локализация', N'Локализация', 'messages'
exec insertLanguageInterface 'en', 'Локализация', N'Localization', 'messages'

exec insertLanguageInterface 'uk', 'Here are collected all the additional services developed for you.', N'Тут зібрані всі додаткові сервіси розроблені для вас.', 'messages'
exec insertLanguageInterface 'ru', 'Here are collected all the additional services developed for you.', N'Тут собраны все дополнительные сервисы разработанные для вас.', 'messages'
exec insertLanguageInterface 'en', 'Here are collected all the additional services developed for you.', N'Here are collected all the additional services developed for you.', 'messages'

exec insertLanguageInterface 'uk', 'Препараты', N'Препарати', 'messages'
exec insertLanguageInterface 'ru', 'Препараты', N'Препараты', 'messages'
exec insertLanguageInterface 'en', 'Препараты', N'Preparations', 'messages'

exec insertLanguageInterface 'uk', 'Промо-материалы', N'Промо-матеріали', 'messages'
exec insertLanguageInterface 'ru', 'Промо-материалы', N'Промо-материалы', 'messages'
exec insertLanguageInterface 'en', 'Промо-материалы', N'Promotional materials', 'messages'

exec insertLanguageInterface 'uk', 'Учреждения', N'Установи', 'messages'
exec insertLanguageInterface 'ru', 'Учреждения', N'Учреждения', 'messages'
exec insertLanguageInterface 'en', 'Учреждения', N'Companies', 'messages'

exec insertLanguageInterface 'uk', 'Add row', N'Додати запис', 'messages'
exec insertLanguageInterface 'ru', 'Add row', N'Добавить запись', 'messages'
exec insertLanguageInterface 'en', 'Add row', N'Add note', 'messages'

exec insertLanguageInterface 'uk', 'Companies', N'Установи', 'messages'
exec insertLanguageInterface 'ru', 'Companies', N'Учреждения', 'messages'
exec insertLanguageInterface 'en', 'Companies', N'Companies', 'messages'

exec insertLanguageInterface 'uk', 'Contacts', N'Клієнти', 'messages'
exec insertLanguageInterface 'ru', 'Contacts', N'Клиенты', 'messages'
exec insertLanguageInterface 'en', 'Contacts', N'Clients', 'messages'

exec insertLanguageInterface 'uk', 'Actions', N'Завдання', 'messages'
exec insertLanguageInterface 'ru', 'Actions', N'Задачи', 'messages'
exec insertLanguageInterface 'en', 'Actions', N'Actions', 'messages'

exec insertLanguageInterface 'uk', 'Privileges', N'Привілеї', 'messages'
exec insertLanguageInterface 'ru', 'Privileges', N'Привилегии', 'messages'
exec insertLanguageInterface 'en', 'Privileges', 'Privileges', 'messages'

exec insertLanguageInterface 'uk', 'Control', N'Контроль', 'messages'
exec insertLanguageInterface 'ru', 'Control', N'Контроль', 'messages'
exec insertLanguageInterface 'en', 'Control', N'Control', 'messages'

exec insertLanguageInterface 'uk', 'Agreement', N'Домовленість', 'messages'
exec insertLanguageInterface 'ru', 'Agreement', N'Договоренность', 'messages'
exec insertLanguageInterface 'en', 'Agreement', N'Agreement', 'messages'

exec insertLanguageInterface 'uk', 'Select all', N'Обрати все', 'messages'
exec insertLanguageInterface 'ru', 'Select all', N'Выбрать все', 'messages'
exec insertLanguageInterface 'en', 'Select all', N'Select all', 'messages'

exec insertLanguageInterface 'uk', 'Deselect all', N'Зняти все', 'messages'
exec insertLanguageInterface 'ru', 'Deselect all', N'Снять все', 'messages'
exec insertLanguageInterface 'en', 'Deselect all', N'Deselect all', 'messages'

exec insertLanguageInterface 'uk', 'Company types', N'Типи установ', 'messages'
exec insertLanguageInterface 'ru', 'Company types', N'Типы учреждений', 'messages'
exec insertLanguageInterface 'en', 'Company types', N'Company types', 'messages'

exec insertLanguageInterface 'uk', 'Code', N'Код', 'messages'
exec insertLanguageInterface 'ru', 'Code', N'Код', 'messages'
exec insertLanguageInterface 'en', 'Code', N'Code', 'messages'

exec insertLanguageInterface 'uk', 'Contact types', N'Типи клієнтів', 'messages'
exec insertLanguageInterface 'ru', 'Contact types', N'Типы клиентов', 'messages'
exec insertLanguageInterface 'en', 'Contact types', N'Client types', 'messages'

exec insertLanguageInterface 'uk', 'Task types', N'Типи візитів', 'messages'
exec insertLanguageInterface 'ru', 'Task types', N'Типы визитов', 'messages'
exec insertLanguageInterface 'en', 'Task types', N'Task types', 'messages'

exec insertLanguageInterface 'uk', 'Action types', N'Типи завдань', 'messages'
exec insertLanguageInterface 'ru', 'Action types', N'Типы задач', 'messages'
exec insertLanguageInterface 'en', 'Action types', N'Action types', 'messages'

exec insertLanguageInterface 'uk', 'General fields', N'Основні поля', 'messages'
exec insertLanguageInterface 'ru', 'General fields', N'Основные поля', 'messages'
exec insertLanguageInterface 'en', 'General fields', N'General fields', 'messages'

exec insertLanguageInterface 'uk', 'Field marked as hidden', N'Поле позначено як приховане', 'messages'
exec insertLanguageInterface 'ru', 'Field marked as hidden', N'Поле помечено как скрытое', 'messages'
exec insertLanguageInterface 'en', 'Field marked as hidden', N'Field marked as hidden', 'messages'

exec insertLanguageInterface 'uk', 'Field marked as visible', N'Поле позначено як видиме', 'messages'
exec insertLanguageInterface 'ru', 'Field marked as visible', N'Поле помечено как видимое', 'messages'
exec insertLanguageInterface 'en', 'Field marked as visible', N'Field marked as visible', 'messages'

exec insertLanguageInterface 'uk', 'Data type', N'Тип даних', 'messages'
exec insertLanguageInterface 'ru', 'Data type', N'Тип данных', 'messages'
exec insertLanguageInterface 'en', 'Data type', N'Data type', 'messages'

exec insertLanguageInterface 'uk', 'For all types', N'Для усіх типів', 'messages'
exec insertLanguageInterface 'ru', 'For all types', N'Для всех типов', 'messages'
exec insertLanguageInterface 'en', 'For all types', N'For all types', 'messages'

exec insertLanguageInterface 'uk', 'Disable editing', N'Заборона редагування', 'messages'
exec insertLanguageInterface 'ru', 'Disable editing', N'Запрет редактирования', 'messages'
exec insertLanguageInterface 'en', 'Disable editing', N'Disable editing', 'messages'

exec insertLanguageInterface 'uk', 'Create', N'Створити', 'messages'
exec insertLanguageInterface 'ru', 'Create', N'Создать', 'messages'
exec insertLanguageInterface 'en', 'Create', N'Create', 'messages'

exec insertLanguageInterface 'uk', 'Select blocks to be copied', N'Виберіть блоки для копіювання', 'messages'
exec insertLanguageInterface 'ru', 'Select blocks to be copied', N'Выберите блоки для копирования', 'messages'
exec insertLanguageInterface 'en', 'Select blocks to be copied', N'Select blocks to be copied', 'messages'

exec insertLanguageInterface 'uk', 'Select type to copy on', N'Виберіть типи для копіювання', 'messages'
exec insertLanguageInterface 'ru', 'Select type to copy on', N'Выберите типы для копирования', 'messages'
exec insertLanguageInterface 'en', 'Select type to copy on', N'Select type to copy on', 'messages'

exec insertLanguageInterface 'uk', 'Please, choose at least one type to copy on', N'Будь ласка, виберіть, як мінімум один тип для копіювання', 'messages'
exec insertLanguageInterface 'ru', 'Please, choose at least one type to copy on', N'Пожалуйста, выберите, как минимум один тип для копирования', 'messages'
exec insertLanguageInterface 'en', 'Please, choose at least one type to copy on', N'Please, choose at least one type to copy on', 'messages'

exec insertLanguageInterface 'uk', 'Please, choose at least one group to be copied', N'Будь ласка, виберіть, хоча б, одну групу для копіювання', 'messages'
exec insertLanguageInterface 'ru', 'Please, choose at least one group to be copied', N'Пожалуйста, выберите, хотя бы, одну группу для копирования', 'messages'
exec insertLanguageInterface 'en', 'Please, choose at least one group to be copied', N'Please, choose at least one group to be copied', 'messages'

exec insertLanguageInterface 'uk', 'Are you sure you want to delete this field?', N'Ви впевнені, що хочете видалити це поле?', 'messages'
exec insertLanguageInterface 'ru', 'Are you sure you want to delete this field?', N'Вы уверены, что хотите удалить это поле?', 'messages'
exec insertLanguageInterface 'en', 'Are you sure you want to delete this field?', N'Are you sure you want to delete this field?', 'messages'

exec insertLanguageInterface 'uk', 'Yes', N'Так', 'messages'
exec insertLanguageInterface 'ru', 'Yes', N'Да', 'messages'
exec insertLanguageInterface 'en', 'Yes', N'Yes', 'messages'

exec insertLanguageInterface 'uk', 'No', N'Ні', 'messages'
exec insertLanguageInterface 'ru', 'No', N'Нет', 'messages'
exec insertLanguageInterface 'en', 'No', N'No', 'messages'

exec insertLanguageInterface 'uk', 'Task type', N'Тип візиту', 'messages'
exec insertLanguageInterface 'ru', 'Task type', N'Тип визита', 'messages'
exec insertLanguageInterface 'en', 'Task type', N'Task type', 'messages'

exec insertLanguageInterface 'uk', 'Access to mcm', N'Доступ до MCM', 'messages'
exec insertLanguageInterface 'ru', 'Access to mcm', N'Доступ к MCM', 'messages'
exec insertLanguageInterface 'en', 'Access to mcm', N'Access to MCM', 'messages'

exec insertLanguageInterface 'uk', 'Access to ETMS', N'Доступ до ETMS', 'messages'
exec insertLanguageInterface 'ru', 'Access to ETMS', N'Доступ к ETMS', 'messages'
exec insertLanguageInterface 'en', 'Access to ETMS', N'Access to ETMS', 'messages'

exec insertLanguageInterface 'uk', 'Access to BI Analytics', N'Доступ до BI Аналітики', 'messages'
exec insertLanguageInterface 'ru', 'Access to BI Analytics', N'Доступ к BI Аналитике', 'messages'
exec insertLanguageInterface 'en', 'Access to BI Analytics', N'Access to BI Analytics', 'messages'

exec insertLanguageInterface 'uk', 'Access to routes', N'Доступ до Маршрутів', 'messages'
exec insertLanguageInterface 'ru', 'Access to routes', N'Доступ к Маршрутам', 'messages'
exec insertLanguageInterface 'en', 'Access to routes', N'Access to Routes', 'messages'

exec insertLanguageInterface 'uk', 'Access to CLM Manager', N'Доступ до CLM Менеджера', 'messages'
exec insertLanguageInterface 'ru', 'Access to CLM Manager', N'Доступ к CLM Менеджеру', 'messages'
exec insertLanguageInterface 'en', 'Access to CLM Manager', N'Access to CLM Manager', 'messages'

exec insertLanguageInterface 'uk', 'Access to campaign', N'Доступ до Компанії', 'messages'
exec insertLanguageInterface 'ru', 'Access to campaign', N'Доступ к Кампании', 'messages'
exec insertLanguageInterface 'en', 'Access to campaign', N'Access to Campaign', 'messages'

exec insertLanguageInterface 'uk', 'Access to expenses', N'Доступ до Витрат', 'messages'
exec insertLanguageInterface 'ru', 'Access to expenses', N'Доступ к Расходам', 'messages'
exec insertLanguageInterface 'en', 'Access to expenses', N'Access to Expenses', 'messages'

exec insertLanguageInterface 'uk', 'Access to feedback', N'Доступ до Зворотного зв''язку', 'messages'
exec insertLanguageInterface 'ru', 'Access to feedback', N'Доступ к Обратной связи', 'messages'
exec insertLanguageInterface 'en', 'Access to feedback', N'Access to Feedback', 'messages'

exec insertLanguageInterface 'uk', 'Access to report', N'Доступ до Звітів', 'messages'
exec insertLanguageInterface 'ru', 'Access to report', N'Доступ к Отчетам', 'messages'
exec insertLanguageInterface 'en', 'Access to report', N'Access to Reports', 'messages'

exec insertLanguageInterface 'uk', 'Access to pharma sales', N'Доступ до Pharma sales', 'messages'
exec insertLanguageInterface 'ru', 'Access to pharma sales', N'Доступ к Pharma sales', 'messages'
exec insertLanguageInterface 'en', 'Access to pharma sales', N'Access to Pharma sales', 'messages'

exec insertLanguageInterface 'uk', 'Access to photo report', N'Доступ до Фото викладки', 'messages'
exec insertLanguageInterface 'ru', 'Access to photo report', N'Доступ к Фото выкладке', 'messages'
exec insertLanguageInterface 'en', 'Access to photo report', N'Access to Photo report', 'messages'

exec insertLanguageInterface 'uk', 'Access to planning', N'Доступ до Планування', 'messages'
exec insertLanguageInterface 'ru', 'Access to planning', N'Доступ к Планированию', 'messages'
exec insertLanguageInterface 'en', 'Access to planning', N'Access to Planning', 'messages'

exec insertLanguageInterface 'uk', 'Access to targeting', N'Доступ до Таргетування', 'messages'
exec insertLanguageInterface 'ru', 'Access to targeting', N'Доступ к Таргетированию', 'messages'
exec insertLanguageInterface 'en', 'Access to targeting', N'Access to Targeting', 'messages'

exec insertLanguageInterface 'uk', 'Access to CRM Verification', N'Доступ до CRM Verification', 'messages'
exec insertLanguageInterface 'ru', 'Access to CRM Verification', N'Доступ к CRM Verification', 'messages'
exec insertLanguageInterface 'en', 'Access to CRM Verification', N'Access to CRM Verification', 'messages'

exec insertLanguageInterface 'uk', 'Access to quality control', N'Доступ до Контролю якості', 'messages'
exec insertLanguageInterface 'ru', 'Access to quality control', N'Доступ к Контролю качества', 'messages'
exec insertLanguageInterface 'en', 'Access to quality control', N'Access to Quality control', 'messages'

exec insertLanguageInterface 'uk', 'Access to master list', N'Доступ до Майстер листів', 'messages'
exec insertLanguageInterface 'ru', 'Access to master list', N'Доступ к Мастер листам', 'messages'
exec insertLanguageInterface 'en', 'Access to master list', N'Access to Master list', 'messages'

exec insertLanguageInterface 'uk', 'Access to granually planning', N'Доступ до Грануального планування', 'messages'
exec insertLanguageInterface 'ru', 'Access to granually planning', N'Доступ к Грануальному планированию', 'messages'
exec insertLanguageInterface 'en', 'Access to granually planning', N'Access to granually planning', 'messages'

exec insertLanguageInterface 'uk', 'Access to activities', N'Доступ до Активностей', 'messages'
exec insertLanguageInterface 'ru', 'Access to activities', N'Доступ к Активностям', 'messages'
exec insertLanguageInterface 'en', 'Access to activities', N'Access to activities', 'messages'

exec insertLanguageInterface 'uk', 'All types', N'Всі типи', 'messages'
exec insertLanguageInterface 'ru', 'All types', N'Все типы', 'messages'
exec insertLanguageInterface 'en', 'All types', N'All types', 'messages'

exec insertLanguageInterface 'uk', 'Only enabled', N'Тільки ввімкнені', 'messages'
exec insertLanguageInterface 'ru', 'Only enabled', N'Только включенные', 'messages'
exec insertLanguageInterface 'en', 'Only enabled', N'Only enabled', 'messages'

exec insertLanguageInterface 'uk', 'Control settings', N'Параметри контролю', 'messages'
exec insertLanguageInterface 'ru', 'Control settings', N'Настройки контроля', 'messages'
exec insertLanguageInterface 'en', 'Control settings', N'Control settings', 'messages'

exec insertLanguageInterface 'uk', 'Active', N'Видимість', 'messages'
exec insertLanguageInterface 'ru', 'Active', N'Видимость', 'messages'
exec insertLanguageInterface 'en', 'Active', N'Active', 'messages'

exec insertLanguageInterface 'uk', 'Prev', N'Дані поперед. візиту', 'messages'
exec insertLanguageInterface 'ru', 'Prev', N'Данные пред. визита', 'messages'
exec insertLanguageInterface 'en', 'Prev', N'Prev', 'messages'

exec insertLanguageInterface 'uk', 'Fill from prev', N'Зап. від поперед. візиту', 'messages'
exec insertLanguageInterface 'ru', 'Fill from prev', N'Зап. от пред. визита', 'messages'
exec insertLanguageInterface 'en', 'Fill from prev', N'Fill from prev', 'messages'

exec insertLanguageInterface 'uk', 'Column width', N'Ширина стовпця', 'messages'
exec insertLanguageInterface 'ru', 'Column width', N'Ширина колонки', 'messages'
exec insertLanguageInterface 'en', 'Column width', N'Column width', 'messages'

exec insertLanguageInterface 'uk', 'Formula', N'Формула', 'messages'
exec insertLanguageInterface 'ru', 'Formula', N'Формула', 'messages'
exec insertLanguageInterface 'en', 'Formula', N'Formula', 'messages'

exec insertLanguageInterface 'uk', 'Dict code', N'Код словника', 'messages'
exec insertLanguageInterface 'ru', 'Dict code', N'Код словаря', 'messages'
exec insertLanguageInterface 'en', 'Dict code', N'Dict code', 'messages'

exec insertLanguageInterface 'uk', 'Agreement settings', N'Параметри домовленності', 'messages'
exec insertLanguageInterface 'ru', 'Agreement settings', N'Настройки договоренности', 'messages'
exec insertLanguageInterface 'en', 'Agreement settings', N'Agreement settings', 'messages'

exec insertLanguageInterface 'uk', 'Activities', N'Активності', 'messages'
exec insertLanguageInterface 'ru', 'Activities', N'Активности', 'messages'
exec insertLanguageInterface 'en', 'Activities', N'Activities', 'messages'

exec insertLanguageInterface 'uk', 'Expenses', N'Витрати', 'messages'
exec insertLanguageInterface 'ru', 'Expenses', N'Расходы', 'messages'
exec insertLanguageInterface 'en', 'Expenses', N'Expenses', 'messages'

exec insertLanguageInterface 'uk', 'Quality control', N'Контроль якості', 'messages'
exec insertLanguageInterface 'ru', 'Quality control', N'Контроль качества', 'messages'
exec insertLanguageInterface 'en', 'Quality control', N'Quality control', 'messages'

exec insertLanguageInterface 'uk', 'BI Analytics', N'BI Аналітика', 'messages'
exec insertLanguageInterface 'ru', 'BI Analytics', N'BI Аналитика', 'messages'
exec insertLanguageInterface 'en', 'BI Analytics', N'BI Analytics', 'messages'

exec insertLanguageInterface 'uk', 'Clear сache', N'Очистити кеш', 'messages'
exec insertLanguageInterface 'ru', 'Clear сache', N'Очистить кэш', 'messages'
exec insertLanguageInterface 'en', 'Clear сache', N'Clear сache', 'messages'

exec insertLanguageInterface 'uk', 'Current version supports all services from the list above. If some service is absent, version update required, contact the development department.', N'Поточна версія підтримує всі сервіси зі списку вище. Якщо який-небудь сервіс відсутній, потрібне оновлення версії, зверніться до відділу розробки.', 'messages'
exec insertLanguageInterface 'ru', 'Current version supports all services from the list above. If some service is absent, version update required, contact the development department.', N'Текущая версия поддерживает все сервисы из списка выше. Если какой-либо сервис отсутствует, требуется обновление версии, обратитесь в отдел разработки.', 'messages'
exec insertLanguageInterface 'en', 'Current version supports all services from the list above. If some service is absent, version update required, contact the development department.', N'Current version supports all services from the list above. If some service is absent, version update required, contact the development department.', 'messages'

exec insertLanguageInterface 'en', 'visible by default', N'visible by default', 'messages'
exec insertLanguageInterface 'ru', 'visible by default', N'отображается по умолчанию', 'messages'
exec insertLanguageInterface 'uk', 'visible by default', N'відображається за замовчуванням', 'messages'

-- Crm
exec insertLanguageInterface 'uk', 'New action', N'Нове завдання', 'crm'
exec insertLanguageInterface 'ru', 'New action', N'Новая задача', 'crm'
exec insertLanguageInterface 'en', 'New action', N'New action', 'crm'

exec insertLanguageInterface 'uk', 'New task', N'Новий візит', 'crm'
exec insertLanguageInterface 'ru', 'New task', N'Новый визит', 'crm'
exec insertLanguageInterface 'en', 'New task', N'New task', 'crm'

exec insertLanguageInterface 'uk', 'Task subjects', N'Теми візиту', 'crm'
exec insertLanguageInterface 'ru', 'Task subjects', N'Темы визита', 'crm'
exec insertLanguageInterface 'en', 'Task subjects', N'Task subjects', 'crm'

exec insertLanguageInterface 'uk', 'Result', N'Результат', 'crm'
exec insertLanguageInterface 'ru', 'Result', N'Результат', 'crm'
exec insertLanguageInterface 'en', 'Result', N'Result', 'crm'

exec insertLanguageInterface 'uk', 'Action type', N'Тип завдання', 'crm'
exec insertLanguageInterface 'ru', 'Action type', N'Тип задачи', 'crm'
exec insertLanguageInterface 'en', 'Action type', N'Action type', 'crm'

exec insertLanguageInterface 'uk', 'Med. representative', N'Мед. представник', 'crm'
exec insertLanguageInterface 'ru', 'Med. representative', N'Мед. представитель', 'crm'
exec insertLanguageInterface 'en', 'Med. representative', N'Med. representative', 'crm'

exec insertLanguageInterface 'uk', 'No employees', N'Немає співробітників', 'crm'
exec insertLanguageInterface 'ru', 'No employees', N'Нет сотрудников', 'crm'
exec insertLanguageInterface 'en', 'No employees', N'No employees', 'crm'

exec insertLanguageInterface 'uk', 'If you have additional questions about CRM, or you need technical advice - our support consultants are ready to help you.', N'Якщо у Вас виникли питання при роботі з програмним комплексом PharmaMobile, якщо Ви потребуєте технічної консультації - інженери служби технічної підтримки готові оперативно і кваліфіковано відповісти на ці та інші виниклі у Вас питання.', 'crm'
exec insertLanguageInterface 'ru', 'If you have additional questions about CRM, or you need technical advice - our support consultants are ready to help you.', N'Если у Вас возникли вопросы при работе с программным комплексом PharmaMobile, если Вы нуждаетесь в технической консультации – инженеры службы технической поддержки готовы оперативно и квалифицированно ответить на эти и другие возникшие у Вас вопросы.', 'crm'
exec insertLanguageInterface 'en', 'If you have additional questions about CRM, or you need technical advice - our support consultants are ready to help you.', N'If you have additional questions about CRM, or you need technical advice - our support consultants are ready to help you.', 'crm'

exec insertLanguageInterface 'uk', 'Coverage', N'Покриття', 'crm'
exec insertLanguageInterface 'ru', 'Coverage', N'Покрытие', 'crm'
exec insertLanguageInterface 'en', 'Coverage', N'Coverage', 'crm'

exec insertLanguageInterface 'uk', 'No rows', N'Немає записів', 'crm'
exec insertLanguageInterface 'ru', 'No rows', N'Нет записей', 'crm'
exec insertLanguageInterface 'en', 'No rows', N'No rows', 'crm'

exec insertLanguageInterface 'uk', 'Select plan', N'Будь ласка, виберіть план', 'crm'
exec insertLanguageInterface 'ru', 'Select plan', N'Пожалуйста, выберите план', 'crm'
exec insertLanguageInterface 'en', 'Select plan', N'Please choose a plan', 'crm'

exec insertLanguageInterface 'uk', 'Select details preparation', N'Виберіть бренд', 'crm'
exec insertLanguageInterface 'ru', 'Select details preparation', N'Выберите бренд', 'crm'
exec insertLanguageInterface 'en', 'Select details preparation', N'Select brand', 'crm'

exec insertLanguageInterface 'uk', 'Select detail presentation', N'Виберіть презентацію', 'crm'
exec insertLanguageInterface 'ru', 'Select detail presentation', N'Выберите презентацию', 'crm'
exec insertLanguageInterface 'en', 'Select detail presentation', N'Select detail presentation', 'crm'

exec insertLanguageInterface 'uk', 'Edit task', N'Редагувати візит', 'crm'
exec insertLanguageInterface 'ru', 'Edit task', N'Редактировать визит', 'crm'
exec insertLanguageInterface 'en', 'Edit task', N'Edit task', 'crm'

exec insertLanguageInterface 'uk', 'You don''t have permissions to create new task or action', N'У вас немає прав на створення нового візиту чи завдання', 'crm'
exec insertLanguageInterface 'ru', 'You don''t have permissions to create new task or action', N'У вас нет прав для создания нового визита или задачи', 'crm'
exec insertLanguageInterface 'en', 'You don''t have permissions to create new task or action', N'You don''t have permissions to create new task or action', 'crm'

exec insertLanguageInterface 'uk', 'Empty brands', N'Немає брендів', 'crm'
exec insertLanguageInterface 'ru', 'Empty brands', N'Нет брендов', 'crm'
exec insertLanguageInterface 'en', 'Empty brands', N'Empty brands', 'crm'

exec insertLanguageInterface 'uk', 'Task done', N'Візит виконаний', 'crm'
exec insertLanguageInterface 'ru', 'Task done', N'Визит выполнен', 'crm'
exec insertLanguageInterface 'en', 'Task done', N'Task done', 'crm'

exec insertLanguageInterface 'uk', 'Previous task result', N'Результат попереднього візиту', 'crm'
exec insertLanguageInterface 'ru', 'Previous task result', N'Результат предыдущего визита', 'crm'
exec insertLanguageInterface 'en', 'Previous task result', N'Previous task result', 'crm'

exec insertLanguageInterface 'uk', 'Result comment does not exist!', N'Результат попереднього візиту відсутній!', 'crm'
exec insertLanguageInterface 'ru', 'Result comment does not exist!', N'Результат предыдущего визита отсутствует!', 'crm'
exec insertLanguageInterface 'en', 'Result comment does not exist!', N'Result comment does not exist!', 'crm'

exec insertLanguageInterface 'uk', 'CANCEL', N'Відмінити', 'crm'
exec insertLanguageInterface 'ru', 'CANCEL', N'Отменить', 'crm'
exec insertLanguageInterface 'en', 'CANCEL', N'Cancel', 'crm'

exec insertLanguageInterface 'uk', 'Action done', N'Завдання виконано', 'crm'
exec insertLanguageInterface 'ru', 'Action done', N'Задача выполнена', 'crm'
exec insertLanguageInterface 'en', 'Action done', N'Action done', 'crm'

exec insertLanguageInterface 'uk', 'Select details promos', N'Виберіть промо-матеріали', 'crm'
exec insertLanguageInterface 'ru', 'Select details promos', N'Выберите промо-материалы', 'crm'
exec insertLanguageInterface 'en', 'Select details promos', N'Select details promos', 'crm'

exec insertLanguageInterface 'uk', 'Delete plan preparation', N'Видалити препарат?', 'crm'
exec insertLanguageInterface 'ru', 'Delete plan preparation', N'Удалить препарат?', 'crm'
exec insertLanguageInterface 'en', 'Delete plan preparation', N'Delete plan preparation?', 'crm'

exec insertLanguageInterface 'uk', 'Plan deleted', N'План видалений', 'crm'
exec insertLanguageInterface 'ru', 'Plan deleted', N'План удален', 'crm'
exec insertLanguageInterface 'en', 'Plan deleted', N'Plan deleted', 'crm'

exec insertLanguageInterface 'uk', 'Company name', N'Назва', 'crm'
exec insertLanguageInterface 'ru', 'Company name', N'Название', 'crm'
exec insertLanguageInterface 'en', 'Company name', N'Company name', 'crm'

exec insertLanguageInterface 'uk', 'Account owner dcr', N'Власник аккаунта dcr', 'crm'
exec insertLanguageInterface 'ru', 'Account owner dcr', N'Владелец аккаунта dcr', 'crm'
exec insertLanguageInterface 'en', 'Account owner dcr', N'Account owner dcr', 'crm'

exec insertLanguageInterface 'uk', 'Request has been sent', N'Запит надіслано', 'crm'
exec insertLanguageInterface 'ru', 'Request has been sent', N'Запрос отправлен', 'crm'
exec insertLanguageInterface 'en', 'Request has been sent', N'Request has been sent', 'crm'

--Crm_отчеты

exec insertLanguageInterface 'uk', 'Column name', N'Назва колонки', 'crm'
exec insertLanguageInterface 'ru', 'Column name', N'Название колонки', 'crm'
exec insertLanguageInterface 'en', 'Column name', N'Column name', 'crm'

exec insertLanguageInterface 'uk', 'Column formula', N'Формула колонки', 'crm'
exec insertLanguageInterface 'ru', 'Column formula', N'Формула колонки', 'crm'
exec insertLanguageInterface 'en', 'Column formula', N'Column formula', 'crm'

exec insertLanguageInterface 'uk', 'columns', N'Колонки', 'ag-grid'
exec insertLanguageInterface 'ru', 'columns', N'Колонки', 'ag-grid'
exec insertLanguageInterface 'en', 'columns', N'Сolumns', 'ag-grid'

exec insertLanguageInterface 'uk', 'filters', N'Фильтры', 'ag-grid'
exec insertLanguageInterface 'ru', 'filters', N'Фільтри', 'ag-grid'
exec insertLanguageInterface 'en', 'filters', N'Filters', 'ag-grid'

exec insertLanguageInterface 'uk', 'searchOoo', N'Пошук...', 'ag-grid'
exec insertLanguageInterface 'ru', 'searchOoo', N'Поиск...', 'ag-grid'
exec insertLanguageInterface 'en', 'searchOoo', N'Search...', 'ag-grid'

exec insertLanguageInterface 'uk', 'applyFilter', N'Застосувати фільтр', 'ag-grid'
exec insertLanguageInterface 'ru', 'applyFilter', N'Применить фильтр', 'ag-grid'
exec insertLanguageInterface 'en', 'applyFilter', N'Apply filter', 'ag-grid'

exec insertLanguageInterface 'uk', 'resetFilter', N'Скасувати фильтр', 'ag-grid'
exec insertLanguageInterface 'ru', 'resetFilter', N'Сбросить фильтр', 'ag-grid'
exec insertLanguageInterface 'en', 'resetFilter', N'Rest filter', 'ag-grid'

exec insertLanguageInterface 'uk', 'rowGroupColumnsEmptyMessage', N'Перетягніть сюди щоб згрупувати', 'ag-grid'
exec insertLanguageInterface 'ru', 'rowGroupColumnsEmptyMessage', N'Перетащите сюда чтобы сгруппировать', 'ag-grid'
exec insertLanguageInterface 'en', 'rowGroupColumnsEmptyMessage', N'Drag here to group', 'ag-grid'

exec insertLanguageInterface 'uk', 'groups', N'Група рядків', 'ag-grid'
exec insertLanguageInterface 'ru', 'groups', N'Группа строк', 'ag-grid'
exec insertLanguageInterface 'en', 'groups', N'Row group', 'ag-grid'

exec insertLanguageInterface 'uk', 'values', N'Значення', 'ag-grid'
exec insertLanguageInterface 'ru', 'values', N'Значения', 'ag-grid'
exec insertLanguageInterface 'en', 'values', N'Values', 'ag-grid'

exec insertLanguageInterface 'uk', 'valueColumnsEmptyMessage', N'Перетягніть сюди щоб підрахувати', 'ag-grid'
exec insertLanguageInterface 'ru', 'valueColumnsEmptyMessage', N'Перетащите сюда чтобы подсчитать', 'ag-grid'
exec insertLanguageInterface 'en', 'valueColumnsEmptyMessage', N'Drag here to aggregate', 'ag-grid'

--Маршруты
exec insertLanguageInterface 'uk', 'Routes', N'Маршрути','messages'
exec insertLanguageInterface 'ru', 'Routes', N'Маршруты','messages'
exec insertLanguageInterface 'en', 'Routes', N'Routes','messages'

exec insertLanguageInterface 'uk', 'find', N'пошук', 'messages'
exec insertLanguageInterface 'ru', 'find', N'поиск', 'messages'
exec insertLanguageInterface 'en', 'find', N'find', 'messages'

exec insertLanguageInterface 'uk', 'NoResults', N'За обраним фільтром нічого не знайдено', 'messages'
exec insertLanguageInterface 'ru', 'NoResults', N'По данному фильтру ничего не найдено', 'messages'
exec insertLanguageInterface 'en', 'NoResults', N'Nothing found', 'messages'

--CLM

exec insertLanguageInterface 'uk', 'Copy presentation', N'Копіювати презентацію', 'messages'
exec insertLanguageInterface 'en', 'Copy presentation', N'Copy presentation', 'messages'
exec insertLanguageInterface 'ru', 'Copy presentation', N'Копировать презентацию', 'messages'

exec insertLanguageInterface 'en', 'Move presentation', N'Move presentation', 'messages'
exec insertLanguageInterface 'ru', 'Move presentation', N'Переместить презентацию', 'messages'
exec insertLanguageInterface 'uk', 'Move presentation', N'Перемістити презентацію', 'messages'

exec insertLanguageInterface 'en', 'Confirm archivation', N'Confirm archivation', 'messages'
exec insertLanguageInterface 'ru', 'Confirm archivation', N'Подтвердить архивирование', 'messages'
exec insertLanguageInterface 'uk', 'Confirm archivation', N'Підтвердити архівацію', 'messages'

exec insertLanguageInterface 'en', 'Archive', N'Archive', 'messages'
exec insertLanguageInterface 'ru', 'Archive', N'Архивировать', 'messages'
exec insertLanguageInterface 'uk', 'Archive', N'Архівувати', 'messages'

exec insertLanguageInterface 'en', 'Directories', N'Directories', 'messages'
exec insertLanguageInterface 'ru', 'Directories', N'Папки', 'messages'
exec insertLanguageInterface 'uk', 'Directories', N'Папки', 'messages'

exec insertLanguageInterface 'en', 'Directory Name', N'Directory name', 'messages'
exec insertLanguageInterface 'ru', 'Directory Name', N'Название папки', 'messages'
exec insertLanguageInterface 'uk', 'Directory Name', N'Назва папки', 'messages'

exec insertLanguageInterface 'en', 'Parent directory', N'Parent directory', 'messages'
exec insertLanguageInterface 'ru', 'Parent directory', N'Родительская папка', 'messages'
exec insertLanguageInterface 'uk', 'Parent directory', N'Батьківська папка', 'messages'

exec insertLanguageInterface 'en', 'Move', N'Move', 'messages'
exec insertLanguageInterface 'ru', 'Move', N'Переместить', 'messages'
exec insertLanguageInterface 'uk', 'Move', N'Перемістити', 'messages'

exec insertLanguageInterface 'en', 'Copy', N'Copy', 'messages'
exec insertLanguageInterface 'ru', 'Copy', N'Копировать', 'messages'
exec insertLanguageInterface 'uk', 'Copy', N'Копіювати', 'messages'

exec insertLanguageInterface 'en', 'Edit presentation', N'Edit presentation', 'messages'
exec insertLanguageInterface 'ru', 'Edit presentation', N'Редактировать презентацию', 'messages'
exec insertLanguageInterface 'uk', 'Edit presentation', N'Редагувати презентацію', 'messages'

exec insertLanguageInterface 'en', 'Create/edit directory', N'Create/edit directory', 'messages'
exec insertLanguageInterface 'ru', 'Create/edit directory', N'Создать/редактировать папку', 'messages'
exec insertLanguageInterface 'uk', 'Create/edit directory', N'Створити/редагувати папку', 'messages'

exec insertLanguageInterface 'en', 'Send by email', N'Send by email', 'messages'
exec insertLanguageInterface 'ru', 'Send by email', N'Разрешить отправку на почту', 'messages'
exec insertLanguageInterface 'uk', 'Send by email', N'Дозволити надсилання на пошту', 'messages'

---------------------------------------------------------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------------------------------------------------------

----------------------------------------------------------------------------------------------------------------------------------------------------------------------
-- v-16
-- https://teamsoft.atlassian.net/browse/EGISRU-78
----------------------------------------------------------------------------------------------------------------------------------------------------------------------
exec insertLanguageInterface 'uk', 'You don''t have permissions to create new task or action', N'У Вас недостатньо прав для створення візиту чи задачі', 'crm'
exec insertLanguageInterface 'ru', 'You don''t have permissions to create new task or action', N'У Вас недостаточно прав для создания визита или задачи', 'crm'
---------------------------------------------------------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------------------------------------------------------

----------------------------------------------------------------------------------------------------------------------------------------------------------------------
-- v-16
--https://teamsoft.atlassian.net/browse/PHARMAWEB-266
----------------------------------------------------------------------------------------------------------------------------------------------------------------------
IF NOT EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE table_name = 'info_teamlocation' AND column_name = 'gmt_offset')
    ALTER TABLE info_teamlocation ADD gmt_offset int NULL
GO

IF NOT EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE table_name = 'info_teamlocation_clustered' AND column_name = 'gmt_offset')
    ALTER TABLE info_teamlocation_clustered ADD gmt_offset int NULL
GO

IF NOT EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE table_name = 'info_task' AND column_name = 'gmt_offset')
    ALTER TABLE info_task ADD gmt_offset int NULL
GO

IF NOT EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE table_name = 'info_action' AND column_name = 'gmt_offset')
    ALTER TABLE info_action ADD gmt_offset int NULL
GO
---------------------------------------------------------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------------------------------------------------------
delete from po_entityfields where page = 8 and field = 'brand_id';
delete from po_entityfields  where page = 8 and field = 'brend_id';
update po_entityfields set [group] = 'reports' where name in (N'Промо-материалы', N'Бренды')
GO

-- https://teamsoft.atlassian.net/browse/MCM-317
---------------------------------------------------------------------------------------------------------
IF NOT EXISTS (SELECT 1 FROM information_schema.columns WHERE table_name = 'info_contactphone' AND column_name = 'isvibermcm')
    BEGIN
        ALTER TABLE info_contactphone ADD isvibermcm INT NULL
    END
GO

IF NOT EXISTS (SELECT 1 FROM information_schema.columns WHERE table_name = 'info_contactphone' AND column_name = 'issmsmcm')
    BEGIN
        ALTER TABLE info_contactphone ADD issmsmcm INT NULL
    END
GO
---------------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------------

---------------------------------------------------------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------------------------------------------------------

UPDATE po_webservice
SET service_id = (SELECT id FROM info_service WHERE identifier = 'crm')
WHERE identifier = 'crm'
UPDATE po_webservice
SET service_id = (SELECT id FROM info_service WHERE identifier = 'mcm')
WHERE identifier = 'mcm'
UPDATE po_webservice
SET service_id = (SELECT id FROM info_service WHERE identifier = 'administration')
WHERE identifier = 'administration'
UPDATE po_webservice
SET service_id = (SELECT id FROM info_service WHERE identifier = 'geomarketing')
WHERE identifier = 'geomarketing'
UPDATE po_webservice
SET service_id = (SELECT id FROM info_service WHERE identifier = 'qlik_view')
WHERE identifier = 'qlik_view'
UPDATE po_webservice
SET service_id = (SELECT id FROM info_service WHERE identifier = 'routes')
WHERE identifier = 'routes'
UPDATE po_webservice
SET service_id = (SELECT id FROM info_service WHERE identifier = 'clm_manager')
WHERE identifier = 'clm_manager'
UPDATE po_webservice
SET service_id = (SELECT id FROM info_service WHERE identifier = 'campaign')
WHERE identifier = 'campaign'
UPDATE po_webservice
SET service_id = (SELECT id FROM info_service WHERE identifier = 'expenses')
WHERE identifier = 'expenses'
UPDATE po_webservice
SET service_id = (SELECT id FROM info_service WHERE identifier = 'feedback')
WHERE identifier = 'feedback'
UPDATE po_webservice
SET service_id = (SELECT id FROM info_service WHERE identifier = 'report')
WHERE identifier = 'report'
UPDATE po_webservice
SET service_id = (SELECT id FROM info_service WHERE identifier = 'pharma_sales')
WHERE identifier = 'pharma_sales'
UPDATE po_webservice
SET service_id = (SELECT id FROM info_service WHERE identifier = 'photo_report')
WHERE identifier = 'photo_report'
UPDATE po_webservice
SET service_id = (SELECT id FROM info_service WHERE identifier = 'planning')
WHERE identifier = 'planning'
UPDATE po_webservice
SET service_id = (SELECT id FROM info_service WHERE identifier = 'targeting')
WHERE identifier = 'targeting'
UPDATE po_webservice
SET service_id = (SELECT id FROM info_service WHERE identifier = 'quality_control')
WHERE identifier = 'quality_control'
UPDATE po_webservice
SET service_id = (SELECT id FROM info_service WHERE identifier = 'master_list')
WHERE identifier = 'master_list'
UPDATE po_webservice
SET service_id = (SELECT id FROM info_service WHERE identifier = 'granually_planning')
WHERE identifier = 'granually_planning'
UPDATE po_webservice
SET service_id = (SELECT id FROM info_service WHERE identifier = 'event')
WHERE identifier = 'event'
GO

------------------------------------------------------------------------------------------------------------------------
-- Last translation fixes
------------------------------------------------------------------------------------------------------------------------

update po_dictionarygroup set name = 'Localization' where name = 'Локализация';
update po_dictionarygroup set name = 'General' where name = 'Общие';
update po_dictionarygroup set name = 'Companies' where name = 'Учреждения';
update po_dictionarygroup set name = 'Clients' where name = 'Клиенты';
update po_dictionarygroup set name = 'Tasks' where name = 'Визиты';
update po_dictionarygroup set name = 'Actions' where name = 'Задачи';
update po_dictionarygroup set name = 'Preparations' where name = 'Препараты';
update po_dictionarygroup set name = 'Promotional materials' where name = 'Промо-материалы';
update po_dictionary set name = 'User positions' where name = 'Должности пользователей';
update po_dictionary set name = 'Cycle plan model' where name = 'Модель плана цикла';
update po_dictionary set name = 'Users' where name = 'Пользователи';
update po_dictionary set name = 'User dictionaries' where name = 'Пользовательские справочники';
update po_dictionary set name = 'User dictionaries - values' where name = 'Пользовательские справочники - значения';
update po_dictionary set name = 'Product directions' where name = 'Продуктовые направления';
update po_dictionary set name = 'Step distribution in a cycle' where name = 'Распределение этапов в цикле';
update po_dictionary set name = 'Languages' where name = 'Языки';
update po_dictionary set name = 'Action states' where name = 'Состояния задач';
update po_dictionary set name = 'Action types' where name = 'Типы задач';
update po_dictionary set name = 'Grouping of specializations to target groups' where name = 'Группировка специализаций до таргет-групп';
update po_dictionary set name = 'Positions' where name = 'Должности';
update po_dictionary set name = 'Client categories' where name = 'Категории клиентов';
update po_dictionary set name = 'Clients' where name = 'Клиенты';
update po_dictionary set name = 'Loyalty' where name = 'Лояльность';
update po_dictionary set name = 'Branch offices' where name = 'Отделения';
update po_dictionary set name = 'Competitor preparations' where name = 'Препараты конкурентов';
update po_dictionary set name = 'Reasons for archiving clients' where name = 'Причины архивации клиентов';
update po_dictionary set name = 'Specialization' where name = 'Специализация';
update po_dictionary set name = 'Target groups' where name = 'Таргет-группы';
update po_dictionary set name = 'Types of clients' where name = 'Типы клиентов';
update po_dictionary set name = 'Categories of companies' where name = 'Категории учреждений';
update po_dictionary set name = 'Reasons for archiving companies' where name = 'Причины архивации учреждений';
update po_dictionary set name = 'Roles in relationships' where name = 'Роли во взамосвязях';
update po_dictionary set name = 'Binding category and type of company' where name = 'Связка категория и типа компании';
update po_dictionary set name = 'Binding of task type and company type' where name = 'Связка типа визита и типа компании';
update po_dictionary set name = 'Areas of activity' where name = 'Сферы деятельности';
update po_dictionary set name = 'Types of companies' where name = 'Типы учреждений';
update po_dictionary set name = 'Companies' where name = 'Учреждения';
update po_dictionary set name = 'Human settlements' where name = 'Населенные пункты';
update po_dictionary set name = 'Areas' where name = 'Области';
update po_dictionary set name = 'Belonging to regions' where name = 'Принадлежность регионов';
update po_dictionary set name = 'Regions' where name = 'Регионы';
update po_dictionary set name = 'Countries' where name = 'Страны';
update po_dictionary set name = 'Types of settlement' where name = 'Типы населенного пункта';
update po_dictionary set name = 'Street types' where name = 'Типы улиц';
update po_dictionary set name = 'Brands' where name = 'Бренды';
update po_dictionary set name = 'Brands of preparations' where name = 'Бренды препаратов';
update po_dictionary set name = 'Units of measurement' where name = 'Единицы измерений';
update po_dictionary set name = 'Preparations categories' where name = 'Категории препаратов';
update po_dictionary set name = 'Preparation lines' where name = 'Линии препарата';
update po_dictionary set name = 'Feedback on preparations' where name = 'Отзывы о препаратах';
update po_dictionary set name = 'Preparations' where name = 'Препараты';
update po_dictionary set name = 'Preparations results' where name = 'Результаты по препаратам';
update po_dictionary set name = 'Binding preparations and brands' where name = 'Связка препаратов и брендов';
update po_dictionary set name = 'Binding preparations and prod.directions' where name = 'Связка препаратов и прод.направлений';
update po_dictionary set name = 'Binding preparations and specialties' where name = 'Связка препаратов и специальностей';
update po_dictionary set name = 'Speciality' where name = 'Специальность';
update po_dictionary set name = 'Degree of loyalty to the preparaion' where name = 'Степень лояльности к препарату';
update po_dictionary set name = 'Edition forms' where name = 'Формы выпуска';
update po_dictionary set name = 'Brands of promotional materials' where name = 'Бренды промоматериалов';
update po_dictionary set name = 'Product directions for promotional materials' where name = 'Продуктовые направления промоматериалов';
update po_dictionary set name = 'Promotional materials' where name = 'Промоматериалы';
update po_dictionary set name = 'Prices of promotional materials' where name = 'Цены промоматериалов';
update po_dictionary set name = '(КАМ) Promotions' where name = '(КАМ) Акции';
update po_dictionary set name = '(КАМ) Agreements' where name = '(КАМ) Договора';
update po_dictionary set name = '(КАМ) Comments' where name = '(КАМ) Комментарии';
update po_dictionary set name = '(КАМ) Commitments' where name = '(КАМ) Обязательства';
update po_dictionary set name = 'Marketing activities' where name = 'Маркетинговые активности';
update po_dictionary set name = 'Tasks results' where name = 'Результаты визитов';
update po_dictionary set name = 'Tasks statuses' where name = 'Состояния визитов';
update po_dictionary set name = 'Tasks topics' where name = 'Темы визитов';
update po_dictionary set name = 'Tasks types' where name = 'Типы визитов';
update po_dictionary set name = 'Types of tasks purpose' where name = 'Типы цели визитов';
update po_dictionary set name = 'Task targets' where name = 'Цели визитов';
update po_dictionary set name = 'Task cycle' where name = 'Цикл визитов';
update po_entityfields set name = 'Company type' where name = 'Тип учреждения';
update po_entityfields set name = 'Code' where name = 'Код';
update po_entityfields set name = 'Branch offices' where name = 'Отделения';
update po_entityfields set name = 'Postal code' where name = 'Почтовый индекс';
update po_entityfields set name = 'Area' where name = 'Область';
update po_entityfields set name = 'Human settlement' where name = 'Населенный пункт';
update po_entityfields set name = 'Street type' where name = 'Тип улицы';
update po_entityfields set name = 'Street' where name = 'Улица';
update po_entityfields set name = 'House' where name = 'Дом';
update po_entityfields set name = 'Extras info. at the address' where name = 'Доп. инфо. по адресу';
update po_entityfields set name = 'Phone' where name = 'Телефон';
update po_entityfields set name = 'Phone (extra)' where name = 'Телефон (доп.)';
update po_entityfields set name = 'E-mail' where name = 'Электронная почта';
update po_entityfields set name = 'Web-site' where name = 'Электронная страница';
update po_entityfields set name = 'Township area' where name = 'Район нас. пункта';
update po_entityfields set name = 'Responsible' where name = 'Ответственный';
update po_entityfields set name = 'Additionally responsible' where name = 'Доп. ответственные';
update po_entityfields set name = 'Verification status' where name = 'Статус верификации';
update po_entityfields set name = 'Reason for archiving' where name = 'Причина архивации';
update po_entityfields set name = 'Active' where name = 'Активный';
update po_entityfields set name = 'Related companies' where name = 'Связанные учреждения';
update po_entityfields set name = 'Coordinates' where name = 'Координаты';
update po_entityfields set name = 'District' where name = 'Округ';
update po_entityfields set name = 'Related doctors' where name = 'Связанные врачи';
update po_entityfields set name = 'Main company' where name = 'Главное учреждение';
update po_entityfields set name = 'Parent company' where name = 'Родительское учреждение';
update po_entityfields set name = 'Name' where name = 'Название';
update po_entityfields set name = 'Category' where name = 'Категория';
update po_entityfields set name = 'Main' where name = 'Главное';
update po_entityfields set name = 'Main company(ID)' where name = 'Главное учреждение(ID)';
update po_entityfields set name = 'Description' where name = 'Описание';
update po_entityfields set name = 'District' where name = 'Район';
update po_entityfields set name = 'Tax number' where name = 'Налоговый номер';
update po_entityfields set name = 'Account MDN ID' where name = 'Аккаунт MDN ID';
update po_entityfields set name = 'Street-Home' where name = 'Улица-дом';
update po_entityfields set name = 'Login' where name = 'Логин';
update po_entityfields set name = 'Visibility' where name = 'Видимость';
update po_entityfields set name = 'State of task' where name = 'Состояние визита';
update po_entityfields set name = 'Plan' where name = 'План';
update po_entityfields set name = 'Date of previous task' where name = 'Дата предыдущего визита';
update po_entityfields set name = 'Date of task' where name = 'Дата визита';
update po_entityfields set name = 'Date of next task' where name = 'Дата следующего визита';
update po_entityfields set name = 'Task type' where name = 'Тип визита';
update po_entityfields set name = 'Supervisor (Employee)' where name = 'Супервайзер (Сотрудник)';
update po_entityfields set name = 'Observers' where name = 'Наблюдатели';
update po_entityfields set name = 'Company' where name = 'Учреждение';
update po_entityfields set name = 'Client' where name = 'Клиент';
update po_entityfields set name = 'The purpose of the next visit' where name = 'Цель следующего визита';
update po_entityfields set name = 'Branch' where name = 'Отделение';
update po_entityfields set name = 'Brand comments' where name = 'Комментарии по брендам';
update po_entityfields set name = 'Training Block' where name = 'Блок тренингов';
update po_entityfields set name = 'Training participants' where name = 'Участники тренинга';
update po_entityfields set name = 'Training participant' where name = 'Участник тренинга';
update po_entityfields set name = 'PTC Evaluation Form' where name = 'Форма оценки ПТС';
update po_entityfields set name = 'Number of participants' where name = 'Кол-во участников';
update po_entityfields set name = 'Firstman' where name = 'Первостольники';
update po_entityfields set name = 'Specialization' where name = 'Специализация';
update po_entityfields set name = 'Lastname' where name = 'Фамилия';
update po_entityfields set name = 'Firstname' where name = 'Имя';
update po_entityfields set name = 'Midlename' where name = 'Отчество';
update po_entityfields set name = 'Signature' where name = 'Подпись';
update po_entityfields set name = 'Hide sip-call' where name = 'Скрытие кнопки вызова sip телефонии';
update po_entityfields set name = 'Administrative relations' where name = 'Административные связи';
update po_entityfields set name = 'Social relations' where name = 'Социальные связи';
update po_entityfields set name = 'Categoryprep' where name = 'Автоматическая категория';
update po_entityfields set name = 'Morion ID' where name = 'MDM ID Клиент';
update po_entityfields set name = 'Case number' where name = 'Номер дела';
update po_entityfields set name = 'Additional workspace' where name = 'Доп. места работы';
update po_entityfields set name = 'Position' where name = 'Должность';
update po_entityfields set name = 'Company' where name = 'Учреждение';
update po_entityfields set name = 'Department' where name = 'Отделение';
update po_entityfields set name = 'Additional department' where name = 'Доп. отделения';
update po_entityfields set name = 'Contact type' where name = 'Тип клиента';
update po_entityfields set name = 'Sex' where name = 'Пол';
update po_entityfields set name = 'Birthdate' where name = 'Дата рождения';
update po_entityfields set name = 'Company relation' where name = 'Прескрайбер-аптека';
update po_entityfields set name = 'Additional specializations' where name = 'Доп. специализации';
update po_entityfields set name = 'Action state' where name = 'Состояние задачи';
update po_entityfields set name = 'Date from' where name = 'Дата начала задачи';
update po_entityfields set name = 'Date till' where name = 'Дата окончания задачи';
update po_entityfields set name = 'Action type' where name = 'Тип задачи';
update po_entityfields set name = 'Description' where name = 'Комментарии';
update po_entityfields set name = 'Development map' where name = 'Карта развития';
update po_entityfields set name = 'Brands' where name = 'Бренды';
update po_entityfields set name = 'Promo materials' where name = 'Промо-материалы';
update po_entityfields set name = 'Planogram (promo for control)' where name = 'Планограмма (промо для контроля)';
update po_entityfields set name = 'Projects' where name = 'Проекты';
update po_entityfields set name = 'Share' where name = 'Акция';
update po_entityfields set name = 'Tender list' where name = 'Тендерный список';
update po_entityfields set name = 'KAM Agreement' where name = 'Договоренность КАМ';
update po_entityfields set name = 'Sales Plan Fact KAM' where name = 'План-факт продаж КАМ';
update po_entityfields set name = 'Goals' where name = 'Цели';
update po_entityfields set name = 'Marketing projects' where name = 'Маркетинговые проекты';
update po_entityfields set name = 'Objectives of double task' where name = 'Цели двойного визита';
update po_entityfields set name = 'Note' where name = 'Примечание';
update po_entityfields set name = 'Task result' where name = 'Результат визита';
update po_entityfields set name = 'Previous task result' where name = 'Результат прошлого визита';
update po_entityfields set name = 'Topics of task' where name = 'Темы визита';
update po_entityfields set name = 'MHH Questionnaires' where name = 'МНН Анкеты';
update po_entityfields set name = 'QR Promocode' where name = 'QR Промокод';
update po_entityfields set name = 'Hide sip telephony call button' where name = 'Скрытие кнопки вызова sip телефонии';
GO

exec insertLanguageInterface 'en', 'Branch offices', N'Branch offices', 'messages';
exec insertLanguageInterface 'ru', 'Branch offices', N'Отделения', 'messages';
exec insertLanguageInterface 'uk', 'Branch offices', N'Відділення', 'messages';

exec insertLanguageInterface 'en', 'Postal code', N'Postal code', 'messages';
exec insertLanguageInterface 'ru', 'Postal code', N'Почтовый индекс', 'messages';
exec insertLanguageInterface 'uk', 'Postal code', N'Поштовий індекс', 'messages';

exec insertLanguageInterface 'en', 'Area', N'Area', 'messages';
exec insertLanguageInterface 'ru', 'Area', N'Область', 'messages';
exec insertLanguageInterface 'uk', 'Area', N'Область', 'messages';

exec insertLanguageInterface 'en', 'Human settlement', N'Human settlement', 'messages';
exec insertLanguageInterface 'ru', 'Human settlement', N'Населенный пункт', 'messages';
exec insertLanguageInterface 'uk', 'Human settlement', N'Населений пункт', 'messages';

exec insertLanguageInterface 'en', 'Extras info. at the address', N'Extras info. at the address', 'messages';
exec insertLanguageInterface 'ru', 'Extras info. at the address', N'Доп. инфо. по адресу', 'messages';
exec insertLanguageInterface 'uk', 'Extras info. at the address', N'Додат. інф. по адресі', 'messages';

exec insertLanguageInterface 'en', 'Phone (extra)', N'Phone (extra)', 'messages';
exec insertLanguageInterface 'ru', 'Phone (extra)', N'Телефон (доп.)', 'messages';
exec insertLanguageInterface 'uk', 'Phone (extra)', N'Телефон (додатк.)', 'messages';

exec insertLanguageInterface 'en', 'Cycle plan model', N'Cycle plan model', 'messages';
exec insertLanguageInterface 'ru', 'Cycle plan model', N'Модель плана цикла', 'messages';
exec insertLanguageInterface 'uk', 'Cycle plan model', N'Модель плану циклу', 'messages';

exec insertLanguageInterface 'en', 'User positions', N'User positions', 'messages';
exec insertLanguageInterface 'ru', 'User positions', N'Должности пользователей', 'messages';
exec insertLanguageInterface 'uk', 'User positions', N'Посади користувачів', 'messages';

exec insertLanguageInterface 'en', 'Users', N'Users', 'messages';
exec insertLanguageInterface 'ru', 'Users', N'Пользователи', 'messages';
exec insertLanguageInterface 'uk', 'Users', N'Користувачі', 'messages';

exec insertLanguageInterface 'en', 'User dictionaries', N'User dictionaries', 'messages';
exec insertLanguageInterface 'ru', 'User dictionaries', N'Справочники пользователей', 'messages';
exec insertLanguageInterface 'uk', 'User dictionaries', N'Довідники користувачів', 'messages';

exec insertLanguageInterface 'en', 'User dictionaries - values', N'User dictionaries - values', 'messages';
exec insertLanguageInterface 'ru', 'User dictionaries - values', N'Справочники пользователей - значения', 'messages';
exec insertLanguageInterface 'uk', 'User dictionaries - values', N'Довідники користувачів - значення', 'messages';

exec insertLanguageInterface 'en', 'Product directions', N'Product directions', 'messages';
exec insertLanguageInterface 'ru', 'Product directions', N'Продуктовые направления', 'messages';
exec insertLanguageInterface 'uk', 'Product directions', N'Продуктові напрямки', 'messages';

exec insertLanguageInterface 'en', 'Step distribution in a cycle', N'Step distribution in a cycle', 'messages';
exec insertLanguageInterface 'ru', 'Step distribution in a cycle', N'Распределение этапов в цикле', 'messages';
exec insertLanguageInterface 'uk', 'Step distribution in a cycle', N'Розподіл етапів в циклі', 'messages';

exec insertLanguageInterface 'en', 'Grouping of specializations to target groups', N'Grouping of specializations to target groups', 'messages';
exec insertLanguageInterface 'ru', 'Grouping of specializations to target groups', N'Группировка специализаций по таргет-группам', 'messages';
exec insertLanguageInterface 'uk', 'Grouping of specializations to target groups', N'Групування спеціалізацій по тергет-групам', 'messages';

exec insertLanguageInterface 'en', 'Positions', N'Positions', 'messages';
exec insertLanguageInterface 'ru', 'Positions', N'Должности', 'messages';
exec insertLanguageInterface 'uk', 'Positions', N'Посади', 'messages';

exec insertLanguageInterface 'en', 'Client categories', N'Client categories', 'messages';
exec insertLanguageInterface 'ru', 'Client categories', N'Категории клиентов', 'messages';
exec insertLanguageInterface 'uk', 'Client categories', N'Категорії клієнтів', 'messages';

exec insertLanguageInterface 'en', 'Clients', N'Clients', 'messages';
exec insertLanguageInterface 'ru', 'Clients', N'Клиенты', 'messages';
exec insertLanguageInterface 'uk', 'Clients', N'Клієнти', 'messages';

exec insertLanguageInterface 'en', 'Loyalty', N'Loyalty', 'messages';
exec insertLanguageInterface 'ru', 'Loyalty', N'Лояльность', 'messages';
exec insertLanguageInterface 'uk', 'Loyalty', N'Лояльність', 'messages';

exec insertLanguageInterface 'en', 'Branch offices', N'Branch offices', 'messages';
exec insertLanguageInterface 'ru', 'Branch offices', N'Отделения', 'messages';
exec insertLanguageInterface 'uk', 'Branch offices', N'Відділи', 'messages';

exec insertLanguageInterface 'en', 'Competitor preparations', N'Competitor preparations', 'messages';
exec insertLanguageInterface 'ru', 'Competitor preparations', N'Препараты конкурентов', 'messages';
exec insertLanguageInterface 'uk', 'Competitor preparations', N'Препарати конкурентів', 'messages';

exec insertLanguageInterface 'en', 'Reasons for archiving clients', N'Reasons for archiving clients', 'messages';
exec insertLanguageInterface 'ru', 'Reasons for archiving clients', N'Причины архивации клиентов', 'messages';
exec insertLanguageInterface 'uk', 'Reasons for archiving clients', N'Причини архівації клієнтів', 'messages';

exec insertLanguageInterface 'en', 'Specialization', N'Specialization', 'messages';
exec insertLanguageInterface 'ru', 'Specialization', N'Специализация', 'messages';
exec insertLanguageInterface 'uk', 'Specialization', N'Спеціалізація', 'messages';

exec insertLanguageInterface 'en', 'Types of clients', N'Types of clients', 'messages';
exec insertLanguageInterface 'ru', 'Types of clients', N'Типы клиентов', 'messages';
exec insertLanguageInterface 'uk', 'Types of clients', N'Типи клієнтів', 'messages';

exec insertLanguageInterface 'en', 'Target groups', N'Target groups', 'messages';
exec insertLanguageInterface 'ru', 'Target groups', N'Таргет-группы', 'messages';
exec insertLanguageInterface 'uk', 'Target groups', N'Таргет-групи', 'messages';

exec insertLanguageInterface 'en', 'Categories of companies', N'Categories of companies', 'messages';
exec insertLanguageInterface 'ru', 'Categories of companies', N'Категории учреждений', 'messages';
exec insertLanguageInterface 'uk', 'Categories of companies', N'Категорії установ', 'messages';

exec insertLanguageInterface 'en', 'Reasons for archiving companies', N'Reasons for archiving companies', 'messages';
exec insertLanguageInterface 'ru', 'Reasons for archiving companies', N'Причины архивации учреждений', 'messages';
exec insertLanguageInterface 'uk', 'Reasons for archiving companies', N'Причини архівації установ', 'messages';

exec insertLanguageInterface 'en', 'Roles in relationships', N'Roles in relationships', 'messages';
exec insertLanguageInterface 'ru', 'Roles in relationships', N'Роли во взаимосвязях', 'messages';
exec insertLanguageInterface 'uk', 'Roles in relationships', N'Ролі у взаємозв''язках', 'messages';

exec insertLanguageInterface 'en', 'Binding category and type of company', N'Binding category and type of company', 'messages';
exec insertLanguageInterface 'ru', 'Binding category and type of company', N'Связка категории и типа учреждения', 'messages';
exec insertLanguageInterface 'uk', 'Binding category and type of company', N'Зв''язка категорії і типу установи', 'messages';

exec insertLanguageInterface 'en', 'Binding of task type and company type', N'Binding of task type and company type', 'messages';
exec insertLanguageInterface 'ru', 'Binding of task type and company type', N'Связка типа визита и типа учреждения', 'messages';
exec insertLanguageInterface 'uk', 'Binding of task type and company type', N'Зв''язка типу візиту і типу установи', 'messages';

exec insertLanguageInterface 'en', 'Areas of activity', N'Areas of activity', 'messages';
exec insertLanguageInterface 'ru', 'Areas of activity', N'Сферы деятельности', 'messages';
exec insertLanguageInterface 'uk', 'Areas of activity', N'Сфери діяльності', 'messages';

exec insertLanguageInterface 'en', 'Types of companies', N'Types of companies', 'messages';
exec insertLanguageInterface 'ru', 'Types of companies', N'Типы учреждений', 'messages';
exec insertLanguageInterface 'uk', 'Types of companies', N'Типи установ', 'messages';

exec insertLanguageInterface 'en', 'Companies', N'Companies', 'messages';
exec insertLanguageInterface 'ru', 'Companies', N'Учреждения', 'messages';
exec insertLanguageInterface 'uk', 'Companies', N'Установи', 'messages';

exec insertLanguageInterface 'en', 'Human settlements', N'Human settlements', 'messages';
exec insertLanguageInterface 'ru', 'Human settlements', N'Населенные пункты', 'messages';
exec insertLanguageInterface 'uk', 'Human settlements', N'Населені пункти', 'messages';

exec insertLanguageInterface 'en', 'Areas', N'Areas', 'messages';
exec insertLanguageInterface 'ru', 'Areas', N'Области', 'messages';
exec insertLanguageInterface 'uk', 'Areas', N'Області', 'messages';

exec insertLanguageInterface 'en', 'Belonging to regions', N'Belonging to regions', 'messages';
exec insertLanguageInterface 'ru', 'Belonging to regions', N'Принадлежность регионам', 'messages';
exec insertLanguageInterface 'uk', 'Belonging to regions', N'Належність регіонам', 'messages';

exec insertLanguageInterface 'en', 'Regions', N'Regions', 'messages';
exec insertLanguageInterface 'ru', 'Regions', N'Регионы', 'messages';
exec insertLanguageInterface 'uk', 'Regions', N'Регіони', 'messages';

exec insertLanguageInterface 'en', 'Countries', N'Countries', 'messages';
exec insertLanguageInterface 'ru', 'Countries', N'Страны', 'messages';
exec insertLanguageInterface 'uk', 'Countries', N'Країни', 'messages';

exec insertLanguageInterface 'en', 'Types of settlement', N'Types of settlement', 'messages';
exec insertLanguageInterface 'ru', 'Types of settlement', N'Типы населенных пунктов', 'messages';
exec insertLanguageInterface 'uk', 'Types of settlement', N'Типи населених пунктів', 'messages';

exec insertLanguageInterface 'en', 'Street types', N'Street types', 'messages';
exec insertLanguageInterface 'ru', 'Street types', N'Типы улиц', 'messages';
exec insertLanguageInterface 'uk', 'Street types', N'Типи вулиць', 'messages';

exec insertLanguageInterface 'en', 'Brands', N'Brands', 'messages';
exec insertLanguageInterface 'ru', 'Brands', N'Бренды', 'messages';
exec insertLanguageInterface 'uk', 'Brands', N'Бренди', 'messages';

exec insertLanguageInterface 'en', 'Binding preparations and brands', N'Binding preparations and brands', 'messages';
exec insertLanguageInterface 'ru', 'Binding preparations and brands', N'Связка препаратов и брендов', 'messages';
exec insertLanguageInterface 'uk', 'Binding preparations and brands', N'Зв''язка препаратів і брендів', 'messages';

exec insertLanguageInterface 'en', 'Binding preparations and prod.directions', N'Binding preparations and prod.directions', 'messages';
exec insertLanguageInterface 'ru', 'Binding preparations and prod.directions', N'Связка препаратов и прод. направлений', 'messages';
exec insertLanguageInterface 'uk', 'Binding preparations and prod.directions', N'Зв''язка препаратів і прод. напрямків', 'messages';

exec insertLanguageInterface 'en', 'Binding preparations and specialties', N'Binding preparations and specialties', 'messages';
exec insertLanguageInterface 'ru', 'Binding preparations and specialties', N'Связка препаратов и специальностей', 'messages';
exec insertLanguageInterface 'uk', 'Binding preparations and specialties', N'Зв''язка препаратів і спеціальностей', 'messages';

exec insertLanguageInterface 'en', 'Speciality', N'Speciality', 'messages';
exec insertLanguageInterface 'ru', 'Speciality', N'Специальность', 'messages';
exec insertLanguageInterface 'uk', 'Speciality', N'Спеціальність', 'messages';

exec insertLanguageInterface 'en', 'Brands of promotional materials', N'Brands of promotional materials', 'messages';
exec insertLanguageInterface 'ru', 'Brands of promotional materials', N'Бренды промоматериалов', 'messages';
exec insertLanguageInterface 'uk', 'Brands of promotional materials', N'Бренди промоматеріалів', 'messages';

exec insertLanguageInterface 'en', 'Product directions for promotional materials', N'Product directions for promotional materials', 'messages';
exec insertLanguageInterface 'ru', 'Product directions for promotional materials', N'Прод. направления промоматериалов', 'messages';
exec insertLanguageInterface 'uk', 'Product directions for promotional materials', N'Прод. напрямки промоматеріалів', 'messages';

exec insertLanguageInterface 'en', 'Promotional materials', N'Promotional materials', 'messages';
exec insertLanguageInterface 'ru', 'Promotional materials', N'Промоматериалы', 'messages';
exec insertLanguageInterface 'uk', 'Promotional materials', N'Промоматеріали', 'messages';

exec insertLanguageInterface 'en', 'Prices of promotional materials', N'Prices of promotional materials', 'messages';
exec insertLanguageInterface 'ru', 'Prices of promotional materials', N'Цена промоматериалов', 'messages';
exec insertLanguageInterface 'uk', 'Prices of promotional materials', N'Ціна промоматеріалів', 'messages';

exec insertLanguageInterface 'en', '(КАМ) Agreements', N'(КАМ) Agreements', 'messages';
exec insertLanguageInterface 'ru', '(КАМ) Agreements', N'(КАМ) Договора', 'messages';
exec insertLanguageInterface 'uk', '(КАМ) Agreements', N'(КАМ) Договори', 'messages';

exec insertLanguageInterface 'en', 'Types of tasks purpose', N'Types of tasks purpose', 'messages';
exec insertLanguageInterface 'ru', 'Types of tasks purpose', N'Типы цели визитов', 'messages';
exec insertLanguageInterface 'uk', 'Types of tasks purpose', N'Типи цілі візитів', 'messages';

exec insertLanguageInterface 'en', 'Languages', N'Languages', 'messages';
exec insertLanguageInterface 'ru', 'Languages', N'Языки', 'messages';
exec insertLanguageInterface 'uk', 'Languages', N'Мови', 'messages';

exec insertLanguageInterface 'en', 'Action states', N'Action states', 'messages';
exec insertLanguageInterface 'ru', 'Action states', N'Состояния задач', 'messages';
exec insertLanguageInterface 'uk', 'Action states', N'Стани задач', 'messages';

exec insertLanguageInterface 'en', 'Action types', N'Action types', 'messages';
exec insertLanguageInterface 'ru', 'Action types', N'Типы задач', 'messages';
exec insertLanguageInterface 'uk', 'Action types', N'Типи задач', 'messages';

exec insertLanguageInterface 'en', 'Brands of preparations', N'Brands of preparations', 'messages';
exec insertLanguageInterface 'ru', 'Brands of preparations', N'Бренды препаратов', 'messages';
exec insertLanguageInterface 'uk', 'Brands of preparations', N'Бренди препаратів', 'messages';

exec insertLanguageInterface 'en', 'Units of measurement', N'Units of measurement', 'messages';
exec insertLanguageInterface 'ru', 'Units of measurement', N'Единицы измерений', 'messages';
exec insertLanguageInterface 'uk', 'Units of measurement', N'Одиниці вимірів', 'messages';

exec insertLanguageInterface 'en', 'Preparations categories', N'Preparations categories', 'messages';
exec insertLanguageInterface 'ru', 'Preparations categories', N'Категории препаратов', 'messages';
exec insertLanguageInterface 'uk', 'Preparations categories', N'Категорії препаратів', 'messages';

exec insertLanguageInterface 'en', 'Preparation lines', N'Preparation lines', 'messages';
exec insertLanguageInterface 'ru', 'Preparation lines', N'Линии препарата', 'messages';
exec insertLanguageInterface 'uk', 'Preparation lines', N'Лінії препарату', 'messages';

exec insertLanguageInterface 'en', 'Feedback on preparations', N'Feedback on preparations', 'messages';
exec insertLanguageInterface 'ru', 'Feedback on preparations', N'Отзывы о препаратах', 'messages';
exec insertLanguageInterface 'uk', 'Feedback on preparations', N'Відгуки про препарати', 'messages';

exec insertLanguageInterface 'en', 'Preparations', N'Preparations', 'messages';
exec insertLanguageInterface 'ru', 'Preparations', N'Препараты', 'messages';
exec insertLanguageInterface 'uk', 'Preparations', N'Препарати', 'messages';

exec insertLanguageInterface 'en', 'Preparations results', N'Preparations results', 'messages';
exec insertLanguageInterface 'ru', 'Preparations results', N'Результаты по препаратам', 'messages';
exec insertLanguageInterface 'uk', 'Preparations results', N'Результати по препаратам', 'messages';

exec insertLanguageInterface 'en', 'Degree of loyalty to the preparaion', N'Degree of loyalty to the preparaion', 'messages';
exec insertLanguageInterface 'ru', 'Degree of loyalty to the preparaion', N'Степень лояльности к препарату', 'messages';
exec insertLanguageInterface 'uk', 'Degree of loyalty to the preparaion', N'Ступінь лояльності до препарату', 'messages';

exec insertLanguageInterface 'en', 'Edition forms', N'Edition forms', 'messages';
exec insertLanguageInterface 'ru', 'Edition forms', N'Формы выпуска', 'messages';
exec insertLanguageInterface 'uk', 'Edition forms', N'Форми випуску', 'messages';

exec insertLanguageInterface 'en', '(КАМ) Promotions', N'(КАМ) Promotions', 'messages';
exec insertLanguageInterface 'ru', '(КАМ) Promotions', N'(КАМ) Акции', 'messages';
exec insertLanguageInterface 'uk', '(КАМ) Promotions', N'(КАМ) Акції', 'messages';

exec insertLanguageInterface 'en', '(КАМ) Comments', N'(КАМ) Comments', 'messages';
exec insertLanguageInterface 'ru', '(КАМ) Comments', N'(КАМ) Комментарии', 'messages';
exec insertLanguageInterface 'uk', '(КАМ) Comments', N'(КАМ) Коментарі', 'messages';

exec insertLanguageInterface 'en', '(КАМ) Commitments', N'(КАМ) Commitments', 'messages';
exec insertLanguageInterface 'ru', '(КАМ) Commitments', N'(КАМ) Обязательства', 'messages';
exec insertLanguageInterface 'uk', '(КАМ) Commitments', N'(КАМ) Обов''язки', 'messages';

exec insertLanguageInterface 'en', 'Marketing activities', N'Marketing activities', 'messages';
exec insertLanguageInterface 'ru', 'Marketing activities', N'Маркетинговые активности', 'messages';
exec insertLanguageInterface 'uk', 'Marketing activities', N'Маркетингові активності', 'messages';

exec insertLanguageInterface 'en', 'Tasks results', N'Tasks results', 'messages';
exec insertLanguageInterface 'ru', 'Tasks results', N'Результаты визитов', 'messages';
exec insertLanguageInterface 'uk', 'Tasks results', N'Результати візитів', 'messages';

exec insertLanguageInterface 'en', 'Tasks statuses', N'Tasks statuses', 'messages';
exec insertLanguageInterface 'ru', 'Tasks statuses', N'Состояния визитов', 'messages';
exec insertLanguageInterface 'uk', 'Tasks statuses', N'Стани візитів', 'messages';

exec insertLanguageInterface 'en', 'Tasks topics', N'Tasks topics', 'messages';
exec insertLanguageInterface 'ru', 'Tasks topics', N'Темы визитов', 'messages';
exec insertLanguageInterface 'uk', 'Tasks topics', N'Теми візитів', 'messages';

exec insertLanguageInterface 'en', 'Tasks types', N'Tasks types', 'messages';
exec insertLanguageInterface 'ru', 'Tasks types', N'Типы визитов', 'messages';
exec insertLanguageInterface 'uk', 'Tasks types', N'Типи візитів', 'messages';

exec insertLanguageInterface 'en', 'Task targets', N'Task targets', 'messages';
exec insertLanguageInterface 'ru', 'Task targets', N'Цели визитов', 'messages';
exec insertLanguageInterface 'uk', 'Task targets', N'Цілі візитів', 'messages';

exec insertLanguageInterface 'en', 'Task cycle', N'Task cycle', 'messages';
exec insertLanguageInterface 'ru', 'Task cycle', N'Цикл визитов', 'messages';
exec insertLanguageInterface 'uk', 'Task cycle', N'Цикл візитів', 'messages';

exec insertLanguageInterface 'en', 'General', N'General', 'messages';
exec insertLanguageInterface 'ru', 'General', N'Общее', 'messages';
exec insertLanguageInterface 'uk', 'General', N'Загальне', 'messages';

exec insertLanguageInterface 'en', 'Localization', N'Localization', 'messages';
exec insertLanguageInterface 'ru', 'Localization', N'Локализация', 'messages';
exec insertLanguageInterface 'uk', 'Localization', N'Локалізація', 'messages';

exec insertLanguageInterface 'en', 'Tasks', N'Tasks', 'messages';
exec insertLanguageInterface 'ru', 'Tasks', N'Визиты', 'messages';
exec insertLanguageInterface 'uk', 'Tasks', N'Візити', 'messages';

exec insertLanguageInterface 'en', 'Actions', N'Actions', 'messages';
exec insertLanguageInterface 'ru', 'Actions', N'Задачи', 'messages';
exec insertLanguageInterface 'uk', 'Actions', N'Задачі', 'messages';

exec insertLanguageInterface 'en', 'Postal code', N'Postal code', 'messages';
exec insertLanguageInterface 'ru', 'Postal code', N'Почтовый индекс', 'messages';
exec insertLanguageInterface 'uk', 'Postal code', N'Поштовий індекс', 'messages';

exec insertLanguageInterface 'en', 'Area', N'Area', 'messages';
exec insertLanguageInterface 'ru', 'Area', N'Область', 'messages';
exec insertLanguageInterface 'uk', 'Area', N'Область', 'messages';

exec insertLanguageInterface 'en', 'Human settlement', N'Human settlement', 'messages';
exec insertLanguageInterface 'ru', 'Human settlement', N'Населенный пункт', 'messages';
exec insertLanguageInterface 'uk', 'Human settlement', N'Населений пункт', 'messages';

exec insertLanguageInterface 'en', 'Extras info. at the address', N'Extras info. at the address', 'messages';
exec insertLanguageInterface 'ru', 'Extras info. at the address', N'Доп. инфо. по адресу', 'messages';
exec insertLanguageInterface 'uk', 'Extras info. at the address', N'Додатк. інф. по адресі', 'messages';

exec insertLanguageInterface 'en', 'Phone (extra)', N'Phone (extra)', 'messages';
exec insertLanguageInterface 'ru', 'Phone (extra)', N'Телефон (доп.)', 'messages';
exec insertLanguageInterface 'uk', 'Phone (extra)', N'Телефон (додатк.)', 'messages';

exec insertLanguageInterface 'en', 'Web-site', N'Web-site', 'messages';
exec insertLanguageInterface 'ru', 'Web-site', N'Электронная страница', 'messages';
exec insertLanguageInterface 'uk', 'Web-site', N'Електронна сторінка', 'messages';

exec insertLanguageInterface 'en', 'Additionally responsible', N'Additionally responsible', 'messages';
exec insertLanguageInterface 'ru', 'Additionally responsible', N'Доп. ответственные', 'messages';
exec insertLanguageInterface 'uk', 'Additionally responsible', N'Додатк. відповідальні', 'messages';

exec insertLanguageInterface 'en', 'Verification status', N'Verification status', 'messages';
exec insertLanguageInterface 'ru', 'Verification status', N'Статус верификации', 'messages';
exec insertLanguageInterface 'uk', 'Verification status', N'Статус верифікації', 'messages';

exec insertLanguageInterface 'en', 'Reason for archiving', N'Reason for archiving', 'messages';
exec insertLanguageInterface 'ru', 'Reason for archiving', N'Причина архивации', 'messages';
exec insertLanguageInterface 'uk', 'Reason for archiving', N'Причина архівації', 'messages';

exec insertLanguageInterface 'en', 'Related companies', N'Related companies', 'messages';
exec insertLanguageInterface 'ru', 'Related companies', N'Связанные учреждения', 'messages';
exec insertLanguageInterface 'uk', 'Related companies', N'Зв''язані установи', 'messages';

exec insertLanguageInterface 'en', 'Coordinates', N'Coordinates', 'messages';
exec insertLanguageInterface 'ru', 'Coordinates', N'Координаты', 'messages';
exec insertLanguageInterface 'uk', 'Coordinates', N'Координати', 'messages';

exec insertLanguageInterface 'en', 'Related doctors', N'Related doctors', 'messages';
exec insertLanguageInterface 'ru', 'Related doctors', N'Связанные врачи', 'messages';
exec insertLanguageInterface 'uk', 'Related doctors', N'Пов''язані лікарі', 'messages';

exec insertLanguageInterface 'en', 'Main company', N'Main company', 'messages';
exec insertLanguageInterface 'ru', 'Main company', N'Главное учреждение', 'messages';
exec insertLanguageInterface 'uk', 'Main company', N'Головна установа', 'messages';

exec insertLanguageInterface 'en', 'Parent company', N'Parent company', 'messages';
exec insertLanguageInterface 'ru', 'Parent company', N'Родительское учреждение', 'messages';
exec insertLanguageInterface 'uk', 'Parent company', N'Батьківська установа', 'messages';

exec insertLanguageInterface 'en', 'Township area', N'Township area', 'messages';
exec insertLanguageInterface 'ru', 'Township area', N'Район нас. пункта', 'messages';
exec insertLanguageInterface 'uk', 'Township area', N'Район населеного пункту', 'messages';

exec insertLanguageInterface 'en', 'Main', N'Main', 'messages';
exec insertLanguageInterface 'ru', 'Main', N'Главное', 'messages';
exec insertLanguageInterface 'uk', 'Main', N'Головне', 'messages';

exec insertLanguageInterface 'en', 'Main company(ID)', N'Main company(ID)', 'messages';
exec insertLanguageInterface 'ru', 'Main company(ID)', N'Главное учреждение(ID)', 'messages';
exec insertLanguageInterface 'uk', 'Main company(ID)', N'Головна установа(ID)', 'messages';

exec insertLanguageInterface 'en', 'Tax number', N'Tax number', 'messages';
exec insertLanguageInterface 'ru', 'Tax number', N'Налоговый номер', 'messages';
exec insertLanguageInterface 'uk', 'Tax number', N'Податковий номер', 'messages';

exec insertLanguageInterface 'en', 'Account MDN ID', N'Account MDN ID', 'messages';
exec insertLanguageInterface 'ru', 'Account MDN ID', N'Аккаунт MDN ID', 'messages';
exec insertLanguageInterface 'uk', 'Account MDN ID', N'Аккаунт MDN ID', 'messages';

exec insertLanguageInterface 'en', 'Firstname', N'Firstname', 'messages';
exec insertLanguageInterface 'ru', 'Firstname', N'Имя', 'messages';
exec insertLanguageInterface 'uk', 'Firstname', N'Ім''я', 'messages';

exec insertLanguageInterface 'en', 'Middlename', N'Middlename', 'messages';
exec insertLanguageInterface 'ru', 'Middlename', N'Отчество', 'messages';
exec insertLanguageInterface 'uk', 'Middlename', N'По батькові', 'messages';

exec insertLanguageInterface 'en', 'Categoryprep', N'Categoryprep', 'messages';
exec insertLanguageInterface 'ru', 'Categoryprep', N'Автоматическая категория', 'messages';
exec insertLanguageInterface 'uk', 'Categoryprep', N'Автоматична категорія', 'messages';

exec insertLanguageInterface 'en', 'Morion ID', N'Morion ID', 'messages';
exec insertLanguageInterface 'ru', 'Morion ID', N'MDM ID Клиент', 'messages';
exec insertLanguageInterface 'uk', 'Morion ID', N'MDM ID Клієнт', 'messages';

exec insertLanguageInterface 'en', 'Case number', N'Case number', 'messages';
exec insertLanguageInterface 'ru', 'Case number', N'Номер дела', 'messages';
exec insertLanguageInterface 'uk', 'Case number', N'Номер справи', 'messages';

exec insertLanguageInterface 'en', 'Additional workspace', N'Additional workspace', 'messages';
exec insertLanguageInterface 'ru', 'Additional workspace', N'Доп. места работы', 'messages';
exec insertLanguageInterface 'uk', 'Additional workspace', N'Додатк. місця роботи', 'messages';

exec insertLanguageInterface 'en', 'Sex', N'Sex', 'messages';
exec insertLanguageInterface 'ru', 'Sex', N'Пол', 'messages';
exec insertLanguageInterface 'uk', 'Sex', N'Стать', 'messages';

exec insertLanguageInterface 'en', 'Additional specializations', N'Additional specializations', 'messages';
exec insertLanguageInterface 'ru', 'Additional specializations', N'Доп. специализации', 'messages';
exec insertLanguageInterface 'uk', 'Additional specializations', N'Додатк. спеціалізації', 'messages';

exec insertLanguageInterface 'en', 'Signature', N'Signature', 'messages';
exec insertLanguageInterface 'ru', 'Signature', N'Подпись', 'messages';
exec insertLanguageInterface 'uk', 'Signature', N'Підпис', 'messages';

exec insertLanguageInterface 'en', 'Hide sip-call', N'Hide sip-call', 'messages';
exec insertLanguageInterface 'ru', 'Hide sip-call', N'Скрытие кнопки вызова sip телефонии', 'messages';
exec insertLanguageInterface 'uk', 'Hide sip-call', N'Приховування кнопки виклику sip телефонії', 'messages';

exec insertLanguageInterface 'en', 'Administrative relations', N'Administrative relations', 'messages';
exec insertLanguageInterface 'ru', 'Administrative relations', N'Административные связи', 'messages';
exec insertLanguageInterface 'uk', 'Administrative relations', N'Адміністративні зв''язки', 'messages';

exec insertLanguageInterface 'en', 'Social relations', N'Social relations', 'messages';
exec insertLanguageInterface 'ru', 'Social relations', N'Социальные связи', 'messages';
exec insertLanguageInterface 'uk', 'Social relations', N'Соціальні звя''зки', 'messages';

exec insertLanguageInterface 'en', 'Company relation', N'Company relation', 'messages';
exec insertLanguageInterface 'ru', 'Company relation', N'Прескрайбер-аптека', 'messages';
exec insertLanguageInterface 'uk', 'Company relation', N'Прескрайбер-аптека', 'messages';

exec insertLanguageInterface 'en', 'Department', N'Department', 'messages';
exec insertLanguageInterface 'ru', 'Department', N'Отделение', 'messages';
exec insertLanguageInterface 'uk', 'Department', N'Відділення', 'messages';

exec insertLanguageInterface 'en', 'Additional department', N'Additional department', 'messages';
exec insertLanguageInterface 'ru', 'Additional department', N'Доп. отделения', 'messages';
exec insertLanguageInterface 'uk', 'Additional department', N'Додатк. відділення', 'messages';

exec insertLanguageInterface 'en', 'PTC Evaluation Form', N'PTC Evaluation Form', 'messages';
exec insertLanguageInterface 'ru', 'PTC Evaluation Form', N'Форма оценки ПТС', 'messages';
exec insertLanguageInterface 'uk', 'PTC Evaluation Form', N'Форма оцінки ПТС', 'messages';

exec insertLanguageInterface 'en', 'Date of previous task', N'Date of previous task', 'messages';
exec insertLanguageInterface 'ru', 'Date of previous task', N'Дата предыдущего визита', 'messages';
exec insertLanguageInterface 'uk', 'Date of previous task', N'Дата попереднього візиту', 'messages';

exec insertLanguageInterface 'en', 'Date of task', N'Date of task', 'messages';
exec insertLanguageInterface 'ru', 'Date of task', N'Дата визита', 'messages';
exec insertLanguageInterface 'uk', 'Date of task', N'Дата візиту', 'messages';

exec insertLanguageInterface 'en', 'Date of next task', N'Date of next task', 'messages';
exec insertLanguageInterface 'ru', 'Date of next task', N'Дата следующего визита', 'messages';
exec insertLanguageInterface 'uk', 'Date of next task', N'Дата наступного візиту', 'messages';

exec insertLanguageInterface 'en', 'Supervisor (Employee)', N'Supervisor (Employee)', 'messages';
exec insertLanguageInterface 'ru', 'Supervisor (Employee)', N'Супервайзер (Сотрудник)', 'messages';
exec insertLanguageInterface 'uk', 'Supervisor (Employee)', N'Супервайзер (Співробітник)', 'messages';

exec insertLanguageInterface 'en', 'Observers', N'Observers', 'messages';
exec insertLanguageInterface 'ru', 'Observers', N'Наблюдатели', 'messages';
exec insertLanguageInterface 'uk', 'Observers', N'Спостерігачі', 'messages';

exec insertLanguageInterface 'en', 'The purpose of the next visit', N'The purpose of the next visit', 'messages';
exec insertLanguageInterface 'ru', 'The purpose of the next visit', N'Цель следующего визита', 'messages';
exec insertLanguageInterface 'uk', 'The purpose of the next visit', N'Мета наступного візиту', 'messages';

exec insertLanguageInterface 'en', 'Brand comments', N'Brand comments', 'messages';
exec insertLanguageInterface 'ru', 'Brand comments', N'Комментарии по брендам', 'messages';
exec insertLanguageInterface 'uk', 'Brand comments', N'Коментарі по брендам', 'messages';

exec insertLanguageInterface 'en', 'Training Block', N'Training Block', 'messages';
exec insertLanguageInterface 'ru', 'Training Block', N'Блок тренингов', 'messages';
exec insertLanguageInterface 'uk', 'Training Block', N'Блок тренінгів', 'messages';

exec insertLanguageInterface 'en', 'Training participants', N'Training participants', 'messages';
exec insertLanguageInterface 'ru', 'Training participants', N'Участники тренинга', 'messages';
exec insertLanguageInterface 'uk', 'Training participants', N'Учасники тренінгу', 'messages';

exec insertLanguageInterface 'en', 'Training participant', N'Training participant', 'messages';
exec insertLanguageInterface 'ru', 'Training participant', N'Участник тренинга', 'messages';
exec insertLanguageInterface 'uk', 'Training participant', N'Учасник тренінгу', 'messages';

exec insertLanguageInterface 'en', 'Number of participants', N'Number of participants', 'messages';
exec insertLanguageInterface 'ru', 'Number of participants', N'Кол-во участников', 'messages';
exec insertLanguageInterface 'uk', 'Number of participants', N'Кількість учасників', 'messages';

exec insertLanguageInterface 'en', 'Planogram (promo for control)', N'Planogram (promo for control)', 'messages';
exec insertLanguageInterface 'ru', 'Planogram (promo for control)', N'Планограмма (промо для контроля)', 'messages';
exec insertLanguageInterface 'uk', 'Planogram (promo for control)', N'Планограмма (промо для контролю)', 'messages';

exec insertLanguageInterface 'en', 'Projects', N'Projects', 'messages';
exec insertLanguageInterface 'ru', 'Projects', N'Проекты', 'messages';
exec insertLanguageInterface 'uk', 'Projects', N'Проекти', 'messages';

exec insertLanguageInterface 'en', 'Share', N'Share', 'messages';
exec insertLanguageInterface 'ru', 'Share', N'Акция', 'messages';
exec insertLanguageInterface 'uk', 'Share', N'Акція', 'messages';

exec insertLanguageInterface 'en', 'Tender list', N'Tender list', 'messages';
exec insertLanguageInterface 'ru', 'Tender list', N'Тендерный список', 'messages';
exec insertLanguageInterface 'uk', 'Tender list', N'Тендерний список', 'messages';

exec insertLanguageInterface 'en', 'KAM Agreement', N'KAM Agreement', 'messages';
exec insertLanguageInterface 'ru', 'KAM Agreement', N'Договоренность КАМ', 'messages';
exec insertLanguageInterface 'uk', 'KAM Agreement', N'Домовленність КАМ', 'messages';

exec insertLanguageInterface 'en', 'Sales Plan Fact KAM', N'Sales Plan Fact KAM', 'messages';
exec insertLanguageInterface 'ru', 'Sales Plan Fact KAM', N'План-факт продаж КАМ', 'messages';
exec insertLanguageInterface 'uk', 'Sales Plan Fact KAM', N'План-факт продажів КАМ', 'messages';

exec insertLanguageInterface 'en', 'Goals', N'Goals', 'messages';
exec insertLanguageInterface 'ru', 'Goals', N'Цели', 'messages';
exec insertLanguageInterface 'uk', 'Goals', N'Цілі', 'messages';

exec insertLanguageInterface 'en', 'Marketing projects', N'Marketing projects', 'messages';
exec insertLanguageInterface 'ru', 'Marketing projects', N'Маркетинговые проекты', 'messages';
exec insertLanguageInterface 'uk', 'Marketing projects', N'Маркетингові проекти', 'messages';

exec insertLanguageInterface 'en', 'Objectives of double task', N'Objectives of double task', 'messages';
exec insertLanguageInterface 'ru', 'Objectives of double task', N'Цели двойного визита', 'messages';
exec insertLanguageInterface 'uk', 'Objectives of double task', N'Цілі подвійного візиту', 'messages';

exec insertLanguageInterface 'en', 'Note', N'Note', 'messages';
exec insertLanguageInterface 'ru', 'Note', N'Примечание', 'messages';
exec insertLanguageInterface 'uk', 'Note', N'Примітка', 'messages';

exec insertLanguageInterface 'en', 'Task result', N'Task result', 'messages';
exec insertLanguageInterface 'ru', 'Task result', N'Результат визита', 'messages';
exec insertLanguageInterface 'uk', 'Task result', N'Результат візиту', 'messages';

exec insertLanguageInterface 'en', 'Previous task result', N'Previous task result', 'messages';
exec insertLanguageInterface 'ru', 'Previous task result', N'Результат прошлого визита', 'messages';
exec insertLanguageInterface 'uk', 'Previous task result', N'Результат минулого візиту', 'messages';

exec insertLanguageInterface 'en', 'Topics of task', N'Topics of task', 'messages';
exec insertLanguageInterface 'ru', 'Topics of task', N'Темы визита', 'messages';
exec insertLanguageInterface 'uk', 'Topics of task', N'Теми візиту', 'messages';

exec insertLanguageInterface 'en', 'MHH Questionnaires', N'MHH Questionnaires', 'messages';
exec insertLanguageInterface 'ru', 'MHH Questionnaires', N'МНН Анкеты', 'messages';
exec insertLanguageInterface 'uk', 'MHH Questionnaires', N'МНН Анкети', 'messages';

exec insertLanguageInterface 'en', 'QR Promocode', N'QR Promocode', 'messages';
exec insertLanguageInterface 'ru', 'QR Promocode', N'QR Промокод', 'messages';
exec insertLanguageInterface 'uk', 'QR Promocode', N'QR Промокод', 'messages';

exec insertLanguageInterface 'en', 'Clinical situations', N'Clinical situations', 'messages';
exec insertLanguageInterface 'ru', 'Clinical situations', N'Клинические случаи', 'messages';
exec insertLanguageInterface 'uk', 'Clinical situations', N'Клінічні випадки', 'messages';

exec insertLanguageInterface 'en', 'Training', N'Training', 'messages';
exec insertLanguageInterface 'ru', 'Training', N'Тренинг', 'messages';
exec insertLanguageInterface 'uk', 'Training', N'Тренінг', 'messages';

exec insertLanguageInterface 'en', 'Evaluation form', N'Evaluation form', 'messages';
exec insertLanguageInterface 'ru', 'Evaluation form', N'Форма для оценки', 'messages';
exec insertLanguageInterface 'uk', 'Evaluation form', N'Форма для оцінки', 'messages';

exec insertLanguageInterface 'en', 'Test start date and time', N'Test start date and time', 'messages';
exec insertLanguageInterface 'ru', 'Test start date and time', N'Дата и время начала теста', 'messages';
exec insertLanguageInterface 'uk', 'Test start date and time', N'Дата і час початку тесту', 'messages';

exec insertLanguageInterface 'en', 'Date and time of the end; of the test', N'Date and time of the end; of the test', 'messages';
exec insertLanguageInterface 'ru', 'Date and time of the end; of the test', N'Дата и время окончания теста', 'messages';
exec insertLanguageInterface 'uk', 'Date and time of the end; of the test', N'Дата і час закінчення тесту', 'messages';

exec insertLanguageInterface 'en', 'Planogram', 'Planogram', 'messages';
exec insertLanguageInterface 'ru', 'Planogram', 'Планограмма', 'messages';
exec insertLanguageInterface 'uk', 'Planogram', 'Планограмма', 'messages';

exec insertLanguageInterface 'en', 'KAM sales plan-fact', N'KAM sales plan-fact', 'messages';
exec insertLanguageInterface 'ru', 'KAM sales plan-fact', N'План-факт продаж КАМ', 'messages';
exec insertLanguageInterface 'uk', 'KAM sales plan-fact', N'План-факт продажів КАМ', 'messages';

exec insertLanguageInterface 'en', 'Company goals', N'Company goals', 'messages';
exec insertLanguageInterface 'ru', 'Company goals', N'Цели учреждения', 'messages';
exec insertLanguageInterface 'uk', 'Company goals', N'Цілі установи', 'messages';

exec insertLanguageInterface 'en', 'DV goals', N'DV goals', 'messages';
exec insertLanguageInterface 'ru', 'DV goals', N'Цели DV', 'messages';
exec insertLanguageInterface 'uk', 'DV goals', N'Цілі DV', 'messages';

exec insertLanguageInterface 'en', 'Debt overdue', N'Debt overdue', 'messages';
exec insertLanguageInterface 'ru', 'Debt overdue', N'Просроченная задолженность', 'messages';
exec insertLanguageInterface 'uk', 'Debt overdue', N'Прострочена заборгованість', 'messages';

exec insertLanguageInterface 'en', 'Application status', N'Application status', 'messages';
exec insertLanguageInterface 'ru', 'Application status', N'Статус заявления', 'messages';
exec insertLanguageInterface 'uk', 'Application status', N'Статус заяви', 'messages';

exec insertLanguageInterface 'en', 'Current overdue', N'Current overdue', 'messages';
exec insertLanguageInterface 'ru', 'Current overdue', N'Текущая просрочка', 'messages';
exec insertLanguageInterface 'uk', 'Current overdue', N'Поточне прострочення', 'messages';

exec insertLanguageInterface 'en', 'Place an order in', N'Place an order in', 'messages';
exec insertLanguageInterface 'ru', 'Place an order in', N'Разместить заказ в', 'messages';
exec insertLanguageInterface 'uk', 'Place an order in', N'Розмістити замовлення в', 'messages';

exec insertLanguageInterface 'en', 'Percent of discount by default', N'Percent of discount by default', 'messages';
exec insertLanguageInterface 'ru', 'Percent of discount by default', N'Процент скидки по умолчанию', 'messages';
exec insertLanguageInterface 'uk', 'Percent of discount by default', N'Відсоток знижки за замовчуванням', 'messages';

exec insertLanguageInterface 'en', 'Action state', N'Action state', 'messages';
exec insertLanguageInterface 'ru', 'Action state', N'Состояние задачи', 'messages';
exec insertLanguageInterface 'uk', 'Action state', N'Стан завдання', 'messages';

exec insertLanguageInterface 'en', 'Date from', N'Date from', 'messages';
exec insertLanguageInterface 'ru', 'Date from', N'Дата начала задачи', 'messages';
exec insertLanguageInterface 'uk', 'Date from', N'Дата початку завдання', 'messages';

exec insertLanguageInterface 'en', 'Date till', N'Date till', 'messages';
exec insertLanguageInterface 'ru', 'Date till', N'Дата окончания задачи', 'messages';
exec insertLanguageInterface 'uk', 'Date till', N'Дата закінчення завдання', 'messages';

exec insertLanguageInterface 'en', 'Development map', N'Development map', 'messages';
exec insertLanguageInterface 'ru', 'Development map', N'Карта развития', 'messages';
exec insertLanguageInterface 'uk', 'Development map', N'Карта розвитку', 'messages';

exec insertLanguageInterface 'en', 'Next DV session target', N'Next DV session target', 'messages';
exec insertLanguageInterface 'ru', 'Next DV session target', N'Цель на следующую сессию DV', 'messages';
exec insertLanguageInterface 'uk', 'Next DV session target', N'Мета на наступну сесію DV', 'messages';

exec insertLanguageInterface 'en', 'Promo materials', N'Promo materials', 'messages';
exec insertLanguageInterface 'ru', 'Promo materials', N'Промо-материалы', 'messages';
exec insertLanguageInterface 'uk', 'Promo materials', N'Промо-матеріали', 'messages';

exec insertLanguageInterface 'en', 'Show File Id', N'Show File Id', 'messages';
exec insertLanguageInterface 'ru', 'Show File Id', N'Показать ID файла', 'messages';
exec insertLanguageInterface 'uk', 'Show File Id', N'Показати ID файлу', 'messages';
GO

------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------

------------------------------------------------------------------------------------------------------------------------
-- Last translation fixes
------------------------------------------------------------------------------------------------------------------------

update po_dictionarygroup set name = 'Localization' where name = 'Локализация';
update po_dictionarygroup set name = 'General' where name = 'Общие';
update po_dictionarygroup set name = 'Companies' where name = 'Учреждения';
update po_dictionarygroup set name = 'Clients' where name = 'Клиенты';
update po_dictionarygroup set name = 'Tasks' where name = 'Визиты';
update po_dictionarygroup set name = 'Actions' where name = 'Задачи';
update po_dictionarygroup set name = 'Preparations' where name = 'Препараты';
update po_dictionarygroup set name = 'Promotional materials' where name = 'Промо-материалы';
update po_dictionary set name = 'User positions' where name = 'Должности пользователей';
update po_dictionary set name = 'Cycle plan model' where name = 'Модель плана цикла';
update po_dictionary set name = 'Users' where name = 'Пользователи';
update po_dictionary set name = 'User dictionaries' where name = 'Пользовательские справочники';
update po_dictionary set name = 'User dictionaries - values' where name = 'Пользовательские справочники - значения';
update po_dictionary set name = 'Product directions' where name = 'Продуктовые направления';
update po_dictionary set name = 'Step distribution in a cycle' where name = 'Распределение этапов в цикле';
update po_dictionary set name = 'Languages' where name = 'Языки';
update po_dictionary set name = 'Action states' where name = 'Состояния задач';
update po_dictionary set name = 'Action types' where name = 'Типы задач';
update po_dictionary set name = 'Grouping of specializations to target groups' where name = 'Группировка специализаций до таргет-групп';
update po_dictionary set name = 'Positions' where name = 'Должности';
update po_dictionary set name = 'Client categories' where name = 'Категории клиентов';
update po_dictionary set name = 'Clients' where name = 'Клиенты';
update po_dictionary set name = 'Loyalty' where name = 'Лояльность';
update po_dictionary set name = 'Branch offices' where name = 'Отделения';
update po_dictionary set name = 'Competitor preparations' where name = 'Препараты конкурентов';
update po_dictionary set name = 'Reasons for archiving clients' where name = 'Причины архивации клиентов';
update po_dictionary set name = 'Specialization' where name = 'Специализация';
update po_dictionary set name = 'Target groups' where name = 'Таргет-группы';
update po_dictionary set name = 'Types of clients' where name = 'Типы клиентов';
update po_dictionary set name = 'Categories of companies' where name = 'Категории учреждений';
update po_dictionary set name = 'Reasons for archiving companies' where name = 'Причины архивации учреждений';
update po_dictionary set name = 'Roles in relationships' where name = 'Роли во взамосвязях';
update po_dictionary set name = 'Binding category and type of company' where name = 'Связка категория и типа компании';
update po_dictionary set name = 'Binding of task type and company type' where name = 'Связка типа визита и типа компании';
update po_dictionary set name = 'Areas of activity' where name = 'Сферы деятельности';
update po_dictionary set name = 'Types of companies' where name = 'Типы учреждений';
update po_dictionary set name = 'Companies' where name = 'Учреждения';
update po_dictionary set name = 'Human settlements' where name = 'Населенные пункты';
update po_dictionary set name = 'Areas' where name = 'Области';
update po_dictionary set name = 'Belonging to regions' where name = 'Принадлежность регионов';
update po_dictionary set name = 'Regions' where name = 'Регионы';
update po_dictionary set name = 'Countries' where name = 'Страны';
update po_dictionary set name = 'Types of settlement' where name = 'Типы населенного пункта';
update po_dictionary set name = 'Street types' where name = 'Типы улиц';
update po_dictionary set name = 'Brands' where name = 'Бренды';
update po_dictionary set name = 'Brands of preparations' where name = 'Бренды препаратов';
update po_dictionary set name = 'Units of measurement' where name = 'Единицы измерений';
update po_dictionary set name = 'Preparations categories' where name = 'Категории препаратов';
update po_dictionary set name = 'Preparation lines' where name = 'Линии препарата';
update po_dictionary set name = 'Feedback on preparations' where name = 'Отзывы о препаратах';
update po_dictionary set name = 'Preparations' where name = 'Препараты';
update po_dictionary set name = 'Preparations results' where name = 'Результаты по препаратам';
update po_dictionary set name = 'Binding preparations and brands' where name = 'Связка препаратов и брендов';
update po_dictionary set name = 'Binding preparations and prod.directions' where name = 'Связка препаратов и прод.направлений';
update po_dictionary set name = 'Binding preparations and specialties' where name = 'Связка препаратов и специальностей';
update po_dictionary set name = 'Speciality' where name = 'Специальность';
update po_dictionary set name = 'Degree of loyalty to the preparaion' where name = 'Степень лояльности к препарату';
update po_dictionary set name = 'Edition forms' where name = 'Формы выпуска';
update po_dictionary set name = 'Brands of promotional materials' where name = 'Бренды промоматериалов';
update po_dictionary set name = 'Product directions for promotional materials' where name = 'Продуктовые направления промоматериалов';
update po_dictionary set name = 'Promotional materials' where name = 'Промоматериалы';
update po_dictionary set name = 'Prices of promotional materials' where name = 'Цены промоматериалов';
update po_dictionary set name = '(КАМ) Promotions' where name = '(КАМ) Акции';
update po_dictionary set name = '(КАМ) Agreements' where name = '(КАМ) Договора';
update po_dictionary set name = '(КАМ) Comments' where name = '(КАМ) Комментарии';
update po_dictionary set name = '(КАМ) Commitments' where name = '(КАМ) Обязательства';
update po_dictionary set name = 'Marketing activities' where name = 'Маркетинговые активности';
update po_dictionary set name = 'Tasks results' where name = 'Результаты визитов';
update po_dictionary set name = 'Tasks statuses' where name = 'Состояния визитов';
update po_dictionary set name = 'Tasks topics' where name = 'Темы визитов';
update po_dictionary set name = 'Tasks types' where name = 'Типы визитов';
update po_dictionary set name = 'Types of tasks purpose' where name = 'Типы цели визитов';
update po_dictionary set name = 'Task targets' where name = 'Цели визитов';
update po_dictionary set name = 'Task cycle' where name = 'Цикл визитов';
update po_entityfields set name = 'Company type' where name = 'Тип учреждения';
update po_entityfields set name = 'Code' where name = 'Код';
update po_entityfields set name = 'Branch offices' where name = 'Отделения';
update po_entityfields set name = 'Postal code' where name = 'Почтовый индекс';
update po_entityfields set name = 'Area' where name = 'Область';
update po_entityfields set name = 'Human settlement' where name = 'Населенный пункт';
update po_entityfields set name = 'Street type' where name = 'Тип улицы';
update po_entityfields set name = 'Street' where name = 'Улица';
update po_entityfields set name = 'House' where name = 'Дом';
update po_entityfields set name = 'Extras info. at the address' where name = 'Доп. инфо. по адресу';
update po_entityfields set name = 'Phone' where name = 'Телефон';
update po_entityfields set name = 'Phone (extra)' where name = 'Телефон (доп.)';
update po_entityfields set name = 'E-mail' where name = 'Электронная почта';
update po_entityfields set name = 'Web-site' where name = 'Электронная страница';
update po_entityfields set name = 'Township area' where name = 'Район нас. пункта';
update po_entityfields set name = 'Responsible' where name = 'Ответственный';
update po_entityfields set name = 'Additionally responsible' where name = 'Доп. ответственные';
update po_entityfields set name = 'Verification status' where name = 'Статус верификации';
update po_entityfields set name = 'Reason for archiving' where name = 'Причина архивации';
update po_entityfields set name = 'Active' where name = 'Активный';
update po_entityfields set name = 'Related companies' where name = 'Связанные учреждения';
update po_entityfields set name = 'Coordinates' where name = 'Координаты';
update po_entityfields set name = 'District' where name = 'Округ';
update po_entityfields set name = 'Related doctors' where name = 'Связанные врачи';
update po_entityfields set name = 'Main company' where name = 'Главное учреждение';
update po_entityfields set name = 'Parent company' where name = 'Родительское учреждение';
update po_entityfields set name = 'Name' where name = 'Название';
update po_entityfields set name = 'Category' where name = 'Категория';
update po_entityfields set name = 'Main' where name = 'Главное';
update po_entityfields set name = 'Main company(ID)' where name = 'Главное учреждение(ID)';
update po_entityfields set name = 'Description' where name = 'Описание';
update po_entityfields set name = 'District' where name = 'Район';
update po_entityfields set name = 'Tax number' where name = 'Налоговый номер';
update po_entityfields set name = 'Account MDN ID' where name = 'Аккаунт MDN ID';
update po_entityfields set name = 'Street-Home' where name = 'Улица-дом';
update po_entityfields set name = 'Login' where name = 'Логин';
update po_entityfields set name = 'Visibility' where name = 'Видимость';
update po_entityfields set name = 'State of task' where name = 'Состояние визита';
update po_entityfields set name = 'Plan' where name = 'План';
update po_entityfields set name = 'Date of previous task' where name = 'Дата предыдущего визита';
update po_entityfields set name = 'Date of task' where name = 'Дата визита';
update po_entityfields set name = 'Date of next task' where name = 'Дата следующего визита';
update po_entityfields set name = 'Task type' where name = 'Тип визита';
update po_entityfields set name = 'Supervisor (Employee)' where name = 'Супервайзер (Сотрудник)';
update po_entityfields set name = 'Observers' where name = 'Наблюдатели';
update po_entityfields set name = 'Company' where name = 'Учреждение';
update po_entityfields set name = 'Client' where name = 'Клиент';
update po_entityfields set name = 'The purpose of the next visit' where name = 'Цель следующего визита';
update po_entityfields set name = 'Branch' where name = 'Отделение';
update po_entityfields set name = 'Brand comments' where name = 'Комментарии по брендам';
update po_entityfields set name = 'Training Block' where name = 'Блок тренингов';
update po_entityfields set name = 'Training participants' where name = 'Участники тренинга';
update po_entityfields set name = 'Training participant' where name = 'Участник тренинга';
update po_entityfields set name = 'PTC Evaluation Form' where name = 'Форма оценки ПТС';
update po_entityfields set name = 'Number of participants' where name = 'Кол-во участников';
update po_entityfields set name = 'Firstman' where name = 'Первостольники';
update po_entityfields set name = 'Specialization' where name = 'Специализация';
update po_entityfields set name = 'Lastname' where name = 'Фамилия';
update po_entityfields set name = 'Firstname' where name = 'Имя';
update po_entityfields set name = 'Midlename' where name = 'Отчество';
update po_entityfields set name = 'Signature' where name = 'Подпись';
update po_entityfields set name = 'Hide sip-call' where name = 'Скрытие кнопки вызова sip телефонии';
update po_entityfields set name = 'Administrative relations' where name = 'Административные связи';
update po_entityfields set name = 'Social relations' where name = 'Социальные связи';
update po_entityfields set name = 'Categoryprep' where name = 'Автоматическая категория';
update po_entityfields set name = 'Morion ID' where name = 'MDM ID Клиент';
update po_entityfields set name = 'Case number' where name = 'Номер дела';
update po_entityfields set name = 'Additional workspace' where name = 'Доп. места работы';
update po_entityfields set name = 'Position' where name = 'Должность';
update po_entityfields set name = 'Company' where name = 'Учреждение';
update po_entityfields set name = 'Department' where name = 'Отделение';
update po_entityfields set name = 'Additional department' where name = 'Доп. отделения';
update po_entityfields set name = 'Contact type' where name = 'Тип клиента';
update po_entityfields set name = 'Sex' where name = 'Пол';
update po_entityfields set name = 'Birthdate' where name = 'Дата рождения';
update po_entityfields set name = 'Company relation' where name = 'Прескрайбер-аптека';
update po_entityfields set name = 'Additional specializations' where name = 'Доп. специализации';
update po_entityfields set name = 'Action state' where name = 'Состояние задачи';
update po_entityfields set name = 'Date from' where name = 'Дата начала задачи';
update po_entityfields set name = 'Date till' where name = 'Дата окончания задачи';
update po_entityfields set name = 'Action type' where name = 'Тип задачи';
update po_entityfields set name = 'Description' where name = 'Комментарии';
update po_entityfields set name = 'Development map' where name = 'Карта развития';
update po_entityfields set name = 'Brands' where name = 'Бренды';
update po_entityfields set name = 'Promo materials' where name = 'Промо-материалы';
update po_entityfields set name = 'Planogram (promo for control)' where name = 'Планограмма (промо для контроля)';
update po_entityfields set name = 'Projects' where name = 'Проекты';
update po_entityfields set name = 'Share' where name = 'Акция';
update po_entityfields set name = 'Tender list' where name = 'Тендерный список';
update po_entityfields set name = 'KAM Agreement' where name = 'Договоренность КАМ';
update po_entityfields set name = 'Sales Plan Fact KAM' where name = 'План-факт продаж КАМ';
update po_entityfields set name = 'Goals' where name = 'Цели';
update po_entityfields set name = 'Marketing projects' where name = 'Маркетинговые проекты';
update po_entityfields set name = 'Objectives of double task' where name = 'Цели двойного визита';
update po_entityfields set name = 'Note' where name = 'Примечание';
update po_entityfields set name = 'Task result' where name = 'Результат визита';
update po_entityfields set name = 'Previous task result' where name = 'Результат прошлого визита';
update po_entityfields set name = 'Topics of task' where name = 'Темы визита';
update po_entityfields set name = 'MHH Questionnaires' where name = 'МНН Анкеты';
update po_entityfields set name = 'QR Promocode' where name = 'QR Промокод';
update po_entityfields set name = 'Hide sip telephony call button' where name = 'Скрытие кнопки вызова sip телефонии';
GO

exec insertLanguageInterface 'en', 'Branch offices', N'Branch offices', 'messages';
exec insertLanguageInterface 'ru', 'Branch offices', N'Отделения', 'messages';
exec insertLanguageInterface 'uk', 'Branch offices', N'Відділення', 'messages';

exec insertLanguageInterface 'en', 'Postal code', N'Postal code', 'messages';
exec insertLanguageInterface 'ru', 'Postal code', N'Почтовый индекс', 'messages';
exec insertLanguageInterface 'uk', 'Postal code', N'Поштовий індекс', 'messages';

exec insertLanguageInterface 'en', 'Area', N'Area', 'messages';
exec insertLanguageInterface 'ru', 'Area', N'Область', 'messages';
exec insertLanguageInterface 'uk', 'Area', N'Область', 'messages';

exec insertLanguageInterface 'en', 'Human settlement', N'Human settlement', 'messages';
exec insertLanguageInterface 'ru', 'Human settlement', N'Населенный пункт', 'messages';
exec insertLanguageInterface 'uk', 'Human settlement', N'Населений пункт', 'messages';

exec insertLanguageInterface 'en', 'Extras info. at the address', N'Extras info. at the address', 'messages';
exec insertLanguageInterface 'ru', 'Extras info. at the address', N'Доп. инфо. по адресу', 'messages';
exec insertLanguageInterface 'uk', 'Extras info. at the address', N'Додат. інф. по адресі', 'messages';

exec insertLanguageInterface 'en', 'Phone (extra)', N'Phone (extra)', 'messages';
exec insertLanguageInterface 'ru', 'Phone (extra)', N'Телефон (доп.)', 'messages';
exec insertLanguageInterface 'uk', 'Phone (extra)', N'Телефон (додатк.)', 'messages';

exec insertLanguageInterface 'en', 'Cycle plan model', N'Cycle plan model', 'messages';
exec insertLanguageInterface 'ru', 'Cycle plan model', N'Модель плана цикла', 'messages';
exec insertLanguageInterface 'uk', 'Cycle plan model', N'Модель плану циклу', 'messages';

exec insertLanguageInterface 'en', 'User positions', N'User positions', 'messages';
exec insertLanguageInterface 'ru', 'User positions', N'Должности пользователей', 'messages';
exec insertLanguageInterface 'uk', 'User positions', N'Посади користувачів', 'messages';

exec insertLanguageInterface 'en', 'Users', N'Users', 'messages';
exec insertLanguageInterface 'ru', 'Users', N'Пользователи', 'messages';
exec insertLanguageInterface 'uk', 'Users', N'Користувачі', 'messages';

exec insertLanguageInterface 'en', 'User dictionaries', N'User dictionaries', 'messages';
exec insertLanguageInterface 'ru', 'User dictionaries', N'Справочники пользователей', 'messages';
exec insertLanguageInterface 'uk', 'User dictionaries', N'Довідники користувачів', 'messages';

exec insertLanguageInterface 'en', 'User dictionaries - values', N'User dictionaries - values', 'messages';
exec insertLanguageInterface 'ru', 'User dictionaries - values', N'Справочники пользователей - значения', 'messages';
exec insertLanguageInterface 'uk', 'User dictionaries - values', N'Довідники користувачів - значення', 'messages';

exec insertLanguageInterface 'en', 'Product directions', N'Product directions', 'messages';
exec insertLanguageInterface 'ru', 'Product directions', N'Продуктовые направления', 'messages';
exec insertLanguageInterface 'uk', 'Product directions', N'Продуктові напрямки', 'messages';

exec insertLanguageInterface 'en', 'Step distribution in a cycle', N'Step distribution in a cycle', 'messages';
exec insertLanguageInterface 'ru', 'Step distribution in a cycle', N'Распределение этапов в цикле', 'messages';
exec insertLanguageInterface 'uk', 'Step distribution in a cycle', N'Розподіл етапів в циклі', 'messages';

exec insertLanguageInterface 'en', 'Grouping of specializations to target groups', N'Grouping of specializations to target groups', 'messages';
exec insertLanguageInterface 'ru', 'Grouping of specializations to target groups', N'Группировка специализаций по таргет-группам', 'messages';
exec insertLanguageInterface 'uk', 'Grouping of specializations to target groups', N'Групування спеціалізацій по тергет-групам', 'messages';

exec insertLanguageInterface 'en', 'Positions', N'Positions', 'messages';
exec insertLanguageInterface 'ru', 'Positions', N'Должности', 'messages';
exec insertLanguageInterface 'uk', 'Positions', N'Посади', 'messages';

exec insertLanguageInterface 'en', 'Client categories', N'Client categories', 'messages';
exec insertLanguageInterface 'ru', 'Client categories', N'Категории клиентов', 'messages';
exec insertLanguageInterface 'uk', 'Client categories', N'Категорії клієнтів', 'messages';

exec insertLanguageInterface 'en', 'Clients', N'Clients', 'messages';
exec insertLanguageInterface 'ru', 'Clients', N'Клиенты', 'messages';
exec insertLanguageInterface 'uk', 'Clients', N'Клієнти', 'messages';

exec insertLanguageInterface 'en', 'Loyalty', N'Loyalty', 'messages';
exec insertLanguageInterface 'ru', 'Loyalty', N'Лояльность', 'messages';
exec insertLanguageInterface 'uk', 'Loyalty', N'Лояльність', 'messages';

exec insertLanguageInterface 'en', 'Branch offices', N'Branch offices', 'messages';
exec insertLanguageInterface 'ru', 'Branch offices', N'Отделения', 'messages';
exec insertLanguageInterface 'uk', 'Branch offices', N'Відділи', 'messages';

exec insertLanguageInterface 'en', 'Competitor preparations', N'Competitor preparations', 'messages';
exec insertLanguageInterface 'ru', 'Competitor preparations', N'Препараты конкурентов', 'messages';
exec insertLanguageInterface 'uk', 'Competitor preparations', N'Препарати конкурентів', 'messages';

exec insertLanguageInterface 'en', 'Reasons for archiving clients', N'Reasons for archiving clients', 'messages';
exec insertLanguageInterface 'ru', 'Reasons for archiving clients', N'Причины архивации клиентов', 'messages';
exec insertLanguageInterface 'uk', 'Reasons for archiving clients', N'Причини архівації клієнтів', 'messages';

exec insertLanguageInterface 'en', 'Specialization', N'Specialization', 'messages';
exec insertLanguageInterface 'ru', 'Specialization', N'Специализация', 'messages';
exec insertLanguageInterface 'uk', 'Specialization', N'Спеціалізація', 'messages';

exec insertLanguageInterface 'en', 'Types of clients', N'Types of clients', 'messages';
exec insertLanguageInterface 'ru', 'Types of clients', N'Типы клиентов', 'messages';
exec insertLanguageInterface 'uk', 'Types of clients', N'Типи клієнтів', 'messages';

exec insertLanguageInterface 'en', 'Target groups', N'Target groups', 'messages';
exec insertLanguageInterface 'ru', 'Target groups', N'Таргет-группы', 'messages';
exec insertLanguageInterface 'uk', 'Target groups', N'Таргет-групи', 'messages';

exec insertLanguageInterface 'en', 'Categories of companies', N'Categories of companies', 'messages';
exec insertLanguageInterface 'ru', 'Categories of companies', N'Категории учреждений', 'messages';
exec insertLanguageInterface 'uk', 'Categories of companies', N'Категорії установ', 'messages';

exec insertLanguageInterface 'en', 'Reasons for archiving companies', N'Reasons for archiving companies', 'messages';
exec insertLanguageInterface 'ru', 'Reasons for archiving companies', N'Причины архивации учреждений', 'messages';
exec insertLanguageInterface 'uk', 'Reasons for archiving companies', N'Причини архівації установ', 'messages';

exec insertLanguageInterface 'en', 'Roles in relationships', N'Roles in relationships', 'messages';
exec insertLanguageInterface 'ru', 'Roles in relationships', N'Роли во взаимосвязях', 'messages';
exec insertLanguageInterface 'uk', 'Roles in relationships', N'Ролі у взаємозв''язках', 'messages';

exec insertLanguageInterface 'en', 'Binding category and type of company', N'Binding category and type of company', 'messages';
exec insertLanguageInterface 'ru', 'Binding category and type of company', N'Связка категории и типа учреждения', 'messages';
exec insertLanguageInterface 'uk', 'Binding category and type of company', N'Зв''язка категорії і типу установи', 'messages';

exec insertLanguageInterface 'en', 'Binding of task type and company type', N'Binding of task type and company type', 'messages';
exec insertLanguageInterface 'ru', 'Binding of task type and company type', N'Связка типа визита и типа учреждения', 'messages';
exec insertLanguageInterface 'uk', 'Binding of task type and company type', N'Зв''язка типу візиту і типу установи', 'messages';

exec insertLanguageInterface 'en', 'Areas of activity', N'Areas of activity', 'messages';
exec insertLanguageInterface 'ru', 'Areas of activity', N'Сферы деятельности', 'messages';
exec insertLanguageInterface 'uk', 'Areas of activity', N'Сфери діяльності', 'messages';

exec insertLanguageInterface 'en', 'Types of companies', N'Types of companies', 'messages';
exec insertLanguageInterface 'ru', 'Types of companies', N'Типы учреждений', 'messages';
exec insertLanguageInterface 'uk', 'Types of companies', N'Типи установ', 'messages';

exec insertLanguageInterface 'en', 'Companies', N'Companies', 'messages';
exec insertLanguageInterface 'ru', 'Companies', N'Учреждения', 'messages';
exec insertLanguageInterface 'uk', 'Companies', N'Установи', 'messages';

exec insertLanguageInterface 'en', 'Human settlements', N'Human settlements', 'messages';
exec insertLanguageInterface 'ru', 'Human settlements', N'Населенные пункты', 'messages';
exec insertLanguageInterface 'uk', 'Human settlements', N'Населені пункти', 'messages';

exec insertLanguageInterface 'en', 'Areas', N'Areas', 'messages';
exec insertLanguageInterface 'ru', 'Areas', N'Области', 'messages';
exec insertLanguageInterface 'uk', 'Areas', N'Області', 'messages';

exec insertLanguageInterface 'en', 'Belonging to regions', N'Belonging to regions', 'messages';
exec insertLanguageInterface 'ru', 'Belonging to regions', N'Принадлежность регионам', 'messages';
exec insertLanguageInterface 'uk', 'Belonging to regions', N'Належність регіонам', 'messages';

exec insertLanguageInterface 'en', 'Regions', N'Regions', 'messages';
exec insertLanguageInterface 'ru', 'Regions', N'Регионы', 'messages';
exec insertLanguageInterface 'uk', 'Regions', N'Регіони', 'messages';

exec insertLanguageInterface 'en', 'Countries', N'Countries', 'messages';
exec insertLanguageInterface 'ru', 'Countries', N'Страны', 'messages';
exec insertLanguageInterface 'uk', 'Countries', N'Країни', 'messages';

exec insertLanguageInterface 'en', 'Types of settlement', N'Types of settlement', 'messages';
exec insertLanguageInterface 'ru', 'Types of settlement', N'Типы населенных пунктов', 'messages';
exec insertLanguageInterface 'uk', 'Types of settlement', N'Типи населених пунктів', 'messages';

exec insertLanguageInterface 'en', 'Street types', N'Street types', 'messages';
exec insertLanguageInterface 'ru', 'Street types', N'Типы улиц', 'messages';
exec insertLanguageInterface 'uk', 'Street types', N'Типи вулиць', 'messages';

exec insertLanguageInterface 'en', 'Brands', N'Brands', 'messages';
exec insertLanguageInterface 'ru', 'Brands', N'Бренды', 'messages';
exec insertLanguageInterface 'uk', 'Brands', N'Бренди', 'messages';

exec insertLanguageInterface 'en', 'Binding preparations and brands', N'Binding preparations and brands', 'messages';
exec insertLanguageInterface 'ru', 'Binding preparations and brands', N'Связка препаратов и брендов', 'messages';
exec insertLanguageInterface 'uk', 'Binding preparations and brands', N'Зв''язка препаратів і брендів', 'messages';

exec insertLanguageInterface 'en', 'Binding preparations and prod.directions', N'Binding preparations and prod.directions', 'messages';
exec insertLanguageInterface 'ru', 'Binding preparations and prod.directions', N'Связка препаратов и прод. направлений', 'messages';
exec insertLanguageInterface 'uk', 'Binding preparations and prod.directions', N'Зв''язка препаратів і прод. напрямків', 'messages';

exec insertLanguageInterface 'en', 'Binding preparations and specialties', N'Binding preparations and specialties', 'messages';
exec insertLanguageInterface 'ru', 'Binding preparations and specialties', N'Связка препаратов и специальностей', 'messages';
exec insertLanguageInterface 'uk', 'Binding preparations and specialties', N'Зв''язка препаратів і спеціальностей', 'messages';

exec insertLanguageInterface 'en', 'Speciality', N'Speciality', 'messages';
exec insertLanguageInterface 'ru', 'Speciality', N'Специальность', 'messages';
exec insertLanguageInterface 'uk', 'Speciality', N'Спеціальність', 'messages';

exec insertLanguageInterface 'en', 'Brands of promotional materials', N'Brands of promotional materials', 'messages';
exec insertLanguageInterface 'ru', 'Brands of promotional materials', N'Бренды промоматериалов', 'messages';
exec insertLanguageInterface 'uk', 'Brands of promotional materials', N'Бренди промоматеріалів', 'messages';

exec insertLanguageInterface 'en', 'Product directions for promotional materials', N'Product directions for promotional materials', 'messages';
exec insertLanguageInterface 'ru', 'Product directions for promotional materials', N'Прод. направления промоматериалов', 'messages';
exec insertLanguageInterface 'uk', 'Product directions for promotional materials', N'Прод. напрямки промоматеріалів', 'messages';

exec insertLanguageInterface 'en', 'Promotional materials', N'Promotional materials', 'messages';
exec insertLanguageInterface 'ru', 'Promotional materials', N'Промоматериалы', 'messages';
exec insertLanguageInterface 'uk', 'Promotional materials', N'Промоматеріали', 'messages';

exec insertLanguageInterface 'en', 'Prices of promotional materials', N'Prices of promotional materials', 'messages';
exec insertLanguageInterface 'ru', 'Prices of promotional materials', N'Цена промоматериалов', 'messages';
exec insertLanguageInterface 'uk', 'Prices of promotional materials', N'Ціна промоматеріалів', 'messages';

exec insertLanguageInterface 'en', '(КАМ) Agreements', N'(КАМ) Agreements', 'messages';
exec insertLanguageInterface 'ru', '(КАМ) Agreements', N'(КАМ) Договора', 'messages';
exec insertLanguageInterface 'uk', '(КАМ) Agreements', N'(КАМ) Договори', 'messages';

exec insertLanguageInterface 'en', 'Types of tasks purpose', N'Types of tasks purpose', 'messages';
exec insertLanguageInterface 'ru', 'Types of tasks purpose', N'Типы цели визитов', 'messages';
exec insertLanguageInterface 'uk', 'Types of tasks purpose', N'Типи цілі візитів', 'messages';

exec insertLanguageInterface 'en', 'Languages', N'Languages', 'messages';
exec insertLanguageInterface 'ru', 'Languages', N'Языки', 'messages';
exec insertLanguageInterface 'uk', 'Languages', N'Мови', 'messages';

exec insertLanguageInterface 'en', 'Action states', N'Action states', 'messages';
exec insertLanguageInterface 'ru', 'Action states', N'Состояния задач', 'messages';
exec insertLanguageInterface 'uk', 'Action states', N'Стани задач', 'messages';

exec insertLanguageInterface 'en', 'Action types', N'Action types', 'messages';
exec insertLanguageInterface 'ru', 'Action types', N'Типы задач', 'messages';
exec insertLanguageInterface 'uk', 'Action types', N'Типи задач', 'messages';

exec insertLanguageInterface 'en', 'Brands of preparations', N'Brands of preparations', 'messages';
exec insertLanguageInterface 'ru', 'Brands of preparations', N'Бренды препаратов', 'messages';
exec insertLanguageInterface 'uk', 'Brands of preparations', N'Бренди препаратів', 'messages';

exec insertLanguageInterface 'en', 'Units of measurement', N'Units of measurement', 'messages';
exec insertLanguageInterface 'ru', 'Units of measurement', N'Единицы измерений', 'messages';
exec insertLanguageInterface 'uk', 'Units of measurement', N'Одиниці вимірів', 'messages';

exec insertLanguageInterface 'en', 'Preparations categories', N'Preparations categories', 'messages';
exec insertLanguageInterface 'ru', 'Preparations categories', N'Категории препаратов', 'messages';
exec insertLanguageInterface 'uk', 'Preparations categories', N'Категорії препаратів', 'messages';

exec insertLanguageInterface 'en', 'Preparation lines', N'Preparation lines', 'messages';
exec insertLanguageInterface 'ru', 'Preparation lines', N'Линии препарата', 'messages';
exec insertLanguageInterface 'uk', 'Preparation lines', N'Лінії препарату', 'messages';

exec insertLanguageInterface 'en', 'Feedback on preparations', N'Feedback on preparations', 'messages';
exec insertLanguageInterface 'ru', 'Feedback on preparations', N'Отзывы о препаратах', 'messages';
exec insertLanguageInterface 'uk', 'Feedback on preparations', N'Відгуки про препарати', 'messages';

exec insertLanguageInterface 'en', 'Preparations', N'Preparations', 'messages';
exec insertLanguageInterface 'ru', 'Preparations', N'Препараты', 'messages';
exec insertLanguageInterface 'uk', 'Preparations', N'Препарати', 'messages';

exec insertLanguageInterface 'en', 'Preparations results', N'Preparations results', 'messages';
exec insertLanguageInterface 'ru', 'Preparations results', N'Результаты по препаратам', 'messages';
exec insertLanguageInterface 'uk', 'Preparations results', N'Результати по препаратам', 'messages';

exec insertLanguageInterface 'en', 'Degree of loyalty to the preparaion', N'Degree of loyalty to the preparaion', 'messages';
exec insertLanguageInterface 'ru', 'Degree of loyalty to the preparaion', N'Степень лояльности к препарату', 'messages';
exec insertLanguageInterface 'uk', 'Degree of loyalty to the preparaion', N'Ступінь лояльності до препарату', 'messages';

exec insertLanguageInterface 'en', 'Edition forms', N'Edition forms', 'messages';
exec insertLanguageInterface 'ru', 'Edition forms', N'Формы выпуска', 'messages';
exec insertLanguageInterface 'uk', 'Edition forms', N'Форми випуску', 'messages';

exec insertLanguageInterface 'en', '(КАМ) Promotions', N'(КАМ) Promotions', 'messages';
exec insertLanguageInterface 'ru', '(КАМ) Promotions', N'(КАМ) Акции', 'messages';
exec insertLanguageInterface 'uk', '(КАМ) Promotions', N'(КАМ) Акції', 'messages';

exec insertLanguageInterface 'en', '(КАМ) Comments', N'(КАМ) Comments', 'messages';
exec insertLanguageInterface 'ru', '(КАМ) Comments', N'(КАМ) Комментарии', 'messages';
exec insertLanguageInterface 'uk', '(КАМ) Comments', N'(КАМ) Коментарі', 'messages';

exec insertLanguageInterface 'en', '(КАМ) Commitments', N'(КАМ) Commitments', 'messages';
exec insertLanguageInterface 'ru', '(КАМ) Commitments', N'(КАМ) Обязательства', 'messages';
exec insertLanguageInterface 'uk', '(КАМ) Commitments', N'(КАМ) Обов''язки', 'messages';

exec insertLanguageInterface 'en', 'Marketing activities', N'Marketing activities', 'messages';
exec insertLanguageInterface 'ru', 'Marketing activities', N'Маркетинговые активности', 'messages';
exec insertLanguageInterface 'uk', 'Marketing activities', N'Маркетингові активності', 'messages';

exec insertLanguageInterface 'en', 'Tasks results', N'Tasks results', 'messages';
exec insertLanguageInterface 'ru', 'Tasks results', N'Результаты визитов', 'messages';
exec insertLanguageInterface 'uk', 'Tasks results', N'Результати візитів', 'messages';

exec insertLanguageInterface 'en', 'Tasks statuses', N'Tasks statuses', 'messages';
exec insertLanguageInterface 'ru', 'Tasks statuses', N'Состояния визитов', 'messages';
exec insertLanguageInterface 'uk', 'Tasks statuses', N'Стани візитів', 'messages';

exec insertLanguageInterface 'en', 'Tasks topics', N'Tasks topics', 'messages';
exec insertLanguageInterface 'ru', 'Tasks topics', N'Темы визитов', 'messages';
exec insertLanguageInterface 'uk', 'Tasks topics', N'Теми візитів', 'messages';

exec insertLanguageInterface 'en', 'Tasks types', N'Tasks types', 'messages';
exec insertLanguageInterface 'ru', 'Tasks types', N'Типы визитов', 'messages';
exec insertLanguageInterface 'uk', 'Tasks types', N'Типи візитів', 'messages';

exec insertLanguageInterface 'en', 'Task targets', N'Task targets', 'messages';
exec insertLanguageInterface 'ru', 'Task targets', N'Цели визитов', 'messages';
exec insertLanguageInterface 'uk', 'Task targets', N'Цілі візитів', 'messages';

exec insertLanguageInterface 'en', 'Task cycle', N'Task cycle', 'messages';
exec insertLanguageInterface 'ru', 'Task cycle', N'Цикл визитов', 'messages';
exec insertLanguageInterface 'uk', 'Task cycle', N'Цикл візитів', 'messages';

exec insertLanguageInterface 'en', 'General', N'General', 'messages';
exec insertLanguageInterface 'ru', 'General', N'Общее', 'messages';
exec insertLanguageInterface 'uk', 'General', N'Загальне', 'messages';

exec insertLanguageInterface 'en', 'Localization', N'Localization', 'messages';
exec insertLanguageInterface 'ru', 'Localization', N'Локализация', 'messages';
exec insertLanguageInterface 'uk', 'Localization', N'Локалізація', 'messages';

exec insertLanguageInterface 'en', 'Tasks', N'Tasks', 'messages';
exec insertLanguageInterface 'ru', 'Tasks', N'Визиты', 'messages';
exec insertLanguageInterface 'uk', 'Tasks', N'Візити', 'messages';

exec insertLanguageInterface 'en', 'Actions', N'Actions', 'messages';
exec insertLanguageInterface 'ru', 'Actions', N'Задачи', 'messages';
exec insertLanguageInterface 'uk', 'Actions', N'Задачі', 'messages';

exec insertLanguageInterface 'en', 'Postal code', N'Postal code', 'messages';
exec insertLanguageInterface 'ru', 'Postal code', N'Почтовый индекс', 'messages';
exec insertLanguageInterface 'uk', 'Postal code', N'Поштовий індекс', 'messages';

exec insertLanguageInterface 'en', 'Area', N'Area', 'messages';
exec insertLanguageInterface 'ru', 'Area', N'Область', 'messages';
exec insertLanguageInterface 'uk', 'Area', N'Область', 'messages';

exec insertLanguageInterface 'en', 'Human settlement', N'Human settlement', 'messages';
exec insertLanguageInterface 'ru', 'Human settlement', N'Населенный пункт', 'messages';
exec insertLanguageInterface 'uk', 'Human settlement', N'Населений пункт', 'messages';

exec insertLanguageInterface 'en', 'Extras info. at the address', N'Extras info. at the address', 'messages';
exec insertLanguageInterface 'ru', 'Extras info. at the address', N'Доп. инфо. по адресу', 'messages';
exec insertLanguageInterface 'uk', 'Extras info. at the address', N'Додатк. інф. по адресі', 'messages';

exec insertLanguageInterface 'en', 'Phone (extra)', N'Phone (extra)', 'messages';
exec insertLanguageInterface 'ru', 'Phone (extra)', N'Телефон (доп.)', 'messages';
exec insertLanguageInterface 'uk', 'Phone (extra)', N'Телефон (додатк.)', 'messages';

exec insertLanguageInterface 'en', 'Web-site', N'Web-site', 'messages';
exec insertLanguageInterface 'ru', 'Web-site', N'Электронная страница', 'messages';
exec insertLanguageInterface 'uk', 'Web-site', N'Електронна сторінка', 'messages';

exec insertLanguageInterface 'en', 'Additionally responsible', N'Additionally responsible', 'messages';
exec insertLanguageInterface 'ru', 'Additionally responsible', N'Доп. ответственные', 'messages';
exec insertLanguageInterface 'uk', 'Additionally responsible', N'Додатк. відповідальні', 'messages';

exec insertLanguageInterface 'en', 'Verification status', N'Verification status', 'messages';
exec insertLanguageInterface 'ru', 'Verification status', N'Статус верификации', 'messages';
exec insertLanguageInterface 'uk', 'Verification status', N'Статус верифікації', 'messages';

exec insertLanguageInterface 'en', 'Reason for archiving', N'Reason for archiving', 'messages';
exec insertLanguageInterface 'ru', 'Reason for archiving', N'Причина архивации', 'messages';
exec insertLanguageInterface 'uk', 'Reason for archiving', N'Причина архівації', 'messages';

exec insertLanguageInterface 'en', 'Related companies', N'Related companies', 'messages';
exec insertLanguageInterface 'ru', 'Related companies', N'Связанные учреждения', 'messages';
exec insertLanguageInterface 'uk', 'Related companies', N'Зв''язані установи', 'messages';

exec insertLanguageInterface 'en', 'Coordinates', N'Coordinates', 'messages';
exec insertLanguageInterface 'ru', 'Coordinates', N'Координаты', 'messages';
exec insertLanguageInterface 'uk', 'Coordinates', N'Координати', 'messages';

exec insertLanguageInterface 'en', 'Related doctors', N'Related doctors', 'messages';
exec insertLanguageInterface 'ru', 'Related doctors', N'Связанные врачи', 'messages';
exec insertLanguageInterface 'uk', 'Related doctors', N'Пов''язані лікарі', 'messages';

exec insertLanguageInterface 'en', 'Main company', N'Main company', 'messages';
exec insertLanguageInterface 'ru', 'Main company', N'Главное учреждение', 'messages';
exec insertLanguageInterface 'uk', 'Main company', N'Головна установа', 'messages';

exec insertLanguageInterface 'en', 'Parent company', N'Parent company', 'messages';
exec insertLanguageInterface 'ru', 'Parent company', N'Родительское учреждение', 'messages';
exec insertLanguageInterface 'uk', 'Parent company', N'Батьківська установа', 'messages';

exec insertLanguageInterface 'en', 'Township area', N'Township area', 'messages';
exec insertLanguageInterface 'ru', 'Township area', N'Район нас. пункта', 'messages';
exec insertLanguageInterface 'uk', 'Township area', N'Район населеного пункту', 'messages';

exec insertLanguageInterface 'en', 'Main', N'Main', 'messages';
exec insertLanguageInterface 'ru', 'Main', N'Главное', 'messages';
exec insertLanguageInterface 'uk', 'Main', N'Головне', 'messages';

exec insertLanguageInterface 'en', 'Main company(ID)', N'Main company(ID)', 'messages';
exec insertLanguageInterface 'ru', 'Main company(ID)', N'Главное учреждение(ID)', 'messages';
exec insertLanguageInterface 'uk', 'Main company(ID)', N'Головна установа(ID)', 'messages';

exec insertLanguageInterface 'en', 'Tax number', N'Tax number', 'messages';
exec insertLanguageInterface 'ru', 'Tax number', N'Налоговый номер', 'messages';
exec insertLanguageInterface 'uk', 'Tax number', N'Податковий номер', 'messages';

exec insertLanguageInterface 'en', 'Account MDN ID', N'Account MDN ID', 'messages';
exec insertLanguageInterface 'ru', 'Account MDN ID', N'Аккаунт MDN ID', 'messages';
exec insertLanguageInterface 'uk', 'Account MDN ID', N'Аккаунт MDN ID', 'messages';

exec insertLanguageInterface 'en', 'Firstname', N'Firstname', 'messages';
exec insertLanguageInterface 'ru', 'Firstname', N'Имя', 'messages';
exec insertLanguageInterface 'uk', 'Firstname', N'Ім''я', 'messages';

exec insertLanguageInterface 'en', 'Middlename', N'Middlename', 'messages';
exec insertLanguageInterface 'ru', 'Middlename', N'Отчество', 'messages';
exec insertLanguageInterface 'uk', 'Middlename', N'По батькові', 'messages';

exec insertLanguageInterface 'en', 'Categoryprep', N'Categoryprep', 'messages';
exec insertLanguageInterface 'ru', 'Categoryprep', N'Автоматическая категория', 'messages';
exec insertLanguageInterface 'uk', 'Categoryprep', N'Автоматична категорія', 'messages';

exec insertLanguageInterface 'en', 'Morion ID', N'Morion ID', 'messages';
exec insertLanguageInterface 'ru', 'Morion ID', N'MDM ID Клиент', 'messages';
exec insertLanguageInterface 'uk', 'Morion ID', N'MDM ID Клієнт', 'messages';

exec insertLanguageInterface 'en', 'Case number', N'Case number', 'messages';
exec insertLanguageInterface 'ru', 'Case number', N'Номер дела', 'messages';
exec insertLanguageInterface 'uk', 'Case number', N'Номер справи', 'messages';

exec insertLanguageInterface 'en', 'Additional workspace', N'Additional workspace', 'messages';
exec insertLanguageInterface 'ru', 'Additional workspace', N'Доп. места работы', 'messages';
exec insertLanguageInterface 'uk', 'Additional workspace', N'Додатк. місця роботи', 'messages';

exec insertLanguageInterface 'en', 'Sex', N'Sex', 'messages';
exec insertLanguageInterface 'ru', 'Sex', N'Пол', 'messages';
exec insertLanguageInterface 'uk', 'Sex', N'Стать', 'messages';

exec insertLanguageInterface 'en', 'Additional specializations', N'Additional specializations', 'messages';
exec insertLanguageInterface 'ru', 'Additional specializations', N'Доп. специализации', 'messages';
exec insertLanguageInterface 'uk', 'Additional specializations', N'Додатк. спеціалізації', 'messages';

exec insertLanguageInterface 'en', 'Signature', N'Signature', 'messages';
exec insertLanguageInterface 'ru', 'Signature', N'Подпись', 'messages';
exec insertLanguageInterface 'uk', 'Signature', N'Підпис', 'messages';

exec insertLanguageInterface 'en', 'Hide sip-call', N'Hide sip-call', 'messages';
exec insertLanguageInterface 'ru', 'Hide sip-call', N'Скрытие кнопки вызова sip телефонии', 'messages';
exec insertLanguageInterface 'uk', 'Hide sip-call', N'Приховування кнопки виклику sip телефонії', 'messages';

exec insertLanguageInterface 'en', 'Administrative relations', N'Administrative relations', 'messages';
exec insertLanguageInterface 'ru', 'Administrative relations', N'Административные связи', 'messages';
exec insertLanguageInterface 'uk', 'Administrative relations', N'Адміністративні зв''язки', 'messages';

exec insertLanguageInterface 'en', 'Social relations', N'Social relations', 'messages';
exec insertLanguageInterface 'ru', 'Social relations', N'Социальные связи', 'messages';
exec insertLanguageInterface 'uk', 'Social relations', N'Соціальні звя''зки', 'messages';

exec insertLanguageInterface 'en', 'Company relation', N'Company relation', 'messages';
exec insertLanguageInterface 'ru', 'Company relation', N'Прескрайбер-аптека', 'messages';
exec insertLanguageInterface 'uk', 'Company relation', N'Прескрайбер-аптека', 'messages';

exec insertLanguageInterface 'en', 'Department', N'Department', 'messages';
exec insertLanguageInterface 'ru', 'Department', N'Отделение', 'messages';
exec insertLanguageInterface 'uk', 'Department', N'Відділення', 'messages';

exec insertLanguageInterface 'en', 'Additional department', N'Additional department', 'messages';
exec insertLanguageInterface 'ru', 'Additional department', N'Доп. отделения', 'messages';
exec insertLanguageInterface 'uk', 'Additional department', N'Додатк. відділення', 'messages';

exec insertLanguageInterface 'en', 'PTC Evaluation Form', N'PTC Evaluation Form', 'messages';
exec insertLanguageInterface 'ru', 'PTC Evaluation Form', N'Форма оценки ПТС', 'messages';
exec insertLanguageInterface 'uk', 'PTC Evaluation Form', N'Форма оцінки ПТС', 'messages';

exec insertLanguageInterface 'en', 'Date of previous task', N'Date of previous task', 'messages';
exec insertLanguageInterface 'ru', 'Date of previous task', N'Дата предыдущего визита', 'messages';
exec insertLanguageInterface 'uk', 'Date of previous task', N'Дата попереднього візиту', 'messages';

exec insertLanguageInterface 'en', 'Date of task', N'Date of task', 'messages';
exec insertLanguageInterface 'ru', 'Date of task', N'Дата визита', 'messages';
exec insertLanguageInterface 'uk', 'Date of task', N'Дата візиту', 'messages';

exec insertLanguageInterface 'en', 'Date of next task', N'Date of next task', 'messages';
exec insertLanguageInterface 'ru', 'Date of next task', N'Дата следующего визита', 'messages';
exec insertLanguageInterface 'uk', 'Date of next task', N'Дата наступного візиту', 'messages';

exec insertLanguageInterface 'en', 'Supervisor (Employee)', N'Supervisor (Employee)', 'messages';
exec insertLanguageInterface 'ru', 'Supervisor (Employee)', N'Супервайзер (Сотрудник)', 'messages';
exec insertLanguageInterface 'uk', 'Supervisor (Employee)', N'Супервайзер (Співробітник)', 'messages';

exec insertLanguageInterface 'en', 'Observers', N'Observers', 'messages';
exec insertLanguageInterface 'ru', 'Observers', N'Наблюдатели', 'messages';
exec insertLanguageInterface 'uk', 'Observers', N'Спостерігачі', 'messages';

exec insertLanguageInterface 'en', 'The purpose of the next visit', N'The purpose of the next visit', 'messages';
exec insertLanguageInterface 'ru', 'The purpose of the next visit', N'Цель следующего визита', 'messages';
exec insertLanguageInterface 'uk', 'The purpose of the next visit', N'Мета наступного візиту', 'messages';

exec insertLanguageInterface 'en', 'Brand comments', N'Brand comments', 'messages';
exec insertLanguageInterface 'ru', 'Brand comments', N'Комментарии по брендам', 'messages';
exec insertLanguageInterface 'uk', 'Brand comments', N'Коментарі по брендам', 'messages';

exec insertLanguageInterface 'en', 'Training Block', N'Training Block', 'messages';
exec insertLanguageInterface 'ru', 'Training Block', N'Блок тренингов', 'messages';
exec insertLanguageInterface 'uk', 'Training Block', N'Блок тренінгів', 'messages';

exec insertLanguageInterface 'en', 'Training participants', N'Training participants', 'messages';
exec insertLanguageInterface 'ru', 'Training participants', N'Участники тренинга', 'messages';
exec insertLanguageInterface 'uk', 'Training participants', N'Учасники тренінгу', 'messages';

exec insertLanguageInterface 'en', 'Training participant', N'Training participant', 'messages';
exec insertLanguageInterface 'ru', 'Training participant', N'Участник тренинга', 'messages';
exec insertLanguageInterface 'uk', 'Training participant', N'Учасник тренінгу', 'messages';

exec insertLanguageInterface 'en', 'Number of participants', N'Number of participants', 'messages';
exec insertLanguageInterface 'ru', 'Number of participants', N'Кол-во участников', 'messages';
exec insertLanguageInterface 'uk', 'Number of participants', N'Кількість учасників', 'messages';

exec insertLanguageInterface 'en', 'Planogram (promo for control)', N'Planogram (promo for control)', 'messages';
exec insertLanguageInterface 'ru', 'Planogram (promo for control)', N'Планограмма (промо для контроля)', 'messages';
exec insertLanguageInterface 'uk', 'Planogram (promo for control)', N'Планограмма (промо для контролю)', 'messages';

exec insertLanguageInterface 'en', 'Projects', N'Projects', 'messages';
exec insertLanguageInterface 'ru', 'Projects', N'Проекты', 'messages';
exec insertLanguageInterface 'uk', 'Projects', N'Проекти', 'messages';

exec insertLanguageInterface 'en', 'Share', N'Share', 'messages';
exec insertLanguageInterface 'ru', 'Share', N'Акция', 'messages';
exec insertLanguageInterface 'uk', 'Share', N'Акція', 'messages';

exec insertLanguageInterface 'en', 'Tender list', N'Tender list', 'messages';
exec insertLanguageInterface 'ru', 'Tender list', N'Тендерный список', 'messages';
exec insertLanguageInterface 'uk', 'Tender list', N'Тендерний список', 'messages';

exec insertLanguageInterface 'en', 'KAM Agreement', N'KAM Agreement', 'messages';
exec insertLanguageInterface 'ru', 'KAM Agreement', N'Договоренность КАМ', 'messages';
exec insertLanguageInterface 'uk', 'KAM Agreement', N'Домовленність КАМ', 'messages';

exec insertLanguageInterface 'en', 'Sales Plan Fact KAM', N'Sales Plan Fact KAM', 'messages';
exec insertLanguageInterface 'ru', 'Sales Plan Fact KAM', N'План-факт продаж КАМ', 'messages';
exec insertLanguageInterface 'uk', 'Sales Plan Fact KAM', N'План-факт продажів КАМ', 'messages';

exec insertLanguageInterface 'en', 'Goals', N'Goals', 'messages';
exec insertLanguageInterface 'ru', 'Goals', N'Цели', 'messages';
exec insertLanguageInterface 'uk', 'Goals', N'Цілі', 'messages';

exec insertLanguageInterface 'en', 'Marketing projects', N'Marketing projects', 'messages';
exec insertLanguageInterface 'ru', 'Marketing projects', N'Маркетинговые проекты', 'messages';
exec insertLanguageInterface 'uk', 'Marketing projects', N'Маркетингові проекти', 'messages';

exec insertLanguageInterface 'en', 'Objectives of double task', N'Objectives of double task', 'messages';
exec insertLanguageInterface 'ru', 'Objectives of double task', N'Цели двойного визита', 'messages';
exec insertLanguageInterface 'uk', 'Objectives of double task', N'Цілі подвійного візиту', 'messages';

exec insertLanguageInterface 'en', 'Note', N'Note', 'messages';
exec insertLanguageInterface 'ru', 'Note', N'Примечание', 'messages';
exec insertLanguageInterface 'uk', 'Note', N'Примітка', 'messages';

exec insertLanguageInterface 'en', 'Task result', N'Task result', 'messages';
exec insertLanguageInterface 'ru', 'Task result', N'Результат визита', 'messages';
exec insertLanguageInterface 'uk', 'Task result', N'Результат візиту', 'messages';

exec insertLanguageInterface 'en', 'Previous task result', N'Previous task result', 'messages';
exec insertLanguageInterface 'ru', 'Previous task result', N'Результат прошлого визита', 'messages';
exec insertLanguageInterface 'uk', 'Previous task result', N'Результат минулого візиту', 'messages';

exec insertLanguageInterface 'en', 'Topics of task', N'Topics of task', 'messages';
exec insertLanguageInterface 'ru', 'Topics of task', N'Темы визита', 'messages';
exec insertLanguageInterface 'uk', 'Topics of task', N'Теми візиту', 'messages';

exec insertLanguageInterface 'en', 'MHH Questionnaires', N'MHH Questionnaires', 'messages';
exec insertLanguageInterface 'ru', 'MHH Questionnaires', N'МНН Анкеты', 'messages';
exec insertLanguageInterface 'uk', 'MHH Questionnaires', N'МНН Анкети', 'messages';

exec insertLanguageInterface 'en', 'QR Promocode', N'QR Promocode', 'messages';
exec insertLanguageInterface 'ru', 'QR Promocode', N'QR Промокод', 'messages';
exec insertLanguageInterface 'uk', 'QR Promocode', N'QR Промокод', 'messages';

exec insertLanguageInterface 'en', 'Clinical situations', N'Clinical situations', 'messages';
exec insertLanguageInterface 'ru', 'Clinical situations', N'Клинические случаи', 'messages';
exec insertLanguageInterface 'uk', 'Clinical situations', N'Клінічні випадки', 'messages';

exec insertLanguageInterface 'en', 'Training', N'Training', 'messages';
exec insertLanguageInterface 'ru', 'Training', N'Тренинг', 'messages';
exec insertLanguageInterface 'uk', 'Training', N'Тренінг', 'messages';

exec insertLanguageInterface 'en', 'Evaluation form', N'Evaluation form', 'messages';
exec insertLanguageInterface 'ru', 'Evaluation form', N'Форма для оценки', 'messages';
exec insertLanguageInterface 'uk', 'Evaluation form', N'Форма для оцінки', 'messages';

exec insertLanguageInterface 'en', 'Test start date and time', N'Test start date and time', 'messages';
exec insertLanguageInterface 'ru', 'Test start date and time', N'Дата и время начала теста', 'messages';
exec insertLanguageInterface 'uk', 'Test start date and time', N'Дата і час початку тесту', 'messages';

exec insertLanguageInterface 'en', 'Date and time of the end; of the test', N'Date and time of the end; of the test', 'messages';
exec insertLanguageInterface 'ru', 'Date and time of the end; of the test', N'Дата и время окончания теста', 'messages';
exec insertLanguageInterface 'uk', 'Date and time of the end; of the test', N'Дата і час закінчення тесту', 'messages';

exec insertLanguageInterface 'en', 'Planogram', 'Planogram', 'messages';
exec insertLanguageInterface 'ru', 'Planogram', 'Планограмма', 'messages';
exec insertLanguageInterface 'uk', 'Planogram', 'Планограмма', 'messages';

exec insertLanguageInterface 'en', 'KAM sales plan-fact', N'KAM sales plan-fact', 'messages';
exec insertLanguageInterface 'ru', 'KAM sales plan-fact', N'План-факт продаж КАМ', 'messages';
exec insertLanguageInterface 'uk', 'KAM sales plan-fact', N'План-факт продажів КАМ', 'messages';

exec insertLanguageInterface 'en', 'Company goals', N'Company goals', 'messages';
exec insertLanguageInterface 'ru', 'Company goals', N'Цели учреждения', 'messages';
exec insertLanguageInterface 'uk', 'Company goals', N'Цілі установи', 'messages';

exec insertLanguageInterface 'en', 'DV goals', N'DV goals', 'messages';
exec insertLanguageInterface 'ru', 'DV goals', N'Цели DV', 'messages';
exec insertLanguageInterface 'uk', 'DV goals', N'Цілі DV', 'messages';

exec insertLanguageInterface 'en', 'Debt overdue', N'Debt overdue', 'messages';
exec insertLanguageInterface 'ru', 'Debt overdue', N'Просроченная задолженность', 'messages';
exec insertLanguageInterface 'uk', 'Debt overdue', N'Прострочена заборгованість', 'messages';

exec insertLanguageInterface 'en', 'Application status', N'Application status', 'messages';
exec insertLanguageInterface 'ru', 'Application status', N'Статус заявления', 'messages';
exec insertLanguageInterface 'uk', 'Application status', N'Статус заяви', 'messages';

exec insertLanguageInterface 'en', 'Current overdue', N'Current overdue', 'messages';
exec insertLanguageInterface 'ru', 'Current overdue', N'Текущая просрочка', 'messages';
exec insertLanguageInterface 'uk', 'Current overdue', N'Поточне прострочення', 'messages';

exec insertLanguageInterface 'en', 'Place an order in', N'Place an order in', 'messages';
exec insertLanguageInterface 'ru', 'Place an order in', N'Разместить заказ в', 'messages';
exec insertLanguageInterface 'uk', 'Place an order in', N'Розмістити замовлення в', 'messages';

exec insertLanguageInterface 'en', 'Percent of discount by default', N'Percent of discount by default', 'messages';
exec insertLanguageInterface 'ru', 'Percent of discount by default', N'Процент скидки по умолчанию', 'messages';
exec insertLanguageInterface 'uk', 'Percent of discount by default', N'Відсоток знижки за замовчуванням', 'messages';

exec insertLanguageInterface 'en', 'Action state', N'Action state', 'messages';
exec insertLanguageInterface 'ru', 'Action state', N'Состояние задачи', 'messages';
exec insertLanguageInterface 'uk', 'Action state', N'Стан завдання', 'messages';

exec insertLanguageInterface 'en', 'Date from', N'Date from', 'messages';
exec insertLanguageInterface 'ru', 'Date from', N'Дата начала задачи', 'messages';
exec insertLanguageInterface 'uk', 'Date from', N'Дата початку завдання', 'messages';

exec insertLanguageInterface 'en', 'Date till', N'Date till', 'messages';
exec insertLanguageInterface 'ru', 'Date till', N'Дата окончания задачи', 'messages';
exec insertLanguageInterface 'uk', 'Date till', N'Дата закінчення завдання', 'messages';

exec insertLanguageInterface 'en', 'Development map', N'Development map', 'messages';
exec insertLanguageInterface 'ru', 'Development map', N'Карта развития', 'messages';
exec insertLanguageInterface 'uk', 'Development map', N'Карта розвитку', 'messages';

exec insertLanguageInterface 'en', 'Next DV session target', N'Next DV session target', 'messages';
exec insertLanguageInterface 'ru', 'Next DV session target', N'Цель на следующую сессию DV', 'messages';
exec insertLanguageInterface 'uk', 'Next DV session target', N'Мета на наступну сесію DV', 'messages';

exec insertLanguageInterface 'en', 'Promo materials', N'Promo materials', 'messages';
exec insertLanguageInterface 'ru', 'Promo materials', N'Промо-материалы', 'messages';
exec insertLanguageInterface 'uk', 'Promo materials', N'Промо-матеріали', 'messages';

exec insertLanguageInterface 'en', 'Show File Id', N'Show File Id', 'messages';
exec insertLanguageInterface 'ru', 'Show File Id', N'Показать ID файла', 'messages';
exec insertLanguageInterface 'uk', 'Show File Id', N'Показати ID файлу', 'messages';
GO

------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------
