----------------------------------------------------------------------------------------------------
-- https://teamsoft.atlassian.net/browse/UKEKSPO-36
----------------------------------------------------------------------------------------------------
IF NOT EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS
              WHERE table_name = 'info_mcmcontentvariable' AND column_name = 'male_value')
ALTER TABLE info_mcmcontentvariable ADD male_value varchar(255) NULL
GO

IF NOT EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS
              WHERE table_name = 'info_mcmcontentvariable' AND column_name = 'female_value')
ALTER TABLE info_mcmcontentvariable ADD female_value varchar(255) NULL
GO
----------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------

----------------------------------------------------------------------------------------------------
-- https://teamsoft.atlassian.net/browse/MCM-417
----------------------------------------------------------------------------------------------------
IF EXISTS(select 1 from information_schema.constraint_column_usage where
        table_name='info_mcmdistributionaction' AND
        constraint_name='FK_info_mcmdistributionaction_info_task')
BEGIN
ALTER TABLE info_mcmdistributionaction DROP CONSTRAINT FK_info_mcmdistributionaction_info_task
END
GO
----------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------

------------------------------------------------------------------------------------------------------------------------
---https://teamsoft.atlassian.net/browse/MCM-366
------------------------------------------------------------------------------------------------------------------------
---info_rtcroom
IF OBJECT_ID(N'info_rtcroom', 'U') is NULL
begin
create table info_rtcroom(
    [id] int identity(1,1)  not null
    , [sip_user] varchar (255) null
            , [sip_pass] varchar (255) null
            , [responsible_id] int  null
            , [currenttime] datetime  null CONSTRAINT [DF_info_rtcroomcurrenttime] DEFAULT (getdate())
            , [time] timestamp  null
            , [moduser] varchar (16) null CONSTRAINT [DF_info_rtcroommoduser] DEFAULT ([dbo].[Get_CurrentCode]())
            , [guid] uniqueidentifier  null CONSTRAINT [DF_info_rtcroomguid] DEFAULT (newid())
            , CONSTRAINT [PK_info_rtcroom] PRIMARY KEY (id) ON [PRIMARY]
    , CONSTRAINT [FK_info_rtcroom_responsible_id] FOREIGN KEY ([responsible_id]) REFERENCES info_user ([id]) ON UPDATE CASCADE ON DELETE CASCADE
    )
end
else
begin
        if not exists(SELECT * FROM syscolumns sc JOIN sysobjects so ON sc.id = so.id join systypes st on st.xtype = sc.xtype WHERE so.Name = 'info_rtcroom' and sc.name = 'id' and st.name = 'int')
            exec('''Поле [id] отсутствует или не совпадает тип поля''')
        if not exists(SELECT * FROM syscolumns sc JOIN sysobjects so ON sc.id = so.id join systypes st on st.xtype = sc.xtype WHERE so.Name = 'info_rtcroom' and sc.name = 'sip_user' and st.name = 'varchar')
            exec('''Поле [sip_user] отсутствует или не совпадает тип поля''')
        if not exists(SELECT * FROM syscolumns sc JOIN sysobjects so ON sc.id = so.id join systypes st on st.xtype = sc.xtype WHERE so.Name = 'info_rtcroom' and sc.name = 'sip_pass' and st.name = 'varchar')
            exec('''Поле [sip_pass] отсутствует или не совпадает тип поля''')
        if not exists(SELECT * FROM syscolumns sc JOIN sysobjects so ON sc.id = so.id join systypes st on st.xtype = sc.xtype WHERE so.Name = 'info_rtcroom' and sc.name = 'responsible_id' and st.name = 'int')
            exec('''Поле [responsible_id] отсутствует или не совпадает тип поля''')
        if not exists(SELECT * FROM syscolumns sc JOIN sysobjects so ON sc.id = so.id join systypes st on st.xtype = sc.xtype WHERE so.Name = 'info_rtcroom' and sc.name = 'currenttime' and st.name = 'datetime')
            exec('''Поле [currenttime] отсутствует или не совпадает тип поля''')
        if not exists(SELECT * FROM syscolumns sc JOIN sysobjects so ON sc.id = so.id join systypes st on st.xtype = sc.xtype WHERE so.Name = 'info_rtcroom' and sc.name = 'time' and st.name = 'timestamp')
            exec('''Поле [time] отсутствует или не совпадает тип поля''')
        if not exists(SELECT * FROM syscolumns sc JOIN sysobjects so ON sc.id = so.id join systypes st on st.xtype = sc.xtype WHERE so.Name = 'info_rtcroom' and sc.name = 'moduser' and st.name = 'varchar')
            exec('''Поле [moduser] отсутствует или не совпадает тип поля''')
        if not exists(SELECT * FROM syscolumns sc JOIN sysobjects so ON sc.id = so.id join systypes st on st.xtype = sc.xtype WHERE so.Name = 'info_rtcroom' and sc.name = 'guid' and st.name = 'uniqueidentifier')
            exec('''Поле [guid] отсутствует или не совпадает тип поля''')
end
GO
if not exists (select * from dbo.sysobjects where id = object_id('del_info_rtcroom') and OBJECTPROPERTY(id, 'IsTrigger') = 1)
    exec('create trigger del_info_rtcroom on info_rtcroom for delete as if object_id(''empty.dbo.f_rep_del'') is null
insert into rep_del(tablename, del_guid) select ''info_rtcroom'', guid from deleted')
GO
if not exists (select * from dbo.sysobjects where id = object_id('moduser_info_rtcroom') and OBJECTPROPERTY(id, 'IsTrigger') = 1)
    exec('create trigger moduser_info_rtcroom on info_rtcroom for insert, update as if not update(moduser)
update info_rtcroom set moduser = dbo.Get_CurrentCode(), currenttime = getdate() where id in (select id from inserted)')
GO
-----info_rtcroomreserve
IF OBJECT_ID(N'info_rtcroomreserve', 'U') is NULL
begin
create table info_rtcroomreserve(
    [id] int identity(1,1)  not null
    , [datefrom] datetime null
            , [datetill] datetime null
            , [notes] varchar (255) null
            , [rtcroom_id] int  null
            , [contact_id] int  null
            , [task_id] int  null
            , [currenttime] datetime  null CONSTRAINT [DF_info_rtcroomreservecurrenttime] DEFAULT (getdate())
            , [time] timestamp  null
            , [moduser] varchar (16) null CONSTRAINT [DF_info_rtcroomreservemoduser] DEFAULT ([dbo].[Get_CurrentCode]())
            , [guid] uniqueidentifier  null CONSTRAINT [DF_info_rtcroomreserveguid] DEFAULT (newid())
            , CONSTRAINT [PK_info_rtcroomreserve] PRIMARY KEY (id) ON [PRIMARY]
    , CONSTRAINT [FK_info_rtcroomreserve_rtcroom_id] FOREIGN KEY ([rtcroom_id]) REFERENCES info_rtcroom ([id]) ON UPDATE CASCADE ON DELETE CASCADE
    , CONSTRAINT [FK_info_rtcroomreserve_contact_id] FOREIGN KEY ([contact_id]) REFERENCES info_contact ([id]) ON UPDATE CASCADE ON DELETE CASCADE
    , CONSTRAINT [FK_info_rtcroomreserve_task_id] FOREIGN KEY ([task_id]) REFERENCES info_task ([id]) ON UPDATE CASCADE ON DELETE CASCADE
    )
end
else
begin
        if not exists(SELECT * FROM syscolumns sc JOIN sysobjects so ON sc.id = so.id join systypes st on st.xtype = sc.xtype WHERE so.Name = 'info_rtcroomreserve' and sc.name = 'id' and st.name = 'int')
            exec('''Поле [id] отсутствует или не совпадает тип поля''')
        if not exists(SELECT * FROM syscolumns sc JOIN sysobjects so ON sc.id = so.id join systypes st on st.xtype = sc.xtype WHERE so.Name = 'info_rtcroomreserve' and sc.name = 'datefrom' and st.name = 'datetime')
            exec('''Поле [datefrom] отсутствует или не совпадает тип поля''')
        if not exists(SELECT * FROM syscolumns sc JOIN sysobjects so ON sc.id = so.id join systypes st on st.xtype = sc.xtype WHERE so.Name = 'info_rtcroomreserve' and sc.name = 'datetill' and st.name = 'datetime')
            exec('''Поле [datetill] отсутствует или не совпадает тип поля''')
        if not exists(SELECT * FROM syscolumns sc JOIN sysobjects so ON sc.id = so.id join systypes st on st.xtype = sc.xtype WHERE so.Name = 'info_rtcroomreserve' and sc.name = 'notes' and st.name = 'varchar')
            exec('''Поле [notes] отсутствует или не совпадает тип поля''')
        if not exists(SELECT * FROM syscolumns sc JOIN sysobjects so ON sc.id = so.id join systypes st on st.xtype = sc.xtype WHERE so.Name = 'info_rtcroomreserve' and sc.name = 'rtcroom_id' and st.name = 'int')
            exec('''Поле [rtcroom_id] отсутствует или не совпадает тип поля''')
        if not exists(SELECT * FROM syscolumns sc JOIN sysobjects so ON sc.id = so.id join systypes st on st.xtype = sc.xtype WHERE so.Name = 'info_rtcroomreserve' and sc.name = 'contact_id' and st.name = 'int')
            exec('''Поле [contact_id] отсутствует или не совпадает тип поля''')
        if not exists(SELECT * FROM syscolumns sc JOIN sysobjects so ON sc.id = so.id join systypes st on st.xtype = sc.xtype WHERE so.Name = 'info_rtcroomreserve' and sc.name = 'task_id' and st.name = 'int')
            exec('''Поле [task_id] отсутствует или не совпадает тип поля''')
        if not exists(SELECT * FROM syscolumns sc JOIN sysobjects so ON sc.id = so.id join systypes st on st.xtype = sc.xtype WHERE so.Name = 'info_rtcroomreserve' and sc.name = 'currenttime' and st.name = 'datetime')
            exec('''Поле [currenttime] отсутствует или не совпадает тип поля''')
        if not exists(SELECT * FROM syscolumns sc JOIN sysobjects so ON sc.id = so.id join systypes st on st.xtype = sc.xtype WHERE so.Name = 'info_rtcroomreserve' and sc.name = 'time' and st.name = 'timestamp')
            exec('''Поле [time] отсутствует или не совпадает тип поля''')
        if not exists(SELECT * FROM syscolumns sc JOIN sysobjects so ON sc.id = so.id join systypes st on st.xtype = sc.xtype WHERE so.Name = 'info_rtcroomreserve' and sc.name = 'moduser' and st.name = 'varchar')
            exec('''Поле [moduser] отсутствует или не совпадает тип поля''')
        if not exists(SELECT * FROM syscolumns sc JOIN sysobjects so ON sc.id = so.id join systypes st on st.xtype = sc.xtype WHERE so.Name = 'info_rtcroomreserve' and sc.name = 'guid' and st.name = 'uniqueidentifier')
            exec('''Поле [guid] отсутствует или не совпадает тип поля''')
end
GO
if not exists (select * from dbo.sysobjects where id = object_id('del_info_rtcroomreserve') and OBJECTPROPERTY(id, 'IsTrigger') = 1)
    exec('create trigger del_info_rtcroomreserve on info_rtcroomreserve for delete as if object_id(''empty.dbo.f_rep_del'') is null
insert into rep_del(tablename, del_guid) select ''info_rtcroomreserve'', guid from deleted')
GO
if not exists (select * from dbo.sysobjects where id = object_id('moduser_info_rtcroomreserve') and OBJECTPROPERTY(id, 'IsTrigger') = 1)
    exec('create trigger moduser_info_rtcroomreserve on info_rtcroomreserve for insert, update as if not update(moduser)
update info_rtcroomreserve set moduser = dbo.Get_CurrentCode(), currenttime = getdate() where id in (select id from inserted)')
GO
---
---info_callstatistics
IF OBJECT_ID('info_callstatistics','u') IS NULL BEGIN
CREATE TABLE info_callstatistics (
                                     id int identity (1,1) not null,
                                     datetime datetime not null,
                                     duration int null,
                                     user_id int null,
                                     contact_id int null,
                                     company_id int null,
                                     task_id int null,
                                     rtcroomreserve_id int null,
    [currenttime] [datetime]           NULL,
    [time]        [timestamp]          NULL,
    [moduser]     [varchar](16)        NULL,
    [guid]        [uniqueidentifier]   NULL,
    primary key (id)
    )
ALTER TABLE info_callstatistics
    ADD CONSTRAINT [DF_info_callstatistics_currenttime] DEFAULT (getdate()) FOR [currenttime]
ALTER TABLE info_callstatistics
    ADD CONSTRAINT [DF_info_callstatistics_moduser] DEFAULT ([dbo].[Get_CurrentCode]()) FOR [moduser]
ALTER TABLE info_callstatistics
    ADD CONSTRAINT [DF_info_callstatistics_guid] DEFAULT (newid()) FOR [guid]
end
GO
if not exists (SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'info_callstatistics' AND Column_Name = 'rtcroomreserve_id')
begin
alter table info_callstatistics add rtcroomreserve_id int null
ALTER TABLE info_callstatistics ADD CONSTRAINT fk_info_callstatistics_rtcroomreserve_id FOREIGN KEY (rtcroomreserve_id) REFERENCES info_rtcroomreserve (id) ON UPDATE CASCADE ON DELETE CASCADE
end
GO
IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE COLUMN_NAME = 'contact_id' and TABLE_NAME = 'info_callstatistics') BEGIN
ALTER TABLE info_callstatistics ADD contact_id INT NULL;
ALTER TABLE info_callstatistics ADD CONSTRAINT fk_info_callstatistics_contact_id FOREIGN KEY (contact_id) REFERENCES info_contact (id);
end
GO
IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE COLUMN_NAME = 'company_id' and TABLE_NAME = 'info_callstatistics') BEGIN
ALTER TABLE info_callstatistics ADD company_id INT NULL;
ALTER TABLE info_callstatistics ADD CONSTRAINT fk_info_callstatistics_company_id FOREIGN KEY (company_id) REFERENCES info_company (id);
end
GO
IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE COLUMN_NAME = 'task_id' and TABLE_NAME = 'info_callstatistics') BEGIN
ALTER TABLE info_callstatistics ADD task_id INT NULL;
ALTER TABLE info_callstatistics ADD CONSTRAINT fk_info_callstatistics_task_id FOREIGN KEY (task_id) REFERENCES info_task (id) ON UPDATE CASCADE ON DELETE CASCADE;
end
GO
IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE COLUMN_NAME = 'user_id' and TABLE_NAME = 'info_callstatistics') BEGIN
ALTER TABLE info_callstatistics ADD user_id INT NULL;
ALTER TABLE info_callstatistics ADD CONSTRAINT fk_info_callstatistics_user_id FOREIGN KEY (user_id) REFERENCES info_user (id);
end
GO
IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE COLUMN_NAME = 'contactphone_id' and TABLE_NAME = 'info_callstatistics') BEGIN
ALTER TABLE info_callstatistics ADD contactphone_id INT NULL;
ALTER TABLE info_callstatistics ADD CONSTRAINT fk_info_callstatistics_contactphone_id FOREIGN KEY (contactphone_id) REFERENCES info_contactphone (id);
end
GO
---
--info_user
IF NOT EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'info_user' AND COLUMN_NAME = 'siplogin')
BEGIN
ALTER TABLE info_user ADD siplogin VARCHAR(255) NULL
END
GO
IF NOT EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'info_user' AND COLUMN_NAME = 'sippass')
BEGIN
ALTER TABLE info_user ADD sippass VARCHAR(255) NULL
END
GO
--info_task
IF NOT EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'info_rtcroomreserve' AND COLUMN_NAME = 'task_id')
BEGIN
ALTER TABLE info_rtcroomreserve ADD task_id INT NULL
end
GO
IF NOT EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'info_user' AND COLUMN_NAME = 'edetailing_enabled')
BEGIN
ALTER TABLE info_user ADD edetailing_enabled int NULL
end
GO

IF NOT EXISTS(SELECT 1 FROM info_role WHERE code = 'ROLE_RTC_USER')
BEGIN
INSERT INTO info_role (name, code) values ('RTC_USER', 'ROLE_RTC_USER')
end
GO

IF NOT EXISTS(SELECT 1 FROM info_user WHERE name = 'rtc-user')
BEGIN
INSERT INTO info_user (name, po_login, po_password, role_id) values ('rtc-user','rtc', 'y8we0SDFHhfw9e8hgfp9wEU0Gdf9W4EG', (select top 1 id from info_role where code = 'ROLE_RTC_USER'))
end
GO
----------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------
