----------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------
-- https://teamsoft.atlassian.net/browse/FARMAK-288
----------------------------------------------------------------------------------------------------
exec insertLanguageInterface 'en', 'GPS disabled', N'GPS disabled', 'messages'
exec insertLanguageInterface 'ru', 'GPS disabled', N'Отключить GPS трекер', 'messages'
exec insertLanguageInterface 'uk', 'GPS disabled', N'Вимкнути GPS трекер', 'messages'
GO
----------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------
-- https://teamsoft.atlassian.net/browse/FARMAK-278
----------------------------------------------------------------------------------------------------
IF OBJECT_ID(N'[info_plantaskperiod]', 'U') IS NULL
begin
CREATE TABLE [dbo].[info_plantaskperiod]
(
    [id]             [int] IDENTITY (1,1) NOT NULL,
    [plandetail_id]  [int]                NULL,
    [num]            [int]                NULL,
    [dt1]            [datetime]           NULL,
    [dt2]            [datetime]           NULL,
    [currenttime]    [datetime]           NULL,
    [time]           [timestamp]          NULL,
    [moduser]        [varchar](16)        NULL,
    [guid]           [uniqueidentifier]   NULL,
    CONSTRAINT [PK_info_plantaskperiod] PRIMARY KEY CLUSTERED
(
[id] ASC
) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
    ) ON [PRIMARY]

ALTER TABLE [dbo].[info_plantaskperiod] ADD CONSTRAINT [DF_info_plantaskperiodcurrenttime] DEFAULT (getdate()) FOR [currenttime]
ALTER TABLE [dbo].[info_plantaskperiod] ADD CONSTRAINT [DF_info_plantaskperiodmoduser] DEFAULT ([dbo].[Get_CurrentCode]()) FOR [moduser]
ALTER TABLE [dbo].[info_plantaskperiod] ADD CONSTRAINT [DF_info_plantaskperiodguid] DEFAULT (newid()) FOR [guid]
ALTER TABLE [dbo].[info_plantaskperiod] WITH CHECK
    ADD CONSTRAINT [FK_info_plantaskperiod_plandetail_id] FOREIGN KEY ([plandetail_id]) REFERENCES [dbo].[info_plandetail] ([id])
end
GO
----------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------
-- https://teamsoft.atlassian.net/browse/FARMAK-357
----------------------------------------------------------------------------------------------------
exec insertLanguageInterface 'en', 'Task has been closed on another day', N'Task has been closed on another day'
exec insertLanguageInterface 'ru', 'Task has been closed on another day', N'Визит закрыт в другой день'
exec insertLanguageInterface 'uk', 'Task has been closed on another day', N'Візит виконано в інший день'
GO
----------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------
-- https://teamsoft.atlassian.net/browse/FARMAK-292
----------------------------------------------------------------------------------------------------
exec insertLanguageInterface 'en', 'Preparations fields', N'Preparations fields', 'crm'
exec insertLanguageInterface 'ru', 'Preparations fields', N'Поля препаратов для контроля', 'crm'
exec insertLanguageInterface 'uk', 'Preparations fields', N'Поля препаратів для контролю', 'crm'
GO

exec insertLanguageInterface 'en', 'Select period', N'Select period', 'crm'
exec insertLanguageInterface 'ru', 'Select period', N'Выберите период', 'crm'
exec insertLanguageInterface 'uk', 'Select period', N'Оберіть період', 'crm'
GO

exec insertLanguageInterface 'en', 'There is another plan task period from {{ dt1 }} to {{ dt2 }}', N'There is another plan task period from {{ dt1 }} to {{ dt2 }}', 'messages'
exec insertLanguageInterface 'ru', 'There is another plan task period from {{ dt1 }} to {{ dt2 }}', N'Период с {{ dt1 }} по {{ dt2 }} уже существует', 'messages'
exec insertLanguageInterface 'uk', 'There is another plan task period from {{ dt1 }} to {{ dt2 }}', N'Період з {{ dt1 }} до {{ dt2 }} уже існує', 'messages'
GO

exec insertLanguageInterface 'en', 'There is another plan dt from {{ dt1 }} to {{ dt2 }}', N'There is another plan dt from {{ dt1 }} to {{ dt2 }}', 'messages'
exec insertLanguageInterface 'ru', 'There is another plan dt from {{ dt1 }} to {{ dt2 }}', N'Период с {{ dt1 }} по {{ dt2 }} уже существует', 'messages'
exec insertLanguageInterface 'uk', 'There is another plan dt from {{ dt1 }} to {{ dt2 }}', N'Період з {{ dt1 }} до {{ dt2 }} уже існує', 'messages'
GO

IF OBJECT_ID(N'[info_plandt]', 'U') IS NULL
begin
CREATE TABLE [dbo].[info_plandt]
(
    [id]             [int] IDENTITY (1,1) NOT NULL,
    [plan_id]        [int]                NULL,
    [dt1]            [datetime]           NULL,
    [dt2]            [datetime]           NULL,
    [currenttime]    [datetime]           NULL,
    [time]           [timestamp]          NULL,
    [moduser]        [varchar](16)        NULL,
    [guid]           [uniqueidentifier]   NULL,
    CONSTRAINT [PK_info_plandt] PRIMARY KEY CLUSTERED
(
[id] ASC
) WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
    ) ON [PRIMARY]

ALTER TABLE [dbo].[info_plandt] ADD CONSTRAINT [DF_info_plandtcurrenttime] DEFAULT (getdate()) FOR [currenttime]
ALTER TABLE [dbo].[info_plandt] ADD CONSTRAINT [DF_info_plandtmoduser] DEFAULT ([dbo].[Get_CurrentCode]()) FOR [moduser]
ALTER TABLE [dbo].[info_plandt] ADD CONSTRAINT [DF_info_plandtguid] DEFAULT (newid()) FOR [guid]
ALTER TABLE [dbo].[info_plandt] WITH CHECK
    ADD CONSTRAINT [FK_info_plandt_plan_id] FOREIGN KEY ([plan_id]) REFERENCES [dbo].[info_plan] ([id])
end
GO

IF NOT EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE table_name = 'info_plancompanypreparation2set2' AND column_name = 'plandt_id')
    BEGIN
        ALTER TABLE [dbo].[info_plancompanypreparation2set2] ADD plandt_id int NULL
        ALTER TABLE [dbo].[info_plancompanypreparation2set2] WITH CHECK
            ADD CONSTRAINT [FK_info_plancompanypreparation2set2_plandt_id] FOREIGN KEY ([plandt_id]) REFERENCES [dbo].[info_plandt] ([id])
    END
GO
    ----------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------
-- https://teamsoft.atlassian.net/browse/FARMAK-280
----------------------------------------------------------------------------------------------------
    exec insertLanguageInterface 'en', 'Periods', N'Periods', 'crm'
    exec insertLanguageInterface 'ru', 'Periods', N'Периоды', 'crm'
    exec insertLanguageInterface 'uk', 'Periods', N'Періоди', 'crm'
    GO

    exec insertLanguageInterface 'en', 'Task number', N'Task number', 'crm'
    exec insertLanguageInterface 'ru', 'Task number', N'Номер визита', 'crm'
    exec insertLanguageInterface 'uk', 'Task number', N'Номер візиту', 'crm'
    GO

    exec insertLanguageInterface 'en', 'Edit period', N'Edit period', 'crm'
    exec insertLanguageInterface 'ru', 'Edit period', N'Редактировать период', 'crm'
    exec insertLanguageInterface 'uk', 'Edit period', N'Редагувати період', 'crm'
    GO

    exec insertLanguageInterface 'en', 'New period', N'New period', 'crm'
    exec insertLanguageInterface 'ru', 'New period', N'Новый период', 'crm'
    exec insertLanguageInterface 'uk', 'New period', N'Новий період', 'crm'
    GO
    ----------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------

----------------------------------------------------------------------------------------------------
-- https://teamsoft.atlassian.net/browse/NAN-47
----------------------------------------------------------------------------------------------------
    IF OBJECT_ID(N'info_contactpassportdata', 'U') is NULL
begin
create table info_contactpassportdata(
    [id] int identity(1,1)  not null
    , [contact_id] int  null
    , [academicrank_id] int  null
    , [academicdegree_id] int  null
    , [stateemployee_id] int  null
    , [preparation_id] int  null
    , [appointmentexperience_id] int  null
    , [patientamount] int
    , [lecturingexperience_id] int  null
    , [webinarexperience_id] int  null
    , [articleexperience_id] int  null
    , [articletitles] varchar (255) null
    , [communitymember_id] int  null
    , [communityname] varchar (255) null
    , [hobby] varchar (255) null
    , [territory] varchar (255) null
    , [advisedbycolleagues] varchar (255) null
    , [recomendation_id] int  null
    , [currenttime] datetime  null CONSTRAINT [DF_info_contactpassportdatacurrenttime] DEFAULT (getdate())
    , [time] timestamp  null
    , [moduser] varchar (16) null CONSTRAINT [DF_info_contactpassportdatamoduser] DEFAULT ([dbo].[Get_CurrentCode]())
    , [guid] uniqueidentifier  null CONSTRAINT [DF_info_contactpassportdataguid] DEFAULT (newid())
    , CONSTRAINT [PK_info_contactpassportdata] PRIMARY KEY (id) ON [PRIMARY]
    , CONSTRAINT [FK_info_contactpassportdata_contact_id] FOREIGN KEY ([contact_id]) REFERENCES info_contact ([id]) ON UPDATE CASCADE ON DELETE CASCADE
    , CONSTRAINT [FK_info_contactpassportdata_academicrank_id] FOREIGN KEY ([academicrank_id]) REFERENCES info_customdictionaryvalue ([id])
    , CONSTRAINT [FK_info_contactpassportdata_academicdegree_id] FOREIGN KEY ([academicdegree_id]) REFERENCES info_customdictionaryvalue ([id])
    , CONSTRAINT [FK_info_contactpassportdata_stateemployee_id] FOREIGN KEY ([stateemployee_id]) REFERENCES info_customdictionaryvalue ([id])
    , CONSTRAINT [FK_info_contactpassportdata_preparation_id] FOREIGN KEY ([preparation_id]) REFERENCES info_preparation ([id])
    , CONSTRAINT [FK_info_contactpassportdata_appointmentexperience_id] FOREIGN KEY ([appointmentexperience_id]) REFERENCES info_customdictionaryvalue ([id])
    , CONSTRAINT [FK_info_contactpassportdata_lecturingexperience_id] FOREIGN KEY ([lecturingexperience_id]) REFERENCES info_customdictionaryvalue ([id])
    , CONSTRAINT [FK_info_contactpassportdata_webinarexperience_id] FOREIGN KEY ([webinarexperience_id]) REFERENCES info_customdictionaryvalue ([id])
    , CONSTRAINT [FK_info_contactpassportdata_articleexperience_id] FOREIGN KEY ([articleexperience_id]) REFERENCES info_customdictionaryvalue ([id])
    , CONSTRAINT [FK_info_contactpassportdata_communitymember_id] FOREIGN KEY ([communitymember_id]) REFERENCES info_customdictionaryvalue ([id])
    , CONSTRAINT [FK_info_contactpassportdata_recomendation_id] FOREIGN KEY ([recomendation_id]) REFERENCES info_customdictionaryvalue ([id])
    )
end
GO
if not exists (select * from dbo.sysobjects where id = object_id('del_info_contactpassportdata') and OBJECTPROPERTY(id, 'IsTrigger') = 1)
exec('create trigger del_info_contactpassportdata on info_contactpassportdata for delete as if object_id(''empty.dbo.f_rep_del'') is null
insert into rep_del(tablename, del_guid) select ''info_contactpassportdata'', guid from deleted')
GO

if not exists (select * from dbo.sysobjects where id = object_id('moduser_info_contactpassportdata') and OBJECTPROPERTY(id, 'IsTrigger') = 1)
exec('create trigger moduser_info_contactpassportdata on info_contactpassportdata for insert, update as if not update(moduser)
update info_contactpassportdata set moduser = dbo.Get_CurrentCode(), currenttime = getdate() where id in (select id from inserted)')
GO



IF OBJECT_ID(N'info_contactpassportdatavalue', 'U') is NULL
begin
create table info_contactpassportdatavalue(
    [id] int identity(1,1)  not null
    , [passportdata_id] int  null
    , [fieldname] varchar (255) null
    , [value_id] int  null
    , [currenttime] datetime  null CONSTRAINT [DF_info_contactpassportdatavaluecurrenttime] DEFAULT (getdate())
    , [time] timestamp  null
    , [moduser] varchar (16) null CONSTRAINT [DF_info_contactpassportdatavaluemoduser] DEFAULT ([dbo].[Get_CurrentCode]())
    , [guid] uniqueidentifier  null CONSTRAINT [DF_info_contactpassportdatavalueguid] DEFAULT (newid())
    , CONSTRAINT [PK_info_contactpassportdatavalue] PRIMARY KEY (id) ON [PRIMARY]
    , CONSTRAINT [FK_info_contactpassportdatavalue_passportdata_id] FOREIGN KEY ([passportdata_id]) REFERENCES info_contactpassportdata ([id]) ON UPDATE CASCADE ON DELETE CASCADE
    , CONSTRAINT [FK_info_contactpassportdatavalue_value_id] FOREIGN KEY ([value_id]) REFERENCES info_customdictionaryvalue ([id])
    )
end
else
begin
    if not exists(SELECT * FROM syscolumns sc JOIN sysobjects so ON sc.id = so.id join systypes st on st.xtype = sc.xtype WHERE so.Name = 'info_contactpassportdatavalue' and sc.name = 'id' and st.name = 'int')
	exec('''Поле [id] отсутствует или не совпадает тип поля''')
	if not exists(SELECT * FROM syscolumns sc JOIN sysobjects so ON sc.id = so.id join systypes st on st.xtype = sc.xtype WHERE so.Name = 'info_contactpassportdatavalue' and sc.name = 'passportdata_id' and st.name = 'int')
	exec('''Поле [passportdata_id] отсутствует или не совпадает тип поля''')
	if not exists(SELECT * FROM syscolumns sc JOIN sysobjects so ON sc.id = so.id join systypes st on st.xtype = sc.xtype WHERE so.Name = 'info_contactpassportdatavalue' and sc.name = 'fieldname' and st.name = 'varchar')
	exec('''Поле [fieldname] отсутствует или не совпадает тип поля''')
	if not exists(SELECT * FROM syscolumns sc JOIN sysobjects so ON sc.id = so.id join systypes st on st.xtype = sc.xtype WHERE so.Name = 'info_contactpassportdatavalue' and sc.name = 'value_id' and st.name = 'int')
	exec('''Поле [value_id] отсутствует или не совпадает тип поля''')
	if not exists(SELECT * FROM syscolumns sc JOIN sysobjects so ON sc.id = so.id join systypes st on st.xtype = sc.xtype WHERE so.Name = 'info_contactpassportdatavalue' and sc.name = 'currenttime' and st.name = 'datetime')
	exec('''Поле [currenttime] отсутствует или не совпадает тип поля''')
	if not exists(SELECT * FROM syscolumns sc JOIN sysobjects so ON sc.id = so.id join systypes st on st.xtype = sc.xtype WHERE so.Name = 'info_contactpassportdatavalue' and sc.name = 'time' and st.name = 'timestamp')
	exec('''Поле [time] отсутствует или не совпадает тип поля''')
	if not exists(SELECT * FROM syscolumns sc JOIN sysobjects so ON sc.id = so.id join systypes st on st.xtype = sc.xtype WHERE so.Name = 'info_contactpassportdatavalue' and sc.name = 'moduser' and st.name = 'varchar')
	exec('''Поле [moduser] отсутствует или не совпадает тип поля''')
	if not exists(SELECT * FROM syscolumns sc JOIN sysobjects so ON sc.id = so.id join systypes st on st.xtype = sc.xtype WHERE so.Name = 'info_contactpassportdatavalue' and sc.name = 'guid' and st.name = 'uniqueidentifier')
	exec('''Поле [guid] отсутствует или не совпадает тип поля''')
end
GO

if not exists (select * from dbo.sysobjects where id = object_id('del_info_contactpassportdatavalue') and OBJECTPROPERTY(id, 'IsTrigger') = 1)
exec('create trigger del_info_contactpassportdatavalue on info_contactpassportdatavalue for delete as if object_id(''empty.dbo.f_rep_del'') is null
insert into rep_del(tablename, del_guid) select ''info_contactpassportdatavalue'', guid from deleted')
GO

if not exists (select * from dbo.sysobjects where id = object_id('moduser_info_contactpassportdatavalue') and OBJECTPROPERTY(id, 'IsTrigger') = 1)
exec('create trigger moduser_info_contactpassportdatavalue on info_contactpassportdatavalue for insert, update as if not update(moduser)
update info_contactpassportdatavalue set moduser = dbo.Get_CurrentCode(), currenttime = getdate() where id in (select id from inserted)')
GO


IF NOT EXISTS (SELECT 1 FROM info_tabcontrol where type = 42 and parrenttype = 10)
    insert into info_tabcontrol (parrenttype,type,name,isshow) values (10, 42, 'Паспортная часть', 0);


exec insertLanguageInterface 'en', 'Academic rank', 'Academic rank', 'crm'
exec insertLanguageInterface 'ru', 'Academic rank', 'Звание', 'crm'
exec insertLanguageInterface 'uk', 'Academic rank', 'Звання', 'crm'

exec insertLanguageInterface 'en', 'Academic degree', 'Academic degree', 'crm'
exec insertLanguageInterface 'ru', 'Academic degree', 'Ученая степень', 'crm'
exec insertLanguageInterface 'uk', 'Academic degree', 'Наукова ступінь', 'crm'

exec insertLanguageInterface 'en', 'Is a public servant', 'Is a public servant', 'crm'
exec insertLanguageInterface 'ru', 'Is a public servant', 'Является ли гос.служащим', 'crm'
exec insertLanguageInterface 'uk', 'Is a public servant', 'Чи є держслужбовцем', 'crm'

exec insertLanguageInterface 'en', 'Lecturing experience', 'Lecturing experience', 'crm'
exec insertLanguageInterface 'ru', 'Lecturing experience', 'Опыт чтения лекций', 'crm'
exec insertLanguageInterface 'uk', 'Lecturing experience', 'Досвід читання лекцій', 'crm'

exec insertLanguageInterface 'en', 'Experience in hosting webinars', 'Experience in hosting webinars', 'crm'
exec insertLanguageInterface 'ru', 'Experience in hosting webinars', 'Опыт проведения вебинаров', 'crm'
exec insertLanguageInterface 'uk', 'Experience in hosting webinars', 'Досвід проведення вебінарів', 'crm'

exec insertLanguageInterface 'en', 'Experience in writing articles', 'Experience in writing articles', 'crm'
exec insertLanguageInterface 'ru', 'Experience in writing articles', 'Опыт написание статей', 'crm'
exec insertLanguageInterface 'uk', 'Experience in writing articles', 'Досвід написання статей', 'crm'

exec insertLanguageInterface 'en', 'Article titles', 'Article titles', 'crm'
exec insertLanguageInterface 'ru', 'Article titles', 'Название статей', 'crm'
exec insertLanguageInterface 'uk', 'Article titles', 'Назва статей', 'crm'

exec insertLanguageInterface 'en', 'Is a member of scientific communities and expert groups', 'Is a member of scientific communities and expert groups', 'crm'
exec insertLanguageInterface 'ru', 'Is a member of scientific communities and expert groups', 'Является ли членом научных сообществ и экспертных групп', 'crm'
exec insertLanguageInterface 'uk', 'Is a member of scientific communities and expert groups', 'Чи є членом наукових співтовариств і експертних груп', 'crm'

exec insertLanguageInterface 'en', 'Community / group name', 'Community / group name', 'crm'
exec insertLanguageInterface 'ru', 'Community / group name', 'Название сообщества / группы', 'crm'
exec insertLanguageInterface 'uk', 'Community / group name', 'Назва спільноти / групи', 'crm'

exec insertLanguageInterface 'en', 'Preparation', 'Preparation', 'crm'
exec insertLanguageInterface 'ru', 'Preparation', 'Препарат', 'crm'
exec insertLanguageInterface 'uk', 'Preparation', 'Препарат', 'crm'

exec insertLanguageInterface 'en', 'Appointment experience', 'Appointment experience', 'crm'
exec insertLanguageInterface 'ru', 'Appointment experience', 'Опыт назначения', 'crm'
exec insertLanguageInterface 'uk', 'Appointment experience', 'Досвід призначення', 'crm'

exec insertLanguageInterface 'en', 'Patient amount', 'Patient amount', 'crm'
exec insertLanguageInterface 'ru', 'Patient amount', 'Количество пациентов', 'crm'
exec insertLanguageInterface 'uk', 'Patient amount', 'Кількість пацієнтів', 'crm'

exec insertLanguageInterface 'en', 'Affects the application', 'Affects the application', 'crm'
exec insertLanguageInterface 'ru', 'Affects the application', 'Влияет на заявку ВЗН, рекомендации', 'crm'
exec insertLanguageInterface 'uk', 'Affects the application', 'Впливає на заявку ВЗН, рекомендації', 'crm'

exec insertLanguageInterface 'en', 'Territory of influence', 'Territory of influence', 'crm'
exec insertLanguageInterface 'ru', 'Territory of influence', 'Территория влияния', 'crm'
exec insertLanguageInterface 'uk', 'Territory of influence', 'Територія впливу', 'crm'

exec insertLanguageInterface 'en', 'Interests scope', 'Interests scope', 'crm'
exec insertLanguageInterface 'ru', 'Interests scope', 'Сфера интересов', 'crm'
exec insertLanguageInterface 'uk', 'Interests scope', 'Сфера інтересів', 'crm'

exec insertLanguageInterface 'en', 'Language scope', 'Language scope', 'crm'
exec insertLanguageInterface 'ru', 'Language scope', 'Владение иностранным языком', 'crm'
exec insertLanguageInterface 'uk', 'Language scope', 'Володіння іноземною мовою', 'crm'

exec insertLanguageInterface 'en', 'To which colleagues listens to', 'To which colleagues listens to', 'crm'
exec insertLanguageInterface 'ru', 'To which colleagues listens to', 'На кого из KOL (коллег) ориентируется, прислушивается', 'crm'
exec insertLanguageInterface 'uk', 'To which colleagues listens to', 'На кого з KOL (колег) орієнтується, прислухається', 'crm'

exec insertLanguageInterface 'en', 'Hobby', 'Hobby', 'crm'
exec insertLanguageInterface 'ru', 'Hobby', 'Хобби', 'crm'
exec insertLanguageInterface 'uk', 'Hobby', 'Хоббі', 'crm'

exec insertLanguageInterface 'en', 'Professional data', 'Professional data', 'crm'
exec insertLanguageInterface 'ru', 'Professional data', 'Профессиональные данные', 'crm'
exec insertLanguageInterface 'uk', 'Professional data', 'Професійні дані', 'crm'

exec insertLanguageInterface 'en', 'Scientific activity', 'Scientific activity', 'crm'
exec insertLanguageInterface 'ru', 'Scientific activity', 'Научная деятельность', 'crm'
exec insertLanguageInterface 'uk', 'Scientific activity', 'Наукова діяльність', 'crm'

exec insertLanguageInterface 'en', 'Appointment experience and sphere of influence', 'Appointment experience and sphere of influence', 'crm'
exec insertLanguageInterface 'ru', 'Appointment experience and sphere of influence', 'Опыт назначения и сфера влияния', 'crm'
exec insertLanguageInterface 'uk', 'Appointment experience and sphere of influence', 'Досвід призначення та сфера впливу', 'crm'

exec insertLanguageInterface 'en', 'Sphere of interests, tools of development', 'Sphere of interests, tools of development', 'crm'
exec insertLanguageInterface 'ru', 'Sphere of interests, tools of development', 'Сфера интересов, инструменты развития', 'crm'
exec insertLanguageInterface 'uk', 'Sphere of interests, tools of development', 'Сфера інтересів, інструменти розвитку', 'crm'
----------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------

----------------------------------------------------------------------------------------------------
-- https://teamsoft.atlassian.net/browse/EGISRU-369
----------------------------------------------------------------------------------------------------
IF NOT EXISTS(SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS
              WHERE table_name = 'info_mcmdistribution' AND column_name = 'is_link')
    ALTER TABLE info_mcmdistribution ADD is_link int NULL
GO
----------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------
