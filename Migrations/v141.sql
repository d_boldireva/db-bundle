------------------------------------------------------------------------------------------------------------------------
-- https://teamsoft.atlassian.net/browse/PHARMAWEB-691
------------------------------------------------------------------------------------------------------------------------
IF NOT EXISTS(SELECT 1 FROM information_schema.columns WHERE table_name = 'info_country' and column_name = 'gps_la')
BEGIN
    ALTER TABLE info_country ADD gps_la FLOAT;
END;
GO

IF NOT EXISTS(SELECT 1 FROM information_schema.columns WHERE table_name = 'info_country' and column_name = 'gps_lo')
BEGIN
    ALTER TABLE info_country ADD gps_lo FLOAT;
END;
GO

IF NOT EXISTS(SELECT 1 FROM information_schema.columns WHERE table_name = 'info_country' and column_name = 'zoom_level')
BEGIN
    ALTER TABLE info_country ADD zoom_level INT;
END;
GO

IF NOT EXISTS(SELECT 1 FROM information_schema.columns WHERE table_name = 'info_country' and column_name = 'min_zoom')
BEGIN
    ALTER TABLE info_country ADD min_zoom INT;
END;
GO

IF NOT EXISTS(SELECT 1 FROM information_schema.columns WHERE table_name = 'info_country' and column_name = 'max_zoom')
BEGIN
    ALTER TABLE info_country ADD max_zoom INT;
END;
GO
------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------
-- https://teamsoft.atlassian.net/browse/DRREDDYSKZ-244
------------------------------------------------------------------------------------------------------------------------
exec insertPoGridconfig 100, 'removalReason2', 'Removal reason', 1, 1;
exec insertPoGridconfig 100, 'removalReason2', 'Removal reason', 1, 2;
GO
------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------
-- https://teamsoft.atlassian.net/browse/MCM-507
------------------------------------------------------------------------------------------------------------------------
IF NOT EXISTS(SELECT 1 FROM info_options WHERE name = 'RTCCallAppointedTextUser')
BEGIN
INSERT INTO info_options (name, value) VALUES ('RTCCallAppointedTextUser', 'RTC_CALL_APPOINTED_TEXT_USER')
END;
GO

IF NOT EXISTS(SELECT 1 FROM info_options WHERE name = 'RTCCallAppointedTextContact')
BEGIN
INSERT INTO info_options (name, value) VALUES ('RTCCallAppointedTextContact', 'RTC_CALL_APPOINTED_TEXT_CONTACT')
END;
GO

exec insertLanguageInterface 'en', 'RTC_CALL_APPOINTED_TEXT_USER', N'Video call with {{name}} appointed on {{task_datefrom}}', 'crm'
exec insertLanguageInterface 'ru', 'RTC_CALL_APPOINTED_TEXT_USER', N'Видеозвонок с {{name}} назначен на {{task_datefrom}}', 'crm'
exec insertLanguageInterface 'uk', 'RTC_CALL_APPOINTED_TEXT_USER', N'Відеовиклик з {{name}} призначено на {{task_datefrom}}', 'crm'
GO

exec insertLanguageInterface 'en', 'RTC_CALL_APPOINTED_TEXT_CONTACT', N'Video call with medical specialist {{name}} appointed on {{task_datefrom}}', 'crm'
exec insertLanguageInterface 'ru', 'RTC_CALL_APPOINTED_TEXT_CONTACT', N'Видеозвонок с медицинским специалистом {{name}} назначен на {{task_datefrom}}', 'crm'
exec insertLanguageInterface 'uk', 'RTC_CALL_APPOINTED_TEXT_CONTACT', N'Відеовиклик з медичним фахівцем {{name}} призначено на {{task_datefrom}}', 'crm'
GO

exec deleteLanguageInterface 'RTC_CALL_ENDED_TEXT', 'crm';
exec insertLanguageInterface 'en', 'RTC_CALL_ENDED_TEXT', N'Video call appointment expired on {{task_datetill}}', 'crm';
exec insertLanguageInterface 'ru', 'RTC_CALL_ENDED_TEXT', N'Срок видеозвонка вышел {{task_datetill}}', 'crm';
exec insertLanguageInterface 'uk', 'RTC_CALL_ENDED_TEXT', N'Термін дії відеовиклику вийшов {{task_datetill}}', 'crm';

GO
------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------
---https://teamsoft.atlassian.net/browse/GEODATA-823
---https://teamsoft.atlassian.net/browse/GEODATA-837
------------------------------------------------------------------------------------------------------------------------
IF NOT EXISTS(SELECT 1 FROM info_options WHERE name = 'geomarketingEnableFTE')
BEGIN
INSERT INTO info_options (name, value) VALUES ('geomarketingEnableFTE', '0')
END
GO

IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'info_polygon' AND COLUMN_NAME = 'population')
BEGIN
ALTER TABLE info_polygon ADD population INT NULL
END
GO

IF NOT EXISTS (SELECT 1 FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'info_polygon' AND COLUMN_NAME = 'morionid')
BEGIN
ALTER TABLE info_polygon ADD morionid INT NULL
END
GO

exec insertLanguageInterface 'en', 'FTE calculator', N'FTE calculator', 'geomarketing'
exec insertLanguageInterface 'ru', 'FTE calculator', N'Калькулятор целовой ставки', 'geomarketing'
exec insertLanguageInterface 'uk', 'FTE calculator', N'Калькулятор цільової ставки', 'geomarketing'

exec insertLanguageInterface 'en', 'Days per month', N'Days per month', 'geomarketing'
exec insertLanguageInterface 'ru', 'Days per month', N'Дней в месяц', 'geomarketing'
exec insertLanguageInterface 'uk', 'Days per month', N'Днів на місяць', 'geomarketing'

exec insertLanguageInterface 'en', 'Visits per day', N'Visits per day', 'geomarketing'
exec insertLanguageInterface 'ru', 'Visits per day', N'Посещений в день', 'geomarketing'
exec insertLanguageInterface 'uk', 'Visits per day', N'Відвідувань на день', 'geomarketing'

exec insertLanguageInterface 'en', 'Coverage scale', N'Coverage scale', 'geomarketing'
exec insertLanguageInterface 'ru', 'Coverage scale', N'Масштаб покрытия', 'geomarketing'
exec insertLanguageInterface 'uk', 'Coverage scale', N'Маштаб покриття', 'geomarketing'

exec insertLanguageInterface 'en', 'HCP', N'HCP', 'geomarketing'
exec insertLanguageInterface 'ru', 'HCP', N'Мед. работник', 'geomarketing'
exec insertLanguageInterface 'uk', 'HCP', N'Мед. працівник', 'geomarketing'
GO

exec insertLanguageInterface 'en', 'Visits per day to doctors', N'Visits per day to doctors', 'geomarketing'
exec insertLanguageInterface 'ru', 'Visits per day to doctors', N'Посещений врачей в день', 'geomarketing'
exec insertLanguageInterface 'uk', 'Visits per day to doctors', N'Відвідувань лікарів на день', 'geomarketing'

exec insertLanguageInterface 'en', 'Visits per day to pharmacy', N'Visits per day to pharmacy', 'geomarketing'
exec insertLanguageInterface 'ru', 'Visits per day to pharmacy', N'Посещений аптек в день', 'geomarketing'
exec insertLanguageInterface 'uk', 'Visits per day to pharmacy', N'Відвідувань аптек на день', 'geomarketing'

exec insertLanguageInterface 'en', 'more', N'more', 'geomarketing'
exec insertLanguageInterface 'ru', 'more', N'более', 'geomarketing'
exec insertLanguageInterface 'uk', 'more', N'більше', 'geomarketing'
GO
------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------
-- https://teamsoft.atlassian.net/browse/DRREDDYSKZ-213
------------------------------------------------------------------------------------------------------------------------
exec insertLanguageInterface 'en', 'Summary table', N'Summary table', 'messages';
exec insertLanguageInterface 'ru', 'Summary table', N'Суммарная таблица', 'messages';
exec insertLanguageInterface 'uk', 'Summary table', N'Сумарна таблиця', 'messages';
GO;

exec insertLanguageInterface 'en', 'Work table', N'Work table', 'messages';
exec insertLanguageInterface 'ru', 'Work table', N'Рабочая таблица', 'messages';
exec insertLanguageInterface 'uk', 'Work table', N'Робоча таблиця', 'messages';
GO;

exec insertLanguageInterface 'en', 'Granual plan summary table', N'Granual plan summary table', 'messages';
exec insertLanguageInterface 'ru', 'Granual plan summary table', N'Грануальный план суммарная таблица', 'messages';
exec insertLanguageInterface 'uk', 'Granual plan summary table', N'Грануальний план сумарна таблиця', 'messages';
GO;

exec insertLanguageInterface 'en', 'Granual plan work table', N'Granual plan work table', 'messages';
exec insertLanguageInterface 'ru', 'Granual plan work table', N'Грануальный план рабочая таблица', 'messages';
exec insertLanguageInterface 'uk', 'Granual plan work table', N'Грануальний план робоча таблиця', 'messages';
GO;

exec insertLanguageInterface 'en', 'Autoplan (Cur.month)', N'Autoplan (Cur.month)', 'messages';
exec insertLanguageInterface 'ru', 'Autoplan (Cur.month)', N'Автоплан (Текущий месяц)', 'messages';
exec insertLanguageInterface 'uk', 'Autoplan (Cur.month)', N'Автоплан (Поточний місяць)', 'messages';
GO;

exec insertLanguageInterface 'en', 'Granplan (Cur.month)', N'Granplan (Cur.month)', 'messages';
exec insertLanguageInterface 'ru', 'Granplan (Cur.month)', N'Гран-план (Текущий месяц)', 'messages';
exec insertLanguageInterface 'uk', 'Granplan (Cur.month)', N'Гран-план (Поточний місяць)', 'messages';
GO;

exec insertLanguageInterface 'en', 'Granplan per.', N'Granplan %', 'messages';
exec insertLanguageInterface 'ru', 'Granplan per.', N'Гран-план %', 'messages';
exec insertLanguageInterface 'uk', 'Granplan per.', N'Гран-план %', 'messages';
GO;

exec insertLanguageInterface 'en', 'PC-Purchasing', N'PC-Purchasing', 'messages';
exec insertLanguageInterface 'ru', 'PC-Purchasing', N'АС-Закупки', 'messages';
exec insertLanguageInterface 'uk', 'PC-Purchasing', N'АМ-Закупівлі', 'messages';
GO;

exec insertLanguageInterface 'en', 'PC-Remains', N'PC-Remains', 'messages';
exec insertLanguageInterface 'ru', 'PC-Remains', N'АС-Остатки', 'messages';
exec insertLanguageInterface 'uk', 'PC-Remains', N'АМ-Залишки', 'messages';
GO;

exec insertLanguageInterface 'en', 'PC-Sales', N'PC-Sales', 'messages';
exec insertLanguageInterface 'ru', 'PC-Sales', N'АС-Продажи', 'messages';
exec insertLanguageInterface 'uk', 'PC-Sales', N'АМ-Продажі', 'messages';
GO;

exec insertLanguageInterface 'en', 'Before', N'Before', 'messages';
exec insertLanguageInterface 'ru', 'Before', N'До', 'messages';
exec insertLanguageInterface 'uk', 'Before', N'До', 'messages';
GO;

exec insertLanguageInterface 'en', 'Morion ID', N'Morion ID', 'messages';
exec insertLanguageInterface 'ru', 'Morion ID', N'Morion ID', 'messages';
exec insertLanguageInterface 'uk', 'Morion ID', N'Morion ID', 'messages';
GO;

exec insertPoGridconfig 109, 'preparation', 'Preparation';
exec insertPoGridconfig 109, 'last_year', 'Fact';
exec insertPoGridconfig 109, 'month_4', 'Fact';
exec insertPoGridconfig 109, 'month_3', 'Fact';
exec insertPoGridconfig 109, 'month_2', 'Fact';
exec insertPoGridconfig 109, 'month_1', 'Fact';
exec insertPoGridconfig 109, 'sum_plan_preparation', 'Plan total';
exec insertPoGridconfig 109, 'autoplan', 'Autoplan (Cur.month)';
exec insertPoGridconfig 109, 'granplan', 'Granplan (Cur.month)';
exec insertPoGridconfig 109, 'granplan_p', 'Granplan per.';
exec insertPoGridconfig 109, 'purchase', 'AS-Purchasing';
exec insertPoGridconfig 109, 'remain', 'AC-Remains';
exec insertPoGridconfig 109, 'sale', 'AS-Sales';
exec insertPoGridconfig 109, 'before', 'Before';
GO;

exec insertPoGridconfig 110, 'pharmacy', 'Pharmacy';
exec insertPoGridconfig 110, 'morionid', 'Morion ID';
exec insertPoGridconfig 110, 'category', 'Category';
exec insertPoGridconfig 110, 'turnover', 'Potential';
exec insertPoGridconfig 110, 'last_year', 'Fact';
exec insertPoGridconfig 110, 'month_4', 'Fact';
exec insertPoGridconfig 110, 'month_3', 'Fact';
exec insertPoGridconfig 110, 'month_2', 'Fact';
exec insertPoGridconfig 110, 'month_1', 'Fact';
exec insertPoGridconfig 110, 'autoplan', 'Autoplan (Cur.month)';
exec insertPoGridconfig 110, 'granplan', 'Granplan (Cur.month)';
exec insertPoGridconfig 110, 'granplan_money', 'Granplan (Cur.month)';
exec insertPoGridconfig 110, 'purchase', 'AS-Purchasing';
exec insertPoGridconfig 110, 'remain', 'AC-Remains';
exec insertPoGridconfig 110, 'sale', 'AS-Sales';
exec insertPoGridconfig 110, 'before', 'Before';
GO;
------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------
------- https://teamsoft.atlassian.net/browse/MCM-538
------------------------------------------------------------------------------------------------------------------------
exec insertLanguageInterface 'en', 'Import template', N'Import template', 'mcm'
exec insertLanguageInterface 'ru', 'Import template', N'Импортировать шаблон', 'mcm'
exec insertLanguageInterface 'uk', 'Import template', N'Імпортувати шаблон', 'mcm'
GO
------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------
------- https://teamsoft.atlassian.net/browse/PHARMAWEB-748
------------------------------------------------------------------------------------------------------------------------
exec insertLanguageInterface 'en', 'Parent group', N'Parent group', 'messages';
exec insertLanguageInterface 'ru', 'Parent group', N'Родительская группа', 'messages';
exec insertLanguageInterface 'uk', 'Parent group', N'Батьківська група', 'messages';
GO
------------------------------------------------------------------------------------------------------------------------
------------------------------------------------------------------------------------------------------------------------
