<?php

namespace TeamSoft\CrmRepositoryBundle\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Helper\QuestionHelper;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Question\ConfirmationQuestion;
use TeamSoft\CrmRepositoryBundle\Service\DatabasePlatform;
use Doctrine\DBAL\Connection;

class MigrateCommand extends Command
{

    public function __construct(Connection $connection, DatabasePlatform $dp)
    {
        parent::__construct();
        $this->connection = $connection;
        $this->dp = $dp;
    }

    protected function configure()
    {
        parent::configure();
        $this
            ->setName('ts:migrate')
            ->setDescription('Apply migration scripts')
            ->addArgument(
                'folder',
                InputArgument::OPTIONAL,
                'Folder where migrations are located',
                dirname(__DIR__) . DIRECTORY_SEPARATOR . 'Migrations'
            )
            ->addOption(
                'with-changed',
                null,
                InputOption::VALUE_NEGATABLE,
                'Changed migrations will be applied'
            )
            ->addOption(
                'from',
                null,
                InputOption::VALUE_OPTIONAL,
                'Version starting from what migrations will be reapplied'
            );
    }

    /**
     * @throws \Doctrine\DBAL\Exception
     */
    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        /**
         * @var QuestionHelper $questionHelper
         */
        $questionHelper = $this->getHelper('question');

        $this->createSchema();

        $folder = $input->getArgument('folder');
        $from = $input->getOption('from');
        $withChanged = $input->getOption('with-changed');

        $migrations = $this->getMigrations($folder);
        $databaseMigrations = $this->getDatabaseMigrations();

        $applicationVersion = array_key_last($migrations);
        $databaseVersion = array_key_first($databaseMigrations);

        $output->writeln('Database version: ' . $databaseVersion . ' <--> Application version: ' . $applicationVersion);

        if (is_numeric($from) && $from <= $databaseVersion) {
            $fromVersion = $from;
        } else {
            $fromVersion = $databaseVersion + 1;
        }

        $migrationsToApply = [];
        foreach ($migrations as $version => $migration) {
            $databaseMigration = $databaseMigrations[$version] ?? null;
            $newMigration = $version >= $fromVersion;
            $changedMigration = $withChanged && (
                !$databaseMigration || !$databaseMigration['hash_file'] || $databaseMigration['hash_file'] !== $migration['hash']
            );
            if ($newMigration || $changedMigration) {
                $migration['update'] = (bool)$databaseMigration;
                $migrationsToApply[$version] = $migration;
            }
        }

        if (count($migrationsToApply)) {
            $versionsString = implode(', ', array_keys($migrationsToApply));
            $question = new ConfirmationQuestion("Apply next versions: $versionsString?", true, '/^(y|j)/i');
            if ($questionHelper->ask($input, $output, $question)) {
                foreach ($migrationsToApply as $migrationVersion => $migrationData) {
                    if (
                        $this->applyMigration($output, $migrationVersion, $migrationData) === false
                    ) {
                        return 1;
                    }
                }
            }
        } else {
            $output->writeln('All up-to-date');
        }
        return 0;
    }

    /**
     * @return float
     * @throws \Doctrine\DBAL\Exception
     */
    private function getDatabaseMigrations()
    {
        $c = $this->dp->getConcatOperator();
        $sql = "
            SELECT major, COALESCE(minor, 0) minor, hash_file
            FROM rep_versionweb 
            WHERE major IS NOT null
            ORDER BY CAST(CAST(major AS VARCHAR) $c '.' $c CAST(COALESCE(minor, 0) AS VARCHAR) as FLOAT) DESC";
        $migrations = [];
        foreach ($this->connection->fetchAllAssociative($sql) as $migration) {
            ['major' => $major, 'minor' => $minor] = $migration;
            $migrations[$this->getVersion("$major.$minor")] = $migration;
        }
        return $migrations;
    }

    /**
     * @return void
     * @throws \Doctrine\DBAL\Exception
     */
    private function createSchema(): void
    {
        if ($this->dp->isSQLServer2008()) {
            $sql = "
                SET ANSI_NULLS ON                
                SET QUOTED_IDENTIFIER ON 
                IF OBJECT_ID('rep_versionweb') IS NULL
                BEGIN                 
                    CREATE TABLE [dbo].[rep_versionweb](
                        [id] [int] IDENTITY(1,1) NOT NULL,
                        [major] [int] NULL,
                        [minor] [int] NULL,
                        [hash_file] varchar(255) NULL,
                        [createdatetime] [datetime] NULL,
                     CONSTRAINT [PK_rep_versionweb] PRIMARY KEY CLUSTERED 
                    (
                        [id] ASC
                    )WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
                    ) ON [PRIMARY]                
                    ALTER TABLE [dbo].[rep_versionweb] ADD  DEFAULT (getdate()) FOR [createdatetime]
                END
                
                IF NOT EXISTS (SELECT 1 FROM information_schema.columns WHERE table_name = 'rep_versionweb' AND column_name = 'hash_file')
                BEGIN
                    ALTER TABLE rep_versionweb ADD hash_file varchar(255) NULL
                END
                IF NOT EXISTS (SELECT 1 FROM information_schema.columns WHERE table_name = 'rep_versionweb' AND column_name = 'updatedatetime')
                BEGIN
                    ALTER TABLE rep_versionweb ADD updatedatetime [datetime] NULL
                END
            ";
        } else {
            $sql = "
                CREATE TABLE IF NOT EXISTS rep_versionweb(
                    id serial NOT NULL
                    , major integer
                    , minor integer
                    , hash_file text
                    , createdatetime timestamp
                    , CONSTRAINT rep_versionweb_pkey PRIMARY KEY (id)
                );
                ALTER TABLE rep_versionweb ADD COLUMN IF NOT EXISTS hash_file text null;
                ALTER TABLE rep_versionweb ADD COLUMN IF NOT EXISTS updatedatetime timestamp null;
            ";
        }

        $this->connection->executeStatement($sql);
    }

    private function getMigrations($directory): array
    {
        $files = glob(rtrim($directory, DIRECTORY_SEPARATOR) . DIRECTORY_SEPARATOR . '*.sql');

        $migrations = [];
        if (count($files) > 0) {
            foreach ($files as $file) {
                $versionString = str_replace(['v', '.sql'], '', basename($file));
                $migrations[$this->getVersion($versionString)] = [
                    'hash' => hash_file('sha1', $file), 'path' => $file
                ];
            }
            ksort($files);
        }

        return $migrations;
    }

    private function getVersion(string $versionString)
    {
        $versionStringParts = explode('.', $versionString);
        $major = $versionStringParts[0];
        $minor = (int)($versionStringParts[1] ?? 0);
        return $major . ($minor > 0 ? '.' . $minor : '');
    }

    private function applyMigration(OutputInterface $output, $migrationVersion, $migrationData)
    {
        $output->writeln('---------------------------------------------------------------------');
        $applyingMigrationSection = $output->section();
        $succeededExecutions = 0;
        $failedExecutions = 0;
        $applyingMigrationSection->writeln("Applying migration (version $migrationVersion): succeeded $succeededExecutions; failed $failedExecutions");

        $content = file_get_contents($migrationData['path']);
        if ($this->dp->isSQLServer2008()) {
            $migrationScripts = $this->splitContent($content);
        } else {
            $migrationScripts = [$content];
        }

        foreach ($migrationScripts as $migrationScript) {
            try {
                $this->connection->executeStatement($migrationScript);
                $succeededExecutions++;
            } catch (\Exception $e) {
                $failedExecutions++;
                $output->writeln('<error>' . $e->getMessage() . '</error>');
                return false;
            } finally {
                $applyingMigrationSection->overwrite("Applying migration (version $migrationVersion): succeeded $succeededExecutions; failed $failedExecutions");
            }
        }

        $migrationVersionParts = explode('.', $migrationVersion);
        $major = (int)$migrationVersionParts[0];
        $minor = (int)($migrationVersionParts[1] ?? 0);

//        $datetimeFormat = $this->connection->getDatabasePlatform()->getDateTimeFormatString();
        $datetimeFormat = 'Y-m-d H:i:s';
        $datetimeString = (new \DateTime())->format($datetimeFormat);
        if ($migrationData['update'] === true) {
            $this->connection->update('rep_versionweb', [
                'updatedatetime' => $datetimeString, 'hash_file' => $migrationData['hash']
            ], [
                'major' => $major, 'minor' => $minor
            ]);
        } else {
            $this->connection->insert('rep_versionweb', [
                'createdatetime' => $datetimeString, 'major' => $major, 'minor' => $minor, 'hash_file' => $migrationData['hash']
            ]);
        }

        return true;
    }

    protected function splitContent(string $content)
    {
        $parts = [];
        $content = preg_replace("/(\/\*[\s\S]*\*\/)/", '', $content);
        //$sqlComments = '@(([\'"]).*?[^\\\]\2)|((?:\#|--).*?$|/\*(?:[^/*]|/(?!\*)|\*(?!/)|(?R))*\*\/)\s*|(?<=;)\s+@ms';
        //$content = trim(preg_replace($sqlComments, '$1', $content));
        foreach (preg_split('/(?:^|\s|;)go(?:$|\s|;)/i', $content) as $migrationScript) {
            if ($migrationScript = trim($migrationScript)) {
                $parts[] = $migrationScript;
            }
        }

        return $parts;
    }
}
