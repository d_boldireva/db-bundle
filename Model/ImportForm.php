<?php

namespace TeamSoft\CrmRepositoryBundle\Model;

use Doctrine\Common\Collections\ArrayCollection;

class ImportForm
{

    /**
     * @var \DateTime
     */
    private $saledate;

    /**
     * @var ArrayCollection
     */
    private $files;

    /**
     * UploadedFiles constructor.
     */
    public function __construct()
    {
        $this->files = new ArrayCollection();
    }

    /**
     * @param \DateTime $saledate
     */
    public function setSaledate($saledate)
    {
        $this->saledate = $saledate;
    }

    /**
     * @return mixed
     */
    public function getSaledate()
    {
        return $this->saledate;
    }

    /**
     * @param ImportFile $form
     * @return $this
     */
    public function addFile(ImportFile $form)
    {
        $this->files->add($form);

        return $this;
    }

    /**
     * @return ArrayCollection
     */
    public function getFiles()
    {
        return $this->files;
    }
}
