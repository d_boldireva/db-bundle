<?php


namespace TeamSoft\CrmRepositoryBundle\Model;


use TeamSoft\CrmRepositoryBundle\Entity\InfoAction;

interface ContainsActionInterface
{
    /**
     * @return InfoAction|null
     */
    public function getAction(): ?InfoAction;
}