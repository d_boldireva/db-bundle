<?php

namespace TeamSoft\CrmRepositoryBundle\Model;

use TeamSoft\CrmRepositoryBundle\Entity\InfoTask;

interface ContainsTaskInterface
{
    /**
     * @return InfoTask|null
     */
    public function getTask(): ?InfoTask;
}
