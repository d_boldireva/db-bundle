<?php

namespace TeamSoft\CrmRepositoryBundle\Model;

use TeamSoft\CrmRepositoryBundle\Entity\InfoDirection;

class ActiveDirectoryUser
{
    public $identifier;
    public $name;
    public $email;
    public $phone;
    public $city;
    public $direction;

    public function __construct($identifier, $name, $email, $phone, $city, ?InfoDirection $direction)
    {
        $this->name = $name;
        $this->identifier = $identifier;
        $this->email = $email;
        $this->phone = $phone;
        $this->city = $city;
        $this->direction = $direction;
    }
}
