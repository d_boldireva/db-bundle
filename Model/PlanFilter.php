<?php

namespace TeamSoft\CrmRepositoryBundle\Model;

use TeamSoft\CrmRepositoryBundle\Entity\InfoCompany;
use TeamSoft\CrmRepositoryBundle\Entity\InfoContact;
use TeamSoft\CrmRepositoryBundle\Entity\InfoDictionary;
use TeamSoft\CrmRepositoryBundle\Entity\InfoPlan;
use TeamSoft\CrmRepositoryBundle\Entity\InfoTask;
use TeamSoft\CrmRepositoryBundle\Entity\InfoTasktype;

class PlanFilter
{
    /**
     * @var InfoPlan
     */
    private $plan;

    /**
     * @var InfoTasktype
     */
    private $tasktype;

    /**
     * @var InfoCompany|null
     */
    private $company;

    /**
     * @var InfoContact|null
     */
    private $contact;

    /**
     * @var InfoDictionary|null
     */
    private $specialization;

    /**
     * @var \DateTime|null
     */
    private $datetill;

    /**
     * @return InfoPlan
     */
    public function getPlan(): ?InfoPlan
    {
        return $this->plan;
    }

    /**
     * @param InfoPlan $plan
     */
    public function setPlan(InfoPlan $plan): void
    {
        $this->plan = $plan;
    }

    /**
     * @return InfoTasktype
     */
    public function getTasktype(): ?InfoTasktype
    {
        return $this->tasktype;
    }

    /**
     * @param InfoTasktype $tasktype
     */
    public function setTasktype(?InfoTasktype $tasktype): void
    {
        $this->tasktype = $tasktype;
    }

    /**
     * @return InfoCompany|null
     */
    public function getCompany(): ?InfoCompany
    {
        return $this->company;
    }

    /**
     * @param InfoCompany|null $company
     */
    public function setCompany(?InfoCompany $company): void
    {
        $this->company = $company;
    }

    /**
     * @return InfoContact|null
     */
    public function getContact(): ?InfoContact
    {
        return $this->contact;
    }

    /**
     * @param InfoContact|null $contact
     */
    public function setContact(?InfoContact $contact): void
    {
        $this->contact = $contact;
    }

    /**
     * @return InfoDictionary|null
     */
    public function getSpecialization(): ?InfoDictionary
    {
        return $this->specialization;
    }

    /**
     * @param InfoDictionary|null $specialization
     */
    public function setSpecialization(?InfoDictionary $specialization): void
    {
        $this->specialization = $specialization;
    }

    /**
     * @return \DateTimeImmutable|null
     */
    public function getDatetill(): ?\DateTime
    {
        return $this->datetill;
    }

    /**
     * @param \DateTime|null $datetill
     */
    public function setDatetill(?\DateTime $datetill): void
    {
        $this->datetill = $datetill;
    }

    /**
     * @param InfoTask $task
     * @return PlanFilter
     */
    public static function createFilterByTask(InfoTask $task): PlanFilter
    {
        $filter = new PlanFilter();
        $filter->setContact($task->getContact());
        $filter->setCompany($task->getCompany());
        $filter->setSpecialization($task->getSpecialization());
        $filter->setPlan($task->getPlan());
        $filter->setDatetill($task->getDatetill());
        $filter->setTasktype($task->getTasktype());

        return $filter;
    }
}
