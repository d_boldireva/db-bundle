<?php

namespace TeamSoft\CrmRepositoryBundle\Model;

use TeamSoft\CrmRepositoryBundle\Entity\InfoSurvey;
use TeamSoft\CrmRepositoryBundle\Entity\InfoSurveyResult;
use TeamSoft\CrmRepositoryBundle\Entity\InfoTask;

class TaskSurveyResult implements ContainsTaskInterface
{
    /**
     * @var InfoTask
     */
    private $task;

    /**
     * @var InfoSurvey
     */
    private $survey;

    /**
     * @var InfoSurveyResult[]
     */
    private $surveyResults = [];

    /**
     * @return InfoTask
     */
    public function getTask(): ?InfoTask
    {
        return $this->task;
    }

    /**
     * @param InfoTask $task
     * @return self
     */
    public function setTask(InfoTask $task): self
    {
        $this->task = $task;
        $this->updatedSurveyResults();

        return $this;
    }

    /**
     * @return InfoSurvey
     */
    public function getSurvey(): ?InfoSurvey
    {
        return $this->survey;
    }

    /**
     * @param InfoSurvey $survey
     * @return self
     */
    public function setSurvey(InfoSurvey $survey): self
    {
        $this->survey = $survey;
        $this->updatedSurveyResults();

        return $this;
    }

    /**
     * @return InfoSurveyResult[]
     */
    public function getSurveyResults(): array
    {
        return $this->surveyResults;
    }

    /**
     * @param InfoSurveyResult[] $surveyResults
     * @return self
     */
    public function setSurveyResults(array $surveyResults): self
    {
        $this->surveyResults = $surveyResults;
        $this->updatedSurveyResults();

        return $this;
    }

    /**
     * Add surveyResultCollection.
     *
     * @param \TeamSoft\CrmRepositoryBundle\Entity\InfoSurveyResult $surveyResultCollection
     *
     * @return self
     */
    public function addSurveyResults(InfoSurveyResult $surveyResultCollection): TaskSurveyResult
    {
        $this->updatedSurveyResult($surveyResultCollection);
        $this->surveyResults[] = $surveyResultCollection;

        return $this;
    }

    protected function updatedSurveyResults()
    {
        foreach ($this->surveyResults as $surveyResult) {
            $this->updatedSurveyResult($surveyResult);
        }
    }

    protected function updatedSurveyResult(InfoSurveyResult $surveyResult)
    {
        $surveyResult->setTask($this->getTask());
        $surveyResult->setSurvey($this->getSurvey());
    }
}
