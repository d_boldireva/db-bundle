<?php


namespace TeamSoft\CrmRepositoryBundle\Model;

use TeamSoft\CrmRepositoryBundle\Entity\InfoAction;
use TeamSoft\CrmRepositoryBundle\Entity\InfoSurvey;
use TeamSoft\CrmRepositoryBundle\Entity\InfoSurveyResultAction;

class ActionSurveyResult implements ContainsActionInterface
{
    /**
     * @var InfoAction
     */
    private $action;

    /**
     * @var InfoSurvey
     */
    private $survey;

    /**
     * @var InfoSurveyResultAction[]
     */
    private $surveyResultActions = [];

    /**
     * @return InfoAction
     */
    public function getAction(): ?InfoAction
    {
        return $this->action;
    }

    /**
     * @param InfoAction $action
     * @return self
     */
    public function setAction(InfoAction $action): self
    {
        $this->action = $action;
        $this->updatedSurveyResultActions();

        return $this;
    }

    /**
     * @return InfoSurvey
     */
    public function getSurvey(): ?InfoSurvey
    {
        return $this->survey;
    }

    /**
     * @param InfoSurvey $survey
     * @return self
     */
    public function setSurvey(InfoSurvey $survey): self
    {
        $this->survey = $survey;
        $this->updatedSurveyResultActions();

        return $this;
    }

    /**
     * @return InfoSurveyResultAction[]
     */
    public function getSurveyResultActions(): array
    {
        return $this->surveyResultActions;
    }

    /**
     * @param InfoSurveyResultAction[] $surveyResultAction
     * @return self
     */
    public function setSurveyResultActions(array $surveyResultAction): self
    {
        $this->surveyResultActions = $surveyResultAction;
        $this->updatedSurveyResultActions();

        return $this;
    }

    /**
     * Add surveyResultAction.
     *
     * @param InfoSurveyResultAction $surveyResultAction
     *
     * @return self
     */
    public function addSurveyResultAction(InfoSurveyResultAction $surveyResultAction): self
    {
        $this->updatedSurveyResultAction($surveyResultAction);
        $this->surveyResultActions[] = $surveyResultAction;

        return $this;
    }

    protected function updatedSurveyResultActions()
    {
        foreach ($this->surveyResultActions as $surveyResultAction) {
            $this->updatedSurveyResultAction($surveyResultAction);
        }
    }

    protected function updatedSurveyResultAction(InfoSurveyResultAction $surveyResultAction)
    {
        $surveyResultAction->setAction($this->getAction());
        $surveyResultAction->setSurvey($this->getSurvey());
    }
}
