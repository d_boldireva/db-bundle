<?php

namespace TeamSoft\CrmRepositoryBundle\Model;

class WebService
{
    public $identifier;
    public $enable;
    public $granted;
    public $order;
    public $name;
    public $description;
    public $img;
    public $route;
    public $url;
    public $targetBlank;

    public function __construct(
        $identifier,
        $enable,
        $order,
        $granted,
        $targetBlank,
        $name,
        $description,
        $img,
        $route,
        $url
    )
    {
        $this->identifier = $identifier;
        $this->enable = $enable;
        $this->order = $order;
        $this->name = $name;
        $this->description = $description;
        $this->img = $img;
        $this->route = $route;
        $this->url = $url;
        $this->targetBlank = $targetBlank;
        $this->granted = $granted;
    }


    public function getUrl($locale = null) {
        if ($locale) {
            return str_replace('{_locale}', strtolower($locale), $this->url);
        }
        return $this->url;
    }
}
