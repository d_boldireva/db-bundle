<?php

namespace TeamSoft\CrmRepositoryBundle\Model\Filters;

class BaseFilter {
    /** @var string[] */
    protected $orderBy;

    /** @var int */
    protected $offset;

    /** @var int */
    protected $limit;
    /**
     * @return string[]
     */
    public function getOrderBy()
    {
        return $this->orderBy;
    }

    /**
     * @param string[] $orderBy
     * @return static
     */
    public function setOrderBy($orderBy)
    {
        $this->orderBy = $orderBy;
        return $this;
    }

    /**
     * @return int
     */
    public function getOffset()
    {
        return $this->offset;
    }

    /**
     * @param int $offset
     * @return static
     */
    public function setOffset($offset)
    {
        $this->offset = $offset;
        return $this;
    }

    /**
     * @return int
     */
    public function getLimit()
    {
        return $this->limit;
    }

    /**
     * @param int $limit
     * @return static
     */
    public function setLimit($limit)
    {
        $this->limit = $limit;
        return $this;
    }
}
