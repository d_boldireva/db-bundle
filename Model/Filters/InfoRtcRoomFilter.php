<?php

namespace TeamSoft\CrmRepositoryBundle\Model\Filters;

use TeamSoft\CrmRepositoryBundle\Entity\InfoUser;

class InfoRtcRoomFilter extends BaseFilter {
    /**
     * @var int[]|InfoUser[]
     */
    private $userId;

    /**
     * @return int[]|InfoUser[]
     */
    public function getUserId(){
        return $this->userId;
    }

    /**
     * @param int[]|InfoUser[] $userId
     * @return $this
     */
    public function setUserId($userId){
        $this->userId = $userId;
        return $this;
    }
}
