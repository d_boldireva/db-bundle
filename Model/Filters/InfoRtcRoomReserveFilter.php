<?php

namespace TeamSoft\CrmRepositoryBundle\Model\Filters;

use TeamSoft\CrmRepositoryBundle\Entity\InfoContact;
use TeamSoft\CrmRepositoryBundle\Entity\RTC\InfoRtcRoom;
use TeamSoft\CrmRepositoryBundle\Entity\InfoUser;

class InfoRtcRoomReserveFilter extends BaseFilter
{
    /** @var int[]|InfoRtcRoom[] */
    private $rtcRoomId;

    /** @var int[]|InfoUser[] */
    private $responsibleId;

    /** @var int[]|InfoContact[] */
    private $contactId;

    /** @var \DateTime */
    private $datefrom;

    /** @var \DateTime */
    private $datetill;

    /**
     * @return int[]|InfoRtcRoom[]
     */
    public function getRtcRoomId()
    {
        return $this->rtcRoomId;
    }

    /**
     * @param int[]|InfoRtcRoom[] $rtcRoomId
     * @return static
     */
    public function setRtcRoomId($rtcRoomId)
    {
        $this->rtcRoomId = $rtcRoomId;
        return $this;
    }

    /**
     * @return int[]|InfoUser[]
     */
    public function getResponsibleId()
    {
        return $this->responsibleId;
    }

    /**
     * @param int[]|InfoUser[] $responsibleId
     * @return static
     */
    public function setResponsibleId($responsibleId)
    {
        $this->responsibleId = $responsibleId;
        return $this;
    }

    /**
     * @return int[]|InfoContact[]
     */
    public function getContactId()
    {
        return $this->contactId;
    }

    /**
     * @param int[]|InfoContact[] $contactId
     * @return static
     */
    public function setContactId($contactId)
    {
        $this->contactId = $contactId;
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getDatefrom()
    {
        return $this->datefrom;
    }

    /**
     * @param \DateTime $datefrom
     * @return static
     */
    public function setDatefrom(\DateTime $datefrom)
    {
        $this->datefrom = $datefrom;
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getDatetill()
    {
        return $this->datetill;
    }

    /**
     * @param \DateTime $datetill
     * @return static
     */
    public function setDatetill(\DateTime $datetill)
    {
        $this->datetill = $datetill;
        return $this;
    }
}
