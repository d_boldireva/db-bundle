<?php

namespace TeamSoft\CrmRepositoryBundle\Model;

use Symfony\Component\HttpFoundation\File\UploadedFile;
use TeamSoft\CrmRepositoryBundle\Entity\Sales\PsPattern;
use TeamSoft\CrmRepositoryBundle\Entity\Sales\PsPatternType;

class DistributorFile
{
    /**
     * @var UploadedFile
     */
    private $file;

    /**
     * @var int
     */
    private $packages = 0;

    /**
     * @var PsPatternType
     */
    private $type;

    /**
     * @var PsPattern|null
     */
    private $pattern;

    /**
     * @return UploadedFile
     */
    public function getFile()
    {
        return $this->file;
    }

    /**
     * @param UploadedFile $file
     */
    public function setFile(UploadedFile $file)
    {
        $this->file = $file;
    }

    /**
     * @return int
     */
    public function getPackages(): int
    {
        return $this->packages;
    }

    /**
     * @param int $packages
     */
    public function setPackages(?int $packages)
    {
        $this->packages = $packages;
    }

    public function __construct()
    {
        $this->setPackages(0);
    }

    /**
     * @return PsPatternType
     */
    public function getType(): ?PsPatternType
    {
        return $this->type;
    }

    /**
     * @param PsPatternType $type
     */
    public function setType(?PsPatternType $type)
    {
        $this->type = $type;
    }

    /**
     * @return PsPattern|null
     */
    public function getPattern(): ?PsPattern
    {
        return $this->pattern;
    }

    /**
     * @param PsPattern|null $pattern
     */
    public function setPattern(?PsPattern $pattern): void
    {
        $this->pattern = $pattern;
    }
}
