<?php

namespace TeamSoft\CrmRepositoryBundle\Model;


abstract class InfoFile implements FileInterface
{

    abstract public function getName();

    abstract public function getContent();

    abstract public function setContent($content);

    public function setExtension(?string $type)
    {
    }

    public function getExtension(): ?string
    {
        return strtolower(pathinfo($this->getName(), PATHINFO_EXTENSION));
    }

    public function getResourceContent()
    {
        if (is_resource($this->getContent())) {
            rewind($this->getContent());
            return stream_get_contents($this->getContent());
        } else if (is_string($this->getContent()) && $stream = fopen($this->getContent(), 'r')) {
            $content = stream_get_contents($stream);
            fclose($stream);
            return $content;
        } else {
            return $this->getContent();
        }
    }
}