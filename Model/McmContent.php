<?php

namespace TeamSoft\CrmRepositoryBundle\Model;

use App\Service\BaseService;
use TeamSoft\CrmRepositoryBundle\Entity\InfoContact;
use TeamSoft\CrmRepositoryBundle\Entity\InfoMcmcontent;
use TeamSoft\CrmRepositoryBundle\Entity\InfoMcmdistribution;
use TeamSoft\CrmRepositoryBundle\Entity\InfoMcmdistributionaction;
use TeamSoft\CrmRepositoryBundle\Entity\InfoMcmfile;
use TeamSoft\CrmRepositoryBundle\Entity\InfoUser;

class McmContent
{
    /**
     * @var int
     */
    private $contentId;

    /**
     * @var int
     */
    private $distributionId;

    /**
     * @var string|null
     */
    private $name;

    /**
     * @var string|null
     */
    private $subject;

    /**
     * @var string|null
     */
    private $body;

    /**
     * @var string|null
     */
    private $image;

    /**
     * @var string|null
     */
    private $url;

    /**
     * @var string|null
     */
    private $alternateBody;

    /**
     * @var InfoContact
     */
    private $contact;

    /**
     * @var InfoMcmdistribution
     */
    private $distribution;

    /**
     * @var BaseService
     */
    private $service;

    /**
     * @var string
     */
    private $preparedBody;

    /**
     * @var string
     */
    private $preparedAlternateBody;

    /**
     * @var string
     */
    private $type;

    /**
     * @var string|null
     */
    private $caption;

    /**
     * @var InfoUser
     */
    private $sender;

    /**
     * @var InfoMcmcontent
     */
    private $content;

    /**
     * @var InfoMcmfile[]
     */
    private $files;

    public function __construct(
        InfoMcmdistribution $distribution,
        InfoUser $sender,
        BaseService $baseMsmService,
        InfoContact $contact = null
    )
    {
        $this->contact = $contact;
        $this->service = $baseMsmService;
        $this->sender = $sender;
        $this->content = $distribution->getMcmcontent();


        $this->distribution = $distribution;
        $this->distributionId = $this->distribution->getId();

        $this->contentId = $this->content->getId();
        $this->name = $this->content->getName();
        $this->subject = $this->content->getSubject();
        $this->body = $this->content->getBody();
        $this->alternateBody = $this->content->getAlternateBody();
        $this->url = $this->content->getUrl();
        $this->type = $this->content->getType();
        $this->image = $this->content->getImage();
        $this->caption = $this->content->getCaption();

        if ($contact) {
            $this->getPreparedAlternateBody();
            $this->getPreparedBody();
        }

        if ($this->content->isTypeEmail()) {
            $this->files = $this->content->getFileCollection();
        }
    }

    /**
     * @return int
     */
    public function getContentId(): int
    {
        return $this->contentId;
    }

    /**
     * @return string|null
     */
    public function getName(): ?string
    {
        return $this->name;
    }

    /**
     * @return string|null
     */
    public function getSubject(): ?string
    {
        return $this->subject;
    }

    /**
     * @return string|null
     */
    public function getBody(): ?string
    {
        return $this->body;
    }

    /**
     * @return string|null
     */
    public function getImage(): ?string
    {
        return $this->image;
    }

    /**
     * @return string|null
     */
    public function getUrl(): ?string
    {
        return $this->url;
    }

    /**
     * @return string|null
     */
    public function getAlternateBody(): ?string
    {
        return $this->alternateBody;
    }

    /**
     * @return InfoContact
     */
    public function getContact(): InfoContact
    {
        return $this->contact;
    }

    /**
     * @return InfoMcmdistribution
     */
    public function getDistribution(): InfoMcmdistribution
    {
        return $this->distribution;
    }

    /**
     * @return string
     */
    public function getType(): string
    {
        return $this->type;
    }

    /**
     * @return string|null
     */
    public function getCaption(): ?string
    {
        return $this->caption;
    }

    /**
     * @return int
     */
    public function getDistributionId(): int
    {
        return $this->distributionId;
    }

    /**
     * @return string
     */
    public function getPreparedBody(): string
    {
        if ($this->preparedBody === null) {
            $this->preparedBody = $this->preparedMessage($this->getBody());
        }

        return $this->preparedBody;
    }

    /**
     * @return string
     */
    public function getPreparedAlternateBody(): string
    {
        if ($this->preparedAlternateBody === null) {
            $this->preparedAlternateBody = $this->preparedMessage($this->getAlternateBody());
        }

        return $this->preparedAlternateBody;
    }

    /**
     * @param string|null $message
     * @return string
     */
    protected function preparedMessage(?string $message): string
    {
        // '0' is allowed
        if ($message === null || trim($message) === '') {
            return '';
        }

        return $this->service->replaceVariables($message, $this->getContactVariables());
    }

    private $contactVariables;

    /**
     * @return array
     */
    protected function getContactVariables(): array
    {
        if ($this->contactVariables !== null) {
            return $this->contactVariables;
        }

        $this->contactVariables = [];

        $contentVariables = $this->service->getContentVariables($this->content);
        if (!$contentVariables) {
            return [];
        }

        $this->contactVariables = $this->service->getContactVariables(
            $contentVariables,
            $this->getContact(),
            $this->getAction()
        );

        return $this->contactVariables;
    }

    /**
     * @return InfoMcmdistributionaction
     */
    protected function getAction(): InfoMcmdistributionaction
    {
        $action = new InfoMcmdistributionaction();
        $action->setContact($this->getContact());
        $action->setDistribution($this->getDistribution());
        $action->setUser($this->sender);
        $action->setSenddate(new \DateTime());

        return $action;
    }
}
