<?php
namespace TeamSoft\CrmRepositoryBundle\Model;

interface FileInterface {

    public function getName();

    public function getContent();

    public function setContent($content);

    public function getResourceContent();
    
    public function setExtension(?string $type);

    public function getExtension(): ?string;
}