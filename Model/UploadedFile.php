<?php

namespace TeamSoft\CrmRepositoryBundle\Model;

use Symfony\Component\HttpFoundation\File\UploadedFile as HttpUploadedFile;

class UploadedFile
{

    /**
     * @var HttpUploadedFile
     */
    private $file;

    /**
     * @var string
     */
    private $filename;

    /**
     * @var string
     */
    private $type;

    /**
     * @var string
     */
    private $distributor;

    /**
     * @var \DateTime
     */
    private $date;

    /**
     * @return HttpUploadedFile
     */
    public function getFile()
    {
        return $this->file;
    }

    /**
     * @param HttpUploadedFile $file
     */
    public function setFile(HttpUploadedFile $file)
    {
        $this->file = $file;
    }

    /**
     * @return string
     */
    public function getFilename()
    {
        return $this->filename;
    }

    /**
     * @param string $filename
     */
    public function setFilename($filename)
    {
        $this->filename = $filename;
    }

    /**
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param string $type
     */
    public function setType($type)
    {
        $this->type = $type;
    }

    /**
     * @return string
     */
    public function getDistributor()
    {
        return $this->distributor;
    }

    /**
     * @param string $distributor
     */
    public function setDistributor($distributor)
    {
        $this->distributor = $distributor;
    }

    /**
     * @return \DateTime
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * @param \DateTime $date
     */
    public function setDate($date)
    {
        $this->date = $date;
    }


}
