<?php

namespace TeamSoft\CrmRepositoryBundle\DQL;

use Doctrine\DBAL\Platforms\PostgreSqlPlatform;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Query\AST\Functions\FunctionNode;
use Doctrine\ORM\Query\Lexer;
use Doctrine\ORM\Query\SqlWalker;
use TeamSoft\CrmRepositoryBundle\DBAL\Types\Geometry;

/**
 * Class STBuffer
 * @package TeamSoft\CrmRepositoryBundle\DQL
 *
 * STBuffer ::= "STBuffer" "(" ArithmeticExpression "," ArithmeticExpression ")"
 */
class STBuffer extends FunctionNode
{
    /** @var Geometry */
    private $geometryA = null;
    /** @var float */
    private $radius = null;

    /**
     * @param \Doctrine\ORM\Query\Parser $parser
     * @throws \Doctrine\ORM\Query\QueryException
     */
    public function parse(\Doctrine\ORM\Query\Parser $parser)
    {
        $parser->match(Lexer::T_IDENTIFIER);
        $parser->match(Lexer::T_OPEN_PARENTHESIS);
        $this->geometryA = $parser->ArithmeticExpression();
        $parser->match(Lexer::T_COMMA);
        $this->radius = $parser->ArithmeticExpression();
        $parser->match(Lexer::T_CLOSE_PARENTHESIS);
    }

    /**
     * @param \Doctrine\ORM\Query\SqlWalker $sqlWalker
     * @return string
     * @throws \Doctrine\DBAL\DBALException
     */
    public function getSql(SqlWalker $sqlWalker)
    {
        $platform = $sqlWalker->getConnection()->getDatabasePlatform();

        if( $platform instanceof PostgreSqlPlatform){
            return $this->getSQlPGSql($sqlWalker);
        } else {
            return $this->getSqlSQLServer($sqlWalker);
        }
    }

    /**
     * @param SqlWalker $sqlWalker
     * @return string
     */
    private function getSQlPGSql(SqlWalker $sqlWalker){
        return static::toPGSql(
              $sqlWalker->walkArithmeticExpression($this->geometryA)
            , $sqlWalker->walkArithmeticExpression($this->radius)
        );
    }

    /**
     * @param SqlWalker $sqlWalker
     * @return string
     */
    private function getSqlSQLServer(SqlWalker $sqlWalker){
        return static::toSQLSrv(
              $sqlWalker->walkArithmeticExpression($this->geometryA)
            , $sqlWalker->walkArithmeticExpression($this->radius)
        );
    }

    /**
     * @param string $subject
     * @param int $radius
     * @return string
     */
    private static function toPGSql( $subject, $radius ) {
        return sprintf("ST_Buffer(%s,%f)"
            , $subject
            , $radius
        );
    }

    /**
     * @param string $subject
     * @param int $radius
     * @return string
     */
    private static function toSQLSrv( $subject, $radius ) {
        return sprintf("%s.STBuffer(%f)"
            , $subject
            , $radius
        );
    }

    /**
     * @param EntityManagerInterface $em
     * @param string $subject
     * @param int $radius
     * @return string
     * @throws \Doctrine\DBAL\DBALException
     */
    public static function toSQL( EntityManagerInterface $em, $subject, $radius = 0) {
        $dp = $em->getConnection()->getDatabasePlatform();

        if( $dp instanceof PostgreSqlPlatform ) {
            return static::toPGSql($subject, $radius);
        } else {
            return static::toSQLSrv($subject, $radius);
        }
    }
}
