<?php

namespace TeamSoft\CrmRepositoryBundle\DQL;

use Doctrine\DBAL\Platforms\PostgreSqlPlatform;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Query\AST\Functions\FunctionNode;
use Doctrine\ORM\Query\Lexer;
use Doctrine\ORM\Query\SqlWalker;
use TeamSoft\CrmRepositoryBundle\DBAL\Types\Geometry;

/**
 * Class STBuffer
 * @package TeamSoft\CrmRepositoryBundle\DQL
 *
 * BufferWithTolerance ::= "BufferWithTolerance" "(" ArithmeticExpression "," ArithmeticExpression "," ArithmeticExpression "," ArithmeticPrimary ")"
 */
class BufferWithTolerance extends FunctionNode
{
    /** @var Geometry */
    private $geometryA = null;
    /** @var float */
    private $distance = null;
    /** @var float */
    private $tolerance = null;
    /** @var integer */
    private $relative = null;

    /**
     * @param \Doctrine\ORM\Query\Parser $parser
     * @throws \Doctrine\ORM\Query\QueryException
     */
    public function parse(\Doctrine\ORM\Query\Parser $parser)
    {
        $parser->match(Lexer::T_IDENTIFIER);
        $parser->match(Lexer::T_OPEN_PARENTHESIS);
        $this->geometryA = $parser->ArithmeticExpression();
        $parser->match(Lexer::T_COMMA);
        $this->distance = $parser->ArithmeticExpression();
        $parser->match(Lexer::T_COMMA);
        $this->tolerance = $parser->ArithmeticExpression();
        $parser->match(Lexer::T_COMMA);
        $this->relative = $parser->ArithmeticPrimary();
        $parser->match(Lexer::T_CLOSE_PARENTHESIS);
    }

    /**
     * @param \Doctrine\ORM\Query\SqlWalker $sqlWalker
     * @return string
     * @throws \Doctrine\DBAL\DBALException
     */
    public function getSql(SqlWalker $sqlWalker)
    {
        $platform = $sqlWalker->getConnection()->getDatabasePlatform();

        if( $platform instanceof PostgreSqlPlatform){
            return $this->getSQlPGSql($sqlWalker);
        } else {
            return $this->getSqlSQLServer($sqlWalker);
        }
    }

    /**
     * @param SqlWalker $sqlWalker
     * @return string
     */
    private function getSQlPGSql(SqlWalker $sqlWalker){
        return self::toPGSql(
            $sqlWalker->walkArithmeticExpression($this->geometryA)
            , $sqlWalker->walkArithmeticExpression($this->distance)
        );
    }

    /**
     * @param SqlWalker $sqlWalker
     * @return string
     */
    private function getSqlSQLServer(SqlWalker $sqlWalker){
        return static::toSQLSrv(
            $sqlWalker->walkArithmeticExpression($this->geometryA)
            , $sqlWalker->walkArithmeticExpression($this->distance)
            , $sqlWalker->walkArithmeticExpression($this->tolerance)
            , $sqlWalker->walkArithmeticPrimary($this->relative)
        );
    }

    /**
     * @param $subject
     * @param int $distance
     * @return string
     */
    private static function toPGSql( $subject, $distance = 0 ){
        return sprintf("ST_Buffer(%s,%f)"
            , $subject
            , $distance
        );
    }

    /**
     * @param $subject
     * @param int $distance
     * @param int $tolerance
     * @param int $relative
     * @return string
     */
    private static function toSQLSrv( $subject, $distance = 0, $tolerance = 0, $relative = 1 ){
        return sprintf("%s.BufferWithTolerance(%f,%f,%d)"
            , $subject
            , $distance
            , $tolerance
            , $relative
        );
    }

    /**
     * @param EntityManagerInterface $em
     * @param $subject
     * @param int $distance
     * @param int $tolerance
     * @param int $relative
     * @return string
     * @throws \Doctrine\DBAL\DBALException
     */
    public static function toSQL( EntityManagerInterface $em, $subject, $distance = 0, $tolerance = 0, $relative = 1 ) {
        $dp = $em->getConnection()->getDatabasePlatform();

        if( $dp instanceof PostgreSqlPlatform ) {
            return static::toPGSql( $subject, $distance );
        } else {
            return static::toSQLSrv( $subject, $distance, $tolerance, $relative );
        }
    }
}
