<?php declare(strict_types=1);

namespace TeamSoft\CrmRepositoryBundle\DQL;

use Doctrine\DBAL\Platforms\PostgreSQL100Platform;
use Doctrine\DBAL\Platforms\SQLServer2008Platform;
use Doctrine\ORM\Query\AST\Functions\FunctionNode;
use Doctrine\ORM\Query\Lexer;

class ToDate extends FunctionNode
{
    private $date;

    public function parse(\Doctrine\ORM\Query\Parser $parser)
    {
        $parser->match(Lexer::T_IDENTIFIER);
        $parser->match(Lexer::T_OPEN_PARENTHESIS);
        $this->date = $parser->ArithmeticPrimary();
        $parser->match(Lexer::T_CLOSE_PARENTHESIS);
    }

    public function getSql(\Doctrine\ORM\Query\SqlWalker $sqlWalker)
    {
        $platform = $sqlWalker->getConnection()->getDatabasePlatform();
        if ($platform instanceof SQLServer2008Platform) {
            return 'CONVERT( NVARCHAR, ' . $sqlWalker->walkArithmeticPrimary($this->date) . ', 23 )';
        } else if ($platform instanceof PostgreSQL100Platform) {
            return $sqlWalker->walkArithmeticPrimary($this->date) . '::date';
        }
    }
}
