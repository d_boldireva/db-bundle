<?php

namespace TeamSoft\CrmRepositoryBundle\DQL;

use Doctrine\DBAL\Platforms\PostgreSqlPlatform;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Query\AST\Functions\FunctionNode;
use Doctrine\ORM\Query\Lexer;
use Doctrine\ORM\Query\SqlWalker;
use TeamSoft\CrmRepositoryBundle\DBAL\Types\Geometry;

/**
 * Class PointFromCoords
 * @package TeamSoft\CrmRepositoryBundle\DQL
 * PointFromCoords ::= "PointFromCoords" "(" ArithmeticExpression "," ArithmeticExpression ")"
 */
class PointFromCoords extends FunctionNode
{
    /** @var Geometry */
    private $gpsLng = null;
    /** @var Geometry */
    private $gpsLa = null;

    /**
     * @param \Doctrine\ORM\Query\Parser $parser
     * @throws \Doctrine\ORM\Query\QueryException
     */
    public function parse(\Doctrine\ORM\Query\Parser $parser)
    {
        $parser->match(Lexer::T_IDENTIFIER);
        $parser->match(Lexer::T_OPEN_PARENTHESIS);
        $this->gpsLng = $parser->ArithmeticExpression();
        $parser->match(Lexer::T_COMMA);
        $this->gpsLa = $parser->ArithmeticExpression();
        $parser->match(Lexer::T_CLOSE_PARENTHESIS);
    }

    /**
     * @param \Doctrine\ORM\Query\SqlWalker $sqlWalker
     * @return string
     * @throws \Doctrine\DBAL\DBALException
     */
    public function getSql(SqlWalker $sqlWalker)
    {
        $platform = $sqlWalker->getConnection()->getDatabasePlatform();

        if( $platform instanceof PostgreSqlPlatform){
            return $this->getSQlPGSql($sqlWalker);
        } else {
            return $this->getSqlSQLServer($sqlWalker);
        }
    }

    /**
     * @param SqlWalker $sqlWalker
     * @return string
     */
    private function getSQlPGSql(SqlWalker $sqlWalker){
        return static::toPGSql(
              $sqlWalker->walkArithmeticExpression($this->gpsLng)
            , $sqlWalker->walkArithmeticExpression($this->gpsLa)
        );
    }

    /**
     * @param SqlWalker $sqlWalker
     * @return string
     */
    private function getSqlSQLServer(SqlWalker $sqlWalker){
        return static::toSQLSrv(
              $sqlWalker->walkArithmeticExpression($this->gpsLng)
            , $sqlWalker->walkArithmeticExpression($this->gpsLa)
        );
    }

    /**
     * @param string $subjectA
     * @param string $subjectB
     * @return string
     */
    private static function toPGSql($subjectA, $subjectB){
        return sprintf("PointFromText('POINT(' + CAST(%s as VARCHAR(16)) + ' ' + CAST(%s as VARCHAR(16)) + ')', 4326)", $subjectA, $subjectB );
    }

    /**
     * @param string $subjectA
     * @param string $subjectB
     * @return string
     */
    private static function toSQLSrv($subjectA, $subjectB){
        return sprintf("geometry::STPointFromText('POINT (' + CAST(%s as VARCHAR(16)) + ' ' + CAST(%s as VARCHAR(16)) + ')',0)", $subjectA, $subjectB );
    }

    /**
     * @param EntityManagerInterface $em
     * @param string $subjectA
     * @param string $subjectB
     * @return string
     * @throws \Doctrine\DBAL\DBALException
     */
    public static function toSQL(EntityManagerInterface $em, $subjectA, $subjectB){
        $dp = $em->getConnection()->getDatabasePlatform();

        if( $dp instanceof PostgreSqlPlatform ) {
            return static::toPGSql( $subjectA, $subjectB );
        } else {
            return static::toSQLSrv( $subjectA, $subjectB );
        }
    }
}
