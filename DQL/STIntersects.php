<?php

namespace TeamSoft\CrmRepositoryBundle\DQL;

use Doctrine\DBAL\Platforms\PostgreSqlPlatform;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Query\AST\Functions\FunctionNode;
use Doctrine\ORM\Query\Lexer;
use Doctrine\ORM\Query\SqlWalker;
use TeamSoft\CrmRepositoryBundle\DBAL\Types\Geometry;

/**
 * Class STIntersects
 * @package TeamSoft\CrmRepositoryBundle\DQL
 *
 * STIntersects ::= "STIntersects" "(" ArithmeticExpression "," ArithmeticExpression ")"
 */
class STIntersects extends FunctionNode
{
    /** @var Geometry */
    private $geometryA = null;
    /** @var Geometry */
    private $geometryB = null;

    /**
     * @param \Doctrine\ORM\Query\Parser $parser
     * @throws \Doctrine\ORM\Query\QueryException
     */
    public function parse(\Doctrine\ORM\Query\Parser $parser)
    {
        $parser->match(Lexer::T_IDENTIFIER);
        $parser->match(Lexer::T_OPEN_PARENTHESIS);
        $this->geometryA = $parser->ArithmeticExpression();
        $parser->match(Lexer::T_COMMA);
        $this->geometryB = $parser->ArithmeticExpression();
        $parser->match(Lexer::T_CLOSE_PARENTHESIS);
    }

    /**
     * @param \Doctrine\ORM\Query\SqlWalker $sqlWalker
     * @return string
     * @throws \Doctrine\DBAL\DBALException
     */
    public function getSql(SqlWalker $sqlWalker)
    {
        $platform = $sqlWalker->getConnection()->getDatabasePlatform();

        if( $platform instanceof PostgreSqlPlatform){
            return $this->getSQlPGSql($sqlWalker);
        } else {
            return $this->getSqlSQLServer($sqlWalker);
        }
    }

    /**
     * @param SqlWalker $sqlWalker
     * @return string
     */
    private function getSQlPGSql(SqlWalker $sqlWalker){
        return static::toPGSql(
              $sqlWalker->walkArithmeticExpression($this->geometryA)
            , $sqlWalker->walkArithmeticExpression($this->geometryB)
        );
    }

    /**
     * @param SqlWalker $sqlWalker
     * @return string
     */
    private function getSqlSQLServer(SqlWalker $sqlWalker){
        return static::toSQLSrv(
              $sqlWalker->walkArithmeticExpression($this->geometryA)
            , $sqlWalker->walkArithmeticExpression($this->geometryB)
        );
    }

    /**
     * @param string $subjectA
     * @param string $subjectB
     * @return string
     */
    private static function toPGSql(string $subjectA, string $subjectB){
        return sprintf("CASE WHEN ST_Intersects(%s,%s) THEN 1 ELSE 0 END", $subjectA, $subjectB );
    }

    /**
     * @param string $subjectA
     * @param string $subjectB
     * @return string
     */
    private static function toSQLSrv(string $subjectA, string $subjectB){
        return sprintf("%s.STIntersects(%s)", $subjectA, $subjectB );
    }

    /**
     * @param EntityManagerInterface $em
     * @param string $subjectA
     * @param string $subjectB
     * @return string
     * @throws \Doctrine\DBAL\DBALException
     */
    public static function toSQL(EntityManagerInterface $em, string $subjectA, string $subjectB ){
        $dp = $em->getConnection()->getDatabasePlatform();

        if ($dp instanceof PostgreSqlPlatform) {
            return static::toPGSql($subjectA,$subjectB);
        } else {
            return static::toSQLSrv($subjectA,$subjectB);
        }
    }
}
