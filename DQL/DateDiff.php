<?php

namespace TeamSoft\CrmRepositoryBundle\DQL;

use Doctrine\DBAL\Platforms\PostgreSQL100Platform;
use Doctrine\DBAL\Platforms\SQLServer2008Platform;
use Doctrine\ORM\Query\AST\Functions\FunctionNode;
use Doctrine\ORM\Query\AST\Literal;
use Doctrine\ORM\Query\Lexer;

class DateDiff extends FunctionNode
{
    private $expr1;
    private $expr2;
    private $expr3;

    public function parse(\Doctrine\ORM\Query\Parser $parser)
    {
        $parser->match(Lexer::T_IDENTIFIER);
        $parser->match(Lexer::T_OPEN_PARENTHESIS);
        $parser->match(Lexer::T_IDENTIFIER);
        $lexer = $parser->getLexer();
        $this->expr1 = $lexer->token['value'];
        $parser->match(Lexer::T_COMMA);
        $this->expr2 = $parser->ArithmeticPrimary();
        $parser->match(Lexer::T_COMMA);
        $this->expr3 = $parser->ArithmeticPrimary();
        $parser->match(Lexer::T_CLOSE_PARENTHESIS);
    }

    public function getSql(\Doctrine\ORM\Query\SqlWalker $sqlWalker)
    {
        $platform = $sqlWalker->getConnection()->getDatabasePlatform();
        if ($platform instanceof SQLServer2008Platform) {
            return $this->getSQLServerDateDiffExpression(
                $this->expr1,
                $sqlWalker->walkArithmeticPrimary($this->expr2),
                $sqlWalker->walkArithmeticPrimary($this->expr3)
            );
        } else if ($platform instanceof PostgreSQL100Platform) {
            switch ($this->expr1) {
                case 'month':
                    return $this->getPostgreSQLDateDiffMonthExpression(
                        $sqlWalker->walkArithmeticPrimary($this->expr2),
                        $sqlWalker->walkArithmeticPrimary($this->expr3)
                    );
                case 'q':
                    return $this->getPostgreSQLDateDiffQuarterExpression(
                        $sqlWalker->walkArithmeticPrimary($this->expr2),
                        $sqlWalker->walkArithmeticPrimary($this->expr3)
                    );
                case 'yy':
                    return $this->getPostgreSQLDateDiffYearExpression(
                        $sqlWalker->walkArithmeticPrimary($this->expr2),
                        $sqlWalker->walkArithmeticPrimary($this->expr3)
                    );
            }
        }
    }

    private function getSQLServerDateDiffExpression($unit, $date1, $date2)
    {
        return 'DATEDIFF(' . $unit . ', ' . $date1 . ',' . $date2 . ')';
    }

    private function getPostgreSQLDateDiffMonthExpression($date1, $date2)
    {
        return "(DATE_PART('year', " . $date2 . ") - DATE_PART('year', " . $date1 . ")) * 12 + " .
            "(DATE_PART('month', " . $date2 . ") - DATE_PART('month', " . $date1 . "))";
    }

    private function getPostgreSQLDateDiffQuarterExpression($date1, $date2)
    {
        return "(DATE_PART('year', " . $date2 . ") - DATE_PART('year', " . $date1 . ")) * 4 + " .
            "(DATE_PART('quarter', " . $date2 . ") - DATE_PART('quarter', " . $date1 . "))";
    }

    private function getPostgreSQLDateDiffYearExpression($date1, $date2)
    {
        return "(DATE_PART('year', " . $date2 . ") - DATE_PART('year', " . $date1 . "))";
    }
}
