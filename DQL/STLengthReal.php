<?php

namespace TeamSoft\CrmRepositoryBundle\DQL;

use Doctrine\DBAL\Platforms\PostgreSqlPlatform;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Query\AST\Functions\FunctionNode;
use Doctrine\ORM\Query\Lexer;
use Doctrine\ORM\Query\SqlWalker;
use TeamSoft\CrmRepositoryBundle\DBAL\Types\Geometry;

/**
 * Class STLengthReal
 * @package TeamSoft\CrmRepositoryBundle\DQL
 *
 * STLengthReal ::= "STLengthReal" "(" ArithmeticExpression ")"
 */
class STLengthReal extends FunctionNode
{
    /** @var Geometry */
    private $geometryA = null;

    /**
     * @param \Doctrine\ORM\Query\Parser $parser
     * @throws \Doctrine\ORM\Query\QueryException
     */
    public function parse(\Doctrine\ORM\Query\Parser $parser)
    {
        $parser->match(Lexer::T_IDENTIFIER);
        $parser->match(Lexer::T_OPEN_PARENTHESIS);
        $this->geometryA = $parser->ArithmeticExpression();
        $parser->match(Lexer::T_CLOSE_PARENTHESIS);
    }

    /**
     * @param \Doctrine\ORM\Query\SqlWalker $sqlWalker
     * @return string
     * @throws \Doctrine\DBAL\DBALException
     */
    public function getSql(SqlWalker $sqlWalker)
    {
        $platform = $sqlWalker->getConnection()->getDatabasePlatform();

        if( $platform instanceof PostgreSqlPlatform){
            return $this->getSQlPGSql($sqlWalker);
        } else {
            return $this->getSqlSQLServer($sqlWalker);
        }
    }

    /**
     * @param SqlWalker $sqlWalker
     * @return string
     */
    private function getSQlPGSql(SqlWalker $sqlWalker){
        return static::toPGSql($sqlWalker->walkArithmeticExpression($this->geometryA));
    }

    /**
     * @param SqlWalker $sqlWalker
     * @return string
     */
    private function getSqlSQLServer(SqlWalker $sqlWalker){
        return static::toSQLSrv($sqlWalker->walkArithmeticExpression($this->geometryA));
    }

    /**
     * @param string $subject
     * @return string
     */
    private static function toPGSql(string $subject){
        return sprintf("(ST_Length(geography(%s)) / 1000)", $subject );
    }

    /**
     * @param string $subject
     * @return string
     */
    private static function toSQLSrv(string $subject){
        return sprintf("(%s.STLength() * 111.12)", $subject );
    }

    /**
     * @param EntityManagerInterface $em
     * @param string $subject
     * @return string
     * @throws \Doctrine\DBAL\DBALException
     */
    public static function toSQL(EntityManagerInterface $em, string $subject){
        $dp = $em->getConnection()->getDatabasePlatform();

        if ($dp instanceof PostgreSqlPlatform) {
            return static::toPGSql( $subject );
        } else {
            return static::toSQLSrv( $subject );
        }
    }
}
