<?php

namespace TeamSoft\CrmRepositoryBundle\DQL;

use Doctrine\DBAL\Platforms\PostgreSQL100Platform;
use Doctrine\ORM\Query\AST\Functions\DateAddFunction;
use Doctrine\ORM\Query\AST\Literal;
use Doctrine\ORM\Query\SqlWalker;

class DateAdd extends DateAddFunction
{
    public function getSql(SqlWalker $sqlWalker)
    {
        switch (strtolower($this->unit->value)) {
            case 'quarter':
                return $this->getDateAddQuartersExpression($sqlWalker);
            default:
                return parent::getSql($sqlWalker);
        }
    }

    private function getDateAddQuartersExpression(SqlWalker $sqlWalker)
    {
        $platform = $sqlWalker->getConnection()->getDatabasePlatform();
        if ($platform instanceof PostgreSQL100Platform) {
            return $this->getPostgreSQLDateAddQuartersExpression(
                $this->firstDateExpression->dispatch($sqlWalker),
                $this->intervalExpression->dispatch($sqlWalker)
            );
        } else {
            return $platform->getDateAddQuartersExpression(
                $this->firstDateExpression->dispatch($sqlWalker),
                $this->intervalExpression->dispatch($sqlWalker)
            );
        }
    }

    private function getPostgreSQLDateAddQuartersExpression($date, $interval)
    {
        return '(' . $date . ' + ((' . $interval . "::int) * 3 || ' month')::interval)";
    }
}