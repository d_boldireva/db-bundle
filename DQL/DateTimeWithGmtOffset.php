<?php

namespace TeamSoft\CrmRepositoryBundle\DQL;

use Doctrine\ORM\Query\AST\Functions\FunctionNode;
use Doctrine\ORM\Query\Lexer;
use TeamSoft\CrmRepositoryBundle\DBAL\Platforms\PostgreSQL100Platform;
use TeamSoft\CrmRepositoryBundle\DBAL\Platforms\SQLServer2008Platform;

class DateTimeWithGmtOffset extends FunctionNode
{
    private $expr1;
    private $expr2;
    private $expr3;

    public function parse(\Doctrine\ORM\Query\Parser $parser)
    {
        $parser->match(Lexer::T_IDENTIFIER);
        $parser->match(Lexer::T_OPEN_PARENTHESIS);
        $this->expr1 = $parser->ArithmeticPrimary();
        $parser->match(Lexer::T_COMMA);
        $this->expr2 = $parser->ArithmeticExpression();
        $parser->match(Lexer::T_COMMA);
        $this->expr3 = $parser->ArithmeticExpression();
        $parser->match(Lexer::T_CLOSE_PARENTHESIS);
    }

    public function getSql(\Doctrine\ORM\Query\SqlWalker $sqlWalker)
    {
        $platform = $sqlWalker->getConnection()->getDatabasePlatform();
        if ($platform instanceof SQLServer2008Platform || $platform instanceof PostgreSQL100Platform) {
            return $platform->getDateTimeWithGmtOffsetExpression(
                $sqlWalker->walkArithmeticPrimary($this->expr1),
                $sqlWalker->walkArithmeticPrimary($this->expr2),
                $sqlWalker->walkArithmeticPrimary($this->expr3)
            );
        } else {
            return $sqlWalker->walkArithmeticPrimary($this->expr1);
        }
    }
}
