<?php

namespace TeamSoft\CrmRepositoryBundle\DQL;

use Doctrine\DBAL\Platforms\PostgreSqlPlatform;
use Doctrine\ORM\Query\AST\Functions\FunctionNode;
use Doctrine\ORM\Query\Lexer;
use Doctrine\ORM\Query\Parser;
use Doctrine\ORM\Query\SqlWalker;

/**
 * @author Rafael Kassner <kassner@gmail.com>
 */
class Year extends FunctionNode
{
    public $date;

    private function getSqlSQLServer(SqlWalker $sqlWalker){
        return 'YEAR(' . $sqlWalker->walkArithmeticPrimary($this->date) . ')';
    }

    private function getSQlPGSql(SqlWalker $sqlWalker){
        if(date_parse($sqlWalker->walkArithmeticPrimary($this->date))){
            $date = $sqlWalker->walkArithmeticPrimary($this->date);
        } else {
            $date = 'timestamp '.$sqlWalker->walkArithmeticPrimary($this->date);
        }
        return 'EXTRACT(year FROM ' . $date . ')';
    }

    public function getSql(SqlWalker $sqlWalker)
    {
        $dp = $sqlWalker->getConnection()->getDatabasePlatform();

        if($dp instanceof PostgreSqlPlatform){
            return $this->getSQlPGSql($sqlWalker);
        } else {
            return $this->getSqlSQLServer($sqlWalker);
        }
    }

    public function parse(Parser $parser)
    {
        $parser->match(Lexer::T_IDENTIFIER);
        $parser->match(Lexer::T_OPEN_PARENTHESIS);
        $this->date = $parser->ArithmeticPrimary();
        $parser->match(Lexer::T_CLOSE_PARENTHESIS);
    }
}
