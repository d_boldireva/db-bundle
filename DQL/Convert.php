<?php

namespace TeamSoft\CrmRepositoryBundle\DQL;

use Doctrine\DBAL\Platforms\PostgreSQL100Platform;
use Doctrine\DBAL\Platforms\SQLServer2008Platform;
use Doctrine\ORM\Query\AST\Functions\FunctionNode;
use Doctrine\ORM\Query\Lexer;
/**
 * @author Steve Lacey <steve@stevelacey.net>
 */
class Convert extends FunctionNode
{
    public $dataType = null;
    public $expression = null;
    public $style = null;

    public function parse(\Doctrine\ORM\Query\Parser $parser)
    {
        $lexer = $parser->getLexer();
        $parser->match(Lexer::T_IDENTIFIER);
        $parser->match(Lexer::T_OPEN_PARENTHESIS);

        $parser->match(Lexer::T_IDENTIFIER);
        $type = $parser->getLexer()->token['value'];
        if ($parser->getLexer()->isNextToken(Lexer::T_OPEN_PARENTHESIS)) {
            $parser->match(Lexer::T_OPEN_PARENTHESIS);
            $parameter = $parser->Literal();
            $parameters = [$parameter->value];
            if ($lexer->isNextToken(Lexer::T_COMMA)) {
                while ($lexer->isNextToken(Lexer::T_COMMA)) {
                    $parser->match(Lexer::T_COMMA);
                    $parameter = $parser->Literal();
                    $parameters[] = $parameter->value;
                }
            }
            $parser->match(Lexer::T_CLOSE_PARENTHESIS);
            $type .= '(' . implode(', ', $parameters) . ')';
        }
        $this->dataType = $type;

        $parser->match(Lexer::T_COMMA);
        $this->expression = $parser->StringPrimary();
        if ($lexer->isNextToken(Lexer::T_COMMA)) {
            $parser->match(Lexer::T_COMMA);
            $this->style = $parser->ArithmeticExpression();
        }
        $parser->match(Lexer::T_CLOSE_PARENTHESIS);
    }

    public function getSql(\Doctrine\ORM\Query\SqlWalker $sqlWalker)
    {
        $style = $this->style ? $this->style->dispatch($sqlWalker) : null;

        $platform = $sqlWalker->getConnection()->getDatabasePlatform();
        if ($platform instanceof SQLServer2008Platform) {
            return 'CONVERT(' . $this->dataType . ', ' . $this->expression->dispatch($sqlWalker) . ($style ? ', ' . $style : '') . ')';
        } else if ($platform instanceof PostgreSQL100Platform) {
            return 'CAST(' . $this->expression->dispatch($sqlWalker) . ' AS ' . $this->dataType . ')';
        }
    }
}
