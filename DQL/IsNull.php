<?php

namespace TeamSoft\CrmRepositoryBundle\DQL;

use Doctrine\DBAL\Platforms\PostgreSQL100Platform;
use Doctrine\DBAL\Platforms\SQLServer2008Platform;
use Doctrine\ORM\Query\AST\Functions\FunctionNode;
use Doctrine\ORM\Query\Lexer;

class IsNull extends FunctionNode
{
    private $expr1;
    private $expr2;

    public function parse(\Doctrine\ORM\Query\Parser $parser)
    {
        $parser->match(Lexer::T_IDENTIFIER);
        $parser->match(Lexer::T_OPEN_PARENTHESIS);
        $this->expr1 = $parser->ArithmeticExpression();
        $parser->match(Lexer::T_COMMA);
        $this->expr2 = $parser->ArithmeticExpression();
        $parser->match(Lexer::T_CLOSE_PARENTHESIS);
    }

    public function getSql(\Doctrine\ORM\Query\SqlWalker $sqlWalker)
    {
        $platform = $sqlWalker->getConnection()->getDatabasePlatform();
        if($platform instanceof SQLServer2008Platform) {
            return 'ISNULL('
                .$sqlWalker->walkArithmeticPrimary($this->expr1). ', '
                .$sqlWalker->walkArithmeticPrimary($this->expr2).')';
        } else if ($platform instanceof PostgreSQL100Platform) {
            return 'COALESCE('
                .$sqlWalker->walkArithmeticPrimary($this->expr1). ', '
                .$sqlWalker->walkArithmeticPrimary($this->expr2).')';
        }

        return null;
    }
}