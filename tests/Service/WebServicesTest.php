<?php
/**
 * Created by PhpStorm.
 * User: PC
 * Date: 21.12.2018
 * Time: 12:21
 */

namespace TeamSoft\CrmRepositoryBundle\Tests\Service;


use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use TeamSoft\CrmRepositoryBundle\Service\WebServices;

class WebServicesTest extends KernelTestCase
{

    /**
     * @var WebServices
     */
    private $webServices;
    /**
     * {@inheritDoc}
     */
    protected function setUp()
    {
        self::bootKernel();

        $this->webServices = static::$kernel->getContainer()->get(WebServices::class);
    }

    public function testGetServices()
    {
        $services = $this->webServices->getServices();
        $this->assertArrayHasKey('routes', $services);
    }

    /**
     * {@inheritDoc}
     */
    protected function tearDown()
    {
        parent::tearDown();
    }
}