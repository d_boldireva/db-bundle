<?php

namespace TeamSoft\CrmRepositoryBundle\Translation;

abstract class Validators
{
    public const DOMAIN = 'validators';

    public const END_PERIOD_BIGGER_THEN_START = 'The end of the period must be later than the start of the period';
    public const EXPENSE_UNIQUE_DATA = 'Expense on the date already exist. See expense ID: {{expense_id}} or claim ID: {{claim_id}}';

    public const CAR_REPORT_UNIQUE_DATA = 'Report on the date already exist. See report ID: {{report_id}}';
    public const CAR_REPORT_MIN_ODOMETER_FINISH = '"Return, km" cannot be less than {{odometer_start}}';
    public const CAR_REPORT_ODOMETER_FINISH_LAST_DAY_SHEET = 'The last day must correspond to the monthly report';
    public const CAR_REPORT_MISSING_DAY = 'Please, fill out the report on the {{day}}';
    public const CAR_REPORT_MAX_RETURN = '"Return, km" cannot be more monthly report ({{max_report}} km.)';

    public const CAR_REPORT_SHEETE_EDITABLE_UNIQUE_USER = 'This permission already exist. Please see {{editable_id}}';
    
    public const MAX_VALUE_SMALLER_THEN_MIN = 'This params must be more then min param';

    public const UNIQUE_CONSTRAINT = 'The value of the field "{{field_name}}" already exists';

    public const PASSWORD_FIELDS_MUST_MATCH = 'The password fields must match';

    public const PASSWORD_INVALID_MESSAGE = "Password must be at least 8 characters long and contain: one uppercase letter (A-Z), one lowercase letter (a-z) and one digit (0-9)";

    public const OLD_PASSWORD_SAME_MESSAGE = 'Your new password is the same with old one';

    public const OLD_PASSWORD_INVALID_MESSAGE = 'Incorrect old password entered';
}
