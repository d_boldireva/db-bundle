<?php
declare(strict_types=1);


namespace TeamSoft\CrmRepositoryBundle\Translation;


abstract class Calendar
{
    public const DOMAIN = 'calendar';
}