<?php

namespace TeamSoft\CrmRepositoryBundle\Translation;

use Symfony\Bundle\FrameworkBundle\Translation\Translator as SymfonyTranslator;
use TeamSoft\CrmRepositoryBundle\Entity\InfoLanguageinterface;
use TeamSoft\CrmRepositoryBundle\Repository\InfoLanguageinterface as InfoLanguageinterfaceRepository;

class Translator extends SymfonyTranslator
{

    public const LOCALE_RU = 'ru';
    public const LOCALE_EN = 'en';
    public const LOCALE_UK = 'uk';

    /**
     * @var InfoLanguageinterfaceRepository
     */
    private $repository;

    private array $translations = [];

    /**
     * @param InfoLanguageinterfaceRepository $repository
     */
    public function setRepository(InfoLanguageinterfaceRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     * {@inheritdoc}
     */
    public function trans(?string $id, array $parameters = [], string $domain = null, string $locale = null): string
    {
        if (null === $id) {
            $id = '';
        }

        if (null === $domain) {
            $domain = 'messages';
        }

        if (null === $locale && null !== $this->getLocale()) {
            $locale = $this->getLocale();
        }
        $fallbackLocale = $this->getFallbackLocales()[0]; // fallback locale
        if (null === $locale) {
            $locale = $fallbackLocale;
        }

        if (!isset($this->translations[$domain])) {
            $this->translations[$domain] = [];
        }
        if (!isset($this->translations[$domain][$locale])) {
            $this->translations[$domain][$locale] = $this->repository->findByDomainAndLocale($domain, $locale);
        }

        $translation =  $this->translations[$domain][$locale][$id] ?? null;

        if ($translation instanceof InfoLanguageinterface) {
            $id = $translation->getTranslation();
        }

        return parent::trans($id, $parameters, $domain, $locale);
    }
}
