<?php

namespace TeamSoft\CrmRepositoryBundle\Translation;

abstract class Messages
{
    public const DOMAIN = 'messages';

    public const INVALID_MIME_TYPE_MESSAGE = "File type does not suit its extension. To import this file save it as one of following types .csv, .xls, .xlsx, .xlsm";
    public const FILE_IS_TO_LARGE_MESSAGE = 'Uploaded file is too large. Server supports upload of files up to %size%. Try to split it into multiple chunks.';

    //TODO install sentry or email notification about program error and update message
    public const MESSAGE_TABLE_DOES_NOT_EXIST = 'Unknown table for uploaded file {{file_name}}. This is a technical error, please, call support to solve this problem.';
    public const MESSAGE_FILE_UNREADABLE = 'Can not read uploaded file. Please, call support and inform about this problem.';
    public const MESSAGE_EMPTY_COLUMNS = 'Name of the columns are not defined. Please check the uploading file.';

    public const GRANUAL_PLAN_SALE_PERIOD_NOT_FOUND = 'Sale period for granular plan not found. Please, create it.';
}
