<?php

namespace TeamSoft\CrmRepositoryBundle\Repository;

use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use TeamSoft\CrmRepositoryBundle\Entity\InfoLanguageinterface as InfoLanguageinterfaceEntity;

class InfoLanguageinterface extends ServiceEntityRepository
{
    /**
     * InfoLanguageinterface constructor.
     *
     * @param ManagerRegistry $registry
     */
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, InfoLanguageinterfaceEntity::class);
    }

    /**
     * @param string $domain
     * @param string $locale
     *
     * @return mixed
     */
    public function findByDomainAndLocale(string $domain, string $locale)
    {
        return $this
            ->createQueryBuilder('t', 't.key')
            ->join('t.language', 'l')
            ->where('t.domain = :domain')
            ->andWhere('l.slug = :locale')
            ->setParameter('domain', $domain)
            ->setParameter('locale', $locale)
            ->getQuery()
            ->getResult();
    }

    /**
     * @param string $key
     * @param string $domain
     * @param string $locale
     * @return mixed
     * @throws \Doctrine\ORM\NoResultException
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function findByKey(string $key, string $domain, string $locale)
    {
        return $this
            ->createQueryBuilder('t', 't.key')
            ->join('t.language', 'l')
            ->where('t.key = :key')
            ->andWhere('t.domain = :domain')
            ->andWhere('l.slug = :locale')
            ->setParameter('domain', $domain)
            ->setParameter('locale', $locale)
            ->setParameter('key', $key)
            ->setMaxResults(1)
            ->getQuery()
            ->getOneOrNullResult();
    }
}
