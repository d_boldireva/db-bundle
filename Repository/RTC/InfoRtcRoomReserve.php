<?php

namespace TeamSoft\CrmRepositoryBundle\Repository\RTC;

use Doctrine\ORM\EntityRepository;
use TeamSoft\CrmRepositoryBundle\Model\Filters\InfoRtcRoomReserveFilter;

/**
 * @see \TeamSoft\CrmRepositoryBundle\Entity\RTC\InfoRtcRoomReserve
 * Class InfoRtcRoomReserve
 * @package TeamSoft\CrmRepositoryBundle\Repository
 */
class InfoRtcRoomReserve extends EntityRepository
{

    public function getByFilter(InfoRtcRoomReserveFilter $filter)
    {
        $qb = $this->createQueryBuilder('rtcRoomReserve')
            ->select('rtcRoomReserve')
            ->where('1=1');

        if ($filter->getRtcRoomId()) {
            $qb->andWhere($qb->expr()->in('rtcRoomReserve.rrtcroom', ':rtcroom_id'))
                ->setParameter('rtcroom_id', $filter->getRtcRoomId());
        }

        if ($filter->getResponsibleId()) {
            $qb->innerJoin('rtcRoomReserve.rtcRoom', 'rtcRoom')
                ->andWhere($qb->expr()->in('rtcRoom.responsible', ':responsible_id'))
                ->setParameter('responsible_id', $filter->getResponsibleId());
        }

        if ($filter->getContactId()) {
            $qb->andWhere($qb->expr()->in('rtcRoomReserve.contact', ':contact_id'))
                ->setParameter('contact_id', $filter->getContactId());
        }

        if ($filter->getDatefrom()) {
            $qb->andWhere($qb->expr()->gte('rtcRoomReserve.datetill', ':datefrom'))
                ->setParameter('datefrom', $filter->getDatefrom());
        }

        if ($filter->getDatetill()) {
            $qb->andWhere($qb->expr()->lte('rtcRoomReserve.datefrom', ':datetill'))
                ->setParameter('datetill', $filter->getDatetill());
        }

        if ($filter->getLimit()) {
            $qb->setMaxResults($filter->getLimit());
        }

        if ($filter->getOffset()) {
            $qb->setFirstResult($filter->getOffset());
        }

        if ($filter->getOrderBy() && count($filter->getOrderBy())) {
            $qb->orderBy(implode(', ', array_map(function ($orderByCase) {
                return 'rtcRoomReserve.' . $orderByCase;
            }, $filter->getOrderBy())));
        }

        return $qb->getQuery()->getResult();
    }
}
