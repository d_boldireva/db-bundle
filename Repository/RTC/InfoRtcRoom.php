<?php

namespace TeamSoft\CrmRepositoryBundle\Repository\RTC;


use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\EntityRepository;
use TeamSoft\CrmRepositoryBundle\Entity\RTC\InfoRtcRoomReserve;

/**
* @see \TeamSoft\CrmRepositoryBundle\Entity\RTC\InfoRtcRoom
* Class InfoRtcRoom
* @package TeamSoft\CrmRepositoryBundle\Repository
*/
class InfoRtcRoom extends EntityRepository {
    /**
     * Can room be reserved
     *
     * @param InfoRtcRoomReserve $RTCRoomReserve
     * @return bool
     */
    public function isReserveCallAvailable(InfoRtcRoomReserve $RTCRoomReserve){
        $qb = $this->createQueryBuilder('r');

        $qb->innerJoin('r.rtcRoomReserves','rtcRoomReserve')
            ->where($qb->expr()->between(':datefrom','rtcRoomReserve.datefrom','rtcRoomReserve.datetill'))
            ->orWhere($qb->expr()->between(':datetill','rtcRoomReserve.dateefrom','rtcRoomReserve.datetill'))
            ->setMaxResults(1);

        return !count($qb->getQuery()->getResult());
    }
}
