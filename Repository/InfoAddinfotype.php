<?php

namespace TeamSoft\CrmRepositoryBundle\Repository;

use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use TeamSoft\CrmRepositoryBundle\Entity\InfoSystemdictionary;
use TeamSoft\CrmRepositoryBundle\Service\DatabasePlatform;

class InfoAddinfotype extends ServiceEntityRepository
{
    const FILTER_STRING = 1;
    const FILTER_INT = 2;
    const FILTER_FLOAT = 3;
    const FILTER_DATETIME = 4;
    const FILTER_CUSTOMDICTIONARY = 5;
    const FILTER_SYSTEMDICTIONARY = 6;

    public function __construct(ManagerRegistry $registry, DatabasePlatform $dp)
    {
        parent::__construct($registry, \TeamSoft\CrmRepositoryBundle\Entity\InfoAddinfotype::class);
        $this->dp = $dp;
    }

    public function findByDataType($dataType, $roleId = null, $loadHidden = false)
    {
        $qb = $this->createQueryBuilder("ait");

        $qb
            ->where('ait.datatype = :datatype')
            ->setParameter('datatype', $dataType);

        if (!$loadHidden) {
            switch ($dataType) {
                case InfoRequired::COMPANY_PAGE:
                    $qb
                        ->leftJoin('ait.addInfoCompanyTypes', 'company_ait')
                        ->andWhere($qb->expr()->orX(
                            $qb->expr()->isNotNull('company_ait.subjtype'),
                            $qb->expr()->eq('ait.isdefault', 1)
                        ));
                    if ($roleId) {
                        $qb->andWhere($qb->expr()->orX(
                            $qb->expr()->isNull('company_ait.role'),
                            $qb->expr()->eq('company_ait.role', $roleId)
                        ));
                    }
                    break;
                case InfoRequired::CONTACT_PAGE:
                    $qb
                        ->leftJoin('ait.addInfoContactTypes', 'contact_ait')
                        ->andWhere($qb->expr()->orX(
                            $qb->expr()->isNotNull('contact_ait.subjtype'),
                            $qb->expr()->eq('ait.isdefault', 1)
                        ));
                    if ($roleId) {
                        $qb->andWhere($qb->expr()->orX(
                            $qb->expr()->isNull('contact_ait.role'),
                            $qb->expr()->eq('contact_ait.role', $roleId)
                        ));
                    }
                    break;
                case InfoRequired::TASK_PAGE:
                    $qb
                        ->leftJoin('ait.addInfoTaskTypes', 'task_ait')
                        ->andWhere($qb->expr()->orX(
                            $qb->expr()->isNotNull('task_ait.subjtype'),
                            $qb->expr()->eq('ait.isdefault', 1)
                        ));
                    if ($roleId) {
                        $qb->andWhere($qb->expr()->orX(
                            $qb->expr()->isNull('task_ait.role'),
                            $qb->expr()->eq('task_ait.role', $roleId)
                        ));
                    }
                    break;
                case InfoRequired::ACTION_PAGE:
                    $qb
                        ->leftJoin('ait.addInfoActionTypes', 'action_ait')
                        ->andWhere($qb->expr()->orX(
                            $qb->expr()->isNotNull('action_ait.subjtype'),
                            $qb->expr()->eq('ait.isdefault', 1)
                        ));
                    if ($roleId) {
                        $qb->andWhere($qb->expr()->orX(
                            $qb->expr()->isNull('action_ait.role'),
                            $qb->expr()->eq('action_ait.role', $roleId)
                        ));
                    }
                    break;
            }
        }

        $qb->orderBy('ISNULL(ait.xpos, 0), ISNULL(ait.ypos, 0), ait.code');

        return $qb->getQuery()->getResult();
    }

    public function findFieldValues($id)
    {
        $connection = $this->_em->getConnection();

        $filtersMetadata = [];

        $fields = $this->findBy(array('id' => $id));

        //TODO: remove this usage -> getDatabasePlatform()->getLengthExpression()
        $l = $this->dp->getLengthOperator();

        /**
         * @var \TeamSoft\CrmRepositoryBundle\Entity\InfoAddinfotype $field
         */
        foreach ($fields as $key => $field) {
            $metadata = array(
                'id' => $field->getId(),
                'name' => $field->getName(),
                'table' => 'addinfotype'
            );

            switch ($field->getFielddatatype()) {
                case self::FILTER_STRING:
                    $metadata['type'] = 'string';
                    $result = $connection->fetchAllAssociative("
                        SELECT id, stringvalue as name
                        FROM info_contactinfo as q
                        WHERE q.stringvalue IS NOT NULL AND $l(q.stringvalue) > 0 AND infotype_id = {$field->getId()}
                    ");

                    if (empty($result)) {
                        return false;
                    }

                    $metadata['data'] = $result;
                    break;
                case self::FILTER_INT:
                case self::FILTER_FLOAT:
                    $metadata['type'] = 'range';
                    $fieldName = ($field->getFielddatatype() === 2) ? 'intvalue' : 'floatvalue';
                    $result = $connection->fetchAssociative("
                          SELECT COALESCE(MIN(q.$fieldName), 0) min, COALESCE(MAX(q.$fieldName), 0) max
                          FROM info_contactinfo as q
                          WHERE q.$fieldName IS NOT NULL AND infotype_id = {$field->getId()}
                        ");

                    if (empty($result)) {
                        return false;
                    }

                    $metadata['data'] = $result;
                    break;
                case self::FILTER_DATETIME:
                    $metadata['type'] = 'dateRange';
                    $result = $connection->fetchAssociative("
                          SELECT MIN(q.datevalue) min, MAX(q.datevalue) max
                          FROM info_contactinfo as q
                          WHERE q.datevalue IS NOT NULL AND infotype_id = {$field->getId()}
                        ");

                    if (empty($result)) {
                        return false;
                    }

                    $metadata['data'] = $result;
                    break;
                    break;
                case self::FILTER_CUSTOMDICTIONARY:
                    $metadata['type'] = 'string';

                    $result = $connection->fetchAllAssociative("
                        SELECT q.id, dictionaryvalue as name
                        FROM info_customdictionaryvalue as q
                        JOIN info_addinfotype ON info_addinfotype.CustomDictionary_id = q.CustomDictionary_id
                        WHERE q.dictionaryvalue IS NOT NULL AND $l(q.dictionaryvalue) > 0
                        AND info_addinfotype.id = {$field->getId()}
                    ");

                    if (empty($result)) {
                        return false;
                    }

                    $metadata['data'] = $result;
                    break;
                case self::FILTER_SYSTEMDICTIONARY:
                    $metadata['type'] = 'string';
                    /**
                     * @var \TeamSoft\CrmRepositoryBundle\Entity\InfoSystemdictionary $systemDictionary
                     */
                    $systemDictionary = $this->_em
                        ->getRepository(InfoSystemdictionary::class)
                        ->find($field->getSystemDictionaryId());

                    if (!$systemDictionary) {
                        return false;
                    }

                    /**
                     * @var \Doctrine\DBAL\Connection $connection
                     */
                    $connection = $this->_em->getConnection();

                    $query = "SELECT id, name FROM {$systemDictionary->getTablename()}";

                    $result = $connection->fetchAllAssociative($query);

                    if (!$result) {
                        return false;
                    }

                    $metadata['data'] = $result;
                    break;
            }

            $filtersMetadata[] = $metadata;
        }

        return $filtersMetadata;
    }
}
