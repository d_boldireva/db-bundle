<?php

namespace TeamSoft\CrmRepositoryBundle\Repository\Expenses;

use Doctrine\ORM\EntityRepository;
use TeamSoft\CrmRepositoryBundle\Entity\Expenses\InfoExpense;

class InfoExpensedetail extends EntityRepository
{
    public function findExpenseDetailByExpenseIds($ids)
    {
        if (empty($ids)) {
            return null;
        }

        return $this->getEntityManager()->createQueryBuilder()
            ->select('ed.id, ed.amount, ed.vendorname, ed.comment, ed.managersComment as managers_comment, ' .
                'etype.dictionaryvalue, e.id as expense_id')
            ->from($this->_entityName, 'ed')
            ->innerJoin('ed.expenseType', 'etype')
            ->innerJoin('ed.expense', 'e')
            ->where('e.id in (' . implode(',', $ids) . ')')
            ->getQuery()
            ->getArrayResult();
    }

    public function getYearExpenses($year, $user_id, $line2status)
    {

        $endYear = $year + 1;

        $qb = $this->getEntityManager()->createQueryBuilder();
        $this->generateSelectByPeriod($qb);

        return $qb
            ->andWhere('e.dt >= :datefrom')->setParameter('datefrom', "$year-04-01 00:00:00")
            ->andWhere('e.dt <= :datetill')->setParameter('datetill', "$endYear-04-01 23:59:59")
            ->andWhere('e.user = :user_id')->setParameter('user_id', $user_id)
            ->andWhere('e.status = 1 OR (e.status = 2 AND e.line2status = 2)')
            ->andWhere('e.line2status = :line2status')->setParameter('line2status', $line2status)
            ->getQuery()
            ->getArrayResult();
    }

    public function getQyExepnses($quarter, $year, $user_id, $line2status)
    {
        $financialQuarters = [
            "1" => [
                'startMonth' => 1,
                'endMonth' => 3
            ],
            "2" => [
                'startMonth' => 4,
                'endMonth' => 6
            ],
            "3" => [
                'startMonth' => 7,
                'endMonth' => 9
            ],
            "4" => [
                'startMonth' => 10,
                'endMonth' => 12
            ]
        ];

        $startMonth = $financialQuarters[$quarter]['startMonth'];
        $endMonth = $financialQuarters[$quarter]['endMonth'];

        $dayOne = new \DateTime('now');

        $dayOne->setDate(intval($dayOne->format('Y')), $startMonth, 1);

        $dayLast = new \DateTime('now');
        $dayLast->setDate(intval($dayOne->format('Y')), $endMonth, 1);

        $qb = $this->getEntityManager()->createQueryBuilder();
        $this->generateSelectByPeriod($qb);
        return $qb
            ->andWhere('e.dt >= :datefrom')->setParameter('datefrom', $dayOne->format('Y-M-d 00:00:00'))
            ->andWhere('e.dt <= :datetill')->setParameter('datetill', $dayLast->format('Y-M-t 23:59:59'))
            ->andWhere('e.user = :user_id')->setParameter('user_id', $user_id)
            ->andWhere('e.status = 1 OR (e.status = 2 AND e.line2status = 2)')
            ->andWhere('e.line2status = :line2status')->setParameter('line2status', $line2status)
            ->getQuery()
            ->getArrayResult();

    }

    private function generateSelectByPeriod($qb)
    {
        $qb
            ->select('ed.amount as amount, IDENTITY(ed.expenseType) as expensetype_id')
            ->addSelect('e.id as expense_identifier, e.dt as dt, e.iscountrytrip as is_country_trip, e.id as id, COALESCE(e.line2status, 0) as line2status, IDENTITY(e.user) as user_id')
            ->from($this->_entityName, 'ed')
            ->innerJoin(InfoExpense::class, 'e', 'WITH', 'e = ed.expense');
    }

    public function getDictionariesValues()
    {
        return $this->getEntityManager()->createQueryBuilder()
            ->select('IDENTITY(ed.expenseType) as id')
            ->addSelect('etype.dictionaryvalue as expense_type')
            ->from($this->_entityName, 'ed')
            ->innerJoin('ed.expenseType', 'etype')
            ->getQuery()
            ->getArrayResult();
    }

    /**
     * @param array $filter
     * @param int $limit
     * @param int $offset
     * @param string[] $orderBy
     * @return \TeamSoft\CrmRepositoryBundle\Entity\Expenses\InfoExpensedetail[]
     */
    public function findByFilter($filter = [], $limit = 0, $offset = 0, $orderBy = [])
    {
        $qb = $this->createQueryBuilder('ed');
        $qb->select('ed')
            ->where('1=1');

        if (isset($filter['amountFrom']) && $filter['amountFrom']) {
            $qb->andWhere(
                $qb->expr()->gte('ed.amount', ':amountFrom')
            )->setParameter('amountFrom', $filter['amountFrom']);
        }

        if (isset($filter['amountTo']) && $filter['amountTo']) {
            $qb->andWhere(
                $qb->expr()->lte('ed.amount', ':amountTo')
            )->setParameter('amountTo', $filter['amountTo']);
        }

        if (isset($filter['vendorName']) && $filter['vendorName']) {
            $qb->andWhere(
                $qb->expr()->like('ed.vendorname', ':vendorName')
            )->setParameter('vendorName', "%{$filter['vendorName']}%");
        }

        if (isset($filter['comment']) && $filter['comment']) {
            $qb->andWhere(
                $qb->expr()->like('ed.comment', ':comment')
            )->setParameter('comment', "%{$filter['comment']}%");
        }

        if (isset($filter['managersComment']) && $filter['managersComment']) {
            $qb->andWhere(
                $qb->expr()->like('ed.managersComment', ':managersComment')
            )->setParameter('managersComment', "%{$filter['managersComment']}%");
        }

        if (isset($filter['expenses']) && $filter['expenses']) {
            $qb->andWhere(
                $qb->expr()->in('ed.expense', ':expenses')
            )->setParameter('expenses', $filter['expenses']);
        }

        if (isset($filter['expenseType']) && $filter['expenseType']) {
            $qb->innerJoin('ed.expenseType', 'et')
                ->andWhere('et.code = :expenseType')
                ->setParameter('expenseType', $filter['expenseType']);
        }

        if ($limit) {
            $qb->setMaxResults($limit);
        }

        if ($offset) {
            $qb->setFirstResult($offset);
        }

        if ($orderBy && count($orderBy)) {
            foreach ($orderBy as $orderByItem) {
                $order = explode(' ', $orderByItem);
                $qb->addOrderBy("ed.{$order[0]}", $order[1]);
            }
        }

        return $qb->getQuery()->getResult();
    }
}
