<?php

namespace TeamSoft\CrmRepositoryBundle\Repository\Expenses;

use Doctrine\ORM\EntityRepository;

class InfoExpensedetailfile extends EntityRepository{

    public function findByExpenseDetailId($id) {
        return $this->createQueryBuilder('efile')
//            ->select('efile.content', 'efile.filetype')
//            ->from($this->_entityName, 'efile')
            ->where('efile.expensedetail = :expensedetail_id')->setParameter('expensedetail_id', $id)
            ->orderBy('efile.id', 'DESC')
            ->setMaxResults(1)
            ->getQuery()
            ->getOneOrNullResult();
    }
}
