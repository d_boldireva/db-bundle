<?php

namespace TeamSoft\CrmRepositoryBundle\Repository\Expenses;

use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\QueryBuilder;
use Doctrine\Persistence\ManagerRegistry;
use TeamSoft\CrmRepositoryBundle\Entity\Expenses\InfoExpensedetail;
use TeamSoft\CrmRepositoryBundle\Entity\GridConfig\PoGridConfigByRole;
use TeamSoft\CrmRepositoryBundle\Entity\InfoUser;
use TeamSoft\CrmRepositoryBundle\Entity\InfoCustomdictionaryvalue;

//todo replace to doctrine sql
class InfoExpenseRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, \TeamSoft\CrmRepositoryBundle\Entity\Expenses\InfoExpense::class);
    }

    public function findExpenses($submissiveIds, $filters = [])
    {
        $query = $this->getEntityManager()->createQueryBuilder()
            ->addSelect('e.id AS id, COALESCE(e.status, 0) AS status, e.claimid AS claim_id, e.iscountrytrip AS is_country_trip, COALESCE(e.line2status, 0) as line2status, e.dt')
            ->addSelect('SUM(ed.amount) as amount')
            ->addSelect('u.name as name, u.id as user_id')
            ->from($this->_entityName, 'e')
            ->innerJoin('e.user', 'u')
            ->innerJoin(InfoExpensedetail::class, 'ed', 'WITH', 'e.id = ed.expense')
            ->where('u.id IN (:ids)')->setParameter('ids', $submissiveIds);

        $this->addExpensesFilters($query, $filters);

        return $query
            ->groupBy("e.id, e.dt, e.status, e.iscountrytrip, e.claimid, e.line2status")
            ->addGroupBy("u.name, u.id")
            ->getQuery()
            ->getArrayResult();
    }

    public function getLine2Expenses($submissiveIds, $filters)
    {

        $query = $this->getEntityManager()
            ->createQueryBuilder()
            ->select('e.id as expense_identifier, e.status, COALESCE(e.line2status, 0) AS line2status, e.dt as date, e.iscountrytrip AS is_country_trip')
            ->addSelect('ed.amount, IDENTITY(ed.expenseType) as expensetype_id')
            ->addSelect('u.id as user_id, u.name as name')
            ->from($this->_entityName, 'e')
            ->innerJoin('e.user', 'u')
            ->innerJoin(InfoExpensedetail::class, 'ed', 'WITH', 'e.id = ed.expense')
            ->where('u.id IN (:ids)')->setParameter('ids', $submissiveIds)
            ->andWhere('e.status = 1 AND e.line2status != 2');

        $this->addExpensesFilters($query, $filters);
        return $query->getQuery()->getArrayResult();
    }

    /**
     * @param $query \Doctrine\ORM\QueryBuilder
     * @param $filters
     * @throws \Exception
     */
    private function addExpensesFilters($query, $filters)
    {
        foreach ($filters as $filterName => $filter) {
            if ($filterName === 'dateStart') {
                $query->andWhere('e.dt >= :dateStart')->setParameter('dateStart', (new \DateTime($filter))->format('Y-m-d 00:00:00'));
                continue;
            }

            if ($filterName === 'dateEnd') {
                $query->andWhere('e.dt <= :dateEnd')->setParameter('dateEnd', (new \DateTime($filter))->format('Y-m-d 23:59:59'));
                continue;
            }

            if ($filterName === 'name') {
                $query->andWhere('u.name like :userName')->setParameter('userName', "%$filter%");
                continue;
            }

            if ($filterName === 'line2statusNotNull') {
                $query->andWhere('e.line2status IS NOT NULL');
                continue;
            }

            if ($filterName === 'claimId') {
                $query->andWhere("e.claimid  like :$filterName")->setParameter($filterName, "%$filter%");
                continue;
            }

            if ($filterName === 'line1Status') {
                $query->andWhere('e.line1status = :line1status')
                    ->setParameter('line1status', $filter);
                continue;
            }

            if ($filterName === 'line2Status') {
                $query->andWhere('e.line2status = :line2status')
                    ->setParameter('line2status', $filter);
                continue;
            }

            $query->andWhere("e.$filterName  like :$filterName")->setParameter($filterName, "%$filter%");
        }
    }

    /**
     * On create new expense generate next claimId Unique for user
     * @param InfoUser $user
     * @return int
     * @throws \Doctrine\ORM\NoResultException
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function generateNextClaimId(InfoUser $user): int
    {
        $claimid = $this->createQueryBuilder('e')
            ->select('e.claimid as id')
            ->where('e.user = :user')
            ->setParameter('user', $user)
            ->orderBy('e.id', 'DESC')
            ->setMaxResults(1)
            ->getQuery()
            ->getScalarResult();

        if (isset($claimid[0]['id'])) {
            $next = substr($claimid[0]['id'], 0, 0 - strlen($user->getId())) + 1;
        } else {
            $next = 1;
        }

        return (int)($next . $user->getId());
    }

    /**
     * @param array $filters
     * @param int $limit
     * @param int $offset
     * @param string[] $orderBy
     * @return \TeamSoft\CrmRepositoryBundle\Entity\Expenses\InfoExpense[]
     */
    public function findLine1Expenses($filters = [], $limit = 0, $offset = 0, $orderBy = [])
    {
        $qb = $this->getExpensesCommonQuery($filters, $limit, $offset, $orderBy);
        $qb->addSelect("SUM(d.amount) as total_amount")
            ->innerJoin('e.details', 'd');

        $qb->andWhere(
            $qb->expr()->eq("ISNULL(e.status, 0)", ":line1status")
        )->setParameter("line1status", \TeamSoft\CrmRepositoryBundle\Entity\Expenses\InfoExpense::STATUS_APPROVED);

        $qb->andWhere('ISNULL(e.linePMstatus, 0) = 0');
        $qb->groupBy('e.id');

        return $qb->getQuery()->getResult();
    }

    /**
     * @param array $filters
     * @param int $limit
     * @param int $offset
     * @param string[] $orderBy
     * @return \TeamSoft\CrmRepositoryBundle\Entity\Expenses\InfoExpense[]
     */
    public function findLine2Expenses($filters = [], $limit = 0, $offset = 0, $orderBy = [])
    {
        $qb = $this->getExpensesCommonQuery($filters, $limit, $offset, $orderBy);
        $qb->addSelect("SUM(d.amount) as total_amount")
            ->innerJoin('e.details', 'd');

        if (!(isset($filters['type']) && $filters['type'])) {
            $qb->innerJoin('d.expenseType', 'et');
        }

        //is line_1 approved
        $qb->andWhere("ISNULL(e.status, 0) = :line1status")
            ->setParameter("line1status", '' . \TeamSoft\CrmRepositoryBundle\Entity\Expenses\InfoExpense::STATUS_APPROVED);

        /*        $qb->andWhere($qb->expr()->orX(         //is
                    "e.linePMstatus = :linePMstatus",   //line_pm approved
                    "et.code <> :linePMcode"            //or not activity_budget
                ))
                    ->setParameter('linePMstatus', '' . \TeamSoft\CrmRepositoryBundle\Entity\Expenses\InfoExpense::STATUS_APPROVED)
                    ->setParameter('linePMcode', '' . InfoExpensedetail::ACTIVITIES_BUDGET);*/

        //is line_2 open
        $qb->andWhere('ISNULL(e.linePMstatus, 0) = :linePMstatus')
            ->setParameter('linePMstatus', '' . \TeamSoft\CrmRepositoryBundle\Entity\Expenses\InfoExpense::STATUS_APPROVED);
        $qb->andWhere("ISNULL(e.line2status,0) = :line2status")
            ->setParameter("line2status", '' . \TeamSoft\CrmRepositoryBundle\Entity\Expenses\InfoExpense::STATUS_EDITABLE);
        $qb->groupBy('e.id');

        return $qb->getQuery()->getResult();
    }

    /**
     * @param array $filters
     * @param int $limit
     * @param int $offset
     * @param string[] $orderBy
     * @return \TeamSoft\CrmRepositoryBundle\Entity\Expenses\InfoExpense[]
     */
    public function findLinePMExpenses($filters = [], $limit = 0, $offset = 0, $orderBy = [])
    {
        $filters["type"] = InfoCustomdictionaryvalue::CODE_ACTIVITIES_BUDGET;

        $qb = $this->getExpensesCommonQuery($filters, $limit, $offset, $orderBy);
        $qb->addSelect("SUM(d.amount) as total_amount")
            ->innerJoin('e.details', 'd');

        $qb->andWhere("e.status = :line1status")
            ->setParameter("line1status", '' . \TeamSoft\CrmRepositoryBundle\Entity\Expenses\InfoExpense::STATUS_APPROVED);

        $qb->andWhere("ISNULL(e.linePMstatus,0) = :linePMstatus")
            ->setParameter("linePMstatus", '' . \TeamSoft\CrmRepositoryBundle\Entity\Expenses\InfoExpense::STATUS_EDITABLE);
        $qb->groupBy('e.id');

        return $qb->getQuery()->getResult();
    }

    /**
     * @param array $filter
     * @param int $limit
     * @param int $offset
     * @param string[] $orderBy
     * @return QueryBuilder
     */
    private function getExpensesCommonQuery($filter = [], $limit = 0, $offset = 0, $orderBy = []): QueryBuilder
    {
        /** @var QueryBuilder $qb */
        $qb = $this->createQueryBuilder('e')
            ->select('e')
            ->where('1=1');

        if (isset($filter['users']) && count($filter['users'])) {
            $qb->andWhere(
                $qb->expr()->in('e.user', ':users')
            )->setParameter('users', $filter['users']);
        }

        if (isset($filter['brands']) && $filter['brands']) {
            $qb->innerJoin('e.task', 'task')
                ->innerJoin('task.promoProjectCollection', 'pp')
                ->innerJoin('pp.brandCollection', 'ppb')
                ->innerJoin('ppb.brend', 'b')
                ->andWhere($qb->expr()->in('b', ':brands'))
                ->setParameter('brands', $filter['brands']);
        }

        if (isset($filter['dateFrom']) && $filter['dateFrom']) {
            $qb->andWhere(
                $qb->expr()->gte('e.dt', ':dateFrom')
            )->setParameter('dateFrom', (new \DateTime($filter['dateFrom']))->format('Y-m-d 00:00:00'));
        }

        if (isset($filter['dateTill']) && $filter['dateTill']) {
            $qb->andWhere(
                $qb->expr()->lte('e.dt', ':dateTill')
            )->setParameter('dateTill', (new \DateTime($filter['dateTill']))->format('Y-m-d 23:59:59'));
        }

        if (isset($filter['type']) && $filter['type']) {
            $qb
                ->innerJoin('e.expenseType', 'et')
                ->andWhere(
                    $qb->expr()->in('et.code', ':type')
                )->setParameter("type", $filter['type']);
        }

        if (isset($filter["claim"]) && $filter["claim"]) {
            $qb->andWhere(
                $qb->expr()->eq("e.claimid", ":claim")
            )->setParameter("claim", $filter["claim"]);
        }

        if ($offset) {
            $qb->setFirstResult($offset);
        }

        if ($limit) {
            $qb->setMaxResults($limit);
        }

        if ($orderBy && count($orderBy)) {
            foreach ($orderBy as $orderByItem) {
                $order = explode(' ', $orderByItem);
                $qb->addOrderBy("e.{$order[0]}", $order[1]);
            }
        }

        return $qb;
    }
}
