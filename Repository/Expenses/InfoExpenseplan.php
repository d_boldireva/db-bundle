<?php

namespace TeamSoft\CrmRepositoryBundle\Repository\Expenses;

use Doctrine\ORM\EntityRepository;


class InfoExpenseplan extends EntityRepository
{
    public function getUserBudget($userId, $financialYear){
        return $this->getEntityManager()->createQueryBuilder()
            ->select('eplan.budget')
            ->from($this->_entityName, 'eplan')
            ->where('eplan.userId = :user_id')->setParameter('user_id', $userId)
            ->andWhere('eplan.fy = :financialYear')->setParameter('financialYear', $financialYear)
            ->setMaxResults(1)
            ->getQuery()
            ->getOneOrNullResult();
    }
}