<?php
declare(strict_types=1);


namespace TeamSoft\CrmRepositoryBundle\Repository\Expenses;


use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use TeamSoft\CrmRepositoryBundle\Entity\Expenses\InfoExpensehcp;
use TeamSoft\CrmRepositoryBundle\Entity\InfoContact;
use TeamSoft\CrmRepositoryBundle\Entity\Expenses\InfoExpensedetail as InfoExpensedetailEntity;

class InfoExpensehcpRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, InfoExpensehcp::class);
    }

    public function getExpenseHcpContactFullNames($detailIds = []) {
        $queryBuilder =  $this->createQueryBuilder('hcp');

        $queryBuilder->select("IDENTITY(d.expense) as expense_id, d.id as expensedetail_id, 
            TRIM(CONCAT(
                COALESCE(CONCAT(c.middlename, ' '), ''),
                COALESCE(CONCAT(c.lastname, ' '), ''),
                COALESCE(c.firstname, '')
            )) as full_name")
            ->innerJoin(InfoExpensedetailEntity::class, 'd', 'WITH', 'd.id = hcp.expenseDetail')
            ->innerJoin(InfoContact::class, 'c', 'WITH', 'c.id = hcp.contact')
            ->where($queryBuilder->expr()->in('d.id', ':details'))
            ->setParameter('details', $detailIds);

        return $queryBuilder->getQuery()->getArrayResult();
    }
}