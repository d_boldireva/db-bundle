<?php
declare(strict_types=1);

namespace TeamSoft\CrmRepositoryBundle\Repository;

use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use TeamSoft\CrmRepositoryBundle\Service\DatabasePlatform;
use TeamSoft\CrmRepositoryBundle\Entity\InfoContactsign;

class InfoContactsignRepository extends ServiceEntityRepository
{
    private $dp;

    public function __construct(ManagerRegistry $registry, DatabasePlatform $dp)
    {
        parent::__construct($registry, InfoContactsign::class);
        $this->dp = $dp;
    }

    /**
     * @param array $filiterBy
     * @return array[]
     * @throws \Doctrine\DBAL\Exception
     */
    public function getContactSignsIdWithDictionaryRemoteNameByFilter(array $filiterBy = [])
    {
        $tableName = 'info_contactsign';
        $whereParams = [];
        $whereClause = '';
        $subWhereClause = '';

        $c = $this->dp->getConcatOperator();

        if ($filiterBy['contactId']) {
            $whereParams[] = "$tableName.contact_id = {$filiterBy['contactId']}";
        }

        if ($filiterBy['dictionaryType']) {
            $typeCaption = 'caption_' . $filiterBy['dictionaryType'] . '_';
            $subWhereParams[] = "idr2.type {$this->dp->getLikeExpression()} '%{$typeCaption}' $c right(idr.type, 1) $c '%'";
        }

        if (count($whereParams) > 0) {
            $whereClause = ' WHERE ' . implode(' AND ', $whereParams);
        }

        if (count($subWhereParams) > 0) {
            $subWhereClause = ' WHERE ' . implode(' AND ', $subWhereParams);
        }

        $sql = "
            SELECT $tableName.id, (SELECT idr2.name from info_dictionaryremote idr2 {$subWhereClause}) as name, icf.id as file_id
            FROM $tableName 
            INNER JOIN info_dictionaryremote idr ON $tableName.agreement_id = idr.id
            INNER JOIN info_contactsignfile icf on $tableName.id = icf.contactsign_id
            {$whereClause}
        ";

        return $this->_em->getConnection()->fetchAllAssociative($sql);
    }

    public function findByFilter($filter = []) {
        $qb = $this->createQueryBuilder('cs');

        $this->applyFilterToQueryBuilder($qb, $filter);

        return $qb->getQuery()
            ->getResult();
    }

    private function applyFilterToQueryBuilder($query, $filter = []) {
        if(isset($filter['contactId'])) {
            $query->andWhere('cs.contact = :contactId')
                ->setParameter('contactId', $filter['contactId']);
        }

        if(isset($filter['agreementId'])) {
            if($filter['agreementId'] == 'nullable') {
                $query->andWhere("{$query->expr()->isNull('cs.agreement')}");
            }
        }

        if(isset($filter['isArchive'])) {
            if($filter['isArchive'] == 'nullable') {
                $query->andWhere("{$query->expr()->isNull('cs.isArchive')}");
            }
        }

        if(isset($filter['signTypeNullable']) && isset($filter['signType'])) {
            $query->andWhere("cs.signType = :signType OR {$query->expr()->isNull('cs.signType')}");
            $query->setParameter('signType', $filter['signType']);
        } else if(isset($filter['signType'])) {
            $query->andWhere("cs.signType = :signType");
            $query->setParameter('signType', $filter['signType']);
        }
    }
}
