<?php

namespace TeamSoft\CrmRepositoryBundle\Repository;

use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use TeamSoft\CrmRepositoryBundle\Service\DatabasePlatform;
use TeamSoft\CrmRepositoryBundle\Entity\InfoCity as InfoCityEntity;
use Doctrine\ORM\Query\ResultSetMappingBuilder;

class InfoCity extends ServiceEntityRepository
{
    private $dp;

    public function __construct(ManagerRegistry $registry, DatabasePlatform $dp)
    {
        parent::__construct($registry, InfoCityEntity::class);
        $this->dp = $dp;
    }

    public function findBySearch(array $filter)
    {
        $tableName = $this->getClassMetadata()->getTableName();

        $whereParts = [];

        $params = [];

        $concatCityAndRegion = '';

        if ($this->dp->isPostgreSQL100()) {
            $concatCityAndRegion = "concat($tableName.name, ', ', info_region.name)";
        } else if ($this->dp->isSQLServer2008()) {
            $concatCityAndRegion = "$tableName.name + ', ' + info_region.name";
        }

        if (isset($filter['search']) && $filter['search']) {
            $whereParts[] = "$concatCityAndRegion {$this->dp->getLikeExpression()} :search";
            $params['search'] = $filter['search'] . '%';
        }

        $where = $whereParts ? 'WHERE ' . implode(' AND ', $whereParts) : '';

        $sql = "
            SELECT MIN($tableName.id) AS id, $concatCityAndRegion AS name
            FROM $tableName
            LEFT JOIN info_region ON info_region.id = $tableName.region_id
            $where
            GROUP BY $tableName.name, info_region.name
            ORDER BY $tableName.name
        ";

        $sql = $this->_em->getConnection()->getDatabasePlatform()->modifyLimitQuery($sql, $filter['limit'], $filter['offset']);

        $stmt = $this
            ->getEntityManager()
            ->getConnection()
            ->prepare($sql);

        $result = $stmt->executeQuery($params)->fetchAllAssociative();

        $totalCountSql = " 
            SELECT COUNT($tableName.id)
            FROM $tableName
            LEFT JOIN info_region ON info_region.id = $tableName.region_id
            $where
        ";

        $totalCountQuery = $this
            ->getEntityManager()
            ->getConnection()
            ->prepare($totalCountSql);

        return array(
            "total_count" => (int) $totalCountQuery->executeQuery($params)->fetchOne(),
            "data" => $result,
        );
    }

    public function findByFilter(array $filter)
    {
        $tableName = $this->getClassMetadata()->getTableName();

        $whereParts = array();

        if (isset($filter['search']) && $filter['search']) {
            $whereParts[] = "COALESCE(info_city.name, '') " . $this->dp->getLikeExpression() . " '%" . $filter['search'] . "%'";
        }

        if (isset($filter['regionId']) && $filter['regionId']) {
            $whereParts[] = "info_city.region_id = " . $filter['regionId'];
        }

        $rsm = new ResultSetMappingBuilder($this->_em);
        $rsm->addRootEntityFromClassMetadata(
            $this->_entityName,
            $tableName,
            array(),
            ResultSetMappingBuilder::COLUMN_RENAMING_INCREMENT
        );

        $selectClause = $rsm->generateSelectClause();
        $where = $whereParts ? 'WHERE ' . implode(' AND ', $whereParts) : '';
        $sql = "
            SELECT $selectClause FROM $tableName
            $where
            ORDER BY $tableName.name
        ";
        $sql = $this->_em->getConnection()->getDatabasePlatform()->modifyLimitQuery($sql, $filter['limit'], $filter['offset']);
        $query = $this->getEntityManager()->createNativeQuery($sql, $rsm);

        return $query->getResult();
    }

    public function findByFilterWithUniqueLevel(array $filterBy, array $orderBy = null, $limit = null, $offset = null)
    {
        $top = '';
        if (isset($limit) && $limit > 0) {
            if (isset($offset)) {
                $top = 'TOP ' . ($offset + $limit);
            } else {
                $top = 'TOP ' . $offset;
            }
        }

        $whereParts = array();

        if (isset($filterBy['search']) && $filterBy['search']) {
            $whereParts[] = "COALESCE(c.name, '') LIKE '%" . $filterBy['search'] . "%'";
        }

        if (isset($filterBy['regionId']) && $filterBy['regionId']) {
            $whereParts[] = "c.region_id = " . $filterBy['regionId'];
        }

        $rsm = new ResultSetMappingBuilder($this->_em);
        $rsm->addRootEntityFromClassMetadata($this->_entityName, 'c', array(), ResultSetMappingBuilder::COLUMN_RENAMING_INCREMENT);
        $rsm->addEntityResult($this->_entityName, 'c', 'city');
        $rsm->addScalarResult('unique_level', 'unique_level', 'integer');

        $selectClause = $rsm->generateSelectClause();
        $where = $whereParts ? 'WHERE ' . implode(' AND ', $whereParts) : '';
        $sql = "
            SELECT $top $selectClause,
            CASE 
            WHEN (SELECT COUNT(1) FROM info_city WHERE c.name = info_city.name AND c.region_id = info_city.region_id AND c.citytype_id = info_city.citytype_id) > 1 THEN 2
            WHEN (SELECT COUNT(1) FROM info_city WHERE c.name = info_city.name AND c.region_id = info_city.region_id) > 1 THEN 1
            ELSE 0 END unique_level            
            FROM info_city AS c            
            $where
            AND c.id IN (SELECT MAX(id) FROM info_city GROUP BY name, region_id, citytype_id) 
            ORDER BY c.name
        ";

        $query = $this->getEntityManager()->createNativeQuery($sql, $rsm);

        $result = $query->getResult();
        if (isset($filter['offset']) && is_numeric($filter['offset'])) {
            $result = array_slice($result, $filter['offset']);
        }

        return $result;
    }

    /**
     * @param array $filter
     * @param array $orderBy
     * @param int $page
     * @param int $limit
     * @param bool $filter_existing
     * @return mixed
     */
    public function findWithReportsByFilter($filter = [], $orderBy = [], $offset = null, $limit = null, $filter_existing = false)
    {
        $this->findAll();
        $this->_em->getRepository(\TeamSoft\CrmRepositoryBundle\Entity\InfoRegion::class)->findAll();

        $queryBuilder = $this->createQueryBuilder('city');

        $queryBuilder
            ->innerJoin('city.region', 'region')
            ->distinct(true)
            ->select('partial city.{id,name}, partial region.{id}');

        if ($offset) {
            $queryBuilder->setFirstResult($offset);
        }

        if ($limit) {
            $queryBuilder->setMaxResults($limit);
        }

        foreach ($orderBy as $attribute => $direction) {
            $queryBuilder->addOrderBy("city.{$attribute}", $direction);
        }

        if ($responsible = $filter['responsible'] ?? null) {
            $subqueryBuilder = $this->_em->getRepository(\TeamSoft\CrmRepositoryBundle\Entity\InfoUser::class)
                ->createQueryBuilder('user');
            $subqueryBuilder
                ->innerJoin('user.regionCollection', 'user_region')
                ->andWhere($subqueryBuilder->expr()->in('user', ':responsible'))
                ->andWhere($subqueryBuilder->expr()->eq('user_region', 'city.region'))
                ->setParameter(':responsible', $responsible);
        }

        if ($regionpart = $filter['regionpart'] ?? null) {
            $queryBuilder
                ->innerJoin('region.regionparts', 'regionpart')
                ->andWhere($queryBuilder->expr()->in('regionpart', ':regionpart'))
                ->setParameter(':regionpart', $regionpart);
        }

        if ($region = $filter['region'] ?? null) {
            $queryBuilder
                ->andWhere($queryBuilder->expr()->in('city.region', ':region'))
                ->setParameter(':region', $region);
        }

        if ($city = $filter['city'] ?? null) {
            $queryBuilder
                ->andWhere($queryBuilder->expr()->in('city', ':city'))
                ->setParameter(':city', $city);
        }

        if (!$filter_existing) {
            if ($company = $filter['company'] ?? null) {
                $queryBuilder
                    ->innerJoin(\TeamSoft\CrmRepositoryBundle\Entity\InfoCompany::class, 'company', 'WITH', 'company.city = city')
                    ->andWhere($queryBuilder->expr()->in('company', ':company'))
                    ->setParameter(':company', $company);
            }

            if ($main = $filter['main'] ?? null) {
                $queryBuilder
                    ->innerJoin(\TeamSoft\CrmRepositoryBundle\Entity\InfoCompany::class, 'company', 'WITH', 'company.city = city')
                    ->andWhere($queryBuilder->expr()->in('company.main', ':main'))
                    ->setParameter(':main', $main);
            }

            if ($net = $filter['net'] ?? null) {
                $queryBuilder
                    ->innerJoin(\TeamSoft\CrmRepositoryBundle\Entity\InfoCompany::class, 'company', 'WITH', 'company.city = city')
                    ->andWhere($queryBuilder->expr()->in('company.centerId', ':net'))
                    ->setParameter(':net', $net);
            }

            if ($contact = $filter['contact'] ?? null) {
                $queryBuilder
                    ->innerJoin(\TeamSoft\CrmRepositoryBundle\Entity\InfoContact::class, 'contact', 'WITH', 'contact.city = city')
                    ->andWhere($queryBuilder->expr()->in('contact', ':contact'))
                    ->setParameter(':contact', $contact);
            }

            if ($specialization = $filter['specialization'] ?? null) {
                $queryBuilder
                    ->innerJoin(\TeamSoft\CrmRepositoryBundle\Entity\InfoContact::class, 'contact', 'WITH', 'contact.city = city')
                    ->andWhere($queryBuilder->expr()->in('contact.specialization', ':spec'))
                    ->setParameter(':spec', $specialization);
            }
        } else {
            $subqueryBuilder = $this->_em->getRepository(\TeamSoft\CrmRepositoryBundle\Entity\InfoTask::class)->createQueryBuilder($reportAlias = 'report');
            $subqueryBuilder
                ->setMaxResults(1)
                ->innerJoin("{$reportAlias}.taskFileGroupCollection", $tfgAlias = 'tfg')
                ->innerJoin("{$tfgAlias}.taskImageCollection", $tfiAlias = 'tfi')
                ->leftJoin("{$reportAlias}.company", "company")
                ->leftJoin("{$reportAlias}.contact", "contact", 'WITH',
                    $queryBuilder->expr()->isNull('contact')
                )
                ->andWhere(
                    $subqueryBuilder->expr()->orX()->addMultiple([
                        $subqueryBuilder->expr()->eq('company.city', 'city'),
                        $subqueryBuilder->expr()->eq('contact.city', 'city')
                    ])
                );

            if ($net = $filter['net'] ?? null) {
                $subqueryBuilder
                    ->andWhere($subqueryBuilder->expr()->in('company.centerId', ':net'))
                    ->setParameter(':net', $net);
            }

            if ($main = $filter['main'] ?? null) {
                $subqueryBuilder
                    ->andWhere($subqueryBuilder->expr()->in('company.main', ':main'))
                    ->setParameter(':main', $main);
            }

            if ($responsible = $filter['responsible'] ?? null) {
                $subqueryBuilder
                    ->andWhere($subqueryBuilder->expr()->in("{$reportAlias}.responsible", ':responsible'))
                    ->setParameter(':responsible', $responsible);
            }

            if ($tasktype = $filter['tasktype'] ?? null) {
                $subqueryBuilder
                    ->andWhere($subqueryBuilder->expr()->in("{$reportAlias}.tasktype", ':tasktype'))
                    ->setParameter(':tasktype', $tasktype);
            }

            if ($taskstate = $filter['taskstate'] ?? null) {
                $subqueryBuilder
                    ->andWhere($subqueryBuilder->expr()->in("{$reportAlias}.taskstate", ':taskstate'))
                    ->setParameter(':taskstate', $taskstate);
            }

            if ($phototype = $filter['phototype'] ?? null) {
                $phototype = array_map(function (\TeamSoft\CrmRepositoryBundle\Entity\InfoCustomdictionaryvalue $customdictionaryvalue) {
                    return $customdictionaryvalue->getName();
                }, $phototype);

                $subqueryBuilder
                    ->andWhere($subqueryBuilder->expr()->in("{$tfgAlias}.name", ':phototype'))
                    ->setParameter(':phototype', $phototype);
            }

            if ($phototostate = $filter['photostate'] ?? null) {
                $subqueryBuilder
                    ->andWhere($subqueryBuilder->expr()->in("{$tfiAlias}.photostate", ':photostate'))
                    ->setParameter(':phototostate', $phototostate);
            }

            if ($photoservicetype = $filter['photoservicetype'] ?? null) {
                $subqueryBuilder
                    ->andWhere($subqueryBuilder->expr()->in("{$tfiAlias}.photoservicetype", ':photoservicetype'))
                    ->setParameter(':photoservicetype', $photoservicetype);
            }

            if ($whatsinside = $filter['whatsinside'] ?? null) {
                $subqueryBuilder
                    ->andWhere($subqueryBuilder->expr()->in("{$tfiAlias}.whatsinside", ':whatsinside'))
                    ->setParameter(':whatsinside', $whatsinside);
            }

            if ($brand = $filter['brand'] ?? null) {
                $subqueryBuilder
                    ->andWhere($subqueryBuilder->expr()->in("{$tfiAlias}.brand", ':brand'))
                    ->setParameter(':brand', $brand);
            }

            if ($specialization = $filter['specialization'] ?? null) {
                $subqueryBuilder
                    ->innerJoin("{$reportAlias}.contact", 'contact')
                    ->andWhere($subqueryBuilder->expr()->in('contact.specialization', ':specialization'))
                    ->setParameter(':specialization', $specialization);
            }

            $queryBuilder
                ->andWhere($queryBuilder->expr()->exists($subqueryBuilder));
        }

        return $queryBuilder->getQuery()->getResult();
    }
}
