<?php

namespace TeamSoft\CrmRepositoryBundle\Repository;

use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\Query\ResultSetMappingBuilder;
use Doctrine\Persistence\ManagerRegistry;
use TeamSoft\CrmRepositoryBundle\Service\DatabasePlatform;

class InfoPlanpromo extends ServiceEntityRepository
{
    use PlanContentTrait;

    /**
     * @var DatabasePlatform
     */
    private $dp;

    public function __construct(ManagerRegistry $registry, DatabasePlatform $dp)
    {
        parent::__construct($registry, \TeamSoft\CrmRepositoryBundle\Entity\InfoPlanpromo::class);
        $this->dp = $dp;
    }

    /**
     * @param array $filter
     * @return int|mixed|string
     */
    public function findByFilter(array $filter, bool $usePlanTaskPeriod = false)
    {
        $tableName = $this->getClassMetadata()->getTableName();

        $rsm = new ResultSetMappingBuilder($this->_em);
        $rsm->addRootEntityFromClassMetadata($this->_entityName, $tableName, array(), ResultSetMappingBuilder::COLUMN_RENAMING_INCREMENT);

        $selectClause = $rsm->generateSelectClause();
        $planId = (isset($filter["planId"]) && is_numeric($filter["planId"])) ? $filter["planId"] : 0;
        $tasktypeId = (isset($filter["tasktypeId"]) && is_numeric($filter["tasktypeId"])) ? $filter["tasktypeId"] : 0;
        $companyId = (isset($filter["companyId"]) && is_numeric($filter["companyId"])) ? $filter["companyId"] : 0;
        $contactId = (isset($filter["contactId"]) && is_numeric($filter["contactId"])) ? $filter["contactId"] : 0;
        $specializationId = (isset($filter["specializationId"]) && is_numeric($filter["specializationId"])) ? $filter["specializationId"] : 0;
        $isNoContactIsNull = $this->dp->getIsNullFunctionExpression('info_target.isnocontact', 0);
        $datefrom = isset($filter["datefrom"]) ? $filter["datefrom"] : null;
        if($usePlanTaskPeriod) {
            $periodWhere = "and ntask is null or ntask in (select num from info_plantaskperiod ptp where ptp.plandetail_id = info_plandetail.id and '$datefrom' between ptp.dt1 and ptp.dt2)";
        } else {
            $periodWhere = "";
        }

        $sql = "
            SELECT $selectClause
            FROM $tableName
            inner join info_plandetail on info_plandetail.id = info_planpromo.plandetail_id and info_plandetail.plan_id = $planId and info_plandetail.tasktype_id = $tasktypeId
			inner join info_target on info_plandetail.target_id = info_target.id
			left join info_targetspec on info_plandetail.target_id = info_targetspec.target_id
			where  (info_targetspec.spec_id = $specializationId or $isNoContactIsNull = 1)
			and (info_plandetail.category_id is null or info_plandetail.category_id = (select category_id from info_contact where info_contact.id = $contactId))
			and (info_plandetail.categorycompany_id is null or info_plandetail.categorycompany_id = (select category_id from info_company where info_company.id = $companyId))
			$periodWhere
        ";

        $query = $this->getEntityManager()->createNativeQuery($sql, $rsm);
        $result = $query->getResult();

        return $result;
    }
}
