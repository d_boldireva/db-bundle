<?php
declare(strict_types=1);

namespace TeamSoft\CrmRepositoryBundle\Repository;

use Doctrine\ORM\EntityRepository;

class InfoTaskpresentation extends EntityRepository
{

    /**
     * @param int $id
     * @return int|mixed|string
     * @throws \Doctrine\ORM\NoResultException
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function getCountByClmId(int $id)
    {
        return $this->createQueryBuilder('tp')
            ->select('count(tp.id)')
            ->where('tp.task IS NOT NULL')
            ->andWhere('tp.presentation = ' . $id)
            ->getQuery()
            ->getSingleScalarResult();
    }

} 