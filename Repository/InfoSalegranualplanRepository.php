<?php
declare(strict_types=1);

namespace TeamSoft\CrmRepositoryBundle\Repository;

use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\Query\ResultSetMappingBuilder;
use Doctrine\Persistence\ManagerRegistry;
use TeamSoft\CrmRepositoryBundle\Entity\InfoSalegranualplan;
use TeamSoft\CrmRepositoryBundle\Service\DatabasePlatform;
use TeamSoft\CrmRepositoryBundle\Service\Options;

class InfoSalegranualplanRepository extends ServiceEntityRepository
{
    private $options;
    private $tableName;
    private $dp;

    public function __construct(ManagerRegistry $registry, Options $options, DatabasePlatform $dp)
    {
        parent::__construct($registry, InfoSalegranualplan::class);
        $this->options = $options;
        $this->dp = $dp;
        $this->tableName = $this->getClassMetadata()->getTableName();
    }

    public function fetchPotentialByFilter(array $filter = [])
    {
        $connection = $this->getEntityManager()->getConnection();

        $whereClause = [];
        $innerClause = [];

        if (isset($filter['company_id'])) {
            $ids = implode(', ', $filter['company_id']);
            $whereClause[] = "is2.company_id IN({$ids})";
        }
        if (isset($filter['date'])) {
            $whereClause[] = "'{$filter['date']}' between sp.datefrom and sp.datetill";
        }
        if (isset($filter['direction_id']) && !empty($filter['direction_id'])) {
            $directionIds = implode(', ', $filter['direction_id']);
            $innerClause[] = "inner join info_granualplan gp on ip.id = gp.prep_id";
            $innerClause[] = "inner join info_preparationdirection pd on pd.preparation_id = ip.id and gp.direction_id = pd.direction_id";

            $whereClause[] = "pd.direction_id IN ({$directionIds})";
            $whereClause[] = "'{$filter['date']}' between gp.datefrom and gp.datetill";

        }
        $inner = $innerClause ? implode(' ', $innerClause) : "";
        $where = $whereClause ? "WHERE " . implode(' AND ', $whereClause) : "";
        $sql = "
            select 
                   is2.company_id as company_id, 
                   is2.preparation_id as preparation_id,
                   SUM(coalesce(icp.potential, icp2.potential)) as potential 
            from info_salegranualplan is2 
            inner join info_saleperiod sp on sp.id = is2.saleperiod_id 
            inner join info_preparation ip on ip.id = is2.preparation_id 
            {$inner}
            left join info_companypotential icp on icp.company_id  = is2.company_id 
                    and icp.prep_id = is2.preparation_id 
                    and sp.datefrom between icp.\"date\" and icp.date2 
            left join info_companypotential icp2 on icp2.company_id  = is2.company_id 
                    and icp2.brend_id = ip.brend_id 
                    and sp.datefrom between icp2.\"date\" and icp2.date2 
            {$where}
            group by is2.company_id, is2.preparation_id
        ";

        return $connection->executeQuery($sql)->fetchAllAssociative();
    }
}
