<?php

declare(strict_types=1);

namespace TeamSoft\CrmRepositoryBundle\Repository;

use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\Query\ResultSetMapping;
use Doctrine\Persistence\ManagerRegistry;
use TeamSoft\CrmRepositoryBundle\Entity\InfoStorage;

final class InfoStorageRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, InfoStorage::class);
    }

    /**
     * Get promotional materials in the storage.
     * (Optionally) Filter by promotion material id.
     *
     * @param  int  $storageId
     * @param  int|null  $materialId
     *
     * @return array{id: int, name: string, quantity: int, total: float, price: float}
     */
    public function findMaterials(int $storageId, ?int $materialId = null): array
    {
        $sqlWhere = $materialId !== null ? 'WHERE info_skladmove.Promo_id = :materialId' : '';
        $sql = <<<SQL
        select info_skladmove.Promo_id as id, info_promomaterial.name, info_promomaterial.price,
               coalesce(sum(case when info_sklad.ismain = 1 then info_skladmove.cnt else info_skladmove.cnt2 end),0) - coalesce(sum(distinct taskpromo.cnt),0) - coalesce(sum(distinct actionpromo.cnt),0) as cnt,
               coalesce(sum(case when info_sklad.ismain = 1 then info_skladmove.cnt else info_skladmove.cnt2 end),0) - coalesce(sum(taskpromo.cnt),0) - coalesce(sum(actionpromo.cnt),0),
            case when coalesce(sum(info_skladmove.summ),0) != 0 and sum(case when info_sklad.ismain = 1 
                then info_skladmove.cnt else info_skladmove.cnt2 end) != 0 
                then coalesce(sum(info_skladmove.summ),0)  / coalesce(sum(case when info_sklad.ismain = 1
                    then info_skladmove.cnt else info_skladmove.cnt2 end),0) * (coalesce(sum( taskpromo.cnt),0) + coalesce(sum( actionpromo.cnt),0)) 
                else 0 end as summ
        from info_skladmove 
        inner join info_sklad on info_skladmove.sklad_id = info_sklad.id and info_sklad.id = :storageId
        inner join info_promomaterial on info_promomaterial.id = info_skladmove.Promo_id 
        left join (select promomaterial_id as promo_id, sum(quantity) as cnt 
            from info_sklad 
            inner join info_task on info_task.responsible_id = info_sklad.user_id 
            inner join info_taskstate on info_task.taskstate_id = info_taskstate.id and info_taskstate.isfinish = 1
            inner join info_taskmaterial on info_taskmaterial.task_id = info_task.id 
            inner join (select min(date) as date, promo_id 
                from info_skladmove, info_sklad where info_sklad.id = :storageId
                                                and info_skladmove.sklad_id = info_sklad.id 
                group by info_skladmove.promo_id) t on t.promo_id = info_taskmaterial.promomaterial_id 
            where info_sklad.id = :storageId  and info_task.datefrom >=t.date 
            group by promomaterial_id) taskpromo on taskpromo.promo_id = info_promomaterial.id 
        left join (select info_actionmaterial.promo_id, sum(quantity) as cnt 
            from info_sklad 
            inner join info_action on info_action.responsible_id = info_sklad.user_id 
            inner join info_actionstate on info_action.actionstate_id = info_actionstate.id and info_actionstate.isclosed = 1
            inner join info_actionmaterial on info_actionmaterial.action_id = info_action.id 
            inner join (select min(date) as date, promo_id 
                from info_skladmove, info_sklad 
                where info_sklad.id = :storageId and info_skladmove.sklad_id = info_sklad.id 
                group by info_skladmove.promo_id) a on a.promo_id = info_actionmaterial.promo_id 
            where info_sklad.id = :storageId  and info_action.datefrom >=a.date 
            group by info_actionmaterial.promo_id) actionpromo on actionpromo.promo_id = info_promomaterial.id 
        {$sqlWhere}
        group by info_skladmove.Promo_id, info_promomaterial.name, info_promomaterial.price
        SQL;

        $rsm = new ResultSetMapping();
        $query = $this->_em->createNativeQuery($sql, $rsm)
            ->setParameter('storageId', $storageId)
            ->setParameter('materialId', $materialId);
        $rsm->addScalarResult('id', 'id', 'integer');
        $rsm->addScalarResult('name', 'name');
        $rsm->addScalarResult('cnt', 'quantity', 'integer');
        $rsm->addScalarResult('summ', 'total', 'integer');
        $rsm->addScalarResult('price', 'price', 'float');

        return $query->getArrayResult();
    }
}
