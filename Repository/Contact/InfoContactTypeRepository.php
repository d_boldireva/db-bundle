<?php

namespace TeamSoft\CrmRepositoryBundle\Repository\Contact;

use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\Query\ResultSetMappingBuilder;
use Doctrine\Persistence\ManagerRegistry;
use TeamSoft\CrmRepositoryBundle\Entity\InfoContacttype;

class InfoContactTypeRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, InfoContacttype::class);
    }

    /**
     * selects all contact types, which were used in info_contact
     * @return array
     */
    public function getUsedInContacts(): array
    {
        $tableName = $this->getClassMetadata()->getTableName();

        $sql = "
            SELECT DISTINCT info_contacttype.id, info_contacttype.Name
            FROM info_contacttype
            INNER JOIN info_contact ON info_contact.contacttype_id = info_contacttype.id
        ";

        $rsm = new ResultSetMappingBuilder($this->_em);
        $rsm->addRootEntityFromClassMetadata($this->_entityName, $tableName);
        $rsm->addEntityResult($this->_entityName, $tableName);

        $query = $this->getEntityManager()->createNativeQuery($sql, $rsm);

        return $query->getResult();
    }

    public function findByFilter(array $filter, int $roleId = null, int $languageId = null)
    {
        $tableName = $this->getClassMetadata()->getTableName();

        $joinParts = [];
        $whereParts = [];

        if ($roleId) {
            $joinParts[] = "INNER JOIN info_contacttypebyrole on info_contacttype.id = info_contacttypebyrole.contacttype_id";
            $whereParts[] = "info_contacttypebyrole.role_id = $roleId";
        }

        if ($languageId) {
            $whereParts[] = "($tableName.language_id IS NULL OR $tableName.language_id = $languageId)";
        }

        $rsm = new ResultSetMappingBuilder($this->_em);
        $rsm->addRootEntityFromClassMetadata($this->_entityName, $tableName, array(), ResultSetMappingBuilder::COLUMN_RENAMING_INCREMENT);

        $selectClause = $rsm->generateSelectClause();

        $join = implode(' ', $joinParts);
        $where = count($whereParts) ? 'WHERE ' . implode(' AND ', $whereParts) : '';

        $sql = "
            SELECT $selectClause FROM $tableName                       
            $join 
            $where
        ";

        $query = $this->getEntityManager()->createNativeQuery($sql, $rsm);
        return $query->getResult();
    }
}
