<?php

namespace TeamSoft\CrmRepositoryBundle\Repository\Contact;

use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\Query\ResultSetMappingBuilder;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Component\Security\Core\Security;
use TeamSoft\CrmRepositoryBundle\Entity\InfoArchivereason;
use TeamSoft\CrmRepositoryBundle\Entity\InfoCompany;
use TeamSoft\CrmRepositoryBundle\Entity\InfoContact;
use TeamSoft\CrmRepositoryBundle\Entity\InfoContactcateg;
use TeamSoft\CrmRepositoryBundle\Entity\InfoContactcategory;
use TeamSoft\CrmRepositoryBundle\Entity\InfoContactowner;
use TeamSoft\CrmRepositoryBundle\Entity\InfoContactrequest;
use TeamSoft\CrmRepositoryBundle\Entity\InfoContactspec;
use TeamSoft\CrmRepositoryBundle\Entity\InfoAddinfotype as InfoAddinfotypeEntity;
use TeamSoft\CrmRepositoryBundle\Entity\InfoDcrtype;
use TeamSoft\CrmRepositoryBundle\Entity\InfoMcmfilter;
use TeamSoft\CrmRepositoryBundle\Entity\InfoDictionary;
use TeamSoft\CrmRepositoryBundle\Entity\InfoFiltersetmobile as InfoFiltersetmobileEntity;
use TeamSoft\CrmRepositoryBundle\Entity\InfoUser;
use TeamSoft\CrmRepositoryBundle\Entity\RTC\InfoRtcRoomReserve;
use TeamSoft\CrmRepositoryBundle\Entity\InfoUser as InfoUserEntity;
use TeamSoft\CrmRepositoryBundle\Repository\SqlGeneratorTrait;
use TeamSoft\CrmRepositoryBundle\Service\DatabasePlatform;
use TeamSoft\CrmRepositoryBundle\Service\Options;
use TeamSoft\CrmRepositoryBundle\Utils\DateHelper;

/**
 * @see \TeamSoft\CrmRepositoryBundle\Entity\InfoContact
 * Class InfoContact
 * @package TeamSoft\CrmRepositoryBundle\Repository
 */
class InfoContactRepository extends ServiceEntityRepository
{
    use SqlGeneratorTrait;

    //TODO: replace all "mode" code to super class
    const DEFAULT_MODE = "default";

    const DCR_MODE = "dcr";

    private $isFullTextSearchOn;

    private $useCategoryWithDirection;

    private $mode;

    private $options;

    private $dp;

    private $tableName;

    /** @var Security */
    protected $security;

    public function __construct(ManagerRegistry $registry, Options $options, DatabasePlatform $dp, Security $security)
    {
        parent::__construct($registry, InfoContact::class);
        $this->mode = self::DEFAULT_MODE;
        $this->options = $options;
        $this->isFullTextSearchOn = $this->options->get('isFullTextSearchOff') != 1;
        $this->useCategoryWithDirection = $this->options->get(Options::OPTION_USE_CATEGORY_WITH_DIRECTION);
        $this->dp = $dp;
        $this->tableName = $this->getClassMetadata()->getTableName();
        $this->security = $security;
    }

    /**
     * @param $mode
     * @return $this
     */
    public function setMode($mode)
    {
        $this->mode = $mode;

        return $this;
    }

    public function findByFilter(
        array          $filterBy,
        array          $orderBy = null,
                       $limit = null,
                       $offset = null,
        InfoUserEntity $user = null
    )
    {
        $tableName = $this->getClassMetadata()->getTableName();

        $whereParts = array();
        if (isset($filterBy['isArchive'])) {
            $isArchiveIsNull = $this->dp->getIsNullFunctionExpression('info_contact.isarchive', 0);
            $whereParts[] = "$isArchiveIsNull = " . ($filterBy['isArchive'] ? 1 : 0);

            if ($this->mode == self::DCR_MODE) {
                $whereParts[] = "$tableName.morionid IS NOT NULL";
            }
        }

        if (isset($filterBy['companyId']) && is_numeric($filterBy['companyId'])) {
            $companyIdIsNull = $this->dp->getIsNullFunctionExpression('info_contact.company_id', 0);
            $whereParts[] = "(
                $companyIdIsNull = " . $filterBy['companyId'] . " OR $tableName.id IN (
                    SELECT contact_id
                    FROM info_contactworkplace
                    WHERE company_id = " . $filterBy['companyId'] . "
                )
            )";
        }

        $options = $this->options->get(['show_grid_last_task_date', 'visibleregion', 'searchForContactRequestsByModifier_id', 'showDCRRequestsBySubmission']);


        $c = $this->dp->getConcatOperator();
        if ($user && isset($filterBy['ownerId']) && is_numeric($filterBy['ownerId'])) {
            $userId = $user->getId();
            $ownerId = $filterBy['ownerId'];
            $whereParts[] = "
                (
                    $tableName.owner_id = $ownerId
                    OR
                    EXISTS(SELECT 1 FROM info_contactowner co WHERE co.contact_id = $tableName.id and co.owner_id = $ownerId AND co.isuse = 1)
                )
                AND EXISTS(SELECT 1 FROM info_contactSubmission WHERE child_id = $ownerId AND subj_id = $userId)
            ";
        } else if ($user && !$user->isAdmin()) {
            $userId = $user->getId();

            $wherePart = "($tableName.owner_id = $userId)";

            if (isset($options['visibleregion']) && $options['visibleregion'] == 1) {
                $wherePart = "(
                                $tableName.id in (select c.id from info_contact c
                                    inner join info_regioninuser r on r.region_id = c.Region_id and r.user_id = {$userId} 							
                                union 													
                                select c.id from info_contact c 
                                    inner join info_regioninuser r on r.region_id = c.Region_id
                                    inner join info_contactsubmission cs on cs.child_id = r.user_id and subj_id = {$userId}	
                                union
                                select c.id from info_contact c							
                                    where owner_id in (
                                        select child_id from info_contactsubmission 
                                            where subj_id = {$userId} 
                                        union 
                                        select {$userId}
                                    )	
                                union
                                select contact_id as id from info_contactowner co							
                                    where owner_id in  (
                                        select child_id from info_contactsubmission 
                                            where subj_id = {$userId} 
                                        union 
                                        select {$userId}
                                    )								
                                ) 
                            )";
            }

            $wherePart .= " 
            AND (
                        {$this->dp->getIsNullFunctionExpression('info_contacttype.isblogger', 0)} = 0 
                        OR info_contact.id IN (
                            SELECT contact_id FROM info_contactowner 
                            WHERE owner_id = {$userId} 
                            OR owner_id IN (SELECT child_id FROM info_contactsubmission WHERE subj_id = {$userId})
                        )
                    )";

            $whereParts[] = $wherePart;
        } else if (isset($filterBy['ownerId']) && is_numeric($filterBy['ownerId'])) {
            $ownerId = $filterBy['ownerId'];
            $whereParts[] = "(
                $tableName.owner_id = $ownerId
                OR
                EXISTS(SELECT 1 FROM info_contactowner co WHERE co.contact_id = $tableName.id and co.owner_id = $ownerId AND co.isuse = 1)
            )";
        }

        $toJoinCity = $toJoinStreetType = $toJoinCompany = $toJoinSpecialization = false;
        $toJoinContactType = $user && !$user->isAdmin();

        if (isset($filterBy['search']) && $filterBy['search']) {
            if ($this->isFullTextSearchOn) {
                if ($this->dp->isPostgreSQL100()) {
                    $searchArray = explode(' ', $filterBy['search']);
                    $searchQuery = "info_contact.id in (select contact_id from fts_contact ";
                    foreach ($searchArray as $index => $value) {
                        if (!$index) {
                            $searchQuery .= " where search_str @@ to_tsquery('$value:*') ";
                        } else {
                            $searchQuery .= " and search_str @@ to_tsquery('$value:*') ";
                        }
                    }
                    $searchQuery .= ") ";
                    $whereParts[] = $searchQuery;
                } else {
                    $whereParts[] = "CONTAINS(info_contactsearch.value, N'\"*" . mb_ereg_replace("[\s,]+", "*\" AND \"*",
                            str_replace("'", "''", $filterBy['search'])) . "*\"')";
                }
            } else {
                $searchParts = array();
                $firstNameIsNull = $this->dp->getIsNullFunctionExpression('info_contact.firstname', "''");
                $middleNameIsNull = $this->dp->getIsNullFunctionExpression('info_contact.middlename', "''");
                $lastNameIsNull = $this->dp->getIsNullFunctionExpression('info_contact.lastname', "''");
                $contactTypeNameIsNull = $this->dp->getIsNullFunctionExpression('info_contacttype.name', "''");
                $cityNameIsNull = $this->dp->getIsNullFunctionExpression('info_city.name', "''");
                $streetIsNull = $this->dp->getIsNullFunctionExpression('info_contact.street', "''");
                $buildingIsNull = $this->dp->getIsNullFunctionExpression('cast(info_contact.building as varchar)', "''");
                $streetTypeIsNull = $this->dp->getIsNullFunctionExpression('lower(streetType.name)', "''");
                $specializationIsNull = $this->dp->getIsNullFunctionExpression('lower(specialization.name)', "''");
                $companyNameIsNull = $this->dp->getIsNullFunctionExpression('lower(info_company.name)', "''");


                $dcrSearchSqlTemplate = "";
                if ($this->mode == self::DCR_MODE) {
                    $morionIdIsNull = $this->dp->getIsNullFunctionExpression('cast(info_contact.morionid as varchar)', "''");
                    $caseNumberIsNull = $this->dp->getIsNullFunctionExpression('cast(info_contact.case_number as varchar)', "''");
                    $dcrSearchSqlTemplate = " $c ' ' $c $morionIdIsNull $c ' ' $c $caseNumberIsNull";
                }


                $searchSqlTemplate = "LOWER($firstNameIsNull $c ' ' $c $middleNameIsNull $c ' ' $c $lastNameIsNull $c ' ' $c $contactTypeNameIsNull" .
                    " $c ' ' $c $cityNameIsNull $c ' ' $c $streetIsNull $c ' ' $c $buildingIsNull $c ' ' $c $streetTypeIsNull" .
                    " $c ' ' $c $specializationIsNull $c ' ' $c $companyNameIsNull" .
                    $dcrSearchSqlTemplate .
                    ") LIKE LOWER(N'%%%s%%')";
                foreach (mb_split("[\s,]+", $filterBy['search']) as $substring) {
                    if ($substring) {
                        $searchParts[] = sprintf($searchSqlTemplate, $substring);
                    }
                }
                if ($searchParts) {
                    $toJoinContactType = $toJoinCity = $toJoinStreetType = $toJoinCompany = $toJoinSpecialization = true;
                    $whereParts[] = implode(' AND ', $searchParts);
                }
            }
        }

        if (isset($filterBy['firstName']) && $filterBy['firstName']) {
            $whereParts[] = "LOWER(info_contact.firstname) LIKE LOWER(N'%" . str_replace("'", "''", $filterBy['firstName']) . "%')";
        }

        if (isset($filterBy['middleName']) && $filterBy['middleName']) {
            $whereParts[] = "LOWER(info_contact.middlename) LIKE LOWER(N'%" . str_replace("'", "''", $filterBy['middleName']) . "%')";
        }

        if (isset($filterBy['lastName']) && $filterBy['lastName']) {
            $whereParts[] = "LOWER(info_contact.lastname) LIKE LOWER(N'%" . str_replace("'", "''", $filterBy['lastName']) . "%')";
        }

        if (isset($filterBy['companyName']) && $filterBy['companyName']) {
            $toJoinCompany = true;
            $whereParts[] = "LOWER(info_company.name) LIKE LOWER(N'%" . str_replace("'", "''", $filterBy['companyName']) . "%')";
        }

        if (isset($filterBy['specialization']) && $filterBy['specialization']) {
            $toJoinSpecialization = true;
            $whereParts[] = "LOWER(specialization.name) LIKE LOWER(N'%" . str_replace("'", "''", $filterBy['specialization']) . "%')";
        }

        if (isset($filterBy['eaddr']) && $filterBy['eaddr']) {
            $whereParts[] = "LOWER(info_contact.eaddr) LIKE LOWER(N'%" . str_replace("'", "''", $filterBy['eaddr']) . "%')";
        }

        if (isset($filterBy['phone1']) && $filterBy['phone1']) {
            $whereParts[] = "LOWER(info_contact.phone1) LIKE LOWER(N'%" . str_replace("'", "''", $filterBy['phone1']) . "%')";
        }

        if (isset($filterBy['address']) && $filterBy['address']) {
            $toJoinCity = $toJoinStreetType = true;
            $cityNameIsNull = $this->dp->getIsNullFunctionExpression('info_city.name', "''");
            $streetTypeIsNull = $this->dp->getIsNullFunctionExpression('streetType.name', "''");
            $streetIsNull = $this->dp->getIsNullFunctionExpression('info_contact.street', "''");
            $buildingIsNull = $this->dp->getIsNullFunctionExpression('info_contact.building', "''");
            $whereParts[] = "LOWER($cityNameIsNull $c ' ' $c $streetTypeIsNull $c ' ' $c $streetIsNull $c ' ' $c $buildingIsNull) LIKE LOWER(N'%" . str_replace("'",
                    "''", $filterBy['address']) . "%')";
        }

        if (isset($filterBy['tasktypeId']) && is_numeric($filterBy['tasktypeId'])) {
            $tasktypeId = $filterBy['tasktypeId'];
            $toJoinCompany = true;
            $whereParts[] = "(
                info_company.CompanyType_id IN ( SELECT companytype_id FROM info_companytypetasktype WHERE tasktype_id = $tasktypeId)
                OR 
                info_company.id IN (
                    SELECT info_company.id
                    FROM info_companytypetasktype 
                    INNER JOIN info_company ON info_companytypetasktype.companytype_id = info_company.CompanyType_id
                    INNER JOIN info_contactworkplace ON info_contactworkplace.Company_id = info_company.id
                    WHERE info_companytypetasktype.tasktype_id = $tasktypeId
                )
            )";
        }

        $toJoinContactCateg = false;
        if (isset($filterBy['category']) && $filterBy['category']) {
            $toJoinContactCateg = true;
            $whereParts[] = "LOWER(info_contactcateg.name) LIKE LOWER('%" . str_replace("'", "''", $filterBy['category']) . "%')";
        }

        if (isset($options['show_grid_last_task_date'])
            && $options['show_grid_last_task_date'] == 1
            && isset($filterBy['last_task_date'])
            && is_string($filterBy['last_task_date'])
            && $lastTaskDate = json_decode($filterBy['last_task_date'], true)
        ) {
            if ($lastTaskDate['start'] && $lastTaskDate['end']) {
                $whereParts[] = "last_task_date.last_datefrom BETWEEN '{$lastTaskDate['start']}' AND '{$lastTaskDate['end']} 23:59:59'";
            } else {
                if ($lastTaskDate['start']) {
                    $whereParts[] = "last_task_date.last_datefrom >= '{$lastTaskDate['start']}'";
                } else {
                    if ($filterBy['last_task_date']['end']) {
                        $whereParts[] = "last_task_date.last_datefrom <= '{$lastTaskDate['end']} 23:59:59' AND last_task_date.last_datefrom <> '' ";
                    }
                }
            }
        }

        if (isset($filterBy['filters']) && is_array($filterBy['filters'])) {
            $userId = $user ? $user->getId() : -1;
            $filterWhereParts = [];
            foreach ($filterBy['filters'] as $filter) {
                [$filterEntity, $args] = $filter;
                if ($filterEntity instanceof InfoFiltersetmobileEntity) {
                    if ($filterEntity->getPage() == 102) {
                        $wherePart = str_replace("#USER_ID#", $userId, $filterEntity->getFilter());

                        if ($filterEntity->isFlag()) {
                            $flag = isset($args[0]) && $args[0] == 1 ? 1 : 0;
                            $wherePart = str_replace("#FLAG#", $flag, $wherePart);
                        }
                        if ($filterEntity->isSingle()) {
                            $wherePart = str_replace("#SINGLE#", '', $wherePart);
                            $filterWhereParts = [$wherePart];
                            break;
                        }

                        $filterWhereParts[] = $wherePart;
                    }
                }
            }
            $whereParts = array_merge($whereParts, $filterWhereParts);
        }

        if (empty($filterBy['filterIds']) && $this->mode == self::DCR_MODE) {
            $whereParts[] = "$tableName.morionid IS NOT NULL";
        }

        $orderByParts = array();
        if ($orderBy) {
            foreach ($orderBy as $value) {
                $order = strtoupper($value["order"]);
                if ($order === "ASC" || $order === "DESC") {
                    $column = strtolower($value["column"]);
                    if ($column == "lastname") {
                        $orderByParts[] = "info_contact.lastname " . $order;
                    } else {
                        if ($column == "firstname") {
                            $orderByParts[] = "info_contact.firstname " . $order;
                        } else {
                            if ($column == "middlename") {
                                $orderByParts[] = "info_contact.middlename " . $order;
                            } else {
                                if ($column == "companyname") {
                                    $toJoinCompany = true;
                                    $orderByParts[] = "info_company.name " . $order;
                                } else {
                                    if ($column == "specialization") {
                                        $toJoinSpecialization = true;
                                        $orderByParts[] = "specialization.name " . $order;
                                    } else {
                                        if ($column == 'last_task_date') {
                                            $orderByParts[] = "last_task_date " . $order;
                                        } else {
                                            if ($column == "address") {
                                                $toJoinCity = $toJoinStreetType = true;
                                                $cityNameIsNull = $this->dp->getIsNullFunctionExpression('info_city.name', "''");
                                                $streetTypeIsNull = $this->dp->getIsNullFunctionExpression('streetType.name', "''");
                                                $streetIsNull = $this->dp->getIsNullFunctionExpression('info_contact.street', "''");
                                                $buildingIsNull = $this->dp->getIsNullFunctionExpression('info_contact.building', "''");
                                                $orderByParts[] = "$cityNameIsNull $c ' ' $c $streetTypeIsNull $c ' ' $c $streetIsNull $c ' ' $c $buildingIsNull " . $order;
                                            } else {
                                                if ($column == "category") {
                                                    $toJoinContactCateg = true;
                                                    $orderByParts[] = 'info_contactcateg.name ' . $order;
                                                } else {
                                                    if ($this->mode == self::DCR_MODE && $column == "cr_status_verification") {
                                                        $orderByParts[] = "cr_status_verification " . $order;
                                                    } else {
                                                        if ($this->mode == self::DCR_MODE && $column == "get_verification") {
                                                            $orderByParts[] = "getverification " . $order;
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

        $rsm = new ResultSetMappingBuilder($this->_em);
        $rsm->addRootEntityFromClassMetadata($this->_entityName, $tableName, array(),
            ResultSetMappingBuilder::COLUMN_RENAMING_INCREMENT);
        $rsm->addEntityResult($this->_entityName, $tableName, 'contact');
        $rsm->addScalarResult('cr_status_verification', 'cr_status_verification', 'integer');
        $rsm->addScalarResult('modifier_user_name', 'modifier_user_name', 'string');

        if (isset($options['show_grid_last_task_date']) && $options['show_grid_last_task_date'] == 1) {
            $rsm->addScalarResult('last_task_date', 'last_task_date', 'datetime');
        }

        $selectClause = $rsm->generateSelectClause();
        $where = $whereParts ? 'WHERE ' . implode(' AND ', $whereParts) : '';
        $orderBy = $orderByParts ? 'ORDER BY ' . implode(', ', $orderByParts) : '';
        if ($this->mode == self::DCR_MODE && $user) {
            $usersToFilter = [];
            if ($options['showDCRRequestsBySubmission'] ?? 0) {
                $usersToFilter = array_merge([$user], $user->getSubordinates());
            } else if ($options['searchForContactRequestsByModifier_id'] ?? 0) {
                $usersToFilter = [$user];
            }
            if (count($usersToFilter)) {
                $userIds = array_map(function (InfoUserEntity $u) {
                    return $u->getId();
                }, $usersToFilter);
                $usersToFilter = 'AND cr.modifier_id IN (' . implode(',', $userIds) . ')';
            } else {
                $usersToFilter = '';
            }

            $selectClause .= ", (" . $this->dp->modifyOnlyLimitQuery("
                SELECT statusverification FROM (
				SELECT DISTINCT COUNT(*) as count, modified, statusverification, sendverification
				FROM info_contactrequest cr
				WHERE cr.contact_id = $tableName.id {$usersToFilter}
				GROUP BY modified, sendverification, statusverification) q
				ORDER BY CASE WHEN sendverification IS NULL THEN 1 ELSE 0 END DESC, sendverification DESC, modified DESC, count DESC, statusverification
				", 1) . ") AS cr_status_verification
            ";
            $selectClause .= ", (" . $this->dp->modifyOnlyLimitQuery("
				SELECT u.name
				FROM info_contactrequest cr
				INNER JOIN info_user u ON u.id = cr.modifier_id
				WHERE 
				    cr.contact_id = $tableName.id				    			
				ORDER BY cr.modified DESC
				", 1) . ") modifier_user_name
            ";
        }
        $joins = '';
        if (isset($options['show_grid_last_task_date']) && $options['show_grid_last_task_date'] == 1) {
            $selectClause .= ", last_task_date.last_datefrom AS last_task_date";
            $isArchiveIsNull = $this->dp->getIsNullFunctionExpression('c.isarchive', 0);
            $joins .= "
               LEFT OUTER JOIN (SELECT tmp.last_datefrom, c.id 
                   FROM info_contact c
                       LEFT JOIN
                         (SELECT t.contact_id,
                                 MAX(datefrom) last_datefrom
                          FROM info_task t
                          INNER JOIN info_taskstate tS ON t.taskstate_id=ts.id
                          AND ts.isfinish=1
                          WHERE t.contact_id IS NOT NULL
                          GROUP BY t.contact_id) AS tmp ON c.id=tmp.contact_id
                   WHERE $isArchiveIsNull=0) AS last_task_date 
               ON (last_task_date.id = info_contact.id)
				";
        }

        $sql = "SELECT $selectClause \n FROM $tableName ";

        if ($this->isFullTextSearchOn && !$this->dp->isPostgreSQL100() && isset($filterBy['search']) && $filterBy['search']) {
            $sql .= "\nINNER JOIN info_contactsearch ON info_contactsearch.contact_id = $tableName.id ";
        }
        if ($toJoinContactCateg) {
            $sql .= "\nLEFT OUTER JOIN info_contactcateg ON info_contactcateg.id = $tableName.Category_id ";
        }
        if ($toJoinContactType) {
            $sql .= "\nLEFT OUTER JOIN info_contacttype ON info_contacttype.id = $tableName.contacttype_id ";
        }

        if ($toJoinCity) {
            $sql .= "\nLEFT OUTER JOIN info_city ON info_city.id = $tableName.city_id ";
        }
        if ($toJoinStreetType) {
            $sql .= "\nLEFT OUTER JOIN info_dictionary AS streetType ON streetType.id = $tableName.streettype_id ";
        }
        if ($toJoinCompany) {
            $sql .= "\nLEFT OUTER JOIN info_company ON info_company.id = $tableName.company_id ";
        }
        if ($toJoinSpecialization) {
            $sql .= "\nLEFT OUTER JOIN info_dictionary AS specialization ON specialization.id = $tableName.specialization_id ";
        }
        $sql .= "\n$joins\n$where\n$orderBy\n";

        $sql = $this->_em->getConnection()->getDatabasePlatform()->modifyLimitQuery($sql, $limit, $offset);
        $query = $this->getEntityManager()->createNativeQuery($sql, $rsm);

        $result = $query->getResult();

        $totalCountSql = "SELECT COUNT(1) FROM $tableName ";
        if ($this->isFullTextSearchOn && !$this->dp->isPostgreSQL100() && isset($filterBy['search']) && $filterBy['search']) {
            $totalCountSql .= "INNER JOIN info_contactsearch ON info_contactsearch.contact_id = $tableName.id ";
        }
        if ($toJoinContactCateg) {
            $totalCountSql .= "\nLEFT OUTER JOIN info_contactcateg ON info_contactcateg.id = $tableName.Category_id ";
        }
        if ($toJoinContactType) {
            $totalCountSql .= "\nLEFT OUTER JOIN info_contacttype ON info_contacttype.id = $tableName.contacttype_id ";
        }
        if ($toJoinCity) {
            $totalCountSql .= "\nLEFT OUTER JOIN info_city ON info_city.id = $tableName.city_id ";
        }
        if ($toJoinStreetType) {
            $totalCountSql .= "\nLEFT OUTER JOIN info_dictionary AS streetType ON streetType.id = $tableName.streettype_id ";
        }
        if ($toJoinCompany) {
            $totalCountSql .= "\nLEFT OUTER JOIN info_company ON info_company.id = $tableName.company_id ";
        }
        if ($toJoinSpecialization) {
            $totalCountSql .= "\nLEFT OUTER JOIN info_dictionary AS specialization ON specialization.id = $tableName.specialization_id ";
        }
        $totalCountSql .= "\n$joins\n$where\n";

        $withTotalCount = false;
        return array(
            "total_count" => $withTotalCount ? (int)$this->getEntityManager()->getConnection()->executeQuery($totalCountSql)->fetchOne() : null,
            "data" => $result,
        );
    }

    public function findApprovedContacts($user_id, $datesList)
    {

        $firstDate = $datesList[0]['date'];
        $year = (new \DateTime($firstDate))->format('Y');
        $monthCount = sizeof($datesList);

        $month = array_map(function ($item) {
            return $item['number'];
        }, $datesList);
        $monthStr = implode(',', $month);

        $existsSql = "SELECT count(*) cnt FROM info_plancontacttask WHERE user_id = $user_id AND " . $this->dp->getMonthExpression('dt') . "  in ($monthStr) AND " . $this->dp->getYearExpression('dt') . "  = $year";
        $exists = $this->_em->getConnection()->fetchAllAssociative($existsSql);

        if (!$exists[0]['cnt']) {
            $insertSQL = "
                INSERT INTO info_plancontacttask (user_id, contact_id, dt, target_id, tasks, " . $this->dp->escapeColumn('status') . ")
                    SELECT $user_id, contact_id, dt, target_id, visit_cnt, " . $this->dp->escapeColumn('status') . "
                        FROM targeting_contact_initial($user_id, '$firstDate', $monthCount)";

            $joinParts = [];
            for ($i = 0; $i < sizeof($datesList); $i++) {
                $joinParts[] = "SELECT " . $this->dp->dateAddExpression('m', $i, "'" . $firstDate . "'") . " dt";
            }
            $join = implode(" UNION ", $joinParts);

            $insertSQL .= " JOIN ($join) quarterMonth ON 1=1";

            $this->_em->getConnection()->executeQuery($insertSQL);
        }

        $sql = 'SELECT * FROM targeting_contact(?, ?, ?)';

        $result = [];

        $result['approved'] = $this->_em->getConnection()->fetchAllAssociative($sql, [$user_id, $firstDate, $monthCount]);
        foreach ($result['approved'] as $k => &$r) {
            $r['patientcount'] = intval($r['patientcount']);
            $r['potential'] = intval($r['potential']);
            $r['factPotential'] = intval($r['factPotential']);
            $r['plancnt'] = intval($r['plancnt']);
        }

        $groupsSql = 'SELECT * FROM targeting_contact_summary(?, ?, ?)';

        $result['monthCount'] = $monthCount;

        $groups = [];
        $contactSummary = $this->_em->getConnection()->fetchAllAssociative($groupsSql, [$user_id, $firstDate, $monthCount]);

        foreach ($contactSummary as $item) {
            $id = $item['id'];
            if (!isset($groups[$id])) {
                $groups[$id]['group_name'] = $item['group_name'];
            }

            $groups[$id]['categories'][] = $item;
        }

        $result['groups'] = $groups;

        return $result;
    }

    private function getFilterEntities(array $filters = [])
    {
        $filterIds = [];
        foreach ($filters as $filter) {
            $filterId = explode(':', $filter)[0];
            if (is_numeric($filterId)) {
                $filterIds[] = $filterId;
            }
        }
        /**
         * @var InfoFiltersetmobile $filterRepository
         */
        $filterRepository = $this->getEntityManager()->getRepository("TeamSoftCrmRepositoryBundle:InfoFiltersetmobile");

        return count($filterIds) ? $filterRepository->findByIds($filterIds) : null;
    }

    public function findLimit($limit)
    {

        $query = $this->createQueryBuilder('c')
            ->setMaxResults($limit)
            ->getQuery();

        return $query->getResult();
    }

    public function getTasksCountOnStrategy()
    {
        // TODO: do it
        return rand(0, 100);
    }

    public function getTasksCountAll()
    {
        // TODO: do it
        return rand(0, 100);
    }

    public function findThatNotInPlanContactTasks($userId, \DateTime $dateTime, $masterListQuarterPeriod)
    {
        $rsm = new ResultSetMappingBuilder($this->_em);
        $rsm->addRootEntityFromClassMetadata($this->_entityName, 'c', [],
            ResultSetMappingBuilder::COLUMN_RENAMING_INCREMENT);

        $selectClause = $rsm->generateSelectClause();

        $sql = "
                DECLARE @userId int, @currentMonthDate DATETIME;
                SET @userId = :userId;
                SET @currentMonthDate = DATEADD(m, DATEDIFF(m, 0, GETDATE()), 0);
                
                SELECT $selectClause
                FROM info_contact c
                WHERE c.id IN (
                    SELECT t.contact_id FROM info_task t
                    LEFT JOIN info_tasktype tt ON t.tasktype_id = tt.id
                    WHERE t.responsible_id = @userId AND tt.Name = 'Врач'
                        AND t.datefrom BETWEEN ";

        $sql .= $masterListQuarterPeriod ? "DATEADD(month, -3, GETDATE())" : "DATEADD(m, DATEDIFF(m, 0, GETDATE()), 0) ";

        $sql .= "AND GETDATE()
                        AND t.company_id IS NOT NULL
                    UNION
                        SELECT contact_id FROM info_plancontacttask ";
        $sql .= $masterListQuarterPeriod ? "WHERE dt > DATEADD(month, -3, GETDATE()) " :
            "WHERE YEAR(dt) = YEAR(@currentMonthDate) AND MONTH(dt) = MONTH(@currentMonthDate) ";
        $sql .= "AND  user_id = @userId
                ) AND c.id NOT IN (SELECT contact_id FROM info_plancontacttask
                WHERE YEAR(dt) = ':yearString' AND MONTH(dt) = ':monthString' AND  user_id = @userId)";

        $sql = str_replace(':userId', $userId, $sql);
        $sql = str_replace(':yearString', $dateTime->format("Y"), $sql);
        $sql = str_replace(':monthString', $dateTime->format("m"), $sql);

        $query = $this->getEntityManager()->createNativeQuery($sql, $rsm);

        return $query->getResult();
    }

    public function findByCompanyAndWorkplace($companyId)
    {
        $rsm = $this->createResultSetMappingBuilder('c');

        $selectClause = $rsm->generateSelectClause();

        $sql = "
            SELECT $selectClause
            FROM info_contact c
            WHERE c.company_id = $companyId
            OR EXISTS(SELECT 1 FROM info_contactworkplace cw WHERE cw.company_id = $companyId AND cw.contact_id = c.id)
        ";

        $query = $this->getEntityManager()->createNativeQuery($sql, $rsm);

        return $query->getResult();
    }

    /**
     * @param null $requestParams
     * @param boolean $count
     * @param null $targetId
     * @param null $userId
     * @param null $countryId
     * @param bool $getContact
     * @return array|boolean
     * @throws \Doctrine\DBAL\Exception
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function getAvailableForDistribution(
        $requestParams = null,
        $count = true,
        $targetId = null,
        $userId = null,
        $countryId = null,
        $getContact = false
    )
    {
        $whereParams = [];

        $joinContactinfo = '';

        if ($requestParams) {
            $addInfoTypeRepository = $this->_em
                ->getRepository(InfoAddinfotypeEntity::class);

            $mcmFilterRepository = $this->_em
                ->getRepository(InfoMcmfilter::class);

            foreach ($requestParams as $key => $param) {
                if (is_string($param)) {
                    $param = json_decode($param, true);
                }

                if (empty($param)) {
                    continue;
                }

                if (!empty($param['mcmfilter'])) {
                    $filter = $mcmFilterRepository->find($param['mcmfilter']);
                    $filterStr = $filter->getFilter();
                    $filterValues = $param['value'];

                    if ($userId) {
                        $filterStr = str_replace('#USER_ID#', $userId, $filterStr);
                    }

                    if ($filter->getType() === 'string') {
                        if (strpos($filterStr, '#VALUE#') !== false && $filterValues) {
                            $whereParams[] = str_replace('#VALUE#', $filterValues, $filterStr);
                        }
                    } elseif ($filter->getType() === 'range' &&
                        (strpos($filterStr, '#MIN#') !== false && strpos($filterStr, '#MAX#') !== false) &&
                        preg_match('/^[0-9]+\,[0-9]+$/', $filterValues)) {
                        [$min, $max] = explode(',', $filterValues);

                        $whereParams[] = str_replace('#MIN#', $min, str_replace('#MAX#', $max, $filterStr));
                    } elseif ($filter->getType() === 'dateRange' &&
                        (strpos($filterStr, '#MIN#') !== false && strpos($filterStr, '#MAX#') !== false) &&
                        preg_match('/^[0-9\-]+\,[0-9\-]+$/', $filterValues)) {
                        [$min, $max] = explode(',', $filterValues);

                        $filterStr = str_replace('#MIN#', "'{$min} 00:00:00'", $filterStr);

                        $whereParams[] = str_replace('#MAX#', "'{$max} 23:59:59'", $filterStr);
                    }
                } elseif (!empty($param['addinfotype'])) {
                    $filter = $addInfoTypeRepository->find($param['addinfotype']);
                    $filterValues = $param['value'];
                    $joinContactinfo = 'INNER JOIN info_contactinfo ON info_contactinfo.subj_id = info_contact.id';

                    switch ($filter->getFielddatatype()) {
                        case InfoAddinfotype::FILTER_STRING:
                            $whereParams[] = "info_contactinfo.stringvalue IN ($filterValues)";
                            break;
                        case InfoAddinfotype::FILTER_INT:
                            $arrValues = explode(',', $filterValues);
                            $whereParams[] = "info_contactinfo.intvalue BETWEEN $arrValues[0] AND $arrValues[1]";
                            break;
                        case InfoAddinfotype::FILTER_FLOAT:
                            $arrValues = explode(',', $filterValues);
                            $whereParams[] = "info_contactinfo.floatvalue BETWEEN $arrValues[0] AND $arrValues[1]";
                            break;
                        case InfoAddinfotype::FILTER_DATETIME:
                            $arrValues = explode(',', $filterValues);
                            $whereParams[] = "info_contactinfo.datevalue BETWEEN '$arrValues[0]' AND '$arrValues[1]'";
                            break;
                        case InfoAddinfotype::FILTER_CUSTOMDICTIONARY:
                            $whereParams[] = "info_contactinfo.dictvalue_id IN ($filterValues)";
                            break;
                        case InfoAddinfotype::FILTER_SYSTEMDICTIONARY:
                            $whereParams[] = "info_contactinfo.sysdictvalue_id IN ($filterValues)";
                            break;
                    }

                    $whereParams[] = "info_contactinfo.InfoType_id = " . $filter->getId();
                } elseif ($key === 'contact_ids') {
                    $whereIn = [];

                    $idsChunk = array_chunk($param, 2000);
                    for ($i = 0; $i < count($idsChunk); $i++) {
                        $ids = implode(',', $idsChunk[$i]);
                        $whereIn[] = "info_contact.id IN ({$ids})";
                    }
                    $whereParams[] = "(" . implode(" OR ", $whereIn) . ")";
                }
            }
        }

        if ($countryId) {
            $whereParams[] = "info_contact.country_id = $countryId";
        }
        $whereClause = count($whereParams) ? 'AND ' . implode(' AND ', $whereParams) : '';

        if ($count) {
            return $this->getAvailableCountForDistribution($whereClause, $joinContactinfo);
        } elseif ($targetId) {
            return $this->insertAvailableForTarget($userId, $targetId, $whereClause, $joinContactinfo);
        } elseif ($getContact) {
            return $this->getContactForDistribution($whereClause, $joinContactinfo);
        }

        return null;
    }

    /**
     * @param string $whereClause
     * @param string $joinContactinfo
     * @return int|mixed|string|null
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function getAvailableCountForDistribution($whereClause = '', $joinContactinfo = '')
    {
        $rsm = new ResultSetMappingBuilder($this->_em);
        $rsm->addScalarResult('email', 'email');
        $rsm->addScalarResult('email_unsubscribe', 'email_unsubscribe');
        $rsm->addScalarResult('sms', 'sms');
        $rsm->addScalarResult('viber', 'viber');
        $rsm->addScalarResult('sms_unsubscribe', 'sms_unsubscribe');
        $rsm->addScalarResult('viber_unsubscribe', 'viber_unsubscribe');
        $rsm->addScalarResult('telegram', 'telegram');
        $rsm->addScalarResult('telegram_unsubscribe', 'telegram_unsubscribe');

        $sql = "
                SELECT 
                (
                    SELECT COUNT(DISTINCT info_contact.id)
                    FROM info_contact
                    $joinContactinfo
                    LEFT JOIN info_contactemail ON info_contactemail.contact_id = info_contact.id
                    LEFT JOIN info_targetspec ON info_targetspec.spec_id = info_contact.Specialization_id 
                    LEFT JOIN info_target ON info_target.id = info_targetspec.target_id
                    LEFT JOIN info_company ON info_company.id = info_contact.company_id
                    LEFT JOIN info_task ON info_task.contact_id = info_contact.id
                    WHERE COALESCE(info_contact.isarchive, 0) != 1 AND info_contactemail.status = 1 AND
                     info_contactemail.email IS NOT NULL $whereClause
                ) AS email,
                (
                    SELECT COUNT(DISTINCT info_contact.id)
                    FROM info_contact
                    $joinContactinfo
                    LEFT JOIN info_contactemail ON info_contactemail.contact_id = info_contact.id
                    LEFT JOIN info_targetspec ON info_targetspec.spec_id = info_contact.Specialization_id 
                    LEFT JOIN info_target ON info_target.id = info_targetspec.target_id
                    LEFT JOIN info_company ON info_company.id = info_contact.company_id
                    LEFT JOIN info_task ON info_task.contact_id = info_contact.id
                    INNER JOIN info_mcmunsubscribed ON info_mcmunsubscribed.contact_id = info_contact.id
                    LEFT JOIN info_mcmdistribution ON info_mcmunsubscribed.distribution_id = info_mcmdistribution.id
                    LEFT JOIN info_mcmcontent ON info_mcmcontent.id = info_mcmdistribution.mcmcontent_id
                    WHERE COALESCE(info_contact.isarchive, 0) != 1 AND info_contactemail.status = 1 
                            AND info_contactemail.email IS NOT NULL 
                            AND (info_mcmcontent.type = 'email' OR info_mcmunsubscribed.distribution_id IS NULL) 
                            $whereClause
                ) as email_unsubscribe,
                (
                    SELECT COUNT(DISTINCT info_contact.id)
                    FROM info_contact
                    $joinContactinfo
                    LEFT JOIN info_contactphone ON info_contactphone.contact_id = info_contact.id
                    LEFT JOIN info_targetspec ON info_targetspec.spec_id = info_contact.Specialization_id 
                    LEFT JOIN info_target ON info_target.id = info_targetspec.target_id
                    LEFT JOIN info_company ON info_company.id = info_contact.company_id
                    LEFT JOIN info_task ON info_task.contact_id = info_contact.id
                    WHERE COALESCE(info_contact.isarchive, 0) != 1 AND info_contactphone.issmsmcm = 1 AND 
                      info_contactphone.phone IS NOT NULL $whereClause
                ) AS sms,
                (
                    SELECT COUNT(DISTINCT info_contact.id)
                    FROM info_contact
                    $joinContactinfo
                    LEFT JOIN info_contactphone ON info_contactphone.contact_id = info_contact.id
                    LEFT JOIN info_targetspec ON info_targetspec.spec_id = info_contact.Specialization_id 
                    LEFT JOIN info_target ON info_target.id = info_targetspec.target_id
                    LEFT JOIN info_company ON info_company.id = info_contact.company_id
                    LEFT JOIN info_task ON info_task.contact_id = info_contact.id
                    INNER JOIN info_mcmunsubscribed ON info_mcmunsubscribed.contact_id = info_contact.id
                    LEFT JOIN info_mcmdistribution ON info_mcmunsubscribed.distribution_id = info_mcmdistribution.id
                    LEFT JOIN info_mcmcontent ON info_mcmcontent.id = info_mcmdistribution.mcmcontent_id
                    WHERE COALESCE(info_contact.isarchive, 0) != 1 AND info_contactphone.issmsmcm = 1 
                        AND info_contactphone.phone IS NOT NULL
                        AND (info_mcmcontent.type = 'email' OR info_mcmunsubscribed.distribution_id IS NULL) 
                        $whereClause
                    
                ) as sms_unsubscribe,
                (
                    SELECT COUNT(DISTINCT info_contact.id)
                    FROM info_contact
                    $joinContactinfo
                    LEFT JOIN info_contactphone ON info_contactphone.contact_id = info_contact.id
                    LEFT JOIN info_targetspec ON info_targetspec.spec_id = info_contact.Specialization_id 
                    LEFT JOIN info_target ON info_target.id = info_targetspec.target_id
                    LEFT JOIN info_company ON info_company.id = info_contact.company_id
                    LEFT JOIN info_task ON info_task.contact_id = info_contact.id
                    WHERE COALESCE(info_contact.isarchive, 0) != 1 AND info_contactphone.isvibermcm = 1 AND 
                      info_contactphone.phone IS NOT NULL $whereClause
                ) AS viber,
                (
                    SELECT COUNT(DISTINCT info_contact.id)
                    FROM info_contact
                    $joinContactinfo
                    LEFT JOIN info_contactphone ON info_contactphone.contact_id = info_contact.id
                    LEFT JOIN info_targetspec ON info_targetspec.spec_id = info_contact.Specialization_id 
                    LEFT JOIN info_target ON info_target.id = info_targetspec.target_id
                    LEFT JOIN info_company ON info_company.id = info_contact.company_id
                    LEFT JOIN info_task ON info_task.contact_id = info_contact.id
                    INNER JOIN info_mcmunsubscribed ON info_mcmunsubscribed.contact_id = info_contact.id
                    LEFT JOIN info_mcmdistribution ON info_mcmunsubscribed.distribution_id = info_mcmdistribution.id
                    LEFT JOIN info_mcmcontent ON info_mcmcontent.id = info_mcmdistribution.mcmcontent_id and 
                        info_mcmcontent.type in ('viber', 'email')
                    WHERE COALESCE(info_contact.isarchive, 0) != 1 AND info_contactphone.isvibermcm = 1 
                        AND info_contactphone.phone IS NOT NULL 
                        AND (info_mcmcontent.type IN ('email', 'viber') OR info_mcmunsubscribed.distribution_id IS NULL)
                        $whereClause
                ) as viber_unsubscribe,
                (
                    SELECT COUNT(DISTINCT info_contact.id)
                    FROM info_contact
                    $joinContactinfo
                    JOIN info_mcmtelegramuser ON info_mcmtelegramuser.contact_id = info_contact.id
                    LEFT JOIN info_contactphone ON info_contactphone.contact_id = info_contact.id
                    LEFT JOIN info_targetspec ON info_targetspec.spec_id = info_contact.Specialization_id 
                    LEFT JOIN info_target ON info_target.id = info_targetspec.target_id
                    LEFT JOIN info_company ON info_company.id = info_contact.company_id
                    LEFT JOIN info_task ON info_task.contact_id = info_contact.id
                    WHERE COALESCE(info_contact.isarchive, 0) != 1 AND info_mcmtelegramuser.chat_identifier IS NOT NULL 
                      $whereClause
                ) AS telegram,
                (
                    SELECT COUNT(DISTINCT info_contact.id)
                    FROM info_contact
                    $joinContactinfo
                    JOIN info_mcmtelegramuser ON info_mcmtelegramuser.contact_id = info_contact.id
                    LEFT JOIN info_contactphone ON info_contactphone.contact_id = info_contact.id
                    LEFT JOIN info_targetspec ON info_targetspec.spec_id = info_contact.Specialization_id 
                    LEFT JOIN info_target ON info_target.id = info_targetspec.target_id
                    LEFT JOIN info_company ON info_company.id = info_contact.company_id
                    LEFT JOIN info_task ON info_task.contact_id = info_contact.id
                    INNER JOIN info_mcmunsubscribed ON info_mcmunsubscribed.contact_id = info_contact.id
                    LEFT JOIN info_mcmdistribution ON info_mcmunsubscribed.distribution_id = info_mcmdistribution.id
                    LEFT JOIN info_mcmcontent ON info_mcmcontent.id = info_mcmdistribution.mcmcontent_id and info_mcmcontent.type in ('telegram', 'email')
                    WHERE COALESCE(info_contact.isarchive, 0) != 1 AND info_mcmtelegramuser.chat_identifier IS NOT NULL
                        AND (info_mcmcontent.type IN ('email', 'telegram') OR info_mcmunsubscribed.distribution_id IS NULL)
                        $whereClause
                ) AS telegram_unsubscribe
            ";

        $query = $this->getEntityManager()->createNativeQuery($sql, $rsm);
        return $query->getOneOrNullResult();
    }

    /**
     * @param $userId
     * @param $targetId
     * @param string $whereClause
     * @param string $joinContactinfo
     * @return bool
     * @throws \Doctrine\DBAL\Driver\Exception
     * @throws \Doctrine\DBAL\Exception
     */
    public function insertAvailableForTarget($userId, $targetId, $whereClause = '', $joinContactinfo = '')
    {
        $into = $this->dp->isPostgreSQL100() ? 'INTO' : '';
        $moduser = is_iterable($userId) ? 'user' . $userId . 'w' : 'crontab';
        $sql = "
                INSERT $into info_mcmcontactintarget (contact_id, target_id, moduser)
                SELECT contact_id, target_id, '$moduser' FROM (
                    SELECT DISTINCT $this->tableName.id AS contact_id, $targetId as target_id
                    FROM $this->tableName
                    $joinContactinfo
                    LEFT JOIN info_contactemail ON info_contactemail.contact_id = info_contact.id
                    LEFT JOIN info_targetspec ON info_targetspec.spec_id = info_contact.Specialization_id 
                    LEFT JOIN info_target ON info_target.id = info_targetspec.target_id
                    LEFT JOIN info_company ON info_company.id = info_contact.company_id
                    LEFT JOIN info_mcmunsubscribed ON info_mcmunsubscribed.contact_id = info_contact.id
                    LEFT JOIN info_task ON info_task.contact_id = info_contact.id
                        WHERE COALESCE(info_contact.isarchive, 0) != 1 AND info_contactemail.email IS NOT NULL AND info_mcmunsubscribed.id IS NULL $whereClause
                    GROUP BY $this->tableName.id
                    UNION ALL
                    SELECT DISTINCT $this->tableName.id AS contact_id, $targetId as target_id
                    FROM $this->tableName
                    $joinContactinfo
                    LEFT JOIN info_contactphone ON info_contactphone.contact_id = info_contact.id
                    LEFT JOIN info_targetspec ON info_targetspec.spec_id = info_contact.Specialization_id 
                    LEFT JOIN info_target ON info_target.id = info_targetspec.target_id
                    LEFT JOIN info_company ON info_company.id = info_contact.company_id
                    LEFT JOIN info_mcmunsubscribed ON info_mcmunsubscribed.contact_id = info_contact.id
                    LEFT JOIN info_task ON info_task.contact_id = info_contact.id
                        WHERE COALESCE(info_contact.isarchive, 0) != 1 AND info_contactphone.phone IS NOT NULL AND info_mcmunsubscribed.id IS NULL $whereClause
                    GROUP BY $this->tableName.id
                ) AS q
                GROUP BY q.contact_id, q.target_id
            ";
        return $this->_em->getConnection()->prepare($sql)->execute();
    }

    /**
     * @param string $whereClause
     * @param string $joinContactinfo
     * @return array|int|string
     */
    public function getContactForDistribution($whereClause = '', $joinContactinfo = '')
    {
        $rsm = new ResultSetMappingBuilder($this->_em);
        $rsm->addScalarResult('id', 'id', 'integer');
        $rsm->addScalarResult('morionid', 'morionid', 'integer');
        $rsm->addScalarResult('firstname', 'firstname');
        $rsm->addScalarResult('lastname', 'lastname');
        $rsm->addScalarResult('middlename', 'middlename');
        $rsm->addScalarResult('region', 'region');
        $rsm->addScalarResult('email', 'email', 'integer');
        $rsm->addScalarResult('viber', 'viber', 'integer');
        $rsm->addScalarResult('sms', 'sms', 'integer');

        $sql = "
                SELECT q.id, q.morionid, q.firstname, q.lastname, q.middlename, info_region.name as region, 
                       SUM(q.email) as email, SUM(q.viber) as viber, SUM(q.sms) as sms  
                FROM (
                    SELECT DISTINCT $this->tableName.id, $this->tableName.morionid, $this->tableName.firstname, $this->tableName.lastName, 
                        $this->tableName.middlename, $this->tableName.region_id, MAX(info_contactemail.id) as email, 0 as viber, 0 as sms 
                    FROM $this->tableName
                    $joinContactinfo
                    LEFT JOIN info_contactemail ON info_contactemail.contact_id = info_contact.id
                    LEFT JOIN info_targetspec ON info_targetspec.spec_id = info_contact.Specialization_id 
                    LEFT JOIN info_target ON info_target.id = info_targetspec.target_id 
                    LEFT JOIN info_company ON info_company.id = info_contact.company_id
                    LEFT JOIN info_task ON info_task.contact_id = info_contact.id
                        WHERE COALESCE(info_contact.isarchive, 0) != 1 AND info_contactemail.email IS NOT NULL AND info_contactemail.status = 1 $whereClause
                    GROUP BY $this->tableName.id, $this->tableName.firstname, $this->tableName.lastName, 
                            $this->tableName.middlename, $this->tableName.region_id
                    UNION ALL
                    SELECT DISTINCT $this->tableName.id, $this->tableName.morionid, $this->tableName.firstname, $this->tableName.lastName, 
                           $this->tableName.middlename, $this->tableName.region_id, 0 as email, 
                           nullif(MAX(info_contactphone.isvibermcm), null) as viber, 
                           nullif(MAX(info_contactphone.issmsmcm), null) as sms 
                    FROM $this->tableName
                    $joinContactinfo
                    LEFT JOIN info_contactphone ON info_contactphone.contact_id = info_contact.id
                    LEFT JOIN info_targetspec ON info_targetspec.spec_id = info_contact.Specialization_id 
                    LEFT JOIN info_target ON info_target.id = info_targetspec.target_id
                    LEFT JOIN info_company ON info_company.id = info_contact.company_id
                    LEFT JOIN info_task ON info_task.contact_id = info_contact.id
                        WHERE COALESCE(info_contact.isarchive, 0) != 1 AND info_contactphone.phone IS NOT NULL $whereClause
                    GROUP BY $this->tableName.id, $this->tableName.firstname, $this->tableName.lastName, 
                            $this->tableName.middlename, $this->tableName.region_id
                ) AS q
                LEFT JOIN info_region on info_region.id = q.region_id
                GROUP BY q.id, q.morionid, q.firstname, q.lastName, 
                q.middlename, info_region.name
            ";

        $query = $this->getEntityManager()->createNativeQuery($sql, $rsm);
        return $query->getArrayResult();
    }


    /**
     * @param InfoContactrequest $contactRequest
     * @param string $prevStatus
     * @throws \Doctrine\ORM\ORMException
     */
    public function updateContactByRequest(InfoContactrequest $contactRequest, string $prevStatus = null)
    {
        $contact = $contactRequest->getContact();

        if ($contactRequest->getDcrType() &&
            $contactRequest->getDcrType()->getCode() === InfoDcrtype::NEW_AGENCY_CONTACT) {
            $contactOwnerExists = $this->_em
                ->getRepository(InfoContactowner::class)
                ->findOneBy(['contact' => $contact, 'owner' => $contactRequest->getModifier()]);

            if (!$contactOwnerExists) {
                $contactOwner = new InfoContactowner();
                $contactOwner->setContact($contact);
                $contactOwner->setOwner($contactRequest->getModifier());
                $this->_em->persist($contactOwner);
            }
        }

        $contact->setStatusVerification(3);

        $this->_em->flush();

        if ($contactRequest->getStatusverification2() === 'approved') {
            if ($contact) {
                $property = $contactRequest->getField();
                if ($contactRequest->getField() === 'additional_spec') {
                    $property = 'additionalSpec';
                }
                /**
                 * @var \Doctrine\ORM\Mapping\ClassMetadataInfo $classMetadata
                 */
                $classMetadata = $this->_em->getClassMetadata(InfoContactrequest::class);

                try {
                    $associationClassName = $classMetadata->getAssociationTargetClass($property . 'New');
                } catch (\Exception $e) {
                }

                $newValue = (isset($associationClassName) && !$contactRequest->getNewvalue())
                    ? $classMetadata->getFieldValue($contactRequest, $property . 'New')
                    : $contactRequest->getNewvalue();

                $field = $contactRequest->getField();
                /**
                 * @var \Doctrine\ORM\Mapping\ClassMetadataInfo $contactClassMetadata
                 */
                $contactClassMetadata = $this->_em->getClassMetadata(\TeamSoft\CrmRepositoryBundle\Entity\InfoContact::class);

                if ($field === 'spec') {
                    $field = 'specialization';
                }

                if (!empty($newValue)) {
                    if ($contactRequest->getField() === 'additionalcompany' && $newValue instanceof InfoCompany) {
                        $conn = $this->_em->getConnection();
                        $conn->insert(
                            'info_contactconnection',
                            ['contact_id' => $contact->getId(), 'company_slave_id' => $newValue->getId()]
                        );
                    } else if ($contactRequest->getField() === 'additional_spec' && $newValue instanceof InfoDictionary) {
                        $contactSpec = new InfoContactspec();
                        $contactSpec->setContact($contact);
                        $contactSpec->setSpec($newValue);
                        $this->_em->persist($contactSpec);

                        $contact->addContactSpecializationCollection($contactSpec);
                        $this->_em->flush();
                    } else if ($contactRequest->getField() === 'contactcategory' && $newValue instanceof InfoContactcateg) {
                        $contactCategory = new InfoContactcategory();
                        $contactCategory->setDirection($newValue->getDirection());
                        $contactCategory->setContact($contact);
                        $contactCategory->setCategory($newValue);
                        $this->_em->persist($contactCategory);

                        $contact->addContactCategoryCollection($contactCategory);
                        $this->_em->flush();
                    } else if ($contactRequest->getField() === 'additionalowner' && $newValue instanceof InfoUser) {
                        $contactOwner = new InfoContactowner();
                        $contactOwner->setIsuse(1);
                        $contactOwner->setOwner($newValue);
                        $contactOwner->setContact($contact);
                        $this->_em->persist($contactOwner);

                        $contact->addContactOwnerCollection($contactOwner);
                        $this->_em->flush();
                    } else if ($contactClassMetadata->getFieldName($field)) {
                        $contactClassMetadata->setFieldValue(
                            $contact,
                            $contactClassMetadata->getFieldName($field),
                            $newValue
                        );

                        if ($contactRequest->getField() === 'company' && $newValue instanceof InfoCompany) {
                            $contact->setRegion($newValue->getRegion());
                            $contact->setCity($newValue->getCity());
                            $contact->setDistrict($newValue->getDistrict());
                            $contact->setPostcode($newValue->getPostcode());
                            $contact->setStreet($newValue->getStreet());
                            $contact->setBuilding($newValue->getBuilding());
                            $contact->setStreettype($newValue->getStreettype());
                        }

                        $this->_em->flush($contact);
                    } else {
                        throw new \InvalidArgumentException();
                    }
                } else {
                    $oldValue = (isset($associationClassName) && !$contactRequest->getOldvalue())
                        ? $classMetadata->getFieldValue($contactRequest, $property . 'Old')
                        : $contactRequest->getOldvalue();

                    if (!empty($oldValue)) {
                        if ($contactRequest->getField() === 'additionalcompany' && $oldValue instanceof InfoCompany) {
                            $conn = $this->_em->getConnection();
                            $conn->delete('info_contactconnection', ['contact_id' => $contact->getId(), 'company_slave_id' => $oldValue->getId()]);
                        } else if ($contactRequest->getField() === 'additional_spec') {
                            /**
                             * @var InfoContactspec $contactSpec
                             */
                            $contactSpec = $this->_em
                                ->getRepository(InfoContactspec::class)
                                ->findOneBy(['contact' => $contact, 'spec' => $oldValue]);

                            if ($contactSpec) {
                                $contact->removeContactSpecializationCollection($contactSpec);
                                $this->_em->remove($contactSpec);
                                $this->_em->flush();
                            }
                        } else if ($contactRequest->getField() === 'contactcategory') {
                            /**
                             * @var InfoContactcategory $contactCategory
                             */
                            $contactCategory = $this->_em
                                ->getRepository(InfoContactcategory::class)
                                ->findOneBy(['contact' => $contact, 'category' => $oldValue]);

                            if ($contactCategory) {
                                $contact->removeContactCategoryCollection($contactCategory);
                                $this->_em->remove($contactCategory);
                                $this->_em->flush();
                            }
                        } else if ($contactRequest->getField() === 'additionalowner') {
                            /**
                             * @var InfoContactowner $additionalOwner
                             */
                            $additionalOwner = $this->_em
                                ->getRepository(InfoContactowner::class)
                                ->findOneBy(['contact' => $contact, 'owner' => $oldValue]);

                            if ($additionalOwner) {
                                $contact->removeContactOwnerCollection($additionalOwner);
                                $this->_em->remove($additionalOwner);
                                $this->_em->flush();
                            }
                        } else if ($contactClassMetadata->getFieldName($field)) {
                            $contactClassMetadata->setFieldValue(
                                $contact,
                                $contactClassMetadata->getFieldName($field),
                                null
                            );

                            $this->_em->flush();
                        }
                    }
                }
            }
        } else if ($contactRequest->getStatusverification2() === 'reject') {
            if ($contactRequest->getField() === 'company') {
                $oldValue = $contactRequest->getCompanyOld();
                if ($oldValue instanceof InfoCompany) {
                    $contact->setRegion($oldValue->getRegion());
                    $contact->setCity($oldValue->getCity());
                    $contact->setDistrict($oldValue->getDistrict());
                    $contact->setPostcode($oldValue->getPostcode());
                    $contact->setStreet($oldValue->getStreet());
                    $contact->setBuilding($oldValue->getBuilding());
                    $contact->setStreettype($oldValue->getStreettype());
                }
            }

            if ($prevStatus === 'create' &&
                (!$contact->getArchivereason() || $contact->getArchivereason()->getName() !== 'Не прошел верификацию')) {
                /**
                 * @var InfoArchivereason $archiveReason
                 */
                $archiveReason = $this
                    ->_em
                    ->getRepository(InfoArchivereason::class)
                    ->findOneBy(['name' => 'Не прошел верификацию']);

                if ($archiveReason) {
                    $contact->setArchivereason($archiveReason);
                    $this->_em->flush($contact);
                }
            }
        }
    }

    /**
     * @return array
     */
    public function getStatisticsForDistributions($userId, $countryId = null)
    {
        $rsm = new ResultSetMappingBuilder($this->_em);

        $rsm->addScalarResult('name', 'targetGroup');
        $rsm->addScalarResult('sms', 'phoneTotal');
        $rsm->addScalarResult('viber', 'viberTotal');
        $rsm->addScalarResult('email', 'emailTotal');
        $rsm->addScalarResult('whatsapp', 'whatsappTotal');
        $rsm->addScalarResult('telegram', 'telegramTotal');
        $rsm->addScalarResult('total', 'total');

        $countrySubquery = $countryId ? " and info_contact.country_id = $countryId " : "";

        $sql = "select target.id
                 , target.name
                 , sum(email) as email
                 , sum(sms) as sms
                 , sum(viber) as viber
                 , sum(whatsapp) as whatsapp
                 , sum(telegram) as telegram
                from
                (
                    select contact_id
                         , status as email
                         , 0 as sms
                         , 0 as viber
                         , 0 as whatsapp
                         , 0 as telegram
                    from info_contactemail
                    where coalesce(status, 0) = 1
                    union all
                    select contact_id
                         , 0 as email
                         , coalesce(issmsmcm, 0) as sms
                         , coalesce(isvibermcm, 0) as viber
                         , coalesce(iswhatsappmcm, 0) as whatsapp
                         , coalesce(istelegrammcm, 0) as telegram
                        from info_contactphone
                    ) cnt_chanels
                        inner join info_contact on 	info_contact.id = cnt_chanels.contact_id and coalesce(info_contact.isarchive, 0) = 0 $countrySubquery
                        inner join
                    (
                        select info_target.id, info_target.name, info_targetspec.spec_id
                        from info_target
                                 inner join info_targetspec on info_target.id = info_targetspec.target_id
                        where
                                info_target.direction_id in (select direction_id from info_userdirection where user_id = $userId)
                           or info_target.direction_id is null
                           or (select 1 from info_user u, info_role r where u.id = $userId and u.role_id = r.id and r.code = 'ROLE_SUPER_ADMIN') is not null
                    ) as target on target.spec_id = info_contact.specialization_id
                    group by  target.id, target.name";

        $query = $this->getEntityManager()->createNativeQuery($sql, $rsm);

        return $query->getResult();
    }

    /**
     * @param \TeamSoft\CrmRepositoryBundle\Entity\InfoMcmtarget[] $targetAudiences
     * @return array
     */
    public function findByTargetAudiences($targetAudiences)
    {
        $tableName = $this->getClassMetadata()->getTableName();

        $targetAudienceIds = [];

        /**
         * @var \TeamSoft\CrmRepositoryBundle\Entity\InfoMcmtarget $targetAudience
         */
        foreach ($targetAudiences as $targetAudience) {
            $targetAudienceIds[] = $targetAudience->getId();
        }

        $rsm = $this->createResultSetMappingBuilder('c');
        $rsm->addScalarResult('mcmvariable_id', 'mcmvariableId', 'integer');
        $rsm->addScalarResult('stringValue', 'stringValue', 'string');

        $selectClause = $rsm->generateSelectClause();

        $sql = "
            SELECT 
              $selectClause,
              cv.mcmvariable_id,
              cv.stringValue
            FROM $tableName c
            INNER JOIN info_mcmcontactintarget cit ON cit.contact_id = c.id
            LEFT JOIN info_mcmcontactvariables cv ON cv.contact_id = c.id
            WHERE cit.target_id IN (:target_ids)
        ";

        $sql = str_replace(':target_ids', implode(',', $targetAudienceIds), $sql);

        $query = $this->getEntityManager()->createNativeQuery($sql, $rsm);

        return $query->getResult();
    }

    /**
     * Return list of contacts, which email are not verified
     * @return array|boolean
     */
    public function getUnprocessedContacts()
    {
        $count = $this
            ->createQueryBuilder('c')
            ->select('COUNT(c.id)')
            ->leftJoin('c.contactEmail', 'ce')
            ->where('ce.contact IS NULL')
            ->andWhere("c.eaddr <> ''")
            ->getQuery()
            ->getSingleScalarResult();

        if (!$count) {
            return false;
        }

        $result = $this
            ->createQueryBuilder('c')
            ->leftJoin('c.contactEmail', 'ce')
            ->where('ce.contact IS NULL')
            ->andWhere("c.eaddr <> ''")
            ->getQuery()
            ->iterate();

        return [
            'count' => $count,
            'data' => $result,
        ];
    }

    /**
     * @param int $correction
     * @return array
     */
    public function findByBirthday($correction)
    {
        $currentTime = new \DateTime();
        $correctedTime = $currentTime->modify(intval($correction) * -1 . " day");

        $tableName = $this->getClassMetadata()->getTableName();

        $rsm = new ResultSetMappingBuilder($this->_em);
        $rsm->addRootEntityFromClassMetadata(
            $this->_entityName,
            'c',
            [],
            ResultSetMappingBuilder::COLUMN_RENAMING_INCREMENT
        );
        $rsm->addScalarResult('contact_id', 'contact', 'integer');

        $selectClause = $rsm->generateSelectClause();

        $sql = "
            SELECT 
              $selectClause,
              c.id
            FROM $tableName c
            WHERE DAY(c.BirthDate) = :day AND MONTH(c.BirthDate) = :month
        ";

        $sql = str_replace(':day', $correctedTime->format('d'), str_replace(':month', $correctedTime->format('m'), $sql));

        $query = $this->getEntityManager()->createNativeQuery($sql, $rsm);

        return $query->getResult();
    }

    /**
     * @return array
     */
    public function getCountWithContacts($countryId = null)
    {
        $rsm = new ResultSetMappingBuilder($this->_em);

        $rsm->addScalarResult('result', 'result');

        $countrySubquery = $countryId ? " AND info_contact.country_id = $countryId " : "";

        $sql = "
                SELECT COUNT(DISTINCT res.id) as result
				FROM 
					(
						SELECT info_contact.id
						FROM info_contact
						LEFT JOIN info_contactemail ON info_contactemail.contact_id = info_contact.id
						WHERE COALESCE(info_contact.isarchive, 0) != 1 AND info_contactemail.status = 1 AND 
						info_contactemail.email IS NOT NULL $countrySubquery
					
					UNION
					
						SELECT info_contact.id
						FROM info_contact
						LEFT JOIN info_contactphone ON info_contactphone.contact_id = info_contact.id
						WHERE COALESCE(info_contact.isarchive, 0) != 1 AND info_contactphone.issmsmcm = 1 AND 
						      info_contactphone.phone IS NOT NULL $countrySubquery
					
					UNION
					
						SELECT info_contact.id
						FROM info_contact
						LEFT JOIN info_contactphone ON info_contactphone.contact_id = info_contact.id
						WHERE COALESCE(info_contact.isarchive, 0) != 1 AND info_contactphone.isvibermcm = 1 AND 
						      info_contactphone.phone IS NOT NULL $countrySubquery
					) as res
            ";

        $query = $this->getEntityManager()->createNativeQuery($sql, $rsm);

        return $query->getResult();
    }

    /**
     * Can call be reserved for contact
     *
     * @param InfoRtcRoomReserve $RTCRoomReserve
     * @return bool
     */
    public function isReserveCallAvailable(InfoRtcRoomReserve $RTCRoomReserve)
    {
        $qb = $this->createQueryBuilder('c')
            ->innerJoin('c.rtcRoomReserves', 'rtcRoomReserve');
        $qb->where($qb->expr()->orX(
            $qb->expr()->between(':datefrom', 'rtcRoomReserve.datefrom', 'rtcRoomReserve.datetill'),
            $qb->expr()->between(':datetill', 'rtcRoomReserve.datefrom', 'rtcRoomReserve.datetill')
        ))->setParameter('datefrom', $RTCRoomReserve->getDatefrom())
            ->setParameter('datetill', $RTCRoomReserve->getDatetill())
            ->setMaxResults(1);

        if ($RTCRoomReserve->getId()) {
            $qb->andWhere($qb->expr()->neq('rtcRoomReserve', ':rtcRoomReserve'))
                ->setParameter('rtcRoomReserve', $RTCRoomReserve);
        }

        return !count($qb->getQuery()->getResult());
    }

    /**
     * Is rtc available for contact
     *
     * @param InfoContact $contact
     * @return bool
     */
    public function isRtcAvailable(InfoContact $contact)
    {
        $isSmsMcm = false;

        if ($contact) {
            foreach ($contact->getContactPhoneCollection() as $contactphone) {
                $isSmsMcm = $isSmsMcm || $contactphone->getIssmsmcm();
            }
        }

        return $isSmsMcm;
    }

    private function getSqlGeneratorData(string $alias): array
    {
        $c = $this->dp->getConcatOperator();

        $crStatusVerificationJoin = function () use ($alias) {
//  commented according to https://teamsoft.atlassian.net/browse/ER-10
//            $whereUsersToFilter = '';
//            $usersToFilter = [];
//            if ($this->options->get(Options::OPTION_SHOW_DCR_REQUESTS_BY_SUBMISSION)) {
//                /** @var InfoUser $user */
//                $user = $this->security->getUser();
//                $usersToFilter = array_merge([$user], $user->getSubordinates()->getValues());
//            } else if ($this->options->get(Options::OPTION_SEARCH_FOR_COMPANY_REQUESTS_BY_MODIFIER_ID)) {
//                $usersToFilter = [$this->security->getUser()];
//            }
//
//            if ($usersToFilter) {
//                $userIds = array_map(function (InfoUser $u) {
//                    return $u->getId();
//                }, $usersToFilter);
//
//                $whereUsersToFilter = 'AND modifier_id IN (' . implode(',', $userIds) . ')';
//            }

            $whereUsersToFilter = '';
            return "
                LEFT JOIN (
                    SELECT contact_id, modifier_id, modified, statusverification
                    FROM
                    (
                        SELECT statusverification, contact_id, modified, rank() OVER (PARTITION BY contact_id ORDER BY modified DESC, count(*) DESC) AS rnk, modifier_id
                        FROM info_contactrequest
                        WHERE 1=1 {$whereUsersToFilter}
                        GROUP BY statusverification, contact_id, modified, modifier_id
                    ) t
                    WHERE rnk = 1
                ) info_contactrequest ON info_contactrequest.contact_id = info_contact.id";
        };

        return [
            "lastname" => [
                "order_by" => "$alias.lastname",
                "where" => "LOWER($alias.lastname) LIKE LOWER(N'%{0}%')",
                'map_types' => 'string',
            ],
            "firstname" => [
                "order_by" => "$alias.firstname",
                "where" => "LOWER($alias.firstname) LIKE LOWER(N'%{0}%')",
                'map_types' => 'string',
            ],
            "middlename" => [
                "order_by" => "$alias.middlename",
                "where" => "LOWER($alias.middlename) LIKE LOWER(N'%{0}%')",
                'map_types' => 'string',
            ],
            'specialization' => [
                'select' => ['specialization'],
                'order_by' => 'LOWER(specialization.name)',
                'joins' => [
                    "LEFT OUTER JOIN info_dictionary AS specialization ON specialization.id = {$alias}.specialization_id",
                ],
                'where' => 'specialization.id IN ({0})',
                'map_types' => 'array',
            ],
            'companyname' => [
                'select' => ['company'],
                'order_by' => 'LOWER(company.name)',
                'joins' => [
                    "LEFT JOIN info_company AS company ON company.id = {$alias}.company_id",
                ],
                'where' => "company.id IN ({0})",
                'map_types' => 'array',
            ],
            'address' => [
                'select' => ['streettype_id', 'street', 'building', 'city_id', 'region_id', 'country_id'],
                "order_by" => "LOWER(COALESCE(info_city.name, '') $c ' ' $c COALESCE(streetType.name, '') $c ' ' $c COALESCE($alias.street, '') $c ' ' $c COALESCE($alias.building, ''))",
                "joins" => [
                    "LEFT OUTER JOIN info_city ON info_city.id = $alias.city_id",
                    "LEFT OUTER JOIN info_dictionary AS streetType ON streetType.id = $alias.streettype_id",
                ],
                "where" => "LOWER(COALESCE(info_city.name, '') $c ' ' $c COALESCE(streetType.name, '') $c ' ' $c COALESCE($alias.street, '') $c ' ' $c COALESCE($alias.building, '')) LIKE LOWER(N'%{0}%')",
                'map_types' => 'string',
            ],
            'phone1' => [
                "order_by" => "$alias.phone1",
                "where" => "LOWER($alias.phone1) LIKE LOWER(N'%{0}%')",
                'map_types' => 'string',
            ],
            'phone2' => [
                "order_by" => "$alias.phone2",
                "where" => "LOWER($alias.phone2) LIKE LOWER(N'%{0}%')",
                'map_types' => 'string',
            ],
            'eaddr' => [
                "order_by" => "$alias.eaddr", //email
                "where" => "LOWER($alias.eaddr) LIKE LOWER(N'%{0}%')",
                'map_types' => 'string',
            ],
            'cr_status_verification' => [
                'order_by' => 'cr_status_verification',
                "extra_select" => [
                    [
                        'field' => "info_contactrequest.statusverification",
                        'alias' => "cr_status_verification",
                        'map_types' => "integer",
                    ],
                ],
                'joins' => [
                    $crStatusVerificationJoin,
                ],
                "where" => "info_contactrequest.statusverification IN ({0})",
                'map_types' => 'array',
            ],
            'modifier_user_name' => [
                "extra_select" => [
                    [
                        'field' => "modifier_user.name",
                        'alias' => "modifier_user_name",
                        'map_types' => "string",
                    ],
                ],
                'order_by' => 'modifier_user_name',
                'joins' => [
                    $crStatusVerificationJoin,
                    'LEFT OUTER JOIN info_user AS modifier_user ON (modifier_user.id = info_contactrequest.modifier_id)',
                ],
                "where" => "LOWER(modifier_user.name) LIKE LOWER(N'%{0}%')",
                'map_types' => 'string',
            ],
            'get_verification' => [
                'select' => ['getverification'],
                'order_by' => "{$alias}.getverification",
                "where" => function (?array $values) use ($alias) {
                    if (!isset($values['dateFrom'], $values['dateTo'])) {
                        return '';
                    }

                    $dateFrom = DateHelper::toDate($values['dateFrom'])->format('Y-m-d');
                    $dateTo = DateHelper::toDate($values['dateTo'])->format('Y-m-d');

                    return "{$alias}.getverification BETWEEN '{$dateFrom}' AND '{$dateTo} 23:59:59'";
                },
            ],
            "category" => [
                'select' => [
                    !$this->useCategoryWithDirection ? 'category_id' : null,
                ],
                "extra_select" => [
                    $this->useCategoryWithDirection ? [
                        'field' => "info_contactcateg.name",
                        'alias' => "contact_category_name",
                        'map_types' => "string",
                    ] : null,
                ],
                "order_by" => "info_contactcateg.name",
                "joins" => [
                    function () use ($alias) {
                        if ($this->useCategoryWithDirection) {
                            if (!$this->dp->isSQLServer2008()) {
                                return [
                                    "LEFT JOIN (" .
                                        "SELECT STRING_AGG(info_contactcateg.name, ' - ') AS name, contact_id\n" .
                                        "FROM info_contactcategory\n" .
                                        "INNER JOIN info_contactcateg ON info_contactcateg.id = info_contactcategory.category_id\n" .
                                        "GROUP BY contact_id\n" .
                                    ") info_contactcateg ON info_contactcateg.contact_id = {$alias}.id"
                                ];
                            } else {
                                return [
                                    "LEFT JOIN (" .
                                        "SELECT MAX(info_contactcateg.name) AS name, contact_id\n" .
                                        "FROM info_contactcategory\n" .
                                        "INNER JOIN info_contactcateg ON info_contactcateg.id = info_contactcategory.category_id\n" .
                                        "GROUP BY contact_id\n" .
                                    ") info_contactcateg ON info_contactcateg.contact_id = {$alias}.id"
                                ];
                            }
                        } else {
                            return ["LEFT OUTER JOIN info_contactcateg ON info_contactcateg.id = {$alias}.Category_id "];
                        }
                    },
                ],
                "where" => function () {
                    if ($this->useCategoryWithDirection) {
                        return "info_contact.id IN (SELECT contact_id FROM info_contactcategory WHERE category_id IN ({0}))";
                    } else {
                        return "info_contactcateg.id IN ({0})";
                    }
                },
                'map_types' => 'array',
            ],
            "is_archive" => [
                'select' => ['isarchive'],
                "order_by" => "COALESCE($alias.isarchive, 0)",
                "where" => function () use ($alias) {
                    $where = "COALESCE($alias.isarchive, 0) = {0}";
                    if ($this->mode === self::DCR_MODE) {
                        $where .= " AND $alias.morionid IS NOT NULL";
                    }
                    return $where;
                },
                'map_types' => 'boolean',
            ],
            'contacttype' => [
                'select' => ['contacttype_id'],
                'joins' => [
                    "LEFT OUTER JOIN info_contacttype contacttype ON contacttype.id = {$alias}.contacttype_id ",
                ],
                "where" => "contacttype.id IN ({0})",
                'order_by' => 'contacttype.name',
                'map_types' => 'array',
            ],
            'contactspec' => [
                'joins' => [
                    "LEFT OUTER JOIN info_contactspec AS contactspec ON contactspec.contact_id = {$alias}.id ",
                    "LEFT OUTER JOIN info_dictionary AS contact_specialization ON contact_specialization.id = contactspec.spec_id ",
                ],
                'order_by' => 'contact_specialization.name',
                'where' => 'contact_specialization.id IN ({0})',
                'map_types' => 'array',
            ],
            'position' => [
                'select' => ['position_id'],
                'joins' => [
                    "LEFT OUTER JOIN info_dictionary AS position ON position.id = {$alias}.position_id",
                ],
                'order_by' => 'position.name',
                'where' => 'position.id IN ({0})',
                'map_types' => 'array',
            ],
            'postcode' => [
                'order_by' => "LOWER({$alias}.postcode)",
                'where' => "LOWER({$alias}.postcode) LIKE LOWER(N'%{0}%')",
                'map_types' => 'string',
            ],
            'owner' => [
                'select' => [
                    'Owner_id',
                ],
                'joins' => [
                    "LEFT OUTER JOIN info_user AS responsible ON responsible.id = {$alias}.Owner_id",
                ],
                'order_by' => "LOWER(responsible.name)",
                'where' => "LOWER(responsible.name) LIKE LOWER(N'%{0}%')",
                'map_types' => 'string',
            ],
            'contactowner' => [
                'joins' => [
                    "LEFT OUTER JOIN info_contactowner AS cw ON cw.contact_id = {$alias}.id",
                    "LEFT OUTER JOIN info_user AS contactowner ON contactowner.id = cw.Owner_id",
                ],
                'order_by' => "LOWER(contactowner.name)",
                'where' => "LOWER(contactowner.name) LIKE LOWER(N'%{0}%')",
                'map_types' => 'string',
            ],
            'legal_name' => [
                'order_by' => "LOWER({$alias}.legal_name)",
                'where' => "LOWER({$alias}.legal_name) LIKE LOWER(N'%{0}%')",
                'map_types' => 'string',
            ],
            'reg_address' => [
                'select' => ['regaddress'],
                'order_by' => "LOWER({$alias}.regaddress)",
                'where' => "LOWER({$alias}.regaddress) LIKE LOWER(N'%{0}%')",
                'map_types' => 'string',
            ],
            'insurance_certificate' => [
                'order_by' => "LOWER({$alias}.insurance_certificate)",
                'where' => "LOWER({$alias}.insurance_certificate) LIKE LOWER(N'%{0}%')",
                'map_types' => 'string',
            ],
            'identification_number' => [//bic
                'order_by' => "LOWER({$alias}.identification_number)",
                'where' => "LOWER({$alias}.identification_number) LIKE LOWER(N'%{0}%')",
                'map_types' => 'string',
            ],
            'bank' => [
                'order_by' => "LOWER({$alias}.bank)",
                'where' => "LOWER({$alias}.bank) LIKE LOWER(N'%{0}%')",
                'map_types' => 'string',
            ],
            'bic' => [
                'order_by' => "LOWER({$alias}.bic)",
                'where' => "LOWER({$alias}.bic) LIKE LOWER(N'%{0}%')",
                'map_types' => 'string',
            ],
            'correspondent_account' => [
                'order_by' => "LOWER({$alias}.correspondent_account)",
                'where' => "LOWER({$alias}.correspondent_account) LIKE LOWER(N'%{0}%')",
                'map_types' => 'string',
            ],
            'snils' => [
                'order_by' => "LOWER({$alias}.snils)",
                'where' => "LOWER({$alias}.snils) LIKE LOWER(N'%{0}%')",
                'map_types' => 'string',
            ],
            'kpp' => [
                'order_by' => "LOWER({$alias}.kpp)",
                'where' => "LOWER({$alias}.kpp) LIKE LOWER(N'%{0}%')",
                'map_types' => 'string',
            ],
            'passport_seriesnumber' => [
                'order_by' => "LOWER({$alias}.passport_seriesnumber)",
                'where' => "LOWER({$alias}.passport_seriesnumber) LIKE LOWER(N'%{0}%')",
                'map_types' => 'string',
            ],
            'passport_date' => [
                'order_by' => "{$alias}.passport_date",
                "where" => function (?array $values) use ($alias) {
                    if (!isset($values['dateFrom'], $values['dateTo'])) {
                        return '';
                    }

                    $dateFrom = DateHelper::toDate($values['dateFrom'])->format('Y-m-d');
                    $dateTo = DateHelper::toDate($values['dateTo'])->format('Y-m-d');

                    return "{$alias}.passport_date BETWEEN '{$dateFrom}' AND '{$dateTo} 23:59:59'";
                },
            ],
            'passport_issued' => [
                'order_by' => "LOWER({$alias}.passport_issued)",
                'where' => "LOWER({$alias}.passport_issued) LIKE LOWER(N'%{0}%')",
                'map_types' => 'string',
            ],
            'legal_status' => [
                'joins' => [
                    "LEFT OUTER JOIN info_CustomDictionaryValue AS legalStatus ON legalStatus.id = {$alias}.legal_status",
                ],
                'order_by' => "LOWER(legalStatus.DictionaryValue)",
                'where' => "legalStatus.id IN ({0})",
                'map_types' => 'array',
            ],
            'interaction' => [
                'joins' => [
                    "LEFT OUTER JOIN info_CustomDictionaryValue AS inter ON inter.id = {$alias}.interaction",
                ],
                'order_by' => "LOWER(inter.DictionaryValue)",
                'where' => "inter.id IN ({0})",
                'map_types' => 'array',
            ],
            'city' => [
                'joins' => [
                    "LEFT OUTER JOIN info_city AS city ON city.id = {$alias}.City_id",
                ],
                'order_by' => "LOWER(city.name)",
                'where' => "city.id IN ({0})",
                'map_types' => 'array',
            ],
            'region' => [
                'joins' => [
                    "LEFT OUTER JOIN info_region AS region ON region.id = {$alias}.Region_id",
                ],
                'order_by' => "LOWER(region.name)",
                'where' => "region.id IN ({0})",
                'map_types' => 'array',
            ],
            "filters" => [
                "where" => function (?array $values) use ($alias) {
                    $where = '';
                    if (empty($values) && $this->mode === self::DCR_MODE) {
                        $where .= "$alias.morionid IS NOT NULL";
                    }
                    return $where;
                },
            ],
            "search" => [
                "joins" => [
                    "LEFT OUTER JOIN info_contacttype ON info_contacttype.id = $alias.ContactType_id",
                    "LEFT OUTER JOIN info_city AS city ON city.id = {$alias}.City_id",
                    "LEFT OUTER JOIN info_dictionary AS streetType ON streetType.id = $alias.streettype_id",
                    "LEFT OUTER JOIN info_dictionary AS specialization ON specialization.id = {$alias}.specialization_id",
                    "LEFT JOIN info_company AS company ON company.id = {$alias}.company_id",
                ],
                "where" => function (array $value) use ($alias) {
                    $c = $this->dp->getConcatOperator();

                    $names = [
                        $alias . '.firstname',
                        $alias . '.middlename',
                        $alias . '.lastname',
                        'info_contacttype.name',
                        'city.name',
                        $alias . '.street',
                        $alias . '.building',
                        'streetType.name',
                        'specialization.name',
                        'company.name'
                    ];

                    if ($this->mode == self::DCR_MODE) {
                        $names[] = 'CAST(' . $alias . '.morionid as varchar)';
                        $names[] = 'CAST(' . $alias . '.case_number as varchar)';
                    }

                    $where = 'LOWER(';

                    foreach ($names as $index => $name) {
                        if ($index) {
                            $where .= " {$c} ' ' {$c} ";
                        }

                        $where .= "COALESCE({$name}, '')";
                    }

                    $where .= ") LIKE LOWER(N'%%{0}%%')";

                    $sql = [];
                    foreach ($value as $item) {
                        $sql[] = str_replace('{0}', (string)$item, $where);
                    }

                    return implode(' AND ', $sql);
                },
            ],
            "owner_id" => [
                "where" => "(
                        $alias.owner_id = {0}
                        OR
                        EXISTS(SELECT 1 FROM info_contactowner co WHERE co.contact_id = $alias.id and co.owner_id = {0} AND co.isuse = 1)
                    )",
                'map_types' => 'integer',
            ],
            "owner_id_with_submission" => [
                "where" => "(
                $alias.owner_id = {0} OR EXISTS(SELECT 1 FROM info_contactowner co WHERE co.contact_id = $alias.id and co.owner_id = {0} AND co.isuse = 1)
                ) 
                    AND EXISTS(SELECT 1 FROM info_contactSubmission WHERE child_id = {0} AND subj_id = {1})",
                'map_types' => [
                    'integer',
                    'integer',
                ],
            ],
            "user_id" => [
                'joins' => [
                    "LEFT OUTER JOIN info_contacttype ON info_contacttype.id = $alias.ContactType_id",
                ],
                "where" => "($alias.owner_id = {0}) AND (COALESCE(info_contacttype.isblogger, 0) = 0 OR {$alias}.id IN (
                                        SELECT contact_id FROM info_contactowner
                                        WHERE owner_id = {0}
                                        OR owner_id IN (SELECT child_id FROM info_contactsubmission WHERE subj_id = {0})
                                    ))",
                'map_types' => 'integer',
            ],
            "user_id_visible_region" => [
                'joins' => [
                    "LEFT OUTER JOIN info_contacttype ON info_contacttype.id = $alias.ContactType_id",
                ],
                "where" => "(
                    {$alias}.id in (
                    select c.id from info_contact c
                        inner join info_regioninuser r on r.region_id = c.Region_id and r.user_id = {0}
                    union
                    select c.id from info_contact c
                        inner join info_regioninuser r on r.region_id = c.Region_id
                        inner join info_contactsubmission cs on cs.child_id = r.user_id and subj_id = {0}
                    union
                    select c.id from info_contact c
                         where owner_id in (
                             select child_id from info_contactsubmission where subj_id = {0}
                             union
                             select {0}
                         )
                    union
                    select contact_id as id from info_contactowner co
                        where owner_id in  (
                            select child_id from info_contactsubmission where subj_id = {0}
                            union
                            select {0}
                        )
                    )
                   AND (COALESCE(info_contacttype.isblogger, 0) = 0 OR {$alias}.id IN (
                                        SELECT contact_id FROM info_contactowner
                                        WHERE owner_id = {0}
                                        OR owner_id IN (SELECT child_id FROM info_contactsubmission WHERE subj_id = {0})
                                    ))
                )",
                'map_types' => 'integer',
            ],
            "contact_sign_createdate" => [
                'select' => ['contactsign.createdate'],
                'order_by' => 'contactsign.createdate',
                'joins' => [
                    "LEFT JOIN info_contactsign AS contactsign ON contactsign.contact_id = {$alias}.id AND COALESCE(contactsign.isarchive, 0) = 0 AND contactsign.signtype = 'sign'",
                ],
                "where" => function (?array $values) use ($alias) {
                    if (!isset($values['dateFrom'], $values['dateTo'])) {
                        return '';
                    }

                    $dateFrom = DateHelper::toDate($values['dateFrom'])->format('Y-m-d');
                    $dateTo = DateHelper::toDate($values['dateTo'])->format('Y-m-d');

                    return "contactsign.createdate BETWEEN '{$dateFrom}' AND '{$dateTo} 23:59:59'";
                }
            ]
        ];
    }
}
