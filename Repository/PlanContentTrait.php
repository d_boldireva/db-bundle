<?php

namespace TeamSoft\CrmRepositoryBundle\Repository;

use Doctrine\ORM\Query\ResultSetMappingBuilder;
use TeamSoft\CrmRepositoryBundle\Model\PlanFilter;

/**
 * Trait PlanContentTrait
 * @package TeamSoft\CrmRepositoryBundle\Repository
 *
 * @property \Doctrine\ORM\EntityManagerInterface $_em
 * @property string $_entityName
 * @method  \Doctrine\ORM\Mapping\ClassMetadata getClassMetadata
 */
trait PlanContentTrait
{
    /**
     * @see InfoTask::getTaskNumQuery()
     * @param PlanFilter $filter
     * @return mixed
     */
    public function findWithDetailsByFilter(PlanFilter $filter, bool $usePlanTaskPeriodOption = false)
    {
        $tableName = $this->getClassMetadata()->getTableName();

        $rsm = new ResultSetMappingBuilder($this->_em);
        $rsm->addRootEntityFromClassMetadata($this->_entityName, $tableName, array(), ResultSetMappingBuilder::COLUMN_RENAMING_INCREMENT);

        $selectClause = $rsm->generateSelectClause();
        $planId = $filter->getPlan() ? $filter->getPlan()->getId() : 0;
        $tasktypeId = $filter->getTasktype() ? $filter->getTasktype()->getId() : 0;
        $companyId = $filter->getCompany() ? $filter->getCompany()->getId() : 0;
        $contactId = $filter->getContact() ? $filter->getContact()->getId() : 0;
        $specializationId = $filter->getSpecialization() ? $filter->getSpecialization()->getId() : 0;

        $isNoContactIsNull = $this->dp->getIsNullFunctionExpression('info_target.isnocontact', 0);

        $andSelect = '';
        //if not set company and contact return all models from plan(promo|preparation)
        if ($planId && $tasktypeId && ($filter->getCompany() || $filter->getContact())) {
            $andSelect = ' AND (' . $tableName . '.ntask IS NULL OR ' . $tableName . '.ntask = (';
            if ($usePlanTaskPeriodOption) {
                $subQuery = "SELECT ptp.num as ntask from info_plantaskperiod ptp where ptp.plandetail_id = info_plandetail.id and '"
                    . $filter->getDatetill()->format('Y-m-d') . "' between ptp.dt1 and ptp.dt2";
            } else {
                $subQuery = "SELECT ntask FROM {$tableName} WHERE plandetail_id=info_plandetail.id AND ntask <= (";
                $subQuery .= 'SELECT COUNT (t.id) + 1 AS nextCount FROM info_task AS t INNER JOIN info_tasktype AS tt ON (tt.id=t.tasktype_id) WHERE ';
                $subQuery .= 't.plan_id = ' . $planId;
                $subQuery .= ' AND t.tasktype_id = ' . $tasktypeId;
                $subQuery .= ' AND t.taskstate_id IN (SELECT id FROM info_taskstate WHERE (isclosed = 1 AND isfinish = 1) OR (isclosed = 0 AND isfinish = 0))';
                $subQuery .= " AND t.datefrom < '" . $filter->getDatetill()->format('Y-m-d H:i:s') . "'";

                /**
                 * showname=1 company and contact
                 * showname=2 company
                 * showname=3 contact
                 */
                $subQuery .= ' AND (t.company_id = ' . $companyId . ' OR ' . $this->dp->getIsNullFunctionExpression('tt.showname', 2) . ' = 3)';
                $subQuery .= ' AND (contact_id = ' . $contactId . ' OR ' . $this->dp->getIsNullFunctionExpression('tt.showname', 2) . ' = 2)';

                $subQuery .= ') ORDER BY ntask DESC';
            }

            $subQuery = $this->dp->modifyOnlyLimitQuery($subQuery, 1);

            $andSelect .= $subQuery . '))';
        }

        $sql = "
            SELECT {$selectClause}
            FROM {$tableName}
            inner join info_plandetail on info_plandetail.id = {$tableName}.plandetail_id and info_plandetail.plan_id = $planId and info_plandetail.tasktype_id = $tasktypeId
			inner join info_target on info_plandetail.target_id = info_target.id
			left join info_targetspec on info_plandetail.target_id = info_targetspec.target_id
			where  (info_targetspec.spec_id = $specializationId or $isNoContactIsNull = 1)
			and (info_plandetail.category_id is null or info_plandetail.category_id = (select category_id from info_contact where info_contact.id = $contactId))
			and (info_plandetail.categorycompany_id is null or info_plandetail.categorycompany_id = (select category_id from info_company where info_company.id = $companyId))
			{$andSelect}
        ";


        $query = $this->_em->createNativeQuery($sql, $rsm);

        $result = $query->getResult();

        return $result;
    }
}
