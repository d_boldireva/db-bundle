<?php
declare(strict_types=1);

namespace TeamSoft\CrmRepositoryBundle\Repository;

use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\QueryBuilder;
use Doctrine\Persistence\ManagerRegistry;
use TeamSoft\CrmRepositoryBundle\Service\DatabasePlatform;
use TeamSoft\CrmRepositoryBundle\Service\Options;
use TeamSoft\CrmRepositoryBundle\Entity\InfoCalendar;
use TeamSoft\CrmRepositoryBundle\Utils\DateHelper;

/**
 * Class InfoCalendarRepository
 * @package TeamSoft\CrmRepositoryBundle\Repository
 *
 * @method InfoCalendar|null find($id, $lockMode = null, $lockVersion = null)
 * @method InfoCalendar|null findOneBy(array $criteria, array $orderBy = null)
 * @method InfoCalendar[]    findAll()
 * @method InfoCalendar[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class InfoCalendarRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, InfoCalendar::class);
    }

    /**
     * @param string $fromDay
     * @param int $skipDays
     * @return InfoCalendar
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function findFirstWorkingDay($fromDay = 'today', $skipDays = 0): ?InfoCalendar
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.dateFrom >= :from')->setParameter('from', DateHelper::toDate($fromDay))
            ->andWhere('c.workday = 1')
            ->orderBy('c.dateFrom', 'ASC')
            ->setFirstResult($skipDays)
            ->setMaxResults(1)
            ->getQuery()
            ->getOneOrNullResult();
    }

    /**
     * @param string $fromDay
     * @return InfoCalendar|null
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function findPrevWorkingDay($fromDay = 'today'): ?InfoCalendar
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.dateFrom < :from')->setParameter('from', DateHelper::toDate($fromDay))
            ->andWhere('c.workday = 1')
            ->orderBy('c.dateFrom', 'DESC')
            ->setMaxResults(1)
            ->getQuery()
            ->getOneOrNullResult();
    }

    public function getLastWorkingDayInMonth(\DateTime $dateTime): ?InfoCalendar
    {
        return $this->getMonthQuery($dateTime)
            ->andWhere('c.workday = 1')
            ->setMaxResults(1)
            ->orderBy('c.dateFrom', 'DESC')
            ->getQuery()
            ->getOneOrNullResult();
    }

    public function getFirstWorkingDayInMonth(\DateTime $dateTime): ?InfoCalendar
    {
        return $this->getMonthQuery($dateTime)
            ->andWhere('c.workday = 1')
            ->setMaxResults(1)
            ->orderBy('c.dateFrom', 'ASC')
            ->getQuery()
            ->getOneOrNullResult();
    }

    public function getMonthQuery(\DateTime $dateTime): QueryBuilder
    {
        return $this->createQueryBuilder('c')
            ->andWhere('YEAR(c.dateFrom) = :year_dt')->setParameter('year_dt', $dateTime->format('Y'))
            ->andWhere('MONTH(c.dateFrom) = :month_dt')->setParameter('month_dt', $dateTime->format('n'));
    }
}
