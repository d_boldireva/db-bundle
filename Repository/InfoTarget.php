<?php

namespace TeamSoft\CrmRepositoryBundle\Repository;

use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query\Expr\Join;
use Doctrine\ORM\Query\ResultSetMappingBuilder;
use TeamSoft\CrmRepositoryBundle\Entity\InfoUserdirection;

class InfoTarget extends EntityRepository
{
    /**
     * selects all target groups, which were used in info_contact
     * @return array
     */
    public function getUsedInContacts()
    {
        $tableName = $this->getClassMetadata()->getTableName();

        $sql = "
            SELECT DISTINCT info_target.id, info_target.name
            FROM info_target
            JOIN info_targetspec ON info_targetspec.target_id = info_target.id
            JOIN info_contact ON info_contact.specialization_id = info_targetspec.spec_id
            ORDER BY info_target.name
        ";

        $rsm = new ResultSetMappingBuilder($this->_em);
        $rsm->addRootEntityFromClassMetadata($this->_entityName, $tableName);
        $rsm->addEntityResult($this->_entityName, $tableName);

        $query = $this->getEntityManager()->createNativeQuery($sql, $rsm);

        return $query->getResult();
    }


    public function findByUserOnDirection(?int $userId)
    {
        $query = $this->createQueryBuilder('t');
        $query->leftJoin('t.direction', 'd');

        // filter by user
        if ($userId) {
            $query->leftJoin(InfoUserdirection::class, 'ud', Join::WITH, 'ud.direction = t.direction');

            $query->where('ud.user = :user')
                ->orWhere('t.direction IS NULL') // <-- attention
                ->setParameter('user', $userId);
        }

        return $query
            ->orderBy('d.name', 'ASC')
            ->addOrderBy('t.name', 'ASC')
            ->getQuery()
            ->getResult();
    }

    public function findByPlanPresentationFilter(array $filter = [])
    {
        $tableName = $this->getClassMetadata()->getTableName();

        $whereParts = [];

        $optionalWhere = [];
        $optionalJoin = [];

        if (isset($filter['datefrom']) && $filter['datefrom']) {
            $optionalWhere[] = "info_plan.datefrom <= '{$filter['datefrom']}'";
        }

        if (isset($filter['datetill']) && $filter['datetill']) {
            $optionalWhere[] = "info_plan.datetill >= '{$filter['datetill']}'";
        }

        if (isset($filter['directionId']) && $filter['directionId']) {
            $optionalJoin[] = "
                INNER JOIN info_userdirection on info_userdirection.user_id = info_plan.owner_id
            ";
            $optionalWhere[] = "info_userdirection.direction_id = {$filter['directionId']}";
        }

        if (isset($filter['subdirectionId']) && $filter['subdirectionId']) {
            $optionalJoin[] = "
                INNER JOIN info_usersubdirection on info_usersubdirection.user_id = info_plan.owner_id
            ";
            $optionalWhere[] = "info_usersubdirection.subdirection_id = {$filter['subdirectionId']}";
        }

        $optionalJoin = implode(' ', $optionalJoin);
        if (!empty($optionalWhere)) {
            $optionalWhere = ' AND ' . join(' AND ', $optionalWhere);
        } else {
            $optionalWhere = "";
        }

        $whereParts[] = "
            exists(select 1 from info_plan 
                INNER JOIN info_plandetail on info_plandetail.plan_id = info_plan.id
                INNER JOIN info_planpresentation on info_planpresentation.plandetail_id = info_plandetail.id
                INNER JOIN info_clm on info_clm.id = info_planpresentation.presentation_id
                {$optionalJoin}
                where info_plandetail.target_id = {$tableName}.id {$optionalWhere}
            )
        ";

        $rsm = new ResultSetMappingBuilder($this->_em);
        $rsm->addRootEntityFromClassMetadata($this->_entityName, $tableName, array(), ResultSetMappingBuilder::COLUMN_RENAMING_INCREMENT);

        $selectClause = $rsm->generateSelectClause();

        $where = count($whereParts) ? 'WHERE ' . implode(' AND ', $whereParts) : '';

        $sql = "
            SELECT {$selectClause} FROM {$tableName}                        
            {$where}   
        ";

        $query = $this->getEntityManager()->createNativeQuery($sql, $rsm);

        return $query->getResult();
    }
}
