<?php
namespace TeamSoft\CrmRepositoryBundle\Repository;

use Doctrine\ORM\EntityRepository;
use TeamSoft\CrmRepositoryBundle\Entity\InfoRole;
use TeamSoft\CrmRepositoryBundle\Entity\InfoTasktype as InfoTasktypeEntity;

/**
 * @see \TeamSoft\CrmRepositoryBundle\Entity\InfoEntityMap
 * Class InfoEntityMap
 * @package TeamSoft\CrmRepositoryBundle\Repository\EntityMap
 */
class InfoEntityMap extends EntityRepository
{
    /**
     * @param string $entityType
     * @param int $roleId
     * @param int|null $taskTypeId
     * @return \TeamSoft\CrmRepositoryBundle\Entity\InfoEntityMap[]
     */
    public function findByFilter(string $entityType, int $roleId, int $taskTypeId = null): array
    {
        $builder = $this->createQueryBuilder('e');
        $builder->andWhere('e.entityType = :entityType')->setParameter('entityType', $entityType);
        $builder->andWhere('e.role = :roleId')->setParameter('roleId', $roleId);
        if ($taskTypeId) {
            $builder->andWhere('e.taskType = :taskTypeId')->setParameter('taskTypeId', $taskTypeId);
        } else {
            //$taskTypeId = 0 or null
            $builder->andWhere('e.taskType IS NULL');
        }

        return $builder->getQuery()->getResult();
    }

    /**
     * @param  string  $entityType
     * @param  \TeamSoft\CrmRepositoryBundle\Entity\InfoRole[]  $roles
     * @param  \TeamSoft\CrmRepositoryBundle\Entity\InfoTasktype[]|null[]  $taskTypes
     *
     * @return mixed
     */
    public function deleteByTypeAndRolesAndTaskTypes(string $entityType, array $roles, array $taskTypes)
    {
        $builder = $this->createQueryBuilder('e')
            ->delete()
            ->andWhere('e.entityType = :entityType')
            ->andWhere('e.role IN (:roles)');

        if (in_array(null, $taskTypes, true)) {
            $builder->andWhere('(e.taskType IN (:types) OR e.taskType IS NULL)');
        } else {
            $builder->andWhere('e.taskType IN (:types)');
        }

        $builder->setParameters([
            'entityType' => $entityType,
            'roles' => $roles,
            'types' => $taskTypes,
        ]);

        return $builder->getQuery()->execute();
    }

    /**
     * If entity_map not found for $taskTypeId, set $taskTypeId=null and try agent
     *
     * @param string $entityType
     * @param int $roleId
     * @param int|null $taskTypeId
     * @return \TeamSoft\CrmRepositoryBundle\Entity\InfoEntityMap[]
     */
    public function getEntitiesMaps(string $entityType, int $roleId, int $taskTypeId = null): array
    {
        $entityMaps = $this->findByFilter($entityType, $roleId, $taskTypeId);
        if (!$entityMaps && $taskTypeId) {
            $entityMaps = $this->findByFilter($entityType, $roleId);
        }

        return $entityMaps;
    }
}
