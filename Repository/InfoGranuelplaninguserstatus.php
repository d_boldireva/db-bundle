<?php

namespace TeamSoft\CrmRepositoryBundle\Repository;

use Doctrine\ORM\Query\Expr\Join;
use Doctrine\ORM\QueryBuilder;
use TeamSoft\CrmRepositoryBundle\Entity\InfoUser;
use Doctrine\ORM\EntityRepository;

class InfoGranuelplaninguserstatus extends EntityRepository {
    /**
     * @param InfoUser $user
     * @return array
     */
    public function findSubordinateUsersWithStatus(InfoUser $user){
        /**
         * @var InfoUser[] $users
         */
        $users = array_merge([$user],$user->getSubordinates()->toArray());

        /**
         * @var QueryBuilder $queryBuilder
         */
        $queryBuilder = $this->_em->getRepository(InfoUser::class)->createQueryBuilder('user');
        $queryBuilder
            ->leftJoin(\TeamSoft\CrmRepositoryBundle\Entity\InfoGranuelplaninguserstatus::class,'gp',Join::WITH,$queryBuilder->expr()->eq('gp.user','user'))
            ->andWhere($queryBuilder->expr()->in('user',':users'))
            ->setParameter(':users',$users)
            ->select('user.id, user.name, MAX(gp.userStatus) as status')
            ->groupBy('user.id, user.name');

        return $queryBuilder->getQuery()->getResult();
    }
}