<?php

namespace TeamSoft\CrmRepositoryBundle\Repository;

use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query\ResultSetMappingBuilder;

class InfoTasksubject extends EntityRepository {
    public function findByFilter(array $filter) {
        $tableName = $this->getClassMetadata()->getTableName();

        $rsm = new ResultSetMappingBuilder($this->_em);
        $rsm->addRootEntityFromClassMetadata($this->_entityName, $tableName, array(), ResultSetMappingBuilder::COLUMN_RENAMING_INCREMENT);

        $selectClause = $rsm->generateSelectClause();
        $planId = (isset($filter["plan_id"]) && is_numeric($filter["plan_id"])) ? $filter["plan_id"] : 0;
        $planDetailId = (isset($filter["plandetail_id"]) && is_numeric($filter["plandetail_id"])) ? $filter["plandetail_id"] : 0;

        $wheres = '';

        if ($planDetailId && $planId){
            $wheres = "info_plantasksubject.plan_id = $planId AND info_plantasksubject.plandetail_id = $planDetailId";
        } else if ($planDetailId){
            $wheres = "info_plantasksubject.plandetail_id = $planDetailId";
        } else if ($planId){
            $wheres = "info_plantasksubject.plan_id = $planId";
        }

        if($wheres){
            $wheres = "WHERE $wheres";
        }

        $sql = "
            SELECT $selectClause
            FROM $tableName
            INNER JOIN info_plantasksubject ON info_plantasksubject.tasksubject_id = $tableName.id
            $wheres
        ";

        $query = $this->getEntityManager()->createNativeQuery($sql, $rsm);

        $result = $query->getResult();

        return $result;
    }
}