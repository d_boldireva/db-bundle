<?php


namespace TeamSoft\CrmRepositoryBundle\Repository;


use Doctrine\ORM\EntityRepository;

class InfoMcmunsubscribed extends EntityRepository
{
    public function findByContactAndDistribution($contactId = null, $distributionId = null)
    {
        return $this->createQueryBuilder('u')
            ->select('u')
            ->where('u.contact = :contact')
            ->andWhere('u.distribution = :distribution OR u.distribution IS NULL')
            ->setParameter('contact', $contactId)
            ->setParameter('distribution', $distributionId)
            ->setMaxResults(1)
            ->getQuery()
            ->getOneOrNullResult();
    }

    public function getCountUnsubscribedByDistribution($distribution)
    {
        return $this
            ->createQueryBuilder('mu')
            ->select('COUNT(mu.id)')
            ->where('mu.distribution = :distribution')
            ->setParameter('distribution', $distribution->getId())
            ->getQuery()
            ->getSingleScalarResult();
    }

    public function getCountUnsubscribedAll($distribution)
    {
        $type = null;
        if ($distribution->getMcmcontent() !== null && $distribution->getMcmcontent()->getType() == 'email') {
            $type = "'email'";
        } elseif ($distribution->getMcmcontent() !== null && $distribution->getMcmcontent()->getType() == 'viber') {
            $type = "'email', 'viber'";
        } elseif ($distribution->getMcmcontent() !== null && $distribution->getMcmcontent()->getType() == 'sms') {
            $type = "'email'";
        }
        if ($type) {
            $targetIds = [];
            foreach ($distribution->getMcmTargets() as $target) {
                $targetIds[] = $target->getId();
            }
            if (!empty($targetIds)) {
                $targetIds = implode(',', $targetIds);
                return $this
                    ->createQueryBuilder('mu')
                    ->select('COUNT(DISTINCT mu.contact)')
                    ->join('mu.distribution', 'd')
                    ->join('d.mcmTargets', 't')
                    ->join('d.mcmcontent', 'c')
                    ->where("c.type in ({$type})")
                    ->andWhere("t.id in ({$targetIds})")
                    ->getQuery()
                    ->getSingleScalarResult();
            }
        }
        return null;
    }
}
