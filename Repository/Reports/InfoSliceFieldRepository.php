<?php
declare(strict_types=1);


namespace TeamSoft\CrmRepositoryBundle\Repository\Reports;


use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use TeamSoft\CrmRepositoryBundle\Entity\InfoSliceField;

/**
 * Class InfoSliceFieldRepository
 * @package TeamSoft\CrmRepositoryBundle\Repository\Reports
 *
 * @method InfoSliceField|null find($id, $lockMode = null, $lockVersion = null)
 * @method InfoSliceField|null findOneBy(array $criteria, array $orderBy = null)
 * @method InfoSliceField[]    findAll()
 * @method InfoSliceField[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class InfoSliceFieldRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, InfoSliceField::class);
    }
}