<?php

namespace TeamSoft\CrmRepositoryBundle\Repository;

use Doctrine\DBAL\FetchMode;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query\ResultSetMappingBuilder;

/**
 * Class InfoContactowner
 * @see \TeamSoft\CrmRepositoryBundle\Entity\InfoContactowner
 * @package TeamSoft\CrmRepositoryBundle\Repository
 */
class InfoContactowner extends EntityRepository
{
    public function contactOwnerExist($contactId, $ownerId): bool
    {
        $sql = "
            SELECT contact_id FROM info_contactowner 
                WHERE 
                      contact_id = {$contactId} AND 
                      (owner_id = {$ownerId} OR owner_id IN (SELECT child_id FROM info_contactsubmission WHERE subj_id = {$ownerId}))
        ";

        //false or integer
        return (bool) $this->getEntityManager()->getConnection()->executeQuery($sql)->fetch(FetchMode::COLUMN);
    }
}
