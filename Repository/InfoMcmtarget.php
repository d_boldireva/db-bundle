<?php

namespace TeamSoft\CrmRepositoryBundle\Repository;

use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use TeamSoft\CrmRepositoryBundle\Service\DatabasePlatform;
use Doctrine\ORM\Query\ResultSetMappingBuilder;
use Doctrine\Persistence\ManagerRegistry;
use TeamSoft\CrmRepositoryBundle\Entity\InfoMcmtarget as InfoMcmtargetEntity;

class InfoMcmtarget extends ServiceEntityRepository
{
    private $dp;

    public function __construct(ManagerRegistry $registry, DatabasePlatform $dp)
    {
        parent::__construct($registry, InfoMcmtargetEntity::class);
        $this->dp = $dp;
    }

    /**
     * @param array $filters
     * @param integer $offset
     * @param integer $limit
     * @param integer $archived
     * @return array
     */
    public function findByFilter(array $filters, $offset, $limit, $archived, $orderBy = null)
    {
        $queryBuilder = $this->createQueryBuilder('t');

        if ($archived) {
            $queryBuilder
                ->where('t.isarchive = 1');
        } else {
            $queryBuilder
                ->where('t.isarchive = 0')
                ->orWhere('t.isarchive is null');
        }

        if (isset($filters['search'])) {
            $queryBuilder
                ->andWhere('LOWER(t.name) LIKE :search')
                ->setParameter('search', mb_strtolower('%'.$filters['search'].'%'));
        }

        if (isset($filters['user']) && $filters['user']) {
            $users = $filters['user']->getSubordinates()->toArray();
            array_push($users, $filters['user']);
            $queryBuilder
                ->andWhere('t.creator in (:users)')
                ->setParameter('users', $users);
        }

        $totalCountQueryBuilder = clone $queryBuilder;
        $totalCountQueryBuilder->select('COUNT(t.id)');

        $column = 'createdate';
        $order = 'DESC';
        if ($orderBy) {
            foreach ($orderBy as $value) {
                $order = strtoupper($value["order"]);
                if ($order === "ASC" || $order === "DESC") {
                    $column = strtolower($value["column"]);
                    if ($column == "createdate") {
                        $queryBuilder->addOrderBy('t.' . $column, $order);
                    } else {
                        if ($column == "name") {
                            $queryBuilder->addOrderBy('t.' . $column, $order);
                        }
                    }
                }
            }
        } else {
            $queryBuilder->addOrderBy('t.' . $column, $order);
        }

        if ($offset) {
            $queryBuilder->setFirstResult($offset);
        }

        if ($limit) {
            $queryBuilder->setMaxResults($limit);
        }

        return array(
            'total_count' => $totalCountQueryBuilder->getQuery()->getSingleScalarResult(),
            'data' => $queryBuilder->getQuery()->getResult() ?? []
        );
    }

    public function getContactsInTarget($id)
    {
        $rsm = new ResultSetMappingBuilder($this->_em);
        $rsm->addScalarResult('id', 'id', 'integer');
        $sql = "
                select contact_id as id from info_mcmcontactintarget where target_id = {$id}
            ";
        $query = $this->getEntityManager()->createNativeQuery($sql, $rsm);
        return $query->getArrayResult();
    }
}