<?php

namespace TeamSoft\CrmRepositoryBundle\Repository;

use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\Query\ResultSetMappingBuilder;
use Psr\Log\LoggerInterface;
use TeamSoft\CrmRepositoryBundle\Service\DatabasePlatform;
use Doctrine\Persistence\ManagerRegistry;
use TeamSoft\CrmRepositoryBundle\Entity\InfoSurvey as InfoSurveyEntity;

class InfoSurvey extends ServiceEntityRepository
{
    /**
     * @var DatabasePlatform
     */
    private $dp;

    /**
     * @var LoggerInterface
     */
    private $logger;

    public function __construct(ManagerRegistry $registry, DatabasePlatform $dp, LoggerInterface $logger)
    {
        parent::__construct($registry, InfoSurveyEntity::class);

        $this->dp = $dp;
        $this->logger = $logger;
    }

    public function findByFilter(array $filterBy)
    {
        $dateFrom = $filterBy['dateFrom'] ?? null;
        $taskId = $filterBy['taskId'] ?? 0;
        $actionId = $filterBy['actionId'] ?? 0;
        $companyId = $filterBy['companyId'] ?? 0;
        $contactId = $filterBy['contactId'] ?? 0;
        $tasktypeId = $filterBy['tasktypeId'] ?? 0;
        $actiontypeId = $filterBy['actiontypeId'] ?? 0;
        $specializationId = $filterBy['specializationId'] ?? 0;
        $responsibleId = $filterBy['responsibleId'] ?? 0;
        $page = $filterBy['page'] ?? 4;

        $connection = $this->_em->getConnection();
        $sql = "
            select s.id, f.sqlfilter from info_survey s
            inner join info_surveyfilter f on s.id = f.survey_id
            where coalesce(s.isarchive, 0) = 0 and s.page = $page
        ";

        if ($dateFrom) {
            //datefrom and datetill have datetime format but used as date
            $sql .= "and :datefrom BETWEEN s.datefrom AND s.datetill";
        }

        if ($companyId || $contactId) {
            if ($page == 8) {
                $sqlInWhere = "
                    select sra.survey_id from info_action a
                        inner join info_actionstate ast on ast.id=a.actionstate_id and ast.isclosed=1
                        inner join info_surveyresultaction sra on sra.action_id=a.id and sra.survey_id is not null
                        inner join info_survey s2 on s2.id = sra.survey_id and coalesce(s2.ismultiuse, 0) != 1
                        where coalesce(a.company_id, 0) = :companyId and coalesce(a.contact_id, 0) = :contactId
                        and a.id != :actionId
                ";
            } elseif ($page == 4) {
                $sqlInWhere = " select sr.survey_id from info_task t
                        inner join info_taskstate ts on ts.id=t.taskstate_id and ts.isclosed=1
                        inner join info_surveyresult sr on sr.task_id=t.id and sr.survey_id is not null
                        inner join info_survey s2 on s2.id = sr.survey_id and coalesce(s2.ismultiuse, 0) != 1
                        where coalesce(t.company_id, 0) = :companyId and coalesce(t.contact_id, 0) = :contactId
                        and t.id != :taskId                
                ";
            }
            $sql .= "
               and s.id not in ({$sqlInWhere})
            ";
        }

        $statement = $connection->prepare($sql);

        if ($dateFrom) {
            $statement->bindValue(':datefrom', (new \DateTime($dateFrom))->format('Y-m-d'));
        }

        if ($companyId || $contactId) {
            if ($page == 8) {
                $statement->bindValue(':actionId', $actionId);
            } elseif ($page == 4) {
                $statement->bindValue(':taskId', $taskId);
            }

            $statement->bindValue(':companyId', $companyId);
            $statement->bindValue(':contactId', $contactId);
        }

        $filters = $statement->executeQuery()->fetchAllAssociative();

        $surveys = null;
        if (count($filters)) {

            $tableName = $this->getClassMetadata()->getTableName();
            $rsm = new ResultSetMappingBuilder($this->_em);
            $rsm->addRootEntityFromClassMetadata(
                $this->_entityName,
                $tableName,
                [],
                ResultSetMappingBuilder::COLUMN_RENAMING_INCREMENT
            );

            $whereParts = [];
            foreach ($filters as $key => $filter) {
                $whereParts[$key] = "$tableName.id = " . $filter['id'];

                if ($filter['sqlfilter']) {
                    $whereParts[$key] .= " and (" . $filter['sqlfilter'] . ')';
                }
            }
            $whereString = '(' . join(') or (', $whereParts) . ')';

            $whereString = str_replace(
                [':company_id', ':contact_id', ':tasktype_id', ':actiontype_id', ':specialization_id', ':responsible_id'],
                [$companyId, $contactId, $tasktypeId, $actiontypeId, $specializationId, $responsibleId],
                $whereString
            );

            $selectClause = $rsm->generateSelectClause();

            $sql = "
                SELECT $selectClause FROM $tableName
                WHERE $whereString            
            ";

            try {
                $query = $this->getEntityManager()->createNativeQuery($sql, $rsm);
                $surveys = $query->getResult();
            } catch (\Throwable $exception) {
                $surveys = [];
                $this->logger->critical($exception->getMessage(), [
                    'code' => $exception->getCode(),
                    'errorClass' => get_class($exception),
                    'file' => $exception->getFile(),
                    'line' => $exception->getLine(),
                    'filterBy' => json_encode($filterBy),
                ]);
            }
        }

        return $surveys;
    }
}
