<?php

namespace TeamSoft\CrmRepositoryBundle\Repository;

use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query\ResultSetMappingBuilder;

class InfoClm extends EntityRepository
{

    /**
     * @param \TeamSoft\CrmRepositoryBundle\Entity\InfoUser $user
     * @param boolean $hideRoot
     * @return array
     */
    public function findAvailableForUser (\TeamSoft\CrmRepositoryBundle\Entity\InfoUser $user, $hideRoot)
    {

        $userId = $user->getId();
        if($hideRoot == true){
            $whereHideRoot = " COALESCE(cg.root, 0) <> 1 AND ";
        } else {
            $whereHideRoot = "";
        }


        $tableName = $this->getClassMetadata()->getTableName();

        $rsm = new ResultSetMappingBuilder($this->_em);
        $rsm->addRootEntityFromClassMetadata($this->_entityName, $tableName, array(), ResultSetMappingBuilder::COLUMN_RENAMING_INCREMENT);
        $rsm->addEntityResult($this->_entityName, $tableName, 'clm');
        $rsm->addScalarResult('group_id', 'group', 'integer');
        $rsm->addScalarResult('group_name', 'group_name', 'string');

        $selectClause = $rsm->generateSelectClause();

        $sql = "
            SELECT 
              cg.id group_id,
              cg.name group_name,
              $selectClause
            FROM $tableName
            INNER JOIN info_clmingroup cig ON cig.clm_id = $tableName.id
            INNER JOIN info_clmgroup cg ON cg.id = cig.group_id
            INNER JOIN info_clmgroupdirection cgd ON cgd.clmgroup_id = cg.id
            INNER JOIN info_userdirection ud ON ud.direction_id = cgd.direction_id
            WHERE $whereHideRoot ud.user_id = $userId
        ";

        $query = $this->getEntityManager()->createNativeQuery($sql, $rsm);
        $result = $query->getResult();

        return $result;

    }

    /**
     * @param \TeamSoft\CrmRepositoryBundle\Entity\InfoUser $user
     * @return array
     */
    public function findAvailableGroupsForUser (\TeamSoft\CrmRepositoryBundle\Entity\InfoUser $user)
    {
        $userId = $user->getId();
        $tableName = $this->getClassMetadata()->getTableName();

        $rsm = new ResultSetMappingBuilder($this->_em);
        $rsm->addRootEntityFromClassMetadata($this->_entityName, $tableName, array(), ResultSetMappingBuilder::COLUMN_RENAMING_INCREMENT);
        $rsm->addEntityResult($this->_entityName, $tableName, 'clm');
        $rsm->addScalarResult('id', 'id', 'integer');
        $rsm->addScalarResult('name', 'name', 'string');
        $rsm->addScalarResult('parent', 'parent', 'string');

        if ($user->isSuperAdmin()) {
            $sql = "
            SELECT DISTINCT
                cg.id,
                cg.name,
                parent.name as parent
            FROM info_clmgroup cg
            LEFT JOIN info_clmgroup parent on cg.parent_id = parent.id";
        } else {
            $sql = "
            SELECT DISTINCT
                cg.id,
                cg.name,
                parent.name as parent
            FROM info_clmgroup cg
            INNER JOIN info_clmgroupdirection cgd ON cgd.clmgroup_id = cg.id
            INNER JOIN info_userdirection ud ON ud.direction_id = cgd.direction_id
            LEFT JOIN info_clmgroup parent on cg.parent_id = parent.id
            WHERE COALESCE(cg.root, 0) <> 1 AND ud.user_id = $userId";
        }

        $query = $this->getEntityManager()->createNativeQuery($sql, $rsm);

        $result = $query->getResult();

        return $result;

    }
}