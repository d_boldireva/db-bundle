<?php

namespace TeamSoft\CrmRepositoryBundle\Repository;

use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query\ResultSetMappingBuilder;

class InfoQualitycontrol extends EntityRepository
{

    public function findByFilter(array $filterBy, $limit = null, $offset = null)
    {
        $tableName = $this->getClassMetadata()->getTableName();

        $whereParts = array();

        if (isset($filterBy['date']) && $filterBy['date']) {
            $whereParts[] = " CAST(CONVERT(varchar(10), info_qualitycontrol.dt, 105) AS VARCHAR(10)) LIKE '%" . str_replace(",", "','", $filterBy['date']) . "%'";
        }

        if (isset($filterBy['status']) && $filterBy['status']) {
            $whereParts[] = "info_qualitycontrol.status IN ('" . str_replace(",", "','", $filterBy['status']) . "')";
        }

        if (isset($filterBy['responsible']) && $filterBy['responsible']) {
            $whereParts[] = "responsible.name LIKE '%" . str_replace("'", "''", $filterBy['responsible']) . "%'";
        }

        if (isset($filterBy['city']) && $filterBy['city']) {
            $whereParts[] = "city.name LIKE '%" . str_replace("'", "''", $filterBy['city']) . "%'";
        }

        if (isset($filterBy['type']) && $filterBy['type']) {
            $whereParts[] = "type.name LIKE '%" . str_replace("'", "''", $filterBy['type']) . "%'";
        }

        if (isset($filterBy['preparation']) && $filterBy['preparation']) {
            $whereParts[] = "preparation.name LIKE '%" . str_replace("'", "''", $filterBy['preparation']) . "%'";
        }

        if (isset($filterBy['brand']) && $filterBy['brand']) {
            $whereParts[] = "brand.name LIKE '%" . str_replace("'", "''", $filterBy['brand']) . "%'";
        }

        if (isset($filterBy['source']) && $filterBy['source']) {
            $whereParts[] = "COALESCE(contact.LastName, '') + ' ' + COALESCE(contact.FirstName, '') + ' ' + COALESCE(contact.MiddleName, '') + ' ' + COALESCE(company.name, '') + ' ' + COALESCE(streetType.name,'') + ' ' + COALESCE(company.street,'') + ' ' + COALESCE(company.building,'') LIKE '%" . str_replace("'", "''", $filterBy['source']) . "%'";
        }

        $rsm = new ResultSetMappingBuilder($this->_em);

        $where = $whereParts ? 'WHERE ' . implode(' AND ', $whereParts) : '';

        $top = ($limit && !$offset) ? 'TOP ' . $limit : '';

        $sql = "
            SELECT $top
               info_qualitycontrol.id,
               CONVERT(varchar(10), info_qualitycontrol.dt, 105) dt,
               responsible.name responsible,
               city.name city,
               COALESCE(contact.LastName, '') + ' ' + COALESCE(contact.FirstName, '') + ' ' + COALESCE(contact.MiddleName, '') contact,
               COALESCE(company.name, '') + ' ' + COALESCE(streetType.name,'') + ' ' + COALESCE(company.street,'') + ' ' + COALESCE(company.building,'') AS dataSource,
               type.name type,
               brand.name brand,
               preparation.name preparation,
               info_qualitycontrol.status,
               info_qualitycontrol.serialnumber,
               info_qualitycontrol.descr,
               CASE WHEN answer.dt > msg.dt THEN 1 ELSE 0 END as answered
            FROM $tableName
            JOIN info_task task ON task.id = $tableName.task_id
            LEFT JOIN info_user responsible ON responsible.id = task.responsible_id
            LEFT JOIN info_dictionary type ON type.id = $tableName.type_id
            LEFT JOIN info_company company ON company.id = task.company_id
            LEFT JOIN info_dictionary AS streetType ON streetType.id = company.streettype_id
            LEFT JOIN info_city city ON company.city_id = city.id
            LEFT JOIN info_contact contact ON contact.id = task.contact_id
            LEFT JOIN info_preparation preparation ON preparation.id = $tableName.preparation_id
            LEFT JOIN info_preparationbrend brand ON brand.id = preparation.brend_id
            LEFT JOIN (SELECT qualitycontrol_id, MAX(dt) as dt FROM info_qualitycontrolmsg group by qualitycontrol_id) msg ON $tableName.id = msg.qualitycontrol_id
            LEFT JOIN (SELECT qualitycontrol_id, MAX(dt) as dt FROM info_qualitycontrolanswer group by qualitycontrol_id) answer ON $tableName.id = answer.qualitycontrol_id
            $where
            ORDER BY info_qualitycontrol.dt DESC, info_qualitycontrol.currenttime DESC
        ";

        $query = $this->getEntityManager()->createNativeQuery($sql, $rsm);

        $rsm->addScalarResult('id', 'id');
        $rsm->addScalarResult('dt', 'dt');
        $rsm->addScalarResult('responsible', 'responsible');
        $rsm->addScalarResult('city', 'city');
        $rsm->addScalarResult('contact', 'contact');
        $rsm->addScalarResult('dataSource', 'source');
        $rsm->addScalarResult('type', 'type');
        $rsm->addScalarResult('brand', 'brand');
        $rsm->addScalarResult('preparation', 'preparation');
        $rsm->addScalarResult('status', 'status');
        $rsm->addScalarResult('serialnumber', 'serialnumber');
        $rsm->addScalarResult('descr', 'descr');
        $rsm->addScalarResult('answered', 'answered');

        $result = $query->getResult();

        if ($limit && $offset) {
            $result = array_slice($result, $offset, $limit);
        }

        $totalCountSql = "
            SELECT COUNT(1) FROM $tableName
            JOIN info_task task ON task.id = $tableName.task_id
            LEFT JOIN info_user responsible ON responsible.id = task.responsible_id
            LEFT JOIN info_dictionary type ON type.id = $tableName.type_id
            LEFT JOIN info_company company ON company.id = task.company_id
            LEFT JOIN info_dictionary AS streetType ON streetType.id = company.streettype_id
            LEFT JOIN info_city city ON company.city_id = city.id
            LEFT JOIN info_contact contact ON contact.id = task.contact_id
            LEFT JOIN info_preparation preparation ON preparation.id = $tableName.preparation_id
            LEFT JOIN info_preparationbrend brand ON brand.id = preparation.brend_id
            $where
        ";

        return array(
            "total_count" => (int)$this->getEntityManager()->getConnection()->executeQuery($totalCountSql)->fetchOne(),
            "data" => $result,
        );
    }

}
