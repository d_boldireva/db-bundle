<?php

namespace TeamSoft\CrmRepositoryBundle\Repository;

use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\Query\ResultSetMappingBuilder;
use Doctrine\Persistence\ManagerRegistry;
use TeamSoft\CrmRepositoryBundle\Service\DatabasePlatform;

class InfoMcmdistributionaction extends ServiceEntityRepository
{
    private $notSentStatuses = ['EXCLUDED', 'EMPTY_RECIPIENT', 'DUPLICATE', 'UNVERIFIED',
                'MESSAGE_LIMIT', 'INVALID_EMAIL', 'DEFERRED', 'ERROR', 'FAILED'];
    private $notDeliveredStatuses = [
        'UNDELIVERED', 'BOUNCED', 'UNDELIVERABLE', 'EXPIRED', 'REJECTED', 'RESENDING_SMS_UNDELIVERED'
    ];
    private $sentStatuses = ['SENT', 'RESENDING_SMS_SENT'];
    private $readStatuses = ['READ', 'SEEN'];

    public function __construct(ManagerRegistry $registry, DatabasePlatform $dp)
    {
        parent::__construct($registry, \TeamSoft\CrmRepositoryBundle\Entity\InfoMcmdistributionaction::class);
        $this->dp = $dp;
    }

    /**
     * @param \TeamSoft\CrmRepositoryBundle\Entity\InfoMcmdistribution $distribution
     * @return array|boolean
     */
    public function getGeneralStatisticByDistribution($distribution)
    {
        $tableName = $this->getClassMetadata()->getTableName();

        $rsm = new ResultSetMappingBuilder($this->_em);
        $rsm->addScalarResult('delivered', 'delivered', 'integer');
        $rsm->addScalarResult('failed', 'failed', 'integer');

        $sql = "
            SELECT delivered, failed FROM (
			SELECT distribution_id AS id, COUNT(id) as delivered FROM $tableName
			WHERE delivered = 1 GROUP BY distribution_id) AS d
			LEFT JOIN (SELECT distribution_id AS id, COUNT(id) as failed FROM $tableName
			WHERE delivered IS NULL GROUP BY distribution_id) f ON f.id = d.id
			WHERE d.id = :distribution_id
        ";

        $sql = str_replace(':distribution_id', $distribution->getId(), $sql);

        $query = $this->getEntityManager()->createNativeQuery($sql, $rsm);

        $result = $query->getOneOrNullResult();

        if (empty($result) || ($result['delivered'] == 0 && $result['failed'] == 0)) {
            return false;
        }

        return $result;
    }

    /**
     * @param \TeamSoft\CrmRepositoryBundle\Entity\InfoMcmdistribution $distribution
     * @return array
     */
    public function findStatisticByDistribution($distribution)
    {
        $tableName = $this->getClassMetadata()->getTableName();

        $rsm = new ResultSetMappingBuilder($this->_em);
        $rsm->addScalarResult('count', 'count', 'integer');
        $rsm->addScalarResult('status', 'status', 'string');

        $sql = "SELECT 
              COUNT(DISTINCT id) as count, 
              status,
              (SELECT COUNT(id) FROM $tableName WHERE distribution_id = :distribution_id) as total
            FROM $tableName
            WHERE distribution_id = :distribution_id AND NOT status IS NULL
            GROUP BY status
        ";

        $sql = str_replace(':distribution_id', $distribution->getId(), $sql);

        $query = $this->getEntityManager()->createNativeQuery($sql, $rsm);

        $result = $query->getResult();

        $statistic = ['undelivered' => 0, 'sent' => 0, 'not_sent' => 0, 'read' => 0];

        foreach ($result as $key => $value) {
            if (in_array($value['status'], $this->notSentStatuses)) {
                $statistic['not_sent'] += $value['count'];
            } else if (in_array($value['status'], $this->sentStatuses)) {
                $statistic['sent'] += $value['count'];
            } else if (in_array($value['status'], $this->notDeliveredStatuses)) {
                $statistic['undelivered'] += $value['count'];
            } else if (in_array($value['status'], $this->readStatuses)) {
                $statistic['read'] += $value['count'];
            } else {
                $statistic[strtolower($value['status'])] = $value['count'];
            }
        }

        return $statistic;
    }

    /**
     * @param \TeamSoft\CrmRepositoryBundle\Entity\InfoMcmdistribution $distribution
     * @param integer
     * @return array|boolean
     */
    public function getDetailedStatisticByDistribution($distribution, $limit = 100)
    {
        $tableName = $this->getClassMetadata()->getTableName();

        $rsmRecipients = new ResultSetMappingBuilder($this->_em);
        $rsmRecipients->addScalarResult('target', 'target', 'string');
        $rsmRecipients->addScalarResult('contact', 'contact', 'string');
        $rsmRecipients->addScalarResult('status', 'status', 'string');
        $rsmRecipients->addScalarResult('morionid', 'morionid', 'string');

        $rsmClicks = new ResultSetMappingBuilder($this->_em);
        $rsmClicks->addScalarResult('url', 'url', 'string');
        $rsmClicks->addScalarResult('count', 'count', 'integer');

        $top = '';
        $lim = '';

        if ($limit) {
            $top = $this->dp->isPostgreSQL100() ? '' : 'TOP ' . $limit;
            $lim = $this->dp->isPostgreSQL100() ? 'limit ' . $limit : '';
        }

        $top1 = $this->dp->isPostgreSQL100() ? '' : 'TOP 1';
        $lim1 = $this->dp->isPostgreSQL100() ? 'limit 1' : '';
        $c = $this->dp->getConcatOperator();

        $mcmContent = $distribution->getMcmcontent();

        $type = $mcmContent ? $mcmContent->getType() : null;

        $sqlRecipients = "
            SELECT $top
                distribution.name,
                coalesce(info_mcmtarget.name, '') as target,
                coalesce(contact.lastName, '') $c ' ' $c coalesce(contact.firstName, '') $c ' ' $c coalesce(contact.middleName, '') as contact,
                coalesce(contact.morionid, 0) as morionid,";

        $sqlRecipients .= $type === 'email' ? "
                (select $top1 email from info_contactemail where contact_id = contact.id $lim1) AS eaddr,
        " : "
                (select $top1 phone from info_contactphone where contact_id = contact.id $lim1) AS phone2,
        ";

        $sqlRecipients .= "
                $tableName.status
            FROM $tableName
            INNER JOIN info_contact contact ON contact.id = $tableName.contact_id
            INNER JOIN info_mcmdistribution distribution ON distribution.id = $tableName.distribution_id
            LEFT JOIN info_mcmdistributiontarget distributionTarget ON distributionTarget.mcmdistribution_id = distribution.id
            LEFT JOIN info_mcmtarget ON info_mcmtarget.id = distributionTarget.mcmtarget_id
            WHERE distribution.id = :distribution_id AND distribution.state IN ('delivered', 'processing') AND NOT status IS NULL
            GROUP BY
                distribution.name,
                info_mcmtarget.name,
                contact.id,
                coalesce(contact.lastName, '') $c ' ' $c coalesce(contact.firstName, '') $c ' ' $c coalesce(contact.middleName, ''),
                contact.morionid,
                $tableName.status
                $lim
        ";

        $sqlClicks = "
            SELECT   links.url
                    ,count(clicks.contact_id) as count
            FROM mcm_shortlinks links
            LEFT JOIN mcm_linkstats clicks ON links.id = clicks.shortlink_id
            WHERE links.distribution_id = :distribution_id
            GROUP BY links.url HAVING count(clicks.contact_id) > 0 
        ";

        $sqlRecipients = str_replace(':distribution_id', $distribution->getId(), $sqlRecipients);
        $sqlClicks = str_replace(':distribution_id', $distribution->getId(), $sqlClicks);

        $queryRecipients = $this->getEntityManager()->createNativeQuery($sqlRecipients, $rsmRecipients);
        $queryClicks = $this->getEntityManager()->createNativeQuery($sqlClicks, $rsmClicks);

        if (!$this->getGeneralStatisticByDistribution($distribution)) {
            return false;
        }

        $result = array();

        $result['general'] = $this->getGeneralStatisticByDistribution($distribution);

        $result['recipients'] = $queryRecipients->getResult();
        $result['clicks'] = $queryClicks->getResult();

        $result['statistic'] = $this->findStatisticByDistribution($distribution);

        $result['content'] = array(
            'type' => $type,
            'subject' => $mcmContent && $mcmContent->getContentCollection()->count() ? $mcmContent->getContentCollection()->first()->getSubject() : $mcmContent->getSubject(),
            'body' => $mcmContent && $mcmContent->getContentCollection()->count() ? $mcmContent->getContentCollection()->first()->getBody() : $mcmContent->getBody(),
        );

        return $result;
    }

    /**
     * @param \TeamSoft\CrmRepositoryBundle\Entity\InfoMcmdistribution $distribution
     * @return int
     */
    public function getCountRecipientsByDistribution ($distribution)
    {
        return $this
            ->createQueryBuilder('mcmaction')
            ->select('COUNT(mcmaction.id)')
            ->andWhere('mcmaction.distribution = :distribution_id')
            ->setParameter('distribution_id', $distribution->getId())
            ->getQuery()
            ->getSingleScalarResult();
    }

    /**
     * @param \TeamSoft\CrmRepositoryBundle\Entity\InfoMcmdistribution $distribution
     * @return int
     */
    public function getCountSentByDistribution ($distribution)
    {
        return $this
            ->createQueryBuilder('mcmaction')
            ->select('COUNT(mcmaction.id)')
            ->where('NOT (mcmaction.status is null and mcmaction.delivered is null and mcmaction.failed is null)')
            ->andWhere('mcmaction.distribution = :distribution_id')
            ->andWhere("mcmaction.status not in ('" . implode("', '", $this->notSentStatuses) . "')")
            ->setParameter('distribution_id', $distribution->getId())
            ->getQuery()
            ->getSingleScalarResult();
    }

    /**
     * @param \TeamSoft\CrmRepositoryBundle\Entity\InfoMcmdistribution $distribution
     * @return int
     */
    public function getCountUnsubscribedByDistribution ($distribution)
    {
        return $this
            ->createQueryBuilder('mcmaction')
            ->select('COUNT(mcmaction.id)')
            ->where('(mcmaction.status = :undelivered OR mcmaction.status = :undeliverable) AND
                mcmaction.reason = :blocked')
            ->setParameter('undelivered', 'UNDELIVERED')
            ->setParameter('undeliverable', 'UNDELIVERABLE')
            ->orWhere("mcmaction.status = 'UNSUBSCRIBED'")
            ->setParameter('blocked', 'user-blocked')
            ->andWhere('mcmaction.distribution = :distribution_id')
            ->setParameter('distribution_id', $distribution->getId())
            ->getQuery()
            ->getSingleScalarResult();
    }

    /**
     * @param \TeamSoft\CrmRepositoryBundle\Entity\InfoMcmdistribution $distribution
     * @return array|string
     */
    public function getWhatsappActionsByDistribution(
        \TeamSoft\CrmRepositoryBundle\Entity\InfoMcmdistribution $distribution
    )
    {
        return $this
            ->createQueryBuilder('action')
            ->where('action.distribution = ' . $distribution->getId())
            ->andWhere("action.status IN ('SENT', 'DELIVERED')")
            ->andWhere("action.senddate > :date")
            ->setParameter('date', new \DateTime('-1 days'))
            ->getQuery()
            ->getResult();
    }

    /**
     * @param \TeamSoft\CrmRepositoryBundle\Entity\InfoMcmdistribution $distribution
     * @param array $addStatus
     * @return array|string
     */
    public function getViberActionsByDistribution(
        \TeamSoft\CrmRepositoryBundle\Entity\InfoMcmdistribution $distribution,
        $addStatus = []
    )
    {
        $statuses = ['PENDING', 'SENT', 'IN_QUEUE', 'RESENDING_SMS_ENQUEUED', 'DELIVERED', 'UNKNOWN'];
        $statuses = array_merge($statuses, $addStatus);

        return $this
            ->createQueryBuilder('action')
            ->where('action.distribution = ' . $distribution->getId())
            ->andWhere("action.status IN ('" . implode("','", $statuses) . "')")
            ->andWhere("action.senddate > :date")
            ->setParameter('date', new \DateTime('-2 days'))
            ->getQuery()
            ->getResult();
    }

    /**
     * @param \TeamSoft\CrmRepositoryBundle\Entity\InfoMcmdistribution $distribution
     * @return array|string
     */
    public function getSmsActionsByDistribution(
        \TeamSoft\CrmRepositoryBundle\Entity\InfoMcmdistribution $distribution
    )
    {
        return $this
            ->createQueryBuilder('action')
            ->where('action.distribution = ' . $distribution->getId())
            ->andWhere("action.status IN ('PENDING', 'SENT', 'OLD', 'SENDING', 'UNKNOWN')")
            ->andWhere("action.senddate > :date")
            ->setParameter('date', new \DateTime('-2 days'))
            ->getQuery()
            ->getResult();
    }
}
