<?php

namespace TeamSoft\CrmRepositoryBundle\Repository;

use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query\Expr\Join;

use Doctrine\ORM\QueryBuilder;
use TeamSoft\CrmRepositoryBundle\Entity\InfoGranualplan as TmpGranualplanEntity;
use TeamSoft\CrmRepositoryBundle\Entity\InfoPreparationdirection;
use TeamSoft\CrmRepositoryBundle\Entity\InfoSaleperiod;
use TeamSoft\CrmRepositoryBundle\Entity\InfoSaleplan;
use TeamSoft\CrmRepositoryBundle\Entity\InfoSaleplanpreparation;

/**
 * Class InfoGranualplan
 * @package TeamSoft\CrmRepositoryBundle\Repository
 *
 * @see \TeamSoft\CrmRepositoryBundle\Entity\InfoGranualplan
 */
class InfoGranualplan extends EntityRepository
{
    /**
     * @param \DateTime $date
     * @return \TeamSoft\CrmRepositoryBundle\Entity\InfoPreparation[]
     */
    public function findDefaultPlan(\DateTime $date)
    {
        /**
         * @var QueryBuilder $queryBuilder
         */
        $queryBuilder = $this->_em->getRepository(\TeamSoft\CrmRepositoryBundle\Entity\InfoPreparation::class)->createQueryBuilder('preparation');
        $queryBuilder
            ->innerJoin(TmpGranualplanEntity::class, 'gp', Join::WITH, $queryBuilder->expr()->eq('gp.prep', 'preparation'))
            ->where($queryBuilder->expr()->between(':date', 'gp.datefrom', 'gp.datetill'))
            ->setParameter(':date', $date)
            ->select('preparation');

        return $queryBuilder->getQuery()->getResult();
    }

    public function getPlanPreparation(\TeamSoft\CrmRepositoryBundle\Entity\InfoUser $forUser, \DateTime $forTime)
    {
        /**
         *  select
         * preparation.id,
         * preparation.name,
         * ISNULL(saleplanpreparation.Amount,0) as amount
         * from tmp_granualplan granualplan
         * inner join info_preparation preparation on preparation.id = granualplan.prep_id
         * left join info_saleperiod saleperiod on saleperiod.datefrom < granualplan.DateTill
         * and saleperiod.datetill > granualplan.DateFrom
         * left join info_saleplan saleplan on saleplan.SalePeriod_id = saleperiod.id
         * and saleplan.user_id = 728
         * left join info_saleplanpreparation saleplanpreparation on saleplanpreparation.preparation_id = preparation.id
         * and saleplanpreparation.saleplan_id = saleplan.id
         * where '2018-12-31' between granualplan.DateFrom and granualplan.DateTill
         */

        $_plan = $this->createQueryBuilder('granualplan')
            ->innerJoin('granualplan.prep', 'preparation')
            ->innerJoin(InfoPreparationdirection::class, 'pd', Join::WITH, 'pd.preparation = preparation AND pd.direction IN (:directions)')
            ->leftJoin(InfoSaleperiod::class, 'saleperiod', 'WITH', 'saleperiod.datefrom < granualplan.datetill and saleperiod.datetill > granualplan.datefrom')
            ->leftJoin(InfoSaleplan::class, 'saleplan', 'WITH', 'saleplan.saleperiod = saleperiod and saleplan.user = :user')
            ->leftJoin(InfoSaleplanpreparation::class, 'saleplanpreparation', 'WITH', 'saleplanpreparation.preparation = preparation and saleplanpreparation.saleplan = saleplan')
            ->where(':date between granualplan.datefrom and granualplan.datetill')
            ->andWhere('granualplan.direction IN (:directions)')
            ->andWhere('saleperiod.isForGranual = 1')
            ->select('preparation.id as id')
            ->addSelect('saleplanpreparation.count as count')
            ->setParameter(':date', $forTime)
            ->setParameter(':directions', $forUser->getDirectionCollection())
            ->setParameter(':user', $forUser)
            ->getQuery()
            ->getResult();

        $plan = [];

        foreach ($_plan as $preparation) {
            $plan[$preparation['id']] = $preparation['count'] ?: 0;
        }

        return $plan;
    }
}