<?php

namespace TeamSoft\CrmRepositoryBundle\Repository;

use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query\ResultSetMappingBuilder;
use TeamSoft\CrmRepositoryBundle\Entity\InfoUser as InfoUserEntity;

class InfoFiltersetmobile extends EntityRepository {

    public const FILTER_COMPANY = 101;
    public const FILTER_CONTACT = 102;

    public function findByFilter(array $filter) {
        $queryBuilder = $this->createQueryBuilder('fsm');

        $queryBuilder->andWhere('fsm.isdefault = 1');

        if (isset($filter["owner"]) && $filter["owner"] instanceof InfoUserEntity) {
            $queryBuilder->orWhere("fsm.owner = :owner");
            $queryBuilder->setParameter("owner", $filter["owner"]);
        }

        if (isset($filter["page"]) && is_numeric($filter["page"])) {
            $queryBuilder->andWhere("fsm.page = :page");
            $queryBuilder->setParameter("page", $filter["page"]);
        }

        $queryBuilder->orderBy("fsm.name");

        return $queryBuilder->getQuery()->getResult();
    }

    public function findByIds(array $ids) {
        $queryBuilder = $this->createQueryBuilder('fsm');
        $queryBuilder->where($queryBuilder->expr()->in("fsm.id", $ids));
        return $queryBuilder->getQuery()->getResult();
    }

    public function findByIdsAndPageWithDefaults($page, array $ids = array(), $ownerId = null) {
        $rsm = new ResultSetMappingBuilder($this->_em);
        $rsm->addRootEntityFromClassMetadata($this->_entityName, 'fsm', array(), ResultSetMappingBuilder::COLUMN_RENAMING_INCREMENT);

        $selectClause = $rsm->generateSelectClause();
        $orWhereIdIn = !empty($ids) ? 'OR fsm.id IN(' . implode(', ', $ids) . ')' : '';
        $orWhereOwnerId = $ownerId ? 'OR fsm.owner_id = ' . $ownerId : '';
        $sql = "SELECT $selectClause
                FROM info_filtersetmobile fsm
                WHERE fsm.page = $page AND (fsm.isdefault = 1 $orWhereOwnerId) AND (fsm.filter LIKE '%FLAG%' $orWhereIdIn)";

        $query = $this->getEntityManager()->createNativeQuery($sql, $rsm);

        return $query->getResult();
    }
}
