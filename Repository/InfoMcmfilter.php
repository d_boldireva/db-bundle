<?php

namespace TeamSoft\CrmRepositoryBundle\Repository;

use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use TeamSoft\CrmRepositoryBundle\Entity\InfoSystemdictionary;
use \TeamSoft\CrmRepositoryBundle\Entity\InfoCustomdictionary;
use \TeamSoft\CrmRepositoryBundle\Entity\InfoCustomdictionaryvalue;
use TeamSoft\CrmRepositoryBundle\Service\DatabasePlatform;
use TeamSoft\CrmRepositoryBundle\Entity\InfoUser;

class InfoMcmfilter extends ServiceEntityRepository
{

    private $dp;

    public function __construct(ManagerRegistry $registry, DatabasePlatform $dp)
    {
        parent::__construct($registry, \TeamSoft\CrmRepositoryBundle\Entity\InfoMcmfilter::class);
        $this->dp = $dp;
    }

    /**
     * @param mixed $id
     * @param InfoUser|null $user
     * @return array|boolean
     */
    public function findFilterMetadata($id, InfoUser $user = null)
    {
        $filtersMetadata = [];

        $filters = $this->findBy(array('id' => $id));

        /**
         * @var \TeamSoft\CrmRepositoryBundle\Entity\InfoMcmfilter $filter
         */
        foreach ($filters as $k => $filter) {
            $parents = [];
            $children = [];

            foreach ($filter->getParents() as $parent) {
                $parents[] = $parent->getSubjId();
            }

            foreach ($filter->getChildren() as $child) {
                $children[] = $child->getChildId();
            }

            $metadata = [
                'id' => $filter->getId(),
                'name' => $filter->getName(),
                'name_ru' => $filter->getNameRu(),
                'type' => $filter->getType(),
                'table' => 'mcmfilter',
                'parents' => $parents,
                'childs' => $children
            ];


            $metadata['data'] = $this->getFilterData($filter->getId(), [], $user);


            $filtersMetadata[] = $metadata;
        }

        return $filtersMetadata;
    }

    public function getFilterData($id, array $parentParams = [], InfoUser $user = null)
    {
        $filter = $this->findOneBy(['id' => $id]);
        $data = [];

        $customWhere = "";

        if (in_array($filter->getType(), ['range', 'dateRange'])) {
            /**
             * @var \Doctrine\DBAL\Connection $connection
             */
            $connection = $this->_em->getConnection();

            if ($customWhere = $filter->getCustomwhere()) {
                $result = $connection->fetchAllAssociative("
                    SELECT q.min, q.max
                    FROM (" . $customWhere . ") as q
                ");
            }

            if (empty($result)) {
                return false;
            }

            $data = array_shift($result);
        } elseif (!empty($filter->getSystemdictionary())) {

            if (count($parentParams)) {
                $parents = $filter->getParents();
                if (count($parents)) {
                    foreach ($parents as $parent) {
                        $parentId = $parent->getSubj()->getId();
                        if ($parentId && $parentParams[$parentId]) {
                            $customWhere .= ' AND ' . str_replace('#VALUES#',
                                    $parentParams[$parentId], $parent->getCustomWhere());
                        }
                    }
                }
            }

            $systemDictionary = $this->_em
                ->getRepository(InfoSystemdictionary::class)
                ->find($filter->getSystemdictionary());

            if (!$systemDictionary) {
                return false;
            }

            $tableName = $systemDictionary->getTablename();

            $where = 'WHERE 1=1';

            if (!empty($filter->getCustomwhere())) {
                $where .= ' AND ' . $filter->getCustomwhere();
                if ($user) {
                    $where = str_replace('#USER_ID#', $user->getId(), $where);
                }
            }
            $where .= $customWhere;

            /**
             * @var \Doctrine\DBAL\Connection $connection
             */
            $connection = $this->_em->getConnection();

            $c = $this->dp->getConcatOperator();

            $query = "SELECT id, name FROM $tableName $where ORDER BY name ASC";

            $subtitle = $filter->getSubtitle();
            $dependField = $filter->getDependField();
            $additionalDict = $filter->getAdditionalDict();

            if ($subtitle && $dependField && $additionalDict) {
                $query = "
                    SELECT $tableName.id, $tableName.name $c {$this->dp->getIsNullFunctionExpression("NULLIF('(' $c $additionalDict.$subtitle $c ')', '()')", "''")} as name FROM $tableName 
                    LEFT JOIN $additionalDict ON  $tableName.$dependField = $additionalDict.id
                    $where
                    ORDER BY name ASC
                ";
            }

            if ($tableName === 'info_city') {
                $query = "
                    SELECT $tableName.id, $tableName.name $c ', ' $c info_region.name as name
                    FROM $tableName
                    INNER JOIN info_region on $tableName.region_id = info_region.id
                    $where
                    ORDER BY name ASC
                ";
            }

            $result = $connection->fetchAllAssociative($query);

            if (!$result) {
                return false;
            }

            $data = $result;
        } elseif ($customDictionaryId = $filter->getCustomDictionary()) {

            $customDictionary = $this->_em
                ->getRepository(InfoCustomdictionary::class)
                ->find($customDictionaryId);

            if (!$customDictionary) {
                return false;
            }

            /**
             * @var \TeamSoft\CrmRepositoryBundle\Entity\InfoCustomdictionaryvalue $customDictionaryValues
             */
            $customDictionaryValues = $this->_em
                ->getRepository(InfoCustomdictionaryvalue::class)
                ->findBy(array('customdictionary' => $customDictionary));

            if (!$customDictionaryValues) {
                return false;
            }

            foreach ($customDictionaryValues as $key => $value) {
                /**
                 * @var \TeamSoft\CrmRepositoryBundle\Entity\InfoCustomdictionaryvalue $value
                 */
                $dictionaryValue = $value->getDictionaryvalue();
                $data[] = array(
                    'id' => "'$dictionaryValue'",
                    'name' => $dictionaryValue
                );
            }
        }
        return $data;
    }
}
