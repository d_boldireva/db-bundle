<?php

namespace TeamSoft\CrmRepositoryBundle\Repository;

use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query\ResultSetMappingBuilder;

class InfoTasktype extends EntityRepository
{

    public function findByFilter(array $filter = [], array $orderBy = [], int $roleId = null, int $languageId = null)
    {
        $tableName = $this->getClassMetadata()->getTableName();

        $joinParts = ["INNER JOIN info_tasktypebyrole on {$tableName}.id = info_tasktypebyrole.tasktype_id"];
        $whereParts = [];
        $orderByParts = [];

        if ($roleId) {
            $whereParts[] = "info_tasktypebyrole.role_id = $roleId";
        }

        if ($languageId) {
            $whereParts[] = "($tableName.language_id IS NULL OR $tableName.language_id = $languageId)";
        }

        $companyId = $filter['companyId'] ?? null;
        if ($companyId) {
            $optionalWhere = "NOT EXISTS (
                                    SELECT 1 FROM info_company AS c
                                    INNER JOIN info_companytypetasktype AS cttt ON cttt.companytype_id = c.companytype_id
                                    WHERE c.id = $companyId
                                )
                                or";

            if (isset($filter['onlyCompanyFilter'])) {
                $optionalWhere = "";
            }

            $whereParts[] =
                "(
                    $optionalWhere                    
                    EXISTS (
                    SELECT 1 FROM info_company AS c
                    INNER JOIN info_companytypetasktype AS cttt ON cttt.tasktype_id = $tableName.id AND cttt.companytype_id = c.companytype_id
                    WHERE c.id = $companyId
                    )
                )";
        }

        if (isset($filter['planId']) && $filter['planId']) {
            $whereParts[] = "EXISTS (
                SELECT 1 FROM info_plandetail
                WHERE info_plandetail.tasktype_id = {$tableName}.id AND plan_id = " . $filter['planId'] . "
            )";
        }

        $rsm = new ResultSetMappingBuilder($this->_em);
        $rsm->addRootEntityFromClassMetadata($this->_entityName, $tableName, array(), ResultSetMappingBuilder::COLUMN_RENAMING_INCREMENT);

        foreach ($orderBy as $value) {
            $order = strtoupper($value["order"]);
            if ($order === "ASC" || $order === "DESC") {
                $column = strtolower($value["column"]);
                if ($column === 'companytypetasktype_id' && $companyId) {
                    $orderByParts[] = 'info_companytypetasktype.id ' . $order;
                    $joinParts[] = "LEFT JOIN info_company on info_company.id = $companyId";
                    $joinParts[] = "LEFT JOIN info_companytypetasktype on 
                    info_companytypetasktype.companytype_id = info_company.companytype_id 
                    and info_companytypetasktype.tasktype_id =  $tableName.id";
                }
            }
        }

        $selectClause = $rsm->generateSelectClause();

        $join = implode(' ', $joinParts);
        $where = count($whereParts) ? 'WHERE ' . implode(' AND ', $whereParts) : '';
        $order = count($orderByParts) ? implode(',', $orderByParts) : "$tableName.name";

        $sql = "
            SELECT $selectClause FROM $tableName                       
            $join 
            $where 
            ORDER BY $order  
        ";

        $query = $this->getEntityManager()->createNativeQuery($sql, $rsm);
        return $query->getResult();
    }

    public function findByPresentationReplaceFilter($filter = [])
    {
        $tableName = $this->getClassMetadata()->getTableName();

        $joinParts = ["INNER JOIN info_tasktypebyrole on {$tableName}.id = info_tasktypebyrole.tasktype_id"];

        $whereParts = [];

        $optionalWhere = [];
        $optionalJoin = [];

        if (isset($filter['datefrom']) && $filter['datefrom']) {
            $optionalWhere[] = "info_plan.datefrom <= '{$filter['datefrom']}'";
        }

        if (isset($filter['datetill']) && $filter['datetill']) {
            $optionalWhere[] = "info_plan.datetill >= '{$filter['datetill']}'";
        }

        if (isset($filter['directionId']) && $filter['directionId']) {
            $optionalJoin[] = "
                INNER JOIN info_userdirection on info_userdirection.user_id = info_plan.owner_id
            ";
            $optionalWhere[] = "info_userdirection.direction_id = {$filter['directionId']}";
        }

        if (isset($filter['subdirectionId']) && $filter['subdirectionId']) {
            $optionalJoin[] = "
                INNER JOIN info_usersubdirection on info_usersubdirection.user_id = info_plan.owner_id
            ";
            $optionalWhere[] = "info_usersubdirection.subdirection_id = {$filter['subdirectionId']}";
        }

        $optionalJoin = implode(' ', $optionalJoin);
        if (!empty($optionalWhere)) {
            $optionalWhere = ' AND ' . join(' AND ', $optionalWhere);
        } else {
            $optionalWhere = "";
        }

        $whereParts[] = "
            exists(select 1 from info_plan 
                INNER JOIN info_plandetail on info_plandetail.plan_id = info_plan.id
                INNER JOIN info_planpresentation on info_planpresentation.plandetail_id = info_plandetail.id
                INNER JOIN info_clm on info_clm.id = info_planpresentation.presentation_id
                {$optionalJoin}
                where info_plandetail.tasktype_id = {$tableName}.id {$optionalWhere}
            )
        ";

        $rsm = new ResultSetMappingBuilder($this->_em);
        $rsm->addRootEntityFromClassMetadata($this->_entityName, $tableName, array(), ResultSetMappingBuilder::COLUMN_RENAMING_INCREMENT);

        $selectClause = $rsm->generateSelectClause();

        $join = implode(' ', $joinParts);
        $where = count($whereParts) ? 'WHERE ' . implode(' AND ', $whereParts) : '';

        $sql = "
            SELECT $selectClause FROM $tableName                       
            $join 
            $where 
        ";

        $query = $this->getEntityManager()->createNativeQuery($sql, $rsm);
        return $query->getResult();
    }

    /**
     * @param array $filter
     * @param array $orderBy
     * @param integer $offset
     * @param integer $limit
     * @param bool $filter_existing
     * @return mixed
     */
    public function findWithReportsByFilter($filter = [], $orderBy = [], $offset = null, $limit = null, $filter_existing = false)
    {
        $this->findAll();

        $queryBuilder = $this->createQueryBuilder('tasktype');
        $queryBuilder
            ->distinct(true)
            ->select('partial tasktype.{id,name}');

        if ($offset) {
            $queryBuilder->setFirstResult($offset);
        }

        if ($limit) {
            $queryBuilder->setMaxResults($limit);
        }

        if ($filter_existing) {
            $queryBuilder
                ->innerJoin(\TeamSoft\CrmRepositoryBundle\Entity\InfoTask::class, $reportAlias = 'report', 'WITH', "{$reportAlias}.tasktype = tasktype")
                ->innerJoin("{$reportAlias}.taskFileGroupCollection", $tfgAlias = 'tfg')
                ->innerJoin("{$tfgAlias}.taskImageCollection", $tfiAlias = 'tfi');


            //filter tasks
            $datefrom = $filter['datefrom'] ?? null;
            $datetill = $filter['datetill'] ?? null;
            if ($datefrom && $datetill) {
                $queryBuilder
                    ->andWhere($queryBuilder->expr()->between("{$reportAlias}.datefrom", ':datefrom', ':datetill'))
                    ->setParameters([
                        ':datefrom' => $datefrom,
                        ':datetill' => $datetill
                    ]);
            } elseif ($datefrom) {
                $queryBuilder
                    ->andWhere($queryBuilder->expr()->gte("{$reportAlias}.datefrom", ':datefrom'))
                    ->setParameter(':datefrom', $datefrom);
            } elseif ($datetill) {
                $queryBuilder
                    ->andWhere($queryBuilder->expr()->lte("{$reportAlias}.datefrom", ':datetill'))
                    ->setParameter(':datetill', $datetill);
            }

            if ($company = $filter['company'] ?? null) {
                $queryBuilder
                    ->andWhere($queryBuilder->expr()->in("{$reportAlias}.company", ':company'))
                    ->setParameter(':company', $company);
            }

            if ($contact = $filter['contact'] ?? null) {
                $queryBuilder
                    ->andWhere($queryBuilder->expr()->in("{$reportAlias}.contact", ':contact'))
                    ->setParameter(':contact', $contact);
            }

            if ($net = $filter['net'] ?? null) {
                $queryBuilder
                    ->innerJoin("{$reportAlias}.company", 'company')
                    ->andWhere($queryBuilder->expr()->in('company.centerId', ':net'))
                    ->setParameter(':net', $net);
            }

            if ($main = $filter['main'] ?? null) {
                $queryBuilder
                    ->innerJoin("{$reportAlias}.company", 'company')
                    ->andWhere($queryBuilder->expr()->in('company.main', ':main'))
                    ->setParameter(':main', $main);
            }

            if ($responsible = $filter['responsible'] ?? null) {
                $queryBuilder
                    ->andWhere($queryBuilder->expr()->in("{$reportAlias}.responsible", ':responsible'))
                    ->setParameter(':responsible', $responsible);
            }

            if ($taskstate = $filter['taskstate'] ?? null) {
                $queryBuilder
                    ->andWhere($queryBuilder->expr()->in("{$reportAlias}.taskstate", ':taskstate'))
                    ->setParameter(':taskstate', $taskstate);
            }

            if ($phototype = $filter['phototype'] ?? null) {
                $phototype = array_map(function (\TeamSoft\CrmRepositoryBundle\Entity\InfoCustomdictionaryvalue $customdictionaryvalue) {
                    return $customdictionaryvalue->getName();
                }, $phototype);

                $queryBuilder
                    ->andWhere($queryBuilder->expr()->in("{$tfgAlias}.name", ':phototype'))
                    ->setParameter(':phototype', $phototype);
            }

            if ($phototostate = $filter['photostate'] ?? null) {
                $queryBuilder
                    ->andWhere($queryBuilder->expr()->in("{$tfiAlias}.photostate", ':photostate'))
                    ->setParameter(':phototostate', $phototostate);
            }

            if ($photoservicetype = $filter['photoservicetype'] ?? null) {
                $queryBuilder
                    ->andWhere($queryBuilder->expr()->in("{$tfiAlias}.photoservicetype", ':photoservicetype'))
                    ->setParameter(':photoservicetype', $photoservicetype);
            }

            if ($whatsinside = $filter['whatsinside'] ?? null) {
                $queryBuilder
                    ->andWhere($queryBuilder->expr()->in("{$tfiAlias}.whatsinside", ':whatsinside'))
                    ->setParameter(':whatsinside', $whatsinside);
            }

            if ($brand = $filter['brand'] ?? null) {
                $queryBuilder
                    ->andWhere($queryBuilder->expr()->in("{$tfiAlias}.brand", ':brand'))
                    ->setParameter(':brand', $brand);
            }

            if ($specialization = $filter['specialization'] ?? null) {
                $queryBuilder
                    ->innerJoin("{$reportAlias}.contact", 'contact')
                    ->andWhere($queryBuilder->expr()->in('contact.specialization', ':specialization'))
                    ->setParameter(':specialization', $specialization);
            }

            if ($regionpart = $filter['regionpart'] ?? null) {
                $subquery_company = $this->getEntityManager()->getRepository(\TeamSoft\CrmRepositoryBundle\Entity\InfoCompany::class)->createQueryBuilder('company');
                $subquery_company
                    ->innerJoin('company.region', 'company_region')
                    ->innerJoin('company_region.regionparts', 'company_regionpart')
                    ->andWhere($subquery_company->expr()->eq('company', "{$reportAlias}.company"))
                    ->andWhere($subquery_company->expr()->in('company_regionpart', ':regionpart'))
                    ->setMaxResults(1);

                $subquery_contact = $this->getEntityManager()->getRepository(\TeamSoft\CrmRepositoryBundle\Entity\InfoContact::class)->createQueryBuilder('contact');
                $subquery_contact
                    ->innerJoin('contact.region', 'contact_region')
                    ->innerJoin('contact_region.regionparts', 'contact_regionparts')
                    ->andWhere($subquery_company->expr()->eq('contact', "{$reportAlias}.contact"))
                    ->andWhere($subquery_contact->expr()->in('contact_regionparts', ':regionpart'))
                    ->setMaxResults(1);

                $queryBuilder
                    ->andWhere($queryBuilder->expr()->orX()->addMultiple([
                        $queryBuilder->expr()->exists($subquery_company),
                        $queryBuilder->expr()->exists($subquery_contact)
                    ]))
                    ->setParameter(':regionpart', $regionpart);
            }

            if ($region = $filter['region'] ?? null) {
                $subquery_company = $this->getEntityManager()->getRepository(\TeamSoft\CrmRepositoryBundle\Entity\InfoCompany::class)->createQueryBuilder('company');
                $subquery_company
                    ->andWhere($subquery_company->expr()->eq('company', "{$reportAlias}.company"))
                    ->andWhere($subquery_company->expr()->in('company.region', ':region'))
                    ->setMaxResults(1);

                $subquery_contact = $this->getEntityManager()->getRepository(\TeamSoft\CrmRepositoryBundle\Entity\InfoContact::class)->createQueryBuilder('contact');
                $subquery_contact
                    ->andWhere($subquery_contact->expr()->eq('contact', "{$reportAlias}.contact"))
                    ->andWhere($subquery_contact->expr()->in('contact.region', ':region'));

                $queryBuilder
                    ->andWhere($queryBuilder->expr()->orX()->addMultiple([
                        $queryBuilder->expr()->exists($subquery_company),
                        $queryBuilder->expr()->exists($subquery_contact)
                    ]))
                    ->setParameter(':region', $region);
            }

            if ($city = $filter['city'] ?? null) {
                $subquery_company = $this->getEntityManager()->getRepository(\TeamSoft\CrmRepositoryBundle\Entity\InfoCompany::class)->createQueryBuilder('company');
                $subquery_company
                    ->andWhere($subquery_company->expr()->eq('company', "{$reportAlias}.company"))
                    ->andWhere($subquery_company->expr()->in('company.city', ':city'))
                    ->setMaxResults(1);

                $subquery_contact = $this->getEntityManager()->getRepository(\TeamSoft\CrmRepositoryBundle\Entity\InfoContact::class)->createQueryBuilder('contact');
                $subquery_contact
                    ->andWhere($subquery_company->expr()->eq('contact', "{$reportAlias}.company"))
                    ->andWhere($subquery_company->expr()->in('contact.city', ':city'))
                    ->setMaxResults(1);

                $queryBuilder->andWhere($queryBuilder->expr()->orX()->addMultiple([
                    $queryBuilder->expr()->exists($subquery_company),
                    $queryBuilder->expr()->exists($subquery_contact)
                ]))
                    ->setParameter(':city', $city);
            }
        }

        foreach ($orderBy as $attribute => $direction) {
            $queryBuilder->addOrderBy("tasktype.{$attribute}", $direction);
        }

        return $queryBuilder->getQuery()->getResult();
    }
}
