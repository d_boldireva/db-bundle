<?php

namespace TeamSoft\CrmRepositoryBundle\Repository;

use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query\ResultSetMappingBuilder;

class InfoDictionary extends EntityRepository
{

    /**
     * selects all specializations, which were used in info_contact
     * @return array
     */
    public function getSpecializationsUsedInContacts()
    {
        $tableName = $this->getClassMetadata()->getTableName();

        $sql = "
            SELECT info_dictionary.id, info_dictionary.Name
            FROM info_dictionary
            JOIN info_contact ON info_contact.Specialization_id = info_dictionary.id
            ORDER BY info_dictionary.Name
        ";

        $rsm = new ResultSetMappingBuilder($this->_em);
        $rsm->addRootEntityFromClassMetadata($this->_entityName, $tableName);
        $rsm->addEntityResult($this->_entityName, $tableName);

        $query = $this->getEntityManager()->createNativeQuery($sql, $rsm);

        return $query->getResult();
    }

    public function findSpecializationByContactId($contactId, int $languageId = null)
    {
        $tableName = $this->getClassMetadata()->getTableName();

        $whereParts = [
            "$tableName.id IN (
                SELECT specialization_id FROM info_contact WHERE id = $contactId
                UNION
                SELECT spec_id AS specialization_id FROM info_contactspec WHERE contact_id = $contactId
            )"
        ];

        if ($languageId) {
            $whereParts[] = "($tableName.language_id IS NULL OR $tableName.language_id = $languageId)";
        }

        $rsm = new ResultSetMappingBuilder($this->_em);
        $rsm->addRootEntityFromClassMetadata($this->_entityName, $tableName, array(), ResultSetMappingBuilder::COLUMN_RENAMING_INCREMENT);

        $selectClause = $rsm->generateSelectClause();

        $where = count($whereParts) ? 'WHERE ' . implode(' AND ', $whereParts) : '';

        $sql = "
            SELECT $selectClause FROM $tableName
            $where
            ORDER BY $tableName.name
        ";

        $query = $this->getEntityManager()->createNativeQuery($sql, $rsm);

        return $query->getResult();
    }

    public function findByPlanPresentationReplaceFilter(array $filter = [])
    {
        $tableName = $this->getClassMetadata()->getTableName();

        $whereParts = [];
        $joinParts = [
            "INNER JOIN info_user on info_user.position_id = {$tableName}.id",
        ];


        $optionalWhere = [];
        $optionalJoin = [];

        if (isset($filter['datefrom']) && $filter['datefrom']) {
            $optionalWhere[] = "info_plan.datefrom <= '{$filter['datefrom']}'";
        }

        if (isset($filter['datetill']) && $filter['datetill']) {
            $optionalWhere[] = "info_plan.datetill >= '{$filter['datetill']}'";
        }

        if (isset($filter['directionId']) && $filter['directionId']) {
            $optionalJoin[] = "
                INNER JOIN info_userdirection on info_userdirection.user_id = info_plan.owner_id
            ";
            $optionalWhere[] = "info_userdirection.direction_id = {$filter['directionId']}";
        }

        if (isset($filter['subdirectionId']) && $filter['subdirectionId']) {
            $optionalJoin[] = "
                INNER JOIN info_usersubdirection on info_usersubdirection.user_id = info_plan.owner_id
            ";
            $optionalWhere[] = "info_usersubdirection.subdirection_id = {$filter['subdirectionId']}";
        }

        $optionalJoin = implode(' ', $optionalJoin);
        if (!empty($optionalWhere)) {
            $optionalWhere = ' AND ' . join(' AND ', $optionalWhere);
        } else {
            $optionalWhere = "";
        }

        $whereParts[] = "
            exists(select 1 from info_plan 
                INNER JOIN info_plandetail on info_plandetail.plan_id = info_plan.id
                INNER JOIN info_planpresentation on info_planpresentation.plandetail_id = info_plandetail.id
                INNER JOIN info_clm on info_clm.id = info_planpresentation.presentation_id
                {$optionalJoin}
                where info_plan.owner_id = info_user.id {$optionalWhere}
            )
        ";


        $rsm = new ResultSetMappingBuilder($this->_em);
        $rsm->addRootEntityFromClassMetadata($this->_entityName, $tableName, array(), ResultSetMappingBuilder::COLUMN_RENAMING_INCREMENT);
        $rsm->addScalarResult('name', 'name');
        $rsm->addScalarResult('id', 'id', 'integer');

        $join = join(' ', $joinParts);
        $where = count($whereParts) ? 'WHERE ' . implode(' AND ', $whereParts) : '';

        $sql = "
            SELECT DISTINCT {$tableName}.id, {$tableName}.name  FROM {$tableName}   
            {$join}
            {$where}   
        ";

        $query = $this->getEntityManager()->createNativeQuery($sql, $rsm);
        return $query->getResult();
    }
}
