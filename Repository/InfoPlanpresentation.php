<?php

namespace TeamSoft\CrmRepositoryBundle\Repository;

use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query\ResultSetMappingBuilder;
use TeamSoft\CrmRepositoryBundle\Service\DatabasePlatform;
use Doctrine\ORM\Mapping;

class InfoPlanpresentation extends EntityRepository
{
    private $dp;

    public function __construct(EntityManagerInterface $em, Mapping\ClassMetadata $class)
    {
        parent::__construct($em, $class);
        $this->dp = $em->getConnection()->getDatabasePlatform();
    }

    public function findByReplaceFilter(array $filter = [])
    {
        $tableName = $this->getClassMetadata()->getTableName();
        $whereParts = [];
        $joinParts = [
            "INNER JOIN info_plandetail on info_plan.id = info_plandetail.plan_id",
            "INNER JOIN info_planpresentation on info_plandetail.id = info_planpresentation.plandetail_id",
            "INNER JOIN info_clm on info_clm.id = info_planpresentation.presentation_id"
        ];

        $optionalWhere = [];
        $optionalJoin = [];

        if (isset($filter['datefrom']) && $filter['datefrom']) {
            $optionalWhere[] = "info_plan.datefrom <= '{$filter['datefrom']}'";
        }

        if (isset($filter['datetill']) && $filter['datetill']) {
            $optionalWhere[] = "info_plan.datetill >= '{$filter['datetill']}'";
        }

        if (isset($filter['directionId']) && $filter['directionId']) {
            $optionalJoin[] = "
                INNER JOIN info_userdirection on info_userdirection.user_id = info_plan.owner_id
            ";
            $optionalWhere[] = "info_userdirection.direction_id = {$filter['directionId']}";
        }

        if (isset($filter['subdirectionId']) && $filter['subdirectionId']) {
            $optionalJoin[] = "
                INNER JOIN info_usersubdirection on info_usersubdirection.user_id = info_plan.owner_id
            ";
            $optionalWhere[] = "info_usersubdirection.subdirection_id = {$filter['subdirectionId']}";
        }

        if (isset($filter['taskTypeId']) && $filter['taskTypeId']) {
            $optionalJoin[] = "
                INNER JOIN info_tasktype on info_tasktype.id = info_plandetail.tasktype_id
            ";
            $optionalWhere[] = "info_tasktype.id = {$filter['taskTypeId']}";
        }

        if (isset($filter['targetGroupId']) && $filter['targetGroupId']) {
            $optionalJoin[] = "
                INNER JOIN info_target on info_target.id = info_plandetail.target_id
            ";
            $optionalWhere[] = "info_target.id = {$filter['targetGroupId']}";
        }

        if (isset($filter['contactCategoryId']) && $filter['contactCategoryId']) {
            $optionalJoin[] = "
                INNER JOIN info_contactcateg on info_contactcateg.id = info_plandetail.category_id
            ";
            $optionalWhere[] = "info_contactcateg.id = {$filter['contactCategoryId']}";
        }

        if (isset($filter['companyCategoryId']) && $filter['companyCategoryId']) {
            $optionalJoin[] = "
                INNER JOIN info_companycategory on info_companycategory.id = info_plandetail.categorycompany_id
            ";
            $optionalWhere[] = "info_companycategory.id = {$filter['companyCategoryId']}";
        }

        if (isset($filter['userIds']) && $filter['userIds']) {
            $optionalWhere[] = "info_plan.owner_id in ({$filter['userIds']})";
        }

        if (isset($filter['taskNumberId']) && $filter['taskNumberId']) {
            $optionalWhere[] = "info_planpresentation.ntask = {$filter['taskNumberId']}";
        }

        if (isset($filter['presentationPositionId']) && $filter['presentationPositionId']) {
            $optionalWhere[] = "info_planpresentation.position = {$filter['presentationPositionId']}";
        }

        if (isset($filter['dictionaryId']) && $filter['dictionaryId']) {
            $optionalJoin[] = "
                INNER JOIN info_user on info_user.id = info_plan.owner_id
                INNER JOIN info_dictionary on info_user.position_id = info_dictionary.id
            ";
            $optionalWhere[] = "info_dictionary.id = {$filter['dictionaryId']}";
        }

        $optionalJoin = implode(' ', $optionalJoin);

        if (!empty($optionalWhere)) {
            $optionalWhere = join(' AND ', $optionalWhere);
            $whereParts[] = "
                exists(select 1 from info_plan 
                    {$optionalJoin}
                    where info_plandetail.plan_id = info_plan.id AND {$optionalWhere}
                )
            ";
        } else {
            $whereParts = ["datetill >= " . $this->dp->getCurrentDateSQL()];
        }

        $rsm = new ResultSetMappingBuilder($this->_em);
        $rsm->addRootEntityFromClassMetadata($this->_entityName, $tableName, array(), ResultSetMappingBuilder::COLUMN_RENAMING_INCREMENT);
        $rsm->addScalarResult('id', 'id', 'integer');
        $rsm->addScalarResult('name', 'name');
        $rsm->addScalarResult('planpresentation_id', 'planpresentation_id', 'integer');

        $join = join(' ', $joinParts);
        $where = count($whereParts) ? 'WHERE ' . implode(' AND ', $whereParts) : '';

        $sql = "
            SELECT info_clm.id, info_clm.name, info_planpresentation.id as planpresentation_id FROM info_plan 
            {$join}
            {$where}  
            ORDER BY info_clm.name
        ";

        $query = $this->getEntityManager()->createNativeQuery($sql, $rsm);
        return $query->getResult();
    }

    public function replace($to, $planPresentationIds)
    {
        if ($to && count($planPresentationIds) > 0) {
            $planPresentationIds = implode(',', $planPresentationIds);
            $sql = "update info_planpresentation set presentation_id = {$to}
                where id in (
                        select ps.id
                from    info_plan p
                        inner join info_plandetail pd on pd.plan_id = p.id
                        inner join info_planpresentation ps on ps.plandetail_id = pd.id 
                        inner join info_clm c on c.id = ps.presentation_id
                where  ps.id in ($planPresentationIds)
                )";

            return $this->_em->getConnection()
                ->prepare($sql)
                ->execute();
        }

        return false;
    }

    public function findUniqueColumnList($parameter, array $filter = [])
    {

        $whereParts = ["info_planpresentation.{$parameter} IS NOT NULL"];

        $optionalWhere = [];
        $optionalJoin = [];

        if (isset($filter['datefrom']) && $filter['datefrom']) {
            $optionalWhere[] = "info_plan.datefrom <= '{$filter['datefrom']}'";
        }

        if (isset($filter['datetill']) && $filter['datetill']) {
            $optionalWhere[] = "info_plan.datetill >= '{$filter['datetill']}'";
        }

        if (isset($filter['directionId']) && $filter['directionId']) {
            $optionalJoin[] = "
                INNER JOIN info_userdirection on info_userdirection.user_id = info_plan.owner_id
            ";
            $optionalWhere[] = "info_userdirection.direction_id = {$filter['directionId']}";
        }

        if (isset($filter['subdirectionId']) && $filter['subdirectionId']) {
            $optionalJoin[] = "
                INNER JOIN info_usersubdirection on info_usersubdirection.user_id = info_plan.owner_id
            ";
            $optionalWhere[] = "info_usersubdirection.subdirection_id = {$filter['subdirectionId']}";
        }

        $optionalJoin = implode(' ', $optionalJoin);
        if (!empty($optionalWhere)) {
            $optionalWhere = ' AND ' . join(' AND ', $optionalWhere);
        } else {
            $optionalWhere = "";
        }

        $whereParts[] = "
            exists(select 1 from info_plan 
                INNER JOIN info_plandetail on info_plandetail.plan_id = info_plan.id
                INNER JOIN info_planpresentation on info_planpresentation.plandetail_id = info_plandetail.id
                INNER JOIN info_clm on info_clm.id = info_planpresentation.presentation_id
                {$optionalJoin}
                where info_plandetail.id = info_planpresentation.plandetail_id {$optionalWhere}
            )
        ";

        $tableName = $this->getClassMetadata()->getTableName();

        $rsm = new ResultSetMappingBuilder($this->_em);
        $rsm->addRootEntityFromClassMetadata($this->_entityName, $tableName, array(), ResultSetMappingBuilder::COLUMN_RENAMING_INCREMENT);
        $rsm->addScalarResult('id', 'id');
        $rsm->addScalarResult('name', 'name');

        $where = count($whereParts) ? 'WHERE ' . implode(' AND ', $whereParts) : '';

        $sql = "
            SELECT MAX(info_planpresentation.{$parameter}) as id, info_planpresentation.{$parameter} as name FROM info_planpresentation                        
            {$where}   
            Group by info_planpresentation.{$parameter}
            ORDER BY info_planpresentation.{$parameter} ASC
        ";

        $query = $this->getEntityManager()->createNativeQuery($sql, $rsm);
        return $query->getResult();
    }

    /**
     * @param int $id
     * @return int|mixed|string
     * @throws \Doctrine\ORM\NoResultException
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function getCountByClmId(int $id)
    {
        return $this->createQueryBuilder('pp')
            ->select('count(pp.id)')
            ->where('pp.presentation = ' . $id)
            ->getQuery()
            ->getSingleScalarResult();
    }
}
