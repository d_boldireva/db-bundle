<?php

namespace TeamSoft\CrmRepositoryBundle\Repository;

use TeamSoft\CrmRepositoryBundle\Entity\PoApiToken;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method PoApiToken|null find($id, $lockMode = null, $lockVersion = null)
 * @method PoApiToken|null findOneBy(array $criteria, array $orderBy = null)
 * @method PoApiToken[]    findAll()
 * @method PoApiToken[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class PoApiTokenRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, PoApiToken::class);
    }

    // /**
    //  * @return PoApiToken[] Returns an array of PoApiToken objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('p.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?PoApiToken
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
