<?php

namespace TeamSoft\CrmRepositoryBundle\Repository;

use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use TeamSoft\CrmRepositoryBundle\Service\DatabasePlatform;
use Doctrine\Persistence\ManagerRegistry;
use TeamSoft\CrmRepositoryBundle\Entity\InfoMcmcontent as InfoMcmcontentEntity;

/**
 * @see \TeamSoft\CrmRepositoryBundle\Entity\InfoMcmcontent
 * Class InfoMcmcontent
 * @package TeamSoft\CrmRepositoryBundle\Repository
 */
class InfoMcmcontent extends ServiceEntityRepository
{
    private $dp;

    public function __construct(ManagerRegistry $registry, DatabasePlatform $dp)
    {
        parent::__construct($registry, InfoMcmcontentEntity::class);
        $this->dp = $dp;
    }

    /**
     * @param array $filters
     * @param integer $offset
     * @param integer $limit
     * @param integer $archived
     * @return array|boolean
     */
    public function findByFilter(array $filters, $offset, $limit, $archived, $orderBy = null, $conferenceShareContent = false)
    {
        $queryBuilder = $this->createQueryBuilder('c');

        if ($archived) {
            $queryBuilder
                ->where('c.isarchive = 1');
        } else {
            $queryBuilder
                ->where('c.isarchive = 0')
                ->orWhere('c.isarchive is null');
        }

        $queryBuilder
            ->andWhere('c.parent is null');

        if (isset($filters['search'])) {
            $queryBuilder
                ->andWhere('LOWER(c.name) LIKE :search')
                ->setParameter('search', mb_strtolower('%'.$filters['search'].'%'));
        }

        if ($conferenceShareContent) {
            $contents = $this->getConferenceContentsByUser($filters);
            $where = '';
            if ($contents) {
                foreach ($contents as $row) {
                    $contentsId[] = $row['content_id'];
                }
                $where = " OR c.id in (".implode(',', $contentsId).")";
            }

            $andWhere = "(c.user in (:users) ".$where.")";
        } else {
            $andWhere = 'c.user in (:users)';
        }

        if (!empty($filters['user'])) {
            $users = $filters['user']->getSubordinates()->toArray();
            array_push($users, $filters['user']);
            $queryBuilder
                ->andWhere($andWhere)
                ->setParameter('users', $users);
        }

        $totalCountQueryBuilder = clone $queryBuilder;
        $totalCountQueryBuilder->select('COUNT(c.id)');

        $column = 'created';
        $order = 'DESC';
        $orderBy = json_decode($orderBy, true);
        if ($orderBy) {
            foreach ($orderBy as $value) {
                $order = strtoupper($value["order"]);
                if ($order === "ASC" || $order === "DESC") {
                    $column = strtolower($value["column"]);
                    if ($column == "created") {
                        $queryBuilder->addOrderBy('c.' . $column, $order);
                    } else {
                        if ($column == "name") {
                            $queryBuilder->addOrderBy('c.' . $column, $order);
                        }
                    }
                }
            }
        } else {
            $queryBuilder->addOrderBy('c.' . $column, $order);
        }

        if ($offset) {
            $queryBuilder->setFirstResult($offset);
        }

        if ($limit) {
            $queryBuilder->setMaxResults($limit);
        }

        return [
            'total_count' => $totalCountQueryBuilder->getQuery()->getSingleScalarResult(),
            'data' => $queryBuilder->getQuery()->getResult() ?? []
        ];
    }

    private function getConferenceContentsByUser($filters)
    {
        $connection = $this->_em->getConnection();
        $sql = "SELECT distinct(content_id) 
                FROM info_mcmconferencecontent as cc
                JOIN info_mcmconferenceresponsible as cr ON cc.conference_id = cr.conference_id
                WHERE cr.user_id = ".$filters['user']->getId();

        $statement = $connection->prepare($sql);
        return $statement->executeQuery()->fetchAllAssociative();
    }

    /**
     * @param string $type
     * @return array|boolean
     */
    public function findByType(string $type)
    {
        return $this->createQueryBuilder('c')
            ->where("c.type = '" . $type . "'")
            ->andWhere('(c.isarchive = 0 OR c.isarchive is null)')
            ->getQuery()
            ->getResult();
    }
}
