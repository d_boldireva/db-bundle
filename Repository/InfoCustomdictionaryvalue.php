<?php

namespace TeamSoft\CrmRepositoryBundle\Repository;

use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query\ResultSetMappingBuilder;
use TeamSoft\CrmRepositoryBundle\Entity\InfoTaskfilegroup;

/**
 * @see \TeamSoft\CrmRepositoryBundle\Entity\InfoCustomdictionaryvalue
 * Class InfoCustomdictionaryvalue
 * @package TeamSoft\CrmRepositoryBundle\Repository
 */
class InfoCustomdictionaryvalue extends EntityRepository
{

    public function findByCustomDictionaryName($dictName)
    {
        $tableName = $this->getClassMetadata()->getTableName();

        $rsm = new ResultSetMappingBuilder($this->_em);
        $rsm->addRootEntityFromClassMetadata($this->_entityName, $tableName, array(), ResultSetMappingBuilder::COLUMN_RENAMING_INCREMENT);

        //$dictName = iconv('utf-8', 'cp1251', $dictName);

        $selectClause = $rsm->generateSelectClause();
        $sql = "
            SELECT $selectClause FROM $tableName
            JOIN info_CustomDictionary cd ON $tableName.CustomDictionary_id = cd.id 
            WHERE cd.Name = '$dictName'
        ";

        $query = $this->getEntityManager()->createNativeQuery($sql, $rsm);

        $result = $query->getResult();

        return $result;
    }

    /**
     * @param string $code
     * @param array $filter
     * @param array $orderBy
     * @param integer $offset
     * @param integer $limit
     * @param bool $filter_existing
     * @return mixed
     */
    public function findWithReportsByDictionaryCode(string $code, $filter = [], $orderBy = [], $offset = null, $limit = null, $filter_existing = false)
    {
        $fieldsToSelect = [];

        foreach ($this->_em->getClassMetadata(\TeamSoft\CrmRepositoryBundle\Entity\InfoCustomdictionaryvalue::class)->fieldMappings as $fieldMapping) {
            if (in_array($fieldMapping['type'], ['integer', 'float', 'string'])) {
                $fieldsToSelect[] = $fieldMapping['fieldName'];
            }
        }

        $fieldsToSelect = implode(',', $fieldsToSelect);

        $queryBuilder = $this->createQueryBuilder($code);

        $queryBuilder
            ->innerJoin("$code.customdictionary", 'customdictionary')
            ->andWhere($queryBuilder->expr()->eq('customdictionary.code', ':code'))
            ->setParameter(':code', $code)
            ->distinct(true)
            ->select("partial $code.{{$fieldsToSelect}}");

        if ($offset) {
            $queryBuilder->setFirstResult($offset);
        }

        if ($limit) {
            $queryBuilder->setMaxResults($limit);
        }

        if (($phototype = $filter['phototype'] ?? null) && (in_array($code, ['phototype', 'phototype_doctor']))) {
            $queryBuilder
                ->andWhere($queryBuilder->expr()->in($code, ':phototype'))
                ->setParameter(':phototype', $phototype);
        }

        if ($filter_existing) {
            if (!in_array($code, ['phototype', 'phototype_doctor'])) {
                $queryBuilder
                    ->innerJoin(\TeamSoft\CrmRepositoryBundle\Entity\VwTaskfileimage::class, $tfiAlias = 'tfi', 'WITH', "{$tfiAlias}.{$code} = {$code}")
                    ->innerJoin("{$tfiAlias}.subj", $tfgAlias = 'tfg')
                    ->innerJoin("{$tfgAlias}.subj", $reportAlias = 'report');
            } else {
                $queryBuilder
                    ->innerJoin(InfoTaskfilegroup::class, $tfgAlias = 'tfg', 'WITH', "{$tfgAlias}.name = {$code}.dictionaryvalue")
                    ->innerJoin("{$tfgAlias}.subj", $reportAlias = 'report')
                    ->innerJoin("{$tfgAlias}.taskImageCollection", $tfiAlias = 'tfi');
            }

            if ($company = $filter['company'] ?? null) {
                $queryBuilder
                    ->andWhere($queryBuilder->expr()->in("{$reportAlias}.company", ':company'))
                    ->setParameter(':company', $company);
            }

            if ($contact = $filter['contact'] ?? null) {
                $queryBuilder
                    ->andWhere($queryBuilder->expr()->in("{$reportAlias}.contact", ':contact'))
                    ->setParameter(':contact', $contact);
            }

            if ($net = $filter['net'] ?? null) {
                $queryBuilder
                    ->innerJoin("{$reportAlias}.company", 'company')
                    ->andWhere($queryBuilder->expr()->in('company.centerId', ':net'))
                    ->setParameter(':net', $net);
            }

            if ($main = $filter['main'] ?? null) {
                $queryBuilder
                    ->innerJoin("{$reportAlias}.company", 'company')
                    ->andWhere($queryBuilder->expr()->in('company.main', ':main'))
                    ->setParameter(':main', $main);
            }

            if ($responsible = $filter['responsible'] ?? null) {
                $queryBuilder
                    ->andWhere($queryBuilder->expr()->in("{$reportAlias}.responsible", ':responsible'))
                    ->setParameter(':responsible', $responsible);
            }

            if ($tasktype = $filter['tasktype'] ?? null) {
                $queryBuilder
                    ->andWhere($queryBuilder->expr()->in("{$reportAlias}.tasktype", ':tasktype'))
                    ->setParameter(':tasktype', $tasktype);
            }

            if ($taskstate = $filter['taskstate'] ?? null) {
                $queryBuilder
                    ->andWhere($queryBuilder->expr()->in("{$reportAlias}.taskstate", ':taskstate'))
                    ->setParameter(':taskstate', $taskstate);
            }

            if (($phototype = $filter['phototype'] ?? null) && (!in_array($phototype, ['phototype', 'phototype_doctor']))) {
                $phototype = array_map(function (\TeamSoft\CrmRepositoryBundle\Entity\InfoCustomdictionaryvalue $customdictionaryvalue) {
                    return $customdictionaryvalue->getName();
                }, $phototype);

                $queryBuilder
                    ->andWhere($queryBuilder->expr()->in("{$tfgAlias}.name", ':phototype'))
                    ->setParameter(':phototype', $phototype);
            }

            if ($phototostate = $filter['photostate'] ?? null) {
                $queryBuilder
                    ->andWhere($queryBuilder->expr()->in("{$tfiAlias}.photostate", ':photostate'))
                    ->setParameter(':phototostate', $phototostate);
            }

            if (($photoservicetype = $filter['photoservicetype'] ?? null) && ($code != 'servicetype')) {
                $queryBuilder
                    ->andWhere($queryBuilder->expr()->in("{$tfiAlias}.photoservicetype", ':photoservicetype'))
                    ->setParameter(':photoservicetype', $photoservicetype);
            }

            if (($whatsinside = $filter['whatsinside'] ?? null) && ($code != 'whatsinside')) {
                $queryBuilder
                    ->andWhere($queryBuilder->expr()->in("{$tfiAlias}.whatsinside", ':whatsinside'))
                    ->setParameter(':whatsinside', $whatsinside);
            }

            if ($brand = $filter['brand'] ?? null) {
                $queryBuilder
                    ->andWhere($queryBuilder->expr()->in("{$tfiAlias}.brand", ':brand'))
                    ->setParameter(':brand', $brand);
            }

            if ($specialization = $filter['specialization'] ?? null) {
                $queryBuilder
                    ->innerJoin("{$reportAlias}.contact", 'contact')
                    ->andWhere($queryBuilder->expr()->in('contact.specialization', ':specialization'))
                    ->setParameter(':specialization', $specialization);
            }

            if ($regionpart = $filter['regionpart'] ?? null) {
                $subquery_company = $this->getEntityManager()->getRepository(\TeamSoft\CrmRepositoryBundle\Entity\InfoCompany::class)->createQueryBuilder('company');
                $subquery_company
                    ->innerJoin('company.region', 'company_region')
                    ->innerJoin('company_region.regionparts', 'company_regionpart')
                    ->andWhere($subquery_company->expr()->eq('company', "{$reportAlias}.company"))
                    ->andWhere($subquery_company->expr()->in('company_regionpart', ':regionpart'))
                    ->setMaxResults(1);

                $subquery_contact = $this->getEntityManager()->getRepository(\TeamSoft\CrmRepositoryBundle\Entity\InfoContact::class)->createQueryBuilder('contact');
                $subquery_contact
                    ->innerJoin('contact.region', 'contact_region')
                    ->innerJoin('contact_region.regionparts', 'contact_regionparts')
                    ->andWhere($subquery_company->expr()->eq('contact', "{$reportAlias}.contact"))
                    ->andWhere($subquery_contact->expr()->in('contact_regionparts', ':regionpart'))
                    ->setMaxResults(1);

                $queryBuilder
                    ->andWhere($queryBuilder->expr()->orX()->addMultiple([
                        $queryBuilder->expr()->exists($subquery_company),
                        $queryBuilder->expr()->exists($subquery_contact)
                    ]))
                    ->setParameter(':regionpart', $regionpart);
            }

            if ($region = $filter['region'] ?? null) {
                $subquery_company = $this->getEntityManager()->getRepository(\TeamSoft\CrmRepositoryBundle\Entity\InfoCompany::class)->createQueryBuilder('company');
                $subquery_company
                    ->andWhere($subquery_company->expr()->eq('company', "{$reportAlias}.company"))
                    ->andWhere($subquery_company->expr()->in('company.region', ':region'))
                    ->setMaxResults(1);

                $subquery_contact = $this->getEntityManager()->getRepository(\TeamSoft\CrmRepositoryBundle\Entity\InfoContact::class)->createQueryBuilder('contact');
                $subquery_contact
                    ->andWhere($subquery_contact->expr()->eq('contact', "{$reportAlias}.contact"))
                    ->andWhere($subquery_contact->expr()->in('contact.region', ':region'));

                $queryBuilder
                    ->andWhere($queryBuilder->expr()->orX()->addMultiple([
                        $queryBuilder->expr()->exists($subquery_company),
                        $queryBuilder->expr()->exists($subquery_contact)
                    ]))
                    ->setParameter(':region', $region);
            }

            if ($city = $filter['city'] ?? null) {
                $subquery_company = $this->getEntityManager()->getRepository(\TeamSoft\CrmRepositoryBundle\Entity\InfoCompany::class)->createQueryBuilder('company');
                $subquery_company
                    ->andWhere($subquery_company->expr()->eq('company', "{$reportAlias}.company"))
                    ->andWhere($subquery_company->expr()->in('company.city', ':city'))
                    ->setMaxResults(1);

                $subquery_contact = $this->getEntityManager()->getRepository(\TeamSoft\CrmRepositoryBundle\Entity\InfoContact::class)->createQueryBuilder('contact');
                $subquery_contact
                    ->andWhere($subquery_company->expr()->eq('contact', "{$reportAlias}.company"))
                    ->andWhere($subquery_company->expr()->in('contact.city', ':city'))
                    ->setMaxResults(1);

                $queryBuilder->andWhere($queryBuilder->expr()->orX()->addMultiple([
                    $queryBuilder->expr()->exists($subquery_company),
                    $queryBuilder->expr()->exists($subquery_contact)
                ]))
                    ->setParameter(':city', $city);
            }
        }

        foreach ($orderBy as $attribute => $direction) {
            $queryBuilder->addOrderBy("{$code}.{$attribute}", $direction);
        }

        return $queryBuilder->getQuery()->getResult();
    }

    /**
     * @param string $code
     * @return \TeamSoft\CrmRepositoryBundle\Entity\InfoCustomdictionaryvalue[]
     */
    public function findByCode(string $code): array
    {
        return $this->findByCodes([$code]);
    }

    /**
     * @param string[] $codes
     * @param bool $groupBy
     * @return \TeamSoft\CrmRepositoryBundle\Entity\InfoCustomdictionaryvalue[]
     */
    public function findByCodes(array $codes): array
    {
        $queryBuilder = $this->createQueryBuilder('d');

        return $queryBuilder
            ->innerJoin("d.customdictionary", 'customdictionary')
            ->andWhere('customdictionary.code IN (:codes)')
            ->setParameter(':codes', $codes)
            ->getQuery()
            ->getResult();
    }
} 