<?php
declare(strict_types=1);


namespace TeamSoft\CrmRepositoryBundle\Repository;


use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use TeamSoft\CrmRepositoryBundle\Entity\InfoTargetingActivityLog;

class InfoTargetingActivityLogRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, InfoTargetingActivityLog::class);
    }
}