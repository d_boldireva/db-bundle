<?php

namespace TeamSoft\CrmRepositoryBundle\Repository;

use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use TeamSoft\CrmRepositoryBundle\Entity\InfoMcmcontent;
use TeamSoft\CrmRepositoryBundle\Entity\InfoMcmdistribution;
use TeamSoft\CrmRepositoryBundle\Entity\InfoMcmtrigger;
use TeamSoft\CrmRepositoryBundle\Entity\InfoMcmfrom;

class InfoMcmfromRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, InfoMcmfrom::class);
    }

    /**
     * @param string $type
     * @param integer|null $countryId
     * @param integer|null $userId
     * @return \TeamSoft\CrmRepositoryBundle\Entity\InfoMcmfrom|null
     */
    public function findByTypeWithOptions(string $type, ?int $countryId, int $userId = null, bool $triggerType = false)
    {
        $query = $this->createQueryBuilder('f')
            ->where('f.type = :type')
            ->setParameter('type', $type);

        if ($countryId) {
            $query->andWhere('f.country = :country or f.country is NULL')
                ->setParameter('country', $countryId);
        }

        if ($userId) {
            $class = $triggerType ? InfoMcmtrigger::class : InfoMcmdistribution::class;
            $query->join($class, 'd', 'with', 'd.mcmfrom = f.id')
                ->orderBy('d.id', 'DESC')
                ->andWhere('d.owner = :user_id')
                ->setParameter('user_id', $userId)
                ->join(InfoMcmcontent::class, 'c', 'with', 'd.mcmcontent = c.id')
                ->andWhere('c.type = :type')
                ->setParameter('type', $type)
                ->setMaxResults(1);
        }

        return $query->getQuery()->getResult();
    }
}