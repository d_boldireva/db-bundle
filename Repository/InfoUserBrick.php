<?php

namespace TeamSoft\CrmRepositoryBundle\Repository;

use Doctrine\DBAL\FetchMode;
use Doctrine\ORM\EntityRepository;
use \TeamSoft\CrmRepositoryBundle\Entity\InfoUser as UserModel;

/**
 * Class InfoUserBrick
 * @see \TeamSoft\CrmRepositoryBundle\Entity\InfoUserbrick;
 * @package TeamSoft\CrmRepositoryBundle\Repository
 *
 */
class InfoUserBrick extends EntityRepository
{
    public const TYPE_RELATION_CONTACT = 'r_contact';
    public const TYPE_RELATION_COMPANY = 'r_company';

    public function getExistingRelations(UserModel $user, array $searchIds, string $typeRelation): array
    {
        if (!in_array($typeRelation, [self::TYPE_RELATION_COMPANY, self::TYPE_RELATION_CONTACT], true)) {
            throw new \RuntimeException(sprintf('Type "%s" is not support', $typeRelation));
        }

        $field = $typeRelation === self::TYPE_RELATION_CONTACT ? 'contact_id' : 'company_id';

        $sql = sprintf(
            'SELECT %1$s FROM info_userbrick where user_id=%2$d AND %1$s IN (%3$s)',
            $field, $user->getId(), implode(', ', $searchIds)
        );

        $columnIds = $this->getEntityManager()->getConnection()->executeQuery($sql)->fetchFirstColumn();

//        $builder = $this->createQueryBuilder('b')
//            ->where('b.user = :user')->setParameter('user', $user);
//
//        if ($typeRelation === self::TYPE_RELATION_CONTACT) {
//            $builder->andWhere('b.contact IN (:searchIds)');
//            $builder->select('b.contact');
//        } else {
//            $builder->andWhere('b.company IN (:searchIds)');
//            $builder->select('b.company');
//        }
//
//        $builder->setParameter('searchIds', $searchIds);
//
//        $columnIds = $builder->getQuery()->getResult(FetchMode::COLUMN);
        sort($columnIds);

        return $columnIds;
    }
}
