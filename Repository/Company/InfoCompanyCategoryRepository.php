<?php

namespace TeamSoft\CrmRepositoryBundle\Repository\Company;

use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\Query\Expr\Join;
use Doctrine\ORM\Query\ResultSetMappingBuilder;
use Doctrine\Persistence\ManagerRegistry;
use TeamSoft\CrmRepositoryBundle\Entity\InfoCompanycategory;
use TeamSoft\CrmRepositoryBundle\Entity\InfoDirection;
use TeamSoft\CrmRepositoryBundle\Entity\InfoUserdirection;

/**
 * Class InfoCompanycategory
 * @package TeamSoft\CrmRepositoryBundle\Repository\Company
 *
 * @see \TeamSoft\CrmRepositoryBundle\Entity\InfoCompanycategory
 */
class InfoCompanyCategoryRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, InfoCompanycategory::class);
    }

    public function findByUserOnDirection(?int $userId)
    {
        $query = $this->createQueryBuilder('c');
        $query->leftJoin(InfoDirection::class, 'd', Join::WITH, 'd = c.direction');

        // filter by user
        if ($userId) {
            $query->leftJoin(InfoUserdirection::class, 'ud', Join::WITH, 'ud.direction = c.direction');

            $query->where('ud.user = :user')
                ->orWhere('c.direction IS NULL') // <-- attention
                ->setParameter('user', $userId);
        }

        return $query
            ->orderBy('d.name', 'ASC')
            ->addOrderBy('c.name', 'ASC')
            ->getQuery()
            ->getResult();
    }

    public function findApproveCompanyCategory(?int $userId)
    {
        $query = $this->createQueryBuilder('ic')
            ->select(["ic.id", "concat(coalesce(concat(d.name, ' - '), ''), ic.name) as category_name", "d.id as direction_id", "ic.targetingCnt as count"])
            ->leftJoin(InfoDirection::class, 'd', Join::WITH, 'ic.direction = d');

        if ($userId) {
            $query->innerJoin(InfoUserdirection::class, 'iu', Join::WITH, 'iu.direction = d')
                ->where('iu.user = :user')
                ->setParameter('user', $userId);
        }
        $query->orderBy('d.id', 'ASC');
        return $query->getQuery()
            ->getArrayResult();
    }

    public function findByPlanPresentationReplaceFilter(array $filter = [])
    {
        $tableName = $this->getClassMetadata()->getTableName();

        $whereParts = [];
        $joinParts = ["INNER JOIN info_direction on info_direction.id = {$tableName}.direction_id"];


        $optionalWhere = [];
        $optionalJoin = [];

        if (isset($filter['datefrom']) && $filter['datefrom']) {
            $optionalWhere[] = "info_plan.datefrom <= '{$filter['datefrom']}'";
        }

        if (isset($filter['datetill']) && $filter['datetill']) {
            $optionalWhere[] = "info_plan.datetill >= '{$filter['datetill']}'";
        }

        if (isset($filter['directionId']) && $filter['directionId']) {
            $optionalJoin[] = "
                INNER JOIN info_userdirection on info_userdirection.user_id = info_plan.owner_id
            ";
            $optionalWhere[] = "info_userdirection.direction_id = {$filter['directionId']}";
            $whereParts[] = "{$tableName}.direction_id = {$filter['directionId']}";
        }

        if (isset($filter['subdirectionId']) && $filter['subdirectionId']) {
            $optionalJoin[] = "
                INNER JOIN info_usersubdirection on info_usersubdirection.user_id = info_plan.owner_id
            ";
            $optionalWhere[] = "info_usersubdirection.subdirection_id = {$filter['subdirectionId']}";
        }

        $optionalJoin = implode(' ', $optionalJoin);
        if (!empty($optionalWhere)) {
            $optionalWhere = ' AND ' . join(' AND ', $optionalWhere);
        } else {
            $optionalWhere = "";
        }

        $whereParts[] = "
            exists(select 1 from info_plan 
                INNER JOIN info_plandetail on info_plandetail.plan_id = info_plan.id
                INNER JOIN info_planpresentation on info_planpresentation.plandetail_id = info_plandetail.id
                INNER JOIN info_clm on info_clm.id = info_planpresentation.presentation_id
                {$optionalJoin}
                where info_plandetail.categorycompany_id = {$tableName}.id {$optionalWhere}
            )
        ";

        $rsm = new ResultSetMappingBuilder($this->_em);
        $rsm->addRootEntityFromClassMetadata($this->_entityName, $tableName, array(), ResultSetMappingBuilder::COLUMN_RENAMING_INCREMENT);
        $rsm->addScalarResult('name', 'name');
        $rsm->addScalarResult('id', 'id', 'integer');

        $join = join(' ', $joinParts);
        $where = count($whereParts) ? 'WHERE ' . implode(' AND ', $whereParts) : '';

        $sql = "
            SELECT {$tableName}.id, concat(coalesce(concat(info_direction.name, ' - '), ''), {$tableName}.name) as name  FROM {$tableName}   
            {$join}
            {$where}   
        ";

        $query = $this->getEntityManager()->createNativeQuery($sql, $rsm);
        return $query->getResult();
    }
}
