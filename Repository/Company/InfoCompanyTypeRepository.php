<?php
declare(strict_types=1);

namespace TeamSoft\CrmRepositoryBundle\Repository\Company;

use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query\ResultSetMappingBuilder;
use Doctrine\Persistence\ManagerRegistry;
use TeamSoft\CrmRepositoryBundle\Entity\InfoCompanytype;

/**
 * Class InfoCompanyTypeRepository
 * @package TeamSoft\CrmRepositoryBundle\Repository\Company
 * 
 * @method InfoCompanyType|null find($id, $lockMode = null, $lockVersion = null)
 * @method InfoCompanyType|null findOneBy(array $criteria, array $orderBy = null)
 * @method InfoCompanyType[]    findAll()
 * @method InfoCompanyType[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class InfoCompanyTypeRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, InfoCompanyType::class);
    }
    
    /**
     * selects all contact types, which were used in info_contact
     * @return array
     */
    public function getUsedInContacts()
    {
        $tableName = $this->getClassMetadata()->getTableName();

        $sql = "
            SELECT DISTINCT info_companytype.id, info_companytype.Name
            FROM info_contact
            INNER JOIN info_company ON info_company.id = info_contact.company_id
            INNER JOIN info_companytype ON info_companytype.id = info_company.companytype_id
        ";

        $rsm = new ResultSetMappingBuilder($this->_em);
        $rsm->addRootEntityFromClassMetadata($this->_entityName, $tableName);
        $rsm->addEntityResult($this->_entityName, $tableName);

        $query = $this->getEntityManager()->createNativeQuery($sql, $rsm);

        return $query->getResult();
    }

    public function findByFilter(array $filter, int $roleId = null, int $languageId = null)
    {
        $tableName = $this->getClassMetadata()->getTableName();

        $joinParts = ["INNER JOIN info_companytypebyrole on info_companytype.id = info_companytypebyrole.companytype_id"];
        $whereParts = [];

        if ($roleId) {
            $whereParts[] = "info_companytypebyrole.role_id = $roleId";
        }

        if ($languageId) {
            $whereParts[] = "($tableName.language_id IS NULL OR $tableName.language_id = $languageId)";
        }

        $rsm = new ResultSetMappingBuilder($this->_em);
        $rsm->addRootEntityFromClassMetadata($this->_entityName, $tableName, array(), ResultSetMappingBuilder::COLUMN_RENAMING_INCREMENT);

        $selectClause = $rsm->generateSelectClause();

        $join = implode(' ', $joinParts);
        $where = count($whereParts) ? 'WHERE ' . implode(' AND ', $whereParts) : '';

        $sql = "
            SELECT $selectClause FROM $tableName                       
            $join 
            $where
        ";

        $query = $this->getEntityManager()->createNativeQuery($sql, $rsm);
        return $query->getResult();
    }
}
