<?php
declare(strict_types=1);

namespace TeamSoft\CrmRepositoryBundle\Repository\Company;

use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\Query\ResultSetMappingBuilder;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Component\Security\Core\Security;
use TeamSoft\CrmRepositoryBundle\Entity\InfoCompany;
use TeamSoft\CrmRepositoryBundle\Entity\InfoCompanycateg;
use TeamSoft\CrmRepositoryBundle\Entity\InfoCompanycategory;
use TeamSoft\CrmRepositoryBundle\Entity\InfoCompanyowner;
use TeamSoft\CrmRepositoryBundle\Entity\InfoCompanyrequest;
use TeamSoft\CrmRepositoryBundle\Entity\InfoDcrtype;
use TeamSoft\CrmRepositoryBundle\Entity\InfoFiltersetmobile as InfoFiltersetmobileEntity;
use TeamSoft\CrmRepositoryBundle\Entity\InfoUser;
use TeamSoft\CrmRepositoryBundle\Entity\InfoContact;
use TeamSoft\CrmRepositoryBundle\Entity\InfoUser as InfoUserEntity;
use TeamSoft\CrmRepositoryBundle\Repository\SqlGeneratorTrait;
use TeamSoft\CrmRepositoryBundle\Entity\SaleProject\InfoTasksaleproject;
use TeamSoft\CrmRepositoryBundle\Service\DatabasePlatform;
use TeamSoft\CrmRepositoryBundle\Service\Options;
use TeamSoft\CrmRepositoryBundle\Utils\DateHelper;


/**
 * Class InfoCompanyRepository
 * @package TeamSoft\CrmRepositoryBundle\Repository\Company
 *
 * @method InfoCompany|null find($id, $lockMode = null, $lockVersion = null)
 * @method InfoCompany|null findOneBy(array $criteria, array $orderBy = null)
 * @method InfoCompany[]    findAll()
 * @method InfoCompany[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class InfoCompanyRepository extends ServiceEntityRepository
{
    use SqlGeneratorTrait;

    //TODO: replace all "mode" code to super class
    const DEFAULT_MODE = "default";

    const DCR_MODE = "dcr";

    private $mode;

    private $options;

    private $dp;

    private $security;

    public function __construct(
        ManagerRegistry $registry,
        Options $options,
        DatabasePlatform $dp,
        Security $security
    )
    {
        parent::__construct($registry, InfoCompany::class);

        $this->mode = self::DEFAULT_MODE;
        $this->options = $options;
        $this->dp = $dp;
        $this->security = $security;
    }

    /**
     * @param $mode
     * @return self
     */
    public function setMode($mode): self
    {
        $this->mode = $mode ?? self::DEFAULT_MODE;

        return $this;
    }

    public function findByFilter(
        array $filterBy,
        array $orderBy = null,
        $limit = null,
        $offset = null,
        InfoUserEntity $user = null
    )
    {
        $tableName = $this->getClassMetadata()->getTableName();

        $whereParts = array();

        if (isset($filterBy['isArchive'])) {
            $isArchiveIsNull = $this->dp->getIsNullFunctionExpression('info_company.isarchive', 0);
            $whereParts[] = "$isArchiveIsNull = " . ($filterBy['isArchive'] ? 1 : 0);

            if ($this->mode == self::DCR_MODE) {
                $whereParts[] = "$tableName.morionid IS NOT NULL";
            }
        }

        if (isset($filterBy['isMain'])) {
            $isMainIsNull = $this->dp->getIsNullFunctionExpression('info_company.ismain', 0);
            $whereParts[] = "$isMainIsNull = " . ($filterBy['isMain'] ? 1 : 0);
        }

        $options = $this->options->get(['show_grid_last_task_date', 'visibleregion', 'searchForCompanyRequestsByModifier_id', 'showDCRRequestsBySubmission']);

        $c = $this->dp->getConcatOperator();
        if (isset($filterBy['contactId']) && is_numeric($filterBy['contactId'])) {
            $whereParts[] = "
                $tableName.id IN (
                    SELECT company_id
                    FROM info_contact
                    WHERE id = " . $filterBy['contactId'] . "
                    UNION
                    SELECT company_id
                    FROM info_contactworkplace
                    WHERE contact_id = " . $filterBy['contactId'] . "
                )
            ";
        }

        $c = $this->dp->getConcatOperator();
        if ($user && isset($filterBy['ownerId']) && is_numeric($filterBy['ownerId'])) {
            $userId = $user->getId();
            $ownerId = $filterBy['ownerId'];
            $whereParts[] = "
                (
                    $tableName.owner_id = $ownerId
                    OR
                    EXISTS(SELECT 1 FROM info_companyowner co WHERE co.company_id = $tableName.id and co.owner_id = $ownerId AND co.isuse = 1)
                )
                AND EXISTS(SELECT 1 FROM info_contactSubmission WHERE child_id = $ownerId AND subj_id = $userId)
            ";
        } else if ($user && !$user->isAdmin()) {
            $userId = $user->getId();

            $wherePart = "(
                $tableName.owner_id = $userId
                --OR
                --EXISTS(SELECT 1 FROM info_companyowner co WHERE co.company_id = $tableName.id and co.owner_id = $userId AND co.isuse = 1)
            ";

            if (isset($options['visibleregion']) && $options['visibleregion'] == 1) {
                $wherePart .= "
                    OR {$tableName}.region_id IN (
                        select region_id
                        from info_regioninuser
                        where user_id = {$userId}
                        UNION
                        select region_id
                        from info_regioninuser
                        where user_id in (select child_id from info_contactsubmission where subj_id = {$userId})
                    )
                    OR {$tableName}.owner_id in (select child_id from info_contactsubmission where subj_id = {$userId})
                    OR {$tableName}.id in (
                        select company_id 
                        from info_companyowner
                        where owner_id = {$userId} and isuse = 1
                        UNION
                        select company_id 
                        from info_companyowner
                        where owner_id in (select child_id from info_contactsubmission where subj_id = {$userId})
                        and isuse = 1
                    )";
            }

            $whereParts[] = $wherePart . ")";
        } else if (isset($filterBy['ownerId']) && is_numeric($filterBy['ownerId'])) {
            $ownerId = $filterBy['ownerId'];
            $whereParts[] = "(
                $tableName.owner_id = $ownerId
                OR
                EXISTS(SELECT 1 FROM info_companyowner co WHERE co.company_id = $tableName.id and co.owner_id = $ownerId AND co.isuse = 1)
            )";
        }

        $toJoinCompanyType = $toJoinCompanyCategory = $toJoinCity = $toJoinStreetType = $toJoinUser = false;
        if (isset($filterBy['search']) && $filterBy['search']) {
            if ($this->dp->isPostgreSQL100()) {
                $searchArray = explode(' ', $filterBy['search']);
                $searchQuery = "info_company.id in (select company_id from fts_company ";
                foreach ($searchArray as $index => $value) {
                    if (!$index) {
                        $searchQuery .= " where search_str @@ to_tsquery('$value:*') ";
                    } else {
                        $searchQuery .= " and search_str @@ to_tsquery('$value:*') ";
                    }
                }
                $searchQuery .= ") ";
                $whereParts[] = $searchQuery;
            } else {
                $searchParts = array();
                $companyNameIsNull = $this->dp->getIsNullFunctionExpression('info_company.name', "''");
                $companyTypeNameIsNull = $this->dp->getIsNullFunctionExpression('info_companytype.name', "''");
                $cityNameIsNull = $this->dp->getIsNullFunctionExpression('info_city.name', "''");
                $streetIsNull = $this->dp->getIsNullFunctionExpression('info_company.street', "''");
                $buildingIsNull = $this->dp->getIsNullFunctionExpression('cast(info_company.building as varchar)', "''");
                $streetTypeIsNull = $this->dp->getIsNullFunctionExpression('lower(streetType.name)', "''");


                $dcrSearchSqlTemplate = "";
                if ($this->mode == self::DCR_MODE) {
                    $morionIdIsNull = $this->dp->getIsNullFunctionExpression('cast(info_company.morionid as varchar)', "''");
                    $taxNumberIsNull = $this->dp->getIsNullFunctionExpression('cast(info_company.tax_number as varchar)', "''");
                    $dcrSearchSqlTemplate = " $c ' ' $c $morionIdIsNull $c ' ' $c $taxNumberIsNull";
                }

                $searchSqlTemplate = "LOWER($companyNameIsNull $c ' ' $c $companyTypeNameIsNull $c ' ' $c $cityNameIsNull" .
                    " $c ' ' $c $streetIsNull $c ' ' $c $buildingIsNull $c ' ' $c $streetTypeIsNull" .
                    $dcrSearchSqlTemplate .
                    ") LIKE LOWER(N'%%%s%%')";
                foreach (mb_split("[\s,]+", $filterBy['search']) as $substring) {
                    if ($substring) {
                        $searchParts[] = sprintf($searchSqlTemplate, $substring);
                    }
                }

                if ($searchParts) {
                    $toJoinCompanyType = $toJoinCompanyCategory = $toJoinCity = $toJoinStreetType = true;
                    $whereParts[] = implode(' AND ', $searchParts);
                }
            }

        }

        if (isset($filterBy['name']) && $filterBy['name']) {
            $whereParts[] = "LOWER(info_company.name) LIKE LOWER(N'%" . str_replace("'", "''", $filterBy['name']) . "%')";
        }

        if (isset($filterBy['category']) && $filterBy['category']) {
            $toJoinCompanyCategory = true;
            $whereParts[] = "LOWER(info_companycategory.name) LIKE LOWER(N'%" . str_replace("'", "''", $filterBy['category']) . "%')";
        }

        if (isset($filterBy['companytype']) && $filterBy['companytype']) {
            $toJoinCompanyType = true;
            $whereParts[] = "LOWER(info_companytype.name) LIKE LOWER(N'%" . str_replace("'", "''", $filterBy['companytype']) . "%')";
        }

        if (isset($filterBy['owner']) && $filterBy['owner']) {
            $toJoinUser = true;
            $whereParts[] = "LOWER(info_user.name) LIKE LOWER(N'%" . str_replace("'", "''", $filterBy['owner']) . "%')";
        }

        if (isset($filterBy['address']) && $filterBy['address']) {
            $toJoinCity = $toJoinStreetType = true;
            $cityNameIsNull = $this->dp->getIsNullFunctionExpression('info_city.name', "''");
            $streetTypeIsNull = $this->dp->getIsNullFunctionExpression('streetType.name', "''");
            $streetIsNull = $this->dp->getIsNullFunctionExpression('info_company.street', "''");
            $buildingIsNull = $this->dp->getIsNullFunctionExpression('info_company.building', "''");
            $whereParts[] = "LOWER($cityNameIsNull $c ' ' $c $streetTypeIsNull $c ' ' $c $streetIsNull $c ' ' $c $buildingIsNull) LIKE LOWER(N'%" . str_replace("'",
                    "''", $filterBy['address']) . "%')";
        }

        if (isset($filterBy['tasktypeId']) && is_numeric($filterBy['tasktypeId'])) {
            $tasktypeId = $filterBy['tasktypeId'];
            $whereParts[] = "info_company.CompanyType_id IN (SELECT companytype_id FROM info_companytypetasktype WHERE tasktype_id = $tasktypeId)";
        }

        if (isset($filterBy['taskTypeIsInvisibly'])) {
            $taskTypeIsInvisibly = $filterBy['taskTypeIsInvisibly'];
            $whereParts[] = "(
                NOT EXISTS (
                    SELECT 1 FROM info_companytypetasktype
                    WHERE info_companytypetasktype.companytype_id = info_company.CompanyType_id
                )
                or
                info_company.CompanyType_id IN (
                    SELECT companytype_id FROM info_companytypetasktype WHERE tasktype_id IN (
                        SELECT id FROM info_tasktype WHERE COALESCE(isinvisibly, 0) = $taskTypeIsInvisibly
                    )
                )
            )";
        }

        if (isset($options['show_grid_last_task_date'])
            && $options['show_grid_last_task_date'] == 1
            && isset($filterBy['last_task_date'])
            && $filterBy['last_task_date']
            && $filterBy['last_task_date'] = json_decode($filterBy['last_task_date'], true)
        ) {
            if ($filterBy['last_task_date']['start'] && $filterBy['last_task_date']['end']) {
                $whereParts[] = "last_task_date.last_datefrom BETWEEN '{$filterBy['last_task_date']['start']}' AND '{$filterBy['last_task_date']['end']} 23:59:59'";
            } else {
                if ($filterBy['last_task_date']['start']) {
                    $whereParts[] = "last_task_date.last_datefrom >= '{$filterBy['last_task_date']['start']}'";
                } else {
                    if ($filterBy['last_task_date']['end']) {
                        $whereParts[] = "last_task_date.last_datefrom <= '{$filterBy['last_task_date']['end']} 23:59:59' AND last_task_date.last_datefrom <> '' ";
                    }
                }
            }
        }

        if (isset($filterBy['filters']) && is_array($filterBy['filters'])) {
            $userId = $user ? $user->getId() : -1;
            $filterWhereParts = [];
            foreach ($filterBy['filters'] as $filter) {
                list($filterEntity, $args) = $filter;
                if ($filterEntity instanceof InfoFiltersetmobileEntity) {
                    if ($filterEntity->getPage() == 101) {
                        $wherePart = str_replace("#USER_ID#", (string)$userId, $filterEntity->getFilter());

                        if ($filterEntity->isFlag()) {
                            $flag = isset($args[0]) && $args[0] == 1 ? 1 : 0;
                            $wherePart = str_replace("#FLAG#", (string)$flag, $wherePart);
                        }
                        if ($filterEntity->isSingle()) {
                            $wherePart = str_replace("#SINGLE#", '', $wherePart);
                            $filterWhereParts = [$wherePart];
                            break;
                        }

                        $filterWhereParts[] = $wherePart;
                    }
                }
            }
            $whereParts = array_merge($whereParts, $filterWhereParts);
        }

        if (empty($filterBy['filterIds']) && $this->mode == self::DCR_MODE) {
            $whereParts[] = "$tableName.morionid IS NOT NULL";
        }

        $orderByParts = array();
        if ($orderBy) {
            foreach ($orderBy as $value) {
                $order = strtoupper($value["order"]);
                if ($order === "ASC" || $order === "DESC") {
                    $column = strtolower($value["column"]);
                    if ($column == "name") {
                        $orderByParts[] = "info_company.name " . $order;
                    } else {
                        if ($column == "companytype") {
                            $toJoinCompanyType = true;
                            $orderByParts[] = "info_companytype.name " . $order;
                        } else {
                            if ($column == "category") {
                                $toJoinCompanyCategory = true;
                                $orderByParts[] = "info_companycategory.name " . $order;
                            } else {
                                if ($column == 'last_task_date') {
                                    $orderByParts[] = "last_task_date " . $order;
                                } else {
                                    if ($column == "owner") {
                                        $toJoinUser = true;
                                        $orderByParts[] = "info_user.name " . $order;
                                    } else {
                                        if ($column == "address") {
                                            $toJoinCity = $toJoinStreetType = true;
                                            $cityNameIsNull = $this->dp->getIsNullFunctionExpression('info_city.name', "''");
                                            $streetTypeIsNull = $this->dp->getIsNullFunctionExpression('streetType.name', "''");
                                            $streetIsNull = $this->dp->getIsNullFunctionExpression('info_company.street', "''");
                                            $buildingIsNull = $this->dp->getIsNullFunctionExpression('info_company.building', "''");
                                            $orderByParts[] = "$cityNameIsNull $c ' ' $c $streetTypeIsNull $c ' ' $c $streetIsNull $c ' ' $c $buildingIsNull " . $order;
                                        } else {
                                            if ($this->mode == self::DCR_MODE && $column == "cr_status_verification") {
                                                $orderByParts[] = "cr_status_verification " . $order;
                                            } else {
                                                if ($this->mode == self::DCR_MODE && $column == "get_verification") {
                                                    $orderByParts[] = "getverification " . $order;
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

        $rsm = new ResultSetMappingBuilder($this->_em);
        $rsm->addRootEntityFromClassMetadata($this->_entityName, $tableName, array(),
            ResultSetMappingBuilder::COLUMN_RENAMING_INCREMENT);
        $rsm->addEntityResult($this->_entityName, $tableName, 'company');
        $rsm->addScalarResult('cr_status_verification', 'cr_status_verification', 'integer');
        if (isset($options['show_grid_last_task_date']) && $options['show_grid_last_task_date'] == 1) {
            $rsm->addScalarResult('last_task_date', 'last_task_date', 'datetime');
        }
        $rsm->addScalarResult('modifier_user_name', 'modifier_user_name', 'string');

        $selectClause = $rsm->generateSelectClause();
        $where = $whereParts ? 'WHERE ' . implode(' AND ', $whereParts) : '';
        $orderBy = $orderByParts ? 'ORDER BY ' . implode(', ', $orderByParts) : '';
        if ($this->mode == self::DCR_MODE && $user) {
            $usersToFilter = [];
            if ($options['showDCRRequestsBySubmission'] ?? 0) {
                $usersToFilter = array_merge([$user], $user->getSubordinates());
            } else if ($options['searchForCompanyRequestsByModifier_id'] ?? 0) {
                $usersToFilter = [$user];
            }
            if (count($usersToFilter)) {
                $userIds = array_map(function (InfoUserEntity $u) {
                    return $u->getId();
                }, $usersToFilter);
                $usersToFilter = 'AND cr.modifier_id IN (' . implode(',', $userIds) . ')';
            } else {
                $usersToFilter = '';
            }

            $selectClause .= ", (" . $this->dp->modifyOnlyLimitQuery("
                SELECT statusverification FROM (
				SELECT DISTINCT COUNT(*) as count, modified, statusverification, sendverification
				FROM info_companyrequest cr
				WHERE cr.company_id = $tableName.id $usersToFilter
				GROUP BY modified, sendverification, statusverification) q
				ORDER BY CASE WHEN sendverification IS NULL THEN 1 ELSE 0 END DESC, sendverification DESC, modified DESC, count DESC, statusverification
				", 1) . ") AS cr_status_verification
            ";

            $selectClause .= ", (" . $this->dp->modifyOnlyLimitQuery("
				SELECT u.name
				FROM info_companyrequest cr
				INNER JOIN info_user u ON u.id = cr.modifier_id
				WHERE cr.company_id = $tableName.id				    			
				ORDER BY cr.modified DESC
				", 1) . ") modifier_user_name
            ";
        }
        $joins = '';
        if (isset($options['show_grid_last_task_date']) && $options['show_grid_last_task_date'] == 1) {
            $selectClause .= ", last_task_date.last_datefrom AS last_task_date";
            $isArchiveIsNull = $this->dp->getIsNullFunctionExpression('c.isarchive', 0);
            $joins .= "
               LEFT OUTER JOIN (SELECT tmp.last_datefrom, c.id 
                   FROM info_company c
                       LEFT JOIN
                         (SELECT t.company_id,
                                 MAX(datefrom) last_datefrom
                          FROM info_task t
                          INNER JOIN info_taskstate tS ON t.taskstate_id=ts.id
                          AND ts.isfinish=1
                          WHERE t.company_id IS NOT NULL
                          GROUP BY t.company_id) AS tmp ON c.id=tmp.company_id
                   WHERE $isArchiveIsNull=0) AS last_task_date 
               ON (last_task_date.id = info_company.id)
				";
        }

        $sql = "SELECT $selectClause \n FROM $tableName ";

        if ($toJoinCompanyType) {
            $sql .= "\nLEFT OUTER JOIN info_companytype ON info_companytype.id = $tableName.companytype_id ";
        }
        if ($toJoinCompanyCategory) {
            $sql .= "\nLEFT OUTER JOIN info_companycategory ON info_companycategory.id = $tableName.category_id ";
        }
        if ($toJoinCity) {
            $sql .= "\nLEFT OUTER JOIN info_city ON info_city.id = $tableName.city_id ";
        }
        if ($toJoinStreetType) {
            $sql .= "\nLEFT OUTER JOIN info_dictionary AS streetType ON streetType.id = $tableName.streettype_id ";
        }
        if ($toJoinUser) {
            $sql .= "\nLEFT OUTER JOIN info_user ON info_user.id = $tableName.owner_id ";
        }

        $sql .= "\n$joins\n$where\n$orderBy\n";

        $sql = $this->_em->getConnection()->getDatabasePlatform()->modifyLimitQuery($sql, $limit, $offset);
        $query = $this->getEntityManager()->createNativeQuery($sql, $rsm);

        $result = $query->getResult();

        $totalCountSql = "SELECT COUNT(1) FROM $tableName ";
        if ($toJoinCompanyType) {
            $totalCountSql .= "\nLEFT OUTER JOIN info_companytype ON info_companytype.id = $tableName.companytype_id ";
        }
        if ($toJoinCompanyCategory) {
            $totalCountSql .= "\nLEFT OUTER JOIN info_companycategory ON info_companycategory.id = $tableName.category_id ";
        }
        if ($toJoinCity) {
            $totalCountSql .= "\nLEFT OUTER JOIN info_city ON info_city.id = $tableName.city_id ";
        }
        if ($toJoinStreetType) {
            $totalCountSql .= "\nLEFT OUTER JOIN info_dictionary AS streetType ON streetType.id = $tableName.streettype_id ";
        }
        if ($toJoinUser) {
            $totalCountSql .= "\nLEFT OUTER JOIN info_user ON info_user.id = $tableName.owner_id ";
        }
        $totalCountSql .= "\n$joins\n$where\n";

        $withTotalCount = false;
        return array(
            "total_count" => $withTotalCount ? (int)$this->getEntityManager()->getConnection()->executeQuery($totalCountSql)->fetchOne() : null,
            "data" => $result,
        );
    }

    public function findAllMainCompanies()
    {
        $rsm = new ResultSetMappingBuilder($this->_em, ResultSetMappingBuilder::COLUMN_RENAMING_NONE);

        $sql = $this->getAllMainCompaniesSql();

        $query = $this->getEntityManager()->createNativeQuery($sql, $rsm);

        return $query->getResult();
    }

    public function fetchAllMainCompanies(array $selectColumns = [], array $params = [])
    {
        $sql = $this->getAllMainCompaniesSql($selectColumns, $params);

        $result = $this->_em->getConnection()->fetchAllAssociative($sql);

        return $result;
    }

    private function getAllMainCompaniesSql(array $selectColumns = null, $params = [])
    {

        $tableAlias = $this->getClassMetadata()->getTableName();

        $rsm = new ResultSetMappingBuilder($this->_em, ResultSetMappingBuilder::COLUMN_RENAMING_NONE);
        $rsm->addRootEntityFromClassMetadata($this->_entityName, $tableAlias);

        if ($selectColumns) {
            $rsm->columnOwnerMap = array_intersect_key(
                $rsm->columnOwnerMap,
                array_flip($this->getClassMetadata()->getColumnNames($selectColumns))
            );
        }
        $where = [];
        if ($params['region']) {
            $where[] = 'r.id = ' . $params['region'];
        }
        if ($params['q']) {
            $where[] = "LOWER($tableAlias.name) LIKE LOWER('%" . iconv('utf-8', 'cp1251', $params['q']) . "%')";
        }

        $where[] = "(EXISTS(SELECT 1 FROM info_company AS c WHERE c.main_id = $tableAlias.id) OR info_company.main_id IS NULL)";
        $whereStr = implode(' AND ', $where);

        $limit = $params['limit'] ? 'TOP ' . $params['limit'] : '';

        $selectClause = $rsm->generateSelectClause();

        $sql = "
            SELECT $limit $selectClause FROM $tableAlias
            JOIN info_region r on $tableAlias.Region_id = r.id
            WHERE $whereStr
            ORDER BY $tableAlias.name
        ";

        return $sql;
    }

    public function findApprovedCompany($user_id, $datesList)
    {
        $firstDate = $datesList[0]['date'];
        $year = (new \DateTime($firstDate))->format('Y');
        $monthCount = sizeof($datesList);

        $month = array_map(function ($item) {
            return $item['number'];
        }, $datesList);
        $monthStr = implode(',', $month);

        $existsSql = "SELECT count(*) cnt FROM info_plancompanytask WHERE user_id = $user_id AND " . $this->dp->getMonthExpression('dt') . "  in ($monthStr) AND " . $this->dp->getYearExpression('dt') . "  = $year";
        $exists = $this->_em->getConnection()->fetchAllAssociative($existsSql);
        if (!$exists[0]['cnt']) {

            $insertSQL = "
                INSERT INTO info_plancompanytask (user_id, company_id, dt, target_id, tasks, " . $this->dp->escapeColumn('status') . ")
                    SELECT $user_id, company_id, dt, target_id, visit_cnt, " . $this->dp->escapeColumn('status') . "
                        FROM targeting_company_initial($user_id, '$firstDate', $monthCount) ";

            $joinParts = [];
            for ($i = 0; $i < sizeof($datesList); $i++) {
                $joinParts[] = "SELECT " . $this->dp->dateAddExpression('m', $i, "'" . $firstDate . "'") . " dt";
            }
            $join = implode(" UNION ", $joinParts);

            $insertSQL .= " INNER JOIN ($join) quarterMonth ON 1=1";

            $this->_em->getConnection()->executeQuery($insertSQL);
        }

        $sql = 'SELECT * FROM targeting_company(?, ?, ?)';

        $result = [];

        $result['approved'] = $this->_em->getConnection()->fetchAllAssociative($sql, [$user_id, $firstDate, $monthCount]);
        foreach ($result['approved'] as $k => &$r) {
            $r['tovarooborot'] = intval($r['tovarooborot']);
            $r['salePerTovarooborot'] = floatval($r['salePerTovarooborot']);
            $r['plancnt'] = intval($r['plancnt']);
        }

        $groupsSql = 'SELECT * FROM targeting_company_summary(?, ?, ?)';

        $result['monthCount'] = $monthCount;
        $result['groups'] = $this->_em->getConnection()->fetchAllAssociative($groupsSql, [$user_id, $firstDate, $monthCount]);

        return $result;
    }

    public function findThatNotInPlanCompanyTasks($userId, \DateTime $dateTime)
    {

        $rsm = new ResultSetMappingBuilder($this->_em);
        $rsm->addRootEntityFromClassMetadata($this->_entityName, 'c', [],
            ResultSetMappingBuilder::COLUMN_RENAMING_INCREMENT);

        $selectClause = $rsm->generateSelectClause();

        $sql = "
                DECLARE @userId int, @currentMonthDate DATETIME;
                SET @userId = :userId;
                SET @currentMonthDate = DATEADD(m, DATEDIFF(m, 0, GETDATE()), 0);
                
                SELECT $selectClause
                FROM info_company c
                WHERE c.id IN (
                SELECT t.company_id FROM info_task t
                LEFT JOIN info_tasktype tt ON t.tasktype_id = tt.id
                WHERE t.responsible_id = @userId AND tt.Name = 'Аптека'
                    AND t.datefrom BETWEEN ";
        $sql .= $this->options->get('MasterListQuarterPeriod') !== null ? "DATEADD(month, -3, GETDATE()) " : "DATEADD(m, DATEDIFF(m, 0, GETDATE()), 0) ";
        $sql .= "AND GETDATE() 
                AND t.company_id IS NOT NULL
                UNION
                    SELECT company_id FROM info_plancompanytask ";
        $sql .= $this->options->get('MasterListQuarterPeriod') ? "WHERE dt > DATEADD(month, -3, GETDATE()) " : "WHERE YEAR(dt) = YEAR(@currentMonthDate) AND MONTH(dt) = MONTH(@currentMonthDate) ";
        $sql .= "AND user_id = @userId
                ) AND c.id NOT IN (SELECT company_id FROM info_plancompanytask
                WHERE YEAR(dt) = ':yearString' AND MONTH(dt) = ':monthString' AND  user_id = @userId)";

        $sql = str_replace(':userId', $userId, $sql);
        $sql = str_replace(':yearString', $dateTime->format("Y"), $sql);
        $sql = str_replace(':monthString', $dateTime->format("m"), $sql);

        $query = $this->getEntityManager()->createNativeQuery($sql, $rsm);

        return $query->getResult();
    }

    public function findTargetPracticesByFilter(
        array $filterBy,
        $limit = null,
        $offset = null,
        InfoUserEntity $user = null
    )
    {
        $tableName = $this->getClassMetadata()->getTableName();

        $whereParts = ['COALESCE(info_company.IsArchive, 0) = 0'];

        if (!empty($filterBy['practiceno'])) {
            $whereParts[] = 'info_company.practiceno IN (' . implode(',', $filterBy['practiceno']) . ')';
        }

        $c = $this->dp->getConcatOperator();
        if (isset($filterBy['search']) && $filterBy['search']) {
            $searchParts = [];
            foreach (mb_split("[\s,]+", $filterBy['search']) as $substring) {
                if ($substring) {
                    $searchParts[] = sprintf(
                        "LOWER(info_company.name $c ' ' $c info_companytype.name $c ' ' $c COALESCE(info_city.name,'') $c ' ' $c COALESCE(info_company.street,'') $c ' ' $c COALESCE(CAST(info_company.building AS varchar),'') $c ' ' $c COALESCE(streetType.name,'') $c ' ' $c COALESCE(info_contact.firstname, '') $c ' ' $c COALESCE(info_contact.middlename, '') $c ' ' $c COALESCE(info_contact.lastname, '')) LIKE LOWER(N'%%%s%%')",
                        $substring
                    );
                }
            }
            if ($searchParts) {
                $whereParts[] = implode(' AND ', $searchParts);
            }
        }


        if (!empty($filterBy['specializations'])) {
            $joinContactSpecializationCondition = 'AND info_contact.specialization_id IN (' . implode(',',
                    $filterBy['specializations']) . ')';
        } else {
            $joinContactSpecializationCondition = '';
        }


        $orderByParts = [];

        $rsm = $this->createResultSetMappingBuilder($tableName);
        $rsm->addRootEntityFromClassMetadata(InfoContact::class, 'info_contact', [],
            ResultSetMappingBuilder::COLUMN_RENAMING_INCREMENT);

        $selectClause = $rsm->generateSelectClause();
        $where = $whereParts ? 'WHERE ' . implode(' AND ', $whereParts) : '';
        $orderBy = $orderByParts ? 'ORDER BY ' . implode(', ', $orderByParts) : '';

        $sql = "
            SELECT $selectClause FROM $tableName
            LEFT OUTER JOIN info_companytype ON info_companytype.id = $tableName.companytype_id
            LEFT OUTER JOIN info_contact ON info_contact.company_id = $tableName.id AND COALESCE(info_companytype.IsShop, 0) = 0 $joinContactSpecializationCondition
            LEFT OUTER HASH JOIN info_city ON info_city.id = $tableName.city_id
            LEFT OUTER JOIN info_dictionary AS streetType ON streettype.id = $tableName.streettype_id
            $where
            $orderBy
        ";

        $unionSql = "SELECT $selectClause FROM info_organisation_active_inactive_practice_numbers t
        inner join info_person_organisation_link po on po.orgcode = t.orgcode
        inner join info_contact  on info_contact.morionid = po.personcode
        left outer join info_company on info_company.id = info_contact.company_id
        LEFT OUTER HASH JOIN info_city ON info_city.id = info_company.city_id
        LEFT OUTER JOIN info_companytype ON info_companytype.id = info_company.companytype_id
        LEFT OUTER JOIN info_dictionary AS streetType ON streettype.id = info_company.streettype_id
        $where
        and cast(practicenumber as int) not in (select COALESCE(practiceno,0) from info_Contact)";

        if (!empty($filterBy['practiceno'])) {
            $unionSql .= "and  info_company.practiceno IN (" . implode(',', $filterBy['practiceno']) . ")";
        }

        $sql = $sql . ' union all ' . $unionSql;

        if (isset($limit) && $limit > 0) {
            $sql = $this->_em->getConnection()->getDatabasePlatform()->modifyLimitQuery($sql, $limit, $offset);
        }

        $query = $this->getEntityManager()->createNativeQuery($sql, $rsm);

        $rawResult = $query->getResult();

        $result = [];

        foreach ($rawResult as $item) {
            static $company;
            if ($item instanceof \TeamSoft\CrmRepositoryBundle\Entity\InfoCompany) {
                $company = $item;
            } else {
                $result[] = ['company' => $company, 'contact' => $item];
            }
        }

        $totalCountSql = "
            SELECT COUNT(1) FROM $tableName
            LEFT OUTER JOIN info_companytype ON info_companytype.id = $tableName.companytype_id
            LEFT OUTER JOIN info_contact ON info_contact.company_id = $tableName.id AND COALESCE(info_companytype.IsShop, 0) = 0 $joinContactSpecializationCondition
            LEFT OUTER HASH JOIN info_city ON info_city.id = $tableName.city_id
            LEFT OUTER JOIN info_dictionary AS streetType ON streettype.id = $tableName.streettype_id
            $where
        ";

        return [
            "total_count" => (int)$this->getEntityManager()->getConnection()->executeQuery($totalCountSql)->fetchOne(),
            "data" => $result,
        ];
    }

    public function findPharmacyNetworksByName($searchText = '', $limit = 20, $partial = [], $asArray = false, $select = '')
    {
        $qb = $this->createQueryBuilder('c')
            ->join('c.companytype', 'ct')
            ->where("ct.code='pharmacy_network_ofice'")
            ->andWhere('c.centerId IS NOT NULL')
            ->andWhere('ISNULL(c.isarchive, 0)=0')
            ->orderBy('c.name');
        // SELECT c.id, c.name FROM info_company c, info_companytype ct WHERE c.CompanyType_id=ct.id AND ct.code='pharmacy_network_ofice' AND c.CENTER_ID IS NOT NULL AND isnull(c.IsArchive,0)=0

        if (count($partial)) {
            $qb->select('partial c.{' . implode(',', $partial) . '}');
        } elseif ($select) {
            $qb->select($select);
        }

        if ($searchText) {
            $qb->andWhere($qb->expr()->like('c.name', '?1'))
                ->setParameter(1, '%' . $searchText . '%');
        }

        if ($limit) {
            $qb->setMaxResults($limit);
        }

        $query = $qb->getQuery();
        if ($partial) {
            $query->setHint($query::HINT_FORCE_PARTIAL_LOAD, true);
        }
        return $asArray ? $query->getArrayResult() : $query->getResult();
    }

    /**
     * @param array $filter
     * @param array $orderBy
     * @param int $offset
     * @param int $limit
     * @param bool $filter_existing (default = false)
     * @return mixed
     */
    public function findWithReportsByFilter($filter = [], $orderBy = [], $offset = null, $limit = null, $filter_existing = false)
    {
        $queryBuilder = $this->createQueryBuilder('company');

        $queryBuilder
            ->distinct(true)
            ->select('company');

        if ($offset) {
            $queryBuilder->setFirstResult($offset);
        }

        if ($limit) {
            $queryBuilder->setMaxResults($limit);
        }

        $queryBuilder = $this->withReportQueryBuilder($queryBuilder, $filter, $filter_existing);

        foreach ($orderBy as $attribute => $direction) {
            $queryBuilder
                ->addOrderBy("company.{$attribute}", $direction);
        }

        return $queryBuilder->getQuery()->getResult();
    }

    /**
     * @param array $filter
     * @param array $orderBy
     * @param int $page (default = 1)
     * @param int $companiesPerPage (default = 30)
     * @param bool $filter_existing (default = false)
     * @return mixed
     */
    public function findPharmacyNetworksWithReportsByFilter($filter = [], $orderBy = [], $offset = null, $limit = null, $filter_existing = false)
    {
        $queryBuilder = $this->createQueryBuilder('net');

        $queryBuilder
            ->distinct(true)
            ->innerJoin(\TeamSoft\CrmRepositoryBundle\Entity\InfoCompany::class, 'company', 'WITH', 'net = company.centerId')
            ->select('net');

        if ($offset) {
            $queryBuilder->setFirstResult($offset);
        }

        if ($limit) {
            $queryBuilder->setMaxResults($limit);
        }

        $queryBuilder = $this->withReportQueryBuilder($queryBuilder, $filter, $filter_existing);

        foreach ($orderBy as $attribute => $direction) {
            $queryBuilder
                ->addOrderBy("net.{$attribute}", $direction);
        }

        return $queryBuilder->getQuery()->getResult();
    }

    /**
     * @param array $filter
     * @param array $orderBy
     * @param int $offset
     * @param int $limit
     * @param bool $filter_existing (default = false)
     * @return mixed
     */
    public function findCorpsWithReportsByFilter($filter = [], $orderBy = [], $offset = null, $limit = null, $filter_existing = false)
    {
        $queryBuilder = $this->createQueryBuilder('main');

        $queryBuilder = $this->withReportQueryBuilder($queryBuilder, $filter, $filter_existing);
        $queryBuilder
            ->distinct(true)
            ->innerJoin(\TeamSoft\CrmRepositoryBundle\Entity\InfoCompany::class, 'company', 'WITH', 'main = company.main')
            ->select('main');

        if ($offset) {
            $queryBuilder->setFirstResult($offset);
        }

        if ($limit) {
            $queryBuilder->setMaxResults($limit);
        }

        foreach ($orderBy as $attribute => $direction) {
            $queryBuilder
                ->addOrderBy("main.{$attribute}", $direction);
        }

        return $queryBuilder->getQuery()->getResult();
    }

    /**
     * @param InfoCompanyrequest $companyRequest
     * @param string $prevStatus
     * @param int $version
     * @throws \Doctrine\DBAL\DBALException
     * @throws \Doctrine\DBAL\Exception\InvalidArgumentException
     * @throws \Doctrine\ORM\NonUniqueResultException
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function updateCompanyByRequest(InfoCompanyrequest $companyRequest, string $prevStatus = null, int $version = InfoCompanyrequest::COMPANY_VERIFICATION_WITHOUT_DCR)
    {
        $company = $companyRequest->getCompany();

        /**
         * @var \Doctrine\ORM\Mapping\ClassMetadataInfo $companyClassMetadata
         */
        $companyClassMetadata = $this->_em->getClassMetadata(\TeamSoft\CrmRepositoryBundle\Entity\InfoCompany::class);

        if ($companyRequest->getDcrType() && $companyRequest->getDcrType()->getCode() === InfoDcrtype::NEW_SHOP) {
            $userDirections = $companyRequest->getModifier()->getDirectionCollection();

            if ($userDirections && count($userDirections->getValues())) {
                $directionIds = [];

                foreach ($userDirections->getValues() as $direction) {
                    $directionIds[] = $direction->getId();
                }

                $categoryB = $this->_em
                    ->getRepository(InfoCompanycategory::class)
                    ->createQueryBuilder('c')
                    ->where('c.direction IN (:directionIds)')
                    ->andWhere('c.name = :categoryB')
                    ->setParameter('categoryB', 'B')
                    ->setParameter('directionIds', implode(',', $directionIds))
                    ->setMaxResults(1)
                    ->getQuery()
                    ->getOneOrNullResult();

                if ($categoryB) {
                    $categoryExists = $this->_em
                        ->getRepository(InfoCompanycateg::class)
                        ->findOneBy(['company' => $company, 'companycategory' => $categoryB]);

                    if ($categoryB && !$categoryExists) {
                        $companyCategory = new InfoCompanycateg();
                        $companyCategory->setDirection($categoryB->getDirection());
                        $companyCategory->setCompany($company);
                        $companyCategory->setCompanycategory($categoryB);
                        $this->_em->persist($companyCategory);

                        $company->addCompanyCategoryCollection($companyCategory);
                    }
                }
            }

            $companyOwnerExists = $this->_em
                ->getRepository(InfoCompanyowner::class)
                ->findOneBy(['company' => $company, 'owner' => $companyRequest->getModifier()]);

            if (!$companyOwnerExists) {
                $companyOwner = new InfoCompanyowner();
                $companyOwner->setCompany($company);
                $companyOwner->setOwner($companyRequest->getModifier());
                $this->_em->persist($companyOwner);
            }
        }

        if ($companyRequest->getDcrType() && in_array($companyRequest->getDcrType()->getCode(), [InfoDcrtype::NEW_SHOP, InfoDcrtype::NEW_AGENCY])) {
            $companyOwnerExists = $this->_em
                ->getRepository(InfoCompanyowner::class)
                ->findOneBy(['company' => $company, 'owner' => $companyRequest->getModifier()]);

            if (!$companyOwnerExists) {
                $companyOwner = new InfoCompanyowner();
                $companyOwner->setCompany($company);
                $companyOwner->setOwner($companyRequest->getModifier());
                $companyOwner->setIsuse(1);
                $this->_em->persist($companyOwner);
            }
        }

        $company->setStatusverification(3);

        $this->_em->flush();

        if ($companyRequest->getStatusverification2() === 'approved') {
            if ($company) {
                $property = $companyRequest->getField() . 'New';
                /**
                 * @var \Doctrine\ORM\Mapping\ClassMetadataInfo $classMetadata
                 */
                $classMetadata = $this->_em->getClassMetadata(InfoCompanyrequest::class);

                try {
                    $associationClassName = $classMetadata->getAssociationTargetClass($property);
                } catch (\Exception $e) {
                }

                $newValue = (isset($associationClassName) && !$companyRequest->getNewvalue())
                    ? $classMetadata->getFieldValue($companyRequest, $property)
                    : $companyRequest->getNewvalue();

                $field = $companyRequest->getField();

                if (!empty($newValue)) {
                    if ($companyRequest->getField() === 'additionalcompany' && $newValue instanceof \TeamSoft\CrmRepositoryBundle\Entity\InfoCompany) {
                        $conn = $this->_em->getConnection();
                        $conn->insert('info_companyshop', ['company_id' => $company->getId(), 'shop_id' => $newValue->getId()]);
                    } else if ($companyRequest->getField() === 'additionalowner' && $newValue instanceof \TeamSoft\CrmRepositoryBundle\Repository\Contact\Entity\InfoUser) {
                        $companyOwner = new InfoCompanyowner();
                        $companyOwner->setCompany($company);
                        $companyOwner->setIsuse(1);
                        $companyOwner->setOwner($newValue);
                        $this->_em->persist($companyOwner);

                        $company->addCompanyOwnerCollection($companyOwner);
                        $this->_em->flush();
                    } elseif ($companyRequest->getField() === 'companycategory' && $newValue instanceof \TeamSoft\CrmRepositoryBundle\Entity\InfoCompanycategory && $prevStatus != 'create') {
                        $categoryExists = $this->_em
                            ->getRepository(InfoCompanycateg::class)
                            ->findOneBy(['company' => $company]);

                        if (!$categoryExists && ($version === InfoCompanyrequest::COMPANY_VERIFICATION_WITHOUT_DCR ||
                                ($version === InfoCompanyrequest::COMPANY_VERIFICATION_WITH_DCR &&
                                    $companyRequest->getDcrType() && ($companyRequest->getDcrType()->getCode() === InfoDcrtype::NEW_SHOP ||
                                        $companyRequest->getDcrType()->getCode() !== InfoDcrtype::NEW_SHOP)))) {
                            $companyCategory = new InfoCompanycateg();
                            $companyCategory->setDirection($newValue->getDirection());
                            $companyCategory->setCompany($company);
                            $companyCategory->setCompanycategory($newValue);
                            $this->_em->persist($companyCategory);

                            $company->addCompanyCategoryCollection($companyCategory);
                            $this->_em->flush();
                        }
                    } else if ($companyClassMetadata->getFieldName($field)) {
                        $companyClassMetadata->setFieldValue(
                            $company,
                            $companyClassMetadata->getFieldName($field),
                            $newValue
                        );

                        $this->_em->flush();
                    } else {
                        throw new \InvalidArgumentException();
                    }
                } else {
                    $oldValue = (isset($associationClassName) &&
                        !$companyRequest->getOldvalue() &&
                        array_key_exists($companyRequest->getField() . 'Old', $classMetadata->reflFields))
                        ? $classMetadata->getFieldValue($companyRequest, $companyRequest->getField() . 'Old')
                        : $companyRequest->getOldvalue();

                    if (!empty($oldValue)) {
                        if ($companyRequest->getField() === 'additionalcompany' && $oldValue instanceof \TeamSoft\CrmRepositoryBundle\Entity\InfoCompany) {
                            $conn = $this->_em->getConnection();
                            $conn->delete('info_companyshop', ['company_id' => $company->getId(), 'shop_id' => $oldValue->getId()]);
                        } else if ($companyRequest->getField() === 'additionalowner' && $oldValue instanceof \TeamSoft\CrmRepositoryBundle\Repository\Contact\Entity\InfoUser) {
                            /**
                             * @var InfoCompanyowner $companyOwner
                             */
                            $companyOwner = $this
                                ->_em
                                ->getRepository(InfoCompanyowner::class)
                                ->findOneBy([
                                    'company' => $company,
                                    'owner' => $oldValue,
                                ]);

                            if ($companyOwner) {
                                $company->removeCompanyOwnerCollection($companyOwner);
                                $this->_em->remove($companyOwner);
                                $this->_em->flush();
                            }
                        } else if ($companyRequest->getField() === 'companycategory' && $oldValue instanceof \TeamSoft\CrmRepositoryBundle\Entity\InfoCompanycategory) {
                            /**
                             * @var InfoCompanycateg $companyCategory
                             */
                            $companyCategory = $this
                                ->_em
                                ->getRepository(InfoCompanycateg::class)
                                ->findOneBy([
                                    'company' => $company,
                                    'companycategory' => $oldValue,
                                ]);

                            if ($companyCategory) {
                                $company->removeCompanyCategoryCollection($companyCategory);
                                $this->_em->remove($companyCategory);
                                $this->_em->flush();
                            }
                        } else if ($companyClassMetadata->getFieldName($field)) {
                            $companyClassMetadata->setFieldValue(
                                $company,
                                $companyClassMetadata->getFieldName($field),
                                null
                            );
                            $this->_em->flush();
                        }
                    }
                }
            }
        }
    }

    /**
     * @param QueryBuilder $queryBuilder
     * @param array $filter
     * @param bool $filter_existing (default = false)
     * @return QueryBuilder
     */
    protected function withReportQueryBuilder(QueryBuilder $queryBuilder, $filter = [], $filter_existing = false)
    {
        if ($regionpart = $filter['regionpart'] ?? null) {
            $queryBuilder
                ->innerJoin('company.region', 'region')
                ->innerJoin('region.regionparts', 'regionpart')
                ->andWhere($queryBuilder->expr()->in('regionpart', ':regionpart'))
                ->setParameter(':regionpart', $regionpart);
        }

        if ($region = $filter['region'] ?? null) {
            $queryBuilder
                ->andWhere($queryBuilder->expr()->in('company.region', ':region'))
                ->setParameter(':region', $region);
        }

        if ($city = $filter['city'] ?? null) {
            $queryBuilder
                ->andWhere($queryBuilder->expr()->in('company.city', ':city'))
                ->setParameter(':city', $city);
        }

        if ($contact = $filter['contact'] ?? null) {
            $queryBuilder
                ->innerJoin('company.contactCollection', 'contact')
                ->andWhere($queryBuilder->expr()->in('contact', ':contact'))
                ->setParameter(':contact', $contact);
        }

        if ($specialization = $filter['specialization'] ?? null) {
            $queryBuilder
                ->innerJoin('company.contactCollection', 'contact')
                ->andWhere($queryBuilder->expr()->in('contact.specialization', ':specialization'))
                ->setParameter(':specialization', $specialization);
        }

        if ($company = $filter['company'] ?? null) {
            $queryBuilder
                ->andWhere($queryBuilder->expr()->in('company', ':company'))
                ->setParameter(':company', $company);
        }

        if ($net = $filter['net'] ?? null) {
            $queryBuilder
                ->andWhere($queryBuilder->expr()->in('company.centerId', ':net'))
                ->setParameter(':net', $net);
        }

        if ($main = $filter['main'] ?? null) {
            $queryBuilder
                ->andWhere($queryBuilder->expr()->in('company.main', ':main'))
                ->setParameter(':main', $main);
        }

        if ($filter_existing) {
            $queryBuilder
                ->innerJoin(\TeamSoft\CrmRepositoryBundle\Repository\Contact\Entity\InfoTask::class, $reportAlias = 'report', 'WITH', "{$reportAlias}.company = company")
                ->innerJoin("{$reportAlias}.taskFileGroupCollection", $tfgAlias = 'tfg')
                ->innerJoin("{$tfgAlias}.taskImageCollection", $tfiAlias = 'tfi');

            if ($tasktype = $filter['tasktype'] ?? null) {
                $queryBuilder
                    ->andWhere($queryBuilder->expr()->in("{$reportAlias}.tasktype", ':tasktype'))
                    ->setParameter(':tasktype', $tasktype);
            }

            if ($taskstate = $filter['taskstate'] ?? null) {
                $queryBuilder
                    ->andWhere($queryBuilder->expr()->in("{$reportAlias}.taskstate", ':taskstate'))
                    ->setParameter(':taskstate', $taskstate);
            }

            if ($phototype = $filter['phototype'] ?? null) {
                $phototype = array_map(function (\TeamSoft\CrmRepositoryBundle\Repository\Contact\Entity\InfoCustomdictionaryvalue $customdictionaryvalue) {
                    return $customdictionaryvalue->getName();
                }, $phototype);

                $queryBuilder
                    ->andWhere($queryBuilder->expr()->in("{$tfgAlias}.name", ':phototype'))
                    ->setParameter(':phototype', $phototype);
            }

            if ($phototostate = $filter['photostate'] ?? null) {
                $queryBuilder
                    ->andWhere($queryBuilder->expr()->in("{$tfiAlias}.photostate", ':photostate'))
                    ->setParameter(':phototostate', $phototostate);
            }

            if ($photoservicetype = $filter['photoservicetype'] ?? null) {
                $queryBuilder
                    ->andWhere($queryBuilder->expr()->in("{$tfiAlias}.photoservicetype", ':photoservicetype'))
                    ->setParameter(':photoservicetype', $photoservicetype);
            }

            if ($whatsinside = $filter['whatsinside'] ?? null) {
                $queryBuilder
                    ->andWhere($queryBuilder->expr()->in("{$tfiAlias}.whatsinside", ':whatsinside'))
                    ->setParameter(':whatsinside', $whatsinside);
            }

            if ($brand = $filter['brand'] ?? null) {
                $queryBuilder
                    ->andWhere($queryBuilder->expr()->in("{$tfiAlias}.brand", ':brand'))
                    ->setParameter(':brand', $brand);
            }
        }

        return $queryBuilder;
    }

    public function getMode(): string
    {
        return $this->mode;
    }

    public function findWithTaskSaleProject(array $findBy = [])
    {
        $query =  $this->createQueryBuilder('c')
            ->distinct()
            ->select(['c.id', 'c.name as value'])
            ->innerJoin(InfoTasksaleproject::class, 'tsl', 'WITH', 'c.id = tsl.company');

        if($findBy['approvementStatus']) {
            $query = $query->where('tsl.approvementStatus = :status')
                ->setParameter('status', $findBy['approvementStatus']);
        }

        return $query->getQuery()
            ->getArrayResult();
    }

    private function getSqlGeneratorData(string $alias): array
    {
        $c = $this->dp->getConcatOperator();

        $crStatusVerificationJoin = function () use ($alias) {
//  commented according to https://teamsoft.atlassian.net/browse/ER-10
//            $whereUsersToFilter = '';
//            $usersToFilter = [];
//            if ($this->options->get(Options::OPTION_SHOW_DCR_REQUESTS_BY_SUBMISSION)) {
//                /** @var InfoUser $user */
//                $user = $this->security->getUser();
//                $usersToFilter = array_merge([$user], $user->getSubordinates()->getValues());
//            } else if ($this->options->get(Options::OPTION_SEARCH_FOR_COMPANY_REQUESTS_BY_MODIFIER_ID)) {
//                $usersToFilter = [$this->security->getUser()];
//            }
//
//            if ($usersToFilter) {
//                $userIds = array_map(function (InfoUser $u) {
//                    return $u->getId();
//                }, $usersToFilter);
//
//                $whereUsersToFilter = 'AND modifier_id IN (' . implode(',', $userIds) . ')';
//            }

            $whereUsersToFilter = '';
            return "
                LEFT JOIN (
                    SELECT company_id, modifier_id, modified, statusverification
                    FROM
                    (
                        SELECT statusverification, company_id, modified, rank() OVER (PARTITION BY company_id ORDER BY modified DESC, count(*) DESC) AS rnk, modifier_id
                        FROM info_companyrequest
                        WHERE 1=1 {$whereUsersToFilter}
                        GROUP BY statusverification, company_id, modified, modifier_id
                    ) t
                    WHERE rnk = 1
                ) info_companyrequest ON info_companyrequest.company_id = info_company.id";
        };

        return [
            "is_archive" => [
                'select' => ['isarchive'],
                "order_by" => "$alias.isarchive",
                "where" => function () use ($alias) {
                    $where = "COALESCE($alias.isarchive, 0) = {0}";
                    if ($this->mode === self::DCR_MODE) {
                        $where .= " AND $alias.morionid IS NOT NULL";
                    }
                    return $where;
                },
                'map_types' => 'boolean',
            ],
            "ismain" => [
                "order_by" => "$alias.ismain",
                "where" => "COALESCE($alias.ismain, 0) = {0}",
                'map_types' => 'boolean',
            ],
            "contact_id" => [
                "where" => "$alias.id IN (SELECT company_id FROM info_contact WHERE id = {0} UNION SELECT company_id FROM info_contactworkplace WHERE contact_id = {0})",
                'map_types' => 'integer',
            ],
            "name" => [
                "order_by" => "$alias.name",
                "where" => "LOWER($alias.name) LIKE LOWER(N'%{0}%')",
                'map_types' => 'string',
            ],
            "postcode" => [
                "order_by" => "$alias.postcode",
                "where" => "LOWER($alias.postcode) LIKE LOWER(N'%{0}%')",
                'map_types' => 'string',
            ],
            "waddr" => [
                "order_by" => "$alias.waddr",
                "where" => "LOWER($alias.waddr) LIKE LOWER(N'%{0}%')",
                'map_types' => 'string',
            ],
            "eaddr" => [
                "order_by" => "$alias.eaddr",
                "where" => "LOWER($alias.eaddr) LIKE LOWER(N'%{0}%')",
                'map_types' => 'string',
            ],
            "phone1" => [
                "order_by" => "$alias.phone1",
                "where" => "LOWER($alias.phone1) LIKE LOWER(N'%{0}%')",
                'map_types' => 'string',
            ],
            "phone2" => [
                "order_by" => "$alias.phone2",
                "where" => "LOWER($alias.phone2) LIKE LOWER(N'%{0}%')",
                'map_types' => 'string',
            ],
            "code" => [
                "order_by" => "$alias.code",
                "where" => "LOWER($alias.code) LIKE LOWER(N'%{0}%')",
                'map_types' => 'string',
            ],
            "type" => [
                'select' => ['companytype_id'],
                "order_by" => "info_companytype.name",
                "joins" => [
                    "LEFT OUTER JOIN info_companytype ON info_companytype.id = $alias.companytype_id",
                ],
                "where" => "info_companytype.id IN ({0})",
                'map_types' => 'array',
            ],
            "category" => [
                "order_by" => "info_companycategory.name",
                "joins" => [
                    function () use ($alias) {
                        if ($this->options->get(Options::OPTION_USE_CATEGORY_WITH_DIRECTION)) {
                            return [
                                "LEFT OUTER JOIN info_companycateg ON info_companycateg.company_id = $alias.id",
                                "LEFT OUTER JOIN info_companycategory ON info_companycategory.id = info_companycateg.category_id",
                            ];
                        }

                        return ["LEFT OUTER JOIN info_companycategory ON info_companycategory.id = $alias.category_id"];
                    },
                ],
                "where" => "info_companycategory.id IN ({0})",
                'map_types' => 'array',
            ],
            "owner" => [
                "order_by" => "info_user.name",
                "joins" => [
                    "LEFT OUTER JOIN info_user ON info_user.id = $alias.owner_id",
                ],
                "where" => "LOWER(info_user.name) LIKE LOWER(N'%{0}%')",
                'map_types' => 'string',
            ],
            "address" => [
                'select' => ['streettype_id', 'street', 'building', 'city_id', 'region_id', 'country_id'],
                "order_by" => "LOWER(COALESCE(info_city.name, '') $c ' ' $c COALESCE(streetType.name, '') $c ' ' $c COALESCE($alias.street, '') $c ' ' $c COALESCE($alias.building, ''))",
                "joins" => [
                    "LEFT OUTER JOIN info_city ON info_city.id = $alias.city_id",
                    "LEFT OUTER JOIN info_dictionary AS streetType ON streetType.id = $alias.streettype_id",
                ],
                "where" => "LOWER(COALESCE(info_city.name, '') $c ' ' $c COALESCE(streetType.name, '') $c ' ' $c COALESCE($alias.street, '') $c ' ' $c COALESCE($alias.building, '')) LIKE LOWER(N'%{0}%')",
                'map_types' => 'string',
            ],
            "task_type_id" => [
                "where" => "$alias.CompanyType_id IN (SELECT companytype_id FROM info_companytypetasktype WHERE tasktype_id = {0})",
                'map_types' => 'integer',
            ],
            "task_type_is_invisibly" => [
                "where" => "(
                        NOT EXISTS (
                            SELECT 1 FROM info_companytypetasktype
                            WHERE info_companytypetasktype.companytype_id = $alias.CompanyType_id
                        )
                        OR
                        $alias.CompanyType_id IN (
                            SELECT companytype_id FROM info_companytypetasktype WHERE tasktype_id IN (
                                SELECT id FROM info_tasktype WHERE COALESCE(isinvisibly, 0) = {0}
                            )
                        )
                    )",
                'map_types' => 'boolean',
            ],
            "search" => [
                "joins" => [
                    "LEFT OUTER JOIN info_companytype ON info_companytype.id = $alias.companytype_id",
                    "LEFT OUTER JOIN info_city ON info_city.id = $alias.city_id",
                    "LEFT OUTER JOIN info_dictionary AS streetType ON streetType.id = $alias.streettype_id",
                ],
                "where" => function (array $value) use ($alias) {
                    $c = $this->dp->getConcatOperator();
                    $names = [
                        $alias . '.name',
                        'info_companytype.name',
                        'info_city.name',
                        $alias . '.street',
                        $alias . '.building',
                        'streetType.name',
                    ];


                    if ($this->mode === self::DCR_MODE) {
                        $names[] = "CAST({$alias}.morionid as varchar)";
                        $names[] = "CAST({$alias}.tax_number as varchar)";

                    }

                    $where = 'LOWER(';

                    foreach ($names as $index => $name) {
                        if ($index) {
                            $where .= " {$c} ' ' {$c} ";
                        }

                        $where .= "COALESCE({$name}, '')";
                    }

                    $where .= ") LIKE LOWER(N'%%{0}%%')";

                    $sql = [];
                    foreach ($value as $item) {
                        $sql[] = str_replace('{0}', (string)$item, $where);
                    }

                    return implode(' AND ', $sql);
                },
//                'map_types' => 'string',
            ],
            "filters" => [
                "where" => function (?array $values) use ($alias) {
                    $where = '';
                    if (empty($values) && $this->mode === self::DCR_MODE) {
                        $where .= "$alias.morionid IS NOT NULL";
                    }
                    return $where;
                },
            ],
            "owner_id" => [
                "where" => "(
                        $alias.owner_id = {0}
                        OR
                        EXISTS(SELECT 1 FROM info_companyowner co WHERE co.company_id = $alias.id and co.owner_id = {0} AND co.isuse = 1)
                    )",
                'map_types' => 'integer',
            ],
            "owner_id_with_submission" => [
                "where" => "(
                $alias.owner_id = {0} OR EXISTS(SELECT 1 FROM info_companyowner co WHERE co.company_id = $alias.id and co.owner_id = {0} AND co.isuse = 1)
                ) 
                    AND EXISTS(SELECT 1 FROM info_contactSubmission WHERE child_id = {0} AND subj_id = {1})",
                'map_types' => [
                    'integer',
                    'integer',
                ],
            ],
            "user_id" => [
                "where" => "($alias.owner_id = {0})",
                'map_types' => 'integer',
            ],
            "user_id_visible_region" => [
                "where" => "(
                    $alias.owner_id = {0}
                    OR $alias.region_id IN (
                        select region_id
                        from info_regioninuser
                        where user_id = {0}
                        UNION
                        select region_id
                        from info_regioninuser
                        where user_id in (select child_id from info_contactsubmission where subj_id = {0})
                    )
                    OR $alias.owner_id in (select child_id from info_contactsubmission where subj_id = {0})
                    OR $alias.id in (
                        select company_id 
                        from info_companyowner
                        where owner_id = {0} and isuse = 1
                        UNION
                        select company_id 
                        from info_companyowner
                        where owner_id in (select child_id from info_contactsubmission where subj_id = {0})
                        and isuse = 1
                    )
                )",
                'map_types' => 'integer',
            ],
            "last_task_date" => [
                "extra_select" => [
                    [
                        'field' => "last_task_date.last_datefrom",
                        'alias' => "last_task_date",
                        'map_types' => "datetime",
                    ],
                ],
                "order_by" => "last_task_date",
                "joins" => ["
                   LEFT OUTER JOIN (
                       SELECT tmp.last_datefrom, c.id 
                       FROM info_company c
                       LEFT JOIN
                         (SELECT t.company_id, MAX(datefrom) last_datefrom
                          FROM info_task t
                                INNER JOIN info_taskstate tS ON t.taskstate_id=ts.id AND ts.isfinish=1
                          WHERE t.company_id IS NOT NULL
                          GROUP BY t.company_id) AS tmp ON c.id=tmp.company_id
                        WHERE COALESCE(c.isarchive, 0)=0) AS last_task_date 
                   ON (last_task_date.id = $alias.id)",
                ],
                "where" => function (?array $values) {
                    if (!isset($values['dateFrom'], $values['dateTo'])) {
                        return '';
                    }

                    $dateFrom = DateHelper::toDate($values['dateFrom'])->format('Y-m-d');
                    $dateTo = DateHelper::toDate($values['dateTo'])->format('Y-m-d');

                    return "last_task_date.last_datefrom BETWEEN '{$dateFrom}' AND '{$dateTo} 23:59:59'";
                },
            ],

            'cr_status_verification' => [
                'order_by' => 'cr_status_verification',
                "extra_select" => [
                    [
                        'field' => "info_companyrequest.statusverification",
                        'alias' => "cr_status_verification",
                        'map_types' => "integer",
                    ],
                ],
                'joins' => [
                    $crStatusVerificationJoin,
                ],
                "where" => "info_companyrequest.statusverification IN ({0})",
                'map_types' => 'array',
            ],
            'get_verification' => [
                'select' => ['getverification'],
                'order_by' => "{$alias}.getverification",
                "where" => function (?array $values) use ($alias) {
                    if (!isset($values['dateFrom'], $values['dateTo'])) {
                        return '';
                    }

                    $dateFrom = DateHelper::toDate($values['dateFrom'])->format('Y-m-d');
                    $dateTo = DateHelper::toDate($values['dateTo'])->format('Y-m-d');

                    return "{$alias}.getverification BETWEEN '{$dateFrom}' AND '{$dateTo} 23:59:59'";
                },
            ],
            'modifier_user_name' => [
                "extra_select" => [
                    [
                        'field' => "modifier_user.name",
                        'alias' => "modifier_user_name",
                        'map_types' => "string",
                    ],
                ],
                'order_by' => 'modifier_user_name',
                'joins' => [
                    $crStatusVerificationJoin,
                    'LEFT OUTER JOIN info_user AS modifier_user ON (modifier_user.id = info_companyrequest.modifier_id)',
                ],
                "where" => "LOWER(modifier_user.name) LIKE LOWER(N'%{0}%')",
                'map_types' => 'string',
            ],
        ];
    }
}
