<?php
declare(strict_types=1);

namespace TeamSoft\CrmRepositoryBundle\Repository;


use Doctrine\ORM\Query\ResultSetMappingBuilder;
use TeamSoft\CrmRepositoryBundle\Entity\GridConfig\PoGridConfig;
use TeamSoft\CrmRepositoryBundle\Entity\InfoFiltersetmobile as InfoFiltersetmobileEntity;
use TeamSoft\CrmRepositoryBundle\Entity\InfoUser;

trait SqlGeneratorTrait
{

    public function findByFilterAdvanced(array $columns, array $filterBy, array $orderBy, InfoUser $user, bool $withTotalCount = false, int $limit = 100, int $offset = 0)
    {
        $tableAlias = $this->_class->getTableName();
        $rsm = $this->createResultSetMappingBuilderWithEntityResult($tableAlias);
        $sqlGeneratorData = $this->getSqlGeneratorData($tableAlias);

        $filterWhereParts = (isset($filterBy['filters']) && is_array($filterBy['filters'])) ? $this->getFilterWhereParts($filterBy['filters'], $user) : [];

        [
            'select' => $selectParts,
            'join' => $joinParts,
            'where' => $whereParts,
            'order' => $orderByParts,
        ] = $this->getSqlParts($rsm, $sqlGeneratorData, $tableAlias, $columns, $filterBy, $orderBy, true);

        $whereParts = array_merge($whereParts, $filterWhereParts);
        if (!$selectParts) {
            $selectParts = ['*'];
        }

        $select = ' SELECT ' . implode(', ', $selectParts);
        $joins = implode("\n", $joinParts);
        $where = $whereParts ? 'WHERE ' . implode("\nAND ", $whereParts) : '';
        $orderBy = $orderByParts ? 'ORDER BY ' . implode(', ', $orderByParts) : '';
        $sql = "$select\nFROM {$tableAlias} AS $tableAlias\n$joins\n$where\n$orderBy\n";
        $sqlWithLimit = $this->_em->getConnection()->getDatabasePlatform()->modifyLimitQuery($sql, $limit, $offset);
        $query = $this->getEntityManager()->createNativeQuery($sqlWithLimit, $rsm);
        $models = $query->getResult();

        $result = [
            'total_count' => null,
            'data' => $models
        ];

        if (!$withTotalCount) {
            return $result;
        }

        ['join' => $joinParts, 'where' => $whereParts] = $this->getSqlParts($rsm, $sqlGeneratorData, $tableAlias, [], $filterBy, []);
        $whereParts = array_merge($whereParts, $filterWhereParts);
        $joins = $joinParts ? implode("\n", $joinParts) : '';
        $where = $whereParts ? 'WHERE ' . implode("\nAND ", $whereParts) : '';
        $totalCountSql = "SELECT COUNT(1) FROM {$tableAlias} AS $tableAlias\n$joins\n$where\n";

        $result['total_count'] = (int)$this->getEntityManager()->getConnection()->executeQuery($totalCountSql)->fetchOne();

        return $result;
    }

    private function createResultSetMappingBuilderWithEntityResult($tableAlias)
    {
        $rsm = new ResultSetMappingBuilder($this->_em);
        $rsm->addRootEntityFromClassMetadata($this->_entityName, $tableAlias);
        $resultAlias = substr($tableAlias, 5);
        $rsm->addEntityResult($this->_entityName, $tableAlias, $resultAlias);
        return $rsm;
    }

    private function getWhereValue($value, string $type = 'string')
    {
        switch (true) {
            case $type === 'boolean':
                return (int)(bool)$value;
            case $type === 'integer' && is_null($value):
                return -1;
            case $type === 'integer':
                return intval($value);
            case $type === 'empty':
            case $type === 'array' && is_null($value):
            case $type === 'string' && is_null($value):
                return '';
            case $type === 'string':
                return strval($value);
            case $type === 'array':
                if (!$value) {
                    return -1;
                }

                if (is_array($value)) {
                    return implode(', ', $value);
                }

                // $value = '1,2,3,4,5'
                return $value;
            case is_null($value):
                return 'NULL';
            default:
                return $value;
        }
    }

    private function getSelectPart($array, $needle)
    {
        $result = array_filter($array, function ($item) use ($needle) {
            return strpos(strtolower($item), strtolower($needle)) === 0;
        });
        return array_values($result)[0] ?? null;
    }

    private function getSqlParts(ResultSetMappingBuilder $rsm, array $data, string $tableAlias, $columns, $filterBy, $orderBy, $flag = false)
    {
        $selectParts = $joinParts = $whereParts = $orderByParts = [];
        $joinMap = [];

        $addJoin = function ($name, $joins) use (&$joinParts, &$joinMap) {
            if (isset($joinMap[$name])) {
                return;
            }

            foreach ($joins as $join) {
                if (is_callable($join)) {
                    $join = $join();
                }

                if (is_array($join)) {
                    foreach ($join as $item) {
                        if (!in_array($item, $joinParts)) {
                            $joinParts[] = $item;
                        }
                    }
                } elseif (!in_array($join, $joinParts)) {
                    $joinParts[] = $join;
                }
            }

            $joinMap[$name] = $name;
        };
        // filter
        foreach ($filterBy as $name => $values) {
            if (isset($data[$name]) && ($sqlInfo = $data[$name])) {
                $joins = $sqlInfo['joins'] ?? [];
                $addJoin($name, $joins);

                $where = $sqlInfo['where'] ?? null;
                if (is_callable($where)) {
                    $where = $where($values);
                }

                $mapTypes = $sqlInfo['map_types'] ?? null;

                if ($where) {
                    if ($mapTypes) {
                        if (is_array($mapTypes)) {
                            if (!is_array($values)) {
                                $values = [$values];
                            }

                            foreach ($mapTypes as $index => $mapType) {
                                $whereValue = $this->getWhereValue($values[$index] ?? null, $mapType);
                                $where = str_replace('{' . $index . '}', (string) $whereValue, $where);
                            }
                        } else {
                            if (is_array($values)) {
                                $values = $values[0];
                            }

                            $whereValue = $this->getWhereValue($values, $mapTypes);
                            $where = str_replace('{0}', (string) $whereValue, $where);
                        }
                    }

                    $whereParts[] = $where;
                }
            }
        }

        // order
        foreach ($orderBy as $orderByItem) {
            ['column' => $name, 'order' => $value] = $orderByItem;
            if (isset($data[$name]) && ($sqlInfo = $data[$name])) {
                $joins = $sqlInfo['joins'] ?? [];
                $addJoin($name, $joins);

                $orderBy = $sqlInfo['order_by'] ?? $name;
                $value = strtoupper($value) ?: 'ASC';
                $orderByPart = "$orderBy $value";
                if (!in_array($orderByPart, $orderByParts)) {
                    $orderByParts[] = $orderByPart;
                }
            }
        }
        $selectPartsFromRsm = explode(', ', $rsm->generateSelectClause());

        if ($flag) {
            array_unshift($columns, 'id');
        } else {
            $selectParts = $selectPartsFromRsm;
        }

        foreach ($columns as $name) {
            $sqlInfo = $data[$name] ?? [];

            $joins = $sqlInfo['joins'] ?? [];
            $addJoin($name, $joins);

            $extraSelects = $sqlInfo['extra_select'] ?? [];
            foreach ($extraSelects as $extraSelect) {
                if ($extraSelect) {
                    ['field' => $selectValue, 'alias' => $selectAlias, 'map_types' => $selectType] = $extraSelect;
                    $rsm->addScalarResult($selectAlias, $selectAlias, $selectType);
                    $selectPart = "$selectValue AS $selectAlias";
                    if ($selectPart && !in_array($selectPart, $selectParts)) {
                        $selectParts[] = $selectPart;
                    }
                }
            }

            $fields = $sqlInfo['select'] ?? [$name];
            foreach ($fields as $field) {
                if ($field) {
                    $selectPart = $this->getSelectPart($selectPartsFromRsm, "$tableAlias.$field");
                    if ($selectPart && !in_array($selectPart, $selectParts)) {
                        $selectParts[] = $selectPart;
                    }
                }
            }
        }

        return [
            'select' => $selectParts,
            'join' => $joinParts,
            'where' => $whereParts,
            'order' => $orderByParts,
        ];
    }

    protected function getFilterWhereParts(array $filters, InfoUser $user)
    {
        $userId = $user ? $user->getId() : -1;
        $filterWhereParts = [];
        foreach ($filters as $filter) {
            [$filterEntity, $args] = $filter;
            if ($filterEntity instanceof InfoFiltersetmobileEntity) {
                $wherePart = str_replace("#USER_ID#", (string) $userId, $filterEntity->getFilter());

                if ($filterEntity->isFlag()) {
                    $flag = isset($args[0]) && $args[0] == 1 ? 1 : 0;
                    $wherePart = str_replace("#FLAG#", (string) $flag, $wherePart);
                }
                if ($filterEntity->isSingle()) {
                    $wherePart = str_replace("#SINGLE#", '', $wherePart);
                    $filterWhereParts = [$wherePart];
                    break;
                }

                $filterWhereParts[] = $wherePart;
            }
        }
        return $filterWhereParts;
    }
}
