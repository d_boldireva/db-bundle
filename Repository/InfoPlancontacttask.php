<?php

namespace TeamSoft\CrmRepositoryBundle\Repository;

use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\Query\ResultSetMappingBuilder;
use Doctrine\Persistence\ManagerRegistry;
use TeamSoft\CrmRepositoryBundle\Service\DatabasePlatform;
use TeamSoft\CrmRepositoryBundle\Entity\InfoPlancontacttask as InfoPlancontacttaskEntity;

class InfoPlancontacttask extends ServiceEntityRepository
{
    private $dp;

    public function __construct(ManagerRegistry $registry, DatabasePlatform $dp)
    {
        parent::__construct($registry, InfoPlancontacttaskEntity::class);
        $this->dp = $dp;

    }
    function findMinimumApprovementStatus($userId, \DateTime $dateTime)
    {
        $yearString = $dateTime->format("Y");
        $monthString = $dateTime->format("m");

        $rsm = new ResultSetMappingBuilder($this->_em);
        $rsm->addRootEntityFromClassMetadata($this->_entityName, 'pct', array(), ResultSetMappingBuilder::COLUMN_RENAMING_INCREMENT);
        $selectClause = $rsm->generateSelectClause();

        $sql = "select $selectClause
                FROM info_plancontacttask pct
                WHERE COALESCE(approvementstatus, 0) = (
                    SELECT MIN(COALESCE(approvementstatus, 0))
                    FROM info_plancontacttask
                    WHERE user_id = :userId AND " . $this->dp->getYearExpression('dt') . "  = ':yearString' AND " . $this->dp->getMonthExpression('dt') . "  = ':monthString'
                ) AND user_id = :userId AND " . $this->dp->getYearExpression('dt') . "  = ':yearString' AND " . $this->dp->getMonthExpression('dt') . "  = ':monthString'";

        $sql = $this->dp->modifyOnlyLimitQuery($sql, 1);
        $sql = str_replace(':userId', $userId, $sql);
        $sql = str_replace(':yearString', $yearString, $sql);
        $sql = str_replace(':monthString', $monthString, $sql);

        $query = $this->getEntityManager()->createNativeQuery($sql, $rsm);

        return $query->getResult();
    }

    public function findByUserAndDateGroupByTargetGroup($userId, \DateTime $dateTime)
    {
        $yearString = $dateTime->format("Y");
        $monthString = $dateTime->format("m");

        $sql = "DECLARE @userId int, @year varchar(255), @month varchar(255);
				SET @userId = :userId;
				SET @year = :yearString;
				SET @month = :monthString;
				SELECT t.name target, COUNT(pct.id) contacts, SUM(pct.tasks) tasks, 1 _order FROM info_plancontacttask pct
                LEFT JOIN info_contact c on pct.contact_id = c.id
                LEFT JOIN info_targetspec ts on c.Specialization_id = ts.spec_id
                LEFT JOIN info_target t on ts.target_id = t.id
                WHERE pct.user_id = @userId  AND YEAR(pct.dt) = @year AND MONTH(pct.dt) = @month 
                GROUP BY t.name
                UNION
				SELECT 'Всего', COUNT(pct.id) contacts, COALESCE(SUM(pct.tasks), 0) tasks, 2 _order FROM info_plancontacttask pct
                WHERE pct.user_id = @userId  AND YEAR(pct.dt) = @year AND MONTH(pct.dt) = @month
                ORDER BY _order";

        $connection = $this->getEntityManager()->getConnection();
        $statement = $connection->prepare($sql);
        $statement->bindValue(':userId', $userId, \PDO::PARAM_INT);
        $statement->bindValue(':yearString', $yearString, \PDO::PARAM_STR);
        $statement->bindValue(':monthString', $monthString, \PDO::PARAM_STR);

        return $statement->executeQuery()->fetchAllAssociative();
    }

    public function findByUserAndDateWithAdditionalInformation($userId, \DateTime $dateTime, \DateTime $currentMonthTime)
    {
        $rsm = new ResultSetMappingBuilder($this->_em);
        $rsm->addRootEntityFromClassMetadata($this->_entityName, 'pct', array(), ResultSetMappingBuilder::COLUMN_RENAMING_INCREMENT);
        $rsm->addEntityResult($this->_entityName, 'pct', 'pct');
        $rsm->addScalarResult('cnt', 'cnt', 'integer');
        $rsm->addScalarResult('target', 'target', 'string');
        $rsm->addScalarResult('fromPrevMasterList', 'fromPrevMasterList', 'integer');

        $selectClause = $rsm->generateSelectClause();

        $sql = "SELECT $selectClause, COALESCE(p.price, 0) cnt, t.name target, (SELECT 1 where pct.contact_id in
	            (SELECT contact_id FROM info_plancontacttask
		        WHERE YEAR(dt) = ':yearCurrentString' AND MONTH(dt) = ':monthCurrentString' AND  user_id = :userId )) fromPrevMasterList
                FROM info_plancontacttask pct
                LEFT JOIN info_contact c ON pct.contact_id = c.id
                LEFT JOIN info_targetspec ts ON c.Specialization_id = ts.spec_id
                LEFT JOIN info_target t ON ts.target_id = t.id
                LEFT JOIN
                (
                SELECT cpt.contact_id, SUM(p.price * cpt.FactCount)/COUNT(DISTINCT cpt.task_id) price
                FROM info_contactpotential cpt
                LEFT JOIN info_preparation p ON cpt.prep_id = p.id
                LEFT JOIN info_task t ON cpt.task_id = t.id
                LEFT JOIN info_tasktype tt ON t.tasktype_id = tt.id
                LEFT JOIN info_taskstate tst ON t.taskstate_id = tst.id
                WHERE t.responsible_id = :userId
                    AND t.datefrom BETWEEN DATEADD(m, DATEDIFF(m, 0, GETDATE()), 0) AND GETDATE()
                    AND tt.Name = 'Врач'
                    AND tst.Name = 'Выполнен'
                GROUP BY cpt.Contact_id
                ) p ON pct.contact_id = p.Contact_id
                WHERE YEAR(pct.dt) = ':yearString' AND MONTH(pct.dt) = ':monthString' AND  user_id = :userId";

        $sql = str_replace(':userId', $userId, $sql);
        $sql = str_replace(':yearString', $dateTime->format("Y"), $sql);
        $sql = str_replace(':monthString', $dateTime->format("m"), $sql);
        $sql = str_replace(':yearCurrentString', $currentMonthTime->format("Y"), $sql);
        $sql = str_replace(':monthCurrentString', $currentMonthTime->format("m"), $sql);

        $query = $this->getEntityManager()->createNativeQuery($sql, $rsm);

        return $query->getResult();
    }

    public function findByUserAndDate($userId, \DateTime $dateTime)
    {
        $yearString = $dateTime->format("Y");
        $monthString = $dateTime->format("m");

        $rsm = new ResultSetMappingBuilder($this->_em);
        $rsm->addRootEntityFromClassMetadata($this->_entityName, 'pct', array(), ResultSetMappingBuilder::COLUMN_RENAMING_INCREMENT);

        $selectClause = $rsm->generateSelectClause();

        $sql = "SELECT $selectClause
                FROM info_plancontacttask pct
                WHERE YEAR(dt) = ':yearString' AND MONTH(dt) = ':monthString' AND  user_id = :userId";

        $sql = str_replace(':userId', $userId, $sql);
        $sql = str_replace(':yearString', $yearString, $sql);
        $sql = str_replace(':monthString', $monthString, $sql);

        $query = $this->getEntityManager()->createNativeQuery($sql, $rsm);

        return $query->getResult();
    }

    public function getPercentInfo($firstDate, $userId)
    {
        $sql = "
           SELECT SUM(t.removed) AS " . $this->dp->escapeColumn('removed') . " , SUM(t.new) AS " . $this->dp->escapeColumn('new') . " , SUM(changed) as " . $this->dp->escapeColumn('changed') . " FROM (
          
            --removed
            --Были в прошлом
            --В прошлом tasks > 0 AND approvementStatus = 2
            --В текущем tasks = 0 AND approvementStatus in (1, 2)
            --Деленное на общее кол-во клиентов в предыдущем месяце
            
            SELECT ROUND(COUNT(DISTINCT id) * 100.0 / (
              SELECT NULLIF(COUNT(DISTINCT contact_id), 0) FROM info_plancontacttask 
                WHERE user_id = $userId AND dt BETWEEN " . $this->dp->dateAddExpression('month','-1',"'$firstDate'") . " AND " . $this->dp->dateAddExpression('second','-1',"'$firstDate'") . " 
            ), 2) AS " . $this->dp->escapeColumn('removed') . ", 0 AS " . $this->dp->escapeColumn('new') . ", 0 AS " . $this->dp->escapeColumn('changed') . "
              FROM info_plancontacttask 
                WHERE tasks = 0 AND dt between '$firstDate' AND " . $this->dp->dateAddExpression('second','-1', $this->dp->dateAddExpression('month','1',"'$firstDate'")) . " AND user_id = $userId
                AND approvementStatus in (1, 2) AND contact_id IN (
                  SELECT contact_id FROM info_plancontacttask 
                    WHERE user_id = $userId AND dt BETWEEN " . $this->dp->dateAddExpression('month','-1',"'$firstDate'") . " AND " . $this->dp->dateAddExpression('second','-1',"'$firstDate'") . " 
                      AND tasks > 0 AND approvementStatus = 2
                )
            
            UNION
             
            --new
            --Исключить все кто был в прошлом таргетировании
            --В прошлом tasks = 0 AND approvementStatus = 2 или его вообще не было в листе
            --В текущем кратность tasks > 0 AND approvementStatus in (1, 2)
            --Деленное на общее кол-во клиентов в предыдущем месяце
            
            SELECT 0 AS " . $this->dp->escapeColumn('removed') . ", ROUND(COUNT(DISTINCT id) * 100.0 / (
              SELECT NULLIF(COUNT(DISTINCT contact_id), 0) FROM info_plancontacttask 
                WHERE user_id = $userId AND dt BETWEEN " . $this->dp->dateAddExpression('month','-1',"'$firstDate'") . " AND " . $this->dp->dateAddExpression('second','-1',"'$firstDate'") . "
            ), 2) AS " . $this->dp->escapeColumn('new') . ", 0 as " . $this->dp->escapeColumn('changed') . "
              FROM info_plancontacttask 
                WHERE tasks > 0 AND approvementStatus in (1, 2) AND dt between '$firstDate' AND " . $this->dp->dateAddExpression('second','-1', $this->dp->dateAddExpression('month','1',"'$firstDate'")) . "
                AND user_id = $userId AND contact_id NOT IN (
                  SELECT contact_id FROM info_plancontacttask 
                    WHERE user_id = $userId AND dt BETWEEN " . $this->dp->dateAddExpression('month','-1',"'$firstDate'") . " AND " . $this->dp->dateAddExpression('second','-1',"'$firstDate'") . "
                      AND tasks > 0 AND approvementStatus = 2
                )
                 
            UNION
             
            --changed
            --кол-во врачей(аптек) с признаком «Изм.» относительно всех врачей(аптек) в утверждённом таргет-листе.
            
            SELECT 0 AS " . $this->dp->escapeColumn('removed') . ", 0 AS " . $this->dp->escapeColumn('new') . ", ROUND(COUNT(DISTINCT pct.id) * 100.0 / (
              SELECT NULLIF(COUNT(DISTINCT contact_id), 0) FROM info_plancontacttask
                WHERE dt between '$firstDate' AND " . $this->dp->dateAddExpression('second','-1', $this->dp->dateAddExpression('month','1',"'$firstDate'")) . " AND user_id = $userId 
              ), 2) as " . $this->dp->escapeColumn('changed') . "
            FROM info_plancontacttask pct
              LEFT JOIN info_contact c ON c.id = pct.contact_id
              LEFT JOIN info_targetspec ts on ts.spec_id = c.specialization_id
              LEFT JOIN info_target t on t.id = ts.target_id
              LEFT JOIN info_contactcateg cc ON cc.name = c.categoryPrep and cc.contacttype_Id = 1320
              LEFT JOIN info_plancontactrule pcr ON t.id = pcr.target_id AND pcr.category_id = cc.id AND dt BETWEEN pcr.datefrom AND pcr.datetill
                WHERE pct.dt between '$firstDate' AND " . $this->dp->dateAddExpression('second','-1', $this->dp->dateAddExpression('month','1',"'$firstDate'")) . " AND user_id = $userId 
                  AND ((COALESCE(pcr.visit_cnt, 0) = 0 AND pct.tasks > 0) OR (pcr.visit_cnt > 0 AND COALESCE(pct.tasks, 0) = 0))
          ) AS t
        ";

        $stmt = $this->getEntityManager()->getConnection()->prepare($sql);
        $stmt->execute();

        return $stmt->fetch();
    }
}
