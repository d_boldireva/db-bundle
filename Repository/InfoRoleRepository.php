<?php
declare(strict_types=1);

namespace TeamSoft\CrmRepositoryBundle\Repository;

use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use TeamSoft\CrmRepositoryBundle\Entity\InfoRole;

/**
 * @see \TeamSoft\CrmRepositoryBundle\Entity\InfoRole
 * Class InfoRole
 * @package TeamSoft\CrmRepositoryBundle\Repository
 */
class InfoRoleRepository extends ServiceEntityRepository
{
    function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, InfoRole::class);
    }
}
