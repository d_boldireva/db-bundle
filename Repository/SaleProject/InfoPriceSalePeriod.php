<?php

namespace TeamSoft\CrmRepositoryBundle\Repository\SaleProject;

use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\NonUniqueResultException;
use TeamSoft\CrmRepositoryBundle\Entity\InfoPreparation;

class InfoPriceSalePeriod extends EntityRepository
{
    /**
     * Find last record before datetill param in preps
     *
     * @param InfoPreparation|int|string $prep
     * @param \DateTime $datetill
     * @return mixed
     * @throws NonUniqueResultException
     *
     */
    public function findLastBeforeDate($prep, \DateTime $datetill = null)
    {
        $qb = $this->createQueryBuilder('psp');

        $qb
            ->innerJoin('psp.saleperiod', 'sp')
            ->where('sp.isDiscount = 1')
            ->andWhere($qb->expr()->eq('psp.prep', ':prep'))
            ->setParameter('prep', $prep)
            ->orderBy('sp.datefrom', 'DESC')
            ->setMaxResults(1);

        if ($datetill) {
            $qb
                ->andWhere($qb->expr()->lte('sp.datefrom', ':datetill'))
                ->setParameter('datetill', $datetill);
        }

        return $qb->getQuery()->getOneOrNullResult();
    }
}
