<?php
declare(strict_types=1);


namespace TeamSoft\CrmRepositoryBundle\Repository\SaleProject;


use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\Query\ResultSetMappingBuilder;
use Doctrine\Persistence\ManagerRegistry;
use TeamSoft\CrmRepositoryBundle\Entity\SaleProject\InfoTasksaleproject;
use TeamSoft\CrmRepositoryBundle\Service\DatabasePlatform;

/**
 * Class InfoTasksaleprojectRepository
 * @package TeamSoft\CrmRepositoryBundle\Repository\SaleProject
 */
class InfoTasksaleprojectRepository extends ServiceEntityRepository
{
    private $dp;
    private $tableName;

    public function __construct(ManagerRegistry $registry, DatabasePlatform $dp)
    {
        parent::__construct($registry, InfoTasksaleproject::class);
        $this->dp = $dp;
        $this->tableName = $this->getClassMetadata()->getTableName();
    }

    /**
     * @param array|null $filter
     * @param array|null $order
     * @param int|null $limit
     * @param int|null $offset
     * @return array|int|mixed|object[]|string
     * @throws \Doctrine\DBAL\Exception
     */
    public function findByFilter(array $filter, array $order = null, int $limit = null, int $offset = null)
    {
        $rsm = new ResultSetMappingBuilder($this->_em);
        $rsm->addRootEntityFromClassMetadata($this->_entityName, $this->tableName, array());

        $where = "";
        $whereClause = [];

        if(!empty($filter['approvement_status'])) {
            $whereClause[] = "{$this->tableName}.approvementstatus = {$filter['approvement_status']}";
        }

        if(!empty($filter['user_id'])) {
            $userIds = implode(',', $filter['user_id']);
            $whereClause[] = "{$this->tableName}.user_id IN({$userIds})";
        }

        if(!empty($filter['saleproject_id'])) {
            $userIds = implode(',', $filter['saleproject_id']);
            $whereClause[] = "{$this->tableName}.saleproject_id IN({$userIds})";
        }

        if(!empty($filter['company_id'])) {
            $userIds = implode(',', $filter['company_id']);
            $whereClause[] = "{$this->tableName}.company_id IN({$userIds})";
        }

        if(!empty($filter['distributor_id'])) {
            $userIds = implode(',', $filter['distributor_id']);
            $whereClause[] = "{$this->tableName}.distributor_id IN({$userIds})";
        }

        if(!empty($filter['date_from'])) {
            $whereClause[] = "{$this->tableName}.dt >= '{$filter['date_from']}'";
        }

        if(!empty($filter['date_to'])) {
            $whereClause[] = "{$this->tableName}.dt <= '{$filter['date_to']}'";
        }

        $selectClause = $rsm->generateSelectClause();
        $where = "WHERE ".implode(" AND ", $whereClause);

        $sql = "
            SELECT {$selectClause}
            FROM {$this->tableName}
            {$where}
            ORDER BY {$this->tableName}.id ASC
        ";

        $sql = $this->_em->getConnection()->getDatabasePlatform()->modifyLimitQuery($sql, $limit, $offset);

        return $this->_em->createNativeQuery($sql, $rsm)->getResult();
    }
}