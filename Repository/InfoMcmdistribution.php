<?php

namespace TeamSoft\CrmRepositoryBundle\Repository;

use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Collections\Criteria;
use Doctrine\ORM\Query\Expr\Join;
use Doctrine\Persistence\ManagerRegistry;
use Doctrine\ORM\Query\ResultSetMappingBuilder;
use TeamSoft\CrmRepositoryBundle\Service\DatabasePlatform;
use TeamSoft\CrmRepositoryBundle\Entity\InfoMcmcontent;

class InfoMcmdistribution extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry, DatabasePlatform $dp)
    {
        parent::__construct($registry, \TeamSoft\CrmRepositoryBundle\Entity\InfoMcmdistribution::class);
        $this->dp = $dp;
    }

    /**
     * @param array $filters
     * @param integer $offset
     * @param integer $limit
     * @return array|boolean
     */
    public function findByFilter(array $filters, $offset, $limit, $orderBy = null)
    {
        $query = $this
            ->createQueryBuilder('d')
            ->join('d.mcmcontent', 'c')
            ->where('d.parent IS NULL');

        if (isset($filters['datefrom']) && isset($filters['datetill'])) {

            if (isset($filters['use_correct_date_period']) && $filters['use_correct_date_period']) {
                // For example:
                //     |--------| //period in db
                //  |----|
                //            |-----|
                //       |---|
                $query->where("d.datefrom <= '{$filters['datetill']}' AND d.datetill >= '{$filters['datefrom']}'");
            } else {
                $query->where("d.dt between '{$filters['datefrom']}' and '{$filters['datetill']}'");
            }
        }

        if (isset($filters['date']) && $filters['date']) {
            $query->where("d.datetill >= CURRENT_TIMESTAMP()");
        }

        if (isset($filters['search'])) {
            $query
                ->andWhere('LOWER(d.name) LIKE :search')
                ->setParameter('search', mb_strtolower('%'.$filters['search'].'%'));
        }

        if (isset($filters['webinar']) && $filters['webinar']) {
            $query
                ->andWhere('d.iswebinar = 1');
        } else {
            $query
                ->andWhere($this->dp->getIsNullFunctionExpression('d.iswebinar', 0) . '<> 1');
        }

        if (isset($filters['isformp'])) {
            $query
                ->andWhere('d.isformp = 1');
        }

        if (isset($filters['type'])) {
            $query
                ->andWhere("c.type = '{$filters['type']}'");
        }

       if (isset($filters['state'])) {
            $query
                ->andWhere("d.state in (:state)")
                ->setParameter('state', $filters['state']);
        }

        if (isset($filters['user']) && $filters['user']) {
            $users = $filters['user']->getSubordinates()->toArray();
            array_push($users, $filters['user']);
            $query
                ->andWhere('d.owner in (:users)')
                ->setParameter('users', $users);
        }

        if (isset($filters['archived']) && $filters['archived']) {
            $query
                ->andWhere('COALESCE(d.isArchive, 0) = 1');
        } else {
            $query
                ->andWhere('COALESCE(d.isArchive, 0) = 0');
        }


        $totalCountQueryBuilder = clone $query;
        $totalCountQueryBuilder->select('COUNT(d.id)');

        $column = 'createdate';
        $order = 'DESC';
        if ($orderBy) {
            foreach ($orderBy as $value) {
                $order = strtoupper($value["order"]);
                if ($order === "ASC" || $order === "DESC") {
                    $column = strtolower($value["column"]);
                    if ($column == "createdate") {
                        $query->addOrderBy('d.' . $column, $order);
                    } else {
                        if ($column == "name") {
                            $query->addOrderBy('d.' . $column, $order);
                        }
                    }
                }
            }
        } else {
            $query->addOrderBy('d.' . $column, $order);
        }

        if ($offset) {
            $query->setFirstResult($offset);
        }

        if ($limit) {
            $query->setMaxResults($limit);
        }

        return array(
            'total_count' => $totalCountQueryBuilder->getQuery()->getSingleScalarResult(),
            'data' => $query->addSelect('CASE WHEN d.dt IS NULL THEN 1 ELSE 0 END AS HIDDEN dtNull')->getQuery()
                ->getResult() ?? []
        );
    }

    /**
     * @return array
     */
    public function getForStatisticChart()
    {
        return $this->createQueryBuilder('distribution')
            ->select('content.type', 'COUNT(distribution.id)')
            ->innerJoin('distribution.mcmcontent', 'content')
            ->groupBy('content.type')
            ->getQuery()
            ->getResult();
    }

    /**
     * @param \TeamSoft\CrmRepositoryBundle\Entity\InfoMcmdistribution $distribution
     * @return array|boolean
     */
    public function getPrevOrNextDistributionStatistics ($distribution, $comparison , $orderBy)
    {
        return $this
            ->createQueryBuilder('mcmdistribution')
            ->select('mcmdistribution.id, mcmdistribution.name, mcmdistribution.isformp, mcmdistribution.dt')
            ->where('mcmdistribution.dt ' . $comparison . ' :dt')
            ->setParameter('dt', $distribution->getDt())
            ->andWhere('mcmdistribution.state = :delivered')
            ->setParameter('delivered', 'delivered')
            ->orWhere('mcmdistribution.state = :sending')
            ->setParameter('sending', 'sending')
            ->orderBy('mcmdistribution.dt', $orderBy)
            ->setMaxResults(1)
            ->getQuery()
            ->getResult();
    }

    /**
     * @param string $type
     * @return \TeamSoft\CrmRepositoryBundle\Entity\InfoMcmdistribution[]|[]
     */
    public function findCurrentContentByType(string $type = null): array
    {
        $queryBuilder = $this->createQueryBuilder('d');

        // NOW() and CURRENT_TIMESTAMP() for some reason not work
        $dt = new \DateTime('now', new \DateTimeZone('UTC'));
        $queryBuilder->where(':date BETWEEN d.datefrom AND d.datetill')->setParameter('date', $dt->format('Y-m-d H:i:00'));
        $queryBuilder->andWhere('d.isformp = 1');

        if ($type) {
            $queryBuilder
                ->innerJoin(InfoMcmcontent::class, 'c', Join::WITH, 'c=d.mcmcontent');
            $queryBuilder->andWhere('c.type = :type')->setParameter('type', $type);
        }

        return $queryBuilder->getQuery()->getResult();
    }

    /**
     * @param string $types
     * @return array|boolean
     */
    public function findByContentTypes(string $types)
    {
        $criteria = new Criteria();
        $expr = $criteria->expr();
        $criteria->andWhere($expr->in('c.type', explode(',', $types)));

        return $this->createQueryBuilder('d')
            ->join(InfoMcmcontent::class, 'c', Join::WITH, 'c = d.mcmcontent')
            ->where('(d.isArchive = 0 OR d.isArchive is null)')
            ->addCriteria($criteria)
            ->getQuery()
            ->getResult();
    }
}
