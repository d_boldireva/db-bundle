<?php

namespace TeamSoft\CrmRepositoryBundle\Repository;

use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\Query\ResultSetMappingBuilder;
use Doctrine\Persistence\ManagerRegistry;
use TeamSoft\CrmRepositoryBundle\Entity\Sales\PsUploadedFiles as PoUploadedFilesEntity;

class PsUploadedFiles extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, PoUploadedFilesEntity::class);
    }

    /**
     * @param array $filters
     * @param $offset
     * @param $limit
     * @return array|boolean
     * @throws \Exception
     */
    public function findByFilter(array $filters, $offset, $limit)
    {
        $isimported = intval($filters['isimported'] ?? 0);
        $original = $filters['original'] ?? null;
        $pattern = $filters['pattern'] ?? null;
        $id = $filters['id'] ?? null;
        $reportName = $filters['report_name'] ?? null;
        $saledate = $filters['saledate'] ?? null;
        $type = $filters['type'] ?? null;

        $query = $this->createQueryBuilder('f')
            ->where('1 = 1')
            ->orderBy('f.createdate', 'DESC');

        if (!$isimported) {
            $query->andWhere("COALESCE(f.isimported,0) = 0");
        }

        if ($original) {
            $query->andWhere('f.original LIKE :original');
            $query->setParameter('original', '%' . $original . '%');
        }

        if ($pattern) {
            $query->andWhere('f.pattern = :pattern');
            $query->setParameter('pattern', $pattern);
        }

        if ($id) {
            $query->andWhere('f.id = :id');
            $query->setParameter('id', $id);
        }

        if ($reportName) {
            $query->andWhere('f.reportName LIKE :reportName');
            $query->setParameter('reportName', '%' . $reportName . '%');
        }

        if ($saledate) {
            $saledate = new \DateTime($saledate);
            $query->andWhere('f.saledate = :saledate');
            $query->setParameter('saledate', $saledate);
        }

        if ($type) {
            $query->andWhere('f.type = :type');
            $query->setParameter('type', $type);
        }

        $result = $query->getQuery()->getResult();

        if (count($result) == 0) {
            return false;
        }

        $totalCount = count($result);

        $pages = 1;

        if ($limit && $offset >= 0) {
            $result = array_slice($result, $offset, $limit);
        }

        if ($limit && $totalCount > $limit) {
            $pages = ceil($totalCount / $limit);
        }

        return array(
            'pages' => $pages,
            'data' => $result
        );
    }
}
