<?php

declare(strict_types=1);

namespace TeamSoft\CrmRepositoryBundle\Repository;

use Doctrine\ORM\EntityRepository;

/**
 * @see \TeamSoft\CrmRepositoryBundle\Entity\InfoTabcontrol
 * Class InfoTabcontrol
 */
final class InfoTabcontrolRepository extends EntityRepository
{
    /**
     * @param  int  $type
     * @param  int  $role
     * @param  int|null  $languageId
     *
     * @return \TeamSoft\CrmRepositoryBundle\Entity\InfoTabcontrol[]
     */
    public function findByTypeAndRoleAndLanguage(int $type, int $role, ?int $languageId = null): array
    {
        $qb = $this->createQueryBuilder('t');

        $qb->where('t.parrentType = :type')
            ->andWhere('t.role = :role')
            ->setParameters([
                'type' => $type,
                'role' => $role,
            ]);

        if ($languageId !== null) {
            $qb->andWhere('(t.language IS NULL OR t.language = :language)')
                ->setParameter('language', $languageId);
        }

        return $qb->getQuery()->getResult();
    }

    /**
     * @param  int[]  $types
     * @param  \TeamSoft\CrmRepositoryBundle\Entity\InfoRole[]  $roles
     * @param  int|null  $languageId
     */
    public function deleteByTypesAndRolesAndLanguage(array $types, array $roles, ?int $languageId = null): void
    {
        $qb = $this->createQueryBuilder('t')
            ->delete()
            ->where('t.role IN (:roles)')
            ->andWhere('t.parrentType IN (:types)')
            ->setParameters([
                'roles' => $roles,
                'types' => $types,
            ]);

        if ($languageId !== null) {
            $qb->andWhere('(t.language IS NULL OR t.language = :language)')
                ->setParameter('language', $languageId);
        }

        $qb->getQuery()->execute();
    }
}
