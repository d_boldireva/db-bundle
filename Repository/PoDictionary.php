<?php

namespace TeamSoft\CrmRepositoryBundle\Repository;

use Doctrine\ORM\EntityRepository;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use TeamSoft\CrmRepositoryBundle\Entity\InfoLanguage;
use TeamSoft\CrmRepositoryBundle\Service\DatabasePlatform;
use TeamSoft\CrmRepositoryBundle\Entity\PoDictionary as PoDictionaryEntity;
use Doctrine\ORM\Query\ResultSetMappingBuilder;

class PoDictionary extends ServiceEntityRepository
{
    private $dp;

    public function __construct(ManagerRegistry $registry, DatabasePlatform $dp)
    {
        parent::__construct($registry, PoDictionaryEntity::class);
        $this->dp = $dp;
    }

    public function findByReference($referenceId, $rowId = null, $search = null, $language = 'en')
    {

        $sql = "
          SELECT foreign_table, foreign_field, references_table, references_field, identifier, value
          FROM rep_constraint
          JOIN info_dictionaryidentifier di ON lower(di.tablename) = lower(foreign_table) AND lower(di.columnname) = lower(foreign_field)
          WHERE rep_constraint.id = ?";

        $stmt = $this
            ->_em
            ->getConnection()
            ->prepare($sql);

        $row = $stmt->executeQuery([$referenceId])->fetchAssociative();

        if (empty($row) || empty($row['foreign_table']) || empty($row['references_table'])) {
            return false;
        }

        $countStmt = $this->_em->getConnection()->prepare("select count(*) as count from {$row['references_table']}");
        $count = $countStmt->executeQuery()->fetchOne();

        $concatOperator = $this->dp->getConcatOperator();
        $selectParts = ['id'];
        $whereParts = [];
        $join = '';

        if ($row['references_table'] === 'info_contact') {
            $selectParts[] = "lastname $concatOperator ' ' $concatOperator firstname $concatOperator ' ' $concatOperator middlename as name";
        } elseif($row['references_table'] === 'po_dictionary') {
            $selectParts = ['p.id'];
            $selectParts[] = $this->dp->getIsNullFunctionExpression('l.translation', 'p.name') . ' AS name';
            $languageIdSql = $this->dp->modifyOnlyLimitQuery("SELECT id FROM info_language WHERE slug = '$language'", 1);
            $join = ' AS p LEFT JOIN info_languageinterface AS l ON (l.domain=\'messages\' AND l.' . $this->dp->escapeColumn('key') . '=p.name AND l.language_id = (' . $languageIdSql . '))';
        } else {
            $selectParts[] = 'name';
        }

        if ($count > 10 && $rowId) {
            $whereParts[] = "{$row['references_table']}.id = ${rowId}";
        }

        if ($search) {

            if ($this->dp->isPostgreSQL100()) {
                $search = addcslashes($search, '%_');
                $like = 'ILIKE';
            } else {
                $search = str_replace(['_', '%'], ['[_]', '[%]'], $search);
                $like = 'LIKE';
            }

            if ($row['references_table'] === 'info_contact') {
                $whereParts[] = "{$row['references_table']}.lastname $concatOperator ' ' $concatOperator firstname $concatOperator ' ' $concatOperator middlename {$like} '%" .
                    $search . "%'";
            } elseif ($row['references_table'] === 'po_dictionary') {
                $whereParts[] = $this->dp->getIsNullFunctionExpression('l.translation', 'p.name') . " {$like} '%" . $search . "%'";
            } else {
                $whereParts[] = "{$row['references_table']}.name {$like} '%" . $search . "%'";
            }
        }

        if ($row['references_table'] === 'info_dictionary') {
            $whereParts[] = "
                {$row['references_table']}.identifier IN (
                    SELECT identifier
                    FROM info_dictionaryidentifier
                    WHERE tablename = '{$row['foreign_table']}' AND columnname = '{$row['foreign_field']}'
                )
            ";
        }

        $selectClause = implode(',', $selectParts);

        $sql = "
            SELECT {$selectClause}
            FROM {$row['references_table']}
            {$join}
        ";

        if (!empty($whereParts)) {
            $sql .= 'WHERE ' . implode(' AND ', $whereParts);
        }

        $queryResult = $this->_em->getConnection()->prepare($sql)->executeQuery();

        $result = $rowId && $count > 10 ? $queryResult->fetchAssociative() : $queryResult->fetchAllAssociative();

        if (empty($result)) {
            return false;
        }

        return $result;
    }

    public function findColumnsByTable($tableName, $recursive = false, $locale = 'en', $identifier = null)
    {
        $fallback = '';
        if ($locale !== 'en') {
            $fallback = "
                UNION ALL
                select en.tablename, en.columnname, en.value, en.values_list, en.language_id, en.hidden, en.identifier, en.order_num, en.required
                from info_dictionaryidentifier en
                WHERE en.language_id = (select id from info_language where slug = 'en') and NOT EXISTS (
                    select 1
                    from info_dictionaryidentifier d
                    where 
                    d.tablename = en.tablename and
                    d.columnname = en.columnname and
                    d.language_id = (select id from info_language where slug = '$locale')
                )
            ";
        }

        $andTableSchema = '';

        if ($this->dp->isPostgreSQL100()) {
             $andTableSchema = "AND columns.table_schema = 'public'";
        }

        $sql = "
            SELECT
                lower(columns.COLUMN_NAME) AS name,
                lower(columns.TABLE_NAME) AS " . $this->dp->escapeColumn('table') . ",
                columns.CHARACTER_MAXIMUM_LENGTH as maxlength,
                data_type AS " . $this->dp->escapeColumn('type') . ",
                rc.references_table,
                rc.references_field,
                c.hidden,
                c.required,
                c.identifier,
                " . $this->dp->getIsNullFunctionExpression('c.order_num', '(SELECT COUNT(*) FROM info_dictionaryidentifier)') . " as order_num,
                c.value as " . $this->dp->escapeColumn('translation') . ",
                rc.id as reference_id,
                c.values_list
            from (
                select di.tablename, di.columnname, di.value, di.values_list, di.language_id, di.hidden, di.identifier, di.order_num, di.required
                from info_dictionaryidentifier di
                where di.language_id = (select id from info_language where slug = '$locale') and di.tablename = '$tableName'
                $fallback
            ) as c
            INNER JOIN INFORMATION_SCHEMA.columns columns ON lower(c.tablename) = lower(columns.TABLE_NAME) AND lower(c.columnname) = lower(columns.COLUMN_NAME) $andTableSchema
            LEFT JOIN rep_constraint rc ON rc.foreign_table = columns.TABLE_NAME AND rc.foreign_field = columns.COLUMN_NAME
            WHERE " . $this->dp->escapeColumn('table_name') . " = '$tableName'
            AND " . $this->dp->escapeColumn('data_type') . " NOT IN ('image')
            AND " . $this->dp->getIsNullFunctionExpression('c.hidden', '0') . " < 1
            " . (!empty($identifier) ? ' AND c.identifier = ' . $identifier : '') . "
            ORDER BY order_num
        ";

        $statement = $this
            ->_em
            ->getConnection()
            ->prepare($sql);

        $result = $statement->executeQuery()->fetchAllAssociative();

        if ($recursive) {
            foreach ($result as $column) {
                if (!empty($column['references_table'])) {
                    $result = array_merge($result, $this->findColumnsByTable($column['references_table'], null, $locale));
                }
            }
        }

        if (empty($result)) {
            return [];
        }

        return $result;
    }

    public function findRowsByFilter(
        \TeamSoft\CrmRepositoryBundle\Entity\PoDictionary $dictionary,
        array $filterBy = array(),
        array $orderBy = array(),
        int $limit = null,
        int $offset = null
    )
    {
        $tableName = $dictionary->getTablename();

        $columns = $this->findColumnsByTable($tableName, null, $filterBy['locale'], $dictionary->getIdentifier());

        if (!$columns) {
            return false;
        }

        $join = '';
        $columnsList = array();

        $whereParts = array();

        foreach ($columns as $key => $column) {
            $hasName = $this->getEntityManager()->getConnection()
                ->executeQuery("SELECT COUNT(1) FROM INFORMATION_SCHEMA.COLUMNS c WHERE c.table_name = '{$column['references_table']}' AND c.column_name = 'name'")
                ->fetchOne();
            if (!empty($column['name'])) {
                $columnsList[] = 'tableName.' . $column['name'];

                if (!empty($column['references_table']) && !empty($column['references_field'])) {
                    $columns[$key]['index'] = $key;

                    if ($column['references_table'] === 'po_dictionary' && $column['table'] === 'po_dictionarybyrole' && $column['name'] === 'dictionary_id') {
                        $columnsList[] = $this->dp->getIsNullFunctionExpression( "l{$key}.translation", "t{$key}.name") . "  AS reference{$column['name']}";
                        $join .= "\nLEFT JOIN {$column['references_table']} t" . $key .
                            " ON t{$key}.{$column['references_field']} = tableName.{$column['name']}";

                        $join .= "\nLEFT JOIN info_languageinterface AS l{$key} ON (l{$key}.domain='messages' AND l{$key}.{$this->dp->escapeColumn('key')}=t{$key}.name AND l{$key}.language_id = (SELECT id FROM info_language WHERE slug = '{$filterBy['locale']}'))";
                    } else {
                        $columnsList[] = ($hasName ? "t{$key}.name" : "CAST(t{$key}.id AS VARCHAR(11))") . "  AS reference{$column['name']}";
                        $join .= "\nLEFT JOIN {$column['references_table']} t" . $key .
                            " ON t{$key}.{$column['references_field']} = tableName.{$column['name']}";
                    }
                }

                if (array_key_exists(mb_strtolower($column['name']), $filterBy) && mb_strtolower($column['name']) !== 'id' &&
                    !empty($filterBy[mb_strtolower($column['name'])])) {

                    $filter = $filterBy[mb_strtolower($column['name'])];

                    if (json_decode($filter) && (json_last_error() == JSON_ERROR_NONE)) {
                        $filter = json_decode($filter, true);

                        if ($column['references_table'] === 'po_dictionary' && $column['table'] === 'po_dictionarybyrole' && $column['name'] === 'dictionary_id') {
                            $columnName = $this->dp->getIsNullFunctionExpression( "l{$key}.translation", "t{$key}.name");
                        } else {
                            $columnName = ($column['references_table'] && $column['references_field'])
                                ? "t{$key}.name"
                                : 'tableName.' . $column['name'];
                        }


                        if (!empty($filter['condition1']) && !empty($filter['condition2'])) {
                            $condition1 = $this->getOperationByFilter($columnName, $filter['condition1']);
                            $condition2 = $this->getOperationByFilter($columnName, $filter['condition2']);

                            $operator = !empty($filter['operator']) && $filter['operator'] === 'AND' ? 'AND' : 'OR';

                            $whereParts[] = "$condition1 $operator $condition2";
                        } else {
                            $whereParts[] = $this->getOperationByFilter($columnName, $filter);
                        }
                    }
                }
            }
        }

        $columnsList[] = 'tableName.id';

        $selectClause = $columns ? implode(',', $columnsList) : '*';

        if (isset($filterBy['search'])) {
            $whereParts[] = "tableName.name LIKE '" . $filterBy['search'] . "%'";
        }

        if (isset($filterBy['id'])) {
            $whereParts[] = "tableName.id IN (" . $filterBy['id'] . ")";
        }

        if ($dictionary->getIdentifier()) {
            $whereParts[] = "tableName.identifier = " . $dictionary->getIdentifier();
        }

        $orderByParts = array();

        if (!empty($orderBy)) {
            foreach ($columns as $key => $column) {
                foreach ($orderBy as $value) {
                    if (!empty($value['column']) && !empty($value['order']) &&
                        mb_strtolower($value['column']) === mb_strtolower($column['name'])) {
                        $orderByParts[] = (isset($column['index']) ? 't' . $column['index'] . '.name' : 'tableName' . '.' . $value['column']) . ' ' . strtoupper($value['order']);
                    }
                }
            }
        }

        $where = $whereParts ? 'WHERE ' . implode(' AND ', $whereParts) : '';
        $orderBy = $orderByParts ? 'ORDER BY ' . implode(', ', $orderByParts) : '';

        $sql = "SELECT $selectClause\nFROM $tableName tableName\n$join\n$where\n$orderBy";
        $sql = $this->_em->getConnection()->getDatabasePlatform()->modifyLimitQuery($sql, $limit, $offset);
        $result = $this->getEntityManager()->getConnection()->executeQuery($sql)->fetchAllAssociative();

        $totalCountSql = "SELECT COUNT(1) FROM $tableName tableName $join $where";

        return [
            "count" => (int)$this->getEntityManager()->getConnection()->executeQuery($totalCountSql)->fetchOne(),
            "data" => $result,
        ];
    }

    private function getOperationByFilter($column, $filter)
    {
        $filter['type'] = $filter['type'] ?? 'contains';

        switch ($filter['type']) {
            case 'equals':
                $operation = $column . ' = ' . "'" . $filter['filter'] . "'";
                break;
            case 'notEqual':
                $operation = $column . ' <> ' . "'" . $filter['filter'] . "'";
                break;
            case 'startsWith':
                $operation = $column . ' ' . $this->dp->getLikeExpression() . " '" . $filter['filter'] . "%'";
                break;
            case 'endsWith':
                $operation = $column . ' ' . $this->dp->getLikeExpression() . " '%" . $filter['filter'] . "'";
                break;
            case 'contains':
                $operation = $column . ' ' . $this->dp->getLikeExpression() . " '%" . $filter['filter'] . "%'";
                break;
            case 'notContains':
                $operation = $column . ' NOT ' . $this->dp->getLikeExpression() . " '%" . $filter['filter'] . "%'";
                break;
        }

        return $operation;
    }
}
