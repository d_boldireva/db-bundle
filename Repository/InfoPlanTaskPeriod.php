<?php

namespace TeamSoft\CrmRepositoryBundle\Repository;

use Doctrine\ORM\EntityRepository;

class InfoPlanTaskPeriod extends EntityRepository
{

    public function findByDetailAndPeriod($planDetailId, \DateTime $dateFrom, \DateTime $dateTill)
    {
        $dateFrom = \DateTime::createFromFormat('Y-m-d H:i:s', $dateFrom->format('Y-m-d') . ' 00:00:00');
        $dateTill = \DateTime::createFromFormat('Y-m-d H:i:s', $dateTill->format('Y-m-d') . ' 00:00:00');
        $dateTill->add(\DateInterval::createFromDateString('86399 second'));
        $query = $this->createQueryBuilder('p')
            ->where('p.plandetail IN (:planDetailId)')
            ->setParameter(':planDetailId', $planDetailId)
            ->andWhere('((p.dt1 BETWEEN :dateFrom AND :dateTill) OR (p.dt2 BETWEEN :dateFrom1 AND :dateTill1) OR((:dateFrom2 BETWEEN p.dt1 AND p.dt2) AND (:dateTill2 BETWEEN p.dt1 AND p.dt2)))')
            ->setParameter(':dateFrom', $dateFrom)
            ->setParameter(':dateFrom1', $dateFrom)
            ->setParameter(':dateFrom2', $dateFrom)
            ->setParameter(':dateTill', $dateTill)
            ->setParameter(':dateTill1', $dateTill)
            ->setParameter(':dateTill2', $dateTill)
            ->getQuery();
        return $query->getResult();
    }
}
