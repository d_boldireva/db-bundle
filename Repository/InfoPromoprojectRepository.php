<?php

namespace TeamSoft\CrmRepositoryBundle\Repository;

use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\Query\ResultSetMappingBuilder;
use Doctrine\Persistence\ManagerRegistry;
use TeamSoft\CrmRepositoryBundle\Entity\InfoUser as InfoUserEntity;
use TeamSoft\CrmRepositoryBundle\Service\DatabasePlatform;
use TeamSoft\CrmRepositoryBundle\Entity\InfoPromoproject;

class InfoPromoprojectRepository extends ServiceEntityRepository {

    private $dp;

    public function __construct(ManagerRegistry $registry, DatabasePlatform $dp)
    {
        parent::__construct($registry, InfoPromoproject::class);
        $this->dp = $dp;
    }

    public function findForCampaignByFilter(array $filterBy, array $orderBy = null, $limit = null, $offset = null, InfoUserEntity $user = null) {
        $tableName = $this->getClassMetadata()->getTableName();

        $whereParts = array();

        if (isset($filterBy['name']) && $filterBy['name']) {
            $whereParts[] = "info_company.name like N'%" . str_replace("'", "''", $filterBy['name']) . "%'";
        }

        if (isset($filterBy['isarchive']) && $filterBy['isarchive']) {
            $whereParts[] = "$tableName.isarchive = " . $filterBy['isarchive'];
        } else {
            $whereParts[] = "coalesce($tableName.isarchive, 0) = 0";
        }

        $orderByParts = array();
        if ($orderBy) {
            foreach ($orderBy as $value) {
                $order = strtoupper($value["order"]);
                if ($order === "ASC" || $order === "DESC") {
                    $column = strtolower($value["column"]);
                    if ($column == "title") {
                        $orderByParts[] = "$tableName.title " . $order;
                    } else if ($column == "datefrom") {
                        $orderByParts[] = "$tableName.datefrom " . $order;
                    } else if ($column == "participant_cnt") {
                        $orderByParts[] = "participant_cnt " . $order;
                    } else if ($column == "quarter_number") {
                        $orderByParts[] = "quarter_number " . $order;
                    } else if ($column == "brand_name") {
                        $orderByParts[] = "brand_name " . $order;
                    } else if ($column == "brand_cnt") {
                        $orderByParts[] = "brand_cnt " . $order;
                    } else if ($column == "status") {
                        $orderByParts[] = "status " . $order;
                    }
                }
            }
        }

        $rsm = new ResultSetMappingBuilder($this->_em);
        $rsm->addRootEntityFromClassMetadata($this->_entityName, $tableName, array(), ResultSetMappingBuilder::COLUMN_RENAMING_INCREMENT);
        $rsm->addEntityResult($this->_entityName, $tableName, 'campaign');
        $rsm->addScalarResult('participant_cnt', 'participant_cnt', 'integer');
        $rsm->addScalarResult('quarter_number', 'quarter_number', 'integer');
        $rsm->addScalarResult('brand_name', 'brand_name', 'string');
        $rsm->addScalarResult('brand_cnt', 'brand_cnt', 'integer');

        $selectClause = $rsm->generateSelectClause();
        $where = $whereParts ? 'WHERE ' . implode(' AND ', $whereParts) : '';
        $orderBy = $orderByParts ? 'ORDER BY ' . implode(', ', $orderByParts) : '';

        $databasePlatform = $this->_em->getConnection()->getDatabasePlatform();
        $dateExpression = $this->dp->getDateExpression();

        $brandNameSql = $this->dp->modifyOnlyLimitQuery("SELECT info_preparationbrend.name FROM info_promoprojectbrend 
                LEFT JOIN info_preparationbrend ON info_preparationbrend.id =  info_promoprojectbrend.brend_id        
                WHERE info_promoprojectbrend.promoproject_id = $tableName.id                
                ORDER BY info_promoprojectbrend.id ASC", 1);
        $brandCntSql = $this->dp->modifyOnlyLimitQuery("SELECT info_promoprojectbrend.cnt FROM info_promoprojectbrend                     
                WHERE info_promoprojectbrend.promoproject_id = $tableName.id                
                ORDER BY info_promoprojectbrend.id ASC", 1);
        $sql = "
            SELECT $selectClause, (
                SELECT COUNT(1) FROM info_promoprojectcompany
                INNER JOIN info_company ON info_company.id = info_promoprojectcompany.company_id
                LEFT OUTER JOIN info_companytype ON info_companytype.id = info_company.companytype_id
                LEFT OUTER JOIN info_contact ON info_contact.company_id = info_company.id AND COALESCE(info_companytype.IsShop, 0) = 0
                WHERE info_promoprojectcompany.promoproject_id = $tableName.id
            ) AS participant_cnt, (
                SELECT COUNT(1) FROM (SELECT DISTINCT info_promoprojectdetail.nquarter FROM info_promoprojectdetail                
                WHERE info_promoprojectdetail.promoproject_id = $tableName.id) t
            ) AS quarter_number, ($brandNameSql) AS brand_name, ($brandCntSql) AS brand_cnt, 
            CASE WHEN datefrom IS NOT NULL AND datetill IS NOT NULL AND $dateExpression BETWEEN datefrom AND datetill THEN 1
            WHEN datefrom IS NOT NULL AND datefrom > $dateExpression THEN 2 
            WHEN datetill IS NOT NULL AND datetill < $dateExpression THEN 0
            WHEN datefrom IS NOT NULL AND datefrom <= $dateExpression THEN 1             
            WHEN datetill IS NOT NULL AND datetill >= $dateExpression THEN 1
            ELSE -1 END AS status
            FROM $tableName
            $where
            $orderBy
        ";

        $sql = $databasePlatform->modifyLimitQuery($sql, $limit, $offset);
        //echo $sql; exit;
        $query = $this->getEntityManager()->createNativeQuery($sql, $rsm);

        $result = $query->getResult();

        $totalCountSql = "SELECT COUNT(1) FROM $tableName $where
        ";
        return array(
            "total_count" => (int)$this->getEntityManager()->getConnection()->executeQuery($totalCountSql)->fetchOne(),
            "data" => $result,
        );
    }

    /**
     * Find promoprojects by filter
     *
     * @param array $filter
     * @param array $orderBy
     * @return mixed
     */
    public function findByFilter($filter = [], $orderBy = []){
        $qb = $this->createQueryBuilder('pp');

        $qb->select('pp');

        if($filter){
            $qb->where('1=1');

            if(isset($filter['title']) && $filter['title']){
                $qb
                    ->andWhere($qb->expr()->like('pp.title',':title'))
                    ->setParameter('title',"%{$filter['title']}%");
            }

            if(isset($filter['datefrom']) && $filter['datefrom']){
                $qb
                    ->andWhere($qb->expr()->gte('pp.datefrom',':datefrom'))
                    ->setParameter('datefrom', $filter['datefrom']);
            }

            if(isset($filter['datetill']) && $filter['datetill']){
                $qb
                    ->andWhere($qb->expr()->lte('pp.datetill',':datetill'))
                    ->setParameter('datetill', $filter['datetill']);
            }

            if(isset($filter['brandCollection']) && $filter['brandCollection'] && count($filter['brandCollection'])){
                $qb
                    ->innerJoin('pp.brandCollection','pb')
                    ->innerJoin('pb.brend','b')
                    ->andWhere($qb->expr()->in('b',':brandCollection'))
                    ->setParameter('brandCollection',$filter['brandCollection']);
            }
        }

        if($orderBy){
            if(isset($orderBy['title'])){
                $qb->addOrderBy('pp.title',$orderBy['title']);
            }

            if(isset($orderBy['datefrom'])){
                $qb->addOrderBy('pp.datefrom', $orderBy['datefrom']);
            }

            if(isset($orderBy['datetill'])){
                $qb->addOrderBy('pp.datetill', $orderBy['datetill']);
            }
        }

        return $qb->getQuery()->getResult();
    }
}
