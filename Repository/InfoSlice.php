<?php

namespace TeamSoft\CrmRepositoryBundle\Repository;

use Doctrine\DBAL\Exception as DBALException;
use Doctrine\Persistence\ManagerRegistry;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use TeamSoft\CrmRepositoryBundle\Entity\InfoReportTab;
use TeamSoft\CrmRepositoryBundle\Entity\InfoReportTabField;
use TeamSoft\CrmRepositoryBundle\Entity\InfoReportTabFilter;
use TeamSoft\CrmRepositoryBundle\Entity\InfoSliceField;
use TeamSoft\CrmRepositoryBundle\Entity\InfoSliceFilter;
use TeamSoft\CrmRepositoryBundle\Entity\InfoSliceRelation;
use TeamSoft\CrmRepositoryBundle\Service\DatabasePlatform;
use TeamSoft\CrmRepositoryBundle\Entity\InfoSlice as InfoSliceEntity;


class InfoSlice extends ServiceEntityRepository
{
    const PREVIEW_LIMIT = 100000;

    private $dp;

    public function __construct(ManagerRegistry $registry, DatabasePlatform $dp)
    {
        parent::__construct($registry, InfoSliceEntity::class);
        $this->dp = $dp;
    }

    /**
     * Create sql by provided slice and params
     * @param InfoSliceEntity $slice
     * @param $params
     * @param bool $onlyCount
     * @return string
     */
    public function prepareSQL(InfoSliceEntity $slice, $params, $onlyCount = false)
    {
        $leftJoins = [];
        $orderBy = [];
        $groupBy = [];

        if (!empty($params['headers'])) {
            $selectClause = $params['headers'];
        } elseif (!empty($params['fields'])) {
            $selectClause = [];

            /**
             * @var InfoReportTabField $tabField
             */
            foreach ($params['fields'] as $tabField) {
                if (!empty($params['load_hidden']) && !$params['load_hidden'] && !$tabField->getVisible()) {
                    continue;
                }

                if ($tabField->getField()) {
                    $field = $tabField->getField();
                    $selectClause[] = $field->getSlice()->getCode() . '.' . $field->getField() . ' ' . $this->dp->escapeColumn($tabField->getName() ?? $field->getName());

                    if ($tabField->getGroupBy()) {
                        $groupBy[] = $this->dp->escapeColumn($field->getSlice()->getCode() . '.' . $field->getField());
                    }
                } else {
                    if ($tabField->getFormula()) {
                        $selectClause[] = $tabField->getFormula() . ' ' . $this->dp->escapeColumn($tabField->getName());

                        if ($tabField->getGroupBy()) {
                            $groupBy[] = $this->dp->escapeColumn($tabField->getFormula());
                        }
                    }
                }

                if ($tabField->getSorting()) {
                    $orderBy[] = $this->dp->escapeColumn($tabField->getName() ?? $field->getName()) . ' ' . $tabField->getSorting();
                }
            }
        } else {
            $selectClause = $this->getSelectClauseBySlice($slice);
        }


        $whereClause = array();

        $filters = [];

        if (!empty($params['filters'])) {
            /**
             * @var InfoSliceFilter $filter
             */
            $filters = $this
                ->_em
                ->getRepository(InfoSliceFilter::class)
                ->createQueryBuilder('sf')
                ->where('sf.id IN (:ids)')
                ->setParameter('ids', array_keys($params['filters']));

            if (!empty($params['additionalSlices'])) {
                $additionalSlices = !is_array($params['additionalSlices'])
                    ? explode(',', $params['additionalSlices'])
                    : $params['additionalSlices'];
                $filters
                    ->andWhere('sf.slice IN (:slicesIds)')
                    ->setParameter('slicesIds', array_merge([$slice->getId()], $additionalSlices));
            }

            $filters = $filters
                ->getQuery()
                ->getResult();
        }

        if (empty($params['disableFilters'])) {
            $sliceSql = $this->applyFiltersToSlice($slice, $filters, $params);
        } else {
            $sliceSql = $slice->getServerSql();
        }

        if (!empty($params['tabId'])) {
            $this->prepareConditions($slice, $whereClause, $params['tabId']);
        }

        if (!empty($params['additionalSlices'])) {
            /**
             * @var InfoSliceRelation $relations
             */
            $relations = $this->_em->getRepository(InfoSliceRelation::class)
                ->createQueryBuilder('s')
                ->where('s.parent = :parent')
                ->setParameter('parent', $slice)
                ->andWhere('s.child IN (:ids)')
                ->setParameter('ids', $params['additionalSlices'])
                ->getQuery()
                ->getResult();

            if (!empty($relations)) {
                /**
                 * @var InfoSliceRelation $relation
                 */
                foreach ($relations as $relation) {
                    $selectClause = array_merge($selectClause, $this->getSelectClauseBySlice($relation->getChild()));
                    $leftJoins[] = $this->generateJoinFromRelation($relation, $filters, $params);

                    if (!empty($params['tabId'])) {
                        $this->prepareConditions($slice, $whereClause, $params['tabId']);
                    }
                }
            }
        }

        $selectClauseStr = implode(',', $selectClause);

        $leftJoinsStr = implode(' ', $leftJoins);

        $whereClauseStr = count($whereClause) ? 'WHERE ' . implode(' AND ', $whereClause) : '';

        $orderByStr = '';

        if (!empty($orderBy)) {
            $orderByStr = 'ORDER BY ' . implode(',', $orderBy);
        }

        $sliceAlias = $slice->getCode();

        if ($onlyCount) {
            $selectClauseStr = 'count(*) as count';
        }

        return trim("
            SELECT $selectClauseStr
            FROM ($sliceSql) $sliceAlias
            $leftJoinsStr
            $whereClauseStr
            $orderByStr
        ");
    }

    /**
     * @param InfoReportTab $tab
     * @param bool $loadHidden
     * @return array
     */
    public function getFields(InfoReportTab $tab, $loadHidden = true)
    {
        $fields = [];

        $tabFields = $tab->getFields()->toArray();

        usort($tabFields, function (InfoReportTabField $a, InfoReportTabField $b) {
            return $a->getPosition() <=> $b->getPosition();
        });

        /**
         * @var InfoReportTabField $reportTabField
         */
        foreach ($tabFields as $reportTabField) {
            $loadField = $loadHidden ?: $reportTabField->getVisible();
            if ($reportTabField->getField() && $loadField) {
                $fields[$reportTabField->getField()->getSlice()->getCode() . '.' . $reportTabField->getField()->getField()] = $reportTabField->getName();
            } elseif (!empty($reportTabField->getFormula()) && $loadField) {
                $fields[$reportTabField->getFormula()] = $reportTabField->getName();
            }
        }

        return $fields;
    }

    public function getCount(InfoSliceEntity $slice, array $params)
    {
        $connection = $this->getEntityManager()->getConnection();

        $countStatement = $connection->prepare($this->getSQL($slice, $params));
        return $countStatement->executeQuery()->fetchOne() ?? 0;
    }

    private function replaceVariables(string $sql, array $params)
    {
        if (false !== strpos($sql, '#USER_ID#')) {
            $sql = str_replace('#USER_ID#', $params['userId'] ?? 0, $sql);
        }

        if (false !== strpos($sql, '#PLAN_ID#')) {
            $sql = str_replace('#PLAN_ID#', $params['planId'] ?? 0, $sql);
        }

        if (false !== strpos($sql, '#REPLACE#')) {
            $sql = str_replace('#REPLACE#', '1=1', $sql);
        }

        return $sql;
    }

    /**
     * @param InfoSliceEntity $slice
     * @param array $params
     * @return string|string[]
     */
    public function getSQL(InfoSliceEntity $slice, array $params)
    {
        return $this->replaceVariables($this->prepareSQL($slice, $params), $params);
    }

    /**
     * @param InfoSliceEntity $slice
     * @param array $params
     * @return array|false|mixed[]
     * @throws DBALException
     */
    public function getPreview(InfoSliceEntity $slice, array $params)
    {
        $connection = $this->getEntityManager()->getConnection();

        $sql = $this->getSQL($slice, $params);

        if (!empty($params['limit']) && $params['limit'] !== -1) {
            $limit = $params['limit'] ?? self::PREVIEW_LIMIT;
            $offset = $params['offset'] ?? 0;

            $sql = $this->_em->getConnection()->getDatabasePlatform()->modifyLimitQuery($sql, $limit, $offset);
        }

        try {
            $statement = $connection->prepare($sql);
            return $statement->executeQuery()->fetchAllAssociative();
        } catch (DBALException $e) {
            return [];
        }
    }

    /**
     * @param InfoSliceEntity $slice
     * @param array $whereClause
     * @param int $tabId
     * @return string
     */
    private function prepareConditions(InfoSliceEntity $slice, array &$whereClause, int $tabId)
    {
        if ($slice->getConditions()) {
            /**
             * @var InfoReportTabFilter $condition
             */
            foreach ($slice->getConditions()->getValues() as $condition) {
                if (!empty($condition->getServerFilterSql()) && $condition->getReportTab()->getId() === $tabId) {
                    $whereClause[] = '(' . $condition->getServerFilterSql() . ')';
                }
            }
        }
    }

    /**
     * @param InfoSliceRelation $relation
     * @param InfoSliceFilter[] $filters
     * @param array $params
     * @return string
     */
    private function generateJoinFromRelation(InfoSliceRelation $relation, array $filters, array $params)
    {
        /**
         * @var InfoSliceEntity $child
         */
        $child = $relation->getChild();

        /**
         * @var InfoSliceEntity $parent
         */
        $parent = $relation->getParent();

        if (empty($params['disableFilters'])) {
            $childSql = $this->applyFiltersToSlice($child, $filters, $params);
        } else {
            $childSql = $child->getServerSql();
        }

        return 'LEFT JOIN (' . $childSql . ') ' . $child->getCode() . ' ON ' .
            $child->getCode() . '.' . $relation->getChildField() . ' = ' .
            $parent->getCode() . '.' . $relation->getParentField();
    }

    /**
     * @param InfoSliceEntity $slice
     * @param InfoSliceFilter[] $filters
     * @param array $params
     * @return string
     */
    private function applyFiltersToSlice(InfoSliceEntity $slice, array $filters = null, array $params = null)
    {
        $sliceSql = $slice->getServerSql();

        $filtersArr = [];

        if (!empty($filters) && strpos($sliceSql, '#REPLACE#') !== false) {
            foreach ($filters as $filter) {
                if (!empty($params['filters']) && array_key_exists($filter->getId(), $params['filters'])) {
                    $value = $params['filters'][$filter->getId()];
                    $filterStr = $filter->getServerFilter();

                    if (empty($value) && $filter->getServerDefaultFilter()) {
                        $filterStr = $filter->getServerDefaultFilter();
                    }

                    if (!empty($value) || !empty($filter->getServerDefaultFilter())) {
                        $filtersArr[] = '(' . str_replace('#VALUE#', $value, $filterStr) . ')';
                    }
                }
            }
        }

        if (!empty($filtersArr)) {
            $sliceSql = str_replace('#REPLACE#', implode(' AND ', $filtersArr), $sliceSql);
        }

        return $sliceSql;
    }

    /**
     * @param InfoSliceEntity $slice
     * @return array
     */
    private function getSelectClauseBySlice(InfoSliceEntity $slice)
    {
        $selectClause = [];

        if ($slice->getFields()->count()) {
            /**
             * @var InfoSliceField $field
             */
            foreach ($slice->getFields() as $field) {
                $selectClause[] = $slice->getCode() . '.' . $field->getField() . ' ' . $this->dp->escapeColumn($slice->getCode() . '.' . $field->getField());
            }
        } else {
            $selectClause[] = $slice->getCode() . '.*';
        }

        return $selectClause;
    }
}
