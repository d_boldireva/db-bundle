<?php

namespace TeamSoft\CrmRepositoryBundle\Repository;

use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\Query;
use Doctrine\ORM\Query\Expr;
use Doctrine\ORM\Query\ResultSetMappingBuilder;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Bridge\Doctrine\Security\User\UserLoaderInterface;
use TeamSoft\CrmRepositoryBundle\Entity\InfoCallStatistics;
use TeamSoft\CrmRepositoryBundle\Entity\InfoDirection;
use TeamSoft\CrmRepositoryBundle\Entity\RTC\InfoRtcRoomReserve;
use TeamSoft\CrmRepositoryBundle\Entity\SaleProject\InfoTasksaleproject;
use TeamSoft\CrmRepositoryBundle\Entity\User;
use TeamSoft\CrmRepositoryBundle\Entity\InfoUser as InfoUserEntity;
use TeamSoft\CrmRepositoryBundle\Entity\InfoUsersubdirection;
use TeamSoft\CrmRepositoryBundle\Entity\InfoDictionary;
use TeamSoft\CrmRepositoryBundle\Service\DatabasePlatform;
use TeamSoft\CrmRepositoryBundle\Service\Options;

class InfoUser extends ServiceEntityRepository implements UserLoaderInterface
{

    use ResultSetMappingBuilderTrait;

    private $dp;
    private $options;

    public function __construct(ManagerRegistry $registry, DatabasePlatform $dp, Options $options)
    {
        parent::__construct($registry, InfoUserEntity::class);
        $this->dp = $dp;
        $this->options = $options;
    }

    public function createRSM(): ResultSetMappingBuilder
    {
        $rsm = new ResultSetMappingBuilder($this->_em);
        $rsm->addRootEntityFromClassMetadata($this->_entityName, 'u');
        $rsm->addJoinedEntityFromClassMetadata(
            User::class, 'pu', 'u', 'poUser',
            $this->getRenamedColumns($this->_em, User::class, 'pu_')
        );
        return $rsm;
    }

    /**
     * @param $userId
     * @param $onlySubordinates
     * @param bool $showIsNoWork
     * @param int[]|InfoDirection[] $directions
     * @return InfoUserEntity[]
     */
    public function findSubordinateUsers($userId, $onlySubordinates = false, $showIsNoWork = false, $directions = [])
    {
        $rsm = $this->createResultSetMappingBuilder('u');
        $rsm->addJoinedEntityFromClassMetadata(User::class, 'pu', 'u', 'poUser'); // performance improvement
        $rsm->addJoinedEntityFromClassMetadata(InfoUserEntity::class, 'subordinate', 'u', 'subordinates');
        $selectClause = $rsm->generateSelectClause();

        if ($onlySubordinates || $this->dp->isPostgreSQL100()) {
            $allUsersIfAdminSql = '';
        } else {
            $allUsersIfAdminSql = " OR EXISTS(
                SELECT 1 FROM info_user
                WHERE id = :userId
                AND CONVERT(VARCHAR(8000), CAST([profile] AS BINARY)) IN ('lpa0HlFGfiF7L0YU+lE=', 'lpWlGlRcEg==')
            )";
        }

        $isNoWorkExpression = $showIsNoWork ? "" : "AND {$this->dp->getIsNullFunctionExpression('u.isNoWork', 0)} = 0";
        if ($this->options->get('canUsersVisibilityByPositions') == 1) {
            $userPositionVisibilitySql = " OR u.id IN (\n" .
                "SELECT id FROM info_user WHERE position_id IN (\n" .
                "SELECT parentpos_id FROM info_userpositionvisibility " .
                "WHERE info_userpositionvisibility.position_id = (SELECT position_id FROM info_user WHERE id = :userId)\n" .
                ")\n)";
        } else {
            $userPositionVisibilitySql = '';
        }

        $directionJoin = '';
        $directionWhere = '';
        if ($directions && count($directions)) {
            $directionJoin = "INNER JOIN info_userdirection ud ON ud.user_id = u.id";
            $directionWhere = "AND ud.direction_id IN (" . join(',', array_map(function ($direction) {
                    return is_scalar($direction) ? $direction : $direction->getId();
                }, $directions)) . ")";
        }

        $sql = "
            SELECT $selectClause, CASE WHEN u.id = :userId THEN 1 ELSE 0 END AS isAuth
            FROM info_user u
            $directionJoin
            LEFT JOIN po_user pu ON u.id = pu.info_user_id -- performance improvement
            LEFT JOIN info_contactsubmission cs ON cs.subj_id = u.id
            LEFT JOIN info_user subordinate ON cs.child_id = subordinate.id
            WHERE
                (
                    u.id = :userId
                    OR u.id IN (SELECT child_id FROM info_contactSubmission WHERE subj_id = :userId)
                    $allUsersIfAdminSql
                    $userPositionVisibilitySql
                )
                $isNoWorkExpression
                $directionWhere
                ORDER BY isAuth DESC, u.name
		";

        $query = $this->getEntityManager()->createNativeQuery($sql, $rsm);
        $query->setHint(Query::HINT_FORCE_PARTIAL_LOAD, true);
        $query->setParameter('userId', $userId);

        return $query->getResult();
    }

    public function findSupervisorUsers($userId)
    {
        $rsm = $this->createResultSetMappingBuilder('u');
        $selectClause = $rsm->generateSelectClause();

        $sql = "
        SELECT $selectClause
        FROM info_user u
        WHERE u.id IN (SELECT subj_id FROM info_contactsubmission WHERE child_id = $userId)";

        $query = $this->getEntityManager()->createNativeQuery($sql, $rsm);
        return $query->getResult();
    }

    public function findOwnerUsers($userId)
    {
        $rsm = $this->createResultSetMappingBuilder('u');
        $selectClause = $rsm->generateSelectClause();

        $sql = "SELECT $selectClause\nFROM info_user u\nWHERE COALESCE(u.isnowork,0) = 0\n";
        $isOwnerListNotFiltered = $this->options->get('isOwnerListNotFiltered');
        if ($isOwnerListNotFiltered !== 1) {
            $sql .= "and (u.id = $userId or u.id = (select submission_id from info_user where id = $userId)" .
                "or exists (select 1 from info_contactsubmission s where s.child_id = u.id and subj_id = $userId))\n";
        }
        $sql .= "ORDER BY u.name ASC";

        $query = $this->getEntityManager()->createNativeQuery($sql, $rsm);
        return $query->getResult();
    }

    /**
     * @param string $username
     * @return InfoUserEntity|null
     */
    public function loadUserByUsername($username)
    {
        if ($this->dp->isSQLServer2008()) {
            $where = 'u.poLogin = COLLATE(:username, SQL_Latin1_General_CP1_CS_AS)';
        } else {
            $where = 'u.poLogin = :username';
        }
        if (!!json_decode($this->options->get('activeDirectoryConfig'), true)) {
            $where .= ' AND u.adIdentifier IS NULL';
        }
        return $this->createQueryBuilder('u')
            ->leftJoin('u.poUser', 'pu')->addSelect('pu')
            ->where($where)
            ->setParameter('username', $username)
            ->getQuery()
            ->getOneOrNullResult();
    }

    /**
     * @param $imei
     * @return InfoUserEntity|null
     */
    public function findOneByIMEI($imei)
    {
        $query = $this->createQueryBuilder('u')
            ->leftJoin('u.poUser', 'pu')->addSelect('pu')
            ->where('u.primeryimei = :primeryimei')
            ->orWhere('u.alterimei = :alterimei')
            ->setParameter('primeryimei', $imei)
            ->setParameter('alterimei', $imei)
            ->setMaxResults(1)
            ->getQuery();

        return $query->getOneOrNullResult();
    }

    /**
     * @param array $filterBy
     * @return InfoUserEntity[]
     */
    public function findByFilter($filterBy)
    {
        $qb = $this->createQueryBuilder('u');

        if (!isset($filterBy['show_not_working'])) {
            $qb->where($this->dp->getIsNullFunctionExpression('u.isnowork', 0) . '=0');
        }

        if (!empty($filterBy['has_car_report'])) {
            $qb->innerJoin('u.carReportSheets', 'crs');
        }

        if (isset($filterBy['position_id'])) {
            $qb->andWhere("u.position = " . $filterBy['position_id']);
        }

        if (isset($filterBy['submission_id'])) {
            $qb->andWhere("u.submission = " . $filterBy['submission_id']);
        }

        if (isset($filterBy['position_flags'])) {
            $qb->join('u.position', 'p', Expr\Join::WITH, 'p.identifier = ' . InfoDictionary::USER_POSITION)
                ->andWhere("BIT_AND(p.flags, " . $filterBy['position_flags'] . ") != 0");
        }

        if (isset($filterBy['direction_ids'])) {
            $qb->join('u.directionCollection', 'd')
                ->andWhere("d in (" . $filterBy['direction_ids'] .")");
        }

        if (isset($filterBy['region_id'])) {
            $qb->join('u.regionCollection', 'r')
                ->andWhere("r = " . $filterBy['region_id']);
        }

        if (isset($filterBy['subdirection_id'])) {
            $qb->join(InfoUsersubdirection::class, 'usd', 'WITH', 'u = usd.user')
                ->andWhere("usd.subdirection = " . $filterBy['subdirection_id']);
        }

        if (isset($filterBy['has_no_storage'])) {
            $qb->leftJoin('u.storage', 's')
                ->andWhere('s.id IS NULL');
        }

        $query = $qb->orderBy('u.name', 'ASC')->getQuery();

        return $query->getResult();
    }

    /**
     * Find users with photo reports(optional)
     *
     * @param array $filter
     * @param array $orderBy
     * @param int $page (default = 1)
     * @param int $usersPerPage (default = 30)
     * @param bool $withReports (default = false)
     * @return mixed
     */
    public function findWithReportsByFilter($filter = [], $orderBy = [], $offset = null, $limit = null, $withReports = false)
    {
        $this->findAll();

        $partialSelectFields = ['id'];

        foreach ($orderBy as $attr => $direction) {
            $partialSelectFields[] = $attr;
        }
        $partialSelectFields = implode(', ', $partialSelectFields);

        $queryBuilder = $this->createQueryBuilder($userAlias = 'user');
        $queryBuilder
            ->select("partial $userAlias.{{$partialSelectFields}}")
            ->distinct(true);

        if ($offset) {
            $queryBuilder->setFirstResult($offset);
        }

        if ($limit) {
            $queryBuilder->setMaxResults($limit);
        }

        if ($regionpart = $filter['regionpart'] ?? null) {
            $queryBuilder
                ->innerJoin("{$userAlias}.regionCollection", 'region')
                ->innerJoin('region.regionparts', 'regionpart')
                ->andWhere($queryBuilder->expr()->in('regionpart', ':regionpart'))
                ->setParameter(':regionpart', $regionpart);
        }

        if ($region = $filter['region'] ?? null) {
            $queryBuilder
                ->innerJoin("{$userAlias}.regionCollection", 'region')
                ->andWhere($queryBuilder->expr()->in('region', ':region'))
                ->setParameter(':region', $region);
        }

        if ($city = $filter['city'] ?? null) {
            $queryBuilder
                ->innerJoin("{$userAlias}.regionCollection", 'region')
                ->innerJoin('region.citiesCollection', 'city')
                ->andWhere($queryBuilder->expr()->in('city', ':city'))
                ->setParameter(':city', $city);
        }

        if ($direction = $filter['direction'] ?? null) {
            $queryBuilder
                ->innerJoin("{$userAlias}.directionCollection", 'direction')
                ->andWhere($queryBuilder->expr()->in('direction', $direction));
        }

        if ($withReports) {
            $queryBuilder
                ->innerJoin(\TeamSoft\CrmRepositoryBundle\Entity\InfoTask::class, $reportAlias = 'report', 'WITH', "{$reportAlias}.responsible = {$userAlias}")
                ->innerJoin("{$reportAlias}.taskFileGroupCollection", $tfgAlias = 'tfg')
                ->innerJoin("{$tfgAlias}.taskImageCollection", $tfiAlias = 'tfi');

            if ($company = $filter['company'] ?? null) {
                $queryBuilder
                    ->andWhere($queryBuilder->expr()->in("{$reportAlias}.company", ':company'))
                    ->setParameter(':company', $company);
            }

            if ($contact = $filter['contact'] ?? null) {
                $queryBuilder
                    ->andWhere($queryBuilder->expr()->in("{$reportAlias}.contact", ':contact'))
                    ->setParameter(':contact', $contact);
            }

            if ($main = $filter['main'] ?? null) {
                $queryBuilder
                    ->innerJoin("{$reportAlias}.company", 'company')
                    ->andWhere($queryBuilder->expr()->in('company.main', ':main'))
                    ->setParameter(':main', $main);
            }

            if ($net = $filter['net'] ?? null) {
                $queryBuilder
                    ->innerJoin("{$reportAlias}.company", 'company')
                    ->andWhere($queryBuilder->expr()->in('company.centerId', ':net'))
                    ->setParameter(':net', $net);
            }

            if ($specialization = $filter['specialization'] ?? null) {
                $queryBuilder
                    ->innerJoin("{$reportAlias}.contact", 'contact')
                    ->andWhere($queryBuilder->expr()->in('contact.specialization', ':specialization'))
                    ->setParameter(':specialization', $specialization);
            }

            if ($tasktype = $filter['tasktype'] ?? null) {
                $queryBuilder
                    ->andWhere($queryBuilder->expr()->in("{$reportAlias}.tasktype", ':tasktype'))
                    ->setParameter(':tasktype', $tasktype);
            }

            if ($taskstate = $filter['taskstate'] ?? null) {
                $queryBuilder
                    ->andWhere($queryBuilder->expr()->in("{$reportAlias}.taskstate", ':taskstate'))
                    ->setParameter(':taskstate', $taskstate);
            }

            if ($phototype = $filter['phototype'] ?? null) {
                $phototype = array_map(function (\TeamSoft\CrmRepositoryBundle\Entity\InfoCustomdictionaryvalue $customdictionaryvalue) {
                    return $customdictionaryvalue->getName();
                }, $phototype);

                $queryBuilder
                    ->andWhere($queryBuilder->expr()->in("{$tfgAlias}.name", ':phototype'))
                    ->setParameter(':phototype', $phototype);
            }

            if ($phototostate = $filter['photostate'] ?? null) {
                $queryBuilder
                    ->andWhere($queryBuilder->expr()->in("{$tfiAlias}.photostate", ':photostate'))
                    ->setParameter(':phototostate', $phototostate);
            }

            if ($photoservicetype = $filter['photoservicetype'] ?? null) {
                $queryBuilder
                    ->andWhere($queryBuilder->expr()->in("{$tfiAlias}.photoservicetype", ':photoservicetype'))
                    ->setParameter(':photoservicetype', $photoservicetype);
            }

            if ($whatsinside = $filter['whatsinside'] ?? null) {
                $queryBuilder
                    ->andWhere($queryBuilder->expr()->in("{$tfiAlias}.whatsinside", ':whatsinside'))
                    ->setParameter(':whatsinside', $whatsinside);
            }

            if ($brand = $filter['brand'] ?? null) {
                $queryBuilder
                    ->andWhere($queryBuilder->expr()->in("{$tfiAlias}.brand", ':brand'))
                    ->setParameter(':brand', $brand);
            }

            if ($regionpart = $filter['regionpart'] ?? null) {
                $subquery_company = $this->getEntityManager()->getRepository(\TeamSoft\CrmRepositoryBundle\Entity\InfoCompany::class)->createQueryBuilder('company');
                $subquery_company
                    ->innerJoin('company.region', 'company_region')
                    ->innerJoin('company_region.regionparts', 'company_regionpart')
                    ->andWhere($subquery_company->expr()->eq('company', "{$reportAlias}.company"))
                    ->andWhere($subquery_company->expr()->in('company_regionpart', ':regionpart'))
                    ->setMaxResults(1);

                $subquery_contact = $this->getEntityManager()->getRepository(\TeamSoft\CrmRepositoryBundle\Entity\InfoContact::class)->createQueryBuilder('contact');
                $subquery_contact
                    ->innerJoin('contact.region', 'contact_region')
                    ->innerJoin('contact_region.regionparts', 'contact_regionparts')
                    ->andWhere($subquery_company->expr()->eq('contact', "{$reportAlias}.contact"))
                    ->andWhere($subquery_contact->expr()->in('contact_regionparts', ':regionpart'))
                    ->setMaxResults(1);

                $queryBuilder
                    ->andWhere($queryBuilder->expr()->orX()->addMultiple([
                        $queryBuilder->expr()->exists($subquery_company),
                        $queryBuilder->expr()->exists($subquery_contact)
                    ]));
            }

            if ($region = $filter['region'] ?? null) {
                $subquery_company = $this->getEntityManager()->getRepository(\TeamSoft\CrmRepositoryBundle\Entity\InfoCompany::class)->createQueryBuilder('company');
                $subquery_company
                    ->andWhere($subquery_company->expr()->eq('company', "{$reportAlias}.company"))
                    ->andWhere($subquery_company->expr()->in('company.region', ':region'))
                    ->setMaxResults(1);

                $subquery_contact = $this->getEntityManager()->getRepository(\TeamSoft\CrmRepositoryBundle\Entity\InfoContact::class)->createQueryBuilder('contact');
                $subquery_contact
                    ->andWhere($subquery_contact->expr()->eq('contact', "{$reportAlias}.contact"))
                    ->andWhere($subquery_contact->expr()->in('contact.region', ':region'));

                $queryBuilder
                    ->andWhere($queryBuilder->expr()->orX()->addMultiple([
                        $queryBuilder->expr()->exists($subquery_company),
                        $queryBuilder->expr()->exists($subquery_contact)
                    ]));
            }

            if ($city = $filter['city'] ?? null) {
                $subquery_company = $this->getEntityManager()->getRepository(\TeamSoft\CrmRepositoryBundle\Entity\InfoCompany::class)->createQueryBuilder('company');
                $subquery_company
                    ->andWhere($subquery_company->expr()->eq('company', "{$reportAlias}.company"))
                    ->andWhere($subquery_company->expr()->in('company.city', ':city'))
                    ->setMaxResults(1);

                $subquery_contact = $this->getEntityManager()->getRepository(\TeamSoft\CrmRepositoryBundle\Entity\InfoContact::class)->createQueryBuilder('contact');
                $subquery_contact
                    ->andWhere($subquery_company->expr()->eq('contact', "{$reportAlias}.company"))
                    ->andWhere($subquery_company->expr()->in('contact.city', ':city'))
                    ->setMaxResults(1);

                $queryBuilder->andWhere($queryBuilder->expr()->orX()->addMultiple([
                    $queryBuilder->expr()->exists($subquery_company),
                    $queryBuilder->expr()->exists($subquery_contact)
                ]));
            }
        }

        foreach ($orderBy as $attribute => $direction) {
            $queryBuilder->addOrderBy("{$userAlias}.{$attribute}", $direction);
        }

        return $queryBuilder->getQuery()->getResult();
    }

    public function findByCompanyOwner($companyId)
    {
        $rsm = $this->createResultSetMappingBuilder('u');
        $selectClause = $rsm->generateSelectClause();
        $sql = "
            SELECT $selectClause
            FROM info_user u
            WHERE EXISTS(SELECT 1 FROM info_companyowner co WHERE co.company_id = $companyId AND co.owner_id = u.id)
        ";
        $query = $this->getEntityManager()->createNativeQuery($sql, $rsm);
        return $query->getResult();
    }

    public function findByContactOwner($contactId)
    {
        $rsm = $this->createResultSetMappingBuilder('u');
        $selectClause = $rsm->generateSelectClause();
        $sql = "
            SELECT $selectClause
            FROM info_user u
            WHERE EXISTS(SELECT 1 FROM info_contactowner co WHERE co.contact_id = $contactId AND co.owner_id = u.id)
        ";
        $query = $this->getEntityManager()->createNativeQuery($sql, $rsm);
        return $query->getResult();
    }

    /**
     * Get room reserves for user
     *
     * @param InfoUserEntity $user
     * @param array $filter
     * @param array $orderBy
     * @param int $offset
     * @param int $limit
     * @return InfoRtcRoomReserve[]
     */
    public function findRoomReserves(\TeamSoft\CrmRepositoryBundle\Entity\InfoUser $user, $filter = [], $orderBy = [], $limit = 0, $offset = 0)
    {
        $qb = $this->createQueryBuilder('u');
        $qb->innerJoin('u.rtcRoomCollection', 'rtcRoom')
            ->innerJoin('rtcRoom.rtcRoomReserves', 'rtcRoomReserve')
            ->select('rtcRoomReserve')
            ->where('rtcRoom.user = :user')
            ->setParameter('user', $user);

        if (isset($filter['datefrom']) && $filter['datefrom']) {
            $qb->andWhere($qb->expr()->gt('rtcRoomReserve.datetill', ':datefrom'))
                ->setParameter('datefrom', $filter['datefrom']);
        }

        if (isset($filter['datetill']) && $filter['datetill']) {
            $qb->andWhere($qb->expr()->lt('rtcRoomReserve.datefrom', ':datetill'))
                ->setParameter('datetill', $filter['datetill']);
        }

        if (isset($filter['rtcRoom']) && $filter['rtcRoom']) {
            $qb->andWhere('rtcRoom = :rtcRoom')
                ->setParameter('rtcRoom', $filter['rtcRoom']);
        }

        if (isset($filter['contact'])) {
            $qb->andWhere('rtcRoomReserve.contact = :contact')
                ->setParameter('contact', $filter['contact']);
        }

        if (count($orderBy)) {
            $qb->orderBy(implode(', ', array_map(function ($orderByCase) {
                return 'rtcRoomReserve.' . $orderByCase;
            }, $orderBy)));
        }

        if ($offset) {
            $qb->setFirstResult($offset);
        }

        if ($limit) {
            $qb->setMaxResults($limit);
        }

        return $qb->getQuery()->getResult();
    }

    /**
     * Get call statistics for user
     *
     * @param InfoUserEntity $user
     * @param array $filter
     * @param array $orderBy
     * @param int $offset
     * @param int $limit
     * @return InfoCallStatistics[]
     */
    public function findCallStatistics(\TeamSoft\CrmRepositoryBundle\Entity\InfoUser $user, $filter = [], $orderBy = [], $limit = 0, $offset = 0)
    {
        $qb = $this->createQueryBuilder('u');
        $qb->innerJoin('u.rtcRoomCollection', 'rtcRoom')
            ->innerJoin('rtcRoom.rtcRoomReserves', 'rtcRoomReserve')
            ->innerJoin('rtcRoomReserve.callStatisticsCollection', 'callStatistics')
            ->select('callStatistics')
            ->where('rtcRoom.user = :user')
            ->setParameter('user', $user);

        if (isset($filter['datefrom']) && $filter['datefrom']) {
            $qb->andWhere($qb->expr()->gt('callStatistics.datetill', ':datefrom'))
                ->setParameter('datefrom', $filter['datefrom']);
        }

        if (isset($filter['datetill']) && $filter['datetill']) {
            $qb->andWhere($qb->expr()->lt('callStatistics.datefrom', ':datetill'))
                ->setParameter('datetill', $filter['datetill']);
        }

        if (isset($filter['duration']) && $filter['duration']) {
            $qb->andWhere($qb->expr()->between('callStatistics.duration', ':durationMin', ':durationMax'))
                ->setParameter('durationMin', $filter['duration'][0])
                ->setParameter('durationMax', $filter['duration'][1]);
        }

        if (isset($filter['rtcRoom']) && $filter['rtcRoom']) {
            $qb->andWhere('rtcRoom = :rtcRoom')
                ->setParameter('rtcRoom', $filter['rtcRoom']);
        }

        if (isset($filter['rtcRoomReserve']) && $filter['rtcRoomReserve']) {
            $qb->andWhere('rtcRoomReserve = :rtcRoomReserve')
                ->setParameter(':rtcRoomReserve', $filter['rtcRoomReserve']);
        }

        if (isset($filter['contact'])) {
            $qb->andWhere('rtcRoomReserve.contact = :contact')
                ->setParameter('contact', $filter['contact']);
        }

        if (count($orderBy)) {
            $qb->orderBy(implode(', ', array_map(function ($orderByCase) {
                return 'callStatistics.' . $orderByCase;
            }, $orderBy)));
        }

        if ($offset) {
            $qb->setFirstResult($offset);
        }

        if ($limit) {
            $qb->setMaxResults($limit);
        }

        return $qb->getQuery()->getResult();
    }

    public function findByPlanPresentationFilter(array $filter = [])
    {
        $tableName = $this->getClassMetadata()->getTableName();

        $whereParts = [];
        $joinParts = [];

        $optionalWhere = [];
        $optionalJoin = [];

        if (isset($filter['datefrom']) && $filter['datefrom']) {
            $optionalWhere[] = "info_plan.datefrom <= '{$filter['datefrom']}'";
        }

        if (isset($filter['datetill']) && $filter['datetill']) {
            $optionalWhere[] = "info_plan.datetill >= '{$filter['datetill']}'";
        }

        if (isset($filter['directionId']) && $filter['directionId']) {
            $optionalJoin[] = "
                INNER JOIN info_userdirection on info_userdirection.user_id = info_plan.owner_id
            ";
            $optionalWhere[] = "info_userdirection.direction_id = {$filter['directionId']}";
        }

        if (isset($filter['subdirectionId']) && $filter['subdirectionId']) {
            $optionalJoin[] = "
                INNER JOIN info_usersubdirection on info_usersubdirection.user_id = info_plan.owner_id
            ";
            $optionalWhere[] = "info_usersubdirection.subdirection_id = {$filter['subdirectionId']}";
        }

        $optionalJoin = implode(' ', $optionalJoin);
        if (!empty($optionalWhere)) {
            $optionalWhere = ' AND ' . join(' AND ', $optionalWhere);
        } else {
            $optionalWhere = "";
        }

        $whereParts[] = "
            exists(select 1 from info_plan 
                INNER JOIN info_plandetail on info_plandetail.plan_id = info_plan.id
                INNER JOIN info_planpresentation on info_planpresentation.plandetail_id = info_plandetail.id
                INNER JOIN info_clm on info_clm.id = info_planpresentation.presentation_id
                {$optionalJoin}
                where info_plan.owner_id = {$tableName}.id {$optionalWhere}
            )
        ";


        $rsm = new ResultSetMappingBuilder($this->_em);
        $rsm->addRootEntityFromClassMetadata($this->_entityName, $tableName, array(), ResultSetMappingBuilder::COLUMN_RENAMING_INCREMENT);

        $selectClause = $rsm->generateSelectClause();

        $join = join(' ', $joinParts);
        $where = count($whereParts) ? 'WHERE ' . implode(' AND ', $whereParts) : '';

        $sql = "
            SELECT {$selectClause}  FROM {$tableName}   
            {$join}
            {$where}   
        ";

        $query = $this->getEntityManager()->createNativeQuery($sql, $rsm);
        return $query->getResult();
    }

    public function findWithTaskSaleProject(array $findBy = [])
    {
        $query = $this->createQueryBuilder('u')
            ->distinct()
            ->select(['u.id', 'u.name as value'])
            ->innerJoin(InfoTasksaleproject::class, 'tsl', 'WITH', 'u.id = tsl.user');

        if ($findBy['approvementStatus']) {
            $query = $query->where('tsl.approvementStatus = :status')
                ->setParameter('status', $findBy['approvementStatus']);
        }

        return $query->getQuery()
            ->getArrayResult();
    }
}
