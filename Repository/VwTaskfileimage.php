<?php

namespace TeamSoft\CrmRepositoryBundle\Repository;

use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\QueryBuilder;
use Doctrine\Persistence\ManagerRegistry;

class VwTaskfileimage extends ServiceEntityRepository
{

    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, \TeamSoft\CrmRepositoryBundle\Entity\VwTaskfileimage::class);
    }

    /**
     * @param array $filter
     * @param array $orderBy
     * @param null $offset
     * @param null $limit
     * @return \TeamSoft\CrmRepositoryBundle\Entity\VwTaskfileimage[]
     */
    public function getTaskFileImagesByFilter($filter = [], $orderBy = [], $offset = null, $limit = null)
    {
        $queryBuilder = $this->getTaskFileImageFilteredQueryBuilder($filter);

        $queryBuilder
            ->select('tfi');

        if ($offset) {
            $queryBuilder->setFirstResult($offset);
        }

        if ($limit) {
            $queryBuilder->setMaxResults($limit);
        }

        foreach ($orderBy as $attribute => $orderDirection) {
            $queryBuilder
                ->addOrderBy("tfi.{$attribute}", $orderDirection);
        }

        return $queryBuilder->getQuery()->getResult();
    }

    /**
     * @param array $filter
     * @return mixed
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function getTaskFileImagesCount($filter = [])
    {
        $queryBuilder = $this->getTaskFileImageFilteredQueryBuilder($filter);
        $queryBuilder
            ->distinct(false)
            ->select($queryBuilder->expr()->countDistinct('tfi'), ' AS count');

        return $queryBuilder->getQuery()->getSingleScalarResult();
    }

    /**
     * @param array $filter
     * @return QueryBuilder
     */
    public function getTaskFileImageFilteredQueryBuilder($filter = [])
    {
        $queryBuilder = $this->createQueryBuilder('tfi');
        /**
         * join task photos so that only tasks with photos will be selected
         */
        $queryBuilder
            ->innerJoin('tfi.subj', 'tfg')
            ->innerJoin('tfg.subj', 'report')
            ->select('DISTINCT tfi');

        //filter tasks
        $datefrom = $filter['datefrom'] ?? null;
        $datetill = $filter['datetill'] ?? null;
        if ($datefrom && $datetill) {
            $queryBuilder
                ->andWhere($queryBuilder->expr()->between('report.datefrom', ':datefrom', ':datetill'))
                ->setParameters([
                    ':datefrom' => $datefrom,
                    ':datetill' => $datetill
                ]);
        } elseif ($datefrom) {
            $queryBuilder
                ->andWhere($queryBuilder->expr()->gte('report.datefrom', ':datefrom'))
                ->setParameter(':datefrom', $datefrom);
        } elseif ($datetill) {
            $queryBuilder
                ->andWhere($queryBuilder->expr()->lte('report.datefrom', ':datetill'))
                ->setParameter(':datetill', $datetill);
        }

        if ($company = $filter['company'] ?? null) {
            $queryBuilder
                ->andWhere($queryBuilder->expr()->in('report.company', ':company'))
                ->setParameter(':company', $company);
        }

        if ($contact = $filter['contact'] ?? null) {
            $queryBuilder
                ->andWhere($queryBuilder->expr()->in('report.contact', ':contact'))
                ->setParameter(':contact', $contact);
        }

        if ($net = $filter['net'] ?? null) {
            $queryBuilder
                ->innerJoin('report.company', 'company')
                ->andWhere($queryBuilder->expr()->in('company.centerId', ':net'))
                ->setParameter(':net', $net);
        }

        if ($main = $filter['main'] ?? null) {
            $queryBuilder
                ->innerJoin('report.company', 'company')
                ->andWhere($queryBuilder->expr()->in('company.main', ':main'))
                ->setParameter(':main', $main);
        }

        if ($responsible = $filter['responsible'] ?? null) {
            $queryBuilder
                ->andWhere($queryBuilder->expr()->in('report.responsible', ':responsible'))
                ->setParameter(':responsible', $responsible);
        }

        if ($tasktype = $filter['tasktype'] ?? null) {
            $queryBuilder
                ->andWhere($queryBuilder->expr()->in('report.tasktype', ':tasktype'))
                ->setParameter(':tasktype', $tasktype);
        }

        if ($taskstate = $filter['taskstate'] ?? null) {
            $queryBuilder
                ->andWhere($queryBuilder->expr()->in('report.taskstate', ':taskstate'))
                ->setParameter(':taskstate', $taskstate);
        }

        if ($phototype = $filter['phototype'] ?? null) {
            $phototype = array_map(function (\TeamSoft\CrmRepositoryBundle\Entity\InfoCustomdictionaryvalue $customdictionaryvalue) {
                return $customdictionaryvalue->getName();
            }, $phototype);

            $queryBuilder
                ->andWhere($queryBuilder->expr()->in('tfg.name', ':phototype'))
                ->setParameter(':phototype', $phototype);
        }

        if ($phototostate = $filter['photostate'] ?? null) {
            $queryBuilder
                ->andWhere($queryBuilder->expr()->in('tfi.photostate', ':photostate'))
                ->setParameter(':phototostate', $phototostate);
        }

        if ($photoservicetype = $filter['photoservicetype'] ?? null) {
            $queryBuilder
                ->andWhere($queryBuilder->expr()->in('tfi.photoservicetype', ':photoservicetype'))
                ->setParameter(':photoservicetype', $photoservicetype);
        }

        if ($whatsinside = $filter['whatsinside'] ?? null) {
            $queryBuilder
                ->andWhere($queryBuilder->expr()->in('tfi.whatsinside', ':whatsinside'))
                ->setParameter(':whatsinside', $whatsinside);
        }

        if ($brand = $filter['brand'] ?? null) {
            $queryBuilder
                ->andWhere($queryBuilder->expr()->in('tfi.brand', ':brand'))
                ->setParameter(':brand', $brand);
        }

        if ($specialization = $filter['specialization'] ?? null) {
            $queryBuilder
                ->innerJoin('report.contact', 'contact')
                ->andWhere($queryBuilder->expr()->in('contact.specialization', ':specialization'))
                ->setParameter(':specialization', $specialization);
        }

        if ($regionpart = $filter['regionpart'] ?? null) {
            $subquery_company = $this->getEntityManager()->getRepository(\TeamSoft\CrmRepositoryBundle\Entity\InfoCompany::class)->createQueryBuilder('company');
            $subquery_company
                ->innerJoin('company.region', 'company_region')
                ->innerJoin('company_region.regionparts', 'company_regionpart')
                ->andWhere($subquery_company->expr()->in('company_regionpart', ':regionpart'))
                ->setMaxResults(1);

            $subquery_contact = $this->getEntityManager()->getRepository(\TeamSoft\CrmRepositoryBundle\Entity\InfoContact::class)->createQueryBuilder('contact');
            $subquery_contact
                ->innerJoin('contact.region', 'contact_region')
                ->innerJoin('contact_region.regionparts', 'contact_regionparts')
                ->andWhere($subquery_contact->expr()->in('contact_regionparts', ':regionpart'))
                ->setMaxResults(1);

            $queryBuilder
                ->andWhere($queryBuilder->expr()->orX()->addMultiple([
                    $queryBuilder->expr()->exists($subquery_company),
                    $queryBuilder->expr()->exists($subquery_contact)
                ]))
                ->setParameter(':regionpart', $regionpart);
        }

        if ($region = $filter['region'] ?? null) {
            $subquery_company = $this->getEntityManager()->getRepository(\TeamSoft\CrmRepositoryBundle\Entity\InfoCompany::class)->createQueryBuilder('company');
            $subquery_company
                ->andWhere($subquery_company->expr()->eq('company', 'report.company'))
                ->andWhere($subquery_company->expr()->in('company.region', ':region'))
                ->setMaxResults(1);

            $subquery_contact = $this->getEntityManager()->getRepository(\TeamSoft\CrmRepositoryBundle\Entity\InfoContact::class)->createQueryBuilder('contact');
            $subquery_contact
                ->andWhere($subquery_contact->expr()->eq('contact', 'report.contact'))
                ->andWhere($subquery_contact->expr()->in('contact.region', ':region'));

            $queryBuilder
                ->andWhere($queryBuilder->expr()->orX()->addMultiple([
                    $queryBuilder->expr()->exists($subquery_company),
                    $queryBuilder->expr()->exists($subquery_contact)
                ]))
                ->setParameter(':region', $region);
        }

        if ($city = $filter['city'] ?? null) {
            $subquery_company = $this->getEntityManager()->getRepository(\TeamSoft\CrmRepositoryBundle\Entity\InfoCompany::class)->createQueryBuilder('company');
            $subquery_company
                ->andWhere($subquery_company->expr()->eq('company', 'report.company'))
                ->andWhere($subquery_company->expr()->in('company.city', ':city'))
                ->setMaxResults(1);

            $subquery_contact = $this->getEntityManager()->getRepository(\TeamSoft\CrmRepositoryBundle\Entity\InfoContact::class)->createQueryBuilder('contact');
            $subquery_contact
                ->andWhere($subquery_company->expr()->eq('contact', 'report.company'))
                ->andWhere($subquery_company->expr()->in('contact.city', ':city'))
                ->setMaxResults(1);

            $queryBuilder->andWhere($queryBuilder->expr()->orX()->addMultiple([
                $queryBuilder->expr()->exists($subquery_company),
                $queryBuilder->expr()->exists($subquery_contact)
            ]))
                ->setParameter(':city', $city);
        }

        return $queryBuilder;
    }
}
