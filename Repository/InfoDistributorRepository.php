<?php
declare(strict_types=1);


namespace TeamSoft\CrmRepositoryBundle\Repository;


use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use TeamSoft\CrmRepositoryBundle\Entity\InfoDistributor;
use TeamSoft\CrmRepositoryBundle\Entity\SaleProject\InfoTasksaleproject;
use TeamSoft\CrmRepositoryBundle\Service\DatabasePlatform;
use Doctrine\Persistence\ManagerRegistry;

class InfoDistributorRepository extends ServiceEntityRepository
{
    private $dp;

    public function __construct(ManagerRegistry $registry, DatabasePlatform $dp)
    {
        parent::__construct($registry, InfoDistributor::class);
        $this->dp = $dp;
    }

    public function findWithTaskSaleProject(array $findBy = [])
    {
        $query =  $this->createQueryBuilder('d')
            ->distinct()
            ->select(['d.id', 'd.name as value'])
            ->innerJoin(InfoTasksaleproject::class, 'tsl', 'WITH', 'd.id = tsl.distributor');

        if($findBy['approvementStatus']) {
            $query = $query->where('tsl.approvementStatus = :status')
                ->setParameter('status', $findBy['approvementStatus']);
        }

        return $query->getQuery()
            ->getArrayResult();
    }
}