<?php
declare(strict_types=1);

namespace TeamSoft\CrmRepositoryBundle\Repository\PoGridConfig;

use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Mapping;
use Doctrine\ORM\Query\ResultSetMappingBuilder;
use Doctrine\Persistence\ManagerRegistry;
use TeamSoft\CrmRepositoryBundle\Entity\GridConfig\PoGridConfig;
use TeamSoft\CrmRepositoryBundle\Entity\GridConfig\PoGridConfigByRole;
use TeamSoft\CrmRepositoryBundle\Entity\GridConfig\PoGridConfigByUser;
use TeamSoft\CrmRepositoryBundle\Entity\InfoUser;
use TeamSoft\CrmRepositoryBundle\Repository\ResultSetMappingBuilderTrait;

/**
 * Class PoGridConfigRepository
 * @package TeamSoft\CrmRepositoryBundle\Repository\PoGridConfig
 *
 * @method PoGridConfig|null find($id, $lockMode = null, $lockVersion = null)
 * @method PoGridConfig|null findOneBy(array $criteria, array $orderBy = null)
 * @method PoGridConfig[]    findAll()
 * @method PoGridConfig[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class PoGridConfigRepository extends ServiceEntityRepository
{
    use ResultSetMappingBuilderTrait;

    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, PoGridConfig::class);
    }

    public function findAllByPage(int $pageId) {
        return $this
            ->createQueryBuilder('g')
            ->andWhere('g.page=:page')
            ->setParameter('page', $pageId)
            ->getQuery()
            ->getResult();
    }

    /**
     * @param InfoUser $user
     * @param int $pageId
     * @return PoGridConfig[]|[]
     */
    public function findMergedGridConfigs(int $pageId, InfoUser $user): array
    {
        $userId = $user->getId();
        $roleId = $user->getRoleId() ?: -1;


        $rsm = new ResultSetMappingBuilder($this->_em);
        $rsm->addRootEntityFromClassMetadata($this->_entityName, 'gc', [], ResultSetMappingBuilder::COLUMN_RENAMING_NONE);
        $rsm->addJoinedEntityFromClassMetadata(PoGridConfigByRole::class, 'gcr', 'gc', 'gridConfigByRoles',
            $this->getRenamedColumns($this->_em, PoGridConfigByRole::class, 'gcr_')
        );
        $rsm->addJoinedEntityFromClassMetadata(PoGridConfigByUser::class, 'gcu', 'gc', 'gridConfigByUsers',
            $this->getRenamedColumns($this->_em, PoGridConfigByUser::class, 'gcu_')
        );

        // todo
        $sql = "
            SELECT
                {$rsm->generateSelectClause()},

                CASE
                    WHEN (gcr.id IS NOT NULL AND gcu.id IS NOT NULL AND gcr.is_shown = 1) THEN COALESCE(gcu.is_shown, 0)
                    WHEN (gcr.id IS NOT NULL AND gcu.id IS NULL) THEN COALESCE(gcr.is_shown, 0)
                    WHEN (gcr.id IS NOT NULL) THEN COALESCE(gcr.is_shown, 0)
                    WHEN (gcr.id IS NULL AND gcu.id IS NOT NULL) THEN COALESCE(gcu.is_shown, 0)
                    ELSE COALESCE(gc.is_shown, 0)
                END as is_shown,

                CASE
                    WHEN (gcr.id IS NOT NULL AND gcu.id IS NOT NULL AND gcr.is_shown_dcr = 1) THEN COALESCE(gcu.is_shown_dcr, 0)
                    WHEN (gcr.id IS NOT NULL AND gcu.id IS NULL) THEN COALESCE(gcr.is_shown_dcr, 0)
                    WHEN (gcr.id IS NOT NULL) THEN COALESCE(gcr.is_shown_dcr, 0)
                    WHEN (gcr.id IS NULL AND gcu.id IS NOT NULL) THEN COALESCE(gcu.is_shown_dcr, 0)
                    ELSE COALESCE(gc.is_shown_dcr, 0)
                END as is_shown_dcr, 
                
                COALESCE(gcu.sort_order, gcr.sort_order, gc.sort_order, 0) AS sort_order,
                COALESCE(gcu.sort_order, gcr.sort_order, gc.sort_order, 0) AS sort_order_main,
                   
                CASE
                    WHEN (gcr.id IS NULL AND gcu.id IS NOT NULL) OR (gcr.id IS NOT NULL AND gcu.id IS NOT NULL AND COALESCE(gcu.field_width, 0) <> 0) THEN COALESCE(gcu.field_width, 0)
                    WHEN (gcr.id IS NOT NULL AND COALESCE(gcr.field_width, 0) <> 0) THEN COALESCE(gcr.field_width, 0)
                    ELSE COALESCE(gc.field_width, 0) 
                END as field_width
                
            FROM po_gridconfig AS gc
                LEFT OUTER JOIN po_gridconfigbyrole AS gcr ON gcr.gridconfig_id = gc.id AND gcr.role_id = {$roleId}
                LEFT OUTER JOIN po_gridconfigbyuser AS gcu ON gcu.gridconfig_id = gc.id AND gcu.user_id = {$userId}
            WHERE gc.page = {$pageId}
            ORDER BY sort_order_main ASC, gc.field_name ASC
        ";

//        $rsm->addScalarResult('gc.sortable', 'sortable', 'integer');
        $query = $this->_em->createNativeQuery($sql, $rsm);

        return $query->getResult();
    }
}
