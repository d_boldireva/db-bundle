<?php
declare(strict_types=1);

namespace TeamSoft\CrmRepositoryBundle\Repository\PoGridConfig;

use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Mapping;
use Doctrine\Persistence\ManagerRegistry;
use TeamSoft\CrmRepositoryBundle\Entity\GridConfig\PoGridConfig;
use TeamSoft\CrmRepositoryBundle\Entity\GridConfig\PoGridConfigByRole;
use TeamSoft\CrmRepositoryBundle\Entity\GridConfig\PoGridConfigByUser;

/**
 * Class PoGridConfigByRoleRepository
 * @package TeamSoft\CrmRepositoryBundle\Repository\PoGridConfig
 * 
 * @method PoGridConfigByRole|null find($id, $lockMode = null, $lockVersion = null)
 * @method PoGridConfigByRole|null findOneBy(array $criteria, array $orderBy = null)
 * @method PoGridConfigByRole[]    findAll()
 * @method PoGridConfigByRole[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class PoGridConfigByRoleRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, PoGridConfigByRole::class);
    }

    public function findAllByPageAndRole(int $pageId, int $roleId): array
    {
        return $this
            ->createQueryBuilder('r')
            ->innerJoin('r.gridConfig', 'g')
            ->andWhere('g.page=:page')
            ->setParameter('page', $pageId)
            ->andWhere('r.role=:role')
            ->setParameter('role', $roleId)
            ->getQuery()
            ->getResult();
    }
}
