<?php
declare(strict_types=1);

namespace TeamSoft\CrmRepositoryBundle\Repository\PoGridConfig;

use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Mapping;
use Doctrine\Persistence\ManagerRegistry;
use TeamSoft\CrmRepositoryBundle\Entity\GridConfig\PoGridConfig;
use TeamSoft\CrmRepositoryBundle\Entity\GridConfig\PoGridConfigByUser;

/**
 * Class PoGridConfigByUserRepository
 * @package TeamSoft\CrmRepositoryBundle\Repository\PoGridConfig
 *
 * @method PoGridConfigByUser|null find($id, $lockMode = null, $lockVersion = null)
 * @method PoGridConfigByUser|null findOneBy(array $criteria, array $orderBy = null)
 * @method PoGridConfigByUser[]    findAll()
 * @method PoGridConfigByUser[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class PoGridConfigByUserRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, PoGridConfigByUser::class);
    }

    public function findAllByPageAndUser(int $pageId, int $userId): array
    {
        return $this
            ->createQueryBuilder('u')
            ->innerJoin('u.gridConfig', 'g')
            ->andWhere('g.page=:page')
            ->setParameter('page', $pageId)
            ->andWhere('u.user=:user')
            ->setParameter('user', $userId)
            ->getQuery()
            ->getResult();
    }
}
