<?php

namespace TeamSoft\CrmRepositoryBundle\Repository;

use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\Query\Expr\Join;
use Doctrine\ORM\QueryBuilder;
use Doctrine\Persistence\ManagerRegistry;
use TeamSoft\CrmRepositoryBundle\Entity\InfoUser;
use TeamSoft\CrmRepositoryBundle\Entity\InfoCompany;
use TeamSoft\CrmRepositoryBundle\Entity\InfoUserbrick;
use TeamSoft\CrmRepositoryBundle\Entity\InfoCompanyinfo;
use TeamSoft\CrmRepositoryBundle\Service\DatabasePlatform;

class InfoGranuelplaningcompany extends ServiceEntityRepository {

    private $dp;

    public function __construct(ManagerRegistry $registry, DatabasePlatform $dp)
    {
        parent::__construct($registry, \TeamSoft\CrmRepositoryBundle\Entity\InfoGranuelplaningcompany::class);
        $this->dp = $dp;
    }
    
    public function findAvailableCompanies(InfoUser $user, $filter = [], $searchTerritories = false) {
        $infoType = $this->_em->getRepository(\TeamSoft\CrmRepositoryBundle\Entity\InfoAddinfotype::class)->findOneBy(['code' => \TeamSoft\CrmRepositoryBundle\Entity\InfoAddinfotype::CODE_TURNOVER]);
        $companyTypes = $this->_em->getRepository(\TeamSoft\CrmRepositoryBundle\Entity\InfoCompanytype::class)->findBy(['isshop' => 1]);
        /**
         * @var QueryBuilder $queryBuilder
         */
        $queryBuilder = $this->_em->getRepository(InfoCompany::class)->createQueryBuilder('company');
        $queryBuilder->leftJoin(\TeamSoft\CrmRepositoryBundle\Entity\InfoGranuelplaningcompany::class,'gpc','WITH',$queryBuilder->expr()->andX()->addMultiple([
                $queryBuilder->expr()->eq('gpc.company','company'),
                $queryBuilder->expr()->eq('gpc.user',':user')
            ]))
            ->leftJoin(InfoCompanyinfo::class,'info','WITH',$queryBuilder->expr()->andX()->addMultiple([
                $queryBuilder->expr()->eq('info.subj','company'),
                $queryBuilder->expr()->eq('info.infotype',':infotype')
            ]))
            ->leftJoin('company.city','city')
            ->andWhere($queryBuilder->expr()->isNull('gpc.id'))
            ->andWhere($queryBuilder->expr()->in('company.companytype',':companytypes'))
            ->select("company.id, company.name, CONCAT(city.name, ', ', company.street, ', ',company.building) as address, COALESCE(info.intvalue,0) as turnover")
            ->setMaxResults(21)
            ->orderBy('turnover','DESC')
            ->setParameter(':user',$user)
            ->setParameter(':infotype',$infoType)
            ->setParameter(':companytypes',$companyTypes);

        if($search = $filter['name'] ?? null){
            $queryBuilder
                ->andWhere($queryBuilder->expr()->like('company.name',':search'))
                ->setParameter(':search',"%{$search}%");
        }

        if($searchTerritories){
            $queryBuilder
                ->leftJoin(InfoUserbrick::class,'brick',Join::WITH,$queryBuilder->expr()->eq('brick.company','company'))
                ->andWhere($queryBuilder->expr()->eq('brick.user',':user'));
        }

        return $queryBuilder->getQuery()->getResult();
    }
}
