<?php

namespace TeamSoft\CrmRepositoryBundle\Repository;

use Doctrine\ORM\EntityRepository;

class InfoMcmtrigger extends EntityRepository
{

    /**
     * @param array $filter
     * @return array|boolean
     */
    public function findAllByFilter(array $filter)
    {
        $dateNow = new \DateTime();
        $query = $this
            ->createQueryBuilder('t')
            ->where('t.isActive = ' . $filter['isActive'])
            ->andWhere('t.type = ' . $filter['type'])
            ->andWhere(':dateNow between t.datefrom and t.datetill')
            ->setParameter('dateNow', $dateNow->format('Y-m-d H:i:s'));
        if(isset($filter['mcmdistribution'])) {
            $query->andWhere('t.mcmdistribution = ' . $filter['mcmdistribution']);
        }

        $result = $query
            ->getQuery()
            ->getResult();

        return $result;
    }

    /**
     * @param array $filter
     * @param integer $offset
     * @param integer $limit
     * @return array|boolean
     */
    public function findByFilter(array $filters, int $offset, int $limit)
    {
        $query = $this
            ->createQueryBuilder('t');

        if (isset($filters['archived']) && $filters['archived']) {
            $query->where('t.isActive <> 1');
        } else {
            $query->where('t.isActive = 1');
        }

        if(isset($filters['mcmdistribution'])) {
            $query->andWhere('t.mcmdistribution = ' . $filters['mcmdistribution']);
        }

        if (isset($filters['search'])) {
            $query->andWhere('LOWER(c.name) LIKE :search')
                ->setParameter('search', mb_strtolower('%'.$filters['search'].'%'));
        }

        $totalCountQueryBuilder = clone $query;
        $totalCountQueryBuilder->select('COUNT(t.id)');

        if ($offset) {
            $query->setFirstResult($offset);
        }

        if ($limit) {
            $query->setMaxResults($limit);
        }

        $query->addOrderBy('t.id', 'DESC');

        return [
            'total_count' => $totalCountQueryBuilder->getQuery()->getSingleScalarResult(),
            'data' => $query->getQuery()->getResult() ?? []
        ];
    }

}