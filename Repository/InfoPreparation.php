<?php

namespace TeamSoft\CrmRepositoryBundle\Repository;

use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query\ResultSetMappingBuilder;
use TeamSoft\CrmRepositoryBundle\Entity\InfoCompanypreparation2;

class InfoPreparation extends EntityRepository
{

    public function findForTaskPreparation($taskId, $planId, $userId, $datefrom = null, bool $usePlanTaskPeriodOption = false)
    {
        $taskId = is_numeric($taskId) ? $taskId : "0";
        $planId = is_numeric($planId) ? $planId : "0";

        $tableName = $this->getClassMetadata()->getTableName();

        $rsm = new ResultSetMappingBuilder($this->_em);
        $rsm->addRootEntityFromClassMetadata($this->_entityName, $tableName, array(), ResultSetMappingBuilder::COLUMN_RENAMING_INCREMENT);
        $rsm->addJoinedEntityFromClassMetadata(
            InfoCompanypreparation2::class,
            'info_companypreparation2',
            $tableName,
            'companyPreparations2',
            array(),
            ResultSetMappingBuilder::COLUMN_RENAMING_INCREMENT
        );

        $joinDirections = '';
        $joinPlanCompanyPreparation2set2 = '';

        if ($this->countPlanPreparation2ByPlanId($planId) > 0) {
            $where = "$tableName.id in (select info_planpreparation2.prep_id from info_planpreparation2 where info_planpreparation2.plan_id = $planId)";
        } else if ($userId) {
            $joinDirections = "
                JOIN info_preparationdirection ON info_preparationdirection.preparation_id = $tableName.id
                JOIN info_userdirection ON info_preparationdirection.direction_id = info_userdirection.direction_id 
            ";
            $where = "info_userdirection.user_id = $userId";
        } else {
            $where = "$tableName.ismer = 1";
        }

        if ($usePlanTaskPeriodOption && $datefrom) {
            $joinPlanCompanyPreparation2set2 = "
                INNER JOIN info_plancompanypreparation2set2 pcp ON pcp.prep_id = $tableName.id AND pcp.plandt_id IN (
                    SELECT id from info_plandt WHERE plan_id = $planId and '$datefrom' between dt1 and dt2
                )
            ";
        }

        $selectClause = $rsm->generateSelectClause();
        $sql = "
            SELECT $selectClause
            FROM $tableName
            LEFT OUTER JOIN info_companypreparation2 ON $tableName.id = info_companypreparation2.preparation_id AND info_companypreparation2.task_id = $taskId
            $joinDirections
            $joinPlanCompanyPreparation2set2
            WHERE $where
            ORDER BY $tableName.row_num, $tableName.name
        ";

        $query = $this->getEntityManager()->createNativeQuery($sql, $rsm);
        $result = $query->getResult();

        return $result;
    }

    private function countPlanPreparation2ByPlanId($planId)
    {
        $connection = $this->getEntityManager()->getConnection();
        return $connection
            ->executeQuery("
                SELECT COUNT(1)
                FROM info_planpreparation2
                WHERE plan_id = $planId
            ")
            ->fetchOne();
    }
}
