<?php

namespace TeamSoft\CrmRepositoryBundle\Repository;

use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\Query\ResultSetMappingBuilder;
use Doctrine\Persistence\ManagerRegistry;
use TeamSoft\CrmRepositoryBundle\Entity\InfoFiltersetmobile as InfoFilteretmobileEntity;
use TeamSoft\CrmRepositoryBundle\Entity\InfoUser as InfoUserEntity;
use TeamSoft\CrmRepositoryBundle\Entity\InfoPlan as InfoPlanEntity;
use TeamSoft\CrmRepositoryBundle\Service\DatabasePlatform;

class InfoPlan extends ServiceEntityRepository
{
    private $dp;

    public function __construct(ManagerRegistry $registry, DatabasePlatform $dp)
    {
        parent::__construct($registry, InfoPlanEntity::class);
        $this->dp = $dp;
    }

    public function findByFilter(
        array $filterBy,
        array $orderBy = null,
        $limit = null,
        $offset = null,
        InfoUserEntity $user = null
    )
    {
        $tableName = $this->getClassMetadata()->getTableName();
        $orderByColumn = is_array($orderBy) && isset($orderBy[0]) ? $orderBy[0]["column"] : "name";
        $orderByOrder = is_array($orderBy) && isset($orderBy[0]) ? $orderBy[0]["order"] : "asc";

        $whereParts = [];
        $infoPlanWhereParts = [];

        $c = $this->dp->getConcatOperator();
        $workDayIsNull = $this->dp->getIsNullFunctionExpression('info_plan.workday', 0);
        if (isset($filterBy['name']) && $filterBy['name']) {
            $whereParts[] = "LOWER(info_plan.Name $c '' $c d.name) LIKE LOWER('%" . str_replace("'", "''", $filterBy['name']) . "%')";
        }

        if (isset($filterBy['search']) && $filterBy['search']) {
            $whereParts[] = "LOWER(info_plan.name) LIKE LOWER('%" . $filterBy['search'] . "%')";
        }

        if (isset($filterBy['direction']) && $filterBy['direction']) {
            $whereParts[] = "LOWER(d.name) LIKE LOWER('%" . $filterBy['direction'] . "%')";
        }
        if (isset($filterBy['direction_ids']) && $filterBy['direction_ids']) {
            $whereParts[] = "d.id in (" . implode(',', $filterBy['direction_ids']) . ")";
        }

        if (isset($filterBy['subdirection']) && $filterBy['subdirection']) {
            $whereParts[] = "LOWER(sd.name) LIKE LOWER('%" . $filterBy['subdirection'] . "%')";
        }

        if (isset($filterBy['owner']) && $filterBy['owner']) {
            $whereParts[] = "LOWER(iuo.name) LIKE LOWER('%" . $filterBy['owner'] . "%')";
        }

        if (isset($filterBy['owner_position']) && $filterBy['owner_position']) {
            $whereParts[] = "LOWER(op.Name) LIKE LOWER('%" . $filterBy['owner_position'] . "%')";
        }

        if (isset($filterBy['creator']) && $filterBy['creator']) {
            $whereParts[] = "LOWER(iuc.name) LIKE LOWER('%" . $filterBy['creator'] . "%')";
        }

        if (isset($filterBy['datefrom']) && $filterBy['datefrom']) {
            $wherePart = [];
            foreach ($filterBy['datefrom'] as $date) {
                // info_plan.DateFrom datetime in mssql
                // info_plan.DateFrom date in postgress
                $wherePart[] = $this->dp->getDateEqualExpression('info_plan.DateFrom', $date);
            }
            $whereParts[] = "(" . implode(' OR ', $wherePart) . ")";
        }

        if (isset($filterBy['datetill']) && $filterBy['datetill']) {
            $wherePart = [];
            foreach ($filterBy['datetill'] as $date) {
                // info_plan.DateTill datetime in mssql
                // info_plan.DateTill date in postgress
                $wherePart[] = $this->dp->getDateEqualExpression('info_plan.DateTill', $date);
            }
            $whereParts[] = "(" . implode(' OR ', $wherePart) . ")";
        }

        if (isset($filterBy['workday']) && is_numeric($filterBy['workday'])) {
            $whereParts[] = $workDayIsNull . ' = ' . $filterBy['workday'];
        }

        if (isset($filterBy['taskcount']) && is_numeric($filterBy['taskcount'])) {
            $infoPlanWhereParts[] = 'info_plan.taskcount = ' . $filterBy['taskcount'];
        }

        if (isset($filterBy['filters']) && is_array($filterBy['filters'])) {
            $userId = $user ? $user->getId() : -1;
            foreach ($filterBy['filters'] as $filter) {
                list($filterEntity, $args) = $filter;
                if ($filterEntity instanceof InfoFilteretmobileEntity) {
                    if ($filterEntity->getPage() == 103) {
                        $wherePart = str_replace("#USER_ID#", $userId, $filterEntity->getFilter());

                        if ($filterEntity->isFlag()) {
                            $flag = isset($args[0]) && $args[0] == 1 ? 1 : 0;
                            $wherePart = str_replace("#FLAG#", $flag, $wherePart);
                        }
                        if ($filterEntity->isSingle()) {
                            $wherePart = str_replace("#SINGLE#", '', $wherePart);
                        }

                        $whereParts[] = $wherePart;
                    }
                }
            }
        }

        /* Commented to make work logic from https://teamsoft.atlassian.net/browse/OL-35
        if ($user && !$user->isAdmin()) {
            $whereParts[] = "(
                $tableName.owner_id IN (
                    select child_id from info_contactSubmission where subj_id = " . $user->getId() . "
                ) OR $tableName.owner_id = " . $user->getId() . "
            )";
        }
        */

        $rsm = new ResultSetMappingBuilder($this->_em);
        $rsm->addRootEntityFromClassMetadata($this->_entityName, $tableName, array(), ResultSetMappingBuilder::COLUMN_RENAMING_INCREMENT);
        $rsm->addScalarResult('percent', 'percent', 'string');
        $rsm->addScalarResult('direction_name', 'direction_name', 'string');
        $rsm->addScalarResult('subdirection_name', 'subdirection_name', 'string');

        $selectClause = $rsm->generateSelectClause();
        $where = $whereParts ? 'WHERE ' . implode(' AND ', $whereParts) : '';
        $infoPlanWhere = $infoPlanWhereParts ? 'WHERE ' . implode(' AND ', $infoPlanWhereParts) : '';

        $taskCountIsNull = $this->dp->getIsNullFunctionExpression('(SELECT SUM(taskcount) FROM info_plandetail WHERE plan_id = info_plan.id)', 0);
        $isNoContactIsNull = $this->dp->getIsNullFunctionExpression('info_target.isnocontact', 0);
        $sql = "select $selectClause, direction_name, subdirection_name\n" .
//        "case when info_plan.taskcount = 0 then " .
//        $dp->getConcatExpression("'0% ('", 'CAST(info_plan.cnt AS VARCHAR)', "'/'", 'CAST(info_plan.taskcount AS VARCHAR)', "')'") . "\n".
//        "else " . $dp->getConcatExpression('CAST(((info_plan.cnt * 100) / info_plan.taskcount) AS VARCHAR)', "'% ('", 'CAST(info_plan.cnt AS VARCHAR)', "'/'", 'CAST(info_plan.taskcount AS VARCHAR)', "')'") . "\n".
//        "end as " . $this->dp->escapeColumn('percent') . "\n" .
            "from (
        SELECT info_plan.id,
           info_plan.DateFrom,
           info_plan.DateTill,
           info_plan.CreateDate,
           info_plan.Description,
           info_plan.Name,
           info_plan.CountAction,
           $workDayIsNull as workday,
           info_plan.taskinday,
           info_plan.review,
           info_plan.guid,
           info_plan.currenttime,
           info_plan.moduser,
           info_plan.Creator_id,
           info_plan.Owner_id,
        $taskCountIsNull AS taskcount,
        (SELECT COUNT(*)
        FROM info_task
        INNER JOIN info_taskstate ON info_task.taskstate_id = info_taskstate.id AND info_taskstate.isfinish = 1
        INNER JOIN info_plandetail ON info_plandetail.plan_id = info_task.plan_id AND info_plandetail.tasktype_id = info_task.tasktype_id
        INNER JOIN info_target ON info_plandetail.target_id = info_target.id
        LEFT JOIN info_company ON info_task.company_id = info_company.id
        LEFT JOIN info_targetspec ON info_plandetail.target_id = info_targetspec.target_id
        LEFT JOIN info_contact ON info_task.contact_id = info_contact.id
        WHERE info_plandetail.plan_id = info_plan.id
        AND (
        info_contact.category_id = info_plandetail.category_id
        OR info_plandetail.category_id IS NULL
        )
        AND (
        info_company.category_id = info_plandetail.categorycompany_id
        OR info_plandetail.categorycompany_id IS NULL
        )
        AND (
        info_targetspec.spec_id = info_task.Specialization_id
        OR $isNoContactIsNull = 1
        )) AS cnt,
        d.name as direction_name,
        sd.name as subdirection_name
        FROM $tableName
        left join (select user_id, min(direction_id) as direction_id from info_userdirection group by user_id) as ud on ud.user_id = $tableName.owner_id
        left join info_direction d on d.id = ud.direction_id
        left join (select user_id, min(subdirection_id) as subdirection_id from info_usersubdirection group by user_id) as usd on usd.user_id = $tableName.owner_id
        left join info_subdirection sd on sd.id = usd.subdirection_id
        LEFT JOIN info_user iuc ON info_plan.Creator_id = iuc.id
        LEFT JOIN info_user iuo ON info_plan.Owner_id = iuo.id
        LEFT JOIN info_dictionary op ON iuo.position_id = op.id
        $where
        ) as info_plan
        {$infoPlanWhere}
        ORDER BY $tableName.$orderByColumn $orderByOrder
        ";

        $sql = $this->_em->getConnection()->getDatabasePlatform()->modifyLimitQuery($sql, $limit, $offset);
        $query = $this->getEntityManager()->createNativeQuery($sql, $rsm);

        $mixedResult = $query->getResult();

        $totalCountSql = "SELECT COUNT(distinct $tableName.id) FROM $tableName
            left join (select user_id, min(direction_id) as direction_id from info_userdirection group by user_id) as ud on ud.user_id = $tableName.owner_id
            left join info_direction d on d.id = ud.direction_id
            left join (select user_id, min(subdirection_id) as subdirection_id from info_usersubdirection group by user_id) as usd on usd.user_id = $tableName.owner_id
            left join info_subdirection sd on sd.id = usd.subdirection_id
            LEFT JOIN info_user iuc ON info_plan.Creator_id = iuc.id
            LEFT JOIN info_user iuo ON info_plan.Owner_id = iuo.id
            LEFT JOIN info_dictionary op ON iuo.position_id = op.id
            $where";

        $result = array(
            "total_count" => (int)$this->getEntityManager()->getConnection()->executeQuery($totalCountSql)->fetchOne(),
            "data" => array(),
        );

        foreach ($mixedResult as $mixedResultItem) {
            /**
             * @var $planEntity \TeamSoft\CrmRepositoryBundle\Entity\InfoPlan
             */
            $planEntity = $mixedResultItem[0];
            $planEntity->setDirectionName($mixedResultItem['direction_name']);
            $planEntity->setSubDirectionName($mixedResultItem['subdirection_name']);
            $result["data"][] = $planEntity;
        }

        return $result;
    }

    public function copyPlan(InfoPlanEntity $plan, $name, \DateTime $dateFrom, \DateTime $dateTill, InfoUserEntity $creator, InfoUserEntity $owner)
    {
        if (!$name) {
            if ($owner->getId() == $creator->getId()) {
                $name = $creator->getName();
            } else {
                $name = $owner->getName() . "/" . $creator->getName();
            }
            $name .= " " . $dateFrom->format("d.m.Y") . " - " . $dateTill->format("d.m.Y");
        }

        $newPlan = new InfoPlanEntity();
        $newPlan->setName($name);
        $newPlan->setDatefrom($dateFrom);
        $newPlan->setDatetill($dateTill);
        $newPlan->setCreator($creator);
        $newPlan->setOwner($owner);
        $this->_em->persist($newPlan);
        $this->_em->flush();
        $this->_em->detach($newPlan);

        // ToDo: biomedica db method PDO::lastInsertId returns incorrect value
        $connection = $this->getEntityManager()->getConnection();
        //ToDo: remove refetch when PDO::lastInsertId will be working correct
        $lastInsertId = $this->dp->lastInsertId('info_plan');
        /**
         * @var InfoPlanEntity $newPlan
         */
        $newPlan = $this->find($lastInsertId);

        $sql = "
            INSERT INTO info_plandetail(plan_id,tasktype_id,target_id,planmodel_id,TaskCount,pokr,prcnt,description,category_id,categorycompany_id)
            SELECT :newPlanId,tasktype_id,target_id,planmodel_id,TaskCount,pokr,prcnt,description,category_id,categorycompany_id
            FROM info_plandetail
            WHERE plan_id = :planId;
            
            INSERT INTO info_planpresentation (plandetail_id, presentation_id, ntask)
            SELECT ipd.id,presentation_id,ntask
            FROM info_plandetail,info_planpresentation,info_plandetail ipd
            WHERE info_plandetail.plan_id = :planId
            AND ipd.plan_id = :newPlanId
            AND info_planpresentation.plandetail_id = info_plandetail.id
            AND ipd.tasktype_id = info_plandetail.tasktype_id 
            AND ipd.target_id = info_plandetail.target_id
            AND (ipd.category_id = info_plandetail.category_id or 
                    (ipd.category_id is null and info_plandetail.category_id is null))
            AND (ipd.categorycompany_id = info_plandetail.categorycompany_id or 
                    (ipd.categorycompany_id is null and info_plandetail.categorycompany_id is null));
                    
            INSERT INTO info_plantaskperiod (plandetail_id, num, dt1, dt2)
            SELECT ipd.id,num, dt1, dt2
            FROM info_plandetail,info_plantaskperiod,info_plandetail ipd
            WHERE info_plandetail.plan_id = :planId
              AND ipd.plan_id = :newPlanId
              AND info_plantaskperiod.plandetail_id = info_plandetail.id
              AND ipd.tasktype_id = info_plandetail.tasktype_id
              AND ipd.target_id = info_plandetail.target_id
              AND (ipd.category_id = info_plandetail.category_id or
                   (ipd.category_id is null and info_plandetail.category_id is null))
              AND (ipd.categorycompany_id = info_plandetail.categorycompany_id or
                   (ipd.categorycompany_id is null and info_plandetail.categorycompany_id is null));

            INSERT INTO info_plancompanypreparation2set (plan_id,field)
            SELECT :newPlanId,field
            FROM info_plancompanypreparation2set
            WHERE plan_id = :planId;

            INSERT INTO info_planpreparation2 (plan_id,prep_id)
            SELECT :newPlanId,prep_id
            FROM info_planpreparation2
            WHERE plan_id = :planId;

            INSERT INTO info_planpreparation (plandetail_id,brend_id,prepline_id,descr,plancnt,ntask,strPlanCount,strFactCount,strPotential)
            SELECT ipd.id,brend_id,prepline_id,descr,plancnt,ntask,strPlanCount,strFactCount,strPotential
            FROM info_plandetail,info_planpreparation,info_plandetail ipd
            WHERE info_plandetail.plan_id = :planId
            AND ipd.plan_id = :newPlanId
            AND info_planpreparation.plandetail_id = info_plandetail.id
            AND ipd.tasktype_id = info_plandetail.tasktype_id
            AND ipd.target_id = info_plandetail.target_id
            AND (ipd.category_id = info_plandetail.category_id OR (ipd.category_id IS NULL AND info_plandetail.category_id IS NULL))
            AND (ipd.categorycompany_id = info_plandetail.categorycompany_id OR (ipd.categorycompany_id IS NULL AND info_plandetail.categorycompany_id IS NULL));

            INSERT INTO info_planpromo(plandetail_id,promo_id,cnt,ntask)
            SELECT ipd.id,promo_id,cnt,ntask
            FROM info_plandetail,info_planpromo,info_plandetail ipd
            WHERE info_plandetail.plan_id = :planId
            AND ipd.plan_id = :newPlanId
            AND info_planpromo.plandetail_id = info_plandetail.id
            AND ipd.tasktype_id = info_plandetail.tasktype_id
            AND ipd.target_id = info_plandetail.target_id
            AND (ipd.category_id = info_plandetail.category_id OR (ipd.category_id IS NULL AND info_plandetail.category_id IS NULL))
            AND (ipd.categorycompany_id = info_plandetail.categorycompany_id OR (ipd.categorycompany_id IS NULL AND info_plandetail.categorycompany_id IS NULL));

            INSERT INTO info_plantasksubject(plan_id,tasksubject_id,ntask,plandetail_id)
            SELECT ipd.plan_id,tasksubject_id,ntask,ipd.id
            FROM info_plandetail,info_plantasksubject,info_plandetail ipd
            WHERE info_plandetail.plan_id = :planId
            AND ipd.plan_id = :newPlanId
            AND info_plantasksubject.plandetail_id = info_plandetail.id
            AND ipd.tasktype_id = info_plandetail.tasktype_id
            AND ipd.target_id = info_plandetail.target_id
            AND (ipd.category_id = info_plandetail.category_id OR (ipd.category_id IS NULL AND info_plandetail.category_id IS NULL))
            AND (ipd.categorycompany_id = info_plandetail.categorycompany_id OR (ipd.categorycompany_id IS NULL AND info_plandetail.categorycompany_id IS NULL));
        ";
        $sql = str_replace(":planId", $plan->getId(), $sql);
        $sql = str_replace(":newPlanId", $newPlan->getId(), $sql);
        $connection->executeStatement($sql);
        return $newPlan;
    }

    public function findByOwnerAndPeriod($ownerId, \DateTime $dateFrom, \DateTime $dateTill = null)
    {
        $dateFrom = \DateTime::createFromFormat('Y-m-d H:i:s', $dateFrom->format('Y-m-d') . ' 00:00:00');
        $dateTill = $dateTill ? \DateTime::createFromFormat('Y-m-d H:i:s', $dateTill->format('Y-m-d') . ' 00:00:00') : clone $dateFrom;
        $dateTill->add(\DateInterval::createFromDateString('86399 second'));
        $query = $this->createQueryBuilder('p')
            ->where('p.owner IN (:ownerId)')
            ->setParameter(':ownerId', $ownerId)
            ->andWhere('((p.datefrom BETWEEN :dateFrom AND :dateTill) OR (p.datetill BETWEEN :dateFrom1 AND :dateTill1) OR ((:dateFrom2 BETWEEN p.datefrom AND p.datetill) AND (:dateTill2 BETWEEN p.datefrom AND p.datetill)))')
            ->setParameter(':dateFrom', $dateFrom)
            ->setParameter(':dateFrom1', $dateFrom)
            ->setParameter(':dateFrom2', $dateFrom)
            ->setParameter(':dateTill', $dateTill)
            ->setParameter(':dateTill1', $dateTill)
            ->setParameter(':dateTill2', $dateTill)
            ->getQuery();
        return $query->getResult();
    }
}
