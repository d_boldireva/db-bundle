<?php

namespace TeamSoft\CrmRepositoryBundle\Repository;

use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query\ResultSetMappingBuilder;

class InfoSuburb extends EntityRepository
{

    public function findByFilter(array $filter)
    {
        $tableName = $this->getClassMetadata()->getTableName();

        $whereParts = array();

        if (isset($filter['search']) && $filter['search']) {
            $whereParts[] = "COALESCE($tableName.name, '') LIKE '%" . $filter['search'] . "%'";
        }

        if (isset($filter['cityId']) && $filter['cityId']) {
            $whereParts[] = "$tableName.city_id = " . $filter['cityId'];
        }

        $rsm = new ResultSetMappingBuilder($this->_em);
        $rsm->addRootEntityFromClassMetadata(
            $this->_entityName,
            $tableName,
            array(),
            ResultSetMappingBuilder::COLUMN_RENAMING_INCREMENT
        );

        $selectClause = $rsm->generateSelectClause();
        $where = $whereParts ? 'WHERE ' . implode(' AND ', $whereParts) : '';
        $sql = "
            SELECT $selectClause FROM $tableName
            $where
            ORDER BY $tableName.name
        ";

        $query = $this->getEntityManager()->createNativeQuery($sql, $rsm);

        return $query->getResult();
    }
} 