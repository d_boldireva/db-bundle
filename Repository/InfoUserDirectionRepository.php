<?php
declare(strict_types=1);

namespace TeamSoft\CrmRepositoryBundle\Repository;

use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use TeamSoft\CrmRepositoryBundle\Entity\InfoDirection;
use TeamSoft\CrmRepositoryBundle\Entity\InfoUser;
use TeamSoft\CrmRepositoryBundle\Entity\InfoUser as InfoUserEntity;
use TeamSoft\CrmRepositoryBundle\Entity\InfoUserdirection;

class InfoUserDirectionRepository extends ServiceEntityRepository
{

    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, InfoUserdirection::class);
    }

    public function getAllowedDirectionIds(InfoUserEntity $user = null)
    {
        $userDirectionCountSql = "select count(1) from info_userdirection where user_id =" . $user->getId();
        $userDirectionCount = $this->_em->getConnection()->executeQuery($userDirectionCountSql)->fetchOne() ?? 0;

        if ($userDirectionCount == 0) {
            $directionIds = $this->getEntityManager()->getRepository(InfoDirection::class)
                ->createQueryBuilder('d')
                ->select('d.id, d.name')->getQuery()->getResult();
        } else {
            $qb = $this->createQueryBuilder('ud')
                ->select('d.id, d.name')
                ->distinct()
                ->innerJoin('ud.direction', 'd')
                ->innerJoin('ud.user', 'b')
                ->where('b.submission = :currentUser')
                ->orWhere('b.id = :currentUser')
                ->setParameter(':currentUser', $user->getId());
            $directionIds = $qb->getQuery()->getResult();
        }
        return $directionIds;
    }
}
