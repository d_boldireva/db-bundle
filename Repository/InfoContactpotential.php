<?php

namespace TeamSoft\CrmRepositoryBundle\Repository;

use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\Query\Expr\Join;
use Doctrine\ORM\Query\ResultSetMappingBuilder;
use Doctrine\Persistence\ManagerRegistry;
use TeamSoft\CrmRepositoryBundle\Entity\InfoContactpotential as InfoContactPotentialEntity;
use TeamSoft\CrmRepositoryBundle\Service\DatabasePlatform;

class InfoContactpotential extends ServiceEntityRepository
{

    private $dp;

    public function __construct(ManagerRegistry $registry, DatabasePlatform $dp)
    {
        parent::__construct($registry, InfoContactPotentialEntity::class);
        $this->dp = $dp;
    }

    public function findForAgreement(array $brandIds = array())
    {
        $whereParts = array();
        $whereParts[] = (count($brandIds) > 0) ? "b.id IN (" . implode(",", $brandIds) . ")" : "1 = 0";
        $whereParts[] = 'b.id IN (SELECT ip.Brend_id from info_preparation ip where COALESCE(ip.ispromo, 0) = 1 AND ip.Brend_id = b.id)';
        $where = $whereParts ? 'WHERE ' . implode(' AND ', $whereParts) : '';

        $rsm = new ResultSetMappingBuilder($this->_em);
        $rsm->addRootEntityFromClassMetadata(InfoContactpotentialEntity::class, "cp");

        $sql = "SELECT cp.id, cp.brend_id, cp.prep_id
                FROM (
                SELECT 
                COALESCE(p.id, b.id) AS id,
                b.id AS brend_id,
                p.id AS prep_id
                FROM info_preparationbrend b 
                LEFT JOIN info_preparation p ON b.id = p.brend_id 
                AND COALESCE(p.ispromo, 0) = 1
                AND '0' = (SELECT MAX(value) FROM info_options WHERE " . $this->dp->escapeColumn('name') . " = 'contactpotentialbrend')
                $where
                ) cp
        ";

        $query = $this->getEntityManager()->createNativeQuery($sql, $rsm);

        $result = array();
        foreach ($query->getResult() as $contactPotential) {
            /**
             * @var InfoContactpotentialEntity $contactPotential
             */
            $result[] = array(
                'brand' => $contactPotential->getBrend(),
                'preparation' => $contactPotential->getPrep(),
            );
        }
        return $result;
    }

    /**
     * @param $taskId
     * @param $contactId
     * @param \DateTime|null $date
     * @return array
     * @deprecated
     */
    public function findForTaskAgreement($taskId, $contactId, \DateTime $date = null)
    {
        $taskId = is_numeric($taskId) ? $taskId : "0";
        $contactId = is_numeric($contactId) ? $contactId : "0";
        $date = $date ? "'" . $date->format("Y-m-d 00:00:00") . "'" : "NULL";

        $rsm = new ResultSetMappingBuilder($this->_em);
        $rsm->addRootEntityFromClassMetadata($this->_entityName, "c_virtual", array(), ResultSetMappingBuilder::COLUMN_RENAMING_NONE);

        $selectClause = $rsm->generateSelectClause();

        $sql = "
            DECLARE @date datetime, @taskId int, @contactId int;
            DECLARE @prevDate datetime, @prevTaskId int, @prevContactId int;

            SET @date = $date;
            SET @taskId = $taskId;
            SET @contactId = $contactId

            SET @prevDate = NULL;
            SET @prevTaskId = 0;
            SET @prevContactId = 0

			SELECT 
			$selectClause
			FROM (
				select			
				COALESCE(p.name,b.name) as name,
				info_patalog.name as patalog_name,

				b.id as brend_id,
				p.id as prep_id,
				null as rivalprep_id,
				info_patalog.id as patalog_id,
				@date as [Date],

				case when potential is not null
					then potential
					else (select max(potential) from info_contactpotential t1
						 where t1.task_id = @prevTaskId
						 and t1.brend_id = b.id
						 and COALESCE(t1.prep_id,0) = COALESCE(p.id,0)
						 and COALESCE(t1.patalog_id,0) = COALESCE(info_patalog.id,0))
					end as potential,

				case when plancount is not null
					then plancount
					else
					case when '0' = (select max(value) from info_options where name = 'FromSalePlan') then
					 (select max(PlanCount) from info_contactpotential t1
						 where t1.task_id = @prevTaskId
						 and t1.brend_id = b.id
						 and COALESCE(t1.prep_id,0) = COALESCE(p.id,0)
						 and COALESCE(t1.patalog_id,0) = COALESCE(info_patalog.id,0))
					else (select max(saleplan) from info_contactsaleplan t1, info_contactsaleplanprep t2
						 where t1.contact_id = @prevContactId
						 and @prevDate between t1.dt and t1.dt2
						 and t1.id = t2.contactsaleplan_id
						 and t2.prep_id = p.id)
					end
					end as PlanCount,

				case when (factcount is null) and '1' = (select max(value) from info_options where name = 'MonthPotential')
				   then null --prevfact.fact
				   else factcount
				   end as factcount,

				control,
				--p.code,
				--case when factcount is null and  prevfact.fact is null then 1 else 0 end as canupd,
				c.dictionary_id,	
								
				--tsp.amount as tasksaleplan
				COALESCE(c.id, COALESCE(p.id, b.id)) id,
				c.Date2,
				c.guid,
				c.currenttime,
				c.moduser,				
				@contactId contact_id,
				@taskId task_id

				from info_taskpreparation t
				inner join info_preparationbrend b on t.brend_id = b.id
				left join info_preparation p on t.brend_id = p.brend_id and '0' = (select max(value) from info_options where name = 'contactpotentialbrend')
				left join info_formpart f on f.id = p.formpart_id
				inner join info_contact on info_contact.id = @contactId
				left join info_targetspec on info_contact.specialization_id = info_targetspec.spec_id
				left join info_patalog on info_patalog.target_id = info_targetspec.target_id and
				exists(select 1 from info_patalogbrend where info_patalogbrend.patalog_id = info_patalog.id and b.id = info_patalogbrend.brend_id)
				left join info_contactpotential c on b.id = c.brend_id and COALESCE(p.id,0) = COALESCE(c.prep_id,0) and COALESCE(info_patalog.id,0) = COALESCE(c.patalog_id,0) and c.task_id = t.task_id
				--left join (select factcount as fact, prep_id, brend_id, patalog_id  from info_contactpotential where task_id = @P7) prevfact on prevfact.brend_id = b.id and COALESCE(prevfact.prep_id,0) = COALESCE(p.id,0) and COALESCE(info_patalog.id,0) = COALESCE(prevfact.patalog_id,0)
				left join info_tasksaleplan tsp on p.id = tsp.prep_id and t.task_id = tsp.task_id
				where t.task_id = @taskId
				and t.ispromo = 1
				--and ('1' <> COALESCE((select max(value) from info_options where name = 'FilterPotentialByDirection'), '0') or exists (select 1 from info_userdirection ud inner join info_preparationdirection pd on ud.direction_id = pd.direction_id  where pd.preparation_id = p.id and ud.user_id = @P9))
				 
			) c_virtual

			order by 1, 2
        ";

        $query = $this->getEntityManager()->createNativeQuery($sql, $rsm);

        $result = $query->getResult();


        $infoContactPotentialReflection = new \ReflectionClass(InfoContactPotentialEntity::class);
        /**
         * @var InfoContactPotentialEntity $entity
         */
        foreach ($result as $entity) {
            if ($entity->getId() == $entity->getPrep()->getId() || $entity->getId() == $entity->getBrend()->getId()) {
                $idProperty = $infoContactPotentialReflection->getProperty("id");
                $idProperty->setAccessible(true);
                $idProperty->setValue($entity, null);
            }
        }

        return $result;
    }

    /**
     * @param \DateTime $date
     * @param int $responsibleId
     * @param int $companyId
     * @param int $contactId
     * @return InfoContactPotentialEntity[]
     */
    public function getPreviouslyValue(\DateTime $date, int $responsibleId, int $companyId, int $contactId): array
    {
        $taskQuery = $this->getEntityManager()
            ->getRepository(\TeamSoft\CrmRepositoryBundle\Entity\InfoTask::class)
            ->createQueryBuilder('t');

        $taskQuery
            ->andWhere('t.datefrom < :datefrom')->setParameter('datefrom', $date)
            ->andWhere('t.company = :companyId')->setParameter('companyId', $companyId)
            ->andWhere('t.responsible = :responsibleId')->setParameter('responsibleId', $responsibleId)
            ->andWhere('t.contact = :contactId')->setParameter('contactId', $contactId);

        $taskQuery->innerJoin(
            \TeamSoft\CrmRepositoryBundle\Entity\InfoTaskstate::class,
            'ts', Join::WITH,
            'ts = t.taskstate AND ts.isclosed = 1 AND ts.isfinish = 1'
        );

        $taskQuery->innerJoin(InfoContactPotentialEntity::class, 'c', Join::WITH, 'c.task = t');
        $taskQuery->andWhere('c.id IS NOT NULL');

        $taskQuery
            ->orderBy('t.datefrom', 'DESC')
            ->setMaxResults(1);

        /** @var \TeamSoft\CrmRepositoryBundle\Entity\InfoTask $task */
        $task = $taskQuery->getQuery()->getOneOrNullResult();
        if (!$task) {
            return [];
        }

        $builder = $this->createQueryBuilder('cp');

//            ->select('cp', 't')
//        ->innerJoin(\TeamSoft\CrmRepositoryBundle\Entity\InfoTask::class, 't', Join::WITH, 't = cp.task');
        $builder->andWhere('cp.task = :task');
        $builder->setParameter('task', $task);

//        echo $builder->getQuery()->getSQL(), $task->getId(); exit();
        return $builder->getQuery()->getResult();
    }
}

/*
declare @p1 int
set @p1=180150459
declare @p3 int
set @p3=1
declare @p4 int
set @p4=4
declare @p5 int
set @p5=4
exec sp_cursoropen @p1 output,N'select COALESCE(p.id,b.id) as id,
          COALESCE(p.id,b.id)*1000000+COALESCE(info_patalog.id,0) as id2,
          p.name as OrderName,
          b.id as brend_id,
          p.id as prep_id,
          COALESCE(p.name,b.name) as Name,
          null as rivalprep_id,
	  '''' as rivalprepname,
          info_patalog.id as patalog_id,
          info_patalog.name as patalog,
          @P1 as date,
         case when potential is not null
         then potential
         else (select max(potential) from info_contactpotential t1
                 where t1.task_id = @P2
                 and t1.brend_id = b.id
                 and COALESCE(t1.prep_id,0) = COALESCE(p.id,0)
                 and COALESCE(t1.patalog_id,0) = COALESCE(info_patalog.id,0))
          end as potentiall,
         case when plancount is not null
         then plancount
         else
          case when ''0'' = (select max(value) from info_options where name = ''FromSalePlan'') then
             (select max(PlanCount) from info_contactpotential t1
                 where t1.task_id = @P3
                 and t1.brend_id = b.id
                 and COALESCE(t1.prep_id,0) = COALESCE(p.id,0)
                 and COALESCE(t1.patalog_id,0) = COALESCE(info_patalog.id,0))
           else (select max(saleplan) from info_contactsaleplan t1, info_contactsaleplanprep t2
                 where t1.contact_id = @P4
                 and @P5 between t1.dt and t1.dt2
                 and t1.id = t2.contactsaleplan_id
                 and t2.prep_id = p.id)
            end
          end as plancountt,
           case when (factcount is null) and ''1'' = (select max(value) from info_options where name = ''MonthPotential'')
               then prevfact.fact else factcount end as factcount,
          control,
          p.code,
          case when factcount is null and  prevfact.fact is null then 1 else 0 end as canupd,
          c.dictionary_id,
          tsp.amount as tasksaleplan
from info_taskpreparation t
inner join info_preparationbrend b on t.brend_id = b.id
left join info_preparation p on t.brend_id = p.brend_id and ''0'' = (select max(value) from info_options where name = ''contactpotentialbrend'')
left join info_formpart f on f.id = p.formpart_id
inner join info_contact on info_contact.id = @P6
left join info_targetspec on info_contact.specialization_id = info_targetspec.spec_id
left join info_patalog on info_patalog.target_id = info_targetspec.target_id and
exists(select 1 from info_patalogbrend where info_patalogbrend.patalog_id = info_patalog.id and b.id = info_patalogbrend.brend_id)
left join info_contactpotential c on b.id = c.brend_id and COALESCE(p.id,0) = COALESCE(c.prep_id,0) and COALESCE(info_patalog.id,0) = COALESCE(c.patalog_id,0) and c.task_id = t.task_id
left join (select factcount as fact, prep_id, brend_id, patalog_id  from info_contactpotential where task_id = @P7) prevfact on prevfact.brend_id = b.id and COALESCE(prevfact.prep_id,0) = COALESCE(p.id,0)
and COALESCE(info_patalog.id,0) = COALESCE(prevfact.patalog_id,0)
left join info_tasksaleplan tsp on p.id = tsp.prep_id and t.task_id = tsp.task_id
where t.task_id = @P8
and t.ispromo = 1
and (''1'' <> COALESCE((select max(value) from info_options where name = ''FilterPotentialByDirection''), ''0'')
  or exists (select 1 from info_userdirection ud inner join info_preparationdirection pd on ud.direction_id = pd.direction_id
    where pd.preparation_id = p.id and ud.user_id = @P9))
 order by 6, 10
',@p3 output,@p4 output,@p5 output,N'@P1 datetime,@P2 int,@P3 int,@P4 int,@P5 datetime,@P6 int,@P7 int,@P8 int,@P9 int','2016-01-20 00:00:00',0,0,556198,'2016-01-20 00:00:00',556198,0,196815,475
select @p1, @p3, @p4, @p5

*/

/*
public ArrayList<AgreementItem> getListAgreementItems(long dateCurrentVisit, long taskId, long contactId, int identifierId, long userId, long companyId, long planId){

		ArrayList<AgreementItem> dogovorItems = new ArrayList<AgreementItem>();


		String sql = "";

		String plancnt = "PlanCount AS " + AgreementItem.PLANCOUNT + ", FactCount AS " + AgreementItem.FACTCOUNT + ",";

		if (CustomApplication.getInstance().getBoolOption(Constants.IS_ENABLE_PLANCNT)) {
			plancnt = " CASE WHEN PlanCount is not null THEN PlanCount ELSE t.plancnt END AS " + AgreementItem.PLANCOUNT + "," +
					  " CASE WHEN (FactCount is not null) OR ('1' = (SELECT max(value) FROM info_options WHERE name = 'FactNotPlan')) THEN FactCount ELSE t.plancnt END AS " + AgreementItem.FACTCOUNT + ",";
		}

		if (CustomApplication.getInstance().getBoolOption(Constants.PRIOR_PLANNING) && identifierId == AgreementsDialog.AGREEMENT_ID) {
			plancnt = "CSP.cnt AS " + AgreementItem.PLANCOUNT + ", FactCount AS " + AgreementItem.FACTCOUNT + ",";
		}

		String loyaltyJoin = "";
		String loyaltySelect = "";
		if (CustomApplication.getInstance().getBoolOption(Constants.SHOW_LOYALTY)) {
			loyaltySelect = ", loyalty.name AS " + AgreementItem.LOYALTY + ", c.dictionary_id AS " + AgreementItem.LOYALTY_ID;
			loyaltyJoin = " LEFT JOIN info_dictionary loyalty on loyalty._id = c.dictionary_id ";
		}

        switch(identifierId){
            case AgreementsDialog.AGREEMENT_ID:

				if (CustomApplication.getInstance().getBoolOption(Constants.CLIENT_SALES_PLANNING)) {
            		sql = getAgreenentsForSalesPlanning(dateCurrentVisit, taskId, userId, contactId);
            		break;
            	}

				String salePlanCntStr = "";
				String salePlanCntJoin = "";
				String salePlanCntStrForOld = "";
				if (CustomApplication.getInstance().getBoolOption(Constants.SHOW_SALEPLAN_IN_AGREEMENT)){
					salePlanCntStr = ",  plan_saleplan.cnt  - ifnull(fact_saleplan.cnt, 0) as CountCurrent ";
					salePlanCntStrForOld = ", 0 as CountCurrent ";
					salePlanCntJoin =
							" LEFT JOIN (select brend_id, sum(count) as cnt  from info_saleplan " +
							"			inner join info_saleperiod on info_saleperiod._id = info_saleplan.saleperiod_id and " + dateCurrentVisit + " between info_saleperiod.datefrom and info_saleperiod.datetill + 86400 " +
							"			inner join info_saleplanpreparation on info_saleplanpreparation.saleplan_id = info_saleplan._id " +
							"			where info_saleplan.user_id = " + userId + " group by preparation_id, brend_id) plan_saleplan on plan_saleplan.brend_id = t.brend_id " +
							" LEFT JOIN (select info_contactpotential.brend_id, sum(FactCount) as cnt from info_task " +
							"			inner join info_taskstate on info_taskstate._id = info_task.taskstate_id and ifnull(isclosed,0) = 1 and ifnull(isfinish,0) = 1 " +
							"			inner join info_saleperiod on " + dateCurrentVisit + " between info_saleperiod.datefrom and info_saleperiod.datetill + 86400 " +
							"			inner join info_contactpotential on info_contactpotential.task_id = info_task._id " +
							"			where responsible_id = " + userId + " and info_task.datefrom between info_saleperiod.datefrom and info_saleperiod.datetill + 86400 " +
							"			group by info_contactpotential.brend_id) fact_saleplan on fact_saleplan.brend_id = t.brend_id ";
				}


            	Long[] prevTaskId = getPrevTaskId(taskId, dateCurrentVisit, contactId);
            	if (CustomApplication.getInstance().getBoolOption(Constants.SHOW_ONE_LAST_RESULT_IN_ARRANGEMENT)) {
            		prevTaskId[1] = prevTaskId[0];
            	}

            	String potential = " ifnull(Potential, (SELECT max(t1.Potential) FROM info_contactpotential t1 WHERE t1.task_id = " + prevTaskId[0] +
                        " AND t1.brend_id = b._id AND ifnull(t1.prep_id, 0) = ifnull(p._id, 0))) as ";
            	if (CustomApplication.getInstance().getBoolOption(Constants.NO_GET_POTENTIAL_FROM_PREV_TASK)) {
            		potential = " Potential as ";
            	}

                sql = "SELECT ifnull(p._id,b._id) AS " + AgreementItem.ID + ", b._id AS " + AgreementItem.BREND_ID + ", p._id AS " + AgreementItem.PREP_ID + ", " +
						" null AS " + AgreementItem.RIVAL_ID + ", 0 as isRival, " +
						dateCurrentVisit + " AS date, ifnull(p.name, b.name) AS " + AgreementItem.NAME + loyaltySelect +
						", " + potential + AgreementItem.POTENTIAL + "," + plancnt +
                        " 0 AS " + AgreementItem.ISOLD + "," +
						" t.strPlanCount AS strPlanCount," +
						" t.strPotential AS strPotential," +
						" t.strFactCount AS strFactCount " +
						salePlanCntStr +
						" FROM info_taskpreparation t INNER JOIN info_prepline on t.prepline_id = info_prepline._id and info_prepline.name <> 'Напоминание' "+
                        " INNER JOIN info_preparationbrend b ON t.brend_id = b._id " +
                        " LEFT JOIN info_preparation p ON t.brend_id = p.Brend_id AND '0' = (SELECT max(value) FROM info_options WHERE name = 'contactpotentialbrend') " +
                        "		AND p._id in (SELECT preparation_id FROM info_preparationdirection, info_userdirection WHERE info_preparationdirection.direction_id = info_userdirection.direction_id" +
                        " 					  and info_userdirection.user_id = " + userId + ")" +
                        " LEFT JOIN info_contactpotential c ON b._id = c.brend_id AND ifnull(p._id, 0) = ifnull(c.prep_id, 0) AND c.task_id = t.task_id ";

				if (CustomApplication.getInstance().getBoolOption(Constants.PRIOR_PLANNING)) {
					sql += " LEFT JOIN info_contactsaleplan AS CSP ON CSP.brend_id=b._id AND CSP.contact_id=" + contactId + " AND CSP.user_id=" + userId +
					" AND cast(strftime('%m',date(dt, 'unixepoch', 'localtime')) AS INTEGER) = cast(strftime('%m',date(" + dateCurrentVisit + ", 'unixepoch', 'localtime')) AS INTEGER)" +
					" AND cast(strftime('%Y',date(dt,'unixepoch', 'localtime')) AS INTEGER) = cast(strftime('%Y',date(" + dateCurrentVisit + ", 'unixepoch', 'localtime')) AS INTEGER)";
				}

				sql += loyaltyJoin + salePlanCntJoin + " WHERE t.task_id = " + taskId + " AND t.ispromo = 1 " +
                        " AND  b._id in (SELECT brend_id FROM info_preparationdirection, info_userdirection, info_preparation " +
                        "				WHERE info_preparationdirection.direction_id = info_userdirection.direction_id AND info_preparation._id = info_preparationdirection.preparation_id" +
                        " 				and info_userdirection.user_id = " + userId + ")" +
				" UNION ALL " +
                " SELECT c._id, b._id AS " + AgreementItem.BREND_ID + " , p._id AS " + AgreementItem.PREP_ID + "," +
						" null AS " + AgreementItem.RIVAL_ID + ", 0 as isRival, " +
						" c.date, ifnull(p.name,b.name) AS " + AgreementItem.NAME + loyaltySelect +
					    ", Potential, PlanCount, FactCount, 1 AS " +  AgreementItem.ISOLD + "," +
						" t.strPlanCount AS strPlanCount, " +
						" t.strPotential AS strPotential, " +
						" t.strFactCount AS strFactCount " +
						salePlanCntStrForOld +
                        " FROM info_taskpreparation t " +
						" INNER JOIN info_prepline on t.prepline_id = info_prepline._id and info_prepline.name <> 'Напоминание' "+
                        " INNER JOIN info_preparationbrend b ON t.brend_id = b._id " +
                        " LEFT JOIN info_preparation p ON t.brend_id = p.Brend_id AND '0' = (SELECT max(value) FROM info_options WHERE name = 'contactpotentialbrend') " +
                        "		AND p._id in (SELECT preparation_id FROM info_preparationdirection, info_userdirection WHERE info_preparationdirection.direction_id = info_userdirection.direction_id" +
                        " 					 and info_userdirection.user_id = " + userId + ")" +
                        " INNER JOIN info_contactpotential c ON b._id = c.brend_id AND ifnull(p._id, 0) = ifnull(c.prep_id, 0) AND (c.task_id = " + prevTaskId[0] + " OR c.task_id = " + prevTaskId[1] + ")" +
						loyaltyJoin +
                        " WHERE t.task_id = " + taskId + " AND t.ispromo = 1 " +
                        " AND b._id in (SELECT brend_id FROM info_preparationdirection, info_userdirection, info_preparation " +
                        "				WHERE info_preparationdirection.direction_id = info_userdirection.direction_id AND info_preparation._id = info_preparationdirection.preparation_id" +
                        " 				and info_userdirection.user_id = " + userId + ")" ;

				if (CustomApplication.getInstance().getBoolOption(Constants.RIVAL_PREP_FIX)) {
					sql +=  " UNION ALL " +
							" SELECT DISTINCT rp._id AS " + AgreementItem.ID + ", null AS " + AgreementItem.BREND_ID + ", null AS " + AgreementItem.PREP_ID + ", " +
							" rp._id AS " + AgreementItem.RIVAL_ID + ", 1 as isRival, " +
							dateCurrentVisit + " AS date, rp.name AS "  + AgreementItem.NAME + loyaltySelect + ", " +
					        " c.Potential AS Potential, " +
							" c.PlanCount AS PlanCount, " +
							" c.FactCount AS FactCount, " +
							" 0 AS " + AgreementItem.ISOLD + "," +
							" null AS strPlanCount," +
							" null AS strPotential," +
							" null AS strFactCount " +
							salePlanCntStr +
							" FROM info_taskpreparation t INNER JOIN info_prepline on t.prepline_id = info_prepline._id and info_prepline.name <> 'Напоминание' "+
							" INNER JOIN info_preparationbrend b ON t.brend_id = b._id " +
							" INNER JOIN info_preparation p ON t.brend_id = p.Brend_id " +
							"		AND p._id in (SELECT preparation_id FROM info_preparationdirection, info_userdirection WHERE info_preparationdirection.direction_id = info_userdirection.direction_id" +
							" 					  and info_userdirection.user_id = " + userId + ")" +
							" INNER JOIN info_preprivalprep  prp ON  prp.prep_id =  p._id " +
							" INNER JOIN info_rivalpreparation rp  ON  rp._id = prp.rivalprep_id " +
							" LEFT JOIN info_contactpotential c ON  rp._id = c.rivalprep_id AND c.task_id = t.task_id " +
							loyaltyJoin + salePlanCntJoin + " WHERE t.task_id = " + taskId + " AND t.ispromo = 1 " +
							" AND  b._id in (SELECT brend_id FROM info_preparationdirection, info_userdirection, info_preparation " +
							"				WHERE info_preparationdirection.direction_id = info_userdirection.direction_id AND info_preparation._id = info_preparationdirection.preparation_id" +
							" 				and info_userdirection.user_id = " + userId + ")";
				}
				sql += " ORDER BY isRival, name, [date] desc ";
                break;
            case AgreementsDialog.PERVOSTOLNIK_ID:
                sql = getAgreementSql(dateCurrentVisit, taskId, companyId, contactId, plancnt, userId, planId, loyaltySelect, loyaltyJoin);
                break;
        }


		//Log.v(TAG, sql);

		Cursor cursor = mainDb.rawQuery(sql, null);


		if (cursor.moveToFirst()) {
			do {
			ContentValues values = new ContentValues();
			DatabaseUtils.cursorRowToContentValues(cursor, values);
			AgreementItem dogovorItem = new AgreementItem(values);


			if (dogovorItem != null)
				dogovorItems.add(dogovorItem);


		} while (cursor.moveToNext());

		}

		cursor.close();

		return dogovorItems;

	}
*/
