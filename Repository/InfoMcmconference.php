<?php

namespace TeamSoft\CrmRepositoryBundle\Repository;

use Doctrine\ORM\EntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use Doctrine\ORM\Query\ResultSetMappingBuilder;
use TeamSoft\CrmRepositoryBundle\Service\DatabasePlatform;

class InfoMcmconference extends EntityRepository
{
    /**
     * @param array $filters
     * @return array|boolean
     */
    public function findByFilter(array $filters)
    {
        $query = $this
            ->createQueryBuilder('c');

        if (isset($filters['user']) && $filters['user']) {
            $users = $filters['user']->getSubordinates()->toArray();
            array_push($users, $filters['user']);
            $query
                ->innerJoin('c.responsibles', 'r')
                ->andWhere('c.owner in (:users) or r.id in (:users)')
                ->setParameter('users', $users);
        }

        $query->addOrderBy('c.datefrom', 'DESC');

        $result = $query
            ->getQuery()
            ->getResult();

        if (count($result) == 0) {
            return false;
        }

        return $result;
    }
}
