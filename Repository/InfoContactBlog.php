<?php

namespace TeamSoft\CrmRepositoryBundle\Repository;


use Doctrine\ORM\EntityRepository;

/**
 * Class InfoContactBlog
 *
 * @see \TeamSoft\CrmRepositoryBundle\Entity\InfoContactBlog
 * @package TeamSoft\CrmRepositoryBundle\Repository
 */
class InfoContactBlog extends EntityRepository
{
    public function findByUniqueValidate($criteria)
    {
        $link = preg_replace('~\Ahttps?://~', '', $criteria['link']);
        $links = [];
        if ($link === $criteria['link'] || !$link) {
            $links[] = $criteria['link'];
        } else {
            $links[] = 'https://' . $link;
            $links[] = 'http://' . $link;
        }

        $query = $this->createQueryBuilder('b')
            ->where('b.link IN (:links)')->setParameter('links', $links);

        if (isset($criteria['id'])) {
            $query->andWhere('b.id <> :id')->setParameter('id', $criteria['id']);
        }

        return $query->getQuery()->getResult();
    }
}
