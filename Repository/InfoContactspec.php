<?php

namespace TeamSoft\CrmRepositoryBundle\Repository;

use Doctrine\ORM\EntityRepository;

class InfoContactspec extends EntityRepository {
    /**
     * @param array $filter
     * @param array $orderBy
     * @param integer $offset
     * @param integer $limit
     * @param bool $filter_existing
     * @return mixed
     */
    public function findWithReportsByFilter($filter = [], $orderBy = [], $offset = null, $limit = null, $filter_existing = false){
        $fieldsToSelect = [];

        foreach ($this->_em->getClassMetadata(\TeamSoft\CrmRepositoryBundle\Entity\InfoDictionary::class)->fieldMappings as $fieldMapping){
            if(in_array($fieldMapping['type'],['integer','float','string'])){
                $fieldsToSelect[] = $fieldMapping['fieldName'];
            }
        }

        $fieldsToSelect = implode(',',$fieldsToSelect);

        $queryBuilder = $this->_em->getRepository(\TeamSoft\CrmRepositoryBundle\Entity\InfoDictionary::class)->createQueryBuilder('specialization');

        $queryBuilder
            ->innerJoin(\TeamSoft\CrmRepositoryBundle\Entity\InfoContactspec::class,'cs','WITH','cs.spec = specialization')
            ->distinct(true)
            ->select("partial specialization.{{$fieldsToSelect}}");

        if($offset){
            $queryBuilder->setFirstResult($offset);
        }

        if($limit){
            $queryBuilder->setMaxResults($limit);
        }

        if($contact = $filter['contact'] ?? null){
            $queryBuilder
                ->andWhere($queryBuilder->expr()->in('cs.contact',':contact'))
                ->setParameter(':contact',$contact);
        }

        if($company = $filter['company'] ?? null){
            $queryBuilder
                ->innerJoin('cs.contact','contact')
                ->andWhere('contact.company',':company')
                ->setParameter(':company',$company);
        }

        if($net = $filter['net'] ?? null){
            $queryBuilder
                ->innerJoin('cs.contact','contact')
                ->innerJoin('contact.company','company')
                ->andWhere($queryBuilder->expr()->in('company.centerId',':net'))
                ->setParameter(':net',$net);
        }

        if($main = $filter['main'] ?? null){
            $queryBuilder
                ->innerJoin('cs.contact','contact')
                ->innerJoin('contact.company','company')
                ->andWhere($queryBuilder->expr()->in('company.main',':main'))
                ->setParameter(':main',$main);
        }

        if($regionpart = $filter['regionpart'] ?? null){
            $subqueryBuilder = $this->_em->getRepository(\TeamSoft\CrmRepositoryBundle\Entity\InfoContact::class)->createQueryBuilder('contact_region');
            $subqueryBuilder
                ->innerJoin('contact_region.region','region')
                ->innerJoin('region.regionparts','regionpart')
                ->andWhere($subqueryBuilder->expr()->eq('contact_region','sc.contact'))
                ->andWhere($subqueryBuilder->expr()->in('regionpart',':regionparts'))
                ->setParameter(':regionparts',$regionpart);

            $queryBuilder
                ->andWhere($queryBuilder->expr()->exists($subqueryBuilder));
        }

        if($region = $filter['region'] ?? null){
            $queryBuilder
                ->innerJoin('cs.contact','contact')
                ->andWhere($queryBuilder->expr()->in('contact.region',':region'))
                ->setParameter(':region',$region);
        }

        if($city = $filter['city'] ?? null){
            $queryBuilder
                ->innerJoin('cs.contact','contact')
                ->andWhere($queryBuilder->expr()->in('contact.city',':city'))
                ->setParameter(':city',$city);
        }

        if(!$filter_existing){
            if($responsible = $filter['responsible'] ?? null){
                $subqueryBuilder = $this->_em->getRepository(\TeamSoft\CrmRepositoryBundle\Entity\InfoUser::class)
                    ->createQueryBuilder('user');

                $subqueryBuilder
                    ->innerJoin('user.regionCollection','region')
                    ->andWhere($subqueryBuilder->expr()->eq('region','contact.region'))
                    ->andWhere($subqueryBuilder->expr()->in('user',':responsible'))
                    ->setParameter(':responsible',$responsible)
                    ->setMaxResults(1);

                $queryBuilder
                    ->andWhere($queryBuilder->expr()->exists($subqueryBuilder));
            }
        } else {
            $queryBuilder
                ->innerJoin(\TeamSoft\CrmRepositoryBundle\Entity\InfoTask::class,$reportAlias = 'report','WITH',"{$reportAlias}.contact = cs.contact")
                ->innerJoin("{$reportAlias}.taskFileGroupCollection",$tfgAlias = 'tfg')
                ->innerJoin("{$tfgAlias}.taskImageCollection",$tfiAlias = 'tfi');

            if($responsible = $filter['responsible'] ?? null){
                $queryBuilder
                    ->andWhere($queryBuilder->expr()->in("{$reportAlias}.responsible",':responsible'))
                    ->setParameter(':responsible',$responsible);
            }

            if($tasktype = $filter['tasktype'] ?? null){
                $queryBuilder
                    ->andWhere($queryBuilder->expr()->in("{$reportAlias}.tasktype",':tasktype'))
                    ->setParameter(':tasktype',$tasktype);
            }

            if($taskstate = $filter['taskstate'] ?? null){
                $queryBuilder
                    ->andWhere($queryBuilder->expr()->in("{$reportAlias}.taskstate",':taskstate'))
                    ->setParameter(':taskstate',$taskstate);
            }

            if($phototype = $filter['phototype'] ?? null){
                $phototype = array_map(function(\TeamSoft\CrmRepositoryBundle\Entity\InfoCustomdictionaryvalue $customdictionaryvalue){
                    return $customdictionaryvalue->getName();
                },$phototype);

                $queryBuilder
                    ->andWhere($queryBuilder->expr()->in("{$tfgAlias}.name",':phototype'))
                    ->setParameter(':phototype',$phototype);
            }

            if($phototostate = $filter['photostate'] ?? null){
                $queryBuilder
                    ->andWhere($queryBuilder->expr()->in("{$tfiAlias}.photostate",':photostate'))
                    ->setParameter(':phototostate',$phototostate);
            }

            if($photoservicetype = $filter['photoservicetype'] ?? null){
                $queryBuilder
                    ->andWhere($queryBuilder->expr()->in("{$tfiAlias}.photoservicetype",':photoservicetype'))
                    ->setParameter(':photoservicetype',$photoservicetype);
            }

            if($whatsinside = $filter['whatsinside'] ?? null){
                $queryBuilder
                    ->andWhere($queryBuilder->expr()->in("{$tfiAlias}.whatsinside",':whatsinside'))
                    ->setParameter(':whatsinside',$whatsinside);
            }

            if($brand = $filter['brand'] ?? null){
                $queryBuilder
                    ->andWhere($queryBuilder->expr()->in("{$tfiAlias}.brand",':brand'))
                    ->setParameter(':brand',$brand);
            }
        }

        foreach ($orderBy as $attribute => $direction) {
            $queryBuilder->addOrderBy("specialization.{$attribute}", $direction);
        }

        return $queryBuilder->getQuery()->getResult();
    }
}