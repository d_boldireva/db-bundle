<?php

namespace TeamSoft\CrmRepositoryBundle\Repository;

use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\Query\ResultSetMappingBuilder;
use Doctrine\Persistence\ManagerRegistry;
use TeamSoft\CrmRepositoryBundle\Entity\Sales\SynPrep as SynPrepEntity;

class SynPrep extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, SynPrepEntity::class);
    }

    /**
     * @deprecated usages not found
     * @param \DateTime $date
     * @return int|mixed|string
     */
    function findForTlsByDate(\DateTime $date) {

        $rsm = new ResultSetMappingBuilder($this->_em);
        $rsm->addRootEntityFromClassMetadata($this->_entityName, 'sp', array(), ResultSetMappingBuilder::COLUMN_RENAMING_INCREMENT);
        $selectClause = $rsm->generateSelectClause();

        $sql = "
            SELECT $selectClause 
            FROM syn_prep sp
            WHERE sp.prep_id IS NULL
        ";

        $query = $this->getEntityManager()->createNativeQuery($sql, $rsm);

        return $query->getResult();
    }

    /**
     * @deprecated usages not found
     * @param \DateTime $date
     * @return int|mixed|string
     */
    function findForWsByDate(\DateTime $date) {

        $rsm = new ResultSetMappingBuilder($this->_em);
        $rsm->addRootEntityFromClassMetadata($this->_entityName, 'sp', array(), ResultSetMappingBuilder::COLUMN_RENAMING_INCREMENT);
        $selectClause = $rsm->generateSelectClause();

        $sql = "
            SELECT $selectClause FROM syn_prep sp WHERE sp.prep_id IS NULL
        ";

        $query = $this->getEntityManager()->createNativeQuery($sql, $rsm);

        return $query->getResult();
    }
}
