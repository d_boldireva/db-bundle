<?php


namespace TeamSoft\CrmRepositoryBundle\Repository;

use Doctrine\ORM\EntityRepository;

/**
 * Class InfoSurveyResult
 * @see \TeamSoft\CrmRepositoryBundle\Entity\InfoSurveyResultAction
 * @package TeamSoft\CrmRepositoryBundle\Repository
 */
class InfoSurveyResultAction extends EntityRepository
{
    public function removeByActionAndSurvey(\TeamSoft\CrmRepositoryBundle\Entity\InfoAction $action, \TeamSoft\CrmRepositoryBundle\Entity\InfoSurvey $survey): void
    {
        $this->createQueryBuilder('r')
            ->where('r.action=:actionId AND r.survey=:surveyId')
            ->setParameters([
                'actionId' => $action->getId(),
                'surveyId' => $survey->getId(),
            ])
            ->delete()
            ->getQuery()
            ->execute();
    }
}