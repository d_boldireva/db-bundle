<?php

namespace TeamSoft\CrmRepositoryBundle\Repository;

use Doctrine\ORM\EntityRepository;

/**
 * Class InfoContactcategory
 * @package TeamSoft\CrmRepositoryBundle\Repository
 *
 * @see \TeamSoft\CrmRepositoryBundle\Entity\InfoContactcategory
 */
class InfoContactcategory extends EntityRepository
{
}
