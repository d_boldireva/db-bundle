<?php
declare(strict_types=1);

namespace TeamSoft\CrmRepositoryBundle\Repository;

use Doctrine\ORM\EntityRepository;

class InfoTaskpresentationslide extends EntityRepository
{

    /**
     * @param int $id
     * @return int|mixed|string
     * @throws \Doctrine\ORM\NoResultException
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function getCountByClmId(int $id)
    {
        return $this->createQueryBuilder('tps')
            ->select('count(tps.id)')
            ->where('tps.task IS NOT NULL')
            ->andWhere('tps.presentation = ' . $id)
            ->getQuery()
            ->getSingleScalarResult();
    }

} 