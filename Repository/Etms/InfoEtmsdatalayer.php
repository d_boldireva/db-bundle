<?php

namespace TeamSoft\CrmRepositoryBundle\Repository\Etms;

use Doctrine\ORM\EntityRepository;
use TeamSoft\CrmRepositoryBundle\Entity\Etms\InfoEtmsdatalayertag;
use TeamSoft\CrmRepositoryBundle\Entity\InfoDirection;

class InfoEtmsdatalayer extends EntityRepository
{
    public function getByDirection(InfoDirection $direction)
    {
        $layers = [];

        if ($direction) {
            $qb = $this->createQueryBuilder('dl');

            $layers = $qb
                ->join('dl.directionCollection', 'd')
                ->andWhere($qb->expr()->eq('d', ':direction'))
                ->andWhere($qb->expr()->eq('dl.enable', ':enable'))
                ->setParameter(':direction', $direction)
                ->setParameter(':enable', 1)
                ->getQuery()
                ->getResult();
        }

        return $layers;
    }

    public function getByTag(InfoEtmsdatalayertag $tag)
    {
        $layers = [];

        if ($tag) {
            $qb = $this->createQueryBuilder('dl');

            $layers = $qb
                ->select('dl', 't')
                ->join('dl.tagColletion', 't')
                ->andWhere($qb->expr()->eq('t', ':tag'))
                ->setParameter(':tag', $tag)
                ->getQuery()
                ->getArrayResult();
        }

        return $layers;
    }

    /**
     * Get distinct labels for layer
     *
     * @param TeamSoft\CrmRepositoryBundle\Entity\Etms\InfoEtmsdatalayer|int $dataLayer
     * @return string[]
     */
    public function getLabelsForLayer($dataLayer)
    {
        $qb = $this->createQueryBuilder('dl');
        $result = $qb
                ->select('dlv.label as label')
                ->where('dl = :dataLayer')
                ->setParameter(':dataLayer', $dataLayer)
                ->join('dl.valueCollection', 'dlv')
                ->distinct()
                ->getQuery()
                ->getArrayResult() ?? [];

        $result = array_map(
            function ($row) {
                return $row['label'];
            },
            $result
        );

        return $result;
    }
}
