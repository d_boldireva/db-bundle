<?php

namespace TeamSoft\CrmRepositoryBundle\Repository\Etms;

use Doctrine\DBAL\Connection;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\QueryBuilder;
use TeamSoft\CrmRepositoryBundle\Entity\InfoCompanycateg;
use TeamSoft\CrmRepositoryBundle\Entity\InfoContactcategory;
use TeamSoft\CrmRepositoryBundle\Entity\InfoDictionary;
use TeamSoft\CrmRepositoryBundle\Entity\Etms\InfoPolygonlayer;
use TeamSoft\CrmRepositoryBundle\Entity\Etms\InfoEtmsdatalayervalue;
use TeamSoft\CrmRepositoryBundle\Service\DatabasePlatform;
use TeamSoft\CrmRepositoryBundle\Service\Options;

class InfoPolygon extends EntityRepository
{
    private $users;
    /** @var InfoPolygonlayer */
    private $layer;
    private $directionId;
    private $versionId;
    private $userId;
    private $customDataLayers = [];
    private $isBalancing;
    private $balancingId;
    /** @var Options */
    private $options;
    /**
     * @var DatabasePlatform $dp
     */
    private $dp;

    public function setUsers(array $users): void
    {
        $this->users = $users;
    }

    public function setLayer(InfoPolygonlayer $layer): void
    {
        $this->layer = $layer;
    }

    public function setDirectionId(int $directionId): void
    {
        $this->directionId = $directionId;
    }

    public function setVersion(int $versionId): void
    {
        $this->versionId = $versionId;
    }

    public function setUserId($userId)
    {
        $this->userId = $userId;
    }

    public function setIsBalancing(bool $isBalancing): void
    {
        $this->isBalancing = $isBalancing;
    }

    public function setBalancingId($balancingId)
    {
        $this->balancingId = $balancingId;
    }

    public function setCustomDataLayers(array $customDataLayers = []): void
    {
        $this->customDataLayers = $customDataLayers;
    }

    public function setOptions(Options $options): void
    {
        $this->options = $options;
    }

    public function setDatabasePlatform(DatabasePlatform $databasePlatform): void
    {
        $this->dp = $databasePlatform;
    }

    public function getPolygonWKT($id, $useReduce)
    {
        $polygon = $this->find($id);
        if (!$polygon) {
            return null;
        }
        $reduce = 0.0005;

        if ($useReduce && !in_array($polygon->getPolygonlayer()->getTag(), ['user_layer', 'voronoi_layer'])) {
            return $this->_em->getConnection()->fetchOne(
                'SELECT polygon.Reduce(' . $reduce . ').STAsText() FROM info_polygon WHERE id=?', [$id]
            );
        }

        $wkt = $polygon->getPolygon();

        if (is_array($wkt)) {
            $wkt = 'POLYGON (' .
                implode(', ', array_map(function ($path) {
                    return '(' .
                        implode(',', array_map(function ($point) {
                            return $point[0] . ' ' . $point[1];
                        }, $path))
                        . ')';
                }, $wkt))
                . ')';
        }

        return $wkt;
    }

    public function getPolygonVertices($id, $withClosedPoint = false, $inverted = true, $useReduce = false)
    {
        $wkt = $this->getPolygonWKT($id, $useReduce);

        // wkt has closed point
        $polygon = \geoPHP::load($wkt, 'wkt');
        if ($inverted) {
            $polygon->invertxy();
        }
        $result = $polygon->asArray();

        if (!$withClosedPoint) {
            // remove closed point
            foreach ($result as &$vertices) {
                array_pop($vertices);
            }
        }
        return $result;
    }

    public function getPolygonGeoJSON($ids, $useReduce = false)
    {

        if (is_int($ids) || is_string($ids)) {
            $wkt = $this->getPolygonWKT($ids, $useReduce);
            $polygon = \geoPHP::load($wkt, 'wkt');
            return $polygon ? $polygon->out('json', true) : [];

        } else if (is_array($ids)) {
            if (!count($ids)) {
                return null;
            }
            $geoJson = [
                'type' => 'MultiPolygon',
                'coordinates' => [],
            ];
            foreach ($ids as $id) {
                if ($wkt = $this->getPolygonWKT($id, $useReduce)) {
                    $polygon = \geoPHP::load($wkt, 'wkt');
                    $geoJson['coordinates'][] = $polygon->asArray();
                }
            }
            return $geoJson;
        }

        return null;
    }

    public function findByLayerTagAndRegions($layerTag, $regions)
    {
        return $this->getEntityManager()->createQueryBuilder()
            ->select('p.id, p.name, p.encodedPolyline as encoded_polyline', 'r.id as region_id', 'p.isEnclave as is_enclave')
            ->from($this->_entityName, 'p')
            ->join('p.polygonLayer', 'pl')
            ->leftJoin('p.region', 'r')
            ->where('pl.tag = :tag', 'p.region IN (:regions)')
            ->andWhere('(p.iswrongpolygon IS NULL OR p.iswrongpolygon = 0)')
            ->orderBy('p.name')
            ->setParameter(':tag', $layerTag)
            ->setParameter(':regions', $regions)
            ->getQuery()
            ->getArrayResult();
    }

    public function fillEncodedPolyline($polygons)
    {

        /** @var \TeamSoft\CrmRepositoryBundle\Entity\Etms\InfoPolygon $polygon */
        foreach ($polygons as $polygon) {
            if (!$polygon->getEncodedPolyline()) {
                $innerPolygons = $polygon->getPolygon();

                $encodedPolyline = [];
                foreach ($innerPolygons as $innerPolygon) {
                    foreach ($innerPolygon as &$pair) {
                        $pair = array_reverse($pair);
                    }
                    $encodedPolyline[] = \Polyline::encode($innerPolygon);
                }

                $encodedPolyline = json_encode($encodedPolyline);
                $polygon->setEncodedPolyline($encodedPolyline);
                $this->_em->flush($polygon);
            }
        }
    }

    public function countCompanyTypes($polygonId = null)
    {
        $qb = $this->createQueryBuilder('p')
            ->select('ct.id', 'COUNT(DISTINCT cm.id) AS cnt')
            ->join('p.companies', 'pc')
            ->join('pc.company', 'cm')
            ->join('cm.companytype', 'ct')
            ->where('p.polygonLayer = :layer')
            ->setParameter(':layer', $this->layer)
            ->andWhere('(p.iswrongpolygon IS NULL OR p.iswrongpolygon = 0)')
            ->andWhere('(pc.isdeleted IS NULL OR pc.isdeleted = 0)')
            ->andWhere('(cm.isarchive IS NULL OR cm.isarchive = 0)')
            ->groupBy('ct.id');

        if ($polygonId) {
            $qb
                ->andWhere($qb->expr()->eq('p.id', $polygonId))
                ->addSelect('p.id as groupField')
                ->addGroupBy('p.id');
        } else {
            $this->joinUser($qb);

            if ($this->users) {
                $qb
                    ->andWhere($qb->expr()->in('user', ':users'))
                    ->setParameter(':users', $this->users);
            }

            $qb
                ->addGroupBy('user.id')
                ->addSelect('user.id AS groupField');
        }

        $data = $qb->getQuery()->getArrayResult();

        return $this->collect($data);
    }

    public function countCompanyCategories($polygonId = null)
    {
        $qb = $this->createQueryBuilder('p');

        if ($this->options && $this->options->get('geomarketingCompanyCategUseDirection')) {
            $qb->select('cat.id', 'COUNT(DISTINCT cm.id) AS cnt')
                ->join('p.companies', 'pc')
                ->join('pc.company', 'cm')
                ->join(InfoCompanycateg::class, 'cc', 'WITH', 'cc.company = cm AND cc.direction = :direction')
                ->join('cc.companycategory', 'cat')
                ->andWhere('(p.iswrongpolygon IS NULL OR p.iswrongpolygon = 0)')
                ->andWhere('(pc.isdeleted IS NULL OR pc.isdeleted = 0)')
                ->andWhere('(cm.isarchive IS NULL OR cm.isarchive = 0)')
                ->setParameter(':direction', $this->directionId)
                ->groupBy('cat.id');
        } else {
            $qb->select('cc.id', 'COUNT(DISTINCT cm.id) AS cnt')
                ->join('p.companies', 'pc')
                ->join('pc.company', 'cm')
                ->join('cm.category', 'cc')
                ->andWhere('(p.iswrongpolygon IS NULL OR p.iswrongpolygon = 0)')
                ->andWhere('(pc.isdeleted IS NULL OR pc.isdeleted = 0)')
                ->andWhere('(cm.isarchive IS NULL OR cm.isarchive = 0)')
                ->groupBy('cc.id');
        }

        $qb
            ->andWhere('p.polygonLayer = :layer')
            ->setParameter(':layer', $this->layer);

        if ($polygonId) {
            $qb
                ->andWhere($qb->expr()->eq('p.id', $polygonId))
                ->addSelect('p.id as groupField')
                ->addGroupBy('p.id');
        } else {
            $this->joinUser($qb);

//            $qb
//                ->join('user.regionCollection','region')
//                ->andWhere($qb->expr()->eq('region','cm.region'));

            if ($this->users) {
                $qb->andWhere('user IN(:users)')->setParameter(':users', $this->users);
            }

            $qb
                ->addSelect('user.id AS groupField')
                ->addGroupBy('user.id');
        }

        $data = $qb->getQuery()->getArrayResult();

        return $this->collect($data);
    }

    public function countContactTargetGroups($polygonId = null)
    {
        $qb = $this->createQueryBuilder('p')
            ->select('t.id', 'COUNT(DISTINCT c.id) AS cnt')
            ->join('p.companies', 'pc')
            ->join('pc.company', 'cm')
            ->join('cm.contactCollection', 'c')
            ->join('c.targetCollection', 't')
            ->where('p.polygonLayer = :layer')
            ->setParameter(':layer', $this->layer)
            ->andWhere('(p.iswrongpolygon IS NULL OR p.iswrongpolygon = 0)')
            ->andWhere('(pc.isdeleted IS NULL OR pc.isdeleted = 0)')
            ->andWhere('(cm.isarchive IS NULL OR cm.isarchive = 0)')
            ->andWhere('(c.isarchive IS NULL OR c.isarchive = 0)')
            ->groupBy('t.id');

        if ($polygonId) {
            $qb
                ->andWhere($qb->expr()->eq('p.id', $polygonId))
                ->addSelect('p.id as groupField')
                ->addGroupBy('p.id');
        } else {
            $this->joinUser($qb);

            if ($this->users) {
                $qb
                    ->andWhere('user IN(:users)')
                    ->setParameter(':users', $this->users);
            }

            $qb
                ->addSelect('user.id AS groupField')
                ->addGroupBy('user.id');
        }

        if ($this->options->get('geomarketingTargetUseDirection')) {
            $qb->andWhere('t.direction = :direction');

            if (!$qb->getParameter('direction')) {
                $qb->setParameter(':direction', $this->directionId);
            }
        }

        $data = $qb->getQuery()->getArrayResult();

        return $this->collect($data);
    }

    public function countContactSpecialization($polygonId = null)
    {
        $qb = $this->createQueryBuilder('p')
            ->select('d.id', 'COUNT(DISTINCT c.id) AS cnt')
            ->join('p.companies', 'pc')
            ->join('pc.company', 'cm')
            ->join('cm.contactCollection', 'c')
            ->join(InfoDictionary::class, 'd', 'WITH', 'c.specialization = d.id')
            ->where('p.polygonLayer = :layer')
            ->setParameter(':layer', $this->layer)
            ->andWhere('(p.iswrongpolygon IS NULL OR p.iswrongpolygon = 0)')
            ->andWhere('(pc.isdeleted IS NULL OR pc.isdeleted = 0)')
            ->andWhere('(cm.isarchive IS NULL OR cm.isarchive = 0)')
            ->andWhere('(c.isarchive IS NULL OR c.isarchive = 0)')
            ->andWhere('d.identifier=' . InfoDictionary::CONTACT_SPECIALIZATION)
            ->groupBy('d.id');

        if ($polygonId) {
            $qb
                ->andWhere($qb->expr()->eq('p.id', $polygonId))
                ->addSelect('p.id as groupField')
                ->addGroupBy('p.id');
        } else {
            $this->joinUser($qb);

            if ($this->users) {
                $qb
                    ->andWhere('user IN(:users)')
                    ->setParameter(':users', $this->users);
            }

            $qb
                ->addSelect('user.id AS groupField')
                ->addGroupBy('user.id');
        }

        $data = $qb->getQuery()->getArrayResult();

        return $this->collect($data);
    }

    public function countCustomLayer($customLayerId = 0, $polygonId = 0)
    {
        $params = [];
        $paramTypes = [];

        $qb = $this->createQueryBuilder('p')
            ->select('dlv.label as id', 'SUM(dlv.value) AS cnt')
            ->join(InfoEtmsdatalayervalue::class, 'dlv', 'with', 'STContains(p.polygon, dlv.gpspoint) = 1')
            ->where('dlv.direction = :directionId OR dlv.direction IS NULL')
            ->andWhere('dlv.datalayer = :dataLayerId')
            ->andWhere('p.polygonLayer = :polygonLayer')
            ->andWhere('dlv.gpspoint IS NOT NULL')
            ->setParameter(':directionId', $this->directionId)
            ->setParameter(':dataLayerId', $customLayerId)
            ->setParameter(':polygonLayer', $this->layer)
            ->groupBy('dlv.label');

        $params = [$this->directionId, $customLayerId, $this->layer->getId()];
        $paramTypes = [\PDO::PARAM_INT, \PDO::PARAM_INT, \PDO::PARAM_INT];

        if ($this->layer->isCustomLayer()) {
            $qb->andWhere('p.direction = :directionId');
            $params[] = $this->directionId;
            $paramTypes[] = \PDO::PARAM_INT;
        }

        if ($polygonId) {
            $qb
                ->andWhere($qb->expr()->eq('p.id', ':polygon_id'))
                ->addSelect('p.id as groupField')
                ->addGroupBy('p.id');

            $params[] = $polygonId;
            $paramTypes[] = \PDO::PARAM_INT;
        } else {
            if ($this->isBalancing) {
                $qb->join('p.polygonUserBalancing', 'pub')
                    ->join('pub.user', 'user')
                    ->join('pub.balancing', 'b')
                    ->andWhere('b.direction = :direction')
                    ->andWhere('b.version = :version')
                    ->andWhere('b.creator = :creator')
                    ->andWhere('b.id = :balancingId');

                if (!$qb->getParameter('creator')) {
                    $qb->setParameter(':creator', $this->userId);
                }

                if (!$qb->getParameter('direction')) {
                    $qb->setParameter(':direction', $this->directionId);
                }

                if (!$qb->getParameter('version')) {
                    $qb->setParameter(':version', $this->versionId);
                }

                if (!$qb->getParameter('balancingId')) {
                    $qb->setParameter(':balancingId', $this->balancingId);
                }

                $params[] = $this->userId;
                $paramTypes[] = \PDO::PARAM_INT;

                $params[] = $this->directionId;
                $paramTypes[] = \PDO::PARAM_INT;

                $params[] = $this->versionId;
                $paramTypes[] = \PDO::PARAM_INT;

                $params[] = $this->balancingId;
                $paramTypes[] = \PDO::PARAM_INT;
            } else {
                $qb->join('p.polygonUser', 'pu')
                    ->join('pu.user', 'user');

                if ($this->versionId) {
                    $qb->andWhere('pu.version = :version')
                        ->setParameter(':version', $this->versionId);

                    $params[] = $this->versionId;
                    $paramTypes[] = \PDO::PARAM_INT;
                } elseif ($this->directionId) {
                    $qb->andWhere('pu.version IS NULL')
                        ->andWhere('pu.direction = :direction')
                        ->setParameter(':direction', $this->directionId);

                    $params[] = $this->directionId;
                    $paramTypes[] = \PDO::PARAM_INT;
                }
            }

            if ($this->users) {
                $qb
                    ->andWhere('user IN(:users)')
                    ->setParameter(':users', $this->users);

                $params[] = $this->users;
                $paramTypes[] = Connection::PARAM_INT_ARRAY;
            }

            $qb
                ->addSelect('user.id AS groupField')
                ->addGroupBy('user.id');
        }

        $sql = $qb->getQuery()->getSQL();

        $matches = [];

        if ($polygonId && !$this->dp->isPostgreSQL100()) {
            $sql = str_replace('info_etmscustomdatalayervalue i0_', 'info_etmscustomdatalayervalue i0_ WITH ( INDEX (ix_info_etmscustomdatalayervalue_gpspoint))', $sql);
        } else if (!$this->dp->isPostgreSQL100() && preg_match("/info_polygon\ i\d\_/", $sql, $matches)) {
            $sql = str_replace($matches[0], $matches[0] . ' WITH( INDEX(ix_info_polygon_polygon))', $sql);
        }

        $sql = str_replace('label_0', 'id', $sql);
        $sql = str_replace('sclr_1', 'cnt', $sql);
        $sql = str_replace('id_2', 'groupField', $sql);

        //die($sql);

        $data = $this->_em->getConnection()->fetchAllAssociative($sql, $params, $paramTypes);

        return $this->collect($data);
    }

    public function countContactCategory($polygonId = null)
    {
        $qb = $this->createQueryBuilder('p')
            ->select('cc.categoryId AS id', 'COUNT(DISTINCT c.id) AS cnt')
            ->join('p.companies', 'pc')
            ->join('pc.company', 'cm')
            ->join('cm.contactCollection', 'c')
            ->join(InfoContactcategory::class, 'cc', 'WITH', 'c.id = cc.contact')
            ->where('p.polygonLayer = :layer')
            ->andWhere('(p.iswrongpolygon IS NULL OR p.iswrongpolygon = 0)')
            ->andWhere('(pc.isdeleted IS NULL OR pc.isdeleted = 0)')
            ->andWhere('(c.isarchive IS NULL OR c.isarchive = 0)')
            ->andWhere('cc.direction = :direction')
            ->setParameter(':layer', $this->layer)
            ->setParameter(':direction', $this->directionId)
            ->groupBy('cc.categoryId');

        if ($polygonId) {
            $qb
                ->andWhere($qb->expr()->eq('p.id', $polygonId))
                ->addSelect('p.id AS groupField')
                ->addGroupBy('p.id');
        } else {
            $this->joinUser($qb);

            if ($this->users) {
                $qb
                    ->andWhere('user IN(:users)')
                    ->setParameter(':users', $this->users);
            }

            $qb
                ->addSelect('user.id AS groupField')
                ->addGroupBy('user.id');
        }

        $data = $qb->getQuery()->getArrayResult();

        return $this->collect($data);
    }

    public function countTurnover($polygonId = null)
    {
        $layerId = $this->layer->getId();
        $directionId = $this->directionId;
        $versionId = $this->versionId;

        $where = '';
        $join = '';
        if ($polygonId) {
            $groupByField = 'p.id';
        } else {
            if ($this->isBalancing) {
                $join = "
  LEFT JOIN info_polygonuserbalancing pu on p.id = pu.polygon_id
  LEFT JOIN info_etmsbalancing b ON b.id = pu.balancing_id
            ";
                $where = " AND b.id = {$this->balancingId}";
            } else {
                $join = "
  LEFT JOIN info_polygonuser pu ON p.id = pu.polygon_id
                ";
                $where = " AND pu.direction_id={$directionId} AND pu.version_id={$versionId} ";
            }

            $groupByField = 'pu.user_id';
        }

        $sql = "SELECT sum(t.turnover) cnt, {$groupByField} AS groupField
FROM info_company c
left join (select * from info_turnover where dt>=NOW() - interval '1 year' ) t on t.company_id = c.id
JOIN info_polygoncompany pc ON c.id = pc.company_id
LEFT JOIN (select id, polygonlayer_id, direction_id, isWrongPolygon from info_polygon where polygonlayer_id = 1) p ON pc.polygon_id = p.id
{$join}

WHERE {$groupByField} IN( ? ) AND {$groupByField} IS NOT NULL
AND p.polygonlayer_id={$layerId}" . ($polygonId ? '' : " AND (p.direction_id = {$directionId} or p.direction_id is null)") . " 
AND (p.isWrongPolygon = 0 or p.isWrongPolygon is null) 
{$where}
GROUP BY {$groupByField}";


        $conn = $this->_em->getConnection();
        $data = $conn->fetchAllAssociative($sql, [$polygonId ? [$polygonId] : $this->users], [Connection::PARAM_INT_ARRAY]);

        return $this->collect($data, true);
    }

    private function collect($data, $intValue = false)
    {
        $result = [];
        foreach ($data as $item) {
            if ($intValue) {
                $result[$item['groupField'] ?? $item['groupfield']] = +$item['cnt'];
            } else {
                $result[$item['groupField'] ?? $item['groupfield']][] = ['id' => $item['id'] ?? 0, 'cnt' => $item['cnt']];
            }
        }

        return $result;
    }

    private function joinUser(QueryBuilder $qb)
    {
        if ($this->isBalancing) {
            $qb->join('p.polygonUserBalancing', 'pub')
                ->join('pub.user', 'user')
                ->join('pub.balancing', 'b')
                ->andWhere('b.direction = :direction')
                ->andWhere('b.version = :version')
                ->andWhere('b.creator = :creator')
                ->andWhere('b.id = :balancingId');

            if (!$qb->getParameter('creator')) {
                $qb->setParameter(':creator', $this->userId);
            }

            if (!$qb->getParameter('direction')) {
                $qb->setParameter(':direction', $this->directionId);
            }

            if (!$qb->getParameter('version')) {
                $qb->setParameter(':version', $this->versionId);
            }

            if (!$qb->getParameter('balancingId')) {
                $qb->setParameter(':balancingId', $this->balancingId);
            }
        } else {
            $qb->join('p.polygonUser', 'pu')
                ->join('pu.user', 'user');

            if ($this->versionId) {
                $qb->andWhere('pu.version = :version');

                if (!$qb->getParameter('version')) {
                    $qb->setParameter(':version', $this->versionId);
                }
            } elseif ($this->directionId) {
                $qb->andWhere('pu.version IS NULL')
                    ->andWhere('pu.direction = :direction');

                if (!$qb->getParameter('direction')) {
                    $qb->setParameter(':direction', $this->directionId);
                }
            }
        }
    }

    /**
     * @param string $name
     * @param array $arguments
     * @return array|mixed
     */
    public function __call($name, $arguments = [])
    {
        $name = strtolower($name);

        if (strpos($name, 'countcustomlayer') === 0) {
            $customLayerId = intval(str_replace('countcustomlayer', '', $name));

            if (count($arguments) && $arguments[0]) {
                $result = $this->countCustomLayer($customLayerId, $arguments[0]);
            } else {
                $result = $this->countCustomLayer($customLayerId);
            }

            return $result;
        }

        return [];
    }

    /**
     * User has assigned polygons
     *
     * @param \TeamSoft\CrmRepositoryBundle\Entity\InfoUser $user
     * @return bool
     */
    public function userHasAssignedPolygons(\TeamSoft\CrmRepositoryBundle\Entity\InfoUser $user)
    {
        return !!$this->createQueryBuilder('p')
            ->select('p.id')
            ->innerJoin('p.polygonUser', 'pu')
            ->where('pu.user = :user')
            ->setParameter(':user', $user)
            ->setMaxResults(1)
            ->getQuery()
            ->getOneOrNullResult();
    }

    /**
     * Assign user polygons to user
     *
     * @return mixed
     */
    public function assignUserPolygonsToUser($sourceUser, $targetUser)
    {
        return !!$this->createQueryBuilder('p')
            ->update(\TeamSoft\CrmRepositoryBundle\Entity\Etms\InfoPolygonuser::class, 'pu')
            ->set('pu.user', ':targetUser')
            ->where('pu.user = :sourceUser')
            ->setParameter(':sourceUser', $sourceUser)
            ->setParameter(':targetUser', $targetUser)
            ->getQuery()
            ->execute();
    }

    /**
     * Clear polygons for user
     *
     * @param $user
     * @return bool
     */
    public function clearPolygonsForUser($user)
    {
        return !!$this->createQueryBuilder('p')
            ->delete(\TeamSoft\CrmRepositoryBundle\Entity\Etms\InfoPolygonuser::class, 'pu')
            ->where('pu.user = :user')
            ->setParameter(':user', $user)
            ->getQuery()
            ->execute();
    }

    /**
     * Get polygons
     *
     * @param string $layerTag
     * @param int|null $direction_id
     * @param int[] $region_ids
     * @param int $lastId
     * @param int $limit
     * @return \TeamSoft\CrmRepositoryBundle\Entity\Etms\InfoPolygon[]
     */
    public function getPolygons(string $layerTag = 'user_layer', int $direction_id = null, array $region_ids = [], int $limit = 0, int $lastId = null)
    {
        $qb = $this->createQueryBuilder('p');
        $qb->setCacheable(true)
            ->select(
                'p.id, p.name, p.area, p.polygon, p.centerLat, p.centerLng, IDENTITY(p.region) as regionId, 
                p.population, p.perimeter, IDENTITY(p.creator) as creatorId, IDENTITY(p.direction) as directionId'
            )
            ->innerJoin('p.polygonLayer', 'pl')
            ->where('pl.tag = :tag')
            ->andWhere('(p.iswrongpolygon IS NULL OR p.iswrongpolygon = 0)')
            ->setParameter(':tag', $layerTag);

        if ($direction_id) {
            $qb->andWhere('p.direction = :direction')
                ->setParameter(':direction', $direction_id);
        }

        if ($region_ids) {
            $qb->andWhere('p.region IN (:regions)')->setParameter(':regions', $region_ids);
        }

        if ($lastId) {
            $qb->andWhere('p.id > :lastId')->setParameter(':lastId', $lastId);
        }

        $qb->orderBy('p.id', 'ASC');


        if ($limit) {
            $qb->setMaxResults($limit);
        }

        return $qb->getQuery()->getResult();
    }

    /**
     * Get polygons dictionary
     *
     * @param string $layerTag
     * @param int|null $direction_id
     * @param array $region_ids
     * @return array
     */
    public function getPolygonsDictionary(string $layerTag = 'user_layer', int $direction_id = null, array $region_ids = [])
    {
        $qb = $this->createQueryBuilder('p');
        $qb->setCacheable(true)
            ->select('p.id, p.name')
            ->innerJoin('p.polygonLayer', 'pl')
            ->where('pl.tag = :tag')
            ->andWhere('(p.iswrongpolygon IS NULL OR p.iswrongpolygon = 0)')
            ->setParameter(':tag', $layerTag);

        if ($direction_id) {
            $qb->andWhere('p.direction = :direction')
                ->setParameter(':direction', $direction_id);
        }

        if ($region_ids) {
            $qb->andWhere('p.region IN (:regions)')
                ->setParameter(':regions', $region_ids);
        }

        return $qb->getQuery()->getResult();
    }

    /**
     * Get last modified polygon in selection
     *
     * @param string $layerTag
     * @param int|null $direction_id
     * @param int[] $region_ids
     * @return \DateTime|mixed
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function getLastModified(string $layerTag = 'user_layer', int $direction_id = null, array $region_ids = [])
    {
        $qb = $this->createQueryBuilder('p');
        $qb->setCacheable(true)
            ->select('MAX(p.currenttime) as currenttime')
            ->innerJoin('p.polygonLayer', 'pl')
            ->where('pl.tag = :tag')
            ->setParameter(':tag', $layerTag);

        if ($direction_id) {
            $qb->andWhere('p.direction = :direction')
                ->setParameter(':direction', $direction_id);
        }

        if ($region_ids) {
            $qb->andWhere('p.region IN (:regions)')
                ->setParameter(':regions', $region_ids);
        }

        $result = $qb->getQuery()->getOneOrNullResult();

        if ($result) {
            $result = new \DateTime($result['currenttime']);
        }

        return $result;
    }

    /**
     * Get etag from currenttime and count
     *
     * @param string $layerTag
     * @param int|null $direction_id
     * @param int[] $region_ids
     * @return string|null
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function getEtag(string $layerTag = 'user_layer', int $direction_id = null, array $region_ids = [])
    {
        $qb = $this->createQueryBuilder('p');
        $qb->setCacheable(true)
            ->select('MAX(p.currenttime) as currenttime, COUNT(p.id) as count')
            ->innerJoin('p.polygonLayer', 'pl')
            ->where('pl.tag = :tag')
            ->setParameter(':tag', $layerTag);

        if ($direction_id) {
            $qb->andWhere('p.direction = :direction')
                ->setParameter(':direction', $direction_id);
        }

        if ($region_ids) {
            $qb->andWhere('p.region IN (:regions)')
                ->setParameter(':regions', $region_ids);
        }

        $result = $qb->getQuery()->getOneOrNullResult();

        if ($result) {
            $result = md5(json_encode($result));
        }

        return $result;
    }
}
