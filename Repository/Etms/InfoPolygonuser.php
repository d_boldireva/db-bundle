<?php

namespace TeamSoft\CrmRepositoryBundle\Repository\Etms;

use Doctrine\ORM\EntityRepository;
use TeamSoft\CrmRepositoryBundle\Entity\InfoUser;

/**
 * Class InfoPolygonuser
 * @package TeamSoft\CrmRepositoryBundle\Repository\Etms
 */
class InfoPolygonuser extends EntityRepository
{
    /**
     * Check if user has assigned polygons
     *
     * @param InfoUser $user
     * @return bool
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function userHasPolygons(InfoUser $user)
    {
        $qb = $this->createQueryBuilder('pu');
        $qb
            ->where($qb->expr()->eq('pu.user', ':user'))
            ->setParameter(':user', $user)
            ->setMaxResults(1);

        return !!$qb->getQuery()->getOneOrNullResult();
    }

    /**
     * Assign polygons from one user to another
     *
     * @param InfoUser $fromUser
     * @param InfoUser $toUser
     * @return bool
     */
    public function reassignPolygonsToUser(InfoUser $fromUser, InfoUser $toUser)
    {
        $qb = $this->createQueryBuilder('pu');
        $qb
            ->update(\TeamSoft\CrmRepositoryBundle\Entity\Etms\InfoPolygonuser::class, 'pu')
            ->set('pu.user', ':toUser')
            ->where($qb->expr()->eq('pu.user', ':fromUser'))
            ->setParameter(':toUser', $toUser)
            ->setParameter(':fromUser', $fromUser);

        return !!$qb->getQuery()->execute();
    }

    /**
     * Clear polygons by user
     *
     * @param InfoUser $user
     * @return bool
     */
    public function clearPolygonsForUser(InfoUser $user)
    {
        $qb = $this->createQueryBuilder('pu');
        $qb
            ->delete(\TeamSoft\CrmRepositoryBundle\Entity\Etms\InfoPolygonuser::class, 'pu')
            ->where($qb->expr()->eq('pu.user', ':user'))
            ->setParameter(':user', $user);

        return !!$qb->getQuery()->execute();
    }

    /**
     * Get polygon users
     *
     * @param string $layer_tag
     * @param int $version_id
     * @param int[] $region_ids
     * @param InfoUser|null $supervisor
     * @return array
     */
    public function getPolygonUsers(string $layer_tag, int $version_id, $region_ids = [], InfoUser $supervisor = null)
    {
        $qb = $this->createQueryBuilder('pu')
            ->select('p.id as polygon_id, u.id as user_id')
            ->innerJoin('pu.polygon', 'p')
            ->innerJoin('p.polygonLayer', 'pl')
            ->innerJoin('pu.user', 'u')
            ->where('pl.tag = :tag')
            ->andWhere('pu.version = :version')
            ->andWhere('IsNull(p.iswrongpolygon,0) = 0')
            ->setParameter(':tag', $layer_tag)
            ->setParameter(':version', $version_id);

        if ($supervisor) {
            $qb->andWhere('pu.user IN (:users)')
                ->setParameter(':users', array_merge([$supervisor], $supervisor->getSubordinates()->toArray()));
        }

        if ($region_ids) {
            $qb->andWhere('p.region IN (:regions)')
                ->setParameter(':regions', $region_ids);
        }

        $result = $qb->getQuery()->getResult();

        $mappedResult = [];

        foreach ($result as $row) {
            $mappedResult[$row['polygon_id']] = $row['user_id'];
        }

        return $mappedResult;
    }

    /**
     * Get last modified polygonuser
     *
     * @param string $layer_tag
     * @param int $version_id
     * @param int[] $region_ids
     * @param InfoUser|null $supervisor
     * @return \DateTime|null
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function getLastModified(string $layer_tag, int $version_id, $region_ids = [], InfoUser $supervisor = null)
    {
        $qb = $this->createQueryBuilder('pu')
            ->select('MAX(pu.currenttime) as currenttime')
            ->innerJoin('pu.polygon', 'p')
            ->innerJoin('p.polygonLayer', 'pl')
            ->where('pl.tag = :tag')
            ->andWhere('pu.version = :version')
            ->setParameter(':tag', $layer_tag)
            ->setParameter(':version', $version_id);

        if ($supervisor) {
            $qb->andWhere('pu.user IN (:users)')
                ->setParameter(':users', array_merge([$supervisor], $supervisor->getSubordinates()->toArray()));
        }

        if ($region_ids) {
            $qb->andWhere('p.region IN (:regions)')
                ->setParameter(':regions', $region_ids);
        }

        $result = $qb->getQuery()->getOneOrNullResult();

        if ($result) {
            $result = new \DateTime($result['currenttime']);
        }

        return $result;
    }

    /**
     * Get polygonuser etag
     *
     * @param string $layer_tag
     * @param int $version_id
     * @param int[] $region_ids
     * @param InfoUser|null $supervisor
     * @return string|null
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function getEtag(string $layer_tag, int $version_id, $region_ids = [], InfoUser $supervisor = null)
    {
        $qb = $this->createQueryBuilder('pu')
            ->select('MAX(pu.currenttime) as currenttime, COUNT(pu.id) as count')
            ->innerJoin('pu.polygon', 'p')
            ->innerJoin('p.polygonLayer', 'pl')
            ->where('pl.tag = :tag')
            ->andWhere('pu.version = :version')
            ->setParameter(':tag', $layer_tag)
            ->setParameter(':version', $version_id);

        if ($supervisor) {
            $qb->andWhere('pu.user IN (:users)')
                ->setParameter(':users', array_merge([$supervisor], $supervisor->getSubordinates()->toArray()));
        }

        if ($region_ids) {
            $qb->andWhere('p.region IN (:regions)')
                ->setParameter(':regions', $region_ids);
        }

        $result = $qb->getQuery()->getOneOrNullResult();

        if ($result) {
            $result = md5(json_encode($result));
        }

        return $result;
    }
}
