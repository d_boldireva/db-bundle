<?php

namespace TeamSoft\CrmRepositoryBundle\Repository\Etms;

use Doctrine\ORM\EntityRepository;
use TeamSoft\CrmRepositoryBundle\Entity\InfoUser;

class InfoEtmsactionlog extends EntityRepository
{
    /**
     * @param \TeamSoft\CrmRepositoryBundle\Entity\Etms\InfoPolygon $polygon
     * @param \TeamSoft\CrmRepositoryBundle\Entity\Etms\InfoEtmsmapsversion $version
     * @param InfoUser $user
     * @param \TeamSoft\CrmRepositoryBundle\Entity\Etms\InfoEtmsactionlog|null $etmsactionlog
     *
     * @return \TeamSoft\CrmRepositoryBundle\Entity\Etms\InfoEtmsactionlog|null
     *
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function getPrevByPolygon(\TeamSoft\CrmRepositoryBundle\Entity\Etms\InfoPolygon $polygon, \TeamSoft\CrmRepositoryBundle\Entity\Etms\InfoEtmsmapsversion $version, InfoUser $user, \TeamSoft\CrmRepositoryBundle\Entity\Etms\InfoEtmsactionlog $etmsactionlog = null)
    {
        $qb = $this->createQueryBuilder('al');
        $qb
            ->innerJoin('al.batch', 'batch')
            ->innerJoin('batch.owner', 'batchOwner')
            ->where($qb->expr()->eq('al.polygon', ':polygon'))
            ->andWhere($qb->expr()->eq('batch.version', ':version'))
            ->andWhere($qb->expr()->eq('batchOwner.owner', ':owner'))
            ->orderBy('al.id', 'desc')
            ->setMaxResults(1);

        $qb
            ->setParameter(':polygon', $polygon)
            ->setParameter(':version', $version)
            ->setParameter(':owner', $user);

        if ($etmsactionlog) {
            $qb
                ->andWhere($qb->expr()->lt('al.id', ':actionlog_id'))
                ->setParameter(':actionlog_id', $etmsactionlog->getId());
        } else {
            $qb
                ->setFirstResult(2);
        }

        return $qb->getQuery()->getOneOrNullResult();
    }

    /**
     * @param \TeamSoft\CrmRepositoryBundle\Entity\Etms\InfoPolygon $polygon
     * @param \TeamSoft\CrmRepositoryBundle\Entity\Etms\InfoEtmsmapsversion $version
     * @param InfoUser $user
     * @param \TeamSoft\CrmRepositoryBundle\Entity\Etms\InfoEtmsactionlog $etmsactionlog
     *
     * @return \TeamSoft\CrmRepositoryBundle\Entity\Etms\InfoEtmsactionlog|null
     *
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function getNextByPolygon(\TeamSoft\CrmRepositoryBundle\Entity\Etms\InfoPolygon $polygon, \TeamSoft\CrmRepositoryBundle\Entity\Etms\InfoEtmsmapsversion $version, InfoUser $user, \TeamSoft\CrmRepositoryBundle\Entity\Etms\InfoEtmsactionlog $etmsactionlog)
    {
        $qb = $this->createQueryBuilder('al');
        $qb
            ->innerJoin('al.batch', 'batch')
            ->innerJoin('batch.owner', 'batchOwner')
            ->where($qb->expr()->eq('al.polygon', ':polygon'))
            ->andWhere($qb->expr()->eq('batch.version', ':version'))
            ->andWhere($qb->expr()->eq('batchOwner.owner', ':owner'))
            ->andWhere($qb->expr()->gt('al.id', ':actionlog_id'))
            ->orderBy('al.id', 'asc')
            ->setMaxResults(1);

        $qb
            ->setParameter(':polygon', $polygon)
            ->setParameter(':version', $version)
            ->setParameter(':owner', $user)
            ->setParameter(':actionlog_id', $etmsactionlog->getId());

        return $qb->getQuery()->getOneOrNullResult();
    }
}
