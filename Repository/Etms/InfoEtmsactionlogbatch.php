<?php

namespace TeamSoft\CrmRepositoryBundle\Repository\Etms;

use Doctrine\DBAL\Platforms\PostgreSqlPlatform;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Mapping;
use TeamSoft\CrmRepositoryBundle\Entity\InfoUser;

class InfoEtmsactionlogbatch extends EntityRepository
{
    private $dp;

    /**
     * InfoEtmsactionlogbatch constructor.
     * @param EntityManagerInterface $em
     * @param Mapping\ClassMetadata $class
     * @throws \Doctrine\DBAL\DBALException
     */
    public function __construct(EntityManagerInterface $em, Mapping\ClassMetadata $class)
    {
        parent::__construct($em, $class);
        $this->dp = $em->getConnection()->getDatabasePlatform();
    }

    /**
     * @param \TeamSoft\CrmRepositoryBundle\Entity\Etms\InfoEtmsactionlogbatch $batch
     * @return \TeamSoft\CrmRepositoryBundle\Entity\Etms\InfoEtmsactionlogbatch|null
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function getPrevBatch(\TeamSoft\CrmRepositoryBundle\Entity\Etms\InfoEtmsactionlogbatch $batch)
    {
        return $this->getPrevBatchInVersion($batch->getVersion(), $batch->getOwnerUser(), $batch);
    }

    /**
     * @param \TeamSoft\CrmRepositoryBundle\Entity\Etms\InfoEtmsmapsversion $version
     * @param InfoUser $user
     * @param \TeamSoft\CrmRepositoryBundle\Entity\Etms\InfoEtmsactionlogbatch|null $batch
     *
     * @return \TeamSoft\CrmRepositoryBundle\Entity\Etms\InfoEtmsactionlogbatch|null
     *
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function getPrevBatchInVersion(\TeamSoft\CrmRepositoryBundle\Entity\Etms\InfoEtmsmapsversion $version, InfoUser $user, \TeamSoft\CrmRepositoryBundle\Entity\Etms\InfoEtmsactionlogbatch $batch = null)
    {
        $qb = $this->createQueryBuilder('alb');
        $qb
            ->innerJoin('alb.owner', 'batchOwner')
            ->where($qb->expr()->eq('alb.version', ':version'))
            ->andWhere($qb->expr()->eq('batchOwner.owner', ':owner'))
            ->orderBy('alb.id', 'desc')
            ->setMaxResults(1);

        $qb
            ->setParameter(':version', $version)
            ->setParameter(':owner', $user);

        if ($batch) {
            $qb
                ->andWhere($qb->expr()->lt('alb.id', ':batch_id'))
                ->setParameter(':batch_id', $batch);
        } else {
            $qb
                ->setFirstResult(2);
        }

        return $qb->getQuery()->getOneOrNullResult();
    }

    /**
     * @param \TeamSoft\CrmRepositoryBundle\Entity\Etms\InfoEtmsactionlogbatch $batch
     * @return \TeamSoft\CrmRepositoryBundle\Entity\Etms\InfoEtmsactionlogbatch|null
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function getNextBatch(\TeamSoft\CrmRepositoryBundle\Entity\Etms\InfoEtmsactionlogbatch $batch)
    {
        return $this->getNextBatchInVersion($batch->getVersion(), $batch->getOwnerUser(), $batch);
    }

    /**
     * @param \TeamSoft\CrmRepositoryBundle\Entity\Etms\InfoEtmsmapsversion $version
     * @param InfoUser $user
     * @param \TeamSoft\CrmRepositoryBundle\Entity\Etms\InfoEtmsactionlogbatch $batch
     *
     * @return \TeamSoft\CrmRepositoryBundle\Entity\Etms\InfoEtmsactionlogbatch|null
     *
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function getNextBatchInVersion(\TeamSoft\CrmRepositoryBundle\Entity\Etms\InfoEtmsmapsversion $version, InfoUser $user, \TeamSoft\CrmRepositoryBundle\Entity\Etms\InfoEtmsactionlogbatch $batch)
    {
        $qb = $this->createQueryBuilder('alb');
        $qb
            ->innerJoin('alb.owner', 'batchOwner')
            ->where($qb->expr()->eq('alb.version', ':version'))
            ->andWhere($qb->expr()->eq('batchOwner', ':owner'))
            ->andWhere($qb->expr()->gt('alb.id', 'batch_id'))
            ->orderBy('alb.id', 'asc')
            ->setMaxResults(1);

        $qb
            ->setParameter(':version', $version)
            ->setParameter(':owner', $user)
            ->setParameter(':batch_id', $batch->getId());

        return $qb->getQuery()->getOneOrNullResult();
    }

    /**
     * @param \TeamSoft\CrmRepositoryBundle\Entity\Etms\InfoEtmsactionlogbatch $etmsactionlogbatch
     * @return array
     * @throws \Doctrine\DBAL\DBALException
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function revertBatch(\TeamSoft\CrmRepositoryBundle\Entity\Etms\InfoEtmsactionlogbatch $etmsactionlogbatch)
    {
        if ($this->dp instanceof PostgreSqlPlatform) {
            $updateSQL = "
            UPDATE info_polygonuser
            SET user_id = pu_prev.user_id
            FROM (
                SELECT 
                    alb.version_id, al.polygon_id, prev.user_id
                FROM info_etmsactionlogbatch alb
                INNER JOIN info_etmsactionlog al ON al.batch_id = alb.id
                INNER JOIN info_etmsactionlogprev prev ON prev.action_id = al.id
                WHERE alb.id = :actionLogId
            ) pu_prev
            WHERE info_polygonuser.version_id = pu_prev.version_id
              AND info_polygonuser.polygon_id = pu_prev.polygon_id;
            ";
        } else {
            $updateSQL = "
            UPDATE pu
            SET pu.user_id = prev.user_id
            FROM info_polygonuser pu
            INNER JOIN info_etmsactionlogbatch alb ON alb.version_id = pu.version_id
            INNER JOIN info_etmsactionlog al ON al.batch_id = alb.id AND al.polygon_id = pu.polygon_id
            INNER JOIN info_etmsactionlogprev prev ON prev.action_id = al.id
            WHERE alb.id = :actionLogId;
            ";
        }
        $this->_em->getConnection()->executeUpdate($updateSQL, [':actionLogId' => $etmsactionlogbatch->getId()]);

        $updateSQL = "
            INSERT INTO info_polygonuser (version_id, direction_id, polygon_id, user_id)
            SELECT
                alb.version_id, v.direction_id, al.polygon_id, prev.user_id 
            FROM info_etmsactionlogbatch alb
            INNER JOIN info_etmsmapsversion v ON v.id = alb.version_id
            INNER JOIN info_etmsactionlog al ON al.batch_id = alb.id
            INNER JOIN info_etmsactionlogprev prev ON prev.action_id = al.id
            LEFT JOIN info_polygonuser pu ON pu.version_id = alb.version_id AND pu.polygon_id = al.polygon_id
            WHERE pu.id IS NULL 
              AND alb.id = :actionLogId
              ";

        $this->_em->getConnection()->executeUpdate($updateSQL, [':actionLogId' => $etmsactionlogbatch->getId()]);
        $this->_em->flush();

        $etmsactionlogbatch->setCurrenttime(new \DateTime('now'));
        $this->_em->persist($etmsactionlogbatch);
        $this->_em->flush();

        $qb2 = $this->createQueryBuilder('alb');
        $qb2->innerJoin('alb.actionLogCollection', 'al')
            ->innerJoin('al.polygon', 'p')
            ->innerJoin('al.prev', 'prev')
            ->leftJoin('prev.user', 'user')
            ->select(['p.id as polygon_id', 'user.id as user_id'])
            ->where($qb2->expr()->eq('alb', ':actionLogBatch'))
            ->setParameter(':actionLogBatch', $etmsactionlogbatch);

        return $qb2->getQuery()->getArrayResult();
    }

    /**
     * @param \TeamSoft\CrmRepositoryBundle\Entity\Etms\InfoEtmsactionlogbatch $etmsactionlogbatch
     * @return array
     * @throws \Doctrine\DBAL\DBALException
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function applyBatch(\TeamSoft\CrmRepositoryBundle\Entity\Etms\InfoEtmsactionlogbatch $etmsactionlogbatch)
    {
        if ($this->dp instanceof PostgreSqlPlatform) {
            $updateSQL = "
            UPDATE info_polygonuser
            SET user_id = pu_next.user_id
            FROM (
                SELECT 
                    alb.version_id, al.polygon_id, next.user_id
                FROM info_etmsactionlogbatch alb
                INNER JOIN info_etmsactionlog al ON al.batch_id = alb.id
                LEFT JOIN info_etmsactionlognew next ON next.action_id = al.id
                WHERE alb.id = :actionLogId
            ) pu_next
            WHERE info_polygonuser.version_id = pu_next.version_id
              AND info_polygonuser.polygon_id = pu_next.polygon_id;
            ";
        } else {
            $updateSQL = "
            UPDATE pu
            SET pu.user_id = next.user_id
            FROM info_polygonuser pu
            INNER JOIN info_etmsactionlogbatch alb ON alb.version_id = pu.version_id
            INNER JOIN info_etmsactionlog al ON al.batch_id = alb.id AND al.polygon_id = pu.polygon_id
            LEFT JOIN info_etmsactionlognew next ON next.action_id = al.id
            WHERE alb.id = :actionLogId;
            ";
        }
        $this->_em->getConnection()->executeUpdate($updateSQL, [':actionLogId' => $etmsactionlogbatch->getId()]);

        $updateSQL = "
            INSERT INTO info_polygonuser (version_id, direction_id, polygon_id, user_id)
            SELECT
                alb.version_id, v.direction_id, al.polygon_id, next.user_id 
            FROM info_etmsactionlogbatch alb
            INNER JOIN info_etmsmapsversion v ON v.id = alb.version_id
            INNER JOIN info_etmsactionlog al ON al.batch_id = alb.id
            LEFT JOIN info_etmsactionlognew next ON next.action_id = al.id
            LEFT JOIN info_polygonuser pu ON pu.version_id = alb.version_id AND pu.polygon_id = al.polygon_id
            WHERE pu.id IS NULL 
              AND alb.id = :actionLogId
              ";

        $this->_em->getConnection()->executeUpdate($updateSQL, [':actionLogId' => $etmsactionlogbatch->getId()]);
        $this->_em->flush();

        $etmsactionlogbatch->setCurrenttime(new \DateTime('now'));
        $this->_em->persist($etmsactionlogbatch);
        $this->_em->flush();

        $qb2 = $this->createQueryBuilder('alb');
        $qb2->innerJoin('alb.actionLogCollection', 'al')
            ->innerJoin('al.polygon', 'p')
            ->innerJoin('al.new', 'next')
            ->leftJoin('next.user', 'user')
            ->select(['p.id as polygon_id', 'user.id as user_id'])
            ->where($qb2->expr()->eq('alb', ':actionLogBatch'))
            ->setParameter(':actionLogBatch', $etmsactionlogbatch);

        return $qb2->getQuery()->getArrayResult();
    }
}
