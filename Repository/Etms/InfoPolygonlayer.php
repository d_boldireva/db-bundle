<?php

namespace TeamSoft\CrmRepositoryBundle\Repository\Etms;

use Doctrine\ORM\EntityRepository;

class InfoPolygonlayer extends EntityRepository
{
    public const TAG_USER = 'user_layer';

    function findOneByDirection($directionId)
    {
        $layer = $this->createQueryBuilder('pl')
            ->join('pl.polygonLayerDirection', 'pld')
            ->where('pld.direction = :direction')
            ->setParameter('direction', $directionId)
            ->setMaxResults(1)
            ->getQuery()->getOneOrNullResult();

        return $layer ?: $this->findOneBy(['tag' => 'user_layer']);
    }

}