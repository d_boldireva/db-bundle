<?php

namespace TeamSoft\CrmRepositoryBundle\Repository\Etms;

use Doctrine\ORM\EntityRepository;
use TeamSoft\CrmRepositoryBundle\Entity;

class InfoEtmsmapsversion extends EntityRepository
{
    public function getPolygonsByVersion(InfoEtmsmapsversion $version)
    {
        /**
         * @var \TeamSoft\CrmRepositoryBundle\Entity\Etms\InfoPolygon[]
         */
        $polygons = [];

        /*
         * TODO: Get polygons by version
         */

        return $polygons;
    }

    public function initVersion(Entity\InfoDirection $direction)
    {

        /**
         * @var Entity\Etms\InfoPolygonlayer $layer
         */
        $layer = $this->_em->getRepository(Entity\Etms\InfoPolygonlayer::class)
            ->findOneByDirection($direction);

        /**
         * @var Entity\Etms\InfoEtmsmapsversion $version
         */
        $version = new Entity\Etms\InfoEtmsmapsversion();
        $version
            ->setDirection($direction)
            ->setCreator($this->getUser())
            ->setName($direction->getName() . ' ' . $layer->getName() . "(" . date("Y-m-d H:i:s") . ")");

        $this->_em->persist($version);
        $this->_em->refresh($version);


        /*
         * TODO: Init version
         */

        return $version;
    }

    public function getVersionByDirection(Entity\InfoDirection $direction)
    {
        /**
         * @var \TeamSoft\CrmRepositoryBundle\Entity\Etms\InfoEtmsmapsversion $version
         */
        $version = null;

        /*
         * TODO: Get active version by direction
         */

        return $version;
    }
}
