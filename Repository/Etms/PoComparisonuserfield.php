<?php

namespace TeamSoft\CrmRepositoryBundle\Repository\Etms;

use Doctrine\Common\Collections\Criteria;
use Doctrine\ORM\EntityRepository;
use TeamSoft\CrmRepositoryBundle\Entity;

class PoComparisonuserfield extends EntityRepository
{
    const FIELD_ENTITY_MAP = [
        'companyTypes' => Entity\InfoCompanytype::class,
        'companyCategories' => Entity\InfoCompanycategory::class,
        'contactTargetGroups' => Entity\InfoTarget::class,
        'contactSpecialization' => Entity\InfoDictionary::class,
        'contactCategory' => Entity\InfoContactcateg::class,
        'mssku' => Entity\InfoPreparation::class,
        'msskuByBrands' => Entity\InfoPreparationbrend::class,
        'target' => Entity\InfoTarget::class,
    ];

    public function findAllWithSubItems(Entity\InfoUser $user, $morionApiEnable)
    {
        $fieldsRepository = $this->_em->getRepository(Entity\Etms\PoComparisonfield::class);

        $criteria = new Criteria();
        $expr = $criteria->expr();
        $criteria->where($expr->eq('enable', true));
        if (!$morionApiEnable) {
            $criteria->andWhere($expr->notIn('name', ['mssku', 'msskuByBrands']));
        }
        $fieldsAll = $fieldsRepository->matching($criteria)->getValues();

        $fieldsUser = $this->findBy(['user' => $user, 'subitemId' => null]);

        $nodesList = [
            'companyTypes',
            'companyCategories',
            'contactTargetGroups',
            'contactSpecialization',
            'contactCategory',
            'mssku',
            'msskuByBrands',
            'target'
        ];

        $result = [];
        /** @var Entity\Etms\PoComparisonfield $field */
        foreach ($fieldsAll as $field) {
            $fieldName = $field->getName();
            $fieldChecked = new \ArrayObject();

            /** @var Entity\Etms\PoComparisonuserfield $fieldUser */
            foreach ($fieldsUser as $fieldUser) {
                if ($fieldUser->getField() === $field) {
                    if (empty($direction = $fieldUser->getDirection())) {
                        continue;
                    }
                    $fieldChecked[$direction->getId()] = !!$fieldUser->getChecked();
                }
            }

            $isNode = in_array($fieldName, $nodesList);

            $result[] = [
                'name' => $fieldName,
                'isNode' => $isNode,
                'checked' => $fieldChecked,
                'children' => $isNode ? $this->getSubItems($field, $user) : [],
                'isHistogram' => $field->getIsHistogram(),
            ];
        }
        return $result;
    }

    /**
     * @param $field Entity\Etms\PoComparisonfield
     * @param $user Entity\InfoUser
     * @return array
     */
    private function getSubItems($field, $user)
    {
        $fieldName = $field->getName();

        $userFieldsSub = $this->createQueryBuilder('cuf')
            ->where('cuf.subitemId IS NOT NULL')
            ->andWhere('cuf.field=' . $field->getId())
            ->andWhere('cuf.user=' . $user->getId())
            ->getQuery()->getResult();

        $result = [];
        $subItems = $this->findAllSubItemsByFieldName($fieldName, $user);
        foreach ($subItems as $subItem) {
            $subResult = [
                'id' => $subItem->getId(),
                'name' => $subItem->getName(),
                'checked' => new \ArrayObject(),
            ];

            if (method_exists($subItem, 'getDirection')) {
                /** @var Entity\InfoDirection|null $direction */
                $direction = $subItem->getDirection();
                $subResult['direction'] = $direction ? $direction->getId() : null;
            }

            if (method_exists($subItem, 'getDirectionCollection')) {
                $subResult['directions'] = $subItem->getDirectionCollection()->map(function ($direction) {
                    /** @var Entity\InfoDirection $direction */
                    return $direction->getId();
                })->toArray();
            }

            if (method_exists($subItem, 'getMorionId')) {
                $subResult['morionId'] = $subItem->getMorionId();
            }

            if (method_exists($subItem, 'getBrend') && $subBrand = $subItem->getBrend()) {
                $subResult['brand'] = [
                    'id' => $subBrand->getId(),
                    'name' => $subBrand->getName(),
                ];
            }

            /** @var Entity\Etms\PoComparisonuserfield $item */
            foreach ($userFieldsSub as $item) {
                $direction = $item->getDirection();
                if (empty($direction)) {
                    continue;
                }
                if ($subItem->getId() == $item->getSubitemId()) {
                    $subResult['checked'][$direction->getId()] = !!$item->getChecked();
                }
            }

            $result[] = $subResult;
        }

        return $result;
    }

    private function findAllSubItemsByFieldName($fieldName, Entity\InfoUser $user)
    {

        $criteria = new Criteria();
        $criteria->orderBy(['name' => 'ASC']);
        $expr = $criteria->expr();
        if ($fieldName == 'companyTypes') {
            $criteria->where($expr->eq('geodataenable', 1));
        }
        if ($fieldName == 'contactSpecialization') {
            $criteria->where($expr->eq('identifier', Entity\InfoDictionary::CONTACT_SPECIALIZATION));
        }
        if ($fieldName == 'mssku') {
            $criteria->where($expr->neq('morionId', null));
        }

        $subItemRepo = $this->_em->getRepository(self::FIELD_ENTITY_MAP[$fieldName]);
        $subItemsResult = $subItemRepo->matching($criteria);

        if ($fieldName == 'mssku') {
            // info_preparation prefetch
            $subItemRepo->createQueryBuilder('p')
                ->addCriteria($criteria)
                ->getQuery()->getResult();

            // info_preparationdirection re-hydration
            $subItemRepo->createQueryBuilder('p')
                ->select('partial p.{id}', 'd')
                ->leftJoin('p.directionCollection', 'd')
                ->addCriteria($criteria)
                ->getQuery()->getResult();

            // info_preparationbrend re-hydration
            $subItemRepo->createQueryBuilder('p')
                ->select('partial p.{id}', 'b')
                ->leftJoin('p.brend', 'b')
                ->addCriteria($criteria)
                ->getQuery()->getResult();

            // todo: rewrite to query builder
            $userDirections = $user->getDirectionCollection();
            $subItemsResult = $subItemsResult->filter(function ($preparation) use ($userDirections) {
                /** @var Entity\InfoPreparation $preparation */
                return $preparation->getDirectionCollection()->exists(function ($key, $direction) use ($userDirections) {
                    return $userDirections->contains($direction);
                });
            });
        }
        return $subItemsResult->getValues();
    }

    public function save($fields, $user, $directionId)
    {
        $fieldsRepo = $this->_em->getRepository(Entity\Etms\PoComparisonfield::class);
        $directionRepo = $this->_em->getRepository(Entity\InfoDirection::class);
        $direction = $directionRepo->find($directionId);

        foreach ($fields as $field) {
            $fieldEntity = $fieldsRepo->findOneBy(['name' => $field['name']]);

            $userField = $this->findOneBy(['user' => $user, 'field' => $fieldEntity, 'subitemId' => null, 'direction' => $direction]);
            if (!$userField) {
                $userField = new Entity\Etms\PoComparisonuserfield();
                $userField->setField($fieldEntity);
                $userField->setUser($user);
                $userField->setDirection($direction);
            }
            $userField->setChecked((int)@$field['checked'][$directionId]);
            $this->_em->persist($userField);

            foreach ($field['children'] as $child) {
                $userField = $this->findOneBy(['user' => $user, 'field' => $fieldEntity, 'subitemId' => $child['id'], 'direction' => $direction]);
                if (!$userField) {
                    $userField = new Entity\Etms\PoComparisonuserfield();
                    $userField->setField($fieldEntity);
                    $userField->setUser($user);
                    $userField->setSubitemId($child['id']);
                    $userField->setDirection($direction);
                }
                $userField->setChecked((int)@$child['checked'][$directionId]);
                $this->_em->persist($userField);
            }
        }
        $this->_em->flush();
    }
}
