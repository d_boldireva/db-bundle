<?php

namespace TeamSoft\CrmRepositoryBundle\Repository\Etms;

use Doctrine\ORM\EntityRepository;
use TeamSoft\CrmRepositoryBundle\Entity\InfoUser;

/**
 * Class InfoPolygonuserbalancing
 * @package TeamSoft\CrmRepositoryBundle\Repository\Etms
 */
class InfoPolygonuserbalancing extends EntityRepository
{
    /**
     * Get polygon users balancing
     *
     * @param string $layer_tag
     * @param int $version_id
     * @param int $balancing_id
     * @param int[] $region_ids
     * @param InfoUser|null $supervisor
     * @return array
     */
    public function getPolygonUserBalancing(string $layer_tag, int $version_id, int $balancing_id, $region_ids = [], InfoUser $supervisor = null)
    {
        $qb = $this->createQueryBuilder('pu')
            ->select('p.id as polygon_id, u.id as user_id')
            ->innerJoin('pu.balancing', 'b')
            ->innerJoin('pu.polygon', 'p')
            ->innerJoin('p.polygonLayer', 'pl')
            ->innerJoin('pu.user', 'u')
            ->where('pl.tag = :tag')
            ->andWhere('b.version = :version')
            ->andWhere('IsNull(p.iswrongpolygon,0) = 0')
            ->andWhere('b = :balancing')
            ->setParameter(':tag', $layer_tag)
            ->setParameter(':version', $version_id)
            ->setParameter(':balancing', $balancing_id);

        if ($supervisor) {
            $qb->andWhere('pu.user IN (:users)')
                ->setParameter(':users', array_merge([$supervisor], $supervisor->getSubordinates()->toArray()));
        }

        if ($region_ids) {
            $qb->andWhere('p.region IN (:regions)')
                ->setParameter(':regions', $region_ids);
        }

        $result = $qb->getQuery()->getResult();

        $mappedResult = [];

        foreach ($result as $row) {
            $mappedResult[$row['polygon_id']] = $row['user_id'];
        }

        return $mappedResult;
    }

    /**
     * Get last modified polygonuser
     *
     * @param string $layer_tag
     * @param int $version_id
     * @param int[] $region_ids
     * @param InfoUser|null $supervisor
     * @return \DateTime|null
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function getLastModified(string $layer_tag, int $version_id, int $balancing_id, $region_ids = [], InfoUser $supervisor = null)
    {
        $qb = $this->createQueryBuilder('pu')
            ->select('MAX(pu.currenttime) as currenttime')
            ->innerJoin('pu.balancing', 'b')
            ->innerJoin('pu.polygon', 'p')
            ->innerJoin('p.polygonLayer', 'pl')
            ->where('pl.tag = :tag')
            ->andWhere('b.version = :version')
            ->andWhere('b = :balancing')
            ->setParameter(':tag', $layer_tag)
            ->setParameter(':version', $version_id)
            ->setParameter(':balancing', $balancing_id);

        if ($supervisor) {
            $qb->andWhere('pu.user IN (:users)')
                ->setParameter(':users', array_merge([$supervisor], $supervisor->getSubordinates()->toArray()));
        }

        if ($region_ids) {
            $qb->andWhere('p.region IN (:regions)')
                ->setParameter(':regions', $region_ids);
        }

        $result = $qb->getQuery()->getOneOrNullResult();

        if ($result) {
            $result = new \DateTime($result['currenttime']);
        }

        return $result;
    }

    /**
     * Get polygonuser etag
     *
     * @param string $layer_tag
     * @param int $version_id
     * @param int $balancing_id
     * @param int[] $region_ids
     * @param InfoUser|null $supervisor
     * @return string|null
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function getEtag(string $layer_tag, int $version_id, int $balancing_id, $region_ids = [], InfoUser $supervisor = null)
    {
        $qb = $this->createQueryBuilder('pu')
            ->select('MAX(pu.currenttime) as currenttime, COUNT(pu.id) as count')
            ->innerJoin('pu.balancing', 'b')
            ->innerJoin('pu.polygon', 'p')
            ->innerJoin('p.polygonLayer', 'pl')
            ->where('pl.tag = :tag')
            ->andWhere('b.version = :version')
            ->andWhere('b = :balancing')
            ->setParameter(':tag', $layer_tag)
            ->setParameter(':version', $version_id)
            ->setParameter(':balancing', $balancing_id);

        if ($supervisor) {
            $qb->andWhere('pu.user IN (:users)')
                ->setParameter(':users', array_merge([$supervisor], $supervisor->getSubordinates()->toArray()));
        }

        if ($region_ids) {
            $qb->andWhere('p.region IN (:regions)')
                ->setParameter(':regions', $region_ids);
        }

        $result = $qb->getQuery()->getOneOrNullResult();

        if ($result) {
            $result = md5(json_encode($result));
        }

        return $result;
    }
}
