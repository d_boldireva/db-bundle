<?php

namespace TeamSoft\CrmRepositoryBundle\Repository;

use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use TeamSoft\CrmRepositoryBundle\Service\DateTimeWithGmtOffset;
use TeamSoft\CrmRepositoryBundle\Service\Options;
use TeamSoft\CrmRepositoryBundle\Entity\InfoAction as InfoActionEntity;
use TeamSoft\CrmRepositoryBundle\Entity\InfoActionparamtext;


class InfoAction extends ServiceEntityRepository
{
    private $options;
    private $dateTimeWithGmtOffset;

    public function __construct(ManagerRegistry $registry, Options $options, DateTimeWithGmtOffset $dateTimeWithGmtOffset)
    {
        parent::__construct($registry, InfoActionEntity::class);
        $this->options = $options;
        $this->dateTimeWithGmtOffset = $dateTimeWithGmtOffset;
    }

    public function findByResponsibleAndDate($responsibleId, \DateTime $date) {
        $dateTill = clone $date;
        $dateTill->add(\DateInterval::createFromDateString('86399 second'));
        $clientGmtOffset = $this->dateTimeWithGmtOffset->getClientGmtOffset() ?? 0;
        $dateFromExpression = "DateTimeWithGmtOffset(a.datefrom, a.gmtOffset, $clientGmtOffset)";
        $dateTillExpression = "DateTimeWithGmtOffset(a.datetill, a.gmtOffset, $clientGmtOffset)";
        $query = $this->createQueryBuilder('a')
            ->where('a.responsible = :responsibleId')
            ->setParameter(':responsibleId', $responsibleId)
            ->andWhere("(($dateFromExpression BETWEEN :dateFrom AND :dateTill) OR ($dateTillExpression BETWEEN :dateFrom1 AND :dateTill1) OR (:dateFrom2 BETWEEN $dateFromExpression AND $dateTillExpression))")
            ->setParameter(':dateFrom', $date)
            ->setParameter(':dateFrom1', $date)
            ->setParameter(':dateFrom2', $date)
            ->setParameter(':dateTill', $dateTill)
            ->setParameter(':dateTill1', $dateTill)
            ->andWhere('a.datetill > :dateFrom3')
            ->setParameter(':dateFrom3', $date)
            ->getQuery();
        return $query->getResult();
    }

    public function findByResponsibleAndPeriod($responsibleId, \DateTime $dateFrom, \DateTime $dateTill) {
        $dateTill->add(\DateInterval::createFromDateString('86399 second'));
        $qb = $this->createQueryBuilder('a');
        if ($this->options->get('useDevelopmentMap')) {
            $subQueryParamTexts = $this->getEntityManager()
                ->getRepository(InfoActionparamtext::class)
                ->createQueryBuilder('t');

            $subQueryParamTexts
                ->andWhere($subQueryParamTexts->expr()->eq('t.responsible', $responsibleId))
                ->andWhere($subQueryParamTexts->expr()->eq('t.action', 'a.id'))
                ->setMaxResults(1);

            $qb
                ->andWhere($qb->expr()->orX(
                    $qb->expr()->eq('a.responsible', $responsibleId),
                    $qb->expr()->eq('a.responsible2', $responsibleId),
                    $qb->expr()->exists($subQueryParamTexts)
                ));
        } else {
            $qb
                ->andWhere('a.responsible = :responsibleId')
                ->setParameter(':responsibleId', $responsibleId);
        }

        $clientGmtOffset = $this->dateTimeWithGmtOffset->getClientGmtOffset() ?? 0;
        $dateFromExpression = "DateTimeWithGmtOffset(a.datefrom, a.gmtOffset, $clientGmtOffset)";
        $dateTillExpression = "DateTimeWithGmtOffset(a.datetill, a.gmtOffset, $clientGmtOffset)";
        $qb
            ->andWhere("(($dateFromExpression BETWEEN :dateFrom AND :dateTill) OR ($dateTillExpression BETWEEN :dateFrom1 AND :dateTill1) OR ((:dateFrom2 BETWEEN $dateFromExpression AND $dateTillExpression) AND (:dateTill2 BETWEEN $dateFromExpression AND $dateTillExpression)))")
            ->setParameter(':dateFrom', $dateFrom)
            ->setParameter(':dateFrom1', $dateFrom)
            ->setParameter(':dateFrom2', $dateFrom)
            ->setParameter(':dateTill', $dateTill)
            ->setParameter(':dateTill1', $dateTill)
            ->setParameter(':dateTill2', $dateTill)
            ->andWhere('a.datetill > :dateFrom3')
            ->setParameter(':dateFrom3', $dateFrom);

        $query = $qb->getQuery();
        return $query->getResult();
    }
}
