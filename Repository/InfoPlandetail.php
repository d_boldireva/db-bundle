<?php

namespace TeamSoft\CrmRepositoryBundle\Repository;

use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Query\ResultSetMappingBuilder;
use Doctrine\Persistence\ManagerRegistry;
use TeamSoft\CrmRepositoryBundle\Service\DatabasePlatform;
use TeamSoft\CrmRepositoryBundle\Entity\InfoPlandetail as InfoPlandetailModel;

class InfoPlandetail extends ServiceEntityRepository
{
    public const TYPE_COPY_BRAND = 'preparation';
    public const TYPE_COPY_CLM = 'presentation';
    public const TYPE_COPY_PROMO = 'promo';

    public function __construct(ManagerRegistry $registry, DatabasePlatform $dp)
    {
        parent::__construct($registry, \TeamSoft\CrmRepositoryBundle\Entity\InfoPlandetail::class);
        $this->dp = $dp;
    }

    /**
     * @param $id
     * @return array
     */
    public function findPlanById($id)
    {
        $tableName = $this->getClassMetadata()->getTableName();

        $rsm = new ResultSetMappingBuilder($this->_em);
        $rsm->addRootEntityFromClassMetadata($this->_entityName, $tableName, array(), ResultSetMappingBuilder::COLUMN_RENAMING_INCREMENT);
        $rsm->addScalarResult('_percent', 'percent', 'string');

        $selectClause = $rsm->generateSelectClause();
        $c = $this->dp->getConcatOperator();
        $sql = "
            select $selectClause, _percent
            from $tableName
            left join info_plan on info_plandetail.plan_id = info_plan.id
            left join info_tasktype on info_plandetail.tasktype_id = info_tasktype.id
            left join info_target on info_plandetail.target_id = info_target.id
            left join (
            select pd.id,
            CAST((pd.TaskCount * tt.Promocount * 100 / pt.total) AS VARCHAR) $c '% " .
            "(' $c CAST(pd.TaskCount * tt.Promocount AS VARCHAR ) $c '/' $c CAST(pt.total AS VARCHAR) $c ')' as _percent
            FROM info_plandetail pd
            LEFT JOIN info_tasktype tt ON pd.tasktype_id = tt.id
            LEFT JOIN (select
            plan_id,
            SUM(pd.TaskCount * tt.Promocount) as total
            FROM info_plandetail pd
            LEFT JOIN info_tasktype tt ON pd.tasktype_id = tt.id
            WHERE plan_id = $id AND pd.TaskCount * tt.Promocount <> 0
            GROUP BY plan_id) as pt ON pt.plan_id = pd.plan_id
            WHERE pd.plan_id = $id
            ) AS prc ON prc.id = info_plandetail.id
            where plan_id = $id
        ";
        $query = $this->getEntityManager()->createNativeQuery($sql, $rsm);

        $mixedResult = $query->getResult();

        $result = [];

        foreach ($mixedResult as $mixedResultItem) {
            /**
             * @var $planEntity \TeamSoft\CrmRepositoryBundle\Entity\InfoPlandetail
             */
            $planEntity = $mixedResultItem[0];
            $planEntity->setPercent($mixedResultItem['percent']);
            $result[] = $planEntity;
        }

        return $result;
    }

    /**
     * Remove details from $toPlanDetail and clone details from $fromPlanDetail to $toPlanDetail
     *
     * @param InfoPlandetailModel $fromPlanDetail
     * @param InfoPlandetailModel $toPlanDetail
     * @param string[] $parts
     * @throws \Throwable
     */
    public function rewriteDetails(InfoPlandetailModel $fromPlanDetail, InfoPlandetailModel $toPlanDetail, array $parts): void
    {
        $rewriteDetail = function (Collection $fromCollection, Collection $toCollection) use($toPlanDetail) {
            foreach ($toCollection as $removeModel) {
                $this->getEntityManager()->remove($removeModel);
            }
            $toCollection->clear();

            foreach ($fromCollection as $fromModel) {
                /**
                 * @var \TeamSoft\CrmRepositoryBundle\Entity\InfoPlanpreparation|\TeamSoft\CrmRepositoryBundle\Entity\InfoPlanpresentation|\TeamSoft\CrmRepositoryBundle\Entity\InfoPlanpromo $model
                 */
                $model = clone $fromModel;
                $model->setModuser(null);
                $model->setPlandetail($toPlanDetail);

                $toCollection->add($model);

                $this->getEntityManager()->persist($model);
            }
        };

        $this->getEntityManager()->getConnection()->beginTransaction();
        try {
            $parts = array_unique($parts);
            foreach ($parts as $part) {
                if ($part === self::TYPE_COPY_BRAND) {
                    $rewriteDetail($fromPlanDetail->getPlanPreparations(), $toPlanDetail->getPlanPreparations());
                } elseif ($part === self::TYPE_COPY_PROMO) {
                    $rewriteDetail($fromPlanDetail->getPlanPromos(), $toPlanDetail->getPlanPromos());
                } elseif ($part === self::TYPE_COPY_CLM) {
                    $rewriteDetail($fromPlanDetail->getPlanPresentations(), $toPlanDetail->getPlanPresentations());
                }
            }
            $this->getEntityManager()->flush();
            $this->getEntityManager()->getConnection()->commit();
        } catch (\Throwable $exception) {
            $this->getEntityManager()->getConnection()->rollBack();
            throw $exception;
        }
    }
}
