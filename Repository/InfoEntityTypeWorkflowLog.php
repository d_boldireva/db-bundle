<?php
namespace TeamSoft\CrmRepositoryBundle\Repository;

use Doctrine\Persistence\ManagerRegistry;
use TeamSoft\CrmRepositoryBundle\Entity\InfoEntityTypeWorkflowLog as InfoEntityTypeWorkflowLogEntity;
use TeamSoft\CrmRepositoryBundle\Entity\InfoUser;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;

class InfoEntityTypeWorkflowLog extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, InfoEntityTypeWorkflowLogEntity::class);
    }

    /**
     * @param InfoUser $user
     * @return \TeamSoft\CrmRepositoryBundle\Entity\InfoEntityTypeWorkflowLog[]
     */
    public function findByUser(InfoUser $user, bool $isSuperAdmin = false): array
    {
        $qb = $this->createQueryBuilder('wl');

        $qb->select("wl.id, a.id as action_id, r.name as responsible, at.name as action_type, coalesce(s.name, '') as submission, a.datefrom, a.datetill")
            ->distinct()
            ->join('wl.workflow', 'w')
            ->join('wl.action', 'a')
            ->join('a.responsible', 'r')
            ->join('a.actiontype', 'at')
            ->leftJoin('r.submission', 's')
            ->where('wl.status is null');

        if (!$isSuperAdmin) {
            $qb->andWhere($qb->expr()->orX(
                $qb->expr()->eq('wl.user', $user->getId()),
                $qb->expr()->eq('w.role', $user->getRole()->getId())
            ));
        }

        return $qb->getQuery()->getResult();
    }

}
