<?php

namespace TeamSoft\CrmRepositoryBundle\Repository;

use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use TeamSoft\CrmRepositoryBundle\Service\DatabasePlatform;

class InfoSaleperiod extends ServiceEntityRepository
{

    private $dp;

    public function __construct(ManagerRegistry $registry, DatabasePlatform $dp)
    {
        parent::__construct($registry, \TeamSoft\CrmRepositoryBundle\Entity\InfoSaleperiod::class);
        $this->dp = $dp;
    }

    public function findByFilter(array $filter, ?array $orderBy = null, $limit = null, $offset = null)
    {

        $c = $this->dp->getConcatOperator();
        $like = $this->dp->getLikeExpression();

        $whereParts = [];
        if (isset($filter['search']) && $filter['search']) {
            $dateFromExpression = $this->dp->getDateFormatExpression("COALESCE(sp.datefrom, '')", 'yyyy-MM-dd');
            $dateTillExpression = $this->dp->getDateFormatExpression("COALESCE(sp.datetill, '')", 'yyyy-MM-dd');
            $whereParts[] = "$dateFromExpression $c $dateTillExpression $like '%" . $filter['search'] . "%'";
        }
        if (isset($filter['is_discount'])) {
            $whereParts[] = "COALESCE(sp.is_discount, 0) = " . $filter['is_discount'];
        }

        $orderByParts = [];
        if ($orderBy) {
            foreach ($orderBy as $fieldName => $order) {
                $order = strtoupper(trim($order));
                if ($order === "ASC" || $order === "DESC") {
                    $fieldName = strtolower($fieldName);
                    if ($fieldName === "datefrom") {
                        $orderByParts[] = "sp.datefrom " . $order;
                    } else if ($fieldName === "datetill") {
                        $orderByParts[] = "sp.datetill " . $order;
                    }
                }
            }
        }

        $rsm = $this->createResultSetMappingBuilder('sp');

        $select = 'SELECT ' . $rsm->generateSelectClause();
        $from = 'FROM info_saleperiod sp';
        $where = $whereParts ? 'WHERE ' . implode(' AND ', $whereParts) : '';
        $orderBy = $orderByParts ? 'ORDER BY ' . implode(', ', $orderByParts) : '';

        $sql = "$select $from $where $orderBy";
        $sql = $this->_em->getConnection()->getDatabasePlatform()->modifyLimitQuery($sql, $limit, $offset);
        $query = $this->getEntityManager()->createNativeQuery($sql, $rsm);

        return $query->getResult();
    }

    public function findByRangePeriod(array $filter, ?array $orderBy = null, $limit = null, $offset = null)
    {
        $whereParts = [];
        if (isset($filter['id'])) {
            $whereParts[] = "sp.id = '{$filter['id']}'";
        }
        if (isset($filter['date_from'])) {
            $dateEnd = new \DateTime($filter['date_from']);
            $dateEnd = $dateEnd->modify('+6 months')->format('Y-m-d');
            $whereParts[] = "sp.datefrom >= '{$filter['date_from']}' AND sp.datefrom <= '{$dateEnd}'";
        }
        if (isset($filter['is_for_granual'])) {
            $whereParts[] = "sp.is_for_granual = {$filter['is_for_granual']}";
        }

        $orderByParts = [];
        if ($orderBy) {
            foreach ($orderBy as $fieldName => $order) {
                $order = strtoupper(trim($order));
                if ($order === "ASC" || $order === "DESC") {
                    $fieldName = strtolower($fieldName);
                    if ($fieldName === "datefrom") {
                        $orderByParts[] = "sp.datefrom " . $order;
                    } else if ($fieldName === "datetill") {
                        $orderByParts[] = "sp.datetill " . $order;
                    }
                }
            }
        }

        $rsm = $this->createResultSetMappingBuilder('sp');

        $select = 'SELECT ' . $rsm->generateSelectClause();
        $from = 'FROM info_saleperiod sp';
        $where = $whereParts ? 'WHERE ' . implode(' AND ', $whereParts) : '';
        $orderBy = $orderByParts ? 'ORDER BY ' . implode(', ', $orderByParts) : '';

        $sql = "$select $from $where $orderBy";
        $sql = $this->_em->getConnection()->getDatabasePlatform()->modifyLimitQuery($sql, $limit, $offset);
        $query = $this->getEntityManager()->createNativeQuery($sql, $rsm);

        return $query->getResult();
    }
}
