<?php
declare(strict_types=1);

namespace TeamSoft\CrmRepositoryBundle\Repository;

use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use TeamSoft\CrmRepositoryBundle\Service\DatabasePlatform;
use TeamSoft\CrmRepositoryBundle\Entity\InfoCurrency as InfoCurrencyEntity;
use Doctrine\ORM\Query\ResultSetMappingBuilder;

class InfoCurrency extends ServiceEntityRepository
{
    private $dp;

    public function __construct(ManagerRegistry $registry, DatabasePlatform $dp)
    {
        parent::__construct($registry, InfoCurrencyEntity::class);
        $this->dp = $dp;
    }

    public function findByUserId(int $id)
    {

        $rsm = new ResultSetMappingBuilder($this->_em);
        $rsm->addRootEntityFromClassMetadata($this->_entityName, 'c', [], ResultSetMappingBuilder::COLUMN_RENAMING_INCREMENT);

        $selectClause = $rsm->generateSelectClause();

        $sql = "select {$selectClause} from (select ic.* from info_currency ic 
            inner join info_country icr on icr.currency_id = ic.id
            inner join info_user iu on iu.country_id = icr.id
            where iu.id = {$id}
            union all
            select ic.* from info_currency ic) as c";

        $sql = $this->_em->getConnection()->getDatabasePlatform()->modifyLimitQuery($sql, 1);

        $query = $this->_em->createNativeQuery($sql, $rsm);

        return $query->getOneOrNullResult();
    }
}