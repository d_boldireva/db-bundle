<?php
declare(strict_types=1);

namespace TeamSoft\CrmRepositoryBundle\Repository;

use Doctrine\ORM\EntityRepository;

class InfoClmviewlog extends EntityRepository
{

    /**
     * @param int $id
     * @return int|mixed|string
     * @throws \Doctrine\ORM\NoResultException
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function getCountByClmId(int $id)
    {
        return $this->createQueryBuilder('cvl')
            ->select('count(cvl.id)')
            ->where('cvl.task IS NOT NULL')
            ->andWhere('cvl.clm = ' . $id)
            ->getQuery()
            ->getSingleScalarResult();
    }

} 