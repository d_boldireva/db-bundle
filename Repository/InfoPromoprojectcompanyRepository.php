<?php

namespace TeamSoft\CrmRepositoryBundle\Repository;

use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use TeamSoft\CrmRepositoryBundle\Entity\InfoCompany;
use TeamSoft\CrmRepositoryBundle\Service\DatabasePlatform;

class InfoPromoprojectcompanyRepository extends ServiceEntityRepository
{
    private $dp;

    public function __construct(ManagerRegistry $registry, DatabasePlatform $dp)
    {
        parent::__construct($registry, \TeamSoft\CrmRepositoryBundle\Entity\InfoPromoprojectcompany::class);
        $this->dp = $dp;
    }

    public function findTargetPracticesByFilter($filter)
    {
        $c = $this->dp->getConcatOperator();

        $contactNameString = "CONCAT(ct.firstname , ' ' ,  ct.middlename  , ' ' , ct.lastname)";
        $sql2007contactedString = "(cp.name $c ' ' $c ct.firstname $c ' ' $c  ct.middlename  $c ' ' $c ct.lastname)";
        if (!empty($filter['promoproject_id'])) {
            return $query = $this->getEntityManager()->createQueryBuilder()
                ->addSelect("$contactNameString as contact_name, ct.id as contact_id")
                ->addSelect('cp.name AS company_name , cp.id AS company_id')
                ->from($this->_entityName, 'prc')
                ->innerJoin('prc.company', 'cp')
                ->leftJoin('prc.contact', 'ct')
                ->andWhere('prc.promoproject = :promoproject_id')->setParameter('promoproject_id', $filter['promoproject_id'])
                ->setMaxResults(100)
                ->distinct()->getQuery()->getArrayResult();
        }

        $query = $this->getEntityManager()->createQueryBuilder()
            ->addSelect("$contactNameString as contact_name, ct.id as contact_id")
            ->addSelect('cp.name AS company_name , cp.id AS company_id')
            ->from(InfoCompany::class, 'cp')
            ->leftJoin('cp.contactCollection', 'ct');


        if (!empty($filter['limit'])) {
            $query->setMaxResults($filter['limit']);
        }

        if (!empty($filter['search'])) {
            $query->andWhere("ct.firstname LIKE :name")->setParameter('name', '%' . $filter['search'] . '%');
        }


        if (!empty($filter['practiceno'])) {
            $query->andWhere('cp.practiceno in  ( :practicenos ) OR ct.practiceno in  ( :practicenos )')->setParameter('practicenos', $filter['practiceno']);
        }

        $practices = $query->distinct()->getQuery()->getArrayResult();

        if (!empty($filter['promoproject_id'])) {
            return $practices;
        }

        $mainPracticesCount = !empty($filter['limit']) ? max(0, 100 - count($practices)) : null;

        $tempPractices = $this->getTempPractices($filter, $mainPracticesCount, $sql2007contactedString);
        return array_merge($practices, $tempPractices);
    }


    private function getTempPractices($filter, $mainPracticesCount, $sql2007contactedString)
    {

        $conn = $this->getEntityManager()->getConnection();

        $where = "";
        if (!empty($filter['search'])) {
            $where = "where $sql2007contactedString LIKE " . "'%" . $filter['search'] . "%'";
        }

        if (!empty($filter['practiceno'])) {
            $practicenos = implode(',', $filter['practiceno']);
            $where = "where ct.practiceno in ( $practicenos ) or cp.practiceno in ( $practicenos )";
        }

        $where .= ' and ct.id  IS NOT NULL';

        $c = $this->dp->getConcatOperator();
        $sql = "SELECT DISTINCT (ct.FirstName $c ' ' $c ct.MiddleName  $c ' '  $c ct.LastName) AS contact_name, 
NULL AS id,
cp.name AS company_name,
ct.id AS contact_id,
cp.id AS company_id
FROM info_organisation_active_inactive_practice_numbers t
        INNER JOIN info_person_organisation_link po ON po.orgcode = t.orgcode
        LEFT JOIN info_contact ct  ON ct.morionid = " . $this->dp->getIsNumericExpression('po.personcode') . "
        INNER JOIN info_company cp ON cp.id = ct.company_id
        LEFT OUTER JOIN info_city ON info_city.id = cp.city_id
        LEFT OUTER JOIN info_companytype ON info_companytype.id = cp.companytype_id
        LEFT OUTER JOIN info_dictionary AS streetType ON streettype.id = cp.streettype_id
        $where";

        if ($mainPracticesCount) {
            $sql = $this->dp->modifyOnlyLimitQuery($sql, $mainPracticesCount);
        }

        return $conn->executeQuery($sql)->fetchAllAssociative();
    }
}
