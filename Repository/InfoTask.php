<?php

namespace TeamSoft\CrmRepositoryBundle\Repository;

use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use Doctrine\ORM\QueryBuilder;
use Doctrine\ORM\Query\ResultSetMappingBuilder;
use TeamSoft\CrmRepositoryBundle\Entity\InfoTask as InfoTaskEntity;
use TeamSoft\CrmRepositoryBundle\Entity\InfoTaskfilegroup;
use TeamSoft\CrmRepositoryBundle\Entity\VwTaskfileimage;
use TeamSoft\CrmRepositoryBundle\Entity\InfoPlantaskperiod;
use TeamSoft\CrmRepositoryBundle\Service\DateTimeWithGmtOffset;
use TeamSoft\CrmRepositoryBundle\Service\Options;

class InfoTask extends ServiceEntityRepository
{

    private $options;
    private $dateTimeWithGmtOffset;

    public function __construct(
        ManagerRegistry $registry,
        Options $options,
        DateTimeWithGmtOffset $dateTimeWithGmtOffset
    )
    {
        parent::__construct($registry, InfoTaskEntity::class);
        $this->options = $options;
        $this->dateTimeWithGmtOffset = $dateTimeWithGmtOffset;
    }

    public function findByFilter(array $filter, ?array $orderBy = null, ?int $limit = null)
    {
        $queryBuilder = $this->createQueryBuilder('t');
        $this->applyFilterToQueryBuilder($queryBuilder, $filter);

        if ($orderBy) {
            foreach ($orderBy as $attribute => $direction) {
                $queryBuilder->addOrderBy("t.{$attribute}", $direction);
            }
        } else {
            $queryBuilder->orderBy('t.datetill');
        }

        if ($limit) {
            $queryBuilder->setMaxResults($limit);
        }

        return $queryBuilder->getQuery()->getResult();
    }

    public function findById($id)
    {
        $queryBuilder = $this->createQueryBuilder('t');
        $taskNumEditOption = $this->options->get('TaskNumEdit');
        $usePlanTaskPeriodOption = $this->options->get('usePlanTaskPeriod');
        $hideTaskNumOption = $this->options->get('hideTaskNum');
        if ($hideTaskNumOption) {
            $queryBuilder->addSelect("'' as taskNum");
        } else if ($usePlanTaskPeriodOption) {
            $queryBuilder->addSelect('(' . $this->getTaskPeriodNumQuery() . ') as taskNum');
        } else if ($taskNumEditOption) {
            $queryBuilder->leftJoin('t.plan', 'p');
            $queryBuilder->leftJoin('t.tasktype', 'tt');
            $queryBuilder->addSelect('(' . $this->getTaskNumQuery($taskNumEditOption) . ') as taskNum');
        }
        $query = $queryBuilder->where('t.id = :id')->setParameter(':id', $id)->getQuery();
        /**
         * @var \TeamSoft\CrmRepositoryBundle\Entity\InfoTask $result
         */
        $result = $query->getOneOrNullResult();
        if ($result && $taskNumEditOption) {
            /**
             * @var \TeamSoft\CrmRepositoryBundle\Entity\InfoTask $task
             */
            $task = $result[0];
            $task->setTaskNum($result['taskNum']);
            return $task;
        }
        return $result;
    }

    public function findByResponsibleAndDate($responsibleId, \DateTime $date)
    {
        $useWatchersOnTaskOption = $this->options->get('useWatchersOnTask');
        $taskNumEditOption = $this->options->get('TaskNumEdit');
        $usePlanTaskPeriodOption = $this->options->get('usePlanTaskPeriod');
        $hideTaskNumOption = $this->options->get('hideTaskNum');
        $dateTill = clone $date;
        $dateTill->add(\DateInterval::createFromDateString('86399 second'));

        $queryBuilder = $this->createQueryBuilder('t');
        if ($useWatchersOnTaskOption == 1) {
            $queryBuilder->leftJoin('t.taskWatchers', 'tw')
                ->where('(t.responsible = :responsibleId OR t.responsible2 = :responsibleId OR tw.user = :responsibleId)');
        } else {
            $queryBuilder->where('(t.responsible = :responsibleId OR t.responsible2 = :responsibleId)');
        }
        if ($hideTaskNumOption) {
            $queryBuilder->addSelect("'' as taskNum");
        } else if ($usePlanTaskPeriodOption) {
            $queryBuilder->addSelect('(' . $this->getTaskPeriodNumQuery() . ') as taskNum');
        } else if ($taskNumEditOption) {
            $queryBuilder->leftJoin('t.plan', 'p');
            $queryBuilder->leftJoin('t.tasktype', 'tt');
            $queryBuilder->addSelect('(' . $this->getTaskNumQuery($taskNumEditOption) . ') as taskNum');
        }
        $clientGmtOffset = $this->dateTimeWithGmtOffset->getClientGmtOffset() ?? 0;
        $dateFromExpression = "DateTimeWithGmtOffset(t.datefrom, t.gmtOffset, $clientGmtOffset)";
        $dateTillExpression = "DateTimeWithGmtOffset(t.datetill, t.gmtOffset, $clientGmtOffset)";
        $query = $queryBuilder->setParameter(':responsibleId', $responsibleId)
            ->andWhere("(($dateFromExpression BETWEEN :dateFrom AND :dateTill) OR ($dateTillExpression BETWEEN :dateFrom1 AND :dateTill1) 
                OR (:dateFrom2 BETWEEN $dateFromExpression AND $dateTillExpression))")
            ->setParameter(':dateFrom', $date)
            ->setParameter(':dateFrom1', $date)
            ->setParameter(':dateFrom2', $date)
            ->setParameter(':dateTill', $dateTill)
            ->setParameter(':dateTill1', $dateTill)
            ->andWhere('t.datetill > :dateFrom3')
            ->setParameter(':dateFrom3', $date)
            ->andWhere('(t.istrans IS NULL OR t.istrans = 0)')
            ->getQuery();

        if ($taskNumEditOption) {
            $result = array_map(function ($row) {
                /**
                 * @var \TeamSoft\CrmRepositoryBundle\Entity\InfoTask $task
                 */
                $task = $row[0];
                $task->setTaskNum($row['taskNum']);

                return $task;
            }, $query->getResult());
        } else {
            $result = $query->getResult();
        }

        return $result;
    }

    public function findByResponsibleAndPeriod($responsibleId, \DateTime $dateFrom, \DateTime $dateTill)
    {
        $useWatchersOnTaskOption = $this->options->get('useWatchersOnTask');
        $taskNumEditOption = $this->options->get('TaskNumEdit');
        $usePlanTaskPeriodOption = $this->options->get('usePlanTaskPeriod');
        $hideTaskNumOption = $this->options->get('hideTaskNum');
        $dateTill->add(\DateInterval::createFromDateString('86399 second'));
        $queryBuilder = $this->createQueryBuilder('t');
        if ($useWatchersOnTaskOption == 1) {
            $queryBuilder->leftJoin('t.taskWatchers', 'tw')
                ->where('(t.responsible = :responsibleId OR tw.user = :responsibleId)');
        } else {
            $queryBuilder->where('(t.responsible = :responsibleId OR t.responsible2 = :responsibleId)');
        }
        if ($hideTaskNumOption) {
            $queryBuilder->addSelect("'' as taskNum");
        } else if ($usePlanTaskPeriodOption) {
            $queryBuilder->addSelect('(' . $this->getTaskPeriodNumQuery() . ') as taskNum');
        } else if ($taskNumEditOption) {
            $queryBuilder->leftJoin('t.plan', 'p');
            $queryBuilder->leftJoin('t.tasktype', 'tt');
            $queryBuilder->addSelect('(' . $this->getTaskNumQuery($taskNumEditOption) . ') as taskNum');
        }
        $clientGmtOffset = $this->dateTimeWithGmtOffset->getClientGmtOffset() ?? 0;
        $dateFromExpression = "DateTimeWithGmtOffset(t.datefrom, t.gmtOffset, $clientGmtOffset)";
        $dateTillExpression = "DateTimeWithGmtOffset(t.datetill, t.gmtOffset, $clientGmtOffset)";
        $query = $queryBuilder->setParameter(':responsibleId', $responsibleId)
            ->andWhere("(($dateFromExpression BETWEEN :dateFrom AND :dateTill) OR (t.datetill BETWEEN :dateFrom1 AND :dateTill1) 
                OR ((:dateFrom2 BETWEEN $dateFromExpression AND $dateTillExpression) 
                AND (:dateTill2 BETWEEN $dateFromExpression AND $dateTillExpression)))")
            ->setParameter(':dateFrom', $dateFrom)
            ->setParameter(':dateFrom1', $dateFrom)
            ->setParameter(':dateFrom2', $dateFrom)
            ->setParameter(':dateTill', $dateTill)
            ->setParameter(':dateTill1', $dateTill)
            ->setParameter(':dateTill2', $dateTill)
            ->andWhere('t.datetill > :dateFrom3')
            ->setParameter(':dateFrom3', $dateFrom)
            ->andWhere('(t.istrans IS NULL OR t.istrans = 0)')
            ->getQuery();

        $result = $query->getResult();

        if ($taskNumEditOption) {
            $result = array_map(function ($row) {
                /**
                 * @var \TeamSoft\CrmRepositoryBundle\Entity\InfoTask $task
                 */
                $task = $row[0];
                $task->setTaskNum($row['taskNum']);

                return $task;
            }, $result);
        }

        return $result;
    }

    public function findByFilterWithCompleteCoordinates(array $filter): array
    {
        $queryBuilder = $this->createQueryBuilder('t')
            ->select([
                't.id taskId',
                't.datefrom taskDateFrom',
                't.datetill taskDateTill',
                't.factclosed taskDateClosed',
                't.gpsLat taskGpsLat',
                't.gpsLng taskGpsLong',
                't.gpsAccuracy taskGpsAccuracy',
                't.gpsDate taskGpsDate',
                'tt.name taskType',
                't.gmtOffset taskGmtOffset',
                'c.name compName',
                'c.gpsLa compLat',
                'c.gpsLo compLong',
                'c.street compStreet',
                'c.building compBuild',
                'ct.isshop compIsShop',
                'streettype.name compStreetType',
                'city.name compCity',
                'companyCategory.name compCategory',
                'responsible.id respId',
                'responsible.name respName',
                'contact.firstname contFirstName',
                'contact.lastname contLastName',
                'contact.middlename contMiddleName',
                'contactSpec.name contSpec',
                'contactCategory.name contCategory',
            ])
            ->innerJoin('t.tasktype', 'tt')
            ->innerJoin('t.taskstate', 'taskState')
            ->innerJoin('t.company', 'c')
            ->leftJoin('c.city', 'city')
            ->innerJoin('c.companytype', 'ct')
            ->leftJoin('c.category', 'companyCategory')
            ->leftJoin('c.streettype', 'streettype')
            ->leftJoin('t.responsible', 'responsible')
            ->leftJoin('t.contact', 'contact')
            ->leftJoin('contact.specialization', 'contactSpec')
            ->leftJoin('contact.category', 'contactCategory')
            ->where('(t.gpsLat IS NOT NULL AND t.gpsLng IS NOT NULL) OR (c.gpsLa IS NOT NULL AND c.gpsLo IS NOT NULL)')
            ->orderBy('t.datetill');

        $this->applyFilterToQueryBuilder($queryBuilder, $filter);

        return $queryBuilder->getQuery()->getScalarResult();
    }

    public function findByFilterWithCoordinates(array $filter, $fetchMode = null)
    {
        $queryBuilder = $this->createQueryBuilder('t')
            ->addSelect('tt')
            ->addSelect('c')
            ->addSelect('city')
            ->addSelect('ct')
            ->addSelect('companyCategory')
            ->addSelect('responsible')
            ->addSelect('contact')
            ->addSelect('contactSpec')
            ->addSelect('contactCategory')
            ->addSelect('t.datefrom')
            ->addSelect('t.datetill')
            ->addSelect('streettype')
            ->innerJoin('t.tasktype', 'tt')
            ->innerJoin('t.company', 'c')
            ->leftJoin('c.city', 'city')
            ->innerJoin('c.companytype', 'ct')
            ->leftJoin('c.category', 'companyCategory')
            ->innerJoin('t.responsible', 'responsible')
            ->leftJoin('t.contact', 'contact')
            ->leftJoin('contact.specialization', 'contactSpec')
            ->leftJoin('contact.category', 'contactCategory')
            ->leftJoin('c.streettype', 'streettype')
            ->where('t.gpsLat IS NOT NULL AND t.gpsLng IS NOT NULL');
        $this->applyFilterToQueryBuilder($queryBuilder, $filter);
        $queryBuilder->orderBy('t.gpsDate');

        return $queryBuilder->getQuery()->getScalarResult($fetchMode);
    }

    public function findByFilterWithCompanyCoordinates(array $filter, $fetchMode = null)
    {
        $queryBuilder = $this->createQueryBuilder('t')
            ->addSelect('tt')
            ->addSelect('c')
            ->addSelect('responsible')
            ->addSelect('responsible2')
            ->addSelect('streettype')
            ->addSelect('city')
            ->addSelect('companyCategory')
            ->addSelect('companyType')
            ->addSelect('contact')
            ->addSelect('contactSpec')
            ->addSelect('contactCategory')
            ->addSelect('taskState')
            ->addSelect('t.gpsDate')
            ->innerJoin('t.tasktype', 'tt')
            ->innerJoin('t.company', 'c')
            ->innerJoin('t.taskstate', 'taskState')
            ->leftJoin('c.category', 'companyCategory')
            ->leftJoin('c.companytype', 'companyType')
            ->innerJoin('t.responsible', 'responsible')
            ->leftJoin('t.responsible2', 'responsible2')
            ->leftJoin('c.streettype', 'streettype')
            ->innerJoin('c.city', 'city')
            ->leftJoin('t.contact', 'contact')
            ->leftJoin('contact.specialization', 'contactSpec')
            ->leftJoin('contact.category', 'contactCategory')
            ->where('c.gpsLa IS NOT NULL AND c.gpsLo IS NOT NULL');
        $this->applyFilterToQueryBuilder($queryBuilder, $filter);
        $queryBuilder->orderBy('t.datetill');

        return $queryBuilder->getQuery()->getResult($fetchMode);
    }

    /**
     * @param string $phone
     * @param \DateTime $date
     * @return mixed
     */
    public function getTaskCountByPhone(string $phone, \DateTime $date)
    {
        $clientGmtOffset = $this->dateTimeWithGmtOffset->getClientGmtOffset() ?? 0;
        $dateFromExpression = "DateTimeWithGmtOffset(t.datefrom, t.gmtOffset, $clientGmtOffset)";
        $queryBuilder = $this->createQueryBuilder('t')
            ->leftJoin('t.contact', 'c')
            ->leftJoin('c.contactPhoneCollection', 'cp')
            ->where("cp.phone = :phone AND $dateFromExpression < :date")
            ->setParameter('phone', $phone)
            ->setParameter('date', $date);

        return $queryBuilder->getQuery()->getResult();
    }

    /**
     * @return array
     */
    public function findByModified()
    {
        $currentTime = new \DateTime();
        $correctedTime = $currentTime->modify("-15 min");

        $tableName = $this->getClassMetadata()->getTableName();

        $rsm = new ResultSetMappingBuilder($this->_em);
        $rsm->addRootEntityFromClassMetadata(
            $this->_entityName,
            't',
            array(),
            ResultSetMappingBuilder::COLUMN_RENAMING_INCREMENT
        );
        $rsm->addScalarResult('task_id', 'task', 'integer');

        $selectClause = $rsm->generateSelectClause();

        $sql = "
            SELECT 
              $selectClause,
              t.id
            FROM $tableName t
            WHERE t.modified > ':correctedTime'
        ";

        $sql = str_replace(':correctedTime', $correctedTime->format('Y-m-d H:i:s'), $sql);

        $query = $this->getEntityManager()->createNativeQuery($sql, $rsm);

        return $query->getResult();
    }

    public function findPhotoReports($filter = [], $orderBy = [], $limit = null, $offset = 0)
    {
        $queryBuilder = $this->createQueryBuilder('task');
        $queryBuilder
            ->innerJoin(InfoTaskfilegroup::class, 'tfg', 'WITH', 'tfg.subj = task')
            ->innerJoin(VwTaskfileimage::class, 'tfi', 'WITH', 'tfi.subj = tfg');

        if (isset($filter['company']) && $filter['company']) {
            $queryBuilder->innerJoin('task.company', 'company')
                ->andWhere('company in (:companies)')
                ->setParameter(':companies',
                    $this->getEntityManager()->getRepository(InfoCompany::class)->findBy($filter['company']));
        }

        if ($limit) {
            $queryBuilder->setMaxResults($limit);
        }

        if ($offset) {
            $queryBuilder->setFirstResult($offset);
        }

        $queryBuilder->addGroupBy('task.id')
            ->select('task.id');

        $resultBuilder = $this->createQueryBuilder('task_result')
            ->andWhere('task_result.id IN (:task_ids)')
            ->setParameter(':task_ids', $queryBuilder->getQuery()->getResult());

        return $resultBuilder->getQuery()->getResult();
    }

    private function applyFilterToQueryBuilder(QueryBuilder $queryBuilder, array $filter)
    {
        if (!$queryBuilder->getDQLPart('where')) {
            $queryBuilder->where('1 = 1');
        }

        if (isset($filter['hasSurveyByInn']) && $filter['hasSurveyByInn']) {
            $queryBuilder->andWhere('SIZE(t.taskSurveyByInnCollection) > 0');
        }

        if (isset($filter['taskparamFilled']) && $filter['taskparamFilled']) {
            $queryBuilder->andWhere('SIZE(t.taskParamCollection) > 0');
        }

        if (isset($filter['companyId']) && $filter['companyId']) {
            $queryBuilder->andWhere('t.company = :companyId')
                ->setParameter('companyId', $filter['companyId']);
        }

        if (isset($filter['tasktypeId']) && $filter['tasktypeId']) {
            $queryBuilder->andWhere('t.tasktype = :tasktypeId')
                ->setParameter('tasktypeId', $filter['tasktypeId']);
        }

        if (isset($filter['contactId']) && $filter['contactId']) {
            if (!empty($filter['includeEmptyContact'])) {
                $queryBuilder->andWhere('t.contact = :contactId or t.contact is null')
                    ->setParameter('contactId', $filter['contactId']);
            } else {
                $queryBuilder->andWhere('t.contact = :contactId')
                    ->setParameter('contactId', $filter['contactId']);
            }
        }

        if (isset($filter['responsibleId']) && is_numeric($filter['responsibleId'])) {
            $queryBuilder->andWhere('t.responsible = ' . $filter['responsibleId']);
        }

        if (isset($filter['responsible2Id']) && is_numeric($filter['responsible2Id'])) {
            $queryBuilder->andWhere('t.responsible2 = ' . $filter['responsible2Id']);
        }

        if (isset($filter['gpsAccuracyLessThan']) && $filter['gpsAccuracyLessThan'] > 0) {
            $queryBuilder->andWhere("t.gpsAccuracy < '" . $filter['gpsAccuracyLessThan'] . "'");
        }

        if (isset($filter['isDone']) && $filter['isDone'] === true) {
            $queryBuilder
                ->innerJoin('t.taskstate', 'taskstate')
                ->andWhere('taskstate.isclosed = 1')
                ->andWhere('taskstate.isfinish = 1');
        }

        $clientGmtOffset = $this->dateTimeWithGmtOffset->getClientGmtOffset() ?? 0;
        $dateFromExpression = "DateTimeWithGmtOffset(t.datefrom, t.gmtOffset, $clientGmtOffset)";
        $dateTillExpression = "DateTimeWithGmtOffset(t.datetill, t.gmtOffset, $clientGmtOffset)";
        if (isset($filter['gpsDateStart']) && $filter['gpsDateStart'] instanceof \DateTime &&
            isset($filter['gpsDateEnd']) && $filter['gpsDateEnd'] instanceof \DateTime
        ) {
            $queryBuilder
                ->andWhere("$dateFromExpression BETWEEN :gpsDateStart AND :gpsDateEnd")
                ->setParameters(array(
                    ':gpsDateStart' => $filter['gpsDateStart'],
                    ':gpsDateEnd' => $filter['gpsDateEnd'],
                ));
        } elseif (isset($filter['gpsDateStart']) && $filter['gpsDateStart'] instanceof \DateTime) {
            $queryBuilder
                ->andWhere("$dateFromExpression >= :gpsDateStart")
                ->setParameter(':gpsDateStart', $filter['gpsDateStart']);
        } elseif (isset($filter['gpsDateEnd']) && $filter['gpsDateEnd'] instanceof \DateTime) {
            $queryBuilder
                ->andWhere("$dateFromExpression <= :gpsDateEnd")
                ->setParameter(':gpsDateEnd', $filter['gpsDateEnd']);
        }

        if (isset($filter['dateFrom']) && $filter['dateFrom'] instanceof \DateTime &&
            isset($filter['dateTill']) && $filter['dateTill'] instanceof \DateTime
        ) {
            $queryBuilder
                ->andWhere("($dateFromExpression BETWEEN :dateFrom AND :dateTill) OR ($dateTillExpression BETWEEN :dateFrom1 
                    AND :dateTill1) OR ((:dateFrom2 BETWEEN $dateFromExpression AND $dateTillExpression) 
                    AND (:dateTill2 BETWEEN $dateFromExpression AND $dateTillExpression))")
                ->setParameters(array(
                    ':dateFrom' => $filter['dateFrom'],
                    ':dateFrom1' => $filter['dateFrom'],
                    ':dateFrom2' => $filter['dateFrom'],
                    ':dateTill' => $filter['dateTill'],
                    ':dateTill1' => $filter['dateTill'],
                    ':dateTill2' => $filter['dateTill'],
                ));
        } elseif (isset($filter['dateFrom']) && $filter['dateFrom'] instanceof \DateTime) {
            $queryBuilder
                ->andWhere("$dateFromExpression >= :dateFrom")
                ->setParameter(':dateFrom', $filter['dateFrom']);
        } elseif (isset($filter['dateTill']) && $filter['dateTill'] instanceof \DateTime) {
            $queryBuilder
                ->andWhere("$dateTillExpression <= :dateTill")
                ->setParameter(':dateTill', $filter['dateTill']);
        }

        if (isset($filter['dateFromLess']) && $filter['dateFromLess'] instanceof \DateTime) {
            $queryBuilder
                ->andWhere("$dateFromExpression < :dateFromLess")
                ->setParameter(':dateFromLess', $filter['dateFromLess']);
        }
    }

    /**
     * @param $taskNumEditOption
     * @return string
     * @see PlanContentTrait::findWithDetailsByFilter()
     */
    private function getTaskNumQuery($taskNumEditOption)
    {
        $query = $this->createQueryBuilder('it')
            ->select('count(it.id) + 1')
            ->join('it.tasktype', 'tt2')
            ->where('t.responsible = it.responsible')
            /**
             * showname=1 company and contact
             * showname=2 company
             * showname=3 contact
             */
            ->andWhere('(t.company = it.company OR COALESCE(tt2.showname, 2) = 3)')
            ->andWhere('(t.contact = it.contact or COALESCE(tt2.showname, 2) = 2)');

        switch ($taskNumEditOption) {
            case 'План цикла':
                $query->andWhere('it.datefrom >= p.datefrom');
                break;
            case 'Месяц':
                $query->andWhere("it.datefrom >= DATEADD(DATE('1900-01-01'), DATEDIFF(month, DATE('1900-01-01'), t.datefrom), 'month')");
                break;
            case 'Квартал':
                $query->andWhere("it.datefrom >= DATEADD(DATE('1900-01-01'), DATEDIFF(q, DATE('1900-01-01'), t.datefrom), 'quarter')");
                break;
            case 'Год':
                $query->andWhere("it.datefrom >= DATEADD(DATE('1900-01-01'), DATEDIFF(yy, DATE('1900-01-01'), t.datefrom), 'year')");
                break;
        }

        //skip self
        $query->andWhere('t.datefrom > it.datefrom');
        //open and finished, not cancel
        $query->andWhere('it.taskstate in (
            select ts.id 
            from TeamSoftCrmRepositoryBundle:InfoTaskstate ts
            where ((ts.isclosed = 1 and ts.isfinish = 1) or (ts.isclosed = 0 and ts.isfinish = 0))
            )');
        return $query->getQuery()->getDQL();
    }

    /**
     * @return string
     */
    private function getTaskPeriodNumQuery()
    {
        $query = $this->_em->createQueryBuilder()
            ->from(InfoPlantaskperiod::class, 'ptp')
            ->select('max(ptp.num)')
            ->where('t.datefrom between ptp.dt1 and ptp.dt2+1')
            ->andWhere('ptp.plandetail in (
                select pd.id from TeamSoftCrmRepositoryBundle:InfoPlandetail pd
                where 
                pd.plan in (
                    select p.id from TeamSoftCrmRepositoryBundle:InfoPlan p where (
                        t.datefrom between p.datefrom and p.datetill
                    ) and p.owner = t.responsible
                )
                and pd.tasktype = t.tasktype and 
                pd.target in (
                    select IDENTITY(ts.target) from TeamSoftCrmRepositoryBundle:InfoTargetspec ts
                        where ts.spec = t.specialization
                )
            )');

        return $query->getQuery()->getDQL();
    }
}
