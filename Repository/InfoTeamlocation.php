<?php

namespace TeamSoft\CrmRepositoryBundle\Repository;

use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;

use Doctrine\ORM\Query;
use Doctrine\Persistence\ManagerRegistry;
use TeamSoft\CrmRepositoryBundle\Service\DatabasePlatform;
use TeamSoft\CrmRepositoryBundle\Entity\InfoTeamlocation as InfoTeamlocationEntity;
use TeamSoft\CrmRepositoryBundle\Entity\InfoUser as InfoUserEntity;

class InfoTeamlocation extends ServiceEntityRepository
{

    private $dp;

    public function __construct(ManagerRegistry $registry, DatabasePlatform $dp)
    {
        parent::__construct($registry, InfoTeamlocationEntity::class);
        $this->dp = $dp;
    }

    public function findCoordinatesByFilter(array $filter, $hydrationMode = Query::HYDRATE_OBJECT)
    {
        $rsm = $this->createResultSetMappingBuilder('tl');
        $rsm->addJoinedEntityFromClassMetadata(InfoUserEntity::class, 'u', 'tl', 'user');
        $selectClause = $rsm->generateSelectClause();

        $sql = "SELECT $selectClause\nFROM info_teamlocation tl\nINNER JOIN info_user u on tl.user_id = u.id\n";

        if (isset($filter['userId']) && is_numeric($filter['userId'])) {
            $sql .= ' WHERE  u.id = ' . $filter['userId'];
        } else {
            $sql .= ' WHERE u.id = 0';
        }

        $dateStart = $filter['dateStart'];
        $dateEnd = $filter['dateEnd'];
        $gpsDateExpression = $this->dp->getDateTimeWithGmtOffsetExpression('tl.gps_date', 'tl.gmt_offset');

        if ($dateStart instanceof \DateTime && $dateEnd instanceof \DateTime) {
            $dateStartString = $dateStart->format('Y-m-d H:i:s');
            $dateEndString = $dateEnd->format('Y-m-d H:i:s');
            $sql .= " AND ($gpsDateExpression BETWEEN '$dateStartString' AND '$dateEndString')";
        } elseif ($dateStart instanceof \DateTime) {
            $dateStartString = $dateStart->format('Y-m-d H:i:s');
            $sql .= " AND ($gpsDateExpression >= '$dateStartString')";
        } elseif ($dateEnd instanceof \DateTime) {
            $dateEndString = $dateEnd->format('Y-m-d H:i:s');
            $sql .= " AND ($gpsDateExpression <= '$dateEndString')";
        }

        if ($filter['gpsAccuracyLessThan']) {
            $sql .= " AND tl.accuracy < " . $filter["gpsAccuracyLessThan"];
        }

        $sql .= ' AND latitude IS NOT NULL AND longitude IS NOT NULL';

        $sql .= ' ORDER BY tl.gps_date';

        return $this->getEntityManager()->createNativeQuery($sql, $rsm)->getResult($hydrationMode);
    }
}
