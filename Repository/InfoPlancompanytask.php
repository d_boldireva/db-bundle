<?php

namespace TeamSoft\CrmRepositoryBundle\Repository;

use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\Query\ResultSetMappingBuilder;
use Doctrine\Persistence\ManagerRegistry;
use TeamSoft\CrmRepositoryBundle\Service\DatabasePlatform;
use TeamSoft\CrmRepositoryBundle\Entity\InfoPlancompanytask as InfoPlancompanytaskEntity;
use \TeamSoft\CrmRepositoryBundle\Entity\InfoOptions;

class InfoPlancompanytask extends ServiceEntityRepository
{

    private $dp;
    private $options;

    public function __construct(ManagerRegistry $registry, DatabasePlatform $dp)
    {
        parent::__construct($registry, InfoPlancompanytaskEntity::class);
        $this->dp = $dp;

    }

    function findMinimumApprovementStatus($userId, \DateTime $dateFrom)
    {
        $yearString = $dateFrom->format("Y");
        $monthString = $dateFrom->format("m");

        $rsm = new ResultSetMappingBuilder($this->_em);
        $rsm->addRootEntityFromClassMetadata($this->_entityName, 'pct', array(), ResultSetMappingBuilder::COLUMN_RENAMING_INCREMENT);
        $selectClause = $rsm->generateSelectClause();

        $sql = "select $selectClause
                FROM info_plancompanytask pct
                WHERE COALESCE(approvementstatus, 0) = (
                    SELECT MIN(COALESCE(approvementstatus, 0))
                    FROM info_plancompanytask
                    WHERE user_id = :userId AND " . $this->dp->getYearExpression('dt') . "  = ':yearString' AND " . $this->dp->getMonthExpression('dt') . "  = ':monthString'
                ) AND user_id = :userId AND " . $this->dp->getYearExpression('dt') . "  = ':yearString' AND " . $this->dp->getMonthExpression('dt') . "  = ':monthString'";

        $sql = $this->dp->modifyOnlyLimitQuery($sql, 1);
        $sql = str_replace(':userId', $userId, $sql);
        $sql = str_replace(':yearString', $yearString, $sql);
        $sql = str_replace(':monthString', $monthString, $sql);

        $query = $this->getEntityManager()->createNativeQuery($sql, $rsm);

        return $query->getResult();
    }

    public function findByUserAndDateGroupByCompanyType($userId, \DateTime $dateTime)
    {
        $yearString = $dateTime->format("Y");
        $monthString = $dateTime->format("m");
        $sql = "
                DECLARE @userId int, @year varchar(255), @month varchar(255);
				SET @userId = :userId;
				SET @year = :yearString;
				SET @month = :monthString;

				SELECT ct.name type, COUNT(pct.id) companies, SUM(pct.tasks) tasks, 1 _order FROM info_plancompanytask pct
                LEFT JOIN info_company c on pct.company_id = c.id
                LEFT JOIN info_companytype ct on c.CompanyType_id = ct.id
                WHERE pct.user_id = @userId AND YEAR(pct.dt) = @year AND MONTH(pct.dt) = @month
                GROUP BY ct.Name
				UNION
				SELECT 'Всего', COUNT(pct.id) companies, COALESCE(SUM(pct.tasks), 0), 2 _order FROM info_plancompanytask pct
				WHERE pct.user_id = @userId AND YEAR(pct.dt) = @year AND MONTH(pct.dt) = @month
				ORDER BY _order
				";

        $connection = $this->getEntityManager()->getConnection();
        $statement = $connection->prepare($sql);
        $statement->bindValue(':userId', $userId, \PDO::PARAM_INT);
        $statement->bindValue(':yearString', $yearString, \PDO::PARAM_STR);
        $statement->bindValue(':monthString', $monthString, \PDO::PARAM_STR);

        return $statement->executeQuery()->fetchAllAssociative();
    }

    public function findByUserAndDateWithAdditionalInformation($userId, \DateTime $dateTime, \DateTime $currentMonthTime)
    {
        $rsm = new ResultSetMappingBuilder($this->_em);
        $rsm->addRootEntityFromClassMetadata($this->_entityName, 'pct', array(), ResultSetMappingBuilder::COLUMN_RENAMING_INCREMENT);
        $rsm->addEntityResult($this->_entityName, 'pct', 'pct');
        $rsm->addScalarResult('fromPrevMasterList', 'fromPrevMasterList', 'integer');

        $showColumnRanbaxySales = $this->_em->getRepository(InfoOptions::class)->findOneBy(array('name' => 'MasterListShowColumnRanbaxySales'));
        $showColumnRanbaxySalesConfig = $showColumnRanbaxySales ? (int)$showColumnRanbaxySales->getValue() : 0;
        $sqlAdd = '';
        if ($showColumnRanbaxySalesConfig) {
            $sqlAdd = '
SELECT SUM(rcl.amount) FROM ranbaxy_cl rcl
JOIN info_saleperiod sp ON rcl.saleperiod_id = sp.id
JOIN info_preparationdirection pd ON rcl.preparation_id = pd.preparation_id
JOIN info_userdirection ud ON pd.direction_id = ud.direction_id
WHERE ud.user_id = :userId AND rcl.company_id=pct.company_id
AND DATEADD(month, -1, GETDATE()) between sp.datefrom and sp.datetill';
            $sqlAdd = ', (' . $sqlAdd . ') AS sales';
            $rsm->addScalarResult('sales', 'sales', 'integer');
        }

        $selectClause = $rsm->generateSelectClause();

        $sql = "SELECT $selectClause
                , (
                SELECT 1 where pct.company_id in
	              (SELECT company_id FROM info_plancompanytask
		          WHERE YEAR(dt) = ':yearCurrentString' AND MONTH(dt) = ':monthCurrentString' AND  user_id = :userId )
		        ) fromPrevMasterList
		        $sqlAdd
                FROM info_plancompanytask pct
                LEFT JOIN info_company c on pct.company_id = c.id
                LEFT JOIN info_city ct on c.City_id = ct.id
                WHERE YEAR(dt) = ':yearString' AND MONTH(dt) = ':monthString' AND  user_id = :userId
        ";

        $sql = str_replace(':userId', $userId, $sql);
        $sql = str_replace(':yearString', $dateTime->format("Y"), $sql);
        $sql = str_replace(':monthString', $dateTime->format("m"), $sql);
        $sql = str_replace(':yearCurrentString', $currentMonthTime->format("Y"), $sql);
        $sql = str_replace(':monthCurrentString', $currentMonthTime->format("m"), $sql);

        $query = $this->getEntityManager()->createNativeQuery($sql, $rsm);

        return $query->getResult();
    }

    public function findByUserAndDate($userId, \DateTime $dateTime)
    {
        $yearString = $dateTime->format("Y");
        $monthString = $dateTime->format("m");

        $rsm = new ResultSetMappingBuilder($this->_em);
        $rsm->addRootEntityFromClassMetadata($this->_entityName, 'pct', array(), ResultSetMappingBuilder::COLUMN_RENAMING_INCREMENT);

        $selectClause = $rsm->generateSelectClause();

        $sql = "SELECT $selectClause
                FROM info_plancompanytask pct
                WHERE YEAR(dt) = ':yearString' AND MONTH(dt) = ':monthString' AND  user_id = :userId
        ";

        $sql = str_replace(':userId', $userId, $sql);
        $sql = str_replace(':yearString', $yearString, $sql);
        $sql = str_replace(':monthString', $monthString, $sql);

        $query = $this->getEntityManager()->createNativeQuery($sql, $rsm);

        return $query->getResult();
    }

    public function getPercentInfo($firstDate, $userId)
    {
        $sql = "
          SELECT SUM(t.removed) AS " . $this->dp->escapeColumn('removed') . " , SUM(t.new) AS " . $this->dp->escapeColumn('new') . " , SUM(changed) as " . $this->dp->escapeColumn('changed') . "  FROM (
          
            --removed
            --Были в прошлом
            --В прошлом tasks > 0 AND approvementStatus = 2
            --В текущем tasks = 0 AND approvementStatus in (1, 2)
            --Деленное на общее кол-во клиентов в предыдущем месяце
            
            SELECT ROUND(COUNT(DISTINCT id) * 100.0 / (
              SELECT NULLIF(COUNT(DISTINCT company_id), 0) FROM info_plancompanytask 
                WHERE user_id = $userId AND dt BETWEEN " . $this->dp->dateAddExpression('month','-1',"'$firstDate'") . " AND " . $this->dp->dateAddExpression('second','-1',"'$firstDate'") . " 
            ), 2) AS " . $this->dp->escapeColumn('removed') . ", 0 AS " . $this->dp->escapeColumn('new') . ", 0 AS " . $this->dp->escapeColumn('changed') . "
              FROM info_plancompanytask 
                WHERE tasks = 0 AND dt between '$firstDate' AND " . $this->dp->dateAddExpression('second','-1', $this->dp->dateAddExpression('month','1',"'$firstDate'")) . " AND user_id = $userId
                AND approvementStatus in (1, 2) AND company_id IN (
                  SELECT company_id FROM info_plancompanytask 
                    WHERE user_id = $userId AND dt BETWEEN " . $this->dp->dateAddExpression('month','-1',"'$firstDate'") . " AND " . $this->dp->dateAddExpression('second','-1',"'$firstDate'") . " 
                      AND tasks > 0 AND approvementStatus = 2
                )
            
            UNION
             
            --new
            --Исключить все кто был в прошлом таргетировании
            --В прошлом tasks = 0 AND approvementStatus = 2 или его вообще не было в листе
            --В текущем кратность tasks > 0 AND approvementStatus in (1, 2)
            --Деленное на общее кол-во клиентов в предыдущем месяце
            
            SELECT 0 AS " . $this->dp->escapeColumn('removed') . ", ROUND(COUNT(DISTINCT id) * 100.0 / (
              SELECT NULLIF(COUNT(DISTINCT company_id), 0) FROM info_plancompanytask 
                WHERE user_id = $userId AND dt BETWEEN " . $this->dp->dateAddExpression('month','-1',"'$firstDate'") . " AND " . $this->dp->dateAddExpression('second','-1',"'$firstDate'") . "
            ), 2) AS " . $this->dp->escapeColumn('new') . ", 0 as " . $this->dp->escapeColumn('changed') . "
              FROM info_plancompanytask 
                WHERE tasks > 0 AND approvementStatus in (1, 2) AND dt between '$firstDate' AND " . $this->dp->dateAddExpression('second','-1', $this->dp->dateAddExpression('month','1',"'$firstDate'")) . "
                AND user_id = $userId AND company_id NOT IN (
                  SELECT company_id FROM info_plancompanytask 
                    WHERE user_id = $userId AND dt BETWEEN " . $this->dp->dateAddExpression('month','-1',"'$firstDate'") . " AND " . $this->dp->dateAddExpression('second','-1',"'$firstDate'") . "
                      AND tasks > 0 AND approvementStatus = 2
                )
                 
            UNION
             
            --changed
            --кол-во врачей(аптек) с признаком «Изм.» относительно всех врачей(аптек) в утверждённом таргет-листе.
            
            SELECT 0 AS " . $this->dp->escapeColumn('removed') . ", 0 AS " . $this->dp->escapeColumn('new') . ", ROUND(COUNT(DISTINCT pct.id) * 100.0 / (
              SELECT NULLIF(COUNT(DISTINCT company_id), 0) FROM info_plancompanytask
                WHERE dt between '$firstDate' AND " . $this->dp->dateAddExpression('second','-1', $this->dp->dateAddExpression('month','1',"'$firstDate'")) . " AND user_id = $userId 
              ), 2) as " . $this->dp->escapeColumn('changed') . "
            FROM info_plancompanytask pct
              LEFT JOIN info_company co ON co.id = pct.company_id
              LEFT JOIN info_companyinfo ci ON co.id = ci.Subj_id AND ci.InfoType_id IN (SELECT id FROM info_AddInfoType WHERE Code = 'pharm_categ')
              LEFT JOIN info_companycategory cat ON ci.sysdictvalue_id = cat.id
              LEFT JOIN info_plancompanyrule pcr ON cat.id = pcr.category_id AND pct.dt BETWEEN pcr.datefrom AND pcr.datetill
                WHERE pct.dt between '$firstDate' AND " . $this->dp->dateAddExpression('second','-1', $this->dp->dateAddExpression('month','1',"'$firstDate'")) . " AND user_id = $userId 
                  AND ((COALESCE(pcr.visit_cnt, 0) = 0 AND pct.tasks > 0) OR (pcr.visit_cnt > 0 AND COALESCE(pct.tasks, 0) = 0))
          ) AS t
        ";

        $stmt = $this->getEntityManager()->getConnection()->prepare($sql);
        $stmt->execute();

        return $stmt->fetch();
    }
}
