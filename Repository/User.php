<?php

namespace TeamSoft\CrmRepositoryBundle\Repository;

use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Bridge\Doctrine\Security\User\UserLoaderInterface;
use TeamSoft\CrmRepositoryBundle\Entity\SaleProject\InfoTasksaleproject;
use TeamSoft\CrmRepositoryBundle\Service\DatabasePlatform;
use TeamSoft\CrmRepositoryBundle\Entity\User as UserEntity;

class User extends ServiceEntityRepository implements UserLoaderInterface
{

    private $dp;

    public function __construct(ManagerRegistry $registry, DatabasePlatform $dp)
    {
        parent::__construct($registry, UserEntity::class);
        $this->dp = $dp;
    }

    public function loadUserByUsername($username)
    {
        if ($this->dp->isSQLServer2008()) {
            $where = '_u.poLogin = COLLATE(:username, SQL_Latin1_General_CP1_CS_AS)';
        } else {
            $where = '_u.poLogin = :username';
        }
        return $this->createQueryBuilder('u')
            ->innerJoin('u.infoUser', '_u')
            ->where($where)
            ->setParameter('username', $username)
            ->getQuery()
            ->getOneOrNullResult();
    }
}
