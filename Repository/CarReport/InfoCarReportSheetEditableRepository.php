<?php declare(strict_types=1);

namespace TeamSoft\CrmRepositoryBundle\Repository\CarReport;

use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use TeamSoft\CrmRepositoryBundle\Entity\CarReport\InfoCarReport;
use TeamSoft\CrmRepositoryBundle\Entity\CarReport\InfoCarReportSheet;
use TeamSoft\CrmRepositoryBundle\Entity\CarReport\InfoCarReportSheetEditable;
use TeamSoft\CrmRepositoryBundle\Repository\InfoCalendarRepository;
use TeamSoft\CrmRepositoryBundle\Utils\DateHelper;

/**
 * Class InfoCarReportSheetEditableRepository
 * @package TeamSoft\CrmRepositoryBundle\Repository\CarReport
 *
 * @method InfoCarReportSheetEditable|null find($id, $lockMode = null, $lockVersion = null)
 * @method InfoCarReportSheetEditable|null findOneBy(array $criteria, array $orderBy = null)
 * @method InfoCarReportSheetEditable[]    findAll()
 * @method InfoCarReportSheetEditable[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class InfoCarReportSheetEditableRepository extends ServiceEntityRepository
{
    public const DAYS_OF_EDITABLE_FROM_START_PERIOD_FOR_REPORT = 14;

    /** @var InfoCalendarRepository */
    protected $infoCalendarRepository;

    public function __construct(ManagerRegistry $registry, InfoCalendarRepository $infoCalendarRepository)
    {
        parent::__construct($registry, InfoCarReportSheetEditable::class);

        $this->infoCalendarRepository = $infoCalendarRepository;
    }

    public function isEditableSheet(InfoCarReportSheet $sheet): bool
    {
        $timestamp = DateHelper::toDate()->getTimestamp();

        /** @var InfoCarReportSheetEditable|null $editable */
        $editable = $this->createQueryBuilder('e')
            ->andWhere('e.user=:user')
            ->andWhere('e.month=:month')
            ->setParameter('user', $sheet->getUser())
            ->setParameter('month', $sheet->getMonth())
            ->getQuery()
            ->getOneOrNullResult();

        if (!$editable) {
            $period = $this->getDefaultPeriodByDate($sheet->getMonth());
        } else {
            $period = $editable->getDatePeriod();
        }

        return $period->getStartDate()->getTimestamp() <= $timestamp && $timestamp <= $period->getEndDate()->getTimestamp();
    }


    public function isEditableReport(InfoCarReport $report): bool
    {
        $timestamp = DateHelper::toDate()->getTimestamp();

        $sheet = $report->getCarReportSheet();
        /** @var InfoCarReportSheetEditable|null $editable */
        $editable = $this->createQueryBuilder('e')
            ->andWhere('e.user=:user')
            ->andWhere('e.month=:month')
            ->setParameter('user', $sheet->getUser())
            ->setParameter('month', $sheet->getMonth())
            ->getQuery()
            ->getOneOrNullResult();

        if (!$editable) {
            $period = $this->getDefaultPeriodByDate($sheet->getMonth());
        } else {
            $period = $editable->getDatePeriod();
        }

        /** @var \DateTime $endDate */
        $endDate = clone $period->getStartDate();
        $endDate->modify(self::DAYS_OF_EDITABLE_FROM_START_PERIOD_FOR_REPORT . ' days')->setTime(23, 59, 59);

        return $period->getStartDate()->getTimestamp() <= $timestamp && $timestamp <= $endDate->getTimestamp();
    }

    public function getDefaultPeriodByDate(\DateTime $time): \DatePeriod
    {
        // last working day of this month
        $startPeriodModel = $this->infoCalendarRepository->getLastWorkingDayInMonth($time);

        if (!$startPeriodModel) {
            $startPeriod = (clone $time)->modify('last day of this month');
        } else {
            $startPeriod = $startPeriodModel->getDateFrom();
        }

        $startPeriod->setTime(0, 0);

        // first working day of the next month
        $endPeriod = (clone $startPeriod)->modify('first day of next month');
        $infoCalendar = $this->infoCalendarRepository->getFirstWorkingDayInMonth($endPeriod);

        if ($infoCalendar) {
            $endPeriod = $infoCalendar->getDateFrom();
        }

        $endPeriod->setTime(23, 59, 59);
        return new \DatePeriod($startPeriod, new \DateInterval('P1D'), $endPeriod);
    }
}
