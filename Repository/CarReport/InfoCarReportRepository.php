<?php declare(strict_types=1);

namespace TeamSoft\CrmRepositoryBundle\Repository\CarReport;

use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use TeamSoft\CrmRepositoryBundle\Entity\CarReport\InfoCarReport;

/**
 * Class InfoCarReportRepository
 * @package TeamSoft\CrmRepositoryBundle\Repository\CarReport
 *
 * @method InfoCarReport|null find($id, $lockMode = null, $lockVersion = null)
 * @method InfoCarReport|null findOneBy(array $criteria, array $orderBy = null)
 * @method InfoCarReport[]    findAll()
 * @method InfoCarReport[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class InfoCarReportRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, InfoCarReport::class);
    }

    public function findPrevReportOnSheet(InfoCarReport $report): ?InfoCarReport
    {
        $query = $this->createQueryBuilder('r')
            ->andWhere('r.user = :user')->setParameter('user', $report->getUser())
            ->andWhere('r.date < :date')->setParameter('date', $report->getDate())
            ->andWhere('r.carReportSheet = :sheet')->setParameter('sheet', $report->getCarReportSheet())
            ->setMaxResults(1)
            ->orderBy('r.date', 'DESC');

        if ($report->getId()) {
            $query->andWhere('r.id <> :maxId')->setParameter('maxId', $report->getId());
        }

        return $query->getQuery()->getOneOrNullResult();
    }
}
