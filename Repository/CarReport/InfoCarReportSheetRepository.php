<?php
declare(strict_types=1);

namespace TeamSoft\CrmRepositoryBundle\Repository\CarReport;

use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\EntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use TeamSoft\CrmRepositoryBundle\Entity\InfoUser;
use TeamSoft\CrmRepositoryBundle\Entity\CarReport\InfoCarReportSheet;

/**
 * Class InfoCarReportSheet
 * @package TeamSoft\CrmRepositoryBundle\Repository\CarReport
 *
 * @method InfoCarReportSheet|null find($id, $lockMode = null, $lockVersion = null)
 * @method InfoCarReportSheet|null findOneBy(array $criteria, array $orderBy = null)
 * @method InfoCarReportSheet[]    findAll()
 * @method InfoCarReportSheet[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class InfoCarReportSheetRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, InfoCarReportSheet::class);
    }

    /**
     * @param InfoUser $user
     * @return InfoCarReportSheet|null
     */
    public function getLastSheetByUser(InfoUser $user): ?InfoCarReportSheet
    {
        return $this
            ->createQueryBuilder('r')
            ->where('r.user = :user')->setParameter('user', $user)
            ->setMaxResults(1)
            ->orderBy('r.month', 'DESC')
            ->getQuery()
            ->getOneOrNullResult();
    }

    public function isFirstSheet(InfoCarReportSheet $carReportSheet): bool
    {
        $last = $this->getFirstSheetByUser($carReportSheet->getUser());

        return $last && $carReportSheet->getId() === $last->getId();
    }

    public function getFirstSheetByUser(InfoUser $user): ?InfoCarReportSheet
    {
        return $this
            ->createQueryBuilder('r')
            ->where('r.user = :user')->setParameter('user', $user)
            ->setMaxResults(1)
            ->orderBy('r.month', 'ASC')
            ->getQuery()
            ->getOneOrNullResult();
    }

    public function findNextSheet(InfoCarReportSheet $sheet): ?InfoCarReportSheet
    {
        return $this
            ->createQueryBuilder('r')
            ->where('r.user = :user')->setParameter('user', $sheet->getUser())
            ->andWhere('r.month > :month')->setParameter('month', $sheet->getMonth())
            ->setMaxResults(1)
            ->orderBy('r.month', 'ASC')
            ->getQuery()
            ->getOneOrNullResult();
    }
}