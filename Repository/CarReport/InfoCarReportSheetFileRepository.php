<?php
declare(strict_types=1);

namespace TeamSoft\CrmRepositoryBundle\Repository\CarReport;

use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use TeamSoft\CrmRepositoryBundle\Entity\CarReport\InfoCarReportSheetFile;

/**
 * Class InfoCarReportSheetFileRepository
 * @package TeamSoft\CrmRepositoryBundle\Repository\CarReport
 * 
 * @method InfoCarReportSheetFile|null find($id, $lockMode = null, $lockVersion = null)
 * @method InfoCarReportSheetFile|null findOneBy(array $criteria, array $orderBy = null)
 * @method InfoCarReportSheetFile[]    findAll()
 * @method InfoCarReportSheetFile[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class InfoCarReportSheetFileRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, InfoCarReportSheetFile::class);
    }
}
