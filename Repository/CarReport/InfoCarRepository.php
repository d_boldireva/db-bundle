<?php
declare(strict_types=1);

namespace TeamSoft\CrmRepositoryBundle\Repository\CarReport;

use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use TeamSoft\CrmRepositoryBundle\Entity\CarReport\InfoCar;

/**
 * Class InfoCarRepository
 * @package TeamSoft\CrmRepositoryBundle\Repository\CarReport
 * @method InfoCar|null find($id, $lockMode = null, $lockVersion = null)
 * @method InfoCar|null findOneBy(array $criteria, array $orderBy = null)
 * @method InfoCar[]    findAll()
 * @method InfoCar[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class InfoCarRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, InfoCar::class);
    }
}
