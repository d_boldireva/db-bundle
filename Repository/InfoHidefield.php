<?php

namespace TeamSoft\CrmRepositoryBundle\Repository;

use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\Query\ResultSetMappingBuilder;
use Doctrine\Persistence\ManagerRegistry;
use TeamSoft\CrmRepositoryBundle\Service\DatabasePlatform;

class InfoHidefield extends ServiceEntityRepository
{

    const TASK_PAGE = 4;

    const ACTION_PAGE = 8;

    const COMPANY_PAGE = 1;

    const CONTACT_PAGE = 2;

    private $dp;

    public function __construct(ManagerRegistry $registry, DatabasePlatform $dp)
    {
        parent::__construct($registry, \TeamSoft\CrmRepositoryBundle\Entity\InfoHidefield::class);
        $this->dp = $dp;
    }

    public function findByPage(int $page, ?int $typeId = null, ?int $roleId = null, ?string $name = null)
    {
        $tableName = $this->getClassMetadata()->getTableName();

        $rsm = new ResultSetMappingBuilder($this->_em);
        $rsm->addRootEntityFromClassMetadata(
            $this->_entityName,
            $tableName,
            array(),
            ResultSetMappingBuilder::COLUMN_RENAMING_INCREMENT
        );

        switch ($page) {
            case self::TASK_PAGE:
                $typeTable = 'info_tasktype';
                break;
            case self::ACTION_PAGE:
                $typeTable = 'info_actiontype';
                break;
            case self::COMPANY_PAGE:
                $typeTable = 'info_companytype';
                break;
            case self::CONTACT_PAGE:
                $typeTable = 'info_contacttype';
                break;
            default:
                return false;
        }

        $roleId = is_numeric($roleId) ? $roleId : -1;
        $whereParts = ["$tableName.page = $page", "$tableName.role_id = $roleId"];

        $isNullEntityTypeExpression = $this->dp->getIsNullFunctionExpression("$tableName.entityType", "''");
        $entityTypeWherePart = "$isNullEntityTypeExpression = ''";
        if ($typeId) {
            $concatOperator = $this->dp->getConcatOperator();
            $entityTypeWherePart .= " OR $tableName.entitytype IN (
                SELECT '{' $concatOperator CAST(guid AS VARCHAR(36)) $concatOperator '}' FROM $typeTable where id = $typeId
            )";
        }
        $whereParts[] = "($entityTypeWherePart)";

        $isNullNameExpression = $this->dp->getIsNullFunctionExpression("$tableName.name", "''");
        $whereParts[] = "$isNullNameExpression <> ''";
        if ($name) {
            $whereParts[] = "$tableName.name = '$name'";
        }

        $selectClause = $rsm->generateSelectClause();
        $where = implode(' AND ', $whereParts);
        $sql = "
            SELECT $selectClause FROM $tableName
            WHERE $where            
        ";
        $query = $this->getEntityManager()->createNativeQuery($sql, $rsm);
        $result = $query->getResult();
        return $result;
    }
}
