<?php

namespace TeamSoft\CrmRepositoryBundle\Repository;

use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\Query\ResultSetMappingBuilder;
use Doctrine\Persistence\ManagerRegistry;
use TeamSoft\CrmRepositoryBundle\Service\DatabasePlatform;

class InfoRequired extends ServiceEntityRepository
{
    const TASK_PAGE = 4;

    const ACTION_PAGE = 8;

    const COMPANY_PAGE = 1;

    const CONTACT_PAGE = 2;

    private $dp;

    public function __construct(ManagerRegistry $registry, DatabasePlatform $dp)
    {
        parent::__construct($registry, \TeamSoft\CrmRepositoryBundle\Entity\InfoRequired::class);
        $this->dp = $dp;
    }

    public function findByFilter($page, $typeId = null, $stateId = null, $roleId = null)
    {
        $tableName = $this->getClassMetadata()->getTableName();

        $rsm = new ResultSetMappingBuilder($this->_em);
        $rsm->addRootEntityFromClassMetadata(
            $this->_entityName,
            $tableName,
            array(),
            ResultSetMappingBuilder::COLUMN_RENAMING_INCREMENT
        );

        switch ($page) {
            case self::TASK_PAGE:
                $typeTable = 'info_tasktype';
                $stateTable = 'info_taskstate';
                break;
            case self::ACTION_PAGE:
                $typeTable = 'info_actiontype';
                $stateTable = 'info_actionstate';
                break;
            case self::COMPANY_PAGE:
                $typeTable = 'info_companytype';
                break;
            case self::CONTACT_PAGE:
                $typeTable = 'info_contacttype';
                break;
            default:
                return false;
        }

        $orTypeWhere = '';
        $concatOperator = $this->dp->getConcatOperator();
        if ($typeId && isset($typeTable)) {
            $orTypeWhere = " OR $tableName.entitytype IN (
                SELECT '{' $concatOperator CAST(guid AS VARCHAR(36)) $concatOperator '}' FROM $typeTable where id = $typeId
            )";
        }

        $orStateWhere = '';

        if ($stateId && isset($stateTable)) {
            $orStateWhere = " OR $tableName.type IN (
                SELECT '{' $concatOperator CAST(guid AS VARCHAR(36)) $concatOperator '}' FROM $stateTable where id = $stateId
            )";
        }

        $addInfoTypeSQL = "(SELECT '{' $concatOperator CAST(guid as VARCHAR(36)) $concatOperator '}' FROM info_addinfotype)";

        $selectClause = $rsm->generateSelectClause();
        $isNullEntityTypeExpression = $this->dp->getIsNullFunctionExpression("$tableName.entityType", "''");
        $isNullTypeExpression = $this->dp->getIsNullFunctionExpression("$tableName.type", "''");
        $roleId = is_numeric($roleId) ? $roleId : -1;
        $sql = "
            SELECT $selectClause FROM $tableName
            WHERE $tableName.page = $page
            AND ($isNullEntityTypeExpression = '' $orTypeWhere)
            AND ($isNullTypeExpression = '' $orStateWhere)
        ";

        if (!empty($roleId)) {
            $sql .= "AND $tableName.role_id = $roleId";
        } else {
            $sql .= "AND $tableName.role_id IS NULL";
        }

        $query = $this->getEntityManager()->createNativeQuery($sql, $rsm);

        $result = $query->getResult();

        return $result;
    }
}
