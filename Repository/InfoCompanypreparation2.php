<?php

namespace TeamSoft\CrmRepositoryBundle\Repository;

use Doctrine\ORM\EntityRepository;

class InfoCompanypreparation2 extends EntityRepository {
    public function findPrevious(int $responsibleId, int $companyId, \DateTime $dateFrom)
    {
        $connection = $this->getEntityManager()->getConnection();
        $sql = "SELECT t.id FROM info_task t
            INNER JOIN info_taskstate ts ON ts.id = t.taskstate_id AND ts.isclosed = 1 AND ts.isfinish = 1  
            WHERE t.datefrom < :datefrom AND t.company_id = :companyId AND t.responsible_id = :responsibleId 
            AND EXISTS (SELECT 1 FROM info_companypreparation2 WHERE task_id = t.id) 
            ORDER BY t.datefrom desc
        ";
        $sql = $this->_em->getConnection()->getDatabasePlatform()->modifyLimitQuery($sql, 1);
        $statement = $connection->prepare($sql);
        $statement->bindValue(':responsibleId', $responsibleId, \PDO::PARAM_INT);
        $statement->bindValue(':companyId', $companyId, \PDO::PARAM_INT);
        $statement->bindValue(':datefrom', $dateFrom->format('Y-m-d H:i:s'), \PDO::PARAM_STR);
        $taskId = $statement->executeQuery()->fetchOne();
        return $this->findBy(['task' => $taskId]);
    }
}
