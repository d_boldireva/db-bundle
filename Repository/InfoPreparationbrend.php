<?php

namespace TeamSoft\CrmRepositoryBundle\Repository;

use Doctrine\ORM\EntityRepository;

class InfoPreparationbrend extends EntityRepository {
    /**
     * @param array $filter
     * @param array $orderBy
     * @param int $offset
     * @param int $limit
     * @param bool $filter_existing
     * @return mixed
     */
    public function findWithReportsByFilter($filter = [], $orderBy = [], $offset = null, $limit = null, $filter_existing = false){
        $queryBuilder = $this->createQueryBuilder('brand');

        $queryBuilder
            ->distinct(true)
            ->select('brand');

        if($offset){
            $queryBuilder->setFirstResult($offset);
        }

        if($limit){
            $queryBuilder->setMaxResults($limit);
        }

        foreach ($orderBy as $attribute => $direction) {
            $queryBuilder->addOrderBy("brand.{$attribute}", $direction);
        }

        if($filter_existing){
            $queryBuilder
                ->innerJoin(\TeamSoft\CrmRepositoryBundle\Entity\VwTaskfileimage::class,$tfiAlias = 'tfi','WITH',"{$tfiAlias}.brand = brand")
                ->innerJoin("{$tfiAlias}.subj",$tfgAlias = 'tfg')
                ->innerJoin("{$tfgAlias}.subj",$reportAlias = 'report');

            if($company = $filter['company'] ?? null){
                $queryBuilder
                    ->andWhere($queryBuilder->expr()->in("{$reportAlias}.company",':company'))
                    ->setParameter(':company',$company);
            }

            if($contact = $filter['contact'] ?? null){
                $queryBuilder
                    ->andWhere($queryBuilder->expr()->in("{$reportAlias}.contact",':contact'))
                    ->setParameter(':contact',$contact);
            }

            if($net = $filter['net'] ?? null){
                $queryBuilder
                    ->innerJoin("{$reportAlias}.company",'company')
                    ->andWhere($queryBuilder->expr()->in('company.centerId',':net'))
                    ->setParameter(':net',$net);
            }

            if($main = $filter['main'] ?? null){
                $queryBuilder
                    ->innerJoin("{$reportAlias}.company",'company')
                    ->andWhere($queryBuilder->expr()->in('company.main',':main'))
                    ->setParameter(':main',$main);
            }

            if($responsible = $filter['responsible'] ?? null){
                $queryBuilder
                    ->andWhere($queryBuilder->expr()->in("{$reportAlias}.responsible",':responsible'))
                    ->setParameter(':responsible',$responsible);
            }

            if($tasktype = $filter['tasktype'] ?? null){
                $queryBuilder
                    ->andWhere($queryBuilder->expr()->in("{$reportAlias}.tasktype",':tasktype'))
                    ->setParameter(':tasktype',$tasktype);
            }

            if($taskstate = $filter['taskstate'] ?? null){
                $queryBuilder
                    ->andWhere($queryBuilder->expr()->in("{$reportAlias}.taskstate",':taskstate'))
                    ->setParameter(':taskstate',$taskstate);
            }

            if($phototype = $filter['phototype'] ?? null){
                $phototype = array_map(function(\TeamSoft\CrmRepositoryBundle\Entity\InfoCustomdictionaryvalue $customdictionaryvalue){
                    return $customdictionaryvalue->getName();
                },$phototype);

                $queryBuilder
                    ->andWhere($queryBuilder->expr()->in("{$tfgAlias}.name",':phototype'))
                    ->setParameter(':phototype',$phototype);
            }

            if($phototostate = $filter['photostate'] ?? null){
                $queryBuilder
                    ->andWhere($queryBuilder->expr()->in("{$tfiAlias}.photostate",':photostate'))
                    ->setParameter(':phototostate',$phototostate);
            }

            if($photoservicetype = $filter['photoservicetype'] ?? null){
                $queryBuilder
                    ->andWhere($queryBuilder->expr()->in("{$tfiAlias}.photoservicetype",':photoservicetype'))
                    ->setParameter(':photoservicetype',$photoservicetype);
            }

            if($whatsinside = $filter['whatsinside'] ?? null){
                $queryBuilder
                    ->andWhere($queryBuilder->expr()->in("{$tfiAlias}.whatsinside",':whatsinside'))
                    ->setParameter(':whatsinside',$whatsinside);
            }

            if($specialization = $filter['specialization'] ?? null){
                $queryBuilder
                    ->innerJoin("{$reportAlias}.contact",'contact')
                    ->andWhere($queryBuilder->expr()->in('contact.specialization',':specialization'))
                    ->setParameter(':specialization',$specialization);
            }

            if($regionpart = $filter['regionpart'] ?? null){
                $subquery_company = $this->getEntityManager()->getRepository(\TeamSoft\CrmRepositoryBundle\Entity\InfoCompany::class)->createQueryBuilder('company');
                $subquery_company
                    ->innerJoin('company.region','company_region')
                    ->innerJoin('company_region.regionparts','company_regionpart')
                    ->andWhere($subquery_company->expr()->eq('company',"{$reportAlias}.company"))
                    ->andWhere($subquery_company->expr()->in('company_regionpart',':regionpart'))
                    ->setMaxResults(1);

                $subquery_contact = $this->getEntityManager()->getRepository(\TeamSoft\CrmRepositoryBundle\Entity\InfoContact::class)->createQueryBuilder('contact');
                $subquery_contact
                    ->innerJoin('contact.region','contact_region')
                    ->innerJoin('contact_region.regionparts','contact_regionparts')
                    ->andWhere($subquery_company->expr()->eq('contact',"{$reportAlias}.contact"))
                    ->andWhere($subquery_contact->expr()->in('contact_regionparts',':regionpart'))
                    ->setMaxResults(1);

                $queryBuilder
                    ->andWhere($queryBuilder->expr()->orX()->addMultiple([
                        $queryBuilder->expr()->exists($subquery_company),
                        $queryBuilder->expr()->exists($subquery_contact)
                    ]))
                    ->setParameter(':regionpart',$regionpart);
            }

            if($region = $filter['region'] ?? null){
                $subquery_company = $this->getEntityManager()->getRepository(\TeamSoft\CrmRepositoryBundle\Entity\InfoCompany::class)->createQueryBuilder('company');
                $subquery_company
                    ->andWhere($subquery_company->expr()->eq('company',"{$reportAlias}.company"))
                    ->andWhere($subquery_company->expr()->in('company.region',':region'))
                    ->setMaxResults(1);

                $subquery_contact = $this->getEntityManager()->getRepository(\TeamSoft\CrmRepositoryBundle\Entity\InfoContact::class)->createQueryBuilder('contact');
                $subquery_contact
                    ->andWhere($subquery_contact->expr()->eq('contact',"{$reportAlias}.contact"))
                    ->andWhere($subquery_contact->expr()->in('contact.region',':region'));

                $queryBuilder
                    ->andWhere($queryBuilder->expr()->orX()->addMultiple([
                        $queryBuilder->expr()->exists($subquery_company),
                        $queryBuilder->expr()->exists($subquery_contact)
                    ]))
                    ->setParameter(':region',$region);
            }

            if($city = $filter['city'] ?? null){
                $subquery_company = $this->getEntityManager()->getRepository(\TeamSoft\CrmRepositoryBundle\Entity\InfoCompany::class)->createQueryBuilder('company');
                $subquery_company
                    ->andWhere($subquery_company->expr()->eq('company',"{$reportAlias}.company"))
                    ->andWhere($subquery_company->expr()->in('company.city',':city'))
                    ->setMaxResults(1);

                $subquery_contact = $this->getEntityManager()->getRepository(\TeamSoft\CrmRepositoryBundle\Entity\InfoContact::class)->createQueryBuilder('contact');
                $subquery_contact
                    ->andWhere($subquery_company->expr()->eq('contact',"{$reportAlias}.company"))
                    ->andWhere($subquery_company->expr()->in('contact.city',':city'))
                    ->setMaxResults(1);

                $queryBuilder->andWhere($queryBuilder->expr()->orX()->addMultiple([
                    $queryBuilder->expr()->exists($subquery_company),
                    $queryBuilder->expr()->exists($subquery_contact)
                ]))
                    ->setParameter(':city',$city);
            }
        }

        return $queryBuilder->getQuery()->getResult();
    }
} 