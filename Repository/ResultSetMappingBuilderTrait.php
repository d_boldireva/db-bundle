<?php

namespace TeamSoft\CrmRepositoryBundle\Repository;

use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Mapping\ClassMetadataInfo;

trait ResultSetMappingBuilderTrait
{
    protected function getRenamedColumns(EntityManagerInterface $em, string $className, string $prefix)
    {
        $columnAlias = [];
        $class = $em->getClassMetadata($className);

        foreach ($class->getColumnNames() as $columnName) {
            $columnAlias[$columnName] = "$prefix$columnName";
        }

        foreach ($class->associationMappings as $associationMapping) {
            if ($associationMapping['isOwningSide'] && $associationMapping['type'] & ClassMetadataInfo::TO_ONE) {
                foreach ($associationMapping['joinColumns'] as $joinColumn) {
                    $columnName = $joinColumn['name'];
                    $columnAlias[$columnName] = "$prefix$columnName";
                }
            }
        }

        return $columnAlias;
    }
}
