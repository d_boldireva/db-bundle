<?php

namespace TeamSoft\CrmRepositoryBundle\Repository;

use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use TeamSoft\CrmRepositoryBundle\Entity\InfoFilterDictionary;

class InfoFilterDictionaryRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, InfoFilterDictionary::class);
    }
}
