<?php

namespace TeamSoft\CrmRepositoryBundle\Repository;

use Doctrine\ORM\EntityRepository;
use TeamSoft\CrmRepositoryBundle\Entity\InfoClmgroup as InfoClmgroupModel;

/**
 * Class InfoClmgroup
 * @package TeamSoft\CrmRepositoryBundle\Repository
 * @see \TeamSoft\CrmRepositoryBundle\Entity\InfoClmgroup
 */
class InfoClmgroup extends EntityRepository
{
    public function findRoot(): InfoClmgroupModel
    {
        return $this->createQueryBuilder('c')->where('c.root = 1')->getQuery()->getOneOrNullResult();
    }
}
