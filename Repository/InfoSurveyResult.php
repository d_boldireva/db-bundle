<?php

namespace TeamSoft\CrmRepositoryBundle\Repository;

use Doctrine\ORM\EntityRepository;

/**
 * Class InfoSurveyResult
 * @see \TeamSoft\CrmRepositoryBundle\Entity\InfoSurveyResult
 * @package TeamSoft\CrmRepositoryBundle\Repository
 */
class InfoSurveyResult extends EntityRepository
{
    public function removeByTaskAndSurvey(\TeamSoft\CrmRepositoryBundle\Entity\InfoTask $task, \TeamSoft\CrmRepositoryBundle\Entity\InfoSurvey $survey): void
    {
        $this->createQueryBuilder('r')
            ->where('r.task=:taskId AND r.survey=:surveyId')
            ->setParameters([
            'taskId' => $task->getId(),
            'surveyId' => $survey->getId(),
        ])
        ->delete()
        ->getQuery()
        ->execute();
    }
}
