<?php

namespace TeamSoft\CrmRepositoryBundle\Repository;

use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\Query\ResultSetMappingBuilder;
use Doctrine\Persistence\ManagerRegistry;
use TeamSoft\CrmRepositoryBundle\Entity\Sales\SynRegion as SynRegionEntity;

class SynRegion extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, SynRegionEntity::class);
    }

    /**
     * @deprecated usages not found
     * @param \DateTime $date
     * @return int|mixed|string
     */
    function findForTlsByDate(\DateTime $date) {

        $rsm = new ResultSetMappingBuilder($this->_em);
        $rsm->addRootEntityFromClassMetadata($this->_entityName, 'sr', array(), ResultSetMappingBuilder::COLUMN_RENAMING_INCREMENT);
        $selectClause = $rsm->generateSelectClause();

        $sql = "
            SELECT $selectClause FROM syn_region sr
            WHERE sr.region_id IS NULL
        ";

        $query = $this->getEntityManager()->createNativeQuery($sql, $rsm);

        return $query->getResult();
    }
}
