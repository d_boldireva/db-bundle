<?php

namespace TeamSoft\CrmRepositoryBundle\Repository;

use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use Doctrine\ORM\Query;
use TeamSoft\CrmRepositoryBundle\Service\DatabasePlatform;
use TeamSoft\CrmRepositoryBundle\Entity\InfoTeamlocationClustered as InfoTeamlocationClusteredEntity;
use TeamSoft\CrmRepositoryBundle\Entity\InfoUser as InfoUserEntity;

class InfoTeamlocationClustered extends ServiceEntityRepository {

    private $dp;

    public function __construct(ManagerRegistry $registry, DatabasePlatform $dp)
    {
        parent::__construct($registry, InfoTeamlocationClusteredEntity::class);
        $this->dp = $dp;
    }

    public function findByFilter(array $filter, $hydrationMode = Query::HYDRATE_OBJECT) {
        $rsm = $this->createResultSetMappingBuilder('tlc');
        $rsm->addJoinedEntityFromClassMetadata(InfoUserEntity::class, 'u', 'tlc', 'user');
        $selectClause = $rsm->generateSelectClause();

        $sql = "SELECT $selectClause\nFROM info_teamlocation_clustered tlc\nINNER JOIN info_user u on tlc.user_id = u.id\n";


        $userId = (isset($filter['userId']) && is_numeric($filter['userId'])) ? $filter['userId'] : -1;
        $sql .= ' WHERE  u.id = ' . $userId;

        $dateStart = $filter['dateStart'];
        $dateEnd = $filter['dateEnd'];
        $gpsDateExpression = $this->dp->getDateTimeWithGmtOffsetExpression('tlc.gps_date', 'tlc.gmt_offset');

        if ($dateStart instanceof \DateTime && $dateEnd instanceof \DateTime) {
            $dateStartString = $dateStart->format('Y-m-d H:i:s');
            $dateEndString = $dateEnd->format('Y-m-d H:i:s');
            $sql .= " AND ($gpsDateExpression BETWEEN '$dateStartString' AND '$dateEndString')";
        } else if ($dateStart instanceof \DateTime) {
            $dateStartString = $dateStart->format('Y-m-d H:i:s');
            $sql .= " AND ($gpsDateExpression >= '$dateStartString')";
        } else if ($dateEnd instanceof \DateTime) {
            $dateEndString = $dateEnd->format('Y-m-d H:i:s');
            $sql .= " AND ($gpsDateExpression <= '$dateEndString')";
        }

        if (isset($filter['ignoredGpsAccuracyLessThan']) && $filter['ignoredGpsAccuracyLessThan'] > 0) {
            $sql .= "AND tlc.accuracy < " . $filter['ignoredGpsAccuracyLessThan'];
        }

        $sql .= ' AND latitude IS NOT NULL AND longitude IS NOT NULL';

        $sql .= ' ORDER BY tlc.gps_date';

        return $this->getEntityManager()->createNativeQuery($sql, $rsm)->getResult($hydrationMode);
    }
}
