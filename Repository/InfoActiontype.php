<?php

namespace TeamSoft\CrmRepositoryBundle\Repository;

use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query\ResultSetMappingBuilder;

class InfoActiontype extends EntityRepository {

    public function findByFilter(array $filter, int $roleId = null, int $languageId = null)
    {
        $tableName = $this->getClassMetadata()->getTableName();

        $joinParts = ["INNER JOIN info_actiontypebyrole on info_actiontype.id = info_actiontypebyrole.actiontype_id"];
        $whereParts = [];

        if ($roleId) {
            $whereParts[] = "info_actiontypebyrole.role_id = $roleId";
        }

        if ($languageId) {
            $whereParts[] = "($tableName.language_id IS NULL OR $tableName.language_id = $languageId)";
        }

        $rsm = new ResultSetMappingBuilder($this->_em);
        $rsm->addRootEntityFromClassMetadata($this->_entityName, $tableName, array(), ResultSetMappingBuilder::COLUMN_RENAMING_INCREMENT);

        $selectClause = $rsm->generateSelectClause();

        $join = implode(' ', $joinParts);
        $where = count($whereParts) ? 'WHERE ' . implode(' AND ', $whereParts) : '';

        $sql = "
            SELECT $selectClause FROM $tableName                       
            $join 
            $where
        ";

        $query = $this->getEntityManager()->createNativeQuery($sql, $rsm);
        return $query->getResult();
    }
}
