<?php

namespace TeamSoft\CrmRepositoryBundle\Repository\MCM;

use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use TeamSoft\CrmRepositoryBundle\Entity\InfoMcmfile;

class InfoMcmfileRepository  extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, InfoMcmfile::class);
    }

    /**
     * @return array|null
     */
    public function getAllWithMaxContentfileId()
    {
        $connection = $this->_em->getConnection();
        $sql = "
            select c.id, fsize, height, name, width, max(cf.id) as max_contentfile_id
            from info_mcmfile as c
            left join info_mcmcontentfile as cf on cf.mcmfile_id = c.id
            where c.id is not null
            group by c.id, fsize, height, name, width
        ";

        $statement = $connection->prepare($sql);
        return $statement->executeQuery()->fetchAllAssociative();
    }
}
