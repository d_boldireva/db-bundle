<?php

namespace TeamSoft\CrmRepositoryBundle\Repository;

use Doctrine\DBAL\Connection;
use Doctrine\ORM\Query;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\Query\ResultSetMappingBuilder;
use Doctrine\Persistence\ManagerRegistry;
use TeamSoft\CrmRepositoryBundle\Service\DatabasePlatform;
use TeamSoft\CrmRepositoryBundle\Service\Options;
use TeamSoft\CrmRepositoryBundle\Entity\InfoRegion as InfoRegionEntity;
use TeamSoft\CrmRepositoryBundle\Entity\InfoUser;

class InfoRegion extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, InfoRegionEntity::class);
    }

    /**
     * selects all districts, which were used in info_contact
     * @return array
     */
    public function getDistrictsUsedInContacts()
    {
        $tableName = $this->getClassMetadata()->getTableName();

        $sql = "
            SELECT DISTINCT info_region.id, info_region.Name
            FROM info_region
            INNER JOIN info_contact ON info_contact.Region_id = info_region.id
        ";

        $rsm = new ResultSetMappingBuilder($this->_em);
        $rsm->addRootEntityFromClassMetadata($this->_entityName, $tableName);
        $rsm->addEntityResult($this->_entityName, $tableName);

        $query = $this->getEntityManager()->createNativeQuery($sql, $rsm);

        return $query->getResult();
    }

    /**
     * selects all regions, which were used in info_contact
     * @return array
     */
    public function getRegionsUsedInContacts()
    {
        $tableName = $this->getClassMetadata()->getTableName();

        $sql = "
            SELECT DISTINCT info_regionpart.id, info_regionpart.Name
            FROM info_region
            INNER JOIN info_regioninregionpart ON info_regioninregionpart.region_id = info_region.id
            INNER JOIN info_regionpart ON info_regionpart.id = info_regioninregionpart.regionpart_id
            INNER JOIN info_contact ON info_contact.Region_id = info_region.id
        ";

        $rsm = new ResultSetMappingBuilder($this->_em);
        $rsm->addRootEntityFromClassMetadata($this->_entityName, $tableName);
        $rsm->addEntityResult($this->_entityName, $tableName);

        $query = $this->getEntityManager()->createNativeQuery($sql, $rsm);

        return $query->getResult();
    }

    public function findAllAscendingOrderedByName()
    {
        return $this->createQueryBuilder('r')
            ->orderBy('r.name', 'ASC')
            ->getQuery()->getResult();
    }

    /**
     * @param array $filter
     * @param array $orderBy
     * @param int $offset
     * @param int $limit
     * @param bool $filter_existing
     * @return mixed
     */
    public function findWithReportsByFilter($filter = [], $orderBy = [], $offset = null, $limit = null, $filter_existing = false)
    {
        $this->findAll();
        $this->_em->getRepository(\TeamSoft\CrmRepositoryBundle\Entity\InfoRegionpart::class)->findAll();

        $queryBuilder = $this->createQueryBuilder('region');

        $queryBuilder
            ->innerJoin('region.regionparts', 'regionpart')
            ->distinct(true)
            ->select('partial region.{id,name}, partial regionpart.{id}');

        if ($offset) {
            $queryBuilder->setFirstResult($offset);
        }

        if ($limit) {
            $queryBuilder->setMaxResults($limit);
        }

        foreach ($orderBy as $attribute => $direction) {
            $queryBuilder->addOrderBy("region.{$attribute}", $direction);
        }

        if ($responsible = $filter['responsible'] ?? null) {
            $subqueryBuilder = $this->_em->getRepository(\TeamSoft\CrmRepositoryBundle\Entity\InfoUser::class)
                ->createQueryBuilder('user');
            $subqueryBuilder
                ->innerJoin('user.regionCollection', 'user_region')
                ->andWhere($subqueryBuilder->expr()->in('user', ':responsible'))
                ->andWhere($subqueryBuilder->expr()->eq('user_region', 'region'))
                ->setParameter(':responsible', $responsible);

            $queryBuilder
                ->andWhere($queryBuilder->expr()->exists($subqueryBuilder));
        }

        if ($regionpart = $filter['regionpart'] ?? null) {
            $queryBuilder
                ->andWhere($queryBuilder->expr()->in('regionpart', ':regionpart'))
                ->setParameter(':regionpart', $regionpart);
        }

        if ($region = $filter['region'] ?? null) {
            $queryBuilder
                ->andWhere($queryBuilder->expr()->in('region', ':region'))
                ->setParameter(':region', $region);
        }

        if ($city = $filter['city'] ?? null) {
            $queryBuilder
                ->innerJoin('region.citiesCollection', 'city')
                ->andWhere($queryBuilder->expr()->in('city', ':city'))
                ->setParameter(':city', $city);
        }

        if (!$filter_existing) {
            if ($company = $filter['company'] ?? null) {
                $queryBuilder
                    ->innerJoin(\TeamSoft\CrmRepositoryBundle\Entity\InfoCompany::class, 'company', 'WITH', 'company.region = region')
                    ->andWhere($queryBuilder->expr()->in('company', ':company'))
                    ->setParameter(':company', $company);
            }

            if ($main = $filter['main'] ?? null) {
                $queryBuilder
                    ->innerJoin(\TeamSoft\CrmRepositoryBundle\Entity\InfoCompany::class, 'company', 'WITH', 'company.region = region')
                    ->andWhere($queryBuilder->expr()->in('company.main', ':main'))
                    ->setParameter(':main', $main);
            }

            if ($net = $filter['net'] ?? null) {
                $queryBuilder
                    ->innerJoin(\TeamSoft\CrmRepositoryBundle\Entity\InfoCompany::class, 'company', 'WITH', 'company.region = region')
                    ->andWhere($queryBuilder->expr()->in('company.centerId', ':net'))
                    ->setParameter(':net', $net);
            }

            if ($contact = $filter['contact'] ?? null) {
                $queryBuilder
                    ->innerJoin(\TeamSoft\CrmRepositoryBundle\Entity\InfoContact::class, 'contact', 'WITH', 'contact.region = region')
                    ->andWhere($queryBuilder->expr()->in('contact', ':contact'))
                    ->setParameter(':contact', $contact);
            }

            if ($specialization = $filter['specialization'] ?? null) {
                $queryBuilder
                    ->innerJoin(\TeamSoft\CrmRepositoryBundle\Entity\InfoContact::class, 'contact', 'WITH', 'contact.region = region')
                    ->andWhere($queryBuilder->expr()->in('contact.specialization', ':spec'))
                    ->setParameter(':spec', $specialization);
            }
        } else {
            $subqueryBuilder = $this->_em->getRepository(\TeamSoft\CrmRepositoryBundle\Entity\InfoTask::class)->createQueryBuilder($reportAlias = 'report');
            $subqueryBuilder
                ->setMaxResults(1)
                ->innerJoin("{$reportAlias}.taskFileGroupCollection", $tfgAlias = 'tfg')
                ->innerJoin("{$tfgAlias}.taskImageCollection", $tfiAlias = 'tfi')
                ->leftJoin("{$reportAlias}.company", "company")
                ->leftJoin("{$reportAlias}.contact", "contact", 'WITH',
                    $queryBuilder->expr()->isNull('contact')
                )
                ->andWhere(
                    $subqueryBuilder->expr()->orX()->addMultiple([
                        $subqueryBuilder->expr()->eq('company.region', 'region'),
                        $subqueryBuilder->expr()->eq('contact.region', 'region')
                    ])
                );

            if ($net = $filter['net'] ?? null) {
                $subqueryBuilder
                    ->andWhere($subqueryBuilder->expr()->in('company.centerId', ':net'))
                    ->setParameter(':net', $net);
            }

            if ($main = $filter['main'] ?? null) {
                $subqueryBuilder
                    ->andWhere($subqueryBuilder->expr()->in('company.main', ':main'))
                    ->setParameter(':main', $main);
            }

            if ($responsible = $filter['responsible'] ?? null) {
                $subqueryBuilder
                    ->andWhere($subqueryBuilder->expr()->in("{$reportAlias}.responsible", ':responsible'))
                    ->setParameter(':responsible', $responsible);
            }

            if ($tasktype = $filter['tasktype'] ?? null) {
                $subqueryBuilder
                    ->andWhere($subqueryBuilder->expr()->in("{$reportAlias}.tasktype", ':tasktype'))
                    ->setParameter(':tasktype', $tasktype);
            }

            if ($taskstate = $filter['taskstate'] ?? null) {
                $subqueryBuilder
                    ->andWhere($subqueryBuilder->expr()->in("{$reportAlias}.taskstate", ':taskstate'))
                    ->setParameter(':taskstate', $taskstate);
            }

            if ($phototype = $filter['phototype'] ?? null) {
                $phototype = array_map(function (\TeamSoft\CrmRepositoryBundle\Entity\InfoCustomdictionaryvalue $customdictionaryvalue) {
                    return $customdictionaryvalue->getName();
                }, $phototype);

                $subqueryBuilder
                    ->andWhere($subqueryBuilder->expr()->in("{$tfgAlias}.name", ':phototype'))
                    ->setParameter(':phototype', $phototype);
            }

            if ($phototostate = $filter['photostate'] ?? null) {
                $subqueryBuilder
                    ->andWhere($subqueryBuilder->expr()->in("{$tfiAlias}.photostate", ':photostate'))
                    ->setParameter(':phototostate', $phototostate);
            }

            if ($photoservicetype = $filter['photoservicetype'] ?? null) {
                $subqueryBuilder
                    ->andWhere($subqueryBuilder->expr()->in("{$tfiAlias}.photoservicetype", ':photoservicetype'))
                    ->setParameter(':photoservicetype', $photoservicetype);
            }

            if ($whatsinside = $filter['whatsinside'] ?? null) {
                $subqueryBuilder
                    ->andWhere($subqueryBuilder->expr()->in("{$tfiAlias}.whatsinside", ':whatsinside'))
                    ->setParameter(':whatsinside', $whatsinside);
            }

            if ($brand = $filter['brand'] ?? null) {
                $subqueryBuilder
                    ->andWhere($subqueryBuilder->expr()->in("{$tfiAlias}.brand", ':brand'))
                    ->setParameter(':brand', $brand);
            }

            if ($specialization = $filter['specialization'] ?? null) {
                $subqueryBuilder
                    ->innerJoin("{$reportAlias}.contact", 'contact')
                    ->andWhere($subqueryBuilder->expr()->in('contact.specialization', ':specialization'))
                    ->setParameter(':specialization', $specialization);
            }

            $queryBuilder
                ->andWhere($queryBuilder->expr()->exists($subqueryBuilder));
        }

        return $queryBuilder->getQuery()->getResult();
    }

    /**
     * Select GMT (TimeZone) from info_region by ids ignore
     * @param int[] $regionIds
     * @return string[]|array
     */
    public function getGMTByRegions(array $regions): array
    {
        $queryBuilder = $this->createQueryBuilder('r');
        $queryBuilder
            ->select(['r.gmt AS name'])
            ->distinct()
            ->where('r.id IN (:ids)')
            ->andWhere('r.gmt IS NOT NULL')
            ->orderBy('name', 'ASC')
            ->setParameter(':ids', $regions, Connection::PARAM_INT_ARRAY);

        return $queryBuilder->getQuery()->getArrayResult();
    }
}
