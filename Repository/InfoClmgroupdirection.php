<?php

namespace TeamSoft\CrmRepositoryBundle\Repository;

use Doctrine\ORM\EntityRepository;

class InfoClmgroupdirection extends EntityRepository
{

    /**
     * @param \TeamSoft\CrmRepositoryBundle\Entity\InfoUser $user
     * @return array
     */

    public function findAvailableForUser (\TeamSoft\CrmRepositoryBundle\Entity\InfoUser $user)
    {

        $queryBuilder = $this->createQueryBuilder('gd');

        $queryBuilder
            ->distinct()
            ->select('g.id', 'g.name')
            ->innerJoin('gd.clmgroup', 'g')
            ->innerJoin('gd.direction', 'ud')
            ->where('ud.user = :userId')
            ->andWhere($queryBuilder->expr()->orX(
                $queryBuilder->expr()->neq('g.root', ':root'),
                $queryBuilder->expr()->isNull('g.root')
            ))
            ->orderBy('g.name')
            ->setParameter('userId', $user->getId())
            ->setParameter('root', 1);

        return $queryBuilder->getQuery()->getResult();

    }
}