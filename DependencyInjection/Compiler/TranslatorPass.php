<?php

namespace TeamSoft\CrmRepositoryBundle\DependencyInjection\Compiler;

use TeamSoft\CrmRepositoryBundle\Translation\Translator;
use Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Reference;
use TeamSoft\CrmRepositoryBundle\Repository\InfoLanguageinterface;

class TranslatorPass implements CompilerPassInterface
{
    private $translatorServiceId;
    private $readerServiceId;
    private $loaderTag;
    private $debugCommandServiceId;
    private $updateCommandServiceId;

    /**
     * @param string $translatorServiceId
     * @param string $readerServiceId
     * @param string $loaderTag
     * @param string $debugCommandServiceId
     * @param string $updateCommandServiceId
     */
    public function __construct(
        string $translatorServiceId = 'translator.default',
        string $readerServiceId = 'translation.reader',
        string $loaderTag = 'translation.loader',
        string $debugCommandServiceId = 'console.command.translation_debug',
        string $updateCommandServiceId = 'console.command.translation_update'
    ) {
        $this->translatorServiceId = $translatorServiceId;
        $this->readerServiceId = $readerServiceId;
        $this->loaderTag = $loaderTag;
        $this->debugCommandServiceId = $debugCommandServiceId;
        $this->updateCommandServiceId = $updateCommandServiceId;
    }

    /**
     * {@inheritdoc}
     */
    public function process(ContainerBuilder $container)
    {
        $loaders = [];
        $loaderRefs = [];
        foreach ($container->findTaggedServiceIds($this->loaderTag, true) as $id => $attributes) {
            $loaderRefs[$id] = new Reference($id);
            $loaders[$id][] = $attributes[0]['alias'];
            if (isset($attributes[0]['legacy-alias'])) {
                $loaders[$id][] = $attributes[0]['legacy-alias'];
            }
        }

        if ($container->hasDefinition($this->readerServiceId)) {
            $definition = $container->getDefinition($this->readerServiceId);
            foreach ($loaders as $id => $formats) {
                foreach ($formats as $format) {
                    $definition->addMethodCall('addLoader', [$format, $loaderRefs[$id]]);
                }
            }
        }

        $definition = $container->findDefinition($this->translatorServiceId);

        $definition
            ->setClass(Translator::class)
            ->addMethodCall("setRepository", [new Reference(InfoLanguageinterface::class)])
        ;

        if (!$container->hasParameter('twig.default_path')) {
            return;
        }

        if ($container->hasDefinition($this->debugCommandServiceId)) {
            $container->getDefinition($this->debugCommandServiceId)->replaceArgument(4, $container->getParameter('twig.default_path'));
        }

        if ($container->hasDefinition($this->updateCommandServiceId)) {
            $container->getDefinition($this->updateCommandServiceId)->replaceArgument(5, $container->getParameter('twig.default_path'));
        }
    }
}
