<?php

namespace TeamSoft\CrmRepositoryBundle\DependencyInjection;

use Symfony\Component\Config\Definition\Builder\ArrayNodeDefinition;
use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;

class Configuration implements ConfigurationInterface
{
    /**
     * {@inheritdoc}
     */
    public function getConfigTreeBuilder(): TreeBuilder
    {
        $treeBuilder = new TreeBuilder('db');
        $rootNode = $treeBuilder->getRootNode();

        $this->addWebServicesSection($rootNode);

        return $treeBuilder;
    }

    private function addWebServicesSection(ArrayNodeDefinition $rootNode)
    {
        $rootNode
            ->children()
                ->arrayNode('web_services')->useAttributeAsKey('name')
                    ->prototype('array')
                        ->children()
                            ->scalarNode('allow_if')->end()
                            ->arrayNode('roles')->prototype('array')->prototype('scalar')->end()->end()->end()
                        ->end()
                    ->end()
                ->end()
            ->end()
        ;
    }
}
