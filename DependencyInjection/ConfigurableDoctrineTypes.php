<?php

namespace TeamSoft\CrmRepositoryBundle\DependencyInjection;

use Doctrine\DBAL\Types\BlobType;
use Doctrine\DBAL\Types\StringType;
use Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use TeamSoft\CrmRepositoryBundle\DBAL\Types\NVarcharImage;
use TeamSoft\CrmRepositoryBundle\DBAL\Types\VarcharImage;

class ConfigurableDoctrineTypes implements CompilerPassInterface
{
    public function process(ContainerBuilder $container)
    {
        $customDoctrineTypes = $container->getParameter('doctrine.dbal.connection_factory.types');

        $customDoctrineTypes['blob_or_string'] = ['class' => null, 'commented' => true];
        $customDoctrineTypes['varchar_or_n_varchar_image_or_string'] = ['class' => null, 'commented' => true];

        if ($container->getParameter('__sql_server_2008')) {
            $customDoctrineTypes['blob_or_string']['class'] = BlobType::class;
            if ($container->hasParameter('database_encoding') && $container->getParameter('database_encoding') === 'unicode') {
                $customDoctrineTypes['varchar_or_n_varchar_image_or_string']['class'] = NVarcharImage::class;
            } else {
                $customDoctrineTypes['varchar_or_n_varchar_image_or_string']['class'] = VarcharImage::class;
            }
        } else {
            $customDoctrineTypes['blob_or_string']['class'] = StringType::class;
            $customDoctrineTypes['varchar_or_n_varchar_image_or_string']['class'] = StringType::class;
        }

        $container->setParameter('doctrine.dbal.connection_factory.types', $customDoctrineTypes);
    }
}