<?php

$pathToFile = __DIR__ .  DIRECTORY_SEPARATOR . 'teamsoft_dbo_info_languageinterface.sql';
$sqlFile = fopen($pathToFile, 'r');

$languages = [
    1 => 'en',
    2 => 'ru',
    3 => 'uk',
];

$content = '';
while (($line = fgets($sqlFile)) !== false) {
    $match = [];
    if (preg_match('/VALUES \((?P<VALUES>.*)\)/', $line, $match)) {
        $values = [];
        if (preg_match('/ {0,1}\'(?P<key>.*)\', {0,2}\'(?P<translation>.*)\', {0,2}(?P<language_id>\d{1,1}), {0,2}\'(?P<domain>.*)\'/', $line, $values)) {
            $content .= sprintf(
                "exec insertLanguageInterface '%s', '%s', N'%s', '%s'",
                $languages[$values['language_id']],
                $values['key'],
                $values['translation'],
                $values['domain']
            ) . PHP_EOL;
        } else {
            var_dump($line);
        }
    }
}

$content .= 'GO' . PHP_EOL;

file_put_contents(__DIR__ .  DIRECTORY_SEPARATOR . 'prepare_' . uniqid() . '.sql', $content);