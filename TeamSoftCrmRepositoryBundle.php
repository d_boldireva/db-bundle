<?php

namespace TeamSoft\CrmRepositoryBundle;

use Doctrine\DBAL\Connection;
use Doctrine\DBAL\DriverManager;
use Doctrine\DBAL\Platforms\PostgreSQL100Platform;
use Doctrine\DBAL\Platforms\SQLServer2008Platform;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Extension\ExtensionInterface;
use Symfony\Component\HttpKernel\Bundle\Bundle;
use TeamSoft\CrmRepositoryBundle\DependencyInjection\ConfigurableDoctrineTypes;
use TeamSoft\CrmRepositoryBundle\DependencyInjection\TeamSoftCrmRepositoryExtension;

class TeamSoftCrmRepositoryBundle extends Bundle
{

    private $connection;
    private $webServices;
    private $languages;

    public function __construct()
    {
        $this->connection = $this->getConnection();
        $this->webServices = $this->getWebServices();
        $this->languages = $this->getLanguages();
    }

    public function getContainerExtension(): ?ExtensionInterface
    {
        return new TeamSoftCrmRepositoryExtension($this->webServices);
    }

    public function build(ContainerBuilder $container)
    {
        parent::build($container);
        $databasePlatform = $this->connection->getDatabasePlatform();
        $container->setParameter('__sql_server_2008', $databasePlatform instanceof SQLServer2008Platform);
        $container->setParameter('__postgre_sql_100', $databasePlatform instanceof PostgreSQL100Platform);
        $container->setParameter('security.role_hierarchy.roles', $this->getRoles());
        $container->addCompilerPass(new ConfigurableDoctrineTypes());

        $container->setParameter('languages', $this->languages);

        $locales = array_map(function ($language) {
            return $language['slug'];
        }, $this->languages);
        $container->setParameter('_locale', 'en');
        $container->setParameter('locales', $locales);
        $container->setParameter('_locales', implode('|', $locales));
    }

    private function getConnection(): Connection
    {
        return DriverManager::getConnection([
            'url' => $_ENV['DATABASE_URL'],
            'driver' => 'pdo_sqlsrv',
            'driverClass' => $_ENV['DATABASE_DRIVER_CLASS'],
        ]);
    }

    /**
     * Generate roles hierarhy
     * @return array
     */
    private function getRoles()
    {
        $rolesHierarchy = [];
        try {
            $sql = "
                SELECT r.code AS role, i.code AS privilege
                FROM info_role r
                INNER JOIN info_roleprivilege ir on r.id = ir.role_id
                INNER JOIN info_serviceprivilege i on ir.privilege_id = i.id
            ";

            $rolePrivileges = $this->connection->fetchAllAssociative($sql);

            $rolesHierarchy = $this->getPrivilegesHierarchy();

            foreach ($rolePrivileges as $rolePrivilege) {
                if (!isset($rolesHierarchy[$rolePrivilege['role']])) {
                    $rolesHierarchy[$rolePrivilege['role']] = [];
                }
                $rolesHierarchy[$rolePrivilege['role']][] = $rolePrivilege['privilege'];
            }

        } catch (\Exception $exception) {
        }

        return $rolesHierarchy;
    }

    private function getPrivilegesHierarchy($serviceId = null)
    {
        $sql = 'SELECT id, code, parent_id FROM info_serviceprivilege';
        if (is_numeric($serviceId)) {
            $sql .= ' WHERE service_id = ' . $serviceId;
        }
        $privileges = $this->connection->fetchAllAssociative($sql);
        return $this->buildPrivilegesHierarchy($privileges, null);
    }

    private function buildPrivilegesHierarchy($privileges, $parentId = null)
    {
        $hierarchy = [];
        foreach ($privileges as $privilege) {
            if ($privilege['parent_id'] === $parentId) {
                $hierarchy[$privilege['code']] = [];
                $childHierarchy = $this->buildPrivilegesHierarchy($privileges, $privilege['id']);
                foreach ($childHierarchy as $childKey => $childValues) {
                    $childHierarchy[$childKey][] = $privilege['code'];
                }
                $hierarchy = array_merge($hierarchy, $childHierarchy);
            }
        }
        return $hierarchy;
    }

    private function getWebServices()
    {
        $webServices = [];
        try {
            $dp = $this->connection->getDatabasePlatform();
            $codeExpression = $dp->getConcatExpression("'ROLE_ACCESS_TO_'", "UPPER(identifier)");
            $services = $this->connection->fetchAllAssociative("
                SELECT s.id, s.identifier, MIN(sp.code) AS privilege
                FROM info_service s
                LEFT JOIN info_serviceprivilege sp ON sp.service_id = s.id 
                AND sp.code = $codeExpression
                GROUP BY s.id, s.identifier
            ");

            foreach ($services as $key => $service) {
                $privilegesHierarchy = $this->getPrivilegesHierarchy($service['id']);
                if (!($privilege = $service['privilege'])) {
                    $privilege = 'ROLE_ACCESS_TO_' . strtoupper($service['identifier']);
                }

                $webServices[$service['identifier']] = [
                    'allow_if' => "is_granted(\"$privilege\") == true",
                    'roles' => $privilegesHierarchy,
                ];
            }
        } catch (\Exception $exception) {
        }

        return $webServices;
    }

    private function getLanguages()
    {
        $languages = [];
        try {
            $languages = $this->connection->fetchAllAssociative('select * from info_language where COALESCE(enabled, 0) = 1 or COALESCE(isdefault, 0) = 1');
        } catch (\Exception $exception) {
        }
        return $languages;
    }
}
