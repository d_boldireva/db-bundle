<?php

namespace TeamSoft\CrmRepositoryBundle\Exception;

class CredentialsExpiredException extends AccountStatusException
{

    public function getMessageKey()
    {
        return 'Credentials have expired.';
    }
}
