<?php

namespace TeamSoft\CrmRepositoryBundle\Exception;

use Symfony\Component\Security\Core\Exception\AuthenticationException as BaseAuthenticationException;
use Symfony\Component\Security\Core\User\UserInterface;


abstract class AccountStatusException extends BaseAuthenticationException
{
    private UserInterface $user;

    public function getUser(): UserInterface
    {
        return $this->user;
    }

    public function setUser(UserInterface $user)
    {
        $this->user = $user;
    }

    public function __serialize(): array
    {
        return [$this->user, parent::__serialize()];
    }

    public function __unserialize(array $data): void
    {
        [$this->user, $parentData] = $data;
        $parentData = \is_array($parentData) ? $parentData : unserialize($parentData);
        parent::__unserialize($parentData);
    }
}
