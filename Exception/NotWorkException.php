<?php

namespace TeamSoft\CrmRepositoryBundle\Exception;


class NotWorkException extends AccountStatusException
{
    public function getMessageKey()
    {
        return "User doesn't work.";
    }
}
