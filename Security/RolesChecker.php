<?php

namespace TeamSoft\CrmRepositoryBundle\Security;


use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface;
use Symfony\Component\Security\Http\AccessMapInterface;
use TeamSoft\CrmRepositoryBundle\Entity\InfoContact;
use TeamSoft\CrmRepositoryBundle\Entity\InfoContactowner;
use TeamSoft\CrmRepositoryBundle\Entity\InfoUser;
use TeamSoft\CrmRepositoryBundle\Entity\SecureEntityInterface;
use TeamSoft\CrmRepositoryBundle\Service\Options;

class RolesChecker
{
    private $accessMap;
    private $authorizationChecker;
    private $requestStack;
    private $tokenStorage;
    private $em;
    /**
     * @var Options
     */
    private $options;

    public function __construct(
        AccessMapInterface $accessMap,
        AuthorizationCheckerInterface $authorizationChecker,
        RequestStack $requestStack,
        TokenStorageInterface $tokenStorage,
        EntityManagerInterface $em,
        Options $options
    )
    {
        $this->accessMap = $accessMap;
        $this->authorizationChecker = $authorizationChecker;
        $this->requestStack = $requestStack;
        $this->tokenStorage = $tokenStorage;
        $this->em = $em;
        $this->options = $options;

    }

    public function isGrantedForUrl($url, $server)
    {
        $request = Request::create($url, 'GET', [], [], [], $server);

        $roles = $this->accessMap->getPatterns($request)[0];

        $roles = $roles ?: [];
        $isGranted = (count($roles) == 0);
        foreach ($roles as $role) {
            if ($this->authorizationChecker->isGranted($role, $this->requestStack->getCurrentRequest())) {
                $isGranted = true;
                break;
            }
        }

        return $isGranted;
    }

    public function isGrantedForEntity(SecureEntityInterface $entity)
    {
        $isGranted = false;
        if ($entityRoles = $entity->getRoles()) {
            foreach ($entityRoles as $entityRole) {
                if ($this->authorizationChecker->isGranted($entityRole->getCode())) {
                    $isGranted = true;
                    break;
                }
            }
        }
        return $isGranted;
    }

    private function getUser()
    {
        if (null === $token = $this->tokenStorage->getToken()) {
            return;
        }

        if (!\is_object($user = $token->getUser())) {
            return;
        }

        return $user;
    }

    public function isGrantedForContact(InfoContact $contact): bool
    {
        /** @var InfoUser $user */
        $user = $this->getUser();
        if (!$user) {
            return false;
        }

        if ($user->isAdmin()) {
            return true;
        }

        if (!$contact->getContacttype() || !$contact->getContacttype()->getIsBlogger()) {
            return true;
        }

        return $this
            ->em
            ->getRepository(InfoContactowner::class)
            ->contactOwnerExist($contact->getId(), $user->getId());
    }
}
