<?php

namespace TeamSoft\CrmRepositoryBundle\Security;

use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\Security\Core\Exception\AccountExpiredException;
use Symfony\Component\Security\Core\Exception\BadCredentialsException;
use Symfony\Component\Security\Core\Exception\DisabledException;
use Symfony\Component\Security\Core\Exception\LockedException;
use Symfony\Component\Security\Core\User\UserCheckerInterface;
use Symfony\Component\Security\Core\User\UserInterface;
use TeamSoft\CrmRepositoryBundle\Entity\AdvancedUserInterface;
use TeamSoft\CrmRepositoryBundle\Exception\NotWorkException;
use TeamSoft\CrmRepositoryBundle\Exception\CredentialsExpiredException;
use TeamSoft\CrmRepositoryBundle\Service\Options;

class UserChecker implements UserCheckerInterface
{

    private $requestStack;

    public function __construct(RequestStack $requestStack, Options $options)
    {
        $this->requestStack = $requestStack;
        $this->options = $options;
    }

    public function checkPreAuth(UserInterface $user)
    {
        if (!$user instanceof AdvancedUserInterface) {
            return;
        }

        if (!$user->isEnabled()) {
            $ex = new DisabledException('User account is disabled.');
            $ex->setUser($user);
            throw $ex;
        }

        if (!$user->isAccountNonLocked()) {
            $ex = new LockedException('User account is locked.');
            $ex->setUser($user);
            throw $ex;
        }

        if (!$user->isAccountNonExpired()) {
            $ex = new AccountExpiredException('User account has expired.');
            $ex->setUser($user);
            throw $ex;
        }

        $request = $this->requestStack->getCurrentRequest();
        if ($user->allowedHost() && $request->getHost() && $user->allowedHost() !== $request->getHost()) {
            throw new BadCredentialsException('Bad credentials.');
        }

        if (!$user->isWork()) {
            $ex = new NotWorkException("User doesn't work.");
            $ex->setUser($user);
            throw $ex;
        }
    }

    public function checkPostAuth(UserInterface $user)
    {
        $daysIntervalForPasswordChange = (int)$this->options->get('daysIntervalForPasswordChange');
        if (!$user->isCredentialsNonExpired($daysIntervalForPasswordChange)) {
            $ex = new CredentialsExpiredException('User credentials have expired.');
            $ex->setUser($user);
            throw $ex;
        }
    }
}
