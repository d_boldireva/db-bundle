<?php

namespace TeamSoft\CrmRepositoryBundle\Security;

use Symfony\Component\Security\Core\Authorization\Voter\VoterInterface;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\Voter\RoleVoter as SymfonyRoleVoter;

/**
 * Class RoleVoter
 */
class RoleVoter extends SymfonyRoleVoter
{
    protected $prefix;


    /**
     * RoleVoter constructor.
     *
     * @param string $prefix
     */
    public function __construct(string $prefix = '')
    {
        $this->prefix = $prefix;

        parent::__construct($prefix);
    }


    /**
     * @param TokenInterface $token
     * @param mixed          $subject
     * @param array          $attributes
     *
     * @return int
     */
    public function vote(TokenInterface $token, $subject, array $attributes)
    {
        $result = VoterInterface::ACCESS_ABSTAIN;

        $roles = $this->extractRoles($token);

        foreach ( $attributes as $attribute ) {
            if (! \is_string($attribute)) {
                continue;
            }

            if ($this->prefix && ( 0 !== strpos($attribute, $this->prefix) )) {
                continue;
            }

            if ('ROLE_PREVIOUS_ADMIN' === $attribute) {
                trigger_deprecation('symfony/security-core', '5.1', 'The ROLE_PREVIOUS_ADMIN role is deprecated and will be removed in version 6.0, use the IS_IMPERSONATOR attribute instead.');
            }

            $result = VoterInterface::ACCESS_DENIED;

            foreach ( $roles as $role ) {
                if ($attribute === $role) {
                    return VoterInterface::ACCESS_GRANTED;
                }
            }
        }

        return $result;
    }
}
