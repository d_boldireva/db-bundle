<?php

namespace TeamSoft\CrmRepositoryBundle\Security;

use Symfony\Component\HttpFoundation\RequestMatcherInterface;
use Symfony\Component\HttpFoundation\Request;

class ApiTokenFirewallMatcher implements RequestMatcherInterface
{

    public function matches(Request $request): bool
    {
        $credentialsInHeader = $request->headers->has('Authorization')
            && 0 === strpos($request->headers->get('Authorization'), 'Bearer ');
        $credentialsInUrl = $request->query->has('access_token')
            && !!$request->query->get('access_token');
        return $credentialsInHeader || $credentialsInUrl;
    }

}
