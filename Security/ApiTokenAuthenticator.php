<?php

namespace TeamSoft\CrmRepositoryBundle\Security;

use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Exception\AuthenticationException;
use Symfony\Component\Security\Http\Authenticator\AbstractAuthenticator;
use Symfony\Component\Security\Http\Authenticator\Passport\Badge\UserBadge;
use Symfony\Component\Security\Http\Authenticator\Passport\Passport;
use Symfony\Component\Security\Http\Authenticator\Passport\SelfValidatingPassport;
use Symfony\Component\Security\Http\EntryPoint\AuthenticationEntryPointInterface;
use TeamSoft\CrmRepositoryBundle\Repository\PoApiTokenRepository;

class ApiTokenAuthenticator extends AbstractAuthenticator implements AuthenticationEntryPointInterface
{

    private $credentialsInHeader;

    private $credentialsInUrl;

    private $apiTokenRepository;

    public function __construct(PoApiTokenRepository $apiTokenRepository)
    {
        $this->apiTokenRepository = $apiTokenRepository;
    }

    public function supports(Request $request): bool
    {
        $this->credentialsInHeader = $request->headers->has('Authorization')
            && 0 === strpos($request->headers->get('Authorization'), 'Bearer ');
        $this->credentialsInUrl = $request->query->has('access_token')
            && !!$request->query->get('access_token');
        return $this->credentialsInHeader || $this->credentialsInUrl;
    }

    public function authenticate(Request $request): Passport
    {
        $apiToken = $this->getApiToken($request);

        $passport = new SelfValidatingPassport(
            new UserBadge($apiToken, [$this, 'getUser'])
        );

        return $passport;
    }

    public function getUser($token)
    {
        $apiToken = $this->apiTokenRepository->findOneBy([
            'token' => $token
        ]);

        if (!$apiToken) {
            throw new AuthenticationException('Invalid API Token');
        }

        if ($apiToken->isExpired()) {
            throw new AuthenticationException('Token expired');
        }

        return $apiToken->getUser();
    }

    public function onAuthenticationSuccess(Request $request, TokenInterface $token, string $providerKey): ?Response
    {
        return null;
    }

    public function onAuthenticationFailure(Request $request, AuthenticationException $exception): ?Response
    {
        return new JsonResponse([
            'message' => $exception->getMessage()
        ], 401);
    }

    public function start(Request $request, AuthenticationException $authException = null): Response
    {
        return new Response('', 401);
    }

    private function getApiToken(Request $request)
    {
        if ($this->credentialsInHeader) {
            $authorizationHeader = $request->headers->get('Authorization');
            return substr($authorizationHeader, 7);
        } else if ($this->credentialsInUrl) {
            return $request->query->get('access_token');
        }
    }
}
