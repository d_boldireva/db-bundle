<?php
declare(strict_types=1);

namespace TeamSoft\CrmRepositoryBundle\Utils;

class DateHelper
{
    /**
     * @see https://www.php.net/manual/ru/datetime.formats.relative.php
     * @see https://www.php.net/manual/ru/datetime.format.php
     * @see
     * @param string $date
     * @param bool $asImmutable
     * @param bool $catchException
     * @return \DateTimeInterface|null
     * @throws \Exception
     */
    public static function toDate($date = 'now', bool $asImmutable = false, bool $catchException = true): ?\DateTimeInterface
    {
        try {
            $timeZone = new \DateTimeZone('UTC');
            if ($date instanceof \DateTimeInterface) {
                $dt = clone $date;
            } elseif (is_numeric($date)) {
                $dt = new \DateTime('@' . $date, $timeZone);
            } else {
                $dt = new \DateTime($date, $timeZone);
            }

            if ($asImmutable && !($dt instanceof \DateTimeImmutable)) {
                $dt = \DateTimeImmutable::createFromMutable($dt);
            }

            return $dt;
        } catch (\Throwable $exception) {
            if (!$catchException) {
                throw $exception;
            }

            return null;
        }
    }
    public static function getTimeZones(): array
    {
        return timezone_identifiers_list();
    }
}
