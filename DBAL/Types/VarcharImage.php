<?php
namespace TeamSoft\CrmRepositoryBundle\DBAL\Types;

use Doctrine\DBAL\Types\Type;
use Doctrine\DBAL\Platforms\AbstractPlatform;

class VarcharImage extends Type
{
    public function getSQLDeclaration(array $fieldDeclaration, AbstractPlatform $platform): string
    {
        return $platform->getBlobTypeDeclarationSQL($fieldDeclaration);
    }

    public function canRequireSQLConversion(): bool
    {
        return true;
    }

    public function convertToPHPValueSQL($sqlExpr, $platform): string
    {
        if ($platform instanceof \Doctrine\DBAL\Platforms\SQLServer2008Platform) {
            return "CAST(CAST($sqlExpr AS VARBINARY(MAX)) AS VARCHAR(MAX))";
        } else if ($platform instanceof \Doctrine\DBAL\Platforms\SQLServer2000Platform) {
            return "CAST(CAST($sqlExpr AS VARBINARY(8000)) AS VARCHAR)";
        } else {
            return $sqlExpr;
        }
    }

    public function convertToDatabaseValueSQL($sqlExpr, AbstractPlatform $platform): string
    {
        if ($platform instanceof \Doctrine\DBAL\Platforms\SQLServer2008Platform) {
            return "CAST(CAST($sqlExpr AS VARCHAR(MAX)) AS VARBINARY(MAX))";
        } else if ($platform instanceof \Doctrine\DBAL\Platforms\SQLServer2000Platform) {
            return "CAST(CAST($sqlExpr AS VARCHAR) AS VARBINARY(8000))";
        } else {
            return $sqlExpr;
        }
    }

    public function getName(): string
    {
        return 'varchar_image';
    }
}
