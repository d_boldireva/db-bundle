<?php
namespace TeamSoft\CrmRepositoryBundle\DBAL\Types;

use Doctrine\DBAL\Types\Type;
use Doctrine\DBAL\Platforms\AbstractPlatform;

class VarbinaryImage extends Type
{
    public function getSQLDeclaration(array $fieldDeclaration, AbstractPlatform $platform): string
    {
        return $platform->getBlobTypeDeclarationSQL($fieldDeclaration);
    }

    public function canRequireSQLConversion(): bool
    {
        return true;
    }

    public function convertToPHPValueSQL($sqlExpr, $platform): string
    {
        if ($platform instanceof \Doctrine\DBAL\Platforms\SQLServer2008Platform) {
            return "CAST($sqlExpr AS VARBINARY(MAX))";
        } else if ($platform instanceof \Doctrine\DBAL\Platforms\SQLServer2000Platform) {
            return "CAST($sqlExpr AS VARBINARY(8000))";
        } else {
            return $sqlExpr;
        }
    }

    public function convertToDatabaseValueSQL($sqlExpr, AbstractPlatform $platform): string
    {
        if ($platform instanceof \Doctrine\DBAL\Platforms\SQLServer2008Platform) {
            return "CAST($sqlExpr AS VARBINARY(MAX))";
        } else if ($platform instanceof \Doctrine\DBAL\Platforms\SQLServer2000Platform) {
            return "CAST($sqlExpr AS VARBINARY(8000))";
        } else {
            return $sqlExpr;
        }
    }

    public function getName(): string
    {
        return 'varbinary_image';
    }
}
