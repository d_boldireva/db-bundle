<?php

namespace TeamSoft\CrmRepositoryBundle\DBAL\Types;

use Doctrine\DBAL\Platforms\AbstractPlatform;
use Doctrine\DBAL\Platforms\PostgreSQL100Platform;
use TeamSoft\CrmRepositoryBundle\DBAL\Platforms\SQLServer2008Platform;

class Polygon extends Geometry {
    /**
     * @return string
     */
    public function getName(): string
    {
        return 'polygon';
    }

    /**
     * @param mixed $value
     * @param AbstractPlatform $platform
     * @return mixed
     */
    public function convertToPHPValue($value, AbstractPlatform $platform): mixed
    {
        if ($platform instanceof PostgreSQL100Platform && strpos($value,'POLYGON') === 0) {
            $value = str_replace('POLYGON','',$value);
            $value = str_replace('((','(((',$value);
            $value = str_replace('))',')))',$value);
            $value = str_replace(',','),(',$value);
            $value = str_replace(' ',',',$value);
            $value = str_replace('(','[',$value);
            $value = str_replace(')',']',$value);
            $value = json_decode($value);
        } else if (strpos($value, 'POLYGON ') === 0) {
            $value = str_replace('POLYGON ','',$value);
            $value = str_replace('((','[[[',$value);
            $value = str_replace('))',']]]',$value);
            $value = str_replace('), (',']],[[',$value);
            $value = str_replace(', ','],[',$value);
            $value = str_replace(' ',',',$value);
            $value = json_decode($value);
        } else {
            $value = [];
        }
        return $value;
    }

    /**
     * @param mixed $value
     * @param AbstractPlatform $platform
     * @return mixed
     */
    public function convertToDatabaseValue($value, AbstractPlatform $platform): mixed
    {
        if(!$value) return null;

        $return = 'POLYGON ((';

        $return .= implode('), (',array_map(function($polyline){
            return implode(', ',array_map(function($point){
                return $point[0].' '.$point[1];
            }, $polyline));
        }, $value));

        $return .= '))';

        return $return;
    }
}
