<?php
namespace TeamSoft\CrmRepositoryBundle\DBAL\Types;

use Doctrine\DBAL\Platforms\PostgreSQL100Platform;
use Doctrine\DBAL\Platforms\SQLServer2008Platform;
use Doctrine\DBAL\Types\Type;
use Doctrine\DBAL\Platforms\AbstractPlatform;

class Geometry extends Type
{
    // ToDo: implement all geometry types
    const GEOMETRY           = 'Geometry';
    const POINT              = 'Point';
    const LINESTRING         = 'LineString';
    const POLYGON            = 'Polygon';
    const MULTIPOINT         = 'MultiPoint';
    const MULTILINESTRING    = 'MultiLineString';
    const MULTIPOLYGON       = 'MultiPolygon';
    const GEOMETRYCOLLECTION = 'GeometryCollection';

    public function getName(): string
    {
        return 'geometry';
    }

    public function canRequireSQLConversion(): bool
    {
        return true;
    }

    public function getSQLDeclaration(array $fieldDeclaration, AbstractPlatform $platform): string
    {
        return 'geometry';
    }

    public function convertToPHPValueSQL($sqlExpr, $platform): string
    {
        if ($platform instanceof SQLServer2008Platform) {
            return $sqlExpr ? $sqlExpr . '.STAsText()' : ''; // .Reduce(.005).STAsText()
        } else if ($platform instanceof PostgreSQL100Platform) {
            return $sqlExpr ? 'ST_AsText('. $sqlExpr .')' : '';
        } else {
            return $sqlExpr ?? '';
        }
    }

    public function convertToDatabaseValueSQL($sqlExpr, AbstractPlatform $platform): string
    {
        if ($platform instanceof SQLServer2008Platform) {
            return $sqlExpr ? 'geometry::STGeomFromText('. $sqlExpr .',0)' : '';
        } else if ($platform instanceof PostgreSQL100Platform) {
            return $sqlExpr ? 'ST_GeomFromText('.$sqlExpr.',4326)' : '';
        } else {
            return $sqlExpr ?? '';
        }
    }
}
