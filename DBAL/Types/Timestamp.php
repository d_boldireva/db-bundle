<?php

namespace TeamSoft\CrmRepositoryBundle\DBAL\Types;

use Doctrine\DBAL\Platforms\AbstractPlatform;
use Doctrine\DBAL\Platforms\SQLServer2008Platform;
use Doctrine\DBAL\Types\Type;

class Timestamp extends Type
{
    public function getName(): string
    {
        return 'timestamp';
    }

    /**
     * {@inheritdoc}
     */
    public function getSQLDeclaration(array $fieldDeclaration, AbstractPlatform $platform): string
    {
        return 'timestamp';
    }

    /**
     * {@inheritdoc}
     */
    public function convertToDatabaseValue($value, AbstractPlatform $platform): mixed
    {
        return ($value !== null) ? $value : null;
    }

    /**
     * {@inheritdoc}
     */
    public function convertToPHPValue($value, AbstractPlatform $platform): mixed
    {

        if ($platform instanceof SQLServer2008Platform) {
            return '0x' . unpack('H*hex', $value)['hex'];
        }
        // todo: check timestamps for postgres

        return $value;
    }
}
