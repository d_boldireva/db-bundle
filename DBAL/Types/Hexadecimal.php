<?php

namespace TeamSoft\CrmRepositoryBundle\DBAL\Types;

use Doctrine\DBAL\Types\Type;
use Doctrine\DBAL\Platforms\AbstractPlatform;
use TeamSoft\CrmRepositoryBundle\DBAL\Platforms\SQLServer2008Platform;

/**
 * @deprecated use BlobType instead
 */
class Hexadecimal extends Type
{
    public function getSQLDeclaration(array $fieldDeclaration, AbstractPlatform $platform): string
    {
        return $platform->getBlobTypeDeclarationSQL($fieldDeclaration);
    }

    //TODO: delete from all custom types
    public function convertToPHPValue($value, AbstractPlatform $platform): mixed
    {
        if ($platform instanceof SQLServer2008Platform) {
            return unpack("H*hex", $value)['hex'];
        } else {
            return $value;
        }
    }

    //TODO: delete from all custom types
    public function convertToDatabaseValue($value, AbstractPlatform $platform): mixed
    {
        if (null === $value) {
            return null;
        }

        if (is_string($value)) {
            $fp = fopen('php://temp', 'rb+');
            fwrite($fp, pack("H*", $value));
            fseek($fp, 0);
            $value = $fp;
        }

        if (!is_resource($value)) {
            throw ConversionException::conversionFailed($value, $this->getName());
        }

        return $value;
    }

    public function getName(): string
    {
        return 'hexadecimal';
    }

    public function getBindingType(): int
    {
        return \PDO::PARAM_LOB;
    }
}
