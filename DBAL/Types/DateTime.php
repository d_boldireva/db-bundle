<?php

namespace TeamSoft\CrmRepositoryBundle\DBAL\Types;

use Doctrine\DBAL\Platforms\SQLServer2008Platform;
use Doctrine\DBAL\Types\DateTimeType;
use Doctrine\DBAL\Platforms\AbstractPlatform;

class DateTime extends DateTimeType
{

    public function convertToDatabaseValue($value, AbstractPlatform $platform): mixed
    {
        $value = parent::convertToDatabaseValue($value, $platform);

        if ($value && $platform instanceof SQLServer2008Platform){
            $value = substr($value, 0, strlen($value) - 3);
        }

        return $value;
    }

}
