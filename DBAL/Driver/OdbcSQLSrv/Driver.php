<?php

namespace TeamSoft\CrmRepositoryBundle\DBAL\Driver\OdbcSQLSrv;

use Doctrine\DBAL\Connection;
use Doctrine\DBAL\Driver\API\ExceptionConverter as ExceptionConverterInterface;
use Doctrine\DBAL\Platforms\AbstractPlatform;
use Doctrine\DBAL\Schema\SQLServerSchemaManager;
use TeamSoft\CrmRepositoryBundle\DBAL\Platforms\SQLServer2000Platform;

class Driver implements \Doctrine\DBAL\Driver
{
    /**
     * {@inheritdoc}
     */
    public function connect(array $params, $username = null, $password = null, array $driverOptions = array())
    {
        if (!isset($params['host'])) {
            throw new \Exception("Missing 'host' in configuration for odbc driver.");
        }
        if (!isset($params['dbname'])) {
            throw new \Exception("Missing 'dbname' in configuration for odbc driver.");
        }

        return new OdbcSQLSrvConnection($this->_constructDsn($params), $username, $password);
    }

    private function _constructDsn(array $params)
    {
        $dsn = 'Driver={SQL Server};Server=';

        if (isset($params['host'])) {
            $dsn .= $params['host'];
        }

        if (isset($params['dbname'])) {
            $dsn .= ';Database=' .  $params['dbname'];
        }

        return $dsn;
    }

    /**
     * {@inheritdoc}
     */
    public function getDatabasePlatform()
    {
        return new SQLServer2000Platform();
    }

    /**
     * {@inheritdoc}
     */
    public function getSchemaManager(Connection $conn, AbstractPlatform $platform)
    {
        return new SQLServerSchemaManager($conn, $platform);
    }

    /**
     * {@inheritdoc}
     */
    public function getName()
    {
        return 'odbc_sqlsrv';
    }

    /**
     * {@inheritdoc}
     */
    public function getDatabase(Connection $conn)
    {
        $params = $conn->getParams();
        return $params['dbname'];
    }

    public function getExceptionConverter(): ExceptionConverterInterface
    {
        return new ExceptionConverter();
    }
}
