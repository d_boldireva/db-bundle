<?php

namespace TeamSoft\CrmRepositoryBundle\DBAL\Driver\OdbcSQLSrv;

use Doctrine\DBAL\Driver\API\ExceptionConverter as ExceptionConverterInterface;
use Doctrine\DBAL\Driver\Exception;
use Doctrine\DBAL\Exception\DriverException;
use Doctrine\DBAL\Query;

class ExceptionConverter implements ExceptionConverterInterface
{

    public function convert(Exception $exception, ?Query $query): DriverException
    {
        return new DriverException();
    }
}
