<?php

namespace TeamSoft\CrmRepositoryBundle\DBAL\Driver\PDOSqlsrv;

use Doctrine\DBAL\Driver\Middleware\AbstractConnectionMiddleware;
use Doctrine\DBAL\Driver\PDO\Connection as PDOConnection;
use Doctrine\DBAL\Driver\Statement as StatementInterface;
use Doctrine\DBAL\Driver\PDO\SQLSrv\Statement;
use PDO;

final class Connection extends AbstractConnectionMiddleware
{
    /** @var PDOConnection */
    private $connection;

    public function __construct(PDOConnection $connection)
    {
        parent::__construct($connection);

        $this->connection = $connection;
    }

    public function prepare(string $sql): StatementInterface
    {
        return new Statement(
            $this->connection->prepare($sql)
        );
    }

    /**
     * {@inheritDoc}
     */
    public function lastInsertId($name = null)
    {
        if (null !== $name) {
            $result = $this->prepare('SELECT IDENT_CURRENT(?)')->execute([$name]);
//            $result = $this->prepare('SELECT CONVERT(VARCHAR(MAX), current_value) FROM sys.sequences WHERE name = ?')
//                ->execute([$name]);
        } else {
            $result = $this->query('SELECT @@IDENTITY');
        }

        return $result->fetchOne();
    }

    public function getNativeConnection(): PDO
    {
        return $this->connection->getNativeConnection();
    }
}
