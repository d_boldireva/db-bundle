<?php

namespace TeamSoft\CrmRepositoryBundle\DBAL\Platforms;

use Doctrine\DBAL\Platforms\PostgreSQL100Platform as BasePostgreSQL100Platform;

class PostgreSQL100Platform extends BasePostgreSQL100Platform
{
    public function getDateTimeWithGmtOffsetExpression($dateTimeExpression, $gmtOffsetExpression, $clientGmtOffset)
    {
        $clientGmtOffset = $clientGmtOffset !== null ? $clientGmtOffset : 'null';
        return "(($dateTimeExpression)::timestamp - COALESCE($clientGmtOffset - ($gmtOffsetExpression), 0) * INTERVAL '1 second')";
    }

    public function getCastExpression($value, $type)
    {
        switch ($type) {
            case 'datetime':
                $type = 'timestamp';
                break;
        }
        return 'CAST(' . $value . ' AS ' . $type . ')';
    }
}
