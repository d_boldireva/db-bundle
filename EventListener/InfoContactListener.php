<?php

namespace TeamSoft\CrmRepositoryBundle\EventListener;

use Doctrine\ORM\Event\PreFlushEventArgs;
use Doctrine\ORM\Event\LifecycleEventArgs;
use Psr\Log\LoggerInterface;
use TeamSoft\CrmRepositoryBundle\Entity\InfoContact;
use TeamSoft\CrmRepositoryBundle\Entity\InfoContactHistory;
use TeamSoft\CrmRepositoryBundle\Entity\InfoContactrequest;
use TeamSoft\CrmRepositoryBundle\Service\Options;

class InfoContactListener
{

    /**
     * @var Options
     */
    private $options;

    /**
     * @var LoggerInterface
     */
    private $logger;

    private $attributeForHistory = [
        'firstname' => 'Фамилия',
        'middlename' => 'Имя',
        'lastname' => 'Отчество',
        'company' => 'Учреждение',
        'specialization' => 'Специализация',
        'archivereason' => 'Причина архивации',
        'position' => 'Должность',
    ];

    public function __construct(Options $options, LoggerInterface $logger)
    {
        $this->options = $options;
        $this->logger = $logger;
    }

    public function postLoad(InfoContact $contact, LifecycleEventArgs $args)
    {
    }

    public function preFlush(InfoContact $contact, PreFlushEventArgs $args)
    {
    }

    public function prePersist(InfoContact $contact, LifecycleEventArgs $args)
    {
        if ($contact->getStatusverification() === null) {
            $contact->setStatusverification(InfoContact::STATUS_WAIT_VERIFICATION);
        }

        $contact->setIsarchive($contact->getArchivereasonId() ? 1 : 0);
    }

    public function preUpdate(InfoContact $contact, LifecycleEventArgs $args)
    {
        if ($this->options->get('contactverification') > 0) {
            $qb = $args
                ->getEntityManager()
                ->getRepository(InfoContactrequest::class)
                ->createQueryBuilder('cr');

            $contactRequestsExists = $qb
                ->where('cr.contact = ' . $contact->getId())
                ->select('count(cr)')
                ->andWhere('cr.statusverification2 IN (\'create\', \'update\')')
                ->getQuery()
                ->getSingleScalarResult();

            if (!$contactRequestsExists && !$contact->getArchivereasonId()) {
                $contact->setStatusverification(InfoContact::STATUS_VERIFIED);
            } elseif ($contact->getStatusverification() !== InfoContact::STATUS_NEED_VERIFICATION_AGAIN) {
                $contact->setStatusverification(InfoContact::STATUS_WAIT_VERIFICATION);
            } elseif (array_intersect_key($this->getChangeArguments($args), $this->attributeForHistory)) {
                $contact->setStatusverification(InfoContact::STATUS_NEED_VERIFICATION_AGAIN);
            }
        } elseif (!$contact->isSpecializationVerification() || !$contact->isRegionVerification()) {
            $contact->setStatusverification(InfoContact::STATUS_NOT_SUBJECT_VERIFICATION);
        } elseif (array_intersect_key($this->getChangeArguments($args), $this->attributeForHistory)) {
            $contact->setStatusverification(InfoContact::STATUS_WAIT_VERIFICATION);
        }


        $contact->setIsarchive($contact->getArchivereasonId() ? 1 : 0);
    }

    public function postPersist(InfoContact $contact, LifecycleEventArgs $args)
    {
//        $this->resetBloggers($contact, $args);
    }

    public function postUpdate(InfoContact $contact, LifecycleEventArgs $args)
    {
        try {
            $this->saveHistory($contact, $args);
        } catch (\Throwable $exception) {
            $this->logger->critical($exception->getMessage(), [
                'file' => $exception->getFile(),
                'line' => $exception->getLine(),
                'modelClass' => InfoContact::class,
                'model' => $contact->getId()
            ]);
        }
    }

    /**
     * @param InfoContact $contact
     * @param LifecycleEventArgs $args
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    protected function saveHistory(InfoContact $contact, LifecycleEventArgs $args)
    {
        $em = $args->getEntityManager();
        $changeAttributes = $this->getChangeArguments($args);

        foreach ($this->attributeForHistory as $field => $fieldName) {
            if (!isset($changeAttributes[$field])) {
                continue;
            }

            $history = new InfoContactHistory();
            $history->setContactId($contact->getId());
            $history->setModified($contact->getModified());
            $history->setField($fieldName);
            $history->setStatusVerification($contact->getStatusverification());
            $history->setSendVerification($contact->getSendverification());

            if ($contact->getModifier()) {
                $history->setModifierId($contact->getModifier()->getId());
            }

            $oldValue = $changeAttributes[$field][0];
            $newValue = $changeAttributes[$field][1];

            switch ($field) {
                case 'company':
                    $history->setCompanyIdOld($oldValue ? $oldValue->getId() : null);
                    $history->setCompanyIdNew($newValue ? $newValue->getId() : null);
                    break;
                case 'specialization':
                    $history->setSpecializationIdOld($oldValue ? $oldValue->getId() : null);
                    $history->setSpecializationIdNew($newValue ? $newValue->getId() : null);
                    break;
                case 'archivereason':
                    $history->setArchiveReasonIdOld($oldValue ? $oldValue->getId() : null);
                    $history->setArchiveReasonIdNew($newValue ? $newValue->getId() : null);
                    break;
                case 'position':
                    $history->setPositionIdOld($oldValue ? $oldValue->getId() : null);
                    $history->setPositionIdNew($newValue ? $newValue->getId() : null);
                    break;
                default:
                    $history->setOldValue($oldValue);
                    $history->setNewValue($newValue);
            }

            $em->persist($history);
        }

        $em->flush();
    }

    /**
     * Todo float is not correct. Addition float need check round($float1, 2) !== round($float2, 2) or abs($float2 - $float1) > .01
     * @param LifecycleEventArgs $args
     * @return array
     */
    protected function getChangeArguments(LifecycleEventArgs $args): array
    {
        return $args->getEntityManager()->getUnitOfWork()->getEntityChangeSet($args->getObject());
    }

    protected function resetBloggers(InfoContact $contact, LifecycleEventArgs $args)
    {
        if ($contact->getContacttype() && $contact->getContacttype()->getIsBlogger()) {
            return;
        }

        $em = $args->getEntityManager();
        foreach ($contact->getContactBlogCollection() as $blog) {
            $em->remove($blog);
        }

        $em->flush();
    }
}
