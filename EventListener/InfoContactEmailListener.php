<?php

namespace TeamSoft\CrmRepositoryBundle\EventListener;

use Doctrine\ORM\Event\LifecycleEventArgs;
use Doctrine\ORM\Event\OnFlushEventArgs;
use Doctrine\ORM\Event\PostFlushEventArgs;
use Doctrine\ORM\Event\PreFlushEventArgs;
use TeamSoft\CrmRepositoryBundle\Entity\InfoContact;
use TeamSoft\CrmRepositoryBundle\Entity\InfoContactemail;
use TeamSoft\CrmRepositoryBundle\Entity\InfoContactHistory;
use TeamSoft\CrmRepositoryBundle\Entity\InfoContactphone;

class InfoContactEmailListener
{
    /** @var InfoContactHistory[]  */
    public $histories = [];

    // insert
    public function postPersist(InfoContactemail $contactemail, LifecycleEventArgs $args)
    {
        $this->saveHistory($contactemail->getContact(), null, $contactemail->getEmail(), $args);
    }

    // update
    public function postUpdate(InfoContactemail $contactemail, LifecycleEventArgs $args)
    {
        $changes = $args->getEntityManager()->getUnitOfWork()->getEntityChangeSet($args->getObject());
        if (isset($changes['email'], $changes['email'][0], $changes['email'][1]) && $changes['email'][0] != $changes['email'][1]) {
            $this->saveHistory($contactemail->getContact(), $changes['email'][0], $changes['email'][1], $args);
        }
    }

    public function postFlush(PostFlushEventArgs $args)
    {
        if (!$this->histories) {
            return;
        }

        foreach ($this->histories as $contactHistory) {
            $args->getEntityManager()->persist($contactHistory);
        }

        $this->histories = [];
        $args->getEntityManager()->flush();
    }

    // delete
    public function postRemove(InfoContactemail $contactemail, LifecycleEventArgs $args)
    {
        $this->saveHistory($contactemail->getContact(), $contactemail->getEmail(), null, $args);
    }

    protected function saveHistory(InfoContact $contact, $oldValue, $newValue, LifecycleEventArgs $args)
    {
        $history = new InfoContactHistory();
        $history->setContactId($contact->getId());
        $history->setModified($contact->getModified());
        $history->setField('contact_emails');
        $history->setStatusVerification($contact->getStatusverification());
        $history->setSendVerification($contact->getSendverification());

        $history->setOldValue($oldValue);
        $history->setNewValue($newValue);

        $this->histories[] = $history;
    }
}
