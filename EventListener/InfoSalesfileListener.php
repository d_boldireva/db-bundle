<?php

namespace TeamSoft\CrmRepositoryBundle\EventListener;

use Doctrine\ORM\Event\PreFlushEventArgs;
use Doctrine\ORM\Event\LifecycleEventArgs;
use Symfony\Component\HttpFoundation\File\Exception\UploadException;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use TeamSoft\CrmRepositoryBundle\Entity\Sales\InfoSalesfile;
use TeamSoft\CrmRepositoryBundle\Service\DatabasePlatform;
use TeamSoft\CrmRepositoryBundle\Service\Options;
use TeamSoft\CrmRepositoryBundle\Service\SalesFileParser;

class InfoSalesfileListener
{
    /**
     * @var DatabasePlatform
     */
    private $dp;
    /**
     * @var SalesFileParser
     */
    private $parser;
    /**
     * InfoSalesfileListener constructor.
     * @param DatabasePlatform $dp
     * @param SalesFileParser $parser
     */
    public function __construct(DatabasePlatform $dp, SalesFileParser $parser)
    {
        $this->dp = $dp;
        $this->parser = $parser;
    }

    /**
     * @param InfoSalesfile $salesfile
     * @param LifecycleEventArgs $args
     */
    public function postLoad(InfoSalesfile $salesfile, LifecycleEventArgs $args)
    {
    }

    /**
     * @param InfoSalesfile $salesfile
     * @param PreFlushEventArgs $args
     */
    public function preFlush(InfoSalesfile $salesfile, PreFlushEventArgs $args)
    {

    }

    /**
     * @param InfoSalesfile $salesfile
     * @param LifecycleEventArgs $args
     * @throws \Exception
     */
    protected function preChange(InfoSalesfile &$salesfile, LifecycleEventArgs &$args)
    {
    }

    /**
     * @param InfoSalesfile $salesfile
     * @param LifecycleEventArgs $args
     * @throws \Exception
     */
    public function prePersist(InfoSalesfile $salesfile, LifecycleEventArgs $args)
    {
        $this->preChange($salesfile, $args);
    }

    /**
     * @param InfoSalesfile $salesfile
     * @param LifecycleEventArgs $args
     * @throws \Exception
     */
    public function preUpdate(InfoSalesfile $salesfile, LifecycleEventArgs $args)
    {
        $this->preChange($salesfile, $args);
    }

    /**
     * @param InfoSalesfile $salesfile
     * @param LifecycleEventArgs $args
     */
    public function postPersist(InfoSalesfile $salesfile, LifecycleEventArgs $args)
    {
    }

    /**
     * @param InfoSalesfile $salesfile
     * @param LifecycleEventArgs $args
     */
    public function postUpdate(InfoSalesfile $salesfile, LifecycleEventArgs $args)
    {
    }

    /**
     * @param InfoSalesfile $salesfile
     * @param LifecycleEventArgs $args
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function preRemove(InfoSalesfile $salesfile, LifecycleEventArgs $args)
    {
        if ($salesfile->getTableName()) {
            $args->getEntityManager()->beginTransaction();
            try {
                $this->parser->removeAssociatedTable($salesfile);
                $args->getEntityManager()->commit();

            } catch (\Throwable $exception) {
                $args->getEntityManager()->rollback();

                throw $exception;
            }

            $args->getEntityManager()->flush($salesfile);
        }
    }

    /**
     * @param InfoSalesfile $salesfile
     * @param LifecycleEventArgs $args
     */
    public function postRemove(InfoSalesfile $salesfile, LifecycleEventArgs $args)
    {
        if ($salesfile->isPattern()) {
            $pathToFile = $this->parser->getPathToSavePatternFile($salesfile);
            file_exists($pathToFile) && @unlink($pathToFile);
        }
    }
}
