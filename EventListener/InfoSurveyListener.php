<?php


namespace TeamSoft\CrmRepositoryBundle\EventListener;

use Doctrine\ORM\Event\PreUpdateEventArgs;
use TeamSoft\CrmRepositoryBundle\Entity\InfoSurvey;
use TeamSoft\CrmRepositoryBundle\Entity\InfoSurveyFilter;

class InfoSurveyListener
{
    public function preUpdate(InfoSurvey $survey, PreUpdateEventArgs $args)
    {
        if ($args->hasChangedField('page')) {
            $surveyFilterIds = $survey->getSurveyFilterCollection()->map(function ($value) {
                return $value->getId();
            })->toArray();

            if (!empty($surveyFilterIds)) {
                $em = $args
                    ->getEntityManager()
                    ->createQueryBuilder()
                    ->delete(InfoSurveyFilter::class, 'isf');

                $em->where('isf.id in (' . implode(',', $surveyFilterIds) . ')');
                $em->getQuery()->execute();
            }
        }
    }
}