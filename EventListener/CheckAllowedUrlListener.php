<?php
namespace TeamSoft\CrmRepositoryBundle\EventListener;

use Symfony\Component\HttpKernel\Event\RequestEvent;
use Symfony\Component\HttpKernel\KernelEvents;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\Security\Core\Authentication\AuthenticationManagerInterface;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;

class CheckAllowedUrlListener implements EventSubscriberInterface
{
    private $tokenStorage;
    private $authManager;

    public function __construct(TokenStorageInterface $tokenStorage, AuthenticationManagerInterface $authManager)
    {
        $this->tokenStorage = $tokenStorage;
        $this->authManager = $authManager;
    }

    public function onKernelRequest(RequestEvent $event)
    {
        $token = $this->tokenStorage->getToken();
        if ($token && !$token->getUser()) {
            $token = $this->authManager->authenticate($token);
            $this->tokenStorage->setToken($token);
        }
        if ($token instanceof UsernamePasswordToken) {
            $request = $event->getRequest();
            /**
             * @var \TeamSoft\CrmRepositoryBundle\Entity\InfoUser $user
             */
            $user = $token->getUser();
            if(method_exists($user,'allowedHost')){
                if ($user->allowedHost() && $request->getHost() && $user->allowedHost() !== $request->getHost()) {
                    $event->setResponse(new Response(null, 401));
                }
            } else if ($user instanceof \Symfony\Component\Security\Core\User\User) {
                //TODO: Используется для авторизации in-memory пользователей. Когда все in-memory пользователи будут перенесены в базу необходимо удалить.
            } else {
                $event->setResponse(new Response(null, 401));
            }
        }
    }

    public static function getSubscribedEvents(): array
    {
        return [
            KernelEvents::REQUEST => array(array('onKernelRequest', 7)),
        ];
    }
}
