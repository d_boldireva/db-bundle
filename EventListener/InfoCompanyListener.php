<?php

namespace TeamSoft\CrmRepositoryBundle\EventListener;

use Doctrine\ORM\Event\PreFlushEventArgs;
use Doctrine\ORM\Event\LifecycleEventArgs;
use Psr\Log\LoggerInterface;
use TeamSoft\CrmRepositoryBundle\Entity\InfoCompany;
use TeamSoft\CrmRepositoryBundle\Entity\InfoCompanyHistory;
use TeamSoft\CrmRepositoryBundle\Entity\InfoCompanyrequest;
use TeamSoft\CrmRepositoryBundle\Service\Options;

class InfoCompanyListener
{
    /**
     * @var Options
     */
    private $options;

    /**
     * @var LoggerInterface
     */
    private $logger;

    private $attributeForHistory = [
        'name' => 'Название',
        'postcode' => 'Индекс',
        'street' => 'Улица',
        'building' => 'Дом',
        'phone1' => 'Телефон1',
        'phone2' => 'Телефон2',
        'companytype' => 'Тип учреждения',
        'archivereason' => 'Причина архивации',
        'city' => 'Населенный пункт'
    ];

    public function __construct(Options $options, LoggerInterface $logger)
    {
        $this->options = $options;
        $this->logger = $logger;
    }

    public function postLoad(InfoCompany $company, LifecycleEventArgs $args)
    {
    }

    public function preFlush(InfoCompany $company, PreFlushEventArgs $args)
    {
    }

    public function prePersist(InfoCompany $company, LifecycleEventArgs $args)
    {
        if ($company->getStatusverification() === null) {
            $company->setStatusverification(InfoCompany::STATUS_WAIT_VERIFICATION);
        }
        $company->setIsarchive($company->getArchivereasonId() ? 1 : 0);
    }

    public function preUpdate(InfoCompany $company, LifecycleEventArgs $args)
    {
        if ($this->options->get('companyverification') > 0) {
            $qb = $args
                ->getEntityManager()
                ->getRepository(InfoCompanyrequest::class)
                ->createQueryBuilder('cr');

            $companyRequestsExists = $qb
                ->where('cr.company = ' . $company->getId())
                ->select('count(cr)')
                ->andWhere('cr.statusverification2 IN (\'create\', \'update\')')
                ->getQuery()
                ->getSingleScalarResult();

            if (!$companyRequestsExists && !$company->getArchivereasonId()) {
                $company->setStatusverification(InfoCompany::STATUS_VERIFIED);
            } else if ($company->getStatusverification() !== InfoCompany::STATUS_NEED_VERIFICATION_AGAIN) {
                $company->setStatusverification(InfoCompany::STATUS_WAIT_VERIFICATION);
            } elseif (array_intersect_key($this->getChangeArguments($args), $this->attributeForHistory)) {
                $company->setStatusverification(InfoCompany::STATUS_NEED_VERIFICATION_AGAIN);
            }
        } elseif (!$company->isRegionVerification()) {
            $company->setStatusverification(InfoCompany::STATUS_NOT_SUBJECT_VERIFICATION);
        } elseif (array_intersect_key($this->getChangeArguments($args), $this->attributeForHistory)) {
            $company->setStatusverification(InfoCompany::STATUS_WAIT_VERIFICATION);
        }

        $company->setIsarchive($company->getArchivereasonId() ? 1 : 0);
    }

    public function postPersist(InfoCompany $company, LifecycleEventArgs $args)
    {
    }

    public function postUpdate(InfoCompany $company, LifecycleEventArgs $args)
    {
        try {
            $this->saveHistory($company, $args);
        } catch (\Throwable $exception) {
            $this->logger->critical($exception->getMessage(), [
                'file' => $exception->getFile(),
                'line' => $exception->getLine(),
                'modelClass' => InfoCompany::class,
                'model' => $company->getId()
            ]);
        }
    }

    /**
     * @param InfoCompany $company
     * @param LifecycleEventArgs $args
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    private function saveHistory(InfoCompany $company, LifecycleEventArgs $args)
    {
        $em = $args->getEntityManager();
        $changeAttributes = $this->getChangeArguments($args);

        foreach ($this->attributeForHistory as $field => $fieldName) {
            if (!isset($changeAttributes[$field])) {
                continue;
            }
            $history = new InfoCompanyHistory();
            $history->setCompanyId($company->getId());
            $history->setModified($company->getModified());
            $history->setField($fieldName);
            $history->setStatusVerification($company->getStatusverification());
            $history->setSendVerification($company->getSendverification());

            if ($company->getModifier()) {
                $history->setModifierId($company->getModifier()->getId());
            }

            $oldValue = $changeAttributes[$field][0];
            $newValue = $changeAttributes[$field][1];

            switch ($field) {
                case 'companytype':
                    $history->setCompanyTypeIdOld($oldValue ? $oldValue->getId() : null);
                    $history->setCompanyTypeIdNew($newValue ? $newValue->getId() : null);
                    break;
                case 'archivereason':
                    $history->setArchiveReasonIdOld($oldValue ? $oldValue->getId() : null);
                    $history->setArchiveReasonIdNew($newValue ? $newValue->getId() : null);
                    break;
                case 'city':
                    $history->setCityIdOld($oldValue ? $oldValue->getId() : null);
                    $history->setCityIdNew($newValue ? $newValue->getId() : null);
                    break;
                default:
                    $history->setOldValue($oldValue);
                    $history->setNewValue($newValue);
            }

            $em->persist($history);
        }

        $em->flush();
    }

    /**
     * Todo float is not correct. Addition float need check round($float1, 2) !== round($float2, 2) or abs($float2 - $float1) > .01
     * @param LifecycleEventArgs $args
     * @return array
     */
    protected function getChangeArguments(LifecycleEventArgs $args): array
    {
        return $args->getEntityManager()->getUnitOfWork()->getEntityChangeSet($args->getObject());
    }
}
