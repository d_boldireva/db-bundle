<?php

namespace TeamSoft\CrmRepositoryBundle\EventListener;

use Doctrine\DBAL\Connection;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Event\LifecycleEventArgs;
use Doctrine\ORM\Event\PreUpdateEventArgs;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\Security\Core\User\User;
use TeamSoft\CrmRepositoryBundle\Entity\DateTimeWithGmtOffsetInterface;
use TeamSoft\CrmRepositoryBundle\Model\FileInterface;
use TeamSoft\CrmRepositoryBundle\Entity\InfoUser;
use TeamSoft\CrmRepositoryBundle\Entity\ServiceFieldInterface;
use TeamSoft\CrmRepositoryBundle\Model\FileTypeInterface;
use TeamSoft\CrmRepositoryBundle\Service\DatabasePlatform;
use TeamSoft\CrmRepositoryBundle\Service\DateTimeWithGmtOffset;
use TeamSoft\CrmRepositoryBundle\Service\UploadFile;

class DoctrineEventListener
{

    private $tokenStorage;

    private $em;

    private $dp;

    private $uploadFile;

    private $dateTimeWithGmtOffset;

    public function __construct(
        TokenStorageInterface $tokenStorage,
        EntityManagerInterface $em,
        DatabasePlatform $dp,
        UploadFile $uploadFile,
        DateTimeWithGmtOffset $dateTimeWithGmtOffset
    )
    {
        $this->tokenStorage = $tokenStorage;
        $this->em = $em;
        $this->dp = $dp;
        $this->uploadFile = $uploadFile;
        $this->dateTimeWithGmtOffset = $dateTimeWithGmtOffset;
    }

    public function postLoad(LifecycleEventArgs $args)
    {
        $entity = $args->getEntity();
        $user = $this->getUser();
        $uow = $this->em->getUnitOfWork();
        if ($this->dp->isSQLServer2008() && ($user instanceof InfoUser)) {
            if (($userGmt = $user->getGmt()) !== null) {
                $entityReflection = new \ReflectionClass($entity);
                foreach ($entityReflection->getProperties() as $propertyReflection) {
                    $propertyReflection->setAccessible(true);
                    $propertyValue = $propertyReflection->getValue($entity);
                    if ($propertyValue instanceof \DateTime && $propertyValue->getTimestamp() >= 0) {
//                      $unixTime = $this->getUnixTime($propertyValue, $userGmt);
//                      $dateTime = \DateTime::createFromFormat('U', $unixTime);
//                      $dateTime->setTimezone(new \DateTimeZone(($userGmt >= 0 ? '+' : '') . $userGmt));
                        $dateTimeZone = new \DateTimeZone(($userGmt >= 0 ? '+' : '') . $userGmt);
                        $dateTime = \DateTime::createFromFormat('Y-m-d H:i:s.u', $propertyValue->format('Y-m-d H:i:s.u'), $dateTimeZone);
                        $propertyReflection->setValue($entity, $dateTime);
                        $uow->setOriginalEntityProperty(spl_object_hash($entity), $propertyReflection->getName(), $dateTime);
                    }
                }
            }
        } else if ($this->dp->isPostgreSQL100() && ($entity instanceof DateTimeWithGmtOffsetInterface)) {
            $this->dateTimeWithGmtOffset->tryModifyDateTimeEntity($entity);
        }
    }

    public function prePersist(LifecycleEventArgs $args)
    {
        $entity = $args->getEntity();
        $em = $args->getEntityManager();
        $connection = $em->getConnection();

        $user = $this->getUser();

        if ($this->dp->isSQLServer2008() && ($user instanceof InfoUser)) {
            $this->convertDateToUsersGMT($connection, $user, $entity);
        }

        if ($entity instanceof ServiceFieldInterface) {
            $entity->setCurrenttime(new \DateTime);

            if ($entity->getGuid() === null) {
                $entity->setGuid($this->dp->generateGiud());
            }

            if ($user instanceof InfoUser) {
                $entity->setModuser("user" . $user->getId() . "w");
            } else if ($user instanceof User) {
                $entity->setModuser($user->getUsername() . "_w");
            } else {
                $entity->setModuser($this->dp->generateCode());
            }
        }

        if ($entity instanceof FileInterface) {
            if (is_resource($entity->getContent())) {
                $mime = mime_content_type($entity->getContent());

                if (strpos($mime, 'image/') === 0) {
                    $fileType = strtoupper(str_replace('image/', '', $mime));
                    $entity->setExtension($fileType);
                }
            }

            if ($this->dp->isPostgreSQL100()) {
                $entity->setContent($this->uploadFile->saveEntityFile($entity));
            }
        }

        if ($entity instanceof DateTimeWithGmtOffsetInterface) {
            $this->dateTimeWithGmtOffset->tryModifyGmtOffsetEntity($entity);
        }
    }


    public function preUpdate(PreUpdateEventArgs $args)
    {
        $entity = $args->getEntity();
        $em = $args->getEntityManager();
        $connection = $em->getConnection();

        $user = $this->getUser();

        if ($this->dp->isSQLServer2008() && ($user instanceof InfoUser)) {
            $this->convertDateToUsersGMT($connection, $user, $entity);
        }

        if ($entity instanceof ServiceFieldInterface) {
            $uow = $em->getUnitOfWork();
            $entityChangeSet = $this->unsetServiceFields($uow->getEntityChangeSet($entity));
            if (count($entityChangeSet)) {
                $entity->setCurrenttime(new \DateTime);
                $uow->setOriginalEntityData(
                    $entity, array_merge($uow->getOriginalEntityData($entity), array('moduser' => null))
                );
                if ($user instanceof InfoUser) {
                    $entity->setModuser("user" . $user->getId() . "w");
                } else if ($user instanceof User) {
                    $entity->setModuser($user->getUsername() . "_w");
                } else {
                    $entity->setModuser($this->dp->generateCode());
                }
            }
        }

        if ($entity instanceof FileInterface) {
            if (is_resource($entity->getContent())) {
                $mime = mime_content_type($entity->getContent());

                if (strpos($mime, 'image/') === 0) {
                    $fileType = strtoupper(str_replace('image/', '', $mime));
                    $entity->setExtension($fileType);
                }
            }

            if ($this->dp->isPostgreSQL100()) {
                $entity->setContent($this->uploadFile->saveEntityFile($entity));
            }
        }

        if ($entity instanceof DateTimeWithGmtOffsetInterface) {
            $this->dateTimeWithGmtOffset->tryModifyGmtOffsetEntity($entity);
        }
    }


    private function convertDateToUsersGMT(Connection $connection, InfoUser $user, $entity)
    {
        $userGmt = $user->getGmt();
        if ($userGmt !== null && is_int($userGmt)) {
            $entityReflection = new \ReflectionClass($entity);
            $dateTimeZone = new \DateTimeZone(($userGmt >= 0 ? '+' : '') . $userGmt);
            foreach ($entityReflection->getProperties() as $propertyReflection) {
                $propertyReflection->setAccessible(true);
                $propertyValue = $propertyReflection->getValue($entity);
                if ($propertyValue instanceof \DateTime && $propertyValue->getTimestamp() >= 0) {
//                    $timeStamp = \DateTime::createFromFormat('Y-m-d H:i:s.u', $propertyValue->format('Y-m-d H:i:s.u'), $dateTimeZone)->format('U');
//                    $dateString = $this->getLocalDateTime($timeStamp, $userGmt);
//                    $date = \DateTime::createFromFormat('Y-m-d H:i:s.u', $dateString, $dateTimeZone);
                    $date = \DateTime::createFromFormat('Y-m-d H:i:s.u', $propertyValue->format('Y-m-d H:i:s.u'), $dateTimeZone);
                    $propertyReflection->setValue($entity, $date);
                }
            }
        }
    }

    /**
     * @return InfoUser|null
     */
    private function getUser()
    {
        if (null === $token = $this->tokenStorage->getToken()) {
            return null;
        }

        /** @var InfoUser $user */
        if (!is_object($user = $token->getUser())) {
            // e.g. anonymous authentication
            return null;
        }

        return $user;
    }

    private function unsetServiceFields(array $changeSet)
    {
        unset($changeSet['currenttime']);
        unset($changeSet['moduser']);
        return $changeSet;
    }

    private function getUnixTime(\DateTime $dateTime, $userGmt)
    {
        $unixTime = $this->em->getConnection()->fetchOne(
            'SELECT dbo.getUnixTime(?,?)',
            array($dateTime->format('Y-m-d H:i:s'), $userGmt)
        );

        return $unixTime;
    }

    private function getLocalDateTime($timeStamp, $userGmt)
    {
        $dateString = $this->em->getConnection()->fetchOne(
            'SELECT dbo.getLocalDateTime(?,?)',
            array($timeStamp, $userGmt)
        );
        return $dateString;
    }
}
