<?php
declare(strict_types=1);


namespace TeamSoft\CrmRepositoryBundle\EventListener\CarReport;


use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Event\LifecycleEventArgs;
use Doctrine\ORM\Event\PostFlushEventArgs;
use TeamSoft\CrmRepositoryBundle\Entity\CarReport\InfoCarReport;

class InfoCarReportListener
{
    /** @var InfoCarReport[] */
    protected $reports = [];

    // insert
    public function postPersist(InfoCarReport $sheet, LifecycleEventArgs $args)
    {
    }

    // update
    public function postUpdate(InfoCarReport $carReport, LifecycleEventArgs $args)
    {
        $changes = $args->getEntityManager()->getUnitOfWork()->getEntityChangeSet($args->getObject());

        $mapChanges = [
            'odometerFinish' => false,
        ];

        foreach ($mapChanges as $field => $notMatter) {
            $mapChanges[$field] = isset($changes[$field], $changes[$field][0], $changes[$field][1]) && round($changes[$field][0], 2) != round($changes[$field][1], 2);
        }

        if ($mapChanges['odometerFinish']) {
            $this->reports[] = $carReport;
        }
    }

    // delete
    public function postRemove(InfoCarReport $sheet, LifecycleEventArgs $args)
    {
    }

    //
    public function postFlush(PostFlushEventArgs $args)
    {
        $this->doUpdateNextReport($args->getEntityManager());
    }

    protected function doUpdateNextReport(EntityManagerInterface $entityManager): void
    {
        if (!$this->reports) {
            return;
        }

        // clear data before new loop
        $reports = $this->reports;
        $this->reports = [];

        foreach ($reports as $report) {
            $sheet = $report->getCarReportSheet();

            $carReports = $sheet->getCarReports();
            $nextIndex = $carReports->indexOf($report) + 1;


            //last day
            if (!isset($carReports[$nextIndex])) {
                continue;
            }

            /** @var InfoCarReport[] $nextReports */
            $nextReports = $carReports->slice($nextIndex, $carReports->count());

            $odometerFinish = (int)$report->getOdometerFinish();
            self::updateNextReport($nextReports, $odometerFinish);
        }

        $entityManager->flush();
    }

    /**
     * @param InfoCarReport[] $collection
     * @param int $odometerFinish
     */
    public static function updateNextReport(array $collection, int $odometerFinish): void
    {
        foreach ($collection as $nextReport) {
            $nextReport->setOdometerStart($odometerFinish);
            if (((int) $nextReport->getOdometerFinish()) < $odometerFinish || $nextReport->isSkipReport()) {
                $nextReport->setOdometerFinish($odometerFinish);
            } else {
                break;
            }
        }
    }
}