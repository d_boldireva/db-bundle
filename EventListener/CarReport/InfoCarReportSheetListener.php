<?php declare(strict_types=1);

namespace TeamSoft\CrmRepositoryBundle\EventListener\CarReport;

use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Event\LifecycleEventArgs;
use Doctrine\ORM\Event\PostFlushEventArgs;
use TeamSoft\CrmRepositoryBundle\Entity\CarReport\History\InfoCarReportSheetHistory;
use TeamSoft\CrmRepositoryBundle\Entity\CarReport\InfoCarReportSheet;
use TeamSoft\CrmRepositoryBundle\Repository\CarReport\InfoCarReportSheetRepository;

class InfoCarReportSheetListener
{
    /** @var InfoCarReportSheetHistory[] */
    protected $histories = [];

    /** @var InfoCarReportSheet[] */
    protected $checkNextSheets = [];

    /** @var InfoCarReportSheet[] */
    protected $checkReportsOfSheets = [];

    /** @var InfoCarReportSheetRepository */
    protected $carReportSheetRepository;

    public function __construct(InfoCarReportSheetRepository $carReportSheetRepository)
    {
        $this->carReportSheetRepository = $carReportSheetRepository;
    }

    // insert
    public function postPersist(InfoCarReportSheet $sheet, LifecycleEventArgs $args)
    {
        $this->addHistory($sheet, $args);
    }

    // update
    public function postUpdate(InfoCarReportSheet $sheet, LifecycleEventArgs $args)
    {
        $changes = $args->getEntityManager()->getUnitOfWork()->getEntityChangeSet($args->getObject());

        $mapChanges = [
            'odometerFinish' => false,
            'odometerStart' => false,
            'fuelStart' => false,
            'fuelFinish' => false,
            'refueled' => false,
        ];

        foreach ($mapChanges as $field => $notMatter) {
            $mapChanges[$field] = isset($changes[$field], $changes[$field][0], $changes[$field][1]) && round($changes[$field][0], 2) != round($changes[$field][1], 2);
        }

        if ($mapChanges['odometerFinish'] || $mapChanges['odometerStart'] || $mapChanges['fuelStart'] || $mapChanges['fuelFinish'] || $mapChanges['refueled']) {
            $this->addHistory($sheet, $args);
        }

        if ($mapChanges['odometerFinish'] || $mapChanges['fuelFinish']) {
            $this->checkNextSheets[] = $sheet;
        }

        if ($mapChanges['odometerStart']) {
            $this->checkReportsOfSheets[] = $sheet;
        }
    }

    // delete
    public function postRemove(InfoCarReportSheet $sheet, LifecycleEventArgs $args)
    {
    }

    //
    public function postFlush(PostFlushEventArgs $args)
    {
        $this->doHistories($args->getEntityManager());
        $this->doUpdateNextSheets($args->getEntityManager());
        $this->doUpdateReportsOfSheets($args->getEntityManager());
    }

    protected function addHistory(InfoCarReportSheet $sheet, LifecycleEventArgs $args = null)
    {
        $history = new InfoCarReportSheetHistory();
        $history->setSheet($sheet);
        $history->setUser($sheet->getUser());
        $history->setMonth($sheet->getMonth());

        $history->setOdometerStart($sheet->getOdometerStart());
        $history->setOdometerFinish($sheet->getOdometerFinish());
        $history->setFuelStart($sheet->getFuelStart());
        $history->setFuelFinish($sheet->getFuelFinish());
        $history->setRefueled($sheet->getRefueled());

        $this->histories[] = $history;
    }

    // save history sheet
    protected function doHistories(EntityManagerInterface $entityManager): void
    {
        if (!$this->histories) {
            return;
        }

        foreach ($this->histories as $history) {
            $entityManager->persist($history);
        }

        $this->histories = [];
        $entityManager->flush();
    }

    // find next sheet and update start params (recursive above child -> child -> child)
    protected function doUpdateNextSheets(EntityManagerInterface $entityManager): void
    {
        if (!$this->checkNextSheets) {
            return;
        }

        $sheets = $this->checkNextSheets;

        //clear before start next loop
        $this->checkNextSheets = [];

        foreach ($sheets as $sheet) {
            $nextSheet = $this->carReportSheetRepository->findNextSheet($sheet);

            if (!$nextSheet) {
                continue;
            }

            if ($nextSheet->getOdometerStart() !== $sheet->getOdometerFinish()) {
                $nextSheet->setOdometerStart($sheet->getOdometerFinish());

                if ($sheet->getOdometerFinish() > $nextSheet->getOdometerFinish()) {
                    $nextSheet->setOdometerFinish($sheet->getOdometerFinish());
                }
            }

            if ($nextSheet->getFuelStart() !== $sheet->getFuelFinish()) {
                $nextSheet->setFuelStart($sheet->getFuelFinish());
            }
        }

        $entityManager->flush();
    }

    // update start odometer if change odometer in month sheet
    protected function doUpdateReportsOfSheets(EntityManagerInterface $entityManager): void
    {
        if (!$this->checkReportsOfSheets) {
            return;
        }

        foreach ($this->checkReportsOfSheets as $sheet) {
            $newOdometerStart = (int) $sheet->getOdometerStart();

            InfoCarReportListener::updateNextReport($sheet->getCarReports()->getValues(), $newOdometerStart);
        }

        $this->checkReportsOfSheets = [];
        $entityManager->flush();
    }
}
