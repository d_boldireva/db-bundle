<?php

namespace TeamSoft\CrmRepositoryBundle\EventListener;

use Doctrine\ORM\Event\LifecycleEventArgs;
use TeamSoft\CrmRepositoryBundle\EventListener\helpers\ChangeAttributes;

abstract class Listener
{
    /**
     * Todo float is not correct. Addition float need check round($float1, 2) !== round($float2, 2) or abs($float2 - $float1) > .01
     * @param LifecycleEventArgs $args
     * @return array
     */
    protected function getChangeArguments(LifecycleEventArgs $args): array
    {
        return $args->getEntityManager()->getUnitOfWork()->getEntityChangeSet($args->getObject());
    }

    /**
     * @return ChangeAttributes
     */
    protected function getChangeAttributesCollection(LifecycleEventArgs $args): ChangeAttributes {
        return new ChangeAttributes($this->getChangeArguments($args));
    }
}
