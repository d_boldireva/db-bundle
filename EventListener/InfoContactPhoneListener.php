<?php

namespace TeamSoft\CrmRepositoryBundle\EventListener;

use Doctrine\ORM\Event\LifecycleEventArgs;
use Doctrine\ORM\Event\PostFlushEventArgs;
use TeamSoft\CrmRepositoryBundle\Entity\InfoContact;
use TeamSoft\CrmRepositoryBundle\Entity\InfoContactHistory;
use TeamSoft\CrmRepositoryBundle\Entity\InfoContactphone;

class InfoContactPhoneListener
{
    /** @var InfoContactHistory[]  */
    public $histories = [];
    // insert
    public function postPersist(InfoContactphone $contactphone, LifecycleEventArgs $args)
    {
        $this->saveHistory($contactphone->getContact(), null, $contactphone->getPhone(), $args);
    }

    // update
    public function postUpdate(InfoContactphone $contactphone, LifecycleEventArgs $args)
    {
        $changes = $args->getEntityManager()->getUnitOfWork()->getEntityChangeSet($args->getObject());
        if (isset($changes['phone'], $changes['phone'][0], $changes['phone'][1]) && $changes['phone'][0] != $changes['phone'][1]) {
            $this->saveHistory($contactphone->getContact(), $changes['phone'][0], $changes['phone'][1], $args);
        }
    }

    // delete
    public function postRemove(InfoContactphone $contactphone, LifecycleEventArgs $args)
    {
        $this->saveHistory($contactphone->getContact(), $contactphone->getPhone(), null, $args);
    }

    public function postFlush(PostFlushEventArgs $args)
    {
        if (!$this->histories) {
            return;
        }

        foreach ($this->histories as $contactHistory) {
            $args->getEntityManager()->persist($contactHistory);
        }

        $this->histories = [];
        $args->getEntityManager()->flush();
    }

    protected function saveHistory(InfoContact $contact, $oldValue, $newValue, LifecycleEventArgs $args)
    {
        $history = new InfoContactHistory();
        $history->setContactId($contact->getId());
        $history->setModified($contact->getModified());
        $history->setField('contact_phones');
        $history->setStatusVerification($contact->getStatusverification());
        $history->setSendVerification($contact->getSendverification());

        $history->setOldValue($oldValue);
        $history->setNewValue($newValue);

        $this->histories[] = $history;
    }
}
