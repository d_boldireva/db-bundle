<?php

namespace TeamSoft\CrmRepositoryBundle\EventListener\helpers;

use Doctrine\Common\Collections\ArrayCollection;

class ChangeAttributes extends ArrayCollection
{
    /**
     * @param string $field
     * @return mixed|null
     */
    public function getOldValue(string $field)
    {
        $column = $this->get($field);
        return $column[0] ?? null;
    }

    /**
     * @param string $field
     * @return mixed|null
     */
    public function getNewValue(string $field)
    {
        $column = $this->get($field);
        return $column[1] ?? null;
    }
}