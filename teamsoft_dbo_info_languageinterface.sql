INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('"[user] already pinned in this polygon"', '"[user] already pinned in this polygon"', 1, 'geomarketing');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('"[user] already pinned in this polygon"', '"[user] уже прикреплен к этому полигону"', 2, 'geomarketing');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('"[user] has no pin. Click on map to set"', '"У пользователя [user] нету пина. Нажмите на карту чтобы установить пин"', 2, 'geomarketing');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('"[user] has no pin. Click on map to set"', '"[user] hSearchas no pin. Click on map to set"', 1, 'geomarketing');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('"Delete"', 'Delete', 1, 'geomarketing');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('"Delete"', 'Удалить', 2, 'geomarketing');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('"Delete"', 'Delete', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('"Delete"', 'Удалить', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('"Display limit', '100 records": "Предел отображения: 100 записей"', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('"Display limit', '100 records": "Display limit: 100 records"', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('"Pins for users', '[users] places in the same polygon": "Пины для пользователей: [users] находятся внутри одного полигона"', 2, 'geomarketing');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('"Pins for users', '[users] places in the same polygon": "Pins for users: [users] placed within the same polygon"', 1, 'geomarketing');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('"Please, indicate the following fields', '%fields%": "Please, indicate the following fields: %fields%"', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('"Please, indicate the following fields', '%fields%": "Пожалуйста, укажите следующие поля: %fields%"', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('(m)', '(m)', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('(m)', '(м)', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('(source\loaded)', '(source\loaded)', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('(source\loaded)', '(исходник\загружено)', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('1 day after', '1 day after', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('1 day after', 'Через день после', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('1 day before', 'За день до', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('1 day before', '1 day before', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('1 week after', '1 week after', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('1 week after', 'Через неделю после', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('1 week before', 'За неделю до', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('1 week before', '1 week before', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('3 days after', '3 days after', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('3 days after', 'Через три дня после', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('3 days before', 'За три дня до', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('3 days before', '3 days before', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('AC', 'AC...', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('AC', 'AC...', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Accepted range', 'Accepted range', 1, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Accepted range', 'Доступный диапазон', 2, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Access denied', 'Доступ запрещён', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Access denied', 'Access denied', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('access restriction message', 'Service is enabled from 20th till the end of the month.', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('access restriction message', 'Доступ к сервису открывается только с 20го по конец месяца включительно.', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('According to the preliminary check in the syndicated database, the following institutions were found by the given companies', 'По предварительной проверке в синдикативной базе данных, по заданным критериям были найдены следующие учреждения', 2, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('According to the preliminary check in the syndicated database, the following institutions were found by the given company', 'According to the preliminary check in the syndicated database, the following institutions were found by the given company', 1, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('According to the preliminary check in the syndicated database, the following institutions were found by the given contacts', 'According to the preliminary check in the syndicated database, the following institutions were found by the given contacts', 1, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('According to the preliminary check in the syndicated database, the following institutions were found by the given contacts', 'По предварительной проверке в синдикативной базе данных, по заданным критериям были найдены следующие контакты', 2, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Account has expired.', 'Account has expired.', 1, 'security');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Account has expired.', 'Время действия учетной записи истекло.', 2, 'security');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Account is disabled.', 'Учетная запись отключена.', 2, 'security');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Account is disabled.', 'Account is disabled.', 1, 'security');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Account is locked.', 'Account is locked.', 1, 'security');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Account is locked.', 'Учетная запись заблокирована.', 2, 'security');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Account name', 'Название учреждения', 2, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Account name', 'Account name', 1, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Account owner', 'Account owner', 1, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Account status', 'Account status', 1, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Account status', 'Статус учреждения', 2, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Account_owner_dcr', 'Ответственность', 2, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Accounts', 'Учреждения', 2, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Accounts', 'Accounts', 1, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Accuracy', 'Точность(м)', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Accuracy', 'Accuracy(m)', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Action', 'Action', 1, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Action', 'Задача', 2, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Action is performed and can not be edited', 'Задача выполнена и не может быть редактирована', 2, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Action is performed and can not be edited', 'Action is performed and can not be edited', 1, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Action not found', 'Задача не найдена', 2, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Action not found', 'Action not found', 1, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Action state', 'Состояние задачи', 2, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Action state', 'Action state', 1, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Action Type', 'Action Type', 1, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Action type', 'Тип задачи', 2, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Action type', 'Тип задачи', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Action Type', 'Action Type', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Actions', 'Actions', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Actions', 'Действия', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Actions', ' Actions', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Actions', ' Действия', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Actions', 'Задачи', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Actions', 'Actions', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Active', 'Active', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Active', 'Видимость', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Actual shipment percentage', 'Фактический процент отгрузки', 2, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Actual shipment percentage', 'Actual shipment percentage', 1, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Actually paid', 'Actually paid', 1, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Actually paid', 'Фактически оплачено', 2, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Add', 'Добавить', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Add', 'Добавить', 2, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Add', 'Add', 1, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Add', 'Добавить', 2, 'geomarketing');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Add', 'Добавить', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Add', 'Add', 1, 'geomarketing');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Add', 'Add', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Add', 'Add', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Add', 'Добавить', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Add', 'Add', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Add', 'Добавить', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Add brand', 'Add brand', 1, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Add brand', 'Добавить бренд', 2, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Add brands to companies', 'Add brands to companies', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Add brands to companies ', 'Добавить бренды к ЛПУ', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Add client', 'Add client', 1, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Add client', 'Добавить клиента', 2, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Add coach', 'Add coach', 1, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Add coach', 'Добавить тренера', 2, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Add companies to brands ', 'Добавить ЛПУ к брендам', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Add companies to brands', 'Add companies to brands', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Add companies to polygon', 'Добавить учреждения полигону', 2, 'geomarketing');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Add companies to polygon', 'Add companies to polygon', 1, 'geomarketing');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Add company', 'Add company', 1, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Add company', 'Добавить учреждение', 2, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Add company', 'Добавить аптеку', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Add company', 'Add pharmacy', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Add company logo', 'Add egnlish compoay logo', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Add company logo', ' Добавить логотип компании', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Add detail', 'Добавить детали', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Add detail', 'Add detail', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Add employee', 'Add employee', 1, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Add employee', 'Добавить сотрудника', 2, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Add file', 'Add file', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Add file', 'Добавить файл', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Add filter', 'Add filter', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Add filter', 'Добавить фильтр', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Add group', 'Add group', 1, 'geomarketing');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Add group', 'Добавить группу', 2, 'geomarketing');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Add logo', ' Добавить лого', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Add member', 'Добавить участника', 2, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Add member', 'Add member', 1, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Add more', ' Добавить еще', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Add owners', 'Add owners', 1, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Add owners', 'Добавить исполнителей', 2, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Add participant', 'Add participant', 1, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Add participant', 'Добавить участника', 2, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Add polygon', 'Добавить', 2, 'geomarketing');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Add polygon', 'Add polygon', 1, 'geomarketing');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Add presentation', ' Добавить презентацию', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Add presentation', ' Add presentation', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Add promo material', 'Добавить промо материал', 2, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Add promo material', 'Add promo material', 1, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Add responsible', 'Добавить ответственного', 2, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Add responsible', 'Add responsible', 1, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Add row', 'Добавить запись', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Add row', 'Add row', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Add specialization', 'Add specialization', 1, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Add specialization', 'Добавить специализацию', 2, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Add subject', 'Add subject', 1, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Add subornidate', 'Add subornidate', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Add subornidate', 'Добавить подчиненного', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Add synonym', 'Добавить синоним', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Add synonym', 'Add synonym', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Add target practices', 'Add target practices', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Add target practices', 'Добавить целевые практики', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Add task', 'Добавить визит', 2, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Add task', 'Add task', 1, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Add task subject', 'Добавить тему визита', 2, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Add user', 'Добавить пользователя', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Add user', 'Add user', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Add watcher', 'Добавить наблюдателя', 2, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Add watcher', 'Add watcher', 1, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Add workplace', 'Add workplace', 1, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Add workplace', 'Добавить место работы', 2, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Add your corporate logo in png or svg format in the 594х165px extension. To do this, click on the field with a plus sign.', ' Добавьте ваш корпоративный логотип в формате png, jpg или svg в разрешении 594х165px. Для этого кликните на кнопку Добавить.', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Adding data to database', 'Adding data to database', 1, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Additional field', 'Дополнительное поле', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Additional field', 'Additional field', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Additional field created', 'Дополнительное поле добавлено', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Additional field deleted', 'Дополнительное поле удалено', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Additional fields', 'Дополнительные поля', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Additional fields', 'Additional fields', 1, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Additional fields', 'Дополнительные поля', 2, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Additional fields', 'Available add. fields', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Additional fields updated', 'Дополнительные поля обновлены', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Additional Information', 'Additional Information', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Additional Information', 'Дополнительная информация', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Additional phone', 'Additional phone', 1, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Additional phone', 'Доп. телефон', 2, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Additional responsibility', 'Дополнительная ответственность', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Additional responsible', 'Additional responsible', 1, 'geomarketing');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Additional responsible', 'Additional responsible', 1, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Additional responsible', 'Дополнительные ответственные', 2, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Additional responsible', 'Доп.ответственные', 2, 'geomarketing');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Additional settings', 'Additional settings', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Additional settings', 'Дополнительные настройки', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Additional specializations', 'Дополнительные специализации', 2, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Additional specializations', 'Additional specializations', 1, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Additional workplaces', 'Дополнительные места работы', 2, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Additional workplaces', 'Additional workplaces', 1, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('additional_specialization', ' Доп. специальность', 2, 'change_requests');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('additional_specialization', ' Additional specialization', 1, 'change_requests');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('additionalcompany', ' Связанные учреждения', 2, 'change_requests');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('additionalcompany', ' Related company', 1, 'change_requests');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('additionalowner', ' Additional resposible', 1, 'change_requests');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('additionalowner', ' Доп. ответственный', 2, 'change_requests');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('additionalspec', ' Additional specialization', 1, 'change_requests');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('additionalspec', ' Доп. специализация', 2, 'change_requests');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Address', 'Адрес', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Address', 'Address', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Address', 'Адрес', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Address', 'Адрес', 2, 'geomarketing');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Address', 'Address', 1, 'geomarketing');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Address', 'Address', 1, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Address', 'Адрес', 2, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Address', 'Address', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Admin toolkit', 'Панель администратора', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Administration Panel', 'Панель Администрирования', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Administration Panel', 'Administration Panel', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Administrator', 'Administrator (full access)', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Administrator', 'Админитстратор (полный доступ)', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Adress', 'Адрес', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Adress', 'Adress', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Advanced mode', 'Расширенный режим', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Advanced mode', 'Advanced mode', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Age', 'Age', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Age', 'Возраст', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Agreement', 'Договоренность', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Agreement', 'Договоренность', 2, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Agreement', 'Agreement', 1, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Agreement settings', 'Настройки договоренности', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('alerts.companiesAreMissing', 'Companies are missing', 1, 'geomarketing');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('alerts.companiesAreMissing', 'Учреждения не найдены', 2, 'geomarketing');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('alerts.emptyFilters', 'Фильтры не указаны. Пожалуйста укажите хотя бы один параметр', 2, 'geomarketing');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('alerts.emptyFilters', 'Filters are empty. Please specify at least one filter', 1, 'geomarketing');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('alerts.noCompanies', 'No companies for [[userName]]', 1, 'geomarketing');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('alerts.noCompanies', 'Для [[userName]] в данном полигоне нет учреждений', 2, 'geomarketing');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('alerts.noPolygonsInSelection', 'В выборке нет полигонов', 2, 'geomarketing');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('alerts.noPolygonsInSelection', 'No polygons in selection', 1, 'geomarketing');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('alerts.polygonsAreMissing', 'Polygons are missing', 1, 'geomarketing');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('alerts.polygonsAreMissing', 'Полигоны отсутствуют', 2, 'geomarketing');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('alerts.selfIntersection', 'Неправильный полигон!<br>Полигоны с самопересечением недопустимы.', 2, 'geomarketing');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('alerts.selfIntersection', 'Invalid polygon!<br>Polygons with self-intersection unacceptable.', 1, 'geomarketing');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('alerts.threeVertices', 'Polygon must have at least three vertices', 1, 'geomarketing');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('alerts.threeVertices', 'Полигон должен содержать не менее трёх вершин', 2, 'geomarketing');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('All', 'Все', 2, 'geomarketing');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('All', 'All', 1, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('All', 'All', 1, 'geomarketing');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('All', 'Все', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('All', 'All', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('All', 'Все', 2, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('All data', 'All data', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('All data', 'Все данные', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('All data were copied and saved', 'Все данные были скопированы и сохранены', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('All database', 'Вся база', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('All important messages will be displayed in this block.', ' Все важные сообщения будут отображаться в этом блоке.', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('All show', 'All show', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('All show', 'Показать все', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('All types', 'Все типы', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Allowed format', ' формат файла SVG, PNG, JPG', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Allowed input', ' Разрешенный ввод', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Allowed ratio', ' соотношение сторон 594 х 165', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Alternative IMEI', 'Дополнительный IMEI', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Alternative IMEI', 'Alternative IMEI', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Amount', 'Количество', 2, 'geomarketing');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Amount', 'Amount', 1, 'geomarketing');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('An authentication exception occurred.', 'Ошибка аутентификации.', 2, 'security');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('An authentication exception occurred.', 'An authentication exception occurred.', 1, 'security');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Analytics', 'Аналитика', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Analytics', 'Analytics', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('AND', 'И', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Android', 'Android', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Android', 'Android', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Answer', 'Answer', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Answer', 'Ответ', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Answer date', 'Дата ответа', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Answer date', 'Answer date', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('appealType', 'Тип обращения', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('appealType', 'Appeal Type', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Applied filters', 'Applied filters', 1, 'geomarketing');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Applied filters', 'Применённые фильтры', 2, 'geomarketing');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Apply', 'Применить', 2, 'geomarketing');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Apply', 'Apply', 1, 'geomarketing');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Apply', 'Применить', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Apply', 'Apply', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Apply', 'Применить', 2, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Apply', 'Apply', 1, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('applyFilter', 'Apply Filter', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('applyFilter', 'Применить', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Approval denied', 'Не утвержден', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Approval denied', 'Approval denied', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Approve', 'Approve', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Approve', 'Утвердить', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Approve', 'Approve', 1, 'geomarketing');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Approve', 'Утвердить', 2, 'geomarketing');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Approve', 'Утвердить', 2, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Approve granual plan', 'Утвердить грануальный план', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Approve granual plan', 'Approve granual plan', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('approved', 'approved', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Approved', 'Approved', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('approved', 'подтвержден', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Approved', 'Подтвержден', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('approved', 'approved', 1, 'geomarketing');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('approved', 'утвержден', 2, 'geomarketing');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Approved', 'Утверждено', 2, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Approved list', 'Утвердили', 2, 'geomarketing');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Approved list', 'Approved list', 1, 'geomarketing');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Approximate number of messages', 'Approximate number of messages', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Approximate number of messages', 'Примерное количество смс-сообщений', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('April', 'April', 1, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('April', 'Апрель', 2, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('April', ' Апреля', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Archive', 'Архив', 2, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Archive', 'Archive', 1, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Archive is not readable', ' Архив поврежден', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Archive is not readable', ' Archive is not readable', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Archive reason', 'Archive reason', 1, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Archive reason', 'Причина архивации', 2, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Archived', 'Archived', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Archived', 'Архивировать', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('archivereason', ' Причина архивации', 2, 'change_requests');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('archivereason', ' Archive reason', 1, 'change_requests');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Are you sure that you want to delete the reports', 'Вы уверены, что хотите удалить отчеты', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Are you sure that you want to delete the reports', 'Are you sure that you want to delete the reports', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Are you sure that you want to delete the template?', 'Are you sure that you want to delete the template?', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Are you sure that you want to delete the template?', 'Вы уверены, что хотите удалить шаблон?', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Are you sure you want to approve sales plan?', 'Вы точно хотите утвердить план продаж?', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Are you sure you want to approve sales plan?', 'Are you sure you want to approve sales plan?', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Are you sure you want to confirm this granual plan?', 'Are you sure you want to confirm this granual plan?', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Are you sure you want to confirm this granual plan?', 'Вы уверены, что хотите утвердить этот грануальный план?', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Are you sure you want to delete this field', 'Are you sure you want to delete this field', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Are you sure you want to delete this field', 'Вы уверены, что хотите удалить это поле?', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Are you sure you want to delete this field?', 'Вы уверены, что хотите удалить это поле?', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Are you sure you want to delete this row?', 'Вы уверены, что хотите удалить эту запись?', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Are you sure you want to delete this row?', 'Are you sure you want to delete this row?', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Are you sure you want to reject sales plan?', 'Вы точно хотите отклонить план продаж?', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Are you sure you want to reject sales plan?', 'Are you sure you want to reject sales plan?', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Are you sure you want to reject this granual plan?', 'Are you sure you want to reject this granual plan?', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Are you sure you want to reject this granual plan?', 'Вы уверены, что хотите отклонить этот грануальный план?', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Are you sure you want to remove this pharmacy?', 'Вы уверены, что хотите удалить эту аптеку?', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Are you sure you want to remove this pharmacy?', 'Are you sure you want to remove this pharmacy?', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Are you sure you want to save current granual plan?', 'Are you sure you want to save current granual plan?', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Are you sure you want to save current granual plan?', 'Вы уверены, что хотите сохранить текущий грануальный план?', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Are you sure, you want to close window without saving changes?', ' Вы уверены, что хотите выйты без сохранения данных?', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Are you sure, you want to delete company logo?', ' Вы уверены, что хотите удалить логотип компании?', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Are you sure, you want to delete your profile picture?', ' Вы уверены, что хотите удалить фото профиля?', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Are you sure, you want to log out?', ' Вы уверены, что хотите выйти из своего аккаунта?', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('associate', 'Associate...', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('associate', 'Сотрудник...', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('At the top is your profile. Where you can view or edit basic information about you.', ' Вверху находится ваш профиль. Где вы можете посмотреть или редактировать базовую информацию о себе.', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Attach file', 'Attach file', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Attach file', 'Прикрепить файл', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Attachments', 'Attachments', 1, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Attachments', 'Вложения', 2, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Attributes', 'Атрибуты', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('August', ' Августа', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('August', 'Август', 2, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('August', 'August', 1, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Authentication credentials could not be found.', 'Authentication credentials could not be found.', 1, 'security');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Authentication credentials could not be found.', 'Аутентификационные данные не найдены.', 2, 'security');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Authentication request could not be processed due to a system problem.', 'Запрос аутентификации не может быть обработан в связи с проблемой в системе.', 2, 'security');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Authentication request could not be processed due to a system problem.', 'Authentication request could not be processed due to a system problem.', 1, 'security');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Auto update', 'Автообновление', 2, 'geomarketing');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Auto update', 'Auto update', 1, 'geomarketing');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Automatic generation polygon', 'Автоматическое генерирование полигона', 2, 'geomarketing');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Automatic generation polygon', 'Automatic generation polygon', 1, 'geomarketing');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Autoplan', 'Авто-план', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Autoplan', 'Autoplan', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Available', 'Available', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Available', 'Доступно', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Available directions', 'Доступные направления', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Available directions', 'Available directions', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Available fields', 'Доступные поля', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Available fields', 'Available fields', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Available regions', 'Available regions', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Available regions', 'Доступные области', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Available settings', 'Доступные настройки', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Available settings', 'Available settings', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Avg prescription (uah)', 'Avg prescription (uah)', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Avg prescription (uah)', 'Среднее назначение (грн)', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Awaiting verification', 'Awaiting verification', 1, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Back', 'Назад', 2, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Back', 'Назад', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Back', 'Back', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Back', 'Back', 1, 'geomarketing');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Back', 'Назад', 2, 'geomarketing');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Back', 'Back', 1, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Back to editor', 'Back to editor', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Back to previous', ' Back to previous', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Back to previous', ' Вернуться на предыдущий шаг', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Bad credentials.', 'Неверные учетные данные.', 2, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Bad credentials.', 'Bad credentials.', 1, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Balance', 'Balance', 1, 'geomarketing');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Balance', 'Балансировать', 2, 'geomarketing');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Balancing', 'Балансировка', 2, 'geomarketing');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Balancing', 'Balancing', 1, 'geomarketing');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Balancing error code', 'Balancing error code', 1, 'geomarketing');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Balancing error code', 'Ошибка балансировки, код', 2, 'geomarketing');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Balancing result', 'Есть результат балансировки', 2, 'geomarketing');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Balancing result', 'Balancing result', 1, 'geomarketing');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Billing', 'Оплата', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Bind', 'Связать', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Bind', 'Bind', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Birth Date', 'День рождения', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Birth Date', 'Birth Date', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Birth date', 'Birth date', 1, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Birth date', 'Дата рождения', 2, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Birthdate', ' Дата рождения', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Birthday', 'Birthday', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Birthday', 'День рождения', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Body', 'Контент', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Body', 'Body', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('BOUNCED', ' Не доставлено', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('BOUNCED', ' Not delivered', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Brand', 'Brand', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Brand', 'Бренд', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Brand name', 'Brand name', 1, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Brand name', 'Бренд', 2, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('brandName', 'Бренд', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('brandName', 'Brand', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Brands', 'Бренды', 2, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Brands', 'Brands', 1, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Browse', 'Обзор', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Browse', 'Browse', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Browse', ' Обзор', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Browse', ' Browse', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Brush', 'Brush', 1, 'geomarketing');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Brush', 'Кисть', 2, 'geomarketing');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Budget', 'Budget', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Budget', 'Бюджет', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('building', ' Building', 1, 'change_requests');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('building', ' Дом', 2, 'change_requests');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Building', 'Building', 1, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Building', 'Дом', 2, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Bulk editing', 'Bulk editing', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Bulk editing', 'Массовое изменение', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Bundle disabled', 'Service is off', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Bundle disabled', 'Сервис выключен', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Bussines region', 'Bussines region', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Bussines region', 'Бизнес регион', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Button options', 'Параметры кнопки', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Button options', 'Button options', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Button URL', 'Button URL', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Button URL', 'Ссылка в кнопке', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Calendar', 'Календарь', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Calendar', 'Calendar', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Call №', 'Call №', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Call №', 'Call №', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Campaign name', 'Campaign name', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Campaign name', 'Название кампании', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Campaign was successfully saved', 'Campaign was successfully saved', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Campaign was successfully saved', 'Кампания была удачно сохранена', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Campaigns', 'Кампании', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Campaigns', 'Campaigns', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Can not edit', 'Can not edit', 1, 'geomarketing');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Can not edit', 'Редактирование недоступно', 2, 'geomarketing');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Cancel', 'Отмена', 2, 'geomarketing');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Cancel', 'Cancel', 1, 'geomarketing');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Cancel', 'Cancel', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Cancel', 'Отмена', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Cancel', 'Отмена', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Cancel', 'Cancel', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Cancel', ' Cancel', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Cancel', ' Отмена', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Cancel', 'Cancel', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Cancel', 'Отмена', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Cancel', 'Отмена', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Cancel', ' Отменить', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Cancel', 'Cancel', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Cancel', 'Отменить', 2, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Cancel', 'Cancel', 1, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Can''t get action', 'Can''t get action', 1, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Can''t get action', 'Не удалось загрузить задачу', 2, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Can''t get client', 'Can''t get client', 1, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Can''t get client', 'Не удалось загрузить карточку клиента', 2, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Can''t get company', 'Can''t get company', 1, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Can''t get company', 'Не удалось загрузить данные о компании', 2, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Can''t get plan', 'Can''t get plan', 1, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Can''t get plan', 'Не удалось загрузить план', 2, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Can''t get task', 'Can''t get task', 1, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Can''t get task', 'Не удалось загрузить визит', 2, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Caption', 'Button caption', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Caption', 'Текст кнопки', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Car', 'Автомобиль', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Car', 'Car', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Card', 'Карточка', 2, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Card', 'Card', 1, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Card', 'Картка', 3, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Card Number', 'Card Number', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Card Number', 'Номер карты', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Cards', 'Карточки', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Cards configuration', 'Настройка карточек', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Cards configuration', 'Cards configuration', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Categories', 'Categories', 1, 'geomarketing');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Categories', 'Категории', 2, 'geomarketing');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Category', 'Категория', 2, 'geomarketing');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Category', 'Category', 1, 'geomarketing');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('category', ' Category', 1, 'change_requests');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('category', ' Категория', 2, 'change_requests');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Category', 'Категория', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Category', 'Category', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Category', 'Категория', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Category', 'Category', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Category', 'Category', 1, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Category', 'Категория', 2, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Change', ' Изменить', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Change (client card)', 'Изменение (карточка клиента)', 2, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Change (institution card)', 'Изменение (карточка учреждения)', 2, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Change initiator', 'Инициатор изменений', 2, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Change responsibility', 'Изменить ответственность', 2, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Change responsibility', 'Change responsibility', 1, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Change responsibility of related records?', 'Изменить ответственность связанных записей?', 2, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Change responsibility of related records?', 'Change responsibility of related records?', 1, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Change responsible', 'Change responsible', 1, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Change responsible', 'Изменение ответственности', 2, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Changes will be lost. Do you want to save changes?', 'Изменения будут утеряны. Сохранить изменения?', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Changes will be lost. Do you want to save changes?', 'Changes will be lost. Do you want to save changes?', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('characters left', 'characters left', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('characters left', 'символов осталось', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Charts', 'Графики', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Charts', 'Charts', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Check all', 'Отметить все', 2, 'geomarketing');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Check all', 'Check all', 1, 'geomarketing');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Check your email!', 'Проверьте Ваш е-майл!', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Check your email!', 'Check your email!', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Choose', 'Выберите', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Choose', ' Выберите', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Choose', 'Choose', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Choose', 'Выберите', 2, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Choose', ' Выберите', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Choose file', ' Выберите файл', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Choose file', ' Choose file', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Choose file', 'Выберите файл', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Choose file', 'Choose file', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Choose polygon for comparison', 'Выберите полигоны для сравнения (максимум 5)', 2, 'geomarketing');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Choose polygon for comparison', 'Choose polygon for comparison (maximum 5)', 1, 'geomarketing');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Choose region', 'Choose region', 1, 'geomarketing');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Choose region', 'Выберите область', 2, 'geomarketing');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Choose region part', 'Выберите регион', 2, 'geomarketing');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Choose region part', 'Choose region part', 1, 'geomarketing');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Chosen company does not have any available task type', ' Chosen company does not have any available task type', 1, 'validators');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Chosen company does not have any available task type', ' В плане нет типов визита, доступных для данной компании', 2, 'validators');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Chosen company doesn''t have any available task type', 'В плане нет типов визита, доступных для данной компании', 2, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Chosen company doesn''t have any available task type', 'Chosen company doesn''t have any available task type', 1, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Cities', 'Города', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Cities', 'Cities', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('City', 'Город', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('city', ' Город', 2, 'change_requests');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('city', ' City', 1, 'change_requests');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('City', 'Город', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('City', 'City', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('City', 'Город', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('City', 'City', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('City', 'City', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('City', 'Город', 2, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('City', 'City', 1, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('City', ' Город', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('City', 'Город', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('City', 'City', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('City Department', 'City Department', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('City Department', 'Городской отдел', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Clear filters', 'Clear filters', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Clear filters', 'Очистить фильтры', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Click map', 'Click map', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Click map', 'Карта кликов', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('CLICKED', ' Clicked', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('CLICKED', ' Перешел по ссылке', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Client', 'Клиент', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Client', 'Клиент', 2, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Client', 'Client', 1, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Client', 'Client', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Client category', 'Категория клиента', 2, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Client category', 'Client category', 1, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Client not found', 'Client not found', 1, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Client not found', 'Клиент не найден', 2, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Client type', 'Тип клиента', 2, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Client type', 'Client type', 1, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('CLIENT_REQUEST_TYPE_1', 'Новый клиент', 2, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('CLIENT_REQUEST_TYPE_2', 'Изменение параметров клиента', 2, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('CLIENT_REQUEST_TYPE_3', 'Деактивация клиента', 2, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('CLIENT_REQUEST_TYPE_4', 'Добавление данных в базу', 2, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Clients', 'Clients', 1, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Clients', 'Клиенты', 2, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Clients name', 'Clients name', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Clients name', 'ФИО клиента', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('CLM', 'CLM', 1, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('CLM', 'CLM', 2, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('CLM Manager', ' CLM Менеджер', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('CLM Manager', ' CLM Manager', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Close', ' Close', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Close', ' Закрыть', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('close', 'Close', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Close', 'Закрыть', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Close', 'Close', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Close', 'Закрыть', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('close', 'Закрыть', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Close', 'Close', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Close', 'Close', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Close', 'Закрыть', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Close', 'Close', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Close', 'Закрыть', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Close', 'Закрыть', 2, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Close', 'Close', 1, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Close', 'Закрыть', 2, 'geomarketing');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Close', 'Close', 1, 'geomarketing');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Close', 'Close', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Close', 'Закрыть', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Coach already added', 'Coach already added', 1, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Coach already added', 'Тренер уже добавлен', 2, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Coaches', 'Тренеры', 2, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Coaches', 'Coaches', 1, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Code', 'Code', 1, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Code', 'Код', 2, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Code', 'Код', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Code', 'Code', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('code', ' Code', 1, 'change_requests');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('code', ' Код', 2, 'change_requests');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Coefficient', 'Coefficient', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Coefficient', 'Коэфициент', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Collapse All', 'Collapse All', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Collapse All', 'Свернуть все', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Collapse All Categories', 'Свернуть все категории', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Collapse All Categories', 'Collapse All Categories', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Collapse Category', 'Collapse Category', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Collapse Category', 'Свернуть категорию', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Collapse Option', 'Свернуть настройку', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Collapse Option', 'Collapse Option', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Color', 'Color', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Color', 'Цвет', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('color', 'Polygon color', 1, 'geomarketing');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Color', 'Polygon color', 1, 'geomarketing');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Color', 'Цвет', 2, 'geomarketing');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('color', 'Цвет полигона', 2, 'geomarketing');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Column', 'Column', 1, 'geomarketing');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Column', 'Колонки', 2, 'geomarketing');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Column groups', 'Группы колонок', 2, 'geomarketing');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Column groups', 'Column groups', 1, 'geomarketing');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Column name', 'Название колонки', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Column name', 'Column name', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Column names in first line', 'Названия колонок в первой строке', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Column names in first line', 'Column names in first line', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Column options', 'Column options', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Column options', 'Настройки колонки', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Columns missing', 'Отсутствуют колонки', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Columns missing', 'Columns missing', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Comment', 'Comment', 1, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Comment', 'Комментарий', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Comment', 'Comment', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Comment', 'Комментарий', 2, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Comment 0', 'Comment', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Comment 0', 'Комментарий', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Comment 1', 'Комментарий 1', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Comment 1', 'Comment 1', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Comment 2', 'Comment 2', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Comment 2', 'Комментарий 2', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Comment field is empty', 'Comment field is empty', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Comment field is empty', 'Поле комментарий не заполнено', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Comment saved', 'Ваш комментарий сохранен', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Comment saved', 'Comment saved', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('comments', 'Комментарии к обращению', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('comments', 'Comments', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Compactness', 'Compactness', 1, 'geomarketing');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Compactness', 'Компактность территории', 2, 'geomarketing');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Companies', 'Учреждения', 2, 'geomarketing');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Companies', 'Учреждения', 2, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Companies', 'Companies', 1, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Companies', 'Companies', 1, 'geomarketing');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Companies', 'Аптеки', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Companies', 'Pharmacies', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Companies', 'Учреждения', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Companies', 'Companies', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Companies not found', 'Companies not found', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Companies not found', 'Компании не найдены', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Company', 'Company', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Company', 'Учреждение', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Company', 'Pharmacy', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Company', 'Аптека', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Company', 'Учреждение', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Company', 'Company', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('company', ' Company', 1, 'change_requests');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('company', ' Компания', 2, 'change_requests');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Company', 'Company', 1, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Company', 'Компания', 2, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Company address', 'Company address', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Company address', 'Адрес учреждения', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Company category', 'Company category', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Company category', 'Категория компании', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Company category', 'Company category', 1, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Company category', 'Категория учреждения', 2, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Company name', 'Название', 2, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Company not found', 'Компания не найдена', 2, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Company not found', 'Company not found', 1, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Company selection ', 'Выбрать компанию', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Company selection', 'Company selection', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Company Type', 'Тип учреждения', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Company Type', 'Company Type', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Company type', 'Company type', 1, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Company type', 'Тип компании', 2, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Company type', 'Тип учреждения', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Company types', 'Типы учреждений', 2, 'geomarketing');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Company types', 'Company types', 1, 'geomarketing');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Company verification', 'Company verification', 1, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Company verification', 'Верификация учреждения', 2, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Company visits', 'Company visits', 1, 'geomarketing');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Company visits', 'Визиты в учреждения', 2, 'geomarketing');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('COMPANY_REQUEST_TYPE_1', 'Новое учреждение', 2, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('COMPANY_REQUEST_TYPE_2', 'Изменение параметров учреждения', 2, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('COMPANY_REQUEST_TYPE_3', 'Деактивация учреждения', 2, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('COMPANY_REQUEST_TYPE_4', 'Добавление данных в базу', 2, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('companyCategories', 'Учреждения по категориям', 2, 'geomarketing');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('companyCategories', 'Company per categories', 1, 'geomarketing');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('companyCategory', 'Company categories', 1, 'geomarketing');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('companyCategory', 'Категории учреждений', 2, 'geomarketing');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('companycategory', ' Category', 1, 'change_requests');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('companycategory', ' Категория', 2, 'change_requests');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('companyCnt', 'Учреждений', 2, 'geomarketing');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('companyCnt', 'Companies', 1, 'geomarketing');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('companytype', ' Тип компании', 2, 'change_requests');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('companytype', ' Company type', 1, 'change_requests');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('companyTypes', 'Company per type', 1, 'geomarketing');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('companyTypes', 'Учреждения по типам', 2, 'geomarketing');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Compare', 'Сравнить', 2, 'geomarketing');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Compare', 'Compare', 1, 'geomarketing');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('comparison', 'Comparison', 1, 'geomarketing');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('comparison', 'Сравнение', 2, 'geomarketing');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Comparison limit', 'Выберите от двух до пяти элементов', 2, 'geomarketing');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Comparison limit', 'Choose from two to five items', 1, 'geomarketing');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Comparison per', 'Comparison per:', 1, 'geomarketing');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Comparison per', '"Сравнение по:"', 2, 'geomarketing');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Competitive group', 'Конкурентные группы', 2, 'geomarketing');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Competitive group', 'Competitive group', 1, 'geomarketing');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Completed', 'Completed', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Completed', 'Завершено', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Concentration of population', ' Concentration of population', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Concentration of population', ' Концентрация населения', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Conduction first visit', 'Conduction first visit', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Conduction first visit', 'Совершение первого визита', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Conduction the first visit', 'Первый визит', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Conduction the first visit', 'Conduction the first visit', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Conduction visit', 'Conduction visit', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Conduction visit', 'МП совершил визит', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Config grid column', 'Config grid column', 1, 'geomarketing');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Config grid column', 'Настроить колонки', 2, 'geomarketing');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Config group grid column', 'Настроить группы колонок', 2, 'geomarketing');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Config group grid column', 'Config group grid column', 1, 'geomarketing');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('confirm', 'Confirm', 1, 'geomarketing');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('confirm', 'Подтвердите', 2, 'geomarketing');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Confirm', 'Подтвердить', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Confirm', 'Confirm', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Confirm', 'Подтверждение', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Confirm', 'Confirmation', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Confirm deletion', 'Подтвердите удаление', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Confirm deletion', 'Confirm deletion', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Confirm deletion', ' Подтвердите удаление', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Confirm deletion', ' Confirm deletion', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Confirmation', 'Confirmation', 1, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Confirmation', 'Подтверждение', 2, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Contact category', 'Категория специалиста', 2, 'geomarketing');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Contact category', 'Contact category', 1, 'geomarketing');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Contact helpdesk', 'Contact helpdesk', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Contact helpdesk', 'Обратиться в helpdesk', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Contact owner', 'Contact owner', 1, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Contact status', 'Contact status', 1, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Contact status', 'Статус контакта', 2, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Contact type', 'Тип клиента', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Contact Type', 'Contact Type', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Contact us', ' Контакты', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Contact verification', 'Верификация клиента', 2, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Contact_owner_dcr', 'Ответственность', 2, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('contactCategory', 'Специалистов по категориям', 2, 'geomarketing');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('contactCategory', 'Contacts per category', 1, 'geomarketing');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('contactCnt', 'Contacts', 1, 'geomarketing');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('contactCnt', 'Специалистов', 2, 'geomarketing');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Contacts', 'Сотрудники', 2, 'geomarketing');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Contacts', 'Контакты', 2, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Contacts', 'Contacts', 1, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Contacts', 'Contacts', 1, 'geomarketing');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Contacts', 'Клиенты', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Contacts', 'Contacts', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Contacts concentration', 'Contacts concentration', 1, 'geomarketing');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Contacts concentration', 'Концентрация специалистов', 2, 'geomarketing');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('contactshop', ' Прескрайбер-аптека', 2, 'change_requests');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('contactshop', ' Prescriber pharmacy', 1, 'change_requests');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('contactSpecialization', 'Специалистов по специализациям', 2, 'geomarketing');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('contactSpecialization', 'Contacts per specialization', 1, 'geomarketing');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('contactTargetGroups', 'Contacts per target groups', 1, 'geomarketing');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('contactTargetGroups', 'Специалистов по таргет-группам', 2, 'geomarketing');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('contacttype', ' Contact type', 1, 'change_requests');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('contacttype', ' Тип контакта', 2, 'change_requests');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('contactTypes', 'Специалистов', 2, 'geomarketing');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('contactTypes', 'Contacts', 1, 'geomarketing');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('contains', 'Contains', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Contains', 'Содержит', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Content', 'Content', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Content', 'Контент', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Content between "starts with" and "ends with" will be extracted', 'Содержимое ячейки между "после" и "до" будет извлечено', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Content between "starts with" and "ends with" will be extracted', 'Content between "starts with" and "ends with" will be extracted', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Content has been successfully archived', 'Content has been successfully archived', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Content has been successfully archived', 'Контент заархивирован', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Content not found', 'Контент отсутствует', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Content not found', 'Content not found', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Content was not archived', 'Content was not archived', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Content was not archived', 'Контент не заархивирован', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Continue importing', 'Continue importing', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Continue importing', 'Продолжить импорт', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Contract date', 'Contract date', 1, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Contract date', 'Дата заключения контракта', 2, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Contract number', 'Номер контракта', 2, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Contract number', 'Contract number', 1, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Contract price', 'Contract price', 1, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Contract price', 'Цена контракта', 2, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Contract status', 'Статус контракта', 2, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Contract status', 'Contract status', 1, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Contracts', 'Contracts', 1, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Contracts', 'Контракты', 2, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Control', 'Контрольный', 2, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Control', 'Control', 1, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Control', 'Контроль', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Control', 'Control', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Control options', 'Настройки', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Control settings', 'Настройки контроля', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Converting', 'Converting', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Converting', 'Конвертирование', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Cookie has already been used by someone else.', 'Cookie has already been used by someone else.', 1, 'security');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Cookie has already been used by someone else.', 'Cookie уже был использован кем-то другим.', 2, 'security');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Coordinate obtained successfully', 'Coordinate ontained successfully', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Coordinate obtained successfully', 'Координата снята успешно', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Coordinate obtainment', 'Успешность снятия координаты', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Coordinate obtainment', 'Coordinate obtainment', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Coordinate obtainment accuracy', 'Coordinate obtainment accuracy', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Coordinate obtainment accuracy', 'Точность снятия координаты(м)', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('coordinates', 'Координаты', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('coordinates', 'Coordinates', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Copy', 'Copy', 1, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Copy', 'Копировать', 2, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Copy from another task', 'Копировать из другого визита', 2, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Copy from another task', 'Copy from another task', 1, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Copy plan', 'Copy plan', 1, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Copy plan', 'Копировать план', 2, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Copy settings', 'Копировать настройки', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Count', 'Count', 1, 'geomarketing');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Count', 'Кол.', 2, 'geomarketing');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Count', 'Количество', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Count', 'Count', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Country', 'Страна', 2, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Country', 'Country', 1, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Create', 'Create', 1, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Create', 'Создать', 2, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Create', 'Создать', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Create additional field', 'Добавить доп. поле', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Create additional field', 'Create additional field', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Create additional fields', 'Создать дополнительное поле', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Create additional fields', 'Create additional fields', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Create company', 'Create company', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Create company', 'Создать организацию', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Create company', 'Create company', 1, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Create company', 'Создать учреждение', 2, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Create contact', 'Создать клиента', 2, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Create contact', 'Create contact', 1, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Create Email', 'Create Email', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Create Email', 'Создать Email', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Create field', 'Создать поле', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Create field', 'Create field', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Create new', 'Создать нового', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Create new user', 'Создать нового пользователя', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Create new user', 'Create new user', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Create polygon', 'Create polygon', 1, 'geomarketing');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Create polygon', 'Создание полигона', 2, 'geomarketing');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Create template', 'Создать шаблон', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Create template', 'Create template', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Created', 'Created', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Created', 'Создана', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('creator', 'Creator', 1, 'geomarketing');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('creator', 'Создатель', 2, 'geomarketing');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Creator', 'Создатель', 2, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Creator', 'Creator', 1, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Credentials have expired.', 'Время действия аутентификационных данных истекло.', 2, 'security');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Credentials have expired.', 'Credentials have expired.', 1, 'security');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Cur.month', 'Cur.month', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Cur.month', 'Тек.месяц', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Current', 'Current', 1, 'geomarketing');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Current', 'Текущая', 2, 'geomarketing');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Current month', 'Текущий месяц', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Current month', 'Current month', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('current user', 'current user', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('current user', 'текущий пользователь', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Current value', 'Текущее значение', 2, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Custom dictionary', 'Справочник', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Custom dictionary', 'Custom dictionary', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Cyrillic', 'Cyrillic', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Cyrillic', 'Кириллица', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('D', 'D', 1, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Data processing', 'Обработка данных', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Data processing', 'Data processing', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Data type', 'Data type', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Data type', 'Тип данных', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Data was saved!', ' Данные были сохранены!', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Data were processed successfully', 'Данные успешно обработаны', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Date', 'Дата', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Date', 'Дата', 2, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Date', 'Date', 1, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Date', 'Date', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Date', 'Date', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Date', 'Date', 1, 'geomarketing');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Date', 'Дата', 2, 'geomarketing');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('date', 'Дата', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('date', 'Date', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Date', 'Дата', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Date', 'Дата', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Date', 'Date', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Date and time', 'Дата и время', 2, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Date and time', 'Date and time', 1, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Date correction', 'Корректировка даты', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Date correction', 'Date correction', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Date finish testing', 'Date finish testing', 1, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Date finish testing', 'Дата окончания тестирования', 2, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Date from', 'Date from', 1, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Date from', 'Дата начала', 2, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Date of change', 'Дата изменения', 2, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Date of placement of the purchase', 'Дата размещения закупки', 2, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Date of placement of the purchase', 'Date of placement of the purchase', 1, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Date start testing', 'Date start testing', 1, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Date start testing', 'Дата начала тестирования', 2, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Date till', 'Дата окончания', 2, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Date till', 'Date till', 1, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('date1', 'Дата(1)', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('date2', 'Дата(2)', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('date3', 'Дата(3)', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('dateEnd', 'Date end', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('dateEnd', 'Дата окончания', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('dateStart', 'Дата начала', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('dateStart', 'Date start', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('datetime', 'дата', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('datetime', 'date', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Day', 'Day', 1, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Day', 'день', 2, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('DD', ' ДД', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Deactivate client', 'Deactivate client', 1, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Deactivate company', 'Deactivate company', 1, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('December', 'December', 1, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('December', 'Декабрь', 2, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('December', ' Декабря', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Default Value', 'Default Value', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Default Value', 'Значение по умолчанию', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('DEFERRED', ' Not delivered', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('DEFERRED', ' Не доставлено', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Delayed', 'Отложено', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Delayed', 'Delayed', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Delete', ' Delete', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Delete', 'Удалить', 2, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Delete', 'Delete', 1, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Delete', ' Удалить', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Delete', 'Удалить', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Delete action?', 'Delete action?', 1, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Delete action?', 'Удалить задачу?', 2, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Delete file', 'Удалить файл', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Delete file', 'Delete file', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Delete photo', ' Удалить фото', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Delete plan', 'Удалить выбранный план?', 2, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Delete plan detail', 'Удалить выбранные детали плана?', 2, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Delete plan detail preparation', 'Удалить выбранный бренд?', 2, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Delete plan detail preparations', 'Удалить выбранные бренды?', 2, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Delete plan detail presentation', 'Удалить презентацию', 2, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Delete plan detail presentations', 'Удалить презентации', 2, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Delete plan detail promo', 'Удалить промо материал', 2, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Delete plan detail promos', 'Удалить промо материалы', 2, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Delete plan details', 'Удалить выбранные детали плана?', 2, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Delete plan preparation', 'Удалить выбранный препарат?', 2, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Delete plan preparations', 'Удалить выбранные препараты?', 2, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Delete plans', 'Удалить выбранные планы?', 2, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Delete row', 'Удалить запись', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Delete row', 'Delete row', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Delete selected plan detail?', 'Delete selected plan detail?', 1, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Delete selected plan details preparation?', 'Delete selected plan details preparation?', 1, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Delete selected plan details preparations?', 'Delete selected plan details preparations?', 1, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Delete selected plan details?', 'Delete selected plan details?', 1, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Delete selected plan getails presentation?', 'Delete selected plan getails presentation?', 1, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Delete selected plan getails presentations?', 'Delete selected plan getails presentations?', 1, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Delete selected plan getails promo material?', 'Delete selected plan getails promo material?', 1, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Delete selected plan getails promo materials?', 'Delete selected plan getails promo materials?', 1, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Delete selected plan?', 'Delete selected plan?', 1, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Delete selected plans?', 'Delete selected plans?', 1, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Delete selected preparation?', 'Delete selected preparation?', 1, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Delete selected preparations?', 'Delete selected preparations?', 1, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Delete task?', 'Delete task?', 1, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Delete task?', 'Удалить визит?', 2, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Delete untreated', 'Delete untreated', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Delete untreated', 'Удалить необработанные', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('delete.constraint.error', ' "Unable to delete selected rows: there are related records"', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('delete.constraint.error', 'Невозможно удалить запись: существуют связанные записи', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Deleting', 'Deleting...', 1, 'geomarketing');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Deleting', 'Удаление...', 2, 'geomarketing');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Deletion confirmation', ' Вы уверены, что хотите удалить эту презентацию?', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Deletion confirmation', ' Are you sure, you want to delete this item?', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('DELIVERED', ' Delivered', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('DELIVERED', ' Доставлено', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Delivered', 'Delivered', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Delivered', 'Разослано', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Delivery schedule', 'Delivery schedule', 1, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Delivery schedule', 'График поставки', 2, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Delta ', 'Разбежность', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Delta', 'Delta', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Department', 'Department', 1, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Department', 'Отделение', 2, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Description', 'Описание', 2, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Description', 'Description', 1, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Description', 'Описание', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Description', 'Description', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('description', ' Описание', 2, 'change_requests');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('description', ' Description', 1, 'change_requests');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Deselect all', 'Снять все', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Details', 'Details', 1, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Details', 'Содержание', 2, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('details', 'Details', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('details', 'Детали обращения', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Dictionaries', 'Dictionaries', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Dictionaries', 'Справочники', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Dictionary', 'Справочник', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Dictionary', 'Dictionary', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('direction', 'Prod. direction', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('direction', 'Прод. направление', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Direction', 'Продуктовое направление', 2, 'geomarketing');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Direction', 'Direction', 1, 'geomarketing');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Directions', 'Directions', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Directions', 'Направления', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Directory is not configured for current user', ' Directory is not configured for current user.', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Directory is not configured for current user', ' Для вас не настроена папка для загрузки CLM', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Disable editing', 'Запрет редактирования', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Disable editing', 'Disable editing', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('disapprove', 'отклонен', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('disapprove', 'disapproved', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Dismiss', ' Отменить', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Distance from', 'Дистанция от', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Distance from', 'Distance from', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Distribution', 'Distribution', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Distribution', 'Рассылки', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Distribution %name%', ' Рассылка %name%', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Distribution %name%', ' Distribution %name%', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Distribution can not be changed after start sending', 'Рассылка не может быть изменена после начала отправки сообщений', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Distribution can not be changed after start sending', 'Distribution can not be changed after start sending', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Distribution date and time', 'Distribution date and time', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Distribution date and time', 'Плановая дата и время рассылки', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Distribution is creating, please wait', 'Рассылка создается, пожалуйста, подождите', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Distribution is creating, please wait', 'Distribution is creating, please wait', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Distribution is deleting, please wait', 'Distribution is deleting, please wait', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Distribution is deleting, please wait', 'Рассылка удаляется, пожалуйста, подождите', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Distribution name', 'Название рассылки', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Distribution name', 'Distribution name', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Distribution report', ' Distribution report', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Distribution report', ' Отчет по рассылке', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Distribution statistics', 'Distribution statistics', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Distribution statistics', 'Статистика по рассылке', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Distribution statistics not available', 'Статистика рассылки не доступна', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Distribution statistics not available', 'Distribution statistics not available', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Distribution Type', 'Distribution Type', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Distribution Type', 'Тип рассылки', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Distributions not found', 'Рассылки отсутствуют', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Distributions not found', 'Distributions not found', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Distributor', 'Дистрибютор', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Distributor', 'Distributor', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Distributors', 'Distributors', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Distributors', 'Дистрибьюторы', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('District', 'District', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('District', 'Область', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('district', ' District', 1, 'additional_fields');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('district', ' Округ', 2, 'additional_fields');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('district', ' District', 1, 'change_requests');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('district', ' Округ', 2, 'change_requests');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Doctor', 'ФИО врача', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Doctor', 'Doctor', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('doctors', 'Doctors', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('doctors', 'Врачи', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('done', 'Done', 1, 'status');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('done', 'Обработанные', 2, 'status');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('done', 'Done', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('done', 'Обработанные', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Download', 'Download', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Download', 'Скачать', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Download', 'Download', 1, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Download', 'Загрузить', 2, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Download a template to fill in the data', 'Загрузите шаблон для заполнения данных', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Download template', 'Скачать шаблон', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Download template', 'Download template', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Download unprocessed data', 'Скачать необработанные данные', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Drag and drop files here or click to upload', 'Перетащите нужные файлы сюда или нажмите для добавления файлов', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Drag and drop files here or click to upload', 'Drag and drop files here or click to upload', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Drag and drop files into the workspace to load', 'Drag and drop files into the workspace to load', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Drag and drop files into the workspace to load', 'Перетащите файлы в рабочую область для загрузки', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Drop', 'Сбросить', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Drop', 'Drop', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Drop file or click to upload.', 'Перетащите файл или нажмите, чтобы загрузить.', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Drop file or click to upload.', 'Drop file or click to upload.', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Drop your .csv file or', 'Drop your .csv file or', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Drop your Excel file or', 'Перетяните Excel файл или', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Drop your file here', 'Перетащите файл сюда', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Drug', 'Препарат', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Drug', 'Drug', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Drug form', 'Drug form', 1, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Drug form', 'Лекарственная форма', 2, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Drugstore', 'Аптека', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Drugstore', 'Drugstore', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('DUPLICATE', ' Дубликат', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('DUPLICATE', ' Duplicate', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Duplicate columns', 'Duplicate columns', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Duplicate columns', 'Повторяются названия колонок', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Each block has name and short service description. To start working with click on block', ' На каждом блоке есть название и короткое описание сервиса. Для того чтобы начать работу с ним кликните на соответствующем блоке.', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('eaddr', ' Электронный адрес', 2, 'change_requests');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('eaddr', ' E-mail', 1, 'change_requests');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Edit', 'Edit', 1, 'geomarketing');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Edit', 'Edit', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Edit', 'Редактировать', 2, 'geomarketing');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Edit', 'Редактировать', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Edit account', 'Редакторовать учреждение', 2, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Edit account', 'Edit account', 1, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Edit account request', 'Edit account request', 1, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Edit account request', 'Редактировать запрос учреждения', 2, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Edit action', 'Редактировать задачу', 2, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Edit action', 'Edit action', 1, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Edit brand', 'Edit brand', 1, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Edit brand', 'Редактировать бренд', 2, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Edit campaign', 'Edit campaign', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Edit campaign', 'Редактирование кампании', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Edit client', 'Редактирование клиента', 2, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Edit client', 'Edit client', 1, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Edit client request', 'Edit client request', 1, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Edit client request', 'Запрос на редактирование клиента', 2, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Edit company', 'Edit company', 1, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Edit company', 'Редактирование учреждения', 2, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Edit company request', 'Запрос на редактирование учреждения', 2, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Edit company request', 'Edit company request', 1, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Edit contact', 'Редакторовать контакт', 2, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Edit contact', 'Edit contact', 1, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Edit contact request', 'Edit contact request', 1, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Edit contact request', 'Редактировать запрос контакта', 2, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Edit dictionary', 'Редактировать справочник', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Edit Email', 'Редактировать Email', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Edit Email', 'Edit Email', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Edit group', 'Редактировать группу', 2, 'geomarketing');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Edit group', 'Edit group', 1, 'geomarketing');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Edit HTML', 'Edit HTML', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Edit HTML', 'Редактировать HTML', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Edit photo', ' Редактирование фото', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Edit plan', 'Редактировать план', 2, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Edit plan', 'Edit plan', 1, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Edit plan detail', 'Edit plan detail', 1, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Edit plan detail', 'Редактировать детали плана', 2, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Edit plan preparation', 'Редактировать бренд', 2, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Edit plan preparation', 'Edit plan preparation', 1, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Edit plan presentation', 'Edit plan presentation', 1, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Edit plan presentation', 'Редактировать презентацию', 2, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Edit plan promo', 'Редактировать промо материал', 2, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Edit plan promo material', 'Edit plan promo material', 1, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Edit preparation', 'Редактировать препарат', 2, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Edit preparation', 'Edit preparation', 1, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Edit profile', 'Редактировать', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Edit profile', 'Edit profile', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Edit promo material', 'Редактировать промо материал', 2, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Edit promo material', 'Edit promo material', 1, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Edit row', 'Редактировать запись', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Edit row', 'Edit row', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Edit SMS Template', 'Редактировать SMS шаблон', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Edit SMS Template', 'Edit SMS Template', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Edit specialization', 'Редактировать специализацию', 2, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Edit specialization', 'Edit specialization', 1, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Edit Target Audience', 'Edit Target Audience', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Edit Target Audience', 'Редактировать ЦА', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Edit task', 'Редактировать визит', 2, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Edit Task', 'Edit Task', 1, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Edit user', 'Редактировать пользователя', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Edit user', 'Edit user', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Edit value', 'Редактировать значение', 2, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Edit workplace', 'Edit workplace', 1, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Edit workplace', 'Редактировать место работы', 2, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Email', 'Е-майл', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Email', 'Email', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Email', ' Электронная почта', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Email', ' Email', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Email', ' Email', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('E-mail', 'E-mail', 1, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('E-mail', 'Электронная почта', 2, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('E-mail', ' Логин', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Email %email% was successfully added to this list', 'Электронный адрес %email% был добавлен в базу рассылки', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Email %email% was successfully added to this list', 'Email %email% was successfully added to database', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Email %email% was successfully removed from this list', 'Электронный адрес %email% был удален из базы рассылки', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Email %email% was successfully removed from this list', 'Email %email% was successfully removed from database', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Email body', 'Контент', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Email body', 'Email body', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Email Distribution script', 'Email Distribution script', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Email Distribution script', 'Сценарий Email рассылки', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Email does not exists', ' Адрес электронной почты не зарегистрирован', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Email subject', 'Тема', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Email subject', 'Email subject', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Email Template', 'Email Template', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Email Template', 'Email Шаблон', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Email title', 'Название', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Email title', 'Email title', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Employee', 'Сотрудник', 2, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Employee', 'Employee', 1, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Employees', 'Employees', 1, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Employees', 'Сотрудники', 2, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Empty brands', 'Нет брендов', 2, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Empty list', 'Empty list', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Empty list', 'Список пуст', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Empty presentations', ' Нет презентаций', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Empty presentations', ' Empty presentations', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Empty promos', 'Нет промо материалов', 2, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('EMPTY_RECIPIENT', ' Empty Recipient', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('EMPTY_RECIPIENT', ' Пустой получатель', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('emptyComments', 'Комментариев нет', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('emptyComments', 'There are no comments', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('emptyDetails', 'There are no details', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('emptyDetails', 'Деталей нет', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('emptyList', 'Список пуст', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('emptyList', 'No data', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('emptyResponsibles', 'Нет ответсвенных', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('emptyResponsibles', 'empty responsible users', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('emptyUsers', 'Users not found.', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('emptyUsers', 'Пользователей нет', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('en', 'English', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('en', 'English', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Enable', ' Подключить', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Enabled', 'Включено', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Ends with', 'Заканчивается на', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Ends with', 'Ends with', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('endsWith', 'Ends with', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('endsWith', 'Заканчивается', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Enter a new password', ' Введите новый пароль длиной от 6 символов', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Enter polygon name', 'Enter polygon name', 1, 'geomarketing');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Enter polygon name', 'Введите имя полигона', 2, 'geomarketing');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Enter regex expression. Use "match" group to specify content to extract', 'Введите регулярное выражение. Используйте название группы "match", чтобы извлечь содержимое', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Enter regex expression. Use "match" group to specify content to extract', 'Enter regex expression. Use "match" group to specify content to extract', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Enter text', 'Enter text', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Enter text', 'Введите текст', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Enter the start and end date', 'Enter the start and end date', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Enter the start and end date', 'Введите дату начала и окончания', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Enter the start and end time', 'Введите время начала и окончания', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Enter the start and end time', 'Enter the start and end time', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Enter your credentials to start working with our system.', ' Для начала работы с сервисом введите логин и пароль. Если вы забыли пароль воспользуйтесь восстановлением или обратитесь к своему администратору.', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Enter your email address and we will send you a letter', ' Введите почту и мы отправим Вам письмо!', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Enter your email address and we will send you a link to restore access to your account', ' Введите электронный адрес и мы отправим вам ссылку для восстановления доступа к аккаунту', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Equals', 'Равно', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('equals', 'Equals', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Erase', 'Стереть', 2, 'geomarketing');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Erase', 'Erase', 1, 'geomarketing');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Erase all', 'Erase all', 1, 'geomarketing');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Erase all', 'Стереть все', 2, 'geomarketing');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Erase by filter', 'Стереть по фильтру', 2, 'geomarketing');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Erase by filter', 'Erase by filter', 1, 'geomarketing');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Erase by users and regions', 'Erase by users and regions', 1, 'geomarketing');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Erase by users and regions', 'Стереть по пользователям и областям', 2, 'geomarketing');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Erasing', 'Стирание', 2, 'geomarketing');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Erasing', 'Erasing', 1, 'geomarketing');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('error', 'Ошибка!', 2, 'geomarketing');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Error', 'Ошибка', 2, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Error', 'Error', 1, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('error', 'Error!', 1, 'geomarketing');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Error', 'Ошибка', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Error', 'Error', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Error 404', 'Error 404', 1, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Error 404', 'Ошибка 404', 2, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Error starting balance, code', 'Error starting balance, code', 1, 'geomarketing');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Error starting balance, code', 'Ошибка старта балансировки, код', 2, 'geomarketing');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Essence', 'Essence', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Essence', 'Суть', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Evaluation form', 'Форма оценки', 2, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Evaluation form', 'Evaluation form', 1, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('EXCLUDED', ' Исключен', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('EXCLUDED', ' Excluded', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('exit', 'Выйти', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('exit', 'Exit', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Expand All', 'Expand All', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Expand All', 'Развернуть все', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Expand All Categories', 'Развернуть все категории', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Expand All Categories', 'Expand All Categories', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Expand Category', 'Expand Category', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Expand Category', 'Развернуть категорию', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Expand Option', 'Развернуть настройку', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Expand Option', 'Expand Option', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Export to Excel', 'Экспорт в Excel', 2, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Export to Excel', 'Export to Excel', 1, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Export to Excel', 'Export to Excel', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Export to Excel', 'Экспорт в Excel', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('exportXLS', 'Экспорт в Excel', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('exportXLS', 'Export to Excel', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Extended search', 'Found by id_link', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Extended search', 'Показать все', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('External ID', 'Внешний идентификатор пользователя', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('External ID', 'External ID', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('External link', 'Внешняя ссылка', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('External link', 'External link', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Fact', 'Факт', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Fact', 'Fact', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Fact all', 'Fact all', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Fact all', 'Всего факт', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Fact count', 'Фактическое количество', 2, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Fact count', 'Fact count', 1, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Fact plan', 'Fact plan', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Fact plan ', 'Факт план', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Fact plan successfully updated', 'Факт план успешно изменен', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Fact plan successfully updated', 'Fact plan successfully updated', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Fact route(clustered)', 'Факт. маршрут(кластеризованный)', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Fact route(clustered)', 'Fact route(clustered)', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Fact route(task)', 'Fact route(task)', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Fact route(task)', 'Факт. маршрут(визит)', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Fact route(tracker)', 'Факт. маршрут(трекер)', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Fact route(tracker)', 'Fact route(tracker)', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Failed', 'Failed', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Failed', 'Не доставлено', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Failed to obtain coordinate', 'Failed to obtain coordinate', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Failed to obtain coordinate', 'Не удалось снять координату', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Fatal error', 'Не удалось обновить', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Fatal error', 'Fatal Error', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('February', 'February', 1, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('February', 'Февраль', 2, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('February', ' Февраля', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Federal subject', 'Область', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Federal subject', 'State', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Feedback', 'Обратная связь', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Feedback', 'Feedback', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Female', 'Женский', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Female', 'Female', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Field', 'Поле', 2, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Field %field% has reference and cannot be deleted', 'Field %field% has reference and cannot be deleted', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Field %field% has reference and cannot be deleted', ' Поле %field% имеет связанную запись и не может быть удалено', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Field has reference and cannot be deleted', 'Поле имеет связанную запись и не может быть удалено', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Field is required', 'Поле является обязательным', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Field is required', 'Field is required', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Field marked as hidden', 'Поле помечено как скрытое', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Field marked as not required', 'Поле помечено как не обязательное', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Field marked as required', 'Поле помечено как обязательное', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Field marked as visible', 'Поле помечено как видимое', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Field name', 'Field name', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Field name', 'Связанное поле', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Fields saved', 'Поля сохранены', 2, 'geomarketing');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Fields saved', 'Fields saved', 1, 'geomarketing');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('File', 'Файл', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('File', 'File', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('File already exists', 'File already exists.', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('File already exists', 'Файл с таким именем уже существует.', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('File already imported', 'File already imported', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('File already imported', 'Файл уже импортирован', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('File can not be associated with pattern. Lacking fields', 'Файл не может быть сопоставлен с выбранным шаблоном. Не хватает колонок', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('File can not be associated with pattern. Lacking fields', 'File can not be associated with pattern. Lacking fields', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('File extension is not supported.', 'Расширение файла не поддерживается. Выберите другой файл', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('File extension is not supported.', 'File extension is not supported. Select another file', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('File extension is not supported.', 'File extension is not supported. Select another file', 1, 'validators');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('File extension is not supported.', 'Расширение файла не поддерживается. Выберите другой файл', 2, 'validators');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('File is not found or removed. Please, attach another one.', 'File is not found or removed. Please, attach another one.', 1, 'validators');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('File is not found or removed. Please, attach another one.', 'Файл на найден или удалён, пожалуйста, прикрепите другой.', 2, 'validators');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('File Name', ' Название файла', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('File Name', ' File Name', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('File Name', 'Название файла', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('File Name', 'File Name', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('File not found', 'Файл не найден', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('File not found', 'File not found', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('File not selected', 'Файл не выбран', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('File slides.json does not exists', ' File slides.json does not exists', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('File slides.json does not exists', ' Файл со слайдами не найден', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('File slides.json is incorrect', ' Файл со слайдами некорректно отформатирован', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('File slides.json is incorrect', ' File slides.json is incorrect', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('File successfully imported to database', 'Файл успешно импортирован в базу данных', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('File successfully imported to database', 'File successfully imported to database', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('File type does not suit its extension. To import this file save it as one of following types .csv, .xls, .xlsx, .xlsm', 'File type does not suit its extension. To import this file save it as one of following types .csv, .xls, .xlsx, .xlsm', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('File type does not suit its extension. To import this file save it as one of following types .csv, .xls, .xlsx, .xlsm', 'File type does not suit its extension. To import this file save it as one of following types .csv, .xls, .xlsx, .xlsm', 1, 'validators');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('File type does not suit its extension. To import this file save it as one of following types .csv, .xls, .xlsx, .xlsm', 'Формат файла не соответствует расширению файла. Для успешной загрузки пересохраните файл в одном из расширений .csv, .xls, .xlsx, xlsm', 2, 'validators');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('File type does not suit its extension. To import this file save it as one of following types .csv, .xls, .xlsx, .xlsm', 'Формат файла не соответствует расширению файла. Для успешной загрузки пересохраните файл в одном из расширений .csv, .xls, .xlsx, xlsm', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('File upload', 'Загрузка файла', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('File upload', 'File upload', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('File uploaded', ' File uploaded', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('File uploaded', ' Файл выбран', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('File uploading', ' Файл загружается', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('File uploading', ' File uploading', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('File(s) uploaded successfully', 'Файл(ы) успешно загружены', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('File(s) uploaded successfully', 'File(s) uploaded successfully', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Files', 'Files', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Files', 'Файлы', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Filled', 'Filled', 1, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Filled', 'Заполнено', 2, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Filter', 'Фильтр', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Filter list', 'Доступные фильтры', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Filter list', 'Available filters', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Filter target audience', 'Filter target audience', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Filter target audience', 'Только не получавшие рассылку за период', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Filtered column', 'Filtered column', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Filtered column', 'Колонка с фильтром', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Filtering by username', 'Filtering by username', 1, 'geomarketing');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Filtering by username', 'Фильтрация по имени пользователя', 2, 'geomarketing');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('filterOoo', 'Filter', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('filterOoo', 'Фильтр...', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('filters', 'Фильтры', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Filters', 'Фильтр учреждений', 2, 'geomarketing');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Filters', 'Companies parameters', 1, 'geomarketing');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('filters', 'Filters', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Filters', 'Фильтры', 2, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Filters', 'Filters', 1, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Final completed task coordinate', 'Final completed task coordinate', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Final completed task coordinate', 'Последняя точка закрытия визита', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('find', 'Найти', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('find', 'Найти', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('find', 'Find', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('find', 'Find', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Find companies in region', 'Find companies in region', 1, 'geomarketing');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Find companies in region', 'Поиск учреждения по региону', 2, 'geomarketing');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Find company', 'Поиск учреждения', 2, 'geomarketing');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Find company', 'Find company', 1, 'geomarketing');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Finished', 'Finished', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Finished', 'Завершено', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('First completed task coordinate', 'First completed task coordinate', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('First completed task coordinate', 'Первая точка закрытия визита', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('First Name', ' First Name', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('First Name', ' Имя', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('First name', 'First name', 1, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('First name', 'Имя', 2, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('First tracker coordinate', 'Первая координата трекера', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('First tracker coordinate', 'First tracker coordinate', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('firstname', ' Firstname', 1, 'change_requests');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('firstname', ' Имя', 2, 'change_requests');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('flag1', 'Флаг(1)', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('flag2', 'Флаг(2)', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('flag3', 'Флаг(3)', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('flag4', 'Флаг(4)', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('flag5', 'Флаг(5)', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('FLCM', 'FLCM', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('FLCM', 'FLCM', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Float', 'Дробное число', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Float', 'Float', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('float1', 'Дробное число(1)', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('float2', 'Дробное число(2)', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('float3', 'Дробное число(3)', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Fold', 'Fold', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Fold', 'Скрыть', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Folder to upload', ' Folder to upload', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Folder to upload', ' Папка для загрузки', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Follow', 'Follow', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Follow', 'Перейти', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Footer Language', 'Footer Language', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Footer Language', 'Язык текста отписки', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('For all types', 'Для всех типов', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('For all types', 'For all types', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('For creating poligon close the line', 'For creating poligon close the line', 1, 'geomarketing');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('For creating poligon close the line', 'Для создания полигона замкните линию', 2, 'geomarketing');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('For Medical Representative distribution', 'Для рассылки мед.представителями', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('For Medical Representative distribution', 'For Medical Representative distribution', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('For period', 'За период', 2, 'geomarketing');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('For period', 'For period', 1, 'geomarketing');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Forgot password?', ' Забыли пароль?', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Forgot password?', 'Forgot password?', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Format', ' Формат', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Format', ' Format', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('From', 'From', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('From', 'От', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('from', 'from', 1, 'geomarketing');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('from', 'с', 2, 'geomarketing');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('From', 'От', 2, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('From', 'From', 1, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('From file', 'From file', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('From file', 'Из файла', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Full name', 'Full name', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Full name', ' ФИО', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('fullName', 'ФИО', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('fullName', 'Full Name', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Gender', 'Пол', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Gender', 'Gender', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('General', 'Общее', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('General fields', 'Основные поля', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('General information', 'Основная информация', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('General responsibility', 'Основная ответственность', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Generate', 'Generate', 1, 'geomarketing');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Generate', 'Сгенерировать', 2, 'geomarketing');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Geo division', 'Гео деление', 2, 'geomarketing');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Geo division', 'Geo division', 1, 'geomarketing');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Geo filters', 'Фильтр по географии', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Geo filters', 'Filter by geodata', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('geomarketing', 'ETMS', 1, 'geomarketing');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('geomarketing', 'ETMS', 2, 'geomarketing');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('GMT', 'GMT', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('GMT', ' Часовой пояс', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Go back to dashboard', 'Go back to dashboard', 1, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Go back to dashboard', 'Вернуться', 2, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Go to', 'Перейти', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Go to', 'Go to', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Go to the Synonyms page', 'Go to the Synonyms page', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Go to the synonyms page', 'Перейдите на страницу синонимов', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Got to', 'Перейти к', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Got to', 'Got to', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('gps_la', ' Широта', 2, 'change_requests');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('gps_la', ' Latitude', 1, 'change_requests');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('gps_lo', ' Longitude', 1, 'change_requests');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('gps_lo', ' Долгота', 2, 'change_requests');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('GR to prev. month', 'GR to prev. month', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('GR to prev. month', 'Прирост к пред. месяцу', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Granplan', 'Гран-план', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Granplan', 'Granplan', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Granual plan', 'Granual plan', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Granual plan', 'Грануальный план', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Granual plan not available', 'Грануальный план не доступен', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Granual plan not available', 'Granual plan not available', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Granually planing', 'Granually planing', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Granually planing', 'Грануальное планироване', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('greaterThan', 'Greater than', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('greaterThan', 'Больше', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Group by', 'Группировка', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Handling unprocessed company synonyms...', 'Обработка компаний без синонимов...', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Handling unprocessed company synonyms...', 'Handling unprocessed company synonyms...', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('haveBrands', 'Have brands', 1, 'geomarketing');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('haveBrands', 'Есть продажи бренда', 2, 'geomarketing');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('hello', 'Здравствуйте', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('hello', 'Hello', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Help', 'Help', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Help', 'Помощь', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Help', 'Помощь', 2, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Help', 'Help', 1, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Here are collected all the additional services developed for you.', ' Тут собраны все дополнительные сервисы разработанные для вас.', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Hide', ' Скрыть', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Hide all', 'Свернуть все', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Hide all', 'Hide all', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Hide inactive', 'Скрыть неактивных', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Hide inactive', 'Hide inactive', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Hide related', 'Скрыть связанные', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Hide related dictionary', 'Скрыть связанный справочник', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Hide related dictionary', 'Hide related dictionary', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('hierarchy', 'Hierarhy', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('hierarchy', 'Иерархия', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Hospital ', 'Стационар', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Hospital', 'Hospital', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Hour outside of accepted range', 'Hour outside of accepted range', 1, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Hour outside of accepted range', 'Время вне доступного диапазона', 2, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('House', 'House', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('House', 'Дом', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Identifier', 'Identifier', 1, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Identifier', 'Идентификатор', 2, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('If you have additional questions about CRM, or you need technical advice - our support consultants are ready to help you.', 'If you have additional questions about CRM, or you need technical advice - our support consultants are ready to help you.', 1, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('If you have additional questions about CRM, or you need technical advice - our support consultants are ready to help you.', 'Если у Вас возникли вопросы при работе с программным комплексом PharmaMobile, если Вы нуждаетесь в технической консультации – инженеры службы технической поддержки готовы оперативно и квалифицированно ответить на эти и другие возникшие у Вас вопросы.', 2, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('If you have forgotten something, tips are always at your fingertips, in the upper right corner of the screen!', ' Если вы что-нибудь забыли, подсказки всегда у вас под рукой, в верхнем правом углу экрана!', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Ignore', 'Ignore', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Ignore', 'Игнорировать', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('image', 'Photo', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('image', 'Фото', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('IMCP', 'IMCP', 1, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('IMCP', 'НМЦК', 2, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Import', 'Импортировать', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Import', 'Перейти к импорту', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Import', 'Import', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Import', ' Import', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Import', ' Перейти к импорту', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Import dictionary', 'Import dictionary', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Import file', 'Импорт файла', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Import file', 'Import file', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Import from file', 'Импортировать из файла', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Import from file', 'Import from file', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Importance', 'Важность', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Importance', 'Importance', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Importing', 'Importing', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Importing', 'Импортирование', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Importing file...', 'Importing file...', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Importing file...', 'Импорт файлов...', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Importing files', 'Импортирование файлов', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Importing files', 'Importing files', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Importing other synonyms...', 'Importing other synonyms...', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Importing other synonyms...', 'Импорт остальных синонимов...', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Importing synonyms...', 'Импортирование синонимов...', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Importing synonyms...', 'Importing synonyms...', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('In process', 'In progress', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('In progress', 'Загрузка', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('In progress', 'In progress', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('income', 'Income', 1, 'status');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('income', 'Входящие', 2, 'status');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('income', 'Входящие', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('income', 'Income', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Incomplete month', 'Incomplete month', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Incomplete month', 'Неполный месяц', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Incorrect file type', ' Некорректный формат файла', 2, 'validators');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Index', 'Index', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Index', '№ п/п', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Info', 'Info', 1, 'geomarketing');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Info', 'Инфо', 2, 'geomarketing');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Inhabited locality', 'Населенный пункт', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Inhabited locality', 'Inhabited locality', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Inherit', 'Inherit', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Inherit', 'Наследовать', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Inherit user', 'Наследовать пользователя', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Inherit user', 'Inherit user', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Inheritance', 'Inheritance', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Inheritance', 'Наследование', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('INN', 'МНН', 2, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('INN', 'INN', 1, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('INN not specified', 'INN not specified', 1, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('INN not specified', 'МНН не указано', 2, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('INN Survey', 'МНН Анкета', 2, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('INN Survey', 'INN Survey', 1, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('INN Surveys', 'INN Surveys', 1, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('INN Surveys', 'МНН Анкеты', 2, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('inProgress', 'Взяты в обработку', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('inProgress', 'In progress', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('in-progress', 'In progress', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('in-progress', 'Взяты в обработку', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('in-progress', 'Взяты в обработку', 2, 'status');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('in-progress', 'In progress', 1, 'status');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Input error', ' Некорректный ввод', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Input name', ' Введите название презентации', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Input name', ' Input name', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Insert raw html', 'Paste raw HTML', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Insert raw html', 'Вставить HTML код', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Inserting data', 'Добавление данных', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Inserting data', 'Inserting data', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Installing CRM on the tablet', ' Установка CRM на планшет', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('int', 'число', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('int', 'number', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('int1', 'Целое число(1)', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('int2', 'Целое число(2)', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('int3', 'Целое число(2)', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Invalid column name', 'Invalid column name', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Invalid column name', 'Невалидное название колонки', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Invalid credentials.', 'Invalid credentials.', 1, 'security');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Invalid credentials.', 'Неверный логин или пароль.', 2, 'security');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Invalid CSRF token.', 'Invalid CSRF token.', 1, 'security');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Invalid CSRF token.', 'Недействительный токен CSRF.', 2, 'security');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Invalid data exists, cells will be ignored', 'Найдены неправильные значения, ячейки будут проигнорированы', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Invalid date', ' Invalid date', 1, 'validators');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Invalid date', ' Неправильная дата', 2, 'validators');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Invalid email', ' Некорректный адрес электронной почты', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Invalid page name', 'Невалидное название листа', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Invalid page name', 'Invalid page name', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Invalid type, cell will be ignored', 'Неправильный тип данных, ячейка будет проигнорирована', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Invalid username and password combination', 'Неверная комбинация имени пользователя и пароля', 2, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Invalid username and password combination', 'Invalid username and password combination', 1, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Invalid value', ' Некорректное значение', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('INVALID_RECIPIENT', ' Invalid Email', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('INVALID_RECIPIENT', ' Некорректный Email', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('iOS', 'iOS', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('iOS', 'iOS', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Is active', 'Активен', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Is active', 'Is active', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Is main', 'Главное учреждение', 2, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Is main', 'Is main', 1, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Is primary', 'Главное учреждение', 2, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Is primary', 'Is primary', 1, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Is required', 'Is required', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Is required', 'Обязательная', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('IS_PROCESSING', ' В процессе', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('IS_PROCESSING', ' Processing', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('ismain', ' Главноное учреждение', 2, 'change_requests');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('ismain', ' Is main', 1, 'change_requests');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('January', 'Январь', 2, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('January', 'January', 1, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('January', ' Января', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('js.dateClosed', 'Closing date of the visit', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('js.dateClosed', 'Дата закрытия визита', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('js.error.error1', 'Для отображения маршрута, необходимо включить опцию определения местоположения в настройках устройства и браузера', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('js.error.error1', 'To display the route, you must enable the option of defining location in settings of device and browser', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('js.error.error2', 'To display the route, you must enable the option of defining location in settings of device and browser', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('js.error.error2', 'Для отображения маршрута, необходимо включить опцию определения местоположения в настройках устройства и браузера', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('js.error.timeout', 'Время ожидания истекло.', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('js.error.timeout', 'Timeout expired.', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('js.error.unknown', 'Unknown error', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('js.error.unknown', 'Неизвестная ошибка', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('July', ' Июля', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('July', 'July', 1, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('July', 'Июль', 2, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('June', 'Июнь', 2, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('June', 'June', 1, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('June', ' Июня', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Kazakhstan', 'Казахстан', 2, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Kazakhstan', 'Kazakhstan', 1, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Keep me logged in', 'Keep me logged in', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Keep me logged in', 'Оставаться в системе', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Language', ' Язык', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('language', 'Language', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Last available quarter', 'Последний доступный квартал', 2, 'geomarketing');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Last available quarter', 'Last available quarter', 1, 'geomarketing');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Last five not mapped IDs', 'Последние 5 не найденных айди', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Last five not mapped IDs', 'Last five not mapped IDs', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Last name', ' Last name', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Last name', ' Фамилия', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Last name', 'Фамилия', 2, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Last name', 'Last name', 1, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Last quarter', 'Last quarter', 1, 'geomarketing');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Last quarter', 'Последний квартал', 2, 'geomarketing');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Last task date', ' Дата последнего визита', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Last task date', ' Last task date', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Last task date', 'Дата последнего визита', 2, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Last task date', 'Last task date', 1, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Last tracker coordinate', 'Last tracker coordinate', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Last tracker coordinate', 'Последняя координата трекера', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('lastname', ' Lastname', 1, 'change_requests');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('lastname', ' Фамилия', 2, 'change_requests');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Latin', 'Latin', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Latin', 'Латиница', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Layers', 'Слои', 2, 'geomarketing');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Layers', 'Layers', 1, 'geomarketing');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('legend', 'Легенда карты', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('legend', 'Map legend', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('lessThan', 'Less than', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('lessThan', 'Меньше', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Line', 'Линия', 2, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Line', 'Line', 1, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Link', 'Ссылка', 2, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Link', 'Link', 1, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Link to schedule of MPI procurement', 'Ссылка на План-график закупок ЛПУ', 2, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Link to schedule of MPI procurementLink to schedule of MPI procurement', 'Link to schedule of MPI procurementLink to schedule of MPI procurement', 1, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('List', 'Список', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('List', 'List', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Load more...', 'Загрузить больше...', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Load more...', 'Load more...', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Load next', 'Загрузить следующие', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Load next', 'Show more', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('loading', 'Загрузка...', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('loading', 'Loading...', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Loading', 'Loading', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Loading', 'Загрузка', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Loading', 'Загрузка', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Loading', 'Loading', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Loading', 'Loading...', 1, 'geomarketing');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Loading', 'Загрузка...', 2, 'geomarketing');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Loading companies', 'Загрузка учреждений...', 2, 'geomarketing');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Loading companies', 'Loading companies...', 1, 'geomarketing');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Loading data', 'Данные загружаются', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Loading data', 'Loading data', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Loading data...', 'Загрузка данных из файла...', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Loading data...', 'Loading data...', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Loading distributions', 'Рассылки загружаются', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Loading distributions', 'Loading distributions', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Loading sales', 'Loading sales...', 1, 'geomarketing');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Loading sales', 'Загрузка продаж...', 2, 'geomarketing');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Loading...', 'Загрузка...', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Loading...', 'Loading...', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Loading...', 'Загрузка...', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Loading...', 'Loading...', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Log in', 'Log in', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Log In', ' Войти', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Log out', ' Выйти', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Login', ' Вход', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Login', 'Логин', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Login', 'Login', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Login', 'Login', 1, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Login', 'Логин', 2, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Login can''t be blank.', 'Login can''t be blank.', 1, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Login can''t be blank.', 'Логин не может быть пустым.', 2, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Login success', 'Успешно вошли', 2, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Login success', 'Login success', 1, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Logo', ' Логотип', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Logo file recommendations', ' Рекомендации к файлу логотипа', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Logout', 'Logout', 1, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Logout', 'Выйти', 2, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Logout?', 'Выйти?', 2, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Logout?', 'Logout?', 1, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Looking for synonyms via linked servers...', 'Looking for synonyms via linked servers...', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Looking for synonyms via linked servers...', 'Поиск синонимов компаний по связанным серверам...', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Loyalty', 'Лояльность', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('LS', 'ЛС', 2, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Main', 'Main', 1, 'geomarketing');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Main', 'Основное', 2, 'geomarketing');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('main', ' Аптечная сеть', 2, 'change_requests');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('main', ' Main company', 1, 'change_requests');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Main company', 'Сеть', 2, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Main company', 'Main company', 1, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Main responsible', 'Main responsible', 1, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Main responsible', 'Главный ответственный', 2, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('mainCompany', 'Main company...', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('mainCompany', 'Аптечная сеть...', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Make', 'Make', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Make', 'Производитель', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Male', 'Мужской', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Male', 'Male', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Mapping', 'Соответствие полей', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Mapping', 'Mapping', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('March', 'Март', 2, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('March', 'March', 1, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('March', ' Марта', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Markers circle', 'Markers circle', 1, 'geomarketing');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Markers circle', 'Количество специалистов', 2, 'geomarketing');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Markers circle turnover', 'Товарооборот и посещаемость', 2, 'geomarketing');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Markers circle turnover', 'Markers circle turnover', 1, 'geomarketing');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Master lists', 'Мастер листы', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Master lists', 'Master lists', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Max allowed filesize is 300 MB', ' Max allowed filesize is 300 MB', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Max allowed filesize is 300 MB', ' Максимальный размер файла 300 МБ', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Maximal password length is 255 symbols', ' Минимальная длина пароля - 6 символов', 2, 'validators');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Maximal password length is 6 symbols', ' Minimal password length is 255 symbols', 1, 'validators');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('May', ' Мая', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('May', 'May', 1, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('May', 'Май', 2, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('mcm', 'Multi Chanel Marketing', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('mcm', 'Мультиканальный Маркетинг', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Medical Representative', 'Медицинский представитель', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Medical Representative', 'Medical Representative', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Member', 'Участник', 2, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Member', 'Member', 1, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Member already added', 'Member already added', 1, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Member already added', 'Участник уже добавлен', 2, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Members', 'Участники', 2, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Members', 'Members', 1, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Message', 'Message', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Message', 'Сообщение', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('message', 'Message', 1, 'geomarketing');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('message', 'Оповещение', 2, 'geomarketing');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Message', 'Сообщение', 2, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Message', 'Message', 1, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Message for SMS', 'Сообщение для СМС', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Message for SMS', 'Message for SMS', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Message has been successfully sent', 'Message has been successfully sent', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Message has been successfully sent', 'Письмо/сообщение отправлено успешно', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Message was not sent', 'Письмо/сообщение не отправлено', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Message was not sent', 'Message was not sent', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Messages', 'Сообщения', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Messages', 'Messages', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Middle Name', ' Отчетство', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Middle Name', ' Middle Name', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Middle name', 'Middle name', 1, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Middle name', 'Отчество', 2, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('middlename', ' Middlename', 1, 'change_requests');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('middlename', ' Отчество', 2, 'change_requests');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Midway completed task coordinate', 'Промежуточные точки закрытия визита', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Midway completed task coordinate', 'Midway completed task coordinate', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Midway tracker coordinate', 'Midway tracker coordinate', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Midway tracker coordinate', 'Промежуточные координаты трекера', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Minimal password length is 6 symbols', ' Минимальная длина пароля - 6 символов', 2, 'validators');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Minimal password length is 6 symbols', ' Minimal password length is 6 symbols', 1, 'validators');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Missing parameters. ''username'' and ''password'' required', 'Missing parameters. \"username\" and \"password\" required', 1, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Missing parameters. ''username'' and ''password'' required', 'Отсутствующие параметры. Требуется «имя пользователя» и «пароль»', 2, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('MM', ' ММ', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Mobile', 'Мобильный телефон', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Mobile', 'Mobile', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Model', 'Модель', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Model', 'Model', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Month', 'Месяц', 2, 'geomarketing');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Month', 'Month', 1, 'geomarketing');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Month', 'Month', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Month', 'Месяц', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Month', 'месяц', 2, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Month', 'Month', 1, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('More', 'More', 1, 'geomarketing');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('More', 'Ещё', 2, 'geomarketing');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('More about polygon', 'Подробнее о полигоне', 2, 'geomarketing');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('More about polygon', 'More about polygon', 1, 'geomarketing');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('More then 20 pharmacies found. Make search more specific', 'More then 20 pharmacies found. Make search more specific', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('More then 20 pharmacies found. Make search more specific', 'Найдено больше 20 аптек. Уточните поиск', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('mssku', 'MS and SKU', 1, 'geomarketing');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('mssku', 'MS и SKU', 2, 'geomarketing');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('msskuByBrands', 'MS и Brand', 2, 'geomarketing');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('msskuByBrands', 'MS and Brand', 1, 'geomarketing');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Multiselect dictionary', 'Мультивыбор справочника', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('My links', 'Мои привязки', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('My links', 'Show from synonyms from API', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('My profile', ' Мой профиль', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('N/A', 'Н/Д', 2, 'geomarketing');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('N/A', 'N/A', 1, 'geomarketing');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Name', 'Название', 2, 'geomarketing');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Name', 'Name', 1, 'geomarketing');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Name', 'Name', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Name', 'Название', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Name', 'Название', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('name', ' Название', 2, 'change_requests');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('name', ' Name', 1, 'change_requests');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Name', 'Название', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Name', 'Name', 1, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Name', 'Название', 2, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Name', 'Name', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Name', 'Name', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Name of MPI', 'Наименование ЛПУ', 2, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Name of MPI', 'Name of MPI', 1, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Need help?', 'Нужна помощь?', 2, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Need help?', 'Need help?', 1, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('need to approve', 'требует подтверждения', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('need to approve', 'need to approve', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Net', 'Аптечная сеть', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Net', 'Pharmacy network', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('New account', 'New account', 1, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('New account', 'Новое учреждение', 2, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('New account request', 'Новый запрос учреждения', 2, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('New account request', 'New account request', 1, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('New action', 'Новая задача', 2, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('New Action', 'New Action', 1, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('New campaign', 'New campaign', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('New campaign', 'Новая кампания', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('New client', 'Новый клиент', 2, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('New client', 'New client', 1, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('New client request', 'New client request', 1, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('New client request', 'Запрос на добавление клиента', 2, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('New company', 'New company', 1, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('New company', 'Новое учреждение', 2, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('New company request', 'Запрос на добавление учреждения', 2, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('New company request', 'New company request', 1, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('New contact', 'Новый контакт', 2, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('New contact', 'New contact', 1, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('New contact request', 'New contact request', 1, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('New contact request', 'Новый запрос контакта', 2, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('New institution', 'Новое учреждение', 2, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('New nask', 'Новый визит', 2, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('New photo', ' Новое фото (камера)', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('New plan', 'New plan', 1, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('New plan', 'Новый план', 2, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('New plan detail', 'Новые детали плана', 2, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('New plan detail', 'New plan detail', 1, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('New plan preparation', 'New plan preparation', 1, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('New plan preparation', 'Новый бренд', 2, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('New plan presentation', 'New plan presentation', 1, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('New plan presentation', 'Новая презентация', 2, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('New plan promo material', 'Новый промо материал', 2, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('New plan promo material', 'New plan promo material', 1, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('New preparation', 'Новый препарат', 2, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('New preparation', 'New preparation', 1, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('New task', 'Новый визит', 2, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('New Task', 'New Task', 1, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('New task added', 'Новый визит добавлен', 2, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('New task created', 'New task created', 1, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('New value', 'Новое значение', 2, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Next', ' Далее', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Next', 'Далее', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Next', ' Next', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Next', ' Далее', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('No', 'Нет', 2, 'geomarketing');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('No', 'No', 1, 'geomarketing');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('No', 'Нет', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('No', 'No', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('No authentication provider found to support the authentication token.', 'Не найден провайдер аутентификации, поддерживающий токен аутентификации.', 2, 'security');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('No authentication provider found to support the authentication token.', 'No authentication provider found to support the authentication token.', 1, 'security');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('No brands', 'No brands', 1, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('No brands', 'Нет промо материалов', 2, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('No companies', 'No companies', 1, 'geomarketing');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('No companies', 'Нет учреждений', 2, 'geomarketing');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('No companies assigned to selected user', 'No pharmacies assigned to selected user', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('No companies assigned to selected user', 'Нет аптек привязанных к выбранному пользователю', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('No companies found', 'No companies found', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('No companies found ', 'Компании не найдены', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('No contacts', 'Сотрудников нет', 2, 'geomarketing');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('No contacts', 'No contacts', 1, 'geomarketing');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('No coordinates for the selected period', 'За выбранный период нет координат', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('No coordinates for the selected period', 'No coordinates for the selected period', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('No data', 'Нет данных', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('No data', 'No data', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('No data for selected region and period', 'No data for selected region and period', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('No data for selected region and period ', 'Нет данных за выбранные период и регион', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('No data found', 'Данные не найдены', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('No employees', 'Нет сотрудников', 2, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('No fields found', 'Поля не найдены', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('No fields found', 'No fields found', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('No files selected', 'Не выбрано ни одного файла', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('No files selected', 'No files selected', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('No filters to show', 'No filters to show', 1, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('No filters to show', 'Нет фильтров', 2, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('No items', 'No items', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('No items', 'Нет данных', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('No items available', 'No items available', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('No items available', 'Нет доступных элементов', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('No items selected', 'Ничего не выбрано', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('No items selected', 'No items selected', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('No items selected', 'Ничего не выбрано', 2, 'geomarketing');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('No items selected', 'No items selected', 1, 'geomarketing');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('No participants found', 'Участники не найдены', 2, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('No participants found', 'No participants found', 1, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('No plan for selected user', 'План по выбранному пользователю отсутсвует', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('No plan for selected user', 'No plan for selected user', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('No preparations', 'Нет препаратов', 2, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('No preparations', 'No preparations', 1, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('No promos', 'No promos', 1, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('No promos', 'Нет брендов', 2, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('No reports available', 'Нету доступных отчетов', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('No reports available', 'No reports available', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('No responsible', 'No responsible', 1, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('No responsible', 'Ответственных нет', 2, 'geomarketing');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('No responsible', 'No responsible', 1, 'geomarketing');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('No responsible', 'Ответственные не найдены', 2, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('No results', 'No results', 1, 'geomarketing');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('No results', 'No results', 1, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('No results', 'Нет результатов', 2, 'geomarketing');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('No results', 'Нет результатов', 2, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('No results found', 'Нет результатов', 2, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('No results found', 'No results found', 1, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('No results found', 'No results found', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('No results found', 'Нет результатов', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('No Rows To Show', 'No Rows To Show', 1, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('No Rows To Show', 'Нет записей', 2, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('No rows to show', 'Нет записей', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('No rows to show', 'No rows to show', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('No sales', 'Продажи отсутствуют', 2, 'geomarketing');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('No sales', 'No sales', 1, 'geomarketing');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('No session available, it either timed out or cookies are not enabled.', 'Сессия не найдена, ее время истекло, либо cookies не включены.', 2, 'security');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('No session available, it either timed out or cookies are not enabled.', 'No session available, it either timed out or cookies are not enabled.', 1, 'security');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('No subjects found', 'No subjects found', 1, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('No subordinates found', 'Нет подчиненных', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('No subordinates found', 'No subordinates found', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('No task subjects found', 'Темы не найдены', 2, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('No tasks', 'Нет визитов', 2, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('No tasks', 'No tasks', 1, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('No token could be found.', 'No token could be found.', 1, 'security');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('No token could be found.', 'Токен не найден.', 2, 'security');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('No turnover', 'Товарооборот отсутствует', 2, 'geomarketing');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('No turnover', 'No turnover', 1, 'geomarketing');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('No turnover per year', 'No turnover per [[year]]', 1, 'geomarketing');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('No turnover per year', 'За [[year]] год товарооборот отсутствуют', 2, 'geomarketing');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('No visits', 'Визитов нет', 2, 'geomarketing');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('No visits', 'Not visited', 1, 'geomarketing');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('No watchers found', 'Наблюдатели не найдены', 2, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('No watchers found', 'No watchers found', 1, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('No, cancel', ' Нет, я случайно', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('noBrands', 'No brands', 1, 'geomarketing');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('noBrands', 'Без продаж', 2, 'geomarketing');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('NoResults', 'No result found for selected filters', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('NoResults', 'По выбранному фильтру ничего не найдено', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('noRowsToShow', 'No rows to show', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('noRowsToShow', 'Нет данных', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('not approved', 'not approved', 1, 'geomarketing');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('not approved', 'не утвержден', 2, 'geomarketing');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Not contains', 'Не содержит', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Not enough points to create layer', 'Недостаточно точек для создания слоя. Выберите другие параметры', 2, 'geomarketing');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Not enough points to create layer', 'Not enough points to create layer', 1, 'geomarketing');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Not equals', 'Не равно', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Not found', 'Not found', 1, 'geomarketing');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Not found', 'Не найдено', 2, 'geomarketing');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Not mapped', 'Не используется', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Not mapped', 'Not mapped', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Not needed', 'Не требуется', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Not needed', 'Not needed', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Not privileged to request the resource.', 'Отсутствуют права на запрос этого ресурса.', 2, 'security');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Not privileged to request the resource.', 'Not privileged to request the resource.', 1, 'security');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Not required', 'Необязательное', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Not required', 'Not required', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Not selected', 'Not selected', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Not selected', 'Не выбрано', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Not selected', 'Not selected', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Not selected', 'Не выбрано', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Not Specified', 'Не указано', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Not Specified', 'Not Specified', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Not specified', 'Не указано', 2, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Not specified', 'Not specified', 1, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Not subject to verification', 'Not subject to verification', 1, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Not to approve', 'Not to approve', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Not to approve', 'Не утверждать', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Not verified', 'Not verified', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Not verified', 'Не подтверждено', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Not Working', 'Не работает', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Not Working', 'Not Working', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('notContains', 'Не содержит', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('notContains', 'Not contains', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('notEqual', 'Not equals', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('notEqual', 'Не равно', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Nothing found', 'Nothing found', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Nothing found', 'Ничего не найдено,', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Nothing found', 'Nothing found', 1, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Nothing found', 'Ничего не найдено', 2, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('notification', 'Оповещение', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('notification', 'Notification', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Notifications', ' Оповещения', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('notVisited', 'Без визитов', 2, 'geomarketing');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('notVisited', 'Not visited', 1, 'geomarketing');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('November', ' Ноября', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('November', 'November', 1, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('November', 'Ноябрь', 2, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Number', 'Целое число', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Number', 'Number', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Number of doctors', 'Кол-во врачей', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Number of doctors', 'Number of doctors', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Number of packages', 'Number of packages', 1, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Number of packages', 'Количество упаковок', 2, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Number of pharmacies', 'Number of pharmacies', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Number of pharmacies', 'Кол-во аптек', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Number of quarters', 'Number of quarters', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Number of quarters', 'Количество кварталов', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Number of sold units', 'Number of sold units', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Number of sold units', 'Кол-во проданных упаковок', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Number of units', 'Number of units', 1, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Number of units', 'Количество единиц', 2, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Number of visits', 'Кол-во визитов', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Number of visits', 'Number of visits', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Number task', 'Number task', 1, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('October', 'Октябрь', 2, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('October', 'October', 1, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('October', ' Октября', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Off', 'Выкл', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Off', 'Off', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('OK', 'OK', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('OK', 'OK', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('OKPO', 'ОКПО', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('OKPO', 'OKPO', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('On', 'On', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('On', 'Вкл', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('One of the following fields must contain value ("Preparation", "Preparation code")', 'One of the following fields must contain value ("Preparation", "Preparation code")', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('One of the following fields must contain value ("Preparation", "Preparation code")', 'Одно из полей долно содержать значение ("Препарат", "Код препарата")', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Only .csv files supported', 'Поддерживаются .csv файлы', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Only .csv files supported', 'Only .csv files supported', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Only CSV file allowed', 'Формат файла не поддерживается, загрузите CSV файл', 2, 'validators');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Only enabled', 'Только включенные', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Only Excel format supported', 'Поддерживается только формат Excel', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Only numbers allowed', ' Разрешены только числовые значения', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Only XLS/XLSX file allowed', 'Только файлы xls, xlsx доступны для загрузки', 2, 'validators');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Only XLS/XLSX file allowed', 'Only XLS/XLSX file allowed', 1, 'validators');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Open form', 'Открыть форму', 2, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Open form', 'Open form', 1, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Opening email', 'Opening email', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Opening email', 'Открыл письмо рассылки', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Opening link in email', 'Переход по ссылке в письме', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Opening link in email', 'Opening link in email', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Option disabled', 'Настройка отключена', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Option enabled', 'Настройка включена', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Options configuration', 'Общие настройки', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Options configuration', 'Options configuration', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('OR', 'ИЛИ', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Or', 'or', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Organization', 'Организация', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Organization', 'Organization', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Organization type', 'Organization type', 1, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Organization type', 'Тип организации', 2, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Other', 'Other', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Other', 'Другое', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Out of the map', 'Нет на карте', 2, 'geomarketing');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Out of the map', 'Out of the map', 1, 'geomarketing');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Outside of polygon', 'Outside of polygon', 1, 'geomarketing');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Outside of polygon', 'Вне полигона', 2, 'geomarketing');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('owner', ' Resposible', 1, 'change_requests');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('owner', ' Ответственный', 2, 'change_requests');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Owner', 'Ответственный', 2, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Owner', 'Owner', 1, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Owners', 'Owners', 1, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Owners', 'Исполнители', 2, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Pack price', 'Цена за упаковку', 2, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Pack price', 'Pack price', 1, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Packages', 'Упаковки', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Packages', 'Packages', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Page not found', 'Страница не найдена', 2, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Page not found', 'Page not found', 1, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Paint', 'Paint', 1, 'geomarketing');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Paint', 'Рисовать', 2, 'geomarketing');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Painting options', 'Параметры рисования', 2, 'geomarketing');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Painting options', 'Painting options', 1, 'geomarketing');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Parent distribution', 'Родительская рассылка', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Parent distribution', 'Parent distribution', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Participant already added', 'Participant already added', 1, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Participant already added', 'Участник уже добавлен', 2, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Participants', 'Участники', 2, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Participants', 'Participants', 1, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Participants', 'Участники', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Participants', 'Participants', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Password', 'Password', 1, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Password', 'Пароль', 2, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Password', 'Password', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Password', 'Пароль', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Password can''t be blank.', 'Пароль не может быть пустым.', 2, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Password can''t be blank.', 'Password can''t be blank.', 1, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Password recovery', ' Восстановление пароля', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Pattern', 'Шаблон', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Pattern', 'Pattern', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Pattern has associated records!', 'Pattern has associated records!', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Pattern has associated records!', 'У шаблона есть связанные записи!', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Pattern Name', 'Pattern Name', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Pattern Name', 'Название шаблона', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Pattern not found', 'Pattern not found', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Pattern not found', 'Шаблон не найден', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Pattern successfully saved', 'Pattern successfully saved', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Pattern successfully saved', 'Шаблон успешно сохранен', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Pattern type', 'Тип шаблона', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Pattern type', 'Pattern type', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Patterns', 'Patterns', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Patterns', 'Шаблоны', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('pcs', 'pcs', 1, 'geomarketing');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('pcs', 'уп', 2, 'geomarketing');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('PENDING', ' Pending', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('PENDING', ' Ожидает', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Pending approval', 'Pending approval', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Pending approval', 'Требует утверждения', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Pending for approval', 'Pending for approval', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Pending for approval', 'Ожидает утверждения', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Penetration', 'Penetration', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Penetration', 'Пенетрация', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Per polygons', 'Per polygons', 1, 'geomarketing');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Per polygons', 'По полигонам', 2, 'geomarketing');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Per users', 'По пользователям', 2, 'geomarketing');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Per users', 'Per users', 1, 'geomarketing');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Perc', '% выполнения', 2, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Percent', 'Процент', 2, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Percent', 'Percent', 1, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Percentage', 'Percentage', 1, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Period', 'Период', 2, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Period', 'Period', 1, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Period', 'Period', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Period', 'Период', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Period', 'Период', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Period', 'Period', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Period type', 'Тип периода', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Period type', 'Period type', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Personalization Key', 'Personalization Key', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Personalization Key', 'Ключ персонализации', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Personalization Key must contain only 0-9, A-Z and .', 'Поле Ключ персонализации может содержать только 0-9, A-Z и точку', 2, 'validators');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Personalization Key must contain only 0-9, A-Z and .', 'Personalization Key must contain only 0-9, A-Z and .', 1, 'validators');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Pharamacy coverage', 'Pharamacy coverage', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Pharamacy coverage', 'Покрытие аптек', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Pharm agent', 'Фарм представитель', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Pharm agent', 'Pharm agent', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('pharmacies', 'Аптеки', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('pharmacies', 'Pharmacies', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Pharmacies not found', 'Pharmacies not found', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Pharmacies not found', 'Аптеки не найдены', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Pharmacy', 'Аптека', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Pharmacy', 'Pharmacy', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Pharmacy coverage', 'Pharmacy coverage', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Pharmacy coverage', 'Покрытие аптек', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Pharmacy networks', 'Pharmacy networks', 1, 'geomarketing');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Pharmacy networks', 'Аптечная сеть', 2, 'geomarketing');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Phone', ' Телефон', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Phone', ' Phone', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Phone', 'Телефон', 2, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Phone', 'Phone', 1, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Phone', ' Телефон', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('phone1', ' Телефон', 2, 'change_requests');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('phone1', ' Phone number', 1, 'change_requests');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('phone2', ' Additional phone number', 1, 'change_requests');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('phone2', ' Доп. телефон', 2, 'change_requests');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Photo report', 'Фото выкладки', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Photo report', 'Photo report', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Photo state', 'State', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Photo state', 'Статус', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Photo type', 'Тип фото', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Photo type', 'Type of photo', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('photoType', 'Type of photo', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('photoType', 'Тип фото', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Pivot mode', 'Pivot mode', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Pivot mode', 'Режим группировки', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('pkg', 'pkg', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('pkg', 'уп', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Place of work', 'Место работы', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Place of work', 'Place of work', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Plan', 'Plan', 1, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Plan', 'План', 2, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Plan', 'Plan', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Plan', 'План', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Plan all', 'Всего план', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Plan all', 'Plan all', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Plan count', 'Plan count', 1, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Plan count', 'Плановое количество', 2, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Plan cycle', 'Plan cycle', 1, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Plan cycle', 'План цикла', 2, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Plan cycle', 'План цикла', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Plan cycle', 'Plan cycle', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Plan date should be greater than current date', 'Plan date should be greater than current date', 1, 'validators');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Plan date should be greater than current date', 'Плановая дата рассылки должна быть позже текущей даты', 2, 'validators');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Plan detail preparation was deleted.', 'Plan detail preparation was deleted.', 1, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Plan detail preparation was deleted.', 'Препарат деталей плана был удален.', 2, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Plan detail preparation: {{message}}', 'Препарат деталей плана: {{message}}', 2, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Plan detail preparation: {{message}}', 'Plan detail preparation: {{message}}', 1, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Plan detail presentation was deleted.', 'Plan detail presentation was deleted.', 1, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Plan detail presentation was deleted.', 'Презентация удалена', 2, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Plan detail presentation: {{message}}', 'Презентация {{message}}', 2, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Plan detail promo material was deleted.', 'Материал удален', 2, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Plan detail promo material was deleted.', 'Plan detail promo material was deleted.', 1, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Plan detail promo material: {{message}}', 'Материал {{message}}', 2, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Plan detail was deleted.', 'Детали плана были удалены.', 2, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Plan detail was deleted.', 'Plan detail was deleted.', 1, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Plan detail: {{message}}', 'Plan detail: {{message}}', 1, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Plan detail: {{message}}', 'Детали плана: {{message}}', 2, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Plan fact', 'Plan fact', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Plan fact ', 'План факт', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Plan is not found', 'План не найден', 2, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Plan model', 'Plan model', 1, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Plan not found', 'Plan not found', 1, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Plan not found', 'План не найден', 2, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Plan office ', 'План офис', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Plan office', 'Plan office', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Plan ofice', 'Plan ofice', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Plan preparation was deleted.', 'Препарат был удален.', 2, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Plan preparation was deleted.', 'Plan preparation was deleted.', 1, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Plan preparation: {{message}}', 'Plan preparation: {{message}}', 1, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Plan preparation: {{message}}', 'Препарат: {{message}}', 2, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Plan recommended route', 'Plan recommended route', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Plan recommended route', 'План. маршрут рекомендуемый', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Plan route', 'План. маршрут', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Plan route', 'Plan route', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Plan total', 'Plan total', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Plan total', 'Всего план', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Plan was deleted.', 'План был удален.', 2, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Plan was deleted.', 'Plan was deleted.', 1, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Plan: {{message}}', 'Plan: {{message}}', 1, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Plan: {{message}}', 'План: {{message}}', 2, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Plan model', 'Модель плана', 2, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Planned', 'Planned', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Planned', 'Запланировано', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Planned task route', 'Planned task route', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Planned task route', 'Запланированный маршрут визитов', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('plannedRoute', 'Запл. маршрут', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('plannedRoute', 'Planned route', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('plannedRouteAvailable', 'Available in a range of day', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('plannedRouteAvailable', 'Доступно в диапазоне дня', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Planning', 'Planning', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Planning', 'Планирование', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Planning acinus', 'Планирование на ацинусы', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Planning acinus', 'Planning acinus', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Planning brands', 'Planning brands', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Planning brands', 'Планирование Бренд-ЛПУ', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Planning companies', 'Планирование ЛПУ-Бренд', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Planning companies', 'Planning companies', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Planning error', 'Planning error', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Planning error', 'Ошибка при планировании', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Planning for acinus', 'Планирование на ацинусы', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Planning for acinus', 'Planning for acinus', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Planning successful', 'Планирование успешно', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Planning successful', 'Planning successful', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('planningForAcinus', 'Планирование на ацинусы', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Platform', 'Platform', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Platform', 'Платформа', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Platform settings', 'Настройки платформы', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Platform settings', 'Platform settings', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Platform settings will be applied for only one user', 'Настройки платформы будут применены только для одного пользователя', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Please pick a value from the drop-down list', ' Пожалуйста, выберите значение из списка', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Please upload a valid file', ' Пожалуйста, загрузите корректный файл', 2, 'validators');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Please upload a valid file', ' Please upload a valid file', 1, 'validators');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Please upload a valid file', 'Please upload a valid file', 1, 'validators');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Please upload a valid file', 'Пожалуйста, загрузите корректный файл', 2, 'validators');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Please upload a valid file', 'Пожалуйста, загрузите корректный файл', 2, 'validators');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Please upload a valid file', 'Please upload a valid file', 1, 'validators');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Please, add at least {{ limit }} promo material.|Please, add at least {{ limit }} promo materials.', '  Пожалуйста, заполните, как минимум, {{ limit }} промо материал.|Пожалуйста, заполните, как минимум, {{ limit }} промо материалa.', 2, 'validators');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Please, choose at least one group to be copied', 'Пожалуйста, выберите, хотя бы, одну группу для копирования', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Please, choose at least one type to copy on', 'Пожалуйста, выберите, как минимум один тип для копирования', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Please, choose field that matches appropriate column', 'Пожалуйста, выберите поле, которое соответствует колонке', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Please, choose field that matches appropriate column', 'Please, choose field that matches appropriate column', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Please, choose the file', ' Пожалуйста, выберите файл', 2, 'validators');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Please, choose the file', ' Please, choose the file', 1, 'validators');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Please, choose the file', ' Пожалуйста, выберите файл', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Please, choose the file', ' Please, choose the file', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Please, choose user to copy settings', 'Пожалуйста, выберите пользователя, чтобы копировать настройки', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Please, choose, at least one type', 'Please, choose, at least one type', 1, 'validators');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Please, choose, at least one type', 'Пожалуйста, укажите, как минимум, один тип рассылки', 2, 'validators');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Please, fill at least one agreement', ' Пожалуйста, заполните, как минимум, одну договоренность', 2, 'validators');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Please, fill at least one agreement', ' Please, fill at least one agreement', 1, 'validators');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Please, fill at least one attribute', ' Please, fill at least one attribute', 1, 'validators');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Please, fill at least one attribute', ' Пожалуйста, выберите, как минимум, один атрибут', 2, 'validators');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Please, fill at least one brand', ' Пожалуйста, заполните, как минимум, один бренд', 2, 'validators');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Please, fill at least one brand', ' Please, fill at least one brand', 1, 'validators');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Please, fill at least one field', ' Пожалуйста, заполните хотя бы одно поле', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Please, fill at least one material', ' Please, fill at least one material', 1, 'validators');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Please, fill at least one material', ' Пожалуйста, заполните, как минимум, один промо материал', 2, 'validators');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Please, fill at least one preparation', ' Пожалуйста, заполните, как минимум, один препарат', 2, 'validators');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Please, fill at least one preparation', ' Please, fill at least one preparation', 1, 'validators');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Please, fill promotions', ' Please, all promotions', 1, 'validators');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Please, fill promotions', ' Пожалуйста, заполните промоции', 2, 'validators');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Please, indicate the required columns', 'Пожалуйста, укажите обязательные колонки', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Please, indicate the required columns', 'Please, indicate the required columns', 1, 'validators');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Please, indicate the required columns', 'Пожалуйста, укажите обязательные колонки', 2, 'validators');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Please, indicate the required columns', 'Please, indicate the required columns', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Please, indicate which data relates to the columns from the file', 'Пожалуйста, укажите каким данным соответствуют колонки из файла', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Please, indicate which data relates to the columns from the file', 'Please, indicate which data relates to the columns from the file', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Please, match at least one column', ' Пожалуйста, выберите, как минимум, одну колонку', 2, 'validators');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Please, provide the reason, you want to stop receive messages', 'Пожалуйста, укажите причину почему вы хотите перестать получать рассылку', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Please, provide the reason, you want to stop receive messages', 'Please, provide the reason, you want to stop receive messages', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Please, select at least one filter', 'Пожалуйста, выберите, как минимум, один из фильтров', 2, 'validators');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Please, select at least one filter', 'Please, select at least one filter', 1, 'validators');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Please, select at least one group', ' Пожалуйста, выберите, как минимум, одну группу', 2, 'validators');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Please, select at least one user', ' Пожалуйста, выберите, как минимум, одного пользователя', 2, 'validators');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Please, select brands and set promotion', 'Please, select brands and set promotion', 1, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Please, select client', 'Please, select client', 1, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Please, select client', 'Пожалуйста, выберите контактное лицо', 2, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Please, select companies', 'Please, select companies', 1, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Please, select companies', 'Выберите компании', 2, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Please, select company', 'Please, select company', 1, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Please, select company', 'Пожалуйста, выберите учреждение', 2, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Please, select contacts', 'Выберите контакты', 2, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Please, select contacts', 'Please, select contacts', 1, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Please, select details preparations', 'Please, select details preparations', 1, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Please, select details preparations', 'Выберите препараты', 2, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Please, select one of responsibility options', 'Пожалуйста, выберите один из вариантов наследования ответственности', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Please, select plan', 'Please, select plan', 1, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Please, select plan detail', 'Please, select plan detail', 1, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Please, select plan details', 'Выберите детали плана', 2, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Please, select plan details', 'Please, select plan details', 1, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Please, select presentation', 'Please, select presentation', 1, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Please, select promo material', 'Please, select promo material', 1, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Please, select report', 'Please, select report', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Please, select report', 'Please, select report', 1, 'validators');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Please, select report', 'Пожалуйста, выберите файл', 2, 'validators');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Please, select report', 'Пожалуйста, выберите файл', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Please, select, at least one of target audiences', 'Пожалуйста, выберите, как минимум одну целевую аудиторию', 2, 'validators');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Please, select, at least one target audience', 'Please, select, at least one target audience', 1, 'validators');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Please, select, at least, one filter first', 'Пожалуйста, выберите, как минимум один фильтр', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Please, select, at least, one filter first', 'Please, select, at least, one filter first', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Please, upload at least one file', 'Загрузите хотя бы один файл', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Please, upload at least one file', 'Please, upload at least one file', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Point division applied', 'Применено деление по точкам', 2, 'geomarketing');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Point division applied', 'Point division applied', 1, 'geomarketing');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Pointer', 'Pointer', 1, 'geomarketing');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Pointer', 'Указатель', 2, 'geomarketing');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Pokr', 'Pokr', 1, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Pokr', 'Покрытие', 2, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Policy', ' Политика', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Polyclinic ', 'Поликлиника', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Polyclinic', 'Polyclinic', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Polygon', 'Полигон', 2, 'geomarketing');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Polygon', 'Polygon', 1, 'geomarketing');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Polygon Name', 'Polygon Name', 1, 'geomarketing');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Polygon Name', 'Имя полигона', 2, 'geomarketing');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Polygon name was successfully saved', 'Имя полигона сохранено', 2, 'geomarketing');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Polygon name was successfully saved', 'Polygon name was successfully saved', 1, 'geomarketing');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('polygons', 'Polygons', 1, 'geomarketing');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('polygons', 'Полигоны', 2, 'geomarketing');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('polygons2', 'polygons', 1, 'geomarketing');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('polygons2', 'полигонов', 2, 'geomarketing');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('position', ' Должность', 2, 'change_requests');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('position', ' Position', 1, 'change_requests');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Position', 'Должность', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Position', 'Position', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Position', 'Должность', 2, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Position', 'Position', 1, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Post code', 'Post code', 1, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Post code', 'Почтовый индекс', 2, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('postcode', ' Post code', 1, 'change_requests');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('postcode', ' Почтовый индекс', 2, 'change_requests');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Post-training', 'Post-training', 1, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Post-training', 'Посттренинг', 2, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Post-training support', 'Посттренинговое сопровождение', 2, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Post-training support', 'Post-training support', 1, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Potential', 'Потенциал', 2, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Potential', 'Potential', 1, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Potential', 'Потенциал', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Potential', 'Потенциал', 2, 'geomarketing');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Potential', 'Potential', 1, 'geomarketing');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Pre month', 'Пред месяц', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Pre month', 'Prev month', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Preparation', 'Препарат', 2, 'geomarketing');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Preparation', 'Preparation', 1, 'geomarketing');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Preparation', 'Preparation', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Preparation', 'Препарат', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Preparation', 'Preparation', 1, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Preparation', 'Препарат', 2, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Preparation brand', 'Бренд', 2, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Preparation brand', 'Preparation brand', 1, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Preparation brand', 'Preparation brand', 1, 'geomarketing');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Preparation brand', 'Бренд препарата', 2, 'geomarketing');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Preparation line', 'Preparation line', 1, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Preparation line', 'Линия', 2, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Preparation name', 'Название препарата', 2, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Preparation name', 'Preparation name', 1, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Preparation name', 'Preparation name', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Preparation name', 'Название препарата', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Preparation number task', 'Номер визита', 2, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('preparationName', 'Preparation', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('preparationName', 'Препарат', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Preparations', 'Препараты', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Preparations', 'Preparations', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Preparations', 'Контроль', 2, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Preparations', 'Preparations', 1, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Preparations to control', 'Препараты для контроля', 2, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Preparations to control', 'Preparations to control', 1, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Presentation', 'Presentation', 1, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Presentation', 'Презентация', 2, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Presentation can not be deleted, used in clm view log', ' Презентация не удалена, т.к. CLM использовалась в clm log.', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Presentation can not be deleted, used in clm view log', ' Presentation can not be deleted, used in task clm view log', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Presentation can not be deleted, used in task presentation', ' Presentation can not be deleted, used in task presentation', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Presentation can not be deleted, used in task presentation', ' Презентация не удалена, т.к. CLM использовалась в визите.', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Presentation can not be deleted, used in task presentation slide', ' Презентация не удалена, т.к. CLM использовалась в визите.', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Presentation can not be deleted, used in task presentation slide', ' Presentation can not be deleted, used in task presentation slide', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Presentation Name', ' Presentation Name', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Presentation Name', ' Название презентации', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Presentation name', 'Название', 2, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Presentation number task', 'Номер визита', 2, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Press enter to save', 'Press enter to save', 1, 'geomarketing');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Press enter to save', 'Нажмите enter чтобы сохранить', 2, 'geomarketing');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Prev', 'Данные пред визита', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Prev', 'Prev', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Preview', 'Preview', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Preview', 'Предпросмотр', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Previous', ' Назад', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Price', 'Price', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Price', 'Цена', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Price for all packages', 'Price for all packages', 1, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Price for all packages', 'Цена за все упаковки', 2, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Price for all units', 'Цена за все единицы', 2, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Price for all units', 'Price for all units', 1, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Primary IMEI', 'Основной IMEI', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Primary IMEI', 'Primary IMEI', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Privileges', 'Privileges', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Privileges', 'Привилегии', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Process', 'Обработать', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Process', 'Process', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Processed', 'Processed', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Processed', 'Обработан', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Processing', 'Рассылается', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Processing', 'Processing', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Processing "Client sales"', 'Обработка "Клиентские продажи"', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Processing "Client sales"', 'Processing "Client sales"', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Processing "Region plan"', 'Processing "Region plan"', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Processing "Region plan"', 'Обработка "План по регионам"', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Processing "TLS"', 'Обработка "TLS"', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Processing "TLS"', 'Processing "TLS"', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Processing "Week stocks"', 'Processing "Week stocks"', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Processing "Week stocks"', 'Обработка "Остатки"', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Processing company synonyms...', 'Processing company synonyms...', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Processing company synonyms...', 'Обработка синонимов компаний...', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Processing error', 'Ошибка при обработке данных', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Processing error', 'Processing error', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Processing file - %name%', 'Обработка файла - %name%', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Processing file - %name%', 'Processing file - %name%', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Processing is completed', 'Processing is completed', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Processing is completed', 'Обработка завершена', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Processing synonyms', 'Обработка синонимов', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Processing synonyms', 'Processing synonyms', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Processing...', 'Processing...', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Processing...', 'Обработка...', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Procurement stage', 'Procurement stage', 1, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Procurement stage', 'Этап закупки', 2, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Product', 'Продукт', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Product', 'Product', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Product direction', 'Продуктовое направление', 2, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Product direction', 'Product direction', 1, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Product directions', 'Продуктовые направления', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Product directions', 'Product directions', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('productName', 'Наименование продукции', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('productName', 'Product Name', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Profile', 'Профиль', 2, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Profile', 'Profile', 1, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Profile', ' Профиль', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Profile', 'Profile', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Promo', 'Промо', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Promo', 'Promo', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Promo material', 'Промо материал', 2, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Promo material', 'Promo material', 1, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Promo material: : {{message}}', 'Promo material: : {{message}}', 1, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Promo materials', 'Promo materials', 1, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Promo materials', 'Промо материалы', 2, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Promo materials', 'Промо-материалы', 2, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Promo name', 'Название', 2, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Promo number task', 'Номер визита', 2, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Promotion', 'Promotion', 1, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Promotion', 'Промоция', 2, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Promotions', 'Промоции', 2, 'geomarketing');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Promotions', 'Promotions', 1, 'geomarketing');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Proposed changes', 'Предлагаемое значение', 2, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Purchase number', 'Номер закупки', 2, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Purchase number', 'Purchase number', 1, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Purchase state', 'Purchase state', 1, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Purchase state', 'Стадия закупки', 2, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Purchase type', 'Тип закупки', 2, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Purchase type', 'Purchase type', 1, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Purchases', 'Purchases', 1, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Purchases', 'Закупки', 2, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Q1', 'Кв1', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Q1', 'Q1', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Q2', 'Q2', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Q2', 'Кв2', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Q3', 'Кв3', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Q3', 'Q3', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Q4', 'Q4', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Q4', 'Кв4', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Quality Control', 'Контроль качества', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Quality Control', 'Quality Control', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Quantity', 'Quantity', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Quantity', 'Количество', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Quantity', 'Quantity', 1, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Quantity', 'Количество', 2, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Quarter', 'Квартал', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Quarter', 'Quarter', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Quarter', 'Квартал', 2, 'geomarketing');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Quarter', 'Quarter', 1, 'geomarketing');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('questions.confirmErase', 'Do you really want to ERASE all colored territories?', 1, 'geomarketing');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('questions.confirmErase', 'Точно стереть все окрашенные территории?', 2, 'geomarketing');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('questions.deletePolygon', 'Удалить полигон?', 2, 'geomarketing');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('questions.deletePolygon', 'Delete polygon?', 1, 'geomarketing');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('questions.saveEdits', 'You change the polygon.<br>Save old change in company list?', 1, 'geomarketing');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('questions.saveEdits', 'Вы изменили полигон.<br>Учитывать старые изменения в списке учреждений?', 2, 'geomarketing');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('questions.undo', 'Отменить изменения?', 2, 'geomarketing');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('questions.undo', 'Undo change?', 1, 'geomarketing');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Rating', 'Rating', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Rating', 'Оценка', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('READ', ' Прочитано', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('READ', ' Read', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('recalculate', 'Recalculate', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('recalculate', 'Пересчитать', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Recipient', ' Получатель', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Recipient', ' Recipient', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Recipient', 'Получатель', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Recipient', 'Recipient', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Recipient in format', 'Recipient in format', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Recipient in format', 'Получатель в формате', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Recipient name', 'Recipient name', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Recipient name', 'Имя получателя', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Recipients', 'Список получателей', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Recipients', 'Recipients', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Recommended route', 'Recommended route', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Recommended route', 'Рекомендуемый маршрут', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Reference address', 'Эталон адрес', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Reference address', 'Reference address', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Reference company', 'Reference company', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Reference company', 'Эталон учреждение', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Refresh', 'Refresh', 1, 'geomarketing');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Refresh', 'Обновить', 2, 'geomarketing');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Regex', 'Регулярное выражение', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Regex', 'RegEx', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Region', 'Область', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Region', 'Region', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('region', 'Region', 1, 'geomarketing');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Region', 'Region', 1, 'geomarketing');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Region', 'Область', 2, 'geomarketing');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('region', 'Регион', 2, 'geomarketing');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('region', ' Область', 2, 'change_requests');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('region', ' Region', 1, 'change_requests');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Region', 'Region', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Region', 'Регион', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('region', 'Region...', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('region', 'Область...', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Region', 'Область', 2, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Region', 'Region', 1, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Region part', 'Регион', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Region part', 'Region', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Region/City', 'Region/City', 1, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Region/City', 'Регион/Город', 2, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Regional manager', 'Regional manager', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Regional manager', 'Региональный менеджер', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Regional Representative', 'Региональный представитель', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Regional Representative', 'Regional Representative', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Regions', 'Regions', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Regions', 'Области', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('regions', 'Regions', 1, 'geomarketing');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('regions', 'Области', 2, 'geomarketing');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Regions filled', 'Regions filled', 1, 'geomarketing');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Regions filled', 'Областей заполнено', 2, 'geomarketing');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Registration', 'Регистрация', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Registration', 'Registration', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Reject', 'Отклонить', 2, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Reject', 'Отклонить', 2, 'geomarketing');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Reject', 'Reject', 1, 'geomarketing');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Reject', 'Reject', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Reject', 'Отклонить', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Reject granual plan', 'Отклонить грануальный план', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Reject granual plan', 'Reject granual plan', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Rejected', 'Rejected', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Rejected', 'Отклонен', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Rejected', 'Отклонено', 2, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Rejected', 'Rejected', 1, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Related dictionary exists', 'Существуют связанные справочники', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Related dictionary exists', 'Related dictionary exists', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Release form, dosage', 'Форма выпуска, дозировка', 2, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Release form, dosage', 'Release form, dosage', 1, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Release form, dosage, units', 'Release form, dosage, units', 1, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Release form, dosage, units', 'Форма выпуска, дозировка, единицы измерения', 2, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Remind', ' Напомнить', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Remind password', 'Напомнить пароль', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Remind password', 'Remind password', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Remove additional field', 'Удалить доп. поля', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Remove additional field', 'Remove additional field', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Remove pharmacy', 'Удалить аптеку', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Remove pharmacy', 'Remove pharmacy', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Removed', 'Removed', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Removed', 'Удалено успешно', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Repeat password', ' Повторите пароль', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Report', 'Отчет', 2, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Report', 'Report', 1, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Report', 'Отчёт', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Report', 'Report', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Report data isn''t found', 'Report data isn''t found', 1, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Report is not found', 'Отчет не найден', 2, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Report sales', 'Отчет продажи', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Report sales', 'Report sales', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Reports', 'Reports', 1, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Reports', 'Отчеты', 2, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('reports', 'reports', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('reports', 'отчетов', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Reports', 'Reports', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Reports', 'Отчеты', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Request initiator', 'Request initiator', 1, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Request initiator', 'Инициатор запроса', 2, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Requests', 'Запросы', 2, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Requests', 'Requests', 1, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Required', 'Обязательное', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Required', 'Required', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Required field can not be hidden', 'Required field can not be hidden', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Required field can not be hidden', 'Обязательное поле не может быть скрыто', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('RequiredShort', 'Обяз', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('RequiredShort', 'Required', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Requires re-verification', 'Requires re-verification', 1, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Resend with SMS', 'Переотправить в СМС', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Resend with SMS', 'Resend with SMS', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('RESENDING_SMS_DELIVERED', ' Resending sms - delivered', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('RESENDING_SMS_DELIVERED', ' Переотправка смс - доставлено', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('RESENDING_SMS_SENT', ' Переотправка смс - отправлено', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('RESENDING_SMS_SENT', ' Resending sms - sent', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('RESENDING_SMS_VISITED', 'Resending sms - visited', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('RESENDING_SMS_VISITED', 'Переотправка смс - посещено', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Reset', 'Сбросить', 2, 'geomarketing');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Reset', 'Reset', 1, 'geomarketing');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Reset', 'Reset', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Reset', 'Сбросить', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Reset all', 'Сбросить все', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Reset all', 'Reset all', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Reset password', ' Восстановить пароль', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Reset selected category', 'Сбросить выбранную категорию', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Responsibility', 'Ответственность', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Responsibility', 'Responsibility', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Responsibility Coefficient', 'Responsibility Coefficient', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Responsibility Coefficient', 'Коэффициент ответственности', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Responsible', 'Виконувач', 3, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('responsible', 'Responsible...', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('responsible', 'Руководитель...', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Responsible', 'Responsible', 1, 'geomarketing');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('responsible', 'Responsible', 1, 'geomarketing');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('responsible', 'Ответственный', 2, 'geomarketing');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Responsible', 'Ответственный', 2, 'geomarketing');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Responsible', 'Ответственный', 2, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Responsible', 'Responsible', 1, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Responsible already added', 'Responsible already added', 1, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Responsible already added', 'Ответственный уже добавлен', 2, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Responsible PM', 'Responsible PM', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Responsible PM', 'Ответственный PM', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Responsible was successfully saved', 'Ответственный был успешно сохранен', 2, 'geomarketing');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Responsible was successfully saved', 'Responsible was successfully saved', 1, 'geomarketing');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Role', 'Role', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Role', 'Роль', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('ROLE_ACCESS_TO_CLM_MANAGER', 'CLM', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('ROLE_ACCESS_TO_CLM_MANAGER', 'CLM', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('ROLE_ACCESS_TO_CRM', 'CRM', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('ROLE_ACCESS_TO_CRM', 'CRM', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('ROLE_ACCESS_TO_FEEDBACK', 'Обратная связь', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('ROLE_ACCESS_TO_FEEDBACK', 'Feedback Service', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('ROLE_ACCESS_TO_GEOMARKETING', 'ETMS', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('ROLE_ACCESS_TO_GEOMARKETING', 'ETMS', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('ROLE_ACCESS_TO_PHOTO_REPORT', 'Фотоотчет', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('ROLE_ACCESS_TO_PHOTO_REPORT', 'Photo report', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('ROLE_ACCESS_TO_QUALITY_CONTROL', 'Quality control', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('ROLE_ACCESS_TO_QUALITY_CONTROL', 'Контроль качества', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('ROLE_ACCESS_TO_ROUTES', 'Маршруты', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('ROLE_ACCESS_TO_ROUTES', 'Routes', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('ROLE_ADMIN', 'Administrator', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('ROLE_ADMIN', 'Администратор', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('ROLE_GPS_TRACKER', 'GPS трекер', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('ROLE_GPS_TRACKER', 'GPS tracker', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('ROLE_NO_ACCESS_TO_ROUTE_MEDICAL_REPRESENTATIVE', 'Deny access to Routes', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('ROLE_NO_ACCESS_TO_ROUTE_MEDICAL_REPRESENTATIVE', 'Запрет доступа к маршрутам', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('ROLE_USER', 'Пользователь', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('ROLE_USER', 'User', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Roles and priveleges', 'Roles and priveleges', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Roles and priveleges', 'Роли и привилегии', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Rotate clockwise', ' Повернуть по часовой', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Rotate counterclockwise', ' Повернуть против часовой', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Routes', 'Routes', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Routes', 'Маршруты', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Row successfully removed', 'Запись успешно удалена', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Row successfully removed', 'Row successfully removed', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Rows where this column is empty will be ignored', 'Записи с пустым значением в этой колонке будут игнорироваться', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Rows where this column is empty will be ignored', 'Rows where this column is empty will be ignored', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('ru', 'Русский', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('ru', 'Русский', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('RUB', 'РУБ', 2, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('RUB', 'RUB', 1, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('.y', 'г.', 2, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('.y', '', 1, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('LS', 'ЛС', 2, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('LS', 'D', 1, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Ruler', 'Линейка', 2, 'geomarketing');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Ruler', 'Ruler', 1, 'geomarketing');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Running', 'Начато', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Running', 'Running', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Russia', 'Россия', 2, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Russia', 'Russia', 1, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Russian', 'Русский', 2, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Sale plan', 'План цикла', 2, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Sales', 'Продажи', 2, 'geomarketing');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Sales', 'Sales', 1, 'geomarketing');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Sales (month) uah', 'Продажи(мес.) грн', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Sales (month) uah', 'Sales (month) uah', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Sales amount', 'Sales amount', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Sales amount', 'Сумма продаж', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Sales per year', 'Товарооборот за год', 2, 'geomarketing');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Sales per year', 'Sales per year', 1, 'geomarketing');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Sales period', 'Sales period', 1, 'geomarketing');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Sales period', 'Период продаж', 2, 'geomarketing');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Sales plan', 'План продаж', 2, 'geomarketing');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Sales plan', 'Sales plan', 1, 'geomarketing');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Sales report', 'Sales report', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Sales report', 'Отчет по продажам', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('sales.description', 'Отчет по продажам', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('sales.description', 'Sales report', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Save', 'Сохранить', 2, 'geomarketing');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Save', 'Save', 1, 'geomarketing');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Save', 'Save', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Save', 'Сохранить', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Save', 'Сохранить', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Save', 'Save', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Save', 'Сохранить', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Save', 'Save', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Save', ' Save', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Save', ' Сохранить', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Save', 'Сохранить', 2, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Save', 'Save', 1, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Save', 'Сохранить', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Save', ' Сохранить', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Save changes', 'Сохранить изменения', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Save changes', 'Save changes', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Save granual plan', 'Сохранить грануальный план', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Save granual plan', 'Save granual plan', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Save pattern', 'Сохранить шаблон', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Save pattern', 'Save pattern', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Save photos', 'Сохранить фотографии', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Save photos', 'Save photos', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Save report', 'Save report', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Save report', 'Сохранить отчет', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('saveComment', 'Send', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('saveComment', 'Отправить', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Saved', 'Saved!', 1, 'geomarketing');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Saved', 'Сохранено!', 2, 'geomarketing');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Saved', 'Сохранено успешно', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Saved', 'Saved', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Saving', 'Сохранение...', 2, 'geomarketing');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Saving', 'Saving...', 1, 'geomarketing');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Saving changes...', 'Сохранение изменений...', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Saving changes...', 'Saving changes...', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Saving plan. Please wait...', 'Saving plan. Please wait...', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Saving plan. Please wait...', 'Грануальный план сохраняется. Пожалуйста подождите...', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Schedule of MPI procurement', 'Schedule of MPI procurement', 1, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Schedule of MPI procurement', 'План-график закупки МНН', 2, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Search', 'Search', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Search', 'Поиск', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Search', 'Искать', 2, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Search', 'Поиск', 2, 'geomarketing');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Search', 'Search', 1, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Search', 'Search', 1, 'geomarketing');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Search', 'Поиск', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Search', 'Search', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Search', 'Найти', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Search', 'Search', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Search', 'Поиск', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Search', 'Поиск', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Search', 'Search', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Search', 'Search', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Search for', 'Search for', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Search for', 'Искать', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Search for', 'Искать...', 2, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Search for', 'Search for', 1, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Search for companies...', 'Искать по компаниям...', 2, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Search for companies...', 'Search for companies...', 1, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Search for presentations', ' Найти презентации', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Search for presentations', ' Search for presentations', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Search in brands', 'Search in brands', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Search in brands ', 'Искать в брендах', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Search in companies ', 'Искать в компаниях', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Search in companies', 'Search in companies', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Search selected', 'Search selected', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Search selected ', 'Искать в выбранных', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Search territories', 'Search territories', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Search territories', 'Искать по территориям', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Searching', 'Поиск...', 2, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Searching synonyms', 'Searching synonyms', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Searching synonyms', 'Поиск синонимов', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Searching...', 'Searching...', 1, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Select', 'Выбрать', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Select', 'Select', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Select all', 'Выбрать все', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Select blocks to be copied', 'Выберите блоки для копирования', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Select brand ', 'Выбрать юренд', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Select brand', 'Select brand', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Select city', 'Select city', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Select city', 'Укажите город', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Select company', 'Select company', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Select company ', 'Выбрать компанию', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Select datefrom', 'От', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Select datefrom', 'Select datefrom', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Select datetill', 'Select datetill', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Select datetill', 'До', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Select details presentations', 'Выберите презентацию', 2, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Select details promo', 'Выберите промо материал', 2, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Select Directory', ' Select Directory', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Select Directory', ' Выберите директорию', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Select distributor', 'Select distributor', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Select distributor', 'Выберите дистрибьютора', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Select end date', 'Выберите дату окончания', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Select end date', 'Select end date', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Select field', 'Выберите колонку', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Select field', 'Select field', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Select items', 'Select items', 1, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Select items', 'Выберите пункты', 2, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Select member', 'Select member', 1, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Select member', 'Выберите участника', 2, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Select month', 'Select month', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Select month', 'Выберите месяц', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Select organization', 'Укажите организацию', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Select organization', 'Select organization', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Select pattern', 'Select pattern', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Select pattern', 'Выберите шаблон', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Select period', 'Выберите период', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Select period', 'Select period', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Select period type', 'Выберите тип периода', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Select period type', 'Select period type', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Select plan', 'Пожалуйста, выберите план', 2, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Select plan detail', 'Пожалуйста, выберите детали плана', 2, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Select preparation', 'Select preparation', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Select preparation', 'Укажите препарат', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Select reason', 'Select reason', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Select reason', 'Выберите причину', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Select region', 'Укажите регион', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Select region', 'Select region', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Select start date', 'Select start date', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Select start date', 'Выберите дату начала', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Select task presentations', 'Пожалуйста, выберите бренды и установите промоцию', 2, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Select type to copy on', 'Выберите типы для копирования', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Select user', 'Выберите пользователя', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Select user', 'Select user', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Select user to view', 'Для просмотра выберите пользователя', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Select user to view', 'Select user to view data', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Select whom to copy', 'Выберите кому скопировать', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Selected', 'Selected', 1, 'geomarketing');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Selected', 'Выбрано', 2, 'geomarketing');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Selected directions', 'Selected directions', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Selected directions', 'Выбранные направления', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Selected regions', 'Выбранные области', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Selected regions', 'Selected regions', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Selected reports', 'Выбранные отчеты', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Selected reports', 'Selected reports', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Selected user', 'Selected user', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Selected user', 'Выбранного пользователя', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Send', 'Отправить', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Send', 'Send', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Send', 'Send', 1, 'geomarketing');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Send', 'Отправить', 2, 'geomarketing');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Send data', 'Send data', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Send data', 'Отправить данные', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Send data on email', 'Отправить учетные данные на почту', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Send data on email', 'Send data on email', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Send Date', 'Send Date', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Send Date', 'Отправка', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Send device info', 'Оправить информацию об устройствах', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Send new password', 'Отправить новый пароль', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Send new password', 'Send new password', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Send now', 'Отправить сейчас', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Send now', 'Send now', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Send request', 'Send request', 1, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Send request', 'Отправить запрос', 2, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Send test email', 'Send test email', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Send test email', 'Отправить тестовое письмо', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Send test message', 'Отправить тестовое сообщение', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Send test message', 'Send test message', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Sender Name', 'Sender Name', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Sender Name', 'Имя отправителя', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('SENT', ' Отправлено', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('SENT', ' Sent', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Sent on verification', 'Sent on verification', 1, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('September', 'Сентябрь', 2, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('September', 'September', 1, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('September', ' Сентября', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('serialNumber', 'Serial Number', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('serialNumber', 'Номер серии', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Services', 'Services', 1, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Services', 'Сервисы', 2, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Services', ' Сервисы', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Set', ' Установить', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Set', 'Выбрать', 2, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Set', 'Set', 1, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Set count', 'Set count', 1, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Set count', 'Укажите', 2, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Set required fields', 'Set required fields', 1, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Set required fields', 'Заполните обязательные поля', 2, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('setDateStartEnd', 'Enter the start and end date', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('setDateStartEnd', 'Введите дату начала и окончания', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('setTimeStartEnd', 'Введите время начала и окончания', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('setTimeStartEnd', 'Enter the start and end time', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Settings', 'Настройки', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Sheet', 'Подложка', 2, 'geomarketing');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Sheet', 'Sheet', 1, 'geomarketing');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Sheet approved by user', 'Sheet approved by user', 1, 'geomarketing');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Sheet approved by user', 'Подложка утверждена пользователем', 2, 'geomarketing');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Sheet will be approved for all subordinates', 'Sheet will be approved for all subordinates', 1, 'geomarketing');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Sheet will be approved for all subordinates', 'Подложка будет утверждена для всех подчиненных', 2, 'geomarketing');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Shop name', 'Shop name', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Shop name', 'Название аптеки', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('shortDescription', 'Краткое описание', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('shortDescription', 'Description', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Shortify links', 'Укорачивать ссылки', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Shortify links', 'Shortify links', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Show', 'Показать', 2, 'geomarketing');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Show', 'Show', 1, 'geomarketing');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Show', ' Показать', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Show all', 'Отобразить все', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Show all', 'Show all', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Show all comments', 'Отображать все комментарии', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Show all comments', 'Show all comments', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Show all reports', 'Показывать все отчеты', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Show all reports', 'Show all reports', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Show archived', 'Показать архивные', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Show archived', 'Show archived', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Show Editor', 'Показать редактор', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Show editor', 'Show editor', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Show example', 'Show example', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Show example', 'Показать пример', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Show inactive', 'Показать неактивных', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Show inative', 'Show inative', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Show more', 'Show more', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Show more', 'Показать больше', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Show related', 'Показать связанные', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Sign', 'Признак', 2, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Sign In', 'Войти', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Sign In', 'Sign In', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Sign In', 'Sign In', 1, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Sign up', 'Register', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Sign up', 'Зарегистрироваться', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Signin', 'Войти', 2, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Simple mode', 'Упрощенный режим', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Simple mode', 'Simple mode', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Site', 'Сайт', 2, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Site', 'Site', 1, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Skip', 'Пропустить', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Skip', 'Skip', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Skip untreated', 'Skip untreated', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Skip untreated', 'Пропустить необработанные', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('SMS', 'SMS', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('SMS', 'SMS', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('SMS Body', 'SMS контент', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('SMS Body', 'SMS body', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('SMS Distribution script', 'SMS Distribution script', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('SMS Distribution script', 'Сценарий СМС рассылки', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('SMS Template', 'SMS Шаблон', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('SMS Template', 'SMS Template', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('SMS Title', 'SMS Title', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('SMS Title', 'Название', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Some rows were not processed because of invalid data', 'Некоторые строки не были обработаны из-за неверных данных', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Something went wrong', 'Что-то пошло не так', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Something went wrong', 'Something went wrong', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Sorry but we couldn’t find the page you are looking for', 'К сожалению, страница не найдена или была удалена ', 2, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Sorry but we couldn’t find the page you are looking for', 'Sorry but we couldn’t find the page you are looking for', 1, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('source', 'Источник', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('source', 'Data source', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('sourceAddress', 'Appeal Source', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('sourceAddress', 'Источник обращения', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('sourceCity', 'Город', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('sourceCity', 'City', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('spec', ' Specialization', 1, 'change_requests');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('spec', ' Специализация', 2, 'change_requests');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('specialization', ' Специализация', 2, 'change_requests');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('specialization', ' Specialization', 1, 'change_requests');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Specialization', 'Specialization', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Specialization', 'Specialization', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Specialization', 'Специализация', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Specialization', 'Специализация', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Specialization', 'Специализация', 2, 'geomarketing');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Specialization', 'Specialization', 1, 'geomarketing');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Specialization', 'Специализация', 2, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Specialization', 'Specialization', 1, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Specializations', 'Specializations', 1, 'geomarketing');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Specializations', 'Специализации', 2, 'geomarketing');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Specializations', 'Специализациии', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Specializations', 'Specializations', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Split by points', 'Делить по точкам', 2, 'geomarketing');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Split by points', 'Split by points', 1, 'geomarketing');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Start import', 'Start import', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Start import', 'Старт импорта', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Starts with', 'Starts with', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Starts with', 'Начинается с', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('startsWith', 'Начинается с', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('startsWith', 'Starts with', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('State', 'Состояние', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('State', 'State', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Statistics', 'Статистика', 2, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Statistics', 'Statistics', 1, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Statistics', 'Statistics', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Statistics ', 'Статистика', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Status', 'Статус', 2, 'geomarketing');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Status', 'Status', 1, 'geomarketing');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Status', 'Статус', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Status', 'Status', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('status', 'Статус обращения', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Status', ' Status', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('status', 'Status', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Status', ' Статус', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Status', 'Статус', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Status', 'Status', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Status', 'Статус', 2, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Status verification', 'Status verification', 1, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('STATUS_VERIFICATION', 'Статус верификации', 2, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('STATUS_VERIFICATION_1', 'Ожидает верификацию', 2, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('STATUS_VERIFICATION_2', 'Отправлено на верификацию', 2, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('STATUS_VERIFICATION_3', 'Верифицировано', 2, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('STATUS_VERIFICATION_4', 'Отклонено', 2, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('STATUS_VERIFICATION_5', 'Требует повторной верификации', 2, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('STATUS_VERIFICATION_6', 'Не подлежит верификации', 2, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Step %step%', 'Шаг %step%', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Step %step%', 'Step %step%', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Stop balancing', 'Остановить балансировку', 2, 'geomarketing');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Stop balancing', 'Stop balancing', 1, 'geomarketing');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('str1', 'Строка(1)', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('str2', 'Строка(2)', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('str3', 'Строка(3)', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Street', 'Улица', 2, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Street', 'Street', 1, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Street', 'Street', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Street', 'Улица-дом', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('street', ' Street', 1, 'change_requests');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('street', ' Улица', 2, 'change_requests');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Street type', 'Street type', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Street type', 'Тип улицы', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Street type', 'Street type', 1, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Street type', 'Тип улицы', 2, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Street types', 'Типы улиц', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('streettype', ' Тип улицы', 2, 'change_requests');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('streettype', ' Street type', 1, 'change_requests');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('String', 'Строка', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('String', 'String', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Subject', 'Тема', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Subject', 'Subject', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Subjects', 'Subjects', 1, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Submission', 'Подчинение', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Submission', 'Submission', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Submit changes', 'Submit changes', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Submit changes', 'Подтвердить изменения', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Submit current plan for approval?', 'Отправить текущий грануальный план на утверждение?', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Submit current plan for approval?', 'Submit current plan for approval?', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Submit for approval', 'Submit for approval', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Submit for approval', 'Отправить план на утверждение', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Subordinates', 'Subordinates', 1, 'geomarketing');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Subordinates', 'Подчинённые', 2, 'geomarketing');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Subordinates', 'Подчиненные', 2, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Subortinates', 'Subortinates', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Subortinates', 'В подчинении', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Subproduct direction', 'Суб продуктовое направление', 2, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Subproduct direction', 'Subproduct direction', 1, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Subscribe', 'Subscribe', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Subscribe', 'Подписаться', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('suburb', ' Suburb', 1, 'change_requests');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('suburb', ' Район', 2, 'change_requests');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Suburb', 'Район', 2, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Suburb', 'Suburb', 1, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Success', 'Готово', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Success', 'Done', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Successfully subscribed', 'Успешно подписались', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Successfully subscribed', 'Successfully subscribed', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Successfully unsubscribed', 'Successfully unsubscribed', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Successfully unsubscribed', 'Успешно отписались', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Sum', 'Sum', 1, 'geomarketing');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Sum', 'Сумма', 2, 'geomarketing');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Sum of packages', 'Сумма упаковок', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Sum of packages', 'Sum of packages', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Summary table', 'Summary table', 1, 'geomarketing');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Summary table', 'Сводная таблица', 2, 'geomarketing');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('supervisor', 'Супервайзер', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Supervisor', 'Супервизор', 2, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Supervisor', 'Supervisor', 1, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('supervisor', 'Supervisor', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('supervisorNotFond', 'Для %name% за время с %timeStart% по %timeEnd% координат нет', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('supervisorNotFond', 'For %name% in time between %timeStart% and %timeEnd% no coordinates', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Supported files are', 'Supported files are', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Supported files are', 'Поддерживаемые форматы', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Synonym', 'Синоним', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Synonym', 'Synonym', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Synonyms', 'Synonyms', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Synonyms', 'Синонимы', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Synonyms not found', 'Необработанные синонимы не найдены', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Synonyms not found', 'Synonyms not found', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('System dictionary', 'System dictionary', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('System dictionary', 'Системный справочник', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('System name', 'Системное имя', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Tables', 'Tables', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Tables', 'Таблици', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Tags', 'Теги', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Tags', 'Tags', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Target', 'Target', 1, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Target', 'Таргет-группа', 2, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Target Audience', ' Целевая аудитория', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Target Audience', ' Target Audience', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Target Audiences not found', 'Целевые аудитории отсутствуют', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Target Audiences not found', 'Target Audiences not found', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Target group', 'Target group', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Target group', 'Таргет-группа', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Target Name', 'Название ЦА', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Target Name', 'Target Name', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Target practices', 'Target practices', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Target practices', 'Целевые практики', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Target-group', 'Target-group', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Target-group', 'Таргет-группа', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Targeting', 'Targeting', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Targeting', 'Таргетирование', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Task', 'Task', 1, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Task', 'Визит', 2, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Task activity report', 'Отчет о визитной активности представителей', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Task activity report', 'Task activity report', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Task count', 'Количество визитов', 2, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Task count', 'Task count', 1, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Task is declined and can not be edited', 'Task is declined and can not be edited', 1, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Task is declined and can not be edited', 'Визит отменен и не может быть редактирован', 2, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Task is performed and can not be edited', 'Визит выполнен и не может быть редактирован', 2, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Task is performed and can not be edited', 'Task is performed and can not be edited', 1, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Task not found', 'Task not found', 1, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Task not found', 'Визит не найден', 2, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Task promo', 'Task promo', 1, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Task promo', 'Промоции', 2, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Task state', 'Состояние визита', 2, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Task state', 'Task state', 1, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Task state', 'Cостояние визита', 2, 'geomarketing');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Task state', 'Task state', 1, 'geomarketing');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Task subject already added', 'Task subject already added', 1, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Task subject already added', 'Тема уже добавлена', 2, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Task subjects', 'Темы визита', 2, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Task type', 'Тип визита', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Task type', 'Тип визита', 2, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Task type', 'Task type', 1, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Task type', 'Task type', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Task type', 'Task type', 1, 'geomarketing');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Task type', 'Тип визита', 2, 'geomarketing');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Task type', 'Тип визита', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('taskactivity.description', 'Task activity report', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('taskactivity.description', 'Отчет о визитной активности представителей', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Tasks', 'Tasks', 1, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Tasks', 'Визиты', 2, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Tasks', 'Tasks', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Tasks', 'Визиты', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('team_soft_clm_manager.description', ' ''', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('team_soft_clm_manager.description', ' Удобная и простая загрузка интерактивных презентаций', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('team_soft_geomarketing.description', 'Electronic territory management system', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('team_soft_geomarketing.description', 'Сервис визуального управления территориями на карте. Брики, Полигоны', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('team_soft_master_list.description', 'Формирование клиентской базы исходя из существующих данных визитов', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('team_soft_master_list.description', 'Формирование клиентской базы исходя из существующих данных визитов', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('team_soft_mcm.description', 'Инструмент проведения массовых рассылок. Email, SMS, Messengers', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('team_soft_pharma_angular.description', 'Визитная активность, база клиентов; план-факт по стратегии. Отчетность.', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('team_soft_routes.description', 'Мониторинг перемещений сотрудников внешней службы', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('team_soft_targeting.description', 'Формирование наиболее потенциальной клиентской базы исходя из стратегии компании', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Teams', 'Teams', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Teams', 'Команды', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Template example', 'Template example', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('tendersku', 'Tender SKU', 1, 'geomarketing');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('tendersku', 'Тендерные закупки', 2, 'geomarketing');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Terms', ' Условия', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Territories will be reset. Continue?', 'Territories will be reset. Continue?', 1, 'geomarketing');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Territories will be reset. Continue?', 'Территории сбросятся. Продолжить?', 2, 'geomarketing');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('territory', 'Территория', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('territory', 'Territory', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Territory of work', 'Территория работы', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Testing 360', 'Testing 360', 1, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Testing 360', 'Тестирование 360', 2, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('text', 'текст', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('The "Count" field can only contain numeric values', 'Поле "Количество" может содержать только числовые значения', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('The "Count" field can only contain numeric values', 'The "Count" field can only contain numeric values', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('The company was downloaded. Attention! The record was found in the syndicated database, but the institution is not included in your coverage area, refer to the manual for expanding the territory and download a complete list of institutions from this area', 'The company was downloaded. Attention! The record was found in the syndicated database, but the institution is not included in your coverage area, refer to the manual for expanding the territory and download a complete list of institutions from this area', 1, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('The company was downloaded. Attention! The record was found in the syndicated database, but the institution is not included in your coverage area, refer to the manual for expanding the territory and download a complete list of institutions from this area', 'Учреждение успешно добавлено. Внимание! Запись была найдена в синдикативной базе данных, однако учреждение не входит в Вашу территорию покрытия, обратитесь к руководству для расширения территории и загрузки полного списка учреждений из данной области', 2, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('The contact was downloaded. Attention! The record was found in the syndicated database, but the institution is not included in your coverage area, refer to the manual for expanding the territory and download a complete list of institutions from this area', 'The contact was downloaded. Attention! The record was found in the syndicated database, but the institution is not included in your coverage area, refer to the manual for expanding the territory and download a complete list of institutions from this area', 1, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('The contact was downloaded. Attention! The record was found in the syndicated database, but the institution is not included in your coverage area, refer to the manual for expanding the territory and download a complete list of institutions from this area', 'Клиент успешно добавлен. Внимание! Запись была найдена в синдикативной базе данных, однако клиент не входит в Вашу территорию покрытия, обратитесь к руководству для расширения территории и загрузки полного списка клиентов из данной области', 2, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('The data is processed. Dashboard update will take about 5 minutes, please wait', 'The data is processed. Dashboard update will take about 5 minutes, please wait', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('The data is processed. Dashboard update will take about 5 minutes, please wait', 'Данные обрабатываются. Обновление Дашборда займет около 5 минут, пожалуйста подождите', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('The department of technical support works from 7 am to 9 pm Kyiv time on weekdays.', 'Часы работы службы: с 7 до 21 часа по киевскому времени в будние дни.', 2, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('The department of technical support works from 7 am to 9 pm Kyiv time on weekdays.', 'The department of technical support works from 7 am to 9 pm Kyiv time on weekdays.', 1, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('The department of technical support works from 8 am to 10 pm Moscow time on weekdays.', 'The department of technical support works from 8 am to 10 pm Moscow time on weekdays.', 1, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('The department of technical support works from 8 am to 10 pm Moscow time on weekdays.', 'Часы работы службы: с 8 до 22 часа по московскому времени в будние дни.', 2, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('The downloaded file does not have all required columns or their name is invalid', 'В загружаемом файле присутствуют не все обязательные колонки либо их названия невалидны', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('The downloaded file doesn not have all required columns or their name is invalid', 'The downloaded file does not have all required columns or their name is invalid', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('The fields %fields% are missing.', 'The fields %fields% are missing.', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('The fields %fields% are missing.', 'Поля %fields% не указаны.', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('The file does not match the selected template', 'Файл не соответствует выбранному шаблону', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('The file does not match the selected template', 'The file does not match the selected template', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('The file must contain, at least, one column', 'The file must contain, at least, one column', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('The file must contain, at least, one column', 'Файл должен содержать, как минимум, одну колонку', 2, 'validators');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('The file must contain, at least, one column', 'Файл должен содержать, как минимум, одну колонку', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('The file must contain, at least, one column', 'The file must contain, at least, one column', 1, 'validators');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('The main thing to remember is where to look.', ' Главное помнить, где искать.', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('The maximum file size for the import should not exceed 50 mb.', 'Максимальный размер файла для импорта не должен превышать 50 мб.', 2, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('The maximum file size for the import should not exceed 50 mb.', 'The maximum file size for the import should not exceed 50 mb.', 1, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('The maximum file size should not exceed 12 MB', 'Файл не должен превышать 12 Мбайт', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('The maximum file size should not exceed 12 MB', 'The maximum file size should not exceed 12 MB', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('The page you requested could not be found, either contact your webmaster or try again. Use your browsers Back button to navigate to the page you have prevously come from message', 'Запрашиваемая Вами страница не может быть найдена, либо обратитесь к веб-мастеру либо повторите попытку. Используйте кнопку Назад вашего браузера, чтобы перейти на страницу, которую ранее посетили', 2, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('The page you requested could not be found, either contact your webmaster or try again. Use your browsers Back button to navigate to the page you have prevously come from message', 'The page you requested could not be found, either contact your webmaster or try again. Use your browsers Back button to navigate to the page you have prevously come from message', 1, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('The password fields must match.', ' Пароли должны совпадать', 2, 'validators');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('The subordination of the pharmacy network', 'Подчиненность организации', 2, 'geomarketing');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('The subordination of the pharmacy network', 'Headquarter', 1, 'geomarketing');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('The value should be greater than datefrom', 'Значение должно быть больше даты начала периода', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('The value should be greater than datefrom', 'The value should be greater than datefrom', 1, 'validators');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('The value should be greater than datefrom', 'Значение должно быть больше даты начала периода', 2, 'validators');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('The value should be greater than datefrom', 'The value should be greater than datefrom', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('There are no available values', 'Нет доступных значений', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('There are no available values', 'There are no available values', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('There are no employees', 'There are no employees', 1, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('This date should be greater than parent date', 'Плановая дата должна быть позже даты родительской рассылки', 2, 'validators');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('This date should be greater than parent date', 'This date should be greater than parent date', 1, 'validators');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('This synonym is already binded. Rebinding this synonym will unbind it from current company. Therefore altering resulting data', 'This synonym is already binded. Rebinding this synonym will unbind it from current company. Therefore altering resulting data', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('This synonym is already binded. Rebinding this synonym will unbind it from current company. Therefore altering resulting data', 'Выбранный синоним уже связан классификатором. Связывая этот синоним вы отвяжете его от привязанной компании. Изменение привязки приведет к изменениям в результирующих данных', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('This value should not be blank', ' Значение не может быть пустым.', 2, 'validators');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('This value should not be blank.', ' This value should not be blank.', 1, 'validators');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('This value should not be null.', 'This value should not be null.', 1, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('This value should not be null.', 'Значение не должно быть пустым.', 2, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Time', 'Время', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Time', 'Time', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('timeBetween', '"За время: c %timeStart% по %timeEnd%"', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('timeBetween', 'In time between %timeStart% and %timeEnd%', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('timeEnd', 'Время окончания...', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('timeEnd', 'Time end...', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('timeStart', 'Time start...', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('timeStart', 'Время начала...', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Timezone', 'Часовой пояс', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Timezone', 'Timezone', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Title', 'Title', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Title', 'Заголовок', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('To ', 'До', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('To ', 'To', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('to', 'по', 2, 'geomarketing');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('to', 'to', 1, 'geomarketing');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('To change the password, click the “Recover” button. If you did not send a request, ignore this email', ' Для смены пароля нажмите кнопку “Восстановить”. Если Вы не отправляли запрос, проигнорируйте это письмо', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('To connect additional services, contact your manager', ' Для подключения дополнительных сервисов обратитесь к Вашему менеджеру', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('To edit the information, exit copy mode!', 'Для редактирования информации выйдите из режима копирования!', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Today', 'сегодня', 2, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Today', 'today', 1, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('tomln', 'Turnover, million', 1, 'geomarketing');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('tomln', 'ТО, млн', 2, 'geomarketing');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Tool panel', 'Tool panel', 1, 'geomarketing');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Tool panel', 'Настройки колонок', 2, 'geomarketing');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('tooltip.geomarketingCompanyInfoTurnoverByMonthDisabled', 'Turnover per quarter', 1, 'geomarketing');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('tooltip.geomarketingCompanyInfoTurnoverByMonthDisabled', 'Поквартальный товарооборот', 2, 'geomarketing');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('tooltip.geomarketingCompanyInfoTurnoverByMonthEnabled', 'Товарооборот за месяц', 2, 'geomarketing');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('tooltip.geomarketingCompanyInfoTurnoverByMonthEnabled', 'Turnover per month', 1, 'geomarketing');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Top bricks', 'Top bricks', 1, 'geomarketing');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Top bricks', 'Top bricks', 2, 'geomarketing');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Top pharmacies', 'Top pharmacies', 1, 'geomarketing');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Top pharmacies', 'Потенциальные аптеки', 2, 'geomarketing');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Total', 'Всего', 2, 'geomarketing');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Total', 'Total', 1, 'geomarketing');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Total', 'Всего', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Total', 'Total', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Total coefficient', 'Total coefficient', 1, 'geomarketing');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Total coefficient', 'Суммарный коэффициент', 2, 'geomarketing');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Total contacts in the distribution', 'Total contacts in the distribution', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Total contacts in the distribution', 'Всего контактов в рассылке', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Total mark', 'Total mark', 1, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Total mark', 'Общая оценка', 2, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Total volume of MPI financing', 'Общий объем финансирования ЛПУ', 2, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Total volume of MPI financing', 'Total volume of MPI financing', 1, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Trade name', 'Trade name', 1, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Trade name', 'Торговое наименование', 2, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Training', 'Training', 1, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Training', 'Тренинг', 2, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Trigger', 'Триггер', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Trigger', 'Trigger', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Trigger date', 'Trigger date', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Trigger date', 'Период действия триггера', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Trigger name', 'Название триггера', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Trigger name', 'Trigger name', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Trigger type', 'Trigger type', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Trigger type', 'Тип триггера', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Triggers', 'Триггеры', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Triggers', 'Triggers', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Triggers not found', 'Triggers not found', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Triggers not found', 'Триггеры отсутствуют', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Turnover', 'Товароборот', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Turnover', 'Turnover', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('turnover', 'Товарооборот', 2, 'geomarketing');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Turnover', 'Товарооборот', 2, 'geomarketing');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Turnover', 'Turnover', 1, 'geomarketing');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('turnover', 'Turnover', 1, 'geomarketing');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('turnoverRange', 'Turnover range', 1, 'geomarketing');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('turnoverRange', 'Товарооборот', 2, 'geomarketing');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Tutorial', ' Туториал', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Type', 'Тип', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Type', 'Тип', 2, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Type', 'Type', 1, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Type', 'Type', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Type', 'Тип', 2, 'geomarketing');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Type', 'Type', 1, 'geomarketing');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Type of changes', 'Тип изменений', 2, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Type of request', 'Type of request', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Type of request', 'Тип запроса', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Type your message', 'Type your message', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Type your message', 'Напишите ваше сообщение', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('typeComment', 'Введите комментарий...', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('typeComment', 'Type comment...', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Types', 'Types', 1, 'geomarketing');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Types', 'Типы', 2, 'geomarketing');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Ukraine', 'Украина', 2, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Ukraine', 'Ukraine', 1, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Unable to get status', 'Невозможно получить статус', 2, 'geomarketing');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Unable to get status', 'Unable to get status', 1, 'geomarketing');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Unable to start balancing', 'Unable to start balancing', 1, 'geomarketing');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Unable to start balancing', 'Невозможно запустить балансировку', 2, 'geomarketing');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Unbind', 'Отвязать', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Unbind', 'Unbind', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Uncategorized', 'Без категории', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Uncategorized', 'Uncategorized', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('uncheck all', 'снять все', 2, 'geomarketing');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('uncheck all', 'uncheck all', 1, 'geomarketing');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('UNDELIVERED', ' Не доставлено', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('UNDELIVERED', ' Undelivered', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Unfold', 'Unfold', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Unfold', 'Раскрыть', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Unidentified error', 'Unidentified error', 1, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Unidentified error', 'Неопознанная ошибка', 2, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Unit name', 'Имя объекта', 2, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Unit price', 'Цена за единицу', 2, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Unit price', 'Unit price', 1, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Unprocessed', 'Необработанные', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Unprocessed', 'Unprocessed', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Unresolved synonyms found', 'Unresolved synonyms found', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Unresolved synonyms found', 'Найдены необработанные синонимы', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Unsubscribe', 'Unsubscribe', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Unsubscribe', 'Отписаться', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Unsubscribe from distribution', 'Отписаться от рассылки', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Unsubscribe from distribution', 'Unsubscribe from distribution', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Unsubscribe reason', 'Unsubscribe reason', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Unsubscribe reason', 'Причина отписки', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('UNSUBSCRIBED', ' Unsubscribed', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('UNSUBSCRIBED', ' Отписано', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Unsupported format', 'Неподдерживаемый формат', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Unsupported format', 'Unsupported format', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Until', 'До', 2, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Until', 'Until', 1, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Untitled', 'Untitled', 1, 'geomarketing');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Untitled', 'Без названия', 2, 'geomarketing');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('UNVERIFIED', ' Неподтвержденный', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('UNVERIFIED', 'Unverified', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Update', 'Update', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Update', 'Обновить', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Update', 'Update', 1, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Update', 'Обновить', 2, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Update list', 'Редактировать список', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Update list', 'Update list', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Updating analytics...', 'Обновление аналитики...', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Updating analytics...', 'Updating analytics...', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Updating packages from table [&table]...', 'Updating packages from table [&table]...', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Updating packages from table [&table]...', 'Обновление количества продаж из временной таблицы [&table]...', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Updating packages...', 'Обновление количества продаж...', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Updating packages...', 'Updating packages...', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Updating synonym packages...', 'Updating synonym packages...', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Updating synonym packages...', 'Перерасчет синонимов...', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Updating synonyms...', 'Обновление синонимов...', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Updating synonyms...', 'Updating synonyms...', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Upload', 'Upload', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Upload', ' Загрузить', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Upload avatar', ' Загрузите фото профиля', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Upload date', 'Upload date', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Upload date', 'Дата загрузки', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Upload file', ' Загрузить файл', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Upload file', 'Upload file', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Upload reports', 'Загрузить отчеты', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Upload reports', 'Upload reports', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Uploaded file is too large. Server supports upload of files up to %size%. Try to split it into multiple chunks.', 'Uploaded file is too large. Server supports upload of files up to %size%. Try to split it into multiple chunks.', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Uploaded file is too large. Server supports upload of files up to %size%. Try to split it into multiple chunks.', 'Загружаемый файл слишком большой. Сервер поддерживает загрузку файлов до %size%. Попробуйте разбить файл на несколько.', 2, 'validators');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Uploaded file is too large. Server supports upload of files up to %size%. Try to split it into multiple chunks.', 'Uploaded file is too large. Server supports upload of files up to %size%. Try to split it into multiple chunks.', 1, 'validators');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Uploaded file is too large. Server supports upload of files up to %size%. Try to split it into multiple chunks.', 'Загружаемый файл слишком большой. Сервер поддерживает загрузку файлов до %size%. Попробуйте разбить файл на несколько.', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Uploaded files not found', 'Загруженные файлы отсутствуют', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Uploaded files not found', 'Uploaded files not found', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Uploaded reports', 'Uploaded reports', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Uploaded reports', 'Загруженные отчеты', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Uploading files', 'Uploading files', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Uploading files', 'Загрузка файлов', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Uploading files into database complete!', 'Файлы загружены в базу данных. Обработка...', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Uploading files into database complete!', 'Uploading files into database complete!', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Uploading files into database...', 'Uploading files into database...', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Uploading files into database...', 'Загрузка файлов в базу данных...', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Uploading...', 'Загрузка...', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Uploading...', 'Uploading...', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Use latin characters, digits and underscores only', 'Use latin characters, digits and underscores only', 1, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Use latin characters, digits and underscores only', 'Используйте только латинские буквы, цифры и нижнее подчеркивание', 2, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Use regex to extract cell value', 'Use regex to extract cell value', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Use regex to extract cell value', 'Использовать регулярное выражение чтобы извлечь значение', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('user', 'User', 1, 'geomarketing');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('User', 'User', 1, 'geomarketing');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('User', 'Пользователь', 2, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('User', 'User', 1, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('user', 'Пользователь', 2, 'geomarketing');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('User', 'Пользователь', 2, 'geomarketing');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('user', 'Пользователь...', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('User', 'Пользователь', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('user', 'User...', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('User', 'User', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('User {{name}} already has the plan in this period.', 'User {{name}} already has the plan in this period.', 1, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('User {{name}} already has the plan in this period.', 'У пользователя {{name}} уже есть план в этом периоде.', 2, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('User {{name}} has no plan in this period.', 'У пользователя {{name}} нет плана в этом периоде.', 2, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('User {{name}} has no plan in this period.', 'User {{name}} has no plan in this period.', 1, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('User account is disabled.', 'User account is disabled.', 1, 'security');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('User account is disabled.', 'Аккаунт пользователя не активирован.', 2, 'security');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('User created', 'Пользователь создан', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('User created', 'User created', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('User credentials', 'User credentials', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('User credentials', 'Данные пользователя', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('User doesnt work.', 'Пользователь не работает.', 2, 'security');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('User doesnt work.', 'User doesnt work.', 1, 'security');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('User doesn''t work.', 'User doesn''t work.', 1, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('User doesn''t work.', 'Пользователь не работает.', 2, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('User list', 'User list', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('User list', 'Список пользователей', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('User management', 'Управление пользователями', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('User management', 'User management', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('User not selected', 'Пользователь не выбран', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('User positions', 'Должности пользователей', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('User updated', 'Пользователь обновлен', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('User updated', 'User updated', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Username could not be found.', 'Имя пользователя не найдено.', 2, 'security');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Username could not be found.', 'Username could not be found.', 1, 'security');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('users', 'Пользователи', 2, 'geomarketing');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('users', 'Users', 1, 'geomarketing');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Users and polygons', 'Users and polygons', 1, 'geomarketing');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Users and polygons', 'Пользователи и полигоны', 2, 'geomarketing');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Users list', 'Список пользователей', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Users list', 'Users list', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Vacancy', 'Вакансия', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Vacancy', 'Vacancy', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Vacation', 'Vacation', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Vacation', 'В отпуске', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Value', 'Объём', 2, 'geomarketing');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Value', 'Value', 1, 'geomarketing');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Value already using by user', ' Value already used by %user%', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Value already using by user', ' Это значение уже используется пользователем %user%', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Value is not a number', ' Значение должно быть числовым', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Value is not in list', ' Значение не из списка', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Value should be more then 0 and should be integer', 'Value should be more then 0 and should be integer', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Value should be more then 0 and should be integer', 'Значение должно быть больше 0 и должно быть целым', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Value updated', 'Значение обновлено', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Values of enabled settings', 'Значения включенных настроек', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Values of enabled settings', 'Values of enabled settings', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('varchar', 'text', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('varchar', 'текст', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Variable', 'Значение', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Variable', 'Variable', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Variables not found', 'Variables not found', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Variables not found', 'Поля отсутствуют', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Verification', 'Verification', 1, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Verification', 'Верификация', 2, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Verified', 'Подтверждено', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Verified', 'Verified', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Verified', 'Verified', 1, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Viber', 'Viber', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Viber', 'Viber', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Viber Distribution script', 'Viber Distribution script', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Viber Distribution script', 'Сценарий Viber рассылки', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Viber subject', 'Подпись', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Viber subject', 'Viber subject', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Viber Template', 'Viber Template', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Viber Template', 'Viber Шаблон', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Viber title', 'Название', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Viber title', 'Viber title', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('View action', 'Просмотр задачи', 2, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('View action', 'View action', 1, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('View as', 'View as', 1, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('View as', 'Вид', 2, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('View task', 'View task', 1, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('View task', 'Просмотр визита', 2, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Visibility', 'Видимость', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Visible add. fields', 'Visible add. fields', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Visible fields', 'Visible fields', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Visible fields', 'Отображаемые поля', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('VISITED', ' Посещено', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('VISITED', ' Visited', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Visits', 'Визиты', 2, 'geomarketing');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Visits', 'Visits', 1, 'geomarketing');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('visitsRange', 'Visits range', 1, 'geomarketing');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('visitsRange', 'Период визитов', 2, 'geomarketing');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Volume of MPI financing', 'Volume of MPI financing', 1, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Volume of MPI financing', 'Объем финансирования МНН', 2, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('VP_EXPIRED', ' Expired', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('VP_EXPIRED', ' Просрочено', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('waddr', ' Site', 1, 'change_requests');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('waddr', ' Сайт', 2, 'change_requests');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Waiting', 'Waiting', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Waiting', 'Ожидает', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Waiting response from server', 'Ожидаем ответа сервера...', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Waiting response from server', 'Waiting response from server', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Warning', 'Warning', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Warning', 'Внимание', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Watcher already added', 'Watcher already added', 1, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Watcher already added', 'Наблюдатель уже добавлен', 2, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Watchers', 'Наблюдатели', 2, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Watchers', 'Watchers', 1, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('We have already sent you an email with a password recovery instructions', ' Мы уже отправили вам на почту письмо с инструкцией по восстановлению пароля!', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Web Apps', ' Web Apps', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Week', 'неделя', 2, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Week', 'Week', 1, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Welcome', 'Добро пожаловать', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Welcome', 'Welcome', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Welcome to Administration Panel', 'Добро пожаловать в панель администрирования', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Welcome to Administration Panel', 'Welcome to Administration Panel', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Welcome to PharmaHRM', 'Вітаємо в PharmaHRM', 3, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Welcome to PharmaHRM', 'Приветствуем в PharmaHRM', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Whats inside', 'Что на фото', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Whats inside', 'Whats inside', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Without status', 'Без статуса', 2, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('withUserBrick', 'Искать на территории', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('withUserBrick', 'Search on territory', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Workday', 'К-во рабочих дней', 2, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Working days', 'Working days', 1, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('workplace', ' Workplace', 1, 'change_requests');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('workplace', ' Доп. место работы', 2, 'change_requests');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('workplace (position)', ' Доп. место работы (должность)', 2, 'change_requests');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('workplace (position)', ' Workplace (position)', 1, 'change_requests');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('y.', 'г.', 2, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Year', 'Год', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Year', 'Year', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Year', 'Год', 2, 'geomarketing');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Year', 'Year', 1, 'geomarketing');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Yes', 'Yes', 1, 'geomarketing');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Yes', 'Да', 2, 'geomarketing');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Yes', 'Да', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Yes', 'Yes', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Yes, close', ' Да, выйти', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Yes, delete', ' Да, удалить', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Yes, log out', ' Да, выйти', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('You can`t copy to previous date', 'Вы не можете копировать на прошедшую дату', 2, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('You can`t copy to previous date', 'You can`t copy to previous date', 1, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('You have already sent a request for archiving. You can send the next request for archiving after', ' You have already sent a request for archiving. You can send the next request for archiving after', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('You have already sent a request for archiving. You can send the next request for archiving after', ' Вы уже отправляли запрос на архивацию. Следующий запрос на архивацию вы сможете отправить после', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('You have filled agreements incorrectly', 'You have filled agreements incorrectly', 1, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('You have filled agreements incorrectly', 'Вы неправильно заполнили договоренность', 2, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('You have filled brands incorrectly', 'Вы неправильно заполнили бренды', 2, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('You have filled brands incorrectly', 'You have filled brands incorrectly', 1, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('You have filled preparations incorrectly', 'You have filled preparations incorrectly', 1, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('You have filled preparations incorrectly', 'Вы неправильно заполнили контроль', 2, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('You have filled promo materials incorrectly', 'Вы неправильно заполнили промо материалы', 2, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('You have filled promo materials incorrectly', 'You have filled promo materials incorrectly', 1, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('You have unsubscribed from distribution', 'Вы отписались от рассылки', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('You have unsubscribed from distribution', 'You have unsubscribed from dictribution', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('You want to delete previously imported reports. The reports data will be deleted from the analytics', 'Вы хотите удалить ранее импортированные отчеты. Данные отчетов будут удалены из аналитики', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('You want to delete previously imported reports. The reports data will be deleted from the analytics', 'You want to delete previously imported reports. The reports data will be deleted from the analytics', 1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Your profile has beed saved', ' Ваш профиль сохранен', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Your site always has buttons for installing a PharmaTouch CRM application on tablets. To do this, open the link on the tablet and click on the appropriate button. After that follow the installation instructions.', ' На вашем сайте всегда доступны кнопки для установки CRM приложения PharmaTouch на планшеты. Для этого откройте ссылку на планшете и кликните на соответствующую кнопку. После этого следуйте инструкциям по установке.', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('YYYY', ' ГГГГ', 2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Zoom to all companies', 'Zoom to all companies', 1, 'geomarketing');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Zoom to all companies', 'Масштабировать по всем учреждениям', 2, 'geomarketing');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Cancel', 'Отмена', 2, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Progress status', 'Статус выполнения', 2, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Date of execution', 'Дата выполнения', 2, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Responsible for implementation', 'Ответственный за выполнение', 2, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Med. representative', 'Мед. представитель', 2, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Result', 'Результат', 2, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Development map', 'Карта развития', 2, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Delete task', 'Удалить видит', 2, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Attach file', 'Прикрепить файл', 2, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('No rows', 'No rows', 1, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('No rows', 'Нет записей', 2, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Coverage', 'Coverage', 1, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Coverage', 'Покрытие', 2, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('No rows', 'No rows', 1, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('No rows', 'Нет записей', 2, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Coverage', 'Coverage', 1, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Coverage', 'Покрытие', 2, 'crm');

INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Active', 'Active', 1, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Active', 'Активный', 2, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('No active', 'No active', 1, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('No active', 'Не активный', 2, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Actual shipment percentage', 'Actual % of shipment at the time of the visit', 1, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Actual shipment percentage', 'Фактический % отгрузки на момент визита', 2, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Survey on the results of the task', 'Survey on the results of the task', 1, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Survey on the results of the task', 'Анкета по результатам визита', 2, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Please, fill at least one survey', 'Please, fill at least one survey', 1, 'validators');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Please, fill at least one survey', 'Пожалуйста, заполните хотя бы одну анкету', 2, 'validators');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Cat. Calc.', 'Cat. Calc.', 1, 'targeting');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Cat. Calc.', 'Кат. Расч.', 2, 'targeting');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Cat. Calc.', 'Кат. Розр.', 3, 'targeting');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Cat. Appr.', 'Cat. Appr.', 1, 'targeting');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Cat. Appr.', 'Кат. Утв.', 2, 'targeting');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Cat. Appr.', 'Кат. Затв.', 3, 'targeting');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Patient flow', 'Patient flow', 1, 'targeting');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Patient flow', 'Пациенто-поток', 2, 'targeting');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Patient flow', 'Потік пацієнтів', 3, 'targeting');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Potency.', 'Potency.', 1, 'targeting');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Potency.', 'Потенц.', 2, 'targeting');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Potency.', 'Потенц.', 3, 'targeting');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Fact/potency.', 'Fact/potency.', 1, 'targeting');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Fact/potency.', 'Факт/потенц.', 2, 'targeting');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Fact/potency.', 'Факт/потенц.', 3, 'targeting');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Institutions by plan', 'Institutions by plan', 1, 'targeting');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Institutions by plan', 'Учреждений по стратегии', 2, 'targeting');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Institutions by plan', 'Установ по стратегії', 3, 'targeting');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Calls per quarter by plan', 'Calls per quarter by plan', 1, 'targeting');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Calls per quarter by plan', 'Визитов по стратегии за квартал', 2, 'targeting');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Calls per quarter by plan', 'Візитів по стратегії за квартал', 3, 'targeting');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Found more than [[modalList.limit-1]] records. Refine your search criteria', 'Found more than [[modalList.limit-1]] records. Refine your search criteria', 1, 'targeting');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Found more than [[modalList.limit-1]] records. Refine your search criteria', 'Найдено более [[modalList.limit-1]] записей. Уточните критерий для поиска', 2, 'targeting');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Found more than [[modalList.limit-1]] records. Refine your search criteria', 'Знайдено понад [[modalList.limit-1]] записів. Уточніть критерій для пошуку', 3, 'targeting');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Calls per month by plan', 'Calls per month by plan', 1, 'targeting');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Calls per month by plan', 'Визитов по стратегии за месяц', 2, 'targeting');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Calls per month by plan', 'Візитів по стратегії за місяць', 3, 'targeting');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Doctors by plan', 'Doctors by plan', 1, 'targeting');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Doctors by plan', 'Врачей по стратегии', 2, 'targeting');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Doctors by plan', 'Лікарів по стратегії', 3, 'targeting');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Status Calc.', 'Status Calc.', 1, 'targeting');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Status Calc.', 'Статус расч.', 2, 'targeting');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Status Calc.', 'Статус розр.', 3, 'targeting');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Status Appr.', 'Status Appr.', 1, 'targeting');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Status Appr.', 'Статус утв.', 2, 'targeting');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Status Appr.', 'Статус затв.', 3, 'targeting');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Change', 'Change', 1, 'targeting');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Change', 'Изм.', 2, 'targeting');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Change', 'Змін.', 3, 'targeting');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Category', 'Category', 1, 'targeting');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Category', 'Категория', 2, 'targeting');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Category', 'Категорія', 3, 'targeting');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Institutions(genitive)', 'Institution', 1, 'targeting');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Institutions(genitive)', 'Учреждений', 2, 'targeting');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Institutions(genitive)', 'Установ', 3, 'targeting');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Doctors(genitive)', 'Doctors', 1, 'targeting');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Doctors(genitive)', 'Врачей', 2, 'targeting');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Doctors(genitive)', 'Лікарів', 3, 'targeting');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Visits(genitive)', 'Visits', 1, 'targeting');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Visits(genitive)', 'Визитов', 2, 'targeting');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Visits(genitive)', 'Візитів', 3, 'targeting');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('In Total', 'In Total    ', 1, 'targeting');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('In Total', 'Итого', 2, 'targeting');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('In Total', 'Разом', 3, 'targeting');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('New(genitive_plural)', 'New', 1, 'targeting');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('New(genitive_plural)', 'Новых', 2, 'targeting');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('New(genitive_plural)', 'Нових', 3, 'targeting');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Delete(genitive_plural)', 'Deleted', 1, 'targeting');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Delete(genitive_plural)', 'Удаленных', 2, 'targeting');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Delete(genitive_plural)', 'Видалених', 3, 'targeting');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Modifications(genitive)', 'Modifications', 1, 'targeting');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Modifications(genitive)', 'Модификаций', 2, 'targeting');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Modifications(genitive)', 'Модифікацій', 3, 'targeting');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Save', 'Save', 1, 'targeting');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Save', 'Сохранить', 2, 'targeting');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Save', 'Зберегти', 3, 'targeting');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Download Excel', 'Download Excel', 1, 'targeting');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Download Excel', 'Скачать Excel', 2, 'targeting');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Download Excel', 'Завантажити Excel', 3, 'targeting');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Sorting', 'Sorting', 1, 'targeting');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Sorting', 'Сортировка', 2, 'targeting');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Sorting', 'Сортування', 3, 'targeting');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Descending', 'Descending', 1, 'targeting');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Descending', 'По убыванию', 2, 'targeting');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Descending', 'За зменшенням', 3, 'targeting');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Ascending', 'Ascending', 1, 'targeting');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Ascending', 'По возрастанию', 2, 'targeting');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Ascending', 'По зростанню', 3, 'targeting');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Count', 'Count', 1, 'targeting');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Count', 'Количество', 2, 'targeting');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Count', 'Кількість', 3, 'targeting');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Status', 'Status', 1, 'targeting');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Status', 'Статус', 2, 'targeting');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Status', 'Статус', 3, 'targeting');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Institution name', 'Institution name', 1, 'targeting');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Institution name', 'Название учреждения', 2, 'targeting');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Institution name', 'Назва установи', 3, 'targeting');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('City', 'City', 1, 'targeting');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('City', 'Город', 2, 'targeting');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('City', 'Місто', 3, 'targeting');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Address', 'Address', 1, 'targeting');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Address', 'Адрес', 2, 'targeting');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Address', 'Адреса', 3, 'targeting');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Turnover', 'Turnover', 1, 'targeting');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Turnover', 'Товаро­оборот', 2, 'targeting');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Turnover', 'Товарообіг', 3, 'targeting');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('turnover', 'turnover', 1, 'targeting');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('turnover', 'товаро­оборот', 2, 'targeting');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('turnover', 'товарообіг', 3, 'targeting');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Sales', 'Sales', 1, 'targeting');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Sales', 'Продажи', 2, 'targeting');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Sales', 'Продажі', 3, 'targeting');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Segment', 'Segment', 1, 'targeting');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Segment', 'Сегмент', 2, 'targeting');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Segment', 'Сегмент', 3, 'targeting');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Visits for', 'Visits for', 1, 'targeting');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Visits for', 'Визитов за', 2, 'targeting');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Visits for', 'Візитів за', 3, 'targeting');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Empty list', 'Empty list', 1, 'targeting');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Empty list', 'Список пуст', 2, 'targeting');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Empty list', 'Список порожній', 3, 'targeting');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('New', 'New', 1, 'targeting');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('New', 'Новый', 2, 'targeting');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('New', 'Новый', 3, 'targeting');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Was previously', 'Was previously', 1, 'targeting');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Was previously', 'Ранее был', 2, 'targeting');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Was previously', 'Раніше був', 3, 'targeting');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Deleted', 'Deleted', 1, 'targeting');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Deleted', 'Удалён', 2, 'targeting');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Deleted', 'Видалений', 3, 'targeting');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('No data', 'No data', 1, 'targeting');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('No data', 'Нет данных', 2, 'targeting');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('No data', 'Немає даних', 3, 'targeting');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Add', 'Add', 1, 'targeting');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Add', 'Добавить', 2, 'targeting');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Add', 'Добавити', 3, 'targeting');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Delete', 'Delete', 1, 'targeting');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Delete', 'Удалить', 2, 'targeting');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Delete', 'Видалити', 3, 'targeting');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Keep', 'Keep', 1, 'targeting');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Keep', 'Оставить', 2, 'targeting');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Keep', 'Залишити', 3, 'targeting');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Search', 'Search', 1, 'targeting');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Search', 'Поиск', 2, 'targeting');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Search', 'Пошук', 3, 'targeting');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('To find', 'To find', 1, 'targeting');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('To find', 'Найти', 2, 'targeting');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('To find', 'Найти', 3, 'targeting');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Filter', 'Filter', 1, 'targeting');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Filter', 'Фильтр', 2, 'targeting');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Filter', 'Фільтр', 3, 'targeting');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('pharmacy', 'pharmacy', 1, 'targeting');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('pharmacy', 'аптека', 2, 'targeting');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('pharmacy', 'аптека', 3, 'targeting');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Pharmacy', 'Pharmacy', 1, 'targeting');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Pharmacy', 'Аптека', 2, 'targeting');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Pharmacy', 'Аптека', 3, 'targeting');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Targeting', 'Targeting', 1, 'targeting');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Targeting', 'Таргетирование', 2, 'targeting');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Targeting', 'Таргетування', 3, 'targeting');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Search on territory', 'Search on territory', 1, 'targeting');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Search on territory', 'Искать на территории', 2, 'targeting');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Search on territory', 'Пошук на території', 3, 'targeting');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('doctors', 'doctors', 1, 'targeting');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('doctors', 'врачи', 2, 'targeting');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('doctors', 'лікарі', 3, 'targeting');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Doctors', 'Doctors', 1, 'targeting');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Doctors', 'Врачи', 2, 'targeting');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Doctors', 'Лікарі', 3, 'targeting');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Loading', 'Loading', 1, 'targeting');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Loading', 'Идёт загрузка', 2, 'targeting');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Loading', 'Іде завантаження', 3, 'targeting');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Filtered by', 'Filtered by', 1, 'targeting');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Filtered by', 'Отфильтровано по', 2, 'targeting');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Filtered by', 'Відфільтровано по', 3, 'targeting');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Approve', 'Approve', 1, 'targeting');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Approve', 'Утвердить', 2, 'targeting');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Approve', 'Затвердити', 3, 'targeting');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Not found', 'Not found', 1, 'targeting');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Not found', 'Не найдено', 2, 'targeting');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Not found', 'Не знайдено', 3, 'targeting');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Searching', 'Searching', 1, 'targeting');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Searching', 'Идёт поиск', 2, 'targeting');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Searching', 'Йде пошук', 3, 'targeting');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Close', 'Close', 1, 'targeting');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Close', 'Закрыть', 2, 'targeting');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Close', 'Закрити', 3, 'targeting');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Reject', 'Reject', 1, 'targeting');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Reject', 'Отклонить', 2, 'targeting');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Reject', 'Відхилити', 3, 'targeting');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('per quarter', 'per quarter', 1, 'targeting');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('per quarter', 'за квартал', 2, 'targeting');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('per quarter', 'за квартал', 3, 'targeting');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('per month', 'per month', 1, 'targeting');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('per month', 'за месяц', 2, 'targeting');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('per month', 'за місяць', 3, 'targeting');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Untitled', 'Untitled', 1, 'targeting');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Untitled', 'Без названия', 2, 'targeting');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Untitled', 'Без назви', 3, 'targeting');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Target-group', 'Target-group', 1, 'targeting');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Target-group', 'Таргет-группа', 2, 'targeting');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Target-group', 'Таргет-група', 3, 'targeting');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Full name', 'Full name', 1, 'targeting');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Full name', 'ФИО', 2, 'targeting');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Full name', 'ПІБ', 3, 'targeting');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('place of work', 'place of work', 1, 'targeting');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('place of work', 'место работы', 2, 'targeting');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('place of work', 'місце роботи', 3, 'targeting');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('All', 'All', 1, 'targeting');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('All', 'Все', 2, 'targeting');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('All', 'Всі', 3, 'targeting');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Changed', 'Changed', 1, 'targeting');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Changed', 'Изменено', 2, 'targeting');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Changed', 'Змінено', 3, 'targeting');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Not changed', 'Not changed', 1, 'targeting');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Not changed', 'Не изменено', 2, 'targeting');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Not changed', 'Не измінено', 3, 'targeting');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('January', 'January', 1, 'targeting');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('January', 'Январь', 2, 'targeting');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('January', 'Січень', 3, 'targeting');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('February', 'February', 1, 'targeting');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('February', 'Февраль', 2, 'targeting');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('February', 'Лютий', 3, 'targeting');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('March', 'March', 1, 'targeting');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('March', 'Март', 2, 'targeting');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('March', 'Березень', 3, 'targeting');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('April', 'April', 1, 'targeting');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('April', 'Апрель', 2, 'targeting');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('April', 'Квітень', 3, 'targeting');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('May', 'May', 1, 'targeting');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('May', 'Май', 2, 'targeting');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('May', 'Травень', 3, 'targeting');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('June', 'June', 1, 'targeting');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('June', 'Июнь', 2, 'targeting');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('June', 'Червень', 3, 'targeting');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('July', 'July', 1, 'targeting');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('July', 'Июль', 2, 'targeting');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('July', 'Липень', 3, 'targeting');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('August', 'August', 1, 'targeting');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('August', 'Август', 2, 'targeting');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('August', 'Серпень', 3, 'targeting');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('September', 'September', 1, 'targeting');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('September', 'Сентябрь', 2, 'targeting');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('September', 'Вересень', 3, 'targeting');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('October', 'October', 1, 'targeting');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('October', 'Октябрь', 2, 'targeting');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('October', 'Жовтень', 3, 'targeting');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('November', 'November', 1, 'targeting');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('November', 'Ноябрь', 2, 'targeting');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('November', 'Листопад', 3, 'targeting');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('December', 'December', 1, 'targeting');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('December', 'Декабрь', 2, 'targeting');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('December', 'Грудень', 3, 'targeting');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Not approve', 'Not approve', 1, 'targeting');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Not approve', 'Не утвержден', 2, 'targeting');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Not approve', 'Не затверджений', 3, 'targeting');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Requires confirmation', 'Requires confirmation', 1, 'targeting');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Requires confirmation', 'Требует подтверждения', 2, 'targeting');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Requires confirmation', 'Вимагає підтвердження', 3, 'targeting');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Approved', 'Approved', 1, 'targeting');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Approved', 'Утвержден', 2, 'targeting');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Approved', 'Затверджений', 3, 'targeting');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Are you sure you want to approve the base?', 'Are you sure you want to approve the base?', 1, 'targeting');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Are you sure you want to approve the base?', 'Вы точно хотите утвердить базу?', 2, 'targeting');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Are you sure you want to approve the base?', 'Ви точно хочете затвердити базу?', 3, 'targeting');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('List cannot be empty', 'List cannot be empty', 1, 'targeting');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('List cannot be empty', 'Список не может быть пустым', 2, 'targeting');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('List cannot be empty', 'Список не може бути порожнім', 3, 'targeting');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Copy from another tasks', 'Copy from another tasks', 1, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Copy from another tasks', 'Копировать из других задач', 2, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Previous task result', 'Previous task result', 1, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Previous task result', 'Результат предыдущего визита', 2, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Result comment does not exist!', 'Result comment does not exist!', 1, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Result comment does not exist!', 'Нет комментария результата', 2, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'NULL','Нове значення',3,'crm')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Next','Далі',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'No','Ні',3,'geomarketing')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'NULL','Ні',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'No brands','Немає промо матеріалів',3,'crm')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'NULL','Немає скасувати',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Pharmacy','Установа',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'No contacts','Співробітників немає',3,'geomarketing')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'No coordinates for the selected period','Немає координат для вибраного періоду',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'No data','Немає даних',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'No data for selected region and period','Немає даних для вибраного регіону та періоду',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'NULL','Дані не знайдено',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'NULL','Немає співробітників',3,'crm')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'No fields found','Не знайдено полів',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'No files selected','Не вибрано жодного файлу',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'No filters to show','Немає фільтрів',3,'crm')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'No items','Немає даних',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'No items available','Немає даних',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'No items selected','Нічого не вибрано',3,'geomarketing')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'No items selected','Нічого не вибрано',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'No participants found','Учасників не знайдено',3,'crm')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'No plan for selected user','Немає плану для вибраного користувача',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'No preparations','Немає препаратів',3,'crm')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'No promos','Немає брендів',3,'crm')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'No reports available','Нема доступних звітів',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'No responsible','Відповідальні не знайдені',3,'crm')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'No responsible','Відповідальних немає',3,'geomarketing')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'No results','Немає результатів',3,'crm')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'No results','Немає результатів',3,'geomarketing')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'No results found','Немає результатів',3,'crm')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'No results found','Немає результатів',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'No Rows To Show','Немає записів',3,'crm')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'No rows to show','Немає записів',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'No sales','Немає продажів',3,'geomarketing')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'No subjects found','Немає результатів',3,'crm')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'NULL','Немає підлеглих',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'NULL','Теми не знайдені',3,'crm')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'No tasks','Немає візитів',3,'crm')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'No turnover','Товарообіг відсутній',3,'geomarketing')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'No turnover per [[year]]','За [[year]] рік товарообіг відсутній',3,'geomarketing')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Not visited','Візитів немає',3,'geomarketing')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'No watchers found','Не знайдено спостерігачів',3,'crm')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'No brands','Без продажів',3,'geomarketing')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'No result found for selected filters','Не знайдено жодного результату для вибраних фільтрів',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'No rows to show','Немає рядків для показу',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'not approved','не затверджено',3,'geomarketing')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'NULL','Не містить',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Not enough points to create layer','Недостатньо точок для створення шару. Виберіть інші параметри',3,'geomarketing')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'NULL','Не дорівнює',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Not found','Не знайдено',3,'geomarketing')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Not mapped','Не використовується',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Not needed','Не потрібно',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Not required','Не потрібно',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Not selected','Не вибрано',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Not specified','Не вказано',3,'crm')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Not specified','Не вказано',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Not subject to verification','Не підлягає верифікації',3,'crm')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Not to approve','Не затверджувати',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Not verified','Не підтверджено',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Not Working','не працює',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Not contains','Не містить',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Not equals','Не дорівнює',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Nothing found','Нічого не знайдено',3,'crm')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Nothing found','Нічого не знайдено',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Notification','Оповіщення',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Not visited','Без візитів',3,'geomarketing')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'November','Листопад',3,'crm')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'NULL','Листопад',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'NULL','Ціле число',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Number of doctors','Кількість лікарів',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Number of packages','Кількість упаковок',3,'crm')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Number of pharmacies','Кількість аптек',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Number of quarters','Кількість кварталів',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Number of sold units','Кількість проданих одиниць',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Number of units','Кількість одиниць',3,'crm')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Number of visits','Кількість візитів',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Number task','номер завдання',3,'crm')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'October','Жовтень',3,'crm')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'NULL','Жовтень',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'NULL','Викл',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'OK','ОК',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'OKPO','ОКПО',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'NULL','Вкл',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'One of the following fields must contain value ("Preparation" "Preparation code")','Одне з наступних полів повинно містити значення ("Препарат" "Код препарату")',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'NULL','Підтримуються .csv файли',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'NULL','Тільки включені',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'NULL','Підтримується тільки формат Excel',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'NULL','Дозволені тільки числові значення',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'NULL','Тільки файли xls xlsx доступні для завантаження',3,'validators')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Open form','Відкрити форму',3,'crm')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Opening email','Відкрив лист розсилки',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'You have filled preparations incorrectly','Ви неправильно заповнили контроль',3,'crm')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'You have filled promo materials incorrectly','Ви неправильно заповнили промо матеріали',3,'crm')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'You have unsubscribed from dictribution','Ви відписалися від розсилки',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'You want to delete previously imported reports. The reports data will be deleted from the analytics','Ви хочете видалити раніше імпортовані звіти. Дані звітів буде видалено з аналітики',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Zoom to all companies','Масштабувати по всім установам',3,'geomarketing')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Task is performed and can not be edited','Візит виконаний і не може бути редагований',3,'crm')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Task not found','Візит не найден',3,'crm')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Task promo','Промоції',3,'crm')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Task state','Стан візиту',3,'crm')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Task state','Стан візиту',3,'geomarketing')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Task subject already added','Тема вже додана',3,'crm')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'NULL','Теми візиту',3,'crm')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Task type','Тип візиту',3,'crm')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Task type','Тип візиту',3,'geomarketing')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Task type...','Тип візиту',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Task type...','Тип візиту',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Task activity report','Звіт про візитну активності представників',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Tasks','Візити',3,'crm')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Tasks','Візити',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( '','Зручне і просте завантаження інтерактивних презентацій',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Electronic territory management system','Сервіс візуального управління територіями на карті',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Формирование клиентской базы исходя из существующих данных визитов','Формування клієнтської бази виходячи з існуючих даних візитів',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'NULL','Інструмент проведення масових розсилок. Email SMS Messengers',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'NULL','Моніторинг перемещених співробітників зовнішньої служби',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'NULL','Формування найбільш потенційної клієнтської бази виходячи зі стратегії компанії',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Teams','Команди',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Template example','приклад шаблону',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Tender SKU','Тендерні закупівлі',3,'geomarketing')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Territories will be reset. Continue?','Території будуть скинуті. Продовжувати?',3,'geomarketing')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'NULL','Територія',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Testing 360','Тестування 360',3,'crm')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'NULL','текст',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'The "Count" field can only contain numeric values','Поле «кількість» може містити лише числові значення',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'The company was downloaded. Attention! The record was found in the syndicated database but the institution is not included in your coverage area refer to the manual for expanding the territory and download a complete list of institutions from this area','Установа успішно додана. Увага! Запис був знайдений в Синдикативні базі даних однак установа не входить в Вашу територію покриття зверніться до керівництва для розширення території і завантаження повного списку установ з даної області',3,'crm')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'The contact was downloaded. Attention! The record was found in the syndicated database but the institution is not included in your coverage area refer to the manual for expanding the territory and download a complete list of institutions from this area','Клієнт успішно доданий. Увага! Запис була знайдений в Синдикативні базі даних однак клієнт не входить в Вашу територію покриття зверніться до керівництва для розширення території і завантаження повного списку клієнтів з даної області',3,'crm')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'The data is processed. Dashboard update will take about 5 minutes please wait','Дані обробляються. Оновлення інформаційної панелі займе близько 5 хвилин. Зачекайте',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'The department of technical support works from 7 am to 9 pm Kyiv time on weekdays.','Відділ технічної підтримки працює з 7 ранку до 21 години за київським часом у будні дні.',3,'crm')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'The department of technical support works from 8 am to 10 pm Moscow time on weekdays.','Відділ технічної підтримки працює з 8 ранку до 22 вечора за московським часом.',3,'crm')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'NULL','У завантаженому файлі присутній не всі обов''язкові стовпчики, або їх назва недійсна',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'The downloaded file does not have all required columns or their name is invalid','У завантаженому файлі немає всіх необхідних стовпців або їх ім’я недійсне',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'The fields %fields% are missing.','Поля %fields% відсутні.',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'The file does not match the selected template','Файл не відповідає обраному шаблону',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'The file must contain at least one column','Файл не відповідає обраному шаблону',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'The file must contain at least one column','Файл не відповідає обраному шаблону',3,'validators')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'The maximum file size for the import should not exceed 50 mb.','Максимальний розмір файлу для імпорту не повинен перевищувати 50 мб.',3,'crm')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'The maximum file size should not exceed 12 MB','Максимальний розмір файлу не повинен перевищувати 12 МБ',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'The page you requested could not be found either contact your webmaster or try again. Use your browsers Back button to navigate to the page you have prevously come from message','Запитувана Вами сторінка не може бути знайдена або зверніться до веб-майстру або спробуйте ще раз. Використовуйте кнопку Назад вашого браузера щоб перейти на сторінку яку раніше відвідали',3,'crm')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'NULL','Паролі повинні співпадати',3,'validators')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Headquarter','Підпорядкованість організації',3,'geomarketing')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'The value should be greater than datefrom','Значення має бути більше дати початку періоду',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'The value should be greater than datefrom','Значення має бути більше дати початку періоду',3,'validators')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'NULL','Немає доступних значень',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'NULL','Деактивация установи',3,'crm')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Loading data','Завантаження даних',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Loading data...','Завантаження даних з файлу ...',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Loading distributions','Розсилки завантажуються',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Loading sales...','Завантаження продажів ...',3,'geomarketing')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Loading...','Завантаження ...',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Loading...','Завантаження ...',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'NULL','Авторизуватись',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'NULL','Вихід',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Login','Логін',3,'crm')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Login','Вхід',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Login can''t be blank.','Логін не може бути порожнім.',3,'crm')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Login success','Успішно увійшли',3,'crm')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Logout','Вийти',3,'crm')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Logout?','Вийти?',3,'crm')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Looking for synonyms via linked servers...','Пошук синонімів установ по зв''язаним серверам ...',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'NULL','Лояльність',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'NULL','ЛС',3,'crm')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Main company','Аптечна мережа',3,'change_requests')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Main','Основне',3,'geomarketing')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'NULL','Додавання даних у базу',3,'crm')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Main responsible','Головний відповідальний',3,'crm')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Main company...','Аптечна мережа...',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Make','Виробник',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Male','Чоловіча',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Mapping','Відповідність полів',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'March','Березень',3,'crm')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'NULL','Березень',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Markers circle','Кількість фахівців',3,'geomarketing')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Markers circle turnover','Товарообіг і відвідуваність',3,'geomarketing')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Master lists','Майстер листи',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Max allowed filesize is 300 MB','Максимально дозволений розмір файлів - 300 МБ',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'NULL','Максимальна довжина пароля - 6 символів',3,'validators')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'May','Травень',3,'crm')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'NULL','Травень',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Multi Chanel Marketing','Мультиканальний маркетинг',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'NULL','Мед. представник',3,'crm')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Medical Representative','медичний представник',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Member','Учасник',3,'crm')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Member already added','Учасника уже додано',3,'crm')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Members','Учасники',3,'crm')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Message','Повідомлення',3,'crm')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Message','Оповіщення',3,'geomarketing')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Message','Повідомлення',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Message for SMS','Повідомлення для SMS',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Message has been successfully sent','Лист/повідомлення надіслано успішно',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Message was not sent','Лист/повідомлення не відправлено',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Messages','Повідомлення',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Middle name','По батькові',3,'crm')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Middle name','По батькові',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Middle name','По батькові',3,'change_requests')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Pharmacies','Установи',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Midway tracker coordinate','Проміжні координати трекера',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Minimal password length is 6 symbols','Мінімальна довжина пароля - 6 символів',3,'validators')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Missing parameters. ''username'' and ''password'' required','Відсутні параметри. Потрібно «ім''я користувача» і «пароль»',3,'crm')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Mobile','мобільний',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Model','Модель',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Month','місяць',3,'crm')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Month','Місяць',3,'geomarketing')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Month','Місяць',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'More','Більше',3,'geomarketing')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'More about polygon','Детальніше про полігон',3,'geomarketing')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'More then 20 pharmacies found. Make search more specific','Більше 20 аптек знайдено. Зробіть пошук більш конкретним',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'MS and SKU','MS та SKU',3,'geomarketing')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'MS and Brand','MS та Brand',3,'geomarketing')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'NULL','Мультивибір довідника',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Show from synonyms from API','Мої прив''язки',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'NULL','Профіль',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'N/A','Н/Д',3,'geomarketing')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Name','Назва',3,'change_requests')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Name','Назва',3,'crm')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Name','Назва',3,'geomarketing')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Name','Назва',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Name of MPI','Найменування ЛПУ',3,'crm')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Need help?','Вам потрібна допомога?',3,'crm')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'need to approve','потрібно затвердити',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Pharmacy network','Аптечна мережа',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'New account','Нова установа',3,'crm')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'New account request','Новий запит установи',3,'crm')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'New Action','Нове завдання',3,'crm')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'New campaign','Нова кампанія',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'New client','Новий клієнт',3,'crm')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'New client request','Запит на додавання клієнта',3,'crm')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Pharmacies','Аптеки',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Pharmacy','Аптека',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'New contact','Новий клієнт',3,'crm')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'New contact request','Новий запит на клієнта',3,'crm')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'NULL','Нова установа',3,'crm')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'NULL','Новий візит',3,'crm')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'New plan','Новий план',3,'crm')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'New plan detail','Нові деталі плану',3,'crm')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'New plan preparation','Новий бренд',3,'crm')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'New plan presentation','Нова презентація',3,'crm')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'New plan promo material','Новий промо матеріал',3,'crm')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'New preparation','Новий препарат',3,'crm')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'New Task','Новий візит',3,'crm')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'NULL','Новий візит доданий',3,'crm')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'New task created','Створено нове завдання',3,'crm')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'File uploading','Файл завантажується',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'File(s) uploaded successfully','Файли успішно завантажено',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Files','Файли',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Filled','Заповнено',3,'crm')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'NULL','Фільтр',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Available filters','Доступні фільтри',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Filter target audience','Тільки ті хто не отримав розсилку за період',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Filtered column','Стовпчик з фільтром',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Filtering by username','Фільтрування за іменем користувача',3,'geomarketing')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Filter','Фільтр…',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Filters','Фільтри',3,'crm')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Companies parameters','Фільтр установ',3,'geomarketing')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Filters','Фільтри',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'No companies found','Установи не знайдені',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Find','Знайти',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'No pharmacies assigned to selected user','Немає аптек прив''язаних до обраного користувача',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'NULL','Назва',3,'crm')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Finished','Готово',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'NULL','Нова установа',3,'crm')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'First name','Ім''я',3,'crm')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'First Name','Ім''я',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'First tracker coordinate','Перша координата трекера',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Firstname','Ім''я',3,'change_requests')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'NULL','Прапор (1)',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'NULL','Прапор (2)',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'NULL','Прапор (3)',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'NULL','Прапор (4)',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'NULL','Прапор (5)',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'FLCM','FLCM',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'NULL','Дробове число',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'NULL','Дробове число (1)',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'NULL','Дробове число (2)',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'NULL','Дробове число (3)',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Fold','Сховати',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Folder to upload','Папка для завантаження',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Follow','Перейти',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Footer Language','Мова тексту відписки',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'NULL','Для всіх типів',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'For creating poligon close the line','Для створення полігону замкніть лінію',3,'geomarketing')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'For Medical Representative distribution','Для розсилки медичним представникам',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'For period','За період',3,'geomarketing')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'NULL','Забули пароль?',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Format','Формат',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'From','Від',3,'crm')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'from','з',3,'geomarketing')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'From','Від',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'From file','З файлу',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Full name','ПІБ',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Full Name','ПІБ',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Gender','Стать',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'NULL','Основна відповідальність',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Generate','Згенерувати',3,'geomarketing')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Geo division','Гео розподіл',3,'geomarketing')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Filter by geodata','Фільтр по географії',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'ETMS','ETMS',3,'geomarketing')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'GMT','GMT',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Go back to dashboard','Повернутись',3,'crm')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Go to','Перейти',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Go to the Synonyms page','Перейти на сторінку синонімів',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Got to','Перехід до',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Latitude','Широта',3,'change_requests')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Longitude','Довгота',3,'change_requests')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'GR to prev. month','Приріст до попереднього місяця',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Granplan','Гран-план',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Granual plan','Грануальний план',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Granual plan not available','Грануальний план недоступний',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Granually planing','Грануальние планування',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Greater than','Більше',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'NULL','Зміна параметрів установи',3,'crm')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Have brands','Маємо продажи бренда',3,'geomarketing')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'NULL','Вітаємо',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Help','Допомога',3,'crm')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Help','Допомога',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'NULL','Тут зібрані всі додаткові сервіси розроблені для вас.',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'NULL','Приховати',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Hide all','Згорнути все',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Hide inactive','приховати неактивних',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'NULL','Приховати пов''язаний довідник',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'NULL','Ієрархія',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Hospital','Стаціонар',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Hour outside of accepted range','Час поза доступного діапазону',3,'crm')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'House','Будинок',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'There was an internal server error.','Була внутрішня помилка сервера.',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Try loading this page again in some minutes or <a href="%url%">go back to the homepage</a>.','Спробуйте завантажити цю сторінку ще раз за кілька хвилин або <a href="%url%"> повернутися на головну сторінку </a>.',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Internal server error','Внутрішня помилка сервера',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Identifier','Ідентифікатор',3,'crm')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'If you have additional questions about CRM or you need technical advice - our support consultants are ready to help you.','Якщо у вас є додаткові запитання щодо CRM або вам потрібна технічна порада - наші консультанти з підтримки готові вам допомогти.',3,'crm')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Ignore','Ігнорувати',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Image','Зображення',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Photo','Зображення',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Image','Фото',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Photo','Фото',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'IMCP','НМЦК',3,'crm')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Import','Імпортувати',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'NULL','Немає відповідальних',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'NULL','Користувачів немає',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'NULL','English',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'NULL','Включено',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Ends with','Закінчується на',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Ends with','Закінчується',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Enter polygon name','Введіть назву полігона',3,'geomarketing')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Enter regex expression. Use "match" group to specify content to extract','Введіть регулярний вираз. Використовуйте назву групи "match" щоб витягти вміст',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'NULL','Введіть дату початку і закінчення',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'NULL','Введіть час початку і закінчення',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'NULL','Введіть свої облікові дані щоб почати роботу з нашою системою',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'NULL','Введіть свою електронну пошту і ми Вам вишлемо лист',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Equals','Дорівнює',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Erase','Стерти',3,'geomarketing')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Erase all','Стерти все',3,'geomarketing')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Erase by filter','Стерти за допомогою фільтра',3,'geomarketing')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Erase by users and regions','Стерти по користувачах і областям',3,'geomarketing')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Erasing','Стирання',3,'geomarketing')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Error','Помилка',3,'crm')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Error!','Помилка!',3,'geomarketing')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Error','Помилка',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Error 404','Помилка 404',3,'crm')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Error starting balance code','Помилка старту врівноваження код',3,'geomarketing')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Essence','Суть',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Evaluation form','Форма оцінки',3,'crm')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Excluded','Виключено',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Exit','Вихід',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Expand all','Показати всі',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'NULL','Розгорнути всі категорії',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'NULL','Розгорнути категорію',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'NULL','Розгорнути настройку',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Export to Excel','Експорт у Excel',3,'crm')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Export to Excel','Експорт у Excel',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Export to Excel','Експорт у Excel',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Found by id_link','Показати всі',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'NULL','Зовнішній ідентифікатор користувача',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Fact','Факт',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Fact all','Всього факт',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Fact count','Фактична кількість',3,'crm')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Fact plan','Факт план',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Fact plan successfully updated','Фактичний план успішно оновлений',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'NULL','Факт. маршрут (кластерізованний)',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'NULL','Факт. маршрут (візит)',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'NULL','Факт. маршрут (трекер)',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Failed','Не доставлено',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Failed to obtain coordinate','Не вдалося отримати координату',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Fatal Error','Не вдалося оновити',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'February','Лютий',3,'crm')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'NULL','Лютий',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'State','Область',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Feedback','Обратная связь',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Female','Жіночий',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'NULL','Поле',3,'crm')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'NULL','Поле %field% має пов''язаний запис і не може бути видалено',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'NULL','Поле має пов''язантй запис і не може бути видалено',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Field is required','Поле обов''язкове',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Field name','Пов''язане поле',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Fields saved','поля збережені',3,'geomarketing')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'File','Файл',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'File already exists.','Файл з такою назвою вже існує.',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'File already imported','Файл вже імпортований',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'File can not be associated with pattern. Lacking fields','Файл не може бути зіставлений з обраним шаблоном. Бракує стовпчиків',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'File extension is not supported. Select another file','Розширення файлу не підтримується. Виберіть інший файл',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'File extension is not supported. Select another file','Розширення файлу не підтримується. Виберіть інший файл',3,'validators')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'File is not found or removed. Please attach another one.','Файл на знайдений або вилучений будь ласка прикріпіть інший.',3,'validators')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'File Name','Ім''я файлу',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'File Name','Назва файлу',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'File not found','Файл не знайдено',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'NULL','Файл не вибрано',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'File slides.json does not exists','Файл із слайдами не знайдений',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'File slides.json is incorrect','Файл із слайдами некоректно відформатований',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'File successfully imported to database','файл успішно імпортований у базу даних',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'File type does not suit its extension. To import this file save it as one of following types .csv .xls .xlsx .xlsm','Тип файлу не відповідає його розширенням. Щоб імпортувати цей файл, збережіть його як один із наступних типів .csv .xls .xlsx .xlsm',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'File type does not suit its extension. To import this file save it as one of following types .csv .xls .xlsx .xlsm','Тип файлу не відповідає його розширенням. Щоб імпортувати цей файл, збережіть його як один із наступних типів .csv .xls .xlsx .xlsm',3,'validators')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'File upload','Завантаження файлу',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'File uploaded','Файл обраний',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( '(m)','(м)',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( '(source\loaded)','(джерело\завантажено)',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( '"[user] already pinned in this polygon"','"[user] вже приєднаний до цього полігону"',3,'geomarketing')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( '"[user] hSearchas no pin. Click on map to set"','"У користувача [user] немає піну. Натисніть на карту, щоб встановити пін"',3,'geomarketing')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'NULL','+ Додати ще''',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( '1 day after','Через день після',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( '1 day before','За день до',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( '1 week after','Через тиждень після',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( '1 week before','За тиждень до',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( '3 days after','Через три дні після',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( '3 days before','За три дні до',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'AC...','AC...',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Accepted range','Доступний діапазон',3,'crm')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Service is enabled from 20th till the end of the month.','Доступ до сервісу відкривається тільки з 20го по кінець місяця включно.',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'According to the preliminary check in the syndicated database the following institutions were found by the given companies','Відповідно до попередньої перевірки в синдикованій базі даних були знайдені наступні записи згідно наданої назви',3,'crm')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'According to the preliminary check in the syndicated database the following institutions were found by the given company','Відповідно до попередньої перевірки в синдикованій базі даних були знайдені наступні записи згідно наданої назви',3,'crm')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'According to the preliminary check in the syndicated database the following institutions were found by the given contacts','Відповідно до попередньої перевірки в синдикованій базі по даним контактам були знайдені наступні установи',3,'crm')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Account name','Назва установи',3,'crm')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Account owner','Власник установи',3,'crm')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Account status','Статус установи',3,'crm')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'NULL','Відповідальність',3,'crm')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Accounts','Установи',3,'crm')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Accuracy(m)','Точність (м)',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Action','Завдання',3,'crm')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Action is performed and can not be edited','Завдання виконано і не може бути відредаговано',3,'crm')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Action not found','Завдання не знайдено',3,'crm')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Action state','Стан завдання',3,'crm')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Action Type','Тип завдання',3,'crm')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Action Type','Тип завдання',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Action Type','Тип дії',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Actions','Дії',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'NULL','Видимість',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'NULL','Активності',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Actual shipment percentage','Фактичний відсоток відвантаження',3,'crm')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Actually paid','Фактично оплачено',3,'crm')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Add','Додати',3,'crm')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Add','Додати',3,'geomarketing')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Add','Додати',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Add brand','Додати бренд',3,'crm')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Add brands to companies','Додати бренди до ЛПУ',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Add client','Додати клієнта',3,'crm')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Add coach','Додати тренера',3,'crm')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Add companies to brands','Додати ЛПУ до брендів',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Add companies to polygon','Додати установи полігону',3,'geomarketing')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Add company','Додати установу',3,'crm')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Add company','Додати установу',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Add pharmacy','Додати аптеку',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Add detail','Додати деталі',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Add employee','Додати співробітника',3,'crm')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Add file','Додати файл',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Add filter','Додати фільтр',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Add group','Додати групу',3,'geomarketing')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Add member','Додати учасника',3,'crm')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Add owners','Додати виконавців',3,'crm')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Add participant','Додати учасника',3,'crm')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Add polygon','Додати',3,'geomarketing')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Add presentation','Додати презентацію',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Add promo material','Додати промо-матеріал',3,'crm')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Add responsible','Додати відповідального',3,'crm')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'NULL','Додати запис',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Add specialization','Додати спеціалізацію',3,'crm')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Add subject','Додати тему',3,'crm')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Add subornidate','Додати підлеглого',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Add synonym','Додати синонім',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Add target practices','Додати цільові практики',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Add task','Додати візит',3,'crm')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'NULL','Додати тему візита',3,'crm')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Add user','Додати користувача',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Add watcher','Додати спостерігача',3,'crm')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Add workplace','Додати місце роботи',3,'crm')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Adding data to database','Додавання даних до бази даних',3,'crm')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Additional fields','Додаткові поля',3,'crm')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Available add. fields','Додаткові поля',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Additional Fields','Додаткові поля',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'NULL','Додаткові поля оновлені',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Additional Information','Додаткова інформація',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Additional phone','Додатковий телефон',3,'crm')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'NULL','Додаткова відповідальність',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Additional responsible','Додаткові відповідальні',3,'crm')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Additional responsible','Дод. відповідальні',3,'geomarketing')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Additional settings','Додаткові налаштування',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Additional specializations','Додаткові спеціалізації',3,'crm')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Additional workplaces','Додаткові робочі місця',3,'crm')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'NULL','Зміна (карточка клієнтів)',3,'crm')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'NULL','Зміна (картка установи)',3,'crm')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'NULL','Ініціатор змін',3,'crm')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Change responsibility','Змінити відповідальність',3,'crm')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Change responsibility of related records?','Змінити відповідальність пов''язаних записів?',3,'crm')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Change responsible','Зміна відповідальної',3,'crm')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'NULL','Зміни будуть втрачені. Зберегти зміни?',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'characters left','залишилось символів',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Charts','Графіки',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Check all','Відзначити всі',3,'geomarketing')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'NULL','Виберіть',3,'crm')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Choose','Виберіть',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Choose file','Виберіть файл',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Choose from two to five items','Виберіть від двох до п''яти елементів',3,'geomarketing')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Choose region','Виберіть область',3,'geomarketing')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Choose region part','Виберіть регіон',3,'geomarketing')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Choose polygon for comparison (maximum 5)','Виберіть полігон для порівняння (максимум 5)',3,'geomarketing')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Chosen company does not have any available task type','У плані немає типів візиту доступних для даної установи',3,'validators')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Cities','Міста',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'City','Місто',3,'change_requests')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'City','Місто',3,'crm')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'City','Місто',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'City Department','Відділ',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Clear filters','Очистити фільтри',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Click map','Карта кліків',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Clicked','Перейшов за посиланням',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Client','Клієнт',3,'crm')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'NULL','Клієнт',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Client category','Категорія клієнта',3,'crm')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Client not found','Клієнта не знайдено',3,'crm')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Client type','Тип клієнта',3,'crm')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'NULL','Новий кліент',3,'crm')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'NULL','Зміна параметрів клієнта',3,'crm')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'NULL','Деактивація клієнтів',3,'crm')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'NULL','Додавання даних у базу',3,'crm')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Clients','Клієнти',3,'crm')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Clients name','ПІБ клієнта',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'CLM','CLM',3,'crm')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'CLM Manager','CLM Менеджер',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Close','Закрити',3,'crm')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Close','Закрити',3,'geomarketing')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Close','Закрити',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Coach already added','Тренер вже доданий',3,'crm')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Coaches','Тренери',3,'crm')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Code','Код',3,'change_requests')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Code','Код',3,'crm')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'NULL','Код',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Coefficient','Коефіцієнт',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Collapse All','Згорнути все',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'NULL','Згорнути всі категорії',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'NULL','Згорнути категорію',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'NULL','Згорнути настройку',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Polygon color','Колір',3,'geomarketing')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Polygon color','Колір полігону',3,'geomarketing')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Color','Колір',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Column','Стовчик',3,'geomarketing')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Column groups','Групи стовпчиків',3,'geomarketing')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Column name','Назва стовпчика',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'NULL','Назва стовпчика в першому рядку',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Column options','Налаштування стовпчика',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Columns missing','Відсутні стовпчики',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Comment','Коментар',3,'crm')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Comment','Коментар',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Comment','Коментар',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Comment 1','Коментар 1',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Comment 2','Коментар 2',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Comment field is empty','Поле коментар не заповнено',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Comment saved','Коментар збережено',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Comments','Коментарі до звернення',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Chosen company doesn''t have any available task type','У плані немає типів візиту доступних для даної установи',3,'crm')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Compactness','Компактність території',3,'geomarketing')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Companies','Установи',3,'crm')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Companies','Установи',3,'geomarketing')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Companies','Установи',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Companies','Аптеки',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Companies','Установ',3,'geomarketing')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Companies are missing','Установи відсутні',3,'geomarketing')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Companies not found','Установи не знайдено',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Company','Установа',3,'change_requests')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Company','Установа',3,'crm')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Company','Аптека',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Company','Установа',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Company address','Адреса установи',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Company categories','Категорії установ',3,'geomarketing')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Company category','Категорія установи',3,'crm')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Company category','Категорія установи',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Company not found','Установа не знайдена',3,'crm')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Company per categories','Установи за категоріями',3,'geomarketing')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Company per type','Установи за типами',3,'geomarketing')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Company selection','Обрати установу',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Company type','Тип установи',3,'crm')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Company type','Тип установи',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Company type','Тип установи',3,'change_requests')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Company types','Типи установ',3,'geomarketing')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Company verification','Верифікація установи',3,'crm')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Company visits','Візити до установи',3,'geomarketing')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Compare','Порівняти',3,'geomarketing')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Comparison','Порівняння',3,'geomarketing')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Uploaded file is too large. Server supports upload of files up to %size%. Try to split it into multiple chunks.','Завантажений файл занадто великий. Сервер підтримує завантаження файлів до %size%. Спробуйте розділити його на кілька фрагментів.',3,'validators')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Uploaded files not found','завантажені файли не знайдено',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Uploaded reports','Завантажені звіти',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Uploading files','Завантаження файлів',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Uploading files into database complete!','Файли завантажені до бази даних. Обробка ...',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Uploading files into database...','Завантаження файлів у базу даних ...',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Uploading...','Завантаження ...',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Use latin characters, digits and underscores only','Використовуйте тільки латинські букви, цифри та нижнє підкреслювання',3,'crm')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Use regex to extract cell value','Використовувати регулярний вираз щоб витягти значення',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'User','Користувач',3,'crm')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'User','Користувач',3,'geomarketing')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'User...','Користувач ...',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'User','Користувач ...',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'User {{name}} already has the plan in this period.','Користувач {{name}} вже має план на цей період.',3,'crm')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'User {{name}} has no plan in this period.','Користувач {{name}} не має плану на цей період.',3,'crm')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'NULL','Користувач створений',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'User credentials','Дані для входу',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'User doesn''t work.','Користувач не працює.',3,'crm')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'User list','Список користувачів',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'User management','Керування користувачами',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'NULL','Користувач не вибраний',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'NULL','посади користувачів',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'NULL','Користувач оновлений',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Users','Користувачі',3,'geomarketing')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Users and polygons','Користувачі та полігони',3,'geomarketing')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Users list','Список користувачів',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Vacancy','вакансія',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Vacation','Відпустка',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Value','Обсяг',3,'geomarketing')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Value already used by %user%','Це значення вже використовується користувачем %user%',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'NULL','Значение должно быть числовым',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'NULL','Значение не з списка',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Value should be more then 0 and should be integer','значення повинно бути більше 0 і має бути цілим числом',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'NULL','Значення оновлено',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'NULL','Значення включених налаштувань',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'NULL','текст',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Variable','Значення',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Variables not found','Поля відсутні',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Verification','Верифікація',3,'crm')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Verified','Підтверджено',3,'crm')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Verified','Підтверджено',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Viber','Viber',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Viber Distribution script','Сценарій Viber розсилки',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Viber subject','Підпис',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Viber Template','Шаблон Viber',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Viber title','Назва',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'View action','Перегляд задачі',3,'crm')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'View as','Вид',3,'crm')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'View task','Перегляд візиту',3,'crm')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'NULL','Видимість',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Visible fields','видимі поля',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Visited','Відвідано',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Visits','Візити',3,'geomarketing')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Visits range','Період візитів',3,'geomarketing')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Volume of MPI financing','Обсяг фінансування МНН',3,'crm')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Expired','Прострочено',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Site','Сайт',3,'change_requests')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Waiting','Чекає',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Waiting response from server','Чекаємо відповіді сервера ...',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Warning','Увага',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Watcher already added','Спостерігача уже додано',3,'crm')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Watchers','Спостерігачі',3,'crm')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'NULL','Веб сервіси',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Week','тиждень',3,'crm')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Welcome to Administration Panel','Ласкаво просимо до Панелі адміністрації',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'NULL','Привіт у PharmaHRM',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Whats inside','Що на фото',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'NULL','Без статусу',3,'crm')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Search on territory','Пошук на території',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'NULL','К-сть робочих днів',3,'crm')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Working days','робочі дні',3,'crm')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Workplace','Дод. місце роботи',3,'change_requests')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Workplace (position)','Дод. місце роботи (посада)',3,'change_requests')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'NULL','р.',3,'crm')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Year','Рік',3,'geomarketing')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Year','Рік',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Yes','Так',3,'geomarketing')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'NULL','Так',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'messages','Так',3,'close')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'messages','Так',3,'log out')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'You can`t copy to previous date','Ви не можете копіювати на минулу дату',3,'crm')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'You have already sent a request for archiving. You can send the next request for archiving after','Ви вже надіслали запит на архівування. Наступний запит на архівування можна надіслати після',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'You have filled agreements incorrectly','Ви неправильно заповнили домовленість',3,'crm')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'You have filled brands incorrectly','Ви неправильно заповнили бренди',3,'crm')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'There are no employees','працівників немає',3,'crm')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'This date should be greater than parent date','Ця дата повинна бути більшою від батьківської дати',3,'validators')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'This synonym is already binded. Rebinding this synonym will unbind it from current company. Therefore altering resulting data','Обраний синонім вже пов''язаний класифікатором. Пов''язуючи цей синонім ви відв''яже його від прив''язаної установи. Зміна прив''язки призведе до змін у результуючих даних',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'NULL','Значення не може бути порожнім.',3,'validators')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'This value should not be blank.','Значення не повинно бути порожнім.',3,'validators')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'This value should not be null.','Значення не повинно бути нульовим.',3,'crm')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Time','Час',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'In time between %timeStart% and %timeEnd%','"За час між %timeStart% і %timeEnd%"',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Time end...','Час закінчення...',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Time start...','Час початку ...',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Title','Заголовок',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'to','по',3,'geomarketing')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'To','До',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'NULL','Для зміни пароля натисніть кнопку "Відновити". Якщо Ви не відправляли запит ігноруйте цей лист',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'NULL','Для редагування інформації вийдіть з режиму копіювання!',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'today','сьогодні',3,'crm')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Turnover','million',3,'geomarketing')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Tool panel','Налаштування стовпчиків',3,'geomarketing')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Turnover per quarter','Поквартальный товарооборот',3,'geomarketing')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Turnover per month','Товарообіг за місяць',3,'geomarketing')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Top bricks','Top bricks',3,'geomarketing')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Top pharmacies','Потенційні аптеки',3,'geomarketing')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Total','Всього',3,'geomarketing')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Total','Всього',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Total coefficient','Загальний коефіцієнт',3,'geomarketing')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Total contacts in the distribution','Всього контактів в розсилці',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Total mark','Загальна оцінка',3,'crm')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Total volume of MPI financing','Загальний обсяг фінансування ЛПУ',3,'crm')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Trade name','Торгова назва',3,'crm')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Training','Тренінг',3,'crm')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Trigger','Тригер',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Trigger date','Період дії тригера',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Trigger name','Ім''я тригера',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Trigger type','Тип тригера',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Triggers','Тригери',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Triggers not found','Тригери не знайдено',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Turnover','Товарообіг',3,'geomarketing')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Turnover','Товарообіг',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Turnover range','Товарообіг',3,'geomarketing')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Type','Тип',3,'crm')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Type','Тип',3,'geomarketing')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Type','Тип',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'NULL','Тип зміни',3,'crm')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Type of request','Тип запиту',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Type your message','Введіть своє повідомлення',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Type comment...','Введіть коментар ...',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Types','Типи',3,'geomarketing')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Ukraine','Україна',3,'crm')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Unable to get status','Неможливо отримати статус',3,'geomarketing')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Unable to start balancing','Неможливо почати врівноваження',3,'geomarketing')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Unbind','Від’єднати',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Uncategorized','Без категорії',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'uncheck all','Зняти всі',3,'geomarketing')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Undelivered','Не доставлено',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Unfold','Розгорнути',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Unidentified error','Невстановлена ​​помилка',3,'crm')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'NULL','Ім''я об''єкта',3,'crm')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Unit price','Одинична ціна',3,'crm')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Unprocessed','Необроблені',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Unresolved synonyms found','Знайдено необроблені синоніми',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Unsubscribe','Відписатись',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Unsubscribe from distribution','Відписатися від розсилки',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Unsubscribe reason','Причина відписки',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Unsubscribed','Відписався',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Unsubscribed','Відписано',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Unsupported format','Непідтримуваний формат',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Until','До',3,'crm')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Untitled','Без назви',3,'geomarketing')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Unverified','Непідтверджений',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Update','Оновити',3,'crm')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Update','Оновити',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Update list','Оновити список',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Updating analytics...','Оновлення аналітики ...',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Updating packages from table [&table]...','Оновлення кількості продажів з тимчасової таблиці [&table] ...',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Updating packages...','Оновлення кількості продажів ...',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Updating synonym packages...','Перерахунок синонімів ...',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Updating synonyms...','Оновлення синонімів ...',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Upload','Завантажити',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Upload date','Дата завантаження',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'NULL','Виберіть файл',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Upload reports','Завантажити звіти',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Uploaded file is too large. Server supports upload of files up to %size%. Try to split it into multiple chunks.','Завантажений файл занадто великий. Сервер підтримує завантаження файлів до %size%. Спробуйте розділити його на кілька фрагментів.',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Recalculate','Перерахувати',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Recipient','Одержувач',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Recipient in format','Одержувач у форматі',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Recipient name','ім''я одержувача',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Recipients','Список одержувачів',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Recommended route','Рекомендований маршрут',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Reference address','Еталон адреса',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Reference company','Еталон установа',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Refresh','Оновити',3,'geomarketing')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'RegEx','Регулярні вирази',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Region','Область',3,'change_requests')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Region','Область',3,'crm')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Region','Область',3,'geomarketing')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Region','Регіон',3,'geomarketing')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Region','Область',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Region...','Область',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Region','Область...',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Region...','Область...',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Region','Регіон',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Region...','Регіон',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Region','Регіон',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Region/City','Регіон / Місто',3,'crm')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Regional manager','Регіональний менеджер',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Regional Representative','Регіональний представник',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Regions','Області',3,'geomarketing')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Regions','Регіони',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Regions filled','Областей заповнено',3,'geomarketing')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'NULL','Відхилити',3,'crm')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Reject','Відхилити',3,'geomarketing')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Reject','Відхилити',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Reject granual plan','Відхилити грануальний план',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Rejected','Відхилено',3,'crm')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Rejected','Відхилений',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Release form dosage','Форма випуску дозування',3,'crm')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Release form dosage units','Форма випуску дозування одиниці виміру',3,'crm')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'NULL','Нагадати',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Remind password','Нагадати пароль',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Remove pharmacy','Видалити аптеку',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'NULL','Видалено успішно',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Report','Звіт',3,'crm')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Report','Звіт',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Report data isn''t found','дані звіту не знайдено',3,'crm')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'NULL','Звіт не найден',3,'crm')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Report sales','Звіт про продаж',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Reports','Звіти',3,'crm')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Reports','Звіти',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Reports','звітів',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Request initiator','Ініціатор запиту',3,'crm')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Requests','Запити',3,'crm')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Required','обов''язкове',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'NULL','Обов''язкове поле не може бути приховано',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'NULL','Обов',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Requires re-verification','Потрібна повторна перевірка',3,'crm')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Resend with SMS','Переслати повторно за допомогою SMS',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Resending sms - delivered','Повторне надсилання sms - доставлено',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Resending sms - sent','Повторне надсилання sms - надіслано',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Resending sms - visited','Повторне надсилання sms - відвідано',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Reset','Скинути',3,'geomarketing')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Reset','Скинути',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Reset all','Скинути все',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'NULL','Відновити пароль',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'NULL','Скинути обрану категорію',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Responsibility Coefficient','коефіцієнт відповідальності',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Responsible','Відповідальний',3,'crm')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Responsible','Відповідальний',3,'geomarketing')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'NULL','Керівник ...',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Responsible already added','Відповідальний вже доданий',3,'crm')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'NULL','Відповідальний за виконання',3,'crm')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Responsible PM','Відповідальний PM',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Responsible was successfully saved','Відповідальний був успішно збережений',3,'geomarketing')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'NULL','Результат',3,'crm')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Role','роль',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'NULL','Маршрути',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'NULL','Запис успішно видалена',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Rows where this column is empty will be ignored','Записи з порожнім значенням в цьому ствопчику будуть ігноруватися',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'NULL','Російська',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'RUB','RUB',3,'crm')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Ruler','Лінійка',3,'geomarketing')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Running','Розпочато',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Russia','Росія',3,'crm')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'NULL','Російська',3,'crm')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'NULL','План цикла',3,'crm')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Sales','Продажі',3,'geomarketing')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Sales (month) uah','Продажі (міс.) Грн',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Sales amount','Сума продажів',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Sales per year','Товарообіг за рік',3,'geomarketing')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Sales period','Період продажів',3,'geomarketing')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Sales plan','План продажів',3,'geomarketing')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Sales report','Звіт з продажу',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Sales report','Звіт з продажу',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Save','Зберегти',3,'crm')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Save','Зберегти',3,'geomarketing')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Save','Зберегти',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Save changes','Зберегти',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Save granual plan','Зберегти грануальний план',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Save pattern','Зберегти шаблон',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Save photos','Зберегти фотографії',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Save report','Зберегти звіт',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Send','Надіслати',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Saved!','Збережено!',3,'geomarketing')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'NULL','Збережено успішно',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Comparison per:','"Порівняння по:"',3,'geomarketing')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Competitive group','Конкурентні групи',3,'geomarketing')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Completed','Виконано',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Create company','Створити установу',3,'crm')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Create company','Створити установу',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Deactivate company','Відключити установу',3,'crm')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Edit company','Редагування установи',3,'crm')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Edit company request','Запит на редагування установи',3,'crm')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Final completed task coordinate','Остання точка закриття візиту',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Find companies in region','Пошук установи по регіону',3,'geomarketing')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Find company','Пошук установи',3,'geomarketing')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'First completed task coordinate','Перша точка закриття візиту',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Handling unprocessed company synonyms...','Обробка установ без синонімів ...',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Loading companies...','Завантаження установ ...',3,'geomarketing')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Concentration of population','Концентрація населення',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Conduction first visit','Проведення першого візиту',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Conduction the first visit','Перший візит',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Conduction visit','МП здійснив візит',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Config grid column','Налаштувати стовпчики',3,'geomarketing')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Config group grid column','Налаштувати групи стовпчики',3,'geomarketing')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Confirm','Підтвердіть',3,'geomarketing')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Confirm','Підтвердити',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Confirm deletion','Підтвердіть видалення',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Confirmation','Підтвердження',3,'crm')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Contact category','Категорія фахівця',3,'geomarketing')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'NULL','Звернутися в helpdesk',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Contact owner','Відповідальний за клієнта',3,'crm')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Contact status','Статус клієнта',3,'crm')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Contact Type','Тип клієнта',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'NULL','Верифікація клієнтів',3,'crm')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'NULL','Відповідальність',3,'crm')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Contacts per category','Фахівців за категоріями',3,'geomarketing')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Contacts','Фахівців',3,'geomarketing')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Contacts','Клієнти',3,'crm')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Contacts','Співробітники',3,'geomarketing')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Contacts','клієнти',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Contacts concentration','Концентрація фахівців',3,'geomarketing')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Prescriber pharmacy','Прескрайбер-аптека',3,'change_requests')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Contacts per specialization','Фахівців за спеціалізаціями',3,'geomarketing')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Contacts per target groups','Фахівців за таргет-групами',3,'geomarketing')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Contact type','Тип клієнта',3,'change_requests')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Contacts','Фахівців',3,'geomarketing')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Contains','Містить',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Content','Контент',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Content between "starts with" and "ends with" will be extracted','Вміст комірки між "після" і "до" буде вилучено',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Content has been successfully archived','Контент заархівований',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Content not found','Контент відсутній',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Content was not archived','Контент не заархівований',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Continue importing','Продовжити імпорт',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Contract date','Дата укладення контракту',3,'crm')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Contract number','Номер контракту',3,'crm')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Contract price','Ціна контракту',3,'crm')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Contract status','Статус контракту',3,'crm')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Contracts','Контракти',3,'crm')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Control','Контрольний',3,'crm')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'NULL','Контроль',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'NULL','Налаштування',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'NULL','Налаштування контролю',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Converting','Конвертація',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Coordinate ontained successfully','Координата знята успішно',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Coordinate obtainment','Отримання координат',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Coordinate obtainment accuracy','Точність зняття координати (м)',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Coordinates','Координати',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Copy','Копіювати',3,'crm')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Copy from another task','Копіювати з іншого візиту',3,'crm')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Copy plan','Копіювати план',3,'crm')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Count','К-сть.',3,'geomarketing')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Count','Кількість',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Country','Країна',3,'crm')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Create','Створити',3,'crm')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'NULL','Створити',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Create additional fields','Створити додаткове поле',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Main company','Мережа',3,'crm')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Midway completed task coordinate','Проміжні точки закриття візиту',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Create contact','Створити клієнта',3,'crm')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Create Email','Створити Email',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Create field','Створити поле',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'NULL','Створити нового',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Create polygon','створення полігону',3,'geomarketing')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Create template','Створити шаблон',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Created','Створена',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Creator','Автор',3,'crm')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Creator','Автор',3,'geomarketing')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Cur.month','Пот. місяць',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Current','Поточна',3,'geomarketing')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Current month','Поточний місяць',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'current user','Поточний користувач',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'NULL','Поточне значення',3,'crm')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'NULL','Довідник',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Cyrillic','Кирилиця',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'D','D',3,'crm')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Data processing','Обробка даних',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'NULL','Тип даних',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Additional specialization','Додаткова спеціалізація',3,'change_requests')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Related company','Пов''язана установа',3,'change_requests')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Additional resposible','Доп. відповідальний',3,'change_requests')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Additional specialization','Доп. спеціалізація',3,'change_requests')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Address','Адреса',3,'crm')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Address','Адреса',3,'geomarketing')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Address','Адреса',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'NULL','Панель адміністратора',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Administration Panel','Панель адміністрації',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Administrator (full access)','Адміністратор (повний доступ)',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Adress','Адреса',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Advanced mode','Розширений режим',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Age','Вік',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Agreement','Домовленість',3,'crm')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'NULL','Домовленість',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'NULL','Налаштування домовленості',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Can''t get company','Не вдалося завантажити дані про установу',3,'crm')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Filters are empty. Please specify at least one filter','Фільтри не вказані. Будь ласка вкажіть хоча б один параметр',3,'geomarketing')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'No companies for [[userName]]','Немає установ в даному полігоні для [[userName]]',3,'geomarketing')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'No polygons in selection','У вибірці немає полігонів',3,'geomarketing')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Polygons are missing','Полігони відсутні',3,'geomarketing')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Invalid polygon!<br>Polygons with self-intersection unacceptable.','Неправильний полігон! <br> Полігони з самоперетином неприпустимі.',3,'geomarketing')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Polygon must have at least three vertices','Полігон повинен мати не менше трьох вершин',3,'geomarketing')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'All','Все',3,'crm')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'All','Все',3,'geomarketing')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'All','Все',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'All data','Всі дані',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'NULL','Усі дані були скопіровані та збережені',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'NULL','Вся база',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'All show','Показати все',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'NULL','Всі типи',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'NULL','Дозволене введення',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Alternative IMEI','Альтернативний IMEI',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Amount','Кількість',3,'geomarketing')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Analytics','Аналітика',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'NULL','І',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Android','Android',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Answer','Відповідь',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Answer date','Дата відповіді',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Appeal Type','Тип звернення',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Applied filters','Застосовуються фільтри',3,'geomarketing')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Apply','Застосувати',3,'crm')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Apply','Застосувати',3,'geomarketing')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Apply','Застосувати',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Apply Filter','Застосувати',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Approval denied','Не затверджений',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'NULL','Затвердити',3,'crm')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Approve','Затвердити',3,'geomarketing')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Approve','Затвердити',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Approve granual plan','Затвердити графічний план',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'NULL','Затверджено',3,'crm')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'approved','Затверджено',3,'geomarketing')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'approved','Підтверджений',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'approved','Затверджений',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Approved list','Затвердили',3,'geomarketing')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Approximate number of messages','Приблизна кількість sms повідомлень',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'April','Квітень',3,'crm')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'NULL','Квітень',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Archive','Архів',3,'crm')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Archive is not readable','Архів пошкоджений',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Archive reason','Причина архівації',3,'crm')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Archived','Архівувати',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Archive reason','Причина архівації',3,'change_requests')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Are you sure that you want to delete the reports','Ви впевнені, що хочете видалити звіти',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Are you sure that you want to delete the template?','Ви впевнені, що хочете видалити шаблон?',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Are you sure you want to approve sales plan?','Ви впевнені, що хочете затвердити план продажів?',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'NULL','Ви впевнені що хочете закрити вікно без збереження змін?',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Are you sure you want to confirm this granual plan?','Ви впевнені що хочете затвердити цей грануальний план?',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Are you sure you want to delete this field','Ви впевнені що хочете видалити це поле?',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'NULL','Ви впевнені що хочете видалити це поле?',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'NULL','Ви впевнені що хочете видалити цей запис?',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'NULL','Ви впевнені, що хочете вийти?',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Are you sure you want to reject sales plan?','Ви впевнені, що хочете відхилити план продажу?',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Are you sure you want to reject this granual plan?','Ви впевнені що хочете відхилити цей грануальний план?',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Are you sure you want to remove this pharmacy?','Ви впевнені що хочете видалити цю аптеку?',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Are you sure you want to save current granual plan?','Ви впевнені що хочете зберегти поточний грануальний план?',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'NULL','Співробітник ...',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Attach file','Прикріпити файл',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Attachments','Вкладення',3,'crm')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'August','Серпень',3,'crm')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'NULL','Серпень',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Auto update','Автоматичне оновлення',3,'geomarketing')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'NULL','Дані успішно оброблені',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Date','Дата',3,'crm')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Date','Дата',3,'geomarketing')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Date','Дата',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Date and time','Дата та час',3,'crm')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Date correction','Виправлення дати',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Date finish testing','Дата закінчення тестування',3,'crm')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Date from','Дата з',3,'crm')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'NULL','Дата зміни',3,'crm')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'NULL','Дата виконання',3,'crm')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Date of placement of the purchase','Дата розміщення закупівлі',3,'crm')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Date start testing','Дата початку тестування',3,'crm')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Date till','Дата закінчення',3,'crm')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'NULL','Дата (1)',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'NULL','Дата (2)',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'NULL','Дата (3)',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Date end','Дата закінчення',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Date start','Дата початку',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'NULL','дата',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Day','день',3,'crm')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Deactivate client','відключити клієнта',3,'crm')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'New company','Нова установа',3,'crm')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'December','Грудень',3,'crm')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'NULL','Грудень',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Default Value','Значення за замовчуванням',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Not delivered','Не доставлено',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Delayed','Відкладено',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Delete','Видалити',3,'geomarketing')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Delete','Видалити',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Delete','Видалити',3,'crm')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Delete action?','Видалити завдання?',3,'crm')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Delete file','Видалити файл',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'NULL','Видалити обраний план?',3,'crm')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'NULL','Видалити вибрані деталі плану?',3,'crm')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'NULL','Видалити вибраний бренд?',3,'crm')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'NULL','Видалити вибрані бренди?',3,'crm')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'NULL','Видалити презентацію',3,'crm')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'NULL','Видалити презентації',3,'crm')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'NULL','Видалити промо матеріал',3,'crm')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'NULL','Видалити промо матеріали',3,'crm')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'NULL','Видалити вибрані деталі плану?',3,'crm')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'NULL','Видалити вибраний препарат?',3,'crm')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'NULL','Видалити вибрані препарати?',3,'crm')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'NULL','Видалити вибрані плани?',3,'crm')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'NULL','Видалити запис',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Delete selected plan detail?','Видалити вибрану деталь плану?',3,'crm')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Delete selected plan details preparation?','Видалити вибрану детальну інформацію про план?',3,'crm')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Delete selected plan details preparations?','Видалити вибрану підготовку деталей плану?',3,'crm')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Delete selected plan details?','Видалити вибрані дані плану?',3,'crm')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Delete selected plan getails presentation?','Видалити презентацію вибраних планів для плану?',3,'crm')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Delete selected plan getails presentations?','Видалити вибрані презентації із плану посилань?',3,'crm')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Delete selected plan getails promo material?','Видалити вибраний рекламний матеріал із плану вибраних подій?',3,'crm')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Delete selected plan getails promo materials?','Видалити вибрані рекламні матеріали з плану?',3,'crm')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Delete selected plan?','Видалити вибраний план?',3,'crm')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Delete selected plans?','Видалити вибрані плани?',3,'crm')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Delete selected preparation?','Видалити вибраний препарат?',3,'crm')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Delete selected preparations?','Видалити вибрані препарати?',3,'crm')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'NULL','Видалити візит',3,'crm')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Delete task?','Видалити візит?',3,'crm')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Delete untreated','Видалити необроблені',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( '"Unable to delete selected rows: there are related records"','Неможливо видалити запис: існують пов''язані записи',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Deleting...','Видалення ...',3,'geomarketing')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Are you sure you want to delete this item?','Ви впевнені що хочете видалити цю презентацію?',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Delivered','Розіслано',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Delivered','Доставлено',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Delivery schedule','Графік поставки',3,'crm')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Delta','Розбіжність',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Department','Відділ',3,'crm')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Description','Опис',3,'change_requests')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Description','Опис',3,'crm')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Description','Опис',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'NULL','Зняти все',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Details','Зміст',3,'crm')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Details','Деталі звернення',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'NULL','Карта розвитку',3,'crm')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Dictionaries','Довідники',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'NULL','Довідник',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Direction','Продуктовий напрямок',3,'geomarketing')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'NULL','Прод. напрямок',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Directions','напрямки',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Directory is not configured for current user.','Для вас не налаштована папка для завантаження CLM',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'NULL','Заборона редагування',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'disapproved','відхилений',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( '100 records: "Display limit: 100 records"','100 записів: "Ліміт відображення: 100 записів"',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Distance from','Дистанція від',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Distribution','Розсилки',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Distribution %name%','Розсилка %name%',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Distribution can not be changed after start sending','Розсилка не може бути змінена після початку відправки повідомлень',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Distribution date and time','Дата та час розсилки',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Distribution is creating please wait','Розсилка створюється будь ласка почекайте',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Automatic generation polygon','Автоматичне генерування полігону',3,'geomarketing')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Autoplan','Авто-план',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Available','Доступно',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Available directions','Доступні вказівки',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Available fields','Наявні поля',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Available regions','Доступні регіони',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'NULL','Доступні настройки',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Avg prescription (uah)','Середнє призначення (грн)',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Awaiting verification','Чекаємо підтвердження',3,'crm')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Back','Назад',3,'crm')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Back','Назад',3,'geomarketing')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Back','Повернутися',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Back','Назад',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Back to editor','Повернутися до редактора',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Back to previous','Повернутися на попередній крок',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Bad credentials.','Невірні облікові дані.',3,'crm')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Balance','Врівноважити',3,'geomarketing')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Balancing','Врівноваження',3,'geomarketing')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Balancing error code','Код помилки врівноваження',3,'geomarketing')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Balancing result','Є результат врівноваження',3,'geomarketing')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'QlikView','QlikView',3,'crm')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'QlikView','QlikView',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Bind','Зв''язати',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Birth date','Дата народження',3,'crm')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Birth Date','Дата народження',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'NULL','Дата рождения',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Birthday','Дата народження',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Body','Контент',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Not delivered','Не доставлено',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Brand','Бренд',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Brand name','Бренд',3,'crm')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Brand','Бренд',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Brands','Бренди',3,'crm')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Browse','Огляд',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Brush','Пензлик',3,'geomarketing')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Budget','Бюджет',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Building','Будинок',3,'change_requests')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Building','Будинок',3,'crm')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Bulk editing','Масове редагування',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Bussines region','Бізнес регіон',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Button options','Параметри кнопки',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Button URL','Посилання в кнопці',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Calendar','Календар',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Call №','Візит №',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Campaign name','Назва кампанії',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Campaign was successfully saved','Кампанію успішно збережено',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Campaigns','Кампанії',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Can not edit','Редагування недоступно',3,'geomarketing')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Can''t get action','Не вдалося завантажити завдання',3,'crm')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Can''t get client','Не вдалося завантажити картку клієнта',3,'crm')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Category','Категорія',3,'change_requests')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Can''t get plan','Неможливо отримати план',3,'crm')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Can''t get task','Не вдалося завантажити візит',3,'crm')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Cancel','Скасувати',3,'crm')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Cancel','Скасувати',3,'crm')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Cancel','Скасувати',3,'geomarketing')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Cancel','Скасувати',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Button caption','Текст кнопки',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Car','Автомобіль',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Card','Картка',3,'crm')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Card Number','Номер картки',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Cards configuration','Конфігурація карт',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Categories','Категорії',3,'geomarketing')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Category','Категорія',3,'change_requests')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Category','Категорія',3,'crm')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Category','Категорія',3,'geomarketing')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Category','Категорія',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Distribution is deleting please wait','Розсилка видаляється ласка зачекайте',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Distribution name','Назва розсилки',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Distribution report','Звіт по розсилці',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Distribution statistics','Статистика розсилки',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Distribution statistics not available','Статистика розсилки недоступна',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Distribution Type','Тип розсилки',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Distributions not found','Розсилки відсутні',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Distributor','Дистриб''ютор',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Distributor','Дистриб''ютор',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Distributors','Дистриб''ютор',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'District','Округ',3,'additional_fields')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'District','Округ',3,'change_requests')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'District','Область',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'District','Район',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Doctor','ПІБ лікаря',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Doctors','Лікарі',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Done','Оброблені',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Done','Оброблені',3,'status')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Download','Завантажити',3,'crm')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Download','Завантажити',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'NULL','Завантажте шаблон для заповнення даних',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'NULL','Завантажити шаблон',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'NULL','Завантажити необроблені дані',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Drag and drop files here or click to upload','Перетягніть сюди файли або натисніть для завантаження',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Drag and drop files into the workspace to load','Перетягніть файли в робочу область для завантаження',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'NULL','Скинути',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Drop file or click to upload.','Перетягніть файл або натисніть щоб завантажити.',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'NULL','Перетягніть Excel файл або',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'NULL','Перетягніть файл сюди',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Drug','Препарат',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Drug form','Лікарська форма',3,'crm')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Drugstore','Аптека',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Duplicate','Дублікат',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Duplicate columns','Повторюються назви стовпчиків',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'E-mail','Електронна пошта',3,'crm')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'E-mail','Електронна пошта',3,'change_requests')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Edit','Редагувати',3,'geomarketing')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Edit','Редагувати',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Edit account','Редагувати установу',3,'crm')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Edit account request','Редагувати запит установи',3,'crm')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Edit action','Редагувати завдання',3,'crm')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Edit brand','Редагувати бренд',3,'crm')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Edit campaign','редагування кампанії',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Edit client','редагування клієнта',3,'crm')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Edit client request','Запит на редагування клієнта',3,'crm')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'New company request','Запит на додавання установи',3,'crm')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'No companies','Немає установ',3,'geomarketing')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Edit contact','Редагувати клієнта',3,'crm')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Edit contact request','Редагувати запит на клієнта',3,'crm')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'NULL','Редагувати довідник',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Edit Email','Редагувати Email',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Edit group','Редагувати групу',3,'geomarketing')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Edit HTML','Редагувати HTML',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Edit plan','Редагувати план',3,'crm')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Edit plan detail','Редагувати деталі плану',3,'crm')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Edit plan preparation','Редагувати бренд',3,'crm')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Edit plan presentation','Редагувати презентацію плану',3,'crm')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'NULL','Редагувати промо матеріал',3,'crm')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Edit plan promo material','Редагувати промо-матеріал плану',3,'crm')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Edit preparation','Редагувати препарат',3,'crm')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Edit profile','Редагувати профіль',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Edit promo material','Редагувати промо-матеріал',3,'crm')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'NULL','Редагувати запис',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Edit SMS Template','Редагувати шаблон SMS',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Edit specialization','Редагувати спеціалізацію',3,'crm')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Edit Target Audience','Редагувати ЦА',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Edit Task','Редагувати візит',3,'crm')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Edit user','Редагувати користувача',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'NULL','Редагувати значення',3,'crm')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Edit workplace','Редагувати робоче місце',3,'crm')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Email','Email',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Email %email% was successfully added to database','Електронна адреса %email% був доданий в базу розсилки',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Email %email% was successfully removed from database','Електронна адреса %email% був видалений з бази розсилки',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Email body','Контент',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Email Distribution script','Скрипт розповсюдження електронної пошти',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'NULL','Адреса електронної пошти не зареєстрована',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Email subject','Тема',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Email Template','Email Шаблон',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Email title','Назва',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Employee','Співробітник',3,'crm')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Employees','Співробітник',3,'crm')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'NULL','Немає брендів',3,'crm')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Empty list','Порожній список',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Empty presentations','Немає презентацій',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'NULL','Немає промо матеріалів',3,'crm')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Empty Recipient','Порожній отримувач',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'There are no comments','Немає коментарів',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'There are no details','Деталей немає',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'No data','Порожній список',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Import','Перейти до імпорту',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Import','Імпорт',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Import file','Імпортувати',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'NULL','Імпортувати з файлу',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Importance','Важливість',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Importing','Імпорт',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Importing file...','Імпорт файлів',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Importing files','імпортування файлів',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Importing other synonyms...','Імпорт решти синонімів ...',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Importing synonyms...','Імпортування синонімів ...',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'In progress','Завантаження',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'In progress','У прогресі',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'In progress','Завантаження',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'In progress','Взяті в обробку',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'In progress','Взяті в обробку',3,'status')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Income','Вхідні',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Income','Вхідні',3,'status')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Incomplete month','Неповний місяць',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'NULL','Некоректний формат файлу',3,'validators')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'NULL','№ п / п',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Info','Інфо',3,'geomarketing')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Inhabited locality','Населений пункт',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Inherit','Спадщина',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Inheritance','Спадкування',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'INN','МНН',3,'crm')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'INN not specified','МНН не вказано',3,'crm')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'INN Survey','МНН Анкета',3,'crm')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'INN Surveys','МНН Анкети',3,'crm')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'In progress','Взяті в обробку',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'NULL','Некоректне введення',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Input name','Введіть назву презентації',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Paste raw HTML','Вставити HTML код',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Inserting data','Додавання даних',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'NULL','число',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'NULL','Ціле число (1)',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'NULL','Ціле число (2)',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'NULL','Ціле число (2)',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Invalid column name','Невалідна назва стовпчика',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'NULL','Знайдено неправильні значення комірки будуть проігноровані',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Invalid date','Неправильна дата',3,'validators')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'NULL','Некоректна адреса електронної пошти',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Invalid page name','Невалідна назва листа',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'NULL','Неправильний тип даних клітинка буде проігнорована',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Invalid username and password combination','Невірна комбінація імені користувача та пароля',3,'crm')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'NULL','Некоректне значення',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Invalid Email','Некоректний Email',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'iOS','iOS',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Is active','Активний',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Is main','Головна установа',3,'crm')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Is primary','Головна установа',3,'crm')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Is required','Обов''язкове',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Processing','В процесі',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Is main','Головна установа',3,'change_requests')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'January','Січень',3,'crm')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'NULL','Січень',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'NULL','Дата закриття візиту',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'NULL','Для відображення маршруту необхідно включити опцію визначення місцеположення в налаштуваннях пристрою і браузера',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'NULL','Для відображення маршруту необхідно включити опцію визначення місцеположення в налаштуваннях пристрою і браузера',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'NULL','Час очікування минув.',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'NULL','Невідома помилка',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'July','Липень',3,'crm')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'NULL','Липень',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'June','Червень',3,'crm')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'NULL','Червень',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Kazakhstan','Казахстан',3,'crm')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'NULL','Залишатися в системі',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'NULL','Мова',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Last available quarter','Останній доступний квартал',3,'geomarketing')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Last five not mapped IDs','Останні 5 не знайдених айди',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Last name','Прізвище',3,'crm')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Last name','Прізвище',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Last quarter','Останній квартал',3,'geomarketing')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Last task date','Дата останнього завдання',3,'crm')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Last task date','Дата останнього завдання',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Last tracker coordinate','Остання координата трекера',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Lastname','Прізвище',3,'change_requests')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Latin','Латинська',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Layers','Шари',3,'geomarketing')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'NULL','Легенда карти',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Less than','Менше',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Line','Лінія',3,'crm')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Link','Посилання',3,'crm')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'NULL','Посилання на План-графік закупівель ЛПУ',3,'crm')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Link to schedule of MPI procurementLink to schedule of MPI procurement','Посилання на графік закупівель MPIПосилання на графік закупівель MPI',3,'crm')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'List','Список',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Load more...','Завантажити більше ...',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Show more','Завантажити наступні',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Loading...','Завантаження ...',3,'geomarketing')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Loading','Завантаження ...',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Loading...','Завантаження ...',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Loading','Завантаження ...',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Loading...','Завантаження ...',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Loading','Завантаження',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Loading...','Завантаження',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Opening link in email','Перехід за посиланням в листі',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'NULL','Налаштування вимкнено',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'NULL','Налаштування увімкнено',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'or','АБО',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Organization','Організація',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Organization type','Тип організації',3,'crm')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Other','Інше',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Out of the map','Немає на карті',3,'geomarketing')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Outside of polygon','Поза полігоном',3,'geomarketing')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Resposible','Відповідальний',3,'change_requests')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Owner','Відповідальний',3,'crm')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Owners','Виконавці',3,'crm')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Pack price','Ціна за упаковку',3,'crm')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Packages','Упаковки',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Page not found','Сторінку не знайдено',3,'crm')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Paint','Малювати',3,'geomarketing')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Painting options','Параметри малювання',3,'geomarketing')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Parent distribution','Батьківська розсилка',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Participant already added','Учасник вже доданий',3,'crm')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Participants','Учасники',3,'crm')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Participants','Учасники',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Password','Пароль',3,'crm')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Password','Пароль',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Password can''t be blank.','Пароль не може бути порожнім.',3,'crm')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'NULL','Відновлення пароля',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Pattern','Шаблон',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Pattern has associated records!','Шаблон має пов’язані записи!',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Pattern Name','Назва шаблону',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Pattern not found','Шаблон не знайдено',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Pattern successfully saved','Шаблон успішно збережений',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Pattern type','Тип шаблону',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Patterns','Шаблони',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'pcs','уп',3,'geomarketing')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Pending','Чекає',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Pending approval','Вимагає затвердження',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Pending for approval','Очікує затвердження',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Penetration','Пенетрація',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Per polygons','По полігонам',3,'geomarketing')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Per users','По користувачам',3,'geomarketing')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'NULL','% виконання',3,'crm')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Percent','Відсоток',3,'crm')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Percentage','Відсоток',3,'crm')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Period','Період',3,'crm')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Period','Період',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Period type','Тип періоду',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Personalization Key','Ключ персоналізації',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Personalization Key must contain only 0-9 A-Z and .','Поле Ключ персоналізації може містити тільки 0-9 A-Z і точку',3,'validators')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Pharamacy coverage','Покриття аптек',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Pharm agent','Фарм представник',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Pharmacies','Аптеки',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Pharmacies not found','Аптеки не знайдено',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Pharmacy','Аптека',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Pharmacy','Аптека',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Pharmacy coverage','Аптечне покриття',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Pharmacy networks','Аптечні мережі',3,'geomarketing')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Phone','Телефон',3,'crm')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Phone','Телефон',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Phone number','Телефон',3,'change_requests')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Additional phone number','Дод. телефон',3,'change_requests')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Photo report','Фото викладки',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'State','Статус',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Type of photo','Тип фотографії',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Type of photo','Тип фотографії',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( '[users] places in the same polygon: "Pins for users: [users] placed within the same polygon"','[users] places in the same polygon: "Піни для користувачів: [users] розміщені в одному полігоні"',3,'geomarketing')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Pivot mode','Режим груповання',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'pkg','уп',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Place of work','Місце роботи',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Plan','План',3,'crm')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Plan','План',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Plan all','Всього план',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Plan count','Планова кількість',3,'crm')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Plan cycle','План циклу',3,'crm')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Plan date should be greater than current date','Планова дата розсилки повинна бути пізніше поточної дати',3,'validators')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Plan detail preparation was deleted.','препарат видалено з плану.',3,'crm')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Plan detail preparation: {{message}}','План препарату: {{message}}',3,'crm')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Plan detail presentation was deleted.','Презентація видалена',3,'crm')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'NULL','Презентація {{message}}',3,'crm')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Plan detail promo material was deleted.','Матеріал видалений',3,'crm')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'NULL','Матеріал {{message}}',3,'crm')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Plan detail was deleted.','Деталі плану були видалені.',3,'crm')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Plan detail: {{message}}','Деталі плану: {{message}}',3,'crm')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Plan fact','План факт',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'NULL','План не знайдений',3,'crm')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Plan model','модель плану',3,'crm')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Plan not found','План не знайдено',3,'crm')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Plan office','План офіс',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Plan office','План офісу',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Plan preparation was deleted.','Препарат було видалено.',3,'crm')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Plan preparation: {{message}}','Препарат: {{message}}',3,'crm')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Plan recommended route','План. маршрут рекомендований',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Plan route','План. маршрут',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Plan total','Загальний план',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Plan was deleted.','План було видалено.',3,'crm')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'NULL','Модель плану',3,'crm')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Plan: {{message}}','План: {{message}}',3,'crm')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Planned','Заплановано',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Planned task route','Запланований маршрут візитів',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'NULL','Запл. маршрут',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'NULL','Доступно в діапазоні дня',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Planning','Планування',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Planning acinus','Планування на ацинуси',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Planning brands','Планування Бренд-ЛПУ',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Planning companies','Планування ЛПУ-Бренд',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Planning error','Помилка при плануванні',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Planning for acinus','Планування на ацинуси',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Planning successful','Планування успішне',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'NULL','Планування на ацинуси',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Platform','платформа',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Platform settings','Налаштування платформи',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'NULL','Налаштування платформи будуть застосовані тільки для одного користувача',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'NULL','Будь ласка заповніть як мінімум {{limit}} промо матеріал. | будь ласка заповніть як мінімум {{limit}} промо матеріалa.',3,'validators')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'NULL','Будь ласка виберіть хоча б одну групу для копіювання',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Please choose at least one type','Будь ласка вкажіть як мінімум один тип розсилки',3,'validators')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'NULL','Будь ласка виберіть як мінімум один тип для копіювання',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'NULL','Будь ласка виберіть поле яке відповідає стовпчику',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Please choose the file','Будь ласка, виберіть файл',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Please choose the file','Будь ласка, виберіть файл',3,'validators')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'NULL','Будь ласка виберіть користувача щоб копіювати налаштування',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Please fill at least one agreement','Будь ласка заповніть як мінімум одну домовленість',3,'validators')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Please fill at least one attribute','Будь ласка виберіть як мінімум один атрибут',3,'validators')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Please fill at least one brand','Будь ласка заповніть як мінімум один бренд',3,'validators')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'NULL','Будь ласка заповніть хоча б одне поле',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Please fill at least one material','Будь ласка заповніть як мінімум один промо матеріал',3,'validators')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Please fill at least one preparation','Будь ласка заповніть як мінімум один препарат',3,'validators')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Please all promotions','Будь ласка заповніть промоції',3,'validators')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Please indicate the required columns','Будь ласка вкажіть обов''язкові стовпчики',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Please indicate the required columns','Будь ласка вкажіть обов''язкові стовпчики',3,'validators')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Please indicate which data relates to the columns from the file','Будь ласка вкажіть яким даними відповідають стовпчики з файлу',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'NULL','Будь ласка оберіть як мінімум одни стовпчик',3,'validators')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'NULL','Будь ласка виберіть значення зі списку',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Please provide the reason you want to stop receive messages','Вкажіть, будь ласка, причину відмови від отримання розсилки',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Please select at least one filter','Пожалуйста выберите как минимум один из фильтров',3,'validators')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Please select at least one filter first','Виберіть спочатку хоча б один фільтр',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'NULL','Будь ласка виберіть як мінімум одну групу',3,'validators')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'NULL','Будь ласка виберіть як мінімум одну цільову аудиторію',3,'validators')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Please select at least one target audience','Виберіть Принаймні одну цільову аудиторію',3,'validators')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'NULL','Будь ласка виберіть як мінімум одного користувача',3,'validators')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Please select brands and set promotion','Виберіть бренди і встановіть промоцію',3,'crm')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Please select client','Будь ласка виберіть контактну особу',3,'crm')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Please select companies','Оберіть установи',3,'crm')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Please select company','Будь ласка виберіть установу',3,'crm')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Please select contacts','Виберіть клієнтів',3,'crm')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Please select details preparations','Виберіть препарати',3,'crm')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'NULL','Будь ласка виберіть один з варіантів успадкування відповідальності',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Please select plan','Виберіть план',3,'crm')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Please select plan detail','Виберіть детальну інформацію про план',3,'crm')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Please select plan details','Виберіть деталі плану',3,'crm')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Please select presentation','виберіть презентацію',3,'crm')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Please select promo material','Виберіть рекламний матеріал',3,'crm')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Please select report','Будь ласка, виберіть файл',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Please select report','Будь ласка, виберіть файл',3,'validators')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Please upload a valid file','Будь ласка завантажте коректний файл',3,'validators')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Please upload at least one file','Завантажте принаймні один файл',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( '%fields%: "Please','indicate the following fields: %fields%"',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Point division applied','Застосовано розділення точок',3,'geomarketing')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Pointer','Вказівник',3,'geomarketing')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Pokr','Покриття',3,'crm')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Polyclinic','Поліклініка',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Polygon','Полігон',3,'geomarketing')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Polygon Name','Назва полігона',3,'geomarketing')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Polygon name was successfully saved','Ім''я полігону було успішно збережено',3,'geomarketing')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Polygons','Полігони',3,'geomarketing')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'polygons','Полігонів',3,'geomarketing')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Position','Посада',3,'change_requests')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Position','Посада',3,'crm')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Position','Посада',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Post code','Поштовий індекс',3,'crm')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Post-training','Післятренінг',3,'crm')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Post-training support','Підтримка після тренінгу',3,'crm')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Post code','Поштовий індекс',3,'change_requests')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Potential','Потенціал',3,'crm')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Potential','Потенціал',3,'geomarketing')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'NULL','Потенціал',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Prev month','Поп.місяць',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Preparation','Препарат',3,'crm')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Preparation','Препарат',3,'geomarketing')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Preparation','Препарат',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Preparation brand','Бренд',3,'crm')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Preparation brand','Бренд препарата',3,'geomarketing')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Preparation line','Лінія',3,'crm')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Preparation name','Назва препарату',3,'crm')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Preparation name','Назва препарату',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'NULL','Номер візиту',3,'crm')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Preparation','Препарат',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Preparations','Контроль',3,'crm')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Preparations','Препарати',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Preparations to control','Препарати для контроля',3,'crm')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Presentation','Презентація',3,'crm')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Presentation can not be deleted used in task clm view log','Презентація не знищена тому CLM використовувалася в clm log.',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Presentation can not be deleted used in task presentation','Презентація не знищена тому CLM використовувалася у візиті.',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Presentation can not be deleted used in task presentation slide','Презентація не знищена тому CLM використовувалася у візиті.',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'NULL','Назва',3,'crm')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Presentation Name','Назва презентації',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'NULL','Номер візиту',3,'crm')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Press enter to save','Натисніть Enter, щоб зберегти',3,'geomarketing')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'NULL','Дані поперед візиту',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Preview','Попередній перегляд',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Preview','Передперегляд',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'NULL','Результат минулого візиту',3,'crm')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Price','Ціна',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Price for all packages','Ціна за всі упаковки',3,'crm')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Price for all units','Ціна за всі одиниці',3,'crm')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Primary IMEI','первинний IMEI',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Privileges','привілеї',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Process','Обробити',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Processed','Оброблено',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Processing','Обробка',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Processing','Обробка',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Processing "Client sales"','Обробка "Клієнтські продажі"',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Processing "Region plan"','Обробка "Плану регіону"',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Processing "TLS"','Обробка "TLS"',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Processing "Week stocks"','Обробка "Залишки"',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Processing company synonyms...','Обробка синонімів установ ...',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Processing error','Помилка при обробці даних',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Processing file - %name%','Обробка файлу - %name%',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Processing is completed','Обробка завершена',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Processing synonyms','Обробка синонімів',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Processing...','Обробка ...',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Procurement stage','Етап закупівель',3,'crm')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Product','Продукт',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Product direction','Продуктовий напрямок',3,'crm')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'NULL','Продуктовий напрямок',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Product Name','Найменування продукції',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Profile','Профіль',3,'crm')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Profile','Профіль',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'NULL','Статус виконання',3,'crm')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Promo','Промо',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Promo material','Промо-матеріал',3,'crm')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Promo material: : {{message}}','промо-матеріал:: {{message}}',3,'crm')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Promo materials','Промо матеріали',3,'crm')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Promo materials','Промо-матеріали',3,'crm')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'NULL','Назва',3,'crm')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'NULL','Номер візиту',3,'crm')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Promotion','Промоція',3,'crm')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Promotions','Промоція',3,'geomarketing')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'NULL','Пропоноване значення',3,'crm')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Purchase number','Номер закупівлі',3,'crm')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Purchase state','Стадія закіпівлі',3,'crm')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Purchase type','Тип закупівлі',3,'crm')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Purchases','Закупівлі',3,'crm')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Q1','Кв1',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Q2','Кв2',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Q3','Кв3',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Q4','Кв4',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Quality Control','Контроль якості',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Quantity','Кількість',3,'crm')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Quantity','Кількість',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Quarter','Квартал',3,'geomarketing')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Quarter','Квартал',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Do you really want to ERASE all colored territories?','Ви дійсно хочете стерти всі кольорові території?',3,'geomarketing')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Delete polygon?','Видалити полігон?',3,'geomarketing')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'You change the polygon.<br>Save old change in company list?','Ви змінили полігон. <br> Враховувати старі зміни в списку установ?',3,'geomarketing')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Undo change?','Скасувати зміну?',3,'geomarketing')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Rating','Оцінка',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Read','Прочитано',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Saving...','Збереження ...',3,'geomarketing')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Saving changes...','Збереження змін ...',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Saving plan. Please wait...','Грануальний план зберігається. Будь ласка зачекайте...',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Schedule of MPI procurement','План-графік закупівлі МНН',3,'crm')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Search','Шукати',3,'crm')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Search','Пошук',3,'geomarketing')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Search','Знайти',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Search','Знайти',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Search for','Шукати',3,'crm')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Search for','Шукати',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Search for companies...','Пошук установ ...',3,'crm')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Search for presentations','Знайти презентації',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Search in brands','Шукати в брендах',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Search in companies','Шукати в установах',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Search selected','Шукати в обраних',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Search territories','Шукати по територіям',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'NULL','Пошук ...',3,'crm')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Searching synonyms','Пошук синонімів',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Searching...','Пошук ...',3,'crm')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Select','Виберіть',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'NULL','Вибрать все',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'NULL','Виберіть блоки для копіювання',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Select brand','вибрати бренд',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Select city','Виберіть місто',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Select company','Виберіть установу',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Select datefrom','З',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Select datetill','По',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'NULL','Виберіть презентацію',3,'crm')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'NULL','Виберіть промо матеріал',3,'crm')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Select Directory','Виберіть каталог',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Select distributor','Виберіть дистриб''ютора',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Select end date','Виберіть кінцеву дату',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'NULL','Оберіть стовпчик',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Select items','Виберіть пункти',3,'crm')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Select member','Виберіть учасника',3,'crm')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Select month','Виберіть месяц',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Select organization','Вкажіть організацію',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Select pattern','Виберіть шаблон',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Select period','Вкажіть період аналізу',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Select period','Виберіть період',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Select period type','Виберіть тип періоду',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'NULL','Будь ласка виберіть план',3,'crm')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'NULL','Будь ласка виберіть деталі плану',3,'crm')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Select preparation','Вкажіть препарат',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Select reason','Вкажіть причину',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Select region','Вкажіть регіон',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Select start date','Виберіть дату початку',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'NULL','Виберіть бренди та встановіть промоцію',3,'crm')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'NULL','Виберіть типи для копіювання',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Select user','Виберіть користувача',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Select user to view data','Виберіть користувача для перегляду даних',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'NULL','Виберіть кому скопіювати',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Selected','Вибрано',3,'geomarketing')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Selected directions','вибрані напрямки',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Selected regions','вибрані регіони',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Selected reports','вибрані звіти',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Selected user','вибраний користувач',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Send','Надіслати',3,'geomarketing')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Send','Надіслати',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Send data','Надіслати дані',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Send data on email','надсилання даних електронною поштою',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Send Date','Надіслати',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Send now','Надіслати зараз',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Send request','Надіслати запит',3,'crm')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Send test email','Надіслати тестовий лист',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Send test message','Надіслати тестове повідомлення',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Sender Name','Ім''я відправника',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Sent','Надіслано',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Sent on verification','надіслано на перевірку',3,'crm')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'September','Вересень',3,'crm')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'NULL','Вересень',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Serial Number','Серійний номер',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Services','Сервіси',3,'crm')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Set','Вибрати',3,'crm')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Set count','встановити кількість',3,'crm')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Set required fields','Встановити обов''язкові поля',3,'crm')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Enter the start and end date','Введіть дату початку та кінця',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Enter the start and end time','Введіть час початку та закінчення',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Sheet','Лист',3,'geomarketing')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Sheet approved by user','Лист, затверджений користувачем',3,'geomarketing')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Sheet will be approved for all subordinates','Лист буде затверджений для всіх підлеглих',3,'geomarketing')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Shop name','Назва аптеки',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Description','Короткий опис',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Shortify links','Вкорочувати посилання',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Show','Показати',3,'geomarketing')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'NULL','Показати',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Show all','Відобразити все',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Show all','Показати все',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Show all comments','Відображати всі коментарі',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Show all reports','Показувати всі звіти',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Show archived','Показати в архіві',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Show editor','Показати редактор',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Show example','Показати приклад',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Show inative','Показати неактивних',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Show more','Показати більше',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'NULL','Ознака',3,'crm')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Sign In','Увійти',3,'crm')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'NULL','Увійти',3,'crm')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Simple mode','Спрощений режим',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Site','Сайт',3,'crm')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Skip','Пропустити',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Skip untreated','Пропустити необроблений',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Sms','SMS',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'SMS body','SMS контент',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'SMS Distribution script','Сценарій SMS розсилки',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'SMS Template','шаблон SMS',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'SMS Title','Назва',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'NULL','Деякі рядки не були оброблені через невірні дані',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Something went wrong','Щось пішло не так',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Sorry but we couldn’t find the page you are looking for','Вибачте, але ми не знайшли потрібну сторінку',3,'crm')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Data source','Джерело',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Appeal Source','Джерело звернення',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'City','Місто',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Specialization','Спеціалізація',3,'change_requests')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Specialization','Спеціалізація',3,'change_requests')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Specialization','Спеціалізація',3,'crm')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Specialization','Спеціалізація',3,'geomarketing')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Specialization','Спеціалізація',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Specializations','Спеціалізація',3,'geomarketing')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Specializations','Спеціалізація',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Split by points','Ділити по точкам',3,'geomarketing')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Start import','Старт імпорту',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Starts with','Починається з',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Starts with','Починається з',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'State','стан',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Statistics','Статистика',3,'crm')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Statistics','Статистика',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'NULL','Статус',3,'crm')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Status','Статус',3,'geomarketing')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Status','Статус',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Status','Статус звернення',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Status verification','Статус верифікації',3,'crm')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'NULL','Статус верифікації',3,'crm')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'NULL','Чекає верифікацію',3,'crm')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'NULL','Відправлено на верифікацію',3,'crm')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'NULL','Верифіковано',3,'crm')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'NULL','Відхилено',3,'crm')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'NULL','Вимагає повторну перевірку',3,'crm')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'NULL','Не підлягає верифікації',3,'crm')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Step %step%','Крок %step%',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Stop balancing','Зупинити врівноваження',3,'geomarketing')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'NULL','Строка (1)',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'NULL','Строка (2)',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'NULL','Строка (3)',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Street','Вулиця',3,'change_requests')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Street','Вулиця',3,'crm')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Street','Вулиця',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Street','Вулиця будинок',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Street type','Тип вулиці',3,'crm')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Street type','Тип вулиці',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Street type','Тип вулиці',3,'change_requests')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'NULL','Рядок',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Subject','Тема',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Subjects','Тема',3,'crm')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Submit changes','Підтвердити зміни',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Submit current plan for approval?','Надіслати поточний план на затвердження?',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Submit for approval','Надіслати план на затвердження',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'NULL','Підлеглі',3,'crm')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Subordinates','Підлеглі',3,'geomarketing')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Subordinates','Підлеглі',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Subproduct direction','Суб продуктове напрямок',3,'crm')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Subscribe','Підписатися',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Suburb','Район',3,'change_requests')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Suburb','Район',3,'crm')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Done','Готово',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Successfully subscribed','Успішно підписалися',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Successfully unsubscribed','Успішно відписалися',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Sum','Сума',3,'geomarketing')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Sum of packages','Сума упаковок',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Summary table','Підсумкова таблиця',3,'geomarketing')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Supervisor','Супервайзер',3,'crm')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'NULL','Супервайзер',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'For %name% in time between %timeStart% and %timeEnd% no coordinates','Для %name% в часі між %timeStart% і %timeEnd% немає координат',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Supported files are','Допустимі формати',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Synonym','Синонім',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Synonyms','Синоніми',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Synonyms not found','Необроблені синоніми не знайдені',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'NULL','Системний довідник',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'NULL','Системне ім''я',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Tables','Таблиці',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Tags','Теги',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Target','Таргет-група',3,'crm')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Target Audience','Цільова аудиторія',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Target Audiences not found','Цільові аудиторії відсутні',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Target group','Таргет-група',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Target Name','назва ЦА',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Target practices','Цільові практики',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Target-group','Таргет-группа',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Targeting','Таргетування',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Task','Візит',3,'crm')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Task activity report','Звіт про візитну активності представників',3,'messages')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Task count','Кількість візитів',3,'crm')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ( 'Task is declined and can not be edited','Візит скасований і не може бути редагований',3,'crm')
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Targeting', 'Таретування', 3, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('team_soft_targeting.description', 'Формування найбільш потенційної клієнтської бази виходячи зі стратегії компанії', 3, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Cat. Calc.', 'Кат. Розр.', 3, 'targeting');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Cat. Appr.', 'Кат. Затв.', 3, 'targeting');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Patient flow', 'Потік пацієнтів', 3, 'targeting');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Potency.', 'Потенц.', 3, 'targeting');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Fact/potency.', 'Факт/потенц.', 3, 'targeting');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Institutions by plan', 'Установ по стратегії', 3, 'targeting');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Calls per quarter by plan', 'Візитів по стратегії за квартал', 3, 'targeting');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Found more than [[modalList.limit-1]] records. Refine your search criteria', 'Знайдено понад [[modalList.limit-1]] записів. Уточніть критерій для пошуку', 3, 'targeting');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Calls per month by plan', 'Візитів по стратегії за місяць', 3, 'targeting');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Doctors by plan', 'Лікарів по стратегії', 3, 'targeting');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Status Calc.', 'Статус розр.', 3, 'targeting');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Status Appr.', 'Статус затв.', 3, 'targeting');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Change', 'Змін.', 3, 'targeting');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Category', 'Категорія', 3, 'targeting');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Institutions(genitive)', 'Установ', 3, 'targeting');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Doctors(genitive)', 'Лікарів', 3, 'targeting');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Visits(genitive)', 'Візитів', 3, 'targeting');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('In Total', 'Разом', 3, 'targeting');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('New(genitive_plural)', 'Нових', 3, 'targeting');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Delete(genitive_plural)', 'Видалених', 3, 'targeting');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Modifications(genitive)', 'Модифікацій', 3, 'targeting');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Save', 'Зберегти', 3, 'targeting');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Download Excel', 'Завантажити Excel', 3, 'targeting');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Sorting', 'Сортування', 3, 'targeting');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Descending', 'За зменшенням', 3, 'targeting');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Ascending', 'По зростанню', 3, 'targeting');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Count', 'Кількість', 3, 'targeting');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Status', 'Статус', 3, 'targeting');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Institution name', 'Назва установи', 3, 'targeting');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('City', 'Місто', 3, 'targeting');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Address', 'Адреса', 3, 'targeting');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Turnover', 'Товарообіг', 3, 'targeting');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('turnover', 'товарообіг', 3, 'targeting');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Sales', 'Продажі', 3, 'targeting');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Segment', 'Сегмент', 3, 'targeting');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Visits for', 'Візитів за', 3, 'targeting');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Empty list', 'Список порожній', 3, 'targeting');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('New', 'Новый', 3, 'targeting');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Was previously', 'Раніше був', 3, 'targeting');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Deleted', 'Видалений', 3, 'targeting');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('No data', 'Немає даних', 3, 'targeting');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Add', 'Добавити', 3, 'targeting');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Delete', 'Видалити', 3, 'targeting');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Keep', 'Залишити', 3, 'targeting');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Search', 'Пошук', 3, 'targeting');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('To find', 'Найти', 3, 'targeting');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Filter', 'Фільтр', 3, 'targeting');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('pharmacy', 'аптека', 3, 'targeting');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Pharmacy', 'Аптека', 3, 'targeting');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Targeting', 'Таргетування', 3, 'targeting');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Search on territory', 'Пошук на території', 3, 'targeting');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('doctors', 'лікарі', 3, 'targeting');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Doctors', 'Лікарі', 3, 'targeting');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Loading', 'Іде завантаження', 3, 'targeting');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Filtered by', 'Відфільтровано по', 3, 'targeting');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Approve', 'Затвердити', 3, 'targeting');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Not found', 'Не знайдено', 3, 'targeting');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Searching', 'Йде пошук', 3, 'targeting');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Close', 'Закрити', 3, 'targeting');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Reject', 'Відхилити', 3, 'targeting');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('per quarter', 'за квартал', 3, 'targeting');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('per month', 'за місяць', 3, 'targeting');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Untitled', 'Без назви', 3, 'targeting');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Target-group', 'Таргет-група', 3, 'targeting');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Full name', 'ПІБ', 3, 'targeting');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('place of work', 'місце роботи', 3, 'targeting');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('All', 'Всі', 3, 'targeting');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Changed', 'Змінено', 3, 'targeting');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Not changed', 'Не измінено', 3, 'targeting');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('January', 'Січень', 3, 'targeting');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('February', 'Лютий', 3, 'targeting');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('March', 'Березень', 3, 'targeting');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('April', 'Квітень', 3, 'targeting');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('May', 'Травень', 3, 'targeting');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('June', 'Червень', 3, 'targeting');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('July', 'Липень', 3, 'targeting');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('August', 'Серпень', 3, 'targeting');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('September', 'Вересень', 3, 'targeting');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('October', 'Жовтень', 3, 'targeting');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('November', 'Листопад', 3, 'targeting');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('December', 'Грудень', 3, 'targeting');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Not approve', 'Не затверджений', 3, 'targeting');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Requires confirmation', 'Вимагає підтвердження', 3, 'targeting');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Approved', 'Затверджений', 3, 'targeting');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Are you sure you want to approve the base?', 'Ви точно хочете затвердити базу?', 3, 'targeting');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('List cannot be empty', 'Список не може бути порожнім', 3, 'targeting');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Delete plan detail preparation', 'Delete plan detail preparation', 1, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Delete plan detail preparation', 'Видалити вибраний бренд?',  3, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Delete plan detail promo', 'Delete plan detail promo', 1, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Delete plan detail promo', 'Видалити промо матеріал?',  3, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Promo name', 'Promo name', 1, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Promo name', 'Назва',  3, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Promo number task', 'Promo number task', 1, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Promo number task', 'Номер візиту',  3, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Workday', 'Workday', 1, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Workday', 'К-сть робочих днів',  3, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Delete plan', 'Delete plan', 1, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Delete plan', 'Видалити вибраний план,',  3, 'crm');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Export to Word', 'Export to Word',  1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Export to Word', 'Експорт в Word',  3, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Export to Word', 'Экспорт в Word',  2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Close', 'Close',  1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Close', 'Закрити',  3, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Close', 'Закрыть',  2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Update', 'Update',  1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Update', 'Оновити',  3, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Update', 'Обновить',  2, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Client''s name', 'Client''s name',  1, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Client''s name', 'Ім''я кліента',  3, 'messages');
INSERT INTO info_languageinterface ([key], translation, language_id, domain) VALUES ('Client''s name', 'Имя клиента',  2, 'messages');
